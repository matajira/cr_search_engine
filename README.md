# Christian Reconstruction Search Engine

![alt text](.img/crsearch.png "Fill your sudoku")

## Objective
Given the bast literature concerning Christian Reconstruction. I wanted to create a tool that allowed me to search for specific words or verses in all the books that compose the Christian Reconstruction literature (at least the ones present in garynorth.com). I also want in the future to be able to share this tool, hence I thought that creating a web page is appropriate.

I took advantage of this challenge to:
1. Learn and apply Teseract OCR to scan the pages of each book into text.
1. Learn and apply the basics of the Flask Microframework and HTML.
2. Learn and apply the ElasticSearch client for Python.

## Prerequisites
```
Python 3.82
docker-ce 19.03.8
docker-compose 1.25.0
Flask  1.1.2 (not necesarily the latest)
elasticsearch 7.7.0
```
## Running
```
./how_to_run.sh
```
Then, the Flask Development Service is exposed at localhost:8080

## Author
* Camilo MATAJIRA see www.camilomatajira.com
