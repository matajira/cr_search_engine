Prayer and Precept 99

God’s work in God's way, according to God’s will, or we will lack
for God’s blessing.

And we cannot expect to know God’s work or will or way if we
do not diligently seek Him,

Of course, we are to seek Him objectively and not subjec-
tively. We're not looking for “voices in the night.” We are to be ruled
by the Bible in all our seeking.

The law of the Lord is perfect, converting the soul; the testi-
mony of the Lord is sure, making wise the simple (Psalm 19:7).

The entrance of Your words gives light; it gives understanding
to the simple (Psalm 119:130).

For the commandment is a lamp, and the law is light; reproofs
of instruction are the way of life (Proverbs 6:23).

When we are ruled by prayer and precept, we are protected
and preserved from both the canker of indifference and the cancer
of extremism. When we are ruled by prayer and precept, we are
assured of God’s direction and God's anointing.

If My people who are called by My name will humble them-
selves, and pray and seek My face, and turn from their wicked
ways, then I will hear from heaven, and will forgive their sin and
heal their land (2 Chronicles 7:14).

Summary

Jesus called his disciples to be world-changers. They were to
take action. But the first action they were to take was to pray.

Nehemiah was a paradigm of Biblical political action who
modeled this priority on prayer in his own life and ministry.

Looking back over history, the disciples could readily see that
Nehemiah wove prayer into everything that he said or did.

He understood that prayer changes things. It is a powerful
force. It is a force for good.

But if prayer and politics sounds like a frightening combina-
tion, it is only because we have misunderstood the nature of Bibli-
cal prayer. It is noé subjective.
