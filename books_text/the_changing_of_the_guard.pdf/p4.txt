Copyright © 1987 by George Grant

All rights reserved. Written permission must be secured from the
publisher to use or reproduce any part of this book, except for
brief quotations in critical reviews or articles.

Published by Dominion Press
7112 Burns Street, Ft. Worth, Texas 76118

‘Dppesetting by Thoburn Press, Tyler, Texas
Printed in the United States of America

Unless otherwise noted, all Scripture quotations are from the
New King James Version of the Bible, copyrighted 1984 by
Thomas Nelson, Inc., Nashville, Tennessee.

Library of Congress Catalog Card Number 87-070996
ISBN 0-930462-27-0
