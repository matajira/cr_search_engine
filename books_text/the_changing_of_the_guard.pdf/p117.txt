The Challenge of the Compromise 85

slave—just as the Son of Man did not come to be served, but to
serve, and to give His life a ransom for many” (Matthew 20:25-28),

Thus the reason Daniel was able to square off against the
forces of evil without compromise involved not only what he knew,
but what he was. And ke was humble. His steadfastness was not
closed-minded obstinacy. Instead, his understanding of absolute
sovereignty, persistent opposition, and the promise of prison was
rooted in a true humility.

The fact is Daniel couldn't have done what he was able to do
had he not been humble, no matter how well he understood the
situations and circumstances swirling about him.

Living by faith, walking in steadfastness, and partaking of res-
urrection power is completely and entirely dependent on right-
eous humility. Recognizing and prospering amidst the reaction—
repression —resurrection pattern is utterly impossible apart from
godly meekness.

Hope from Prison

The Apostle Paul emphasized this truth when he wrote to the
besieged believers in Philippi. He reminded them that their stand
for the Gospel had to remain absolutely uncompromised, but in
doing so he outlined in broad brush strokes the Biblical reaction —
repression— resurrection pattern; he reminded them that it is God
who really rules men and nations (Philippians 1:12-26), and he
reminded them that prison is the first stage of dominion (Philip-
pians 1:1-11),

Once he had stated the necessity for steadfastness, he reiterated
that humility is the on/y means to attain to that steadfastness. The
Philippians were not to have any confidence in their own flesh
(Philippians 3:1-7). On the contrary, they were to imitate the Lord
Jesus:

Let this mind be in you which was also in Christ Jesus, who,
being in the form of God, did not consider it robbery to be equal
with God, but made Himself of no reputation, taking the form of a
servant, and coming in the likeness of men. And being found in
