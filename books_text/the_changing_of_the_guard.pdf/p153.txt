What the Church Should Do 121

order of multiple jurisdictions, separate but cooperating, under the sover-
eignty of God and the rule of His Law.

The current dogma of the “wall of separation” between
Church.and state is thus a far cry from our founding fathers’ in-
tent. It is, in fact, a denial of the multiplicity of institutions and
jurisdictions. It cripples the Church and exalts the state. It denies
the universal sovereignty of God over all institutions and asserts
the absolute authority of the state, It excludes believers from their
God-ordained ministry of social, cultural, and political involvement.

This “wall of separation” idea was slow to catch on in our na-
tion. Until the War Between the States erupted, Christianity was
universally encouraged at every level and dy every level of the civil
government. Then in 1861, urider the influence of the radical Uni-
tarians, the Northern Union ruled in the courts that the civil
sphere should remain “indifferent” to the Church. After the war,
that judgment was imposed on the Southern Confederation. One
hundred years later in 1961, the erosion of the American system of
Biblical checks and balances continued with the judicial declara-
tion that all religious faiths were to be “leveled” by the state. By
1963 the courts were protecting and favoring a new religion—“hu-
manism” had been declared a religion by the Supreme Court in
1940—while persecuting and limiting Christianity. The govern-
ment in Washington began to make laws “respecting an establish-
ment of religion” and “prohibiting the free exercise thereof.” It
banned posting the Ten Commandments in school rooms, allowed
the Bible to be read in tax supported institutions only as an histor-
ical document, forbade prayer in the public domain, censored
seasonal displays at Christmas, Easter, and Thanksgiving, regu-
Jated Church schools and outreach missions, demanded IRS reg-
istration, and denied equal access to the media. It has stripped the
Church of its jurisdiction and dismantled the institutional differ-
entiation the founding fathers were so careful to construct.

In light of these historical realities and in light of the ten
basic principles in the Biblical blueprint of political action, what
should the Church do? What should the Church do to rectify the
wrongs of a social and political system gone awry? What should
