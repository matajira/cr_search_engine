The Challenge of the Compromise 79

That was why Daniel refused to play the game.

He was a man of principle. He was a man of conviction. He
refused to be drawn into the petty world of party pandering and
peccant patronizing. He was a wise man who simply would not
sacrifice integrity for pragmatism.

He had been called into the ministry of political action dy
Almighty God (Daniel 1:4, 17-20) and so owed his first allegiance
ta Almighty God (Daniel 6:10-11). He would not, and he could
not, compromise that commitment, God’s will, God’s Law, God’s
purpose, and God’s agenda for men and nations were non-negoti-
able for him.

Now, this does not mean that Daniel was inflexible and moss-
backed. On the contrary, he was remarkably creative (Daniel
1:12-13). He was capable and discerning (Daniel 1:4). He was
learned and facile (Daniel 1:20). He was dedicated and teachable
{Daniel 1:17-18), He was selfless and discreet (Daniel 2:14-18), His
extraordinary wisdom and insight won for him the audience of
kings (Daniel 5:13-14), and his extraordinary piety and devotion
won for him the audience of hosts (Daniel 9:3-22). He was highly
esteemed by men (Daniel 4:18), by God (Daniel 9:23), and by
angels (Daniel 10:11).

Still, his unswerving commitment and his righteous deter-
mination were inimitable. He was forthright in his condemnation
of sin (Daniel 4:27). He was unguarded in his pronouncement of
truth (Daniel 5:13-28). He was single-minded in his adherence to
the Word of God (Daniel 6:5).

Like Josiah before him, Daniel

. . . did what was right in the sight of the Lord, and walked in all
the ways of his father David; he did not turn aside to the right hand
or to the left (2 Kings 22:2).

He dutifully obeyed the clear commands of Scripture to be
steadfast and unwavering:

Therefore be very courageous to keep and to do ail that is writ-
ten in the Book of the Law of Moses, lest you turn aside from it to
the right hand or to the left, . . . but you shall hold fast to the Lord
your God, as you have done to this day (Joshua 23:6, 8).
