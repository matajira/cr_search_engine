Sins of Commission 27

The enemies of Gad’s rule have always attempted to avoid the
implications of salvation by grace alone. They cannot stand the
thought of being at the mercy of God. And so they are constantly
dreaming up new versions of the same old heresy: salvation by
works, salvation by law, salvation by education, salvation by leg-
islation.

The Galatians had fallen under the sway of this heresy and
thus were “preaching another Gospel” contrary to that which they
had received (Galatians 1:9). They had turned the Law and the
Gospel into something that neither was ever intended to be.

The Purpose of the Law

According to the Bible, the Law is something that we obey
because we have been justified. We do not obey it in order to be. Obe-
dience to the Law is the effect of salvation, not the cause of salvation.
In other words, the Law is designed to be a tool of dominion and
sanctification, not the means of justification and redemption. It is a
way of life, not a way of salvation.

Jesus constantly upheld the validity of Gad’s Law as a guide
for living and an expression of the unchanging standards of His
rule:

Man shall not live on bread alone, but on every word that pro-
ceeds out of the mouth of God (Matthew 4:4).

It is easier for heaven and earth to pass away than for one
stroke of a letter of the Law to fail (Luke 16:17).

Whoever then annuls one of the least of these commandments,
and so teaches others, shall be called least in the Kingdom of

Heaven; but whoever keeps and teaches them, he shall be called
great in the Kingdom of Heaven (Matthew 5:19).

Again and again He affirmed the truth that “All Scripture is
God breathed (2 Timothy 3:16) and that it “cannot be broken”
(John 10:35), He did not come to do away with the Law—to abol-
ish or abrogate it. On the contrary, He came to fulfill it~to con-
firm and uphold it (Matthew 5:17). He reiterated the fact that
every one of “His righteous ordinances is everlasting” (Psalm
