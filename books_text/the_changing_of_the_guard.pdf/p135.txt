Facing the Odds 103

ises, stopped the mouths of lions, quenched the edge of the sword,
out of weakness were made strong, became valiant in battle, turned
to flight the armies of the aliens” (Hebrews 11:33-34). Though they
were mocked and persecuted, imprisoned and tortured, impover-
ished and dispossessed, they were unshaken and eventually ob-
tained God’s great reward (Hebrews 11:35-40).

And God has set these before us as examples: Abraham (Gen-
esis 12:1-4), Sarah (Genesis 18:11-14), Isaac (Genesis 27:27-29),
Jacob (Genesis 48:1-20), Joseph (Genesis 50:24-26), Moses
(Exodus 14:22-29), Rahab (Joshua 6:23), Ruth (Ruth 1:16-17),
Gideon (Judges 6:1-8:35), Barak (Judges 4:1-5:31), Samson
(Judges 13:1-16:31), Jephthah (Judges 11:1-12:7), David (1 Samuel
16:1-17:58), Isaiah (Isaiah 1:1-6:13), Samuel, and all the prophets
(1 Samuel 1:1-28; Hebrews 11:32).

Therefore we also, since we are surrounded by so great a cloud
of witnesses, let us lay aside every weight, and the sin which so eas-
ily ensnares us, and let us run with endurance, the race that is set
before us, looking unto Jesus, the author and finisher of our faith,
who for the joy that-was set before Him endured the cross, despis-
ing the shame, and has sat down at the right hand of the throne of
God (Hebrews 12:1-2).

By their example we can be and should be assured that we
are more than conquerors (Romans 8:37), overcomers (1 John
5:4), and victorious in Christ (1 Corinthians 15:57). By their ex-
ample we can be and should be assured that the future is ours.

Avoiding Mistakes

When Israel went up after the exodus to take possession of the
land, they too had examples of encouragement set before them.

They were very well aware of what God had said. They knew
His promises. They knew that the land was theirs for the taking
(Genesis 12:1-3), that it belonged to God, not to the Canaanites
(Psalm 24:1), and that God had entrusted it into ¢heir care ( Joshua
1:2). They knew that if only they would obey God’s Word and do
God’s work, they would be prosperous and successful (Joshua
