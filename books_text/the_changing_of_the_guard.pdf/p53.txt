Render Unio Him 2

ministry of political action (1 Samuel 16:1-13). Though he was the
youngest child in a family of poor shepherds (1 Samuel 16:11) and
despised by his own brethren (i Samuel 17:28), God raised him up
to be the greatest of Israel’s many kings (2 Samuel 2:4), a man
after God’s heart (1 Samuel 16:7).

There can be no doubt that God also called Josiah into the
ministry of political action (2 Kings 22:1). As the leader of his na-
tion, he restored the Word of God to its rightful place (2 Kings
23:3-15). He rid the land of idolatry and debauchery (2 Kings
23:19-24), and he made the name of the Lord great, serving Him
with all his heart and soul and might (2 Kings 23:25).

There can be no doubt that God also called Nehemiah into the
ministry of political action (Nehemiah 1:1-11). He utilized his posi-
tion of authority to remove the shame of his people (Nehemiah
2:1-20), to restore them to their proper stature (Nehemiah 4:1-23),
and to reestablish civil integrity in the land (Nehemiah 13:4-28).

There can be no doubt that God called Daniel into that minis-
try as well (Daniel 1:2-20). From his youth, God endowed him
with great wisdom, deep understanding, discerning knowledge,
and an ability for governmental service (Daniel 1:4), He studied
diligently (Daniel 1:18) and demonstrated faithfulness (Daniel
1:20), so that the cause of the Kingdom and the name of the Lord
were exalted throughout the land (Daniel 4:1-3).

Each of these heroes of the faith understood clearly that there was
no wall of separation between God and the state. They knew that
God had ordained and established civil government just as surely
as He had the family and the Church. So they looked upon political
action as a holy calling, a ministry, a sacred service before God.

They knew that:

When it goes well with the righteous, the city rejoices, and
when the wicked perish, there is glad shouting. By the blessing of
the upright a city is exalted, but by the mouth of the wicked it is
torn down (Proverbs 11:10-i1).

And that:

Righteousness exalts a nation, but sin is a disgrace to any peo-
ple (Proverbs 14:34),
