The Earth Is the Lord’s B

The American Legacy

In this regard it would stand us in good stead to recall our her-
itage. The founders and early leaders of the American republic
had no trouble whatsoever acknowledging the theocracy of
heaven and earth. Their political actions were thoroughly rooted
in the realization that God rules.

George Washington (who. was actually our eighth president, but
the first under the present constitution) added the pledge, “So help
me, God,” to his inauguration oath, and then stooped to kiss the
Bible as an affirmation of his submission to the rule of God. He
later asserted, “It is impossible to rightly govern the world without
God and the Bible.”

John Adams, the second president under the present constitu-
tion, made no secret of the fact that he studied the Bible often and
with diligence in order to discern the proper administration of
God's rule. He said, “Our constitution was made only for a moral
and religious people . . . so great is my veneration of the Bible
that the earlier my children begin to read it, the more confident
will be my hope that they will prove useful citizens of their coun-
try and respectful members of society.”

Thomas Jefferson, primary author of the Declaration of Inde-
pendence and the third president, was also quite forthright in his
acknowledgement of universal theocracy. He said, “The Bible is
the cornerstone of liberty . . , students’ perusal of the sacred volume
will make us better citizens, better fathers, and better husbands.”

Benjamin Franklin, the patriarch of the fledgling American re-
public, said, “A nation of well informed men who have been
taught to know the price of the rights which God has given them,
cannot be enslaved.”

William Penn, founder and governor of the Pennsylvania col-
ony, stated the case plainly when he said, “If we will not be gov-
erned by God, then we will be ruled by tyrants.”

Andrew Jackson, the country’s seventh president, read the Bible
daily in deference to God’s immutable reign, and referred to it as
“the Rock on which our republic rests.”
