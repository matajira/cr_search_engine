Reclaiming the Land 49

memorials to the Lord (Genesis 12:4-9). He met with God at
Shechem (Genesis 12:6), at Bethel (Genesis 12:8), at Hebron
(Genesis 13:18), and at Moriah (Genesis 22:2), where the Jeru-
salem temple would later be built (2 Chronicles 3:1), Abram con-
secrated each of these sites to the Lord, establishing them as
places of worship. He was thus, reclaiming the sanctuary.

Like the pattern of digression, this pattern of redemption
recurs throughout the Bible and throughout history. When Israel
returned to the land from the exodus they first reclaimed their do-
minion, then their home, and finally their sanctuary.

When Ezra and Nehemiah brought the people back from the
Babylonian captivity, the pattern went into effect again. Domin-
ion was reclaimed (Nehemiah 2:1-8), their home was reclaimed
(Nehemiah 6:15-16), and finally the sanctuary was reclaimed
(Haggai 1:12-15).

Whenever and wherever people respond to the grace of God,
we are simultaneously lifted up from our fallen estate and commis-
sioned to go forth and reclaim the land (Matthew 28:19-20;
Romans 8:19-22). With great, great privilege comes great, great
responsibility. We are to counter the pattern of digression with the
pattern of redemption.

(For these insights into the life of Abraham, I am indebted to
James B. Jordan’s fine study, The Life of Abraham, available from
Geneva Ministries, Box 131300, Tyler, Texas, 75713.)

The Task of Political Action

“The earth is the Lorp’s” (Psalm 24:1). That is clear enough.
He executes dominion over it with “an everlasting dominion” (Dan-
iel 4:3), and He rudes it from His “throne on high” (Psalm 11:4), But
at the same time, He graciously apportions it out to His people.
He commissions us to exercise stewardship over it. He gives us
land. And we are able to express His dominion by ordering and
subduing that land. He gives us an inheritance. And we are. to
affirm His rule by forging out of that inheritance a harmonious
world, a secure home, and a holy sanctuary.

This is the crux of Christian political action. This is the task
