154 The Changing of the Guard

ure out what you're talking about, your letter hasn’t done a whole
lot of good.

The same basic rules apply to letters to the editor or op-ed
pieces. Whatever you do, make your views heard. And make your
words count,

Working Within the System

The American political system was designed by our founding
fathers to be a decentralized, confederated, and theocratic repub-
lic. That means it was designed to operate from the bottom up. The
policies, procedures, and platforms of both major parties begin at
the precinct level.

Fewer than 3% of all voters ever even attend a precinct con-
vention or caucus. In most precincts, that means.-that all you have
to do is show up and you can shape national policy! A core group
of between five and ten individuals can almost always control a pre-
cinct meeting. And it is the precinct meeting that elects delegates,
sends resolutions, and formulates platforms for the district, state,
and national party conventions.

Go to your precinct convention prepared. Have your. resolu-
tions typed and printed in triplicate.

Get neighbors, friends, and church members in your precinct
together for a pre-caucus strategy session. Decide on a convention
chairman and an agenda that you all can vote for en masse. Then
go expecting to win.

Propose and push through resolutions calling for an immedi-
ate end to abortion, pornography, entrepreneurial regulation,
higher taxes, and all the other liberal humanist boondoggles of
our day. Work for a pro-life, pro-family, pro-business platform by
having specific recommendations to make.

But don’t make your political activism a one night stand, or
you will lose all credibility. Get involved in the party network.
Volunteer to work in the campaign headquarters. Work the phone
banks. Take a turn as a poll watcher. Go to the city Council meet~
ings. Join the PTA. Get involved. Remember: dominion comes
through service.
