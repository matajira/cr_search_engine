80 The Changing of the Guard

Therefore, my beloved brethren, be steadfast, immovable,
always abounding in the work of the Lord, knowing that your
labor is not in vain in the Lord (4 Corinthians 15:58).

And do not turn aside; for then you would go after empty
things which cannot profit or deliver, for they are nothing. For the
Lord will not forsake His people, for His great name’s sake,
because it has pleased the Lord to make you His people. Only fear
the Lord, and serve Him in trath with all your heart; for consider
what great things He has done for you. But if you still do wickedly,
you shall be swept away, both you and your king (1 Samuel
12:21-22, 24-25).

A stand like that can be costly though, and it nearly cost Dan-
iel everything.

Think of it, He had power (Daniel 6:3). He had influence
(Daniel 5:12). He had prominence (Daniel 5:14). But he risked it
all for the sake of conscience.

A simple compromise would have preserved his power, influ-
ence, and prominence. But he 7¢fused to compromise.

He could have tried to work within the system, He could have
tried to wait out the edict. He wouldn’t have had to deny his faith,
just keep it quiet for a while. He could have tried conciliation, ac-
commodation, or negotiation. Why waste everything that he had
gained over such a small matter? Why not just play along, at-
tempting to do good as the opportunity presented itself?

Daniel refused to compromise, risking prison and even death.
He refused for three reasons.

Loyalty to God
First, Daniel understood who really governs men and nations,

He did not need to tremble before mere human edicts. God and
God alone directs the ebb and flow of history:

The king’s heart is in the hand of the Lord, like the rivers of
water; He turns it wherever He wishes (Proverbs 21:1).

For I know that the Lord is great, and our Lord is above all
gods, Whatever the Lord pleases He does, in heaven and in earth,
in the seas and in all deep places (Psalm 135:5-6).
