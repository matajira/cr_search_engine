70 The Changing of the Guard

(1 Samuel 17:32). He showed courage in his service in the king’s
court (1 Samuel 18:1-14), in battle against the Philistines (1 Samuel
18:20-30), as a fugitive from Saul (1 Samuel 23:1-29), and in his
dying days (1 Kings 2:1-11). It was his courage that enabled him
honorably to oppose the king.

Once again though, this courage was not a special character
trait unique to David. On the contrary, he was simply submitting
to the ethical standard for ail of Gad’s people:

The wicked flee when no one pursues, but the righteous are
bold as a lion (Proverbs 28:1).

God is our refuge and strength, a very present help in trouble.
Therefore we will not fear, though the earth be removed, and
though the mountains be carried into the midst of the sea; though
its waters roar and be troubled, though the mountains shake with
its swelling (Psalm 46:1-3).

For God has not given us a spirit of fear, but of power and of
love and of a sound mind (2 Timothy 1:7).

Each of us is to be courageous, fearless, and brave (Isaiah
12:2). Because we know that God is sovereign, we are to be cour-
ageous (2 Chronicles 32:7). Because we know that God is ever
present, we are to be courageous (Psalm 118:6). We are to be
brave in the face of our enemies (Deuteronomy 31:6) and brave in
the midst of chastisement (Job 5:17-24). We are to show valor in
obedience to the Word of God ( Joshua 23:6) and for the sake of
His people (2 Samuel 10:12). In all our service to the Lord we are
ever to be courageous (1 Chronicles 28:20), even when we are ter-
rified (Psalm 91:5) or dismayed (Joshua 10:25).

Courage in political action protects us from weak-willed and
godless men who exploit our people and abuse our resources in
exchange for mere token rhetoric. But it also protects us from re-
treatism and neutralism.

Courage means standing against the tide, struggling for right
to the bitter end, and investing our all-in-all for the cause of the
Kingdom.

David knew that.

So should we.
