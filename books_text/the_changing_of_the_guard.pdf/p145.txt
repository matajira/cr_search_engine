Conclusion 113

stances. Yet, we must not in the same breath commit an equal and
opposite error: antinomian lawlessness. The Law is good in that it
reveals the moral standards.of God’s rule, it convicts us of sin and
leads us to Christ, it is a testimony to the nations, calling them to
repentance, and, finally, it is a blueprint for living, a means for at-
taining our promised victory. Thus, as a provision of grace, the
Law is to be utilized as the ethical standard for any and all Chris-
tian political action (see Chapter Three).

The second immediate and practical implication of God’s es-
tablished ethical decree for political action is strategic. We must
not compromise. We must meet the challenge of politics with the
same commitment to Biblical surety that we convey in every other
sphere of life and Godliness. We must not waffle or waver when
God's purposes are at issue. Of course, our steadfastness needs to
be couched in humility and integrity, but never are we to senti-
mentally dull the edge of God’s Word to man (see Chapter Eight),

Judgment

God judges sin. When a society refuses to acknowledge His
sovereign rule, when it revolts against His ordering structure,
when it spurns His ethical standards, it invites God’s wrath. ‘This
is an inescapable principle: a nation reaps what it sows. If it sows
obedience to God, it will reap blessings and abundance. But if it
sows disobedience to Gad, it will reap judgment and paucity.
God’s sanctions are universal and immutable.

Christian political action recognizes the principle of judgment
and works to protect the land from sin’s consequences. It is the ul-
timate “strategic defense initiative.”

This has two immediate and very practical implications:

The first is legal. The people of God are commissioned to be
priests guarding the land. We are to protect society with our own
righteousness—in word and in deed. Without that preserving and
restraining juridical activity, our society is doomed. Without that
seasoning and sanctifying mediatorial activity, our nation will
perish. We must stand in the gap as Christ’s ambassadors, thus in-
tegrating politics and faith (see Chapter Four).
