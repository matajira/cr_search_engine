162 The Changing of the Guard

—__.. LifeLighi; The Bible and the Sanctity of Human Life
(Ft. Worth, Texas: Dominion Press, 1987).

James B. Jordan, Entangling Alliances: Christianity and International
Relations (Fort Worth, Texas: Dominion Press, 1987).

————____. judges: God’s War Against Humanism (Tyler, Texas:
Geneva Ministries, 1985).

—_____. The Law of the Covenant: An Exposition of Exodus
21-23 (Tyler, Texas: Institute for Christian Economics, 1984).

Vishal Mangalwadi, Tiuth and Social Reform (New Delhi, India:
Nivedit Books, 1986).

Gary North, Inherit the Earth (Fort Worth, Texas: Dominion
Press, 1987).

______. Liberating Planet Earth (Fort Worth, TX: Domin-
ion Press, 1987).

——_______. Moses and Pharaoh: Dominion Religion Versus Power
Religion (Tyler, Texas: Institute for Christian Economics, 1985).

—________. Unconditional Surrender (Tyler, Texas: Geneva
Divinity School Press, 1982).

—_____... The Sinai Strategy: Economics and the Ten Command-
ments (Tyler, Texas: Institute for Christian Economics, 1986).

Dennis Peacocke, Christ the Liberator of the Nations (Tustin, Califor-
nia: Metz Communications, 1987).

Mary Pride, The Big Book of Home Learning (Westchester, [linois:
Crossway Books, 1986).

The Child Abuse Industry (Westchester, Hlinois:
Crossway Books, 1986).

Rushdoony, R. J., The Institutes of Biblical Law (Nutley, New Jer-
sey: Craig Press, 1973).

——_______. The Nature of the American System (Tyler, Texas:
Thoburn Press, 1978).

_____.__.. Politics of Guilt and Pity (Tyler, Texas: Thoburn
Press, 1978).

 
