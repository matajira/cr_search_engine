The Standard of Excellence 359

Let them praise the name of the Lord, for His name alone is
exalted; His glory is above the earth and heaven (Psalm 148:13).

Therefore God also has highly exalted Him and given Him the
name which is above every name, that at the name of Jesus every
knee should bow, of those in heaven, and of those on earth, and of
those under the earth, and that every tongue should confess that Jesus
Christ is Lord, to the glory of God the Father (Philippians 2:9-11).

His lovingkindness is excellent:

Your mercy, O Lord, is in the heavens, and Your faithfulness
reaches to the clouds. Your righteousness reaches to the clouds.
Your righteousness is like the great mountains; Your judgments
are a great deep; O Lord, You preserve man and beast. How ex-
cellent is Your lovingkindness, O God! Therefore the children of
men put their trust under the shadow of Your wings. Oh, continue
Your lovingkindness to those who know You, and Your right-
eousnss to the upright in heart (Psalm 36:5-7, 10).

Because Your lovingkindness is better than life, my lips shall
praise You. Thus I will bless You while I live; I will lift up my
hands in Your name (Psalm 63:4-5).

His greainess and power are excellent:

Praise the Lord! Praise God in His sanctuary; praise Him in
His mighty firmament! Praise Him for His mighty acts; praise
Him according to His excellent greatness! (Psalm 150:1-2).

Blessed are You, Lord God of Israel, our Father, forever and
ever. Yours, O Lord, is the greatness, the power and the glory, the
victory and the majesty; for all that is in heaven and in earth is
Yours; Yours is the Kingdom, O Lord, and You are exalted as
head over all. Both riches and honor come from You, and You reign
over all. In Your hand is power and might; in Your hand it is to
make great and to give strength to all. Now therefore, our God, we
thank You and praise Your glorious name (1 Chronicles 29:10-13).

His salvation is excellent:

And in that day you will say: “O Lord, I will praise You;
though You were angry with me, Your anger is turned away, and
You comfort me, Behold, God is my salvation, I will trust and not
