Sins of Omission 39

(Isaiah 30:24). It was symbolic of loyalty (Numbers 18:19), discre-
tion (Colossians 4:6), purity (Mark 9:49), and perpetuity
{2 Chronicles 13:5). It was used as a medicine (Ezekiel 16:4) and as
a preservative (Exodus 30:35). Interestingly, it was also the man-
datory accompaniment of some of the priestly sacrifices, including
the cereal (Leviticus 2:13) and burnt offerings (Ezekiel 43:24).

When Jesus told His disciples that they were to be “the salt of
the earth” (Matthew 5:13), the priest/y connotations were inescapa-
ble; they were to guard the nations with their covenantal loyalty,
discretion, purity, and perpetuity. They were to be the medicine
of hope; they were to be a preserving agent, restraining sin; they
were to be living sacrifices.

The survival of any nation depends entirely on the fulfillment
of the disciple’s priestly duties, If the people of Gad fail to be salt,
then the nation cannot be preserved from putrefaction, and it will
fall into judgment.

It is not enough for Christians simply to recognize the rule of
God and to avoid sins of commission. They must avoid the sins of
omission as well, actively guarding the nations from the cor-
ruption of sin. They must be salt, serving as true priests of the
Living God,

Notice that when God describes the priestly task of His peo-
ple, He invariably uses egal or governmental terminology: We are to
be ambassadors (2 Corinthians 5:20), judges (1 Corinthians 6:3),
rulers (Ephesians 1:11), and witnesses (Acts 1:8).

Notice too that this language is not simply symbolic. All too
often when God wants to preserve the earth, He puts His priests
in positions of political prominence: Joseph (Genesis 42:6), Nehe-
miah (Nehemiah 2:1), Mordecai (Esther 10:3), and Daniel (Dan-
iel 6:25-28).

Clearly then, one aspect of our priestly work is to preserve the
political realm from corruption with our presence and participa-
tion. We are to be salt.

The American Theocracy
Even the most cursory examination of the thousands of tracts,
pamphlets, essays, broadsides, and leaflets during the formative
years of our republic shows the determination of our founding
