60 The Changing of the Guard

be afraid; For YAH, the Lord, is my strength and my song; He
also has become my salvation.” And in that day you will say:
“Praise the Lord, call upon His name; declare His deeds among
the peoples, make mention that His name is exalted. Sing to the
Lord, for He has done excellent things; this is known in all the
earth” (Isaiah 12:1-2, 4-5).

Your right hand, O Lord, has become glorious in power; Your
right hand, O Lord, has dashed the enemy in pieces, And in the
greatness of Your excellence You have overthrown those who rose
against You; You sent forth Your wrath which consumed them like
stubble, Fear and dread will fall on them; by the greatness of your
arm they will be as still as a stone, till Your people pass over, O
Lord, till the people pass over whom You have purchased (Exodus
15:6-7, 16).

His will is perfect:

God is clothed in fearful splendor: He, Shaddai, is far beyond
our reach, Supreme in power, in equity, excelling in justice and in
willful purpose toward ‘men ( Job 37:22-23).

And do not be conformed to this world, but be transformed by

the renewing of your mind, that you may prove what is that good
and acceptable and perfect will of God (Romans 12:2).

His great and mighty deeds are excellent:

Sing to the Lord, for He has done excellent things; this is
known in all the earth. Cry out and shout, O inhabitant of Zion,
for great is the Holy One of Israel in your midst! (Isaiah 12:5-6),

Great and marvelous are Your works, Lord God Almighty!
Just and true are Your ways, O King of the saints! Who shall not
fear You, O Lord, and glorify Your name? For You alone are holy.
For all nations shall come and worship before You, for Your judg-
ments have been manifested (Revelation 15:3-4).

His ways are excellent:
He has made His counsel excellent and His wisdom great (Isa-
iah 28:29, NASV).

As for God, His way is perfect; the Word of the Lord is proven;
He is a shield to all who trust in Him. For who is God, except the
Lord? And who is a rock, except our God?” (2 Samuel 22:31-32).
