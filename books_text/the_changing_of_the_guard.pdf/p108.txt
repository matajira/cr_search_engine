76 The Changing of the Guard

O my God, I trust in You. Let me not be ashamed. Let not my
enemies triumph over me. Indeed, let no one who waits on the
Lord be ashamed (Psalm 25:2-3).

Conclusion

The seventh basic principle in the Biblical blueprint for political
action is that we must pose an honorable opposition to the powers
that be. Our politics must be thoroughly ethical.

Tf we do the right thing in the wrong way, we will not succeed,
nor will we glorify the name of Christ.

‘We must oppose the enemies of the kingdom, but we must op-
pose them honorably. We must learn to fight fair. Like David, we
must develop a pattern of righteous resistance.

‘We must be alert. It is essential that we be aware and informed.

‘We must be steadfast. Our convictions must be unmovable
and unshakable.

We must be brave. Fear and trembling before men have no
place in our agenda.

‘We must be strong. God has endowed us with His might. Now
we must use it,

And we must be loving. A deep respect for authority, office,
and influence must mark our every thought, word, and deed.

In short, the ethical standards we carry into the political
sphere must go well beyond mere honesty and sincerity. It is not
enough to know that God desires for us to be salt and light,
reclaiming the land through uncompromising conviction and un-
deterred excellence. We must oppose the forces of darkness with
patience and honor.

Summary
David could have easily killed King Saul and claimed the
throne of Israel for himself.
But he didn’t. He was an honorable man.
Like the honorable man the Apostle Paul describes (1 Corin-
thians 16:13-14), David demonstrated his honor in five ways.
First, he was alert. He was aware and informed.
