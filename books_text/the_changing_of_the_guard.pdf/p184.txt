152 The Changing of the Guard

The Pen Is Mightier Than the Sword

Once an election is over, the battle has only just begun.

The pen is mightier than the sword. Or, so they say. And even
a cursory glance at the vast annals of history seems to bear that
out, not merely as truism, but as the truth.

What do Plato and Marx, Thucidides and Newton, Calvin
and Darwin, Locke and Hobbes, Luther and Rousseau, Augus-
tine and Hegel all have in common? They all shook up the world,
changed the world with a mere stroke of the pen. . . some for
great good, some for great evil. Instead of wielding sword and
spear, they parried and reposted on the printed page. Instead of
commanding cavalries and leading assaults, they wrote. They
wrote books and pamphlets. They wrote articles and tracts. They
wrote letters and manifestos. They gripped the world and mar-
shalied legions not with saber rattling, but with words. Words and
ideas.

And what words! Words to topple kings and queens and pres-
idents. Words to spark revolutions. Words to transform cultures,

At a time when Christians in America are calling for massive
changes in the moral ecology of the land, in the ideological foun-
dations of the culture, it is odd to note our obvious in-attention
and ill-attention to words. Only one out of every ten Christians
has ever written his representative in Congress. Only one out of
every twenty has ever written a letter to the editor of his local
newspaper, Only one out of every 700 has ever written an article
or tract or pamphlet. This, despite the fact that the issues at hand
are life and death issues. This, despite the fact that assaults
against liberty, justice, and truth have grown to crisis proportions.
This, despite the fact that one or two well-written, well-timed let-
ters can turn the tide, wielding tremendous influence.

Perhaps it’s time for Christians to drown the enemies of the
Gospel—in a sea of ink. Perhaps it’s time for Christians to bom-
bard them—with words. Perhaps it’s time to prove once again
that indeed the pen és mightier than the sword.

Make a difference.

Write your State Senators and Representatives. Contact your
