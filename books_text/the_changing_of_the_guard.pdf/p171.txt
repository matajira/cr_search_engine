What the Family Should De 139

Issachar very obviously were taught, while still quite young, the
way they should go—so, when they were old, they did not depart
from it (Proverbs 22:6).

Notice the familiar refrain in the family lives of Moses (Ex-
odus 2:1-10), Samson (Judges 13:2-25), David (Ruth 4:13-22),
John the Baptist (Luke 1:5-25), Paul (Philippians 3:4-8), and
Timothy (2 Timothy 3:14-15). Like these other godly men of valor,
the sons of Issachar knew the joys of righteous homes. And the na-
tion was enhanced immeasurably by it.

The sons of Issachar “understood the times.” They “knew what
Israel should do.” And they did it. As a family.

What can the family do to restore our nation’s godly founda-
tions? How can it contribute to the ministry of political action?

It can create a righteous home atmosphere.

It can inculcate holiness.

It can build sturdy foundations of discipline, nurture, and
love.

Tt can saturate its progeny with the Word of Truth and Life.

In short, it can put all the raging issues of our day into per-
spective,

Putting Issues in Perspective

Thankfully, the long and protracted feud between Christian
families and social action is now all but over.

Unfortunately, the cease-fire was predicated on something
other than full Biblical authority.

Over the last several years, our families have awakened from a
carefully sequestered cultural sleep . . . in horror, We saw the
awful consequences of pagan humanism in unchecked promiscu-
ity, runaway materialism, no-fault easy divorce, and abortion on
demand. We saw the need to do something more than simply
snatch brands from the flickering flames of hell. We saw the need
for action.

As commendable as this action has been, it was and is estab-
lished upon woefully weak premises. As necessary as the action has
been, it was and is established upon pitifully inadequate theology.
