Sins of Commission 33

all would be well with the world. Christians do not believe in saiva-
tion by legislation or salvation by politics. To believe that would
be to repeat the Galatian error. To believe that would be to mimic
the sins of Gain, Balaam, and Korah. To believe that would be to
invite statist tyranny.

Christian political action does not put its trust in legal, judi-
cial, or mechanical messiahs. Its trust is in the one true Messiah,
“the only mediator between God and man” (1 Timothy 2:5), Jesus
Christ.

Thus, Christian political action is a recognition and submis-
sion to the rule of God. It is a rendering unto Him the things that
are His, and above all it is a visible manifestation in the civil
sphere of the grace of God.

Conclusion

The third basic principle in the Biblical blueprint for political
action is that salvation by Law is the most ancient of all heresies.
It is an Old Testament heresy and a New Testament heresy.

Man is saved “by grace through faith” (Ephesians 2:8). Any
attempt to bypass that fundamental reliance on the mercy of God
is the most rank form of rebellion.

The Law was intended by God to be a way of life for us, nof a
way of salvation. It was designed to be the effect of salvation, not
the cause of it, That is why unlike the Communists or the modern
liberals, Christians do not rely on an all-encompassing messianic
state to solve all the problems of society. Christians do not believe
in the power of legislation, but in the power of God.

Thus, Christians are politically active because of salvation, not
in order to provoke salvation.

Summary
The Galatian church, bewitched by Jewish legalists, began to
believe that in order to be saved, men must do something more than
trust the mercy and grace of God. They began to believe in salva-
tion by Law.
Salvation by Law is a heresy. All through the Bible from Gen-
