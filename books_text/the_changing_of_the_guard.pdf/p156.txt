124 The Changing of the Guard

Let My people go, that they may hold a feast to Me in the wil-
derness (Exodus 5:1).

And again:

Let My people go, that they may serve Me in the wilderness
(Exodus 7:16).

As theologian David Chilton has written, “We know the story
of Israel. God forces Pharaoh to release them, and they went on to
inherit the Promised Land. But the really crucial aspect of the
whole Exodus event, as far as the peopie’s activity was concerned,
was worship.” And so it continues to be today. Chilton concludes,
“The orthodox Christian faith cannot be reduced to personal ex-
periences, academic discussions, or culture-building activity —as
important as all these are.in varying degrees. The essence of Bibli-
cal religion is the worship of God. . . . True Christian reconstruc-
tion of culture is far from being simply a matter of passing Law X
and electing Congressman Y. Christianity is not a political cult. It
is the divinely ordained worship of the Most High God.” (Paradise
Restored, Tyler, Texas: Reconstruction Press, 1985).

Notice what happens when God’s people forget this very fun-
damental truth:

In the second year of King Darius, in the sixth month, on the
first day of the month, the word of the Lord came by Haggai the
prophet to Zerubbabel the son of Shealiel, governor of Judah, and
to Joshua the son of Jehozadak, the high priest, saying, “Thus
speaks the Lord of hosts, saying: ‘This people says, “The time has
not come, the time that the Lord’s house should be built.”’”

Then the word of the Lord came by Haggai the prophet, say-
ing, “Is it time for you yourselves to dwell in your paneled houses,
and this temple to lie in ruins?” Now therefore, thus says the Lord
of hosts: “Consider your ways! You have sown much, and bring in
little; you eat, but do not have enough; you drink, but you are not
filed with drink; you clothe yourselves, but no one is warm; and
he who earns wages earns wages to put into a bag with holes.”

“Thus says the Lord of hosts: ‘Consider your ways! Go up to
the mountains and bring wood and build the temple, that I may
