138 The Changing of the Guard

When the Family Succeeds

Just as Samuel's family failure had a dramatic and negative
impact on the health of the entire nation, so Issachar’s family suc-
cess had a dramatic and positive impact on the health of the entire
nation.

Issachar was the ninth son of Jacob, the fifth of Leah (Genesis
30:68; 35:23). The family descended from him consisted of four
great tribal clans: the Tolaites, the Punites, the Joshubites, and
the Shimranites (Numbers 26:23-24). At Sinai they numbered
54,000 (Numbers 1:29), but by the end of the desert wandering,
when the people were counted at Kadesh Barnea, their popula-
tion had swollen to 64,300 (Numbers 26:25).

Issachar’s family was not merely huge. It was also a godly
family.

The sons of Issachar were men. They had understanding of the
times, and knew what Israel ought to do (1 Chronicles 12:32),

The sons of Issachar included such heroes of the faith as Tola,
the deliverer and judge (Judges 10:1-2), and Barak, the com-
mander of Deborah's army (Judges 4:6-10; 5:1).

The sons of Issachar were among those named who rose up
courageously against Sisera and Jabin (Judges 5:15), and who
fought bravely beside David during the Ziklag exile (1 Chronicles
12:19-40).

So great was their valor, that the name of the sons of Issachar
will forever be enshrined above one of the gates in the New Jeru-
salem (Ezekiel 48:33).

Such a wealth of godliness and faithfulness does not mate-
rialize in a family by chance. Family purity does not occur in a
vacuum.

The sons of Issachar very obviously were nurtured in the ad-
monition of the Lord (Ephesians 6:4). The sons of Issachar very
obviously were raised up by godly parents who set the command-
ments of God upon their hearts, who talked about them when
they sat at home, when they walked along the way, when they lay
down, and when they rose up (Deuteronomy 6:6-7). The sons of
