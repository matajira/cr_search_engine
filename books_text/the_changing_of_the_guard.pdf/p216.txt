184 The Changing of the Guard

Ethics, 65-77
Evangelical ghetto, 3, 122123
Evangelismn, 19, 61
Examples, 101-108
Excellence, 19-61

Pamily, 3, 19, 134-144
Fowler, Gene, ix

Frank, Douglas, xxvii
Franklin, Benjamin, 13, 14
Free Masonry, 40

French Revolution, xxvii
Fund raising, 143-144

Gay rights, 140
Godliness, 54-55
Goldwater, Barry, 149
Guardians, 35-41

Hall, Verna M., 40

Hawkins, Paula, 145

Healing, 95

Hebrew Republic, 40-41

Hegel, G. W. F, 152

Heresy, 25-34

HHS, 127

Hitler, Adolf, 145

Hobbes, Thomas, 152

Homeland, 45, 46, 48

Honorable opposition, 65-77

Human Rights, 139-141

Humanism, 3, 4, 5, 23, 121,
141-143

Humility, 83-85

Hunt, Dave, xvi

Hyneman, Charles, 40

Illiteracy, 141-143
Imprecatory Psalms, 125
IRS 501 (c} (3), 132

Jackson, Andrew, 13
Jefferson, Thomas, 13, 14

Jordan, James B., 49
Jurisdictions, 119-122, 134-135
Kennedy, John, 145

Kline, Meredith, xxix

Land, 43-52

Landslide elections, 145
Law, 27-31

Laxalt, Paul, 145
Leadership, 53-64
Legalism, 25-34

Letter writing, 152-154, 167-168
Liberal humanism, 4, 5, 32
Lincoln, Abraham, 14
Locke, John, 152

Love, 73-74

Loyalty, 80-81

Luther, Martin, 152

Lutz, Donald, 40

Magic, 32

Malediction, 125

Marx, Karl, 152
McGovern, George, 145
Media, 3, 23, 148-151
Mediocrity, 61-62
Midterm elections, 145
Mistakes, 101-108
Models, 101-108
Motivation, 30
Multiple jurisdictions, 121

National Education Association, i27,
141-143

National Organization for Women,
128, 156

Nazi Party, 145

NBA, 127, 141-143

Newton, John, 152

Nisbet, Robert, xxv, xxvi

Nixon, Richard, 145

North, Gary, xii, xiii, 44

NOW, 128, 156
