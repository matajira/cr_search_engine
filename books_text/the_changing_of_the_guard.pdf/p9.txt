ACKNOWLEDGEMENTS

“Writing is painfully easy,” Gene Fowler was wont to say, “All
you do is stare at a blank sheet of paper until drops of blood form
on your forehead.”

Fortunately, a number of kind-hearted souls coaxed me
through the agony and ecstasy of this book project, and as a result,
my hemoglobin loss was minimal. David Dunham, Jim Jordan,
Kemper Crabb, Frank Marshall, Gary DeMar, Dave Marshall,
and Brian Martin all afforded me transfusions of wisdom, encour-
agement, and joy when I most needed them. Kathe Salazar, J. D.
McWilliams, and Suzanne Martin mopped my brow and nursed
me along, all the while taking care of business and holding down
the fort. Great thanks is here given to each of these dear friends
and fellow-workers in the Kingdom.

Of course, even with the valiant efforts of all these, completion
of this book still would have been impossible were it not for the
blood of my blood, the love of my love: Karen, Joel, Joanna, and
Jesse. No expression of gratitude can even begin to approach ade-
quacy. And sa:

“T thank my God always concerning you for the grace of God
which was given to you by Christ Jesus, that you were enriched in
everything by Him in all utterance and all knowledge, even as the
testimony of Christ was confirmed in you, so that you come short
in no gift, eagerly waiting for the revelation of our Lord Jesus.
Christ, who will also confirm you to the end, that you may be
blameless in the day of our Lord Jesus Christ. God is faithful, by

ix
