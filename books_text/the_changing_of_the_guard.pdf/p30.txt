XXX The Changing of the Guard

Simple, isn’t it? Yet it has implications beyond your wildest
imagination. Here is the key that unlocks the structure of human
government. Here is the structure that Christians can use to ana-
lyze church, state, family, and numerous other non-covenantal
but contractual institutions.

It can be used to unlock the long-debated structure of the Ten
Commandments; 1-5, with a parallel 6-10. I spotted this almost as
soon as Sutton described his discovery to me, just as I was finish-
ing my economic commentary on the Ten Commandments, The
Sinai Strategy (Institute for Christian Economics, 1986), which I
outlined in the Preface. It can also be used to make sense out of
some of the basic concepts of economics, as I show in my book in
the Biblical Blueprints Series, Inherit the Earth (1987). In fact, once
you begin to work with this model, it becomes difficult not to see it
everywhere you look. This means that this model is either very
powerful or very hypnotizing.

Where the intellectual payoff really gets high is in the study of
government. Gary DeMar did not deliberately structure his first
draft of Ruler of the Nations (1987) around Deuteronomy’s five-point
covenant model. Nevertheless, as I read it, I recognized the five
points. I simply moved his chapters around. He had already cov-
ered the basic topics of government that the five-point model
reveals: in two sets of five chapters. Once again, we see the power
of this covenant model. Without deliberately imitating it, DeMar
asked the questions raised by the covenant model. He just didn’t
originally ask them in the covenant model's order. I had almost
the identical experience in editing George Grant’s book.

Let us consider the five simple questions that this model raises
for those studying the various institutions of government.

. Who's in charge here?

. To whom do I report?

. What are the rules?

. What happens to me if I obey (disobey)?
. Does this outfit have a future?

Aron e
