52 The Changing of the Guard

of Ur, it was to go forth and claim an inheritance of land. When
he established sanctuaries, forged alliances, dug wells, and con-
quered evil doers, he was attesting to the rule of God. He was
countering the effects of sin out in the world, in his home, and be-
fore the altar of God.

He was reclaiming the land.

This principle runs all through the Bible. Gad’s redemptive
work involves more than saving souls. It involves more than pre-
serving the status quo. It involves making “ail things: new”
(2 Corinthians 5:17). It involves taking “authority over the na-
tions” with the applied rule of God (Revelation 2:27-28). It in-
volves Christian political action.

Anything more or anything less simply cannot be warranted
from Scripture.

Summary

Abram was called by God to lay claim to a great inheritance.
That inheritance was a promise of dominion over all the nations
of the earth. It was a promise of a /and and a future. Land and
faith in the Bible are interrelated in three essential ways:

First, land provides us with a sanctuary.

Second, land provides us with a home.

Third, Jand provides us with dominion out in the world.

When we rebel against God, we lose access to these privileges:
first, we lost the right to worship. Then we lose our home. And
finally we lose the whole world.

This pattern of digression runs throughout the Bible and
throughout history.

But there is another pattern in Scripture and history. It is the
pattern of redemption,

The task of the people of God is to counter the effects of sin
with the redemptive work of Christ. The task of the people of God
is to reclaim the land.

This is the crux of Christian political action.

We've become so heavenly minded that we’re no earthly good.
The Gospel calls us to be so heavenly minded (Hebrews 3:1) that
we make the earth good (2 Corinthians 10:5).
