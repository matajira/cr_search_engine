The Standard of Excellence 63

Therefore Jethro, his father-in-law, suggested that the entire civil
structure of the fledgling Israelite nation be reformed. Why not
appoint men from out of the midst of the congregation who could
exercise the ministry of political action, he asked? Of course, he
pointed out these appointees would have to be qualified: They
would have to be godly men, trustworthy men, and men free of
covetousness. And in addition to these prerequisites, they would
have to be able men. The task of caring for the civil sphere is cer-
tainly too important to leave in the hands of the godless, the lech-
erous, and the greedy. But it is also too important to leave in the
hands of the sloppy and careless.

Tt was on this foundation of excellence that Moses built the
political action of the nation of Israel, and it is on that same foun-
dation of excellence that we must build the political action of our
own nation.

Summary

Jethro advised Moses to call men out of the congregation to
exercise the ministry of political action, to lead the thousands, the
hundreds, the fifties, and the tens.

It was necessary that these men be qualified.

First, they had to adhere to.the Biblical standard of godliness.
They had to be God-fearers.

Second, they had to adhere to the Biblical standard of truth-
fulness. They had to be men of honesty and integrity.

Third, they had to adhere to the Biblical standard of selfless-
ness. They had to be men of impeccability, hating covetousness.

But, besides these prerequisites, they had to be men of ability
as well. They had to adhere to the Biblical standard of excellence.

This standard of excellence is illustrated in the lives of Joseph,
Nehemiah, and Daniel, but it is rooted in the very character of
God Himself.

The great God of excellence calls us to eraulate Him. He calls
us to be men and women of excellence.

Sadly, the Church in our day has failed to hear or heed this
call.
