The Standard of Excellence 57

15:8-9); Ahab, in desiring Naboth’s vineyard (1 Kings 21:2-16);
and Judas, in swiping from the purse (John 12:6).

Selfless leaders, on the other hand, often brought great blessing
upon the nation: Abraham, in according to Lot first choice in the
land of Canaan (Genesis 13:9); Moses, in choosing to suffer
affliction with his people rather than enjoy the passing pleasures
of sin (Hebrews 11:25); Samuel, in his administration of justice
(1 Samuel 12:3-4), the widow of Zarephath, in sharing with Elijah
the last of her sustenance (1 Kings 17:12-15); Daniel, in refusing
rewards from Belshazzar (Daniel 5:16-17); and Esther in risking
her life for the deliverance of her people (Esther 4:16).

Clearly, those who exercised the ministry of political action in
Israel would not only have to fear God and maintain strict hon-
esty, they would have to serve in utter selflessness as well.

Able Men.

But taking priority over these three essential character traits,
Jethro made it clear to Moses that the men who were selected out
of the congregation to serve in civil government had to be men of
ability (Exodus 18:21). They had to adhere to the Biblical standard
of excellence, They had to demonstrate wisdom, discernment,
understanding, and sheer skill.

God granted Joseph great success as a leader because he com-
bined godliness, truthfulness, and selflessness with evident ex-
cellence (Genesis 39:6). Everything he did prospered in his hand
(Genesis 39:3). He served with excellence in Potiphar’s household
(Genesis 39:5). He served with excellence in the Egyptian prison
(Genesis 39:22), He served with excellence in Pharaoh’s court (Gen-
esis 41:37-45). Joseph was not a dullard who was able to get dy in
this world simply because he had the love of God in his heart. He
changed the world because he worked out what God had worked in
(Philippians 2:12-13),

Similarly, God granted Nehemiah great success as a leader
because he combined godliness, truthfulness, and selflessness
with evident excellence (Nehemiah 2:18). Everything he did pros-
pered in his hand (Nehemiah 13:14, 22, 31). He served with ex-
