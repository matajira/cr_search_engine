I. Transcendence/Presence

6

THE STANDARD OF EXCELLENCE

Select from all the people able men, such as fear God, men of
truth, hating covetousness; and place such over them to be rulers
of thousands, rulers of hundreds, rulers of fifties, and rulers of
tens. And let them judge the people at all times. Then it will be
that every great matter they shall bring to you, but every small
maitter they themselves shall judge. So it will be easier for you, for
they will bear the burden with you (Exodus 18:21-22).

Moses was facing a political crisis. The governmental ap-
Paratus of the nation had begun to bog down. Justice was im-
paired. Administration was slowed. And services were stymied.

Jethro, Moses’ father-in-law, suggested that the entire civil
structure be reformed to accord with God’s will (Exodus 18:23).
He said that all the people, being the priests of God, commis-
sioned as they were to be salt and light, should be taught the
statues and the laws of God (Exodus 18:20). And then, according
to Jethro, they should exercise their responsibilities as righteous
citizens (Exodus 18:18). Men should be selected out of the midst of
the holy congregation he said, who could exercise the ministry of
political action (Exodus 18:21).

But these men had to adhere to certain standards before they
could be considered for this ministry. Their character had to be
tested. They had to be screened by the Law. To govern in terms of
the covenant, first they had to e governed in terms of the covenant.
‘To judge, first they had to be judged. Those who are not willing to
be judged by God's law are warned not to execute judgment:
“Judge not, that you be not judged. For with what judgment you

53
