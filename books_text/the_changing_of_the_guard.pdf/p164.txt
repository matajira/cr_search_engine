132 The Changing of the Guard

tural relevance, and spiritual efficacy. The Churches of our land
should tap the enormous power of approbative and imprecatory
psalmody.. They should ring out with benedictory and maledic-
tory fervor. They should address the issues, dilemmas, and crises
the way the Church throughout the ages always has: in prayer,
through worship.

In seroing:

The Church should initiate programs of compassion and care.
Biblical charity is cheaper than state welfare. Thus, it confronts the
tyranny of overbearing taxes. Biblical charity is more efficient than state
welfare, Thus, it confronts the tyranny of overarching bureaucracy. Bib-
lical charity is private. Thus, it confronts the tyranny of power cen-
tralization. Biblical charity is family-centered. Thus, it confronts the
tyranny of unaccountability, Biblical charity is /oca’. Thus, it con-
fronts the tyranny of statistical arbitrariness. Biblical charity is
temporary. Thus, it confronts the tyranny of need—reinforcement
and subsidization, When we care for the aged, the unborn, the
neglected, and the poor we not only reclaim from the liberal hu-
manists the moral high ground, we also reclaim from them the
leverage of benevolence.

In prophesying:

The Church should speak out, The Church should call sin
“sin.” The Church should take a bold, uncompromising stand on
the moral issues, identifying the enemies of the family, the ene-
mies of life, the enemies of truth, the enemies of liberty, exposing
them for what they are. We desperately need a Church that will
willingly jeopardize its “reputation” in order to be counted with
Noah, Moses, Elijah, Jeremiah, Amos, Ezekiel, and John the
Baptist. We desperately need. a Church that will willingly risk its
IRS 501 (c) (3) designation for the sake of truth.

In judging:

The Church should once again hold court. The Church
should take seriously its mandate from God to be a real govern-
ment by binding and loosing, judging and discerning. The
Church should begin to exercise discipline once again. It should
reclaim its lost legacy of purity and integrity, It should reassert its
