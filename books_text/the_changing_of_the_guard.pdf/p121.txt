The Challenge of the Compromise 89

He was willing to risk that power, influence, and privilege
because:

First, he understood who really governs men and nations. God
is sovereign and thus the risks of uncompromising faithfulness are
more than balanced out by the benefits.

Second, he understood that opposition was inevitable. To
compromise would not have appeased Daniel’s enemies. Better to
keep his message undiluted and face the risks than to compromise
and still be under the fires of persecution.

Third, he understood that God would transform persecution
into promise. Prison is the first stage of dominion, so an uncom-
promising stand is worth the risk. Risk offers resurrection.

This reaction—repression—resurrection pattern is not only
evident in Daniel’s life, it appears throughout the Scriptures pro-
viding for us the security we need to imitate the heroes of the
faith.

But imitating unwavering steadfastness is not all we must do
in order to procure victory. We must root our uncompromising
posture in humility as well.

Our confidence must be in God. Our own ingenuity, our own
skillfulness, and our own willfulness is not sufficient or satisfac-
tory. We must yield to Him. We must be uncompromising. But
we must also be humble.

In short, we must unswervingly speak the truth. But we must
speak that truth with true, loving kindness.
