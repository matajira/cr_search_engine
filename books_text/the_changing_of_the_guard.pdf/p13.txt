Editor's Introduction xiii

dom of God in history, toward which we Christians are to labor
and pray.

Does this earthly Kingdom include politics? How could it not
include politics if it is truly a comprehensive Kingdom? Yet there
are millions of Christians today who deny that politics can ever be
Christian. Somehow they believe that Christ's offer of salvation is
limited in scope—that churches can be healed by grace, families
can be healed by grace, businesses can be healed by grace, but
“politics is dirty, now and forever more, amen!” They refuse to
face the obvious: politics is a dirty business because Christians have long
avoided politics. Politics is corrupt because Christians have not
bothered to search the Scriptures to discover the God-given prin-
ciples of political action. This lack of concern, this full-time pro-
fessional Christian apathy, is what The Changing of the Guard is
designed to overcome.

“Not of This World”

The standard, run-of-the-mill negative Christian response to
the Biblical message of Christians’ political responsibilities rests
ona faulty reading of John 18:36. We need to ask: What did Jesus
mean when He said that His Kingdom is not of this world? Jesus
was explaining to Pontius Pilate how He could be a king, yet also
be standing before Pilate to be judged. Pilate was asking Jesus:
“How can you be a king? Where are your defenders? Where is
your army?” Pilate believed in the power religion of the ancient
world.2 Without earthly armies, he believed, a man cannot be a
king. His earth-bound view was echoed over 1,900 years later by
the tyrant Joseph Stalin, who is said to have dismissed the authority
of the Pope with the comment: “How many divisions does the
Pope have?”

Jesus’ response to Pilate was clear: the source of His kingly au-
thority is not earthly. His Kingdom is not of this world. The source
of His authority as king is from far above this world, His is a iran-

2. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985).
