The Chatlenge of the Compromise 87

Even so, the dirty little game of politics makes for strange bed-
fellows. All too often naive, inexperienced, and uninitiated, the
religious right often found itself allied with the old line traditional-
ists and conservatives. And it found itself being used. It found it-
self yoked together with Belial (2 Corinthians 6:14-18). Rushing
headlong into established partisan politics, Christians failed to
recognize until late in the game that a conservative humanism is
little better than a liberal humanism. By exchanging yippies for
yuppies, Democrats for Republicans, and Modernists for tradi-
tionalists, the religious right did little more than domesticate its
own steadfastness. It wound up compromised by default.

If the religious right is ever to accomplish its stated goal of re-
turning our nation to moral sanity and spiritual stability, it must
humbly but determinedly set its own course according to the wind of
the Spirit of God. It must no longer be the pawn of powers and
principalities, of godless men and institutions be they left or right.

In short, the religious right must not compromise—even in its
pose of humble steadfastness!

We must be uncompromising. But we must also be humble.
‘We must meet the challenge of politics with the same commitment
to Biblical surety that we convey in every other sphere of life and
godliness. We must “speak boldly” (Titus 3:8), “confidently”
(Ephesians 6:20), and “without fear” (Philippians 1:14). But we
must speak the truth in /ove (Ephesians 4:15).

Conclusion

The eighth basic principle in the Biblical blueprint for poli-
tical action is that we must not compromise. But, that steadfast-
ness must be marked by humility.

Like Daniel, we owe our full allegiance to God Almighty. We
Toust not—we can not—waffle when God’s purposes are at issue.

There can be no compromise on the sanctity of human life.
Abortion must be stopped. Infanticide must be stopped. Euthana-
sia must be stopped. This must be an absolute priority for anyone
called by God into the ministry of political action. This must be an
absolute priority for every other Christian citizen as well. There
