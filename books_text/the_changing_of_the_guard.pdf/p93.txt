The Standard of Excellence 61

From cover to cover the Bible is a pantheon of praise to the ex-
cellencies of our God.

Called to Excellence

As God’s representatives before men (2 Corinthians 5:20), we
are to “proclaim His excellencies” (1 Peter 2:9). But our proclama-
tion must not merely be “in word or with tongue, but in deed and.
with truth” (1 John 3:18). We proclaim His excellence by our ex-
cellence. In everything we do and in everything we say, we are to
manifest Him who has “called us by His own glory and excellence”
(2 Peter 1:3), As we follow after Him (Matthew 4:19), as we walk
in His footsteps (1 Peter 2:21), and as we imitate His attributes
(1 Peter 1:16), excellence is to be our hallmark. Like Jethro, Joseph,
Nehemiah, Daniel, and even God Himself, we are to be noted for
our excellencies.

God expects nothing less of us.

He who long ago demanded excellent sacrifices (Malachi
1:8-10), excellent artistry (Exodus 28:2), and excellent service
(Proverbs 12:4), has in no way altered His standards of disciple-
ship. We are to live lives marked by moral excellence (2 Peter 1:5).
We are to keep our behavior excellent at all times (1 Peter 1:12).
Our minds are to dwell constantly on excellence (Philippians 4:8).
We are to walk in the way of excellence (1 Corinthians 12:31),
manifesting cultural excellence (Genesis 1:28), economic excel-
lence (Matthew 25:14-30), familial excellence (Proverbs 31:10-31),
spiritual excellence (Philippians 1:10), and evangelistic excellence
(Matthew 28:18-20). Mediocrity and triviality are to be the far-
thest things from the experience of faithful Christians.

The great God of excellence calls us to be men and women of ex-
cellence (1 Thessalonians 4:1, 10), Just as God called those under
Moses and Jethro to excellence, He calls us to be men and women
of ability (Exodus 18:21).

Unfortunately, the modern evangelical Church has failed to
heed this call. It has almost become an evangelical legacy to churn
out the basest sort of triviality: sloppy literature, sloppy music,
sloppy social outreach, sloppy scholarship, sloppy worship, and
