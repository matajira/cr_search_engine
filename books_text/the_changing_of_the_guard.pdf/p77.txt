Reclaiming the Land 45

food” (Genesis 2:9), and it was lavishly irrigated by the head-
waters of four great rivers (Genesis 2:10). “In the midst” of this
luxuriant garden God placed “the tree of life” and “the tree of the
knowledge of good and evil,” thus establishing His sacramental
presence (Genesis 2:9), God set aside this Jand so that He could
meet with man, ordaining true worship, true fellowship, and true
sanctuary.

Second, land gives us a place to rest. It affords us with a dwell-
ing place where we can retire into the comfortable confines of the
family hearth, It provides us with a home.

The garden sanctuary was located in the easternmost corner .
of the land of Eden (Genesis 2:8). That land was a vast moun-
tainous domain (Ezekiel 28:14) that, like the garden, was well
watered, lavishly stocked, gloriously adorned, and fabulously fur-
nished (Genesis 2:10-14). It was a genuine paradise laden high
with precious stones, jewels, and minerals (Ezekiel 28:13). God
placed man in the midst of this paradise to live out his days estab-
lishing a godly seed. God set aside this Jand so that man could
have a home.

Third, Jand gives us a place to work. It affords us with an
arena for the practical outworking of our faith. It provides us with
dominion.

The garden was set in the midst of Eden (Genesis 2:8), and
Eden was set in the midst of the world (Genesis 2:10-14). It was
there in the various outlying lands of the world that man was to
exercise authority and diligence bringing to bear God’s order and
purpose for all things (Genesis 1:26). The lands of Havilah (Gen-
esis 2:11), Cush (Genesis 2:13), and Assyria (Genesis 2:14) were
rich and good, but they needed the subordinated rule of God in
man (Genesis 1:28). So, God set aside /and so that man could work
and take dominion over the earth,

The Pattern of Digression
When man sinned against God, not only did Ae fall (Genesis
3:7), but the /and fell as well (Genesis 3:17-19), Not only were the
sons of men shackled to “bodies of sin and death” (Romans 7:24),
