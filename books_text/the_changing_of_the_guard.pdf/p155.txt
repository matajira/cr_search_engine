What the Church Should Do 123

speaks. And they believed that it speaks to everything. They be-
lieved that, “All Scripture is given by inspiration of God, and is
profitable for doctrine, for reproof, for correction, for instruction
in righteousness, that the man of God may be complete, thor-
oughly equipped for every good work” (2 Timothy 3:16-17). They
believed that “not one jot or tittle” had in any wise passed from it
(Matthew 5:18). They believed it is “settled in heaven” (Psalm
119:89) and “established on the earth” (Psalm 119:90). They be-
lieved the Bible, and they taught it systematically.

From their pulpits and in their Sunday schools, the believers
in early America ‘earned the basic principles in the Biblical blue-
print for political action. They earned how to serve Christ through
the righteous exercise of citizenship. They /earned what it meant to
minister as a magistrate.

We must begin to reassert this teaching role in our own day.

Materials are now available to help pastors, teachers, and
Bible study leaders really teach (see the bibliography). We don’t
have to start from scratch. There are books, cassettes, newslet-
ters, videos, magazines, candidate score cards, workbooks, and
seminars that can help the Church beat its long-held addiction to
triviality and irrelevance. All we have to do is utilize the resources
God has given us. All we have to do is teach the practical truth of
the Bible, and God will take care of the rest.

Then Jesus said to those Jews who believed Him, “If you abide
in My Word, you are My disciples indeed. And you shall know the
truth, and the truth shall make you free” ( John 8:31-32).

Worshipping

Man’s chief end is to glorify and enjoy God. We are to glorify
and enjoy Him in every area of life. But we are to do it particularly
in worship.

When Moses went before Pharaoh to lobby for Israel’s liberty,
he did not say, “Let My people go that they may start a new poli-
tical party.” He did not say, “Let My people go that they may
found a confederated theocracy.” No, instead he said:
