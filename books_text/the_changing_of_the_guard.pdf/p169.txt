What the Family Should Do 137

for his chariots. He will take your daughters to be perfumers,
cooks, and bakers. And he will take a tenth of your grain and your
vintage, and give it to his officers and servants. And he will take
your menservants and your maidservants and your finest young
men and your donkeys, and put them to his work. He will take a
tenth of your sheep. And you will be his servants. And you will cry
out in that day because of your king whom you have chosen for your-
selves, and the Lord will not hear you in that day” (1 Samuel 8:6-18).

In desperation, Samuel attempted to warn the people of the
inherent dangers of their scheme. There would be taxation. There
would be conscription. There would be coercion. There would be
tyranny. It was inevitable.

But the people could not be swayed.

Nevertheless the people refused to obey the voice of Samuel;
and they said, “No, but we will have a king over us, that we also
may be like all the nations, and that our king may judge us and go
out before us and fight our battles” (1 Samuel 8:19-20).

The prospect of tyranny looked much better to the people than
an eroding social and political order under Samuel’s debauched
family. A king and his tyranny then, it would be.

And Samuel heard all the words of the people, and he repeated
them in the hearing of the Lord. So the Lord said to Samuel,
“Heed their voice, and make them a king.” And Samuel said to the
men of Israel, “Every man go to his city” (1 Samuel 8:21-22).

Throughout his life, Samuel worked hard, traversing the
countryside, weaving a social and political fabric impervious to
the rending attacks of lawlessness, godlessness, and truthlessness.
He poured himself into this work to the exclusion of all else—only
to discover late in life that his sorely neglected family was unravel-
ing every stitch.

When the family fails, the entire social and political system
suffers. When the family fails, the Church suffers. When the fam-
ily fails, the state suffers.

Thus, a key to rebuilding our nation through the ministry of
political action relies to a great degree on the righteous participa-
tion of families.
