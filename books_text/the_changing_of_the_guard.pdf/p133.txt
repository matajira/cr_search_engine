V._Inheritance/Continuity

10
FACING THE ODDS

Moreover, brethren, I do not want you to be unaware that all
our fathers were under the cloud, all passed through the sea, all
were baptized into Moses in the cloud and in the sea, all ate the
same spiritual food, and all drank the same spiritual drink. For
they drank of that spiritual Rock that followed them, and that
Rock was Christ. But with most of them God was not well pleased,
for their bodies were scattered in the wilderness. Now these things
became our examples, to the intent that we should not Iust after
evil things as they also lusted. And do not become idolaters as were
some of them. As it is written, “The people sat down to eat and
drink, and rose up to play.” Nor let us commit sexual immorality,
as some them did, and in one day twenty-three thousand fell; nor let
us tempt Christ, as some of them also tempted, and were destroyed
by serpents; nor murmur, as some of them also murmured, and
were destroyed by the destroyer. Now all these things happened to
them as examples, and they were written for our admonition, on
whom the ends of the ages have come (1 Corinthians 10:1-11).

God has set before us examples. Examples of victory. Exam-
ples of defeat. Examples of righteousness. Examples of debauch-

ery. Good examples and bad examples.

God has set them before us for our instruction, He wants us to

learn some important lessons. He wants us to hear and heed.

We are to imitate those who walked before us in holiness and
truth, We are to avoid the mistakes of those who walked before us
in wickedness. The great heroes of the faith are to be our models
for godly living (Hebrews 12:1). The great villains are to be our

warnings (1 Corinthians 15:33).
101
