126 The Changing of the Guard

Serving

God has called the Church to serve. And through service He
grants us favor with the people. This is a fundamental principle of
dominion in the Bible: dominion through service.

This principle is understood all too well by the humanists who
run the modern Welfare State. They recognize that whatever
agency serves the needs of the people will ultimately gain the alle-
giance of the people. So, they “serve.” So, they continue the pre-
posterous charade of government charity. And so, they gain
dominion.

And He said to them, “The kings of the Gentiles exercise lord-
ship over them, and those who exercise authority over them are
called ‘benefactors’ But not so among you, let him be as the
younger, and he who governs as he who serves, For who is greater,
he who sits at the table, or he who serves? Is it not he who sits at
the table? Yet I am among you as the One who serves. But you are
those who have continued with Me in My trials. And I bestow
upon you a kingdom, just as My Father bestowed one upon Me,
that you may eat and drink at My table in My kingdom, and sit on
thrones judging the twelve tribes of Israel” (Luke 22:25-30).

Sadly, Christians have not comprehended this link between.
charity and authority. They have not understood that dominion
comes through service.

When people are needy, or fearful, or desperate, they seek out
protection. They seek out benefactors. They seek out authorities
with whom they can trade allegiance for security.

Early in our nation’s history it was the Church that operated
the hospitals, orphanages, alms houses, rescue missions, hostels,
soup kitchens, welfare agencies, schools, and universities. The
Church was a home to the homeless and a refuge to the rejected.
Asa result, the Church had authority. It earned its authority by
serving.

Canvassing neighborhoods is fine. Registering voters is good.
Evaluating candidates is important. Phone banks and direct mail
centers and media campaigns are all necessary. But, if the Church
