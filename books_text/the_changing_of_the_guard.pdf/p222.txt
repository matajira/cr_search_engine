190 The Changing of the Guard

tion to the contractor. Nobody wants to be on the twelfth floor of a
building that collapses.

Today, we are unquestionably on the twelfth floor, and maybe
even the fiftieth. Most of today’s “buildings” (institutions) were de-
signed by humanists, for use by humanists, but paid for mostly by
Christians (investments, donations, and taxes). These “buildings”
aren’t safe. Christians (and a lot of non-Christians) now are hear-
ing the creaking and groaning of these tottering buildings. Mil-
lions of people have now concluded that it’s time to: (1) call in a
totally new team of foundation and structural specialists to begin
a complete renovation, or (2) hire the original contractors to make
at least temporary structural modifications until we can all move
to safer quarters, or (3) call for an emergency helicopter team
because time has just about run out, and the elevators aren’t safe
either.

The writers of this series believe that the first option is the wise
one: Christians need to rebuild the foundations, using the Bible as
their guide. This view is ignored by those who still hope and pray
for the third approach: God’s helicopter escape. Finally, those who
haye faith in minor structural repairs don’t tell us what or where
these hoped-for safe quarters are, or how humanist contractors
are going to build them any safer next time.

Why is it that some Christians say that God hasn’t drawn up
any blueprints? If God doesn’t give us blueprints, then who does?
Hf God doesn’t set the permanent standards, then who does? If
God hasn’t any standards to judge men by, then who judges man?

The humanists’ answer is inescapable: man does— autonomous,
design-it-yourself, do-it-yourself man. Christians call this man-
glorifying religion the religion of humanism. It is amazing how
many Christians until quite recently have believed humanism’s
first doctrinal point, namely, that God has not established per-
manent blueprints for man and man’s institutions. Christians who
hold such a view of God’s law serve as humanism’s chaplains.

Men are God’s appointed “contractors.” We were never sup-
posed to draw up the blueprints, but we are supposed to execute
them, in history and then after the resurrection. Men have been
