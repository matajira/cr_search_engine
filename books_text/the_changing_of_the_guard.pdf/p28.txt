xxviii The Changing of the Guard

us to answer the hoped-for question of God-fearing new converts
to Christ: “I'm saved; what now?”

The Covenant Structure

To get the right answers, we need first to ask the right ques-
tions. For a long, long time, Christians and Jews have had the
right questions right under their noses, but no one paid any atten-
tion, The questions concerning lawful government are organized
in the Bible around a single theme: the covenant.

Most Christians and Jews have heard the word “covenant.”
They regard themselves (and occasionally even each other) as
covenant people, They are taught from their youth about God’s
covenant with Israel and how this covenant extends (or doesn’t)
to the Christian church. Everyone talks about the covenant, but
until late 1985, nobody did anything about it.

Not in 3,400 years of (non-inspired) Bible commentaries.

Am I exaggerating? Test me. Go to a Christian or a Jew and
ask him to outline the basic features of the Biblical covenant. He
will not be able to do it rapidly or even believably. Ask two Jews
or two Christians who talk about the covenant, and compare the
answers. The answers will not fit very well.

For over four centuries, Calvinist theologians have talked
about the covenant. They are even known as covenant theologians.
The Puritans of the 1600s wrote seemingly endless numbers of
books about the covenant. The problem is, nobody has ever been
able to come up with “the” covenant model in the writings of Cal-
vin, let alone all his followers. The Calvinists have hung their
theological hats on the covenant, yet they have never put down on
paper precisely what it is, what it involves, and how it works—in
the Bible or in church history.

Then, in late 1985, Pastor Ray Sutton made an astounding
discovery. He was thinking about Biblical symbols, and he strug-
gled with the question of two New Testament covenant symbols,
baptism and communion. This in turn raised the question of the
Old Testament’s covenant symbols, circumcision and passover,
What do these symbols have in common? Obviously, the cove-
