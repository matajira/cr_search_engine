Honorable Opposition 77

Second, he was steadfast. His faith was unshakable.

Third, he was brave. His courage was evident at all times, on
the field of battle and off.

Fourth, he was strong. He walked in the might and power of
the Lord.

And fifth, he was loving. He respected Saul and held him in
fond affection.

These five attributes of patience and honor were essential to
Dayid’s great success. And they will certainly need to be a part of
our work in the ministry of political action if we are to meet with
similar success.

The right way to exercise our political mandate as am-
bassadors of the King is the good way.
