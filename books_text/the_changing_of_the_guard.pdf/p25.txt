Editor's Introduction xxv

omy and the international political order will provide an un-
precedented opportunity for Christians to become dominant in
politics.

The question is: Will they make good use of this opportunity?

God and Government

Politics is the means of establishing and controlling civil gov-
ernment. It is one of the great heresies of our era that most people
believe that civil government is “government,” and that other law-
ful, God-ordained governments are something less than govern-
ment. It is this monopolizing of the concept of government by the
state that is at the heart of the loss of liberty in our day. (See Gary
DeMar’s book in the Biblical Blueprints Series, Ruler of the Nations.)

Conservative sociologist and historian Robert Nisbet has writ-
ten in his classic book, The Quest for Community (1952), that “The ar-
gument of this book is that the single most decisive influence upon
Western social organization has been the rise and development of
the centralized territorial State.”?7 He goes on to say, “Unlike
either kinship or capitalism, the State has become, in the contem-
porary world, the supreme allegiance of men and, in most recent
times, the greatest refuge from the insecurities of and frustrations
of other spheres of life. . . . [T]he State has risen as the dominant
institutional force in our society and the most evocative symbol of
cultural unity and purpose.”* He is correct when he says that this
modern faith in the State as the supreme manifestation of man’s
unity, purpose, and power “makes control of the State the greatest
single goal, or prize, in modern struggles for power.”

It is this struggle for control over the state that is the equiva-
lent of medieval man’s quest for salvation. What Prof. Wolin
wishes to accelerate —the substitution of political participation for
religious participation—his former faculty colleague at Berkeley,

7. Nisbet, The Quest for Community (New York: Oxford University Press, 1952),

p. 98.
8. Ibid., p. 99.
9. Ibid., p. 103.
