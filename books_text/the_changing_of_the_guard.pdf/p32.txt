 

Then Jesus came and spoke to them, saying, “All au-
thority has been given to Me in heaven and on earth. Go
therefore and make disciples of all the nations, baptizing
them in the name of the Father and of the Son and the
Holy Spirit, teaching them to observe al! things that I
have commanded you; and lo, I am with you always,
even to the end of the age.

Matthew 28:18-20

 
