Reclaiming the Land 51

World conquest. That's what Christ has commissioned us to
accomplish. We must win the world with the power of the Gospel.
And we must never settle for anything less.

If Jesus Christ is indeed Lord, as the Bible says, and if our
commission is to bring the land into subjection to His Lordship,
as the Bible says, then all our activities, all our witnessing, all our
preaching, all our craftsmanship, all our stewardship, and all our
political action will aim at nothing short of that sacred purpose.

‘Thus, Christian politics has as its primary intent the conquest
of the land—of men, families, institutions, bureaucracies, courts,
and governments for the Kingdom of Christ. It is to reinstitute the
authority of God's Word as supreme over all judgments, over all
legislation, over all declarations, constitutions, and confedera-
tions. True Christian political action seeks to rein the passions of
men and curb the pattern of digression under God’s rule.

Fortunately, because of the theocratic orientation of our
founding fathers, our nation has virtually all the apparatus extant
to implement such a reclamation. Unfortunately, the enemies of
the Gospel have hand-in-hand eroded the strength of those godly
foundations.

Thus, we stand at the crossroads. And the choice, the future,
is in our hands. The destiny of the land depends entirely on
whether we will go the way of Cain or Abraham, the way of Belial
or Christ, the way of pagan digression or theocratic redemption.

“How long halt ye between two opinions? If the Lord be God,
follow Him; but if Baal, then follow him” (1 Kings 18:21). “Choose
you this day whom ye will serve . . . but as for me and my house,
we will serve the Lord” (Joshua 24:15).

Conclusion

The fifth basic principle in the Biblical blueprint for political
action is that we must reclaim the land. We must be salt and light,
preservative and redemptive. We must ot only serve as priests,
protecting the land, we must serve as stewards reclaiming the
jand. We must effect a pattern of redemption that applies the rule
of God to everything under the heavens.

Land and Faith are inseparable concepts in the Bible. This
can be readily seen in the life of Abram. When God called him out
