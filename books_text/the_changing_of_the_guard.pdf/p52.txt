20 The Changing of the Guard

God and the state. In truth, everything that the state is, every au-
thority that it wields, every jurisdiction that it holds, and every
issue that it governs has been delegated to it by God. Rulers,
magistrates, and judges are “servants” (Romans 13:6) and “minis-
ters” (Romans 13:4) accountable to God.

Thus, like the family and the Church, the state is to be consid-
ered sacred: ruled by God, ordered by His Word, and entrusted
to men as a divine arena for the proving of righteousness, And
like the family and the Church, the state is to be considered by
faithful Christians to be a legitimate, and indeed, an essential
area of calling and ministry. It is as honorable and holy a pursuit
as is fatherhood or evangelism or the pastorate.

When was the last time you heard of someone submitting to a
call into the ministry —the ministry of political action?

There can be no doubt that God called Joseph into such a min-
istry (Genesis 41:39-41). Though the path to power was difficult,
there was never any question in his mind that God had chosen
him to govern (Genesis 37:5-10) in order to bring glory to God
(Genesis 50:20) and to save his people (Genesis 45:1).

There can be no doubt that God also called Gideon into such a
ministry (Judges 6:11-14). He was a simple farmer (Judges 6:11).
But he was obedient to God’s call (Judges 6:33-35) and took seri-
ously his holy occupation (Judges 7:2-9). As a result, God de-
livered the nation (Judges 7:19-25).

There can be no doubt that God also called Deborat into the
ministry of political action (Judges 5:1-7). She was a prophetess
(Judges 4:4) renowned throughout the land for her anointing and
judgment ( Judges 4:5). Had it not been for her courageous lead-
ership, the land would have been sorely oppressed and the ene-
mies of God would have triumphed ( Judges 4:6-24).

There. can be no doubt that God called Samuel into the minis-
try of political action as well (1 Samuel 3:1-19). He had been set
aside for service to the Lord from birth (1 Samuel 1:27-28) and
answered the call early to judge the civil affairs of the nation
(1 Samuel 7:15-17).

There can be no doubt that God also called David into the
