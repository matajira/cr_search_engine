26 The Changing of the Guard

The Galatians believed this twisted doctrine despite the fact
that the whole of the Bible—from Genesis to Revelation—is dia-
metrically opposed to it. Legalism is heresy. It is an Old Testament
heresy and a New Testament heresy. It was repudiated by Abra-
ham (Genesis 15:6; Romans 4:3; Galatians 3:6). It was denied by
Moses (Deuteronomy 27:26). It was condemned by Isaiah (Isaiah
1:10-18), and Jeremiah (Jeremiah 4:1-9), and Amos (Amos 5:1-7),
and Habakkuk (Habakkuk 2:4). David exposed the futility of sal-
vation by works (Psalm 32: Psalm 51:1-17) no less vehemently
than did Paul (Romans 9:32; Galatians 3:10; Ephesians 2:9),
Matthew (Matthew 8:22), Luke (Luke 9:60; 15:24, 32), John
(John 5:25; 15:4-6), Jude (Jude 416), or Peter (2 Peter 1:3-4;
2:1-22), Legalism is heresy.

It is heretical in every way imaginable. It abolishes the signifi-
cance of the cross (Galatians 5:11). It makes light of Christ’s sacri-
fice (Galatians 2:21). It nullifies the work of the Holy Spirit (Gala-
tians 3:3-5). It abrogates the necessity of grace (Romans 4:4).
“Faith is made void and the promise is nullified” (Romans 4:14)
because it makes man and man’s ability, rather than the rule of
God, the measure of all things (Matthew 15:6-9). Thus, salvation
by law is the rankest form of humanistic paganism.

Salvation according to the Bible is a work of grace. There is
nothing we can do to merit God’s favor,

 

But God, being rich in mercy, because of His great love with
which He loved us, even when we were dead in our transgressions,
made us alive together with Christ (by grace you have been saved),
and raised us up with Him, and seated us with Him in the heav-
enly places, in Christ Jesus, in order that in the ages to come He
might show the surpassing riches of His grace in kindness toward
us in Christ Jesus. For by grace you have been saved through
faith; and that not of yourselves, it is the gift of God; not as a result
of works, that no one should boast (Ephesians 2:4-9).

He saved us, not on the basis of deeds which we have done in
righteousness, but according to His mercy, by the washing of
regeneration and renewing by the Holy Spirit, whom He poured
out upon us richly through Jesus Christ our Savior, that being jus-
tified by His grace we might be made heirs according to the hope
of eternal life (Titus 3:5-7).
