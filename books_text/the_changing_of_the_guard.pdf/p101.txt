Honorable Opposition 69

Stand fast therefore in the liberty by which Christ has made us
free, and do not be entangled again with a yoke of bondage (Gala-
tians 5:1).

Each of us is to be steadfast in the faith (2 Thessalonians
2:25). We are commanded to stand firm in the midst of suffering
(i Peter 5:9), in the face of strange teaching (Hebrews 13:9), and
in times of trying circumstances (James 1:12). We are to be stead-
fast in good works (Galatians 6:9). We are to be steadfast in en-
during love (Hosea 6:4). We are to be steadfast in our conduct
(Philippians 1:27), in our decision making (1 Kings 18:21), and in
our absolute fealty to the Lord (Proverbs 24:21).

Steadfastness in political action protects us from ideological
bullies, both liberal and conservative, who pursue the ever shift-
ing, ever changing whims of fallen men. But it also protects us
from our own doublemindedness. [t secures us against doubt.

Steadfastness means adhering to God’s standards, upholding
God’s statutes, applying God’s principles, and enforcing God's
decrees,

David knew that.

So should we.

Therefore, my beloved and longed-for brethren, my joy and
crown, so stand fast in the Lord, beloved (Philippians 4:1).

Be Brave

In his dealings with Saul, David was not just alert and stead-
fast, he was also brave (1 Corinthians 16:13). He was a man of
valor. He demonstrated courage.

In the same way that Caleb stood fearlessly before the enemies
of God (Joshua 14:12); in the same way that Jonathan exhibited
great courage (1 Samuel 14:6); in the same way that Jael (Judges
4:18-22), Ezra (Ezra 5:11), Nehemiah (Nehemiah 6:11), Daniel
(Daniel 6:10), Ezekiel (Ezekiel 3:8-9), and Esther (Esther 4:8, 16)
showed undeterred bravery, so David was a man of unquestioned
valor. When all of Israel trembled in cowardice and fear before
Goliath (1 Samuel 17:24), David remained confident and fearless
