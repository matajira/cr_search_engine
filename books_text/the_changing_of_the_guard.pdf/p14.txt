xiv The Changing of the Guard

scendent Kingdom—distinct from this world—yet it is also imma-
nent, meaning that it is present among men in history, His King-
dom reflects His own dual nature: transcendent (perfect God) yet
also immanent (perfect man). But there are millions of Christians
today who in principie dismiss the doctrine of Christ’s perfect hu-
manity in history because they dismiss His Kingdom in history.
They spiritualize away His Kingdom, and therefore in principle
they spiritualize away His humanity. They are the implicit theological
defenders of the ancient heresy of docetism—that Christ was God,
but not man.

Anyone who argues that politics is permanently corrupt is nec-
essarily arguing that Christ’s saloation cannot heal and restore politics.
Anyone who argues that Christ’s salvation cannot heal politics is
also implicitly arguing that the humanists have a God-granted right
in history to exercise control over politics, since Christians should
spend their time bringing the Gospel to those people and institutions
that God says can be healed. They ask: “Why waste precious time
working on the impossible? Let the humanists do their work while
we do ours. Render unto the Caesar the things that are Caesar's.”
This assumes, of course, that God has in fact given the humanist
Caesars of this world permanent possession of political power. Yet
this is precisely what the humanists assume, too.

Question: If Caesar gets converted to Christ, should he
change his ways? If not, why not? If the answer is “yes,” then there
must be God-required ways for Caesar to change io. In short, is it better
to live under Nero or Constantine?

Anyone who argues that the Bible shows us how souls can be
saved but. not how anything else can be saved has run up the
white flag in history to the secular humanists, for if there are no
God-required standards of righteousness in politics, then there
can be no historical judgment by God over politics. Lf God has im-
posed no daw over something, then He exercises no jurisdiction over
it. God does not hold anyone responsible for what a person does if
He has not placed that person under the specific terms of His cov-
enant. The Bible is quite clear on this point: “. . . sin is not im-
puted when there is no law” (Romans 5:13b). So, if we argue that
