Render Unto Him 23

poral, all things earthly are spurned. Political action is thus left in
the lands of evil-doers. Although the Bible asserts that we are to
think hard about the nature of Christian civilization (1 Peter 1:13),
to try to develop Biblical alternatives to humanistic society (Mat-
thew 18:15-20), and to prophesy Biblically to the cultural prob-
lems of our age (Isaiah 6:8), we have isolated ourselves behind the
wails of a vast evangelical ghetto. We have rendered unto Caesar
everything—what is Caesar’s and what is God's.

Meanwhile, the modern day party of the Herodians is busy
with its work of oppression and repression. The Herodians hold
seats of power: in government, in education, in the media, in the
judiciary, and in the financial world, They care nothing for the
evangelicals’ morality. They abhor our puritanical virtues. They
chafe against our piety. They despise our nonconformity,

But, they applaud our irrelevancy. They appreciate our distrac-
tion from the things of this world, They know that as long as we
separate God and state, éhey will continue to have a free reign.
They will be able to perpetuate their slaughter of the unborn,
their assault on the family, and their defamation of all things holy,
all things sacred, and all things pure. They will be able to transfer
deity and rule from God to themselves, “doing what is right in
their own eyes” (Judges 20:25). In this way, these Herodians (or
secular humanists, as we call them today) count the irrelevant,
isolationist evangelical Church as their most trusted ally.

They couldn't do what they do without us.

Conclusion

The second basic principle in the Biblical blueprint for poli-
tical action is that God has ordained civil government, thus making
it a sacred institution. It is God's institution. Since it is our duty to
“render unto God the things that are God's” (Matthew 22:21;
Mark 12:17; Luke 20:25), we must work diligently to render unto
Him “the powers that be” (Romans 13:1).

The Pharisees and Herodians both opposed Jesus on this mat-
ter, They both argued for a wall of separation between God and
the state. The Pharisees avoided politics for fear it would pollute
