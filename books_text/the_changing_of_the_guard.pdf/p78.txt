6 The Changing of the Guard

but the land was “subjected to futility” (Romans 8:20) and “en-
slaved to corruption” as well (Romans 8:21), Man lost his inherit-
ance, his land. .

First, man lost his sanctuary-land. As a consequence of his sin,
tan was expelled from the garden (Genesis 3:23). Access to the
presence of God was cut off (Genesis 3:24). Man could only wor-
ship God “from afar” (Exodus 24:1-2).

Next, man lost his home-dand. Sin continued to progress in the
sons of men. When Cain rose up and murdered his brother Abel
(Genesis 4:8), he was driven out of his home in the same way that
Adam and Eve were driven out of the sanctuary (Genesis 3:24-4:12),
And whereas the land only yielded up its strength to Adam and
Eve after great travail and difficulty (Genesis 3:17-19), it would
not yield up its strength af ail to Cain (Genesis 4:12). Thus, his sin
sent him out homeless, as “a vagrant and wanderer on the earth”
(Genesis 4:14).

Finally, man lost the whole world. The progression of sin’s vile
destruction continued even after Cain, Anarchy and wholesale
murder became commonplace (Genesis 4:23-24). Debauchery
and lust prevailed (Genesis 6:2-4). “The wickedness of man was
great on the earth” (Genesis 6:5), so that even the wilderness of
man’s wanderings was laid waste. After much forbearance, God
judged the land with a sweeping flood and the whole earth was
lost (Genesis 7:17-24),

Whenever we rebel against God we set into motion this same
sequence of events—this pattern of digression. First, we lose our
sanctuary. Then we lose our home. And finally, if sin is left unchecked,
we lose the whole world.

When Israel sinned by taking idols into its midst, God sent the
overlords of Assyria to pillage and plunder the temple (2 Chroni-
cles 28:20-21). They lost the sanctuary.

When the nation continued on, unrepentant, God sent the
warlords of Babylon to sack and sunder the cities (2 Kings 5:1-4).
And thus the people were exiled from their inheritance, They lost
their home.

Finally, when the people remained stiff-necked in the face of
