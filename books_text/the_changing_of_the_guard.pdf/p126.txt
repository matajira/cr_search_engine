4 The Changing of the Guard

The Foundation of Political Action

Of course, praying wasn’t ail that he did. But it was the founda-
tion of all that he did. Like anyone called by God into the ministry
of political action, he invested himself in careful planning (Nehe-
miah 2:5-6). He laid the groundwork with cautious attention to
detail (Nehemiah 2:7-8). He enlisted qualified help (Nehemiah
2:9), He encouraged his workers (Nehemiah 2:17-18). He moti-
vated them (Nehemiah 4:14-20). He organized and delegated the
various tasks (Nehemiah 3:1-32). He anticipated difficulty and
made provision for it (Nehemiah 2:19-20; 6:1-14). He improvised,
when he had to (Nehemiah 4:2i-23), and worked (Nehemiah
4:23). He sacrificed (Nehemiah 5:14-19), led (Nehemiah 13:4-30),
and governed (Nehemiah 7:1-7). But undergirding all these neces-
sary political activities was his constant reliance upon Almighty
God. Undergirding them all was prayer.

Nehemiah knew that it is pointless to attempt anything apart
from God's blessing and purpose:

Unless the Lord builds the house, they labor in vain who build
it; unless the Lord guards the city, the watchman stays awake in
vain, It is vain for you to rise up early, to sit up late, to eat the bread
of sorrows; for so He gives His beloved sleep (Psalm 127:1-2).

The prayer life of Nehemiah indicated that he wanted, more
than anything else, to do God's will. He wanted to be accounta-
ble—accountable for his actions, for his intentions, and for the
fruit of his labor.

He didn’t want God to simply “okay” Ais plans. He wanted to
do what God wanted him to do.

Prayer held him accountable to that. It gave him the resolve to
stick to that. Prayer gave him access to God’s will, God’s way,
God’s purposes, and God’s plan.

Nehemiah was confident that God would give him success
(Nehemiah 2:20). He was sure God would give him strength
(Nehemiah 6:9), show him favor (Nehemiah 2:18), and see him
through (Nehemiah 2:12). He was unwavering in his optimism
because the work was conceived by God, not by him (Nehemiah
7:5). It was God's project, not his.
