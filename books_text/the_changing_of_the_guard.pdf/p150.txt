 

If you faint in the day of adversity, your strength is
small. Deliver those who are drawn toward death, and
hold back those stumbling to the slaughter. If you say,
“Surely we did not know this,” does not He who weighs
the hearts consider it? He who keeps your soul, does He
not know it? And will He not render to each man accord-
ing to his deeds?

Proverbs 24:10-12

 
