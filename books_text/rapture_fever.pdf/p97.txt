Pessimism Produces Paralysis 63

“The Church Cannot Change the World!”

I realize that there are premillennialists who will take offense
at this statement, They will cite their obligations under Luke
19:13: “Occupy till I come.” But the leaders of the traditional
premillennial_ movement are quite self-conscious about their
eschatology, and we need to take them seriously as spokesmen.
For example, John Walvoord, author of many books on eschat-
ology, and the long-time president of Dallas Theological Semi-
nary, the premier dispensational academic institution, has not
minced any words in this regard. In an interview with Christian-
ity Today (Feb. 6, 1987), Kenneth Kantzer asked:

Kanizer: For all of you who are not postmils, is it worth your
efforts to improve the physical, social, and political situation on
earth?

Walvoord: The answer is yes and no. We know that our efforts to
make society Christianized is [sic] futile because the Bible doesn’t
teach it. On the other hand, the Bible certainly doesn’t teach that
we should be indifferent to injustice and famine and to all sorts
of things that are wrong in our current civilization. Even though
we know our efforts aren’t going to bring a utopia, we should do
what we can to have honest government and moral laws. It’s
very difficult from Scripture to advocate massive social improve-
ment efforts, because certainly Paul didn’t start any, and neither
did Peter. They assumed that civilization as a whole is hopeless
and subject to God’s judgment (pp. 5-1, 6-1).

Who said anything about expecting utopia? Only the pessi-
mists, who use the word in order to ridicule people who preach
that Christians are not foreordained to be losers in history. Why
is civilization more hopeless than the soul of any sinner? The
gospel saves sinners, after all. Why should we expect no major
social improvements in society? Jesus said, “All power is given
unto me in heaven and in earth” (Matt. 28:18). When He dele-
gated power to His Church — power manifested in miraculous
