x RAPTURE FEVER

mistake. It is analogous to the person who refuses the gift of
eternal life because he knows he will have to live differently
after he receives this gift. He prefers to turn down the gift of
salvation rather than allow God to change his life for the better.

The Gift of Eternal Life

In July, 1959, a man sat down with me after a church meet-
ing. He opened a Bible. I had never read the entire Bible; I
was 17 years old, and I lived in a non-Christian home. Here is
the first verse he showed me:

For all have sinned, and come short of the glory of God (Rom.
3:23).

I knew this was true. Men are not God, and God is perfect.
Back in 1959, a pagan could go through the U.S. government
school system and still know this much. I knew that I was in-
cluded under the words “for all.” But then he showed me
another verse:

For the wages of sin is death; but the gift of God is eternal
life through Jesus Christ our Lord (Rom. 6:23).

That verse drew a conclusion from the earlier verse: all men
are going to die. My friend told me that I had better believe
the Bible’s conclusion regarding the wages of sin if I believed
the Bible’s premise about all having sinned. I did draw this
conclusion. My father had been a military policeman and was at
the time an F.B.I. agent, one of J. Edgar Hoover's men. I knew
there is cause and effect in breaking laws and receiving punish-
ment. So, I accepted the truth of the warning: “For the wages
of sin is death.” But there is a way to. escape death. The second
half of the verse is crucial, “but the gift of God is eternal life
through Jesus Christ our Lord.” Jesus Christ is Lord. He offers
a gift. I had better take it, my friend warned, and so I did.
