Conclusion 205

Fourth, because God’s revealed law is annulled in the New
Testament era, so are the sanctions attached to that law, espe-
cially civil sanctions. Because God supposedly refuses to bring
sanctions in history in terms of His law, Christians believe that
they should not seek to bring civil sanctions in terms of God’s
law. After all, if God rewards covenant-breakers with victory in
history and curses covenant-keepers with defeat in history, why
should God's chosen representatives seek to bring negative civil
sanctions against covenant-breakers in history? If God refuses to
honor the system of historical sanctions in Leviticus 26 and
Deuteronomy 28 in the New Testament era, why should His
people honor Exodus 21-23: the case laws?®

Fifth, Because God supposedly promises to disinherit His
Church in history, why should Christians pay any attention to
the historical long run? After all, Keynes was correct: in the
long run we are all dead. In history’s long run; the Church will
be embarrassed. Why sacrifice oneself for a lifetime to study
what God’s law requires, since all plans to impose God's law in
history are at best utopian and at worst tyrannical? This is why
social ethics has been the idiosyncratic pastime of a handful of
Christians who have been taught to impose humanism on the
Bible’s categories, abandoning the Bible whenever it contradicts
the latest humanist intellectual fad. This has been true for
about 1,800 years.

The Lure of Pretribulational Dispensationalism

Few people can function psychologically under the threat of
inevitable historical defeat. The genius of pretribulation dispen-
sationalism is its appeal to psychologically defeated people,
whose name is legion in the modern Church because they have

8. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1990).

6, Comelius Van Til, A Christian Theory of Knowledge (Nutley, New Jersey:
Presbyterian & Reformed, 1969).
