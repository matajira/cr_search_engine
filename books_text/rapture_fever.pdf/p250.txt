216 RAPTURE FEVER

Almost the entire review is devoted to the Dallas Seminary
version of the history of rival eschatologies. Finally, he did refer
to House Divided again. And what he said can serve as an epi-
taph for dispensationalism:

A reasoned answer to this book would require another book
of equal size, which the reviewer does not intend to write. When
Whisenant announced that the Rapture would occur in Septem-
ber 1988, many people suggested that this reviewer answer that
teaching. His answer, however, was, ‘Just wait.” As the alleged
date of the Rapture came and went, that teaching was seen to be
wrong. The same will be true of dominion theology.

So, Walvoord’s answer also is: “Just wait.” But at his age, one
can hardly afford to wait. His followers, like Dallas Seminary’s
donors, have been waiting for generations. What kind of theo-
logical response is “just wait,” when your critics have used your
movement's mania for date-setting as one of the most obvious
signs of its deformity? When the widely acknowledged theologi-
cal leader of a movement can only respond “just wait” to a book
as detailed and theologically rigorous as House Divided, that
movement is drawing near to the end. When dispensationalism’s
premier theological journal runs such a book review as if it were intel-
leciually adequate, the movement is visibly brain-dead.

Walvoord did what I never thought likely. He appealed to
newspapers as the proof of his eschatology. Remember the
words of Hal Lindsey: “Some of the future events that were
predicted hundreds of years ago read like today’s newspa-
per.”"° Now hear Walvoord: “One wonders how the writers of
this book can read the newspapers with their accounts of in-
creased crime and a decaying church and come up with the
idea that Christianity is triumphant in the world.” Here it is, in
black and white: newspaper exegesis. Here is the dean of dispen-

10, Hal Lindsey (with C. C. Carlson), The Late Great Planet Earth (Grand Rapids,
Michigan: Zondervan, 1970), p. 20.
