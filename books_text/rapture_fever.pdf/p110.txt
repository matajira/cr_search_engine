4

DISPENSATIONALISM REMOVES
EARTHLY HOPE

A Psalin of David. The Lorn is my shepherd; I shall not want. He
maketh me to lie down in green pastures: he leadeth me beside the still
waters. He restoreth my soul: he leadeth me in the paths of righteousness
for his name's sake. Yea, though I walk through the valley of the shadow
of death, I will fear no evil: for thou art with me; thy rod and thy staff
they comfort me. Thou preparest a table before me in the presence of
mine enemies: thou anoiniest my head with oil; my cup runneth over
Surely goodness and mercy shall follow me ail the days of my life: and I
will dwell in the house of the Lorp for ever (Psa. 23:1-6).

One of the great evils of dispensationalism is that it self-
consciously strips from Christians the Old Testament’s many
comforts offered by God to His people. Dispensationalists re-
gard the 23rd psalm as the equivalent of Santa Claus: a com-
forting story fit for children but not for adults. There are many
dispensational local churches that refuse to recite any of the
psalms. There are even some local assemblies that refuse to
recite the Lord’s Prayer, consigning it to the “pre-crucifixion
Jewish dispensation.” They refuse to acknowledge the lawful
inheritance of the Church in history:
