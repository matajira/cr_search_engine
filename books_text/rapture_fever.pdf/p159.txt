A Ghetto Eschatology 125

an accident that as of 1993, all of the major academic works in
the Christian Reconstruction movement have been written by
postmillennialists. I am speaking here of books written from the
perspective ofa Christian theology of positive cultural transfor-
mation, in contrast to merely negative Christian academic criti-
cism.’® I mean books that really do propose specific, Bible-
mandated ways to reconstruct today’s humanism-dominated.
society.

It is also not an accident that the bulk of the premillennial
leaders and their organizations that directed the formation of
the New Christian Right in 1979 and 1980 have disappeared
from the political scene, just as I predicted in 1982.1 Most
people are highly unlikely to stay in the front lines of Christian
social and political reform without the psychological support of
a consistent theology of social and political reform. The human-
ist news media sharks will grind them down relentlessly on the
altogether relevant question of theocracy, and premillennialist
leaders’ timid supporters will cease sending them money if they
say publicly that they believe in theocracy. So the leaders either
waffle or grow suspiciously silent. Neither waffling nor silence
changes society or gathers the troops together for a full-scale
confrontation. Christian political leaders need biblical law
{which dispensationalism denies) and a positive eschatology
(which premillennialism denies). Christian ‘media leaders are
presently terrified of both.

Christian Reconstructionists therefore have won the intellec-
tual leadership of Christian activists by default. Like Harry Trn-
man, we can stand the heat, so we stay in the kitchen.

improper use of the second law of thermodynamics, or “entropy” See Gary North,
Is the World Running Down? Crisis in the Christian Worldview (Tyler, Texas: Institute for
Christian Economics, 1988).

10. [have in mind here the negative critical works of premillennialist Francis
Schaeffer and Dutch tradition amillenniatists Herman Dooyeweerd and Cornelius
Van Til. I also have in mirid Herbert Schlossherg’s Idols for Destruction.

11. Gary North, “The Intellectuat Schizophrenia of the New Christian Right,”
Christianity and Givilization, 1 (1982). See also Chapter 11, below.
