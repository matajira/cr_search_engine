106 RAPTURE FEVER

The culmination and epitaph of the dispensational system
can be seen on one short bookshelf: the collected paperback
writings of “serial polygamist” Hal Lindsey and accountant
Dave Hunt, plus a pile of unread copies of Edgar C. Whise-
nant’s two-in-one book, On Borrowed Time and 88 Reasons Why
the Rapture Is in 1988 (1988), which predicted that the Rapture
would take place in September of 1988. (It also appeared
under other titles.) Mr. Whisenant claims that it sold over a
million copies in 1988. I have also seen the figure of over four
million copies. In any case, a lot of copies were distributed.

That these authors best represent dispensationalism in our
day is. denied (always in private conversation) by the faculty and.
students of Dallas Theological Seminary, but the embarrassed
critics have ignored the obvious: the dispensationial movement
is inherently a paperback book movement, a pop-theology
movement, and always has been. It does not thrive on scholar-
ship; it thrives on sensational predictions that never come true.
Anyone who doubts this need only read Dwight Wilson’s book,
Armageddon Now!"®

1988-1991

‘The year 1988 was the year of the public demise of dispensa-
tional theology: no Rapture. The Church is still here despite
the 40th year of “the generation of the fig tree,” Le., the State
of Israel. Whisenant’s book appeared in July, confidently prop-
hesying the Rapture for September, 1988." Dave Hunt's
Whatever Happened to Heaven? also appeared.

Then, in October, came the book by House and Ice, Domin-
ion Theology: Blessing or Curse? It was a hardback dispensational

16. Dwight Wilson, Armageddon Now!: The Premillenarian Response to Russia and
Israel Since 1917 (Tyler, Texas: Institute for Christian Economics, [1977] 1991).

17. Later, he said it would be by January of 1989. Then he updated it to Sep-
tember of 1989. By then, his victimized former disciples were not listening to him
any more.
