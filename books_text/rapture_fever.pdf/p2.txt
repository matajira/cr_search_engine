copyright, Gary North, 1993

 

Library of Congress Cataloging-in-Publication Data
North, Gary.

Rapture fever: why dispensationalism is paralyzed / Gary

North
p. cm.

Includes bibliographical references and index.

ISBN 0-930464-67-2 : $25. - ISBN 0-930464-65-6 (pbk.) :

$12.95

1, Dispensationalism — Controversial literature 2. Millen-
nialism — United States — History - 20th century. 3. Rapture
(Christian theology) - Controversial literature. 4. Walvoord,
John. 5, House, H. Wayne. Dominion theology: blessing or
curse? 6. Dominion theology. 7. Dallas Theological Seminary.
1. Title

BT157.N67 1993 93-444
230’.046~dc20 CIP

 

Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711
