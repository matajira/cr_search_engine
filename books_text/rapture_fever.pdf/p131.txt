A Commitment to Cultural Irrelevance 97

Fundamentalists believe that the individual Christian must
live in both realms during his stay on earth, but he is not sup-
posed to take the first realm very seriously — the realm of a
person’s job. This is why fundamentalists have invented the
phrase, “full-time Christian service”: it contrasts the world of
faith where ministers and missionaries work vs. the world
where the rest of us work. This distinction is very similar to the
monastic outlook of Roman Catholicism, which distinguishes
between the “secular clergy’ — parish priests who work with
common people in their common affairs — and the “regular
clergy,” meaning the monks who have retreated from the nor-
mal hustle and bustle of life (the “rat race”). Yet your average
fundamentalist would be shocked to learn that he is thinking as
a Roman Catholic thinks. He would probably deny it. But he
has to think this way, for he has adopted the Roman Catholic
(scholastic) doctrine of law: “natural law” for the lower storey,
and God's revelation for the upper storey.

A Culturally Impotent Gospel

Fundamenitalists believe that Christians are not supposed to
devote very much time, money, and effort to transforming the
“secular” world. We are assured that it cannot be transformed,
according to Bible prophecy, until Jesus comes physically seven
years after the Rapture to set up His One World State with
headquarters in Jerusalem. Anything that Christians do today
to build a better world will be destroyed during the seven-year
tribulation period. John Walvoord, former president of Dallas

6. In 1962, I was told by a dispensational college’s president that the Stewart
brothers, who financed the creation of formerly dispensationalist Biola College (then
called the Bible Institute of Los Angeles), and who also financed the publication and
distribution of the tracts that became known as The Fundamentals, shipped crates of
Bibles to Israel to be hidden in caves there, so that Jews could find them during the
Great Tribulation, I was told years later by an amillennial pastor that Arabs tater
used pages in these Bibles for cigarette paper, which may just be a “sour grapes”
amillennial apocryphal legend. The point is this; Why waste money on Bibles to be
hidden in caves? Answer: because of a specific eschatology,
