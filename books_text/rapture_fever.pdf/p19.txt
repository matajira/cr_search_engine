Foreword xxi

And that servant, which knew his lord’s will, and prepared not
himself, neither did according to his will, shall be beaten with
many stripes. But he that knew not, and did commit things
worthy of stripes, shall be beaten with few stripes. For unto
whomsoever much is given, of him shall be much required: and
to whom men have committed much, of him they will ask the
more (Luke 12:47-48).

Just as a parent has more responsibility before God than his
child does, so do those who become successful. They receive
greater blessings, and so they bear more responsibility. But our
successes are supposed to establish our confidence in the fulfill-
ment in history of God's covenantal promises, which should
produce greater obedience, which increases our confidence,
and so on, until He comes again in final judgment. This is
positive feedback: progress. But beware, God warned, that

thou say in thine heart, My power and the might of mine hand
hath gotten me this wealth. But thou shalt remember the Lorp
thy God: for it is he that giveth thee power to get wealth, that he
may establish his covenant which he sware unto thy fathers, as it
is this day (Deut. 8:17-18).

God offers us the possibility of marching from victory unto
victory, if we obey Him by obeying His law. But there are many
Christians who prefer to believe in the Church’s defeat in histo-
ry so that they can live under humanist man’s laws instead of
God's law. They even proclaim this subservience to humanist
politicians, judges, and lawyers as God’s plan for His Church.

I say there is a better choice. That is why I wrote this book.

Ask Yourself These Three Questions

First, do you hope that your work on earth will leave a posi-
tive legacy to future generations, no matter how small the lega-
cy is, even if no one in the future remembers who you were or
what you did? Of course you do. Second, does God's Word
