Endless Unfulfilled Prophecies Produce Paralysis 25

theological discussion of why the events of his day cannot possi-
bly be fulfillments of Bible prophecy if orthodox dispensational
eschatology is correct. At most, they are shadows of things to
come. But such a discussion would kill the excitement of the
reader in hearing “the latest dope” about fulfilled prophecy.
Scholarly books on eschatology do not become best-sellers.

Oswald T. Allis, a postmillennial critic of dispensationalism,
commented in 1945 on this schizophrenic aspect of dispensa-
tional authors: “One of the clearest indications that Dispensa-
tionalists do not believe that the rapture is really ‘without a
sign, without a time note, and unrelated to other prophetic
events’ [he cited Scofield, What Do the Prophets Say?, p. 97] is the
fact that they cannot write a book on prophecy without devot-
ing a considerable amount of space to ‘signs’ that this event
must be very near at hand. .. . This is of course quite incom-
patible with their any moment doctrine.”'* In late 1990 and
early 1991, a huge increase in the sales of “ticking clock” dis-
pensational prophecy books once again proved him correct on
this point.’® The addiction continues. It also debilitates.

A Publishing Coup in the First Half of 1991

In 1974, the year following the beginning of the oil crisis,
Dr. Walvoord wrote one of these paperback potboilers, Arma-
geddon, Oil and the Middle East Crisis. Eventually, it went out of
print. In late 1990, it was resurrected from the dead.'* The
headlines about the imminent war in Kuwait were too powerful

14, Oswald T. Allis, Prophecy and the Church (Philadelphia: Presbyterian & Re-
formed, 1945), pp. 174, 175.

15. Scott Baradell, “Prophets of Doom: We've a leg up on Armageddon,” Dallas
Times Herald (Sept. 8, 1990); Edwin McDowell, “World Is Shaken, and Some Book-
sellers Rejoice,” New York Times (Oct. 22, 1990); “Prophecy Books Become Big
Sellers,” Christianity Today (March 11, 1991}; Nancy Kruh, “The End,” Dallas Morning
News (Feb. 17, 1991).

16. I like to think of this as Dr. Walvoord’s “Lazarus” book. Paraphrasing
Martha's comment to Jesus: “But after 16 years in the tomb, it stinketh!”
