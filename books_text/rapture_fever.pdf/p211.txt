Dispensationalism vs. Sanctification 177

Ideas Have Consequences

MacArthur tells us frankly what traditional dispensational
theology’s rampant antinomianism has produced: churches
filled with immorality.

One of the most malignant by-products of the debacle in
contemporary evangelism is a gospel that fails to confront indi-
viduals with the reality of their sin. Even the most conservative
churches are teeming with people who, claiming to be born
again, live like pagans, Contemporary Christians have been
conditioned never to question anyone's salvation. If a person.
declares he has trusted Christ as Savior, no one challenges his
testimony, regardless of how inconsistent his life-style may be
with God's Word (p. 59).

Who teaches such doctrines of “once saved, always saved, no
matter what”? Col. Bob Thieme does. Yes, the man who had
the world’s largest tape ministry in the early 1960’s. MacArthur
cites Thieme’s book, Apes and Peacocks in Pursuit of Happiness
(1973): “It is possible, even probable, that when a believer out
of fellowship falls for certain types of philosophy, if he is a
logical thinker, will become an ‘unbelieving believer.’ Yet be-
lievers who become agnostics are still saved; they are still born
again. You can even become an atheist; but once you accept
Christ as savior, you cannot lose your salvation, even though
you. deny God” (Thieme, p. 23; MacArthur, pp. 97-98). So
much for I John 2:19: “They went out from us, but they were
not of us; for if they had been of us, they would no doubt have
continued with us: but they went out, that they might be made
manifest that they were not all of us.”

Imputation and Confessien.

MacArthur did his best to refute Lane Hodges and others
who proclaim that people are saved even though they refuse to
proclaim Jesus as Lord. Proclaiming Jesus as savior is sufficient
