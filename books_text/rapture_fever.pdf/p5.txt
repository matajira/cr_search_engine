TABLE OF CONTENTS

 

 

 
  

Foreword 2066p c cece cere tenet eee n eet eennee ix
Preface... 6. cece eee ce eee eect ee eee xxiv
Introduction 6.6... c eee cece eee renee 1
1. Endless Unfulfilled Prophecies Produce Paralysis ..... 19
2. Fear of Man Produces Paralysis............. --40
3. Pessimism Produces Paralysis .....6..6eeeeueeves 61
4. Dispensationalism Removes Earthly Hope .......... 76
5. A Commitment to Cultural Irrelevance ....... +. 91
6. A Ghetto Eschatology ..............000006 - 110
7. House of Seven Garbles ............00000. - 129
8. Revising Dispensationalism to Death ......... - 145
9. Dispensationalism vs. Six-Day Creationism . . - 163
10. Dispensationalism vs. Sanctification ...... - 172
11. Theological Schizophrenia ............65- - 180

12. When “Babylon” Fell, So Did Dispensationalism .... 188
13. The Strange Disappearance of

 

Dispensational Institutions .........+0+e. eee 195
Conclusion 2.6... eee eee eee teen e eee 202
Bibliography .. 2.6.0.0 eee eee 222
Scripture Index . - 235
Index....... 238
A Challenge to Dallas Theological Seminary tener 247
A Three-Year Strategy for Pastors beeen eee ». 248
A Three-Year Strategy for Laymen .......-.00+ e000 249

 

About the Author «0.6... eee cee eee eee 250
