70 RAPTURE FEVER

is more powerful in history than Christ’s defeat of Satan at
Calvary. There is no pessimism greater than Dave Hunt's state-
ment, which is representative of all premillennialism (and amil-
lennialism, for that matter): even the millennial reign of Christ
physically on earth will end when the vast majority of people
will rebel against Him, converge upon Jerusalem, and try to
destroy the faithful people inside the city: “Converging from all
over the world to war against Christ and the saints at Jerusa-
Jem, these rebels will finally have to be banished from God’s
presence forever (Revelation 20:7-10). The millennial reign of
Christ upon earth, rather than being the kingdom of God, will
in fact be the final proof of the incorrigible nature of the hu-
man heart.” (Why these rebellious human idiots will bother to
attack Jerusalem, a city which Hunt believes will be filled with
millions of resurrected, death-proof Christians who returned to
rule with Christ at the beginning of the millennium, is beyond
me. I will let premillennialists worry about this. I have already
provided a postmillennial answer as to what Revelation 20:7-10
means, including who rebels and why, in my book, Deminion
and Common Grace,"® which was written specifically to deal with
this exegetical problem.)

Hunt goes on (and on, and on): “A perfect Edenic environ-
ment where all ecological, economic, sociological, and political
problems are solved fails to perfect mankind. So much for the
theories of psychology and sociology and utopian dreams,”!*
Here is the key word used again and again by premillennialists
to dismiss postmillennialism: wopia. (“Utopia”: ou = no, topos =
place.) In short, they regard as totally mythological the idea
that God's word, God's Spirit, and God’s Church can change
the hearts of mest people sometime in the future. They asswme

9. Dave Hunt, Beyond Seduction, p. 250.

10. Gary North, Dominion and Common Crace: The Biblical Basis of Progress Ciylen,
‘Texas: Institute for Christian Economics, 1987).

LL. Hunt, Beyond Seduction, p. 251.
