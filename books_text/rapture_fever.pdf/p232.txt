198 RAPTURE FEVER

The restructuring we have announced to begin in the fall of
this year [1993] involves both philosophy and method and re-
affirms the primary focus of Grace Theological Seminary on
preparation of ministry leaders."

But what of the previously crucial task of defending dispensa-
tional theology? What of Grace’s intellectual leadership within
American dispensational churches? Silence. Dead silence.

I conclude that President Davis, like an earlier President
Davis, is leading a secession movement within the Grace Breth-
ren Church and American dispensationalism. Grace Seminary has
just seceded from historic dispensationalism. Will he succeed? Proba-
bly. The younger donors are as embarrassed by traditional
dispensationalism as the younger theologians are. This secession
is visible at the top, but bureaucrats always count the costs of
change well in advance. They think they know what the next
generation will accept, and they have concluded that the semin-
ary’s donors will follow them into a theologically vague future.

Dallas Seminary is next. There is not one man remaining on
that faculty who can or will answer Ken Gentry’s book, He Shall
Have Dominion: A Postmillennial Eschatology (Institute for Chris-
tian Economics, 1992). We are hearing the silence of the lambs.
They have left the field of intellectual battle, to the extent that
they were ever on it. Their continuing silence regarding O. T.
Allis’ refutation of dispensationalism, Prophecy and the Church
(1945), indicates that they have never tried to respond intellec-
tually to the challenges offered by professional theologians and
scholars. Charles Ryrie’s brief attempt to respond in Dispensa-
tionalism Today came twenty years too late, and was an ill-fated
effort anyway. Besides, he disappeared from the Dallas faculty
in the early 1980's under clouded circumstances. Dallas Semin-
ary’s faculty members have become theological deaf-mutes.

1. President to Pastor (1st quarter 1993), p. 1.
