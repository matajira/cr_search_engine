Introduction 3

alcoholics and other addicts, a lot of well-meaning dispensation-
alists swear off the addictive prophetic substance, only to return.
to it again as soon as the next pusher shows up with a paper-
back book with a gleaming, multi-color cover. “Only $9.95. Be
the first in your church to know the inside dope!” Everyone
wants the inside dope; again and again, millions of them be-
come the inside dopes.

Hal Lindsey is the most successful pusher in dispensation-
alism’s comparatively brief history. He made a fortune and a
reputation by selling inside dope. As part of his “prophecy
poppers,” Lindsey has written about the terminal generation.
That is also an underlying theme in Rapture Fever. We are now
witnessing the birth of dispensationalism’s terminal generation.
The torch being passed to it is burning very low. Over its tomb-
stone should be placed these words: “Overdosed on Sensation-
alism.”

A Brief History of Dispensationalism’s Brief History

Dispensationalism was invented around 1830, either by 20-
year-old Margaret Macdonald, who received a vision regarding
the pre-tribulation Rapture while in a trance,’ or by John Nel-
son Darby.’ It escalated in popularity in the United States after
the Civil War (1861-65), especially when William E. Blackstone
(W.E.B.) wrote Jesus ts Coming in 1878, Prophecy conferences
became the order of the day. Then came C. 1. Scofield’s im-
mensely successful Scofield Reference Bible (1909). After the wide-
ly publicized embarrassment of the Scopes’ “Monkey Trial” of
1925, Protestant evangelicals retreated into a kind of cultural
shell. Dispensational theology was used to justify this withdraw-
al. The creation of the State of Israel in 1948 seemed to prove
that the prophetic message of dispensationalism was on track:

2. Dave MacPherson, The Unbelievable Pre-Tib Origin (Kansas City, Missouri:
Heart of America Bible Society, 1973).
3. This is the conventional view.
