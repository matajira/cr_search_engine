164 RAPTURE FEVER

ers taught no other view of creation. Augustine wrote in the
City of God concerning those people who believe in a long histo-
ry of the earth: “They are deceived, too, by those highly men-
dacious documents which profess to give the history of many
thousand years, though, reckoning by the sacred writings, we
find that not 6000 years have yet passed” (XII.10). Mendacity,
indeed!

Such was the opinion of Christian orthodoxy for almost sev-
enteen centuries. But then in the late 1600's opinions began to
change. As men began to study the geologic column in detail,
they began to conclude that the world is much older than had
previously been believed by Christians, though of course all
religions not tied to the Old Testament had always denied that
history is so short. The pagan presuppositions of all anti-biblical
religions began to seep into the Church through sedimentary
rocks, as it were.

A Loss of Faith

For fifty years prior to the publication of Charles Darwin's
Origin of Species in 1859, Christian intellectuals steadily aban-
doned faith in a literal six-day creation. They extended the
time frame of what was considered acceptable regarding biblical
chronology. Writes evolutionist Michael Ruse:

However, by 1859, even in Victorian Britain, nearly all intelli-
gent and informed people realized that one could no longer
hold to a traditional, Biblically inspired picture of the world: a
world created by God in six days (of twenty-four hours each); a
world of very, very recent origin (4004 B.C, was the favored date
of creation, based on genealogies of the Bible); and, a world.
which at some subsequent point had. been totally covered and
devastated by a monstrous flood. Through the first half of the
nineteenth century, scientific discovery after scientific discovery
had modified these traditional beliefs.’

3. Michael Ruse, Darwinism Defended: A Guide to the Evolution Controversies (Read-
