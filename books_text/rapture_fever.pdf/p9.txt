Foreword xi

There was a cost in not taking it: death. There was a valu-
able benefit in taking it: eternal life. But what he did not tell
me - what so few fundamentalist-dispensationalists ever tell
those with whom they share the message of salvation — is that
there ave costs in accepting the gift. It is a free gift in the sense that
it is offered freely to those who do not deserve it, but it is not
a free gift in the sense that it does not require changes in a
persons life ~ a lifetime of changes. For example:

Know ye not that the unrighteous shall not inherit the kingdom
of God? Be not deceived: neither fornicators, nor idolaters, nor
adulterers, nor effeminate, nor abusers of themselves with man-
kind, Nor thieves, nor covetous, nor drunkards, nor revilers, nor
extortioners, shall inherit the kingdom of God {I Cor. 6:9-10).

Clear, isn’t it? It was surely clear to those who received Paul’s
warning, for some of them had come out of such a lifestyle:
“And such were some of you: but ye are washed, but ye are
sanctified, but ye are justified in the name of the Lord Jesus,
and by the Spirit of our God” (I Cor. 6:11).

There are some henefits available to you as a Christian that
you have not been willing to claim as a dispensationalist. Like
the free gift of grace that must be acknowledged and accepted
by the recipient in order to complete salvation’s transaction, so
must these other gifts be acknowledged and accepted by the
recipient in order to be completed. But millions of Christians
have been told by their teachers and friends that these gifts are
not for Christians. These gifts are supposedly only for those
people who will be converted to Christ during a future millen-
nium. Until then, dispensational theologians insist, it is “hands
offi” These gifts are supposedly not for this dispensation.

With this book, I intend to persuade you to accept both the
reality of these gifts and their obligations, just as my friend in
1959 persuaded me to accept the gift of eternal life.
