174 RAPTURE FEVER

to them all, If any man will come after me, let him deny him-
self, and take up his cross daily, and follow me.” And finally, he
cites Hebrews 12:14: “Follow peace with all men, and holiness,
without which no man shall see the Lord.”

You mean that the Bible teaches that without holiness ~ ethi-
cal set-apartness — the professing Christian’s verbal confession
of faith in Christ is of zero value? Worse: that his verbal profes-
sion in fact will testify against him eternally in the lake of fire?
You mean to say, as MacArthur says, that

Real salvation is not only justification. It cannot be isolated
from regeneration, sanctification and, ultimately, glorification.
Salvation is an ongoing process as much as it is a past event. It is
the work of God through which we are “conformed to the image
of His Son” (Romans 8:29, cf. Romans 13:11). Genuine assur-
ance comes from seeing the Holy Spirit's transforming work in
one’s life, not from clinging to the memory of some experience
(p. 28).

Oh, my! To say that this book caused great consternation in
the “just confess Jesus as Savior, but not necessarily as Lord,
and be eternally saved” camp is putting it mildly. Dr. Mac-
Arthur did more than launch a torpedo into the side of the
good ship Antinomianism; he in fact detonated a charge from
deep inside its bulwarks.

What Is the Target of His Attack?

His target is the theology (soteriology) of C. 1. Scofield,
Lewis Sperry Chafer, Charles C. Ryrie, Zane C. Hodges, and
Col. R. B. Thieme, whose works MacArthur footnotes scrupu-
lously and refutes thoroughly. It is a full-scale assault on the
theological foundations of Dallas Seminary, Moody Monthly, and
almost every independent Bible church and Bible college in the
country.
