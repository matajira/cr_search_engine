Dispensationalism vs. Sanctification 175

Whose writings does he use in order to refute dispensation-
alism’s ethics? Calvinist Presbyterians Benjamin B. Warfield,
Geerhardus Vos, and J. Gresham Machen, Calvinist Baptist
Arthur Pink, the Puritans, and Calvinist-charismatic Martyn
Lloyd-Jones. Oh yes, and Ken Gentry, whose two books on the
pre-70 A.D. dating of the Book of Revelation were published
the next year, 1989.

Something very peculiar is going on here.

He starts out by announcing Clearly that “salvation is by
God's sovereign grace and grace alone. Nothing a lost, degen-
erate, spiritually dead sinner can do will in any way contribute
to salvation. Saving faith, repentance, commitment, and obedi-
ence are all divine works, wrought by the Holy Spirit in the
heart of everyone who is saved. I have never taught that some
pre-salvation works of righteousness are necessary to or are any
part of salvation” (p. xiii). So much for Arminianism, “free will,”
and the agreed-upon theology of 95% of those who call them-
selves evangelicals today. But he does not stop with the doc-
trines of total depravity and salvation by grace alone. He imme-
diately goes to ethics: the doctrine of progressive sanctification:

But I do believe without apology that real salvation cannot and
will not fail to produce works of righteousness in the life of a
true believer. There are no human works in the saving act, but
God's work of salvation includes a change of intent, will, desire,
and attitude that inevitably produce the fruit of the Spirit. The
very essence of God’s saving work is the transformation of the
will that results in a love for God, Salvation thus establishes the
root that will surely produce the fruit (p. xiii).

What is also astounding is that this dispensationalist author
actually cites the very verses we Calvinists appeal to in demon-
strating that God’s grace includes predestined good works, Ephe-
sians 2:8-10: “For by grace are ye saved through faith; and that
not of yourselves: it is the gift of God: Not of works, lest any
man should boast. For we are his workmanship, created in
