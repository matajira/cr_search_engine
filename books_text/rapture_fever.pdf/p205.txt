Dispensationalism vs. Six-Day Creationism 171

extent of the task that lies ahead. Why? Because they do not
really believe that there is sufficient time remaining to the
Church to complete this task.

Conclusion

The failure of the vast majority of dispensationalism’s theolo-
gians to defend a six-literal-day creation is only one part of its
intellectual paralysis. Meanwhile, the failure of the few dispen-
sational six-day creationists to extend their doctrine of God’s
fiat creation into the social sciences and the arts constitutes
another aspect of this paralysis.

The Darwinists knew what to do with their scientific alterna-
tive to Christianity’s view of God, man, law, and time. Within
one generation after the publication of The Origin of Species,
they were completely victorious in the academic and intellectual
world. In contrast, within one generation after the publication
of The Genesis Flood, not one dispensational creationist scholar
has produced a book in his own academic field on the applica-
tion of six-day creationism, except in the natural sciences. (I
must not be unfair; amillennial six-day creationists have been
equally silent in the social sciences and the arts.)

The paralysis of dispensationalism has not yet been relieved,
even by the most courageous of its academic representatives.
Ideas have consequences. The idea of the imminent Rapture,
the idea of the inevitable cultural defeat of the Church, and the
idea that God’s revealed law is annulled in this dispensation are
bad ideas, and they have produced bad results.

‘To reconstruct theology, we must begin with the doctrine of
the six-day creation. It points to the absolute sovereignty of
God and the absolute distinction between God's being and
creation. God spoke the world into existence, and He placed it
under Jaw. Man was given the dominion covenant: to serve as
God’s representative in history. The curse on the earth is pro-
gressively removed in history through the power of the resur-
rection: the biblical doctrine of corporate sanctification.
