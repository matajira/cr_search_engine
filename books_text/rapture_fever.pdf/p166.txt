132 RAPTURE FEVER

books. In fact, until House’s book appeared, Dallas Seminary’s
recent faculty members were known mainly for their unwilling-
ness to write books on dispensational themes. They have avoid-
ed the whole topic like a plague — or like a topic that could get
them fired if they slipped up. They know that if they initiate an
attack, they will then be called upon to defend themselves, and
they all know that they cannot successfully defend themselves by using
the broken shield of Scofield’s rickety, patched-up system. This is
House’s dilemma; he must now defend himself. It was a risk-
free deal for Tommy Ice; not for Wayne House.

A quiet revolution has been going on at Dallas Seminary.
Dallas has quietly abandoned “the true and ancient faith, as
delivered by Lewis Sperry Chafer.” The outline of the “new,
improved” dispensational faith, as tentatively offered by Profes-
sor Craig Blaising in 1988, is as yet unclear in its details. [He
co-edited the 1993 book, Dispensationalism, Israel and the Church.
This book is narrow in its focus. What is lacking is a compre-
hensive presentation of the new dispensationalism.]

Obviously, the holders of this reworked version of the faith are
skating on thin career ice. If they go too far, they will lose their
jobs, and where do you go to teach seminary as a “not quite
dispensationalist”? Yet they know that they can no longer de-
fend the dispensational faith, even in its revised, 1967, New
Scofield Reference Bible version. Dallas Seminary’s theological
position has become increasingly murky as its student body has
grown to.1,700.

Don Quixote Rides Again!

Into this scene rode Tommy Ice and his faithful, cautious,
and somewhat hesitant partner, Wayne House, like Don Qui-
xote and Sancho Panza, with Ice seated shakily on the aging
Rosinante of the Scofield Reference Bible notes. These two chival-
yous warriors have engaged in a seriés of fierce battles against
a squad of stick men, mostly of their own creation, labeled
“Rushdoony,” “North,” “Bahnsen,” “Chilton,” and “Jordan.”
