xii RAPTURE FEVER

Gift #1: Our Participation in God’s Earthly Kingdom

Jesus said of eternal life that it does not begin at the time of
a redeemed person’s physical death. It begins when a person
accepts as his possession Jesus Christ’s perfect life, His bodily
death and resurrection, and His ascension in glory to the right
hand of God in history. Eternal life begins in history:

He that believeth on the Son hath everlasting life: and he that
believeth not the Son shall not see life; but the wrath of God
abideth on him (John 3:36).

Similarly, Jesus’ told us that as members of His eternal
Church, we are the heirs of the Old Covenant kingdom that
God had given by grace to the Jews. The Church receives the
kingdom inheritance of Israel. Not only did Jesus tell this to His
disciples, He also told it to the Jews of His day, who hated Him
for saying it:

Therefore say I unto you, The kingdom of God shall be taken
from you, and given to a nation bringing forth the fruits thereof
(Matt. 21:48),

This new “nation” is not some geographical, political entity; it
is an international, spiritual entity: His Church. But the Church
is more than spiritual; it is an institution, made up of real, live
human beings. God’s kingdom is broader than His Church.

Benefits

As members of Christ’s kingdom, born-again Christians are
heirs of all the promises attached to such membership. Jesus
said in His famous Sermon on the Mount: “But seek ye first the
kingdom of God, and his righteousness; and all these things
shall be added unto you” (Matt. 6:33). Al these things: food to
eat, liquids to drink, and clothes to wear (Matt. 6:31). Meek
before almighty God, Christians can be bold before men. This
