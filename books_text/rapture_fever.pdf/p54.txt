20 RAPTURE FEVER

turned out - by premillennial, dispensational authors. The
book received guarded praise from the dean of dispensational
scholars, John F Walvoord, who for three decades served as
the president of Dallas Theological Seminary. He said modern
dispensationalists can “learn from it many important lessons
applicable to interpretation today.”* But one dispensational
scholar failed to learn a single lesson from Wilson’s book: John
F. Walvoord.

As a U.S. war with Iraq loomed in late 1990, Walvoord
revised his 1974 book, Armageddon, Oil and the Middle East Crisis,
and it sold over a million and a half copies ~ a million by Feb-
ruary, 1991.4 It did so by rejecting Dr. Wilson’s warning: do
not use sensational interpretations of Bible prophecy in order
to sell books. If you do, he warned, you will look like a charla-
tan in retrospect, and you will also injure the reputation of
Christ and His Church. But the tremendous lure of sensational-
ism’s benefits — book royalties and fame — was too great for Dr.
Walvoord. A dispensational feeding frenzy for prophecy books
was in full force as war loomed in the Middle East in the sec-
ond half of 1990. Dr. Walvoord decided to feed this frenzy.

It was at that point that Walvoord publicly rejected his earli-
er belief in the “any-moment Rapture” doctrine. This was proof
that he had abandoned traditional scholarly dispensationalism
and had adopted the pop-dispensationalism of Hal Lindsey,
Dave Hunt, and Constance Cumbey ~ what I like to call dispen-
sensationalism. (Most of his colleagues at Dallas Theological
Seminary remained, as usual, discreetly silent. They know
exactly how their bread is buttered: by donations from laymen
who are thoroughly addicted to sensational prophecies.)

The leaders of American dispensationalism have not resisted
the lure of huge book royalties and a few moments in the pub-

8. J. E Walvoord, “Review of Armageddon Now,” Bibliotheca Sacra (ApriiJune
1981), p. 178.
4. Time (Feb. 11, 1991).
