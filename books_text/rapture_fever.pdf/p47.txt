Introduction 13

eAre there biblical alternatives for sinful thoughts and practices
in every area of life?

elf so, what are they, specifically?

*Where do we discover them, specifically?

*Who has taught in detail about these alternatives?

Have any of these teachers been dispensationalists?

*Does the phrase “we're under grace, not law” apply to crimi-
nals? To policemen and civil judges? To lawyers?

*Is politics dirty?

*Could the gospel of Jesus Christ clean up politics?

*1f most politicians were converted to saving faith in Jesus Christ
today, what changes could we expect tomorrow? In a century?
In a millennium?

*If the answer is “none,” is Christianity politically irrelevant?
*If the answer is “many,” where in the Bible should we look to
discover the actual content of these specific changes?

“If Old Testament law is not valid in the New Testament era,
where do we find New Testament legal standards for social
ethics?

“What dispensational author has written a detailed study of New
Testament law and soctal ethics?

“What dispensational institution teaches courses in New Testa-
ment social ethics?

*Is abortion a.sin?

«Is abortion grounds for excommunication?

*Is abortion a crime?

*Where do we look in the Bible to find a law against abortion,
other than Exodus 2] :22-26?

*What public stand did the seminaries take in 1973 when Roe v.
Wade was handed down? In 1983? In 1993?

¢Should seminary professors actively preach against abortion in
classes on ethics?

¢Do dispensational seminaries provide classes on ethics?

*Is homosexuality a sin?
*Is homosexuality said to be a crime in the Bible?
