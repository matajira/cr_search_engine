26 RAPTURE FEVER

a temptation. They offered him a unique opportunity to revive
his career at age 80. Since the first version of the book had not
cost him his academic reputation within dispensational circles
{he had none outside these circles), there seemed to be no
reason not to try to cash in again. Feeding frenzies must be fed,
after all. Apparently, publishing highly specific interpretations
of Bible prophecy — interpretations that are disproved within a
year or two, and possibly six months — has something important
to do with spreading the gospel. So, Dr. Walvoord allowed
Zondervan to republish this revised 1974 potboiler, and it sold
{as of late August, 1991) 1,676,886 copies.’ The theological
cost of this publishing coup was high: Walvoord’s explicit aban-
donment of the “any moment Rapture” doctrine of traditional
dispensational eschatology. Yet in the July/September 1990
issue of Bibliotheca Sacra, Walvoord had dismissed Gentry’s state-
ment in House Divided that dispensationalists are date-setters:
“[VJery few of its adherents indulge in this procedure.”

To complete Walvoord’s move to dispensensationalism, his
publisher announced his latest book, Major Bible Prophecies: 37
Crucial Prophecies That Affect You Today, in August, 1991. The
timing, as we shall see, was perfect . . . for anti-dispensational
critics of the system.

if John Walvoord, who at age 80 was the last of the old-line
dispensational theologians, could not resist the siren call of
sensationalism in his own “last days,” then what dispensation-
alist can? As Dr. Wilson proves, not many dispensational au-
thors have resisted it since 1917. Dispensationalists have been
visibly addicted to sensationalism. It is an addiction that is not
easily broken. The “highs” that sensationalism briefly provides
during any Middle Eastern crisis are just too alluring. Gary
DeMar identifies this devastating addiction as “last days mad-

17. Press Release, “Kudos,” Zondervan Publishing House (August, 1991). This
figure may not include returned copies which ought to be quite high, given what
happened in the USSR in August.
