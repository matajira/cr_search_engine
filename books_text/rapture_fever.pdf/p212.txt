178 RAPTURE FEVER

to get them into heaven. MacArthur denies this. He says that
people must accept Jesus as Lord or else their sins will destroy
the validity of their verbal confession.

Neither side in this debate understands the biblical doctrine
of imputation. God imputes Christ’s perfection to each person
at the time of his or her conversion. That is, God declares
judicially that the person is not guilty in His court because of
the completed work of Jesus Christ in His substitutionary
death. What MacArthur and his opponents do not discuss is the
content of this judicial imputation. It is total. That is, the total
perfection of Christ’s ministry becomes the inheritance of the
redeemed believer. This means that Christ's public confession of His
own work is imputed to the believer. Christ is a legal representative,
just as Adam was. His perfect confession becomes each be-
liever’s confession before God. It does not matter that the be-
liever confesses imperfectly in history. He may or may not ever
recognize that Jesus is His sovereign Lord, but Lordship salva-
tion is inescapable judicially. Jesus Christ makes the sinner’s confes-
sion for him. This is a neglected aspect of the biblical doctrine of
representation, commonly called the substitutionary atonement.

The definitive sanctification — Christ’s moral perfection -
which the believer received through imputation at the time of
his conversion cannot stay still. History is inescapable. We move
from spiritual infancy to spiritual maturity. We work out the
salvation that God extends to us. Thus, definitive sanctification
leads to progressive sanctification in history. Progressive sancti-
fication culminates in final sanctification on the day of final
judgment. So, as Christians mature, their confessions are sup-
posed to become more precise, and their behavior is more and
more to reflect these ever-improving personal confessions.
‘Thus, Hodges is correct: men do not need to confess Jesus as
Lord in order to be saved. Thus, MacArthur is correct: if there
is no evidence of progressive sanctification in the confessing
church member’s life, he is not saved, i.e., he was not the recip-
ient of definitive sanctification. “Once saved, always saved” is
