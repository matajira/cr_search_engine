Bibliography 225

Eschatology. Tyler, TX: Institute for Christian Economics, 1992.
A comprehensive statement for postmillennialism and against
premillennialism and amillennialism.

Henry, Matthew. Matthew Henry’s Commentary. 6 volumes.
New York: Fleming H. Revell, (1714). A popular commentary
on the whole Bible by a still-popular seventeenth-century theol-
ogian.

Hodge, A. A. Outlines of Theology. Enlarged edition. London:
The Banner of Truth Trust, (1879) 1972. A nineteenth-century
introduction to systematic theology: question-and-answer form.

Hodge, Charles. Systematic Theology. 3 volumes. Grand Rap-
ids, MI: Eerdmans, (1871-73) 1986. A standard Reformed text
by Princeton Seminary’s most renowned nineteenth-century
theologian. Volume 3 includes a discussion of eschatology.

Kik, J. Marcellus. An Eschatology of Victory. N.p.: Presbyterian
and Reformed, 1975. Preterist exegetical studies of Matthew 24
and Revelation 20.

Murray, Iain. The Puritan Hope: Revival and the Interpretation
of Prophecy. (Edinburgh: Banner of Truth, 1971). Historical
study of postmillennialism in England and Scotland, beginning
in the eighteenth century.

North, Gary, ed. The Journal of Christian Reconstruction,
Symposium on the Millennium (Winter 1976-77). Historical and
theological essays on postmillennialism.

North, Gary. Millennialism and Social Theory. Tyler, TX: Insti-
tute for Christian Economics, 1990. A study of the failure of
premillennialism and amillennialism to deal with social theory
as an explicitly biblical enterprise.

Owen, John. Works, ed. William H. Goold. 16 volumes. Edin-
burgh: The Banner of Truth Trust, 1965. The seventeenth-
century Puritan preacher and theologian; volume 8 includes
