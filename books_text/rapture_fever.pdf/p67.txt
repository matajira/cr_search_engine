Endless Unfulfilled Prophecies Produce Paralysis 33

tains of Israel.” This was followed in 1991 by Mr. Jeffries’ Messt-
ah: War in the Middle East & the Road to Armageddon. It included
such “hot off the press” chapters as these: “Russia's Appoint-
ment With Destiny” and “The Rise of Babylon, The War in the
Gulf.” He warned his readers: “Watch for Iraq to recover and
return to the project of rebuilding mighty Babylon” (p. 109).
This is the dispensationalist’s equivalent of a never-ending,
thrill-packed serial called, “The Perils of Paulene Eschatology,”
which always ends: “Continued Next Book!” Fundamentalists
just cannot seem to get enough of these books: the literary
equivalents to romance novels, The addiction never ends.

Failed visions require extensive revisions. Let me list a few of
what I call the “harvest” of soon-to-be-revised books:

Dave Hunt, Global Peace and the Rise of Antichrist (Harvest
House, 1990)

E. Davidson, islam Israel and the Last Days (Harvest House,
1991)

Jerry Johnson, The Last Days on Planet Earth (Harvest House,
1991)

Peter Lalonde, One World Under Anti-Christ (Harvest House,
1991)

Chuck Smith, The Final Curtain (Harvest House, 1991)

To this list we can add:

Thomas S$. McCall and Zola Levitt, The Coming Russian Inva-
sion. of Israel, Updated (Moody Press, 1987)

Robert W. Faid, Gorbachev! Has the Real Antichrist Come? (Vic-
tory House, 1988)

Erwin W. Lutzer, Coming to Grips with the Role of Europe in
Prophecy (Moody Press, 1990)

Gary D. Blevins, The Final Warning! (Vision of the End Minis-
tries, 1990)

Paul McGuire, Who Will Rule the Future? A Resisiance to the New
World Order (Huntington House, 1991)
