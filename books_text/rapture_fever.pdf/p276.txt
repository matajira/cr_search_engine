242 RAPTURE FEVER

dispensationalism fading,
59

dominion, 99-100, 124

helicopter man, 87-88

imminent Rapture, 89

King must be present, 80

New Age & Reconstruction,
73-74

recruitment & suicide, 122

reforms are hopeless, 89-90

Whatever Happened? 105

Hussein, Saddam, 190

Ice, Thomas D.
Dallas Seminary, 145, 214
House &, 129, 221
Jast defender, xxxiv, 221
premillennial ethics, 83, 92
Whitby myth, 152
imputation, 178
inferiority complex, xxvi
inheritance
earth, xvi, 77, 120
kingdom, xiv, xv
personal, xxi-xxii
saints’, xvii
Iraq, 20, 25, 34, 107, 190
irrationalism, 92
Israel, State of, 27-28, 31,35-
38, 190-91, 211
{see also Jews)

Jerusalem, 43, 45, 49, 70, 97
Jesus (sse Christ)

Jews, 35-38, 42

John the Baptist, 196
Johnson, S. Lewis, 83, 131

Kant, Immanuel, 92
Keynes, John Maynard, 42
KGB, 188
kingdom
all creation, 94
Caesar’s, 110-11
Church &, xii, xiii
civilization, xiv, xxii, 85
cost, xv-xvi
footstool, xix
God's vs. Christ’s, xx
God’s vs. heaven, xiii-xiv
House (1992), xiv-xv, 144
inheritance, xiv
inner only, 139
judgment rendered, 111
Politics &, 40-43
Tepresentation, xix
Satan’s vs. God’s, xviii
Scofield, xii
seek first, xii
Kirban, Salem, 54-58
Kuhn, Thomas, 6
Kuwait, 190

Lalonde, Peter, 100

Larkin, Clarence, 176

last days, 45, 47-51

Latin, 114-15

Lattage, Beverly, 187

Jaw.
commandments, xvi
dispensational, xxvii, xxix
fundamentalism vs., 94-95
gospel &, 206
grace, 95, 176
inheritance, xv
kingdom, xv
