Bibliography 229

thorough critical analysis of the theological failures of the pre-
trib position’s developers and defenders.

Boersma, T. Is the Bible a Jigsaw Puzzle: An Evaluation of Hal
Lindsey's Writings. Ontario, Ganada: Paideia Press, 1978. An
examination of Lindsey’s interpretive method, and an exegesis
of important prophetic passages.

Bray, John L. Israel in Bible Prophecy. Lakeland, FL: John L.
Bray Ministry, 1983. An amillennial historical and biblical dis-
cussion of the Jews in the New Covenant.

Brown, David. Christ’s Second Coming: Will It Be Premillennial?
Edmonton, Alberta, Canada: Still Water Revival Books, (1876)
1990. A detailed exegetical study of the Second Coming and the
Millennium by a former premillennialist who became postmil-
lennial.

Cox, William E. An Examination of Dispensationalism. Philadel-
phia, PA: Presbyterian and Reformed, 1963. A critical.look at
major tenets of dispensationalism by former dispensationalist
who became amillennial.

Gox, William E. Why I Left Scofieldism. Phillipsburg, NJ: Pres-
byterian and Reformed, n.d. A critical examination of major
flaws of dispensationalism.

Crenshaw, Curtis I. and Grover E. Gunn, II. Dispensation-
alism Today, Yesterday, and Tomormw. Memphis, TN: Footstool
Publications, (1985) 1989. Two Dallas Seminary graduates take
a critical and comprehensive look at dispensationalism.

DeMar, Gary. The Debate Over Christian Reconstruction. Ft.
Worth, TX: Dominion Press, 1988. A response to Dave Hunt
and Thomas Ice after their 1988 debate with Gary North and
Gary DeMar. Includes a brief commentary on Matthew 24. The
book was co-published by American Vision, Adlanta, GA. Audio-
tapes and a videotape of this debate are available from ICE, P.
O. Box 8000, Tyler, TX 75711.
