Conclusion 219

to what you are doing, you are going to have to start defending
your own point of view and giving us solid reasons for giving
credence to what you are doing. I find this strangely lacking in
your literature.

This is a very strange statement from a representative of a
theological ‘position that has produced nothing new since
Charles Ryrie’s Dispensationalism Today (1965), from a professor
at the seminary that unceremoniously fired Ryrie over a decade
ago. Misrepresentations, if they really are misrepresentations,
are quite easy to prove. All it takes is a published, book-length
Tesponse with line-by-line refutations. I offer as a fine example
Bahnsen and Gentry's House Divided. They took apart the accu-
sations of Dr. House and Rev. Ice, piece by piece. But academic
dispensationalists have refused to respond to our supposed mis-
representations, except for Dr. House, and then (mysteriously)
he was no longer on the Dallas Seminary faculty. Eventually,
the younger faculty members learn a lesson: publicly defend
the system from its critics, and you will find yourself unem-
ployed. It is prudent to remain silent. And so they do.

Rapture Fever is my response to my challenger’s accusation. If
dispensationa! scholars have the material, and also have the
willingness to enter into a public debate with me in the form of
a series of books like this one, they should do so. Gentlemen, it
really isn’t very difficult to respond if you have done your
homework. But when the intellectual representatives of a 160-
year-old theological movement can muster only one book-
length response in a decade — Dominion Theology: Blessing or
Curse? — and then the men who offered it subsequently fail to
answer the immediate book-length rebuttal - House Divided - a
perceptive observer is tempted to conclude: “They just don’t
have the firepower! They are out of ammo.” Indeed, they are.

Perhaps the Dallas professor who initiated the challenge will
review Rapture Fever in Bibliotheca Sacra. Or perhaps he will
prudently remain silent. One ching is sure: he will not write a
