12 RAPTURE FEVER

dispensational paradigm. So are the self-conscious evasions by the
few remaining academic theologians who are willing to defend
traditional dispensationalism in print. These theologians have
rarely been six-day creationists, especially those who have

taught at Dallas Theological Seminary, which, following Sco-

field’s notes, has never made the six-day creation a test of or-

thodoxy. (Scofield was a “gap” theologian: an indeterminate
gap of time between Genesis 1:1 and 1:2. See Chapter 9.) Here

are just a few of the questions that demand answers but which
receive no responses in print from dispensationalists.

*Is evolution the religious faith which undergirds every human-
ist institution in today’s world?

*Does the Bible teach evolution or creation?

*Should biblical creationism also have comparable effects in
every human institution?

¢What are these creationist alternatives to evolutionism in social
thought?

*Where do we find information about them?

*Have dispensational creationists ever discussed these creationist
social alternatives in detail?

*Is humanism religiously neutral? -

*Is humanism morally neutral?

*Are the public schools religiously neutral?

*If they aren’t, where do we find dispensational schools and
especially colleges that provide comprehensive alternatives to
humanism in every classroom?

*Is intellectual neutrality a myth?

*If intellectual neutrality is a myth, does the Bible provide real-
world intellectual alternatives?

1s sin comprehensive, affecting everything in history?

*Are all men completely responsible to God for every sin they
commit?

*Is the gospel as comprehensive as sin?

*Is the healing power of the gospel as comprehensive as sin?
