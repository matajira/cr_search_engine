A Ghetto Eschatology 121

preach God's covenani lawsuit to nations as well as individuals: a
covenant lawsuit in history that includes both law and sanctions.
Problem: premillennialists deny the historical validity of God’s
sanctions in New Covenant history. They also have a tendency
to deny the continuing validity of God's Old Covenant case law
applications of the Ten Gommandments. They are, in short,
antinomians. They reject the specific sanctions that God has
always required His covenant people to preach to the lost.
Premillennialists no longer believe that God raises up Jonahs to
preach God’s covenant lawsuit: a message warning of the com-
ing destruction of any covenant-breaking society that persists in
its evil ways. They no longer believe that God brings negative
sanctions in history against covenant-breaking societies.

The pessimilfennialist, whether premillennial or amillennial,
wants Christians to believe that God no longer backs up His
own covenant with action. In fact, God supposedly has allowed
Satan to impose the terms of his covenant: covenant-breakers
get steadily richer and more powerful, while covenant-keepers
are consigned by covenant-breakers to living in ghettos in be-
tween persecutions. The pessimillennialist is content with life in
his ghetto because he believes that the only alternatives in
history are life in the Gulag archipelago or literal execution.

_ Does Eschatology Matter?

People frequently ask me, “Does it really make much differ-
ence what eschatology a Christian holds?” My answer: “It de-
pends on what the particular Christian wants to do with his
life." So far, at least, eschatology has been a major factor in
sorting out the published leaders from literate followers in what
has become known as the Christian Reconstruction movement.
This is the more academically oriented branch of the dominion
theology movement. There are numerous defenders of domin-
ion theology who maintain publicly that they are still premiilen-
nialists, although we have yet to see a book by one of these
premillennialists that states clearly just exactly how God’s call to
