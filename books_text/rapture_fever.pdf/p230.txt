196 RAPTURE FEVER

Significant change rarely comes without significant pain and challenge.
None of the seven full-time faculty contracts will be renewed for next
year. [Italics in original]

You may have wondered why President Davis and the Board
fired Professor John C. Whitcomb in 1990, when Whitcomb
had only three months to go until retirement. Whitcomb was
the only Grace Seminary faculty member with a reputation
outside the campus, as the co-author of The Genesis Flood. Some
people could not understand why he had been fired, and Presi-
dent Davis’ official explanation was, to put it charitably, non-
informative. Well, now we know. “Significant change” was in
the works, and Dr. Whitcomb had been warning against it. He
wound up analogous to John the Baptist: decapitated.

My friends, when the entire full-time faculty of an institution
is fired in one shot, we are talking about something greater
than rearranging the academic furniture.

Then President Davis went on: “We will be discontinuing the
Th.M. and Th.D. programs... .” That is to say, Grace Theological
Seminary is leaving the field of advanced academic studies. It
will no longer grant an advanced academic theological degree.
The other “practical” degrees will remain — degrees for men
who want preaching jobs: Master of Divinity, Doctor of Minis-
try, etc, Also, Grace Theological Journal is being discontinued. In
short, Grace Theological Seminary has just committed suicide at the
professional academic level. The question is: Why?

In part, because Grace is facing universal pressures in theo-
logical education today. Small, struggling movements can no
longer afford the luxury of full-time faculties that teach a hand-
ful of academic specialists. That is to say, they can no longer
afford to pay their faculty members to reproduce themselves.
They can no longer afford traditional academic certification.
Dispensationalism is becoming a small, struggling movement. Paralysis
has produced attrition. Attrition is steadily shrinking the dis-
pensational movement. Yet no one inside admits this in public.
