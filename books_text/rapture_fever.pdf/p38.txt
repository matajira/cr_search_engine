4 RAPTURE FEVER

there was at long last a nation for the army of the invader from
the North to surround. The post-Rapture Great Tribulation of
the Jews now became geographically possible. During the Great
Tribulation, according to dispensational theology, two-thirds of
the world’s Jews will surely perish.*

But a change in outlook began in 1976 with the nomination
of Jimmy Carter as the Democratic Party’s candidate for Presi-
dent. Initially, he seemed to many voters to be an evangelical.
Bob Slosser, who later became Pat Robertson’s ghost writer,’
co-authored The Miracle of Jimmy Carter (1976), and Logos
Books published it. When Carter’s Presidency turned out to be
just another humanist experiment in internationalism, just as
conservatives and libertarians had predicted, the evangelicals
did not retreat back into political isolation. The Reagan candi-
dacy in 1980 galvanized them. Thus was born the Christian
Right. Its premier manifestation was the Religious Roundtable’s
National Affairs Briefing Conference, held in Dallas in August,
1980, when thousands of Christians came to the Reunion Arena
for three days of political education. (See Chapter 11.)

With the return of fundamentalists to politics came a quiet,
almost embarrassed shelving of the doctrine of the Rapture.
This doctrine had long served them as a theological justification
for passivity. After all, if all of a man’s good works and all of the
church’s efforts to reform this world will inevitably be smashed
by the Antichrist during the seven-year Great Tribulation, then
there is no earthly payoff. Conclusion: concentrate on passing
out gospel tracts instead.

We have seen very few gospel tracts being passed out. by
North American Christians since the 1970’s. The era of the
gospel tract appears to be over. The gospel tract has been re-

4, John F Walvoord, israel in Prophecy (Grand Rapids, Michigan: Zondervan
Academie, [1962] 1988), p. 108.

5. Pat Robertson (with Bob Slosser), The Seeret Kingdom (Nashville: Nelson, 1982).
Slosser tater wrote (with Cynthia Ellenwood) Changing the Way America Thinks (Dallas:
Word, 1989).
