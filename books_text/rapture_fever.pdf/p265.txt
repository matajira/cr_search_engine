Bibliography 231

Mauro, Philip. The Seventy Weeks and the Great Tribulation.
Swengel, PA: Reiner Publishers, n.d. A-former dispensationalist
re-examines prophecies in Daniel and the Olivet Discourse.

Miladin, George C. Is This Really the End?: A Reformed Analysis
of The Late Great Planet Earth. Cherry Hill, NJ: Mack Publish-
ing, 1972. A brief postmillennial response to Hal Lindsey’s
prophetic works; concludes with a defense of postmillennial
optimism.

Provan, Charles D. The Church Is Israel Now: The Transfer of
Conditional Privilege. Vallecito, CA: Ross House Books, 1987, A
collection of Scripture texts with brief comments.

Vanderwaal, C. Hal Lindsey and Biblical Prophecy. Ontario,
Canada: Paideia Press, 1978. A lively critique of dispensation-
alism and Hal Lindsey by a preterist amillennialist.

Weber, Timothy P Living in the Shadow of the Second Coming:
American Premillennialism 1875-1982. Grand Rapids, MI: Zon-
dervan/Academie, 1983, This touches on American dispensa-
tionalism in a larger historical and social context.

Wilson, Dwight. Armageddon Now!: The Premillenarian Response
to Russia and Israel Since 1917. Tyler, TX: Institute for Christian
Economics, (1977) 1991. A premillennialist historian studies the
history of failed dispensational prophecy. He warns against
“newspaper exegesis.”

Wocedrow, Ralph. Great Prophecies of the Bible. Riverside, CA:
Ralph Woodrow Evangelistic Association, 1971. An exegetical
study of Matthew 24, the Seventy Weeks of Daniel, and the
doctrine of the Anti-Christ.

Woodrow, Ralph. His Truth Is Marching On; Advanced Studies
on Prophecy in the Light of History. Riverside, CA: Ralph Wood-
row Evangelistic Association, 1977. An exegetical study of im-
portant prophetic passages in Old and New Testaments.
