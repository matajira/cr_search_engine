Dispensationalism vs. Sanctification 173

sensation. Here was a major dispensationalist author who chose
to have J. I. Packer, Calvinist theologian and neo-Puritan pro-
fessor, write one Foreword, and James Montgomery Boice,
Calvinist theologian and Presbyterian minister, write a second
Foreword. But even more surprising, they both consented to
write.

Something very peculiar was going on here.

The Issue is Obedience: Sanctification

Boice writes:

... In The Gospel According to Jesus, MacArthur is not dealing
with some issue or issues external to the faith, but with the cen-
tral issue of all, namely, What does it mean to be a Christian?
His answers address themselves to what I consider to be the
greatest weakness of contemporary evangelical Christianity in
America.

Did I say weakness? It is more. It is a tragic error. It is the
idea — where did it ever come from? - that one can be a Chris-
tian without being a follower of the Lord Jesus Christ. It reduces
the gospel to the mere fact of Christ’s having died for sinners,
requires of sinners only that they acknowledge this by the barest
intellectual assent, and then assures them of their eternal security
when they may very well not be born again. This view bends
faith far beyond recognition — at least for those who know what
the Bible says about faith ~ and promises a false peace td thou-
sands who have given verbal assent to this reductionist Christian-
ity but are not truly in God’s family (p. xi).

Boice then goes on to quote Matthew 10:22, the verse that
defends the traditional Calvinist doctrine of the perseverance of
the saints: “And ye shall be hated of all men for my name’s
sake: but he that endureth to the end shall be saved.” Then he
cites Luke 6:46: “And why call ye me, Lord, Lord, and do not
the things which I say?” Then he cites Luke 9:23: “And he said
