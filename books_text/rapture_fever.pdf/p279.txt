restoration, xxiv
resurrection, xix
Revelation, 44-45
revolution, 140

Robertson, Pat, 187, 211
Robison, James, 180, 187
Roe us. Wade, xxxii, 13, 141
Romania, 208-9

Ruse, Michael, 164

Russia, 27, 31, 188-89, 190-91
Byrie, 46, 118, 131, 147-48

sacrifices, 48
Sadducees, 43
sanctification
corporate, 171
definitive, 178
dispensationalism vs., 172-
79
progressive, 175
Santa Claus, 76
Satan
army, xvi
covenant, 121
earthly rule, 80
power, 69-70
power of, xvii
representation, xix
victor, 65-66
Saucy, Robert, 148
Sauer, Erich, 147
Schaeffer, Francis, 9
schizophrenia, 42
Schnitger, David, 84, 104, 134
scholarship, xxix, xvi
Scofield, C. I.
apostles’ rule, xx
“At hand,” 23

Index

245

creation, 163
“gap theory,” xxix, 72-73,
163, 166
kingdom, xiii
kingdoms, xx
sacrifices restored, 50n
sayonara, 201
shuffle, 166-67
secession, 198
secular, 95, 100
secular clergy, 97
secularism, xxvi
seminaries
abortion &, xxxti
Big Three, 5
black-out, 155-57
donors, 157
paradigm shift, 14-15
silence, 34, 109
sensationalism, 20, 26, 28, 59,
193
signs, 58
silence
Dallas Seminary, 5, 14, 20,
129, 131, 132, 158, 160-
61, 198, 219
dispensationalism, xxxi-
xxxii, 221
Grace Seminary, 198
leaders, 125
Lindsay’s teachers, 2
social theory, 141
strategy, xxxi-xxxii, 220
suicidal, xxxv, 160

sin, xviii, 12-13
Smith, Bailey, 183
social ethics, 92, 119, 206-7
