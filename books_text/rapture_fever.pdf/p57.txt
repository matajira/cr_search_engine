Endless Unfulfilled Prophecies Produce Paralysis 23

ology of dispensationalism, which stresses the Church as a
“Great Parenthesis” which was neither known nor prophesied
about in the Old Testament. The New Testament Church (pre-
Rapture) supposedly has no connection whatsoever with the
dispensation of the Mosaic law. Therefore, if the prophecies of
the Old Testament apply to the Church in any sense rather
than exclusively to national Israel, the entire dispensational
system collapses.

CG. I, Scofield understood this clearly. Dispensationally speak-
ing, there can be no biblically prophesied event in between the
founding of the Church and the Rapture. Citing Matthew
4:17b, “Repent: for the kingdom of heaven is at hand,” Scofield
wrote: “‘At hand’ is never a positive affirmation that the person
or thing said to be ‘at hand’ will immediately appear, but only
that no known or predicted event must intervene.” Therefore,
the Rapture can take place at any moment. But if this is true,
then its corollary is also necessarily true: the Rapture cannot be
said to be imminent for our generation. It may be, but it may not
be. An orthodox dispensationalist cannot legitimately say when
it will be, one way or the other. The Rapture cannot legitimate-
ly be said to be almost inevitable tomorrow, next month, or next
year. Edgar Whisenant’s 88 reasons for the Rapture in Septem-
ber, 1988, were wrong = all 88 of them.'° So were his (revised)
89 reasons for 1989." (As Stayskill put in a cartoon, how
many will reasons will he offer in the year 2000?) Yet the mass
appeal of the system is its near-term date-setting.

9. Scofield Reference Bible (New York: Oxford University Press, 1909), p. 998, note

10, Edgar C. Whisenant, 88 Reasons Why the Rapture Will Be in 1988. It was also
published as The Rosh Hash Ana 1988 and 88 Reasons, Why (1988). The name is
pronounced “WHIZnant.”

11, A 1989 Associated Press story reported on Whisenant’s revised predictions.
The Rapture was due in September, 1989. He published The Final Shout: Rapture
Report — 1989. “The time is short,” he said. “Everything points to it, All the evidence
has piled up.” Tyler Morning Telegraph (Aug. 25, 1989). Something had indeed piled
up, but it was not evidence.
