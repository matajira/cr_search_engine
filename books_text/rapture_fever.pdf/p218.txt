184 RAPTURE FEVER

Gentlemen, all this talk about overcoming the political, moral,
economic, and social evils of our nation is sheer nonsense. The
Bible tells us that everything will get steadily worse, until Christ
comes to rapture His church out of this miserable world. Noth-
ing we can do will turn this world around, all your enthusiasm
is wasted. All your efforts are in vain. All the money and time
you devote to this earthly cause will go down the drain. You
can't use biblical principles — a code term for Old Testament
jaw — to reconstruct society. Biblical Jaw is not for the church
age. Victory is not for the church age. However, get out there
and work like crazy. It’s your moral duty.” Not a very inspiring
speech, is it? Not the stuff of political victories, you say. How
correct you are!

Ever try to get your listeners to send you money to battle the
forces of social evil by using some variation of this sermon? The
Moral Majority fundamentalists have smelled the opposition’s
blood. since 1978, and the savory odor has overwhelmed their
official theology. So they have stopped talking about the Rap-
ture.

But this schizophrenia cannot go on forever. In off-years, in
between elections, the enthusiasm may wane. Or the “Christian”
political leaders may appoint the same tired faces to the posi-
tions of high authority. (I use the word “may” facetiously; the
Pied Pipers of politics appoint nobody except secular human-
ists. Always. It will take a real social and political upheaval to
reverse this law of political life. That upheaval is coming.) In
any case, the folks in the pews will be tempted to stop sending
money to anyone who raises false hopes before them. So the
Moral Majority fundamentalist preachers are in a jam. If they
preach victory, the old-line pessimists will stop sending in
checks. And if they start preaching the old-line dispensational,
premillennial, earthly defeatism, their recently motivated audi-
ences may abandon them in order to follow more optimistic,
more success-oriented pastors.
