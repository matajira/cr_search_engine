xiv RAPTURE FEVER

heaven, he says that this idea goes all the way back to John
Nelson Darby, who is generally regarded as the founder of
dispensationalism.’ It was taught by Scofield and by the found-
er of Dallas Seminary, Lewis Sperty Chafer’ But then he adds:
“Subsequent publications by dispensationalists show signs of
revision.” He cites J.. Dwight Pentecost, Alva McClain, and John
EF. Walvoord. “Other dispensationalists have: essentially aban-
doned any distinction between the kingdom of heaven and the
kingdom of God.” He cites the Ryrie Study Bible, Clarence E.
Mason, Jr., Stanley Toussant, and Robert Saucy. He concludes:
“Again this shows that dispensationalism is not a fixed set of
confessional interpretations but that development is taking
place.”* A theological distinction which for over a century was
regarded as crucial to the dispensational system is now optional.

This means that there is now no good theological reason for
dispensationalists not to accept the magnificent inheritance that
all other branches of the Christian Church have accepted since
the early Church: the kingdom of God, which is the same as
the kingdom of heaven. These are two terms used to describe
the kingdom of Jesus Christ, both in history and in eternity.

The kingdom of God is not some purely internal experience;
it is the realm of God’s authority in history, the true civilization
of God, where our churches, our families, our schools, our
businesses, and our governments are all operated in order to
please God, according to His will. As Jesus taught us to pray:
“Thy kingdom come. Thy will be done in earth, as it is in heay-
en” (Matt. 6:10). This prayer is answered progressively in history, not
merely in heaven, just as “Give us this day our daily bread” is also
answered in time. This is why H. Wayne House, a dispensa-
tional theologian and social activist, can write the following
about God’s kingdom:

2. Idem.
3. Iid., p. 260.
4, Ibid., p. 262.
