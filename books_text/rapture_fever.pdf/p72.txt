38 RAPTURE FEVER

of tribulation? Is it because we deny that the Great Tribulation
is in the future? I think so. Yet Lindsey calls us anti-Semitic!”*

A defective view of evangelism testifies to a defective theology. This
is why dispensationalism is in a state of near-paralysis. It cannot
make sense of the Bible. It cannot make sense of what Chris-
tians’ responsibilities are in history, even the responsibility of a
narrowly defined form of evangelism. Rapture fever is morally
paralyzing. It is cherefore culturally paralyzing.

Conclusion

There is a heavy price to be paid for all of this, and the
fading reputation of the American evangelical Church is part of
that price. Dispensational fundamentalists are increasingly
regarded by the humanist media as “prophecy junkies” ~ not
much different psychologically from those supermarket tabloid
newspaper readers who try to make sense of the garbled writ-
ings of Nostradamus, whose name is also selling lots of books
these days. When secular newspaper reporters start calling
Christian leaders to expound on Bible prophecy and its rela-
tionship to the headlines, and then call occultists and astrolo-
gers for confirmation, the Church of Jesus Christ is in bad
shape. Read Armageddon Now! to find out just what bad shape
this is, and has been for over seven decades.

John Walvoord’s “ticking clock” book and others just like it
in 1991 were the equivalent of General Norman Schwarzkopf’s
saturation bombing strategy: they flattened orthodox dispensa-
tionalism. Almost immediately after theses books’ publication,
General Schwarzkopf’s strategy in Iraq buried the very short-
lived “Babylon Literally Rebuilt” dispensationalism. Then, six
months later, the failed Soviet coup buried “Magog from the
North” dispensationalism. What is left? Not much. Dispensa-
tionalists must now begin to rebuild the ruins. It will do no
good to deny the existence of these ruins. They are much too

28. Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989).
