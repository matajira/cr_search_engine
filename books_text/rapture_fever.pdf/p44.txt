10 RAPTURE FEVER

question remains. As the humanist savages continue to shoot
their flaming arrows into the highly flammable wagons of fun-
damentalism, it is becoming clear to a minority of those trapped
inside that fundamentalism’s traditional defensive tactic is no
longer working. Captain Jesus has not visibly arrived. But those
in leadership positions who steadfastly refuse to consider the
alternative ~ an offensive breakout — have only one response:
“Captain Jesus is coming soon! This time, He will! Trust us!”

This is Rapture fever. Rapture fever destroys the will to
extend God's principles of justice and restoration beyond the
narrow confines of a religious ghetto. Its public manifestation is
a series of increasingly frantic appeals for everyone to believe
that “history belongs to the savages, and there is not much
history remaining.” Its philosophy of history is simple to sum-
marize: “All efforts of Christians to build a world that will in-
creasingly reflect Christ’s glory and righteousness are doomed
in our dispensation.” What is the proof? There is no proof.
There is only an appeal: “Trust us!”

An increasing number of younger fundamentalists are saying
to themselves, and occasionally to their peers: “Why should we
trust them? They have been wrong about the imminent Rap-
ture for over a century and a half. Why shouldn't Christians go
on the offensive for a change? Why must we live out our lives
inside this little circle, with both the wind and these savages
howling in our ears until we die or get raptured, whichever
comes first?” These questions demand answers. This is why a
paradigm shift-has begun, in evangelicalism in general and
dispensationalism in particular.

Questions Producing Dispensationalism’s Paradigm Shift

In the pews of fundamentalist churches, faithful, simple
people still accept the broad outlines of the received dispensa-
tional paradigm, even though they are incapable of sitting
down, Bible in hand, and explaining to a nondispensationalist
the evidence for their belief, verse by verse. When they search
