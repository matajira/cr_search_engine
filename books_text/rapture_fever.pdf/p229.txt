13

THE STRANGE DISAPPEARANCE OF
DISPENSATIONAL INSTITUTIONS

But sanctify the Lord God in your hearts: and be ready always to give an
answer to every man that asketh you @ reason of the hope that is in you
with meekness and fear (I Pet. 3:15)

Where can traditional dispensationalists find answers? In
1985, there were three major theological seminaries that taught
dispensational theology: Dallas, Grace, and Talbot. In the mid-
to-late 1980's, Talbot very quietly replaced both ‘its faculty and
its theology. Not wishing to alienate its dispensational donors,
it did not do this with fanfare, but it was done. Its new theology
has been left undefined. The school is in transition.

On December 10, 1992, John J. Davis, the president of
Grace Theological Seminary, sent out a form letter. It began,
“Dear Friend of Grace Seminary,” in good form letter fashion.
President Davis spoke of “recent changes taking place here at
the seminary.” He mentioned the fact that “a rumor is usually
half-way around the world before truth has his shoes on!”

This is true. In this case, however, “the truth, the whole
truth, and nothing but the truth” is still being covered up, in
good academic administrator fashion. He wrote:
