176 RAPTURE FEVER

Christ Jesus unto good works, which God hath before ordained
that we should walk in them” (pp. 95-96). Fundamentalists
almost never quote Ephesians 2:10. MacArthur does. He even
refers the reader to James 2:14-26, the New Testament’s pre-
mier passage on faith and good works.

Then MacArthur goes for the jugular. After affirming duti-
fully that “Dispensationalism is a fundamentally correct system
of understanding God’s program through the ages” (p. 25), he
then rejects on the same poge the number-one thesis of dispensation-
alism, the distinction between ages of law and grace.

The age of law/age of grace distinction in particular has
wreaked havoc on dispensationalist theology and contributed to
confusion about the doctrine of salvation. Of course, there is an
important distinction to be made between law and grace. But it
is wrong to conclude, as Chafer apparently did, that law and
grace are mutually exclusive in the program of God for any age.

Next, he goes after the traditional dispensationalist dichoto-
my between the Sermon on the Mount (“law for Israel and the
Jewish-Christian Millennium only”) and the Church Age. He
cites Clarence Larkin’s 1918 standard, Dispensational Truth, in
which Larkin affirmed that the teachings of Jesus delivered in
His Sermon on the Mount “have no application to the Chris-
tian, but only to those who are under the Law, and therefore
must apply to another Dispensation than this” (p. 26). To which
MacArthur replies:

But that is a dangerous and untenable presupposition. Jesus
did not come to proclaim a message that would be invalid until
the Tribulation or the Millennium. He came to seek and to save
the lost (Luke 19:10). He came to call sinners to repentance
(Matthew 9:13). He came so the world through Him might be
saved (John 3:17). He proclaimed the saving gospel, not merely
a manifesto for some future age (p. 27).
