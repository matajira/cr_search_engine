Conclusion 207

The dispensationalists are more consistent in their rejection
of the task of developing Christian social ethics than other
Protestant evangelicals are, but the reality is this: all of them
have rejected the motivation to become. a social ethicist (post-
millennialism) as well.as the judicial foundation of biblical social
ethics (theonomy). They hate God’s law. They hate personal
and corporate responsibility. Therefore, they hate the idea of
Christianity’s victory in history, for corporate ethical conformity
to God’s law inevitably produces victory (Deut. 28:1-14).

Joining the Losing Side

Over four decades ago, Whittaker Chambers gave his rea-
sons for his departure from the Communist Party. His book,
Witness (1952), is the classic among many book-long testimonies
by former American Communists. I bought the book in 1959,
after it had gone out of print, in a book store run by the Forest
Home Christian Conference Center in California. No one had
bought that lone copy in seven years. I suspect that it had been
put on the shelf because the book store manager thought it was
a book on handing out gospel tracts. I don’t know. What I do
know is that no one had bought it. Chambers gave this explana-
tion of his defection from the Party:

Tn 1937, I repudiated Marx’s doctrines and Lenin’s tactics.
Experience and the record had convinced me that Communism
is a form of totalitarianism, that its triumph means slavery to
men wherever they fall under its sway and spiritual night to the
human mind and soul. I resolved to break with the Communist
Party at whatever risk to my life or to myself and my family. Yet,
so strong is the hold which the insidious evil of Communism
secures upon its disciples, that I could still say to someone at that
time: ‘I know that I am leaving the winning side for the losing
side, but it is better to die on the losing side than to live under
Communism.’ (p. 541)
