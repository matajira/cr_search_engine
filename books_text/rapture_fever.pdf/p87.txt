Fear of Men Produces Paralysis 53

mentalist Christians are repeatedly lured by the tempting pro-
mise that they can be “the first ones on their block” to be “on
the inside” — to be the early recipients of the “inside dope.”
And that is exactly what they have been solid, decade after
decade.

Nine-year-old children were not totally deceived in 1938.
They knew the difference between real life and make-believe.
Make-believe was thrilling; it was fun; it was inexpensive; but it
was not real. The decoded make-believe secrets turned out to
provide only fleeting excitement, but at least they could drink
the Ovaltine. Furthermore, children eventually grow up, grow
tired of Ovaltine, and stop ordering secret decoders.

When will Christians grow up? When will they grow tired of
an endless stream of the paperback equivalent of secret decod-
ers? When will they be able to say of themselves as Paul said of
himself: “When I was a child, I spake as a child, I understood
as a child, I thought as a child: but when I became a man, I
put away childish things” -(I Cor. 13:11)?

False Prophecies for Fun and Profit

Those Christians who believe that we are drawing close to
the last days are continually trying to identify both the Beast
and the Antichrist. This game of “find the Beast and identify
the Antichrist” has become the adult Christians’ version of the
child’s game of pin the tail on the donkey. Every few years, the
participants place blindfolds over their eyes, turn around six
times, and march toward. the wall. Sometimes they march out
the door and over a cliff, as was the case with Edgar C. Whise-
nant, whose best-selling two-part book announced in the sum-
mer of 1988 that Jesus would surely appear to rapture His
Church during Rosh Hashanah week in mid-September. Half
the book was called On Borrowed Time. The other was more
aptly titled, 88 Reasons why the Rapture is in 1988. I can think of
one key argument why his book’s thesis was incorrect: no Rap-
ture so far, and it is now February, 1993. So much for all 88
