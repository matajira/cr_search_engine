32 RAPTURE FEVER

to the point, how long will vulnerable premillennial Christians
allow themselves to be subjected to the fires of eschatological
sensationalism —- prophecies that never come true? How many
best-selling, “ticking clock,” paperback prophecy books will they
buy before they catch on to what is being done to them?

The dispensational world deliberately ignored Dr. Wilson’s
superb chronicling in 1977 of the wildly false predictions about
Russia and the State of Israel that began after the October
Revolution of 1917. Can today’s dispensationalists also ignore
the obvious implications of the failed coup of August 19-21,
1991? How? Will they argue that the millions of Bibles shipped
to the Soviet Union after 1985 had no impact? That God does
not honor in history those who honor His written word? If
dispensationalists argue this way, what does this say about their
view of God?

Wanted: Revised Editions

Dozens of paperback prophecy books were published in the
U.S. from 1981 to 1991. None of the pre-1989 books forecasted
the fall of the Berlin Wall in 1989; none of the 1990 and 1991
books forecasted the failed Soviet coup of 1991. Were these two
events relevant prophetically? If the answer is “yes,” the paper-
back prophets should have foreseen both events. If the answer
is “no,” why were Communism and Russia said for decades to
be relevant prophetically? These “experts” never foresee accu-
rately. They bury their previous prophecies in unmarked graves.

Usually, these authors do not bother to revise their books.
They just publish new books. Revisions are just too embarrass-
ing. Think of Grant Jeffrey’s book, Armageddon: Appointment
With Destiny (1988), published by an obscure press in Ontario,
Canada, which proclaimed “144,000 in print” just before the
book was picked up by Hal Lindsey’s secular publisher, Bantam
Books (located at 666 Fifth Avenue, New York City). Section 3
of Mr. Jeffrey's book is titled, “Russia’s Appointment With
God.” Chapter 7 is “Russia’s Day of Destruction on the Moun-
