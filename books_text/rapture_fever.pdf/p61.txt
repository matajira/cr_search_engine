Endless Unfulfilled Prophecies Produce Paralysis 27

ness.”"5 The addicts never remember-their last round of with-
drawal pain, when their confident expectations of imminent
deliverance once again failed to come true. Dr. Wilson’s book,
Armageddon Now, is an attempt to remind them of those many
failed prophecies. It offers them an example of academic integ-
rity, as well as a helping hand psychologically. Addicts of prop-
hecy sensationalism need both: integrity and psychological help.

A Soviet Coup in the Second Half of 1991

In early 1991, Walvoord told the world that the biblical clock
of prophecy was ticking. He was wrong. It was not the clock of
prophecy that he heard ticking; it was a time bomb for popular
dispensationalism. It exploded on August 21, 1991: the defeat
of the Communist coup in the Soviet Union, unquestionably the
most startling three-day geopolitical reversal of the twentieth
century.

When the coup began on August 19, geopolitical affairs still
looked as though dispensensational prophecy books could
conceivably be salvaged. But when this coup failed, it ended any
immediate or even intermediate threat to the State of Israel
from Russia (“Magog”). The Soviet Union has disintegrated.
The republics declared their independence. During the coup,
the Soviet KGB’ and the Red Army’s military masters could
not even contro! downtown Moscow, let alone invade the State
of Israel. Today, whatever military resources Russia has at its
disposal must be reserved for a possible civil war. Unless the

18. Gary DeMar, Last Days Madness (rev, ed.; Atlanta, Georgia: American Vision,
1993).

19. The Soviet KGB must be distinguished from the Russian KGB, which was at
odds with the Soviet wing. I have been informed that the opposition of the Russian
KGB is what saved Yeltsin’s life. The head of che Russian KGB gave an ultimatum to
the coup’s leaders on Monday morning: if Yeltsin dies, there will be immediate
consequences. Since he was the General in command of the air force bases surround-
ing Moscow, he had the clout to enforce this ultimatum. This is all hearsay, but it is
worth pursuing by some historian.
