The Strange Disappearance of Dispensational Institutions 197

But this is only part of the story. President Davis then
dropped this theological bombshell on page three of his form
letter:

12. What are the mission and values of Grace Seminary?
dis mission is to: “develop Christian ministry leaders who can influence
culture with an integrated biblical world and life view.”

He re-stated this mission in his newsletter, President to Pastor
(Ist quarter 1993, p. 2). Question: When was the last time you
heard a leader of a dispensational seminary speak of the need
to influence culture? When was the last time you heard him call
for “an integrated biblical world and life view’? I'll tell you: the
next time will be the first time.

Where is Grace Seminary heading? I don’t know. But this I
do know: when a seminary president starts talking about “bibli-
cal world and life view,” he is not talking about traditional
dispensationalism. He has moved to a new theological position, and
he is planning to take the school with him. This ts what Talbot did
in the late 1980's.

Of course, I could be wrong. President Davis is retaining the
part-time faculty and hiring new full-time members. Perhaps
members of this new faculty, including President Davis, have
quietly developed a brand-new synthesis of Scofield’s theology.
Maybe they are about to introduce the long-awaited “new dis-
pensationalism,” whose theology leads to Christian cultural rele-
vance and activism. This synthesis has never been published, of
course. The present faculty's members have not mentioned it.
But perhaps the long-awaited dispensational paradigm shift will
be completed and presented in public in broad outline: culeur-
ally relevant dispensationalism. If so, its details will not be pub-
lished in the Grace Theological Journal, since President Davis and
his supporters on the Board have discontinued the journal.
(Perhaps it will be resurrected during the millennium.) Presi-
dent Davis announced to the seminary’s pastor-donors:
