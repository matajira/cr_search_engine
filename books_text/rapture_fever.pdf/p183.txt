Revising Dispensationalism to Death 149

water, but underneath the surface there is a lot of rapid pad-
dling going on. The fact is, when House and Ice were finished
with their. attack on Christian Reconstructionism, their targets
remained intact ~ in fact, completely untouched — but House
and Ice were out of ammunition. Worse: they had blown up
the barrel of their lone remaining canon. That they suspected
that this might be the case was indicated by their refusal to
allow me and Gary DeMar to see their book’s pre-publication
manuscript in early 1988, despite the fact that we were sched-
uled to debate Tommy Ice, who was not a published book
author at the time. (A similar lack of confidence burdened Hal
Lindsey, who also refused to allow me to read the pre-publica-
tion manuscript of The Road to Holocaust, despite my repeated
written appeals.) People who are confident about their opinions
will allow their targeted victims, upon request, to read the
attacking manuscripts in advance. (Our responses get into print
so rapidly anyway, why bother to play coy?)

Within months of the publication of Dominion Theology, Pro-
fessor House had departed from Dallas Seminary. The reasons
were always obscure — rather like Dr. Ryrie’s departure earlier
in the decade. House was hired by an obscure Baptist college
on the West Coast. In 1992, House left that college, too. He is
no longer employed by any fundamentalist institution.

Pentecost’s Quiet Revision: Leaven and. Evil

Dispensationalists can appeal to modern books on eschatol-
ogy and the millennial kingdom written by McClain and John
Walvoord, but the major presentation of their eschatological
position is found in Things to Come (1958) by Dallas Seminary
professor J. Dwight Pentecost. Unknown to most readers, he
has significantly revised the book in a key area, and in doing
so, he has abandoned the traditional dispensational case for the
inevitable defeat of the Church in what the dispensationalists
call the “Church Age.” In the original edition, he argued for
the eventual triumph of unbelief in this, the “Church Age.” He
