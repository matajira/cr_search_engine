eschatology, 43-44, 121-22
eternal life, x-xi

eternity, 93-94

ethics, 206

evangelicalism, xxxii
evangelism, 35-38, 41, 91, 122

false prophets, 54
Falwell, Jerry, 187, 194, 199-
201
FBL, x
final judgment, 74, 79-80
footstool, xix, 79
foreign policy, 31
fandamentalism
defense, 9
despair, 96
divided, 9
fed up, 9
Taw, 94-95
Rapture fever, 88
Roman Catholic, 97
time perspective, 115
two storeys, 95-97, 100,
101-2
withdrawal, 8

“gap” theory, xxix, 72-73, 166-
67

Geisler, Norman, 42, 203

Genesis Flood, 168, 170, 171

ghetto, xxv, 8, 10, 110-28

ghostwriter, 1-2

gift, xi, xxt

God, x, 68-69

good works, xxii, 175-76

Gorbachev, 107

gospel, 63, 64, 65-66, 77

241

(see also Great Commission)
gospel tracts, 4-5
grace, 176
Grace Seminary, 108, 195-99
Graham, Billy, 182
Great Commission, xxiii, 41,
64, 88
Great Tribulation
Daniel’s 70th week, 21
date of, 45, 52
good works &, xxii
Jews perish, 4, 35-38
past event, xxiii

Halsel, Grace, 36n, 37n

head transplants, 55

heaven, xv, 94, 96, 105

helicopter escape, 87-88

history, 119, 178

hoax, 190-91

Hodge, Charles, 169

holiness, 174

Holmes, O. W., 170

House, H. Wayne, 129-44
date of Revelation, 44
death penalty, 146
Dominion Theology, 129, 221
kingdom, xiv-xv, 144
“night shift” religion, 78
retreat, 221
silence, xxxiii, 220

Howe, Frederick, 168

humanism, xxv-xxvii, xxxii, 9,
92-93, 128, 203

Hunt, Dave
dating Revelation, 44-45
defeat is inevitable, 70, 77,

99
