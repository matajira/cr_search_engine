140 RAPTURE FEVER

They then quote favorably Samuel-J. Andrews, whose words,
if taken literally (the “Dallas hermeneutic”), set forth the foun-
dation for the rule of the tyrannical Sainis - something the authors
accuse us Reconstructionists of promoting. We have seen the
results of this sort of premillennialism in the revolutionary
“Christian” communist movements of the late Middle Ages."
This is what can happen when you combine apocalyptic premil-
lennialism and a theological hostility to revealed biblical law:

It is as its Head that He rules over [the Church], not as its King;
for this latter title is never used of this relation. Nor is His rule
over His Church legal and external, like that of an earthly king.
... The relation between Him, the Head, and the Church, His
Body, is a living one, such as nowhere else exists, or can exist;
His will is the law, not merely of its action, but of its life... . He
rules in the Church through the law of common life. . . (p. 235).

Spoken like a true Brother of the Free Spirit! Get out your
shotguns and hide your wives and daughters: premillennialists
are on the march again. Fortunately, this book tells dispensa-
tionalists not to march, but to stand dead still:-“. . . God has
told us to take up a defensive posture against the enemy...
stand and resist . . . the sword is for a counterattack . . . stand”
(p. 156). This book is a 460-page tract to stand pat for Jesus. It is
a theological defense of John Milton’s line, “They also serve
who only stand and wait.” Milton had an excuse, however. He
was totally blind. Then again, now that I think about it... .

Garble #5: Neither Biblical Law Nor Natural Law

The non-Reconstructionist Christian, we are told, “is not
under the law as a rule of life; rather we are under the law of
Christ” (p. 184). This they call Wisdom. Wisdom “does not

12, Norman Cohn, The Pursuit of the Millennium (2nd ed.; New York: Harper
Torchbook, 1961); and the early modern era: Igor Shafarevich, The Socialist Phenome-
non (New York: Harper & Row, [1975] 1980), ch. 2.
