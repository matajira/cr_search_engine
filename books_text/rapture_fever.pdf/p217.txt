Theological Schizophrenia 183

1980. Almost nobody was talking about the imminent return of
Christ. The one glaring exception was Bailey Smith, President
of the Southern Baptist Convention, who later told reporters
that he really was not favorable to the political thrust of the
meeting, and that he came to speak only because some of his
friends in the evangelical movement asked him. (It was Smith,
by the way, who made the oft-quoted statement that “God does
not hear the prayer of a Jew.” Ironically, the Moral Majority
got tarred with that statement by the secular press, when the
man who made it had publicly dissociated himself from the
Moral Majority. He has since disavowed the statement, but he
certainly said it with enthusiasm at the time. I was seated on the
podium behind him when he said it. It is not the kind of state-
ment that a wise man makes without a lot of theological qualifi-
cation and explanation.)

In checking with someone who had attended a similar con-
ference in California a few weeks previously, I was told that the
same neglect of the Rapture doctrine had been noticeable. All
of a sudden, the word has dropped out of the vocabulary of
politically oriented fundamentalist leaders. Perhaps they still
use it in their pulpits back home, but on the activist circuit, you
seldom hear the term. More people are talking about the sover-
eignty of God than about the Rapture. This is extremely signifi-
cant.

How can you motivate people to get out and work for a
political cause if you also tell them that they cannot be success-
ful in their efforts? How can you expect to win if you don’t
expect to win? How can you get men elected if you tell the
voters that their votes cannot possibly reverse society's down-
ward drift into Satan’s kingdom? What successful political
movement was ever based on expectations of inevitable external
defeat?

The Moral Majority is feeling its political strength. These
people smell the blood of the political opposition. Who is going
to stand up and tell these people the following? “Ladies and
