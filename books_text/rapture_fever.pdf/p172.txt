138 RAPTURE FEVER

Church history professor John Hannah presented the evidence?
Why has he been silent about this since 1973? Why hasn't any
fulltength historical refutation of MacPherson’s thesis appeared
from the traditional dispensational camp?

The two authors then attack postmillennialism because it was
supposedly invented by unitarian Daniel Whitby, who was born
in 1638, despite the overwhelming evidence that the New Eng-
land Puritans of the 1630’s were postmillennial, and that they
brought the doctrine to North America from England. The
authors know about Iain Murray's book, The Puritan Hope, in
which the Puritan origins of postmillennialism are discussed.’*
They know that as the editor of The Journal of Christian Recon-
struction, I published an entire issue on “Puritanism and Prog-
ress” (Summer, 1979), in which the documentary evidence is
presented. They simply ignore all this. They write: “Thus, the
system called postmillennialism was born in the early 1700s as
a hypothesis” (p. 209) This is not scholarship; this is self-conscious
propaganda and active deception of their unsuspecting and overly
trusting readers. This is a high school debating technique dis-
guised as scholarship. This is not the way that men with aca-
demic integrity are supposed to conduct public debate, let
alone Christian academics. As I said earlier, Professor House
has the most to lose; he had an academic reputation prior to
this book.

When Mr. Ice launched this ancient “Whitby” attack in the
rebuttal portion of our April 1988 debate, I reminded him that
his system was invented in 1830, and that Calvinistic postmillen-
nialism can trace its history at least back to 1630. (Actually, it
goes back to John Calvin.) He did not respond to my rebuttal.
How could he? But he drags out all the old arguments again, as
if he had neyer attended the debate, as if he were deaf. He is
deaf. Judicially deaf. Hearing he will not hear. (And let it be
known: Professor House was also in attendance that evening.)

10. Edinburgh: Banner of Truth, 1971.
