Revising Dispensationalism to Death 147

Chafer’s clothing, It is not that the dispensational Emperor has
no clothes; it is that the few presentable clothes that he has
were stolen from his long-term rival's wardrobe.

Ryvie’s Tactic

It should also be noted that Charles Ryrie played a similar
academic game in Dispensationalism Today back in 1965. He used
arguments very similar to.O. T. Allis’ covenant theology to
defend traditional dispensationalism against the discontinuity-
based attacks by ultradispensationalists (e.g., E. W. Bullinger, C.
R. Stam, J. C. O’Hair). I refer here to the devastating and
utterly irrefutable (for a Scofield dispensationalist) argument of
the ultradispensationalists that Acts 2 (Pentecost) was clearly a
fulfillment of Joel 2. Peter specifically referred to the prophecy
in Joel 2 in Acts 2:16-20. This means that an Old Testament
prophet forecasted the events of Acts 2. This poses a horren-~
dous problem for Scofieldism. Dispensational theology has
always taught that the so-called “Church Age” - also called “the
great parenthesis” - was completely unknown in the Old Testa-
ment and not predicted by any prophet. But Peter said that
Pentecost was known to an Old Testament prophet, Joel. The
conclusion is inescapable: the Church could not have begun at ~
Pentecost; it must have started later. This is exactly what the ultra-
dispensationalists argue — a heretical idea, clearly, but absolutely
consistent with the dispensational view of the Church as the
great parenthesis.

To escape this problem of radical discontinuity, ie., New
Testament Church vs. Old Testament prophecy, Ryrie appealed
to Erich Sauer, but in fact Sauer’s argument rests squarely on the
arguments of postmillennial Calvinist O, T. Allis. The Church was
indeed founded at Pentecost; the events of Pentecost were
merely transitional. No radical discontinuity should be assumed.
here, Ryrie insisted. So did Allis.? Ryrie also used Stam-type

8. Ryric cites Sauer’s argument that the “mystery” of Ephesians 3:1-12 — the
