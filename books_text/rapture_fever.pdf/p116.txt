82 RAPTURE FEVER

better place than they found it? None, says Lehman Strauss in
Dallas Seminary’s journal, Bibliotheca Sacra:

We are witnessing in this twentieth century the collapse of
civilization. It is obvious that we are advancing toward the end of
the age. Science can offer no hope for the future blessing and
security of humanity, but instead it has produced devastating
and deadly results which threaten to Jead us toward a new dark
age. The frightful uprisings among races, the almost unbeliev-
able conquests of Communism, and the growing antireligious
philosophy throughout the world, all spell out the fact that doom
is certain. I can see no bright prospects, through the efforts of
man, for the earth and its inhabitants.

This same pessimism regarding Christians’ ability to improve
society through the preaching of the gospel has been affirmed.
by John Walvoord, for three decades the president of Dallas
Seminary: “Well, I personally object to the idea that premillen-
nialism is pessimistic. We are simply realistic in believing that
man cannot change the world. Only God can.” But why can’t
God change it through His servants, just as Moses changed the
world, and as the apostles changed it? The apostles’ enemies
announced regarding them: “These that have turned the world
upside down are come hither also” (Acts 17:6b). No one has
ever announced this about dispensationalists!

A Question of Responsibility
This utter pessimism concerning the earthly future of the
institutional Church and Christian civilization is what lies be-
hind the traditional premillennialists’ lack of any systematic
social theory or recommended social policies. They believe that
it is a waste of their time thinking about such “theoretical”

9, Lehman Strauss, “Our Only Hope,” Biliiatheca Sacra, Vol. 120 (April/June
1963), p. 154.
10. Christianity Teday (Feb. 6, 1987), p. 11-1.
