166 RAPTURE FEVER

God was shoved out of their mental universe. The result was
predictable: the eventual decline of biblical orthodoxy.

Scofield’s Shuffle

You might imagine that by 1900, every orthodox Christian
scholar would have recognized the tight connection between
the evolutionists’ geological time frame and their rejection of
biblical truth. You would be wrong. Virtually no.leading theolo-
gian in any orthodox camp was steadfast in his defense of Bish-
op Ussher’s chronology or anything remotely resembling it.

Into this wasteland came lawyer C. I. Scofield and his Refer-
ence notes in 1909. Beginning with his notes on Genesis 1:1, he
gave away the biblical case for creationism. In note 2, he wrote:
“The first creative act refers to the dateless past, and gives
scope for all the geologic ages.” This is exactly what the be-
sieged defenders of the Bible had been arguing for over a
century, and each generation saw them pushed into the wilder-
ness of ever-greater compromise.

He adopted the so-called “gap” theory of creation. In be-
tween Genesis 1:1 and 1:2, there was a gap of an indeterminate
period — in fact, a gap just long enough to allow Christians to
fill in the latest theories of the geologic time frame. This sup-
posedly removed from Christian scholars the necessity of deal-
ing with biblical chronology prior to Adam. What such a strate-
gy could not do, however, was to remove the stigma of the
biblical account of creation, which places the creation of the
sun, moon, and stars after the creation of the earth (Gen. 1:14-
15). This problem passage was what led Rev. Wilson to sneer
concerning the immensity of the universe, “It was incomparably
more to us than it was to the writer of the first chapter of Gen-
esis, who added incidentally that God ‘made the stars also’.”

Who would have invented the gap theory if uniformitarian
theories of the geologic column had never appeared, or if Dar-
winism had never appeared? No one. It is obviously a half-
hearted, half-baked attempt to escape one problem, fussils,
