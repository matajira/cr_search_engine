A Commitment to Cultural Irrelevance 109

They expect old donors to finance a new theology. They have
adopted a strategy of silence with their donors — the same strategy
they have long used with respect to their published critics,

Conclusion

God has given His people a great degree of responsibility in
the New Testament era. We are required to proclaim His gos-
pel of comprehensive redemption.’® We are to work to fulfill
the Great Commission, which involves far more than preaching
a world-rejecting gospel of personal escape into the clouds.”
Through the Church, Christ’s body, the combined efforts of
Christians through the ages can and will combine to produce
the visible transformation of a sin-governed world: not attaining
perfection, but rolling back the effects of sin in every area of
life. This is the true meaning of progress.

Fundamentalist Christians reject this God-given assignment
in history: the cost of progress seems too high to them. They
have adopted a view of Bible prophecy that rationalizes and
baptizes their flight from responsibility. They invent fairy tales
for children and call them the old-time religion, Stories invent-
ed in 1830 are seriously presented by seminary professors as
the historic legacy of the Church, despite the existence of evi-
dence to the contrary presented by their own students.”!

It is time for grown-up Christians to put away such fairy
tales and accept their God-given responsibilities. Sadly, they
resist. They still hope for deliverance: getting out of life alive at
the terrible price of leaving no legacy to the future.

19. Gary North, Is the World Running Down? Grisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1988), Appendix C: “Comprehen-
sive Redemption: A Theology for Social Action.”

20, Kenneth L. Gentry, Jr, The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990).

21, Alan Patrick Boyd, “A Dispensational Premillennial Analysis of the Eschato!-
ogy of the Post-Apostolic Fathers (Until the Death of Justin Martyr)” Th.M. thesis,
Dallas Theological Seminary, 1977.
