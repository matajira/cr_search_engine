Foreword xxiii

ried about the costs of working hard today: the benefits will not
endure the Great Tribulation. Yes, His people will be safe in
heaven after the Rapture, but their inheritance will be des-
troyed. What a terrible, debilitating effect this belief has on
people’s hopes and dreams! Fortunately, this is an incorrect
belief, as this book will prove.

The Bible tells us that those who are redeemed by God's
grace are assigned a task: to extend His dominion in history
(Gen. 1:28; 9:7). That is both our great honor and our great
responsibility. It is time for Christians to cease looking for
theological loopholes to escape this responsibility.

Conclusion

‘This book presents the case against just this aspect of dispen-
sationalism: the deliberate evasion of responsibility through the
invention of a false doctrine: the “secret” Rapture. This evasion
of responsibility comes at a very high cost: the public denial of
God's earthly blessings on His people. It is time for Christians
to count this terribly high cost of evading their responsibilities
as God’s designated agents in history, the ambassadors of His
kingdom, which progressively extends across the face of the
earth through missionary work and evangelism. It is time for
God’s people to acknowledge the greatness of Christ’s Great
Commission’ and to stop fretting about the so-called Great
Tribulation, which was the great tribulation for Israel in A.D. 70,
not a future event.’ Our work will not be destroyed by the
Antichrist or the Beast (died: A.D. 68) in a future seven-year
tribulation period. Our work will persevere: an inheritance to
future generations. And thus will God's promise be fulfilled: “A
good man leaveth an inheritance to his children: and the
wealth of the sinner is laid up for the just” (Prov. 13:22).

7. Kenneth L. Gentry, Jr, The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990).
8. David Chilton, The Great Tribulation (Ft, Worth, Texas: Dominion Press, 1987).
