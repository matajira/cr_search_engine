92 RAPTURE FEVER

They never say. In over 160 years, no dispensationalist author
has had a book published dealing with the details of New Tes-
tament social ethics.

The result, in the words of dispensationalist author Tommy
Ice, is that “Premillennialists have always been involved in the
present world. And basically, they have picked up on the ethical
positions of their contemporaries.”! The question is: How reli-
able are the ethical positions of their contemporaries?

The Two-Storey World of Humanism

In the early writings of premillennialist Francis Schaeffer, we
read of modern philosophy’s two-storey universe. The bottom
storey is one of reason, science, predictable cause and effect,
ie., Immanuel Kant’s phenomenal realm. This view of the uni-
verse leads inevitably to despair, for to the extent that this
realm is dominant, man is seen to be nothing more than a
freedomless cog in a vast impersonal machine.

In order to escape the pessimistic implications of this lower-
storey worldview, humanists have proposed an escape hatch: a
correlative upper-storey universe. The upper storey is suppos-
edly one of humanistic “freedom”: faith, feeling, emotion, per-
sonality, randomness, religion, non-cognitive reality, i.c., Kant’s
noumenal realm. It also supposedly provides meaning for man,

“but only non-cognitive (“irrational”) meaning. It is meaning
which is meaningless in rational (“lower storey”) terms. There
is no known point of contact or doorway between these two
realms, yet modern man needs such a doorway to hold his
world and his psyche together. This is why the modern world
is in the midst of a monumental crisis, Schaeffer argued.

1. Debate between Dave Hunt and Tommy Ice vs. Gary DeMar and Gary North.
Cited by DeMar, The Debate Over Christian Reconstruction (Ft. Worth, Texas: Dominion
Press, 1988), p. 185.

2. Luse “storey” to identify layers of a building; I use “story” to identify tales.
