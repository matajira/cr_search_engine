Conclusion 211

repeatedly that “postmillennialism is dead,” and so had com-
pletely ignored the rise of the newer postmillennialism of the
Christian Reconstruction movement. They had, in effect, built
themselves a theological Maginot Line, with all its guns trained
on rival “pessimillennialists.” I knew cnough about tactics to
plan a Blitzkrieg around that line.

The Date-Setting Addiction

Little did I suspect that dispensationalism’s “last hurrah”
would begin that very spring, when Edgar C. Whisenant pub-
lished. his “two-books-in-one” paperback book, both of which
appeared under several different titles, on 88 reasons why the
Rapture would surely take place that September. Millions of
copies were printed and distributed. Just about every dispensa-
tional church in America had members who were getting ready
for the Great Escape. “Everything must be put on hold!”

Predictably, nothing happened. “Wait,” said Whisenant in
effect, “I forgot about the B.C to A.D. shift. I lost a year. I
should have said 1989.” No; he should have said nothing. But
he had said enough. The egg on the dispensational movement's
collective face would stick for a long time. Or so I thought.

But wait! There has been yet another reprieve. Pat Robert-
son’s 1990 newsletter identified Israel’s 1967 Six-Day War as
the first time that Jerusalem was fully liberated from Gentile
control, as prophesied in Luke 21:24. “The Six-Day War gave
the Jews control over Jerusalem in June of 1967. That event
started the cosmic clock ticking. The length of a generation in
the Bible is 40 years. Ten is the biblical number of completion.
Forty years from 1967 is 2007.”

But that’s not all. America was founded in 1607 at James-
town. Now, if you take 40 (the number of years in a generation)
and multiply it by ten (the number of completion), you get 400
years. “The end of the generation’ of Gentile decline coincides
with 10 ‘generations’ of America . . . the ‘completion’ in biblical
