House of Seven Garbles 131

A Fig Tree Grows in Dallas

Dominion Theology gives away entire departments of the dis-
pensational store in its attempt to refute Reconstructionism, at
a time when dispensationalism is already sitting on mostly
empty shelves. Readers need to be aware of the historical set-
ting of this book. Here is what they have not been told.

In the mid-1970's, Dallas Seminary sought and received
academic accreditation for the ‘first time. The school added
psychology and counselling courses. It then reduced the Greek
and Hebrew language requirements that had been the standard
at Dallas for half a century.

The school began to lose its best and brightest faculty mem-
bers. $. Lewis Johnson left. Bruce Waltke left, the school’s pre-
eminent Old Testament scholar. Even worse, Waltke subse-
quently became a Reformed amillennial Calvinist and now
teaches at Westminster Seminary in Philadelphia — a devastating
intellectual blow to Dallas, since Dallas had relied on Waltke’s
presence on the faculty as a way to tell the world that its theo-
logical position is defendable exegetically.* Ed Blum left.
Charles Ryrie left (or was fired). Then Dallas fired three of its
men in 1987 for holding charismatic doctrines. One by one, the
exodus has continued. A lot of very cautious faculty members
remain. They have chosen not to rock the boat by exposing
their theological flanks in public debate. Until now.

The old guard of John Walvoord and J. Dwight Pentecost
grew even older and retired. Only Robert Lightner remains to
defend the good old cause, but he does not write scholarly

4, Waleke left Westminster in 1990, just after he contributed a chapter to the ill-
fated Theonomy: A Reformed Critique, edited by William 8. Barker and W. Robert
Godfrey (Grand Rapids, Michigan: Zondervan Academie, 1990). For responses, see
Theonomy: An Informed Response, edited by Gary North (Tyler, Texas: Institute for
Christian Economics, 1991); Greg L. Bahnsen, No Other Standard: Theonomy and Its
Critics (Tyler, Texas: Institute for Christian Economics, 1991); Gary North, Westmins-
ter’s Confession: The Abandonment of Van Til's Legacy (Tyler, Texas: Institute for Chris-
tian Economics, 1991).
