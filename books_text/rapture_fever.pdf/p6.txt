AT GOD’S RIGHT HAND, UNTIL....

A Psalm of David.

The Lorp said unto my Lord, Sit theu at my right hand,
until I make thine enemies thy footstool. The Lorp shall send
the rod of thy strength out of Zion: rule thou in the midst of
thine enemies. Thy people shall be willing [volunteer freely: New
American Standard Bible] in the day of thy power, in the beau-
ties of holiness from the womb of the morning: thou hast the
dew of thy youth. The Lorp hath sworn, and will not repent,
Thou art a priest for ever after the order of Melchizedek. The
Lord at thy right hand shall strike through kings in the day of
his wrath. He shall judge among the heathen, he shall fill the
places with the dead bodies; he shall wound the heads over
many countries. He shall drink of the brook in the way: there-
fore shall he lift up the head (Psalm 110:1-7; emphasis added).

But now is Christ risen from the dead, and become the first-
fruits of them that slept. For since by man came death, by man
came also the resurrection of the dead. For as in Adam all die,
even so in Christ shall all be made alive. But every man in his
own order: Christ the firstfruits; afterward they that are Christ’s
at his coming. Then cometh the end, when he shall have deliv-
ered up the kingdom to God, even the Father; when he shall
have put down ail rule and all authority and power. For he
must reign, till he hath put all enemies under his feet. The last
enemy that shall be destroyed is death. For he hath put alt
things under his feet. But when he saith, all things are put
under him, it is manifest that he is excepted, which did put all
things under him. And when all things shall be subdued unto
him, then shall the Son also himself be subject unto him that
put all things under him, that God may be all in all (I Cor.
15:20-28; emphasis added).
