Dispensationalism vs. Six-Day Creationism 169

better. But they are afraid to insist that creationism be defend-
ed. Laymen still sit silent, and they continue to send in their
checks. This has been going on for over half a century.

The co-author of The Genesis Flood, John Whitcomb, taught
for decades at Grace Theological Seminary, which did maintain
a defense of the six-day creation. In 1990, Whitcomb was fired,
three months short of his retirement. He had complained once
too often regarding what he viewed as the theological drift of
the seminary.

Henry Morris is a fundamentalist and a dispensationalist. He
has waged a lifelong defense of six-day creationism, and he has
yet to convert a single seminary. He is the “odd man out” in
modern dispensationalism, however. Scofield set the standard
of dispensational compromise in 1909, and the vast majority of
his academically certified followers have not departed from the
received tradition. The sell-out continues.

The Triumph of Darwinism

Only one major conservative, seminary-based theologian in
the nineteenth century publicly opposed Darwinism: Charles
Hodge. He wrote What Is Darwinism? in 1874, and concluded
that Darwinism is atheism. By the time he wrote his little book,
Darwinian thought had begun to capture the colleges, academic
departments, and intellectuals of the era. By 1900, Darwinism’s
triumph was institutionally complete.

In between, Christians played no part in the debate. The
debate was between two forms of Darwinism: Social Darwinism
and Statist Darwinism. The Social Darwinists argued that free
market competition is analogous to competition in nature.
Therefore, we should let this competition run its course, pro-
ducing ever-stronger private business firms. The Statist Darwin-
ists switched the debate from planless nature to planning man.
Man, meaning elite scientists, now know the secrets of evolu-
tion. They can use this knowledge to design and direct scientifi-
cally the next stage of the evolutionary process. The politicians
