64 RAPTURE FEVER

healings and the casting out of demons — Christ transferred
power to His followers. Why shouldn’t we expect widespread
social and institutional healing in history?

The Power of Christ in History

Where is the earthly manifestation of this power? Dispensa-
tionalist Dale Hunt is adamant: only in the hearts of believers
and (maybe) inside the walls of a local church or local rescue
mission. As he says, in response to an advertisement for my
Biblical Blueprints Series: “The Bible doesn’t teach us to build
society but instructs us to preach the gospel, for one’s citizen-
ship is in heaven (Col. 3:2).”* (It seems to me that he could
have strengthened his case that we are citizens of only one
“country” by citing a modern translation of Philippians 3:20.)
Christ’s gospel is supposedly a gospel of the heart only. Jesus
supposedly saves hearts only; somehow, His gospel is not power-
ful enough to restore to biblical standards the institutions that
He designed for mankind’s benefit, but which have been cor-
rupted by sin. Hunt’s view of the gospel is this: Jesus can some-
how save sinners without having their salvation affect the world around
them. This, in fact, is the heart, mind, and soul of the pessimil-
lennialists’ “gospel”: “Heal souls, not institutions.”

Hunt separates the preaching of the gospel from society. He
separates heavenly citizenship from earthly citizenship. In short,
he would rewrite the Great Commission: “All power is given
unto me in heaven and none in earth.” (So, for that matter,
would the amillennialist.) Christ’s earthly power can only be
manifested when He returns physically to set up a top-down
bureaucratic kingdom in which Christians will be responsible
for following the direct orders of Christ, issued to meet specific
historical circumstances. The premillennialist has so little faith
in the power of the Bible’s perfect revelation, empowered by

8. Dale Hunt, C/T Bulletin (Feb. 1987), fourth page.
