Introduction 11

for specific verses — a rare event in their lives — they get totally
confused very fast. But they nevertheless cling to the received
faith, just so long as they do not become active in politics or the
battle against abortion or the battle against pornography. Just
so long as they don’t get involved in home schooling. Just so
long as they refuse to commit time and money to the activities
recommended by Phyllis Schafly’s Eagle Forum or Beverly
LaHaye’s Concerned Women of America. In other words, just
so long as they remain content to lose every major battle in history, they
will continue to cling to the received dispensational faith. It
comforts them. It reassures them that their personal commit-
ment to do. nothing to improve society is God’s way of getting
nothing done, since all that God plans for His people to accom-
plish in this dispensation is nothing.

So, I am not talking here about the loyal troops sitting in the
pews, sitting at home, and above all, sitting on their check-
books. I am talking about theological leaders. I am talking
about a series of ideas and those academic institutions that are
expected to deal with these ideas. I am operating on the as-
sumption that ideas have consequences, that men become in-
creasingly consistent with what they believe, and societies be-
come increasingly consistent with what a majority of their mem-
bers have become. I also believe that people do change their
minds ~ sometimes lots of people. This is what evangelism is all
about: offering people the opportunity to change their minds,
and then, when they do change their minds, persuading them
to live consistently with their new beliefs.

Here are a few questions that North American Christians
have been asking themselves over the last two or three decades.
Because of the one intellectual battle that a few fundamentalist
spokesmen have entered into — the public defense of the six-
day creation account found in Genesis 1 ~ some fundamentalists
have been forced to begin considering some of these questions.
These questions demand specific answers, but the search for
these specific answers is steadily undermining the received
