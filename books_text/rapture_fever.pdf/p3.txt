This book is dedicated to
Kenneth L, Gentry, Jr.

whose books would have
paralyzed the dispensational
theologians of this genera-
tion, had it not been for one
thing: their constant revi-
sions to the system had al-
ready paralyzed each other.
