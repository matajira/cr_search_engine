GHRISTIAN RESISTANCE TO TYRANNY 125

because law is seen as a product of will, power, and com-
mand, not of objective right and justice. Ad law then becomes
pragmatic, ever-changing law, for in the course of human events
both conditions and rulers change, and the desires of rulers
change, and in the absence of universal -or enduring moral
norms, there can be no reason why those in power should not
change the laws to effect their wills, just as American
“liberals” on the Supreme Court have done.

In the final analysis, it matters not what particular
naturalistic theory of law one chooses. One may speak of law
as merely historical fact (the Renaissance Humanists, con-
temporary Legal positivists), or as statutory law enacted by
the “sovereign’s” will (Bodin, Hobbes, contemporary Legal
Pragmatists and Positivists), or as command (Hobbes, the
Utilitarians), or as the basic law of the constitution (Locke,
Montesquieu, many Humanistic Conservatives), or as the ex-
pression of “pure reason” (Spinoza, Thomasius), or as the
“general will” (Rousseau, Kant), or as the working of the
Hegelian “Spirit,” freedom in history (Hegel, Historicists,
Liberals), or as the ideology of the ruling class (Marx, Com-
munists, Marxists, and in a sense National Socialists and
Fascists), or as the state's purposes, methods, or processes in
legislating (Liberals, Legal Positivists), or as the open ethical
skepticism and moral relativism of formalism of the executive,
judicial’ or legislative process (Legal Positivism and
Pragmatism, popularly known as “loose construction” of the
Constitution) which have been so destructively influential on
the legal and political thought of American lawyers, political
scientists, and intellectuals since the 1890’s.7° Bué the specific
Sormal differences among these various theoretical sub-species ultimately
matter little. The basic thrust of all — despite the rhetoric of each against
the others—is the same: law is merely will-to-power, command, force,
and the threat of force mixed with decett, not objective justice or morality.

These have been the vehicles for the abandonment of the basically
Biblical principles of law and society in America and the West. These
have been the vehicles for the old false religion of secular salvation by col-
lectivist planning and controls, and are the roots of the acrid and sickening

70. Luse here the categories of legal thought surveyed by Friedrich. The
Biblically thoughtful Christian will be able to supply the extended critiques
which we cannot undertake here. The common denominator of all these
theories in arbitrary will is obvious.
