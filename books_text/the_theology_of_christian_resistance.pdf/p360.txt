320 CHRISTIANITY AND CIVILIZATION

The Germans had immediately violated the war regula-
tions, by attacking Holland without any declaration of war in
advance. Later they demonstrated a total lack of respect for
the treaty’s many stipulations and prohibitions. On the con-
trary, they made an increasing attempt lo force a pagan
political doctrine upon the people’s conscience. All this
worked to convince most Christians that resisting the enemy
was perfectly allowed, insofar as this resistance served the
purpose of breaking the usurper’s power and of preventing
him from being successful in his essentially pagan assassina-
tion of the Dutch nation.

Immediately a second question came up: Was it allowed
to make use of lies and deception in the struggle with the Ger-
mans? Suppose a search was made, resulting in questioning
whether or not suspects had been hiding downed British
pilots, Jews, etc. Confirming such questions delivered the
hunted persons into the enemy’s hands. Refusing an answer
virtually equalled acknowledging that the questioner had
guessed right, and therefore did not offer an acceptable solu-
tion. Something similar occurred in many other situations.
The Bible was scrutinized in search of answers. Some pointed
out that Rahab hid the spies of Israel and deceived Jericho's
king by misleading his soldiers (Joshua, Chapter 2), Other
examples were quoted, ¢.g., the farmer’s wife at Bahurim, do-
ing a similar thing (2 Samuel 17), and many other cases. The
final conclusion could only be that the enemy lost his right to a true
answer; we are at war, and are allowed to make use of
strategems.

Cruel Orders

The Germans closed the net around our people and made
it tighter every day. Immediately following the February
strike, which had been quenched by bloody force, the
persecution of the Jews as enforced by rough orders. All Jews
were forced to wear the yellow Davidic star, marking them in
public, The summer was the scene of many mass arrests, and
the first groups of Jews were transferred to the Westerbork
transition camp, waiting to be transported to the destruction
camps and gas chambers in Poland.

OFf the 140,000 Jewish population in Holland, 110,000
were murdered. Only 6000 survivors returned from Auschwitz
