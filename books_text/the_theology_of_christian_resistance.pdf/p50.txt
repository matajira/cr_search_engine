10 CHRISTIANITY AND CIVILIZATION

law, must be the Bible. As such, Rutherford argued, ail men,
even the king, are under the law and not above it. This con-
cept, of course, was considered political heresy and
punishable as treason.

Rutherford argued that Romans 13 indicates that all
power is from God and that government is ordained and in-
stituted by God. The state, however, is to be administered ac-
cording to the principles of the Bible. Acts of the state that do
not have a clear reference point in the Bible were considered
to be illegitimate and acts of tyranny. Tyranny was defined as
ruling without the sanction of God.

Rutherford held that a tyrannical government is always
immoral. He considered it a work of Satan and that “a power
ethical, politic, or moral, to oppress, is not from God, and is
not a power, but a licentious deviation of a power; and is no
more from God, but from sinful nature and the old serpent,
than a license to sin.” The implications of Rutherford’s thesis
are important for tt makes the vast majority of ciutl governments in
the world today illegitimate.

The discussion of Christian resistance is a very serious
matter—especially with the number of mentally disturbed
people living in the world today. Any concept or proposed ac-
tion can, and most likely will, be taken to its illogical conse-
quences by someone. Martin Luther and the other Reformers
faced this problem. In a fallen world, this is to be expected.
Such a fact, however, should not deter us from looking at the
appropriate levels of resistance as outlined by Samuel Ruther-
ford in Lex, Rex. To the contrary, the fact that we may very
well be facing a totalitarian state compels consideration of
these principles.

Rutherford was not an anarchist, In Lex, Rex he does not
propose armed revolution as a solution. Instead, he sets forth
the appropriate response to interference by the state in the
liberties of the citizenry, Specifically, if the state is deliberately
coramitted to destroying its ethical commitment to God, then
resistance is appropriate. Rutherford suggested that there are
levels of resistance in which a private person may engage. First, he
must defend himself by protest (in contemporary society this
would usually be by legal action); second, he must flee if at all
possible; and, third, he may use force, if absolutely necessary,
to defend himself. One should not employ force if he may save
himself by flight; so one should not employ flight if he can
