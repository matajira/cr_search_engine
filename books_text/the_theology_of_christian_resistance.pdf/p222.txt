182 CHRISTIANITY AND CIVILIZATION

blood of Huguenot men, wornen, children, and infants. Paris
was filled with a fanatical and irresponsible mob of people.
The white cross of St. Bartholomew, being displayed
everywhere, gave to the crowd an identity, and it lent to the
mob the “holy cause” of wiping out the accursed heretics. As
the day progressed, the taste of blood in the Parisian mob’s
collective mouth moved them to an even greater slaughter.
Three to five thousand Huguenots were murdered in one day
in Paris alone. During the coming months the slaughter
spread to the surrounding countryside. In fact, this venom so
threatened anyone of the Reformed faith, that Geneva, which
is southeast of France, feared that the madness might spread
even into Switzerland, Beza wrote to Bullinger at Zurich;
“Our friend Lochmann wilt advise you concering the cruelest,
the most atrocious events. I am sure that in that day more
than 300,000 of our own people bare their throats in France;
one no longer takes account of rank, sex, or age. Here we are
assuredly exposed to the same danger and it is perhaps the last
time I write to you. One could not doubt that it is a question
of universal conspiracy which is going to erupt. My father, be
advised of our common peril and more than ever pray with
us. I am quite particularly aimed at and am thinking about
death more than life. Farewell to you as well as your dear
brother.”*

For a period of three to six months after the initial
massacre, France was controlled by the mob, and violent
death at the hands of one’s neighbor was.a common occur-
rence. As one might guess, what began as a massacre of the
Huguenots soon developed into such a slaughter that anyone
could be a victim. As the nobleman Mexeray writes, “If one
had money, or a well-paid office, or dangerous enemies, or
hungry heirs, then one was a Huguenot.” The overall effect
on France was devastating. Economically, the nation lost the
most productive citizens that it had. Commerce was disrupted
by the widespread destruction from armies, sieges, pillages,
and sackings. The King was not able to control the seas or the
highways. The greatest loss for France, however, was in the
raping of morality and true religion. The Huguenot was a

4. Quoted in Gray, p. 138. Beza was the chief advisor to the Huguenots
from Geneva,*
5. Quoted in Gray, p. 140.
