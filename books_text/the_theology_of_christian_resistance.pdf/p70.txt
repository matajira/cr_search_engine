30 CHRISTIANITY AND GIVILIZATION

summons dismissed. The Internal Revenue Service appealed,
and the case went to the Seventh Circuit Court of Appeals in
Chicago. That Court decided in favor of the government, in
one of the most amazing rulings your reporter has ever read.

So you claim you have withstood the temptations of the
flesh! Pilgrim, that may be, but your obedient servant will not
be convinced until you prove you can resist the temptation to
concoct any number of clever takeoffs on the name of Edward
Dumbauld, the judge who wrote the ruling. When you read
it, you will certainly conclude that no jurist could be as dumb
as his honor appears—a statement that, while true, is also
prima facie proof that your obedient servant has faced the
temptation and failed.

So has Pastor Dykema. Why shouldn’t he? He’s a sinner,
too. He tells your reporter that, during the appeal hearing,
Dumbauld wasted substantial time “asking dumb questions.”
For instance, he asked the jurist, “How do we know it isn’t a
macaroni factory?” This is the kind of question judges often
dismiss as “frivolous,” and it certainly zs more frivolous than it
would be to ask how we know the Court of Appeals isn’t a
loony bin run by the inmates.

Court On Trial

In a footnote on page two, the Dumbauld ruling says this:
“The church is said to be Calvinist in doctrine, but uncon-
nected with any presbytery, synod, or general assembly. It is
said to be composed of approximately only fifteen members,
though a throng of spectators occupied the back rows of the
courtroom during argument of the case.”

The first sentence alone should be enough to throw the
ruling out. Whether it is just plain dumb or wilfully
malevolent, your reporter doesn’t know, Whatever Durm-
bauld’s motive, it is wrong. A church that is Calvinist in doc-
trine doesn’t have to belong to anything. The.Church in ques-
tion is Calvinist and independent. Brother Dykema points out
that the Reformed Baptists, for instance, are also Calvinists,
but belong to no synod. Dumbauld seems to be saying that
church independence is “evidence” that an institution may be a
“macaroni factory,” not a church. Where is the /aw that for-
bids church independence? Dumbauld proves here that his
ruling is based not on law, but on religious discrimination, on
