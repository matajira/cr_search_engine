HUMANISM VERSUS CHRISTIANITY 15

Many of my books, films, etc. deal with a Christian con-
sideration of the Humanities—being thankful for the creativ-
ity which is a natural part of Man because people are made in
the image. of God.

Then we must ask what humanism is, which we must con-
trast with great clarity to the Christian world view. We must
realize that the contrast goes back to two different views of
final reality. What is final reality? In the Judeo-Christian
world view, the final reality is the infinite-personal God wha
truly is there objectively whether we think He is there or not.
He is not there just because we think He is there. He is there
objectively. And He is the Creator. He is the Creator of every-
thing else. And we must never forget that one of the
distinguishing marks of the Judeo-Christian Gad is that not
everything is the same to Him. He has a character, and some
things agree to His character and some things conflict with
that character. To this God (in contrast to Buddhism and
Hinduism for example) not everything is the same, and,
therefore, there are absolutes, right and wrong, in the world.

As we come on the humanist side—to the final reality
which is being taught in our schools and which is much of the
framework of the thinking and writing of our day—the final
reality is thought of as material or energy which has existed in
some form forever and which has its present configuration by
pure chance.

The real issue is the question of final reality. The difference lies
in what the final reality is: Either the infinite-personal God to
whom not everything is the same, or merely material or
energy which is impersonal, totally neutral to any value sys-
tem or any interest in man as man. In this view, the final real-
ity gives no value system, no basis for law, and no basis for
man as unique and important.

Beginning about eighty years ago, we began to move from
a Judeo-Christian consensys in this country to a humanist
consensus, and it has come to a special climax in the last forty
years. For anyone here who is fifty years of age or more, the
whole dramatic shift in this country has come in your adult
lifetime. And today we must say with tears that largely the
consensus in our country, and in the western world, is no
longer Judeo-Christian, but the general consensus is human-
ist, with its idea that the final reality is only material or energy
which has existed forever, shaped into its present form by
