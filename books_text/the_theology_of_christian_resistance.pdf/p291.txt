THE DUTCH-AMERICAN GUERRILLAS 231

tally wounded a Captain.”'® That the same people who had
sold the Hessians food should do this is not difficult to under-
stand. They did not consider binding an oath that had been inflicted
upon them by force, and when they had the chance to retaliate on
the conqueror they did so.

It was at that point that Bergen County truly began to
take on the appearance of a middle ground between the two
sides, as the British moved back ta New York. But it was now
the Loyalists who were the more exposed, as attacks and
plundering raged on both sides. Given all of the foraging that
took place, it was “amazing” that the people in the area not
only found enough food and fuel to carry them through the
winter, but that a thriving business in hard currency sprang
up with the British in New York.

A Guerrilla Civil War

Faced with the reality of perpetual warfare in their area,
the majority in Bergen County in 1777 began to establish a
militia that would function on a permanent basis, The nature
of the American militia in the area began to change during
that year. From a passive force trying to organize defensive measures
against an aggressor, it became a highly mobile force that could strike
back at the invader. The dynamics of how this came about are
important.

During the same period, the “farmer-soldier” of the militia
came under the usual criticism of regular army officers such as
Colonel Aaron Burr and General Alexander McDougall,
though, as Leiby notes, “neither of them had any real reason
to regard himself as a professional military man. Burr com-
plained that ‘not a man of the militia are with me. Some
joined last night but are gone.’ *19 Even as they began to learn
the ways of the guerrilla warfare, the militia, as one would ex-
pect, choose to follow their elected leaders, whom they knew
and in whom they had confidence, rather than simply any
officer sent by the Continental army.

And, as Leiby further observes, “McDougall, for his part,
was entirely unembarrassed by the thought that the militia
could hardly be expected to do what his nine hundred troops
could not do, seeing no irony whatever in complaining that

18. pp. 99-102.
19. p. 138.
