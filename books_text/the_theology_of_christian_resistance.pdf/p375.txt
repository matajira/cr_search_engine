 

REVIEWS 335

non-violence, for this is nothing but a verbal game. (One point: the
statement we have been interacting with may reflect Yoder’s own
view, or that of the position he is setting out—it is not clear, Yoder
does not offer any criticism of it, however.)

Yoder’s own position, apparently, is set out in Chapter 18: The
Pacifism of the Messianic Community. Frankly, we found this posi-
tion ta be very vague, and full of hidden assumptions. Yoder seems
to assume that Jesus taught political pacifism. (Of course, this posi-
tion is defended elsewhere in Mennonite literature.) Moreover,
Yoder seems to pit the New Testament against the Old: “the telling
has come to us not on a tablet of stone chiseled by the finger of God
alone on Sinai, or from the mouth of a prophet, but in the full hu-
manity of a unique and yet complete human being” (p. 124). Do we
really have to oppose these modes of revelation one to another?

Yoder’s views come through at other places also. Yoder is con-
cerned that any sound position be “humane” (cf. e.g., p. 32). On the
face of it, what seems best for human beings is of more concern than
the vindication of the honor and glory of Ged, which is what
underlies the orthodox Christian doctrines of capital punishment
and just war, Yoder fails to interact with this; he seems simply to
assume that the maintenance of human life is of prime importance.
On p. 61; “Now in the case of war it is the enemy (since it is with
him I am asked to deal) who is the neighbor I am to love. My action
must be such as to communicate or proclaim to him the nature of
God’s love for him. It might be possible to argue that this could be
done with a certain kind of force, moral or social or even physical,
but certainly it cannot be said by threatening or taking his life.”
True, but the Bible does not teach that in all situations this kind of
love must be shown to all men. Yoder justly criticizes “situational
ethics” on the ground that it does not do away with the need for
tnoral standards (p. 121). The same is true of love. The same God
who told Israel to love their neighbors and never take vengeance
(Lev. 19:18), in virtually the same breath ordered a whole series of
capital punishments (Lev. 20). The same Jesus who loved Israel for
three years, after giving them forty years to repent, poured out
destruction on them in 70 A.D. (Matt. 23, 24). God is
longsuffering, but His love does “run out” for those who persist in
rebellion, and He casts them into hell. The Bible tells us at what
point men have gone too far, and must be put to death.

Finally, Yoder’s remarks on just war theory might be noted. The
reader will not find an extended pacifistic interaction with just war
theory here, and Yoder’s brief criticisms are not adequate to cause
