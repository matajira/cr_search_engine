ON RECONSTRUCTION 297

powers; but that as in all other cases of compact among parties
having no common Judge, each party has an equal right to judge
for itself, as well of infractions as of the mode and measure of
redress.

(b) And on December 21, 1798, the Virginia Assembly
passed a similar resolution (and later clarified and re-
emphasized it in a subsequent resolution passed on January
7, 1800). In the December 21 Resolution the Virginia
Assembly declared:

. and that in the case of a deliberate, palpable, and dangerous
exercise of other powers, not granted by the said compact, the
States who are parties thereto, have the right, and are in duty
bound, to interpose for arresting the progress of evil, and for main-
taining within their respective limits, the authorities, rights and
liberties appertaining to them (italics added).

In short, what the States of Kentucky and Virginia did
was to interpose themselves as political entities between the
general government and their respective citizens to protect
them against what the legislatures regarded as an unconstitu-
tional arrogation of power. This was the first example of
governmental interposition since the new constitution of 1787
was ratified by the states. Kentucky and Virginia both
declared the Alien and Sedition Acts as unconstitutional and,
therefore, null and void in their states. The legal interposition
of one governmental level between a higher level of govern-
ment and the people can thus lead to nullification of an act of
tyranny, which is defined as the application of unlawful force
or the unlawful application of lawful force. In a self-governing
republic like these United States of America, tyranny is
equivalent to the arrogation of unconstitutional powers by the
ruling authorities at any level of gavernment.

In Vindisiae Contra Tprannos: a Defense of Liberty Against
Tyrants, which first appeared under the pseudonym of
Stephanous Junius Brutus in 1579, a tyrant is defined thus:

We have shewed that he is a king who lawfully governs a
kingdom, either derived to him by succession, or committed to
him by election. It follows, therefore, that he is a reputed tyrant,
which either gains a kingdom by violence of indirect means, or,
being invested therewith by lawful election or succession, gov-
erns it not according to law and equity, or neglects those con-
tracts and agreements, to the observation of which he was strictly
obliged at his reception. All of which may well occur in one and
