CONFIRMATION, CONFRONTATION, AND CAVES 59

The extreme few who managed to salvage some clothes and
some money and who are in a position to purchase some flour are
considered fortunate and rich peaple. Fortunate are those, too,
who could obtain a few watermelons or a sick and skinny goat
from the nomads in exchange for the same weight in gold.
Everywhere one only sees pale faces and emaciated bodies,
wandering skeletons conquered by disease and surcly soon vic-
tims of starvation.

When the measures to transport the entire population into the
desert were adopted, no appropriations were made for any kind
of nourishment, On the contrary, it is obvious that the govern-
ment pursued a plan to let the people dic of starvation. Even an
organized mass-killing such as during the tinies when liberty,
equality and fraternity had not yet been proclaimed in Constan-
tinople, would have been a much more humane measure, since it
would have saved these miserable people from the horrors of
hunger and the slow death and the excruciating pains of tortures
so fiendish that the most cruel of the Mongols could not have im-
agined them. But a massacre is less constitutional than death by
starvation. Civilization is saved!

What remains of the Armenian nation, scattered along the
banks of the Euphrates, consists of old men, women and chil-
dren. Men of middle age and younger people, as far as they have
not been slain, are scattered over the roads of the country where
they smash stones or do other labors for the Army in the name of
the state.

The young girls, many still childen, have become the booty of
the Mohammedans. During the long marches to the destination
of the deportation they were abducted, raped if the opportunity
arose, or sold if they hadn’t been killed by the gendarmes who ac-
companied these gloomy caravans. Many have been carried by
their robbers into the slavery of a harem.

The entrances to these concentration camps could well bear
the legend imprinted on the gates of Dante’s hell ‘Ye who enter
here, abandon all hope.’

Gendarmes on horseback made the rounds to punish all those
that tried ta escape, to seize and punish them with their whips.
The roads are well guarded. And what roads they are! They lead
into the desert where death awaits the refugee as surely as under
the whips of their Ottoman guards. ~

In various locations in the desert I came upon six such
refugees lying in the throes of death. They had managed to
escape their guards. And now they were surrounded by half-
starved dogs; the crazed animals waiting for their last convul-
sions so that they could leap upon them and feed on them.

Everywhere along the way one can find remainders of such
unhappy Armenians, who had simply dropped to the ground. There
are hundreds of these mounds of earth under which they rest, the
victims of unqualified barbarism, sleeping without a name.
