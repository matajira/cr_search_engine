PACIFISM AND THE OLD TESTAMENT 91

tribes”). In the Gospel, “holy war” is the evangel, destroying
sinful men by applying to them the death and resurrection of
Christ in preaching and in sacraments. On the other hand,
“just war” continues to be relevant. We shall return to that at
the end of this essay.

Returning to Lind, there are good paints made in this
study concerning the relation between the deliverer and the
king: that since the Lord is the Deliverer, He is the king.
Again, Lind tends to absolutize this, so that he is unsure
whether the Biblical writers approve of David's subordinate
kingship or not, All the same, the contrasts Lind draws be-
tween the oriental pagan conception of kingship and the
Biblical one are frequently useful.

Lind points out that when Israel adopied the ways of the
pagan kings, the Lord went to war against Israel. Indeed, one
of the most insightful suggestions Lind makes is that there was
a problem between David's personal army, a professional
corps which grew up during David’s years under Saul, and
the Israelite militia. Lind suggests that David's sin of number-
ing the people consisted in an attempt to reduce the voluntary
militia of Israel into a part of his permanent army corps (p.
118f.), The young king David who played the part of God’s
fool and slave by dancing before the Ark, and who earned the
contempt of Sauls daughter (possessed as she was by the
oriental pagan view of kingship), has now become an oriental
tyrant to a great extent himself.

Unfortunately we cannot recommend this book, all the
same. Lind’s method of approaching the Bible is that of radical
redaction criticism. Unless the reader is pretty familiar with the
spurious fantasies of Martin Noth and Gerhard von Rad, he
had best leave this book alone. Radical criticism such as Lind
employs enables the author to pick and choose which parts of
the Bible to hearken to, according to an evolutionary scheme.
The Bible ceases to be the Word of Gad, and becomes a set of
interwoven theologies of men. As a result, the text is frequently
not examined seriously. There are not enough good insights
scattered through this book to make its purchase worthwhile.

* * * *

In conclusion, we should like to make a number of points.
First, it does not seem as if anyone has taken the Old Testa-
