xvi CHRISTIANITY AND CIVILIZATION
i one-column article in a newspaper (once), or
fet 2 20-second slot on the local T.V. station (once). These
stunts are wasteful, unless they are part of an integrated
strategy of publicity and recruitment. The goal ts not to become
cannon fodder for the humanists’ guns, As Gen. George Pation put
it in his famous speech to his troops: ‘Your job isn't to die for
your country. Your job.is to get the other poor dumb bastard
to die for his country.” Patton believed in victory, not in
becoming a martyr for the sake of making a “heroic gesture.”
What Christians should understand is that the recent in-
terest in Christian resistance has come, on the whole, from
Christian leaders who have only in the past few years become
openly committed to social and political change. Mast of them
are premillennialists, and they do not believe that Christians
can win, in time and on earth, prior to the visible, personal,
physical return of Christ in glory. Their idea of a victory is “to
go down fighting and take five of the other side with them!”
But there are too many people in the ranks of the enemy, even
assuming each of us could “take five of them with us.” Our
goal should be to survive aver the long term, developing skills
that will enable us to conquer the enemy, society by society,
institution by institution. We need a boot camp experience before we
can achieve @ dominion experience. That is the lesson of all the
wilderness experiences of the people of God, from Sinai to
Gethsemane.

Delaying a Frontal Atiack

We must be careful to adopt the long-term ‘strategy of the
early church. They did not rise up against the Roman legions.
‘They did not become guerillas. The Jews did, and they were
scattered, becoming an identifiable minority to be persecuted
throughout the Roman Empire. The Christians adopted a
different strategy, although suffering intermittent persecutions
—a strategy of avoiding a frontal assault on Rome. By 313
a.D., the Christians triumphed; a non-pagan Emperor came
to power. They had become too valuable to ignore or
persecute any longer.

The Christians of the first century faced a formidable
political enemy. The Roman Empire seemed indestructible.
‘Yet it had feet of clay, It was being subjected to multiple inter-
nal pressures, especially intellectual and religious pressures.
