CHRISTIAN RESISTANCE TO TYRANNY 97

precisely the result of the fundamental irrationalism of
Humanism, ancient and modern, an irrationalism which
finds its fruit in arbitrary government and_ political
pragmatism and absolutism, together with the economic,
political, and social chaos which God visits upon those who
apostatize from His law (Deuteronomy 8; 28).

In domestic policy, American lawmakers have abandoned
the basically Christian restraints of the Constitution for a cen-
tralized planning bureaucratic monstrosity, which seeks to
play God with the economy, society, the education of our chil-
dren, and even the functioning of the institutional church and
the family. The results have been predictably disastrous: huge
and growing federal and state bureaucracies; teeming, un-
predictable, and often incomprehensible bureaucratically-
concocted-and-enforced regulations with the force of law;
skyrocketing governmental costs, budgets, and taxation; de-
capitalizing governmental borrowing on the investrnent
capital market; government-created monetary dehasement or
inflation; government-imposed inefficiency and increased
costs; counter-productive regulations and declining economic
competitiveness; increasing crime and social unrest; the
murder of more than ten million by abortion; and assaults on
Christian liberty in school, church, and family, as well asin
social and economic life. In the name of “realism,” policy-
makers have abandoned the tried and true—not to mention
the Biblical!—for the illusory and utopian. In the name of
“pragmatism,” they have abandoned the workable for the
disastrous.

American foreign. policy has been no better. Christian rep-
resentative government and honest, consultative, constitu-
tional foreign policy formulation has been largely abandoned
for secretive executive agreements, subservience to organs of
international Humanism, such as the United Nations and the
International Monetary Fund, and subversive machinations
by leftists in the State Department and other agencies,!0 To

10, From a growing number of fashionable academically neglected works
on the reasons for American foreign policy failures, see John T. Flynn, The
Roosevelt Myth (N. Y.: Devin-Adair, [1948] 1965), on F, D. R.’s abuses of ex-
ecutive power and adherence to the Husnanistic myth of peace via interna-
tional organization as prelude to disaster in Europe and Asia; on the Far
East, the best work is Anthony Kubek’s How the Far East Was Lost (Chicago:
Henry Regnery Co., 1963), which details the roles of the Executive, the
