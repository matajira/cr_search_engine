278 CHRISTIANITY AND CIVILIZATION

hibited by the Congress prior to the year 1808.” That this
commonwealth does admit the migration of alien friends
described as the subject of the said act concerning aliens; that
a provision against prohibiting their migration is a provision
against all acts equivalent thereto, or it would be nugatory;
that to remove them, when migrated, is equivalent to a pro-
hibition of their migration, and is, therefore, contrary to the
said provision of the Constitution, and zaid.

6. Resolved, That the imprisonment of a person under the
protection of the laws of this commonwealth, on his failure to
obey the simple order of the President to depart out of the
United States, as is undertaken by the said act, entitled, “An
Act concerning Aliens,” is contrary to the Constitution, one
amendment in which has provided, that “no person shall be
deprived of liberty without due process of law;” and that
another having provided, “that, in all criminal prosecutions,
the accused shall enjoy the right of a public trial by an impar-
tial jury, to be informed as to the nature and cause of the ac-
cusation, to be confronted with the witnesses against him, to
have compulsory process for obtaining witnesses in his favor,
and to have assistance of counsel for his defence,” the same act
undertaking to authorize the President to remove a person out
of the United States who is under the protection of the law, on
his own suspicion, without jury, without having witnesses in
his favor, without defence, without counsel— contrary to these
provisions also of the Constitution —is therefore not law, but
utterly void, and of no force.

That transferring the power of judging any person who is
under the protection of the laws, from the courts to the Presi-
dent of the United States, as is undertaken by the same act
concerning aliens, is against the article of the Constitution
which provides, that “the judicial power of the United States
shall be vested in the courts, the judges of which shall hold
their office during good behavior,” and that the said act is void
for that reason also; and it is further to be noted that this
transfer of judiciary power is to that magistrate of the general
goverment who already possesses all the executive, and
qualified negative in all the legislative powers.

7. Resolved, That the construction applied by the general
government (as is evident by sundry of their proceedings) to
those parts of the Constitution of the United States which
delegate to Congress power to lay and collect taxes, duties,
