222 CHRISTIANITY AND CIVILIZATION

bears him witness, that “he was faithful in all things;” yet we
find him often engaged in bloody battles. One instance may
suffice at present, out of many that might be produced, and
that is, when, on the most reasonable terms, he requests to
pass through the land of Sihon, king of the Amorites, pledging
his honour that no damage should be done; Num. xxi:22. but
Sihon, instead of granting the small privilege of passing along
the highway, advances with all his armies against Israel. Doth
Moses think it his duty to make no defense? Let us view his
conduct. Israel advances with sword in hand, and utterly
discomfits the Amorites. If we read the book of Joshuah, his
immediate successor, we shall find him of the same mind,
Israel are called the peculiar people of Gop, to whom his mind
was revealed, and this is the faith of the whole house of Israel.
If it was a sin to engage in a defensive war, can we suppose.
that Israel should be ignorant of it? Seeing therefore that it is
sinless, it is meet that we should tread in the footsteps of this
flock, which is gone before us. And were we further to attend
to the sacred history, we shall find, that after the death of
Joshuah and the elders, which saw the mighty works of Gop,
that Israel first being enslaved by sin, were oppressed by
various nations; but when they cried into the Lord, he raised
up Geliverers, among whom were Othniel, Ehud, Barak, Gi-
deon, Jephtha, and many others, which performed glorious
exploits, and were blessed instruments under Gop to deliver
Israel from oppression and bondage. Some of these, you
know, are spoken of as the worthies of Israel, and have much
recorded in their praise; and yet all these died in the faith,
that a defensive war is sinless before Gon. Still were we to
descend and pursue the chain of history, we shall find all the
kings of Israel of the same faith. Among many that might be
mentioned, we shall at present take notice only of one. David,
a man eminent for pure religion, the sweet psalmist of Israel,
David, a man after Gon’s own heart, yet all his life is a scene
of war. When he was even a youth, he began his martial ex-
ploits, and delivered Israel by slaying great Goliath of Gath,
the champion of the Philistines. And what shail I say more?
The time would fail me to enumerate all who were avouchers
of this proposition: It is therefore a clear point, that a defen-
sive war is sinless, and consistent with the purest religion.
This indeed is generally acknowledged, when our dispute
is with a foreign enemy, but at present it seems like a house
