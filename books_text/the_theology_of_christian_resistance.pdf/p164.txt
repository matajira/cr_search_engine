124 CHRISTIANITY AND CIVILIZATION

effect their wills by any means. Hence they will either suspend
what men in their cultures believe to be moral norms, or deny
outright (publicly or, as in the ancient Sophists, Machiavelli,
and many modern “liberals,” to a select group of devotees) the
existence of such moral restraints on individual or group ac-
tion, in order that the actions of men may either “save” men
and nations from danger, or enable therm to gratify their
desires.

Ta such a nature, however, each man is morally free to do
whatever he desires, whatever is right in his own eyes. And
many will do whatever they desire, or can get away with, for
such a nature is the “amoral”®? nature of Machiavelli and de
Sade, a nature beyond good and evil, in which“. . . there is
just as much harm in killing an animal as a man, or just as lit-
te, and the difference arises solely from the prejudices of our
vanity.”68 Robbins’s comment on de Sade’s conclusions from
the Natural Law premise that nature is normative, that there
has been no ethical fall and curse, and that Ged is ethically
therefore superfluous, is telling:

Since it is nature that prompts us to murder, steal, slander, for-
nicate, and since we have a “natural inclination to such actions
and ends as are fitting”—to quote Thomas Aquinas—none of
these things can be wrong, for nature is normative. The logic is
commendable.

Such a nature teaches the virtue. of lawlessness, by antinomian
theory and practice. Such a nature teaches the equal validity
of anarchy and totalitarianism. Law in such a nature ts based on
force, deceit, and command, and man can have no objective standards of
ethics, justice, or law to guide him as ruler or citizen, and thus no
just standards of either obedience or disobedience to govern-
ment. Obey, or suffer! Obey, or die! These are the ultimate
standards of law and obedience in such a world, and they are
only as good as the force and deceit of the rulers. All law then
becomes positive law: there can be no constitutional or higher moral
restraints on the will of those in power, on the will-to-power or
rulers, whether they be kings, oligarchies, or majorities,

67. Nature, of course, does point to God, and thus cannot be. truly
amoral, however fallen men are inclined to interpret it as amoral, or as morally
different from God’s authoritative interpretations.

68. Marquis de Sade, quoted in Robbins, p. 17.

69. Robbins, p. 17.
