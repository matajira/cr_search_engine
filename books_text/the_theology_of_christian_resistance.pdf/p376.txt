336 CHRISTIANITY AND CIVILIZATION

the orthodox reader to reconsider his position. Yoder does make one
exceptionally good point, however, and that is that just war theory
generally tends to function as a defense of all war, while in theory it
is a statement that all wars are not justified. In other words,
theologians in orthodox churches tend to pull out the just war argu-
ment to defend nationalistic wars, when in fact the whole point of
the just war theory is to provide criteria with which to critique un-
just wars (wars of aggression rather than defense, for instance).

We highly recommend this volume to anyone studying Christi-
anity and political theory.

Werner ©. Packull, Mysticism and the Early South German-Austrian
Anabaptist Movement 1524-1531 (Studies in Anabaptist and Men-
nonite History, XIX). Scottdale, PA: Herald Press, 1977. 252 pp.
$17.95. Reviewed by James B. Jordan.

Despite its forbiddingly specialized title, this is a quite readable
book, and an indispensible introduction to one of the significant
wings of the Anabaptist movement. In his introduction, John S.
Oyer states that it is the “special merit of Werner Packull’s work . . .
that he delineates more clearly than any previous scholar the
theological and spiritual differences between South German and
Swiss Anabaptists by a thorough examination of the dominant
medieval mystical literature... . Indeed, Packull decides that
South German Anabaptism derives more from medieval mysticism
than from the reformation” (p. 13). The present reviewer would
argue that the Swiss Anabaptists also partook to a large extent of this
mystical tradition, and that mysticism is endemic and pandemic to
all Baptist movements; but this point lies outside the parameters of
Packull’s study. (On this question, see the essays in Christianity and
Ctotlization No. 1: The Failure of the American Baptist Culture [Geneva
Divinity School Press, 1982]; and Willem Balke, Calvin and the
Anabaptist Radicals (Eerdmans, 1981}.)

The first chapter of Packull’s study, “A Medieval Point of Depar-
ture,” is an excellent survey of the medieval mystical tradition,
which saw salvation as deification.

The second chapter, on Hans Denck, shows how this early
Anabaptist was actually a self-conscious defender of these heresies
against the Reformers. Denck made up collections of supposed con-
tradictions in Scripture in order to decry the Biblicism of the
Reformers, and to point to mysticism as the only route to salvation.
At this point, as at others, Denck anticipated the teachings of Karl
