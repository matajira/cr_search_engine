CHRISTIANITY AND POLITICS 151

Ghristians are self-righteous, because they hold this position.
I thought only God could see the heart?!

There are two fundamental weaknesses with this position
held by Sen. Harfield concerning religious neutrality in the
political arena. First, there is a misunderstanding of the rela-
tionship of religion to culture (life, including politics). And,
second, there is an (unintentional) denial of two basic prin-
ciples of Christianity: (1) the lordship of Jesus Christ over all
of life; and (2) the all-embracing authority of the Law of God.

Religion and Culture

What is the relationship of religion to culture (life), ac-
cording to the teachings of the Bible, and recognized by many
Christians and non-Christians alike? This is not an irrelevant
theological debate. It is central to the whole discussion of the
role of the Christian and of Christianity in American politics.
A misunderstanding here can be, and has been, devastating
for Americans.

What do we mean by the word ‘religion’? It is “the binding
tendency in every man to dedicate himself with his whole
heart to the true God or an idol,”* according to F. Nigel Lec.
In this sense all men are religious because no man can escape
being a man in the image of God created to worship and serve
God, rebellious and unregenerate though he be. Romans 1:25
says, “For they [mankind] exchanged the truth of God for a
lie, and worshipped and served the creature rather than the
Creator... .” Man is inescapably religious.

What do we mean by the word ‘culture’? It is religion ex-
ternalized. Culture “is the unavoidable result of man’s neces-
sary efforts to use and to develop thé world in which he lives
either under the guidance of the Lord or under the influence
of sin, in accordance with whichever of the two controls his
heart, As such, culture includes all of man’s works—his arts,
his science, his agriculture, his literature, his language, his as-
tronomical investigations, his rites of worship, his domestic
life, his social customs, (his politics}—in short, the cultural
products of the whole of man’s life stand either in the service

4. FN, Lee, The Central Significance of Culture (Nutley, NJ: Presbyterian
and Reformed Publishing Go., 1976) p. 121.
