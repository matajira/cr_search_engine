CHRISTIAN RESISTANCE TO TYRANNY di

in relation to the person, church, and family under Christian
theories of Natural Law, therefore, have often been highly
similar to those under the regime of Biblical Law. Law has
been seen to be both objective and specific; government has
been seen to be limited in its derivative authority by the con-
tent of Natural Law and God’s word. Consequently, as in-
timated above, Christian disobedience and even resistance to
ungodly, tyrannical laws and governments has been seen to
be both a justified act and a godly duty. As with Biblical Law,
government has been seen to be under, not over, law, and
there has been both a justified act and a godly duty. As with
Biblical Law, government has been seen to be under, not
over, law, and there has been both theoretical and practical
protection for the liberty of the person, as well as for God's or-
dained institutions, the family and the church, by means of
acts of resistance against tyranny.

The problems with Christian theories of Natural Law
originate in its Greco-Roman historical derivation, but stem
particularly from its faulty epistemological foundation, in the
means of knowing or determining the content of “Natural
Law.” For Christian theories of Natural Law make man’s rea-
son the means of knowing the nature and content of God's
creation-manifested laws."Moreover, Christian Natural Law
conceives of man’s reason as being autonomous, in relation to
knowing God’s creation-revealed laws, so that it affirms an
epistemological, ethical, and Natural Law-legal common
ground between Christians and pagans, and thus holds that
pagans, using their God-given reason, can know and will pro-
claim God’s moral law just as well as Christians. Further-
more, because many. Christian Natural Law theorists reduce
God's law taught in nature to merely general ethical prin-
ciples, omitting the case law applications of the Ten Com-
mandments given in the Old Testament, “Natural Law” ac-
cording to their reading loses the specificity, concreteness,
and applicability of Biblical Law, and becomes vague,
abstract, and uncertain, and hence increasingly removed
from the ken of the common man and increasingly useless as a
guide to or restraint on Government. When combined with
natura! men freed to proclaim their naturally ungodly hearts’
desires as law, this spells trouble.”

Even where Christian Natural Law theories explicitly
recognize the necessity of reasoning upon conscience, they
