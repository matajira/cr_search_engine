28 CHRISTIANITY AND CIVILIZATION

against self-incrimination does not permit a taxpayer to refuse
to obey a summons issued under Internal Revenue Code 7602
or a court order directing his appearance. He is required to
appear and cannot use the Fifth Amendment as an excuse for
failure to do so, although he may exercise it in connection
with specific questions. He cannot refuse to bring his records,
but may decline to submit them for inspection on Constitu-
tional grounds.”

That's right, Pilgrim. That's what I.R.S. tells its own
agents. In short, the recipient of an 1.R.8. summons must
show up, but doesn’t have to show his records, That's what
Pastor Dykema did.

To see all this in proper perspective, you need to know that
in 1976, the Communist Party refused to file the information
on contributions that is required of ai! political parties in the
United States. The Federal Election Commission took the
Communists to court, and federal judge Lee Gagliardi ruled
for the Reds. The Commission appealed, and the Second Cir-
cuit Court of Appeals in New York upheld the ruling. The
Court explained that if the Reds were made to obey the law,
many people would not contribute to the Party, because of
fear of “governmental or private harassment and rebuke.”

So, the Communist Party is exempt from government in-
spection of its records. But the Church of Christian Liberty is
not.

The case went to court. On April first, 1980, I.R.S. agent
Henry Graner testified before U.S. Magistrate Aaron Good-
stein. Graner explained that he visited the Church.
Remember, he was going there to determine whether the in-
stitution is in fact a church. In a version of the Inquisition,
LR.S. presumes to rule upon theology. Here’s what he found:
“Items, documents reflecting that Pastor Dykema performed
marriages, and he also performed baptisms. . . . I did receive
some pamphlets, namely a document indicating the type of
worship, ...I received a document where it says they’re
going to have an open house. I received a contract that
extended to teachers which the church considers
ministers, . . .”

Mr. Graner was shocked by this practice. Elsewhere in his
testimony, he explains: “Although, in all fairness, I did see
one thing that’s of question. That is their treating instructors
and teachers as ministers or just based on the fact of discus-
