CHRISTIAN RESISTANCE TO TYRANNY 113

and in case law. Though many of these folks may not like the
degree of theft (ungodly taxation and inflation) or the specific
kinds of theft practiced in Russia or the U_S., they still accept
the principle of the “welfare state,” the forced redistribution of
wealth, or theft. But having accepted the principle of theft as
legitimate for government—thereby saying that God’s law
does not apply to government, or to groups who use govern-
ment for their own ends — how does one objectively tell where
theft must stop? It always must be a matter of individual opin-
ion and debate, in the absence of Biblical Law. Now, since
property is obviously related to the well-being of the family
and the church as well as of the individual, it should be ob-
vious that lacking fixed restraints on governmental taxation
and income redistribution the ruled lack standards by which
to protect not only personal property, but also the family and
the church. It is precisely because we lack such publicly ac-
cepted standards today that the Humanists have been able to
launch such wide-ranging attacks on the Christian family and
church. And iz is precisely because many Christians have
abandoned objective and detailed Biblical standards of law
that they permit such anti-Christian attacks on their brethren.
The state, they reason, must have a legitimate interest in clos-
ing down Christian schools, licensing Christian institutions,
taking over church properties for taxes, and taking children
from Christian parents who discipline them Biblically or send
them to Christian schools.” Christian liberty is the loser
under such theories.

The American War for Independence was based on the
premise that the Bible and created nature give us specific
standards for limits on government, specific protections for
life, liberty, person, property, family, and church. The
Constitution, designed by men who were overwhelmingly

47, For an excellent introduction to this legal and political battle, see
Kent Kelly, The Separation of Church and Freedom (Calvary Press, 400 South
Bennett Street, Southern Pines, N.C. 28287; 1980); Onalee McGraw,
Family Choice in Education: The New Imperative (The Heritage Foundation, 513
© Street, N.E., Washington, D.C. 20002; 1978); and Martin P, Claussen
and Evelyn Bills Claussen, eds., The Voice of Christian and Jewish Dissenters in
America; U.S, Internal Revenue Service Hearings on Proposed “Discrimination” Tax
Controls Over Christian, Jewish, and Secular Private Schools, December 5, 6, 7 8
£978 (The Piedmont Press, P. O. Box 3605, Washington, D.C. 20007;
1982).
