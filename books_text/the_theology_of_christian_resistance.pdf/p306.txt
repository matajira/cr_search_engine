THE DEBATE OVER FEDERAL
SOVEREIGNTY, 1798-99

Introduction by Gary North

The Alien and Sedition Laws of 1798

HE Alien and Sedition Acts were narrowly passed by a

congress controlled by the Federalist Party. The Sedition
Act was passed by a sectional vote of 44 to 41, with only two
people from south of the Potomac voting in favor.1 Federalists
were convinced that the general European war against France
was about to spread to the United States, with France as the
aggressor, despite the fact that the British were “impressing”
~forcibly kidnapping—American sailors to serve in the
British Navy.? The Federalists were concerned that members
of the Jeffersonian Republican Party would favor the French.
Some of the Federalist spokesmen viewed the Jeffersonians as
being pro-French, pro-Jacobin, and even pro-Illuminati.?

The Virginia and Kentucky Resolutions of 1798-99

‘The response to the Alien and Sedition Acts was im-

1, John C. Miller, The Federalist Era 1789-1801 (New York: Harper Torch-
book, [1960] 1963), p. 231.

2, fbid., pp. 224-25,

3, In May, 1798, Rev. Jedediah Morse, a conservative, Galvinistic Con-
gregationalist minister, and America’s best-known geographer — who was also
the father of Samuel F, B. Morse, who later developed the
telegraph—preached a fast day sermon in Boston which announced that a
secret conspiracy, the Illuminati, which had fostered the French Revolution,
had invaded the United States. The sermon created a sensation. Morse
offered no evidence, however. He was relying heavily on the recently pub-
Jished book on the Iuminati, Proofs of a Conspiracy (London, 1797), by John
Robison. Morse’s sermon is contained in the Evans Bibliography, and
reprinted on microcard in the Early American Imprints Series, edited by
Clitford Shipton, and published by the American Antiquarian Society, card
number 34149. On the panic over the Illuminati, sce Vernon Stauffer, New
England and the Bavarian Hluminati (New York: Russell & Russell, [1918] 1967).

266
