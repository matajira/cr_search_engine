JOHN CALVIN’S THEOLOGY OF RESISTANCE 201

“spiritual” and the “temporal” jurisdiction, by which is meant
that the former sort of government pertains to the soul, while
the latter has to do with the concerns of the present life. The
one we may cal] the spiritual kingdom, the other, the political
kingdom. Now these two, as we have divided them, must
always be examined separately; and while one is being con-
sidered, we must call away and turn aside the mind from
thinking about the other. There are in man, so to speak, two
worlds, over which different kings and different laws have au-
thority.”7°

After having dealt with conscience, Christian freedom,
and the Church, Calvin again reiterated the distinction at the
beginning of his section on civil government in order to make
sure that it was understood that he was shifting from speaking
about the spiritual to speaking about the political kingdom:
“The two kingdoms are alike valid, but definitely dis-
tinct... . Whoever knows how to distinguish between bady
and soul, between this present fleeting life and that future
eternal life, will without difficulty know that Christ’s spiritual
kingdom and the civil jurisdiction are things completely
distinct. . . . Spiritual freedom can perfectly well exist along
with civil bondage; .., It makes no difference what your
condition among men may be or under what nation’s laws you
live, since the Kingdom of Christ does not at all consist in
these things.”?!

In light of the thoroughly positive assessment that Calvin
makes of civil government, discussed earlier, it is difficult sim-
ply to conclude on the basis of the above statements that
Calvin is, in the final analysis, a Christian platonist; and, in
his opinion, man is split into a lower, insignificant physical
aspect and a higher spiritual principle. Calvin was not
unaware of this possible misunderstanding, and therefore
hastened to add: “Yet civil government has as its appointed
end, so long as we live among men, to cherish and protect the
outward worship of God, to defend sound doctrine of piety
and the position of the church, to adjust our lives to the soci-
ety of men, to form our social behaviour to civil righteous-
ness, to reconcile us with one another, and to promote general
peace and tranquillity,”72

 

70. Institutes, UT, xix, 15.
71. Institutes, IV, xx, 1.
72. Institutes, WV, xx, 2.
