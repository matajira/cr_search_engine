2 CHRISTIANITY AND CIVILIZATION

sword (Matt. 10:34). Much toa often, the modern church has
sought peace and compromise with the world. As a conse-
quence, the church has compromised and allowed the tide of humanism
to roll over society and encompass it. Nowhere has this been more
true than in the Christian community's silence and ac-
quiescence to the ever growing power and unconstitutionality
of the federal and state governments and their agencies. The
stare is abusing its power. It is up to the Christian community,
which knows that the state is not absolute, to stop it.

A Subtle Caesar

Conflict between Christ and Caesar is not inevitable; in
fact, Jesus specifically commanded His disciples to “render
unto Caesar the things that are Caesar’s” (Matt. 22:21). Con-
flict becomes inevitable when the secular authority ~Cacsar
—demands for himself honors that belong only to God. Church
and state can never be entirely separated, for each is in-
terested in a wide range of human concerns, and their in-
terests inevitably overlap. As long, however, as the state does
not claim absolute authority and autonomy, it-can exercise a
lawful role in establishing order and civil justice. In this
capacity, the state is called the servant of God (Rom. 13:4).
The problem arises when the state claims not a relative and
derivative authority, but an absolute and autonomous one.

In modern America, the state does not openly claim divine
worship, as pagan Rome did; it permits churches to carry on
their worship as before. But in effect it is seeking to make itself
the center of all human loyalties, the goal of all human aspira-
tions, the source of all human values, and the final arbiter of
all human destiny. In so doing, without using the language of
religion, it is claiming to be divine, and it is creating a potentially
devastating conflict with the church.

The state has become the modern divinity. It is now gen-
erally recognized that Communism, despite its commitment
to atheism, is a religion, and this makes inevitable its bitter
conflict with the church, What is equally true, although less
clearly perceived in so-called “Christian” America, is that gov-
ernment in America is also a religion and is already involved in a bitter
conflict with the reliyion of Christ. A president may declare him-
self to be born again and take an active role in congregational
worship, but that is no guarantee that the state he heads will
