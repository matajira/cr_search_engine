292 CHRISTIANITY AND GIVILIZATION

And with the passing of this almost unlimited taxing and
money-creating power into the hands of people at the central
government, the American people have been subsequently
conditioned financially to laok to the national government as
their Great Provider. They now turn to Washington for the
meeting of all their material needs and to solve all their prob-
lems. In short, the American people have eschewed Jesus’ in-
structions to pray to our heavenly Father for our daily bread
(Matt. 6:11), and they have allowed the national government
to become their gad. In fleeing self-responsibility, they have
set up the national State as their secular god. As a people
Americans have become idolatrous! When the Old Testament
Israelites turned idolatrous, judgment was never long in coming.
Can we likewise hope’to escape God’s retributive judgment?

The burning question which now faces concerned
Americans, especially Christian Americans, is this: What can
be done about the disturbing unbiblical growth of absolutist
and tyrannical civil government in America? What can be
done to reclaim the original American Christian dream of in-
dividual freedom and self-responsibility before God, which is
the underlying principle upon which our American Republic
was founded?

Faithful preaching of the saving gospel of Christ is, of
course, part of the answer. But it is not the whole answer, for
it is only too evident that many truly born-again Christians
blindly and ignorantly support the aggressive growth of hu-
manistic civil government that we have been witnessing dur-
ing this century. And why do they lend such support? Because
they fail to see at least two things:

(1) the anti-Christian, pro-humanist thrust which underlies
the present trend toward absolutist unitary government in its ag-
gressive attempts to manipulate and control citizens with the goal
of ushering in an earthly utopia, and

(2) the fact that the civil authority, as well as individuals, can
and does break God’s immutable commandments, “Thou shalt
not steal,” and “Thou shat not covet.”

The English Puritan, William Perkins (1558-1602), was
well aware of this second point when he wrote:
If it should fall out that men’s Jaw be.made of things evil, and for-

bidden by God, then there is no bond of conscience at all; but
contrariwise men are bound in conscience not to obey.
