CHRISTIANITY AND CIVILIZATION

than the United States, rests upon the fact that they are spending
jess for defense than they normally would have because of de-
pendency on the atomic weapons the United States holds. As the
Soviets, with their Jack of care for the individual, have used a
larger portion of their national income for building up their great
military machine, so Western Europeans used less money for
defense and built up their economy.

“If the balance is now destroyed, there is no doubt in my mind
that either militarily or politically the greatly superior Soviet
Forces in Europe would soon overshadow Western Europe. The
leaders like Schmidt of Western Germany understand this, even
if the left wing of his party and the peace movement do not un-
derstand it.

*UJnilateral disarmament in this fallen world, and with the
Soviets’ materialistic, anti-God base, would be totally utopian
and romantic and lead, as utopianisms always do in a fallen
world, to disaster. Further, it may seund reasonable to talk of a
freeze at the present level or ‘we won't ever use atomic weapons
first, but thinking it through, either of these equals practical
unilateral disarmament, The atomic deterrent is removed and
Europe stands at the absolute mercy of the overwhelmingly
superior Soviet forces.

“The world quite properly looks back to the church in Ger-
many during the early days of Hitler's rise and curses ir for not
doing something when something could have been done.
Churchill said in the House of Commons after Chamberlain
signed the Munich pact:

[The people] should know that we have sustained a defeat
without war. . . . They should know that we have passed an
awful milestone in our history . . . and that the terrible words
have tor the time being been pronounced against the Western
democracies: “Thou art weighed in the balance and found
wanting.” And do not suppose this is the end; this is only the
beginning of the reckoning. This is only the first sip, the first
foretaste of a bitter cup which will be proffered to us year after
year unless, by a supreme recovery of moral health and mar-
tial vigor, we arise again and take our stand for freedom as in
olden time.

“I do not always agree with the French theologian-philosopher,
Jacques Ellul, but he certainly is correct when he writes in his
hook, Fatse Presence of the Kingdom:

It was in 1930 that Christians should have alerted the world to
decolonization, to Algeria and to Indochina, That is when the
churches should have mobilized without let-up. By 1956,
those matters no longer held a shadow of interest. The socio-
political process was already in operation, and it could not
have made an iota of difference whether Christians got into
