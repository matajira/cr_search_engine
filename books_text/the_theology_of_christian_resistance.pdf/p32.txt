 

xxii GHRISTIANITY AND CIVILIZATION

of the Soviet Union. .

The war is here; on this, our contributors are agreed.
Alan Stang tells of the war between the Internal Revenue
Service and Christian churches—specifically, the Church of
Christian Liberty in Brookfield, Wisconsin. Churches are
legally independent of the I.R.S., according to the tax code,
and automatically tax exempt. The I.R.3. no longer is honor-
ing the law. Stang’s article shows the legal grounds on which
the Church of Christian Liberty is taking its stand. Stang
argues that “We are headed toward a time when the govern-
ment would license clergymen and churches; when the
churches would be tolerated and permitted to function, as in
ancient Rome, if the preachers bend their knees and hail
Caesar. Such ‘churches’ would be instruments of government
policy.” He provides information that churches can use to de-
fend themselves legally against such infringements on their
liberty.

Section 2 of the journal presents papers on the biblical
theology of resistance. Anyone who doubts the morality of his
stand will find it dificult to pursue a long-term program of
resistance. This is why we must be certain of the
righteousness of our position. In my essay, I present an
analysis of the ministry of Elijah, a ministry in three stages:
confirmation (invisibility), confrontation, and hiding (in-
visibility). He needed confirmation of the validity of his call
before God; so did his enemy, king Ahab. When he came be-
fore the false priests to challenge them, they had already been
“softened up” by three years of drought. Then he retreated to
a cave, as had 100 other prophets, for Gad’s history to work it-
self out in Israel. Elijah avoided a premature challenge, and
he beat a hasty retreat when his opponents became too strong.
Throughout the period, the remnant was being built up by
God,

Again and again, Christians return to that classic case of a
rebel ‘against state power, Rahab. She covenanted with the
Hebrew spies, hid them, and helped them escape. Then she
lied to the authorities concerning their whereabouts. Those
who caution against rebellion (not to mention lying, which
really bothers the pietists and legalists in the church) must deal
with the case of Rahab. Jim West makes their job very
difficult. He deals with the problems of lying, the prohibition
on bearing false witness, and the historical circumstances of
