THE DEBATE OVER FEDERAL SOVEREIGNTY 279

imposts, excises; to pay the debts, and provide for the com-
mon defence and general welfare, of the United States, and to
make all laws which shall be necessary and proper for carry-
ing into execution the powers vested by the Constitution in
the government of the United States, or any department
thereof, goes to the destruction of all the limits prescribed to
their power by the Constitution; that words meant by that in-
strument to be subsidiary only to the execution of the limited
powers, ought not to be so construed as themselves to give
unlimited powers, nor a part so to be taken as to destroy the
whole residue of the instrument; that the proceedings of the
general government, under color of those articles, will be a fit
and necessary subject for revisal and correction at a time of
greater tranquillity, while those specified in the preceding
resolutions call for immediate redress.

8. Resolved, That the preceding resolutions be transmitted
to the senators and representatives in Congress from this com-
monwealth, who are enjoined to present the same to their
respective houses, and to use their best endeavors to procure,
at the next session of Congress, a repeal of the aforesaid un-
constitutional and obnoxious acts.

9. Resolved, lastly, That the governor of this common-
wealth be, and is, authorized and requested to comrnmunicate
the preceding resolutions to the legislatures of the several
states, to assure them that this commonwealth considers
union for special national purposes, and particularly for those
specified in their late federal compact, to be friendly to the
peace, happiness, and prosperity, of all the states: that,
faithful to that compact, according to the plain intent and
meaning in which it was understood and acceded to by the
several parties, it is sincerely anxious for its preservation; thai
it does also believe, that, to take from the states all the powers
of self-government, and transfer them to a general and con-
solidated government, without regard to the special govern-
ment, and reservations solemnly agreed to in that compact, is
not for the peace, happiness, or prosperity of these states; and
that, therefore, this commonwealth is determined, as it
doubts not its co-states are, to submit to undelegated and con-
sequently unlimited powers in no man, or body of men, on
earth; that, if the acts before specified should stand, these con-
clusions would flow from them— that the general government
may place any act they think proper on the list of crimes, and
