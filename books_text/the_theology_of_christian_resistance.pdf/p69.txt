WHAT THE WAR IS REALLY ABOUT 29

sions with Dale Dykema and reading the brochures,

“Q. Gan you tell me, Mr. Graner, did you see that on the
financial statements, about the fact that the church considers
teachers to be ministers?

“A, Yes, it’s on the profit and loss statement. The contract
is for pastors and ministers. I even asked, I said, ‘The
ministers are what?’ And I was informed, I believe, by Mr.
Whitehead, that they were the teachers that receive a ‘call’
and they are issued a contract, that’s mentioned earlier, which
I received in blank.”

There is a mountain of evidence that the treatment of
teachers as ministers is scripturally sound. Scripture ad-
muonishes us to teach our own. There is, for instance, the doc-
trine of the universal priesthood of believers, It is hard to
come up with something that should be less controversial.

But notice that Mr. Graner was shocked. Remember that
one of the reasons he went to the Church was to find out
whether it really is a church; his testimony makes clear that
this perfectly routine Church practice left hirn dubious. His
own testimony makes clear the fact that 1. R. S. has usurped
the power to rule on theology.

Indeed, even with the best of will, the potential for abuse
in such a systerm is great. Mr. Graner was shocked because he
is a Roman Catholic, who was inspecting an independent,
fundamental church. Even less surprising than the church’s
treatment of teachers as ministers, is the fact that Mr. Graner
was shocked. No doubt the same potential for abuse would
exist, were the government to send your obedient servant or
Pastor Dykema to inspect the Roman Catholic Church,

In spite of which, toward the end of Mr. Graner’s
testimony, the attorney for the Church elicited this: “Q. During
your tour of the facility, did you see anything in the facility
which would lead you to believe that this was, in fact, not a
church operation?

“A. From the tour of the facilities, no.

“Q. Did you see anything that might have had anything to
do with outside business activities, which would produce
income?

“A. No.”

Magistrate Goodstein found for the Church, and his recom-
mendation was adopted by U.S. District Judge Myron Gordon,
who ordered the government's petition for enforcement of the
