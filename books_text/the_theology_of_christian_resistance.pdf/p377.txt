REVIEWS 337

Barth (himself an anabaptist) and his followers.

The next three chapters deal with Hans Hut and his influence.
Packull draws on the as-yet-unpublished lifetime-labor of Dr. Gott-
fried Seebass on the life and thought of Hut, which makes Packull’s
discussions by far the most authoritative discussion of Hut in the
English language. Hut had originally been involved in the revolu-
tionary movements headed up by Thomas Mintzer, and only moved
in a more pacifistic direction when that movement failed. Like Denck,
he was a neoplatonic mystic who conceived of salvation as diviniza-
tion, and he also produced lists of supposed Scriptural inconsistencies.
Repeated predictions that the world was coming to an end, supported
by purported visions and miracles, undermined Hut’s credibility
when they consistently failed to come true. [One of these days the
same thing will happen to some members of our modern quasi-
Anabaptist movements: dispensationalism and pentecostalism. ]

‘The final two chapters deal with the collapse of this wing of
anabaptism into isolationist communities, and the histories of two
“homeless minds”: Binderlin and Entfelder.

The importance of Packull’s study is severalfold. First; it is an
indispensible contribution to the typology of the Reformation era,
synthesizing the interpretations of Karl Holl and Ernst Troeltsch,
and thus of major value to students of the Reformation era. Second,
it is a valuable study of the contrast between orthodox Christianity
and mysticism in its non-sacramental form, Packull is not blind to
the issues: “The Reformation emphasis on total human depravity
and the denial of any human merit (inchiding merit derived from
human volition) were logical consequences of Luther’s rejection of
any division of man’s nature. Predestinarianism provided a theocen-
tric corollary to this wholistic anthropology in the realm of theology
proper. In contrast, the tradition focusing upon the unio mystica
assumed the cooperation of man with the divine presence in him as
part of the salvation process. The maintenance of the dualistic
matter-spirit dichotomy in man was a necessary prerequisite for
such a view” (p. 178).

Finally, although modern bipartite and tripartite views of the
soul, modern apocalypticism, modern pentecastalism, and modern
anti-paedobaptism are found in more evangelical garb, the reader of
Packull’s study is struck by the similarities in. type between these
heretics and modern American movements. The piety of paniheism
is, after all, not that far removed from that of the American gospel
refrain hymn tradition, which too often seeks the absorption of the
human person in the rapture of God. The doctrine of justification
