CHRISTIAN RESISTANCE TO TYRANNY 115

religious ideas about God, ethics, men, and things.5! The
Bible is clear that men have two basic religious philosophies:
one anti-Christian and worldly, and the other Christian and
anti-worldly. These two religious philosophies take diametri-
cally opposite views of God and His word. The worldly tradi-
tion of unbelievers makes therm enemies of God, who see
God's word as utter foolishness and will not be subject to it.
The Christian view fears God, sees Christ as the source of all
wisdom and knowledge, and seeks to make every thought cap-
tive to Him. More to the point, these two religious philoso~
phies have diametrically opposite results, The worldly tradition
achieves at best a knowledge falsely so-called, which is a true
error and the opposite of wisdom. The Christian view
achieves genuine knowledge, due to its repentant faith, and is
thus renewed in knowledge and wisdom.5? The Bible con-
demns the ideas of autonomous reason, neutral or impartial
thinking, an intellectual-moral common ground between
Christians and pagans, and the acquisition and advocacy of
Truth by pagans. This is not to say that pagans can learn
nothing, nor that Christians can learn nothing from pagans.
The mind that seeks to replace God’s law with man’s law may
admit the truth of seme of God’s laws, but its enmity against
the Lord, and its desire to be its own Lord, will never let it
admit the goodness and justice either of God's law as a whole or
of God's law in is details. Man wants to make God's thoughts
and laws captive to himself; but God commands man to make every
thought captive to Christ, to think, as Van Til has phrased it,
God's thoughts after Him. We have it on the highest Author-
ity that man cannot serve two masters. Either man will love
God's revealed law-word, or he will love the word and pseudo-

51. Dr. Cornelius Van Til has done the most to make the Church aware
of this humanistically obscured fact. Sec his A Survey af Christian Epistemology
(Presbyterian & Reformed, 1977); his The Defense of the Faith (Presbyterian &
Reformed, 1955); and his A Christian Theory of Knowledge (Presbyterian &
Reformed, 1969).

52. If the reader doubts the applicability of these points to Christian and
other theories of Natural Law, let him but read the writings of those who
adhere to such theories— writings which give but a perfunctory nod to the
reality of the Fall, and which act as though Scripture were irrelevant. Lam
also indebted to John W, Robbins’s important essay, “Some Problems with
Natural Law,” The Journal of Christian Reconstruction 1:2 (Winter,
1975-1976);14-21, for many of the foregoing and subsequent points.
