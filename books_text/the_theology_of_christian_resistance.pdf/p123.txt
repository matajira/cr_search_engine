PACIFISM AND THE OLD TESTAMENT 83

(whatever that means in this system) for one other. Christ is a
gnostic savior who has come down into the world to infuse
toleration of evil, and a power to avoid evil, into the world.

Thus, third, Enz’s theology has no category of Divine
justice. His God is not even a creator, eternally separate from
His creation. The essence of the gospel is a love which woos
people back to God through toleration (pp. 7#f.). The Chris-
tian gospel is that God hates and punishes sin and sinners,
even calling on men to implement His work (through the
sword). Salvation comes from the vicarious death of the unique
God-man under the fury of God’s wrath. All men must die for
sin, but the elect die in Christ, and thus are resurrected.
Judgment, then, is the foundation of resurrection, and of the
experience of love.

Enz closes his book with what must be taken as a wholly
Satanic attack on God: “Because he has his own war to wage,
the Christian breaks completely with the earthly strategy of
physical combat. If the Christian is to be true to the Lord
Jesus Christ, he must make a full break with the anczent and ul-
timate apostasy. His weaponry is not the belligerent word of ultima-
tum issuing in the thrust of the sword that divides, destroys,
and perpetuates the very evil he is seeking to eliminate. His
weaponry is rather the persuasive word of the gospel issuing
in compassionate concern for liberation, reconciliation, and
celebration of the approaching Christ-ordained age of peace”
(p. 88). [Emphasis added.] The God of Scripture, Who
punishes evil, is obviously the author of “thé ancient and ulti-
mate apostasy.” Christ, who came to divide households (Matt.
10:34ff.), is perpetuating evil. The Gospel According to Enz
is but the siren song of Satan, that God ought to love
everybody and excuse sin. In fact, God ought to come down
and taste a little sin for Himself (which Enz says Christ did).
The God of Enz is the Satan of Scripture.

Oh, yes: the Old Testament and pacifism. Well, Enz actu-
ally deals very little with the Old Testament. [is sole argu-
ment seems to be that since God miraculously delivered Israel
on important occasions, this militates against the notion that
Israel should ever have taken up the sword to defend itself
(pp. 52f.). Enz thinks that his gnostic God was trying gradu-
ally to lead people away from violence toward tolerance, by ,
showing them that He could deliver them and so they should
not try to arm themselves to do so. Well, it is true that the
