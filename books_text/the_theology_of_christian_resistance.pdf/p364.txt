324 CHRISTIANITY AND CIVILIZATION

This order was widely sabotaged by the Protestant private
schools, often with active cooperation of school inspectors,
and despite the substantial risk to those schools which refused
io cooperate with the German regulations, of losing their
financial support.

In August 1941, all school boards were ordered to submit
a list of Jewish and half- Jewish children attending Christian
schools, These children would be collected in special schools
in due time, Following the advice of the National School
council for Schools-with-the-Bible, the large majority of
schools ignored this order, which actually implied that Jewish
children were prevenied from receiving a Christian educa-
tion,

Of course, fierce conflict arose about the authority to ap-
point the school’s teaching staff. In a number of cases the Ger:
mans fired teachers who had sharply criticized the Nazism. It
also came about that school boards felt that it was necessary to
fire those teachers who preached Nazi ideologies, clearly in
direct conflict with the Christian faith. Then, the Department
undid the school board’s decisions. In a few instances, the
conflict rose to such a height that the school was shut down
because the parents refused to send their children to a school
under Nazi dominance.

Total Nazification of the Christian schools failed, how-
ever, due to the resistance of most teaching personnel, school
boards, and parents. If the war had lasted much longer, or if it
had ended in total German victory, then the Germans would
certainly have carried out their plans, if necessary with the
toughest measures.

The Start of the “Landetijke Organisatie” (“National Organization”)

The decisive turning point in the war came in the begin-
ning of 1943. Germany suffered a tremendous defeat at Stal-
ingrad. Rumors about an invasion of allied forces in Europe
became stronger every day. Hitler was forced to conscript
more and mor€é men for military service in order to turn the
tide. Workers from occupied territory were transferred to
Germany in large numbers to keep the production of industry
in operation.

In Holland, those born in 1922-1924 were ordered to work
in Germany, all young men aged 19-21. Additionally, the
