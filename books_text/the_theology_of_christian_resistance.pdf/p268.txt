228 GHRISTIANITY AND CIVILIZATION

near one half of North America. Is not this the loudest call to
arms? All is at stake—we can appeal to Gon, that we believe
our cause is just and good. But to attend to our text, “and
fight for your brethren,” our brethren in the Massachusetts
are already declared rebels—they are treated as such, and we
as abettors are involved in the same circumstances — nothing
can be more unjust than such a proclamation. Rebels are men
disaffected with their sovereign in favour of some other per-
son. This is not the case of America; for if Jacobites were not
more common in England than with us, we should not have
had occasion of the present dispute. We very well know what
follows this proclamation, all our estates are confiscated, and
were we even to submit, we should be hanged as dogs. Now
therefore let us join, and fight for our brethren. Remember
our congress is in eminent danger. It is composed of men of
equal characters and fortunes of most, if not superior to any
in North America. These worthy gentlemen have ventured all
in the cause of liberty for our sakes—if we were to forsake
them, they must be abandoned to the rage of a relentless
ministry. Some of them are already proscribed, and no doubt
this would be the fate of the rest: How could we bear to see
these worthy patriots hanged as criminals of the deepest dye?
This, my countrymen, must be the case, if you will not now as
men fight for your brethren: Therefore if we do not stand by
them, even unto death, we should be guilty of the basest in-
gratitude, and entail on ourselves everlasting infamy. But if
the case of our brethren is not so near as suitably to affect us,
let us consider the condition of our sons and daughters. Your
sons are engaged in the present dispute, and therefore subject
to all the consequences: Oh! remember if you submit to ar-
bitrary measures, you will entail on your sons despotic power,
Your sons and your daughters must be strangers to the com-
forts of liberty — they will be considered like beasts of burden,
only made for their master’s use. If the groans and crics of
posterity in oppression can be any argument, come now, my
noble countrymen, fight for your sons and your daughters.
But if this will not alarm you, consider what will be the case of
your wives, if a noble resistance is not made: all your estates
confiscated, and distributed to the favourites of arbitrary
power, your wives must be left to distress and poverty. This
might be the better endured, only the most worthy and flower
of all the land shali be hanged, and widowhood and poverty
