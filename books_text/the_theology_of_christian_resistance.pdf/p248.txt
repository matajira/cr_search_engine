208 CHRISTIANITY AND QIVILIZATION

it was necessary to add this admonition {io obedience),
for... the Romans... subdued these countries more by
unjust guiles than in any legitimate way. Besides, the Caesars
who then reigned had snatched possession of the monarchy by
tyrannical force. Peter, therefore, forbids all these things to be
brought into debate.”!1

“Divine Providence has wisely arranged that various coun-
tries should be riled by various kinds of government. . |.
Countries are best held together according to their own par-
ticular inequality. However, all these things are needlessly
spoken to thase for whom the will of the Lord is enough. ...
It is our duty to show ourselves compliant and obedient to
whomever he sets over the places where we live.”19?

As far as obedience is concerned, it does not matter
whether rulers live up to the expectations put upon them,
whether or not they rule according to biblical principles, or
even whether they have been elected by popular vote or not.
Christians must recognize that it is God who places the ruler
in office, and it is their duty, then, to obey.

The third and final argument that Calvin had to deal with
involved the derelict or tyrannical ruler. Although these are in
reality two different categories, Calvin dealt with them as one.
The prominent argumentation here is Calvin's appeal to the
fifth commandment. As has been previously noted, Calvin
parallels family life and the political life of the nation. Just as
children are bound to chey their father and a wife her hus-
band whether he exercises his authority as he should or not, so
we owe obedience to those in authority over us.1°3 “The
perpetual law of nature is not subverted by the sins of men;
and therefore, however unworthy of honor a father may be,
he still retains, inasmuch as he is a father, his right over his
children.”10+

“Kings and magistrates often abuse their power, and exercise
tyrannical cruelty rather than justice: .. . But. . . tyrants and
those like them do not do such things by their abuse, without
the ordinance of God still remaining im force, just as the
perpetual institution of marriage is not subverted even

101, Commentary on Ist Peter 2:13.

102. Instituzes, TV, xx, 8.

103. Snstitutes, IV, xx, 29.

104. Commentary on Deuteronomy 5:16, Exodus 20:12.
