26 CHRISTIANITY AND GIVILIZATION

The Internal Revenue Service bases most of its case in this
matter on Section 7605(c) of the Code, which allows limited
examination of the church records only if the District Director
“believes” such activities are present, Section 7605(c) thereby
pays reluctant deferrence to the Fourth Amendment doctrine
of “probable cause,” but at no point in all this has the District
Director ever claimed that he “believed” any such thing. In
short, [.R.S. wanted to do what our liberal friends say is so
outrageous when the government tries to do it to a liberal, or
even revolutionary, group: I.R.S. wanted to conduct a
“fishing expedition.” The Internal Revenue Service wanted to
look through the records in search of something “wrong.”

The Church refused. Pastor Dykema recalls that the agents
were surprised, taken aback, even “flabbergasted.” It is in-
teresting to note that a member of the board asked the agents
for proof of identification and their home addresses — which they
gave. Presumably this means that taxpayers are entitled to
receive such information.

The agents left. There were other meetings, in which the
Church, eager to cooperate, did present some limited
financial information, such as a year’s list of contributions,
without the contributors’ names; and eighteen months of cash
flow, including payroll. The Internal Revenue Service still
wasn't satisfied.

On the last day of August, 1979, LR.S. agent Henry
Graner issued a Summons, ordering Pastor Dykema to ap-
pear before him with fourteen different categories of
documents, including: the names of aii contributors during
the previous four years; aff correspondence during that
period; the names and addresses of afl officers, directors,
trustees, and ministers; a// minutes; and, “All documents
reflecting the principles, creeds, precepts, doctrines, prac-
tices, and disciplines espoused by the above-named organiza-
tion.”

Graner also used the phrase, “but not limited to.” This
meant that if the Church had some document he hadn't
thought of listing, Pastor Dykema was supposed to bring that
along, too. One is reminded of the United States Constitu-
tion. There, the Founding Fathers set forth a long list of the
things the federal government is nai allowed to do; ending, in
the Bill of Rights, with the warning that if there is something
the Fathers specifically forget to prohibit, it, too, is forbidden
