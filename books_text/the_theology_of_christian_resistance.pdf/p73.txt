WHAT THE WAR IS REALLY ABOUT 33

liturgical rituals, as well as preaching ministry and evangeli-
cal outreach to the unchurched and missionary activity in par-
tibus infidelium; (b) pastoral counseling and comfort to
members facing grief, illness, adversity, or spiritual problems;
(c) performance by the clergy of customary church ceremonies
affecting the lives of individuals, such as baptism, marriage,
burial and the like; (d) a system of nurture of the young and
education in the doctrine and discipline of the church, as well
as (in the case of mature and well developed churches)
theological seminaries for the advanced study and the training
of ministers.”

Remember that, according to Dumbauld, the list of ap-
proved and maybe mandatory church activities “would in-
clude” these. Presumably there are others he doesn’t mention.

First, we have “corporate worship,” which apparently is
worship conducted by a corporation. Durmbauld disqualifies
unincorporated worship. Then we have a “preaching ministry.”
Does this disqualify a community of cloistered ascetics? Then
we have “evangelical outreach to the unchurched and mis-
sionary activity in partibus infidelium.” This qualification is
blatantly anti-Semitic, since Jews conduct no such activities.
We hope the Jewish community rightly and righteously will
want to protest, In the Christian community, the Primitive
Baptists consider themselves the original Baptists, from whom
the others broke away. As a matter of basic doctrine, they
have no missionaries, and do not evangelize, things which
they regard as presumptuous. According to Dumbauld, they
are disqualified. In partibus infidelium is of course a Latin
phrase. Lawyers and judges like Dumbauld use such phrases
to conceal meaning, to confuse and frighten laymen; to justify
continuation of the legal monopoly, including payment of
those luscious fees.

According to The New Webster Encyclopedic Dictionary of the
English Language (Chicago, Consolidated Book Publishers,
1971), in partibus infidelium means; “In parts belonging to
infidels or countries not adhering to the Roman Catholic
faith.” Does this mean that Dumbauld disqualifies evangelical
outreach and missionary activity to countries that do adhere to
the Roman Catholic faith? Does it mean that to satisfy him a
church must be Catholic? These are amazing questions, but
they are perfectly serious. Dumbauld is using the power of the
federal government to dictate doctrine.
