178 CHRISTIANITY AND CIVILIZATION

and be afraid; but let us at the same time guard most carefully
against spurning or violating the venerable and majestic
authority of rulers, an authority which God has sanctioned by
the surest edicts, although those invested with it should be most
unworthy of it, and, as far as in them lies, pollute it by their ini-
quity. Although the Lord takes vengeance on unbridled domi-
nation, Jet us not therefore suppose that that vengeance is
committed to us, to whom no command has been given but to
obey and suffer. I speak only of private men. For when
popular magistrates have been appointed to curb the tyranny
of kings {as the Ephori, who were opposed to kings among the
Spartans, or Tribunes of the people to consuls among the
Romans, or Demarchs to the senate among the Athenians;
and perhaps there is something similar to this in the pawer ex- ©
ercised in each kingdom by the three orders, when they hold
their primary diets). So far am I from forbidding these
officially to check the undue license of kings, that if they con-
nive at kings when they tyrannize and insult over the humbler
of the people, I affirm that their dissimulation is not free from
nefarious perfidy, because they fraudulently betray the liberty
of the people, while knowing that, by the ordinance of God,
they are its appointed guardians.

But in that obedience which we hold to be due to the com-
mands of rulers, we must always make the exception, nay,
must be particularly careful that it is not incompatible with
obedience to Him to whose will the wishes of all kings should
be subject, to whose decrees their commands must yield, to
whose majesty their sceptres must bow. And, indeed, how
preposterous were it, in pleasing men, to incur the offence of
Him for whose sake you obey men! The Lord, therefore, is
King of kings. When he opens his sacred mouth, he alone is to
be heard, instead of all and above all. We are subject to the
men who rule over us, but subject only in the Lord. If they
command anything against Him let us noi pay the least
regard to it, nor be moved by all the dignity which they
possess as magistrates—a dignity to which no injury is done
when it is subordinated to the special and truly supreme power
of God, On this ground Daniel denies that he had sinned in
any respect against the king when he refused to obey his im-
pious decree (Dan. vi 22), because the king had exceeded his
lirnits, and not only been injurious to men, but, by raising his
horn against God, had virtually abrogated his own power. On
