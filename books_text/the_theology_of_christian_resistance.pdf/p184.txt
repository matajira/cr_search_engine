144 CHRISTIANITY AND CIVILIZATION

Yet stay 2 moment.

There is one discoverable “right” implied and expressed in
all human rights codes, one that is common to all and
identifies each as belonging to the collection of human rights,
one I have noted above. This is the right to rebel,

The Virginia Declaration marks a decisive departure from
such often proposed evolutionary ancestors as the Mayflower
Compact or documents of the Massachusetts and Connecticut
colonies in that it assumes the root premise that man naturally
is without society and government, and that government itself
is humanly devised. It is surprising how universally this con-
cept infected the writings even of such stalwart Eighteenth -
Century Christian thinkers as William Paley.

It may be that the Mayflower Compact itself gave great
impetus to the social contract notion, for certainly it was an
instance in which a body of men consciously entered into con-
tract for a body politic which was previously not explicit. Yet
this must be radically modified by-the fact that the Mayflower
Company already existed as an ecclesiastical body, which by
its own doctrine held Divine authority to do whatever was
necessary to promote tranquility for God’s people on earth, as
well as for the salvation of souls. Moreover, these same men
had never existed apart from society and were giving up no
“rights” appertaining to individual sovereignty or liberty
whatsoever. The Fundamental Orders of Connecticut of
1638/39 represent a parallel case. The statement of Liberties
of the Massachusetts Golony is merely a forthright statement
of common law rights presented in terms of common law
thinking dear to all Christians and bears no hint of rights in-
herent in a supposed lawless state of nature.

In truth, right-to dissent is not a lawful claim to own or to
do something, which is the true right: it is a turning upside
down of right and wrong, calling good evil and evil good. It
says men have the right in the true sense of duty to overthrow
their rulers whenever it pleases them: or, in other words, it is
a negation of right. It is a claim to the worst of all moral evils,
the right to be wrong.

There are many popular slogans which state the case as
well as it can be stated. One is well known: “I disagree wholly
with what you say, but I will defend with my life your right to
say it.”

This is utter nonsense. But it adequately defines the sense
