THE LESSER MAGISTRATES {71

ready minds prove our obedience to them, whether in com-
plying with edicts, or in paying tribute, or in undertaking
public offices and burdens, which related to the common
defence, or in executing any other orders. “Let every soul,”
says Paul, “be subject unto the higher powers.” “Whosoever,
therefore, resisteth the power, resisteth the ordinance of God”
(Rom. xiii 1, 2). Writing to Titus, he says, “Put them in mind
to be subject to principalities and powers, to obey
magistrates, to be ready to every good work” (Tit. iti 1). Peter
also says, “Submit yourselves to every human creature” (or
rather, as I understand it, “ordinance of man”), “for the
Lord’s sake: whether it be to the king, as supreme; or unto
governors, as unto ther that are sent by him for the punish-
ment of evil-doers, and for the praise of them to do well” (1
Pet. ii 13). Moreover, to testify that they do not feign subjec-
tion, but are sincerely and cordially subject, Paul adds, that
they are to commend the safety and prosperity of those under
whorn they live to God. “I exhort, therefore,” says he, “that,
first of all, supplications, prayers, intercessions, and giving of
thanks, be made for all men; for kings, and for all that are in
authority: that we may lead a quiet and peaceable life in all
godliness and honesty” (1 Tim. ii 1, 2). Let no man here
deceive himself, since we cannot resist the magistrate without
resisting God. For, although an unarmed magistrate may
seem to be despised with impunity, yet God is armed, and will
signally avenge this contempt. Under this obedience, I com-
prehend the restraint which private men ought to impose on
themselves in public, not interfering with public business, or
rashly encroaching on the province of the magistrate, or at-
tempting anything at all of a public nature. [fit is proper that
anything in a public ordinance should be corrected, let them
not act tumultuously, or put their hands to a work where they
ought to feel that their hands are tied, but let thern leave it to
the cognizance of the magistrate, whose hand alone here is
free. My meaning is, let them not dare to do it without being
ordered. For when the command of the magistrate is given,
they too are invested with public authority. For as, according.
to the common saying, the eyes and ears of the prince are his
counsellors, so one may not improperly say that those who, by
his command, have the charge of managing affairs, are his
hands.

But as we have hitherto described the magistrate who
