224 CHRISTIANITY AND CIVILIZATION

the higher powers. We cannot suppose, either that this text
enjoins absolute submission to all laws, which may be made
in a land; for some are so wicked, oppressive and unjust in
their nature and tendency, that the best of men have thought
it their indispensable duty to disobey them. You may well
remember, that Nebuchadnezzar made it a certain law, that
all nations in his ernpire should, on pain of death, worship his
golden image. Was it the duty of his subjects to obey or not?
The conduct of Shadrach, Meshach and Abednego will deter-
mine the point, who, refusing to comply, were cast into a fiery
furnace. Remember also, when Darius, king of Persia, made
a statute, that no man should petition either Gop or man,
save himself, for thirty days, Daniel refused obedience unto
the decree, because it was unrighteous. Call to mind also, that
in the days of Pharaoh, King of Egypt, he enjoined it as a law
to all the midwives, that they should kill all the male children
of the Jews. Did they obey or not? The text informs us that
the fear of Gon prevented them, believing that no law can
make that just, which in its own nature is unrighteous. The
higher powers, of which Paul speaks, are ordained of Gop,
and if ordained of him, they must be like.unto him, who is a
Gop of unspotted justice and truth.

Certain it is, that the people must be the judges whether
the laws are good or not—and [ think it must be acknowl-
edged by all, that laws are not good, except they secure every
man’s liberty and property, and defend the subject against the
arbitrary power of kings, or any body of men whatsoever.

Again, another text, used to enforce the ductrine of
nonresistance in the present dispute, is, Submit yourselves to
every ordinance of man, for the Lord’: sake, whether unto the king as
supreme, or unto governors as unto them that are sent by him, for the
punishment of evil-doers, and for the praise of them that do well. I Pet.
1:13, 14, This text is to be understood in a restricted and
limited sense as above. We cannot suppose, that the apostle
meant obedience to all despotic ordinances of tyrants; for this
would condemn the conduct of the midwives, Daniel,
Shadrach, Meshach and Abednego. The ordinances here can
be-none other than such as are good, and ordained of Gon, for
whose sake we are to submit unto them; for these higher
powers, or ordinances, are the provision of Gop, which he has
ordained for the Safety of the people, to defend them against
oppression and arbitrary power. There is none, but Gop,
