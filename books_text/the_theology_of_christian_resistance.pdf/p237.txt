JOHN CALVIN’S THEOLOGY OF RESISTANCE 197

for all men, the concrete historical constitutions and judicial
laws, because of differing circumstances, may well differ, as
long as they look to the same end of equity. This equity alone
must be the rule and end and limit of all laws. As he writes,
“Whatever laws shall be framed to that rule, directed to that
goal, bound by that limit, there is no reason why we should
disapprove of them, however they may differ from the Jewish
law, or among themselves.”5¢

Therefore, the laws made by a specific state ought to he an
expression of the natural equity in terms of the situation and
needs that the context dictates. For that very reason Calvin
refused to speculate about the concrete form of the laws of
different nations, since such discussion could go on forever,
and would not be profitable.5”

In addition to the nature of civil law, the point that is par-
ticularly important to this discussion, and must be reiterated,
is that Calvin described the relationship of the magistrate and
the law by means of the ancient formula, “the law is the silent
magistrate, and the magistrate a living law.” In other words,
the magistrate cannot stand without the law, but it is also true
that the laws themselves have no force apart from the
magistracy. It is extremely important for Calvin that the
magistrate not be beyond the law, or without law.** In fact,
for Galvin, the notion of being beyond the reach of the law is
the main characteristic of what constitutes a tyrant.

The People, or The Ruled

It has already been shown that for Calvin each man has
his distinctive calling before God, whether he realizes it or
not. The place of the magistrate in Calvin’s political thought
has already been examined. Now it is time to turn to the peo-
ple, or the ones who are ruled.

To understand Calvin’s position, it is appropriate to pres-
ent the two major contrasting views alongside of his, As has
been noted, absolutism was a problem in Calvin’s native
France. Absolutism is the tyranny of the ruler. It is best
described by the phrase, “The people are made for the sake of

56. Institutes, IV, xx, 16.
57. Institutes, IV, xx, 16.
58. Commeniary on Exodus 3:22.
