WHAT'S WRONG WITH HUMAN RIGHTS 139

mention of him is the crror of unbelief which falsifies all the
rest that is said about human life,

The true force of this proposition was widely and clearly
understood. It was simply that law and government are not
natural to man; that man wittingly at some point entered into
a state of society of his own making; that human law and gov-
ernment are contrary to nature; and that, finally and most
importantly, Jaw and government are human inventions.
What man gave, man can alter or take away. Many Christian
people found this notion acceptable if they explained law and
government as having been made necessary by Adam’s fall.
That Scripture will not support such a notion did not deter
those who wanted to believe it. That such is the correct under-
standing of what the Virginians and others of their day had in
mind is seen in succeeding paragraphs of the declaration.

The second paragraph says, “That all power is vested in,
and consequently derived from the People; that Magistrates
are their Trustees and Servanis, and at all Times amenable to
them.” The meaning could not be more clear, nor more op-
posite Biblical thought. The ruling proposition of Scripture
and Christian doctrine is that “power belongeth unto God”
(Psalm 62:11); or “There is no power but of God; the powers
that be are ordained of God” (Romans 13:1). Magistrates are
not trustees and servants of the people, but ministers of God
for wrath (Romans 13:4}.

The conclusion that mankind, having given government,
may also take it away is made explicit in the third paragraph:
“Whenever any government be found inadequate or contrary
to these Purposes, a Majority of the Community hath an in-
dubitable, unalienable, and indefeasible Right, to reform,
alter, or abolish it...”

This is the heart of the matter. It is the explosive ground of
revolution in all its forms. The attribution of power to
“reform, alter, or abolish” any government to a “Majority of
the Community” only gives assent to Rousseau’s lame attempt
to shore up his social contract theory by postulating such a
thing as the “general will,” which in turn could be deter-
minable only by majority vote. However, this simply means
law and government are no more than what the strongest
force in the cornmunity can make it at any given time. That
this is almost word for word the analysis of law given by Chief
Justice Oliver Wendell Holmes of the United States Supreme
