THE EARLY CHURCH AND PACIFISM 165

points the finger particularly at the Synod of Arles (314 AD),
which was convoked by Constantine and which “in return for
governmental protection, at last wholeheartedly accepted the
civil and military obligations that the state was entitled to
demand of any citizen” (p. 172). And the pity was, as Hornus
observes, that the Church got so little for its bargain. In
perhaps the best and most forceful chapter in the book,
Hornus shows Constantine for what he probably was: a
hypocrite, a political bargainer, and a murderer, who never
stopped promoting the pagan cults even up to his death, and
who was baptized on his deathbed by an Arian bishop under
the delusion that a delayed baptism would wash away more
sins (pp. 201-208). Mournfully, Hornus’s last chapter con-
cludes that early Christianity overwhelmingly condemned
“homicidal violence” of any sort, but sacrificed its belief to
pursue the “illusion of a Christian empire.”

It seems pretty plain that the starting point of Hornus’s
book is precisely this suspicion of “Christian empire,” and of
statism in general. Of all the people described and quoted in
the book, one suspects that Hornus harbors the greatest ad-
miration for Tertullian, who refused to admit of any higher
authority than God. That certainly would harmonize well
with Hornus’s French Reformed origins. In fact, it is a thor-
oughly laudable proposition in many respects: the opposition
of Cromwell to Charles I, of the Hussites to the Emperor, and
even of the Huguenots to the French kings, were all based on
the same idea. Yet, their application of that idea did not take
the form of pacifism. Therefore, one can heartily agree with
Hornus’s basic thesis that ungodly government ought to be
resisted — without it necessarily following that one ought to do
so by becoming a pacifist. The one, single, tenuous comnec-
tion that Hornus is able to make between just resistance and
pacifism is what he called the “affirmation that all human life
is sacred,” and it is tenuous simply because in quoting
Tertullian, Chrysostom, and Lactantius in support of that
“affirmation,” he quotes them wildly out of context. Ter-
tullian, he insists, opposed even criminal execution: which is
true, but only as we consider that Tertullian was protesting
the type of execution (people being pulled to pieces by beasts in
the arena for the gratification of the spectators and, in the
dedication ceremonies preceding, the gods), not execution per
se. This one failure of historical judgment on Hornus’s part is
