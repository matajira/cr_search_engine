PACIFISM AND THE OLD TESTAMENT
A Survey of Four Recent Books

James B. Jordan

ISTORICALLY, the Reformed and evangelical churches,

not to mention the Roman Catholic and Eastern Ortho-
dox, have held that the Anabaptist tradition is im error in
holding to pacifism. British and American Baptists, being
modified Presbyterians rather than root-and-branch Anabap-
tists, have concurred with that opinion. Recently, however,
Anabaptist theology has enjoyed a tremendous upswing in
popularity, Among “Reformed Baptists” there has been a con-
siderable movement away from a Spurgeonic “modified Pres-
byterianism” toward more of a wholesale adoption of Anabap-
tist theology, which has resulted in not a little tension between
the two camps. (Roughly speaking, and the situation is quite
fluid, we are speaking of a difference between the “Banner of
Truth” Baptists, and the “Baptist Reformation Review” neo-
Anabaptists.)

At the same time, Mennonite theologians have begun to
be regarded as within the boundaries of acceptable “Christi-
anity Today” evangelicalism, and Anabaptist theology has
had continuing and strident advocacy by “left-wing evangeli-
cals,” the best known of which is Ronald Sider. All of this has
meant that Anabaptist views of war, capital punishment, and
resistance to tyranny have become live issues once again in
Christendom.

Generally speaking, Anabaptist theologies have had a
tough time incorporating any kind of positive view of the Old
‘Testament, because of its holy wars and its mandatory capital
punishment, In recent years, however, the development of the
discipline known as “Biblical Theology,” which seeks to un-
derstand the Bible in a more historical, less logicistic fashion,
has been called upon to serve the Anabaptist cause. Recent
writers have argued not that “New Covenant pacifism” is
radically opposed to “Old Covenant militarism,” but that the

75
