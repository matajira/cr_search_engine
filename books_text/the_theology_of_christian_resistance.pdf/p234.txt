194 CHRISTIANITY AND CIVILIZATION

recognize the divine ordination, and respect them accord-
ingly.36

Yet, in spite of his comments concerning the authority and
dignity of the magistracy, Calvin must be understood as not
lending his support to absolutism. He insists that though it is
God alone that invests kings with authority, and it is by His
will that they hold their positions, all power remains lodged in
Him, and in so far as respects themselves, they have no legiti-
mate title to reign.*” Calvin, speaking as the true prophet that
he was, often sounded a warning to the magistrates: “Judges
and magistrates should not arrogate to themselves a power
uncontrolled by any laws, nor allow themselves to decide
anything arbitrarily or wantonly, nor, in a word, assume to
themselves what belongs to God. Magistrates only acquit
themselves properly when they remember that they are repre-
sentatives of God.”3#

“Let not kings and princes flatter themselves that it seems
that the world is created for them, for they are created for the
multitude. Has not God established principalities and
kingdoms for the common good?”39

“Kings are warmed to submit reverently to God’s Word,
and not to think themselves exempted from what is common
to all, or absolved, on account of their dignity, for God has no
respect of persons.”#0

“God has set his own arm and power in opposition to the
pride of those who thought that they stood by their own
power, and did not acknowledge that they were dependent on
the hand of God alone, who sustained them as long as he
pleased, and then overthrew and reduced them to nothing
when it seemed good to him.”#

Calvin's position, rather than supporting the idea that his
elevated view of the magistracy lent itself to the rising ab-
solutism in France, proves without a shadow of a doubt that
his view of the magistrate, instead of diminishing the ruler’s
responsibility to God, the law, and his subjects, rather
enhanced it, and placed the ruler, as well as the people, in the

36. Institutes, UL, viii, 38.

37, Commentary on Psalms 110:1.

38. Commentary on Exodus 18:15.

39. Calvin's Sermons from Job (Baker Book House, 1980), p. 192.
40. Commentary on Jeremiah 36:29.

41. Commentary on Jererniah 27:5.
