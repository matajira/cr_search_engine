WHAT'S WRONG WITH HUMAN RIGHTS 145

of “human rights.”

Scripture says it is wrong to resist authority, “Human
rights” says it is right to resist.

Anyone who wishes may proceed from this point to trace
out particular applications of the “human rights” metaphysic
to the exigencies of tires and occasions. In every case, there is
claimed a specific “right” to resist a particular institution or
custom or law of a given social order.

The Virginia Declaration specified the “right” to dismantle
the order of the nobility, the officer caste of Christian peoples,
who were—~ by virtue of their responsibility — the ruling class.
This was stated in the negation of the true lawful right of in-
heritance which is inseparable from the right to property or
dominion, The declaration said no emoluments or privileges
from the community could be awarded except in considera~
tion of services rendered and are not “descendabile.” Unless
the “community” be equated with the government, the decla-
ration would outlaw all inheritance (a popular cause among
human rightists even today). It also, more to the point, an-
ticipated independence from Britain.

The French revolutionaries, of course, made no bones
about their desire to dismantle the whole social order, not only
the nobility and the clergy but all other structures, and this by
virtue of “the rights of man.”

Yankee radicals inflamed the Northern peoples to mount
the Civil War in the name of a “human right” to be free and, if
they did not destroy the whole Southern order, they. did at
least dismantle its vast and efficient plantation economy.

Tradition, law, and custom, which preserved public peace
and order in the bi-racial state of the union, both North and
South, were the target of the right to resist in the 60's, the sup-
posed human rights justifying the violent means.

The list could be extended considerably, but enough has
been said to make the point. There is, however, one present
application of the “human rights” metaphysic making it
Jawful to revolt: that is the insistence upon the legalization
everywhere of revolutionary parties, especially Communist
parties.

As against the Soviets, no doubt, it makes sense for us to
try to force the government to permit and even protect dissent
without regard to how it may affect the stability of the regime.
It would be to our advantage. But those who cheer for the
