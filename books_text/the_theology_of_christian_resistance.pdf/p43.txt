CHRISTIAN RESISTANCE 3

not continue and even intensify its struggle against Christianity
and the churches. Indeed, it was under a professing evangelical
Christian president, Jimmy Carter, that the federal bureau-
cracy made. some of its most threatening inroads into areas of
Christian concern.

Obviously, Christianity and the new state religion of America can-
noi peacefully co-exist. Since the church cannot claim and do less
than God requires of her, she must call upon the state to back
away from its increasingly explicit pretensions to divinity. Of
course, if the United States’ president were to demand to be
hailed as “divine Caesar,” and the state created an open
religious cult like Roman state paganism, Americans in their
vast majority would rebel. But as long as these claims come in
quietly, and without fanfare, in the form of a shift in the basis of
law and government, Christians may remain tranquil. While
Christians are dormant, the pagan state continues to enlarge
and consolidate its gains. If this continues for much longer,
when Christians wake up it will be too late to do anything but
suffer.

The Bible and Christion Resistance

The silent church does not speak to the issues. And, when
confronted by the state, it either presents a weak protest and,
if pushed, wilts. This stance, however, was not the position of
early Christianity or the position of the biblical writers. The
early Christians went to the lions rather than transgress God's
law and compromise with the secular power. In this sense
they were rebels. That is how Rome saw them. As Francis
Schaeffer in How Should We Then Live? (1976) has written; “Let
us not forget why the Christians were killed. They were not
killed because they worshipped Jesus... . Nobody cared
who worshipped whom as long as the worshipper did not
disrupt the unity of the state, centered in the formal worship
of Gaesar. The reason the Christians were killed was because
they were rebels.”

The battle for. Christian existence is upon us. As the state
becomes increasingly pagan, it will continue to exert and to
expand its claims to total jurisdiction and power over all
areas, including the church. Inasmuch as only biblical Chris-
tians have a reference point outside the state, it will take Chris-
tian rebels to stem the tide of the humanistic state— Christian rebels
