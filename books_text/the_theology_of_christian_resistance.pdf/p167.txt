CHRISTIAN RESISTANCE TO TYRANNY 127

tions which have embodied it.7? Then, as men’s autonomous
thinking follows its natural downhill course, the remaining
Christian content of Natural Law becomes leached out by the
rising current which springs from man’s “autonomous”
desires and logic: “nature” and “Natural Law” become “pro-
gressively” Pagan. As the theory and practice of Pagan “Nat-
ural Law” becomes increasingly destructive of the old order,
and follows its inevitable downhill path, defenders of the old
order awake and arise to protect their inherited ways and
laws: conservatism of one sort or another becomes an intellec-
tual and social force.

American and Western conservatives of one sort or
another have sought to defend the old order's protection of
diversity, localism, tradition, diffusion of authority and power
in society, and individuals’ and groups’ (state and local gov-
ernments, churches, voluntary associations, families} legal
and constitutional rights, together with the inherited morality
and liberty protected and fostered by the old legal order. But
until recently these defenses have been made in terms of “au-
tonomous” reason alone, or of reason over revelation, or of
yeason and tradition: in terms of variants of the older, syn-
cretistic versions of Natural Law. Lacking a Biblical founda-
tion, such defenses are thus open to challenge and assault on
the grounds of both their own premises and their con-
sequences.

The premises of the revivalists of Natural Law”* are defec-
tive on at least four grounds. First, they presuppose the au-
tonomy of man’s reason, which, as we have seen, is both un-
biblical and self-destructive. Second, as a correlative, they
presuppose a non-fallen nature, which is bath unbiblical and
incompatible with men’s diverse definitions of nature, from
which flow their diverse theories of the laws of nature.

Third, they presuppose the practical irrelevance of God,

73. Traditions such as Puritanism and the other Christian faiths which
dominated carly American society, and the English Common Law, which
was incorporated into American law under the Constitution.

74, The term “revivalists” is ironic, af course, for these thinkers are,
almost without exception, opposed to Christian revivalism, even while they
seek to revive their faith. These thinkers are surveyed in Friedrich’s con-
cluding chapters, and in Henry B. Veatch’s important bibliographical essay,
“Natural Law: Dead or Alive?” Literature of Liberty, 1:4 (October/December,
1978):7-31,
