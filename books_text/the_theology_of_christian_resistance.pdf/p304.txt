264 CHRISTIANITY AND CIVILIZATION

be no free riders, and that those whe did not participate in the
militia would not be offered protection against British incur-
sions. Indiscriminate British, and especially Loyalist,
plundering and retaliations further polarized the population
toward the American cause.

As the war progressed, the militia became, in many ways, a
more effective fighting force than the regular army, which
contained a large segment of some of the less desirable
elements in American society. In Major John M. Goetschius
we glimpse an American military officer whose grasp of the
principles of people’s guerrilla warfare was equal to any of the
great historical theorists of those concepts. Complaints about
the militia by officers such as General George Washington
reflected an unwillingness to recognize these concepts of war-
fare, and how such a force could most effectively be used.

Unlike the inflated script used to pay sporadically the regu-
lar army, the Bergen County militia was paid, apparently, in gold.
Their commitment was to attacking the British and Loyalists
whenever they chose to attack that county, and the militia
leaders felt less effective when told to encamp in other, distant
areas. This attitude toward the militia of some army officers is
indicative of one of the major “fault lines” within the revolu-
tionary coalition,43 Some leaders had always desired imperial
territorial gains from the war, as well as independence. This
desire ran so deep that they had launched an attack on
Canada early in the war, greatly over-extending American
resources. Such leaders were not interested in securing peace
in 1778 unless it also included Canada and Florida. Local
farmer militia self-defense forces, as those in Bergen County, were sim-
ply not excited by such imperial adventures. A good example of this
was evident late in the war, in 1781, when Washington sent
General Lafayette north to attempt again to mount an
assault. The leaders of the Vermont militia replied that they
would not enlist unless they were promised “double pay, dou-
ble rations, and plunder,” a clever way of aborting the whole
idea,

For any Americans who have spent these years of the
Bicentennial and after reading back on the origins of the
Republic, it must have become apparent how much yet needs

43. See Marina, “People’s War,” for a more detailed account.
44, Quoted in Smith, People’s History
