wil CHRISTIANITY AND CIVILIZATION
30's. In fact, the Reformation itself can be viewed as a
massive resistance movement against lawfully constituted
authorities, as can the English Puritan Revolution and
Cromwell's reign, 1638-58. Yet both the Reformation and the
Puritan Revolution were grounded in theology. If the Bible
prohibits all forms of resistance against constituted authority,
was the Reformation illegitimate?

Was England's “Glorious Revolution” of 1688 lawful? Can
we take John Locke’s word on this? Classical liberal English
political theory was based to a large extent on Locke’s natural-
law defense of the right of revolution, as was much of the rhet-
oric of the American Revolution. It was a revolution against a
Roman Catholic king, James IJ, so it had religious underpin-
nings. Furthermore, there is little doubt today that the
American Revolution was to a large extent a religious rebel-
lion, and was known as the “Presbyterian revolt” in British
circles (see the essay by Marina and Cuervo).? Was this
revolution immoral from a biblical point of view? For that
matter, what about Magna Carta of 1215? Were the English
feudal barons morally and legally justified in demanding
various concessions, including economic and prestige conces-

1. Norman Cohn, The Pursuit of the Millennium: Revolutionary Messianism in
Medieval and Reformation Europe and Its Bearing on Modern Totatitarian Movements
(and ed.; New York: Harper Torchhook [1957] 1961), chap. 12. See also
Willem Balke, Calvin and the Anabaptist Radicals (Grand Rapids, Michigan:
Eerdmans, 1981).

2. Archie Jones, “The Christian Roots of the War for Independence,”
The Journal of Christian Recinstruction, “Symposium on Christianity and the
American Revolution,” ITI (Summer, 1976); Carl Bridenbaugh, Mitre and
Scepter: Transatlantic Faiths, Ideas, Personalities, and Politiss, 1689-2775 (New
York: Oxford University Press Galaxy Book, [1962] 1967); Nathan O,
Hatch, The Sacred Cause af Liberty: Republican Thought and the Millennium in
Revolutionary New England (New Haven, Connecticut: Yale University Press,
1977); Cushing Strout, The New Heavens and New Earth: Political Religion in
Ammica (New York: Harper & Row, 1974), chaps. 1-3; Alan Heimert,
Religion and the American Mind: From the Great Awaksning to the Revolution (Cam-
bridge, Massachusetts: Harvard University Press, 1966); Alice M.
Baldwin, The New England Clergy and the American Revolution (New York:
Ungar, (1928] 1958); The Christian History of the American Revolution: Consider
and Ponder, edited by Verna M. Hall (San Francisco: Foundation for
American Christian Education, 1976); Bernard Bailyn, “Religion and
Revolution: Three Biographical Studies,” Perspectives in History, TV (1970),
pp. 85-169.
