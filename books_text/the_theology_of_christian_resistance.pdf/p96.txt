56 CHRISTIANITY AND CIVILIZATION

for most periods of time. People prefer to avoid confrontations.
There are times, however, when confrontations cannot be
avoided, when men must visibly choose between Baal and
God. But the Israelites’ faith was a faith in patience as such, to
serve the one who possessed outward power for the moment. It
was a faith in continuity as such—the prevailing faith of most
pagans throughout history. It was a faith in civil power.

The patience of God's people should be different. They are
to have faith in the continuity of God's decree, not the continuity of
pagan time and pagan civic power, They know that there are
times for remaining invisible, quietly working out their salva-
tion. This is why Paul tells us to pray for authorities in power,
so that the church might experience peace (I Tim. 2:1-2), The
Framework of peace allows the church to develop the implications of
biblical faith, to gain experience in the tasks of dominion. Then, when
the time is ripe, the church is ready for the period of conlronta-
tion, a discontinuous break with pagan culture. The
breakthrough eventually takes place, as it did in the early
fourth century in what remained of the Roman Empire, as it
did in the sixteenth century at the time of the Protestant Refor-
mation, and as it did in colonial America in the 1765-89 period.

Endless patience is suicidal. A theology of perpetual patience ts a
theology which denies the necessity of an eventual confrontation. As far as
the Bible inclicates, the Israelites in Moses’ day held just such a
theology. They had to be driven out of Egypt by the Egyptians.
It is a theology which has abandoned an eschatology of domin-
ion. It is a theology which believes that men can live forever on
a hypothetical spiritual mountaintop while dwelling in Egypt.
Tt leads eventually to a confrontation, one which the victims of
persecution are unable to win because they failed to plan for
the confrontation successfully.

Armenia’s Lesson

The most tragic example of a culture which failed to come to
grips with the inescapability of an eventual confrontation is
Armenia. The first nation to adopt Christianity as the official
religion (in 301, a decade before Constantine became Emperor
in Rome), the Armenians suffered repeated attacks and
repeated tyrants. They fought the Persians in the mid-fifth cen-
tury, the Arabs from the seventh century on, the Greeks (who
demanded that Armenians change their theology to Greek
