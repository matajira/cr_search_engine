82 CHRISTIANITY AND CIVILIZATION

Himself to the human race on His cross till the last who wants
it may accept. He demands, but He also descends to the
lowest level of human need to walk upward with those who re-
spond.” There is no notion of the prapitiation of God’s wrath
here, or of any kind of legal transaction on the cross. Every-
thing points to the idea of a gnostic savior reaching down the
scale of being to draw up souls caught in the slime. But
perhaps we are being too harsh, reading too much into this
one sentence.

Dr. Enz, however, leaves no doubt as to where he stands.
A plethora of ancient heresies find expression on pp. 62-65.
For instance, “In addition to the incarnation of God in His
people and in Christ there is yet a sin-neutralized incarnation
of God in everyone.” And, “When we destroy others we are
destroying not only God, but also self since in God our own
self cannot be separated from the self we destroy in others.”
This is pantheism, or panentheism. Christ’s incarnation is the
same as everyone else’s. We are all little pieces of Cod. We are
gods ourselves. Enz’s argument against taking life is identical
to that of the Hindu: all life is God, so to take any life is to at-
tack God.

Again, “We live in a world of right and wrong that has its
own built-in judgment.” No, God is the judge. There is no
such thing as natural law. This is again pantheism, or if Enz
wants to say God created these laws, deism.

‘Again, Enz’s God not only imputes the guilt of sin to
Christ and punishes it in Him, He actually makes Christ a
sinner: “Here is the God of the second mile, for it means the
toleration of sin {not committed, of course, but taken upon
Himself); that which He has forever set Himself against is
taken into His own nature.” There is more: “I am convinced that
there is a basic evident continuity of both incarnation and
substitution from Jesus to us. Here is a willingness to take or
share in the consequences of the wrong act of another even if
we have had no part whatsoever in the wrong act; this ts in
order to help restore integrity in and fellowship with the
wrongdoer. The effectiveness of accepting or sharing those
consequences can only be sure as we share the purily of Christ on
the one hand and completely identify ourselves with the
wrongdoer on the other.” [All emphasis added, except for the
word and.] The work of Christ, in this scheme, is not unique,
but only exemplary. We are all gods, and we can each “atone”
