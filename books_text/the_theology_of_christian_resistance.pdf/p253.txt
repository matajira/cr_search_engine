JOHN GALVIN’S THEOLOGY OF RESISTANCE 213

view, the people have primarily responsibilities and duties to
fulfill; they are not some sort of check against the magistrate.
For those private citizens who would want to rebel and ad-
vocate some change of the order, Calvin reserves his strongest
rebuke: “These men unblushingly belched forth abuse against
the magistrates to take away all respect for public law and
order, and this was openly to attack God with their
blasphemies. There are many turbulent men of this kind to-
day who boast that all the power of the sword is heathen and
unlawful and who busy themselves furiously to overthrow the
body politic. Such ragings which upset the progress of the
gospel are stirred up by Satan.”*#6 In another place he writes
even more categorically and uncompromisingly: “The
wilfullness of kings will run to excess, but it will not be your
part to restrain it; you will have only this left to you: to obey
their commands and hearken to their word.”!!’ The only com-
fort will be that “they who have proudly and tyrannically gov-
erned shal] one day render their account to God,” and that
“God, whose prerogative it is to raise the abject and to relieve
the oppressed,” will come to their help.4® But as far as in-
dividual action on the part of private citizens is concerned,
“the very desire to shake off or remove this yoke is tacit proof
of an evil conscience that is plotting some mischief."118 “It
does not lie in the will of the people to set up princes.”220 “We
should rather consider that an unjust ruler is God’s punish-
ment for the sins of the people and that it is not our calling to
supply a remedy, but that we have to wait upon God and im-
plore His help.”12!

The Lesser Magistrates: Calvin has one long sentence which
outlines his views with regard to the right and duty of
resistance to tyrants by the lesser magistrates. “For if there are
now any magistrates of the people, appointed to restrain!?*
the wilfuilness of kings (as in ancient times the ephors were set
against the Spartan kings, or the tribunes of the people *

116. Commentary on 2nd Peter 2:10.

117. Institutes, IV, xx, 26.

118. Commentary on Genesis 16:8.

119. Commentary on Romans 13:3.

120. Commmiary on Jeremiah 38

121. Commentary on Romans 13:3; Institutes, IV, xx, 29.

122. ‘Lhe Latin has tercedere,” the French editions “s’opposer et
resister,”

 

 

 
