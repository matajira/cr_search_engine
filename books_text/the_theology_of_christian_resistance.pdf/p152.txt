112 CHRISTIANITY AND CIVILIZATION

run afoul of the naturally rebellious heart of the unregenerate
man, which naturally seeks to repeal the law of God; and of
the besetting nature of sin, which plagues even the regenerate
man, and which requires constant grace, and the study of
Scripture and hearing of the revealed word of God to
counteract its destructive tendency in one’s heart—and these
things the natural man lacks. Lacking the regenerating grace
of God, the natural man will always, in some way or another,
distort the promptings of the conscience, so that there is and
can be no true common ground between him and the regen-
erate man whose delight is in the law of the Lord (Ps. 1).

A practical consequence of the abstraction of Christian
Natural Law theories (which depart from Natural Law as the
equivalent of detailed Biblical Law~—the position of Calvin
and the Puritans) is that an abstract and esoteric law known
only to an elite cannot serve as an effective check on abuses of
power by government, and thus cannot serve as an effective
guide for either the rulers or the ruled.” “Lacking clear, con-
crete, and detailed universals, those placed in the ministry of
government (Rom. 13:1-5) lack specific rules by which to
rule. The way is thus opened to both widespread disagree-
ment about what specific laws and policies to enact — which fol-
lows from disagreements about which principles to apply to
specific cases — and to pragmatic, seat-of-the-pants lawmaking.
Should laws be traditional or innovative? How much should
old ways be changed to acommodate new circumstances?
Such questions, answered by Biblical Law, must continually
plague rulers and ruled in a regime founded on abstract Nat-
ural Law, throwing public life into the continual agitation and
turmoil that we see all around us (even among conservatives)
today.

A further consequence of abstract Natural Law is that not
only rulers, but also the ruled, lack an effective guide to civil
obedience, disobedience, and resistance to governmental
tyranny. When law is abstract, esoteric, and pragmatic, there
can be no such clear standards for civil disobedience and
resistance, because there are then no specific standards of law
and justice, right and wrong, good and evil.

For example, many professing Christians today support
(to one degree or another) governmental welfarism and in-
come rédistribution, which obviously involves theft, which is
condemned by. God's word, both in the Ten Commandments

i
i
i
j
\
i

 

 
