244 CHRISTIANITY AND CIVILIZATION

General Burgoyne. Certainly the Frerich fleet offshore and the
American and French forces surrounding him were signif-
cant factors in Lord Cornwallis’s decision to surrender, but we
must not forget that his army had been severely weakened
from numerous encounters with regular army and partisan
forces. Far from liberating the interior of the Carolinas, he
found himse!f losing men, and leaving behind war materials,
as he drove to reach the coast for an attempted evacuation,
Even in the case of Philadelphia, the British had abandoned it
because, despite the use of considerable manpower, it was
simply too difficult to keep it supplied in the face of constant
harassment by militia.

In short, the British were simply never in control of very much of
North America. During the period when their fleet was transfer-
ring the army from Boston to New York, in the face of
Washington’s artillery on the heights above the former, there
were no British in the colonies. Except for relatively short
periods, fram £776 until 1781, the British, on any continuing
basis, controlled little more than the city of New York.

Given these circumstances, there was really very little op-
portunity, or need, for the Americans to organize, or attempt
to sustain, a classic guerrilla insurgency. On the other hand,
after the failure at a negotiated peace during early 1778, the
British began to develop the outlines of a pacification plan.

One way to examine the course of the war and the effec-
tiveness of British strategy, especially with respect to pacifica-
tion, is to study its effect in a smal] area. That is, after all,
what the English and Hessian commanders seemed to be ask-
ing for, a single county that could be pacified, and from
which, like a row of dominoes, they could work out in various
directions, until a whole state, and then others, were secured.

New Jersey: “The Middle Ground”

The British never entertained much hope that New
England would be an initial area for pacification. Connec-
ticut, for example, had only six percent Loyalists, and no
British army ventured into the countryside after the losses at
Lexington and Concord. Late in the war, it was in the South
that the British sought to establish the pacification program,
but there, too, the image of a vast reservoir of Loyalists in the
interior, waiting to be liberated, proved illusory. We noted
