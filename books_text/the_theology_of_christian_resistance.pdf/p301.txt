THE DUTCH-AMERICAN GUERRILLAS 261

Away from his home base, Goetschius and his militia were
faced with the same provision problem that plagued the regu-
lar army. Thus, he wrote to the Governor of New Jersey that
the militia had served some wecks “whilst the army laid here,
[under] about fifteen different commanders as picket to the
whole army,” having to take orders from all these officers
while receiving rations from none. He had applied ta Wash-
ington, to the State, and to the several surrounding counties,
but had received nothing. On Washington’s advice, his men
had also foraged, but there was little left about, and “it must
be taken by force of arms. The inhabitants will not sell any
longer for certificates.” With all these problems, nonetheless,
the militia continued to patrol the dangerous territory be-
tween the two armies, in which occurred most of the fighting.
Leiby’s comment is worth noting: The militia “would have
been more than human, however, if they had noi observed the
Continental’s contempt for all militiamen and if they had not
observed, even more clearly, how often Continentals marched
and countermarched during a whole campaign without seeing
a redcoat, how seldom any Continental ventured down as
close to the British as the militia headquarters posts.

“After the war, when Goetschius’ old militia men stood out-
side the South Church at Schraalenburgh on Sunday morn-
ings waiting for the service to begin and boasting quietly
about their exploits in low Dutch, if some of them were a little
scornful of Continental officers who never saw a British gun,
it was perhaps natural jealousy over their own unsung feats.
No Continental need have troubled himself fer a moment
about their mild grumbling, there were none but Jersey
Dutchmen to hear them, there was to be no Bancroft or
Longfellow to tell of their deeds.”#*

4781; The End of the War

By the middle of 1781 things were little changed in the
Hackensack valley. Cornwallis was in Virginia, but the
British force remained in New York, able to raid into New
Jersey. Washington kept his army nearby, but was unable to
mount an assault on the British base. “The neutral ground
continued to be the stage for probing raids, espionage, and

38. pp. 2918.
