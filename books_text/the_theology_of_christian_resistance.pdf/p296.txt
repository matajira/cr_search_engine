256 CHRISTIANITY AND CIVILIZATION

formation of a fighting organization undreamed of in 1776,
For men thus committed, the British idea of pacification in
1778 was irrelevent. Though few, if any, would have recog-
nized it at the time, even in the middle ground, the war had
essentially been decided by the end of 1777, Though the continental
army in the area did not grow much stronger, the militia con-
tinued to do so. As Leiby notes, “To the patriots the Revolu-
tion was no mere nationalistic revolt against legitimate gov-
ernment, it was a rebellion against Toryism in politics, eco-
nomics, and religion, a Toryism that bred poverty, ignorance,
and despair in Europe and would, given a free hand, do the
same in America. . . . To patriots far more than allegiance to
Britain was at stake; Tory success would have meant a far
different England and a far different world.”2?7 The Jersey
Dutch were no provincials, but understood the larger context
of the war.

The Militia Becomes Dominant

Very slowly the militia began to assert American control of
the area. Two of the more important American victories of
1779 were in the area; General Anthony Wayne’s surprise
bayonet and sword night attack on Stony Point, and the raid
on Paulus Hook, which, while not major engagements, threw
off Glinton’s plans for the year. Wayne’s large foraging expedi-
tion was also a success, though the farmers probably were not
happy about losing their produce and animals to the
American army either. Thus, “it was plain for anyone to see
that it was” the Americans, not the British “who dominated
the neutral ground in 1779; and the land that had filled. the
storehouses of the British during 1776, 1777, and 1778 now
supplied” the American forces,

Late in 1779 a “remarkable indication” of how “the British
cause had lost ground in the past three years” occurred, “for
which there must be few parallels indeed in the history of war
and Revolution,” in 1776 dozens of young men from families
in the valley who were of Loyalist sympathies had enlisted for
three years in the British forces. As their enlistments expired,
these men sought to return to their homes and begin farming

27. p. 215.
28. p. 225.
