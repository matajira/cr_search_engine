THE DUTCH-AMERIGAN GUERRILLAS 257

again, almost as if there had been no war, The American
patriot militia began arresting them to be put on trial for high
treason, but the men claimed to be deserters from the British
army.?9

General Wayne ordered them released on the basis that
such desertion ought to be encouraged, and that prosecution,
“would inevitably deter all others under similar circumstances
from coming over . . . and shutting the door of mercy against
poor deluded wretches who wish to return to the bosom of
their country.” Though the Americans did not know of it, and
no formal effort was ever made to implement it, the British
were at this very time considering planting deserters among
the Americans to serve as spies.*¢

The winter of 1779-80 was a very bad one, made worse by
a drought. In late March, the British, with six hundred men,
launched a raid into New Jersey from two directions. In the
ensuing skirmishes, it appears that the American militia was
less prone to retreat than the regular forces. The British burned
many of the homes of patriots in Hackensack, and carried off
all of the adult males they could find, but the American
harassment was so fierce they could take little or no plunder
with them. As Leiby concludes, “The time was long past
when the British could attack Bergen county as a refreshment
for their troops.”3! The prisoners were later exchanged, but
the British acts only increased the enmity of the Americans. It
was hardly the kind of “pacification” that would win over the
inhabitants. In fairness to Clinton, it appears that the idea for
such reprisals had come from the Loyalist refugees in New
York, who, in the absence of Sir Henry in the South, had con-
vinced the Hessians that such raids were a good policy. On his
return Clinton was “furious,” and later wrote that the raid was
“ill-timed . . . malapropos” based upon “the ill-founded sug-
gestions of .. . over-sanguine Refugees.”3? In April the
British staged a raid on Paramus, much less interested in
retaliation than foraging. While some of the regular army
units were surprised by the action, it was again the militia
which “again turned out like veterans, hanging on the flanks

29. pp, 226-7,
30, p. 219.
31. p. 244.
32. p. 303.
