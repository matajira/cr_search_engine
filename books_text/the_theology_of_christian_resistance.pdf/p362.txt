322 CHRISTIANITY AND CIVILIZATION

different. Some larger periodicals became very well known.
“Het Parool” (“The Password”) had a social-democratic
orientation and remained faithful to its origin during the en-
tire war. “Vry Nederland” (“The Free Netherlands”) was
founded by Protestants and members of the Anti-
Revolutionary Party. Most of its initiators were arrested
rather quickly. During the month of October, 1941, a leading
figure in the editorial board of this newspaper emerged, H.
M. van Randwijk, a talented author and a man of strong
character. He adhered to Christian socialism, however, and
had very radical ideas. The older Christian political parties
acquired his dislike. Holland had to become a socialist coun-
try. Now surprisingly, he admired Soviet Russia greatly,
because of its all-out struggle with Germany. It did not take
very long for this new editor to become engaged in a deep-
seated conflict with his coworkers, most of whom were of anti-
revolutionary background. Many broke ties with the news-
paper; others remained and tried to halt the process. In-
evitably, however, “Vry Nederland” became a socialist illegal
newspaper.

Following these events, a new resistance-supporting
magazine was founded by the Anti-Revolutionaries, named
“Trouw” (“Fidelity”). It soon enjoyed a large readership and
became quite well known.

Maintaining an underground press has required much
sacrifice. Many printers, editors, and distributors were
caught by the enemy, and a large percentage paid with their
lives. The Germans were fiercely hunting down anybody
resisting their deceptive propaganda, and tried everything
possible to smother the free press; but they failed. The illegal
press turned out to have provided a powerful antidote against
the Nazi propaganda, and kept the hope for liberation alive.

The Strugele for the Free Christian School

One of the most precious privileges of Christian Holland
of the pre-war period was its free, Christian schools, which
were not run by the State but by parent-controlled private
boards. Financially, these schools had been given equal rights
with the so-called neutral state-schools.

Almost 80 years of heavy political battle had been needed to
obtain these equal rights for Christian schools. Everybody of
