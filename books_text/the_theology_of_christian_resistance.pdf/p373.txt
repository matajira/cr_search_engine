REVIEWS OF BOOKS

John H. Yoder, Nevertheless: The Varieties and Shortcomings of
Religious Pacifism. Scottdale, PA: Herald Press, 1972, 1976 (second
edition). 144 pages. Indexed. Paperback. $2.50. Reviewed by
James B. Jordan.

The purpose of this brief but quite useful book is to identify and
distinguish twenty-five different types of religious pacifism, some in
more detail than others naturally. In each of the seventeen types
dealt with in detail, Yoder raises objections to the arguments
presented by the particular view of pacifism under inspection, and
then counter arguments. Yoder has, helpfully, not created these
types of pacifism out of any logical scheme or ethical system, trying
to bail various systems down into several “consistent” types; rather,
he has simply allowed each system to speak for itself. This makes the
book a useful survey of existing options.

From the perspective of this reviewer, who does nat believe the
Bible teaches pacifism, Yoder’s interactions are inadequate. Gener-
ally speaking, Yoder does not interact Biblically with the various
positions, but makes very general kinds of comments, showing
alleged logical inconsistencies, or giving gestalt criticisms. That is not
to say that Biblical concerns, from Yoder’s pacificistic perspective,
never come into play; they do, but rather seldom.

Some of Yoder's points are quite well taken. One of the more fre-
quent criticisms of pacifism as a political policy is that it does not
reckon with the depravity of man sufficiently. (The original anabap-
tistic movement was almost to a man self-consciously Pelagian, and
this includes Menno Simons, so that this criticism is not unjust in it-
self.) Yoder's point, however, is that “militarism as well as pacifism is
humanistic and utopian; it places enormous trust in the wisdom of
administrative bureaucracy, in the moral insight of persons who

333
