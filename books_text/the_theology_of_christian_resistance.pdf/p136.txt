96 CHRISTIANITY AND CIVILIZATION

and an imperial judiciary.”

The policy decisions of our new Lawgivers, the anti-
nomian, Constitution-perverting presidents, congresses,
judges, and bureaucrats, have been nothing short of irrational
and disastrous. Their pseudo-laws are irrational in their con-
ception, in their content, and in their consequences. Their
conception is irrational because it denies God and His law and
providential control of history, and thus denies the only possi-
ble source of meaning to history and human action, Their
conception is irrational because it supposes that man in an ir-
rational world devoid of the God of Scripture can save himself
from all that ails him, via the centralized, planning state, and
because the very notion of central planning involves the
assumption by the state of an omniscience which only God
possesses.® Their content is irrational because it seeks to plan
where no adequate knowledge can be obtained, because the
notion of planning as salvation is based on a false view of the
nature of man, and because different statist agencies produce
contradictory regulations (indeed, frequently there are con-
tradictory regulations within a particular agency). The conse-
quences of the pseuda-laws of our would-be-Gods are irrational
(if we are to take their public promises seriously) because the
consequences are contrary to the grandiose and utopian
claims of our would-be saviors.?

All this irrationality should not be surprising: it is

 

collegiate Review, 12:1 (Fall, 1976):3-£0; for some interesting statistical con-
firmation, via an analysis of the growth of the IRS, see James T. Bennett
and Manuel H, Johnson, Jr., “Bureaucratic Imperialism: Some Sobering
Statistics,” The Intercollegiate Review, 13:2 (Winter-Spring, 1978): 101-193,

7, See Rep, Robert K. Dornan and Csaba Vedlik, Jr., Judicial Supremacy:
The Supreme Court on Trial (New York: Nordland Publishing Co., 1980),
Raoul Berger, Government by Judiciary (Cambridge: Harvard University
Press, 1977), and Thomas J. Higgins, S. J., Judicial Review Unmasked (West
Hanover, Massachusetts: Christopher Publishing House, 1981).

8. In order to plan an economy, government must know at least the fol-
lowing: (1) all resources, including that terrible modern term, “human
resources,” currently available, (2) all the resources that will be destroyed or
lost, or that will become available, through discovery or invention, (3) the
current desires of all consumers, and (4) the future desires of consumers,
This is manifestly impossible, for all but God.

9. The best treatment of this is M. Stanton Evan’s Clear and Present
Dangers: A Conservative View of America’s Government (New York: Harcourt
Brace Jovanovich, 1975). Evans is a distinguished Christian convervative.

 
