70 CHRISTIANITY AND CIVILIZATION

See also the Ninth Commandment, “You shall not bear false
witness.” That is to say, you shal] not illegally jeopardize one’s
standing or position in life. Hence, one must conclude thar
Rahab and the midwives were dependable and constructive at
the risk of their own lives, in the face of attemped murder. This is
a prime example of love toward God and their neighbor.+

It is possible to speak the truth “in the Lord” and it is possible
to do so “outside of the Lord” (such as gossipmongering). It is
sometimes argued that Jesus never spoke an untruth, there-
fore we must never utter an untruth, Of course, this proves
too much. If this was valid argumentation then it might be
maintained that since there is no record that Jesus disobeyed
Caesar, then we should never, or since Jesus never disobeyed
His parents, we should never. The Apostles’ “We ought to
obey God rather than man” (Acts 5:29), as well as Jonathan’s
disobedience to his earthly head (I Samuel 20:30, 42), effec-
tively counters this specious reasoning. Yet Jesus does com-
mand us to love our neighbor as ourselves, and Rahab’s un-
truth mirrors this agape. The Ninth Commandment, as a
litotes, does not scrap the idea of bearing false witness in dehaif
of your neighbor.

The Rahab Precedent

Rahab’s falsehood in Joshua 2:4-6 is not an island by itself
but one of many in a vast archipelago. The midwives lied to
Pharaoh’s gendarmes (Exodus 1:15-20); Jael tricked Sisera to
his doom and received the praise of God’s prophetess ( Judges
4:18-21; 5:24-27), Solomon’s threat to divide the child was in-
tended to trick the harlots and thereby extrac the true confes-
sion of the rightful mother, albeit through a disavowal (herein
was “the wisdom of God,” I Kings 3:28); Jeremiah’s lie to the
princes of Jerusalem is noteworthy (Jeremiah 38:24-28);
there is the deception by the Lord Himself to conceal the true
mission of Samuel (I Samuel 16:1-5; and see also I Kings
22:19-23); Michal puts off Saul’s dragoons with sume blanket
legerdemain (I Samuel 19:12-17); David feigns a-psychosis
before Abimelech (the occasion of the praise of Psalm 34);
David gives crafty counsel to Jonathan (I Samuel 20:6,

1. Dr. Hendrick Krabbendam, The Book of james, p. 22. This is a syllabus
printed in 1973 for members of the Sunnyvale Orthodox Presbyterian
Church and ministers of that denomination.
