296 CHRISTIANITY AND CIVILIZATION

insisted in obdurately breaking his feudal contract with the
colonies.

Even a cursory reading of the Declaration of Independ-
ence shows 27 specific points which the colonies claimed King
George TI broke in his feudal contract with them, thus
negating his right of rule. Just before listing these specific
points, the Declaration states:

The history of the present King of Great Britain is a history of
repeated injuries and usurpations, all having in direct object the
establishment of an absolute Tyranny over these States,

(2) The Constitutional Power of Impeachment (1787):

The giving to the House of Representatives the sole power
of impeachment, as the duly elected representatives of the
people, is a constitutional application of the biblical principle
of government interposition. (See Article I, Section 2 of the
Constitution of the United States of America.)

(3) The Kentucky and Virginia Resolutions (1798):

When the national Congress passed the Alien and Sedi-
tion Acts of 1798, many loyal Americans felt that Congress
had overstepped the powers that the States had so very
carefully delegated to Congress in the Constitution of 1787.
The State Legislatures of Kentucky and Virginia carefully
deliberated the issue and passed what are known as the Ken-
tucky and Virginia Resolutions:

(a) On November 10, 1798, the Kentucky Legislature
declared:

Resolved, that the several States composing the United States of
America, are not united on the principles of unlimited submis-
sion to their General Government; but that by compact under
the style and title of a Constitutton for the United States and
amendments thereto, they constituted a General Government for
special purposes, delegated to that Government certain definite
powers, reserving each State to itself, the residuary mass of right
to their own self-Government; and that wheresoever the General
Government assumes undelegated powers, its acts are
unauthoritative, void, and of no force: That to this compact each
State acceded as a State, and is an integral party, its co-States for-
ming as to itself, the other party: That the Government created
by this compact was not made the exclusive or final judge of the
extent of the powers delegated to itself, since that would have
made its discretion, and not the Constitution, the measure of its
