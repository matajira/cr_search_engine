236 CHRISTIANITY AND CIVILIZATION

Spain, accomplished by William the Silent and justified in his
1580 Apology, in the 1581 “Act of Abjuration” by the Estates
General of the Netherlands, and the 1579 Union of Urrecht.6
Both the Glorious Revolution and its Dutch counterpart were
in theory prescriptive and complained of the failure’ of a
prince to be a “shepherd” to and protector of his people, of his
“infringement of their laws and customs,” and of the necessity
of preserving their “lives and privileges.” Neither revolt was a
bellum servile~ a peasants’ revolt — but developed instead when
the aristocratic leaders of a society refused to practice “slavish
compliance” and instead proceeded in the defense of their
country to “the choice of another prince.” James II had cre-
ated courts out of his prerogative and treated other com-
ponents of the English constitutional settlernent of 1660 as if
they existed only by his pleasure —as if his authority as prince
and the lesser authorities beneath him (Lord Lieutenants,
sheriffs, Judges, Members of Commons and the House of
Lords) did not rest on a common ground. Scots found the
same paradigm in their 1580 restraining of young James VI,
in the struggles of John Knox with Queen Mary, and in the
Bishop's War of 1639.7 And many Americans knew the
history of Scotland and the political thought of that kingdom
better than they knew John Locke—especially the ten
Princeton (College of New Jersey) graduates among the 55
Framers who had studied under Dr. John Witherspoon, and
three others wha were cducated in Scotland itself.#

There is, to be sure, no point in denying that there were
latter-day Puritans in Massachusetts, Connecticut, and New
Hampshire who saw in resistance to the Boston Port Bill and
other Coercive Acts, or to the Stamp Act (and the Quebec
Act) a re-enactment of the Good Old Cause that brought
Cromwell's Protectorate and the Commonwealth. But even in
New England those that hungered and thirsted after a New
Zion were exceptions. And to the south of Hartford and New
Haven most Americans went with hesitation or reluctance up

6. A fine discussion of this part of Dutch history is C. V. Wedgwood,
William the Silent: William of Nassau, Prince of Orange, 1533-1584 (New York:
W. W. Norton & Co., 1968).

7. Scotland's view of rightful revolution had a powerful expression in the
Glasgow Assembly of June-December, 1638. See C. V. Wedgwood, The
King’s Peace, 1637-164 (Glasgow: William Collins & Co., 1955).

8. President John Witherspoon and two College of New Jersey graduates
signed the Declaration of Independence. But there were many other
Calvinists (or men exposed to Calvinist thought) wha belonged to the same
special company.
