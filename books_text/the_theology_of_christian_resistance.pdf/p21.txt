EDITOR'S INTRODUCTION xxl

next issue will deal with the tactics of resistance within an
overall strategic framework.

The Theology of Resistance

Christianity and Civilization draws on authors from
numerous denominational backgrounds, not to mention
religious traditions. We have found that many conclusions of
people who do not share our opinion of the Puritan and
Reformed tradition are often in agreement with the conclu-
sions of that tradition. At the same time, there are some
writers who are explicitly Reformed in perspective, yet whose
conclusions at various points do not seern to be consistent with
their theology. We offer these essays with a disclaimer: they
have been published in good faith by all parties, but neither
the editors nor the authors agree on all points with each of the
essays in this issue.

Section 1 presents evidence that we are facing a real crisis
in the United States today. The question of resistance to
tyranny is nol simply hypothetical. Lawyer John Whitehead
discusses the rivalry between statist religion and biblical
religion. The Bible authorizes resistance against an unlawful,
self-deifying state when that state transgresses biblical law.
Romans 13 has been misinterpreted by many commentators
to mean that we must obey Caesar, whatever he says. He then
discusses Samuel Rutherford’s classic work, Lex, Rex (1644),
in which Rutherford outlined levels of resistance that Chris-
tians can use against the state. Rutherford denied the legiti-
macy of the doctrine of the “divine right of kings,” which
taught that kings are accountable to none but God. This is
why he was placed under house arrest. We can use his
theology to deny the divine right of all civil governments.

Francis Schaeffer argues that the implicit relativism of
the religion of humanism makes it a religion of arbitrary
power. By denying God, the humanists deny absolute law and
therefore the absolute worth of human life. Thus, abortion~
on-demand is a “right” on both sides of the Iron Curtain. He
is not a pacifist, he says, because he is a responsible man. He
has the obligation to resist the bully who is beating up a girl;
similarly, he has the right to resist tyrants who are beating up
whole populations. The time has come for serious Christians
to insist. that the government take steps to resist the expansion
