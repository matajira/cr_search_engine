THE DUTCH-AMERICAN GUERRILLAS 259

foreigners in the American army.) The militia was active in
pursuing these men as they re-deserted and tried to make
their way to the British lines in New York.3+

The “middle ground” was thus the locale for an incredible
number of different levels of fighting during the war. It was
near here that the most serious mutiny, that of the Pennsylva-
nia Line, took place late in 1780. The plight of these men dur-
ing the war, many of them foreign-born, was no doubt severe,
their having received little or no pay for months, Some were
deserting, but a larger number simply were tired of fighting
without pay, and went on a rampage of plundering. Major
Goetschius reported to Washington that “the wicked and in-
considerate soldiery” were “entirely destroying the Schraalen-
burgh neighborhood,” having taken all sorts of farm animals
and produce, “and in a violent manner abuse the well-affected
in this place, running about with clubs and bayonets upon
pikes by whole companies as bad as our enemies ever have
done.” General Nathanael Greene wrote, “There have been
committed some of the most horrid acts of plunder by some of
the Pennsylvania Line that has disgraced the American army
during the war... . Two soldiers were taken that were out
upon the business, both of which fired upon the inhabitants to
prevent their giving intelligence. A party plundered a house
yesterday in sight of a number of officers, and even threatened
the officers if they offered to interfere.” Greene recommended
that such offenders be hanged without trial, while Goetschius
and the militia sought to halt any deserters from reaching
New York.35

Washington also found the plundering outrageous. “With-
out a speedy change in circumstances... cither the army
must disband, or what is if possible worse, subsist upon the
plunder of the people.” The army had at this point been with-
out any meat for over a week, and foraging raids had raised
only a supply for several days, “Military coercion is no longer
to any avail, as nothing further can possibly be collected from
the country in which we are obliged to take a position without
depriving the inhabitants of the last morsal. This mode of
subsisting, supposing the desired end could be answered by it,
besides being in the highest degree distressing to individuals,

 

 

34, p. 265.
35, p. 277.
