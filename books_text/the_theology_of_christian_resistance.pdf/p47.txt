CHRISTIAN RESISTANCE 7

In Acts 17 the Christians were accused of criminal acts. In
Acts 17:6 we read that they had “turned the world upside
down.” In other words, they chalienged the basic assumptions
of the pagan culture, But what was their crime?. Acts 17:7 tells
us: “These all do contrary to the decrees of Caesar, saying
that there is another king, one Jesus.” This was an act of
political treason.

A popular myth invoked by Christians and non-Christians
alike to justify their refusal to stand against immoral state acts
has been the assertion that Jesus and the apostles were
pacifists. This is not true. The question of pacifism did not
arise, but Jesus was certainly no quietist. Jesus Himself felt
free to criticize not only the Jewish civil leaders (Jo. 18:23),
but also the Roman-appointed ruler Herod Antipas in refer-
ring to him as a “fox” (Lk. 13:32). Jesus whipped the money
changers and chased them out of the temple (Jo. 2:13-17).
Following that, Christ would not allow any person to carry
“any vessel through the temple” (Mk. 11:16). Christ’s act of
whipping the money changers and blocking the entrance to
the temple were crimes. (Ultimately Christ is portrayed in the
Book of Revelation as exercising righteous vengeance on the
secular humanistic state.)

Paul likewise accused one of the members of a grand jury,
who commanded him to be hit on the mouth, of being a
“whitewashed wall,” although he apologized when he learned
that the man who issued the order was the high priest (Acts
23:1-5). And it was Paul who, when confronted with what he
considered to be illegitimate state acts, said, “I appeal unto
Caesar” (Acts 25:11). Moreover, we must not forget that
many of Paul's epistles in the New Testament were written
from jail cells. Certainly he wasn’t in prison for being a model
citizen. He was in jail because he was considered a perpe-
trator of civil disobedience. It was for doing what was con-
sidered illegal in the eyes of state officials.

Peter’s resistance in Acts 5 is a classic example of standing
for the faith against the illegitimate acts of the state. Peter and
others were thrown in jail for preaching. God himself defied
the local authorities, and an angel opened the doors of the
prison, freeing them. This was highly illegal. God, however, iden-
lifted with the men who defied the state, God himself took Peter out
of the prison. He then instructed the apostles to go and stand
in the temple and preach. Again, this was in contradiction to
