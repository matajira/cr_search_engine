THE EARLY CHURGH AND PACIFISM 163

who would obey imperial law only insofar as he deemed it
good law, to Eusebius, who slavishly hailed all imperial edicts
as divine (pp. 30-37). Christians also displayed their ambiguity
toward military obligation in another way, that of language.
Harly Christianity easily fell into the habit of using military
metaphors, and the prime example is the adaptation of the
term “sacrament,” which referred at first to a soldier’s “iden-
tity plate” but which Christians transformed into a term to fit
baptism and the Lord’s Supper, which also “identify” the
Christian (p. 69). The term “pagan” likewise originally meant
merely “civilian,” but as Christians increasingly came to
speak of themselves as milites Christi, the natural contrast one
had with the unbeliever was to identify him as a paganos. And
yet, Christians used military metaphors strictly as metaphors,
and frequently used them in a spiritualized sense so as to
point up a contrast with literal, carnal warfare (pp. 79, 90).
The most powerful ambiguity Christians dealt with, however,
was in their concept of God. The early Christians, claims
Hornus, were not Marcionites. They did not try to explain
away the fact that the God of Israel was a God of battles, and
they fully expected that God would violently overthrow the
Roman Antichrist as well. But at the same time, the early
Christians saw no immediate role for themselves in that
cataclysm. The Christian was “a witness, not a warrior. He
accepted the wrath of God upon himself; he discerned it in the
history of others. He warned and admonished. He did not
himself administer the punishment” (p. 65).

The point for Hornus, in introducing these sets of
ambiguities, is not to prove that the early Christians were nec-
essarily pacifists, but to establish that at the least there was no
uniform consensus in the early church against conscientious
objection. From establishing this possibility, Hornus moves in
Chapter Three toward suggesting the likelihood of such an at-
titude, primarily in two ways. First, declares Hornus, early
Christians cultivated an attitude of detachment toward
earthly loyalties (pp. 91-106), regarding the “present system”
as “fallen and sinful’ and therefore “completely unin-
teresting.” “One could only let this mad planet go its own
way and desire personally to leave it as soon as possible.”
Second, early Christians regarded human life as “sacred” and
revolted, not only against abortion, but against (and here he
quotes Tertullian, Chrysostom, and Lactantius) execution of
