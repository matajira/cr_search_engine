JOHN GALVIN’S THEOLOGY OF RESISTANCE 215

It is important to understand that this representative body
that Calvin has in mind does not exercise its authority “in
behalf of the people,” but rather they are so commissioned by
God as His lawfully ordained ministers. “What Calvin seems
to have in mind is not a democratic, but rather a judicial ap-
proach to the question of resistance in which the laws and not
the people are supreme and the Estates are thus not so much
representatives as guardians of the people and of their
rights.”!#6

Summary of Calvin's Position

Calvin's position is based primarily upon legal and judicial
argumentation. In Qalvin’s opinion, the grounds for
resistance are not dictated by narrowly conceived theological
or religious considerations, but are based in the legal situation
as it actually exists in a particular country. Legitimate
resistance is not done in the name of some higher order or
principle against the established political order, but it is car-
ried out as part of the existing order. i

Calvin sees no place for the private citizen actively to
resist the duly ordained authorities. Rather he calls upon
those officials whose constitutional power compels them to
protect the established order against a usurper or tyrant. It is
the duty of these officials to resist lawfully, using resistance to
bring order back to the country, and to restore the nation to
the constitutional status qua.

Contemporary Applications

We who believe in the sovereignty of God and in His all-
ruling providence recognize that there is much to learn from
history. History is the unfolding of God's decree, and it is
presumptuous for us to cut off our “Hall of Herces” with the
closing of Hebrews 11. By God's standards, John Calvin is a
hero of the faith, and with regard to Christian resistance, he
should be listened to.

To begin with, Calvin’s division of the agents of resistance
into private citizens and magistrates is important. In 20th
century America, with the radical egalitarianism that has

326. Jungen, p. 100.
