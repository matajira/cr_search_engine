STRUGGLE FOR THE HEART OF THE DUTCH 323

Christian background highly appreciated their independence
and specific character.

The Germans had of course their own, anti-Christian
strategy ready for these schools. They wanted a uniform
school-type for all Dutch children, completely dependent on
the State, not only for its funding, but also for its teaching
program. This school should have a National Socialist
character! The entire young generation was planned to be
subjected to the Nazi brainwashing, to be prepared for total
fraternization with the German state and Party. In the begin-
ning, this goal was somewhat camouflaged. The intention was
to pursue this goal as stealthily as possible.

A German, Dr. Heinrich Schwarz, was appointed as the
Head of the Department of Education, Arts, and Sciences.
His Secretary General —ithe main executive of the plans— was
a Dutchman, Dr. J. van Dam. He had already acted on
behalf of the German authorities as the chairman of a com-
mittee for censoring the books of public libraries. Also, he
established the first black list of books, forbidden for use in
public schools. This committee prohibited the use of 400
books and “improved” thousands more by deleting or chang-
ing particular paragraphs.

During the first year of the occupation, Seyss-Inquart
tried— mostly rather softhanded—to obtain the support of
schools and school teachers for his policies. When he obvi-
ously failed to be successful, he changed to the tough line dur-
ing the second year. The first signal for this change was his
address to the nation on March 12, 1941, when he said,
among other things: “Holland is a Germanic outpost. The
high inner values possessed by the Dutch people, are wel-
comed by the Germans as the characteristic of their Germanic
blood. Presently, Holland is put before the choice: with us or
against us! A third alternative does not exist. The occupying
power is fully determined to realize her goals. We stand on
this soil with a historic duty, given to us by Adolf Hitler him-
self. And we are truly filled with a religious zeal. Here we
stand, lined up. Heil dem Fuhrer!”

A short time later, the Department annexed the right to
appoint school teachers, also at private schools. Their school
boards were only entitled to propose certain candidates for ap-
pointment. The Department decided whether the proposed
person would be appointed.
