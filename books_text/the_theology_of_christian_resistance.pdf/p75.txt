WHAT THE WAR IS REALLY ABOUT 35

the church were just another profit-making business. So how
much should a pastor be paid? “The test of unreasonable
compensation here would be similar to that for determining
deductibility of salaries paid by a business corporation. . . .”
Dumbauld says that “10% of church’s gross income” would
be unreasonable. The average preacher in the United States
is paid $10,000. If a church paid its pastor only half of
that—$5,000—it could lose its tax-exemption, according to
Dumbauld, if its “net earnings” were $50,000. The wishes of
the members don’t count.

He also speaks of scrutiny of the activities of the organiza-~
tion in toto, including correspondence and publications, to
determine whether the Church is attempting to influence
legislation to a substantial extent. This is a not so veiled threat
to the Ghurch to shut up about such issues as abortion.

All of this is difficult to believe, even if you read it for
yourself, which your obedient servant recommends you do. If
Dumbauld did a comparable job in any other line of work, he
would now be in court himself, as a defendant, or in jail; but
federal judges, about whom the Founding Fathers warned us,
now have more power than the President. They are absolute
monarchs.

Day Of Judgment

The Church appealed to the U.S. Supreme Court. In his
brief, Church counsel Joseph Weigel told the Supreme Court
this: “. . . To allow the Seventh Circuit decision to stand
would be to provide that the ILR.S. is free anytime and
anywhere to demand of any church the production of every
written document relating to any aspect of church belief or ac-
tivity, including membership and contributor lists. This is a
monstrous and unprecedented invitation to tyranny which in-
vites blatant and wholly baseless fishing expeditions of the
most comprehensive kind violating every notion of religious
liberty as well as the most fundamental rights of privacy and
association. It lays out a red carpet for the I.R.S. for system-
atic and mendacious harassment that King George IL at his
most villainous could only have viewed with envy, and that
Adolph Hitler did not achieve until his dictatorship was
totally secure. It grants Big Brother an unrestricted license to
pry into and profane what are, by very definition, the most
