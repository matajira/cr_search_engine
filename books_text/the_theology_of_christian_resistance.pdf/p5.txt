TABLE OF CONTENTS

EDITOR'S INTRODUCTION
By Gary North. ... 0.0 o cee nee ee eee vii

PART I: THE CONFLICT OF CHRISTIANITY
WITH HUMANISM

CHRISTIAN RESISTANCE IN THE FACE OF
STATE INTERFERENCE
By John Wo Whitehead. 0.00.6 1

- CONFLICTING WORLD VIEWS: HUMANISM
VERSUS CHRISTIANITY
By Francis A. Schaeffer... cece cee eee 14

WHAT THE WAR IS REALLY ABOUT

  

 

By Alan SUQng. 0606 c cece cece cette cate e nent eeenns 24
PART II: PRINCIPLES OF CHRISTIAN RESISTANCE
CONFIRMATION, CONFRONTATION, AND CAVES

By Gary North. 00.000 c cect eee eee 40

" RAHAB’S JUSTIFIABLE LIE
By Jin Weslo. cc occ cece cc ccee cee ce eee eu cee eeeteteneaes 66

PACIFISM AND THE OLD TESTAMENT (an extended
review of four recent books)
By James B. Jordan... 6.0. ccc ec cc nee eee eee 75

NATURAL LAW AND CHRISTIAN RESISTANCE
TO TYRANNY
By Archie P Jones... 00.0 ccc ccc ec cece eee c eee tee eee ees 94

WHAT'S WRONG WITH HUMAN RIGHTS?
By To Robert Ingram. oo... ccc cece cee cee eveceeteene es 133

. THE COUNTERPRODUCTIVITY OF NOT LINKING
CHRISTIANITY AND POLITICS:
A REPLY TO SENATOR MARK HATFIELD
By Joseph C. Morecraft IID... 00.00. cc ccc vec ceeceseces 148

 

PART II: THE HISTORY OF CHRISTIAN RESISTANCE

THE EARLY CHURCH AND PACIFISM (an extended review
of Jean-Michel Hornus’s It is Not Lawful For Me To Fight)
By Allen C. Gueleo. 00.0.0. cc ccc cee ence cu enven een eves 161

(Continued on next page)
