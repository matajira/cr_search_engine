WHAT’S WRONG WITH HUMAN RIGHTS 147

and inherent in a universally known complex of law and
justice. The other is an imaginary idea of origins which im-
plies self-sufficiency as over and against creatureliness, and in
application leads only to confusion.

“Human rights” postulates conflict between individuals.
Common law rights are blessings of public peace in which
wrongs are punished. Wrongs are nol seen as infringements
of individual rights, but violations of God’s commands. It is
wrong to murder, not because each has a right to live, but
because God said it is wrong for any person to kill a man ex-
cept as a public official acting in the administration of justice
or the conduct of war.

The true import of the “rights” set forth in the Virginia
Declaration is much better and more convincingly expressed
in Article V of the so-called Bill of Rights amending the
United States Constitution. It says, “no person . . . (shall) be
deprived of life, liberty, or property, without due process of
law.” The meaning hangs on the word “law,” which in both
Christian and pagan thinking describes a fixed, universal
decree of right and wrong. The sensc, then, is essentially that
no man may be deprived of life, liberty or property except as a
just penalty for crime.
