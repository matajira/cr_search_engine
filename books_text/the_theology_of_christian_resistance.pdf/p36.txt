xxvi CHRISTIANITY AND GIVILIZATION

Thomas Jefferson, John Adams, and Benjamin Franklin,
perhaps the most prestigious trio of Americans ever to sit on a
committee. Even so, it took six years and two committees to
come up with a seal.

What did this trio of Unitarians propose? Some deistic
design? Some humanistic slogan? On the contrary, they
wanted something which would make use of biblical symbols.
Jefferson proposed a picture of the children of Israel in the
wilderness led by a pillar of fire by night and a cloud by day.
As far as we can tell, it was Franklin who proposed that the
seal contain a picture of Moses dividing the Red Sea and the
waters closing over Pharach’s chariot. He proposed a motto to
accompany the scene: “Rebellion to Tyrants is Obedience to
God.” Jefferson liked the motto. Almost certainly, it was
Jefferson who proposed it for the seal of the state of Virginia a
month later, along with another suggestion of his own, in-
dicating his high opinion of it.23 As it turned out, none of
these suggestions was adopted for the great seal of the U.S.
But the phrase is important for understanding the attitude of
the Founding Fathers toward their resistance against the
British King and Parliament. They saw themselves as doing
the work of God in resisting an illegitimate expansion of bu-
reaucratic control over their affairs.

The American Revolution provides concrete examples of
how Christians conducted a resistance movement within the
framework of a theory of constitutional government. Especially
remarkable was the case of Bergen County, New Jersey.
William Marina and Diane Cuervo offer a study of a guerilla
insurgency movement that was predominantly manned by.
Dutch Christians, The local militia battled successfully for
five years against the British military forces that were head-
quartered in New York City, defending the local region, par-
ticipating with the Continental Army on occasion, and mak-
ing life miserable for the Dutch Loyalist minority. “These
prosperous Dutch farmers were hardly radicals, but a few of
them were from the beginning quite militant in their defense
of American rights. Early in 1775 New Jersey was one of
those states that made the transition from Royal to revolu-
tionary government ‘without the firing of a gun, as the Pro-
vincial Congress replaced the Provincial Assembly. . . .” The

23. Monroe &. Deutch, “E Pluribus Unum,” The Classical Journal, XVII
(April, 1923), pp. 389, 396-98, 403.
