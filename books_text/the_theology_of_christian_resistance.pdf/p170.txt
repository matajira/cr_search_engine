130 CHRISTIANITY AND CIVILIZATION

if moral evaluations do not restrict our freedom to do as we
will, what good are they? Kovesi reduces morality to mere
positivistic academic description, to a speculative game for
idle intellectuals; but the Lord of heaven and earth sees
righteousness as that which exalts a nation, and sin as a
reproach to any people (Prov. 13:34).

Nor is Neo-Thomist Ralph MeInerney’s attempt to cir-
cumvent the “Is/Ought” Gap by arguing that men are in fact
engaged in the pursuit of a vast variety of ends or goods, that
human good is the group of hierarchically ordered values or
goods which perfect human nature, and that human reason
and will, through a sort of process of rationalistic trial and
error, can define and direct men to achieve these goods of any
help.® For the questions of the nature of human nature, the
nature of the good, and the standard for determining the
hierarchy of goods remain unsettled —not to mention the in-
soluble problems of knowing anything via autonomous
human rationality or autonomously interpreted experience—
and until these questions are answered it is obvious that one
cannot know what truly constitutes human perfection, how
ethically to choose among the vast variety of things that men
claim as “good,” or how to formulate a hierarchy of true
goods.

“Hume’s Gap” has not only withstood the assaults of these
would-be restorers of “Natural Law,” it has swallowed them
whole: they have fallen headlong into the deadly pit of moral
and intellectual relativism which lies at the bottom of that
autonomously impassible chasm.

“Natural Law” is dangerous for its consequences as well as
for its premises. Its assumption of the autonomy of man’s rea-
son leads, as Francis Schaeffer has pointed out, to the libera-
tion of fallen man’s claim to autonomy, to his denial of the
very God whom medieval Christian Natural Law theorists
worshipped, to the secularization of all of life, to the denial of
objective morality, and even to the prison of man’s Escape from
Reason.®! The consequences of the doctrine of an autonomous
“Natural Law” derivable from man’s autonomous reasoning
on the nature of nature and the nature of men and things in

80. “Naturalism and Thomistic Ethics,” The Thomist 40 (April, 1976):
222-242; summarized in Literature of Liberty, pp. 38-39.
81. (Downers Grove, THinois: Inter-Varsity Press, 1968).
