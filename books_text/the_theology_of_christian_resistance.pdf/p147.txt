CHRISTIAN RESISTANCE TO TYRANNY 407

of God’s laws was replaced by servitude under the arbitrary
and changing laws of kings, oligarchies, or majorities.

It was precisely against the theory and practice of such
revived pagan doctrines in church and state that Calvin,
Beza, Mornay, and their Reformed Christians reasserted tra-
ditional Biblical doctrines of resistance to tyranny, doctrines
which laid the theoretical and historical groundwork for the
Dutch and American Wars for Independence and Declara-
tions.32 Many of today’s Calvinists, infected with neo-platonic
thinking which tends to reduce their Christianity to pietism,
and burdened by the prevalent ignorance of Calvin's complete
teachings and practice in relation to civil government—
not to mention those Calvinists infected with autonomous
Natural Law notions overlaid with socialist doctrines? — deny
that John Calvin advocated resistance to tyrannical govern-
ment. In an important study, however, Christoph Jungen has
laid this myth to rest, at least intellectually:

. . . we cannot possibly doubt any Jonger that John Calvin him-
self had a clearly developed theory of legitimate political
resistance that went significantly beyond the brief formulation
given in the Jnséztuies, but at the same time was absolutely faithful
to the principles laid down there years before. Calvin did not
desperately try to prevent active resistance from being put into
effect, bui rather, once clear grounds for disobedience were pres-
ent, encouraged and even implored those who he thought were
legitimate agents of resistance.3+

32. Both the historical parallels of circumstance, preparatory theory, and
actual practice, and the theoretical and markedly legal, conservative, and
traditional justifications of the two wars for independence are unmistakable,
though the two events were separated by nearly two centuries. Moreover,
the literature, practice, and justification of the Dutch “Revolution” were im-
portant influences on the American “Revolution.” See Albert Hyma, Christi-
anity and Politics (Birmingham, Michigan: Brant Publishers, 1960), pp.
156-170, and The Journal of Christian Reconstruction TII:1 (Summer, 1976) for
more on the connection.

33. See Frederick Nymeyer's fascinating six year, six volume battle
against the socialist teachings in ostensibly Reformed colleges, in his First
Principles in Morality and Economics (formerly Progressive Calvinism) (South
Holland, Illinois: Libertarian Press, 1955-1960)

34. Christoph Jungen, “Calvin and the Origin of Political Resistance
Theory in the Calvinist Tradition” (Philadelphia: Unpublished Th.M.
Thesis, Westminster Theological Seminary, 1980), p. 186. Jungen’s study is
valuable both for its thesis and for its use of Calvin’s usually neglected
post-1559 letters.
