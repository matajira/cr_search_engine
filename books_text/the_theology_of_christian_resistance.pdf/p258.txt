DEFENSIVE WAR IN A JUST
CAUSE SINLESS (1775)*

David Jones

Aw I looked and rose up, and said unto the nobles, and to the
rulers, and to the rest-of the people, Be not ye afraid of them:
Remember the Lord, which is great and terrible, and fight for your
brethren, your sons and pour daughters, your wives and your houses.
Nehemiah 4:14.

When a people become voluntary slaves to sin; when it is
esteemed a reproach to reverence and serve God; when pro-
faneness and dissolute morals become fashionable; when
pride and luxury predominate, we cannot expect such a na-
tion to be long happy.

Israel, when first planted in the land of Canaan, were a
brave, heroic and virtuous people, being firmly attached to
the true worship of God. They were both formidable and in-
vincible: when their armies went forth to battle, thousands
and tens of thousands fell before them: thus being cloathed
with the majesty of virtue and true religion, a panic seized the
hearts of all their enemies around them. But when vice and
immorality became prevalent; when they forsook and rebelled
against their God, they lost their martial spirit, and were soon
enslaved by the king of Babylon. Yet, as God is gracious and
merciful, when seventy years were expired in this furnace of
affliction, he remembered their low estate, and stirred up
Cyrus, king of Persia, to proclaim liberty for the Jews to
return and build their temple at Jerusalem. Nevertheless
some of the people still remained in Persia, of which
Nehemiah was one. He was a favourite in the days of Artax-
erxes the king, therefore he obtained leave to go and build the

* A sermon preached on the Day of the Continental Fast, at Tredyfryn,
in Chester County, by the Rev. David Jones, A.M., Philadelphia, July 20,
1775.

218
