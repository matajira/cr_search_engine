200 GHRISTIANITY AND CIVILIZATION

off so that He may not reign over them.

The Two-Kingdom Doctrine

Before the specifics of Calvin’s theory of resistance can be
examined, one more foundational element in his teaching on
civil government must be touched upon: his two-kingdom
doctrine.®? It is true that for Calvin all of life is important, and
all areas alike must be brought under subjection to Christ.
But out of concern for that, the fact that he structured his
whole teaching on civil government on the distinction be-
tween the spiritual and the political kingdom must not fail to
be appreciated.®® He begins his chapter on civil government
with these words: “Now, since we have established above that
man is under a two-fold government, . . . this is the place to
say something also about the other kind, which pertains only to
the establishment of civil justice and outward morality, . . .

“First, before we enter into the matter itself, we must keep
in mind that distinction which we previously laid down so that
we do not (as commonly happens) unwisely mingle these two,
which have a completely different nature.”

As has already been noted, Calvin is very systematic with
regard to the arrangement of his teaching. It is, then, ex-
tremely important to understand the significance of these in-
troductory remarks, and to take ther into account in the im-
mediate context of his discussion of civil government, as weil
as the larger context of the Institutes as a whole.

In the last chapter of the 1536 edition of. the Institutes,
Calvin covered the subjects of Christian freedom, ecclesiasti-
cal power, and civil government. Before he dealt with each of
thern in turn, he introduced them with the distinction that
was going to be foundational for what he was going to say:
“There is a two-fold government in man: one aspect is
spiritual, whereby the conscience is instructed in piety and in
reverencing God; the second is political, whereby man is
educated for the duties of humanity and citizenship that must
be maintained among men. These are usually called the

66, Institutes, IV, xx, 7.

67. Jungen, p. 50.

68. In the following section, I am indebted to Christoph Jungen for his
helpful insights.

69. Institutes, IV, xx, 1.
