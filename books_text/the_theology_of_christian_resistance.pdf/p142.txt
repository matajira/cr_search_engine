192 CHRISTIANITY AND CIVILIZATION

Biblical Law

Biblical Law has its source in the eternal plan and
righteous will of the triune Creator and Sustainer of all things
(Gen. 1:1; Heb. 1:3; Acts 17:24-28), the Author and provi-
dential Determiner of history,?? who is the sovereign Victor
over the forces of evil in history (I Gor. 15:24, 25; Ps. 110:1;
Heb. 1:13), and the supreme and final Judge of the world and
men (Acts 17:30, 31). The basis of authority for Biblical Law
is precisely the existence and attributes of God, who is the
Sovereign Creator, Sustainer, Ruler, and Judge of man and
the universe, and who as the Unchanging (Malachi 3:6)
Lawgiver (Ex. 20; Deut. 6-8) has revealed His will to man in
ail of His infallibly inspired Holy Scripture (II Tim. 3:16).
God and His atiributes, not man and his attributes, God and His sov-
ereign unchanging word, not man and hts word of flux,*? is the Source
of Authority of Biblical Law.

Since the God of Scripture is an unchanging God, and a
covenantal God who in faithfulness “keepeth covenant and
mercy with them that love Him and keep His commandments
to a thousand generations and repayeth them that hate Him to
their face, to destroy them” (Deut. 7:9, 10; see also Deut. 28,
the passage upon which American presidents used to be sworn
into office), it is manifest that His law is unaffected by time and
place, and that it is still binding on men and societies. This is
confirmed in the New Testament, by the very authority of the
inspired Apostle Paul, who tells us that all of Scripture is both
inspired of God and profitable for doctrine and instruction in
righteousness, that the man of God may be thoroughly furnished
unto all good works (II Tim. 3:16, 17). How much more is this

22. See Gordon H. Clark, Biblical Predestination (Phillipsburg, N.J.:
Presbyterian & Reformed Publishing Co., 1979) and Predestination in the Old
Testament (Philipsburg: Presbyterian & Reformed, 1978), See also R. J.
Rushdoony, The Biblical Philosophy of History (Presb. & Reformed, 1969).

23. The word of would-be-autonomous man, be he ancient or modern,
must inevitably be a word of fux, since man on his own “autonomous”
—hence Godless— premises must confront a world of irrational chance and
flux, in which he can find no indissoluble Rock, no infallible and sure stand-
ard, upon which to ground his thought or guide his actions. See Cornelius
Van Til, 4 Survey of Christian Epistemology (Presbyterian & Reformed, 1977),
and R. J. Rushdoony, The Word of Flux: Modem Man and the Problem of Knowl
edge (Fairfax, Virginia: Thoburn Press, 1975).
