18 CHRISTIANITY AND CIVILIZATION

sivn of religion. Today it has been turned over by the human-
istic society, the American Civil Liberties Union, and so on,
and the First Amendment is made to say the very opposite:
that Christian values are not allowed to be brought into con-
tact with the governmental process.

The terror is, that in the last forty years, civil government
and especially the courts, has increasingly been the vehicle to
force this other world view on the total population. It is gov-
ernment that has done it by its laws and court rulings.

We find as we look at this country that we have now a largely
hurnanistic consensus, a humanistic culture, a humanistic so-
ciety, but happily we do not have yet a éotally humanistic cul-
ture or consensus. It isn’t total; we still have the Christian
memory, although it is moving with titanic speed. Now, this
being so, our calling is clear. It is that we who hold the Chris-
tian world view, we who love human Life, we who love hu-
manity, should stand first for the honor of God, but we should
also stand so that hurmanness will not be lost in all areas.

And then, we must say with sobriety that the United States
does not have an autonomous “manifest destiny.” Conse-
quently, if we continue to insist in walking down this road, at
some point, as God is God and not all things are the same to
God, we who have trampled so completely on all those armaz-
ing things that God has given us in this country, can we ex-
pect that God does not care? So we must not feel that we are
only playing intellectual and political games. If this God exists
and not all things are the same to Him, we must realize as we
read through the Scriptures that those who trample upon the
great gilts of God one day will know His judgment.

The first part of our topic was the secular humanistic world
view versus the Christian world view, and I have dealt largely
with our own country. The second half is “The Biblical
Perspectives on Military Preparedness.” This is related to
what I have just been speaking about; they are really not twa
divergent subjects at all. You must realize that the Soviet
Union and the Soviet bloc and all of the Marx-Engels-Lenin
Communism all of it also has as its base that the final reality
is only material or energy shaped by pure chance. This is the
central thing, The economics of Marxianism really is not that
which fs central. It is a dialectical materialism ~ you have heard
the term all your life, Iam sure. “Dialectical,” and then comes
