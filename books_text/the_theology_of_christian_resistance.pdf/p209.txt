THE EARLY CHURGH AND PACIFISM 169

more sincerely in the first century than in the twelfth, since St.
Paul’s reminder to the Corinthians that we all share the same
Spirit is applicable to time as well as geography; nevertheless,
many people believe that particular absurdity. The fact that
the Church has, for 1600 years, sanctioned the “just war”
rates, with me, far more heavily than the fact that it may have
not yet developed a “just war” thesis in the first three hun-
dred. But again, Hornus may have some other motives: he
frequently speaks of Roman Catholics as someday sharing
“the same ecclesiastical structures” with Protestants, in a
“faith which increasingly appears to be fundamentally the
same” (p. 239). Little wonder, then, that he should treat the
Protestant end of history so lightly and the “Catholic” end so
heavily; no wonder he should dismiss later witnesses in favor
of “tradition.” That, after all, smoothes the ecumenical
bridges back to Rome.

But enough has been said about Hornus’s ethics, and that
without even touching on the other, linked assumption of
Hornus’s pacifism, that Christians ought to withdraw them-
selves from the world, make no attempt to building righteous
societies, or develop patterns of Christian culture. As history,
Hornus’s book is exhaustively researched and his case—when
a strictly historical one —is skillfully put. But as for his call to
pacifism, that, I am tempted to say, is one that we'll have to
fight about.
