™

ON RECONSTRUCTION 307

still leaves an interesting story to be told about Missouri at the
county level.

Callaway County, a farming community in mid-Missouri
just north of the Missouri River, was strongly Southern in
sympathy. Its citizens took action at the county level when
Callaway was invaded by Union troops. The first military
skirmish took place on the morning of July 17, 1861, when
some 200 Callaway County citizens fired upon and turned
back several hundred Union troops who attempted to enter
the County from the direction of Jefferson City, which by now
was under Union control.

In October, 1861, another body of Union troops
approached the eastern border of Callaway County. Colonel
Jefferson F. Jones quickly moved to oppose the Union troops,
who were headed by General John B. Henderson. Here is
what happened:

Colonel Jones then moved his troops east and within a few miles
of the Montgomery line. General Henderson had meanwhile
halted his command a few miles east of the line, Each army could
plainly see the smoke from the campfires of the other. After a few
days of negotiation between Colonel Jones and General Hender-
son, a treaty was made and signed, the terms of which were that
General Henderson, purporting to speak for the United States of
America, agreed not to invade Callaway Gounty, and Colonel
Jones, acting for Callaway County, agreed not to invade the
United States of America. After this treaty, General Henderson
retired with his troops. Callaway County, having thus dealt as an
absolute equal with a sovereign power, became known as the
Kingdom of Callaway, a designation which it has proudly borne
and doubtless will for all time to come. The treaty between the
United States and Callaway County was signed on October 27,
which naturally is a national holiday in the Kingdom of
Callaway. '?

The Callaway County incident may seem like a very
unimportant event in the grander sweep of events in the
tragedy called the Civil War, but the principle involved is ex-
tremely important: the principle of local self-determination
and, therefore, the concomitant principle of governmental in-
terposition, These two principles are extremely important to
the liberty of a freedom-loving people.

John Calvin in his Institutes of the Christian Religion, Book

12. Hugh P. Williams, The Kingdom of Callaway, p. 33.

 
