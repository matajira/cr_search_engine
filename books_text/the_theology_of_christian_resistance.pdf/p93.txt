CONFIRMATION, CONFRONTATION, AND CAVES 533

and publicly repented, humbling himself before God (v. 27).
God pointed this out to Elijah, and told him that the great evil
would fall upon Ahab’s house in the days of his son (v. 29).
The confrontation again had led to the triumph of Elijah’s
word at the expense of the kingdom of Ahab. His wife did not
see her enemy's death. Elijah did not perish in this act of re-
bellion against a Godless king and his rebellious household.

The Remnant

God told Elijah that there were 7,000 others in Israel who
had not bowed to Baal (19:18). This was a remnant of the
faithful in a Jand of Baal worshippers who thought they were
only being realistic in adhering to the outward manifestations
of Baal worship. They had not been willing to announce their
adherence to Baal before the ritual confrontation on Mt.
Carmel. They had declared their adherence to God once they
had seen his power. They were worshippers of power. This made
them enemies of God, Only 7,000 were still faithful.

But what about Obadiah? He may have been a Baal wor-
shipper outwardly. If this seems strange, we must remember
that Elisha gave Naaman the Syrian permission to continue to
bow before Rimmon, provided his inward intention was to
worship the Lord God of Israel (II Kings 5:18f.). This is very
hard to understand, but whatever it means (and commen-
tators have wrestled with it for millennia), it happened in the
same historical context as the problem confronting Obadiah.
Neither Naaman nor Obadiah was a prophet; they were civil
servants, and this may play a part in understanding how they
might give token outward temporary service to a false god,
while actually serving the true one. This is not absolutely
clear. Shadrach, Meshach, and Abednego were also civil ser-
vants, and not prophets, yet they refused such token worship
(Daniel 3). On the other hand, they were not spies.

Obadiah may have been known as a worshipper of the
God of Israel, but regarded as a foolish old conservative, and
no threat. Jezebel recognized that the prophets were threats,
but Obadiah was a loyal civil servant. She did not think that
his faith counted for much, if he were willing to serve in her
kingdom. Of course, she didn’t know about the 100 prophets
Obadiah was feeding! Obadiah was such a good servant that
he had made himself indispensible to the crown. Ahab needed
