xxiv CHRISTIANITY AND CIVILIZATION

Soviet state (but not to own property), and the right of citizens
of non-Cormmunist authoritarian societies to revolt. The right
of rebellion is basic to all human rights codes, from the
American Revolution to the present. But what are the
standards of rebellion? The doctrine of human rights cannot
say. Thus, it is a doctrine favoring anarchy, and not Chris-
tian. It is God’s law, not a hypothetical universal human right
of rebellion, which is the biblical norm.

Joseph C. Morecraft ITI takes issue with the myth of
neutrality in politics. Specifically, he challenges the political
liberalistn of the neo-anabaptists, as represented by U.S. Sen,
Mark Hatfield and Stanley Mooneyham of World Vision, a
neo-evangelical parachurch missions organization, These
men have resisted the fusion of biblical law and Christian
political action, for such a fusion undermines the intellectual
foundation of the neo-anabaptists’ antinomian flirtation with
Federal power [and Federal aid programs—ed.], namely, the
myth of neutrality. Morecraft then offers examples of the
heritage of biblical law in American politics.

Section 3 takes up the history of resistance in the West.
First, the question of the idea of resistance in Western history.
One of the mast important documents for Protestants is John
Calvin’s final paragraphs in his Institutes of the Christian Religion
(1559 edition), Book IV, Chapter XX, Sections 23-32. Calvin
took the view that the private citizen does not have the right of
armed rebellion against a lawfully constituted monarch, but
lesser magistrates do have this right. Rebellion against the cen-
tral civil government must have the assent and co-operation of
the intermediate or lower civil government. Calvin was clearly
not an anarchist. Michael Gilstrap offers a study of Calvin's
position on the legitimacy of civil government and the grounds
of resistance, legitimate and illegitimate, by and through the
lesser magistrates, He analyzes Cialvin’s Institutes, his commen-
taries on the Bible, and his letters.

The American Revolution was a Christian counter-
revolution against an increasingly centralized and bureau-
cratic Brilish civil government. Parliament claimed absolute
sovereignty over the colonies, and the colonists resisted this
claim.20 The colonists also resisted British officials who

 

20. See the Symposium, “Christianity and the American Revolution,”
The Journal of Christian Reconstruction, INL (Summer, 1976).
