CHRISTIAN RESISTANCE 5

state must not destroy or subvert the good of society but protect
and promote it. Second, the civil government must deter crime
and bring to punishment those who foster evil in society.

The state’s function of protecting the good invariably con-
cerns the issue of the source of the state’s authority, Paul states
very clearly in Romans 13:1 that all government is ordained
and established by God. In the Bible, parents, pastors, civil
authorities, employers, and others are said to have received
their authority to govern from God, This authority, however,
is delegated authority. It is not to be exercised independent of
God’s Word. In fact, the Bible recognizes no power independ-
ent of God (Rom. 13:1). For any delegated sphere of author-
ity to speak of itself as a power independent from God is rebel-
lion against Him. For the courts, the Internal Revenue
Service, and other civil agencies to speak of their authority as
being over all areas of life and as being derived from the state, is
blasphemy.

In general, whenever the state is discussed in the New
Testament, the scope and limits of its authority are defined.
For example, in I Timothy 2:1-4 Paul writes:

T exhort therefore, that, first of all, supplications, prayers, interces-
sions, and giving of thanks, be made tor all men; For kings, and for
all chat are in authority; that we may icad a quiet and peaceable life
in all godliness and honesty. For this is good and acceptable in the
sight of God our Saviour; Who will have all men to be saved, and to
come unto the knowledge of the truth.

Paul makes it clear that as part of its task in protecting the
good, the state is to create an atmosphere where men can be
saved and come to the knowledge of the truth. Paul links this
idea to our prayers and our attitude toward the state. And if
the state limits the church in spreading the gospel, then Chris-
tians are not under a biblical duty to observe such restrictions
on liberty.
In I Peter 2:13-17 we read:

Submit yourselves to every ordinance of man for the Lord’s sake;
whether it be io the king, as supreme; Or unto governors, as unto
them that are sent by him for the punishment of evildoers, and for
the praise of them that do well. For so is the will of God, that with
well doing ye may put to silence the ignorance of foolish men: As
free, and not using your liberty for a cloke of maliciousness, but as
the servants of God. Honour all men, Love the brotherhood. Fear
God. Honour the king.
