128 CHRISTIANITY AND CIVILIZATION

ethically speaking, and hence, as a correlative, a common
ground of autonomous discovery of law between Christians
and pagans; both of these assumptions are unbiblical and the
first is destructive of the possibility of the existence of moral
and legal principles. ‘The strength of this motivation may be
discerned when we note that

even so eminent and devoted a Thomistic scholar as Professor
Vernon Bourke is inclined to give up on the use of the term “nat-
ural law” altogether. He feels that its usage has become almost
totally infected by that one use of the term, which dates back to
the Middie Ages, and which firmly associates the notion of nat-
ural law with “a code of moral precepts divinely implanted in
man’s nature, or mind, and issuing from the legislative will of
God.” Hence he thinks it is hopeless to try to restore to the term
the sense which it had in Aquinas, and according to which law is
defined as a “rational plan and rule of action.”75

There are few clearer assertions of the will to rationalistic au-
tonomy and antinomianism (and this by a selfproclaimed
Thomist!) inherent in such theories of “Natural Law,” but we
must constantly keep such forthright declarations of “Natural
Law” thinkers to be their own lawgivers in mind when
reading their lamentations of the decline of “Natural Law.”
The fourth defective premise of the revivalists of “Natural
Law” is their denial of nature as a God-created and God-
sustained entity. At least two approaches have been ad-
vocated. Friedrich and others adopt the evolutionary dogma,
and so conceive of nature as changing; from this it follows that
“Natural Law” must also be continually changing.” Thus, we
have a changing “justice” and man-created “law,””? both of
which are obviously relativistic and inadequate as moral or
legal standards. Veatch and others opt for a dualism in
human thought, which is ultimately reflective of a dualism in
nature, in order that we may have a teleological (ends-
oriented) ethics and a non-teleological natural science. Thus,
ethics, in typical Neo-Kantian fashion, would be concerned
with “nature-in-itself,” a realm of fore-conceived natural
ends, while natural science would be concerned only with
nature as it appears to be, not with “nature-in-itself,” and the

73. Veatch, p. 29, note 15.
76, Friedrich, pp. 179, 182, and passim,
77, Friedrich, p. 198.
