21:8 PART FIVE: THE SEVEN CHALICES

8 Any possibility of a universalistic interpretation is denied
by this grim verse. God Himself gives nine"! descriptions of the
finally impenitent and unredeemed—a summary accounting of
His enemies, the Dragon’s followers— who “shall not inherit the
Kingdom of God” {1 Cor. 6:9; cf. Gal. 5:21), but whose part will
be in the lake that burns with fire and brimstone, which is the
Second Death. Those condemned to final perdition are the cow-
ardly, in contrast to the godly conquerors; unbelieving, in con-
trast to those who have not denied the faith (cf. 2:13, 19; 13:10;
14:12); simmers, in contrast to the saints (ef. 5:8; 8:3-4; 11:18;
13:7, 10; 14:12; 18:20; 19:8}; abominable (cf, 17:4-5; 21:27; Matt.
24:15); murderers (cf. 13:15; 16:6; 17:6; 18:24); fornicators (cf.
2:14, 20-22; 9:21; 14:8; 17:2, 4-5; 18:3; 19:2); sorcerers (phar-
makoi), a word meaning “poisonous magicians or abortionists”
(ef. 9:21; 18:23; 22:15);'2 idolaters (cf. 2:14, 20; 9:20; 13:4,
12-15); and all liars (cf. 2:2; 3:9; 16:13; 19:20; 20:10; 21:27;
22:15). As Sweet points out, “the list belongs, like similar lists in
the epistles, to the context of baptism, the putting off of the ‘old
man’ and putting on of the new” (cf. Gal, 5:19-26; Eph.
4:17-5:7; Col. 3:5-10; Tit. 3:3-8).'3

 

  

The New Jerusalem (21:9-27)

9 And one of the seven angels who had the Seven Chalices full
of the seven last plagues came and spoke with me, saying:
Come here, I will show you the Bride, the Wife of the Lamb.

10 And he carried me away in the Spirit to a great and high
Mountain, and showed me the holy City, Jerusalem, coming
down out of heaven from God,

IL. Nine, that is, if the “Majority Text” reading of and sinners be accepted;
both the Textus Receptus and the so-called “critical text” (Nestle, etc.) omit
these words, leaving eight descriptions. According to some students of sym-
bolism, the number 9 is associated with judgment in the Bible, but the evi-
dence for chis seems slim and arbitrary; see E. W. Bullinger, Number in Scrip-
ture (Grand Rapids: Kregel Publications, [1894] 1967), pp. 235-42.

12. J. Massyngberde Ford, Revelation: Intraduction, Translation, and
Commentary (Garden City, NY: Doubleday and Co., 1975), p. 345. On the use
of pharmakeia and its cognates with reference to abortion in both pagan and
Christian writings, see Michael J. Gorman, Adortion and the Early Church:
Christian, Jewish, and Pagan Attitudes in the Greco-Roman World (Downers
Grove, IL; InterVarsity Press, 1982), p. 48.

13. J. P.M. Sweet, Revelation (Philadelphia: The Westminster Press, 1979),
p. 300.

550
