THE FAITHFUL WITNESS 10:11

destroyed. And while St. John could rejoice in the victory of the
Church over her enemies, it would still be a wrenching experi-
ence to see the once-holy city levelled to rubble, the Temple torn
down and burned to ashes, and hundreds of thousands of his
relativies and countrymen starved and tortured, murdered, or
sold into slavery. All the prophets experienced this same emo-
tional wrenching—which did not usually involve a rebellion
against their calling (Jonah is a notable exception), but rather a
deeply rooted recognition of the two-edged nature of prophecy,
of the fact that the same “Day of the Lord” would bring both
immeasurable blessing and unspeakable woe (cf. Amos 5:18-20).
It should be noted further, however, that a vast chasm separates
the prophets from many of their interpreters in our own day.
For while modern theologians will affect a weepy attitude over
the sufferings of “humanity” in general, or in the abstract, the
prophets suffered from no such humanitarian impulses.24 The
prophets grieved over the disobedient children of the Covenant.
The bitterness St. John will experience is not aver the fate of the
Roman Empire. He grieves for Israel, considered as the Cove-
nant people. They are about to be disinherited and executed,
never to be restored as the Covenant nation.** The divorce of old
Israel is necessary in God’s plan of redemption, and St. John
both welcomes it and proclaims it with vigorous joy. Yet there is
legitimate sorrow for the lost sheep of the house of Israel.

Il In the Old Testament background of the Boak of Revela-

21, For an incisive analysis of humanitarianism, see Herbert Schlossberg,
idols for Destruction: Christian Faith and ls Confrontation with American
Society (Nashville: Thomas Nelson Publishers, 1983}, pp. 39-87.

22, That Israel will someday repent and tum to Christ is, to me, indisput-
able (Rom. 11; cf. Chilton, Paradise Restored, pp. 125-31}. That is not at issue
here. The point remains, however, that in order to be restored to the Covenant,
Jews must join the Church of Jesus Christ along with everyone else. Israel will
never have @ covenantal identity distinct from the Church. For more in-depth
discussions of the place of israel in prophecy, see (in ascending levels of com-
plexity) Iain Murray, The Puritan Hope: Revival end the Interpretation of
Prophecy (Edinburgh: The Banner of Truth Trust, 1971}; John Murray, The
Epistle to the Romans, 2 vols. (Grand Rapids: William B. Eerdmans Publish-
ing Co., [1959, 1965] 1968), Vol. 2, pp. 65-108; Willem A, VanGemeren, “Israel
as the Hermeneutical Crux in the Interpretation of Prophecy" (I), Westminster
Theological Journal 45 (1983), pp. 132-44; idem, “Israel as the Hermeneutical
Crux in the Interpretation of Prophecy” (IT), Westminster Theolagicai Journal
46 (1984), pp. 254-297,

269
