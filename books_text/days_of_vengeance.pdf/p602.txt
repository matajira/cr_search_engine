2211-2 PART FIVE: THE SEVEN CHALICES

will happen in the dry?” St. Paul saw Christ’s crucifixion as the
fulfillment of the Old Testament curse on one who is hanged on
a tree (Gal. 3:13; cf. Deut. 21:23; Josh. 10:26-27).3 St. Irenaeus
saw the Cross as the Tree of Life, contrasting it with the Tree of
the Knowledge of Good and Evil, through which man fell: Jesus
Christ “has destroyed the handwriting of our debt, and fastened
it to the Cross [Col. 2:14]; so that, just as by means of a tree we
were made debtors to God, so also by means of a tree we may
obtain the remission of our debt.”4 The image was quickly
adopted in the symbolism of the early Church: “Early Christian
art indicates a close relationship between the tree of life and the
cross. The cross of Christ, the wood of suffering and death, is
for Christians a tree of life. In the tomb paintings of the 2nd
century it is thus depicted for the first time as the symbol of vic-
tory over death. It then recurs again and again. The idea that
the living trunk of the cross bears twigs and leaves is a common
motif in Christian antiquity.”5

As in Ezekiel’s vision (Ezek. 47:12), the Tree of Life is con-
tinuously productive, bearing twelve crops of fruit, yielding its
fruit every month in a never-ending supply of life for the over-
comers (2:7}, those who do His commandments (22:14). St.
John goes on to make it clear that the power of Christ’s Tree will
transform the whole world: The leaves of the Tree were for the
healing of the nations. Again, St. John does not conceive of this
as a blessing reserved only for eternity, although its effects con-
tinue into eternity. The Tree of Life is sustaining believers now,
as they partake of Christ:

Truly, truly, I say to you, he who hears My Word, and
believes in Him who sent Me, has eternal life, and does not come

3. The word cross (stauros) can refer either to the tree itself (considered as
the instrument of execution} or to the patibufuwn, (the upper crosspicce to
which Christ’s hands were nailed, and which was then nailed to the tree). Fora
discussion of this wholc issue, see Ernest L. Martin, The Place of Christ's Cru-
cifixion: Hs Discovery and Significance (Pasadena, CA: Foundation for Bibli-
cal Research, 1984), pp. 75-82.

4. St. Irenaeus, Against Heresies, v.xvii.3.

5. Johannes Schneider, in Gerhard Kittel and Gerhard Friedrich, eds.,
Theological Dictionary of the New Testament, 10 vols., trans. Geoffrey W,
Bromily (Grand Rapids: William B. Eerdmans Publishing Co., 1964-76), Vol.
5, pp. 40-41.

568
