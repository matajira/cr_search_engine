2318-20 PART TWO: THE SEVEN LETTERS

24 But I say to you, the rest who are in Thyatira, who do not
hold this teaching, who have not known the deep things of
Satan, as they call them—1 place no other burden on you.

25 Nevertheless what you have, hold fast until I come.

26 And he whe overcomes, and he who keeps My deeds until
the end, to him I will give authority over the nations.

27 And he shall rule them with a rod of iron; like the vessels of
a potter they shall be broken to pieces, as I also have received
from My Father.

28 And I will give him the morning star.

29 He who has an ear, let him hear what the Spirit says to the
churches.

18 One of the most significant things about the city of Thya-
tira was the dominance of trade guilds over the local economy.
Every imaginable manufacturing industry was strictly controlled
by the guilds: In order to work in a trade, you had to belong to
the appropriate guild. And to be a member of a guild meant also
to worship pagan gods; heathen worship was integrally con-
nected with the guilds, which held their meetings and common
meals in pagan temples. Two central aspects of the required
pagan worship were the eating of meat sacrificed to idols, and il-
licit sexual relations. Any Christian who worked in a craft or
trade was thus presented with severe problems: his faithfulness
to Christ would affect his calling, his livelihood, and his ability
to feed his family.

The local god, the guardian of the city, was Tyrimnos, the
son of Zeus; and Tyrimnas-worship was mixed in Thyatira with
the worship of Caesar, who was also proclaimed the incarnate
Son of God. The conflict of Christianity and paganism in Thya-
tira was immediate and central—and so the first word of Christ
to this church is the proclamation that He alone is the Son of
God (the only place in the Revelation where this specific desig-
nation of Christ is used). The letter to this church begins with an
uncompromising challenge to paganism and statism, affirming
the definitive, absolute uniqueness of Jesus Christ.

19-20 There was much that could be commended in the
church at Thyatira. It was active in love and faith and service
and perseverance—in fact, its activity was increasing: Your
deeds of late are greater than at first. But, in spite of all good

112
