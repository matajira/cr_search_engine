THE KING ON MOUNT ZION 14:4-5

16:24; Mark 9:38; 10:21, 28; Luke 9:23; John 8:12; 10:4-5, 27;
21:22). A precise statement of those who comprise this group,
however, is given in the next phrase: These have been purchased
from among men as first fruits to God and to the Lamb, The ex-
pression first fruits refers essentially to a sacrifice, the offering
up of the first harvest of the land to the Lord, claimed by Him
as His exclusive property (Ex. 22:29; 23:16, 19; Lev. 23:9-21;
Deut. 18:4-5; Neh. 10:35-37; Prov. 3:9-10); these Christians have
offered themselves up to God’s service for Christ’s sake. More
than this, though, the New Testament uses first fruits to describe
the Church of the Last Days, the “first-generation” Church
(Rom. 16:5; 1 Cor. 16:15), especially the faithful Remnant from
the twelve tribes of Israel (James 1:1, 18): “The confessors and
martyrs of the apostolic Church, who overcame by reason of
their testimony and the blood of the Lamb, are thus declared to
be a first fruits, a choice selection out of the innumerable com-
pany of saints. The purpose of this Apocalypse was to give spe-
cial encouragement to these virgin spirits.”5

The characteristics of this group are strikingly similar to
those of Israel when she first became God’s Bride:

I remember concerning you the fidelity of your youth,
The love of your betrothais,

Your following after Me in the wilderness,

Through a land not sown.

Israel was holy to the Lorp,

The first of His harvest... . Wer. 2:2-3; cf. v. 32)

Finally, St. John says, no lie was found in their mouth, for
they are blameless. It is tne Dragon who is the deceiver, the false
accuser, the father of the Lie John 8:44; Rev, 12:9); God’s people
are characterized by truthfulness (Eph. 4:24-27). As St. Paul de-
clared regarding the heathen, the basic Lie is idolatry: “Profess-
ing themselves to be wise, they became fools, and exchanged the
glory of the incorruptible God for an image in the form of cor-
ruptible man and of birds and four-footed animals and crawling
creatures. ... For they exchanged the Truth of God for the
Lie, and worshiped and served the creature rather than the

3. Terry, p. 404,
357
