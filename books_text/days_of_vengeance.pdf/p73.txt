THE CONTEMPORARY FOCUS OF REVELATION

ages in which all the parts fit together. If we honestly and care-
fully read the Bible theologically and with respect to the Bible’s
own literary structure, we will not go very far astray.®6

The Contemporary Focus of Revelation

The purpose of the Revelation was to reveal Christ as Lord
to a suffering Church. Because they were being persecuted, the
early Christians could be tempted to fear that the world was get-
ting out of hand—that Jesus, who had claimed “all authority
. .. in heaven and on earth” (Matt. 28:18), was not really in
control at ail. The apostles often warned against this man-
centered error, reminding the people that God’s sovereignty is
over all of history (including our particular tribulations). This
was the basis for some of the most beautiful passages of com-
fort in the New Testament (e.g. Rom. 8:28-39; 2 Cor. 1:3-7;
4:7-15).

St. John’s primary concern in writing the Book of Revelation
was just this very thing: to strengthen the Christian community in
the faith of Jesus Christ’s Lordship, to make them aware that
the persecutions they suffered were integrally involved in the
great war of history. The Lord of glory had ascended to His
throne, and the ungodly rulers were now resisting His authority
by persecuting His brethren. The suffering of Christians was ot
a sign that Jesus had abandoned this world to the devil; rather, it
revealed that He was King. If Jesus’ Lordship were historically
meaningless, the ungodly would have had no reason whatsoever
to trouble the Christians. But instead, they persecuted Jesus’
followers, showing their unwilling recognition of His supremacy
over their rule, The Book of Revelation presents Jesus seated on
a white horse as “King of kings and Lord of lords” (19:16), doing
battle with the nations, judging and making war in righteous-

86. For more on Biblical interpretation, see Gecrhardus Vos, Biblical
Theology: Old and New Testaments (Grand Rapids: Eerdmans, 1948); Mere-
dith G. Kline, Images of the Spirit (Grand Rapids: Bakcr Book House, 1980);
Vern S. Poythress. The Stained-Glass Kaleidoscope: Using Perspectives in
Theology (privately printed syllabus, Westminster Theological Seminary, Phil-
adelphia, 1985); Richard L. Pratt, Jr, “Pictures, Windows, and Mirrors in Old
Testament Exegesis,” Westminster Theological Journal 45 (1983), pp. 156-67.
James B. Jordan’s three lectures on “How to Interpret Prophecy” are an excel-
lent introduction to the understanding of Biblical symbolism. The three tapes
are available from Geneva Ministries, P.O. Box 131300, Tyler, TX 75713.

39
