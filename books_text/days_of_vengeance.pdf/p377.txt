LEVIATHAN AND BEHEMOTH 13:15-17

First we see Satan as the Dragon, the Leviathan; then comes the
Sea Beast, who is in the Dragon’s image; finally, trailing behind
and serving them, comes the Land Beast, in the image of the Sea
Beast, bringing along yet another Image of the Beast. By listing
the Beasts in reverse order, St. John underscores his point:
Israel, which was to have been a kingdom of priests to the na-
tions of the world, has surrendered her position of priority to
Leviathan and the Beast. Instead of placing a godly imprint
upon every culture and society, Israel has been remade into the
image of the pagan, antichristian State, becoming its prophet.
Abraham’s children have become the seed of the Serpent.

During three years of ministry in Ephesus, the Apostle Paul
continually suffered persecution because of “the plots of the
Jews” (Act 20:19); in describing his conflicts with them, he called
them “wild beasts” (1 Cor. 15:32), The Jewish Beast was the early
Church’s most deceptive and dangerous enemy. St. Paul stren-
uously warned the Church about Judaizers who propagated
“Jewish myths”: “They profess to know God, but by their deeds
they deny Him, being detestable and disobedient, and worthless
for any good deed” (Tit. 1:14, 16).

We are now in a position to attempt a more precise identitfi-
cation of the Image of the Beast, which is a continuation of the
Satanic counterfeit, the demonic reversal of God’s order. Just as
the Son of God is the [mage of the Father (John 1:18; Col. 1:15),
so the Church has been redemptively re-created as the Image of
the Son (Rom, 8:29; Eph, 4:24; Col. 3:10). The vision of the
prophetic, priestly, and dominical Church seen by St. John par-
allels that of the Lord Jesus Christ: Like her Lord, she is robed
in glorious light (cf. 1:13-16; 10:1; 12:1; 19:6-8; 21:9-22:5). Assist-
ing the Son in His work throughout Revelation are the Seven
Stars/Angels of the Presence (8:2), led by the Holy Spirit (the
Seven Spirits, connected with the angels in 3:1), The divine
order is thus:

Father

Son (Image of the Father)
Angels/Bishops

Church (Image of the Son)

The Satanic parody of this is:
343
