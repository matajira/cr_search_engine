THE DOMINION MANDATE 3:21-22

of Man and drink His blood, you have no life in yourselves. He
who eats My flesh and drinks My blood has eternal life, and I
will raise him up on the last day. For My flesh is true food, and
My blood is true drink. He who eats My flesh and drinks My
blood abides in Me, and J in Him. As the living Father sent Me,
and I live because of the Father, so he who eats Me, he also shall
live because of Me, (John 6:53-57)

21-22 The final promise to the overcomer is a promise of
dominion with Christ: I will grant to him to sit down with Me on
My Throne, as I also overcame and sat down with My Father on
His Throne. Is this only a future hope? Assuredly not. The privi-
lege of ruling with Christ belongs to all Christians, in time and
on earth, although the dominion is progressive through history
until the final consummation. But Christ has entered upon His
Kingdom already (Col. 1:13); He has disarmed Satan and the
demons already (Col. 2:15); and we are kings and priests with
Him already (Rev. 1:6); and just as He conquered, so we are to
go forth, conquering in His name. He reigns now (Acts
2:29-36), above all creation (Eph. 1:20-22), with all power in
heaven and in earth (Matt. 28:18-20), and is engaged now in put-
ting all enemies under His feet (1 Cor. 15:25), until His kingdom
becomes a great mountain, filling the whole earth (Dan. 2:35,
45).

We have thus been faced again and again in these messages
to the churches with the fundamental command of Revelation,
that which St. John admonished us to keep (1:3): Overcome!
Conquer! Even aside from the fact that the prophecy is not
about the twentieth century, we will miss its point if we concen-
trate on persecutions or emperor-worship in the same way that
the Hal Lindseys of this age concentrate on oil embargoes, com-
mon markets and hydrogen bombs: the basic message is about
none of these, but rather about the duty of the Church to con-
quer the world. R. J. Rushdoony has well said: “The purpose of
this vision is to give comfort and assurance of victory to the
Church, not to confirm their fears or the threats of the enemy. To
read Revelation as other than the triumph of the kingdom of God
in time and eternity is to deny the very essence of its meaning.”?3

23. Rousas John Rushdoony, Thy Kingdom Come: Studies in Daniel and
Revelation (Tyler, TX: Thoburn Press, [1970] 1978), p. 90.

139
