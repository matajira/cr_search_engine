THE THRONE ABOVE THE SEA 4:9-11

Him. . . and will worship Him. . . and will cast their crowns
before the Throne, acknowledging that their authority and do-
minion derive from Him. They go on to praise Him for His
works in creation and history: Worthy art Thou, our Lord and
God, to receive glory and honor and power; for Thou didst create
all things, and because of Thy will they existed, and were created.

To appreciate the full import of this forthright affirmation of
the doctrine of creation, let us contrast it with a statement
issued a few years ago by the officers of one of the largest
churches in the United States:

IN THE BEGINNING~ CHOICE

In the beginning God created choice. Before God made any-
thing—earth, sky, or man—he had already made up his mind
that man was to have a choice. Not limited choice like what color
sacks to wear today. God gave man complete power of selection,
so complete that man could choose—or reject—God. God
placed himself in a rather risky position when he armed man
with such a tool. He gave man a weapon to use against God.

Can you imagine something you've made saying, “I don’t
want you, not even for a friend.” God gave man that very op-
tion, even though he knew what man’s choice would be. God
knew that his creation would turn away from him, hate him, But
he also realized there is no better way to prove love than by risk-
ing the alternative of rejection. Genuine love requires decision,
because genuine love cannot be demanded, ordered, or even reg-
ulated. It must be voluntary.

This tells us something about God. God doesn't do things
just for kicks. He must have felt, in some sense, a need of being
loved. Do you think it is fair to conclude that God “needs” us? I
think so. But he never downgrades the caliber of his love by try-
ing to force us to love him. . . .3

Speaking charitably, this is blasphemous nonsense. The only
honest thing about it is its lack of Bible references. There are
many objectionable points we could consider, but the main one
for our purposes is the issue of God’s sovereignty and independ-
ence. Did God need to create? Is God lonely? Does He stand in
need of His creation? Let the Scriptures speak:

33. Leafiet published c. 1978 by a church in Santa Ana, California, advertis-
ing its Saturday Night Concerts.

161
