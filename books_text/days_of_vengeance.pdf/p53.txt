THE COVENANT LAWSUIT

well as double restitution (Rev. 18:6). (It is also worth repeating
what Ralph Alexander said about the word slaughter in Exodus
22:1: “The root is predominantly used metaphorically, portray-
ing the Lord’s judgment upon Israel and upon Babylon as a
slaughter.” As we will see, St. John brings these ideas together,
metaphorically calling the apostate Jerusalem of his day Baby-
ton the Great.) The Great Tribulation, culminating in the holo-
caust of a.p. 70, was the restitution demanded for its theft and
slaughter of the Old Testament prophets, of the New Testament
martyrs, and of the Lord Jesus Christ (Matt. 21:33-45; 23:29-38;
1 Thess. 2:14-16); and these motifs are built into the very struc-
ture of Revelation, the final Covenant Lawsuit.

All this is further emphasized by St. John’s use of the pro-
phetic Lawsuit terminology: the accusation of harlotry. Through-
out Scripture, Israel is regarded as God’s Wife; the covenant is a
marriage bond, and she is expected to be faithful to it. Her
apostasy from God is called adultery, and she is identified as a
harlot. There are numerous examples of this in the prophets:

How the faithful city has become a harlot,
She who was full of justice!
Righteousness once lodged in her,

But now murderers. (Isa. 1:21)

For long ago I broke your yoke

And tore off your bonds;

But you said: | will not serve!

For on every high hill

And under every green tree

You have lain down as a harlot. (Ser. 2:20)

Your fame went forth among the nations on account of your
beauty, for it was perfect because of My splendor which I
bestowed on you, declares the Lord Gop. But you trusted in
your beauty and played the harlot because of your fame, and
you poured out your harlotries on every passerby who might be
willing. (Ezek, 16:14-15)

Do not rejoice, O Israel, with exultation like the nations!
For you have played the harlot, forsaking your God.
You have loved harlots’ earnings on every threshing floor.
(Hos. 9:1)

19
