AUTHOR AND DATE

casionally in danger of overlooking them altogether; and in lan-
guage such as a professional man would employ, which would
come to him from the previous exercise of his calling. Indeed,
some of the most striking of these references could not have
been understood at all without the professional treatises of the
Rabbis on the Temple and its services. Only the studied minute-
ness of Rabbinical descriptions, derived from the tradition of
eye-witnesses, does not leave the same impression as the unstud-
ied illustrations of St. John.”*

“It seems highly improbable that a book so full of liturgical
allusions as the Book of Revelation—and these, many of them,
Not to great or important points, but to minutiae —could have
been written by any other than a priest, and one who had at one
time been in actual service in the Temple itself, and thus become
so intimately conversant with its details, that they came to him
naturally, as part of the imagery he employed."?

In this connection Edersheim brings up a point that is more
important for our interpretation than the issue of Revelation’s
human authorship (for ultimately [see 1:1] it is Jesus Christ’s
Revelation). St. John’s intimate acquaintance with the minute
details of Temple worship suggests that “the Book of Revelation
and the Fourth Gospel must have been written before the Tem-
ple services had actually ceased.”* Although some scholars have
uncritically accepted the statement of St. Irenaeus (4.p. 120-202}
that the prophecy appeared “toward the end of Domitian’s
reign” (i.e., around a.p. 96),° there is considerable room for
doubt about his precise meaning (he may have meant that the
Apostle John Aimself “was seen” by others).'!® The language of
St. Irenaeus is somewhat ambiguous; and, regardless of what he
was talking about, he could have been mistaken,"! (St. Irenaeus,
incidentally, is the onfy source for this late dating of Revelation;

6. Alfred Edersheim, The Temple: Its Ministry and Services as They Were
at the Time of Christ (Grand Rapids: William B. Eerdmans Publishing Co.,
1980), pp. 141f.

7. Ibid., p. 142.

8. Ibid., p. 141,

9. St. Irenaeus, Against Heresies, v.xxx.3; quoted by Eusebius in his Eecle-
siastical History, iti xvili.2-3; v.viii.6.

10, See Arthur Stapylton Barnes, Christianity at Rome in the Apostolic Age
(London: Methuen Publishers, 1938), pp. 167f.

11, See the discussion in John A. T. Robinson, Redating the New Testament
(Philadelphia: The Westminster Press, 1976), pp. 221ff.

3
