5:8-10 PART THREE: THE SEVEN SBALS

identified as among the redeemed, while the singers in the sec-
ond reading are not necessarily including themselves among
those purchased by Christ’s blood.

Unfortunately for Stonehouse’s interpretation, there are two
facts which, at the outset, argue against it. In the first place,
even if all the manuscripts contained Stonehouse’s preferred
reading, it would not prove his case; Stonehouse was simply
making an assumption that may (but does not necessarily} fol-
low from his premise. (After all, any believer could still pray for
“the Church” or “God’s people” without excluding himself; the
mere fact that the elders thank God for redeeming “men” would
not necessarily mean that they are not redeemed themselves.)

Secondly, however, of the hundreds of manuscripts contain-
ing the Book of Revelation, only one carries this extremely dub-
ious reading. The variant is not found in any “family” of manu-
scripts, and certainly not in anything that could be called a man-
uscript “tradition”; it occurs in only one solitary manuscript. To
base an interpretation on such a shaky foundation is, to say the
least, an exceedingly subjective and precarious method of Bible
study,

Without a doubt, the traditional reading (“us”) is the true
one. But saying this seems to raise two further problems: (1) The
four living creatures, who do not seem to represent the Church,
are said to be singing this song; (2) the song shifts to the third
person between verses 9 and 10. In verse 9 we read: “Thou didst
purchase us”; and in verse 10 we read: “Thou hast made them to
be kings . . . and they will reign.” Actually, these two problems
solve each other. It is apparently an example of what we have
already seen in this book, and what will become more familiar
as we progress through it: antiphonal praise. This pattern of
choral responses continues in this chapter (cf. v. 11-14). A proba-
ble outline of this portion of the heavenly liturgy would be as
follows:

Elders and Living Creatures: Worthy art Thou to take the
Book and to break its seals.

Elders: For Thou wast slain, and didst purchase us for God
with Thy blood out of every tribe and tongue and people and na-
tion.

Living Creatures: And Thou hast made them to be kings and
178
