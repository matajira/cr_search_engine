SEVEN LAST PLAGUES 15:6-7

oath (Ex. 16:34; 25:16, 21-22; 31:18; 32:15; cf. Ps. 19:7; Isa. 8:16,
20). The Tabernacle, in which the Testimony was kept, was
therefore called the Tabernacle of the Testimony (Ex. 38:21;
Num. 1:50, 53; 9:15; 10:11; Acts 7:44). As we have seen, in Reve-
lation the Temple (Greek naos) is the Sanctuary, or Holy Place
(cf. 3:12; 7:15; 1121-2, 19; 14:15, 17).

A major aspect of St. John’s message in Revelation is the
coming of the New Covenant. In his theology (as in the rest of
the New Testament), the Church is the mgos, the Temple. The
writer to the Hebrews shows that the Mosaic Tabernacle was
both a copy of the heavenly Original and a foreshadowing of the
Church in the New Covenant (Heb. 8:5; 10:1); St. John draws
the conclusion, showing that these two, the heavenly Pattern
and the final form, coalesce in the New Covenant age: The
Church tabernacles in heaven. And, if the Temple is the Church,
the Testimony is the New Covenant, the Testimony of Jesus (1:2,
9; 6:95 12:11, 17; 19:10; 20:4).

6-7 The seven angels who had the seven plagues came out of
the Temple, in order to apply the Curses proclaimed by the
Trumpets. As priests of the New Covenant, these angel-ministers
are clothed in linen, clean and bright, and girded around their
breasts with golden girdles, in the image and likeness of their
Lord (1:13; cf. Ex. 28:26-29, 39-43; Lev. 16:4).

And one of the four living creatures gave to the seven angels
seven golden Chalices; presumably, this cherub is the one with
the man’s face (4:7), since the other three have already appeared
on the stage of the drama, and since St. John is proceeding sys-
tematically through the quarters of the Zodiac. We saw that he
began in the Spring (Easter), with the sign of Taurus governing
the Preamble and the Seven Letters; moved through Summer
with Leo ruling the Seven Seals; continued through Autumn
under Scorpio (the Eagle/Scorpion) and the Seven Trumpets;
and now he arrives in Winter, with Aquarius, the Waterer,
supervising the outpouring of the wrath of God from the Seven
Chalices.

I have called these seven containers Chalices (rather than
vials [KTV] or bowls [NASV}) to emphasize their character as a
“negative sacrament.” From one perspective, the substance in
the Chalices (God’s wrath, which is “hot,” cf. 14:10) seems to be

389
