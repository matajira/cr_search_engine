THE MILLENNIUM AND THE JUDGMENT 20:7-8

and rule with Christ now, in this age. The First Resurrection is
taking place now, Jesus Christ is reigning now (Acts 2:29-36;
Rev. 1:5). And this means, of necessity, that the Millennium is
taking place now as well.

The Last Battle (20:7-10)

7 And when the thousand years are compieted, Satan will be
released from his prison,

8 and will come out to deceive the nations which are in the
four corners of the earth, Gog and Magog, to gather them
together for the War; the number of them is like the sand of
the sea.

9 And they came up on the breadth of the earth and surrounded
the camp of the saints and the beloved City, and fire came
down from heaven and devoured them.

10 And the devil who deceived them was thrown into the Lake
of fire and brimstone, where the Beast and the False Prophet
are; there they will be tormented day and night forever and
ever.

7-8 At last the thousand years are completed, and God's
timetable is ready for the final defeat of the Dragon. According
to God’s sovereign purpose, the devil is released from his prison
in order to deceive the nations. Biblical postmillennialism is not
an absolute universalism; nor does it teach that at some future
point in history absolutely everyone living will be converted.
Ezekiel’s prophecy of the River of Life suggests that some out-
lying areas of the world—the “swamps” and “marshes” —will
not be healed, but will be “given over to salt,” remaining unre-
newed by the living waters (Ezek. 47:11). To change the image:
Although the Christian “wheat” will be dominant in world cul-
ture, both the wheat and the tares will grow together until the
harvest at the end of the world (Matt. 13:37-43). At that point,
as the potential of both groups comes to maturity, as each side
becomes fully self-conscious in its determination to obey or
Tebel, there will be a final conflict. The Dragon will be released
for a short time, to deceive the nations in his last-ditch attempt
to overthrow the Kingdom.

We noted at verse 3 that the specific purpose of Satan’s de-
ception of the nations is to gather them together for the War.
This had been at least one of Satan’s goals from the beginning:
to provoke the final war between God and His rebellious crea-

519
