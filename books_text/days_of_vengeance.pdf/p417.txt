15

SEVEN LAST PLAGUES

The Song of Victory (1511-4)

1 And I saw another sign in heaven, great and marvelous,
seven angels who had seven plagues, which are the last,
because in them the wrath of God is finished.

2 And i saw, as it were, a Sea of glass mixed with fire, and
those who had come off victorious from the Beast and from
his image and from the number of his name, standing on the
Sea of glass, holding harps of God.

3 And they sing the song of Moses the bond-servant of God
and the song of the Lamb, saying:

Great and marvelous are Thy works,
© Lord God, the Almighty;
Righteous and true are Thy ways,
Thou King of the nations.
4 Who will not fear Thee, O Lord, and glorify Thy name?
For Thou alone art holy;
For all the nations will come and worship before Thee,
For Thy righteous acts have been revealed.

1. St. John now tells us of another sign in heaven, great and
marvelous. Twice before he has shown us a great sign in heaven:
the Woman clothed with the sun (12:1), and the great red
Dragon (12:3). As Farrer says, it is “as though everything in
12-14 had been the working out of that mighty conflict, and the
next act were now to begin.”! This new sign initiates the climax
of the book: seven plagues, which are the last, because in them
the wrath of God is finished. There is no reason to assume that
these must be the “last” plagues in an ultimate, absolute, and

1. Austin Farrer, The Revelation of St. John the Divine (Oxford: At the
Clarendon Press, 1964), p. 169.

383
