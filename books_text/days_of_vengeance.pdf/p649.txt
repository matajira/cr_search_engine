CHRISTIAN ZIONISM AND MESSIANIC JUDAISM

fact, and has written a book, Jerry Falwell and the Jews.5 This book is
a series of interviews with Rev. Falwell, designed to present him as a
friend of Zionism, and to alleviate suspicions that liberal Zionist Jews
naturally have when it comes to a supposedly orthodox, fundamental
Christian preacher.

T should like to cite some quotations from this book, and make
some appropriate comments. The books says, however, “No part of
this book may be reproduced in any manner without prior written
consent from the publishers,” which rather cramps my style. You'll
just have to believe me, as I summarize Falwell’s comments. You can
always go to your local library and look it up for yourself.

On page 13, Falwell is asked if he considers the destruction of Jeru-
salem in a.D. 70 as a sign of God’s rejection of Israel. Falwell answers
by saying that he surely does not believe a “vengeful” God brought the
Roman army to Jerusalem to destroy the Jews. Falwell ascribes the
event rather to anti-Semitism.

Now let’s hear what the Bible says about it. We needn't quote Lev-
iticus 26 and Deuteronomy 28 in their entirety. Read them at your lei-
sure, and ask this question: Do we see an angry, “vengeful” God here
threatening to bring horrors upon Israel if they apostatize? Also read
Psalm 69:21 and ask Whom this refers to, and then continue reading
until the end of the Psalm, remembering that the Romans surrounded
Jerusalem at Passover time. Notice Psalm 69:25 speaks of the “desola-
tion” of Jerusalem, and consider that in connection with Jesus’ pro-
nouncement of the desolation of Jerusalem in Matthew 23:38. Falwell
is completely out of line with Scripture on this point.

On page 25, Falwell says that he believes anti-Semitism is inspired
exclusively by Satan, as part of his opposition to God. Against this,
read Job chapters | and 2. Here we find that Satan is never allowed to
do anything without God’s permission. Moreover, we find from the
rest of the Bible that God frequently raises up enemies against His peo-
ple, as scourges to punish them. Read the Book of Judges. Read Kings
and Chronicles about Assyria and Babylon. Read Habakkuk. This is
not some minor point tucked away in some obscure passage. Rather,
this truth pervades the entire Scriptures,

It is true that anti-Jewish feelings are not part of the Christian mes-
sage, and that Christians should be as considerate toward Jews as they
are toward all other men. It is also true, however, that it is God Who
stirs up the Babylonians and Assyrians. Until the Jews repent and con-
vert (as Romans 11 promises that someday they shall), they remain
God's enemies, and He does stir up pagans against them. Anti-Jewishness

5, Middle Village, NY: Jonathan David Publishers, Inc., 1984,
617
