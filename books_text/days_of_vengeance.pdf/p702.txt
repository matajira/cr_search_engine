THE DAYS OF VENGEANCE

Related Studies

Adams, Jay. The Time Is at Hand. Nutley, NJ: The Presbyterian and
Reformed Publishing Co., [1966] 1970.

Allen, Richard Hinckley. Star Names: Their Lore and Meaning. New
York: Dover Publications, [1899] 1963.

Allis, Oswald T. Prophecy and the Church. Grand Rapids: Baker
Book House, [1945] 1947.

Ambrose. On the Mysteries.

Athanasius. On the Incarnation of the Word of Ged.

_—___. Orations Against the Arians.

Augustine, Aurelius. Anti-Pelagian Works. Translated by Peter
Holmes and Robert Ernest Wallis. Grand Rapids: William B. Eerd-
mans Publishing Co., 1971.

. The City af God,

—____. On the Trinity.

Bahnsen, Greg L, Theonomy in Christian Ethics. Phillipsburg, NJ:
The Presbyterian and Reformed Publishing Co., [1977] 1984.

—____. “The Person, Work, and Present Status of Satan,” The
Journal of Christian Reconstruction, edited by Gary North. Vol. 1,
No. 2 (Winter, 1974).

» “The Prima Facie Acceptability of Postmillennialism.”
The Journal of Christian Reconstruction, Vol. 3, No. 2 (Winter,
1976-77).

Baldwin, Joyce G. Haggai, Zechariah, Malachi: An Introduction and
Commentary, Downers Grove, IL: Inter-Varsity Press, 1972.

Barr, James. Biblicat Words for Time. Naperville, IL: Alec R. Allen-
son Inc. Revised ed., 1969.

Bavinck, Herman. The Dectrine of God. Translated by William Hen-
driksen. Edinburgh: The Banner of Truth Trust, [1951} 1977.

Berkhof, Louis. The History of Christian Dectrines. Edinburgh: The
Banner of Truth Trust, [1937] 1969.

——______. Systematic Theology. Grand Rapids: William B. Eerd-
mans Publishing Co., fourth revised ed., 1949.

Bettenson, Henry, ed. Documents of the Christian Church. Oxford:
Oxford University Press, second ed., 1963.

. The Early Christian Fathers: A Selection from the Writ-
ings of the Fathers from St. Clement of Rome to St. Athanasius.
Oxford: Oxford University Press, 1956.

. The Later Christian Fathers; A Selection from the Writ-
ings of the Fathers from St. Cyril of Jerusalem to St. Leo the Great.
Oxford: Oxford University Press, [1970] 1977.

Boettner, Loraine. The Millennium. Philadelphia: The Presbyterian
and Reformed Publishing Co., 1957; revised, 1984.

Boersma, T. /s the Bible a Jigsaw Puzzle? An Evaluation of Hal Lind-
sey’s Writings. St. Catherines, Ont.: Paideia Press, 1978.

670
