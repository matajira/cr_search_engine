3
LEVIATHAN AND BEHEMOTH

The Book of Revelation is a Covenant document. It is a
prophecy, like the prophecies of the Old Testament. This means
that it is not concerned with making “predictions” of astonish-
ing events as such. As prophecy, its focus is redemptive and eth-
ical. Its concern is with the Covenant. The Bible is God’s revela-
tion about His Covenant with His people. It was written to show
what God has done to save His people and glorify Himself
through them.

Therefore, when God speaks of the Roman Empire in the
Book of Revelation, His purpose is not to tell us titillating bits
of gossip about life at Nero’s court. He speaks of Rome only in
relation to the Covenant and the history of redemption. “We
should keep in mind that in all this prophetic symbolism we
have before us the Rosman empire as a persecuting power. This
Apocalypse is not concerned with the history of Rome. . . . The
Beast is not a symbol of Rome, but of the great Raman world-
power, conceived as the organ of the old serpent, the Devil, to
persecute the scattered saints of God.”! The most important
fact about Rome, from the viewpoint of Revelation, is not that
it is a powerful state, but that it is Beast, in opposition to the
God of the Covenant; the issue is not essentially political but re-
ligious (cf. comments on 11:7). The Roman Empire is not seen in
terms of itself, but solely in terms of 1) the Land (Israel), and 2)
the Church.

1. Milton Terry, Biblical Apocatyptics: A Study of the Most Notable Reve-
lations of God and of Christ in the Canonical Scriptures (New York: Eaton
and Mains, 1898), pp. 393f.

325
