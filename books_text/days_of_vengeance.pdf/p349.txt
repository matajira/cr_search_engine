THE HOLY WAR 12:10-11

Dragon’s tail swept a third of the stars of heaven and threw
them to the Land; now the Dragon himself is thrown down to
the Land with his evil angels. In the following verses, St. John
explains the vision, telling us clearly when this great ejection of
the demons took place.

10-11 The explanation comes, as it often does with St. John,
in a call to worship from a loud Voice in heaven, exhorting the
assembly to praise the Lord for His marvelous works. The result
of Michael’s victory over the Dragon is fourfold, covering the
earth: Now have come the salvation — the victorious deliverance
into a “wide, open space” —and the power, and the Kingdom of
our God, and the authority of His Christ, The outcome of the
Holy War is this: The Kingdom has arrived! The power of God
and the authority of Christ have come, have been made mani-
fest in history, because the Accuser of our brethren has been
thrown down, the one who accused them before our God day
and night.

This great apocalyptic battle, the greatest fight in all history,
has already been fought and won by the Lord Christ, St. John
says, and the Dragon has been overthrown. Moreover, the mar-
tyrs who spent their lives in Christ’s service did not die in vain;
they are partakers in the victory: They conquered the Dragon by
the blood of the Lamb —by means of ** His definitive, once-for-
all victory—and by the word of their testimony. The martyrs’
faithfulness to Christ is demonstrated in that they did not love
their life even to death, knowing that “he who loves his life loses
it; and he who hates his life in this world shall keep it to life eter-
nal” John 12:25).

The Holy War between Michael and the Dragon therefore
cannot possibly be a portrayal of the final battle of history at the
end of the world. It cannot be future at ail. It is not a battle to
take place at the Second Coming. The victory over the Dragon,
according to St. John, does not take place by means of a cata-
clysmic event at the end of history, but by means of the cataclys-

36. Blood and word are both in the accusative case, but the preposition
should be read in the sense of means as well as grounds here (cf, Matt. 15:6;
John 6:57; 15:3; Eph. 5:19; Rev. 13:14); see Isbon T. Beckwith, The Apocalypse
of John: Studies in introduction with a Critical and Exegetical Commentary
(Grand Rapids: Baker Book House, [1919] 1979), p. 627.

315
