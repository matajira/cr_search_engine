2

THE NEW JERUSALEM

The Bible is a Storybook, with one Story to tell. That Story,
which is of Jesus Christ and His salvation of the world, is pre-
sented again and again throughout the Bible, with innumerable
variations on the same basic theme. One important aspect of
that Story is of God as the Warrior-King, who raises His people
from death, defeats His enemies, takes for Himself the spoils of
war, and builds His House. For example, there is the story of the
Exodus: “Moses said to the people, ‘Do not fear! Stand by and.
see the salvation of the Lorp which He will accomplish for you
today; for the Egyptians whom you have seen today, you will
never see them again forever. The Lorp will fight for you while
you keep silent’” (Ex, 14:13-14), Accordingly, after the success-
ful Red Sea crossing (the baptismal resurrection of Israel and
the baptismal destruction of Egypt), Moses exulted: “The Lorp
is a Warrior!” (Ex. 15:3). Egypt and all its wealth and glory were
completely wiped out; all that was left was what the Israelites
had “plundered,” of silver and gold, and articles of clothing (Ex.
3:21-22; 11:1-2; 12:35-36). Much of this was later turned over to
the Lord for the construction of the Tabernacle, God’s House
(Ex. 35:21-29; 36:3-8}, which He entered in flaming Glory (Ex.
40:34),

The pattern is repeated many times, another well-known ex-
ample being the story of David and Solomon: David acts as God’s
Warrior, fighting the Lord’s battles with Him (cf. 2 Sam. 5:22-25),
and his son Solomon builds the Lord’s House (2 Sam. 7:12-13);
and again, the sign that God has moved in is the descent of fire
(2 Chron. 7:1-3). All these were provisional victories and House-
buildings, anticipations of the definitive Victory in the work of
Jesus Christ.

535
