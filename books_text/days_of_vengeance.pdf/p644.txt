AAR

on

12 and 13
4

5

16

17,18

19

20
21,22

THE LITURGICAL STRUCTURE OF REVELATION

A. Hesrew SacriFice

Revelation.
Introductory.
Christian Worship A. The Creator.
Christian Worship B. The Lamb.
(The Four Horsemen).
Souis under Altar.
Gixth seal.)
Christian Worship C, The Martyrs.
The Trumpets.

Offering of Incense. This does not occur at this point in the daily ritual;
but it does on the Day of Atonement. See below, In the Temple ritual

The Jerusalem Sacrifices.
The High Priest.

The Temple Ornaments.

1. The lamb killed at dawn,

Blood splashed on altar.

(Feast of Tabernacles.)
Three Trumpets.

 

the Silence fo/lows the burning of the Incense.
9 {The Trumpets, orginally three, symbolise the prophetic message.)
(The Call of St. John, and his witness against Jerusalem.)

Opening of Sanctuary in Heaven,

(The Great Interlude.)

The Lamb and his Followers on
Mount Sion.

First fruits. Without blemish.

The Harvest (Passover).

The Vintage (ingathering).

Song of Moses and the Lamb,

The Sanctuary Opens.

The Smoke of the Glory.

No one may enter the Sanctuary.

Gates of Temple and Sanctuary
opened.

2. Preparation of Sacrifice.

Lamb skinned, cut up, washed,
laid by altar.

The meal offering. Bread.

The drink offering. Wine,

Pause for prayer and praise.

3. Offering of Incense.

Silence.

Intercession.

St. John has placed the Incense symbolism earlier, though the smoke
recalls it here. On the Day of Atonement no one might enter the sanc-
tary till the High Priest had finished his work there.

Pouring of the Blood.

The Seven Bows, In the daily ritual this is done at the beginning; but on
the Day of Atonement the High Priest smeared the mercy seat and

altar with blood at this point.
Babyion Burned,
Her Cup.

4, The Burning of the Victim.
The Cup poured out.

17:16 refers to the ritual of the sin-offering;
17:2, 3 is reminiscent of the scapegoat.

Alleluia Chorus.

The Marriage Supper of the Lamb.

The High Priest out of Heaven (cf.
Ecclus. 50).

The Great Supper of God.

5. The Psaims.
Song and Instruments.

6. The Feast on the Sacrifice.

(Wars of the Messiah and Judgments.)
The Tabernacle of God with Men (cf. Lev. 26:11-12).
Christian Worship D. The Universal Worship of Mankind.

Notes — This chart shows how the structure of the older part of Revelation fol-
lows the events of the daily sacrifice, with variations suggested by the ritual of
the Day of Atonement,

610
