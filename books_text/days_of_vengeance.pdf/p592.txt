21:18-21 PART FIVE: THE SEVEN CHALICES

ninth, topaz; the tenth, chrysoprase; the eleventh, jacinth; the
twelfth, amethyst. There have been several attempts to discover
St. John’s rationale for listing the stones in this order, the most
well-known being R. H. Charles’ suggestion that the jewels are
connected to the signs of the Zodiac, and that “the signs or con-
stellations are given in a certain order, and that exactly the re-
verse order of the actual path of the sun through the signs.” This
demonstrates, he says, that St. John “regards the Holy City
which he describes as having nothing to do with the ethnic spec-
ulations of his own and past ages regarding the city of the
gods.””* Charles has been followed on this point by several com-
mentators,?5 but later research has disproved this theory.2*
Sweet points out that “Philo (Special Laws 1.87) and Josephus
(Ant. 111,186) link the jewels with the Zodiac, but only as part of
the cosmic symbolism which they claim for the high priest’s vest-
ments; cf. Wisd. 18:24. John’s aim is similar. Any direct astro-
logical reference is destroyed by his linking them not with the
twelve gates of the heavenly city but with the foundations.”??

The most sensible explanation for the order of the stones
comes, as we would expect, from Austin Farrer. He shows that
the stones are laid out in four rows of three gems in each row, as
on the high priest’s breastplate: “St. John does not adhere either
to the order or to the names of the stones in the LXX Greek of
Exodus, and any query we may raise about translations of the
Hebrew names which he might have preferred to those offered
by the LXX can only land us in an abyss of uncertainty, It is rea-
sonable to suppose that he did not trouble to do more than give
a euphonious list in some general correspondence with the Ex-
odus catalogue. He has so arranged the Greek names, as to em-
phasize the division by threes. All but three of them end with s
sounds, and the three exceptions with n sounds. He has placed
the endings at the points of division, thus:

24. R. H. Charles, A Critical and Exegetical Commentary on the Reveta-
tion of St. John, 2 vols. (Edinburgh: T. & T. Clark, 1920), pp. 167f. Italics his.

25. See, e.g., G. B. Caird, The Revelation of St. John the Divine (New
York: Harper and Row, 1966}, pp. 274-78; Rousas John Rushdoony, Thy
Kingdom Come: Studies in Daniel and Revelation (Tyler, TX: Thoburn Press,
[1970] 1978), pp. 221f.

26. See T. F, Glasson, “The Order of Jewels in Rey, xxi. 19-20: A Theory
Eliminated,” Journal af Theological Studies 26 (1975}, pp. 95-100.

27. Sweet, p. 306.

558
