6:1-2 PART THREE: THE SEVEN SEALS

lies, Hal Lindsey goes all the way and declares that the Anti-
christ is “the only person who could accomplish all of these
feats.”?

But there are several points about this Rider that demonstrate
conclusively that He can be none other than the Lord Jesus
Christ. First, He is riding a white horse, as Jesus does in
19:11-16. Second, He carries a Bow. As we have seen, the passage
from Habakkuk that forms the basis for Revelation 6 shows the
Lord as the Warrior-King carrying a Bow (Hab. 3:9, 11). St.
John is also appealing here to Psalm 45, one of the great prophe-
cies of Christ’s victory over His enemies, in which the psalmist joy-
ously calls to Him as He rides forth conquering, and to conquer:

Gird Thy sword on Thy thigh, O Mighty One,

In Thy splendor and Thy majesty!

And in Thy majesty ride on victoriously,

For the cause of truth and meekness and righteousness;
Let Thy right hand teach Thee awesome things.

Thine arrows are sharp;

The peoples fall under Thee;

Thine arrows are in the heart of the King’s enemies.
{Ps. 45:3-5)

We should ask a rather obvious question at this point—so
obvious that we are apt to miss it altogether: Where did Christ
get the Bow? The answer (as is usually the case) begins in
Genesis. When God made the covenant with Noah, He declared
that He was no longer at war with the earth, because of the
“soothing aroma” of the sacrifice (Gen. 8:20-21); and as
evidence of this He unstrung His Bow and hung it up “in the
Cloud” for all to see (Gen. 9:13-17). Later, when Ezekiel was
“yaptured” up to the Throneroom at the top of the Glory-
Cloud, he saw the Bow hanging above the Throne (Ezek.
1:26-28); and it was still there when St. John ascended to heaven
(Rev. 4:3). But when the Lamb stepped forward to receive the
Book from His Father’s hand, He also reached up and took
down the Bow, to use it in judgment against the apostates of

7, There’s a New World Coming: A Prophetic Odyssey (Eugene, OR: Har-
vest House Publishers, 1973}, p. 103.

186
