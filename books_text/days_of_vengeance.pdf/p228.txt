6:9-10 PART THREE: THE SEVEN SEALS

Testament sacrifices, in which the bloed of the slain victim
would stream down the sides of the altar and form into a pool
around its base (“the sou! [Heb. nephesh] of the flesh is in the
blood,” Lev. 17:11)..7 The blood of the martyrs has been poured
out (cf. 2 Tim. 4:6), and as it fills the trench below the altar it
cries out from the ground with a loud voice, saying, How long,
O Lord, holy and true, dost Thou not judge and avenge our
blood upon those who dwell on the Land? The Church in
heaven agrees with the cherubim in calling forth God's judg-
ments: How long? is a standard phrase throughout Scripture for
invoking divine justice for the oppressed (cf. Ps. 6:3; 13:1-2;
35:17; 74:10; 79:5; 80:4; 89:46; 90:13; 94:3-4; Hab. 1:2; 2:6). The
particular background for its use here, however, is again in the
prophecy of Zechariah (1:12): After the Four Horsemen have
patrolled through the earth, the angel asks, “O Lorp of Hosts,
how long wilt Thou have no compassion for Jerusalem?” St.
John reverses this. After his Four Horsemen have been sent on
their mission, he shows the martyrs asking how long God will
continue to put up with Jerusalem. St. John’s readers would not
have failed to notice another subile point: If the martyrs’ blood
is flowing around the base of the altar, it must be the priests of
Jerusalem who have spilled it. The officers of the Covenant have
slain the righteous. As Jesus and the apostles testified, Jerusalem
was the murderer of the prophets (Matt. 23:34-37; Luke 13:33;
Acts 7:51-52). The connection with “the blood of Abel” crying
out from the ground near the altar (Gen. 4:10) is another indica-
tion that this passage as a whole refers to judgment upon Jeru-
salem (cf. Matt. 23:35-37). Like Cain, the “older brothers” of
the Old Covenant envied and murdered their righteous
“younger brothers” of the New Covenant (cf. 1 John 3:11-12).
And so the blood of the righteous cries out: The saints pray that
Christ’s prophecy of “the days of vengeance” (Luke 21:22) will
be fulfilled.

That this blunt cry for vengeance strikes us as strange just
shows how far our pietistic age has degenerated from the
Biblical worldview. If our churches were more acquainted with
the foundational hymnbook of the Church, the Psalms, instead

17. See Rousas John Rushdoony, Tay Kingdom Come: Studies in Daniet
and Revelation (Tyler, TX: Thobum Press, [1970] 1978), p. 145.

194
