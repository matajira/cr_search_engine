14:17-18 PART FOUR: THE SEVEN TRUMPETS

in heaven to assist in the harvest with his sharp sickle. At first
this appears to be simply a continuation of the first harvest, but
St. John makes a subtle shift, going all the way back to the be-
ginning of this section of Revelation in order to draw on its im-
agery of wrath. Christ instructed his disciples to pray, not just
for the conversion of Israel, but for its destruction as well; and
thus in 6:9-11 we saw the saints gathered around the golden altar
of incense, offering up their imprecatory prayers for vengeance.
Shortly after that scene, at the beginning of the Trumpet vi-
sions, an angel took the censer of the saint’s prayers, filled it
with the fire of the altar, and threw it onto the Land; “and there
followed peals of thunder and voices and flashes of lightning
and an earthquake” (8:3-5). Now, at the close of the Trumpet
section, St. John sees the same angel, the one who has power,
not just “over fire,” as most translations render it, but over the
fire, the fire burning on the altar; and he comes specifically from
the altar of the saints’ prayers in order to render judgment, to
bring about the historical response to the worship and the pray-
ers of the Church. He too prays for a harvest — but this time it
will be a harvest of the wicked, the “grapes of wrath” (Joel 3:13
similarly combines the images of harvest and vintage). So this
third angel calls to the second angel, the one holding the sickle,
and says: Put in your sharp sickle, and gather the clusters from
the vine of the Land, because her grapes are ripe. God’s Vine-
yard, Israel, is ripe for judgment.

My well-beloved had a vineyard on a fertile hill.

And He dug it all around, removed its stones,

And planied it with a bright red grape.

And He built a tower in the middle of it,

And hewed out a wine press in it;

Then He expected it to produce good grapes,

But it produced only worthless ones.

And now, O inhabitants of Jerusalem and men of Judah,

Judge between Me and My vineyard.

‘What more was there to do for My vineyard that I have not dane
in it?

Why, when I expected it to produce good grapes did it produce
worthless ones?

So now let Me tell you what I am going to do to My vineyard:

I will remove its hedge and it will be consumed;

374
