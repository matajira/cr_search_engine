THE LEVITICAL SYMBOLISM IN REVELATION

also used in the morning prayers of the synagogue, where it is associ-
ated with the thought of creation; in the Revelation the praise of God
for his creation is uttered by the Elders, who prostrate themselves at
the sound of the Sanctus.

This is the “firs: movement” of the Anaphora, of the Christian
Eucharist, in which men “join with angels and archangels and all the
company of heaven.” Most of the Greek liturgies show traces of the
“Axios” or “Axion” (worthy) of Revelation; at rather a long remove it
is reflected in “It is meet and right (justum et dignum) so to do.”

The Revelation of St. John then proceeds to show us the Lamb as
it had been slain for Sacrifice; and the Christian liturgies follow him
by narrating the life and death of Christ, and so leading up to the con-
secration and offering. The word Standing, which is applied to the
Lamb, is a translation of Tamid, the technical name for the lamb
which was offered every morning in the temple as a whole burnt-offer-
ing. It was the “standing offering.”

This is followed by the offering of Incense, which stands for inter-
cessory prayer; and then comes a New Song. The New Song was also
mentioned in a hymn used in the temple after the killing of the lamb,
and before the Incense. I shail refer to it later.

The liturgy ends with praise to God and the Lamb, and the singing
of the Amen, which was characteristic of the Eucharist at this point.
All the liturgies follow this outline, and it is from this point onwards
that they vary. The first two parts of the Te Deum follow the same lines
of construction.

‘We now turn to chapter 7, verses 9 to 17, a short passage which is
also the work of the latest period, anticipating the end of the book. It
represents the worship of the Martyrs in heaven.

The thought of martyrdom as sacrifice is as early as the Maccabean
period, and has behind it Jsaiah 53. The man who gives his life for
God or country is both priest and victim; he offers, but what he offers
is himself. In Revelation his priesthood is dependent upon that of
Christ.

In chapter | Christ has been shown as priest and King. He is wear-
ing the long white robe and the girdle at the breast; he stands “in the
midst of” the seven lamps; that is to say, he is in the sanctuary where
the seven-branched candlestick is, and robed like a priest. This plain
linen was worn by the high priest on the Day of Atonement. At the
end of Revefation the same figure comes out of the sanctuary with the
same robe splashed with blood.

The martyrs also wear white robes, which are connected with that
of Christ by the statement that they are washed in the blood of the
lamb; the same mixed character of priest and victim belongs both to

595
