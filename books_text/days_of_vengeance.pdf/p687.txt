COMMON GRACE, ESCHATOLOGY, AND BIBLICAL LAW

is ebb and flow, but always there is direction to the movement. There is
maturity. The creeds are improved. This, in turn, gives Christians cul-
tural power. Is it any wonder that the Westminster Confession of Faith
was drawn up at the high point of the Puritans’ control of England?
Are improvements in the creeds useless culturally? Do improvements
in creeds and theological understanding necessarily lead to impotence
culturally? Nonsense! It was the Reformation that made possible
modern science and technology.

On the other side of the field—indeed, right next to the wheat—
self-awareness by unbelievers also increases. But they do not always
become more convinced of their roots in chaos. The Renaissance was
successful in swallowing up the fruits of the Reformation only to the
extent that it was a pale reflection of the Reformation, The Renais-
sance leaders rapidly abandoned the magic-charged, demonically in-
spired magicians like Giordano Bruno.” They may have kept the hu-
manism of a Bruno, but after 1600, the open commitment to the
demonic receded. In its place came rationalism, Deism, and the logic
of an orderly world. They used stolen premises and gained power. So
compelling was this vision of mathematically autonomous reality that
Christians like Cotton Mather hailed the new science of Newtonian
mechanics as essentially Christian. It was so close to Christian views
of God’s orderly being and the creation’s reflection of His orderliness,
that the Christians unhesitatingly embraced the new science.

What we see, then, is that the Christians were not fully self-
conscious epistemologically, and neither were the pagans. In the time
of the apostles, there was greater epistemological awareness among
the leaders of both sides. The church was persecuted, and it won.
Then there was a lapse into muddled thinking on both sides. The at-
tempt, for example, of Julian the Apostate to revive paganism late in
the fourth century was ludicrous—it was half-hearted paganism, at
best. Two centuries earlier, Marcus Aurelius, a true philosopher-king
in the tradition of Plato, had been a major persecutor of Christians;
Justin Martyr died under his years as emperor. But his debauched son,
Commodus, was too busy with his 300 female concubines and 300
males?) to bother about systematic persecutions. Who was more self-
conscious, epistemologicaily speaking? Aurelius still had the light of
reason before him; his son was immersed in the religion of revolution
culturally impotent. He was more willing than his philosopher-
persecutor father to follow the logic of his satanic faith. He preferred

20. On the magic of the early Renaissance, see Frances Yates, Giordana
Bruna and the Hermetic Tradition (New York: Vintage, [196d] 1969).

2t. Edward Gibbon, The History of the Decline and Fall of the Roman Em-
pire, Milman edition, 5 Vols. (Phitadelphia: Porter & Coates, [1776]), I, p. 144.

655
