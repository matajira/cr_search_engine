20:7-8 PART FIVE; THE SEVEN CHALICES

tures, in order to “spike” God’s work and prevent it from attain-
ing fruition and maturity. That is why there was a sudden out-
break of demonic activity when Christ began His earthly minis-
try; that was Satan’s motivation for tempting Him, for entering
into Judas to betray Him, and for inspiring the Jewish and
Roman authorities to slay Him. His plan backfired, of course
(1 Cor. 2:6-8), and the Cross became his own destruction.
Throughout the Book of Revelation St. John has shown the
devil frantically working to bring about the final battle, and in-
variably being frustrated in his designs. Only after God’s King-
dom has realized its earthly potential, when the full thousand
years have been completed, will Satan be released to foment the
last rebellion— thus engendering his own final defeat and eternal
destruction.

In describing the eschatalogical war, St. John uses the vivid
“apocalyptic” imagery of Ezekiel 38-39, which prophetically
depicts the Maccabees’ defeat of the Syrians in the second cen-
tury B.c,: The ungodly forces are called Gog and Magog. Ac-
cording to some popular premillennial writers, this expression
refers to Russia, and foretells a war between the Soviets and
Israel during a future “Tribulation.” Even apart from the fact
that this interpretation is based on a radically inaccurate reading
of Matthew 24 and the other “Great Tribulation” passages,® it is
beset with numerous internal inconsistencies. First, premillen-
nialists tend to speak of this coming war with the Soviet Union
as synonymous with the “Battle of Armageddon” (16:16). Yet,
on premillennialist assumptions, the Battle of Armageddon
takes place before the Millennium begins—more than 1,000
years before St. John's “Gog and Magog” finally appear! Thus,
premillennial prophecy buffs are treated to prolonged discus-
sions of present Soviet military might and their supposed. prepa-
rations for assuming the role of “Gog and Magog.”34 At the

33. This should be obvious by now; cf. Chilton, Paradise Restored, pp.
77-102.

34. It is certainly true that the Soviet Union’s aggressive imperialism and its
worldwide sponsorship of terrorism pose a grave danger to the Western nations;
see Jean-Francois Revel, How Democracies Perish (Garden City: Doubleday
and Co., 1984). This, however, has nothing to do with fulfilled prophecy, and
everything to do with the fact that the West has simultaneously engaged in an
inercasing renunciation of Christian cthics and a progressive military and tech-
nological outfitting of her enemies; on the latter, see Antony Sutton, Western

520
