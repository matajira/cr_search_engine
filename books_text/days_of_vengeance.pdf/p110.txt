1:17-18 PART ONE: THE SON OF MAN

for the seven stars appeared regularly on the Emperor’s coins as
symbols of his supreme political sovereignty. At least some early
readers of the Revelation must have gasped in amazement at St.
John’s audacity in stating that the seven stars were in Christ’s
hand. The Roman emperors had appropriated to themselves a
symbol of dominion that the Bible reserves for God alone—
and, St. John is saying, Jesus Christ has come to take it back.
The seven stars, and with them all things in creation, belong to
Him. Dominion resides in the right hand of the Lord Jesus
Christ.

Naturally there will be opposition to all this. But St. John
makes it clear that Christ is on the offensive, coming forth to do
battle in the cause of His crown rights: out of His mouth came a
sharp two-edged sword, His Word that works to save and to
destroy. The image here is taken from the prophecy of Isaiah:
“He will strike the Land with the rod of His mouth, and with the
breath of His lips He will slay the wicked” (Isa. 11:4). It is used
again in Revelation to show Christ’s attitude toward heretics: “I
will make war against them with the sword of my mouth” (2:16);
and yet again to show the Word of God conquering the nations
(19:11-16). Not only is Christ in conflict with the nations, but He
declares that He will be completely victorious over them, subdu-
ing them by His bare Word, the sharp and powerful two-edged
sword that comes from His mouth (Heb. 4:12).

St. John’s Commission (1:17-20)

17 And when I saw Him, I fell at His feet as a dead man. And
He Jaid His right hand upon me, saying, Do not be afraid: I
am the first and the last,

18 and the living One; and [ was dead, and behold, [ am alive
forevermore, amen; and I have the keys of Death and of
Hades,

19 Write therefore the things you have seen, and what they are,
and what things shall take place after these things.

20 As for the mystery of the seven stars that you saw in My
right hand, and the seven golden lampstands: the seven stars
are the angels of the seven churches, and the seven lamp-
stands are the seven churches.

17-18 When he saw the Angel of the Lord, Daniel says, “I
fell into a deep sleep with my face to the ground. Then behold, a

76
