COME, LORD IESus! 22:5

the power of the Gospel would sweep across the earth, smashing
idolatry and flooding the nations with the Light of God’s grace.
Relatively speaking, the whole history of the world from Adam’s
Fall to Christ’s Ascension was Night; relatively speaking, the
whole future of the world is bright Day. This follows the pattern
laid down at the creation, in which the heavens and earth move
eschatologically from evening to morning, the lesser light being
succeeded by the greater light, going from glory to Glory (Gen,
1:5, 8, 13, 19, 23, 31): Now, St. John tells us, Jesus Christ has ap-
peared, and is “coming quickly,” as the bright Morning Star (v. 16).

In his concluding comment on the restoration of Paradise,
St. John tells us that the royal priesthood shall reign, not just
for a “millennium,” but forever and ever: “The reign of the
thousand years (20:4-6) is but the beginning of a regal life and
felicity which are to continue through all aeons to come. And so
the kingdom of the saints of the Most High will be most truly, as
Daniel wrote, ‘an everlasting kingdom’ (Dan. 7:27). This is the
‘eternal life’ of Matthew 25:46, just as the second death, the lake
of fire, is the ‘eternal punishment’ into which the ‘cursed’ go
away.”?

Final Warnings and Blessings (22:6-2D

6 And he said to me: These words are faithful and true. And
the Lord, the God of the spirits of the prophets, sent His
angel to’ show to His servants the things which must shortly
take place.

7 And behold, | am coming quickly. Blessed is he who keeps
the words of the prophecy of this book.

8 And 1, John, am the one who heard and saw these things.
And when I heard and saw, I fell down to worship at the feet
of the angel who showed me these things.

9 And he said to me: Don’t do that! | am a fellow servant of
yours and of your brethren the prophets and of those who
keep the words of this book; worship God.

10 And he said to me: Do not seal up the words of the prophecy
of this book, for the time is near.

11 Let the one who does wrong, still do wrong; and let the one
who is filthy, still be filthy; and let the one wha is righteous,

7. Milton Terry, Biblical Apacalyptics: A Study of the Most Notable Reve-
lations of God and of Christ in the Canonical Scriptures (New York: Eaton
and Mains, 1898), p. 471.

573
