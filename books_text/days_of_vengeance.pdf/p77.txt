THE CONTEMPORARY FOCUS OF REVELATION

that God’s faithfulness to His holy Word will not wear out or
diminish,

The Book of Revelation is not about the Second Coming of
Christ. It is about the destruction of Israel and Christ’s victory
over His enemies in the establishment of the New Covenant
Temple. In fact, as we shall see, the word coming as used in the
Book of Revelation never refers to the Second Coming. Revela-
tion prophesies the judgment of God on apostate Israel; and
while it does briefly point to events beyond its immediate con-
cerns, that is done merely as a “wrap-up,” to show that the un-
godly will never prevail against Christ’s Kingdom. But the main
focus of Revelation is upon events which were soon to take
place,

Third, St. John identifies certain situations as contempor-
ary: In 13:18, he clearly encourages his contemporary readers to
calculate the “number of the Beast” and decipher its meaning; in
17:10, one of the seven kings is currently on the throne; and St.
John tells us that the great Harlot “is [present tense] the Great
City, which reigns [present tense] over the kings of the earth”
(17:18). Again, the Revelation was meant to be understood in
terms of its contemporary significance. A futuristic interpreta-
tion is completely opposed to the way St. John himself inter-
prets his own prophecy.

Fourth, we should notice carefully the words of the angel in
22:10: “Do not seal up the words of the prophecy of this book,
for the time is near.” Again, of course, we are told explicitly that
the prophecy is contemporary in nature; but there is more. The
angel’s statement is in contrast to the command Daniel received
at the end of his book: “Conceal the words and seal up the book
until the time of the end” (Dan. 12:4). Daniel was specifically
ordered to seal up his prophecy, because it referred to “the end,”
in the distant future. But St. John is told zor to seal up his
prophecy, because the time of which it speaks is near.

Thus, the focus of the Book of Revelation is upon the con-
temporary situation of St. John and his first-century readers. It
was written to show those early Christians that Jesus is Lord,
“ruler over the kings of the earth” (Rev, 1:5), It shows that Jesus
is the key to world history—that nothing can occur apart from
His sovereign will, that He will be glorified in all things, and that
His enemies will lick the dust. The Christians of that day were

43
