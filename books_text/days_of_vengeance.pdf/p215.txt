6

IN THE PATH OF THE WHITE HORSE

St. John brings us now to the breaking of the Seven Seals of
the Book (six of the Seals are broken in Chapter 6; the Seventh
Seal is broken in 8:1, and is connected to the Seven Trumpets).
‘We have seen that the Book represents the treaty document of
the New Covenant, the opening of which will result in the de-
struction of apostate Israel (see on 5:1-4). What then does the
breaking of the Seals represent? Some have thought this to sig-
nify a chronological reading through the Book, and that the
events depicted are in a straight, historical order. This is unlikely
for two reasons. First, the Seals seem to be on the outside edge
of the Book (which is in the form of a scroll): one cannot really
begin to read the Book until all the Seals are broken. The
Seventh Seal, consisting of a call to action by the blowing of the
Seven Trumpets, actually opens the Book so that we may read
its contents.

Second, a careful reading of the events shown by each Seal
reveals that they are not listed in chronological order. For exam-
ple, in the Fifth Seal—after all the havoc wreaked by the Four
Horsemen—the martyrs calling for judgment are told to wait.
But the judgment is immediately poured out in the Sixth Seal,
the entire creation “unseam’d from the nave to the chaps.” Yet,
after all this, God commands the angels to withhold judgment
until the servants of God are protected (7:3). Obviously, the
Seals are not meant to represent a progressive chronology. It is
more likely that they reveal the main ideas of the Book’s con-
tents, the major themes of the judgments that came upon Israel
during the Last Days, from a.p. 30-70.

R. H. Charles pointed out the close structural similarity be-
tween the Six Seals of this chapter and the events of the so-called

181
