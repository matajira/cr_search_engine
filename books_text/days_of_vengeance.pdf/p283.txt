ALL HELL BREAKS LOOSE 9:13

death will result, In his excellent study of the Levitical system,
Gordon Wenham tells us that “the purification offering dealt
with the pollution caused by sin. If sin polluted the land, it de-
filed particularly the house where God dwelt. The seriousness of
pollution depended on the seriousness of the sin, which in turn
related to the status of the sinner. If a private citizen sinned, his
action polluted the sanctuary only to a limited extent. Therefore
the blood of the purification offering was only smeared on the
horns of the altar of burnt sacrifice. If, however, the whole na-
tion sinned or the holiest member of the nation, the high priest,
sinned, this was more serious. The blood had to be taken inside
the tabernacle and sprinkled on the veil and the altar of incense.”*

The sins of the nation were atoned for by offering a sacrifice
on the brazen altar, then taking the blood and smearing it on the
horns of the golden altar of incense (Lev, 4:13-21). In this way
the altar was purified, so that the incense could be offered with
the assurance that God would hear their prayers, The first-
century readers of Revelation would have recognized the signifi-
cance of this: God’s command to His angels, in response to the
prayers of His people, is spoken from the horns of the golden
altar. Their sins have been covered, and do not stand in the way
of free access to God.

One further point should be observed. The prayers of the
Church at the altar of incense are imprecatory prayers against
the nation of Israel. The “Israel” that has rejected Christ is pol-
Juted and defiled (cf. Lev. 18:24-30), and its prayers will not be
heard by God, for it has rejected the one atonement for sin. The
unclean land of Israel will therefore be judged in terms of the
curses of Leviticus 26, a chapter which repeatedly threatens a
sevenfold judgment upon the nation if it becomes polluted by
sin (Lev. 26:18, 21, 24, 28; we have seen that this is the source for
the repeated sevenfold judgments in the Book of Revelation),
But the Church of Jesus Christ is the new Israel, the holy na-
tion, the true people of God, who possess “confidence to enter
the holy place by the blood of Jesus” (Heb. 10:19). Again, the
first-century Church is assured by St. John that her prayers will
be heard and answered by God. He will take vengeance upon

5. Gordon J. Wenham, The Bovk of Leviticus (Grand Rapids: William B.
Eerdmans Publishing Co., 1979), p. 96.

249
