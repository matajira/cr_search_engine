19:1-2 PART FIVE: THE SEVEN CHALICES

internal connection between the Church of the New Testament
and that of the Old.”! The word itself recalls the Old Testament
Hallel-psalms (Ps. 113-118), songs of victory that were sung at
the festivals of Passover and Tabernacles. These psalms cele-
brated the greatness of God, especially as revealed in the deliver-
ance of His people from Egypt and their restoration to true wor-
ship; and they look forward to the day when all nations will
praise the Lord. Except for minor allusions to a couple of
Ffaitel-psalms in verses 5 and 7, St. John does not construct this
liturgy on their pattern; rather, the use of Hallelujah! alone is
enough to make the connection. The first Biblical occurence of
the expression, however, is in Psalm 104:35, which strikingly
parallels the juxtaposition of judgment and praise in Revela-
tion:

Let sinners be consumed from the earth,
And let the wicked be no more.

Bless the Lorp, O my soul.

Hallelujah!

The destruction of apostate Jerusalem on behalf of Christ
and His Church will be the demonstration that salvation and
power and glory belong to our God~a phrase that recalls
David’s exultation when the preparations for building the Tem-
ple had been completed: “Thine, O Lorn, is the greatness and
the power and the glory and the victory and the majesty, indeed
everything that is in the heavens and the earth; Thine is the do-
minion, O Lorp, and Thou dost exalt thyself as head over all”
(1 Chron. 29:11; Christ also alluded to David’s text in the Lord’s
Prayer, Matt. 6:13: “Thine is the Kingdom and the power and
the glory forever and ever, Amen”). The song also quotes
David’s celebration of the Law’s all-embracing authority in
Psalm 19:9: “The judgments of the Lord are true and righteous
altogether.” In the fulfillment of the Law’s curses on the apos-
tate city, God’s new Israel takes up the chant, affirming that His
judgments are true and righteous.

Israel’s destruction is the showcase of God's righteousness.
God’s honor could not endure the blasphemy of His name occa-

1. E. W. Hengstenberg, The Revelation of St. John, two vols. (Cherry Hill,
NJ: Mack Publishing Ca., n.d.}, vol. 2, p. 238.

470
