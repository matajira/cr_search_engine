LITURGY AND HISTORY 8:3-5

saints. The intimate connection between liturgy and history is an
inescapable fact, one which we cannot afford to ignore. This is
not to suggest that the world is in danger of lapsing into “non-
being” when the Church’s worship is defective. In fact, God will
use historical forces (even the heathen) to chastise the Church
when she fails to live up to her high calling as the Kingdom of
priests. The point here is that the official worship of the cove-
nantal community is cosmically significant. Church history is
the key to world history: When the worshiping assembly calls
upon the Lord of the Covenant, the world experiences His judg-
ments, History is managed and directed from the altar of in-
cense, which has received the prayers of the Church,”

In my distress J called upon the Lorp,

And cried to my God for heip;

He heard my voice out of His Temple,

And my cry for help before Him came into His ears.
Then the earth shook and quaked;

And the foundations of the mountains were trembling
And were shaken, because He was angry.

Smoke went up out of His nostrils,

And fire from His mouth devoured;

Coals were kindled by it.

He bowed the heavens also, and came down

With thick darkness under His feet.

And He rode upon a cherub and flew;

And He sped upon the wings of the wind.

He made darkness His hiding place, His canopy around Him,
Darkness of waters, thick clouds of the skies.

From the brightness before Him passed His thick clouds,
Hailstones and coals of fire.

The Lorp also thundered in the heavens,

And the Most High uttered His voice,

Hailstones and coals of fire.

And He sent out His arrows, and scattered them,

And lightning flashes in abundance, and routed them.
Then the channels of waters appeared,

And the foundations of the world were laid bare

At Thy rebuke, O Lorp,

At the blast of the breath of Thy nostrils. (Psalm 18:6-15)

7. The symbolic use of incense is therefore appropriate (but of course not
binding) in the liturgy of the New Covenant.

233
