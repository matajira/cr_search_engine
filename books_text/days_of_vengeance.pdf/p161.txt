THE DOMINIGN MANDATE 3:8-9

Christ is thus announcing that the officers of apostate Israel
are false stewards: they have been thrown out of office, removed
from all rightful authority, and replaced by the One who is holy
and true. The keepers of the door at the synagogue had excom-
municated the Christians, declaring them to be apostates. In re-
ality, Christ says, it is you of the synagogue who are the
apostates; it is you who have been cast out of the Covenant; and
I have taken your place as the True Steward, the Pastor and
Overseer of the Covenant (cf. 1 Pet. 2:25).

8-9 And so the Lord can comfort these suffering Christians
who, on account of their faithful following of Christ, have
suffered wrongful excommunication from the Covenant. I know
your deeds, He assures them. You have been shut out of the
door by the keyholders, but you must remember that I am the
One who has the key, and behold, I have put before you a door
which no one can shut. The Lord of the Covenant Himself has
admitted them to fellowship, and has cast out those who pre-
tend to hold the keys; the faithful Christians have nothing to
fear. The church of Philadelphia has only a little power — it is
not prominent, stylish, or outwardly prosperous, in contrast to
the impressive, apparently “alive,” compromising church at Sar-
dis. Yet they have been faithful with what they have been given
(cf. Luke 19:26): You... have kept My Word, and have not
denied my name.

Therefore, I will cause those of the synagogue of Satan, who
say that they are Jews, and are not, but lie — behold, I will make
them to come and bow down at your feet, and to know that I
have loved you. Again the apostate Jews are revealed in their
true identity: the synagogue of Satan (cf. 2:9). Again, there is
no such thing as “orthodox” Judaism; there is no such thing asa
genuine belief in the Old Testament that is consistent with a re-
jection of Jesus Christ as Lord and God. Those who do not
believe in Christ do not believe the Old Testament either. The
god of Judaism is the devil. The Jew will not be recognized by
God as one of His chosen people until he abandons his demonic
teligion and returns to the faith of his fathers—the faith which
embraces Jesus Christ and His Gospel. When Christ-rejecting
Jews claim to follow in the footsteps of Abraham, Jesus says,
they lie, And, although they currently have the upper hand in

127
