1619 PART FIVE: THE SEVEN CHALICES

three factions, which became acute after the return of Titus.
While Titus was besieging it from without, the three leaders of
rival factions were fighting fiercely within: but for this the city
might have staved off defeat for a long time, even perhaps inde-
finitely, for no great army could support itself for long in those
days in the neighborhood of Jerusalem; there was no water and
no supplies. This fighting within the city delivered it quickly into
the hands of Titus; ‘the days were shortened,’ ”32

Another indication that the Great City is Jerusalem is the
fact that St. John distinguishes her from the cities of the Gen-
tiles, which fell with her, Jerusalem, we must remember, was the
capital city of the kingdom of priests, the place of the Temple;
within her walls sacrifices and prayers were offered up for all na-
tions. The Old Covenant system was a world-order, the founda-
tion on which the whole world was organized and maintained in
stability. She covenantally represented all the nations of the
world, and in her fall they collapsed. The new organization of
the world was to be based on the New Jerusalem, built on the
Rock. ’

And Babylon the Great (cf. on 14:8) was remembered before
God, to give her the cup of the wine of His fierce wrath. As Ford
observes, “the phrase suits the liturgical setting of the text. The
libations have been poured, but instead of the memorial being a
turning of God towards his people with grace and mercy, it is for
judgment, God’s ‘remembering’ is always an efficacious and cre-
ative act, not a mere intellectual activity; he remembers in the
act of blessing (transmitting vitality or life) and cursing (de-
stroying). The irony of vs. 19 lies in the exhortation to Israel to
‘remember’ God’s covenant and kindness in general. She was es-
pecially admonished, as in Deuteronomy 6, to keep a perpetual
remembrance of the Exodus and Sinai events, to recall them day
and night, and never to forget God who brought them to
pass....

“In this chapter the author intimates that because Israel for-
got and became arrogant, the Egyptian plagues were turned
back on her. Even then she did not repent but blasphemed (cf.
Job 1:22; 2:10), and God remembered her for judgment.”33

32, Carrington, p. 266; cf. Josephus, The Jewish War, v.v.1-5,
33. Ford, p. 275.

416
