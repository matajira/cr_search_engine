THE FEASIS OF THE KINGDOM 1:1

13 And He is clothed with a robe dipped in blood; and His
name is called the Word of Gad.

14 And the armies that are in heaven, clothed in fine linen,
white and clean, were following Him on white horses.

1§ And from His mouth comes a sharp two-edged sword, so
that with it He may smite the nations; and He Himself will
rule them with a rod of iron; and He Himself treads the wine
press of the wine of the fierce wrath of God, the Almighty.

16 And on His robe and on His thigh He has a name written:
KING OF KINGS, AND LORD OF LORDS.

1? And I saw one angel standing in the sun; and he cried out
with a loud voice, saying to all the birds that fly in mid-
heaven: Come, assemble for the great supper of God;

18 in order that you may eat the flesh of kings and the flesh of
commanders and the flesh of mighty men and the fiesh of
horses and of those who sit on them and the flesh of all men,
both free men and slaves, and small and great.

19 And I saw the Beast and the kings of the earth and their
armies, assembled to make war against the One sitting upon
the horse, and against His army.

20 And the Beast was seized, and with him the False Prophet
whe performed the signs in his presence, by which he deceived
those who had received the mark of the Beast and those who
worshiped his image; these two were thrown alive into the
lake of fire which burns with brimstone.

21 And the rest were killed with the sword that came from the
mouth of the One sitting upon the horse, and all the birds
were filled with their flesh.

ll This begins the final section of seven visions, each one
opening with the phrase kai eidon, And I saw (19:11, 17, 19; 20:1,
4, 11; 21:1), With the revelation of the Holy Eucharist St. John
sees, as he has not seen before, heaven opened, and, as Farrer
observes, “every intermediary vanishes between himself and
Christ.” It is the invitation to Communion with Christ that
opens heaven to the Church and reveals her Lord,

St. John sees a white horse, the symbol of Christ’s victory
and dominion (6:2; cf. 14:14). It is important for the proper un-
derstanding of this passage to note that the One sitting upon it is
called Faithful and True: Christ rides forth to victory in His
character as “the faithful and true Witness” (3:14), as “the Word
of God” (19:13). St. John is not describing the Second Coming

481
