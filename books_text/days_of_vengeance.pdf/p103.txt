KING OF KINGS 1:9

And He put all things in subjection under His feet” (Eph.
1:21-22; cf. Mark 1:14-15; Matt. 16:28; 28:18; Acts 2:29-36; Col.
1:13). If all things are now in subjection under His feet, what
more could be added to His dominion? Of course, the “rulers
and authorities” still have got to be put down; that is what much
of St. John’s prophecy is about. But in principle, and defini-
tively, the Kingdom has arrived. This means that we do not have
to wait for some future redemptive or eschatological event be-
fore we can effectively take dominion over the earth. The do-
minion of God’s people throughout the world will simply be the
result of a progressive outworking of what Christ Himself has
already accomplished. St. John wanted his readers to under-
stand that they were in both the Great Tribulation ard the King-
dom-—that, in fact, they were in the Tribulation precisely be-
cause the Kingdom had come (Dan, 7:13-14). They were in a
war, fighting for the Kingdom’s victory (Dan. 7:21-22), and thus
they needed the third element in St. John’s worldview: persever-
ance in Christ Jesus. Perseverance is an important word in the
message of the Revelation, and St. John uses it seven times (1:9;
2:2, 3, 19; 3:10; 13:10; 14:12),

Here, too, there is a radical contrast with much of modern
dispensationalism. Because the diluted version of Christianity
currently fashionable in contemporary America rejects the con-
cepts of the Kingship and Lordship of Christ, it also rejects the
Biblical teaching on perseverance — and the predictable result is
that comparatively few converts of modern evangelicalism are
able to stick with even that minimally-demanding faith!?? The
popular doctrine of “eternal security” is only a half-truth, at
best: it gives people an unbiblical basis for assurance (e.g., the

21. For a recent example of this position, see Norman Geisler, “A Premil-
lennial View of Law and Government,” Bibliotheca Sacra (July-September
1985), pp. 250-66. Writing against the postrnillennialism of R. J. Rushdoony
and other “reconstructionists,” Geisler actually says: “Postmillenarians work
to make a Christian America. Premillenarians work for a truly free America”
(p. 260). The choice is clear: Shall we choose Christianity? Or shall we choose
freedom instead? Geisler must be commended for having stated the matter
with such precision; technically speaking, however, he is nat the first to have
posed the dilemma in this way. He stands in an ancient tradition (Gen. 3:1-5).

22, See Walter Chantry, Today’s Gospel: Authentic or Synthetic? (Rdin-
burgh: The Banner of Truth Trust, 1970), and Arend J. ten Pas, The Lordship
of Christ (Vallecito, CA: Ross House Books, 1978).

69

 
