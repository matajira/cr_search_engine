THE LEVITICAL SYMBOLISM IN REVELATION

Nisan 14. Lamb killed. Crucifixion,
Passover eaten. Burial.
15. High day.
Firstfruit cut. Resurrection.

In the year of the cruxifixion it chanced that Nisan 15 was also a
sabbath; but this was, of course, a coincidence, I have dated the cruxi-
fixion, etc., as in the fourth gospel, which I take to be correct; but in
any case the references in Revelation are to the cruxifixion story as
Telated in that gospel.

5. Lightfoot in his account of the Temple and its services gives an
outline of the ritual for the Omer.

“Those that the Sanhedrin sent about it went out at the evening of
the Holy Day (the first day of the Passover Week); they took baskets
and sickles, etc.

“They went out on the Holy Day when it began to be dark, and a
great company went out with them; when it was now dark, one said to
them,

“On this Sabbath, On this Sabbath, On this Sabbath.

“In this Basket, In this Basket, In this Basket.

“Rabbi Eliezer the son of Zadok saith, With this Sickle, With this
Sickle, With this Sickle, every particular three times over,

“And they answer him, Well, Well, Well; and he bids them reap.”

This is not perhaps on first sight as close a parallel as one might
have desired to the passage we are discussing; but there are points of
likeness: () There was a dialogue which took place at the beginning of
harvest. (4) It explicitly mentions the time: This Sabbath = The Hour
is come, (¢) It explicitly mentions the Sickle. (@) The reaper is then
commanded 10 do his work; but the words of this command are not
given. The two dialogues are of the same character, have the same pur-
pose, involve similar speakers, and have points of resemblance; we
could not expect much more.

(The word Sabbath demands a note. I think I am right in saying
that Nisan 15, though not necessarily a Sabbath, might be called a
Sabbath, because it was in every respect equal to a Sabbath and ob-
served in the same way. The breach of the Sabbath involved in cutting
the first sheaf was excused.)

6. A further very interesting parallel is afforded by the stage we
have now reached in the Tamid, or daily offering. To the pieces of the
lamb were added (a) the meal offering of fine flour, and (4) the daily
offering of the high priest, which consisted of bread and wine. The
Son of Man is, of course, the Christian high priest; the wheat harvest
and the vintage afford some parallel to the bread and wine. The con-

603
