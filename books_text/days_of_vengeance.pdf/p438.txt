16:8-9 PART FIVE: THE SEVEN CHALICES

tice of His rewards and punishments, graces and retributions.”"4

But the apostates refuse to submit to God’s lordship over
them. Like the Beast, whose head is crowned with “names of
blasphemy” (13:1) and whose image they worship, the men blas-
phemed the name of God who has the power over these plagues.
And, like the impenitent Pharaoh (cf. Ex. 7:13, 23; 8:15, 19, 32;
9:7, 12, 34-35; 10:20, 27; 11:10; 14:8), they did not repent so as to
give Him glory. Israel has become an Egypt, hardening its heart;
and, like Egypt, it will be destroyed.

The Last Three Chalices: it Is Finished! (16:10-21)

10 And the fifth angel poured out his Chalice upon the throne
of the Beast; and his kingdom became darkened; and they
gnawed their tongues because of pain,

11 and they blasphemed the God of heaven because of their
pains and their sores; and they did not repent of their deeds.

12 And the sixth angel poured out his Chalice upon the great
river, the Euphrates; and its water was dried up, that the way
might be prepared for the kings from the rising of the sun.

13 And I saw coming out of the mouth of the Dragon and out
of the mouth of the Beast and out of the mouth of the False
Prophet, three unclean spirits like frogs;

14 for they are spirits of demons, performing signs, which go
out to the kings of the whole world, to gather them together
for the War of that great Day of God, the Almighty.

15 Behold, I am coming like a thief. Blessed is the one who
stays awake and keeps his garments, lest he walk about naked
and they see his shame.

16 And they gathered them together to the place which in
Hebrew is called Armageddon,

17 And the seventh angel poured out his Chalice upon the air;
and a loud Voice came from the Temple of heaven, from the
throne, saying: It is done.

18 And there were flashes of lightning and peals of thunder and
voices; and there was a great earthquake, such as there had
not been since the men came to be upon the Land, so mighty
an earthquake, and so great.

19 And the Great City was split into three parts, and the cities

14. St. Augustine, On the Trinity, iii.9; Henry Bettenson, ed. and trans.,
The Later Christian Fathers (Oxford: Oxford University Press, [1972] 1977),
p. 191.

404
