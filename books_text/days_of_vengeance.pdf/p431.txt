JUDGMENT FROM THE SANCTUARY 16:1

of God into the Land.

2 And the first angel went and poured out his Chalice into the
Land; and it became a loathsome and malignant sore upon
the men who had the mark of the Beast and who worshiped
his image.

3 And the second angel poured out his Chalice into the sea,
and it became blood like that of a dead man; and every liy-
ing soul in the sea died.

4 And the third angel poured out his Chalice into the rivers
and the springs of waters; and it became blood.

5 And I heard the Angel of the Waters saying: Righteous art
Thou, who art and who wast, O Holy One, because Thou
didst judge these things;

6 for they poured out the blood of saints and prophets, and
Thou hast given them blood to drink: They are worthy!

7 And I heard the altar saying: Yes, O Lord God, the Al-
mighty, true and righteous are Thy judgments.

8 And the fourth angel poured out his Chalice upon the sun;
and it was given to it to scorch the men with fire.

9 And the men were scorched with great heat; and the men
blasphemed the name of God who has the power over these
plagues; and they did not repent, so as to give Him glory.

1° The command authorizing the judgments is given by a
loud Voice from the Temple, again underscoring both the divine
and ecclesiastical origin of these terrible plagues (cf. 15:5-8).?
“The judgments of the vials are the overflow of the wrath of
God blazing forth and filling his temple, a visitation or presence
vouchsafed in response to the prayers of his sainis.”? The seven
angels (cf. 15:1) are told to pour out the Chalices of God’s
wrath: The Septuagint uses this verb (ekched) in the directions
to the priest to pour out the blood of the sacrifice around the
base of the altar (cf. Lev. 4:7, 12, 18, 25, 30, 34; 8:15; 9:9). The
term is used in Ezekiel with reference to apostate Israel’s forni-
cation with the heathen (Ezek, 16:36; 23:8), of her shedding of
innocent blood through oppression and idolatry (Ezek. 22:3-4,
6, 9, 12, 27), and of God’s threat to pour out His wrath upon her

2. Cf. Isa. 66:6~“A Voice of uproar from the City, a Voice from the Tem-
pic: The Voice of the Lorp who is rendering recompense to His cncmies!”

3. Austin Farrer, The Revelation of St. John the Divine (Oxford: At the
Clarendon Press, 1964), p. 175.

397
