4:5-8 PART THREE: THE SEVEN SEALS

who officiates in the worship of the Church be clothed in a man-
ner corresponding to the task assigned to him and expressing
visibly what he does. Moreover, whoever leads in the act of wor-
ship does not perform as a private party but as a minister of the
Church; he is the representative of the community and the
spokesman of the Lord. Hence, an especially prescribed vest-
ment, a sort of ecclesiastical ‘uniform,’ is useful for reminding
both the faithful and himself that in this act he is not Mr. So-
and-So, but a minister of the Church in the midst of a multitude
of others. What was not any less indispensable in ancient times,
when the sense of community and of the objectivity of cultic ac-
tion prevailed, has become in our time a very useful aid, and in-
deed truly necessary, since individualism and subjectivity have be-
come so deeply rooted in the piety of the Reformed churches.”"8

§-8 St. John describes the heavenly court in terms of the fa-
miliar acoustic and visual effects which accompany the Glory-
Cloud, as at Sinai (Ex. 19:16-19): From the Throne proceed
flashes of lightning and voices and peals of thunder. Again, as in
1:4-5, the imagery is shown to be the heavenly original of the
Tabernacle structure (Heb. 8:5; 9:23): Like the Lampstand with
its seven lamps burning within the Holy Place, there are seven
lamps of fire burning before His Throne, the seven lamps imag-
ing the seven Spirits of God, the Holy Spirit in His sevenfold
fulness of activity. Here, again, is the combination of the three
aspects of the Glory-Cloud imagery: the Voice (v. 1), the radiant
Glory (v. 3), and the Spirit (vy. 5).

Then before the Throne St. John sees, as it were, a sea of
glass like erystal. This is another point at which this vision in-

18. Ibid., p. 138. As it turned our, some of those Reformation churches that
retained the robe chose the black academic gown, perhaps partly in reaction
against what were perceived as the excesses of the Roman Church, and in
order to emphasize the teaching function of the minister. But, as Paquier
points out, “there is not a single reference to black robes in the Bible, whereas
white robes and vestments are mentioned many times, cither actually or sym-
bolically.

“Indeed, if there is one color that suggests itself as an adequate expression
of the Gospel and the evangelical divine service, certainly it is white. In the
Bible the color white is the divine color par excellence because it symbolizes the
holiness and perfection of God (Ps. 104:2; Dan. 7:9; Rev. 1:14; 19:11; 20:11)”
Gbid., pp. 139f).

154
