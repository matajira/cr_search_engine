9:14-16 PART FOUR: THE SEVEN TRUMPETS

her persecutors, for the earth is both blessed and judged by the
liturgical actions and judicial decrees of the Church.

God’s readiness to hear and willingness to grant His people’s
prayers are continually proclaimed throughout Scripture (Ps.
9:105 10:17-18; 18:3; 34:15-17; 37:4-5; 50:14-15; 145:18-19), God
has given us numerous examples of imprecatory prayers, show-
ing repeatedly that one aspect of a godly man’s attitude is hatred
for God’s enemies and fervent prayer for their downfall and de-
struction (Ps, 5:10; 10:15; 35:1-8, 22-26; 59:12-13; 68:1-4;
69:22-28; 83; 94; 109; 137:8-9; 139:19-24; 140:6-11). Why then do
we not see the overthrow of the wicked in our own time? An im-
portant part of the answer is the unwillingness of the modern
Church to pray Biblically; and God has assured us: You do not
have because you do not ask (James 4:2). But the first-century
Church, praying faithfully and fervently for the destruction of
apostate Israel, had been heard at God’s heavenly altar. His
angels were commissioned to strike.

14-16 The sixth angel is commissioned to release the four
angels who had been bound at the great river Euphrates; they
then bring against Israel an army consisting of myriads of myr-
iads. The Euphrates River formed the boundary between Israel
and the fearsome, pagan forces which God used as a scourge
against His rebellious people. “It was the northern frontier of
Palestine [cf. Gen. 15:18; Deut. 11:24; Josh. 1:4], across which
Assyrian, Babylonian, and Persian invaders had come to im-
pose their pagan sovereignty on the people of God. All the
scriptural warnings about a foe from the north, therefore, find
their echo in John’s bloodcurdling vision” (cf. Jer. 6:1, 22; 10:22;
13:20; 25:9, 26; 46:20, 24; 47:2; Ezek. 26:7; 38:6, 15; 39:2).6 It
should be remembered too that the north (the original location
of Eden)? was the area of God’s throne (Isa. 14:13); and both the
Glory-Cloud and God’s agents of vengeance are seen coming
from the north, i.e., from the Euphrates (cf. Ezek. 1:4; Isa.
14:31; Jer. 1:14-15). Thus, this great army from the north is God’s
army, and under His control and direction, although it is plainly

6. G. B. Caird, p. 122,
7. See David Chilton, Paradise Restored: A Biblical Theology of Dominion
(Ft, Worth, TX: Dominion Press, 1985), pp. 294.

250
