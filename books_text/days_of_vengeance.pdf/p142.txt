2214-16 PART TWO: THE SEVEN LETTERS

to outright persecution by an ungodly state—yet they were fall-
ing prey to other forms of compromise with Satan.

What exactly was the Nicolaitan doctrine? St. John de-
scribes it in terms of the doctrine of Balaam, using his ancient
error as a symbol of the contemporary heresy. Like Balaam, the
false apostles attempt to destroy Christians by corrupting them,
by enticing them to eat things sacrificed to idols, and to commit
fornication. Both of these practices were commonplace in the
pagan religious atmosphere of the day, and St. John’s language
seems to be drawn from the Jerusalem Council’s instructions to
Gentile converts:

For it seemed good to the Holy Spirit and to us to lay upon
you no greater burden than these essentials: that you abstain
from things sacrificed to idols and from blood and from things
strangled and from fornication; if you keep yourselves free from
such things, you will do well (Acts 15:28-29).'7

In disobedience to the true apostolic Council, the false Nico-
Jaitan apostles advocated antinomianism—the teaching that,
perhaps through the sacrifice of Christ, Christians were “freed
from the law,” in a sense completely opposed to the Biblical
teaching of sanctification. It was no longer a sin, in their ac-
count, to commit idolatry and fornication; the believer was not
under obligation to obey the law, but can live as he pleases
(although they probably claimed, as antinomians do today, the
“leading of the Spirit” as justification for their abominable prac-
tices).

There is, however, an important aspect of the imagery in-
volved here that we should not overlook: The false apostles are
seeking to seduce the Christians into idoletrous eating and for-

17. “Writing to Corinth some fifteen years after the council 51. Paul had oc-
casion to argue with Christians who regarded the eating of things sacrificed to
idols as a thing indifferent; and though he does not take his stand on the Jeru-
salem decree, he opposes the practice on the ground that it gave offense to
weak brethren (1 Cor, 8:4, 9-10), and also because of the connection which he
tegarded as existing between idol-worship and unclean spirits (1 Cor. 10:20:
The things that the Gentiles sacrifice, they sacrifice to demons, and not to
God; and I do not wart you to become sharers in demons); to partake of the
‘table of unclean spirits’ (1 Cor. 10:21) was inconsistent with participation in the
Eucharist.” Henry Barclay Swete, Commentary on Revelation (Grand Rapids:
Kregel Publications, [1911] 1977), pp. 37f.

108
