THE DAYS OF VENGEANCE

considerable eschatological speculation.

If I am correct, and this shift takes place, The Days of Ven-
geance will be studied by historians as a primary source docu-
ment for the next two or three centuries.

Producing New Leaders: Key to Survival

Because pessimillennialism could not offer students long-
term hope in their earthly futures, both versions have defaulted
culturally. This withdrawal from cultural commitment culmin-
ated during the fateful years, 1965-71. When the world went
through a psychological, cultural, and intellectual revolution,
where were the concrete and specific Christian answers to the
pressing problems of that turbulent era? Nothing of substance
came from traditional seminaries. It was as if their faculty mem-
bers believed that the world would never advance beyond the
dominant issues of 1952. (And even back in 1952, seminary pro-
fessors were mostly whispering.) The leaders of traditional
Christianity lost their opportunity to capture the best minds of a
generation. They were perceived as being muddled and confused.
There was a reason for this. They were muddled and confused.

In the 1970's, only two groups within the Christian commu-
nity came before the Christian public and announced: “We have
the biblical answers.”* They were at opposite ends of the poli-
tical spectrum: the liberation theologians on the Left and the
Christian Reconstructionists on the Right.° The battle between

8. Francis Schaeffer had been announcing since 1965 that humanist civiliza-
tion is an empty shell, and that it has no earthly future. He repeated over and
over that Christianity has the questions that humanism cannot answer. The
problem was that as a Calvinistic premillennialist, he did not believe that any
specifically Christian answers would ever be implemented before Christ’s sec-
ond coming. He did not devote much space in his books to providing specifi-
cally Christian answers to the Christian questions that he raised to challenge
humanist civilization. He asked excellent cultural questions; he offered few
specifically Christian answers. There were reasons for this: Chilton and North,
op. cit.

9. In the highly restricted circles of amiliennial Calvinism, a short-lived
movement of North American Dutch scholars appeared, 1965-75, the “cosmo-
nomic idea” school, also known as the neo-Docyeweerdians, named after the
Dutch legal scholar and philosopher, Herman Dooyeweerd. They made little
impression outside of the North American Dutch community, and have since
faded into obscurity. Their precursors in the early 1960s had been more conser-
vative, but after 1965, too many of them became ideological fellow travellers
of the liberation theologians. They could not compete with the harder-core
tadicalism represented by Sojourners and The Other Side, and they faded.

xxii
