LITURGY AND HISTORY 8:8-11

really expect His disciples to go around praying about moving
literal mountains? Of course not. More importantly, Jesus was
not changing the subject. He was still giving them a lesson about
the fall of Israel. What was the lesson? Jesus was instructing His
disciples to pray imprecatory prayers, beseeching God to
destroy Israel, to wither the fig tree, to cast the apostate moun-
tain into the sea.1¢

And that is exactly what happened. The persecuted Church,
under oppression from the apostate Jews, began praying for
God’s vengeance upon Israel (6:9-11), calling for the mountain
of Israel to “be taken up and cast into the sea.” Their offerings
were received at God’s heavenly altar, and in response God di-
tected His angels to throw down His judgments to the Land
(8:3-5). Israel was destroyed. We should note that St. John is
writing this before the destruction, for the instruction and en-
couragement of the saints, so that they will continue to pray in
faith. As he had told them in the beginning, “Blessed is he who
reads and those who hear the words of the prophecy, and keep
the things that are written in it; for the time is near” (1:3).

The Third Trumpet (8:10-11)

10 And the third angel sounded, and a great star fell from
heaven, burning like a torch, and it fell on a third of the
rivers and on the springs of waters;

11 and the name of the star is called Wormwood; and a third of
the waters became wormwood; and many men died from the
waters, because they were made bitter.

10-11 Like the preceding symbol, the vision of the Third
Trumpet combines Biblical imagery from the fall of both Egypt
and Babylon. The effect of this plague—the waters being made
bitter —is similar to the first plague on Egypt, in which the water
became bitter because of the multitude of dead and decaying
fish (Ex, 7:21). The bitterness of the waters is caused by a great
star that fell from heaven, burning like a torch. This parallels
Isaiah’s prophecy of the fall of Babylon, spoken in terms of the

16. According to William Telford, this mountain was a standard expression
among the Jewish people for the Temple Mount, “the mountain par
excellence”, see The Barren Temple and the Withered Tree (Department of
Biblical Studies, University of Sheffield, 1980), p. 119.

239
