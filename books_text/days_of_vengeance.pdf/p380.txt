13:18 PART FOUR: THE SEVEN TRUMPETS

people, is as tall as “six cubits and a span” (1 Sam. 17:4) -i.e.,
six, plus a hand grasping for more; the head of his spear weighs
600 shekels of iron. (Goliath is, on several counts, a Beast; as
the seed of the Dragon, he wears sca/e-armor, 1 Sam. 17:5; but
the Seed of the Woman destroys him by inflicting a head-
wound, 1 Sam. 17:49-51.) Another striking example of this pat-
tern takes place when King Nebuchadnezzar erects an image of
himself measuring 60 cubits high and 6 cubits across (Dan.
3:1).25 The impact of this is magnified when we consider that the
numerical value of the Hebrew letters?« in Daniel 3:1 (which
describes Nebuchadnezzar’s image) add up to 4,683 — which is 7
times 666 (4,662), plus 21, the triangu/ar of 6 (triangulation will
be explained presently).

A brief digression here will serve to place this point in its
larger symbolic framework, for—in contrast to the multiplied
sixes of Nebuchadnezzar’s image —the names of Daniel and his
three friends who refused to worship the idol add up to 888 in
Hebrew.” This is also the number of Jesus in Greek.?8 The Fall
of man occurred on the seventh day of creation (man’s first full

25, St. Irenaeus sees 666 as a combination of Noah’s age at the Flood (600)
—symbolizing “all the commixture of wickedness which took place previous to
the deluge” —with the 60+ 6 of Nebuchadnezzar’s image, symbolizing “every
error of devised idols since the flood, together with the slaying of the prophets
and the cutting off of the just.” Against Heresies, in Alexander Roberts and
James Donaldson, eds., The Ante-Nicene Fathers (Grand Rapids: Wm. B.
Eerdmans Publishing Co., 1973 reprint), Vol. 1, p. 558.

26. In Hebrew (as in most ancient languages}, the alphabet served double
duty: each letter was also a numeral. Thus any given word or group of words
had a numerical value, which could be computed simply by adding up the
numerals. The langnage-system of the West avoids this by using the Roman
alphabet for its letters and the Arabic alphabet for its numerals. It is thus diffi-
cult and artificial for us to imagine going back and forth between the letter-use
and numeral-use of the characters in our language, but for the ancients it was
quite natural. In all probability, they did not need to engage in any great men-
tal shifts back and forth, but simply saw and comprehended both aspects at
once,

27. See Brnest L. Marlin, The Original Bible Restored (Pasadena, CA:
Foundation for Biblical Research, 1984), p. 110. In his vision of the great image
which represented the heathen empires leading up to Christ's kingdom,
Nebuchadnezzar was the “head of gold” (Dan. 2:37-38); Martin has pointed
out that 666 years after Nebuchadnezzar inaugurated his reign (604 B.c.),
Israel’s last sabbatical cycle began (Autumn, a.p. 63), which ended in the
destruction of Jerusalem and the Temple in the Autumn of 70,

28. IHEZOVE (l= 10+ H=8+ E= 200+ 0 = 70+ ¥ = 400 + E = 200) = 888.

346
