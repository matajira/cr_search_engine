THE LEVITICAL SYMBOLISM IN REVELATION

True and firm it is that thou art Jehoveh: aur God and the God of
our fathers.

Thy Name is from everlasting: and there is no God beside thee.

A new song did they that were delivered: sing to thy Name by the
sea shore.

Together did ait praise and own thee as king: and say Jehovah shall
reign who hath redeemed Israel.

We are not surprised, therefore, to find St. John introducing at this
Point the song of Moses the servant of God and of the Lamb. It is
sung by the martyrs standing by the glassy sea in heaven, which now
appears as if mingled with fire, a clear reference to the Red Sea of the
Mosaic deliverance. St. John’s song is very like the temple ceremonial:

Great and wonderful are thy warks; Jehovah God of hosis.

Just and true are thy ways; O king of the world.

Who shall not fear thee O Jehovah; and giorify thy Name? for
thau onty art holy.

For all the nations shail come and worship before thee: for thy
righteous acts have been shown forth.

The “New Song” mentioned in the temple ritual is alluded to earlier
in 14:3 by those who stand with the Lamb on Mount Sion; but this
song is only known to those who sing it. The song at this point, how-
ever, serves to identify them as priests as well as victims.

A “New Song” has also been given to the twenty-four priestly
elders who lead the Christian worship in chapter 5, This also follows
the revelation of the Lamb of the Tumid as slain for sacrifice (5:9).
“Worthy art thou to take the book . . . for thou wast stain for sacrifice
and redeemed to God in thy blood, out of every tribe and tongue and
people and nation, and hast made them a reyal priesthood to God and
they reign upon the earth.”

It is impossible to say how much of this psalmody is based on the
temple ritual, or how much it has influenced Christian liturgiology.
May not the “True and firm” have suggested the “Meet and right?”

A form of the True and Firm is still used in the Synagogue morning
prayers.

4. The Incense Offering. —The next section of the daily ritual of
the temple was the offering of the incense at the golden altar inside the
Naos, We have noted that St. John has placed this piece of ceremonial
earlier; but that has enabled him to place something far more signifi-
cant here.

Let us note first that he has arranged the ritual of the seven bowls
exactly as he arranged the ritual of the seven trumpets. A comparison
will suffice to show this:

605
