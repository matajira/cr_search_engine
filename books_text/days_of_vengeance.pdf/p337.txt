THE HOLY WAR 12:3

An added bonus: Sundown on September 11, 3 B.c., was the
beginning of Tishri 1 in the Jewish calendar —Rosh Hashanah,
the Day of Trumpets! !4 Martin summarizes: “The central theme
of the Day of Trumpets is clearly that of enthronement of the
great King of kings. This was the general understanding of the
day in early Judaism —and it certainly is that of the New Testa-
ment. In Revelation 11:15 the seventh angel sounds his ‘last
trump’ and the kingdoms of this world become those of Christ.
This happens at a time when a woman is seen in heaven with
twelve stars around her head and the Sun mid-bodied to her,
with the Moon under her feet. This is clearly a New Moon scene
for the Day of Trumpets.”!5

3 St. John sees another sign... in heaven: a great red
Dragon. As he explains in v. 9, the Dragon is none other than
“the Serpent of old who is called the devil and Satan,” the enemy
of God and His people. St. John reveals him as the power be-
hind the imperial thrones of the ancient world that persecuted
the Church; for, like the four Beast-empires of Daniel’s proph-
ecy, the Dragon has seven heads and ten horns: Daniel’s beasts
possessed seven heads among them (the third beast having
four), and the fourth beast had ten horns (Dan. 7:3-7). Baby-
lon, Medo-Persia, Greece, and Rome were all stages in the
Dragon’s attempt to establish his illicit empire over the world.
(The significance of the seven heads is thus not simply that the
Dragon is hard to kill, but rather that he is identified with the
terrible beasts of Daniel’s vision; cf. the “heads” of the Dragon
in Ps, 74;13-15,) He was the great Beast, of which they had been
only partial images. It was he who had been the agelong enemy
of the people of God. In all Israel’s struggles against Beasts,

 

ical phenomena taking place during the years 3-2 a,c. Chief among these
celestial events was the fact that Jupiter, recognized by Jews and Gentiles alike
as the “Planct of the Messiah,” was located in Virgo's womb and standing still,
directly over Bethlehem, on December 25, 2 8.c., when the Child was a litle
over a year old, (Matthew states that the holy family was settled in a house,
not in a stable, by the time the Magi visited [Matt. 2:13]. Moreover, Herod
ordered the slaughter of the innocents “from two years old and under, accord-
ing to the time which he had ascertained from the Magi” [Matt. 2:16], in-
dicating that the Child was no longer a newborn.) For a full account of the as-
tronomical events of 3-2 a.c,, sec Martin, pp. 4-25, 144-77.

14. Ibid., pp. 152ff.

15. Ibid., p. 158.

303
