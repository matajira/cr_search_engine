PART FIVE: THE SEVEN CHALICES

brethren who hold the Testimony of Jesus; worship God!
For the Testimony of Jesus is the Spirit of prophecy.

There are several similarities in language between this pass-
age and that in 1:15-19, the announcement of the seventh
angel’s theme of the completion of “the Mystery of God”: the
opening of the Kingdom and the heavenly Temple to the whole
world in the New Covenant. We can easily see the message of
these verses as an expansion of that idea when we take note of

the parallels:

11:18—loud voices in heaven.

11:15, 17 —He will reign forever
and ever... . Thou hast taken
Thy great power and didst
Teign.

11:16—The twenty-four elders
... fell on their faces and wor-
shiped God.

11:18 —The time came for the
dead to be vindicated, and the
time to give their reward to Thy
servants the prophets and to the
saints.

11:18—Thy servants . . . those
who fear Thy name, the small
and the great.

11:19—There were lightnings,
noises, thunderings. . . .

19:1—a loud voice of a great
multitude in heaven,

19:1, 6—Hallelujah! Salvation
and power and glory belong to
our God... Hallelujah! For
the Lord our God, the Al-
mighty, reigns.

19:4—The twenty-four elders
... fell down and worshiped
God.

18:24-19:2—In her was found
the blood of prophets and of
saints... . His judgments are
true and righteous; for... He
has avenged the blood of His
servants.

19:5— All you His servants, you
who fear Him, the small and the
great.

19:6—The voice of a great mul-
titude and as the sound of many
waters and as the sound of
mighty peals of thunder. .. .

The appearance of the Bride, prepared for marriage, is thus
equivalent to the opening of the Temple and the full establish-

468
