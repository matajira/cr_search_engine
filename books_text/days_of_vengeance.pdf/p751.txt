SUBJECT INDEX

‘Smyrna, 6
Tetter to, 86-87, 89, 90, 99.104
problems at, 100
tribulation of church at, 103-4
Sodom, 226, 281, 363, 365, 453, 472, 491, 525
Solomon, 289, 350, 363, 525, 535
Song, new, 175-78, 355-36
Song of Moses, 217, 386, 395, 460
Song of Mases and of Lamb, 381, 486-87, 402
Song of Solomon, 33, 298
Song of Witness, 380-82, 386
covenant structure of, 380
Son of God, 87, 90, 112, See also Jesus Christ
Son of Man, 50, 66, 306. See also Jesus Christ
advent of, 484
reign of, 371-72, 510-11
sign of, 287
vision of, 72-76, 89
Sorcerers, 550, 578, See also Occultism
Soviet Union, 659-60. See also Marxism
Gog and Magog and, 520-22
Speculation, checks on, 38-39
Spirits, three unclean, 408-9. See also Demons
Star(s), 160, 196-97
darkened, 240-41
Israel likened 10, 159
moning, 579
one third af, cast down, 306
twelve, 297, 300-301, 303, 554
Stars, seven, 75-76, 78, 90, 94, 119, 158
description of, 80
State, 64, 78. See also Rome
‘Worshipers of, 101, 106, 364, 398, 435. See
also Emperor cult
Stephen, 283
Stone, onyx, IL
Stone, white, 87, 109, 10
Suffering, 68-70, 118. See alsa Tribulation(s)
godliness and, 407
purpose of, 39.40
Sun, 260, 301-2, 303, 553-54
black, 196, 240
Supper of Gad, great, 489-91
Sword, two-edged, 105, 484, 485-86
Symbolism
astronomical, 158-60, 300-303
Creation's, 32-33
meaning of, 29, 80-81
nature of Biblical, 33-34, 582
primacy of, 31-36
Revelation’s, 27-31, 51, 79
system of, 23D, 38-39, 582
Synagogues. See also Satan, synagogue of
Christian, 372
of Jews, 341, 344
messianic, 619
Syria, 440, $20

 

Systematic theology, 38

Tabernacle, 6, 130, 150, 155, 266, 380, 381,
392, 596. See also Temple
heavenly, 288-89, 546-47
Tabernacles, Feast of, 143, 216, 596
meaning of, 222-23
Talmud, 254, 336, 618
Tannin, 304, 306
Tares, 526-27, $28, 634-35
parable of, 634, 651
Tarichaeae, massacre of, 399-400
Tau, 205-6
Taurus, 75, 158, 389
Temple, 23, 88, 130, 266, 385, 392, 425, 464,
536, 537, 597-609. See alsu Tabernacle
‘Church as, 201-94, 392-93, 441. See alsa
Jerusalem, New
cleansing of, 217-18, 273, 448
destruction of, 440
earthly, 291
heavenly, 291-93, 388-89. See also
Tabernacle, heavenly
incense ceremony in, 229-30, 600-601, 605-6
measuring the, 272-74
pillars of, 261
prayers in, 604-5, See alsu prayer
Sabbath services in, 366-87
vision of, 149-51
voice from, 397, 412
Temptation, 500-501
40 days af, 217
40 years of, 217
Tempter. See Satan
Ten, 437
Ten Commandments, 142, 167, 264, 202. See
also Law of God
‘Testimony, 264, 266, 388-89, See also Witness
of Jesus, 70, 392, 512, 513
tent of, 268, 381, 392
‘Textus Receptus, 45
Three and a half
days, 283
as number, 274
years, 274, 275n, 321, 348
Throne(s), 60-61, 65, 192, 472
centrality of, 142, 149
chariot-, 149, 188
Satan's, 106
twenty-four, 151
vision of, 149-50, 155
white, 529
Thunder, 473
seven peals of, 262-63
Thyatica, $79
doctrinal laxity at, 113
letter to, 87-88, 90, 111-18
Tiberius Caesar. See Caesar, Tiberius

 

 

719
