THE FEASTS OF THE KINGDOM 19:12

Behold, days are coming, declares the Lorn,

When I shall raise up for David a righteous Branch;
And He will reign as King and act wisely

And do justice and righteousness in the land.

In His days Judah wil! be saved,

And Israel will dwell securely;

And this is His name by which He will be called:
The LORD Our Righteousness. (Jer. 23:5-6)

12 The figure on the white horse is the same as the Son of
Man, the First and the Last, the Living One, of St. John’s first
vision, for His eyes area flame of fire (cf. 1:14): He is the omnis-
cient Lord whose discerning scrutiny is “able to judge the
thoughts and intentions of the heart” (Heb. 4:12). This majestic
figure is already victorious, many times over, as symbolized by
the many diadems He wears.

The gold plate on the forehead of the high priest bore the
sacred Name of the Lorn; appropriately, after taking note of
the many diadems on Christ’s brow, St. John sees that He has a
name written. But this is a name which no one knows except
Himself. How are we to understand this? As we saw at 2:17, the
New Testament use of the words for know (gindskd and oida) is
influenced by a Hebrew idiom, in which the verb to know ac-
quires related meanings: to acknowledge, to acknowledge as
one’s own, and to own (see, e.g., Gen. 4:1; Ex. 1:8; Ps. 1:6; Jer.
28:9; Ezek. 21 Zech. 14:7; Matt. 7:23; John 10:4-5; Rom.
8:29; 1 Cor. 8:3; 2 Tim. 2:19).'4 Thus, the point in this verse is
not that no one can know what the name is (for in fact, as we
shall see, we do “know” the name, in the cognitive sense), but
that He alone properly owns the name; it belongs only to Him.
This is reinforced by the chiastic structure of the passage:

 
 
 

A. He has a name written which no one owns except Himself (v. 12b)
B. He is clothed with a robe dipped in blood (v, 13a)
C, His name is called the Word of God {v. 13b)
C, From His mouth comes a sharp two-edged sword (v. i5a)
B. He treads the wine press of the fierce wrath of God (v.15b)
A. On His robe and on His thigh He has a name written: KING OF
KINGS AND LORD OF LORDS \v.i6)

14, See the brief discussion in Meredith G. Kline, Jmages of the Spirit
{Grand Rapids: Baker Book House, 1980), p. 130.

483
