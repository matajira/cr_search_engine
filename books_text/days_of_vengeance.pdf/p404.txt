14:12-13 PART FOUR: THE SEVEN TRUMPETS

and it reveals death because it is the revelation of Life. Christ is
this Life. And only if Christ is Life is death what Christianity
proclaims it to be, namely the enemy to be destroyed, and not a
‘mystery’ to be explained.”!4

Yes, says the Spirit, that they may rest from their labors; and
their deeds follow with them. Again there is a contrast with the
fate of the Beast-worshipers, who will have no rest day and
night from their torments. The persevering saints are encouraged
to continue in faithfulness, for their eternal rest is coming and
their works will be rewarded. Biblical perseverance is determined
by the rewards of eternity, not by the tribulations of the mo-
ment. Biblical hope transcends the battle. This does not mean
that the Bible commands an other-worldly neglect of the present
life; but neither does it countenance a perspective that is only, or
primarily, this-worldly. Our sinful tendency is to go in one direc-
tion rather than the other, but God calls us to be both this-
worldly and other-worldly. Biblical faith calls us to work in this
world for dominion with all our might (Gen. 1:28; Eccl. 9:10),
and at the same time reminds us constantly of our eternal hope,
our ultimate rest.

The Son of Man, the Harvest, and the Vintage (14:14-20)

14 And | looked, and behold, a white Cloud, and sitting on the
Cloud Gne like the Son of Man, having a golden crown on
His head, and a sharp sickle in His hand.

15 And another angel came out of the Temple, crying out with
aloud Voice to Him who sat on the Cloud: Put in your sickle
and reap, because the hour to reap has come, because the
harvest of the Land is ripe.

16 And He who sat on the Cloud threw His sickle over the
Land; and the Land was reaped.

17 And another angel came out of the Temple which is in
heaven, and he also had a sharp sickle.

18 And another angel, the one who has power over the fire,
came out from the altar; and he called with a loud shout to
him who had the sharp sickle, saying: Send forth your sharp
sickle, and gather the clusters from the vine of the Land, be-
cause her grapes are ripe.

19 And the angel threw out his sickle to the Land, and gathered.

18. Alexander Schmemann, For the Life uf the World: Sacraments and
Orthodoxy (Crestwood, NY: St. Vladimir’s Seminary Press, 1973), pp. 99f.

370
