5:5-7 PART THREE: THE SEVEN SEALS

In what sense is Jesus Christ a Lamb? The passage is not re-
ferring to Jesus in His Nature —He is not “lamblike” in the sense
of being gentle, sweet, or mild, as some would falsely under-
stand this text.4 Christ is called a Lamb, not in view of His Per-
son (which pop-theology degrades to the modern concept of
“personality” anyway), but in view of His work. He is the Lamb
that was slain, “who takes away the sin of the world” (John
1:29). Thus, the center of history is the finished, sacrificial work
of Christ. The foundation for His mediatorial kingship (Christ
as the Lion) is His mediatorial atonement (Christ as the Lamb).
It is because of His sacrifice that He has been exalted to the
place of supreme rule and authority. Christ has attained victory
through His sacrificial suffering and death on our behalf.

St. John emphasizes this by his specific language: a Lamb
standing, as if slain. Philip Carrington suggests that the Greek
word standing (hestzkos) is “a rough Greek translation of the
Hebrew Tamid, which means ‘standing’ or ‘continual,’ and re-
fers to the daily burnt-offering in the Temple. It is the regular
technical term, and forms the title of the section of the Mishnah
which deals with that sacrifice. The Lamb of the Tamid is an in-
telligible expression, which might well have been turned into the
Arnion Hestekos of the Greek. The Greek word Hest@kos does
not mean ‘continual,’ but only ‘standing’ in the literal sense; but
it might be a rough equivalent like Christos (smeared), which
stands for Messiah. Arnion Hestékos might thus be ‘baboo’
Greek for Lamb of the Sacrifice.

“The word Arnion has also aroused discussion. Our Lord is
called Lamb of God in the fourth gospel (1:29), just as he is here
called Lamb of the Tamid; but the two words are different,
Arnion here and Ammos in the gospel. It is possible that while
Amnos is the more common and natural word for Lamb, Arnion
Hest&kos might be a technical term of the Jewish Temple. . . .”?

St. John continues the symbolic imagery: Christ the Lamb
has seven horns. The horn in Scripture is an understandable
symbol for strength and power (cf. Ps. 75:10); more than this,

8. Hal Lindsey speaks in this connection of Christ’s “lamblike meekness
and gentleness” in There’s a New World Coming: A Prophetic Odyssey
(Eugene, OR: Harvest House Publishers, 1973), p. 94.

9. Philip Carrington, The Meaning of The Revelation (London: SPCK,
1931), pp. 1198.

172
