17:6-7 PART FIVE: THE SEVEN CHALICES

tion of the godly, and especially of the prophets (Matt. 21:33-44;
23:29-35; Acts 7:51-53). As St. John tells us in 18:24, “in her was
found the bload of prophets and of saints and of all who have
been slain on the earth.” Jerusalem was the persecutor of the
prophets par excellence.

But it is not always easy to look at things with “theological”
eyes. At the moment of her glory, a successful harlot is beauti-
ful, alluring, seductive. God’s Word is realistic, and does not
pretend that evil always appears repulsive. The temptation to
sin, as we all know, can be very attractive (Gen. 3:6; 2 Cor.
11:14). As St. John beheld the Great Harlot, therefore, he was
quite taken in, fascinated with her beauty: He wondered wlth
great wonder (cf. Rev. 13:3-4: “And the whole Land wondered
after the Beast; and they worshiped the Dragon... .”). The
angel therefore rebukes him: Why do you wonder? St. John
records this to warn his readers against being seduced by the
Harlot, for she és beautiful and impressive. The antidote to be-
ing deceived by the wiles of the False Bride is to understand the
Mystery of the Woman and of the Beast that carries her, The
angel will now reveal the nature of the Harlot’s alliance with the
Beast, her opposition to Christ, and her approaching destruc-
tion. St. John’s readers must understand that there is no longer
any hope of “reform from within.” Jerusalem is implacably at
war with Jesus Christ and His people. The once-Holy City is
now a Whore.

The Angel Explains the Mystery (17:8-18)

8 The Beast that you saw was and is not, and is about Lo as-
cend out of the Abyss and to go to destruction. And those
who dwell on the Land will wonder, whose name has not
been written in the Book of Life from the foundation of the
world, when they see the Beast, that he was and is not and
will come.

9 Here is the mind which has wisdom. The seven heads are
seven mountains on which the Woman sits,

10 and they are seven kings; five have fallen, one is, the other
has not yet come; and when he comes, he must remain a Lit-
tle while.

11 And the Beast which was and is not, is himself also an
eighth, and is of the seven, and he goes to destruction.

432
