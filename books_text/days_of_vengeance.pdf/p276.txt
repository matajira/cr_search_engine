8:13 PART FOUR: THE SEVEN TRUMPETS

Rev, 19:17-18), The Eagle-cherub will reappear in this section of
Revelation as an image of salvation (12:14), and at the end will
be replaced by {or seen again as) an angel flying in midheaven
proclaiming the Gospel to those who dwell on the Land (14:6),
for his mission is ultimately redemptive in its scope. But the sal-
vation of the world will come about through Israel’s fall (Rom.
11:11-15, 25). So the Eagle begins his message with wrath, pro-
claiming three Woes that are to come upon those who dwell on
the Land.

Like the original plagues on Egypt, the curses are becoming
intensified, and more precise in their application. St. John is
building up to a crescendo, using the three woes of the Eagle
(corresponding to the fifth, sixth, and seventh blasts of the
Trumpet; cf. 9:12; 11:14-15) to dramatize the increasing disasters
being visited upon the Land of Israel. After many delays and
much longsuffering by the jealous and holy Lord of Hosts, the
awful sanctions of the Law are finally unleashed against the
Covenant-breakers, so that Jesus Christ may inherit the king-
doms of the world and bring them into His Temple (11:15-19;
21:22-27),

242
