21:3 PART FIVE: THE SEVEN CHALICES

and to the City of the living God, the heavenly Jerusalem, and
to myriads of angels in festal assembly, and to the Church of the
firstborn who are enrolled in heaven, . . .” (Heb. 12:22-23).

3 If we are citizens of heaven, as St. Paul declared (Eph.
2:19), it is also true that heaven dwells within us (Eph. 2:20-22).
Indeed, the Word Himself has tabernacled among us (John
1:14); He and His Father have made Their abode with us (John
14:23); and thus we are the Temple of the Living God (2 Cor.
6:16). Accordingly, St. John’s vision of the Holy City is followed
by a loud Voice from heaven, saying: Behold, the Tabernacle of
God is among men, and He shall dwell among them, and they
shall be His people, and God Himself shall be among them.
Again, this is a repetition of what we have already learned in
this prophecy (3:12; 7:15-17). In the New Testament Church the
promise of the Law and the prophets is realized: “I will make
My Tabernacle among you, and My soul will not reject you; I
will also walk among you and be your God, and you shall be My
people” (Lev. 26:11-12); “And I will make a Covenant of peace
with them; it will be an everlasting Covenant with them. And I
will establish them and multiply them, and will set My sanctuary
in their midst forever. My dwelling place also will be with them;
and I will be their God, and they will be My people. And the na-
tions will know that I am the Lorp who sanctifies Israel, when
My sanctuary is in their midst forever” (Ezek. 37:26-28).

As verse 9 makes explicit, this passage is the conclusion of
the Chalices-section of the prophecy. At its beginning, St. John
saw the Sanctuary of the Tabernacle filling with smoke, so that
no one was able to enter it (15:5-8), and then he heard “a loud
Voice” from the Sanctuary order the seven angels to pour out
their Chalices of wrath into the Land (16:1). At the outpouring
of the seventh Chalice “a loud Voice” again issued from the
Sanctuary, saying: “It is done!” — producing a great earthquake,
in which the cities fell and every mountain and island “fled
away” as the vision turned to focus on the destruction of Baby-
lon, the False Bride (16:17-21). Now, toward the close of the
Chalices-section, earth and heaven have “fled away” (20:11;
21:1), and again St. John hears a loud Voice from heaven, an-
nouncing that access to the Sanctuary has been provided to the
greatest possible degree, for the Tabernacle of God is among

546
