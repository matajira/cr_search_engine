THE DAYS OF VENGEANCE

in the child’s game of “let’s pretend.” “Let’s pretend that these
books were never published. Let’s pretend that our brightest
students are not being picked off by them. Let’s pretend that this
flood of newsletters out of Tyler, Texas doesn’t exist. Let’s pre-
tend that Christian Reconstructionism is going to go away soon.
Let’s pretend that someone else will write a book that answers
them, and that it will be published early next year.” This strategy
is backfiring all over the country. The Christian Reconstruction-
ists own the mailing lists that prove it. When seminary pro-
fessors play a giant game of “let’s pretend,” it is only a matter of
time.

Frankly, it is highly doubtful that the average faculty mem-
ber of the typical Bible-believing seminary is ready to assign my
short paperback book aimed at teenagers: 75 Bible Questions
Your Instructors Pray You Won't Ask (1984). This is why I am
confident that the prevailing theological conservatism is about
to be uprooted, Seminary faculties that need to be on the offen-
sive against a humanist civilization are incapable of even de-
fending their own positions from cheap paperback Christian
books, let alone replace an entrenched humanist order.

I will put it as bluntly as [ can: Our eschatological opponents
will not attack us in print, except on rare occasions. They know
that we will respond in print, and that at that point they will be
stuck, They want to avoid this embarrassment at any price—
even the price of seeing their brightest young men join the
Christian Reconstructionist movement. And, quite frankly, that
suits us just fine. Heads, we win; tails, we win.

Defenseless Traditionalists

If any movement finds that it is being confronted by dedicated
opponents who are mounting a full-scale campaign, it is suicidal to
sit and do nothing, It is almost equally suicidal to do something
stupid, What generally happens is that the leaders of comfortable,
complacent, and intellectually flabby movements do nothing for
too long, and then in a panic they rush out and do a whole series
of stupid things, beginning with the publication of articles or
books that are visibly ineffectual in the eyes of the younger men
who would otherwise become the movement’s future leaders.

16. Published by Spurgeon Press, P.O. Box 7999, Tyler, Texas 75711,

xxviii
