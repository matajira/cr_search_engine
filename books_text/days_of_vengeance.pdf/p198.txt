4:9-11 PART THREE: THE SEVEN SEALS

and to render thanksgiving for all His benefits; and we are to re-
spond to all of this with music and singing. All of this is corpor-
ate, and that necessarily means fiturgy. This may mean certain
complex and involved changes in our habits and patterns of
worship, But God should have nothing less than the best. He is
the King, and worship means serving Him.

164
