20:4 PART FIVE: THE SEVEN CHALICES

upon their forehead and upon their hand (cf. 1:2, 9; 2:13;
12:9-11, 17; 15:2; 19:10). All these lived and reigned with Christ
for a thousand years. Man’s life has always fallen short of a
thousand years: Adam lived 930 years (Gen. 5:5), and Methu-
selah, whose life was the longest recorded in the Bible, lived
only 969 years before he died in the Great Flood (Gen, 5:27).27
{f his heirs had been faithful, David’s kingdom should have en-
dured “forever” — meaning that it should have lasted a thousand
years, until the Coming of Christ (2 Sam. 7:8-29; 1 Chron.
17:7-27; 2 Chron. 13:5; 21:7; Ps. 89:19-37; Isa. 9:7; 16:5; Jer.
30:9; Ezek. 34:23-24; Hos. 3:5; Luke 1:32-33); but, again, man
fell short. No one was able to bring in “the Millennium” —the
Thousand-Year Kingdom—until the Son of God appeared as
the Son of Man (the Second Adam) and Son of David. He ob-
tained the Kingdom for all His people.

Does this reign of the saints take place in heaven or on
earth? The answer should be obvious: both! The saints’ thrones
are in heaven, with Christ (Eph. 2:6); yet, with their Lord, they
exercise rule and dominion on earth (cf. 2:26-27; 5:10; 11:15).
Those who reign with Christ in His Kingdom are all those whom
He has redeemed, the whole Communion of Saints, whether
they are now living or dead (including Old Covenant believers).
In His Ascension, Jesus Christ brought us all to the Throne. As
the Te Deum exults:

When Thou hadst overcome the sharpness of death
Thou didst open the Kingdom of Heaven to all believers.

The reign of the saints is thus analogous to their worship:
The whole Church, in heaven and on earth, worships together
before the Throne of God, “tabernacling” in heaven (7:15; 12:12;
13:6). To ask whether or not the saints’ worship is heavenly or
earthly is to propose a false dilemma, for the Church is both
heavenly and earthly. Similarly, the Church’s sphere of rule in-

27. Based on a strict chronology, this seems to be a reasonable conclu-
sion, since Methuselah died in the Flood year (Methuselah was 187 when his
son Lamech was born, 369 when his grandson Noah was born, and hence 969
when the Flood came; see Gen. 5:25, 28; 7:6). More than a century before the
Flood, God declared the entire human race (except for Noah) to be worthy of
destruction (Gen. 6:1-8; 7:1); there is no apparent reason to exclude Mcthu-
selah from this sweeping condemnation.

514
