THE END OF THE BEGINNING 11:15

made apparent: He has placed Jews and Gentiles on equal foot-
ing in the Covenant. The destruction of apostate Israel and the
Temple revealed that God had created a new nation, a new Tem-
ple, as Jesus had prophesied to the Jewish leaders: “Therefore I
say to you, the Kingdom of God will be taken away from you,
and be given to a nation producing the fruit of it” (Matt. 21:43).
Later, Jesus told his disciples what would be the effect of the de-
struction of Jerusalem: “At that time will appear the sign of the
Son of Man in heaven” (Matt. 24:30). Marcellus Kik explains:
“The judgment upon Jerusalem was the sign of the fact that the
Son of man was reigning in heaven. There has been misunder-
standing due to the reading of this verse, as some have thought
it to be ‘a sign in heaven.’ But this is not what the verse says; it
says the sign of the Son of Man in heaven. The phrase ‘in
heaven’ defines the locality of the Son of Man and not of the
sign, A sign was not to appear in the heavens, but the destruc-
tion of Jerusalem was to indicate the rule of the Son of Man in
heaven.”!7

Kik continues: “The apostle Paul states in the eleventh chap-
ter of Romans that the fall of the Jews was a blessing to the rest
of the world. He speaks of it as the enriching of the Gentiles and
the reconciling of the world. The catastrophe of Jerusalem
really signalized the beginning of a new and world-wide king-
dom, marking the full separation of the Christian Church from
legalistic Judaism. The whole system of worship, so closely as-
sociated with Jerusalem and the Temple, received, as it were, a
death blow from God himself. God was now through with the
Old Covenant made at Sinai: holding full sway was the sign of
the New Covenant.”!®

Thus the Kingdom of God, the “Fifth Kingdom” prophesied
in Daniel 2, becomes universalized, as the heavenly choir sings:
The kingdom of the world has become the Kingdom of our
Lord, and of His Christ; and He will reign forever and ever. The

17. Marcellus Kik, Aa Eschatology of Victory (Nutley, NJ: The Presbyter-
ian and Reformed Publishing Co., 1971), p. 137. The common rendering in
modern versions of the Bible (“then the sign of the Son of Man will appear in
the sky”) simply reflects the unbiblical biases of a few translators and editors.
The more literal translation in the King James Version is what the Greek text
says. Cf. the discussion in Paradise Restored: A Biblical Theology of Domin-
ion (Ft, Worth, TX: Dominion Press, 1985), pp. 97-108.

18. Ibid., p. 138.

287
