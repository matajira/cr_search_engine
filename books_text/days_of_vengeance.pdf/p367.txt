LEVIATHAN AND BEHEMOTH 13:5-7

only the same old world, not Christ and His Kingdom. We do
not realize that we never get anywhere because we never leave
any place behind us.*"!

The Beast was given authority to act for forty-two months
and to make war with the saints and to overcome them. As I ob-
served above (see comments on 11:2), the period of 42 months
(or three and a half years, a broken seven) is a symbolic figure in
prophetic language, signifying a time of trouble, when the ene-
mies of God are in power, or when judgment is being poured
out, while God’s people wait for the coming of the Kingdom (as
we have already noted, the Beast oppressed the Old Covenant
saints for 42 generations, according to Matthew 1:1-17). Its pro-
phetic usage is not primarily literal, although it is interesting
that Nero’s persecution of the Church did in fact last a full 42
months, from the middle of November 64 to the beginning of
dune 68. This period of 42 months thus corresponds (but is not
necessarily identical) to the 42 months/1,260 days of 11:2-3 and
the “time, times, and half a time” of 12:14. During the time of
the Beast’s triumph he wields authority over the fourfold earth:
every tribe and people and tongue and nation. This was true of
the Roman Empire, as it was true of Beast in general. Satan ruled
“all the kingdoms of the world” (cf. Matt. 4:8-9) as their
“prince” (John 12:31; cf. Dan. 10:13, 20). His authority was
“legal,” after a sort, since Adam had abdicated the throne; yet it
was illegitimate as well. The Church Fathers make much of the
fact that the Second Adam won back the world from Satan’s do-
minion by just and lawful means, and not by force.'?

tt, Alexander Schmemann, For the Life of the World: Sacraments and Or-
thodoxy (New York: St. Vladimir’s Seminary Press, revised ed., 1973), p. 28.

12, Cf. the words of St. Irenaeus: “The all-powerful Word of God, who
never fails in justice, acted justly even in dealing with the Spirit of Rebellion.
For it was by persuasion, not by force, that He redeemed His own
property . . . for thus it behoved God to achieve His purpose: with the result
that justice was not infringed, and God’s original handiwork was saved from
perishing” (Against Heresies, v.i.1). St. Augustine adds: “Christ demonstrated
justice by his death, he promised power by his resurrection, What could be
more just than to go as far as the death of the cross, for the sake of justice?
What greater act of power than to rise from the dead, and ascend to heaven
with the very flesh in which he was slain? First justice conquered the devil, then
power; justice, because he had no sin and was most unjustly put to death by
the devil; power, because he lived again after death, never to die thereafter”
(Gn the Trinity, xiii.18).

333
