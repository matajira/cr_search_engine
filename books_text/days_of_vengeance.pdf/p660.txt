APPENDIX C

The Bible is very clear. The problem with the vast majority of
interpreters is that they still are influenced by the standards of self-
proclaimed autonomous humanism. Biblically, love is the fulfilling of
the faw (Rom. 13:8). Love thy neighbor, we are instructed. Treat him
with respect, Do not oppress ar cheat him. Do not covet his goods or
his wife, Do not steal from him. In treating him lawfully, you have ful-
filled the commandment to love him. In so doing, you have rendered
him without excuse on the day of judgment. God’s people are to
become conduits of God's gifts to the unregenerate.

This is not to say that every gift that we give to the lost must be
given in an attempt to heap coals of fire on their heads. We do not
know God’s plan for the ages, except in its broad outlines. We do not
know who God intends to redeem. So we give freely, hoping that some
might be redeemed and the others damned. We play our part in the sal-
vation of some and the damnation of others. For example, regenerate
marriage partners are explicitly instructed to treat their unregenerate
partners lawfully and faithfully. “For what knowest thou, O wife,
whether thou shalt save thy husband? or how knowest thou, O man,
whether thou shalt save thy wife” (I Cor. 7:16)? We treat our friends
and enemies lawfully, for they are made in the image of God. But we
are to understand that our honest treatment does make it far worse on
the day of judgment for those with whom we have dealt righteously
than if we had disobeyed God and been poor testimonies to them,
treating them unlawfully.

God gives rebels enough rope to hang themselves for ail eternity.
This is a fundamental implication of the doctrine of common grace.
The law of God condemns some men, yet it simultaneously serves as a
means of repentance and salvation for others (Rom. 5:19-20). The
same law produces different results in different people. What separates
men is the saving grace of Gad in election. The law af God serves as a
tool of final destruction against the lost, yet it also serves as a tool of
active reconstruction for the Christian. The law rips up the kingdom
of Satan as it serves as the foundation for the kingdom of God on
earth,

Christ is indeed the savior of all people prior to the day of judg-
ment (1 Tim. 4:10). Christ sustains the whole universe (Col. 1:17).
Without Him, no living thing could survive. He grants to His crea-
tures such gifts as time, faw, order, power, and knowledge. He grants
all of these gifts to Satan and his rebellious host. In answer to the
question, “Does God show His grace and mercy to all creation?” the
answer is emphatically yes. To the next question, “Does this mean that
God in some way demonstrates an attitude of favor toward Satan?”
the answer is emphatically no. God is no more favorable toward Satan

628
