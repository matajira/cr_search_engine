THE TRUE ISRAEL 7:4-8

pretence of distinguishing between what was discursively
thought and what intuitively conceived in a mind which
penetrated its images with intelligence and rooted its intel-
lective acts in imagination. . . .

“The reader who perseveres through the analyses which
follow may naturally ask, ‘How much of all this did the
congregations of the Seven Churches comprehend, when
the apocalyptic pastoral of their archbishop was read out
to them?’ The answer is, no doubt, that of the schematic
analysis to which we resort they understood nothing, be-
cause they were listening to the Apocalypse of St. John,
and not to the lucubrations of the present writer. They
were men of his own generation, they constantly heard the
Old Testament in their assemblies, and were trained by the
preacher (who might be St. John himself) to interpret it by
certain conventions. And so, without intellectual analysis,
they would receive the symbols simply for what they were.
They would understand what they would understand, and
that would be as much as they had time to digest.”]!°

Scholars have long puzzled over the order of the tribes in St.
John’s list. Obviously, Judah is named first because that is the
tribe of Jesus Christ; other than that, many have supposed that
the list is either haphazard (given the Biblical writers’— especi-
ally St. John’s—extreme attention to detail, this is highly un-
likely), or else permanently locked in mystery (this is just sheer
arrogance; we should always remember that, if we can’t answer
a question, someone probably will come along in the next hun-
dred years or so who will). As usual, however, Austin Farrer’s
explanation has the most to offer. Pointing out that the names
of the twelve tribes are written on the gates of the four-cornered
New Jerusalem (21:12), he proposes that the order of the tribes
carresponds to the order in which the gates are listed: east,
north, south, west. As we can see in the first diagram (which,
like the maps of the ancient world, is oriented toward the east},!!

10. Austin Farrer, A Rebirth of Images: The Making of St. John’s Apoca-
dypse (Gloucester, MA: Peter Smith, [1949] 1970), pp. 20f.

11, Orient means eust; thus, if you are truly “oriented,” you are “casted”
already, placed so that you are facing the right direction (which is usually, but
not always, east).

209
