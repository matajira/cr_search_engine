THE KING ON MOUNT ZION 14:12-13

1 Pet. 3:19; 4:6). Death is now the entrance to communion in
glory with Christ and the departed saints. Jesus Christ has deliv-
ered us from the ultimate fear of death; we can say, in the fam-
ous lines of John Donne’s “Death Be Not Proud”:

One short sleepe past, wee wake eternally,
And death shall be no more; death, thou shalt die.

The early Christians understood that death had been con-
quered by the resurrection of Christ; this theme recurs repeat-
edly in their writings. Again and again one is struck with the
note of victory in the attitude of the martyrs as they faced
death, St, Athanasius wrote of this fact in his famous defense of
the Christian faith: “All the disciples of Christ despise death;
they take the offensive against it and, instead of fearing it, by
the sign of the cross and by faith in Christ trample on it as on
something dead. Before the divine sojourn of the Saviour even.
the holiest of men were afraid of death, and mourned the dead
as those who perish. But now that the Saviour has raised His
body, death is no longer terrible, but all those who believe in
Christ tread it underfoot as nothing, and prefer to die rather
than to deny their faith in Christ, knowing full well that when
they die they do not perish, but live indeed, and become incor-
ruptible through the resurrection. But that devil who of old
wickedly exulted in death, now that the pains of death are
loosed, he alone it is who remains truly dead. There is proof of
this tao; for men who, before they believe in Christ, think death
horrible and are afraid of it, once they are converted despise it
so completely that they go eagerly to meet it, and themselves
become witnesses of the Saviour’s resurrection from it. Even
children hasten thus to die, and not men only, but women train
themselves by bodily discipline to meet it. So weak has death
become that even women, who used to be taken in by it, mock it
now as a dead thing robbed of all its strength. Death has
become like a tyrant who has been completely conquered by the
legitimate monarch; bound hand and foot as he now is, the
passers-by jeer at him, hitting him and abusing him, no longer
afraid of his cruelty and rage, because of the king who has con-
quered him. So has death been conquered and branded for what
it is by the Saviour on the cross. It is bound hand and foot, all

367
