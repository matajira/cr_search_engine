COME, LORD JESUS! 22:3-4

into judgment, but has passed out of death into life. Truly, truly,
T say to you, an hour is coming, and now is, when the dead shall
hear the Voice of the Son of God; and these who hear shail live.
(ohn 5:24-25)

In the same way, St. John expects the healing virtues of the
Crass to give Life to the nations as nations, in this world; the
nations, he has told us, are made up of “those whase names are
written in the Lamb’s Book of Life,” since the nations as such
are admitted into the Holy City (21:24-27). The River of Life is
flowing now (John 4:14; 7:37-39), and will continue to flow in an
ever-increasing stream of blessing to the earth, healing the na-
tions, bringing an end to lawlessness and warfare (Zech. 14:8-11;
ef. Mic. 4:1-4). This vision of the Church's glorious future,
earthly and heavenly, mends the fabric that was torn in Genesis.
In Revelation we see Man redeemed, brought back to the Moun-
tain, sustained by the River and the Tree of Life, regaining his
lost dominion and ruling as a priest-king over the earth. This is
our privilege and heritage now, definitively and progressively, in
this age; and it will be ours fully in the age to come.

3-4 Thus there shall no longer be any Curse, in fulfillment of
the ancient promises:

Thus says the Lord Gop: On the Day that I cleanse you from
all your iniquities, I will cause the cities to be inhabited, and the
waste places will be rebuilt. And the desolate iand will be culti-
vated instead of being a desolation in the sight of everyone who
passed by. And they will say, “This desolate land has became like
the Garden of Eden; and the waste, desolate, and ruined cities
are fortified and inhabited.” Then the nations that are left round
about you will know that I, the Lorp, have rebuilt the ruined
places; I, the Lorp, have spoken and will do it. (Ezek. 36:33-36)

The Throne of God and of the Lamb shall be in the Holy
City, as St. John implied in 21:3, 11, 22-23. It is striking that the
citizens are called His servants —an expression that is primarily
used to describe prophets (cf. 1:1; 10:7; 11:18; 15:3; 19:2, 5 [cf.
18:24]; 22:6, 9). As we have seen, this has been a significant
theme in Revelation, the fulfillment of the Old Covenant hope
of communion with God: All the Lorp’s people are prophets,
for the Lorp has put His Spirit upon them (Num. 11:29). There-

569
