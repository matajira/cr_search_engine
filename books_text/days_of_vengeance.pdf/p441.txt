JUDGMENT FROM THE SANCTUARY 16:12

careless which side won and glorying in the calamities of the
state.”20

Again St. John draws attention to the impenitence of the
apostates. Their response to God’s judgment is only greater re-
bellion—yet their rebellion is becoming increasingly impotent:
And they gnawed their tongues because of pain, and they blas-
phemed the God of heaven because of their pains and their
sores; and they did not repent, so as to give Him glory. A distin-
guishing mark of the Chalice-plagues is that they come all at
once, with no “breathing space” between them. The plagues are
bad enough one at a time, as in the judgments on Egypt. But
these people are still gnawing their tongues and blaspheming God
on account of their sores —the sores that came upon them when
the First Chalice was poured out. The judgments are being
poured out so quickly that each successive plague finds the peo-
ple still suffering from all those that preceded it. And, because
their character nas not been transformed, they do not repent.
The notion that great suffering produces godliness is a myth.
Only the grace of God can turn the wicked from rebellion; but
Israel has resisted the Spirit, to its own destruction.

12 Corresponding to the Sixth Trumpet (9:13-21), the Sixth
Chalice is poured out upon the great river, the Euphrates; and
its water was dried up, that the way might be prepared for the
kings from the rising of the sun. As we saw on 9:14, the
Euphrates was Israel’s northern frontier, from which invading
armies would come to ravage and oppress the Covenant people.
The image of the drying of the Euphrates for a conquering army
is taken, in part, from a stratagem of Cyrus the Persian, who
conquered Babylon by temporarily turning the Euphrates out of
its course, enabling his army to march up the riverbed into the
city, laking it by surprise.?" The more basic idea, of course, is
the drying up of the Red Sea (Ex. 14:21-22) and the Jordan River
(Josh. 3:9-17; 4:22-24) for the victorious people of God, Again
there is the underlying note of tragic irony: Israel has become

20. Tacitus, The Histories, iii.83; trans, Kenneth Wellesley (New York: Pen-
guin Books, 1964, 1975), pp. 197f.

21. Herodotus, History, i.191; sce the prophecies of this in Jer. $0:38; 51:32,
36.

407
