REVELATION AND THE COVENANT

Biblical prophecy is not “prediction” in the occult sense of Nos-
tradamus, Edgar Cayce, and Jean Dixon), To a man, the proph-
ets were God's legal emissaries to Israel and the nations, acting
as prosecuting attorneys bringing what has become known
among recent scholars as the “Covenant Lawsuit.”

That Biblical prophecy is not simply “prediction” is indi-
cated, for example, by God’s statement through Jeremiah:

At one moment I might speak concerning a nation or con-
cerning a kingdom to uproot, to pull down, or to destroy it; if
that nation against which I have spoken turns from its evil, I will
telent concerning the calamity I planned to bring on it.

Or at another moment I might speak concerning a nation or
concerning a kingdom to build up or to plant it; if it does evil in
My sight by not obeying My voice, then I will repent of the good
with which I had promised to bless it, Jer. 18:7-10)

The purpose of prophecy is not “prediction,” but evaluation
of man’s ethical response to God’s Word of command and
promise. This is why Jonah’s prophecy about Nineveh did not
“come true”; Nineveh repented of its wickedness, and the
calamity was averted, Like the other Biblical writings, the Book
of Revelation is a prophecy, with a specific covenantal orienta-
tion and reference. When the covenantal context of the proph-
ecy is ignored, the message St. John sought to communicate is
lost, and Revelation becomes nothing more than a vehicle for
advancing the alleged expositor’s eschatological thearies.

Let us consider a minor example: Revelation 9:16 tells us of a
great army of horsemen, numbering “myriads of myriads.” In
some Greek texts, this reads two myriads of myriads, and is
sometimes translated 200 million, All sorts of fanciful and con-
trived explanations have been proposed for this. Perhaps the
most well-known theory of recent times is Hal Lindsey’s opinion
that “these 200 million troops are Red Chinese soldiers accom-
panied by other Eastern allies. It’s possible that the industrial
might of Japan will be united with Red China. For the first time
in history there will be a full invasion of the West by the
Orient.”?* Such fortunetelling may or may not be accurate

28. Hal Lindsey, There's a New World Coming (Eugene, OR: Harvest
House Publishers, 1973), p. 140.

ll
