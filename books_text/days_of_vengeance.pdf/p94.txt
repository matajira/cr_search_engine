1:4-6 PART ONE: THE SON OF MAN

wicked. '5

Second, St. John speaks of the Holy Spirit as the seven Spir-
its who are before His Throne. Although some have tried to see
this as a reference to seven angels, it is inconceivable that grace
and peace can originate from anyone but God. The Person
spoken of here is clearly on a par with the Father and the Son,
The picture of the Holy Spirit here (as also in 3:1; 4:5; 5:6) is
based on Zechariah 4, in which the prophet sees the Church asa
lampstand with seven lamps, supplied without human agency by
an unceasing flow of oil through “seven spouts to the seven
lamps” (v, 2)— the interpretation of which is, as God tells Zech-
ariah: “Not by might, nor by power, but by My Spirit” (v. 6).
The Holy Spirit’s filling and empowering work in the Church is
thus described in terms of the number seven, symbolizing full-
ness and completeness. So it is here in Revelation: “To the seven
churches . . . grace and peace be unto you. . . from the seven
Spirits.” And the Spirit’s work in the Church takes place in
terms of God’s dominion and majesty, before His Throne. This
is, in fact, a marked emphasis in the Book of Revelation: The
word Throne occurs here forty-six times (the New Testament
book that comes closest to matching that number is the Gospel
of Matthew, where it is used only five times). The Revelation is a
book, above all, about rude: it reveals Jesus Christ as the Lord
of history, restoring His people to dominion through the power
of the Holy Spirit.

The word Throne is used particularly in Scripture to refer to
God’s official court, where He receives official worship from His
people on the Sabbath. '* The entire vision of the Revelation was
seen on the Lord’s Day (1:10)—the Christian day of corporate,

15. There are several good discussions of the various meanings of Coming
in Scripture. See Oswald T. Allis, Prophecy and the Church (Grand Rapids:
Baker Book House, 1945, 1947), pp. 175-91; Loraine Boettner, The Millen-
nium, pp. 252-62; Roderick Campbell, fsrael and the New Covenant (Tyler,
TX: Geneva Ministries, [1954] 1983), pp. 68-80; David Chilton, Paradise Re-
stored, pp. 67-15, 97-105; Geerhardus Vos, The Pauline Eschatology (Grand
Rapids: Baker Book House, 1930), pp. 70-93.

16. See, for example, 1 Chron. 28:2; Ps. 132:7-8, 13-14; Isa. 11:10. Cf. Mere-
dith G. Kline, fmages of the Spirit (Grand Rapids: Baker Book House, 1980),
pp. 20f., 39ff., 46, 111ff, As Geerhardus Vos observed, the significance of the
Tabernacle in the Old Testament is that “it is the palace of the King in which
the people render Him homage” (Biblical Theolagy: Old and New Testaments
[Grand Rapids: William B. Eerdmans Publishing Co., 1948], p. 168).

60
