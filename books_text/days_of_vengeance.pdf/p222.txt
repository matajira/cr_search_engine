6:3-4 PART THREE: THE SEVEN SEALS

It is Christ who is the Conqueror par excellence. All events
in history are at His command, and it is entirely appropriate
that He should be the One represented here as the leader of the
judgments of God. He is the Center of history, and it is He who
brings judgments upon the Land. His opening of the New Cove-
nant guaranteed the fall of Israel; as He conquered to open the
Book, so He rode out in victory to implement the meaning of
the Book in history. He rode forth at His Resurrection and
Ascension as the already victorious King, conquering and to
conquer, extending the applications of His once-for-all, defini-
tive victory throughout the earth. And we should take special
notice of the awful judgments following in His train. The
Horsemen represent the forces God always uses in breaking dis-
obedient nations, and now they are turned against His covenant
people. The same holds true, of course, for all men and nations.
All attempts to find peace and safety apart from Jesus Christ are
doomed to failure. The nation that will not submit will be
crushed by His armies, by the historical forces that are constantly
at His absolute disposal.

There are differences between this vision of Christ and that
in Revelation 19. The primary reason for this is that in Chapter
19, Christ is seen with a sword proceeding out of His mouth,
and the vision symbolizes His conquest of the nations after a.p.
70 with the Gospel. But that is not in view during the breaking
of the seals. Here, Christ is coming against His enemies in judg-
ment. He is coming, not to save, not to heal, but to destroy. The
awful and terrifying riders who follow Him are not messengers
of hope but of wrath. Israel is doomed.

3-4 The Lamb breaks the Second Seal, and St. John hears
the second living creature saying: Come! In answer to the call, a
rider on a blood-red horse comes forth, who is granted by God
the power to take peace from the Land, and that men should
slay one another; and a great sword is given to him. This second
Tider, standing for war, shows how utterly depraved man is. God
does not have to incite men to fight against each other; He sim-
ply orders His angels to take away the conditions of peace. Ina
sinful world, why are there not more wars than there are? Why
is there not more bloodshed? It is because there are restraints on
man’s wickedness, on man’s freedom to work out the consistent

188
