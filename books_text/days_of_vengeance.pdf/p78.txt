INTRODUCTION

tempted to compromise with the statism and false religions of
their day, and they needed this message of Christ’s absolute
dominion over all, that they might be strengthened in the war-
fare to which they were called.

And we need this message also. We too are subjected daily to
the threats and seductions of Christ's enemies. We too are asked
—even by fellow Christians—to compromise with modern
Beasts and Harlots in order to save ourselves (or our jobs or
property or tax exemptions). And we too are faced with a
choice: surrender to Jesus Christ or surrender to Satan. The
Revelation speaks powerfully today, and its message to us is the
same as it was to the early Church: that “there is not a square
inch of ground in heaven or on earth or under the earth in which
there is peace between Christ and Satan”;® that our Lord de-
mands universal submission to His rule; and that He has predes-
tined His people to victorious conquest and dominion over all
things in His name. We must make no compromise and give no
quarter in the great battle of history. We are commanded to win.

A Note on the Text

T do not profess to be a textual critic. Nevertheless, in order to
produce a detailed commentary, it was necessary to decide one
way or another about which New Testament textual tradition to
follow. The translation in this commentary is based largely on
the recommendations of Hodges and Farstad in their “Majority
Text” Greek New Testament.® The basic arguments for the Ma-
jority Text position have been presented in the works of Jakob
van Bruggen,?! Wilbur N. Pickering,®? Harry A. Sturz,®? and

89. Cornelius Van Til, Essays on Christian Education (Nutley, NJ: Presby-
terian and Reformed Publishing Co., 1977), p. 27.

90. Zane C. Hodges and Arthur L, Farstad, The Greek New Testament Ac-
cording to the Majority Text (Nashvilie: Thomas Nelson Publishers, 1982).
That is to say, where the evidence presented by Hodges and Farstad seems un-
equivocal, I have foltowed it; where il is less clear, I have fcll free to disagree.

91. Jakob van Bruggen, The Ancient Text of the New Testament (Winni-
peg: Premicr Printing Ltd., 1976); idem, The Future of the Bible (Nashville:
Thomas Nelson Publishers, 1978).

92. Wilbur N, Pickering, The Identity of the New Testament Text (Nash-
ville: Thomas Nelson Publishers, 1977).

93. Harry A. Sturz, The Byzantine Text-Type in New Testament Texiual
Criticism (Nashville: Thomas Nelson Publishers, 1984), Sturz takes a much

44
