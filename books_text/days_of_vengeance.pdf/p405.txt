THE KING ON MOUNT ZION 14:14-16

the vine of the Land, and threw it into the great winepress of
the wrath of God.

20 And the winepress was trodden outside the City, and blood
came out from the winepress, up to the horses’ bridles, for
sixteen hundred stadia.

14-16 These verses form the centerpiece of the whole sec-
tion, verses 6-20. We have seen three angels making proclama-
tions to the Land of Israel (v. 6-13); three more will appear, to
perform symbolic actions over the Land (v. 15, 17-20); and in the
center is a white Cloud, and sitting on the Cloud One like a Son
of Man, having a golden crown on His head. This is the familiar
Glory-Cloud, with which Christ was clothed in 10:1; now it is
white, and not dark as on Sinai (Ex. 19:16-18; cf. Zeph. 1:14-15).
St. John’s reason for referring to the Cloud in this context can
be discerned from his connecting it with the Son of Man. The
reference is to Daniel’s prophecy of the Coming of the Messiah
to His inauguration as universal King—a vision which follows
his prophecy of the Beasts with seven heads and ten horns:

I kept looking in the night visions,

And behold, with the Clouds of heaven
One like a Son of Man was coming,

And He came up to the Ancient of Days
And was presented before Him.

And to Him was given dominion,

Glory, and a kingdom,

That all the peoples, nations, and men of every language
Might serve Him.

His dominion is an everlasting dominion
Which will not pass away;

And His kingdom is one

Which will not be destroyed. (Dan. 7:13-14)

St. John’s point is clear: Let the Beasts do their worst—the
Son of Man has ascended in the Clouds and received everlasting
dominion over ail peaples and nations! His Kingdom will never
be overthrown; He will never have a successor. It is clear also
that this is a vision, not of some future coming to earth, but of

371
