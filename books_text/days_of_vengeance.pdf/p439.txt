JUDGMENT FROM THE SANCTUARY 16:10-11

of the Gentiles fell. And Babylon the Great was remembered
before God, to give her the cup of the wine of His fierce
wrath.

20 And every island fled away, and the mountains were not
found.

21 And great hail, about the weight of a talent, comes down
from heaven upon the men; and the men blasphemed God
because of the plague of the hail, because its plague is ex-
ceedingly great.

The symbolic targets of the first four Chalices were the
elements of the physical creation: Land, sea, waters, and the
sun, With the last three plagues, the consequences of the angelic
attack are more “political” in nature: the disruption of the
Beast’s kingdom; the War of the great Day of God; and the Fall
of “Babylon.”

10-11 Although most of the judgments throughout Revela-
tion are aimed specifically at apostate Israel, the heathen who
join Israel against the Church come under condemnation as
well, Indeed, the Great Tribulation itself would prove to be “the
hour of testing, that hour which is to come upon fhe whole
world, to test those who dwell upon ‘he Land” (3:10). The fifth
angel therefore pours out his Chalice upon the throne of the
Beast; and, even as the sun’s heat is scorching those who wor-
ship the Beast, the lights are turned out on his kingdom, and it
becomes darkened —a familiar Biblical symbol for political tur-
moil and the fall of rulers (cf. Isa. 13:9-10; Amos 8:9; Ezek.
32:7-8). The primary significance of this plague is still the judg-
ment on Israel, for (in terms of the message of Revelation) that
was the throne and kingdom of the Beast. Moreover, as we shall
see, the people who suffer from the Fifth Chalice are identified
as suffering as well from the First Chalice, which was poured out
upon the Land, upon the Israelite worshipers of the Beast (v, 2).

It is also likely, however, that this judgment partially corre-
sponds to the wars, revolutions, riots, and “world-wide convul-
sions”'5 that racked the Empire after Nero committed suicide in
June 68. F. W. Farrar writes in this connection of “the horrors
inflicted upon Rome and Romans in the civil wars by provincial

15. Cornelius Tacitus, The Histories, iii.49.

405
