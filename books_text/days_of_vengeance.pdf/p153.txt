3

THE DOMINION MANDATE

Sardis: Judgment on the Dead (3:1-6)

1 And to the angel of the church in Sardis write: He who has
the seven Spirits of God, and the seven stars, says this: I
know your deeds, that you have a name that you are alive,
but you are dead.

2 Wake up, and strengthen the things that remain, which were
about to die; for ] have not found your deeds completed in
the sight of My God.

3 Remember therefore what you have received and heard; and
keep it, and repent. If therefore you will not wake up, I will
come upon you like a thief, and you will not know at what
hour I will come upon you.

4 But you have a few people in Sardis who have not soiled
their garments; and they will walk with Me in white; for they
are worthy.

5 He who overcomes shall thus be clothed in white garments;
anid I will not erase his name from the Book of Life, and I will
confess his name before My Father, and before His angels.

6 He who has an ear, let him hear what the Spirit says to the
churches.

1 To the bishop of the church in Sardis, Christ announces

Himself as the One who has the seven Spirits of God. As we
have seen (on 1:4) this is a term for the Holy Spirit who, as the

Nicene Creed declares, “proceeds from the Father and the Son.

Christ also possesses the seven stars, the angels of the churches
(1:16, 20). The rulers of the churches are owned by Him and are
at all points accountable to Him. And the elders in Sardis des-
perately needed to be reminded of this, for they had allowed the

church to die.
1 know your deeds, the Lord tells them. You have a name

119
