11:8-10 PART FOUR: THE SEVEN TRUMPETS

not permit their dead bodies to be laid in a tomb (cf. 1 Kings
13:20-22; Jer. 8:1-2; 14:16; 16:3-4), The desire for insertion into
the Promised Land in death was a central concern to the faithful
Witnesses of the Old Covenant, as a pledge of their future resur-
tection (Gen. 23; 47:29-31; 49:28-33; 50:1-14, 24-26; Ex. 13:19;
Josh. 24:32; 1 Sam. 31:7-13; Acts 7:15-16; Heb. 11:22). The
oppression of the Kingdom of priests by the heathen was often
expressed in these terms:

O God, the nations have invaded Thine inheritance;

They have defiled Thy holy Temple;

They have laid Jerusalem in ruins.

They have given the dead bodies of Thy servants for food to the
birds of the heavens,

The flesh of Thy godly ones to the beasts of the earth.

They have poured out their blood like water round about
Jerusalem;

And there was no one to bury them. (Ps. 79:1-3)

The irony, however, is that it is now those who dwell on the
Land—the Jews themselves (cf. 3:10)—who join with the
heathen nations in oppressing the righteous. The apostates of
Israel rejoice and make merry; and they will send gifts to one
another, because these two prophets tormented those who dwell
on the Land (cf, Herod’s party, during which John was impris-
oned and then beheaded: Matt. 14:3-12}. The price of the
world’s peace was the annihilation of the prophetic Witness;
Israel and the heathen world united in their evil gloating at the
destruction of the prophets, whose faithful double witness had
tormented the disobedient with conviction of sin, driving them
to commit murder (cf. Gen. 4:3-8; 1 John 3:11-12; Acts 7:54-60).
Natural enemies were reconciled to each other through their
joint participation in the murder of the prophets. This was espe-
cially true in their murder of Christ: “Now Herod and Pilate
became friends with one another that very day; for before they
had been at enmity with each other” (Luke 23:12). At Christ’s
death all manner of people rejoiced and mocked: the rulers, the
priests, the competing religious factions, the Roman soldiers,
the servants, the criminals; all joined in celebrating His death
(cf. Matt. 27:27-31, 39-44; Mark 15:29-32; Luke 22:63-65;
23:8-12, 35-39); all sided with the Beast against the Lamb (Iohn

282
