18:11-17a PART FIVE: THE SEVEN CHALICES

and jewelled cups, rings, and other workmanship of precious
metals; glass, silks, fine linen, woolen stuffs, purple, and costly
hangings; essences, ointments, and perfumes, as precious as
gold; articles of food and drink from foreign lands—in short,
what India, Persia, Arabia, Media, Egypt, Italy, Greece, and
even the far-off lands of the Gentiles yielded, might be had in
these bazaars. Ancient Jewish writings enable us to identify no
fewer than 118 different articles of import from foreign lands,
covering more than even modern luxury has devised.”!!

St. John’s list of trade goods divides into several sections,
generally of four items each; the prosaic, businesslike enumera-
tion concludes with a shock:

1) cargoes of gold, silver, precious stones, and pearls;

2) of fine linen, purple, silk, and scarlet; !2

3) and every kind of citron wood, every article of ivory, and
every article made from very costly wood, bronze, iron, and
marble;

4) and cinnamon, incense, perfume, and frankincense;

5) and wine, olive oil, fine flour, and wheat;

6) and sheep and cattle, even of horses, of chariots, and of
bodies;

7) and souls of men,

The final phrase, adapted from the description of Tyre’s
slave traffic in Ezekiel 27:13, is applied to Jerusalem’s spiritual
bondage of men’s souls. As St. Paul noted in his contrast of the
earthly, apostate Jerusalem with the Church, the heavenly City
of God: “The present Jerusalem . . . is in slavery with her chil-
dren,” while “the Jerusalem above is free; She is our Mother”
(Gal. 4:25-26). Jerusalem trafficked in many goods, from all
over the world. In keeping with the promises of Leviticus 26 and
Deuteronomy 28, God had made her into a great commercial
center. But she abused God’s gifts: Her most basic trade was in
human souls. Instead of fulfilling her proper function as the

ll. Alfred Edersheim, The Life and Times af Jesus the Messiah, two vols.
(McLean, YA: MacDonald Publishing Co., n.d.), Vol. 1, p. 116.

12. As mentioned earlier (on 17:4), this may well be a reference to the Tem-
ple curtain, a “Babylonian tapestry embroidered with blue, scarlet and purple,
and fine linen, wrought with marvelous craftsmanship.” Josephus, The Jewish
Wear, v.v.4.

456
