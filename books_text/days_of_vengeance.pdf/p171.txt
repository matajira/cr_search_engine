THE DOMINION MANDATE 3:19-20

declares, I reprove and discipline. A characteristic of those who
are true sons of God, and not bastards (cf. Heb. 12:5-1) is their
response to rebuke and discipline. All Christians need reproof
and correction at times, and some more than others; what is im-
portant is whether or not we heed the warning, and mend our
ways. As far as Laodicea has fallen, it can still be restored if it
Tenews its obedience and becomes faithful to God’s Word: Be
zealous therefore, and repent!

At this point Jesus speaks some of the most beautiful words
in all the Bible, in what is perhaps the most well-known New
Testament verse aside from John 3:16, Behold, I stand at the
door and knock; if anyone hears My voice and opens the door, I
will come in to him, and will dine with him, and he with Me.
Several Reformed commentators have pointed out the wide-
spread abuse of this passage by modern evangelicals, who rip
the verse from its context as a message to the elders of a church,
and turn it into a watered-down, Arminian request from a weak
and helpless deity who is at the mercy of man. We must remem-
ber that Christ is speaking here as the Amen, the faithful and
true Witness, the Creator and Sovereign Lord of all. He is not
making a feeble plea, as if He did not rule history and predestine
its most minute details; He is the King of Kings, who makes war
on His enemies and damns them to everlasting flames, Nor is he
speaking to people in general, for He is directing His message to
His Church; nor, again, is he simply speaking to Christians as
individuals, but to Christians as members of the Church. This
verse cannot be made to serve the purposes of Arminian, sub-
jective individualism without violently wrenching it from its
covenantal and textual context.!9

Nevertheless, there is a distortion on the other side that is
just as serious. It will not do merely to point out the failures of
Arminians to deal satisfactorily with this text, for Calvinists
have traditionally been at fault here ec well, Reformed worship
tends to be overly intellectual, centered around preaching. In
the name of being centered around the Word, it is actually often

19. Of course, the Lord offers Himself to people outside the Kingdom as
well: Even the dogs are given crumbs from the children’s table (Malt.
15:21-28); and the king in Christ’s parable (Luke 14:23) sent his servants out to
compel the Gentiles to come in. But Christ's offer of salvation is never made
outside the context of the Covenant, the Kingdom, and the Church.

137
