THE END OF THE BEGINNING 11:7

mate godhood instead. By submitting to a beast (the Serpent)
they themselves became “beasts” instead of gods, with the
Beast’s mark of rebellion displayed on their foreheads (Gen.
3:19); even in redemption they remained clothed with the skins
of beasts (Gen. 3:21).8 A later picture of the Fall is displayed in
the fall of Nebuchadnezzar, who was, like Adam, “the king of
kings, to whom the God of heaven has given the kingdom, the
power, the strength, and the glory” (Dan. 2:37). Yet, through
pride, through seeking autonomous godhood, he was judged:
“And he was driven away from mankind and began eating grass
like cattle, and his body was drenched with the dew of heaven,
until his hair had grown like eagles’ feathers and his nails like
birds’ claws” (Dan. 4:33). Man’s rebellion against God is also
imaged by the beasts’ rebellion against man; thus the wicked
persecutors of Christ at the crucifixion are called “dogs” and
“bulls of Bashan,” and are likened to “a ravening and roaring
lion” (Ps. 22:12-13, 16).

Another image of the “beastliness” of rebellion was contained
in the Old Covenant sacrificial/dietary requirements against
“unclean” animals, as James Jordan observes: “All unclean ani-
mals resemble the serpent in three ways. They eat ‘dirt’ (rotting
carrion, manure, garbage), They move in contact with ‘dirt’
{crawling on their bellies, fleshy pads of their feet in touch with
the ground, no scales to keep their skin from contact with their
watery environment), They revolt against human dominion,
killing men or other beasts. Under the symbolism of the Old
Covenant, such Satanic beasts represent the Satanic nations
(Lev. 20:22-26), for animals are ‘images’ of men.° To eat Satanic
animals, under the Old Covenant, was to ‘eat’ the Satanic life-
style, to ‘eat’ death and rebellion.”!?

The enemy of God and the Church is thus always Beast, in

8. Representing the restored image of God, the priests were clothed in veg-
etables (linen) rather than in animals (wool); they were forbidden to wear the
skins of beasts, because they produced sweat (Bzek. 44:17-18; cf. Gen. 3:19).
On ‘judicial godhood” and the clothing of Adam and Eve with skins, see
James 8. Jordan, “Rebellion, Tyranny, and Dominion in the Book of
Genesis,” in Gary North, ed., Christianity and Civilization 3 (1983): Tactics of
Christian Resistance, pp. 43-47.

9. Cf. Prov, 6:6; 26:11; 30:15, 19, 24-31; Dan. 5:21; Ex. 13:2, 13.

10. James B. Jordan, The Law of the Covenant: An Exposition af Exodus
24-23 (Tyler, TX: Institute for Christian Economics, 1984), p. 122,

279
