CHRISTUS VICTOR 5111-14

priests to our God; and they will reign upon the earth.)5

Christ has purchased His people out of the nations, not only
to redeem them from sin, but to enable them to fulfill God’s ori-
ginal Dominion Mandate for man, As the Second Adam, Christ
sets His New Creation the task Adam forfeited —this time, how-
ever, on the unshakeable foundation of His death, resurrection,
and ascension. Salvation has a purpose, a saving fo as wellasa
saving from, Christ has made His people to be kings and priests
to our God, and has guaranteed their destiny: They will reign
upon the earth. This shows us the direction of history: The re-
deemed of the Lord, already a nation of kingly priests, are mov-
ing toward the complete dominion God had planned as His ori-
ginal program for man. In Adam it had been lost; Jesus Christ,
the Second Adam, has redeemed us and restored us to our royal
priesthood, so that we will reign upon the earth. Through the
work of Christ the definitive victory over Satan has been won.
We are promised increasing victories, and increasing rule and
dominion, as we bring the Gospel and law of the great King to
fruition throughout the world.

11-14 In response to the praise of the four living creatures
and the twenty-four elders, the entire choir of angels, compos-
ing myriads of myriads,'* and thousands of thousands, joins in
with a loud voice, proclaiming that the Lamb that was slain is,
on the basis of His Person and work, worthy to inherit all things
{the seven enumerated items indicating fullness) in heaven and
earth: power and riches and wisdom and might and honor and
glory and blessing. And, as if in joyful answer to this great dec-
laration of Christ’s universal inheritance, the whole (fourfold)
creation responds with praise, as a climax to this section of the
liturgy. Every created thing that is a) in heaven and b) on the
earth and c) under the earth and d) in the sea, and all things in
them —all of created reality becomes part of the cosmic chorus,

15, This outline is also suggested by Moses Stuart, A Commentary on the
Apocalypse, 2 vols. (Andover: Allen, Morrill and Wardwell, 1845), Vol. 2, p.
134,

16. Literally, a #tyriad is 10,000; but it is often, especially in the plural, used
in a more vague sense to mean “a very large number.” Myriads of myriads ab-
viously means simply “countless thousands.”

179
