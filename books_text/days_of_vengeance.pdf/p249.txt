THE TRUE ISRAEL WD

Abrahamic promise (Gen. 15:5; 22:17-18); and thus the Church
becomes the whole world. The salvation of Israel alone had
never been God’s intention; He sent his Son “that the worid
should be saved through Him” (John 3716-17), As the Father
said to the Son, in planning the Covenant of Redemption:

It is too small a thing that You should be My Servant
To raise up the tribes of Jacob,

And to restore the preserved ones of Israel;

I will also make of You a Light to the nations

So that My salvation may reach to the end of the earth.
(Isa. 49:6)

The actual number of the saved, far from being limited to
mere tens of thousands, is in reality a multitude that no one
could count, so vast that it cannot be comprehended. For the
fact is that Christ came to save the world, Traditionally—
although Calvinists have been technically correct in declaring
that the full benefits of the atonement were intended only for
the elect—both Calvinists and Arminians have tended to miss
the point of John 3:16. That point has been beautifully summar-
ized by Benjamin Warfield; “You must not fancy, then, that God
sits helplessly by while the world, which He has created for
Himself, hurtles hopelessly to destruction, and He is able only
to snatch with difficulty here and there a brand from the univer-
sal burning. The world does not govern Him in a single one of
its acts: He governs it and ieads it steadily onward to the end
which, from the beginning, or ever a beam of it had been laid,
He had determined for it... . Through all the years one in-
creasing purpose runs, one increasing purpose: the kingdoms of
the earth become ever more and more the Kingdom of our God
and His Christ. The process may be slow; the progress may ap-
pear to our impatient eyes to lag. But it is God who is building:
and under His hands the struciure rises as steadily as it does
slowly, and in due time the capstone shail be set into its place,
and to our astonished eyes shall be revealed nothing less than a
saved world.”'4

M4, Benjamin B. Warfield, from a sermon on John 3:16 entitled “God's Im-
measurable Love,” in Biblical and Theological Studies (Philadelphia: Presby-
terian and Reformed Publishing Co., 1968), pp. SI8f.

215
