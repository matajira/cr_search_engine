REVELATION, EZEKIEL, AND THE LECTIONARY

make literary allusions to Ezekiel. He follows him, step by step
—so much so that Philip Carrington could say, with only mild
hyperbole: “The Revelation is a Christian rewriting of Ezekiel.
Its fundamental structure is the same. Its interpretation depends
upon Ezekiel. The first half of both books leads up to the de-
struction of the earthly Jerusalem; in the second they describe a
new and holy Jerusalem. There is one significant difference.
Ezekiel’s lament over Tyre is transformed into a lament over
Jerusalem, the reason being that St. John wishes to transfer to
Jerusalem the note of irrevocable doom found in the lament
over Tyre. Here lies the real difference in the messages of the two
books. Jerusalem, like Tyre, is to go forever.”45 Consider the
more obvious parallels:**

. The Throne-Vision (Rev. 4/Ezek. 1)

. The Book (Rev. 5/Ezek. 2-3)

. The Four Plagues (Rev. 6:1-8/Ezek. 5)

. The Slain under the Altar (Rev. 6:9-11/Ezek. 6)

. The Wrath of God (Rev. 6:12-17/Ezek, 7}

. The Seal on the Saint’s Foreheads (Rev. 7/Ezek. 9)

. The Coals from the Altar (Rev. 8/Ezek. 10)

. No More Delay (Rev. 10:1-7/Ezek. 12)

. The Eating of the Book (Rev. 10;8-I1/Ezek. 2)

10. The Measuring of the Temple (Rev. 11:1-2/Ezek. 40-43)
11. Jerusalem and Sodom (Rey. 11:8/Ezek. 16)

12, The Cup of Wrath (Rev. 14/Ezek. 23)

13. The Vine of the Land (Rev. 14:18-20/Ezek. 15)

14, The Great Harlot (Rev. 17-18/Ezek. 16, 23}

15, The Lament over the City (Rev. 18/Ezek. 27)

16. The Scavengers’ Feast (Rev. 19/Ezek. 39)

17. The First Resurrection (Rev. 20:4-6/Ezek. 37)

18. The Battle with Gog and Magog (Rev. 20:7-9/Ezek. 38-39)
19, The New Jerusalem (Rev. 21/Ezek. 40-48)

20, The River of Life (Rev. 22/Ezek. 47)

CHOI AURWNe

As M. D. Gouider points out, the closeness of the two
books’ structure —the step-by-step “pegging” of Revelation with

45. Philip Carrington, The Meaning of the Revelation (London: SPCK,
1931), p. 65.

46. This list is based on Carrington (p. 64) and on M, D. Goulder, “The
Apocalypse as an Annual Cycle of Prophecies,” New Testament Studies 27,
No. 3 (April 1981), pp. 342-67.

21
