21:1 PART FIVE: THE SEVEN CHALICES

ing the earth has been completed, established on an entirely new
basis in the work of Christ. Earth’s original uninhabitable con-
dition of deep-and-darkness has been utterly done away with:
There is no longer any Sea or Abyss. There is heaven and earth,
but no “under-the-earth,” the abode of Leviathan. What St.
John reveals to us is the eschatological outcome of the compre-
hensive, cosmic reconciliation celebrated by St. Paul: “For it
was the Father’s good pleasure for all the fulness to dwell in
Him, and through Him to reconcile all things to Himself, hav-
ing made peace through the blood of His cross; through Him,
whether things on earth or things in heaven” (Col. 1:19-20).!

Yet this vision of the new heaven and earth is not to be inter-
preted as wholly future. As we shall see repeatedly throughout
our study of this chapter, that which is to be absolutely and
completely true in eternity is definitively and progressively true
now, Our enjoyment of our eternal inheritance will be a contin-
uation and perfection of what is true of the Church in this life.
We are not simply to look forward to the blessings of Revelation
21 in an eternity to come, but to enjoy them and rejoice in them
and extend them here and now. St. John was telling the early
Church of present realities, of blessings that existed already and
would be on the increase as the Gospel went forth and renewed
the earth.

Salvation is consistently presented in the Bible as re-
creation.? This is why creation language and symbolism are used
in Scripture whenever God speaks of saving His people. We
have seen how God’s deliverances of His people in the Flood
and the Exodus are regarded by the Biblical writers as provi-
sional New Creations, pointing to the definitive New Creation in
the First Advent of Christ. Thus, God spoke through Isaiah of
the blessings of Christ’s coming Kingdom:

For, behold, I create new heavens and a new earth;

And the former things shall not be remembered or come to mind.
But be glad and rejoice forever in what I create;

For behold, I create Jerusalem for rejoicing,

1. See John Murray, “The Reconciliation,” The Westminster Theological
Journal, XXIX (1966) 1, pp. 1-23; Colfected Writings, 4 vols. (Edinburgh: The
Banner of Truth Trust, 1976-82), Vol. 4, pp. 92-112.

2. See David Chilton, Paradise Restored: A Biblical Theology of Dominion
(Ft. Worth, TX: Dominion Press, 1985), pp. 23-26.

538
