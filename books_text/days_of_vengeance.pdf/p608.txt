22:6-7 PART FIVE: THE SEVEN CHALICES

still practice righteousness; and let the one who is holy, still
keep himself holy.

12 Behold, I am coming quickly, and My reward is with Me, to
render to every man according to what he has done.

13 [ am the Alpha and the Omega, the First and the Last, the
Beginning and the End.

14 Blessed are those whe do His commandments, that they may
have the right to the Tree of Life, and may enter by the gates
into the City.

15 Outside are the dogs and the sorcerers and the fornicators
and the murderers and the idolaters, and everyone who loves
and practices lying.

16 I, Jesus, have sent My angel to testify to you these things for
the churches. I am the Root and the Offspring of David, the
bright Morning Star.

17 And the Spirit and the Bride say: Come. And let the one
who hears say: Come. And let the one who is thirsty come;
let the one who wishes take the water of life without cost.

18 I testify to everyone who hears the words of the prophecy of
this book: If anyone adds to them, God shall add to him the
plagues which are written in this book;

19 and if anyone takes away from the words of the book of this
prophecy, God shall take away his part from the Tree of Life
and from the Holy City, which are written in this book.

20 He who testifies to these things says: Yes, I am coming
quickly! Amen. Come, Lord Jesust

21 The grace of the Lord Jesus Christ be with all the saints.
Amen.

6-7 The apostle’s final section reviews and summarizes the
central messages of the book. Appropriately, St. John’s angelic
guide begins by testifying that these words are faithful and true,
in keeping with the character of their Author (1:5; 3:14; 19:11; cf,
19:9; 21:5); they cannot fail to be fulfilled. And the Lord, the
God of the spirits of the prophets, sent His angel to show to His
servants the things which must shortly take place. The word
spirits here may refer to the “Seven Spirits” (cf. 1:4; 4:5), ie. the
Holy Spirit in His manifold operation through the prophets (cf.
19:10: “the Spirit of prophecy”), but it is possible also to under-
stand the expression in the sense of 1 Corinthians 14:32—the
spirit of each prophet in particular, In any case, St. John has re-
peatedly emphasized throughout his prophecy that “all the

574
