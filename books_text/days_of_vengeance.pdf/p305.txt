it

THE END OF THE BEGINNING

The Two Witnesses Against Jerusalem (1:1-14)

1 And there was given me a reed like a staff; and someone
said: Rise and measure the Temple of God, and the altar,
and those who worship in it.

2 And cast out the court that is outside the Temple, and do not
measure it; for it has been given to the nations; and they will
tread under foot the Holy City for forty-two months.

3 And I will grant authority to My two Witnesses, and they
will prophesy for twelve hundred and sixty days, clothed in
sackcloth.

4 These are the two olive trees and the two lampstands that
stand before the Lord of the earth,

5 And if anyone desires to harm them, fire proceeds out of
their mouth and devours their enemies; and if anyone would
desire to harm them, in this manner he must be killed.

6 These have the power to shut up the sky, in order that rain
may not fall during the days of their prophesying; and they
have power over the waters to turn them into blood, and to
smite the earth with every plague, as often as they desire.

7 And when they have finished their testimony, the Beast that
comes up out of the Abyss will make war with them, and
overcome them and kill them.

8 And their dead bodies will lie in the street of the Great City
which Spiritually is called Sodom and Egypt, where also
their Lord was crucified.

9 And those from the peoples and tribes and tongues and na-
tions will look at their dead bodies for three and a half days,
and will not permit their dead bodies to be laid in a tomb.

10 And those who dwell on the Land will rejoice over them and
make merry; and they will send gifts to one another, because
these two prophets tormented those who dwell on the Land.

271
