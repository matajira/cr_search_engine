12:1-2 PART FOUR: THE SEVEN TRUMPETS

As the pregnant woman approaches the time to give birth,
She writhes and cries out in her labor pains,
Thus were we before Thee, Q Lorn. (Isa. 26:17)

As prophetic revelation progresses in Scripture, it becomes
increasingly clear that the Old Covenant Church is laboring to
bring forth the Christ (ef. Mic. 4:9-5:9): He was the basic prom-
ise of the Abrahamic covenant. This is what Israel was waiting
for, being in labor and pain throughout her existence. This is the
Most essential meaning of Israel’s history, apart from which it
has no significance: the bearing of the Manchild (cf. John
16;:20-22), the Savior of the world. From the pretevangelium to
the Flood, from the Abrahamic Covenant through the slavery in
Egypt, the Exodus, the settling of Canaan, the Babylonian Cap-
tivity, the return from exile, and the suffering under the Greeks
and the Romans, Israel was laboring to give birth to the Christ,
to bring in the Messianic age.

In the midst of the Church’s struggles, therefore, she cried
out. This verb (raz) has special significance in Scripture, gen-
erally being used for an oath or the solemn proclamation of
God’s revelation; it is often used of God’s servants speaking in
the face of opposition.’ Here it has reference to the Church’s
official declaration of the Word of God, the prophecy that she
uttered as she travailed in birth. This was the essence of all pro-
phetic revelation, to bear witness to the Christ (John 5:39,
45-46; Luke 24:25-27; Acts 3:24; 13:27).

It is important to recognize the relationship of all this to the
very obvious astronomical symbolism in the text. The word St.
John uses for sign was the term used in the ancient world to
describe the constellations of the Zodiac; St. John’s model for
this vision of the Church is the constellation of Virgo, which
does have a “crown” of twelve stars.* It seems likely that the

7. See, e.g., Matt. 27:50; Mark 3:11; 4; 10:48; 15:13; John 1:15; 7:28;
12:13, 44; Acts 19:28, 32, 34; Rom. + James 5:4; and see its use
especially in Revelation: 6:10; 7:2, 10; 10:3; 44:15; 18:2,18-19; 19:17,

8, The twelve stars are: “(1) Pi, (2) Nu, (3) Beta (near the ecliptic), (4) Sigma,
(S) Chi, (6) lota—these six stars form the southern hemisphere around the head.
of Virgo, Then there are (7) Theta, (8) Star 60, (9) Delta, (10) Star 93, (I} Beta
(the second magnitude star), (12) Omicron—these last six form the northern
hemisphere around the head of Virgo. All these stars are visible anes that could
have been seen by observers.” Ernest L. Martin, The Birth of Christ Recatcu-
laied (Pasadena, CA: Foundation for Biblical Research, 2nd ed., 1980}, p. 159.

300

   
 
