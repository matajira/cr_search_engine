THE DAYS OF VENGEANCE

—__.. Purity and Danger: An Analysis of the Concepts of Poi-
jution and Taboo. London: Routledge and Kegan Paul, [1966] 1969.

Edersheim, Alfred. The Life and Times of Jesus the Messiah. 2 vols.
McLean, VA: MacDonald Publishing Co., n.d.

. The Temple: Its Ministry and Services as They Were at
the Time of Christ. Grand Rapids: William B. Eerdmans Publish-
ing Co., 1980.

Eliade, Mircea. The Myth of the Eternal Return: or, Cosmos and His-
tory. Princeton: Princeton University Press, (1954) 1971.

Eusebius, Ecclesiasticat History,

Fairbairn, Patrick. The Interpretation of Prophecy. Edinourgh: The
Banner of Truth Trust, [1865] 1964.

Frend, W. H. C. The Rise of Christianity. Philadelphia: Fortress
Press, 1984,

Gaston, Lloyd. No Stone on Another: The Fall of Jerusalem in the
Synoptic Gospets. Leiden: E. J. Brill, 1970.

Goulder, M. D. The Evangelists’ Calendar: A Lectionary Explanation
of the Development of Scripture. London: SPCK, 1978.

Grant, Michael. The Twelve Caesars. New York: Charles Scribner's
Sons, 1975.

Harnack, Adolf. The Mission and Expansion of Christianity in the
First Three Centuries. Translated by James Moffat. Gloucester,
MA: Peter Smith, [1908] 1972.

Harrison, R. K., ed. Major Cities of the Biblical World, Nashville:
Thomas Nelson Publishers, 1985.

Irenaeus. Against Heresies.

Jordan, James B. “Christian Piety: Deformed and Reformed.” Geneva
Papers (New Series), No. 1 (September 1985).

. Judges: God’s War Against Humanism. Tylet, TX:
Geneva Ministries, 1985,

__..____. The Law of the Covenant: An Exposition of Exodus
21-23, Tyler, TX: Institute for Christian Economics, 1984.

. “Rebellion, Tyranny, and Dominion in the Book of Gen-
esis.” In Tactics of Christian Resistance, Christianity and Civilization
No. 3. Edited by Gary North. Tyler, TX: Geneva Ministries, 1983.

. Sabbath-Breaking and the Death Penaity: A Theological
Investigation. Tyler, TX: Geneva Ministries, 1986.

. The Sociology of the Church. Tyler, TX: Geneva Minis-
tries, 1986.

,ed. The Failure of the American Baptist Culture (Chris-
tianity and Civilization 1). Tyler, TX: Geneva Ministries, 1982.

, ed. The Reconstruction of the Church (Christianity and
Civilization 4). Tyler, TX: Geneva Ministries, 1985.

672
