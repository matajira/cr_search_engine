INTRODUCTION

stances of those early Christians — many of them enduring cruel
sufferings and grievous persecutions, and all of them eagerly
looking for an approaching hour of deliverance which was now
close at hand—what purpose could it have answered to send
them a document which they were urged to read and ponder,
which was yet mainly occupied with historical events so distant
as to be beyond the range of their sympathies, and so obscure
that even at this day the shrewdest critics are hardly agreed on
any one point?

“Is it conceivable that an apostle would mock the suffering
and persecuted Christians of his time with dark parables about
distant ages? If this book were really intended to minister faith
and comfort to the very persons to whom it was sent, it must un-
questionably deal with matters in which they were practically
and personally interested. And does not this very obvious con-
sideration suggest the true key to the Apocalypse? Must it not of
necessity refer to matters of contemporary history? The only
tenable, the only reasonable, hypothesis is that it was intended
to be understood by its original readers; but this is as much as to
say that it must be occupied with the events and transactions of
their own day, and these comprised within a comparatively brief
space of time.”#!

Second, St. John writes that the book concerns “the things
which must shortly take place” (1:1), and warns that “the time is
near” (1:3). In case we might miss it, he says again, at the close
of the book, that “the Lord, the God of the spirits of the proph-
ets, sent His angel to show to His bond-servants the things
which must shortly take place” (22:6). Given the fact that one
important proof of a true prophet lay in the fact that his predic-
tions came true (Deut. 18:21-22), St. John’s first-century readers
had every reason to expect his bock to have immediate signifi-
cance. The words shortly and near simply cannot be made to
mean anything but what they say, Some will object to this on the
basis of 2 Peter 3:8, that “one day is with the Lord as a thousand
years, and a thousand years as one day.” But the context there is
entirely different: Peter is exhorting his first-century readers to
have patience with respect to God’s promises, assuring them

88. J. Stuart Russell, The Parousia: A Critical Inquiry into the New Testa-
ment Doctrine of Our Lord’s Second Coming (Grand Rapids: Baker Book
House, [1887] 1983), p. 366.

42
