THE DOMINION MANDATE 3:4-6

the womb they are wayward and speak lies” (Ps. 58:3; cf. Ps.
53:2-3; Rom. 3:10-12, 23; Eph. 2:1-3). Now, either the “age of
accountability” doctrine is in error, or the Bible is wrong. Which
are we to believe? The fact is that the idea of the essential sin-
lessness of infants is a pagan notion, unsupported by the Bible.
It is merely antichristian sentimentalism, which refuses to hear the
Word of God and attempts to replace it with the word of man—or,
more likely, with the word of effeminate poets scribbling mushy
greeting cards, It is right on the same level with the sentiment
that every time a fairy blows its wee nose a baby is born.

To conclude this point: The threat stated by Jesus Christ here
is very real. Those who are in the Book of Life—i.e., who are bap-
tized Church members professing Christ, and are thus counted
4s, and treated as, Christians —must remain faithful to Christ. If
they apostatize into heresy, immorality, or simply the “seculariza-
tion” that plagued Sardis, they will be erased, written out of the
record of the redeemed. But the Christian who overcomes these
temptations, thus demonstrating that Christ has truly purchased
him for His own, is in no danger —his name will never be erased.

The final promise to the overcomer reinforces the idea: I will
confess his name before My Father, and before His angels, This
echoes Jesus’ statements in the Gospels: “Everyone therefore
who shall confess Me before men, I will also confess him before
My Father who is in heaven. But whoever shall deny Me before
men, I will also deny him before My Father who is in heaven”
(Matt. 10:32-33; cf. Mark 8:38; Luke 12:8-9). Many of the
Christians in Sardis were denying Christ before their commun-
ity, as they endeavored to be praised of men rather than of God.
At the Last Judgment they would hear these words from the
Son of God: J never knew you; depart from Me, you who prac-
tice lawlessness (Matt. 7:23), But those who overcame these
temptations would be joyfully acknowledged by Christ as His
own. This message is as important and needed today as it was
2000 years ago. Do we have ears to hear what the Spirit says to
the churches?

Philadelphia; Judgment on the Synagogue af Satan (3:7-13)

7 And to the angel of the church in Philadelphia write: He who
is holy, who is true, who has the key of David, who opens and
no one will shut, and who shuts and no one opens, says this:

125
