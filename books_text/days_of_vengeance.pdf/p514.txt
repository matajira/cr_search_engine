19:10 PART FIVE: THE SEVEN CHALICES

tian community; thus he urges St. John to worship God, to
“draw near with confidence to the Throne of grace” (Heb. 4:16).
The fact that St. John’s brethren hold the Testimony of Jesus
demonstrates that they are members of the Council, indwelt by
the Spirit; for Jesus’ Testimony is the Spirit of prophecy; the
Spirit is wherever Jesus’ Testimony is held and proclaimed.

“With perfect justice, therefore, does Bossuet remark, ‘that
the angel rejects the worship in order to place the apostolical
and prophetical ministry on a footing with that of the angels.’
. .. The dissuasion is not based on the consideration that the
worship trenches on God’s glory, but on the consideration that it
trenches on John’s honour. It is as if it were said, go directly to
God with thy worship, so that thou mayest not throw into the
shade the glorious dignity bestowed on thee, and represented by
thee.”11

But what is it about the angel’s proclamation that induced
St. John to bow at his feet in the first place? “It is the eucharistic
reference which it contains. The primitive Church consecrated
the eucharist by the great thanksgiving-prayer which names the
Tite. Lifting their hearts to heaven, they blessed God for his
mighty acts of salvation, thereby both assuring their ultimate
possession of Christ, and making real the foretaste they were
about to receive in his sacramental body and blood. The exulta-
tion of victory has passed into eucharistic prayer in 19:1-8, but it
is the angel’s beatitude which first makes explicit the allusion to
that blessed feast eaten in the kingdom of God and anticipated
in the Church. St. John falls to adore, and every intermediary
vanishes between himself and Christ.”}2

The Son of God Goes Forth to War (19:11-2))

11 And I saw heaven opened; and behold, a white horse, and
the One sitting upon it called Faithful and True; and in right-
eousness He judges and wages war.

12 And His eyes are a flame of fire, and upon His head are
many diadems; and He has a name written which no one
knows except Himself.

ll, EB, W. Hengstenberg, The Revelation of St. John, two vols. (Cherry Hill,
NJ: Mack Publishing Co., [1851] 1972), Vol. 2, p. 256.

12. Austin Farrer, The Revelation of St. John the Divine (Oxford: At the
Clarendon Press, 1964), pp. 195f.

480
