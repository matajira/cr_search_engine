THE DOMINION MANDATE 3212-13

the name of My God, and the name of the City of My God, . . .
and My new name. All this speaks of the full restoration of
God’s people to the image of God, as we see in the final chapter
of Revelation: “And they shall see His face, and His name shal!
be in their foreheads” (Rev. 22:4). One of the basic blessings of
the covenant is contained in the familiar benediction: “The
Lorp make His face shine upon you” (Num. 6:25); to see the
shining of Gad’s face means to partake of salvation and to re-
flect the glory of God as His image-bearer (see Ex. 34:29-35;
Num. 12:6-8; Ps, 80:3, 7, 19; 2 Cor. 3:7-18; 4:6; 1 John 3:2), Sim-
ilarly, as we have already seen, the name of God inscribed on the
forehead symbolizes the restoration of redeemed man to the
ethical and physical glory which belongs to the image of God
(cf. Gen. 3:19; Ex. 28:36-38; Deut. 6:4-9; and contrast 2 Chron.
26:19).

The picture is completed as the Christian is declared to be a
citizen of the new Jerusalem, which comes down out of heaven
from My God. The old Jerusalem, which had apostatized from
the faith of Abraham, was under judgment, about to be de-
stroyed; the old Temple, which God had abandoned, had be-
come a sanctuary for demons, and was soon to be so completely
demolished that not one stone would lie upon another (Matt.
24:1-2). But now the Church of Christ is declared to be the city
of God, the new Jerusalem, whose origin was not on earth but
in heaven, The citizens of the old Jerusalem were to be scattered
to the ends of the earth (Luke 21:24), while the Christian’s rela-
tionship to God is so intimate that he could be described as a
very pillar in the Temple, the dwelling-place of God—a pillar,
moreover, that could not be moved from its place, for the Chris-
tian will net go out from it anymore. The children of the old
Jerusalem were, like their mother, enslaved; while “the Jeru-
salem abave is free; she is our mother” (Gal. 4:26). Jesus had
said: “Many shall come from east and west, and shall recline at
the table with Abraham, arid Isaac, and Jacob, in the kingdom
of heaven; but the sons of the kingdom shall be cast out into the
outer darkness; in that place there shall be weeping and
gnashing of teeth” (Matt. 8:11-12). And this was true of the over-
coming Christians in Philadelphia. Although persecuted and
discriminated against by the false Israel, as Isaac had been by
Ishmael (Gen. 21:8-14; Gal. 4:22-31), they would see the false

131
