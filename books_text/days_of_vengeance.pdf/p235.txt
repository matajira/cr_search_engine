7

THE TRUE ISRAEL

The two visions of this chapter (v. 1-8 and v. 9-17) are still
part of the Sixth Seal, providing a resolution of the problem of
Israel’s fall. Yet they also form an interlude or intermission, a
period of delay between the sixth and seventh seals that serves to
heighten the sense of waiting complained of by the saints in
6:10, since this section is in part the divine answer to their prayer
(cf. the delay between the sixth and seventh trumpets, 10:1-11:14).
Before the Fall of Jerusalem, Christianity was still largely identi-
fied with Israel, and the futures of the two were interconnected.
The Christians were not separatists; they regarded themselves as
the true heirs of Abraham and Moses, their religion as the ful-
fillment of all the promises to the fathers. For the Church to ex-
ist completely separate from the Israelite nationality and from
the Holy Land was virtually unimaginable. Thus, if God’s wrath
were to be unleashed upon Israel with all the undiluted fury por-
trayed in the Sixth Seal, bringing the decreation of heaven and
earth and the annihilation of mankind, what would become of
the Church? What about the faithful who find themselves in the
midst of a collapsing civilization? Would the believing remnant
be destroyed in the coming conflagration along with the enemies
of the faith?

The answer given in these visions is that “God has not des-
tined us for wrath, but for obtaining salvation through our Lord
Jesus Christ” (1 Thess. §:9): The Church will be preserved. In
terms of the coming judgment on Israel, in fact, the Lord had
given explicit instructions about how to escape from the Tribu-
lation (see Matt. 24:15-25; Mark 13:14-23; Luke 21:20-24), The
Christians living in Jerusalem obeyed the prophetic warning,

201
