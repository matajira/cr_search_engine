Part Three

ETHICAL STIPULATIONS:
THE SEVEN SEALS
(Revelation 4-7)

Introduction

The third section of the covenantal treaty (cf. Deut. 5:1-26:19)!
declared the way of Covenant life required of the vassals, the
laws of citizenship in the Kingdom. As St. Paul declared, all
men “live and move and exist” in God (Acts 17:28); He is the
Foundation of our very being. This means that our relationship
to Him is at the center of our existence, of our actions and think-
ing in every area of life. And central to this relationship is His
Sanctuary, where His subjects come to worship Him before His
Throne. Thus the major concern of the Stipulations section is the
thorough consecration of the people to God, with special impor-
tance placed on the establishment of one central Sanctuary:

You shall seek the Lorp at the place which the Lorp your
God shall choose from ali your tribes to establish His name there
for His dwelling, and there you shall come. (Deut. 12:5; cf. all of
ch, 12)

As Meredith Kline observes, “The centralization require-
ment must... be understood in terms of Deuteronomy’s
nature as a suzerainty treaty. Such treaties prohibited the vassal
to engage in any independent diplomacy with a foreign power
other than the covenant suzerain. In particular, the vassal must
not pay tribute to any other lord.”? The centrality of the Sanc-

1. See Meredith G. Kline, Treaty of the Great King: The Covenant Struc-
ture of Deuteronomy (Grand Rapids: William B. Eerdmans Publishing Co.,
1963), pp. 62-120.

2, Ibid,, p. 80.

141
