LITURGY AND HISTORY

The Book ts Opened (8:1-5)

1 And when He broke the Seventh Seal, there was silence in
heaven for about half an hour.

2 And I saw the seven angels who stand before God; and
Seven Trumpets were given to them.

3 And another angel came and stood at the altar, holding a
golden censer; and much incense was given to him, that he
might add it to the prayers of all the saints upon the golden
altar which was before the throne.

4 And the smoke of the incense, with the prayers of the saints,
went up before God out of the angel’s hand.

5 And the angel took the censer; and he filled it with the fire of
the altar and threw it onto the Land; and there followed
peals of thunder and voices and flashes of lightning and an
earthquake.

1-2. Finally, the Seventh Seal is broken, opening up to reveal
the seven trumpets that herald the doom of Jerusalem, the once-
holy City which has become paganized and which, like its pre-
cursor Jericho, will fall by the blast of seven trumpets (cf. Josh.
6:4-5). But first, in this grand heavenly liturgy which makes up
the Book of Revelation, there is silence in heaven for about half
an hour. Milton Terry comments: “Perhaps the idea of this
silence was suggested by the cessation of singers and trumpets
when King Hezekiah and those with him bowed themselves in
reverent worship (2 Chron, 29:28-29), and the kaif hour may
have some reference to the offering of incense described in verses
3 and 4, for that would be about the length of time necessary for
a priest to enter the temple and offer incense and return (comp.

229
