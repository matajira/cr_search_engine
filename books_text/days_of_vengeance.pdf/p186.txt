4:4 PART THREE: THE SEVEN SEALS

continuation of the teachings already presented?

Third, we should consider the symbolism of the number
twenty-four. In general, since twenty-four is a multiple of
twelve, there is again a prima facie reason to assume that this
number has something to do with the Church. Twelve is a num-
ber Biblically associated with the people of God: Israel was
divided into twelve tribes; and even the administration of the
New Covenant Church is spoken of in terms of “twelve tribes,”
because the Church is the New Israel (see Matt. 19:28; Mark
3:14-19; Acts 1:15-26; cf. James 1:1). St. John uses the word elder
twelve times in Revelation (4:4, 10; 5:5, 6, 7, 11, 14; 7:11, 13;
11:16; 14:3; 19:4). The number twenty-four is thus a “double por-
tion” of twelve. Multiples of twelve are also built into the sym-
bolic structure of the New Jerusalem, as we read in the final vi-
sion of the prophecy (21:12-14):

It had a great and high wall, with twelve gates, and at the
gates twelve angels; and names were written on them, which are
those of the twelve tribes of the sons of Israel... .

And the wall of the city had twelve foundation stones, and
on them were the twelve names of the twelve apostles of the
Lamb.

But the picture of the twenty-four elders is based on some-
thing much more specific than the mere notion of multiplying
twelve. In the worship of the Old Covenant there were twenty-
four divisions of priests {1 Chron. 24) and twenty-four divisions
of singers in the Temple (1 Chron. 25). Thus, the picture of
twenty-four leaders of worship was not a new idea to those who
first read the Revelation: It had been a feature of the worship of
God’s people for over a thousand years.'* In fact, St. John has
brought together two images that support our general conclu-
sion: (1) The elders sit on thrones —they are kings; (2) The elders
are twenty-four in number — they are priests. What St. John sees
is simply the Presbytery of Heaven: the representative assembly

14, See Alfred Edersheim, The Temple: Its Ministry and Services as They
Were at the Time of Jesus Christ (Grand Rapids: William B. Eerdmans Pub-
lishing Co., 1980), pp. 75, 86ff. Ezekiel saw twenty-five men serving in the
Temple: the representatives of the twenty-four courses of the priesthood, plus
the High Priest (Ezek. 8:16).

152
