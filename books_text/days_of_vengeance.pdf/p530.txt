PART FIVE; THE SEVEN CHALICES

and rose again for that purpose, they have also seen the King-
dom’s work as a leavening influence, gradually transforming the
world into the image of God. The definitive cataclysm has al-
ready taken place, in the finished work of Christ. Depending on
the specific question being asked, therefore, orthodox Christi-
anity can be considered either amillennial or postmillennial—
because, in reality, it is both.

One further point should be understood: In addition to be-
ing both “amillennialist” and “postmillennialist,” the orthodox
Christian Church has been generally optimistic in her view of
the power of the Gospel to convert the nations. In my book
Paradise Restored: A Biblical Theology of Dominion (Ft.
Worth, TX: Dominion Press, 1985), I opened each chapter with
a quotation from the great St. Athanasius on the subject of the
victory of the Gospel throughout the world and the inevitable
conversion of all nations to Christianity. The point was not to
single out St. Athanasius as such; numerous statements express-
ing the Hope of the Church for the worldwide triumph of the
Gospel] can be found throughout the writings of the great Fath-
ers and teachers, in every age of Christianity.* Even more signifi-
cantly, the universal belief in the coming victory can be seen in
the action of the Church in history. Christians never supposed
that their high calling was to work for some sort of détente with
the Enemy. “Pluralism” was never regarded by the orthodox as a
worthy goal, The Church has always recognized that God sent
His only begotten Son in order to redeem the world, and that

5. See St. Augustine, The City of God, Book XX. On St. Augustine and the
influence of his postmillennial philosophy of history, see Peter Brown, Aug-
ustine of Hippo (Berkeley and Los Angeles: University of California Press,
1967}; Charles Norris Cochrane, Christienity and Classical Culture: A Study
of Thought and Action from Augustus to Augustine (London: Oxford Uni-
versity Press, [1940, 1944], 1957); Robert Nisbet, History of the Idea of Prog-
ress (New York: Basic Books, 1980), pp. 47-76. On the extensive Reformed
heritage of postmillennialism, from John Calvin to the late nineteenth century,
see Greg L. Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
The Journal of Christian Reconstruction, Vol. IIL, No. 2 (Winter, 1976-77),
PP. 48-105, esp. pp. 68-105; James B. Jordan, “A Survey of Southern Presby-
terian Millennial Views Before 1930,” The Journal of Christian Reconstruc-
tion, Vol, III, Na. 2 (Winter, 1976-77), pp. 106-21; J. A. de Jong, As the Waters
Cover the Sea: Millennial Revival and the Interpretation of Prophecy
(Kampen: J, H. Kok, 1970); J. Marcellus Kik, An Eschatology of Victory
(Nutley, NJ: Presbyterian and Reformed Publishing Co., 1971), pp. 3-29; Iain
Murray, The Puritan Hope: A Study in Revival and the Interpretation of
Prophecy (London: The Banner of Truth Trust, 1971),

496
