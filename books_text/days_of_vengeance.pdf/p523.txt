THE FEASTS OF THE KINGDOM 19:17-18

All nations are absolutely required to be Christian, in their
official capacity as well as in the personal character of their indi-
vidual citizens. Any nation that does not submit to the all-
embracing rule of King Jesus will perish; all nations shad/ be
Christianized some day. It is only a matter of time. Jesus Christ
is the universal Sovereign, and He will be recognized as such
throughout the earth, in this world as well as in the next, in time
as well as in eternity. He has promised: “I will be exalted among
the nations, I will be exalted in the earth” (Ps. 46:10). The Lorp
of hosts is with us.

17-18 This is the second of the final seven visions, each of
which begins with the phrase And I saw; thus, while it is cer-
tainly related to the subject of the previous vision, it is not sim-
ply a continuation of it, As we have seen, the chapter begins
with a feast, the Marriage Supper of the Lamb, the sacred Eucha-
tistic meal of the Church before her Lord. But another great
feast is proclaimed here. The Sun of Righteousness has arisen,
with healing in His wings (Mal. 4:2); but He also brings an angel
standing in the sun (the ruler of the Day, Gen. 1:16) who issues
an invitation to all the birds that fly in midheaven, the birds of
prey, We have seen “midheaven” as the place in which the Eagle
warned of woe (8:13), and in which an angel invited the rulers of
the earth to embrace the eternal Gospel (14:6). Now the angel in-
vites the eagles to the Great Supper of God, where they may glut
themselves on the flesh of Christ’s enemies: the flesh of kings
and the flesh of commanders and the flesh of mighty men and
the flesh of horses and of those wha sit on them and the flesh of
atl men, both free men and slaves, and small and great. We
noted at 8:13 that a basic curse of the covenant is that of being
eaten by birds of prey (cf. Deut. 28:26, 49). Israel is now a sacri-
ficial corpse (Matt. 24:28), and there is no longer anyone who
can drive away the scavengers (cf. Gen. 15:11; Deut. 28:26).24

 

24. Genesis 15 describes the ratification ceremony of God’s covenant with
Abram. After Abram culs the sacrificial animals apart and arranges the halves
opposite each other, the unclean birds of prey descend to attack the carcasses,
and Abram drives them away (v, 11). Gordon Wenham interprets this as a
promise that Israel, through Abramic faith and obedience (cf. Gen. 26:5), will
be protected from the attacks of unclean nations; Gordon Wenham, “The Sym-
bolism of the Animal Rite in Genesis 15; A Response to G. F. Hasel, JSOT 19
(1981) 61-78," in Journal for the Study of the Old Testament 22 (1981), 134-37.

489
