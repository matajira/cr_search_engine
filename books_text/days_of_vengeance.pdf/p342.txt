12:5 PART FOUR: THE SEVEN TRUMPETS

later, the Seed was being carried in a shepherd-boy, and again
the Dragon attacked, twice inspiring a demon-possessed king to
throw javelins at him (1 Sam, 18:10-11). In fact, the whole
machinery of Saul’s kingdom went into effect just to try to kill
David (i Sam. 18-27). Similarly, the wicked Queen Athaliah
“destroyed all the seed royal of the House of Judah” (2 Chron.
22:10), yet the Seed was preserved in the infant Joash. Haman,
the evil Prime Minister of Persia, would have succeeded in his
attempt to launch a full-scale pogrom to destroy all the Jews,
had it not been for the courage and wisdom of Queen Esther
{Est. 3-9), The most striking example of this pattern on a latge
scale occurs throughout the history of Israel, from the Exodus
to the Exile: the covenant people’s perennial, consistent tempta-
tion to murder their own children, to offer them up as sacrifices
to demons (Lev. 18:21; 2 Ki. 16:3; 2 Chron. 28:3; Ps. 106:37-38;
Ezek. 16:20). Why? It was the war of the two seeds. The Dragon
was trying to destroy the Christ.

This pattern comes to a dramatic climax at the birth of
Christ, when the Dragon possesses King Herod, the Edomite
ruler of Judea, and inspires him to slaughter the children of
Bethlehem (Matt. 2:13-18); indeed, St. John’s vision of the
Woman, the Child, and the Dragon seems almost an allegory of
that event. The Dragon tried again, of course: tempting the
Lord (Luke 4:1-13), seeking to have Him murdered (Luke
4:28-29), subjecting Him to human and demonic oppression
throughout His ministry, possessing one of the most trusted dis-
ciples to betray Him (John 13:2, 27), and finally orchestrating
His crucifixion. Even then —rather, especially then—the Dragon
was defeated, for the Cross was God’s way of tricking Satan
into fulfilling His purposes, according to His wisdom—“the hid-
den wisdom,” St. Paul says, “which God predestined before the
ages to our glory, the wisdom which none of the rulers of this
age has understood; for if they had understood it, they would
not have crucified the Lord of glory” (1 Cor. 2:7-8), In wounding
the Seed’s heel, the Serpent’s head was crushed.

§ And she gave birth to a Son, a male (cf. Isa. 66:7-8) who is
to rule all nations with a rod of iron. St. John returns to Psalm
2, one of his favorite texts, to explain his symbolism. The Son
is, obviously, Jesus Christ, the Seed of the Wa.nan, the Child of

308
