THE HOLY WAR 12:7-9

7-9 The scene changes abruptly: St. John now sees war in
heaven, Michael and His angels waging war with the Dragon.
This is not, as some suppose, a sequel to the preceding vision, as
if Satan, frustrated in his attempt to devour the Messiah, now
directs his assault toward heaven. On the contrary, St. John un-
veils this scene in order to explain the preceding verse —to show
why the Woman had to flee into the wilderness. Once that is
explained, in verses 7-12, he returns to the theme of the flight of
the Woman. In addition, St. John uses the imagery in this pas-
sage to display another aspect of the Child’s conflict: with the
Dragon. Chronologically, this explanatory section fits in be-
tween verses 5 and 6.

We should note to begin with that the Holy War is initiated,
not by the Dragon, but by Michael and His angels. There should
be little question that this Captain of the angelic host is a symbol
for the Seed of the Woman, the Son of God—represented now
not as a Child, but as Michael, the great Warrior-Protector who
leads the armies of heaven in battle against the demons. St.
John’s symbolism is not casual; it is intentional, and very pre-
cise. He carefully chose to reveal Christ in terms of the specific
Biblical connotations associated with Michael.

The name Michael (meaning Who is like God?) occurs else-
where in the Scriptures only in Daniel and Jude. Michael is por-
trayed in Daniel as “the great Prince” who stands as the special
Protector of the people of God. War breaks out in heaven be-
tween the good and evil angels, and even Gabriel is unable to
overcome the demons until Michael comes to do battle with the
enemy (Dan. 10:12-13, 20-21). In view of what is revealed about
Michael in the latter part of Daniel 10, it is likely that the other-
wise unexplained vision in the first part of the chapter refers to
Him as well: Daniel saw a man

dressed in linen, whose waist was girded with a belt of pure gold
of Uphaz. His body also was like beryl, His face like lightning,
His eyes were like flaming torches, His arms and feet like the
gleam of polished bronze, and the sound of His words like the
sound of a tumult. (Dan. 10:5-6)

The closing passage of Daniel’s prophecy refers to Michael
as the Guardian over God’s people, who will arise to fight on

311
