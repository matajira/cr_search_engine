7

THE FALSE BRIDE

While some in recent years have attempted to see the Great
Harlot of Revelation as the City of Rome, the Church through-
out Christian history has generally understood that she is in
some sense a False Bride, a demonic parody of the True Bride,
the Church. The Biblical motif of the Bride falling into adultery
{apostasy) is so well-known that such an identification is all but
inescapable. The metaphor of harlotry is exclusively used in the
Old Testament for a city or nation that has abandoned the Cove-
nant and turned toward false gods; and, with only two excep-
tions {see on v. 1-2, below), the term is always used for faithless
Israel. The Harlot is, clearly, the False Church. At this point,
however, agreement shatiers into factionalism. To the Donatist
heretics of the fourth century, the Catholic Church was the
Whore, Some Greek Orthodox and Protestant theologians have
seen her in the Roman papacy, while many fundamentalists have
spotted her tinsel charms in the World Council of Churches.
Although it is true that there may be (and certainly have been)
false churches in the image of the Harlot, we must remember the
historical context of the Revelation and the preterist demands it
makes upon its interpreters. Merely to find some example of a
false church and identify her as the Whore is not faithful ex-
egesis. St. John has set our hermeneutical boundaries firmly
within his own contemporary situation, in the first century. He
has, in fact, stated definitely that the Harlot was a current phe-
nomenon (17:18), from which he expects his current readers to
separate themselves. Whatever modern applications are made of
this passage, we must see them as just that: applications. The
primary significance of the vision must refer to the False Church
of St, John’s day.

421
