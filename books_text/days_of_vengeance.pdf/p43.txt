DESTINATION

eternal life” (1 John 5:20-21). The Apostle Peter declared, shortly
after Pentecost: “Salvation is found in no one else, for there is
no other name under heaven given to men by which we must be
saved” (Acts 4:12). “The conflict of Christianity with Rome was
thus political from the Roman perspective, although religious
from the Christian perspective. The Christians were never asked
to worship Rome’s pagan gods; they were merely asked to recog-
nize the religious primacy of the state. As Francis Legge ob-
served, ‘The officials of the Roman Empire in time of persecu-
tion sought to force the Christians to sacrifice, not to any
heathen gods, but to the Genius of the Emperor and the Fortune
of the City of Rome; and at all times the Christians’ refusal was
looked upon not as a religious but as a political offense. . . .’ The
issue, then, was this: should the emperor’s law, state law, govern
both the state and the church, or were both state and church,
emperor and bishop alike, under God’s law? Who represented
true and ultimate order, God or Rome, eternity or time? The
Roman answer was Rome and time, and hence Christianity con-
stituted a treasonable faith and a menace to political order.”26

The charge brought by the Jewish prosecution in one first-
century trial of Christians was that “they are all defying Caesar’s
decrees, saying that there is another king, one called Jesus”
(Acts 17:7), This was the fundamental accusation against all the
Christians of the Empire. The captain of police pleaded with the
aged Bishop of Smyrna, St. Polycarp, to renounce this extreme
position: “What harm is there in saying Caesar is Lord?” St.
Polycarp refused, and was burned at the stake. Thousands
suffered martyrdom on just this issue. For them, Jesus was not
“God” in some upper-story, irrelevant sense; He was the only
God, complete Sovereign in every area. No aspect of reality
could be exempt from His demands. Nothing was neutral. The
Church confronted Rome with the inflexible claim of Christ’s
imperial authority: Jesus is the only-begotten Son; Jesus is God;
Jesus is King; Jesus is Savior; Jesus is Lord. Here were two Em-
pires, both attempting absolute world domination; and they
were implacavly at war.??

26. Ibid., p. 93. Rushdoony cites Francis Legge, Forerunners and Rivals of
Christianity: From 330 B.C. 10 330 A.D. (New Hyde Park, NY: University
Books, [1915], 1964), vol. I, pp. xxivf.

27. CF, Swete, p. Ixxxi.
