13:18 PART FOUR: THE SEVEN TRUMPETS,

and this numerical symbol of the Antichrist, 666, stood in terri-
ble opposition to 888—the three perfect 8's of the name of
Jesus.”36

More than all this, the number 666 is explicitly mentioned in
the books of the Kings and the Chronicles, from which, as we
have seen, St. John takes many of his symbolic numbers (see
comments on 4:4), These inspired historical writings tell us that
Solomon (a Biblical type of both Christ and the Beast) received
666 talents of gold in one year, at the height of his power and
glory (1 Kings 10:14; 2 Chron. 9:13). That number marks both
the high point of his reign and the beginning of his downfall;
from then on, everything goes downhill into apostasy. One by
one, Solomon breaks the three laws of godly kingship recorded
in Deuteronomy 17:16-17: the law against multiplying gold (1
Kings 10:14-25); the law against multiplying horses (1 Kings
10:26-29); and the law against multiplying wives (1 Kings 11:1-8).
For the Hebrews, 666 was a fearful sign of apostasy, the mark of
both a king and a kingdom in the Dragon’s image.

As we have already noted, the ancient languages used each
letter of the alphabet as a numeral as well; thus, the “number”
of anyone’s name could be computed by simply adding up the
numerical value of its letters. Clearly, St. John expected that his
contemporary readers were capable of using this method to dis-
cover the Beast’s name —thus indicating, again, the contermpor-
ary message of Revelation; he did not expect them to figure out
the name of some 20th-century official in a foreign government.
At the same time, however, he tells them that it will not be as
easy as they might think: it will require someone “who has un-
derstanding.” For St. John did not give a number that could be
worked out in Greek, which is what a Roman official scanning
Revelation for subversive content would expect. The unex-
pected element in the computation was that it had to be worked
out in Hebrew, a language that at least some members of the
churches would know. His readers would have guessed by now
that he was speaking of Nero, and those who understood
Hebrew probably grasped it instantly. The numerical values of
the Hebrew letters in Neron Kesar (Nero Caesar) are:

36. F. W. Farrar, The Early Days of Christianity (Chicago and New York:
Belford, Clarke & Co., 1882), p. 539.

350
