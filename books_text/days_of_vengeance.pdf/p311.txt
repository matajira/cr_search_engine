THE END OF THE BEGINNING 11:5-6

are two oil-filled olive trees, which are also two Witnesses, a
king and a priest —all representing the Spirit-inspired prophetic
testimony of the Kingdom of priests (Ex. 19:6). (A major aspect
of St, John’s message, as we have seen, is that the New Cove-
nant Church comes into the full inheritance of the promises as
the true Kingdom of priests, the royal priesthood in which “all
the Lorp’s people are prophets.”) That these Witnesses are
members of the Old Covenant rather than the New is shown,
among other indications, by their wearing of sackcloth—the
dress characteristic of Old Covenant privation rather than New
Covenant fullness,

5-6 St. John now speaks of the two Witnesses in terms of
the two great witnesses of the Old Testament, Moses and Elijah
—the Law and the Prophets. If anyone desires to harm them,
fire proceeds from their mouth and devours their enemies. In
Numbers 16:35, fire came down from heaven at Moses’ word
and consumed the false worshipers who had rebelled against
him; and, similarly, fire fell from heaven and consumed Elijah’s
enemies when he spoke the word (2 Ki. 1:9-12). This becomes a
standard symbol for the power of the prophetic Word, as if fire
actually proceeds from the mouths of God’s Witnesses. As the
Lard said to Jeremiah, “Behold, ] am making My words in your
mouth fire, and this people wood, and it shall consume them”
(Jer. 5:14).

Extending the imagery, St. John says that the Witnesses have
the power to shut up the sky, in order that rain may not fall dur-
ing the days of their prophesying, i.e., for the twelve hundred
and sixty days (three and a half years} — the same duration of the
drought caused by Elijah in 1 Kings 17 (see Luke 4:25; James
5:17}. Like Moses (Ex. 7-13), the Witnesses have power over the
waters to turn them into blood, and to smite the earth with
every plague, as often as they desire.

Both of these prophetic figures pointed beyond themselves
to the Greater Prophet, Jesus Christ. The very last message of
the Old Testament mentions them together in a prophecy of
Christ’s Advent: “Remember the law of Moses My servant. . . .
Behold, I am going to send you Elijah the prophet. . . .” (Mal.
4:4-5). Malachi goes on to declare that Elijah’s ministry would
be recapitulated in the life of John the Baptizer (Mal. 4:5-6; cf.

277
