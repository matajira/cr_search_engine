IN THE PATH OF THE WHITE HORSE 6:11

of the sugary, syrupy, sweetness-and-light choruses that charac-
terize modern evangelical hymnals, we would understand this
much easier. But we have fallen under a pagan delusion that it is
somehow “unchristian” to pray for God’s wrath to be poured out
upon the enemies and persecutors of the Church. Yet that is what
we see God’s people doing, with God’s approval, in both Testa-
ments of the Holy Scriptures.'8 It is, in fact, a characteristic of
the godly man that he despises the reprobate (Ps. 15:4). The spirit
expressed in the imprecatory prayers of Scripture is a necessary
aspect of the Christian’s attitude (cf. 2 Tim. 4:14), Much of the
impotence of the churches today is directly attributable to the
fact that they have become emasculated and effeminate. Such
churches, unable even to confront evil— much less “overcome” it
—will eventually be captured and dominated by their enemies.

11 The righteous and faithful saints in heaven are recognized
as kings and priests of God, and thus there is given to each of
them a white robe, symbolizing God’s acknowledgment of their
purity before Him, a symbol of the victory of the overcomers
(cf. 3:4-5), The whiteness of the robe is part of a pattern already
set up in Revelation (the Seven Letters) in which the last three
items in a sevenfold structure match the first four items. Thus:

First Seal: White horse Fifth Seal: White robes
Second Seat; Red horse Sixth Seal: Moon like blood
Third Seat: Black horse Sun black
Fourth Seal: Green horse Seventh Seal? Green grass burned

In answer to the saints’ plea for vengeance, God answers that
they should rest for a little while longer, until the number of
their fellow servants and their brethren who were to be killed
even as they had been, should be completed also. The full num-
ber of martyrs has not yet been completed; the full iniquity of
their persecutors has not yet been reached (cf. Gen. 15:16), al-
though it is fast approaching the doom of Gad’s “wrath to the

18. See, ¢.g., Ps. 5, 7, 35, 58, 59, 68, 69, 73, 79, 83, 109, 137, 140. The common
term for these and other passages is Imprecatory Psalms; such an expression can
be misleading, however, since ost of the Psalms have imprecatory sections
{curses) in them (cf. Ps. 1:46; 3:7; 6:8-10; 34:16; 37:12-15; 54:7; 104:35;
139:19-22), and a# the Psalms are implicitly imprecatory, in that the blessings of
the righteous are mentioned with the corollary assumed: The wicked are cursed.

195
