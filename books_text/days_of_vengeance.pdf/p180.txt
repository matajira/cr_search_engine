4:1 PART THREE: THE SEVEN SEALS

10 the twenty-four elders will fall down before Him who sits on
the Throne, and will worship Him who lives forever and
ever, and will cast their crowns before the Throne, saying:

su Worthy art Thou, our Lord and Gad, the Holy One, to

receive glory and honor and power; for Thou didst cre-
ate all things, and because of Thy will they existed, and
were created.

1 This verse is used by advocates of Dispensationalism to
support their “Rapture Theory,” the notion that the Church will
be snatched away from this world before a coming Tribulation;
indeed, this verse seems to be the main proof-text for a pre-Trib-
ulation rapture. St. John’s “rapture” into heaven is regarded as a
sign that the whole Church will disappear before the plagues re-
corded in the following chapters are poured out. Part of the ra-
tionale for this understanding is that the Voice John heard was
like the sound of a trumpet, and St. Paul says that a trumpet
will sound at the “rapture” (1 Thess. 4:16). Some advocates of
this position seem oblivious to the fact that God uses a trumpet
on numerous occasions. In fact, as we have seen in the first
chapter, the connection between God’s Voice and the sound of a
trumpet occurs throughout Scripture, beginning with the judg-
ment in the Garden of Eden, For that matter, St. John heard the
voice like a trumpet in the first vision (Rev. 1:10). (Does this indi-
cate a possible “double rapture”?)!

The Dispensationalist school of interpretation also appeals
to the fact that, after the Voice has said Come up here, “The
word ‘church’ does not again occur in the Revelation till all is
fulfilled.”? This singular observation is set forth as abundant
proof that the Book of Revelation does not speak of the
“Church”? from this point until the Second Coming (generally

1. But wait! Chapters 8-11 record the soundings of no less than seven more
trumpets — could there be nine raptures?

2. The Scofield Reference Bible (New York: Oxford University Press, [1909]
1945), note on Rev. 4:1; cf. Hal Lindsey, There’sa New World Coming: A Pro-
phetic Odyssey (Eugene, OR: Harvest House Publishers, 1973), pp. 74ff.

3, The Dispensationalist use of the word Church is very different from its
use in historical, orthodox theology. See O. T. Allis, Prophecy and the Church
(Grand Rapids: Baker Book House, 1945, 1947), pp. 54110; L. Berkhof,
Systematic Theology (Grand Rapids: William B. Eerdmans Publishing Co.,
fourth revised ed., 1949), pp. 562-78; and Roderick Campbell, Israef and the
New Cavenant (Tyler, TX: Geneva Ministries, [1954] 1983).

146
