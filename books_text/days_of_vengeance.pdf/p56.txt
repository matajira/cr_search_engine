INTRODUCTION

Ezekiel—implies something more than a merely literary rela-
tionship. “Level pegging is not usually a feature of literary bor-
rowing: the Chronicler’s work, for example, is far from pegging
level with Samuel-Kings, with his massive expansion of the Tem-
ple material, and his excision of the northern traditions. Level
pegging is a feature rather of lectionary use, as when the Church
sets (set) Genesis to be read alongside Romans, or Deuteronomy
alongside Acts. . . . Furthermore, it is plain that John expected
his prophecies to be read aloud in worship, for he says, ‘Blessed
is he who reads the words of the prophecy, and blessed are those
who hear’ (1:3)—RSV correctly glosses ‘reads aloud.’ Indeed,
the very fact that he repeatedly calls his book ‘the prophecy’
aligns it with the OT prophecies, which were familiar from their
public reading in worship.”*’ In other words, the Book of Reve-
lation was intended from the beginning as a series of readings in
worship throughout the Church Year, to be read in tandem with
the prophecy of Ezekiel (as well as other Old Testament read-
ings). As Austin Farrer wrote in his first study of Revelation,
St. John “certainly did not think it was going to be read once to
the congregations and then used to wrap up fish, like a pastoral
letter."48

Goulder’s thesis on Revelation is supported by the findings
in his recent work on the Gospels, The Evangelists’ Calendar,
which has revolutionized New Testament studies by setting the
Gospels in their proper liturgical context. As Goulder shows,
the Gospels were originally written, not as “books,” but as serial
readings in worship, to accompany the readings in the syna-
gogues (the first New Testament churches). In fact, he argues,
“Luke developed his Gospel in preaching to his congregation, as
aseries of fulfillments of the O.T.; and this development in litur-
gical series explains the basic structure of his Gospel, which has
been a riddle so long.”5¢

47. M. D. Goulder, “The Apocalypse as an Annual Cycle of Prophecies,”
p. 350,

48. Austin Farrer, A Rebirth of fmages: The Making of St. John’s Apoca-
dypse (Gloucester, MA: Peter Smith, [1949] 1970), p. 22.

49. M. D. Goulder, The Evangelists’ Calendar: A Lectionary Explanation
of the Development of Scripture (London: SPCK, 1978).

50. Ibid., p. 7. Goulder suggests that the Book of Revelation was writien in
the same way, as St. John’s meditations on the lectionary readings in his
church.

22
