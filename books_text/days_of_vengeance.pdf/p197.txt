THE THRONE ABOVE THE SEA 4:9-11

Church, as the Church, is not worshiping corporately. In con-
trast, the pattern of Biblical worship is the corporate worship
service, with full participation among the united members of the
congregation, demonstrating a harmony of unity and diversity.

Second, worship must be responsorial. We will see more of
this as we proceed through the Book of Revelation—which is
about worship as much as anything else —but this has already
been the case with the passage we have just studied. The elders
and the four living creatures are shown singing musical re-
sponses back and forth, carrying on a dialogue. And, in the
worship of the Church on earth, that is what we do (or should
do) also. We respond liturgically to the reading of Scripture, to
the prayers, to the singing of Psalms and hymns, to the teach-
ing, and to the Sacraments. For this is what we see in heavenly
worship, and our worship should be structured as far as possible
in imitation of the heavenly pattern, according to the prayer
Christ taught us: “Thy will be done on earth as it is in heaven”
(Matt. 6:10).

Third, worship must be orderly. The elders and the living
creatures do not interrupt each other or attempt to upstage one
another. While worship should be corporate, involving the
entire Church, it must not be chaotic. A basic standard for wor-
ship is laid down in 1 Cor. 14:40: “Let everything be done
decently and in order.” Charismatics tend to have certain correct
instincts—that worship should include the whole congregation
—but their actual practice tends toward confusion and disorder,
with everyone individually “worshiping” all at once. The solu-
tion, recognized in both Old and New Testaments, and by the
Church throughout history, is to provide a common liturgy,
with formal prayers and responses, so that the people may in-
telligently worship together in a manner that is both corporate
and orderly.

Biblical public worship is very different from private or fam-
ily worship; it is radically different from a mere Bible study
group, as important as that may be. The Sunday worship of the
Church is qualitatively unique: It is God’s people coming into
the palace for a formal ceremony before the Throne, an official
audience with the King. We come to confess our faith and alleg-
iance, to take solemn oaths, to receive forgiveness, to offer up
prayers, to be instructed by God’s officers, to eat at His table,

163
