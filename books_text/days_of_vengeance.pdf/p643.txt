THE LEVITICAL SYMBOLISM IN REVELATION

tration and silence; there was for the first time instrumental music. All
this is reflected in the Alleluia chorus which goes up after the fall of
Babylon. The detail of it need not detain us here, except that the Alle-
luias recall the last psalms of the book; and that each chorus begins
with Alleluia, though in one case it has been translated into “Praise
our God” (19:1-10).

7. The Feast on the Sacrifice. —Sin-offerings were followed by the
eating of part of the sacrifice by the priest. Two feasts follow the
psalmody here, one for God’s friends, and one for his enemies. The
first is the marriage feast of the lamb, with its obvious reference to the
eucharist (19:9). The other is the invitation to the birds of heaven to
feed on the flesh of those who fall in the wars of the messiah (19:17).

The Hebrew part of the book has two further liturgical points in it
before it closes: (1) The Coming Out of the Great High Priest (19:11) in
which the liturgical symbolism is already gone; he comes out of
heaven, not out of the Naos. The Naos in heaven seems to vanish with
the earthly temple. I have dealt with the symbolism of this passage;
but it is worth noting again the fine linen, and the priestly garment
splashed with blood. One fine point is the name written on the thigh; I
have given an explanation in the text, which | think is the central one.
But it is worth noting that priestly sacredness attached to the thigh; it
was a part of the sin-offering that went to the priest. [ have seen med-
ieval Jewish drawings with a letter engraved on the thigh. But I do not
know the explanation. (2) The New Naos (21:3). Here too the liturgical
symbolism is gone, though the description of the new order which
replaces the old Jerusalem is taken from Leviticus: “Behold the Taber-
nacle of God is with men, and he shall Tabernacle with them, and they
shall be his peoples, and he (God with them) shall be their God.”

The word Tabernacle is used, but there is only a ghost of the old
priestly symbolism. The new sanctuary is universal, human, catholic,
not national or local. He goes on to describe it more fully in chapter
22; but that belongs to the later part of the book, that deals with
Christian worship.

I have dealt fairly fully in this appendix with the liturgical back-
ground of the book, because it seems to have been neglected and yet to
be all important. It sheds a great deal of light on the tone and motives
of the book. It reinforces the view that Babylon is priestly Jerusalem.
It may shed some light on the development of Christian worship, and
even on the worship in the temple.

I cannot pretend to have done more than blaze a trail through a
dense forest of obscurities; and what I have revealed, I do not profess
to understand. Until we know what a Jew felt when he saw the blood
being splashed on the altar, or the fire consuming the lamb of the
Tamid, we can hardly expect to enter into the complexities of the litur-
gical poetry of St. John.

609
