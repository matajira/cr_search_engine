LEVIATHAN AND BEHEMOTH 13;1-2

from the Garden. The Abyss is the “Deep” of Genesis 1:2, “with-
out form and void,” uninhabitable by man. It is away from the
dry land of human environment, and is the place where the
demons are kept imprisoned as long as men are faithful to God.
When men apostatize, the demons are released; as man is pro-
gressively restored, the evil spirits are sent back into the Abyss
(Luke 8:26-33). Here we see the ultimate source of the “beastli-
ness” of the Beast: In essence, he comes from the sea, from the
chaotic deep-and-darkness of the Abyss, which had to be con-
quered, formed, and filled by the light of the Spirit (Gen. 1:2;
John 1:5). This is not to suggest that there was any real conflict
between God and His creation; in the beginning, everything was
“very good.” The sea is most fundamentally an image of life.
But after the Fall, the picture of the raging deep is used and de-
veloped in Scripture as a symbol of the world in chaos through
the rebellion of men and nations against God: “The wicked are
like the tossing sea; for it cannot be quiet, and its waters toss up
refuse and mud” (Isa. 57:20; ef. Isa. 17:12). St. John is told later
that “the waters which you saw . . . are peoples and multitudes
and nations and tongues” 17:15). Out of this chaotic, rebellious
mass of humanity emerged Rome, an entire empire founded on
the premise of opposition to God.

The Beast has ten horns and seven heads, a mirror-image (cf.
Gen, 1:26) of the Dragon (12:3), who gives the Beast his power
and his throne and great authority, The ten crowned horns
(powers)? of the Beast are explained in 17:12 in terms of the gov-
ernors of the ten imperial provinces, while the seven heads are
explained as the line of the Caesars (17:9-11): Nero is one of the
“heads.” We must keep in mind the logical distinction already
drawn between sense (the meaning and associations of a sym-
bol} and referent (the special significance of the symbol as it is
used in a particular case). The connotations of heads and horns
are the same in both the Dragon and the Beast, but they refer to
different objects.

Jn a nightmarish parody of the Biblical High Priest, who
wore the divine Name on his forehead (Ex. 28:36-38), the Beast
displays on his heads blasphemous names: According to the
Roman imperial theology, the Caesars were gods. Each emperor

2, Cf. 1 Kings 22:11; Zech. 1:18-21; Ps. 75:10.
327
