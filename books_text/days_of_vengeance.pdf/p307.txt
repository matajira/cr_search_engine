THE END OF THE BEGINNING I1:1-2

that we are a kingdom of priests (1:6; 5:10; cf. Matt. 27:51; Heb.
10:19-20), and he has shown us God’s people offering up their
prayers on the altar of incense (5:8; 6:9-10; 8:3-4),

St. John is to measure the inner court, the Church, but he is
to cast out the court that is outside the Temple, and is specific-
ally commanded: Do not measure it. Measuring is a symbolic
action used in Scripture to “divide between the holy and the pro-
fane” and thus to indicate divine protection from destruction
{see Ezek. 22:26; 40-43; Zech. 2:1-5; cf. Jer. 10:16; 51:19; Rev.
21:15-16). “Throughout Scripture the priests are those who mea-
sure out the dimensions of the temple of God, the man with the
measuring rod of Ezekiel 40ff, being but the most prominent ex-
ample. Such measuring, like witness-bearing, entails seeing, and
is the precondition of judging, as we have seen these in God’s
covenant actions in Genesis 1. The priestly aspect of measuring
and witnessing can be seen in that it correlates to guarding,
because it sets up and establishes boundaries, and bears witness
tegarding whether or not those boundaries have been observed.
We might say that the kingly function has to do with filling, and
the priestly with separating, the former with cultivation and the
latter with jealousy, propriety, and protection.”

Between the Sixth and Seventh Seals, the 144,000 saints of the
True Israel were protected from the coming judgment (7:1-8).
That action is paralleled here by St. John’s measuring of the in-
ner court between the sixth and seventh Trumpets, now protect-
ing the True Temple from the outpouring of God’s wrath. The
outer court (the “court of the Gentiles”) accordingly represents
apostate Israel (cf. Isa. 1:12), which is to be cut off from the
number of the faithful Covenant people, God’s dwellingplace.
St. John, as an authoritative priest of the New Covenant, is
commanded to cast out (excommunicate) the unbelievers. This
verb (ekbalia) is generally used in the Gospels for casting out
evil spirits (cf. Mark 1:34, 39; 3:15; 6:13); it is also used for
Christ’s ejection of the moneychangers from the Temple (Matt.
21:12; Mark 11:15; John 2:15). Jesus warned that unbelieving
Israel as a whole would be cast out from the Church, while be-

2. James B. Jordan, “Rebellion, Tyranny, and Dominion in the Book of
Genesis,” in Gary North, ed., Tactics of Christian Resistance, Christianity and
Civilization No. 3 (Tyler, TX: Geneva Ministries, 1983), p. 42.

273
