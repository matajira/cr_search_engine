THE MILLENNIUM AND THE JUDGMENT 20:9-10

in Him, all the power of the enemy will be shattered. Faithful
Israel can taunt her attackers:

Be broken, O peoples, and be shattered;
And give ear, all remote places of the earth.
Gird yourselves, yet be shattered;

Gird yourselves, yet be shattered.

Devise a plan, but it will be thwarted;

State a proposal, but it will not stand,

For God is with us! (Isa. 8:9-10)

Yet St. John’s allusion to [saiah’s prophecy is also a reminder
that old Israel is now apostate. For her there is no longer an Im-
manuel. She has definitively rejected her Maker and Husband,
and He has abandoned her. Instead, God is now with the
Church, and it is the Church’s opponents who will be shattered,
though they be as many in number as the sands of the sea (Gen.
32:12)! Jesus Christ is the Seed of Abraham, and He will possess
the gate of His enemies, for the sake of His Church (Gal. 3:16,
29; Gen, 22:17).

St. John’s image for the gathered people of God combines
Moses’ camp of the saints with David and Solomon’s beloved
City. This City is the New Jerusalem, described in detail in 21:9-
22:5. The significance of this should not be missed: The City ex-
ists during the Millennium (i.e. the period between the First and
Second Advents of Christ), which means that the “new heaven
and new earth” (21:1) are a present as well as future reality. The
New Creation will exist in consummate form after the Final
Judgment, but it exists, definitively and progressively, in the
present age (2 Cor. 5:17).

The apostates rebel, and Satan’s forces briefly surround the
Church; but there is not a moment of doubt about the outcome
of the conflict. In fact, there is no real conflict at all, for the re-
bellion is immediately crushed: Fire came down from heaven
and devoured them, as it had the wicked citizens of Sodom and
Gomorrah (Gen. 19:24-25), and the soldiers of Ahaziah who
came against Elijah (2 Kings 1:10, 12). Is this to be a literal fire at
the end of the world? That seems probable, although we must
remember that St. John is now showing us “a world of symbols

525
