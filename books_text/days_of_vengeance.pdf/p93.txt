KING OF KINGS 1:4-6

What this means is that God is not “basically” one, with the
individual Persons being derived from the oneness; nor is God
“basically” three, with the unity of the Persons being secondary.
Neither God’s oneness nor His “threeness” is prior to the other;
both are basic. God is One, and God is Three. There are three
distinct, individual Persons, each of whom is God. But there is
only One God.!? To put it in more philosophical language,
God’s unity (oneness) and diversity (threeness, individuality) are
equally ultimate. God is basically One and basically Three at the
same time.'3

First, St. John describes the Father: Him who is, and whe
was, and who is to come. Philip Carrington has caught the spirit
of this expression, which is atrocious Greek but excellent theol-
ogy: the Being and the Was and the Coming.” God is eternal
and unchangeable (Mal. 3:6); as the early Christians faced what
seemed to them an uncertain future, they had to keep before
them the absolute certainty of God’s eternal rule. Gad is not at
the mercy of an environment; He is not defined by any external
conditions; all things exist in terms of His inerrant Word.
Threatened, opposed, and persecuted by thase in power, they
were nevertheless to rejoice in the knowledge of their eternal
God who “is to come,” who is coming continually in judgment
against His adversaries. God’s coming refers not simply to the
end of the world but to His unceasing rule over history. He
comes again and again to deliver His people and to judge the

 

12. Contrast this with the all-too-common Sunday School “illustrations” of
the Trinity —such as an egg, the sun, a pie, or water. These are generally more
misleading than helpful. In fact, their ultimate implications are heretical. They
end up either dividing God inte three “parts” —like an cgg’s shell, white, and
yolk —or showing God as one substance taking on three different forms, like
water (solid, liquid and gas).

13. On the radical impact of the doctrine of the Trinity in every area of life,
see R. J. Rushdoony, Foundations af Social Order and The One and the Many
(Tyler, TX: Thoburn Press, 1978).

14. Philip Carrington, The Meaning of the Revetation (London: SPCK,
1931), p. 74. In effect, the whole phrase is one proper noun, and indeclinable.
The grammatical problem arises from St. John’s attempt to render into Greek
the theological nuances contained in the Hebrew of Exodus 3:14: / AM WHO
7 AM. St. John is not afraid to massacre the Greck language in order to get
across a point, us in John 16:13, where he “incorrectly” uses a masculine pro-
noun in order to emphasize the Personality of the Holy Spirit (Spirit in Greek
is neuter, but St. John wanicd to stress that He is truly a He and not an It),

59
