7:13-14 PART THREE: THE SEVEN SEALS

“Great Tribulation” of which Jesus warned as He spoke to His
disciples on the Mount of Olives (Matt. 24:21; Mark 13:19)—a
tribulation that He stated would take place during the then-
existing generation (Matt. 24:34; Mark 13:30; Luke 21:32); the
greatest tribulation that ever was, or ever will be (Matt. 24:21;
Mark 13719),

The point, for the first-century Christians reading it, was
that the Tribulation they were about to suffer would not destroy
them. In facing persecution they were to see themselves, first, as
“the Israel of God” (Gal. 6:16), sealed and protected; and sec-
ond, as an innumerable, victorious multitude. As God saw
them, they were not scattered, isolated groups of poor and per-
secuted individuals accused as criminals by a merciless, demonic
power-State; they were, rather, a vast throng of conquerors,
who had washed their robes and made them white in the blood
of the Lamb, standing before God’s Throne and robed in the
righteousness of Jesus Christ. St. John is probably drawing on
the ordination-investiture ritual after the rigorous examination
for the priesthood. First, the prospective priest was examined as
to his geneology. “If he failed to satisfy the court about his per-
fect legitimacy, the candidate was dressed and veiled in black,
and permanently removed. If he passed that ordeal, inquiry was
next made as to any physical defects, of which Maimonides
enumerates a hundred and forty that permanently, and twenty-
two which temporarily disqualified for the exercise of priestly
office. . . . Those who had stood the twofold test were dressed
in white raiment, and their names permanently inscribed.”?° The
white robes of these priests thus correspond to the white robe of
their High Priest; and just as His robe is said to be “dipped in
blood,” so theirs are washed and made white in the blood of the
Lamb.

In striking contrast to what some Christian groups in recent
years have been taught, the early Church did not expect to be
miraculously preserved from all hardship in this life. They knew
that they would be called upon to suffer persecution (2 Tim.
3:12) and tribulation (John 16:33; Acts 14:22; Rom. 5:3; 8:35;

20. Alfred Edersheim, The Temple: Its Ministry and Services as They Were
at the Time of Jesus Christ (Grand Rapids: William B. Eerdmans Publishing
Co., 1980), p. 95; cf. Rev. 3:5.

220
