THE FEASTS OF THE KINGDOM 19:9

Lord’s Supper, observing it only on rare occasions (some so-
called churches have even abandoned Communion altogether).
What we must realize is that the official worship service of the
Church on the Lord’s Day is not merely a Bible study or some
informal get-together of like-minded souls; to the contrary, it is
the formal wedding feast of the Bride with her Bridegroom.
That is why we meet together on the first day of the week. In
fact, one of the primary issues in the controversy of the Protes-
tant Reformation was the fact that the Roman Church admitted
its members to the Eucharist only once a year.’ Ironically, the
practice of the Roman Church now excels that of most “Protes-
tant” churches; on the issue of frequent communion at least, it
is Rome which has “reformed.”

Commenting on the dictum of the German materialistic phi-
losopher Ludwig Feuerbach that “man is what he eats,” the
great Orthodox theologian Alexander Schmemann wrote:
“With this statement . . . Feuerbach thought he had put an end
to all ‘idealistic’ speculations about human nature. In fact, how-
ever, he was expressing, without knowing it, the most religious
idea of man. For long before Feuerbach the same definition of
man was given by the Bible. In the biblical story of creation man
is presented, first of ail, as a hungry being, and the whole world
as his food. Second only to the direction to propagate and have
dominion over the earth, according to the author of the first
chapter of Genesis, is God’s instruction to men to eat of the
earth: ‘Behold I have given you every herb bearing seed . . . and
every tree, which is the fruit of a tree yielding seed; to you it
shall be for meat. . . ." Man must eat in order to live; he must
take the world into his body and transform it into himself, into
flesh and blood. He is indeed that which he eats, and the whole
world is presented as one all-embracing banquet table for man.
And this image of the banquet remains, throughout the whole
Bible, the central image of life. It is the image of life at its crea-
tion and also the image of life at its end and fulfillment:
‘,.. that you eat and drink at my table in my Kingdom.’ ”#

7. See John Calvin, Jnstitutes af the Christian Religion, iv.xvii.43-46; cf.
idem., Selected Works: Tracts and Letters, ed. by Henry Beveridge and Jules
Bonnet, seven vols. (Grand Rapids: Baker Book House, reprinted 1983), Vol.
2, p. 188.

8. Alexander Schmemann, For the Life of the World: Sacraments and
Orthodoxy (New York: St. Vladimir's Seminary Press, 1973), p. 11.

477
