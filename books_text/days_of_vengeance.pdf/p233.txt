IN THE PATH OF THE WHITE HORSE 6:15-17

Also, the high places of Aven, the sin of Israel, will be destroyed;
Thorn and thistle will grow on their altars.

Then they will say to the mountains: Cover us!

And to the hills: Fall on us! (Hos. 10:6-8)

Jesus cited this text on His way to the crucifixion, stating
that it would be fulfilled upon idolatrous Israel within the life-
times of those who were then present:

And there were following Him a great multitude of the peo-
ple, and of women who were mourning and lamenting Him. But
Jesus turning to them said, Daughters of Jerusalem, stop weep-
ing for Me, but weep for yourselves and for your children. For
behold, the days are coming when they will say: Blessed are the
barren, and the wombs that never bore, and the breasts that
never nursed. Then they will begin to say to the mountains: Fall
on us! and to the hills: Cover us! (Luke 23:27-30)

As the churches in Asia Minor were first reading this vision,
the prophesied judgments were already taking place; the final
End was fast approaching. The generation that had rejected the
Landlord’s Son (cf. Matt. 21:33-45} would soon be screaming
these very words. The crucified and resurrected Lord was com-
ing to destroy the apostates. This was to be the great Day of the
outpoured wrath of the Lamb, whom they had slain.

i199
