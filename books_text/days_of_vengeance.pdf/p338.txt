2:3 PART FOUR! THE SEVEN TRUMPETS

through all the attempts by human empires to destroy the Seed
of the Covenant, the Dragon had been their foe. He wore the
diadems of the persecuting empires.

Why is the devil portrayed as a Dragon? In order to under-
stand this, we must consider the Biblical theology of dinosaurs,
which is surprisingly very detailed, While the Bible does speak
of land dinosaurs (cf. behemoth in Job 40:15-24),'® our focus
here will be on dragons and sea serpents (cf. Job 7:12; 41:1-34).7
Essentially, as part of God’s good creation (see Gen. 1:21: sea
monsters), there is nothing “evil” about these creatures (Gen.
1:31; Ps. 148:7); but, because of the Fall, they are used in Scrip-
ture to symbolize rebellious man at the height of his power and
glory.

Three kinds of dragons are spoken of in Scripture: Tannin
(Dragon; Ps. 91:13), Leviathan (Ps. 104:26), and Rahab (Job
26:12-13).'* The Bible relates each of these monsters to the Ser-
pent, who stands for the subtle, deceitful enemy of God's people
{Gen. 3:1-5, 13-15). Thus, to demonstrate the divine victory and
dominion over man’s rebellion, God turned Moses’ rod into a
“serpent” (Ex. 4:1-4), and Aaron’s rod into a “dragon” (tannin;
Ex, 7:8-12). The Dragon/Serpent, therefore, becomes in Scrip-
ture a symbol of Satanically inspired, rebellious pagan culture
(cf. Jer. 51:34), especially exemplified by Egypt in its war against
the Covenant people. This is particularly true with regard to the
monster Rahab (meaning the proud one), which is often a syn-
onym for Egypt (Ps. 87:4; 89:10; Isa. 30:7). God’s Covenant-
making deliverance of His people in the Exodus is described in
terms of both the original creation and God’s triumph over the
Dragon:

16. Some mistakenly suppose this to be a hippopotamus. Its description in
the Biblical text indicates that it was much closer to @ brontosaurus.

17, The creature mentioned in the latter reference, a huge, fire-breathing
dragon called Leviathan, is actually thought by some to be a crocodile! It is
clear from the statements in Job, however, that at least some great dinosaurs
were contemporaries of this early patriarch. For a sober-minded examination
of supposed sightings of sea monsters in more recent times, see Bernard Heu-
velmans, in the Wake of the Sea-Serpents (New York: Hill and Wang, 1968).
Duane T. Gish has proposed a possible explanation for the biology of “breath-
ing fire” in his Dinosaurs: Those Terrible Lizards (San Diego: Creation-Life
Publishers, 1977), pp. 50ff.

18. In Hebrew, this is a completely different word from the name of Rahab,
the Canaanite harlot who saved the Hebrew spics in Joshua 2.

304

 
