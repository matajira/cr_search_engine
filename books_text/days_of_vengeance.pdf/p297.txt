THE FAITHFUL WITNESS 10:4

are some things (many things, actually) that God has no inten-
tion of telling us beforehand.

This serves well as a rebuke to the tendency of most sermons
and commentaries on this book—that of a curious searching
into those things that God has not seen fit to reveal. “The secret
things belong to the Lorp our God, but the things revealed be-
tong to us and to our sons forever, that we may observe all the
words of this law” (Deut. 29:29). In other words, “Man has been
given the law, which he must obey. He has been told what the
consequences of obedience and disobedience are. More than
that, man does not need to know.”” R. J. Rushdoony writes:
“Man is more often prompted by curiosity than by abedience.
. .. For every question a pastor receives about the details of
God’s law, he normally receives several which express little more
than a curiosity about God, the life to come, and other things
which are aspects of ‘the secret things which belong to God.’. . .
As against curiosity and a probing about ‘secret things,’ we are
plainly commanded to obey God’s law and to recognize that the
law gives us a knowledge of the future which is legitimate.”#

In the final chapter of the book St. John is commanded: “Do
not seal up the words of the prophecy of this book, for the time
is near” (22:10); the message of the Book of Revelation as a
whole is contemporary in nature, referring to events about to
take place. In contrast, however, the message of the Seven
Thunders points us to the far distant future: Daniel was told to
“conceal these words and seal up the book until the time of the
end” (Dan. 12:4), for the reason that the time of its fulfillment
was not at hand. Similarly, when St. John is instructed to seal up
the words spoken by the Thunders, it is another indication that
the purpose of Revelation is not “futuristic”; the prophecy re-
fers to the time of the establishment of the New Covenant, and
points beyond itself to a “time of the end” that was still very dis-
tant to St. John and his readers. We are thus taught two things:
First, the Book of Revelation is a contemporary prophecy, con-
cerned almost entirely with the redemptive-eschatological events
of the first century; second, the events of the first century were

7, Rousas John Rushdoony, Salvation and Godly Rule (Vallecito, CA: Ross
House Books, 1983), p. 388.
8. [bid.

263
