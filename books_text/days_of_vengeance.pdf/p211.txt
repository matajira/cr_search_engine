CHRISTUS VICTOR 5:8-10

Judah conquered so as to open the Book, And because Jesus
Christ obtained the New Covenant for His people, He commis-
sioned the writing of the canonical Scriptures of the New Testa-
ment as the decisive and dramatic display of His triumphant
kingship, His “divine word of triumph.”

Along with the new written revelation, this new and final
stage of redemptive history brought by the New Covenant called
for a New Song, a new liturgical response by God’s worshiping
assembly. Just as the previous epochs in covenantal history
evoked a New Song,'4 the definitive establishment of the new
nation with its new kingdom-treaty necessitated a new worship,
one that would be a true fulfillment of the old, a transcending of
all that it foreshadowed. The new wine of the New Covenant
could not be contained in the wineskins of the Old; the new re-
demption required for its full and proper expression the New
Song of the Christian liturgy. This is exactly what the New Song
proclaims as its basis:

Kingdom-Treaty: Worthy art Thou to take the Book, and to
break its seals.

Redemption: For Thou wast slain, and didst purchase us for
God with Thy blood,

Nationhood: Thou hast made them to be a Kingdom and
priests to our God.

Dominion: And they will reign on the earth.

One aspect of the Song has raised a serious interpretive
issue: As we noted at 4:4, Ned Stonehouse (with a host of
others) heid that the twenty-four elders are a class of angels. The
basis for Stonehouse’s opinion boils down to the fact that one
Greek New Testament manuscript contains a textual variation
which, he claimed, indicates this. Whereas most manuscripts
read that Christ purchased us, the variant reading preferred by
Stonehouse says that Christ purchased men. The difference, ob-
viously, would be that the singers in the first case are definitely

14, Songs produced by the Exodus redemption include those recorded in
Ex. 15, Deut. 32, and Ps. 90; the new organization of the theocratic kingdom
under a human ruler, and the events leading to the establishment of the Tem-
ple, resulted in the Psalter (the definitive collection of “new songs” under the
Old Covenant).

17
