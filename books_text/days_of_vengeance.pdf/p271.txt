LITURGY AND HISTORY 8:6-7

consolation of its own—a consolation which cannot deceive,
and which has in it a surer hope than the tottering and falling
affairs of life can afford. They will not refuse the discipline of
this temporal life, in which they are schooled for life eternal;
nor will they lament their experience of it, for the good things of
life they use as pilgrims who are not detained by them, and its
ills either prove or improve them.

“As for those who insult over them in their trials, and when
ills befall them say, ‘Where is thy God?’ [Ps, 42:10] we may ask
them where their gods are when they suffer the very calamities
for the sake of avoiding which they worship their gods, or main-
tain they ought to be worshipped; for the family of Christ is fur-
nished with its reply: Our God is everywhere present, wholly
everywhere; not confined to any place. He can be present unper-
ceived, and be absent without moving; when He exposes us to
adversities, it is either to prove our perfections or correct our
imperfections; and in return for our patient endurance of the
sufferings of time, He reserves for us an everlasting reward. But
who are you, that we should deign to speak with you even about
your own gods, much less about our God, who is ‘to be feared.
above all gods? For all the gods of the nations are idols; but the
Lorp made the heavens’ [Ps. 96:4-5]."+

The wicked, on the other hand, have only wrath and
anguish, tribulation and distress ahead of them (Rom. 2:8-9).
Literally, the vegetation of Judea, and especially of Jerusalem,
would be destroyed in the Roman scorched-earth methods of
warfare: “The countryside, like the city, was a pitiful sight, for
where once there had been a multitude of trees and parks, there
was now an utter wilderness stripped bare of timber; and no
stranger who had seen the old Judea and the glorious suburbs of
her capital, and now beheld utter desolation, could refrain from
tears or suppress a groan at so terrible a change. The war had
blotted out every trace of beauty, and noe one who had known it
in the past and came upon it suddenly would have recognized
the place, for though he was already there, he would still have
been looking for the city.”!5 Yet this was only the beginning;
many more sorrows—and much worse—lay ahead (cf. 16:21),

14. St. Augustine, The City af God, i.29 (Marcus Dods, trans.; New York:
The Modern Library, 1950, pp. 34f.).
15. Josephus, The Jewish War, vii.

237

 
