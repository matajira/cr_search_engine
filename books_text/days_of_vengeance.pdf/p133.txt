THE SPIRIT SPEAKS TO THE CHURCH: OVERCOME! 2:7

no matter what are the particular problems facing it, each
church is under divine mandate to conquer and completely over-
whelm its opposition. The duty of overcoming is not something
reserved for a select few “super-Christians” who have “dedi-
cated” themselves to God over and above the usual require-
ments for Christians. All Christians are overcomers: Whatever
is born af God overcomes the world; and this is the victory that
has overcome the world—our faith (1 John 5:4). The Christians
spoken of in Revelation overcame the devil “because of the
blood of the Lamb and because of the Word of their testimony”
(12:11). The question is not one of victory or defeat. The ques-
tion is victory or treason.

The Christian overcomes; and to him Christ grants the privi-
lege to eat of the Tree of Life, which is in the Paradise of My
God. This is not only an otherworldly hope. Although the full
consummation of this promise is brought in at the end of his-
tory, it is a present and increasing possession of the people of
God, as they obey their Lord and take dominion over the earth.
For the Tree of Life is Jesus Christ Himself, and to partake of
the Tree is to possess the blessings and benefits of salvation.” In
Christ, the overcoming Christian has Paradise Restored, in this
life and forever.

Smyrna: Judgment on the False Israel (2:8-i1)

8 And to the angel of the church in Smyrna write: The First
and the Last, who was dead, and has come to life, says this:

9 I know your works and your tribulation and your poverty
(but you are rich), and the blasphemy by those who say they
are Jews and are not, but are a synagogue of Satan.

10 Do not fear what you are about to suffer. Behold, the devil is
about to cast some of you into prison, that you may be
tested, and you will have tribulation ten days. Be faithful
unto death, and | will give you the crown of life.

7. The Cross has long been used in Christian art as a symbol far the Tree of
Life. There is strong evidence, however, that Christ was actually crucified on a
living tree (with his wrists nailed to the crosspiece he carried and his feet nailed to
the trunk; cf. Acts 5:30; 10:39; 13:29; Gal. 3:13; 1 Pet. 2:24). The symbol of the
Cross is simply a stylized tree, and was often pictured in ancient churches and
tombs with branches and leaves growing out of it. See Ernest L, Martin’s fasci-
nating and informative work, The Place of Christ's Crucifixion: Its Discovery
and Significance (Pasadena: Foundation for Biblical Research, 1984), pp. 75-94.

99
