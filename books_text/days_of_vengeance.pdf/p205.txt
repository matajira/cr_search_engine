CHRISTUS VICTOR 5:5-7

not simply a matter of the past causing the future; it is also true
that the future causes the past, as R. J. Rushdoony explains:
“The movement of time, according to the Bible, is from eternity,
since it is created by God and moves out of and in terms of His
eternal decree, . . . Because time is predestined, and because its
beginning and end are already established, time does not de-
velop in evolutionary fashion from past to present to future. In-
stead, it unfolds from future to present to past.”

A simple illustration might help us understand this. Let us
say someone finds you packing a sack lunch on a warm Saturday
morning, and asks the reason for it. You answer, “Because I’m
going to have a picnic at the park today.” What has happened?
Ina sense, the future —the planned picnic —has determined the
past, Because you wanted a picnic at the park, you then planned
a lunch. Logically, the picnic preceded, and caused, the making
of the hunch, even though it followed it chronologically. In the
same way, God desired to glorify Himself in Jesus Christ; there-
fore He created Jesse and David, and all the other ancestors of
Christ’s human nature, in order to bring His Son into the world.
The Root of David’s very existence was the Son of David, Jesus
Christ. The “effect” determined the “cause”!”

The Lord Jesus Christ is thus presented in the most radical
way possible as the Center of all history, the divine Root as well
as the Branch, the Beginning and the End, Alpha and Omega.
And it is as the conquering Lion and the determining Root that
He has prevailed so as to open the Book and its seven seals.

St. John turns to see the One who is described in this way —
and, instead of seeing a Lion or 2 Root, he sees a Lamb standing
before the Throne. This is the pattern we first noticed at 1:11, in
which John first Aears, then sees. Obviously, the One St. John
heard about in verse 5 is identical with the One he now beholds
in verse 6. The Lion is the Lamb.

6. Rousas John Rushdoony, The Biblical Philosophy of History (Nutley,
NJ: Presbyterian and Reformed Publishing Co., 1969), p. Il; cf. Rushdoony,
The One and the Many, p. 145; St. Augustine, The City of God, Bk. XI,
Chap. 13-15; Nathan R. Wood, Tie Secret of the Universe (Grand Rapids:
William B. Eerdmans Publishing Co., [1936] 1955), pp. 43-45.

7. One of the clearest statements of this idea is in Gordon H. Clark, Biblical
Predestination (Nutley, NJ: Presbyterian and Reformed Publishing Co.,
1969), esp. pp. 18-30.

171
