16:4-7 PART FIVE: THE SEVEN CHALICES

“One could see the whole lake stained with blood and
crammed with corpses, for not a man escaped. During the days
that followed a horrible stench hung over the region, and it
presented an equally horrifying spectacle. The beaches were
strewn with wrecks and swollen bodies, which, hot and clammy
with decay, made the air so foul that the catastrophe that plunged
the Jews in mourning revolted even those who had brought it
about.”

4-7 The plague of the Third Chalice more directly resembles
the first Egyptian plague (and the Third Trumpet: cf. 8:10-11),
since it affects the rivers and the springs of waters, turning all
the drinking water to blood. Water is a symbol of life and bless-
ing throughout Scripture, beginning from the story of creation
and the Garden of Eden.® In this plague, the blessings of Para-
dise are reversed and turned into a nightmare; what was once
pure and clean becomes polluted and unclean through apostasy.

The Angel of the Waters responds to this curse by praising
God for His just judgment: Righteous art Thou, who art and
who wast, O Holy One, because Thou didst judge these things.
We should not be embarrassed by a passage such as this. The
whole Bible is written from the perspective of cosmic personal-
ism —the doctrine that God, who is absolute personality, is con-
stantly active throughout His creation, everywhere present with
the whole of His being, bringing all things to pass immediately
by His power and mediately through His angelic servants. There
is no such thing as natural “law”; rather, as Auguste Lecerf has
said, “the constant relations which we call natural laws are
simply ‘divine habits’: or, better, the habitual order which God
imposes on nature. It is these habits, or this habitual proc-
ess, which constitute the object of the natural and physical
sciences.”9

This is what guarantees the validity and reliability of both
scientific investigation and prayer: On the one hand, God’s
angels have habits—a cosmic dance, a liturgy involving every

7. Flavius Josephus, The Jewish War, iii.x.9.

8. David Chilton, Paradise Restored: A Biblical Theology af Daminion
(Ft. Worth, TX: Dominion Press, 1985), pp. 18ff, 30f.

9. Auguste Lecerf, An Introduction to Reformed Dogmatics, trans. André
Schlemmer (Grand Rapids: Baker Book House, [1949] 1981), p. 147.

400
