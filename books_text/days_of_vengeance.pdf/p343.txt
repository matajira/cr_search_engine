THE HOLY WAR 12:6

the Virgin, born of Israel to rule the nations. In this verse St.
John telescopes the entire history of Christ’s earthly ministry,
stating (as if it had happened all at once) that her Child was
caught up to God and to His Throne. It is as if Christ’s Incarna-
tion had led directly to His Ascension to the Throne of glory.
St. John’s point is not to belittle the atonement and the resurrec-
tion, but to stress that the Lord’s Anointed completely escapes
the power of the Dragon; and we should note that St. John’s
order follows that of the Psalm. Telling of His exaltation ta the
heavenly Throne, the Christ says:

I will surely tell of the decree of the Lorp:

He said to Me, “Thou art My Son,

Today J have begotten Thee.

Ask of Me, and I will surely give the nations as Thine inheritance,
And the very ends of the earth as Thy possession.

Thou shalt rule them with a rod of iron,

Thou shait shatter them like earthenware.” (Ps. 2:7-9)

“The Psalm makes Messiah’s heavenly birth all one with his
enthronement; if he is fathered by God, he reigns.”25 In spite of
everything that the Dragon does, the Seed is caught up to the
Throne and now rules the nations with a rod of iron, just as if
He had gone straight from the Incarnation to the Throne; Satan
had no power to stop Him. The Ascension was the goal of Christ’s
Advent.

6 And the Woman fled into the wilderness where she has a
place prepared by God. As will become apparent below, the
Woman’s flight into the wilderness is a picture of the flight of the
Judean Christians from the destruction of Jerusalem, so that the
Dragon’s wrath is expended upon apostate rather than faithful
Israel. While she is in the wilderness, the Woman is nourished
for twelve hundred and sixty days, a period equivalent to the
“time, times, and half a time” (34 years) of verse 14, and sym-

24, Some will argue that this phrase refers not to the incarnation or physical
birth of Christ, but to His eternal generation instead; for John’s purposes of
Biblical allusion, however, that question is beside the point. His emphasis is,
with the Psalmist, that the Child goes from birth to reign.

23, Farrer, p. 141.

26. For the relationship of the 1,260 days to the number of thc Beast (666),
see comments on 13:18.

309
