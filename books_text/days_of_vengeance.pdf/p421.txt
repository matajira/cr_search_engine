SEVEN LAST PLAGUES 15:4

Exodus 15, so the victorious Church celebrates her true Sabbath
of rest by singing this same ‘Song of Moses and of the Lamb,’
only in language that expresses the fullest meaning of the Sab-
bath songs in the Temple.”?

It is probably impossible to track down the Song’s Old Testa-
ment allusions completely, but I have at least noted some of
them: Great and marvelous are Thy works, O Lord God, the
Almighty (Ex. 34:10; Deut. 32:3-4; 1 Chron, 16:8-12; Ps. 92:5;
111:2; 139:14; Isa, 47:4; Jer. 10:16; Amos 4:13; cf. Rev. 1:8); St.
John makes it clear that the saints are not merely making a gen-
eral statement of fact, but instead are specifically referring to
the “great and marvelous” finel judgments in which “the wrath
of God is finished” (15:1. Righteous and true are Thy ways
(Deut. 32:4; Ps. 145:17; Hos. 14:9); again, God is said to be
“righteous and true” with special reference to His saving judg-
ments, delivering the Church and destroying His enemies (cf,
16:7). “In seasons of tribulation on earth, when the worldly
power appears to triumph over the church, she has often been
led to doubt the greatness of God’s works, the justice and truth
of His ways; to doubt whether He were really the king of the
heathen. Now this doubt is put to shame; it is dispelled by
deeds; the clouds, which veiled the glory of God from her eyes,
are made entirely to vanish.”® Thou King of the uations (Ps.
22:28; 47:2, 7-8; 82:8; cf. 1 Tim. 1:17; 6:15; Rev. 1:5; 19:16); as
Ruler of all nations He moves the armies of earth to fulfill His
purposes in judgment; He smashes them for their rebellion; and
He brings them to repentance.

4 Who will not fear Thee, O Lord, and glorify Thy name?
(Ex. 15:14-16; Jer. 10:6-7; cf. Rev. 14:7); this means, in language
we are more familiar with: Who will not be converted? Who will
not serve God, worship Him, and obey Him? The clear implica-
tion (to be made explicit in the next sentence) is that the over-
whelming majority of all men will come into the salvation that
God has provided in Jesus Christ, This is the great hope of the

7. Alfred Edersheim, The Temple: Its Ministry and Services As They Were
at the Time of Jesus Christ (Grand Rapids: William B. Eerdmans Publishing
Co., 1980}, p. 76.

8. E. W. Hengstenberg, The Revelation of St. John, two vols, (Cherry Hill,
NJ: Mack Publishing Co., [1851] 1972), Vol. 2. pp. 146f.

387
