THE DAYS OF VENGEANCE

of the world. Again and again it specifically warns that “the time
is near!” St. John wrote his book as a prophecy of the approach-
ing destruction of Jerusalem in a.p. 70, showing that Jesus
Christ had brought the New Covenant and the New Creation,
Revelation cannot be understood unless this fundamental fact is
taken seriously.

4. Revelation is a worship service. St. John did not write a
textbook on prophecy. Instead, he recorded a heavenly worship
service in progress. One of his major concerns, in fact, is that
the worship of God is central to everything in life. It is the most
important thing we do. For this reason I have devoted special
attention throughout this commentary to the very considerable
liturgical aspects of Revelation, and their implications for our
worship services today.

5. Revelation is a book about dominion, Revelation is not a
book about how terrible the Antichrist is, or how powerful the
devil is. It is, as the very first verse says, The Revelation of Jesus
Christ, It tells us about His lordship over all; it tells us about our
salvation and victory in the New Covenant, Gad’s “wonderful
plan for our fife”; it telis us that the kingdom of the world has
become the Kingdom of our God, and of His Christ; and it tells
us that He and His peopie shall reign forever and ever.

1 have many people to thank for making this book possible.
First and foremost, I am grateful to Dr. Gary North, without
whose patience and considerable financial investment it simply
could not have been written. The week [ moved to Tyler, Gary
took me along on one of his periodic book-buying sprees at a
large used bookstore in Dallas. As I] helped him haul hundreds
of carefully chosen volumes to the checkstand {I bought a few
books, too—a couple every hour or so, just to keep my hand in
the game), Gary asked me what long-term project I'd like to
work on, along with my other duties at the Institute for Christian
Economics. “How about a medium-sized, popular-style, intro-
ductory-level, easy-to-read book on Revelation?” I suggested, “I
think I could knock something like that out in about three
months.” That was, almost to the day, 3 years and six months
ago~or, as Gary might be tempted to mutter under his breath:
A time, times, and half a time, At last, the tribulation has ended.

The book, of course, has vastly outgrown its projected size
and scope. No small part of that is due to the Rev. James B.

xii
