THE SYMBOLISM OF REVELATION

Eden.

Indeed, Paradise is where prophecy began. It is worth noting
that the very first promise of the coming Redeemer was stated in
highly symbolic terms. Gad said to the Serpent:

I will put enmity

Between you and the woman

And between your seed and her Seed;

He shall crush your head,

And you shall strike His heel, (Gen, 3:15)

Obviously, this is not simply “history written in advance.” It
is a symbolic statement, very much of a piece with the evacative,
poetic language used throughout the Bible, and especially in
Revelation. In fact, St. John plainly tells us in his opening sen-
tence that the Revelation is written in signs, in symbols. He did
not intend it to be read like a newspaper or a stock market anal-
ysis. He expected his audience ta respond to his prophecy in
terms of the Bible’s own system of symbolism.

l repeat: the Bible’s own system of symbolism. The meaning
of a symbol is not whatever we choose to make it; nor did St.
John create the images of the Book of Revelation out of his own
imagination. He presents Christ to his readers as a Lion and a
Lamb, not because he thinks those are pretty pictures, but be-
cause of the connotations of lions and lambs already established
in the Bible. The Book of Revelation thus tells us from the out-
set that its standard of interpretation is the Bible itself. The
book is crammed with allusions to the Old Testament. Merrill
Tenney says: “It is filled with references to events and characters
of the Old Testament, and a great deal of its phraseology is
taken directly from the Old Testament books. Oddly enough,
there is not one direct citation in Revelation from the Old Testa-
ment with a statement that it is quoted from a given passage; but
a count of the significant allusions which are traceable both by
verbal resemblance and by contextual connection to the Hebrew
canon number three hundred and forty-eight. Of these approx-
imately ninety-five are repeated, so that the actual number of
different Old Testament passages that are mentioned are nearly

“65. See Chilton, Paradise Restored, pp. 15-63.
29
