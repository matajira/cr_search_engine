THE DAYS OF VENGEANCE

Mantzaridis, Georgios 1. The Deification af Man. Translated by
Liadain Sherrard. Crestwood, NY: St. Vladimir’s Seminary Press,
1984,

Martin, Ernest L. The Birth of Christ Recaiculated. Pasadena: Foun-
dation for Biblical Research, 1980.

—_______. The Original Bible Restored, Pasadena: Foundation for
Biblical Research, 1984.

. The Place of Christ's Crucifixion: lis Discovery and Sig-
nificance. Pasadena: Foundation for Biblical Research, 1984.

McGuiggan, Jim, and King, Max. The McGuiggan-King Debate. War-
ren, OH: Parkman Road Church of Christ, n.d.

McKelvey, R. J, “Temple.” In J. D. Douglas, ed. The New Bible Dic-
tionary. Grand Rapids: William B. Eerdmans Publishing Cc.,
[1962] 1965, pp. 1242-50.

Minear, Paul. images of the Church in the New Testament. Philadel-
phia: The Westminster Press, 1960.

Moore, Thomas V. A Commentary on Haggai, Zechariah, and Mal-
achi. Edinburgh: The Banner of Truth Trust, [1856] 1968.

Morris, Leon. Apocalyptic. Grand Rapids: William B. Eerdmans
Publishing Co., 1972.

—_______.. The Apastolic Preaching of the Cross. Grand Rapids:
William B. Eerdmans Publishing Co., 1955.

Murray, Iain. The Puritan Hope: A Study in Revival and the Interpre-
tation of Prophecy. Edinburgh: The Banner of Truth Trust, 1971.

Murray, John. Collected Writings. 4 vols. Edinburgh: The Banner of
Truth Trust, 1976-82.

. The Epistle ta the Romans, 2 vols. Grand Rapids: Wil-
liam B. Eerdmans Publishing Co., 1968.

. The Imputation of Adam's Sin, Nutley, NJ: The Presby-
terian and Reformed Publishing Co., [1959] 1977.

. Redemption: Accomplished and Applied. Grand Rap-
ids: William B. Eerdmans Publishing Co., 1955.

Negev, Avraham, ed. The Archaeological Encyclopedia af the Holy
Land. Nashville: Thomas Nelson Publishers, revised ed., 1986.
Nisbet, Robert. History of the Idea of Progress. New York: Basic

Books, 1980.

North, Gary. Backward, Christian Soldiers? An Action Manual for
Christian Reconstruction. Tyler, TX: Institute for Christian Eco-
nomics, 1984.

. Conspiracy: A Biblical View. Fort Worth: Dominion
Press, 1986.

. The Dominion Covenant: Genesis. Tyler, TX: Institute
for Christian Economics, 1982.

674
