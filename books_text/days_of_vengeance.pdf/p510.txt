19:9 PART FIVE: THE SEVEN CHALICES

that the Eucharist is the center of Christian worship; the Euch-
arist is what we are commanded to do when we come together
on the Lord’s Day. Everything else is secondary. This is not to
suggest that the secondary things are unimportant. The teaching
of the Word, for example, is very important, and in fact neces-
sary for the growth and well-being of the Church. Doctrine has
long been recognized as one of the essential marks of the
Church. Instruction in the faith is therefore an indispensable
part of Christian worship. But it is not the heart of Christian
worship, The heart of Christian worship is the Sacrament of the
Body and Blood of our Lord Jesus Christ. This is assumed by
St. Paul in 1 Corinthians 10:16-17 and 11:20-34, We can see it re-
flected in Luke’s simple statement in Acts 20:7: “And on the first
day of the week, when we were gathered together to break
bread... .” It is also described in the Didache: “But every
Lord’s Day do ye gather yourselves together, and break bread,
and give thanksgiving after having confessed your transgres-
sions, that your sacrifice may be pure.” Justin Martyr reports
the same pattern as the standard for all Christian assemblies:
“On the day called Sunday, all who live in cities or in the country
gather together to one place, and the memoirs of the apostles or
the writings of the prophets are read, as long as time permits;
then, when the reader has ceased, the president verbally in-
structs, and exhorts to the imitation of these good things. Then
we all rise and pray, and, as we before said, when our prayer is
ended, bread and wine and water are brought, and the president
in like manner offers prayers and thanksgivings, according to his
ability, and the people assent, saying, Amen; and there is a dis-
tribution to each, and a participation of that over which thanks
have been given, and to those who are absent a portion is sent
by the deacons.”*

The greatest privilege of the Church is her weekly participa-
tion in the Eucharistic meal, the Marriage Supper of the Lamb.
It is a tragedy that so many churches in our day neglect the

5. The Teaching of the Twelve Apostles, xiv.1, in Alexander Roberts and
James Donaldson, eds,, The Ante-Nicene Fathers (Grand Rapids: Wm. B.
Eerdmans, reprinted 1971), Vol. 7, p. 381.

6. Justin Martyr, The First Apology, chap. xvii, in Alexander Roberts and
James Donaldson, eds., The Ante-Nicene Fathers (Grand Rapids: Wm. B.
Eerdmans, reprinted 1971), Vol. 1, p. 186.

476
