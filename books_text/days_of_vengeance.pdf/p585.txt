THE NEW JERUSALEM 21:8

11 having the glory of God. Her luminary was like a very costly
stone, as a stone of crystal-clear jasper.

12 She had a great and high wall, with twelve gates, and at the
gates twelve angels; and names were written on them, which
are those of the twelve tribes of the sons of Israel.

13 There were three gates on the east and three gates on the north
and three gates on the south and three gates on the west.

14 And the wall of the City had twelve foundation stones, and
on them were the twelve names of the twelve apostles of the
Lamb.

15 And the one who spoke with me had a measure, a gold reed
to measure the City, and its gates and wall.

16 And the City is laid out as a square, and its length is as great
as the width; and he measured the City with the reed, twelve
thousand stadia; its length and width and height are equal.

17 And he measured its wall, one hundred forty-four cubits,
according to human measurements, which are also angelic
measurements.

18 And the material of the wall was jasper; and the City was
pure gold, like clear glass.

19 The foundation stones of the City were adorned with every
kind of precious stone, The first foundation stone was jas-
per; the second, sapphire; the third, chalcedony; the fourth,
emerald;

20 the fifth, sardonyx; the sixth, sardius; the seventh, chryso-
lite; the eighth, beryl; the ninth, topaz; the tenth, chryso-
prase; the eleventh, jacinth; the twelfth, amethyst.

21 And the twelve gates were twelve pearls; each one of the
gates was a single pearl. And the street of the City was pure
gold, like transparent glass.

22 And I saw no Sanctuary in it, for the Lord God, the
Almighty, and the Lamb, are its Sanctuary.

23 And the City has no need of the sun or of the moon to shine
upon it, for the glory of God has illumined it, and its lamp is
the Lamb.

24 And the nations shall walk by its light, and the kings of the
earth shall bring their glory and honor into it.

25 And in the daytime (for there shall be no night there) its
gates shall never be closed;

26 and they shall bring the glory and honor of the nations into it;

27 and nothing unclean and no one who practices abomination
and lying, shall ever come inta it, but only those whose
names are written in the Lamb's Book of Life.

551
