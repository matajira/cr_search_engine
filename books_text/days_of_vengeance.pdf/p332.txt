1211-2 PART FOUR: THE SEVEN TRUMPETS

dominion—indeed, of her ascent from glory to glory (1 Cor.
15:41; 2 Cor. 3:18), Solomon proclaims that the Bride is “lovely
as Jerusalem, terrible as an army with banners” (Cant. 6:4); she

looks forth like the dawn,
Beautiful as the full moon,
Resplendent as the sun,
Terrible as an army with banners. (Cant. 6:10)

This Woman, St. John says, is the Mother of Christ: She is
seen to be with child (the same Greek expression used of the Vir-
gin Mary in Matthew 1:18, 23), carrying in her womb the Mes-
siah who is destined “to rule all the nations with a rod of iron”
(v. 5). The image of the Woman/Mother has its origins all the
way back to the Garden of Eden and the protevangelium—the
first proclamation of the Gospel, in which God revealed that
through the Woman would come the Redeemer to crush the Ser-
pent’s head (Gen, 3:15). The picture then becomes a regular
motif in the historical outworking of God’s purposes with
Israel. One familiar example occurs in the story of Jael and Sis-
era, which tells how the enemy of God’s people is destroyed, his
head shattered, by a woman (Jud. 4:9, 17-22; 5:24-27; ef. the
death of Abimelech in Jud. 9:53), This is also a major theme in
the story of Esther and her deliverance of Israel. The definitive
fulfillment of this prophecy took place in the Virgin Birth, as
Mary clearly recognized:

He has done mighty deeds with His arm;

He has scattered those who were proud in the thoughts of their heart.
He has brought down rulers from their thrones,

And has exalted those who were humble.

He has filled the hungry with good things;

And sent away the rich empty-handed.

He has given help to Israel His servant,

In remembrance of His mercy,

As He spoke to our fathers,

To Abraham and his seed forever. (Luke 1:51-55)

Isaiah’s prophecy of the Virgin Mother is the specific Biblical
background for St. John’s vision of the Woman, as Philip Car-
rington explains: “The actual words are drawn not from any

298
