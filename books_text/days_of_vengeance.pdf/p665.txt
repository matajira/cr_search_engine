COMMON GRACE, ESCHATOLOGY, AND BIBLICAL LAW

But this salvation is expressly Ais¢orica/ in nature. Christ died in time
and on earth for His people. They are regenerated in time and on
earth, He therefore preserves the earth and gives all men, including
rebels, time.

With respect to God's restraint of the total depravity of men, con-
sider His curse of the ground (Gen. 3:17-19). Man must labor in the
sweat of his brow in order to eat. The earth gives up her fruits, but
only through labor. Still, this common curse also involves common
grace. Men are compelled to cooperate with each other in a world of
scarcity if they wish to increase their income. They may be murderers
in their hearts, but they must restrain their emotions and cooperate.
The division of labor makes possible the specialization of production.
This, in turn, promotes increased wealth for all those who labor. Men
are restrained by scarcity, which appears to be a one-sided curse. Not
$0; it is equally a blessing. This is the meaning of common grace; com-
mon curse and common grace go together.

The cross is the best example of the fusion of grace and curse.
Christ was totally cursed on the cross. At the same time, this was
God’s act of incomparable grace. Justice and mercy are linked at the
cross. Christ died, thereby experiencing the curse common to all men.
Yet through that death, Christ propitiated God. That is the source of
common grace on earth —life, law, order, power—as well as the source
of special grace. The common curse of the cross—death —led to spe-
cial grace for God's elect, yet it also is the source of that common
grace which makes history possible. Christ suffered the “first death,”
not to save His people from the first death, and not to save the unre-
generate from the second death of the lake of fire. He suffered the first
death to satisfy the penalty of sin—the first death (which Adam did
not immediately pay, since he did not die physically on the day that he
sinned) and the second death (God's elect will never perish).

At some time in the future, God will cease to restrain men’s evil
(il Thess. 2:6-12). As He gave up Israel to their lusts (Ps. 81:12;
106:15), so shall He give up on the unregenerate who are presently held
back from part of the evil that they would do. This does not necessar-
ily mean that the unregenerate will then crush the peaple of God. In
fact, it means precisely the opposite. When God ceased to restrain
Israel, Israel was scattered. (True, for a time things went badly for
God's prophets.) But the very act of releasing them from His restraint
allowed God to let them fill up their own cup of iniquity. The end
result of God’s releasing Israel was their fall into iniquity, rebellion,
and impotence (Acts 7:42-43). They were scattered by the Assyrians,
the Babylonians, and finally the Romans. The Christian church
became the heir to God's kingdom (Matt. 21:43), The Romans, too,

633
