2:26-29 PART TWO! THE SEVEN LETTERS

order and morality came to mean monotony and devitalizing,
enervating palls, whereas lawlessness means liberty and power.
The middle-aged ‘fling’ and sexual license came into being as a
grasping after renewal, and Negress prostitutes came to be used
as a ‘change of luck’ device, an especial sin against order as a
means of a recharging of luck and power. Basic to all these man-
ifestations, from ancient Egypt through Caesar to modern man,
is one common hope: destroy order to create order afresh, or,
even more bluntly, destroy order to create order.”23

But, Christ says, there are faithful Christians in Thyatira,
who do not hold this teaching, who have not sought after for-
bidden knowledge in Satanic practices, despite the economic
and social consequences of their refusal to compromise; I place
no other burden on you. Nevertheless what you have, hold fast
until I come. This, again, reflects the language of the Jerusalem
Council’s letter to the Gentile converts: “For it seemed good to
the Holy Spirit and to us to lay upon you ne greater burden than
these essentials: that you abstain from things sacrificed to idols
... and from fornication; if you keep yourselves free from
such things, you will do well” (Acts 15:28-29), The faithful are
to continue practicing the essentials of the faith, holding to or-
thodox standards of doctrine and life, until Christ comes with
tribulation to judge the heretics and apostates who are illegally
remaining in the Church.

26-29 The faithful Christians in Thyatira were suffering
from both the heathen world outside and the compromising her-
etics within the church. They probably were tempted to doubt
whether they would ever win in this struggle. The most prosper-
ous and successful Christians were the ones who were the most
faithless to Christ; it looked as if the orthodox were fighting a
losing battle. They were so powerless by now that they were un-
able even to oust the apostates from the church. Yet Christ
promises the angel/bishop: He who overcomes, and he who
keeps My deeds until the end, to him I will give authurity over
the nations. And he shall rule them with a rod of iron, as the
vessels of a potier are broken to pieces, as I also have received

23. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of
Order and Uitimacy (Tyler, TX: Thoburn Press, [1971] 1978), p. 105.

6
