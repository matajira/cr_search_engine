18:3-5 PART FIVE: THE SEVEN CHALICES

8:39, 43, 49; 2 Chron. 30:27; Ps. 33:14; 76:2; 107:7; Eph. 2:22).
Jerusalem, which had been God’s dwelling place, has now be-
come the unclean dwelling place of demons.

3 Israel’s abandonment and perversion of her calling as
teacher-priest to the nations is again stated to be the reason for
her destruction (cf. 14:8; 17:2, 4). She has committed fornica-
tion with the nations, with the kings, and with the merchants,
prostituting her gifts instead of leading the nations to the King-
dom, joining with them in the attempted overthrow of the King.
The stress on the merchants is most likely related to the com-
mercial activities around the Temple (see below, on 18:11-17a).
The corruption of Temple commerce affected the liturgy of the
nation. All of life flows from the religious center of culture;3 if
the core is rotten, the fruit is worthless. This is why Jesus came
into conflict with the Temple moneychangers (Matt. 21:12-13;
John 2:13-22). Observing that many of the shops belonged to
the family of the high priest, Ford cites Josephus’ characteriza-
tion of the high priest Ananias as “the great procurer of money.”
In particular, “the court of the Gentiles appears to have been the
scene of a flourishing trade in animal sacrifice, perhaps sup-
ported by the high priestly family.”* This would agree with the
observation already made, that Babylon is no ordinary prosti-
tute: Her punishment by fire indicates that she is of the priestly
class (see on 17:16),

4-5 Since Israel was to be destroyed, the apostles spent
much of their time during the Last Days summoning God’s peo-
ple to a religious separation from her, urging them to align
themselves instead with the Church (cf, Acts 2:37-40; 3:19-26;
4:8-12; 5:27-32). This is St. John’s message in Revelation. God’s
people must not seek to reform Israel, with its new religion of
Judaism, but must abandon her to her fate. The Jews had “tasted
the good Word of God and the powers of the Age to come”—the

3, See Henry R. Van Til, The Calvinistic Concept af Culture (Philadelphia:
The Presbyterian and Reformed Publishing Co., 1959); Abraham Kuyper,
Lectures on Calvinism (Grand Rapids: William B, Eerdmans Publishing Co.,
1991},

4. J. Massyngberde Ford, Revelation: Introduction, Transtation, and Com-
mentary (Garden City: Doubleday and Co., 1975), pp. 301f.

448
