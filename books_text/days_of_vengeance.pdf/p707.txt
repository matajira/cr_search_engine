SELECT BIBLIOGRAPHY

—_______. Moses and Pharaoh: Dominion Religion Versus Power
Religion, Tyler, TX: Institute for Christian Economics, 1985,

. The Sinai Strategy: Economics and the Ten Command-
ments. Tyler, TX: Institute for Christian Economics, 1986.

—_—_____... Unconditional Surrender: Ged’s Program for Victory.
Tyler, TX: Geneva Ministries, 1983.

—___.. Unholy Spirits: Occuitism and New Age Humanism.
Fort Worth: Dominion Press, 1986.

—___., ed. Foundations of Christian Scholarship: Essays in the
Van Til Perspective. Vallecito, CA: Ross House Books, 1976.

—____., ed. Symposium on the Millennium, The Journal of
Christian Reconstruction, Vol. U1, No. 2 (Winter, 1976-77).

Owen, John. An Exposition of the Epistle to the Hebrews. 7 vols. Ed-
ited by W. H. Goold. Grand Rapids: Baker Book House, [1855] 1980.

__. -~Works, 16 vols. Edited by W. H. Goold, London: The
Banner of Truth Trust, [1850-53] 1965-68,

Paher, Stanley W. If Thou Hadst Known. Las Vegas: Nevada Publica-
tions, 1978.

Paquier, Xichard. Dynamics of Worship: Foundations and Uses of
Liturgy. Philadelphia: Fortress Press, 1967.

Pfeiffer, Charles F., and Vos, Howard F. The Wycliffe Historical Geog-
raphy of Bible Lands, Chicago: Moody Press, 1967.

Plumptre, E. H. Ezekiel. London: Funk and Wagnalls Co., n.d.

Ridderbos, Herman. The Coming of the Kingdom. St. Catherines,
Ont.: Paideia Press, [1962] 1978.

Roberts, Alexander, and Donaldson, James, eds. The Ante-Nicene
Fathers. 10 vols. Grand Rapids: William B. Eerdmans Publishing
Co., 1970,

Robinson, John A. T. Redating the New Testament. Philadelphia: The
Westminster Press, 1976.

Rushdoony, Rousas John. The Biblical Philosophy of History. Nutley,
NIJ: The Craig Press, 1969.

. The Foundations of Social Order: Studies in the Creeds
and Councils of the Early Church. Tyler, TX: Thoburn Press,
[1968] 1978.

. The Institutes of Biblical Law. Nutley, NJ: The Craig
Press, 1973.

—______.. The Mythology of Science. Nutley, NJ: The Presbyterian
and Reformed Publishing Ca., 1967.

. The One and the Many: Studies in the Philosophy of
Order and Ultimacy. Tyler, TX: Thoburn Press, [1971] 1978.

—___.. Salvation and Godly Rule. Vallecito, CA: Ross House
Books, 1983.

675
