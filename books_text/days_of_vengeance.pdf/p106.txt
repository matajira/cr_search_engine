1:11-15 PART ONE: THE SON OF MAN

prophecy (22:8) he tells us: “I, John, am the one who Aeard and
saw these things. And when | heard and saw. . . .” This pattern
is not always followed in the book, but it happens often enough
that we should be aware of St. John’s use of it— for it is occa-
sionally important in understanding how to interpret the sym-
bols (ef. 5:5-6): The verbal revelation is necessary in order to un-
derstand the visual revelation.

St. John suddenly finds himself in the Holy Place, for he
sees seven golden lampstands; and in the middle of the seven
lampstands one like a Son of Man. The imagery here is clearly
taken from the Tabernacle, but with a significant difference: in
the earthly Holy Place, there was one lampstand, with seven
lamps; here, St. John sees seven lampstands, connected to each
other in the Person who stands in their midst. The symbolism
involved here will be discussed under verse 20; the important
thing to note at present is simply the picture conveyed by this
imagery: Jesus Christ is the one Lampstand, uniting the seven
lamps — each of which turns out to be itself a lampstand; Christ
is surrounded by light. As St. Germanus, the eighth-century
Archbishop of Constantinople, put it at the outset of his work
on the Liturgy: “The Church is an earthly heaven in which the
super-celestial God dwells and walks about.”?8

The description of Christ in verses 13-16 involves a blend of
Old Testament images: the Glory-Cloud, the Angel of the Lord,
the Ancient of Days, and the Son of Man. Our understanding
will be heightened if we read this description in conjunction
with the following passages from Daniel:

T kept looking

Until thrones were set up,

And the Ancient of Days took His seat;

His vesture was like white snow,

And the hair of His head like pure wool.

His throne was ablaze with flames,

Its wheels were a burning fire.

A river of fire was flowing

And coming out from before Him;

Thousands upon thousands were attending Him,

28. St. Germanus of Constantinople, On che Divine Liturgy, Paul Meyen-
dorff, trans. (Crestwood, NY: St. Vladimir’s Seminary Press, 1984), p. 57.

72
