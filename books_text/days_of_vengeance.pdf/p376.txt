13:15-17 PART FOUR: THE SEVEN TRUMPETS

great, and the rich and the poor, and the free men and the
slaves, to be given a mark on their right hand, or on their fore-
head, and he provides that no one should be able 19 buy or to
sell, except the one who has the mark, either the name of the
Beast or the number of his name. The Book of Acts is studded
with incidents of organized Jewish persecution of the Church
(Acts 4:1-3, 15-18; 5:17-18, 27-33, 40; 6:8-15; 7:51-60; 9:23, 29;
13:45-50; 14:2-5; 17:5-8, 13; 18:17; 20:3; 22:22-23; 23:12, 20-21;
24:27; 26:21; 28:17-29; cf. 1 Thess. 2:14-16). All of this ultimately
served the interests of Caesar against Christ and the Church;
and the “mark of the Beast,” of course, is the Satanic parody of
the “seal of God” on the foreheads and hands of the righteous
(3:12; 7:2-4; 14:1), the mark of wholehearted obedience to the
Law in thought and deed (Deut. 6:6-8), the mark of blessing and
protection (Ezek. 9:4-6), the sign that one is HOLY TO THE
LORD (cf. Ex. 28:36). Israel has rejected Christ, and is
“marked” with the seal of Rome’s total lordship; she has given
her allegiance to Caesar, and is obedient to his rule and law.
Israel chose to be saved by the pagan state, and persecuted those
who sought salvation in Christ.

The New Testament gives abunaant testimony of this fact.
The Jewish hierarchy was involved in a massive, organized at-
tempt to destroy the Church by both deceit and persecution. In
pursuit of this diabolical goal, they united in a conspiracy with
the Roman government against Christianity. Some of them were
able to perform miracles in the service of Satan. All this is ex-
actly what is told us of the Beast from the Land, The False
Prophet of Revelation represents none other than the leadership
of apostate Israel, who rejected Christ and worshiped the Beast.

There is an interesting reversal of imagery in the text. The
Book of Job has prepared us for St. John’s prophecy, for it too
tells us of a Land Beast (Behemoth, Job 40:15-24) and a Sea
Beast (Leviathan, Job 41:1-34). In the Greek Old Testament
which the early Church used, the Hebrew word Behemoth is
translated Thérion, the same word St. John uses for Beast; and
Leviathan is translated Drakdn (Dragon). But St. John’s visions
expand on Job’s descriptions of these dinosaurs, and the order
of their appearance is reversed. Job first saw the Behemoth (Job
40), then Leviathan (Job 41), and finally God (Job 42). In Reve-
lation, St. John shows us the demonic reverse of this pattern:

342
