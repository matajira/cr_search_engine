APPENDIX C

ings do not necessarily include universal regeneration. The blessings
only require the extension of Christian culture. For the long-term
progress of culture, of course, this increase of common grace (or re-
duction of the common curse) must be reinforced (rejuvenated and re-
newed) by special grace—conversions. But the blessings can remain
for a generation or more after special grace has been removed, and as
far as the external benefits can be measured, it will not be possible to
tell whether the blessings are part of the positive feedback program
(Deut. 8:18) or a prelude to God’s judgment (Deut. 8:19-20). God
respects His conditional, external covenants. External conformity to
His law gains external blessings. These, in the last analysis (and at the
last judgment), produce coals for unregenerate heads.

Universal Regeneration?

The postmillennial system requires a doctrine of common grace and
common curse. It does not require a doctrine of universal regeneration
during the period of millennial blessings. In fact, no postmillennial
Calvinist can afford to be without a doctrine of common grace—one
which links external blessings to the fulfillment of external covenants.
There has to be a period of external blessings during the final genera-
tion. Something must hold that culture together so that Satan can
once again go forth and deceive the nations. The Calvinist denies that
men can “lose their salvation,” meaning their regenerate status, The
rebels are not “formerly regenerate” men. But they are men with
power, or at least the trappings of power. They are powerful enough to
delude themselves that they can destroy the people of God. And
power, as I have tried to emphasize throughout this essay, is not the
product of antinomian or chaos-oriented philosophy. The very exist-
ence of a military chain of command demands a concept of law and
order. Satan commands an army on that final day.

The postmillennial vision of the future paints a picture of historic-
ally incomparable blessings. It also tells of a final rebellion that leads
to God’s total and final judgment. Like the long-lived men in the days
of Methuselah, judgment comes upon them in the midst of power,
prosperity, and external blessings. God has been gracious to them all
to the utmost of His common grace. He has been gracious in response
to their covenantal faithfulness to His civil law-order, and He has been

 

kingdom of the Khazars, see Arthur Koestler, The Thirteenth Tribe: The
Khazar Empire and lis Heritage (New York: Random House, 1976).

If the Isracl referred to in Romans 11 is primarily genetic, then it may not
be necessary that all Jews be converted. What, then, is the Jew in Romans 11?
Covenantal? I wrote to Murray in the late 1960s to get his opinion on the impli-
cations of the Khazars for his exegesis of Romans 11, but he did not respond.

650
