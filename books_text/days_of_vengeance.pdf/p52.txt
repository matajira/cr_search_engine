INTRODUCTION

Lawsuit. The intersection of a fourfold and fivefold curse is
related to another dimension of Biblical imagery, relating to the
laws of multiple restitution. Exodus 22:1 commands: “If a man
steals an ox or a sheep, and slaughters it or sells it, he shall pay
five oxen for the ox and four sheep for the sheep.” James B. Jor-
dan explains the symbolic aspects of this case law: “These are
the animals which particularly symbolize humanity in the sacri-
ficial system. They are, thus, repeatedly set forth as preeminent
analogies for men (cf. ¢.g., Lev, 22:27, with Lev, 12).

“We should note here that the verb used in Exodus 22:1,
‘slaughter,’ is used almost always with reference to men. Ralph
H. Alexander comments, ‘The central meaning of the root oc-
curs only three times (Gen. 43:16; Ex. 22:1; 1 Sam. 25:11). The
root is predominantly used metaphorically, portraying the
Lord’s judgment upon Israel and upon Babylon as a slaughter.’34
This again points to a basic symbolic meaning of this law.”29

Jordan goes on to show that in Scripture the ox primarily
represents the office-bearer in Israel, while the sheep represents
the ordinary citizen, and especially the poor man. Fourfold res-
titution is thus required for the crime of oppressing the poor,
and fivefold restitution is required for the penalty of rebellion
against authority.“° The Covenant Lawsuit is structured in terms
of the penalty of fivefold restitution, since the rebels against the
covenant are revolting against their divinely ordained authority;
and St. John brings the lawsuit against Israel because she has re-
belled against Jesus Christ, her Lord and High Priest (Heb.
2:17; 7:22-8:6).

But Christ was also a sheep, the sacrificial Lamb of God
(John 1:29; Rev. 5:6, 9). He was wrongfully sold (Matt. 26:14-15),
and was treated “like a lamb that is led to slaughter” (Isa. 53:7).
Moreover, the early Christians were largely poor, and were per-
secuted, oppressed, and slaughtered by the wealthy and power-
ful of apostate Israel (Matt. 5:10-12; Luke 6:20-26; James 5:1-6).
Unbelieving Israel thus brought upon herself all the penalties
and curses of the covenant, including fourfold and fivefold as

38. R. Laird Harris, Gleason Archer, and Bruce Waltke, eds., Theological
Wordbook of the Old Testament (Chicago: Moody Press, 1980), p. 341.

39. James B. Jordan, The Law of the Covenant: An Exposition of Exodus
21-23 (Tyler, TX: Institute for Christian Economics, 1984), p. 266.

40. Ibid., pp. 266-71.

18
