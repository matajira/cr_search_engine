Part One

PREAMBLE: THE SON OF MAN
(Revelation 1)

Introduction
The Preamble in Deuteronomy {1:1-5) begins: “These are the
words. .. .”! The text then identifies the speaker as Moses, who

as mediator of the Covenant has been “commanded” to give and
expound God’s “law” to Israel. “Yahweh is, therefore, the Suze-
tain who gives the covenant and Moses is his vicegerent and the
covenant mediator. This section thus corresponds to the pream-
ble of the extra-biblical treaties, which also identified the
speaker, the one who by the covenant was declaring his lordship
and claiming the vassal’s obedience.”? The Preamble in Revela-
tion begins with a similar expression: “The Revelation of Jesus
Christ, which God gave Him to show to His servants, the things
that must shortly take place; and He sent and signified it by His
angel to His servant John, who bore witness to the Word of God
and to the Testimony of Jesus Christ, even to all that he saw”
(1:1-2),

The purpose of the covenantal Preamble is thus to proclaim
the lordship of the Great King, declaring his transcendence and
immanence and making it clear from the outset that his will is to
be obeyed by the vassals, his servants. Biblical treaties set forth
God’s transcendence and immanence by referring to one or
more of three activities: creation, redemption, and revelation. It
is the latter two that are especially emphasized in Revelation’s

 

1. The Hebrew title of Deuteronomy is simply: The Words.
2. Meredith G. Kline, Treety of rhe Great King (Grand Rapids: William B.
Eerdmans Publishing Co., 1963), p. 30.

49
