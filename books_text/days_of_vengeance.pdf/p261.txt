INTRODUCTION TG PART FOUR

Jer. 4:13; Hab. 1:8). Like the opening of Hosea’s Sanctions/
Covenant Ratification section (Hos. 8:1), the Eagle in Revela-
tion is connected with the blowing of Trumpets signalling disas-
ter; yet the Eagle brings salvation as well to the faithful of the
covenant (cf. Rev. 12:14).

As in Deuteronomy, this section of Revelation shows us two
mountains: the Mount of Cursing in Chapter 8, which is ignited
with coals from the altar and thrown into the Abyss; and the
Mount of Blessing in Chapter 14, Mount Zion, where the Lamb
meets with His army of 144,000, the Remnant from the Land of
Israel. Deuteronomy 30:1-10 promises an ultimate restoration of
the people, when God would truly circumcise their hearts, and
when He would again abundantly bless them in every area of
life. Kline comments: “As the development of this theme in the
prophets shows, the renewal and restoration which Moses fore-
tells is that accomplished by Christ in the New Covenant. The
prophecy is not narrowly concerned with ethnic Jews but with
the covenant community, here concretely denoted in its Old Tes-
tament identity as Israel. Within the sphere of the New Cove-
nant, however, the wall of ethnic distinctions disappears. Ac-
cordingly, the Old Testament figure used here of exiled Israelites
being regathered to Yahweh in Jerusalem (v. 3b, 4; cf. 28:64)
finds its chief fulfillment in the universal New Testament gather-
ing of sinners out of the human race, exiled from Paradise, back
to the Lord Christ enthroned in the heavenly Jerusalem.”?

Thus, the central image of this section of Revelation is a Cov-
enant ratification ceremony (Chapter 10), in which the Angel of
the Covenant stands on the Sea and on the Land, lifting His
tight hand to heaven, swearing an oath and proclaiming the
coming of the New Covenant, the inauguration of a new admin-
istration of the world under “the Lord and His Christ; and He
will reign forever and ever” (Rev, 11:15).

2. Kline, pp. 132f.
227
