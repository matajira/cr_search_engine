CHRISTIAN ZIONISM AND MESSIANIC JUDAISM

liturgical forms of the Bible. Those forms were preserved in the
church, and in her alone. Jews who wish to recover their heritage
would do well to study the early Church, not the traditions of Eastern
European cultures.

621
