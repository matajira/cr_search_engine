KING OF KINGS 1:4-6

above all. He is not “only” the Savior, waiting for a future cata-
clysmic event before He can become King; He is the universal
King now, in this age — sitting at His Father’s right hand while all
His enemies are being put under His feet. This process of taking
dominion over all the earth in terms of His rightful title is going
on at this moment, and has been ever since He rose from the
dead. As Firstborn (and only-begotten!), Christ possesses the
crown rights of all creation: “4// authority in heaven and earth
has been given to Me,” He claimed (Matt. 28:18). All nations
have been granted to Him as His inheritance, and the kings of
carth are under court order to submit to Him (Ps. 2:8-12). Com-
menting on Christ’s title Ruler of the kings of the earth, William
Symington wrote: “The persons who are here supposed to be
subject to Christ, are kings, civil rulers, supreme and subordi-
nate, all in civil authority, whether in the legislative, judicial, or
executive branches of government. Of such Jesus Christ is
Prince; — 6 a&pyoov, ruler, lord, chief, the first in power, authority,
and dominion.”!?

This, in fact, is precisely the reason for the persecution of
Christians by the State. Jesus Christ by the Gospel has asserted
His absolute sovereignty and dominion over the rulers and na-
tions of earth, They have a choice: Either submit to His govern-
ment and law, accepting His non-negotiable terms of surrender
and peace, or be smashed to bits by the rod of His anger. Such
an audacious, uncompromising position is an affront to the dig-
nity of any self-respecting humanist—much more so to rulers
who are accustomed to thinking of themselves as gods walking
on earth. Perhaps this Christ can be allowed a place in the pan-
theon, along with the rest of us gods; but for His followers to
proclaim Him as Lord of all, whose law is binding upon all men,
whose statutes call into judgment the legislation and decrees of
the nations —this is too much; it is inexcusable, and cannot be
allowed.

It would have been much easier on the early Christians, of
course, if they had preached the popular retreatist doctrine that

19. William Symington, Messiah the Prince: or, The Mediarorial Dominion
of Jesus Christ (Philadelphia: The Christian Statesman Publishing Co., [1839]
1884), p. 208,

63
