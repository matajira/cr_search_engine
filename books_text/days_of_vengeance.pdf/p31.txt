PUBLISHER’S PREFACE

from what they regarded as modern civilization’s more unsavory
features, things such as liquor, cigarettes, movies, and social
dancing. (I have often said that if anti-abortionists were to
spread the rumor that the local abortionist gives a glass of beer
to each woman to calm her nerves after an abortion, half the
fundamentalists in town would be on the picket lines in front of
his office within a week.}

Amillennialism

Protestant amillennialists, who are primarily members of
Dutch or Lutheran churches, or churches influenced by Con-
tinental European theology, have a far stronger academic tradi-
tion behind them. It stretches back to Augustine. Chilton draws
from these amillennial traditions in explaining Biblical imagery.
Nevertheless, Chilton has demonstrated that this imagery can be
understood far better within a framework of historical Christian
progress than within a framework that presumes increasing his-
torical defeat at the hands of covenant-breakers.

The fundamental! message of Biblical eschatology is victory,
in time and on earth Gin history)— comprehensive victory, not
simply a psychologically internal, “smile on our faces, joy in our
hearts” sort of victory. In short, he makes effective use of their
scholarly contributions, but he does not thereby become de-
pendent on their underlying eschatological presuppositions.
(Again, I have in mind a previously mentioned anonymous
theologian, whose response to all this is easily predictable: lots
more stony silence. Discretion is the better part of valor. He was
thoroughly rebutted by another Reconstructionist on a related
topic, so he is, understandably, a bit gun-shy.)

The fact is, amillennialist churches are not noted for their
evangelism programs. (Those that use the Coral Ridge Presby-
terian Church’s Evangelism Explosion materials are exceptions
to this rule of thumb.) These churches are not out in the theo-
logical arena, challenging humanists or anyone else. Members
see their churches as holding actions, as defensive fortresses, or
as ports in the cultural storm. These churches are simply not on
the offensive. They do not expect to achieve anything culturally.
They also do not expect to see a wave of converts. They proba-

XXxi
