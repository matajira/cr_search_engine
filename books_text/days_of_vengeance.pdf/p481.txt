BABYLON IS FALLEN! 18:2

who lived in the wilderness.? Isaiah had prophesied about the
desolation of Babylon:

Desert creatures will lie down there,

And their houses will be full of owls,
Ostriches also will live there,

And goat-demons will frolic there. (Isa. 13:21)

God’s wrath against Edom was phrased in much the same
language:

Tt shall not be quenched night or day;

Its smoke shall go up forever;

From generation to generation it shall be desolate;
None shall pass through it forever and ever.

But pelican and hedgehog shall possess it,

And owl and raven shall dwell in it;

And He shall stretch over it the measuring line of desolation
And the plumb line of void. . .

And thorns shall come up on its fortified towers,
Nettles and thistles in its fortified cities;

It shall also be a haunt of jackals

And an abode of ostriches.

And the desert creatures shall meet with the wolves,
The goat-demon also shall cry to its kind;

Yes, the night demon [Léith] shail settle there

And shall find herself a resting place. (Isa. 34:10-14)

Now the Angel’s decree applies the ancient curses to the re-
bellious Jews of the first century. Because Israel rejected Christ,
the entire nation becomes demon-possessed, utterly beyond
hope of reformation (cf. Matt. 12:38-45; Rev. 9:1-11). Under-
scoring the tragedy of this is John’s use of the term dwelling
place (katoiketérion), a word elsewhere used for the place of
God’s special Presence, in heaven, in the holy city, in the Tem-
ple, and in the Church; “the Place (katoikéterion) O Lorp,
which Thou hast made for Thy dwelling, the sanctuary, O
Lorp, which Thy hands have established” (Ex. 15:17; cf. 1 Kings

2. This was not to be interpreted as a sacrifice to the demon himself (Lev.
17:7). Centuries tater, the apostate Northern Israelites under Jeroboam did in
fact offer warship to this goat-demon (2 Chron. 11:15).

447
