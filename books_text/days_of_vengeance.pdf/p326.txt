11:19 PART FOUR: THE SEVEN TRUMPETS

itively in His Ascension, progressively each Lord’s Day in wor-
ship). In the Old Covenant, the Ten Commandments were “hid-
den” in the Sanctuary, and no one was allowed in (although
God’s revelation was published provisionally by Moses). But
now, in the New Covenant, the Mystery has been openly pub-
lished, and man in Christ has access. With the sounding of the
Seventh Trumpet the revelation is complete and definitive; the
Mystery is no longer mysterious. St. Paul commended the saints
of Rome “to Him who is able to establish you according to my
Gospel and the preaching of Jesus Christ, according to the reve-
lation of the Mystery which has been kept secret for long ages
past, but now is manifested, and by the Scriptures of the proph-
ets, according to the commandment of the eternal God, has
been made known to all the nations, leading to obedience of
faith” (Rom. 16:25-26).

For this reason all the meteorological phenomena that had
been associated with the Cloud in the Old Covenant revelation
(cf. Ps. 18) are now spoken of by St. John in relation to the
Church: There were flashes of lightning and voices and peals of
thunder and an earthquake and a great hailstorm. In the Church
of Jesus Christ the door of heaven has opened up to us. Our
sanctification is by means of the Church, through its ministry
and sacraments, as St. Irenaeus wrote: “We receive our faith
from the Church and keep it safe; and it is as it were a precious
deposit stored in a fine vessel, ever renewing its vitality through
the Spirit of God, and causing the renewal of the vessel in which
it is stored. For this gift of God has been entrusted to the
Church, as the breath of life to created man, to the end that all
members by receiving it should be made alive. And herein has
been bestowed upon us our means of communion with Christ,
namely the Holy Spirit, the pledge of immortality, the strength-
ening of our faith, the ladder by which we ascend to God. For
the Apostle says, ‘God has set up in the Church Apostles,
prophets, teachers’ [1 Cor. 12:28] and all the other means of the
Spirit's working. But they have no share in this Spirit who do
not join in the activity of the Church. ... For where the
Church is, there is the Spirit of God; and where the Spirit of
God is, there is the Church and every kind of grace. The Spirit is
truth. Therefore those who have no share in the Spirit are not
nourished and given life at their mother’s breast; nor do they en-

292
