THE MILLENNIUM AND THE JUDGMENT 20:2-3

pecks of meal, until it was all leavened” (Matt. 13:33)—Gary
North observes: “The kingdom of God is like leaven. Christian-
ity is the yeast, and it has a leavening effect on pagan, satanic
cultures around it. It permeates the whole of culture, causing it
to rise. The bread which is produced by this leaven is the pre-
ferred bread, In ancient times— indeed, right up until the advent
of late-nineteenth century industrialism and modern agricul-
tural methods —leavened bread was considered the staff of life,
the symbol of God’s sustaining hand. ‘Give us this day our daily
bread,’ Christians have prayed for centuries, and they have
eaten leavened bread at their tables. So did the ancient
Hebrews. The kingdom of God is the force that produces the
fine quality bread which all men seek. The symbolism should be
obvious: Christianity makes life a jay for godly men. It provides
men with the very best.

“Leaven takes time to produce its product. It takes time for
the leaven-laden dough to rise. Leaven is a symbol of historical
continuity, just as unleavened bread was Israel's symbol of his-
torical discontinuity. Men can wait for the yeast to do its work.
God gives man time for the working of His spiritual leaven.
Men may not understand exactly how the leaven works—how
the spiritual power of God’s kingdom spreads throughout their
culture and makes it rise— but they can see and taste its effects.
IF we really push the analogy (pound it, even), we can point to
the fact that dough is pounded down several times by the baker
before the final baking, almost as God, through the agents of
Satan in the world, pounds His kingdom in history. Neverthe-
less, the yeast does its marvelous work, just so lang as the fires
of the oven are not lit prematurely. If the full heat of the oven is
applied to the dough before the yeast has done its work, both
the yeast and the dough perish in the flames, Ged waits to apply
the final heat (2 Pet. 3:9-10). First, His yeast—His church—
must do its work, in time and on earth. The kingdom of God
(which includes the institutional church, but is broader than the
institutional church) must rise, having ‘incorrupted’ the satanic
dough of the kingdom of Satan with the gospel of life, including
the life-giving reconstruction of all the institutions of culture.

“What a marvelous description of God’s kingdom! Chris-
tians work inside the cultural material available in any given cul-
ture, seeking to refine it, permeate it, and make it into some-

505
