14:9-11 PART FOUR: THE SEVEN TRUMPETS

Prophet who speaks like the Dragon), Babylon’s primary occu-
pation is seducing others into fornication, the worship of false
gods.

9-11 And another angel, a third oue, followed them, with an
appropriate message of doom for anyone who worships the
Beast and his image, or receives a mark in his forehead or upon
his hand (see above, on 13:15-18). The great offense of the Land
Beast — apostate Israel’s religious leadership —was the promo-
tion and enforcement of the worship of the Beast (13:11-17). St.
John is thus giving a clue to the great city’s identity by repeating
his words about the Land Beast immediately after his first state-
ment about “Babylon.” He is also reminding the Christians, es-
pecially the “angels,” the Church officers, of their duty in pro-
claiming the whole counsel of God. They must preach the un-
compromising message of the exclusive, all-encompassing lord-
ship of Jesus Christ against all pretenders to the Throne. They
must speak prophetically to their generation, sternly condemn-
ing the worship of the Beast, warning that those who drink of
Babylon’s heretical cup of State-worship also will drink of the
wine of the wrath of God, which is mixed in full strength —liter-
ally, mixed unmixed (or, as one commentator delightfully trans-
lates it, mixed neat'2)—in the cap of His anger. The warning is
clear: You cannot drink one cup without the other,

Moses Stuart explains the imagery: “God is often said to give
the cup of inflammation or indignation to nations whom He is
about to destroy (e.g. Isa. 51:17; Lam. 4:21; Jer. 25:15-16; 49:12;
51:7; Ezek. 23:31-34; Job 21:20; Ps, 75:8). Persons intoxicated
are unable to destroy or even resist those that assail them; so
that to represent them as intoxicated in the way of punishment
is to represent them as devoted to irremedial destruction. Or we
may present the matter in another light. Criminals about to
suffer were often through compassion of executioners or by-
standers presented with a stupefying potion which would dimin-
ish their sensibility to pain, but which of course was the index or
precursor of certain death. Thus in Mark 15:23 it is recorded

12, Carrington, pp. 248f. With the British sense of propriety, Carrington
admits to a certain degree of trepidation in this rendering.

364
