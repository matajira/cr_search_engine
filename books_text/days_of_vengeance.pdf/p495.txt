BABYLON IS FALLEN! 18:21

So they passed through the midst of the sea on dry ground;
And their pursuers Thou didst hurl into the depths,
Like a stone into raging waters. (Neh. 9:9-11)

The symbol is also based on the prophetic drama performed
by Seraiah, Jeremiah’s messenger of judgment (Jer. 51:61-64).
After reading the prophecy of Babylon’s “perpetual desolation,”
he tied the scroll to a stone and threw it into the Euphrates, declar-
ing: “Just so shall Babylon sink down and not rise again. . . .”
Applying Seraiah’s words to the Harlot, the angel says: Thus
will Babylon, the great city, be thrown down with violence, and
will not be found any longer. How was this fulfilled in a.p. 70, if
“Jerusalem” is still standing in the twentieth century? In a physi-
cal sense, of course, Jerusalem was not destroyed forever in A.D.
70, any more than Babylon or Edom or Egypt was destroyed
“forever,” But prophecy is covenantally and ethically oriented; it
is not primarily concerned with geography as such. For exam-
ple, consider Isaiah’s prophecy against Edom:

Its streams shall be turned into pitch,

And its loose earth into brimstone,

And its land shall become burning pitch.

It shail not be quenched night or day;

Its smoke shall go up forever;

From generation to generation it shall be desolate;

None shall pass through it forever and ever. (Isa. 34:9-10)

This is evocative language, associating the desolation of
Edom with the destruction of Sodom and Gomorrah. In a
“literal,” physical sense, the prophecy was not fulfilled; but it
has been fulfilled, in terms of its actual meaning and intent. The
ancient territory of Edom still contains trees and flowers, por-
tions of it are used as cropland, and travelers continue to pass
through it. As Patrick Fairbairn observed, “Edom was to be
stricken with poverty and ruin: Edom, however, not simply, nor
chiefly as a land, but as a people. This was what the prophecy
foretold, and it has been amply verified... .The Edom of
prophecy — Edom considered as the enemy of God, and the rival
of Israel—has perished forever; all, in that respect, is an untrod-
den wilderness, a hopeless ruin; and there, the veracity of God’s

461
