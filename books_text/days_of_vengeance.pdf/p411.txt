THE KING ON MOUNT ZION 14:19-20

tive bloodshedding “outside the City.” In the Old Testament sac-
Tificial system, “the bodies of those animals whose blood is
brought into the holy place by the high priest as an offering for
sin, are burned outside the camp. Therefore Jesus also, that He
might sanctify the people through His own blood, suffered out-
side the gate. Hence, let us go to Him outside the camp, bearing
His reproach. For here we do not have a lasting City, but we are
seeking the City which is to come” (Heb. 13:11-14), Outside the
City, therefore, was the place of judgment, where the bodies of
sacrificed animals were disposed of; and it was the Place of
Judgment, where Christ’s blood was shed by rebellious Israel. In
this layered imagery, then, the blood flowing outside the City
belongs to Christ, sacrificed outside the camp; and it is to be the
blood of apostate Israel as well, cast out and excommunicated
from “the Jerusalem above” and disinherited by the Father.
Here is the doctrine of Limited Atonement, and with a ven-
geance: Blood will flow—if the blood is not Christ's, shed on
our behalf, it will be ours! “In a.p, 70 the Vine of Israel is cut
down and trampled in the Winepress; but this destruction is the
culmination of a process which has lasted over forty years; it
began Outside the City, when one whom they despised and re-
jected trod the Winepress alone, and of the people there was
none with Him. It was in that moment that Jerusalem fell.”

22. Carrington, p. 261,
377
