4

THE THRONE ABOVE THE SEA

The Pattern for Worship (4:1-1D

1 After these things I looked, and behold, a door standing
open in heaven, and the first Voice which I had heard, like
the sound of a trumpet speaking with me, said: Come up
here, and I will show you what must take place after these
things.

2 Immediately I was in the Spirit; and behold, a Throne was
standing in heaven, and One sitting

3 like a jasper stone and a sardius in appearance; and there
was a rainbow around the Throne, like an emerald in ap-
pearance.

4 And around the Throne were twenty-four thrones; and upon
the thrones I saw twenty-four elders sitting, clothed in white
garments, and golden crowns on their heads.

5 And from the Throne proceed Alashes of lightning and voices
and peals of thunder. And there were seven lamps of fire burn-
ing before His Throne, which are the seven Spirits of God;

6 and before the Throne there was, as it were, a sea of glass
like crystal; and in the middle of the Throne and around it
were four living creatures full of eyes in front and behind.

7 And the first creature was like a Lion, and the second crea-
ture was like a Bull, and the third creature had a face like that
of a Man, and the fourth creature was like a flying Eagle.

8 And the four living creatures, each one of them having six
wings, are full of eyes around and within; and they have no
rest day and night, saying:

Holy, holy, holy, is the Lord God, the Almighty, who
was and who is and who is to come.

9 And when the living creatures give glory and honor and
thanks to Him who sits on the Throne, to Him who lives for-
ever and ever,

145
