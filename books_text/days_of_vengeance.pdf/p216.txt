PART THREE: THE SEVEN SEALS

Little Apocalypse recorded in the Synoptic Gospels. As his out-
line (adapted below) demonstrates, “they present practically the
same material.”*

Revelation 6

+ War (y, 1-2)

. International strife (v. 3-4)

. Famine (v. 5-6)

. Pestilence (v. 7-8)

. Persecution (v, 9-11)

. Earthquake; De-creation (v. 12-17)

Matthew 24

« Wars (v. 6)

. International strife (v. 7a)
. Famines (v. 7b)

. Earthquakes (v. 7c)

. Persecutions (v, 9-13)

. De-creation (v, 15-31)

Mark 13

. Wars (v. 7)

. International strife (v. 8a)
. Earthquakes (y. 8b)

. Famines (¥. 8c)

. Persecutions (v. 9-13)

. De-creation (v. 14-27)

uke 21

. Wars (v. 9)

. International strife (v. 10)

. Earthquakes (v. 11a)

. Plagues and famines (v. 11b)
. Persecution (v, 12-19)

. De-creation (vy, 20-27)

Aukwenypre

AuRwNe

DAUPWNEN AUSWNS

This is very perceptive of Charles, and of the many commen-
tators who have followed his lead. What is astonishing is that
they should fail to see St. John’s purpose in presenting “the
same materjal” as the Synoptic writers: to prophesy the events
leading up to the destruction of Jerusalem, While all readily ad-

1. R. H. Charles, A Critical and Exegetical Commentary on the Revelation
of St. John, 2 vols. (Edinburgh: T. & T. Clark, 1920), Vol. 1, p. 158.

182
