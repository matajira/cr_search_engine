71-3 PART THREE: THE SEVEN SEALS

And their pregnant women will be ripped open.
(Hos. 13:15-16)

As we have seen,? the association of angels with “nature” is
not “mere” imagery. God through His angels really does control
weather patterns, and He uses weather as an agency of blessing
and judgment. From the very first verse, the Bible is written in
terms of what Gary North calls cosmic personalism: “God did
not create a self-sustaining universe which is now left to operate
in terms of autonomous laws of nature. The universe is not a giant
mechanism, like a clock, which God wound up at the beginning
of time. Ours is not a mechanistic world, nor is it an autono-
mous biological entity, growing according to some genetic code
of the cosmos. Ours is a world which is actively sustained by
God on a full-time basis (Job 38-41), All creation is inescapably
personal and theocentric. ‘For the invisible things of him from
the creation of the world are clearly seen, being understood by the
things that are made, even his eternal power and Godhead...’
(Rom. 1:20).

“If the universe is inescapably personal, then there can be no
phenomenon or event in the creation which is independent from
God. No phenomenon can be said to exist apart from God's all-
inclusive plan for the ages. There is no uninterpreted ‘brute fac-
tuality.’ Nothing in the universe is autonomous. . . . Nothing in
the creation generates its own conditions of existence, including
the law structure under which something operates or is operated
upon. Every fact in the universe, from beginning to end, is ex-
haustively interpreted by God in terms of His being, plan, and
power."4

The four angels are restraining the judgment in obedience to
the command of another angel, whom St. John sees ascending
from the rising of the sun, whence God’s actions in history tradi-
tionally came (cf, Isa. 41:1-4, 25; 46:11; Ezek, 43:1-3). This angel
comes as the representative of Christ, the Sunrise from on high

3. See comments on 4:5-8, above.

4. The Dominion Covenant: Genesis (Tyler, TX: Institute for Christian
Economics, 1982), pp. 1-2; cf. pp. 2-11, 425-54; see also Rousas John Rush-
doony, The Mythology of Science (Nutley, NJ: The Craig Press, 1967),

204
