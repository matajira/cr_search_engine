SEVEN LAST PLAGUES 15:8

in the destruction of Old Covenant Israel.

E. W. Hengstenberg mentions a related aspect of this sym-
bol: “So long as Israel was the people of the Lord the pillar of
cloud exclaimed to all his enemies, ‘Touch not Mine anointed,
and do My prophets no harm.’ So here; that the temple is full of
smoke, and no one is able to go into it, this is ‘a sign for believ-
ers, that the Lord in love to them was now going to complete the
destruction of their enemies.’!? Besides, we see quite plainly in
Isaiah 6 the reason why none could enter in. If God manifests
Himself in the whole glory of His nature, in the whole energy of
His punitive righteousness, the creature must feel itself pene-
trated by a deep feeling of its nothingness — not merely the sinful
creature, as there in the case of Isaiah, but also the finite, ac-
cording to Job 4:18; 15:15... . Bengel!? remarks, “When God
pours out His fury, it is fit that even those who stand well with
Him should withdraw for a litile, and should restrain their in-
quiring looks. All should stand back in profound reverence, till
by and by the sky become clear again,’ ”!*

2. C.E J, Zillig, Die Offenbarung Johannis erklirt (Stuttgart, 1834-40).
13. J. A. Bengel, Erkidrte Offenburung Johannis (Stuttgart, 1740).
14, Hengstenberg, Vol. 2, p. 153.

393
