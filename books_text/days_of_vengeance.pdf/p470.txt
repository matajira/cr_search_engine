17:11 PART FIVE: THE SEVEN CHALICES

for its “seven hills”; 4 but these also correspond to the line of the
Caesars, for they are seven kings; five have fallen: The first five
Caesars were Julius, Augustus, Tiberius, Caligula, and Claud-
ius.' One is: Nero, the sixth Caesar, was on the throne as St.
John was writing the Revelation. The other has not yet come;
and when he comes, he must remain a little while: Galba, the
seventh Caesar, reigned for less than seven months.

11 But the fall of the Julio-Claudian dynasty and the severe
politica! chaos atiending it must not be interpreted by Christians
to mean the end of troubles. For their real enemy is the Beast,
who will become incarnated in other Caesars as well. He is also
an eighth king, yet is of the seven: the antichristian brutality of
succeeding tyrants will mark them as being of the same stripe as
their predecessors, Fight is the number of resurrection in the
Bible; St. John is warning that even though the Empire will
seem to disintegrate after the rule of the seven kings, it will be
“resurrected” again, to live on in other persecutors of the
Church. Yet the Empire’s comeback will not result in victory for
the Beast, for even the eighth, the resurrected Beast, goes to de-

14, It is not at all necessary, with Russell (Tke Parousia, p. 492), to seek
seven mountains in Jerusalem as the fulfillment of this statement. The Harlot
is seated on the Beast, and thus on the seven hills of Rome; in other words,
apostate Judaism, centered in the City of Jerusalem, is supported by the
Roman Empire.

15. This has been called into question by some, since, in a technical sense,
the Empire began with Augustus, not Julius (cf, Tacitus, Tre Annals, i.l). Yet
that was a technicality which, as far as the normal conversation and writing of
the first century were concerned, was irrelevant. For all practical purposes,
Julius Caesar was Emperor: He claimed the title imperafor, and most early
Roman, Christian, and Jewish writers count him as the first Emperor.
Suetonius begins his Lives of the Twetve Caesars with Julius as the first Em-
peror, as does Dio Cassius in his Roman History. Book § of the Sibyline
Oracies calls Julius “the first king,” and 4 Ezra 12:15 speaks of Augustus as “the
second” of the emperors. For our purposes, Josephus seems to provide the
most convincing testimony, since he wrote for both a Roman and a Jewish au-
dience, in the common parlance of the day. In his Antiquities of the Jews he
clearly speaks of Augustus and Tiberius as the second and third emperors
Ov 2), of Caligula as the fourth (xviii.-vi.l0), and of Julius as the first
(xix.i.1D. The most extensive discussion of all the evidence is in Moses Stuart,
Commentary on the Apocalypse, two vols. (Andover: Allen, Morrill, and
Wardwell, 1845), Vol. 2, pp. 445-52; cf. Isbon T. Beckwith, The Apocalypse of
John: Studies in Introduction with an Exegetical end Critical Commentary
(Grand Rapids: Baker Book House, [1919] 1979), pp. 704f.

436

 
