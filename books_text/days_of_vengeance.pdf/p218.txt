PART THREE: THE SEVEN SEALS.

represents wide-sweeping, national judgments in a fourfold
manner. The Horsemen, therefore, show us God’s means of
controlling and bringing judgment upon the disobedient nation
of Israel.

Milton Terry’s comments are helpful: “The true interpreta-
tion of these first four seals is that which recognizes them as a
symbolic representation of the ‘wars, famines, pestilences, and
earthquakes’ which Jesus declared would be ‘the beginning of
sorrows’ in the desolation of Jerusalem (Matt. 24:6-7; Luke
21:10-11, 20). The attempt to identify each separate figure with
one specific event misses both the spirit and method of apoca-
lyptic symbolism. The aim is to give a fourfold and most im-
pressive picture of that terrible war on Jerusalem which was des-
tined to avenge the righteous blood of prophets and apostles
(Matt. 23:35-37), and to involve a ‘great tribulation,’ the like of
which had never been before (Matt. 24:21). Like the four succes-
sive but closely connected swarms of locusts in Joel 1:4; like the
four riders on different colored horses in Zechariah 1:8, 18, and
the four chariots drawn by as many different colored horses in
Zechariah 6;1-8, these four sore judgments of Jehovah move
forth at the command of the four living creatures by the Throne
to execute the will of Him who declared the ‘scribes, Pharisees,
and hypocrites’ of His time to be ‘serpents and offspring of
vipers,’ and assured them that ‘ali these things should come
upon this generation’ (Matt. 23:33, 36). The writings of
Josephus abundantly show how fearfully all these things were
fulfilled in the bloody war of Rome against Jerusalem.”?

Just as important as Zechariah in the background of this
passage is the Prayer of Habakkuk (Hab. 3), the traditional syn-
agogue reading for the second day of Pentecost,3 in which the
prophet relates a vision of God coming in judgment, shining like
the sun, flashing with lightning (Hab. 3:3-4; cf. Rev. 1:16; 4:5),
bringing pestilence and plague (Hab. 3:5; Rev. 6:8), shattering
the mountains and collapsing the hills (Hab. 3:6, 10; Rev. 6:14),
riding on horses against His enemies (Hab, 3:8, 15; Rev. 6:2,

2, Milton Terry, Biblical Apocatyptics: A Study of the Most Notabie
Revelations of God and of Christ in the Canonical Scriptures (New York:
Eaton and Mains, 1898), pp. 329f.

3. M. D. Goulder, The Evangelists’ Calendar: A Lectionary Explanation for
the Development of Scripture (London: SPCK, 1978), p. 177.

184
