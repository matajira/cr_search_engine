4:5-8 PART THREE: THE SEVEN SEALS

So when we are told (Ps. 18:10) that the Lord travels both on a
cherub and on the wings of the wind, we may begin to see a link
between the four living creatures of 4:6 and the four winds of
7:1, We might call these cherub-creatures ‘nature,’ so long as we
temember what nature really is—an immense construction
throbbing with the ceaseless activity of God. . . . Perhaps their
faces (4:7; Ezek. 1:10) represent his majesty, his strength, his
wisdom, and his loftiness, and their numberless eyes his ceaseless
watchfulness over every part of his creation. It is appropriate
then that there should be four of them, corresponding to the
points of the compass and the corners of the earth, and standing
for God’s world, as the twenty-four elders stand for the Church.”2°
While John Calvin would have agreed with Wilcock, his
remarks on the significance of the four faces of the cherubim are
even more radical: “By these heads all living creatures were rep-
resented to us... . These animals comprehend within them-
selves all parts of the universe by that figure of speech by which
a part represents the whole, Meanwhile since angels are living
creatures we must observe in what sense God attributes to angels
themselves the head of a lion, an eagle, and a man: for this
seems but little in accord with their nature, But he could not bet-
ter express the inseparable connection which exists in the
motion of angels and all creatures. . . . We are to understand,
therefore, that while men move about and discharge their
duties, they apply themselves in different directions to the object
of their pursuit, and so also do wild beasts; yet there are angelic
motions underneath, so that neither men nor animals move them-
selves, but their whole vigor depends on a secret inspiration.”24
As Calvin says a few pages later, with more force, “all crea-
tures are animated by angelic motion.”™ This goes directly

20. Michael Wilcock, 7 Saw Heaven Opened: The Message of Revelation
(Downers Grove, IL: InterVarsity Press, 1975), p. 64.

21. John Calvin, Commentaries an the First Twenty Chapters of the Book of
the Prophet Ezekiel (Grand Rapids: Baker Book House, 1979), Vol. 1, pp. 334f.

22. Tbid., p. 340; cf. pp. 65-74, 333-340. Calvin was attacked by his own
translator for making these and like statements (see Vol. 1, pp. xxvf.; Vol. 2,
Pp. 421f,, 448-55, 466-68, 4731.) Nevertheless, these thoughts are very care-
fully worked out in the course of his exposition, and this commentary, which
Calvin did not live to finish, represents his mature thought on the subject. It is
one of the most fascinating volumes I have ever read, and is a rich storehouse
of valuable insights.

156
