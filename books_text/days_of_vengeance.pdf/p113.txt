KING OF KINGS 1:19

verse 1) “the things that must shortly take place.” It appears that
the phrasing is intended to provide a parallel to the description
of the One “who was and who is and who is coming”: Thus “the
process of temporal history reflects the eternal nature of God.””

We might pause at this point to consider an error that is
common among those who adopt a preterist interpretation of
Revelation. The two facts of St. John’s symbolic style and his
clearly anti-statist content have led some to believe that the pol-
itically sensitive message determined the use of symbolism —that
St. John wrote the Revelation in a secret code in order to hide
his message from the imperial bureaucrats. This is the view of
James Kallas (who, incidentally, also holds that John wrote in
the time of the emperor Domitian, rather than Nero):

He writes in deliberately disguised language. He resorts to
imagery the Romans will not understand. He cannot write in a
literal and obvious way. He cannot say in clear and unambig-
uous terms what lies closest to his heart. What would happen if
he wrote what he believed, that Domitian was a blasphemous
son of the devil himself? What would happen if he cried out that
the Roman empire, in its demand that men bow down and wor-
ship Caesar, was a diabolical scheme of Satan himself designed
to win men away from Jesus? The letter would never be deliv-
ered. It would never clear the censors.

And thus he must camouflage and conceal his true meaning.
He must resort to non-literal symbolism, to obscure and appar-
ently meaningless references which his Roman censors would see
merely as the senile musings of a mad old man.**

There may be some truth to this, as a tangential slant on the
use of the number 666 in 13:18 in reference to Nero (not Domi-
tian)—a “code” that the Romans would be unable to decipher
correctly. But even without that reference, the Book of Revela-
tion is a clearly treasonous document, and any State bureaucrat
would have been able to figure that out. Consider what we have
seen already in St. John’s description of Jesus Christ: The mere
assertion that He is Ruler of the kings of the earth is an assault

37. Philip Carrington, The Meaning of the Revelation, p. 95.
38. James Kallas, Revelation: God and Satan in the Apocalypse (Min-
neapolis: Augsburg Publishing House, 1973), pp. S8f.

79
