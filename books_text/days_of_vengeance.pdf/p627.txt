Appendix A
THE LEVITICAL SYMBOLISM IN REVELATION

Par CaRRiNGTON

The liturgical character of sections in Revelation has often been
pointed out; but I have seen no attempt to study and elucidate the
liturgical scaffolding into which the visions are built. Archbishop Ben-
son came very near to it when he treated the book as a drama, and
printed it so as to display the choric structure. But Revelation is not a
drama; it is a liturgy. A drama deals with the unfolding of personality,
and the actors in it must use their own personalities to interpret it. In
liturgy the hierophants must submerge their personalities and identi-
ties in the movement of the whole composition. It is a real literary
triumph that a sustained poem like Revelation should grip the atten-
tion as it does without the assistance of human interest in character;
and that triumph is liturgical in character,

The author of the Revelation frequented the temple and loved its
liturgy; when he shut his eyes in Ephesus, he could see the priests
going about their appointed tasks at the great altar of burnt-offering.
That vision forms the background of the whole poem.

Iam astonished to find so few discussions on the temple ritual, not
only in connection with the Revelation, but also in connection with the
Palestinian background of the New Testament generally. The recent
advance in this study has concerned itself with the eschatological liter-
ature, and the oral teaching of the Rabbis; it has neglected the temple,
its priesthood, and worship. But in the New Testament period the tem-
ple system was central; after its destruction the Rabbis organized a
new Judaism on enlightened Pharisee lines. But it was a new religion,
not the old. The old religion died in the year a.p. 70, and gave birth to

Reprinted from Philip Carrington, The Meaning of the Revelation (Lon-
don; SPCK, 1931), ] cannot recommend all of Carrington’s opinions —for in-
stance his ridiculous JEDP-style “documentary hypothesis” of Revelation’s
authorship, and his views on the supposed evolution and late date of the text —
but I believe that his overall contribution to our understanding of St. John’s
meaning is very valuable and more than compensates for his shortcomings. In-
stead of registering my disagreement every time Carrington makes an objection-
able statement, f shall take the risk of expecting the reader to think for himself.

593
