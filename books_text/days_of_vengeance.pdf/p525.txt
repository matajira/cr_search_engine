THE FEASTS OF THE KINGDOM 19:19-21

ates a period of unprecedented power for the Church. Second,
the totality of the Warrior’s victory is so great that not even the
slain bodies of His opponents remain. All traces of the beast’s
armies are obliterated. Finally, considered systematically, judg-
ment never occurs apart from accompanying grace. The judg-
ment of Pharaoh is the liberation of Israel. So also here, the
judgment of the beasts and their armies cleanses the earth of
their idolatry and liberates the saints.”

19-21. The third vision in this section, marked again by the
words And I saw, reveals the defeat of Leviathan and Behemoth
in their war against the Kingdom of Christ: The two Beasts are
seized and thrown alive into the lake of fire, the fiery Laver (cf.
15:2) which burns with brimstone. The imagery is borrowed
from the story of the destruction of Sodom and Gomorrah (“fire
and brimstone”) combined with that of the rebels Korah,
Dathan, and Abiram, who with their households were swallowed
up by the earth’s mouth: “So they and all that belonged to them
went down alive into Sheol; and the earth closed over them, and
they perished from the midst of the assembly” (Num. 16:31-33).
St. John’s point, therefore, is not to provide a detailed personal
eschatology of the Beast and the False Prophet; still less is he at-
tempting to describe the Fall of Rome in 410 or 476. Rather, the
Lake of Fire is his symbolic description of the utter defeat and
complete destruction of these enemies in their attempt to seize
the Kingdom: The evil personifications of pagan Rome and
apostate Israel are ruined and overthrown. Rome, like Sodom,
is destroyed by fire and brimstone; Israel’s false prophets, like
Korah, Dathan, and Abiram, are swallowed up alive.

There is one notable contrast, however: Whereas the rest of
Korah’s followers were consumed by a blast of fire “from the
Lorp,” the rest of the Beasts’ followers —the kings of the earth
—are kitted with the sword that came from ihe mouth of Him
who sat upon the horse, The message of the Gospel, the Word-
sword of the Spirit, goes out from Christ’s mouth and destroys
His enemies by converting them, piercing them to the dividing
asunder of soul and spirit, of joints and marrow, judging the
thoughts and intentions of their hearts. The Beasts are doubly

26. Ibid., p. 12.
491
