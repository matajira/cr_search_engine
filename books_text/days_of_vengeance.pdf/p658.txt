APPENDIX C

The Favor of God

This is a key point of dispute between those who affirm and those
who deny the existence of common grace. ] wish to save time, if not
trouble, so let me say from the outset that the Christian Reformed
Church’s 1924 formulation of the first point is defective. The Bible
does not indicate that God in any way favors the unregenerate. The
opposite is asserted: “He that believeth on the Son hath everlasting
life: and he that believeth not the Son shall not see life; but the wrath
of God abideth on him” (John 3:36), The prayer of Christ recorded in
John 17 reveals His favor toward the redeemed and them alone. There
is a fundamental ethical separation between the saved and the lost.
God hated Esau and loved Jacob, before either was born (Rom.
9:10-13).

What are we to make of the Bible’s passages that have been used to
support the idea of limited favor toward creatures in general? Without
exception, they refer to gifts of God to the unregenerate. They do not
imply God’s favor. For example, there is this affirmation: “The Lord is
good to all: and his tender mercies are over ail his works” (Ps. 145:9).
The verse preceding this one tells us that God is compassionate, slow
to anger, gracious. Romans 2:4 tells us He is longsuffering. Luke
6:35-36 says:

But love ye your enemies, and do good, and lend, hoping for
nothing again; and your reward shall be great, and ye shail be
the children of the Highest: for he is kind unto the unthankfu)
and to the evil. Be ye therefore merciful, as your Father also is
merciful.

1 Timothy 4:10 uses explicit language: “For therefore we both
labour and suffer reproach, because we trust in the living God, who is
the Saviour of all men, specially of those that believe.” The Greek
word here translated as “Saviour” is transliterated sotér: one who
saves, heals, protects, or makes whole. God saves (heals) everyone,
especially those who believe. Unquestionably, the salvation spoken of
is universal—not in the sense of special grace, and therefore in the
sense of common grace. This is probably the most difficult verse in the
Bible for those who deny universal salvation from hell and who also
deny common grace.‘

The most frequently cited passage used by those who defend the

4. Gary North, “Aren’t There Two Kinds of Salvation?”, Question 75 in
North, 75 Bible Questions Your Instructors Pray You Won't Ask (Tyler, Texas:
Spurgeon Press, 1984).

626
