THE DOMINION MANDATE 3210-11

to keep them from falling (Jude 24). Although this is one of the
verses that dispensationalists have claimed for support of the
“pre-tribulation rapture” theory, on close examination it actu-
ally reveals itself to be nothing of the sort. In fact, it says noth-
ing about the end of the world or the Second Coming at all: The
“hour of testing” spoken of here is identified as that hour which
is about to come upon the whole world, to test those who dwell
upon the Land. It is speaking of the period of tribulation which,
in the experience of the first-century readers, was about to
come, Does it make sense that Christ would promise the church
in Philadelphia protection from something that would happen
thousands of years later? “Be of good cheer, you faithful, suffer-
ing Christians of first-century Asia Minor: [ won’t let those
Soviet missiles and Killer Bees of the 20th century get you!”
When the Philadelphian Christians were worried about more
practical, immediate concerns—official persecution, religious
discrimination, social ostracism, and economic boycotts —what
did they care about Hal Lindsey’s lucrative horror stories? By
twisting such passages as these to suit their passing fancies, cer-
tain modern dispensationalists have added to the Word of God,
and detracted from its message; and they thus come under the
curses of Revelation 22:18-19.

No, the promised hour of testing was in the immediate
future, as Scripture universally testifies; a mere hour of trial, to
be replaced by a thousand years of rule (20:4-6). St. John uses
the expression those who dwell on the Land twelve times in Rev-
elation (once for each of the twelve tribes) to refer to apostate
Israel (3:10; 6:10; 8:13; 11:10 [twice]; 13:8, 12, 14 [twice]; 14:6;
17:2, 8), In the Greek Old Testament (the version used by the
early Church), it is a common prophetic expression for rebel-
lious, idolatrous Israel about to be destroyed and driven from
the Land (Jer. 1:14; 10:18; Ezek. 7:7; 36:17; Hos. 4:1, 3; Joel 1:2,
14; 2:1; Zeph, 1:18), based on its original usage in the historical
books of the Bible for rebellious, idolatrous pagans about to be
destroyed and driven from the Land (Num. 32:17; 33:52, 55;
Josh. 7:9; 9:24; Judg. 1:32; 2 Sam. 5:6; 1 Chron. 11:4; 22:18;
Neh. 9:24); Israel has become a nation of pagans, atid is about
to be destroyed, exiled, and supplanted by a new nation, the
Church. The entire Roman world itself would be thrown into
massive convulsions, part of which would involve the persecu-

129
