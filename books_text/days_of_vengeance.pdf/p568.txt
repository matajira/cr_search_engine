20:14-15 PART FIVE: THE SEVEN CHALICES

found.”s? And they were judged, each one according to his
works: Again St. John emphasizes that men’s actions will come
into judgment at the Last Day.

14-15 St. Paul proclaimed that when Christ returns at the
end of His mediatorial Kingdom, “the last enemy that will be
abolished is Death” (1 Cor. 15:26). Thus, St. John saw Death
and Hades, which were paired in 1:18 and 6:8, thrown into the
lake of fire. As Terry says, “the entire picture of judgment and
perdition is wrapped in mystic symbolism, and the one certain
revelation is the final overthrow in remediless ruin of all who
live and die as subjects of sin and death.”*? Further, as Morris
observes, “death and Hades are ultimately as powerless as the
other forces of evil. Finally there is no power but that of God.
All else is completely impotent.”*4

This is the Second Death, the lake of fire. And if anyone was
not found written in the Book of Life, he was thrown into the
lake of fire. Universalists have tried for centuries to evade the
plain fact that Scripture slams the furnace lid shut over those
who are finally impenitent, whose names are not inscribed (from
the foundation of the world, 13:8; 17:8) in the Lamb’s Book of
Life. Using a metaphor similar to St. John’s, Jesus said: “If any-
one does not abide in Me, he is thrown away as a branch, and
dries up; and they gather them, and cast them into the fire, and
they are burned” (John 15:6). “The rest of the dead” will never
live, for there is no life outside of Jesus Christ.

52. Milton Terry, Biblical Apoculyptics, p. 457.

53. Terry, Biblical Apocalyptics, p. 458.

54. Leon Mortis, The Revelation of St. John (Grand Rapids: William B.
Eerdmans Publishing Co., 1969), pp. 24If.

534
