THE DAYS OF VENGEANCE

. Selected Shorter Writings. 2 vols. Edited by John E.
Meeter. Nutley, NJ: The Presbyterian and Reformed Publishing
Co., 1973.
Webber, Robert E. Worship: Old and New, Grand Rapids: Zondervan
Publishing House, 1982.
Weeks, Noel. “Admonition and Error in Hebrews.” The Westminster
Theological Journal 39 (Fall 1976) 1, pp. 72-80.
Wenham, Gordon J. The Book of Leviticus. Grand Rapids: William
B. Eerdmans Publishing Co., 1979.
. Numbers: An Introduction and Commentary. Downers
Grove, IL: Inter-Varsity Press, 1981.
Woodrow, Ralph. His Truth fs Marching On: Advanced Studies on
Prophecy in the Light of History, Riverside, CA: Ralph Woodrow
Evangelistic Association, 1977.

678
