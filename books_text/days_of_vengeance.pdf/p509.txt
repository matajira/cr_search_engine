THE FEASTS OF THE KINGDOM 19:9

where than in the Church, scatters the Church of Christ... . He
who does not hold this unity does not hold God’s law, does not
hold the faith of the Father and the Son, does not hold life and
salvation,”

The song of praise continues: And it was given to her to
clothe herself in fine linen, bright and clean; for the fine linen is
the righteous acts of the saints. We have already seen linen used
as a symbol (15:6; cf. 3:4; 4:4; 7:9, 14); now, its symbolic mean-
ing is explicitly stated to be the saints’ righteous acts.4 Two im-
portant points are made here about the saints’ obedience: first, it
was given to her—our sanctification is due wholly to the gra-
cious work of God’s Holy Spirit in our hearts; second, she was
graciously enabled to clothe herself in the linen of righteous
acts—our sanctification is performed by ourselves. This dual
emphasis is found throughout the Scriptures: “You shall sanc-
tify yourselves... . 1 am the Lorp who sanctifies you” (Lev.
20:7-8); “Work out your salvation with fear and trembling; for it
is God who is at work in you, both to will and to work for His
good pleasure” (Phil. 2:12-13),

9 St. John is instructed to write the fourth and central beati-
tude of the Book of Revelation: Blessed are thase who are in-
vited to the Marriage Supper of the Lamb. God’s people have
been saved from the whoredoms of the world to become the
Bride of His only begotten Son; and the constant token of this
fact is the Church’s weekly celebration of her sacred feast, the
Holy Eucharist. The absolute fidelity of this promise is under-
scored by the angel’s assurance to St. John that these are the
true words of God.

It should go without saying (but, unfortunately, it cannot),

3. Cyprian, On the Unity af the Church, 6; in Alexander Roberts and
James Donaldson, eds., The Ante-Nicene Fathers (Grand Rapids: William B.
Eerdmans, reprinted 1971}, Vol. 5, p. 423.

4. The Greek word is generally used in the New Testament to mean God's
“statute” or “ordinance” (Luke 1:6; Rom. 1:32; 8:4; Heb. 9:1, 10; Rev. 15:4);
the related meaning, used here, is “fulfillment of God’s statute” (cf. Rom.
5:18), A further meaning is the “judicial sentence that one has met God's re-
quirement” and hence “justification” (cf. Rom. 5:16). While some have argued
for “justification” as the proper meaning here, both the context and the fact
that the plural form of the word is employed indicate its most natural meaning
to be “righteous acts.”

415
