THE MILLENNIUM AND THE JUDGMENT

He will be satisfied with nothing less than what He paid for.

When the early missionaries from the East first ventured into
the demonized lands of our pagan forefathers, they had not the
slightest intention of developing peaceful coexistence with war-
locks and their terrorizing deities. When St. Boniface came up
against Thor’s sacred oak tree in his mission to the heathen Ger-
mans, he simply chopped it down and built a chapel out of the
wood. Thousands of Thor-worshipers, seeing that their god
had failed to strike St. Boniface with lightning, converted to
Christianity on the spot. As for St. Boniface, he was unruffled
by the incident. He knew that there was only one true God of
thunder —the Triune Jehovah.

There is nothing strange about this. The attitude of Hope,
the expectation of victory, is an absolutely fundamental charac-
teristic of Christianity.‘ The advance of the Church through the
ages is inexplicable apart from it— just as it is also inexplicable
apart from the fact that the Hope is true, the fact that Jesus
Christ has defeated the powers and shall reign “from the River
to the ends of the earth.” W. G. T. Shedd wrote: “Apart from the
power atid promise of God, the preaching of such a religion as
Christianity, to such a population as that of paganism, is the
sheerest Quixotism. It crosses all the inclinations, and condemns
all the pleasures of guilty man. The preaching of the Gospel
finds its justification, its wisdom, and its triumph, only in the at-
titude and relation which the infinite and almighty God sustains
to it. It is His religion, and therefore it must ultimately become a
uftiversal religion,”?

With the rise of divergent eschatologies over the last two
centuries, the traditional evangelical optimism of the Church
was tagged with the term “postmillennialism,” whether the so-
called “postmillennialists” liked it or not. This has had positive
and negative results. On the plus side, it is (as we have seen) a
technically accurate description of orthodoxy; and it carries the
connotation of optimism. On the minus side, it can too often be
confused with heretical millenarianism. And, while “amillen-

6. Consider the fact that the compilers of Tie Book af Common Prayer
provided “Tables for Finding Holy Days” all the way to 4.0. 8400! Clearly,
they were digging in for the “long haul,” and did not expect an imminent “rap-
ture” of the Church.

7. W. G. T. Shedd, Sermons to the Spiritual Man (London: The Banner of
Truth Trust, [1884] 1972), p. 421.

497
