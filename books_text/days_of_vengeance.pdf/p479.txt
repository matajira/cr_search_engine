18

BABYLON IS FALLEN!

Come Out of Her! (18:1-8)

1 After these things I saw another Angel coming down from
heaven, having great authority, and the earth was illumined
with His glory.

2 And He cried out with a mighty voice, saying: Fallen, Fallen
is Babylon the great! And she has become a dwelling place
of demons and a prison of every unclean spirit, and a prison
of every unclean and hateful bird.

3 For all the nations have drunk of the wine of the wrath of
her fornication, and the Kings of the earth have committed
fornication with her, and the merchants of the earth have
become rich by the wealth of her sensuality.

4 And I heard another voice from heaven, saying: Come out
of her, My people, that you may not participate in her sins
and that you may not receive of her plagues;

5 for her sins have piled up as high as heaven, and God has re-
membered her iniquities.

6 Pay her back even as she has paid, and give back double ac-
cording to her deeds; in the cup which she has mixed, mix
twice as much for her.

7 To the degree that she glorified herself and lived sensuously,
to the same degree give her torment and mourning; for she
says in her heart: I sit as a queen and am not a widow, and
will never see mourning.

8 For this reason in one Day her plagues will come, pestilence
and mourning and famine, and she will be burned up with
fire; for strong is the Lord God who judges her.

1 St. John is now introduced to another Angel—probably
the Lord Jesus Christ, considering the description of Him, com-
pared with statements about Christ in St. John’s Gospel: He

445
