SELECT BIBLIOGRAPHY

Josephus, Flavius. The Jewish War. Edited by Gaalya Cornfeld.
Grand Rapids: Zondervan Publishing House, 1982.

. Works. Translated by William Whiston. Four vols.
Grand Rapids: Baker Book House, 1974.

Jungmann, Josef A., S.J. The Early Liturgy to the Time of Gregory
the Great, Translated by Francis A. Brunner, C.S5.R. Notre Dame:
University of Notre Dame Press, 1959.

Justin Martyr. The First Apology.

Kaiser, Walter C. Jr. “The Blessing of David: The Charter for Human-
ity.” In The Law and the Prophets: Old Testament Studies Prepared
in Honor of Oswaid Thompson Allis, edited by John H. Skilton.
Philadelphia: The Presbyterian and Reformed Publishing Co.,
1974,

Kik, J. Marcellus. An Eschatology of Victory. Nutley, NJ: The Pres-
byterian and Reformed Publishing Co., 1971.

Kline, Meredith G. By Oath Consigned: A Reinterpretation of the
Covenant Signs of Circumcision and Baptism. Grand Rapids: Wil-
liam B. Eerdmans Publishing Co., 1968.

. images of the Spirit. Grand Rapids: Baker Book House,
1980,

. Kingdom Prologue, two vols, Privately published, 1981,
1983.

. The Structure of Biblical Authority, Grand Rapids: Wil-
Jiam B, Eerdmans Publishing Co., second ed., 1975.

. Treaty of ihe Great King: The Covenant Structure of
Deuteronomy. Grand Rapids: William B. Eerdmans Publishing
Co., 1963.

Kuyper, Abraham. Lectures on Caivinism. Grand Rapids: William B.
Eerdmans Publishing Co., 1931,

Lecerf, Auguste. An Introduction to Reformed Dogmatics. Trans-
lated by André Schlemmer. Grand Rapids: Baker Book House,
[1949] 1981.

Lee, Francis Nigel. The Central Significance of Culture. Nutley, NJ:
The Presbyterian and Reformed Publishing Co., 1976.

Lewis, C. S. The Weight of Glory: And Other Addresses. New York:
Maemillan Publishing Co., revised ed., 1980.

Lightfoot, J. B. The Christian Ministry. Edited by Philip Edgcumbe
Hughes. Wilton, CT: Morehouse-Barlow Co., 1983.

Lindsey, Hal. The Late Great Planet Earth. Grand Rapids: Zondervan
Publishing House, 1970.

MacGregor, Geddes. Corpus Christi: The Nature of the Church
According to the Reformed Tradition. Philadelphia: The Westmin-
ster Press, 1958.

673
