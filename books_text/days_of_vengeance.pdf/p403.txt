THE KING GN MOUNT ZION 14:12-13

Apostles, I am a convict; they were free, but I am a slave to this
very hour. Yet if I shall suffer, then am 1 a freed-man of Jesus
Christ, and I shall rise free in Him. Now I am learning to put
away every desire,

“From Syria even unto Rome I fight with wild beasts, by
land and sea, by night and day, being bound amidst ten leap-
ards, even a company of soldiers, who only wax worse when
they are kindly treated. Howbeit through their wrongdoings I
become more completely a disciple; yet am I not hereby justi-
fied. May I have joy of the beasts that have been prepared for
me; and I pray that I may find them prompt; nay, I will entice
them that they may devour me promptly, not as they have done
to some, refusing to touch them through fear. Yea, though of
themselves they should not be willing while I am ready, I myself
will force them to it. Bear with me. I know what is expedient for
me, Now I am beginning to be a disciple. May naught of things
visible and things invisible envy me; that I may attain unto Jesus
Christ. Come fire and cross and grapplings with wild beasts, cut-
tings and manglings, wrenching of bones, hacking of limbs,
crushings of my whole body, come cruel tortures of the devil to
assail me. Only be it mine to attain unto Jesus Christ.

“The farthest bounds of the universe shall profit me nothing,
neither the kingdoms of this world. It is good for me to die for
Jesus Christ rather than to reign over the farthest bounds of the
earth. Him I seek, who died on our behalf; Him I desire, who
rose again for our sake. The pangs of a new birth are upon me.
Bear with me, brethren. Do not hinder me from living; do not
desire my death. Bestow not on the world one who desireth to
be God’s, neither allure him with material things. Suffer me to
Teceive the pure light. When I am come thither, then shall I be a
man. Permit me to be an imitator of the passion of my God. If
any man hath Him within himself, let him understand what I
desire, and let him have fellow-feeling with me, for he knows
the things which straiten me.”!”

Alexander Schmemann reminds us, however, that “Christi-
anity is not reconciliation of death. It is the revelation of death,

17, St. Ignatius, Epistle to he Romans, iv-vi, ed. and trans. J. B. Lightfoot,
The Apostolic Fathers (Grand Rapids: Baker Book House, [1891] 1956), pp.
761. On the early Christian altitude toward martyrdom, see Louis Bouyer, The
Spirituality of the New Testament and the Fathers (Minneapolis: The Seabury
Press, 1963), pp. 190-210.

369
