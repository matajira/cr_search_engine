SUBJECT INDEX

Prophets, false
appearance of, 337
Jewish, 338-39
Protestant Reformed Church, 624, 625, 664
Protestants, 421
rationalistic, 219
Protevangelium, 298, 300
Psalms, 194-95
Hallel-, 470
Ptolemais, 440
Puritanism, Gturgical, 24.25
Puritans, 683

Rachel, 2I1, 212, 299, 555
Rahab, 304, 305
Rainbow, 150, 167, 260
Rapture theory, 149. See alsa
Dispensationalismn; Premillennialism
Kingdom parables and, 634-38
pretribulation, 128-29, 146-47, 615
Rebekah, 299, 307
Reconstruction, antinomian revivalism vs., 648-50
Redemption, 50, 87, 176-77, See also Salvation
Reformation, 41, 153, 477, 655
Refarmed faith. See Calvinism: Reformation
Regeneration, universal, 650-51
Remnant, 88, 207, 214, 227, 357
seven-thousand, 285
Repentance, 96, 109
Rest, 70
Restitution, 449-$0
Resurrection(s), 50
age of, 517
bodily, 331
veremonial, $16
Chris's, 101, 284, 290, 316, 517
first, 104, 508-19, 536, 547
second, 516, 517
two, 516
Reuben, 559
Revelation, Book of
arrangement of, 2, 17, 78-79, 167-68, 565
authorship of, 1-3
basic message of, 376
contemporary focus of, 582-83
covenant form of, 13-20, 46, 85, 141, 295,
ce)
date of, 3-6
destination of, 6-10
erroneous views of, 11-13, 7980
Ezekiel’s influence on, 20-23, 168
four sets of judgment in, 89, 422
function of, 575
Gospel of John compared with, 1-2
lessons of, 381-90
Levitical symbolism in, $93-611
Hurgical character of, 22-25, 393, $94.97, 601-2

7

Jiturgical structure of, 610-11
nature of, [1, 13, 28-23, $1, $3, 263-64, $93
outline of, 45-47
purpose of, 39-44, Sl, 352, 582
relevance of, 40-41, 43-44, $81
Roman Empire in, 325
Scripture concluded by, 580
structure of, 23, R9, 231, 295-96
succession arrangements in, 379-82
symbolism of, 27-31, 53, 79
text of, 44-48

Revelation, special, 5, 292
events and, 175-77
Israel's fate and, 13
mystery and, 265-66

Revivalism, antinonsian, 648-50

River of Life, 158, $19, 536, 566, 569

Robes, white. See White garments

Roman church, 477

Roman emperors, 76, 327-28, See also
Caesars; Roman Engpire; Rome

Roman Empire, 40, f18, 228, 269, 270. See
etso Rome
boeast of, 180, 327-35, 344, 433-36. See also

Beast
claims of, 108
Jews allied with, 103, 429
in Revelation, 325

Roman Mass, 594

Rome, 78, 323, 421. See aiso Roman Empire,
beast of.

Christ's kingship vs., 63-64
church vs., 8-9, 130
salvational claims of, 218

Rosh, $22

Rosh Hashanah, 289, 290, 301

Russia. See Soviet Union

Ruth, 426

 

Sabbath, 60-61. Tee also Lord's Day
nature of, 267
original, 70-71
weekly, 71

Sabinus, 406

Sackcloth, 276, 277

Sactaments, 163, 549. See also Baptism;

Eucharist

Sacrifice{s}. See aiso Alanement; Offerings)
Chuist’s, 172. See else Death af Christ
feast on, 609
lamb, $98, 609
preparation of lamb, 601-2
Temple, 597-600

Saint(s), 291-92
angels ruled by, 376
perseverance of. See Perseverance
reign of, $08-19

17
