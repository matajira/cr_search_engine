1731-2 PART FIVE: THE SEVEN CHALICES

God says, If a husband divorces his wife,
And she goes from him

And belongs to another man,

Will he still return to her?

Will not that land be completely polluted?
But you are a harlot with many lovers;

Yet you turn to Me, declares the Lorp.

Lift up your eyes to the bare heights and see;
Where have you not been violated?

By the roads you have sat for them

Like an Arab in the desert,

And you have polluted a land

With your harlotry and with your wickedness.
Therefore the showers have been withheld,
And there has been no spring rain.

Yet you had a harlot’s forehead;

You refused to be ashamed. (Jer. 2:20-24, 30-33; 3:1-3}

Israel's adulteries, Hosea said, took place “on every thresh-
ing floor” (Hos. 9:1): The picture is that of a woman prostituting
hetself for money in the grain house in harvest-time. This car-
ries a double meaning. First, Israel was apostatizing into Baal-
worship, seeking harvest blessing and fertility from false gods
(forgetting that fertility, and blessing in every area, can come
only from the one true God). Second, the Temple was built on a
threshing floor (2 Chron. 3:1), symbolizing God’s action
throughout history in separating the chaff from His holy wheat
(Job 21:18; Ps. 1:4; 35:5; Isa. 17:13; Luke 3:17). The threshing
floor is also symbolic of the marriage relationship: The union of
Boaz and Ruth took place on his threshing floor (Ruth 3), and
the action of grinding at a mill is a Biblical image of sexual rela-
tions (Job 31:10; Isa. 47:2; Jer. 25:10). Thus, instead of consum-
mating her marriage to God through worship at His threshing
floor, the Bride went whoring after every other threshing floor,
prostrating herself before strange gods and alien altars.

Apostate Jerusalem is the Harlot-city; this theme becomes
even more prominent in the prophecy of Ezekiel, particularly in
Ezekiel 16 and 23, where it is clear that her “adulteries” consist

4. For a full discussion of this point, see Calum M. Carmichael, “Treading
in the Book of Ruth,” ZAW 92 (1980), pp. 248-66.

426
