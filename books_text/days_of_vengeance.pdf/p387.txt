14

THE KING ON MOUNT ZION

St. John has just revealed the evil triad of enemies facing the
early Church: the Dragon, the Sea Beast, and the Land Beast.
He has made it clear that these enemies are implacable, that the
conflict with them will require faithfulness unto death. The
question again naturally arises: Will the Church survive such an
all-out attack? In this closing section of the fourth major divi-
sion of his prophecy, therefore, John again addresses these fears
of his audience. The action of the book comes to a halt as the
apostle gives comfort and provides reasons for confidence in the
coming victory of the Church over all her opposition. “The rev-
elation of the three great foes, the dragon, the beast from the
sea, and the beast from the land, is followed immediately by a
sevenfold disclosure of victory and judgment in the heavens.
The purpose of these visions and voices from heaven is obvi-
ously to show that the powers of the heavens are mightier than
those of the infernal serpent and his associates. The trinity of
hostile forces, armed with many lying wonders, might seem
from a human point of view invincible. But John, like the young
servant of Elisha when confronted with the horses and chariots
and immense host of the king of Syria, is here admonished that
they which are with the persecuted Church are more and mightier
than they which make war against her (comp. 2 Kings 6:15-17).”!

The Lamb with His Fair Army (14:1-5)

1 And I looked, and behold, the Lamb was standing on
Mount Zion, and with Him one hundred and forty-four

1. Milton Terry, Biblical Apocalypiics: A Study af the Most Notable Reve-
tations of God and of Christ in the Canonical Scriptures (New York: Baton
and Mains, 1898), p. 402.

353
