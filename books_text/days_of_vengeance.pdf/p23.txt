PUBLISHER'S PREFACE

these groups has intensified since then. Chilton’s book, Produc-
tive Christians in an Age of Guilt-Manipulators (1981),"° is the
most important single document in this theological confronta-
tion. But from the confused middle, there have been no clear-
cut Biblical answers to cither of these two positions.

The future of pessimillennialism is being eroded. As the
world’s social crises intensify, and as it becomes apparent that
traditional conservative Protestantism still has no effective, spe-
cific, workable answers to the crises of our day, a drastic and
presently unanticipated shift of Christian opinion probably wilt
take place—an event analogous to the collapse of a dam. There
will be a revolution in the way millions of conservative Chris-
tians think. Then there will be a revolution in what they do.

The liberation theologians will not win this battle for the
minds of Christians. There will be a religious backlash against
the Left on a scale not seen in the West since the Bolshevik Rev-
olution, and perhaps not since the French Revolution. At that
point, only one group will possess in ready reserve a body of in-
tellectual resources adequate for stemming the tide of human-
ism: the Christian Reconstructionists, meaning those who
preach dominion, and even more specifically, those who preach
dominion by covenant. With this intellectual foundation, given
the existence of catastrophic cultura}, economic, and political
conditions, they will take over leadership of conservative Prot-
estantism. The existing Protestant leaders suspect this, and they
do not like its implications. Nevertheless, they are unwilling or
unable to do what is necessary to counter this development.
Specifically, they are not producing the intellectual resources to
counter what the Christian Reconstructionists are producing.

Instead, they murmur. This tactic will fail.

Silencing the Critics
For over two decades, critics chided the Christian Recon-
structionists with this refrain: “You people just haven’t pro-
duced any Biblical exegesis to prove your case for eschatological
optimism.” Then came Paradise Restored in 1985. A deathly

10. David Chilton, Productive Christians in an Age of Guilt-Manipulators:
A Biblical Response to Ronald J, Sider (4th ed.; Tyler, Texas: Institute for
Christian Economics, 1986).

xxiii
