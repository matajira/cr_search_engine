BABYLON 18 FALLEN! 18:20

apostate, demonized City that led the world in rebellion against
God and persecution of His children. As the smoke of the whole
burnt offering ascends to heaven, the saints are to rejoice that
their prayers have been answered: God has judged your judgment
against her! the angel announces, employing a Hebraic pleon-
asm to express the divine Court’s “double witness” against her.
Again we find that the Biblical image of the Church, tabernacled
in heaven, is firm in its opposition to evil, praying for God to
vindicate His people in the earth. Note well: the judgment on
the Harlot is called “your judgment,” the Church’s judgment. It
was the just retribution to Israel for her oppression of saints,
apostles, and prophets throughout her history, and culminating
in the Last Days in her war against Christ and His Church. It
was she who had inspired the Roman persecution of Christians;
but the heathen wrath which she had stoked up had been poured
out on her head instead. If the Church in our age is to proceed
from victory to victory as did the Church in the apostolic age,
she must recover the triumphalistic perspective of the early
saints. The Church must pray for her enemies’ defeat —a defeat
that must come either by conversion or by destruction. We are
at war, a war in which the definitive victory has been won by our
King. All of history is now a mopping-up operation in terms of
that victory, looking forward to the conversion of the world and
the final overcoming of Death itself. Our opposition is doomed
to perish, and the Church is called to rejoice in the certain
knowledge of her earthly vindication and ultimate triumph.

Babylon is Thrown Down (18:21-29

21 And astrong angel took up a stone like a great millstone and
threw it into the sea, saying: Thus will Babylon, the great
City, be thrown down with violence, and will not be found
any longer.

22 And the sound of harpists and musicians and flute-players
and trumpeters will not be heard in you any longer; and no
craftsman of any craft will be found in you any longer; and
the sound of a mill will not be heard in you any longer;

23 and the light of a lamp will not shine in you any longer; and.
the Voice of Bridegroom and Bride will not be heard in you
any longer; for your merchants were the great men of the
earth, because all nations were deceived by your sorcery.

459
