KING OF KINGS 1:16

when he put on his splendid vestments,
and clothed himself in glorious perfection,
when he went up to the holy altar,
and filled the sanctuary precincts with his grandeur;
when he received the portions from the hands of the priests,
himself standing by the altar hearth,
surrounded by a crowd of his brothers,
like a youthful cedar of Lebanon
as though surrounded by the trunks of palm trees.
(Ecelesiasticus 50:5-12, Jerusalem Bible)

Completing the glorious picture of Christ is the statement
that His Voice was like the sound of rushing waters. St. John is
identifying the voice of Christ with the sound of the Cloud—a
sound which, throughout Scripture, resembles numerous earthly
phenomena: wind, thunder, trumpets, armies, chariots, and
waterfalls;3? or perhaps we should say that all these earthly phe-
nomena were created to resemble various facets of the Cloud.
The conclusion should be obvious: The resurrected, transfig-
ured Jesus is the incarnate Glory of God.

16 In His right hand He held seven stars; St. John goes on
more fully to interpret this in verse 20, but we should consider
first the immediate impression this sight would give to St. John
and his readers. The seven stars make up the open cluster of
stars known as the Pleiades, poetically thought of in the ancient
world as being bound together on a chain, like a necklace. The
Pleiades, forming part of the constellation Taurus, are men-
tioned in Job 9:5-9; 38:31-33; and Amos 5:8. The sun is with
Taurus in Spring (Easter), and the Pleiades are thus a fitting
symbol in connection with the coming of Christ: He holds the
stars that announce the rebirth and flowering of the world. The
other Biblical references make it clear that the One who holds
the seven stars is the almighty Creator and Sustainer of the uni-
verse.

But there is another dimension to this imagery. The symbolic
use of the seven stars was quite well known in the first century,

33. See Chilton, Paradise Restored, p. 58; cf. Ex, 19:16, 19; Ezek. 1:24,
34. See Herman Bavinck, The Doctrine of God (London: The Banner of
Truth Trust, [1951] 1977), pp. 88ff.

1
