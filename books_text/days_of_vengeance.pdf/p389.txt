THE KING ON MOUNT ZION 14:2-3

people and His victorious reign over the earth, when all king-
doms are gathered together to serve Him in the New Covenant
(cf. Ps. 9:1-20; 14:7; 20:1-2; 48:1-14; 69:35; 87:1-3; 99:1-9;
102:13-22; Isa. 24:21-23; 51-52; 59:16-20; Jer. 31:10-37; Zech.
9:9-17)3

The Lamb is thus not alone on Zion, for his people share in
His victory. They are there with Him, the one hundred and
forty-four thousand, the Remnant of Israel ordered for battle
according to the thousands of her tribes (see on 7:4-8). We saw
that the Mark of the Beast (13:16-17) was the parody of the
divine sealing of the true Israel (7:2-8); now St. John reminds us
of the original sealing, the mark of God’s ownership and protec-
tion of His obedient people. That the 144,000 are regarded as
members of the Church, and not ultimately as a separate cate-
gory of ethnic Israelites, is underscored by John’s combination
of previous imagery. We were told before that the 144,000 are
sealed on their foreheads (7:3), while it is alt Christ’s overcomers
who have His name and the name of His Father written on their
foreheads (3:12). The 144,000, therefore, belong to the Church,
the army of overcomers. Yet they are also a special group: the
Remnant-Church of the first generation.

 

2-3 With his eyes on the Lamb and His army, St. John hears
a Voice from heaven, the familiar reminder of God’s presence in
the Glory-Cloud: like the sound of many waters and like the
sound of loud thunder, and . . . like the sound of harpists play-
ing on their harps, the heavenly orchestra playing accompani-
ment to the victory song of the army of saints, who sing a New
Song before the Throne and before the four living creatures and

3. Once we understand that the Garden of Eden was on a mountain, we can
more casily understand the basis for the amazing agreement among the myth-
ologies of the different cultures. All cultures originated from the dispersal at
Mount Ararat, and later at Babel; and they took with them the memories of
the original Paradise. Thus, in every ancient culture, there are myths of the
dwelling-place of God on the Cosmic Mountain (e.g., Mount Olympus), and
of man’s expulsion from Paradise, and his attempts to return (e.g., the almost
universal preoccupation with building tower-gardens, pyramids, and mounds;
ef, the “groves” and “high places” of apostate Isracl), Sec R. J. Rushdoony,
The One and the Many: Studies in the Philosophy of Order and Ultimacy
(Tyler, TX: Thoburn Press, [1971] 1978), pp. 36-53; cf. Mircea Eliade, The
Myth of the Eternal Return: ot, Cosmos and History (Princeton: Princeton
University Press, 1954, 1971), pp. 12-17.

355
