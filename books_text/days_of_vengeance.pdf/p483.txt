BABYLON IS FALLEN! 18:4-5

Age brought in by Christ’s redemptive act—and had fallen
away. It would be “impossible to renew them again to repent-
ance.” Judaism—the vain attempt to continue the Old Cove-
nant while rejecting Christ “is worthless and close to being cursed,
and it ends up being burned” (Heb. 6:4-8), Old Covenant relig-
ion cannot be revivified; it is impossible to have the Covenant
without Christ. There can be no turning “back” to something
which never existed, for even the fathers under the Old Cove-
nant worshiped Christ under the signs and seals of the provi-
sional age (1 Cor. 10:1-4). Now that “the Age to come” has arrived,
salvation is with Christ and the Church. Only destruction awaits
those who are identified with the Harlot: Come out of her, My
people, that you may not participate in her sins and that you
may not receive of her plagues (cf. Heb. 10:19-39; 12:15-29;
13:10-14). Time for Israel’s repentance has run out, and by now
her sins haye piled up [literally, have adhered] to heaven (cf.
Gen. 19:13; 2 Chron. 28:9; Ezra 9:6; Jer. 51:9; Jon. 1:2). Jesus
had foretold that this crucifying generation would “fill up the
measure of the guilt” of their rebellious fathers, and thus that
upon them would fall “all the righteous blood shed on earth”
(Matt. 23:32-35). This prophecy was fulfilled within the first
century, as St. Paul observed: “They are not pleasing to God,
but hostile to all men, hindering us from speaking to the Gen-
tiles that they might be saved; with the result that sey always fill
up the measure of their sins. But wrath has come upon them to
the uttermost” (1 Thess. 2:15-16).

Therefore, not only religious separation was demanded —
that you may not participate in her sins—but physical, geo-
graphical separation was necessary as well (cf. Matt. 24:16-21),
that you may not receive of her plagues. The language is remin-
iscent of God’s call to His people to come out of Babylon at the
end of the captivity. The Old Testament texts speak in terms of
three ideas: the coming destruction of Babylon, the coming re-
demption of the faithful Covenant people, and the rebuilding of
the Temple (Ezra 1:2-3; Isa. 48:20; 52:11-12; Jer. 50:8; 51:6, 9,
45). Similarly, the New Covenant people were to separate them-
selves from Israel. The persecutors were about to suffer destruc-
tion at God’s hands, the Church’s redemption was drawing near
(Luke 21:28, 31), and the new Temple was about to be fully
established.

449
