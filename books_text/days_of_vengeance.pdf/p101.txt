KING OF KINGS 1:9

demptive: “For God did not send His Son into the world to con-
demn the world, but to save the world through Him” (John
3:17). Christ “comes in the Clouds” in historical judgments so
that the world may know the Lord God as the eternal and un-
changeable Source and Goal of all history (Rom. 11:36), the
Alpha and the Omega, the A and Z (cf. Isa. 44:6), who is and
who was and who is to come, the eternal Origin and Consum-
mation of all things. Almighty is the usual translation of the
Greek word Pantokrator, which means the One who has all
power and rules over everything, the New Testament equivalent
of the Old Testament expression Lord of Hosts, the “Captain of
the Armies” (meaning the armies of Israel, or the star/angel
armies of heaven, or the armies of the heathen nations, whom
God used to pour out His wrath on His disobedient people).
Christ was about to demonstrate to Israel and to the world that
He had ascended to the Throne as Supreme Ruler.

desus Christ, Transcendent and Immanent (1:9-16)

9 1, John, your brother and companion in the Tribulation and
Kingdom and perseverance which are in Christ Jesus, was on
the island of Patmos because of the Word of God and the
Testimony of Jesus.

10 | came to be in the Spirit on the Lord’s Day, and 1 heard
behind me a loud Voice like a trumpet,

11 saying: Write in a book what you see and send it to the seven
churches: to Ephesus, Smyrna, Pergamum, Thyatira, Sar-
dis, Philadelphia and Laodicea.

12 And i turned to see the Voice that was speaking to me. And
when [ turned I saw seven golden lampstands;

13 and in the middle of the seven lampstands one like a Son of
Man, clothed in a robe reaching to His feet and with a
golden sash around His chest.

14 And His head and His hair were white like wool, as white as
snow, and His eyes were like blazing fire.

1S His feet were like bronze glowing in a furnace, and His
Voice was like the sound of rushing waters.

16 In His right hand he held seven stars, and out of His mouth
came a sharp two-edged sword; and His face was like the
sun shining in its strength.

9 In this remarkable verse we have a concise summary of St.
67
