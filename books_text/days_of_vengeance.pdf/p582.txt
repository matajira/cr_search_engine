21:6 PART FIVE; THE SEVEN CHALICES

that lives and moves in it. Ad! is yours, says the apostle [1 Cor.
3:22]. Religion is not a single, separate sphere of human life, but
the divine principle by which the entire man is to be pervaded,
refined, and made complete. It takes hold of him in his undivided
totality, in the center of his personal being; to carry light into his
understanding, holiness into his will, and heaven into his heart;
and to shed thus the sacred consecration of the new birth, and
the glorious liberty of the children of God, over his whole in-
ward and outward life. No form of existence can withstand the
renovating power of God’s Spirit, There is no rational element
that may not be sanctified; no sphere of natural life that may
not be glorified. The creature, in the widest extent of the word,
is earnestly waiting for the manifestation of the sons of God,
and sighing after the same glorious deliverance. The whole crea-
tion aims toward redemption; and Christ is the second Adam,
the new universal man, not simply in a religious but also in an
absolute sense. The view entertained by Romish monasticism
and Protestant pietism, by which Christianity is made to consist
in an abstract opposition to the natural life, or inflight from the
world, is quite contrary to the spirit and power of the Gospel, as
well as false to its design. Christianity is the redemption and ren-
ovation of the werfd. It must make aff things new.”?

6 And He said to me: It is done! This is the flip side of the
declaration of Babylon’s destruction (16:17), both texts serving
as echoes of His cry on the Cross: “It is finished!” (John 19:30).
By His redemption, Christ has won the everlasting defeat of His
enemies and the eternal blessing of His people.

The One who sits on the Throne names Himself (as in 1:8)
the Alpha and the Omega (in English, “the A and the Z”), mean-
ing the Beginning and the End, the Source, Goal, and Meaning
of all things, the One who guarantees that the promises will be
fulfilled. This is said here in order to confirm what is to follow,
in Christ’s promise of the Eucharist.

We noted above that our Lord’s final announcement from
the Cross from St. John’s Gospel (“It is finished!”) is echoed
here; but there is more. For after Jesus made that proclamation

9. Philip Schaff, The Principle of Protestantism, trans. John Nevin (Phila-
delphia: United Church Press, [1845] 1964), p. 173.

548
