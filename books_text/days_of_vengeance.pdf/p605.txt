COME, LORD JESUS! 22:5

In much the same way he wrote to the Thessalonians, argu-
ing that their lives must be characterized by the approaching
Dawn rather than by the fading Night:

For you yourselves know full well that the Day of the Lord
will come just like a thief in the night. While they are saying,
“Peace and safety!” then destruction will come upon them sud-
denly like birth pangs upon a woman with child; and they shall
not escape. But you, brethren, are not in darkness, that the Day
should overtake you like a thief; for you are all sons of Light
and sons of Day. We are not of Night nor of Darkness; so then
let us not sleep as others do, but let us be alert and sober. For
those who sleep do their sleeping at night, and those who get
drunk get drunk at night. But since we are of Day, let us be
sober, having put on the breastplate of faith and love, and asa
helmet, the hope of salvation. For God has not destined us for
wrath, but for obtaining salvation through our Lord Jesus
Christ. (1 Thess. 5:2-9)

The era of the Old Covenant was the time of the world’s
dark Night; with the Advent of Jesus Christ has come the age of
Light, the great Day of the Lord, established at His Ascension
and His full inauguration of the New Covenant:

Arise, shine; for your Light has come,

And the Glory of the Lorp has risen upon you.

For behold, Darkness will cover the earth,

And deep Darkness the peoples;

But the Lorn will rise upon you,

And His Glory will appear upon you.

And nations will come to your Light,

And kings to the brightness of your rising. (Isa. 60:1-3)

For behold, the Day is coming, burning like a furnace; and
all the arrogant and every evildoer will be chaff; and the Day that
is coming will set them ablaze, says the Lorp of hosts, so that it
will leave them neither root nor branch. But for you who fear
My name the Sun of righteousness will rise with healing in His
wings; and you will go forth and skip about like calves from the
stall. (Mal. 4:1-2)

Blessed be the Lord Gad of Israel,
For He has visited us and accomplished redemption for His
people... .

37
