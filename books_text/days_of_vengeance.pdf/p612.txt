2215-16 PART FIVE: THE SEVEN CHALICES

we are in Him: The one who says he abides in Him ought himself
to walk in the same manner as He walked. (1 John 2:3-6)

These alone have the right to the Tree of Life (promised to
the overcomers in 2:7) and may enter by the gates into the City
(promised to the overcomers in 3:12). Again, we should note
that the nations of the earth will enter into the City (21:24-26),
which means that the nations and their rulers will be character-
ized by righteousness, by the world-conquering faith of the
overcomer.

1§ Christ provides another catalogue (cf. 21:8), a sevenfold
one this time, of those who are excluded from blessing, banished
outside the City, into the fiery Gehenna (Isa, 66:24; Mark
9:43-48). First are mentioned the dogs, scavengers that are re-
garded with disgust and revulsion throughout the Bible (cf.
Prov, 26:11). In Deuteronomy 23:18, sodomites are called
“dogs,”'? and Christ equated dogs with the unclean nations
(Mark 7:26-28). St. Paul applies the term, in what must have
been a shocking reference, to the faise circumcision, the Jews
who had betrayed the Covenant by rejecting Christ (Phil. 3:2)
and have thus joined the heathen and the perverts. That is prob-
ably the reference here (cf. 2:9; 3:9), God does not give what is
holy unto dogs (Matt. 7:6). The other categories mentioned in
this verse, the sorcerers and the fornicators and the murderers
and the idolaters, and everyone who loves and practices lying,
are also listed at 21:8, 27. Christians have renounced all these
ungodly actions by their baptism to newness of life.

16 I, Jesus, have sent My angel to testify to you these things
for the churches; the word you is plural, meaning that St. John’s
audience is directly addressed by the Lord; and the message is
for the churches generally (“all the saints,” v. 21). Christ repeats
the lesson of 5:5, that He is the bringer of the New Covenant,
the “Charter for Humanity” through which all nations will be
blessed: I am the Root and the Offspring of David, both the
Source and Culmination of the Davidic line. Hengstenberg
comments: “Because Jesus is the root, he is also the race of

12, See Rousas John Rushdoony, The Institutes of Biblical Law (Nutley,
NJ: The Craig Press, 1973), pp. 89f.

578
