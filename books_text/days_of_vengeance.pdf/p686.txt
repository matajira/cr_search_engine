APPENDIX C

both of Bahnsen’s doctrines in his critique of Theonomy,'* and Bahn-
sen in his rebuttal essay did respond to Kline’s criticisms of his post-
millennial eschatology, but he again denies that eschatology has any-
thing logically to do with biblical ethics.'° But Kline was correct: there is
unquestionably a necessary connection between a covenantal concept of
biblical law and eschatology. Kline rejects the idea of a New Testament
covenantal law-order, and he also rejects postmillennialism.

Amillennial Calvinists will continue to be plagued by Dooyeweerd-
ians, mystics, natural-law compromisers, and antinomians of all sorts
until they finally abandon their amillennial eschatology. Furthermore,
biblical law must be preached. It must be seen as the tool of cultural
reconstruction. It must be seen as operating now, in New Testament
times. It must be seen that there is a relationship between covenantal
faithfulness and obedience to law—that without obedience there is no
faithfulness, no matter how emotional believers may become, or how
sweet the gospel tastes (for a while). And there are blessings that fol-
low obedience to God’s law-order. Amillennialists, by preaching es-
chatological impotence culturally, thereby immerse themselves in
quicksand — the quicksand of antinomianism. Some sands are quicker
than others. Eventually, they swallow up anyone so foolish as to try to
walk through them. Antinomianism leads into the pits of impotence
and retreat.

Epistemological Self-Consciousness

What is meant by epistemological self-consciousness? It means a
greater understanding over time of what one’s presuppositions are,
and a greater willingness to put these presuppositions into action. It
affects both wheat and tares.

In what ways does the wheat resemble the tares? In what ways are
they different? The angels saw the differences immediately. Gad there-
fore restrained them from ripping up the tares. He wanted to preserve
the soil—the historical process. Therefore, the full development of
both wheat and tares is permitted by God.

‘What must be understood here is that ‘He doctrine of special grace
in history necessarily involves the doctrine af common grace. As the
Christians develop to maturity, they become more powerful. This is
not a straight-line development. There are times of locusts and blight
and drought, both for Christians and for satanists (humanists). There

18. Kline, op. cit.

19. Greg L. Bahnsen, “M. G. Kline on Theonomic Politics: An Evaluation
of His Reply,” Journal of Christian Reconstruction, VI (Winter, 1979-80}, No.
2, especially p. 215.

654
