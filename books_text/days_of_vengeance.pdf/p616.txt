CONCLUSION

{pp. 29-31). The Book of Revelation is written in “signs,” i.e.
symbols (p. 53). Symbolism is inescapable; in fact, everything is
symbolic (pp. 32-33). Symbolism is analogical, not realistic; it is
fluid, not a “code” (pp. 33-34). The primary controls on undue
speculation must be faithfulness to the Bible’s system of doctrine,
and faithfulness to the Bible’s system of symbolism (pp. 38-39).

The Book of Revelation

The Book of Revelation has a contemporary focus; it is not
about the Second Coming (pp. 39-44), but about the inaugura-
tion of the New Covenant era during the Last Days— the period
A.D. 30-70, from the Ascension of Christ to the fall of Jerusalem
(p. 5]). Written sometime within the final decade of Israel’s his-
tory (pp. 3-6) in the distinctive form of the Biblical Covenant
Lawsuit (pp. 10-20, 46-47, 49-50, 85-86, 141-44, 225-27, 379-82),
its main prophecies were to be fulfilled shortly (p. 51-55), The
prophecy was intended to be read in the liturgical setting of the
first-century churches (p. 54), and so begins with Seven Letters
to the churches of Asia Minor. Each Letter recapitulates the
five-part structure of the historic Biblical covenants (pp. 85-86).
Taken together, the Letters recapitulate all of Covenant history,
from Adam to Christ (pp. 86-89); and they also foreshadow the
entire structure of Revelation (pp. 89-91). The Seven Seals set
forth the period of the Last Days in general (p. 181); the Seven
Trumpets warn of the Tribulation, up to the first siege of Jeru-
salem under Cestius (pp. 252-53, 286); and the Seven Chalices
reveal the final outpouring of God’s wrath upon Jerusalem and
the Temple in a.p. 67-70 (pp. 383-84).

Revelation is written to comfort and instruct the churches
that are plagued and oppressed by an occult, gnostic, statist form
of apostate Judaism which had captured the religious hierarchy
of Israel (pp. 94, 106-07, 115-16). St. John calls this movement var-
ious symbolic names —“Nicolaitans,” “Balaamites,” “Jezebelites,”
and “the Synagogue of Satan”—but all these expressions refer
to the same cult (pp. 98, 101-03, 107-08, 113-14, 127-28).

The meaning of the main symbols in Revelation may be sum-
marized as follows:

The Seven-Sealed Book is the New Covenant, which Christ
obtained at His glorious Ascension and “opened” during the
period of the Last Days, climaxing in the destruction of Jeru-

582,
