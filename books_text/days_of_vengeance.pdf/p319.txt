THE END OF THE BEGINNING 11:13-14

ing of heaven and earth. Scripture connects as one theological
Event —the Advent —Christ’s birth, life, death, resurrection, as-
cension, the outpouring of His Spirit upon the Church in a.p.
30, and the outpouring of His wrath upon Israel in the Holo-
caust of A.p. 66-70: Thus im that Day there was a great earth-
quake (cf. Rev. 6:12; Ezek. 38:19-20; Hag. 2:6-7; Zech. 14:5;
Matt. 27:51-53; Heb. 12:26-28). Because the triumph of Christ
meant the defeat of His enemies, a tenth of the City fell. Actu-
ally, the whole City of Jerusalem fell in a.p, 70; but, as we have
seen, the Trumpet-judgments do not yet reach the final end of
Jerusalem, but (apparently) go only as far as the first siege of
Jerusalem, under Cestius. In conformity to the nature of the
Trumpet as an alarm, God’s taking a “tithe” of Jerusalem in the
first siege was a warning to the City.

For clearly symbolic, Biblical-theological reasons, St. John
tells us that seven thousand people were killed in the earth-
quake. Ultimately, the Earth-and-Heavenquake brought by the
New Covenant killed many more than seven thousand. But the
number represents the exact reverse of the situation in Elijah’s
day. In 1 Kings 19:18, God told Elijah that 7,000 in Israel re-
mained faithful to the covenant. Even then, it was most likely a
symbolic number, indicating completeness (seven) multiplied by
many (one thousand). In other words, Elijah should not be dis-
couraged, for he was not alone. God’s righteous elect were num-
erous, and the whole number was present and accounted for. On
the other hand, however, they were in the minority. But now, in
the New Covenant, the situation is reversed. The latter-day Eli-
jahs, the faithful witnesses in the Church, are not to be dis-
mayed when it seems as if God is destroying all Israel, and the
faithful are few in number, For this time it is the apostates, the
Baal-worshipers, who are the “seven thousand in Israel.” The
tables have been turned. In the Old Testament, only “7000”
faithful existed; in the New Testament, only “7000” are wicked.
They are destroyed, and the rest—the overwhelming majority
—are converted and saved: The rest were terrified and gave
glory to the God of heaven — Biblical language for conversion
and belief (cf. Josh. 7:19; Isa. 26:9; 42:12; Jer. 13:16; Matt. 5:16;
Luke 17:15-19; 18:43; 1 Pet. 2:12; Rev. 14:7; 15:4; 16:9; 19:7;
21:24). The tendency in the New Covenant age is judgment unto
salvation.

285
