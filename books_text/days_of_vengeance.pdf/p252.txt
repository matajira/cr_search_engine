7:10 PART THREE: THE SEVEN SEALS

moneychangers (Matt. 21:12-13; Mark 11:15-17; Luke 19:45-46;
ef. John 2:13-16).

In paralleling the cleansing of the Temple, the scene of the
redeemed multitude in Revelation also reverses the image; for,
unlike the great multitude that greeted Jesus with palm branches
(Matt. 21:8}, but possessed only leaves and no fruit (Matt.
21:19), the multitude of Revelation 7 is Christ’s new nation,
bearing fruit and inheriting the Kingdom (Matt. 21:43), That St.
John intends us to see such a parallel is clear from the fact that
the word translated palm (phoinix) occurs only two times in the
New Testament —here, and in the story of Palm Sunday in the
Gospel of John (12:13).

40 Joining in the heavenly liturgy, the innumerable multi-
tude shouts: Salvation (.e., Hosanna! cf. John 12:13) unto our
God who sits on the Throne, and to the Lamb! —ascribing to
God and to the Lamb what Rome claimed for the Caesars.
Mark Antony said of Julius Caesar that his “only work was to
save where anyone needed to be saved”; +* and now Nero was on
the throne, whom Seneca (speaking as “Apollo”) had praised as
the divine Savior of the world:

He is like me in much, in form and appearance, in his poetry
and singing and playing. And as the red of morning drives away
dark night, as neither haze nor mist endure before the sun’s rays,
as everything becomes bright when my chariot appears, 80 it is
when Nero ascends the throne. His galden locks, his fair counte-
nance, shine like the sun as it breaks through the clouds. Strife,
injustice and envy collapse before him. He restores to the world
the golden age.?

In direct contradiction to the State-worshiping blasphemies
of Rome and Israel, the Church declares that salvation is the
province of God and His Son alone. In every age, this has been
a basic issue. Who is the Owner and Determiner of reality?
Whose word is law? Is the State the provider of salvation? For

18. Ethelbert Stauffer, Christ and the Caesars (Philadelphia: The Westmin-
ster Press, 1955), p. $2.

19. Ibid., p. 139. Nero eventually repaid Seneca for a lifetime of servile idol-
atry by ordering him ta commit suicide,

218
