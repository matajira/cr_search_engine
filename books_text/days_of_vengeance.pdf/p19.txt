PUBLISHER'S PREFACE

sort of isolated theological maverick who simply invented his
findings out of thin air—or worse, in a room filled with odd-
smelling smoke. He came to these insights while he was working
with other men in what has become known as “the Tyler group,”
located in Tyler, Texas, a town of about 75,000 in East Texas.

This book is a good example, for better or worse, of what
has become known as “Tyler theology.” This theology is part of
a larger stream of thought called Christian Reconstruction, also
called “theonomy,” although some members of these schools of
thought prefer to avoid these terms. The broadest term is “do-
minion theology.”

There are many people who espouse dominion theology who
are not theonomists, and there are theonomists who are not
“Tyterites.” In fact, they ate very loudly of Tylerites. They will
go out of their way to buttonhole people to tell them the extent
to which they are not Tylerites. They have come close to defining
themselves and their ministries as “not being Tylerite.” (There is
a scene in the old “Dracula” movie when the professor flashes a
crucifix at Bela Lugosi, who immediately turns aside and pulls
his cape over his face. I think of this scene whenever I think of
these men telling others about Tyler. Some day I would like to
flash a “Welcome to Tyler” sign in front of them, just to see what
happens.) I know several of them who might someday be willing
to start churches with names like “The First Not Tylerite Church
of... .” | know another who thinks of his group as “The First
li a.m. Sunday Morning Not Tylerite Bible Study. . . .” They
will therefore not appreciate Chilton’s book. They will blame
Chilton for adopting ideas that have been distributed from East
Texas. Even though they might otherwise have agreed with his
arguments, they are infected with a serious case of NDH— “Not
Discovered Here” —a common malady among intellectuals.

In short, they may attack The Days of Vengeance when they
are really after Jordan and Sutton. Readers should be aware of
this possibility well in advance. There is more to this book than
meets the eye.

Two things make the Tyler theology unique in the Christian
Reconstruction camp: (1) its heavy accent on the church, with
weekly Communion; and (2) its heavy use of the five-point cove-
nant model. Covenant theology, especially the church covenant,
has not been a major focus in the writings of some of the non-

xix
