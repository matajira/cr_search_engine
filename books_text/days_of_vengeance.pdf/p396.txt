14:8 PART FOUR: THE SEVEN TRUMPETS

The Lord Himself proclaimed the glory and judgment of God to
the authorities of Israel (Matt. 26:64), and warned His disciples
that they would preach an unpopular Gospel to the rulers: “But
beware of men; for they wili deliver you up to the courts, and
scourge you in their synagogues; and you shall even be brought
before governors and kings for My sake, as a testimony to them
and to the Gentiles” (Matt. 10:17-18). Moreover, “this Gospel of
the Kingdom shall be preached in the whole world for a witness
to all the nations, and then the end shall come” (Matt, 24:14).
And this was the Gospel order —to the Jews first, and then to the
Gentiles (Acts 3:26; 11:18; 13:46-48; 28:23-29; Rom. 1:16; 2:9):
The angel preaches to the rulers of Palestine, and then to every
nation and tribe and tongue and people. Before the end came in
A.D. 70, St. Paul tells us, the Gospel was indeed preached to all
the world (Rom. 1:8; 10:18; Col. 1:5-6, 23). In spite of the at-
tempts of the Dragon and his two Beasts to thwart the progress
of the Gospel, the mission of the apostles, evangelists, martyrs,
and confessors of the early Church was successful. The world
was evangelized.?

8 Another angel, a second one follows, presenting another
aspect of the early Church’s proclamation: Fallen, fallen is Bab-
ylon the Great! This is the first mention of “Babylon” in Revela-
tion, a proleptic reference foreshadowing the full exposition to
come in later chapters (similar to the early reference to the Beast
in 11:7), It is certainly possible, however, that St. John’s readers
understood his meaning immediately, In his first epistle, pre-
sumably written before the Revelation, St. Peter described the
local church from which he wrote as “she who is in Babylon”
(1 Pet. 5:13), Many have supposed this to be Rome, where St.
Peter was (according to tradition) later martyred; but it is much
more likely that the apostle was in Jerusalem when he wrote
these words. Based on data from the New Testament itself, our
natural assumption should be that “Babylon” was Jerusalem,
since that was where he lived and exercised his ministry (Acts
8:1; 12:3; Gal. 1:18; 2:1-9; cf. 1 Pet. 4:17). Moreover, St. Peter’s
first epistle also sends greetings from Mark and Silas [Silvanus]

9. See David Chilton, Paradise Restored. A Biblical Theology of Dominion
(Ft. Worth, TX: Dominion Press, 1985), pp. 90f.

362,
