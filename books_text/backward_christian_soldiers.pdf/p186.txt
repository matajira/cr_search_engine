172 BACKWARD, CHRISTIAN SOLDIERS?

last of those who can produce such works.”

The old man may have been exaggerating, but
when a master craftsman tells me that such and such
is basic to his.craft, [ am not in a position to argue.
Perhaps a physiologist might object. Certainly, the
manipulation skills—the “feel” for the use of the
tools — must have been limited to those who had ex-
perienced long years of training,

During my speech, I discussed my theory that it is
mandatory that a person who possesses certain skills
reach back and pull along another person who is far-
ther behind; this is how improvements in everyone’s
productivity and income are increased. I mentioned
an old recommendation I read in a book on manage-
ment that no one should be promoted in a company
until he has trained two men who can replace him.
Afterwards, one of the members told me that
Walters’ philosophy is that each man needs to train
seven potential replacements.

WHY APPRENTICESHIP? It is odd that two men,
operating with such different theological and philo-
sophical presuppositions as Walters and I hold,
should be in such agreement about the educational
process. The impersonalism of bureaucracy,
especially educational bureaucracy, repels us both.
We are convinced that the best managers are trained
by successful managers, and that personal contact
with craftsmen, on the job, provides more insight
into the actual workings of the world than a detailed
textbook explanation.

The “personal touch” is a real phenomenon. If we
