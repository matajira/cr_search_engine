746 BACKWARD, CHRISTIAN SOLDIERS?

that once were taken for granted are taday being
challenged. And as the real threat to Christian civili-
zation is recognized, the former divisions with other
Christian groups are being seen as matters of subsid-
iary importance at this stage of history.

THE NEW COOPERATION The rise of the “New
Christian Right” in the U.S. since the late 1970’s has
yet to be fully understood, even by those within the
movement. But this much is clear: the increasing ar-
rogance of the humanist elite which controls the
West is creating an opposition movement which is it-
self increasingly confident in the foundations of its
own power, namely, the God of the Bible and the
power of biblical revelation. Christian leaders who a
decade ago would have rejected both doctrines are
today preaching about the sovereignty of God and
the law of God. A new Puritanism is developing—a
Puritanism which offers men the hope of God-
honoring social transformation.

This new cooperation can be compared to a stool
which rests on three legs. Each leg is important, yet
as recently as 1959 these three legs either did not ex-
ist or were not being used outside of some narrow
denominational tradition. The three legs are: 1)
Presbyterian scholarship and six-day creationism; 2)
Baptist day schools; and 3) the Pentecostals’ various
satellite communications systems.

PRESBYTERIAN SCHOLARSHIP Conservative
biblical scholarship, outside of the six-day crea-
tionism issue and Wycliffe-based linguistic scholar-
