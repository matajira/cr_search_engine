THE TAPE MINISTRY 205

those who live in another region? Pastors with a uni-
que outlook on the Bible may have a considerable
audience in distant homes. Some ministries are
noted more for their tapes than for their congrega-
tions. Col. Bob Thieme of Houston pioneered the
tape ministry, and Albert Martin of New Jersey has
built up a large following.

It is obvious that the cassette tape has brought a
technological revolution to the churches. Some min-
isters have used tapes effectively. Most of them, from
what I have observed, have not.

LAZINESS It takes time to develop a successful
church tape ministry. It also takes more care in
preparing sermons. If people are going to listen to
sermons over and over, or if others who are more
careful listeners start subscribing, then flaws in the
presentations will become public knowledge. Some
pastors prefer not to let their actual abilities be
broadcasted widely.

Then there is the problem of gctting someone to
produce the tapes. It takes a considerable capital in-
vestment. The tape reproduction equipment is expen-
sive, and these units must be maintained. Also, few
pastors know which units are the “state of the art” in
any price range. Not many parishioners are sure,
either, So mistakes are easy, unless someone devotes a
lot of time to reading up on what machines are best
for a particular church ministry and church budget.

MARKETING ‘Tapes just do not sell themselves. I

have a successful tape business, but it is geared to
