CAPTURING THE ROBES 63

degree experience— persons with doctorates in
religion, with masters and bachelors of divinity
degrees, and so forth. This holds for the entire
religion faculty as well as throughout the vari-
ous denominational groups (p. 86.).

The obvious conclusion is simple: conservative
fundamentalists who run the handful of colleges that
fundamentalist students attend must cease requiring
the Ph.D. from their faculty members. Indeed,
anyone holding the Ph.D. in the humanities must be
screened extra carefully to insure that he is not a lib-
eral. What, in fact, are these colleges doing? “Up-
grading’ their faculties by requiring the Ph.D. The
suicide of the evangelicals, institutionally, is assured.
The liberals have convinced them that they must
structure their colleges “the liberals’ way.” The
academic inferiority complex of American evangeli-
cals is used by the Left to capture their schools, from
Biola College to Wheaton, from Westmont to
Gordon-Conwell. Even the six-day creationist
school, Christian Heritage College, sought and
gained accreditation. Jerry Falwell has hired a law-
yer to force the accreditors to accredit his Liberty
Baptist. College. In short, the fundamenialists simply
will not learn. They seek certification from those same
elitist groups that they say are undermining Western
civilization. The very same people who the fundamentalists
regard as followers of Satan have set up the accreditation sys-
tem, and the fundamentalist leaders have rushed to submit
themselves to them in order to get their certificates of academic
acceptability.
