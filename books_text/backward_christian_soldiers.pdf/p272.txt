258 BACKWARD, CHRISTIAN SOLDIERS?

makes itself felt when the bankruptcy of the existing
establishment becomes obvious, when the State can
no longer supply the vote-getting special privileges
and funds.

The despair which sets in after years of frustrating
losses is natural. It must be resisted. Frustration is
basic to reconstruction. The seeming imperviousness of
the existing social and political order is overwhelm-
ing at times. But Gandhi's experience in India
should remind us that a lifetime of seeming futility
was rewarded with success, at least in the sense that
Gandhi achieved his stated political goal, namely,
independence from Britain. He ran very lean. Actu-
ally, he walked very lean, His march to the sea, his
two fasts almost unto death, and his other public
relations coups made him a formidable opponent of
the entrenched ruling class.

VOLUNTARISM The strength of the non-statist
groups, above all, is the commitment of their sup-
porters to the cause. These people are willing to take
their hard-earned money, and send it to a ministry
they approve of. This is not characteristic of their
opposition. They have real reserves—reserves of
dedication, commitment, and the habit of regular
financial sacrifice. The supporters are willing to take
astand, More than this: they are willing to finance a
stand.

These groups stay small. They get their message
out “by hook or crook,” but seldom with support
from the established intellectual and _ religious
opinion-makers. But the real opinion-makers are not
