284 BACKWARD, CHRISTIAN SOLDIERS?

homosexuality, 52
hospital, 743
humanism
bankruptcy, .20
church vs., xi

commander of, 136
common enemy, 136
control by, 103
definition, 272
funding, 256
giant institutions,
history, 126
pessimism, 227
public education &, 259
time, 227

victory of, x

vs. Christian schools, 148
weakness, 17 57
worldwide, 267
husband, 153

224

ideology,
illiteracy, 156, 157
illiterates, 174

image of God, 68
“impossibility thinking” 7
impotence, 7

Inchon, 100 (see Pusan)
India, 132, 171

inflation, 37

inner city schools, 80
Ireland, 127°

irrelevance, 79
irresponsibility, 7

Islam, 30

162, 165

James t, 230

Japan, 99-100
Jeremiah, 12
Jerusalem, 102

Joab, 7

job, 125

Jonah, 43

judges, U8, 119, 123
judgment, 12, 14, 43, 54
judgment day, &, 125, 136
judgments, 67

juries, 183

Justinian, 128

Keynesianism, 2, 73, 97
Kik, Jf. M., 79
kingdom, 18
kingdom
Christ vs. Satan, 145
comprehensive, 6

institutional church, £9
internal, 17 19

keys of, 19
spaceship, 243

this world, #4

King James Bible, 2

King, Martin L., 219,
220
kings, 43

“Kings X,” 13, 41, 70f
knowledge, 174
Knox, John, 2
Korea, 100
Kriananda, Swami,
Kuhn, Thomas, 166

I70
