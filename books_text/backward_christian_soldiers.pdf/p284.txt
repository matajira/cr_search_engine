270 SACKWARD, CHRISTIAN SOLDIERS?

Ladd, is fost-tribulationist, but argues that there are
only two dispensations, Old and New. This was the
common premillennial view from the early church
until the 1820's.

The amillennialist argues that the millennium is
totally spiritual in nature, not external, and refers to
the church age. Thus, there will never be an earthly
victory for Christians before the return of Jesus at the
final judgment. The Dutch Calvinist tradition, the
Lutheran tradition, and the Church of Christ (funda-
mentalist) tradition are all amillennial in outlook.

The posimillennialist argues that Jesus will come
in final judgment gffer a long era of peace— peace
that is the product of the universal domination of
Christians and Christian institutions across the face
of the earth. John Calvin was usually “post,” but
sometimes “a.” Historically, the Puritans were the
most influential postmillennialists, especially the
New England Puritans of the first generation,
1630-60. Jonathan Edwards was also postmillennial,
as were many of his followers. There was a strong
postmillennial undercurrent during the years pre-
ceding the American Revolution, The Presbyter-
ians, North and South, were postmillennial—the
Southern church until the South lost the Civil War
(War Between the States) in 1865; the Northern
church right up until the First World War. Human-
ists in the churches secularized the postmillennial vi-
sion, from 1830 onward, and ever since the era of the
Social Gospel (1870's), fundamentalists have asserted
repeatedly that postmillennialism means theological
liberalism. But it wasn’t liberal before the Arminian
