BACKWARD, CHRISTIAN SOLDIERS? &

But how many retreatist sermons have been
preached concerning the solely spiritual, exclusively
internal realm of Ghrist’s kingdom, as a supposedly
accurate explanation of this famous biblical text? I
shudder to think of the number: like the sands of the
sea shore.

CHRISTIAN SCHOLARSHIP Why should Chris-
tians be afraid to challenge the secular culture of to-
day? Has there ever been a culture less sure of its
own beliefs, less confident of its own powers, more
confused concerning its own destiny? The news-
papers, the entertainment media, and the univer-
sities can speak of little else but defeat and aliena-
tion, When a rare piece of positive drama appears, it
usually deals with some historical figure, like Patton
or Cromwell, The Christians have hardly heard of
Cromwell, the great Puritan leader and revolu-
tionary general. Secularists like the author Antonia
Fraser or the producers of “Cromwell” have to re-
mind modern Christians of their own heritage, so
forgetful have orthodox believers become. We let the
secularists do even our spiritual work for us, so
debilitating have the effects of emotional, pietistic
withdrawal been.

Why should critiques of modern secularism be left
to neo-orthodox scholars like the theologian Lang-
don Gilkey, whose book, Maker of Heaven and Earth (at
least in the first six chapters), was for years the best
book available on the implications of the doctrine of
God's creation—something Gilkey does not even
believe in its historic form? Why was it left to him to
