Christian Reconstruction,
66, 75-76, 142-50, 160,
250, 262-63, 267-68

Christian soldier, 69

church
authority, 103
conflict, 92-93
doctrine of, 16
institutional 17
judges, 8
perfection,
traditions, 144
weakness, 144

church government, 17-125

Churchill, Winston, 99

civil government, 122

Civil War, 2

Claudius, 36

“clean sweep,” 10

climate, 143

closet revolutionary, 165,
167-68

coins, 37

communications,
221

Communism,

Communists,

competence,
124, 251

computers
“buying ahead,” 239
church uses, 2/4
data bases, 215, 217
Kaypro II, 2/3
word processing,

79

35, 162,
30, H2

101, 247, 48
6 4 123,

214-16

iNOEX 261

conflict, 92, 93
conformity, 23
conservatism

beyond politics, 255
inconsistent, 245, 249-50
short-term, 252
Constantine, 39
continuity, 248, 250-51
controversy, 223
co-operation, 134, 135, 140
costs, 184, 187 200
counselling, 207

courts, 18¢

covenant, 251-52
crackpots, 55, 56
craftsman, 17

creation, 6, 64
creationism, 148

creed, 69

crime, 177

crisis, 57 155, 156, 266
criticism, 56

Cromwell, Oliver, 5, H0-f
cults, 35, 36
culture, 140
cursings, i2
Darwin, Charles, 67
day of judgment

see judgment day
deacons, 124, 178, 214
death, 229, 231
decentralization,
187, 188-89, 257
decisions, 173, 174

135, 181,
