184 BACKWARD, CHRISTIAN SOLDIERS?

JAMMED COURTS ‘The court system is becoming
vulnerable. Courts are tied up today in a series of
endless appeals. (Macklin Fleming, The Price of
Perfect Justice [New York: Basic Books, 1974]). It is
becoming very expensive to prosecute a case success-
fully these days, which is why defense lawyers are
getting reduced sentences or suspended sentences for
their clients through plea-bargaining (pleading
guilty to lesser crimes). The accused agree to plead
guilty to lesser charges rather than tie up the courts
in long cases to prove that the accused committed a
major crime. So far, Christian pastors and Christian
school headmasters have not been wilting to play this
plea-bargaining game. Therefore, it will tie up more
of the State’s economic resources if we stand firm. If
we do not capitulate, but force the prosecutors to
prove every point in court, we can make it very ex-
pensive for the civil government to prosecute hun-
dreds of schools. If we can find a way to reduce our costs
of defense, simultaneously increasing the costs of prosecu-
tion, we can make the State think twice about initiat-
ing new suits against us. How can we do it?

The best way to get most things accomplished is to
persuade a skilled worker that he has both principle
and a profit potential on his side, Show him how to
do well by doing good.

Highly skilled lawyers need good incomes to lure
them away from the more lucrative ways to practice
law. New lawyers are becoming a glut on the
market; they will practice for less. money. Is there a
way to enlist the services of skilled lawyers for lots of
money, pay them once, and then use their skills to
