1994, NOT 1948 57

time yet. Others are riding high all over the world.
They will ride no higher than the economy. They
will ride no higher than their ability to “deliver the
goods.” Socialism, regulation, and inflation do not
deliver the goods. They deliver only the “bads.” The
public will catch on only after a crisis, but the public
will catch on.

This is why we need a remnant. We need a school
of the prophets. We need men and women who
know, in advance, that a crisis is coming. They also
need to know why the crisis is coming, so that once it
hits, they will be able with confidence to explain in
retrospect why it came, and why certain concrete
steps must be taken to see to it that such a crisis does
not come again,

It is insufficient to stand on a streetcorner with a
big sign that reads, “The End is Near.” This familiar
figure of the cartoonists is all around us: in every
church that preaches the imminent return of Christ,
in every humanist study group that teaches that
nuclear war will end all life on earth, and in every
government planning bureau that operates in terms
of an economic philosophy which says that deflation
will destroy civilization as we have known it. The
end of the humanists’ world may well be near. I
believe it is. However, this is not the same thing as
saying that the end of the world is near, unless we
say that humanism is the highest and final stage of
human history, or the worst and final stage of human.
history. Neither position is true. What we should say
is that Aumanism is Satan's most effective imitation of
Christian culture—a perverse mirror image—but that
