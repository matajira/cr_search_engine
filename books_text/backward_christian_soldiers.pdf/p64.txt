Part il: THE ENEMY

6

1984, NOT 1948

 

“There is little likelihood that our perspective
will be taken seriously until this culture works
out its damnation without any fear or
trembting.”

 

What’s wrong with our churches today? A lot. But
if anything is wrong with the evangelical, officially
conservative churches, it is this: the members think
that the way to restore Christian culture is to return
to 1948. At the very latest to 1956. That would be as
close to “heaven on earth” as any church member
could dare to hope for.

Because of the partial isolation of most of our
churches from the grim reality of culture in the
1980's, they are reacting against the evils of 1968.
They want to see a return of patriotism, They want
dirty language off the prime-time T.V. shows, They
want television starlets to put on some underwear,
They dream of the day that Ozzie and Harriet will
