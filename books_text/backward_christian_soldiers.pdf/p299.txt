Kuyper, Abraham,
Ti, 76,77

labor, 15

labor unions, 86
LaHaye, Tim, 23
Lamech, 230

language, 35

law, 30, 128

laws, 68

Jaw school, xi

Lawyer Delay, 186
lawyers, xii, 119, 181-82,
184-85

laymen, J6, 20, 21, 186
leaven, x

legacy, 230, 232, 257
Legal Services Corp., 256
lemmings, 74

Lenin, 252

levers, 223

Lewis, C.8., 79
liberal, 182

liberals, 174-75
liberalism, 9, G1, 62, 73; 96
liberalism

inconsistent, 249-50
statism &, 254
victory, 250
liberation theology,
library, 242
lifeboats, 10
Hifejackets, 20
Lindsey, Hal, xi, 107 231
literacy, 127 152, 153

73, 274

INDEX 285

Luther, Martin, 2, 12% 259
Lutherans, 6
Lloyd-George, 76, 77
Lioyd-Jones, Martin
money &, 72, 73
pessimism, 74, 75, 78
reform, 7/
status quo, 72
surrender &, 70
vs, conservatism, 71, 72

MacArthur, Douglas, 33,
100-1, 10% 14, 147

mailing lists, 199-203

management, 33, 172, I75f

Manchester, Wiliam, Of

Maranatha, xi, 150

marathon, 237

Marines, i0i

market
see free market

marketing, 34

Martin, Albert, 205

Marx, Karl, J61, 245,
246, 252

Marxism, 73, 233

meaning, 228

medicine, 143

Mendel, Gregor, 242

Methuselah, 230

Middle Ages, 2, 128, 129,
131, 240

mind, 66, 68

minimum wage laws,
L677
