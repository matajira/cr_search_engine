ESCHATOLOGIES OF SHPWRECK 21

CONCLUSION So what we find in the twentieth
century is a twofold expansion of power, first by the
defenders of Protestant sacerdotalism, and second
by the secular State. The State needs pastors who
preach a theology of defeat. It keeps the laymen
quiet, in an era in which Christian laymen are the
most significant potential threat to the unwarranted
expansion of State power.
