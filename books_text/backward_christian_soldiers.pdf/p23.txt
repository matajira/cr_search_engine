2

IMPENDING JUDGMENT

 

“How long do we expect God to withhold His
wrath, if by crushing the humanists who pro-
mote mass abortion... He might spare the
lives of literally millions of. innocents?”

 

In recent years, a sense of foreboding has begun to
overtake the West. The optimism of the “can-do”
pragmatic liberals of the Kennedy years died in the
jungles of Vietnam, What has replaced the older op-
timism is a kind of secularized version of “eat, drink,
and be merry, for tomorrow we die.” Can-do liberal-
ism couldn’t, and its spiritual heirs have just about
gone through their inheritance. (Liberalism taxes all
kinds of capital, not just financial capital, and the
result is national decapitalization.}

There are several ways men have accommodated
themselves to this sense of impending doom. One
way is to deny the darkness. Men point to statistical
indicators of national economic growth, or some
