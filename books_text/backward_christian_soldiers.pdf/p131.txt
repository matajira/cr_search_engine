13

PROGRESSIVE RESPONSIBILITY

 

*,.. we are the recipients of a law structure
which is the proper foundation for ali our
personal decisions.”

 

The apostle Paul recognized the necessity of
Christians exercising leadership, first within the
Christian community, and later in the very processes
of the cosmos. When the sin-plagued Corinthian
church faced a major disciplinary problem, Paul
wrote to them that they should handle it themselves,
They should not appeal to a secular law court, he
said, implying that since the court would not be gov-
erned by the standards of biblical law, it would be a
poor testimony to seek judgment there. It was wrong
in principle because it would appear to sanction the
validity of Satan’s rule over the church. “Do ye not
know that the saints shall judge the world? and if the
world shail be judged by you, are ye unworthy to
judge the smallest matters” (I Cor. 6:2)? Not only
