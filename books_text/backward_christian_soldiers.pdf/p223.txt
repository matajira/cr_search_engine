THE TAPE MINISTRY 209

in order to make the products look professional and
to save you time. Cheap is seldom best. I am tired of
seeing those who do have the truth at their disposal
continue to proclaim this truth in forms that are an
embarrassment, which look as though total in-
competents had produced them. It is not glorifying
to God. Christians need more pride in their work-
manship. Quality counts.

When it comes to audio tapes, two pieces of equip-
ment are vital. First, a.high quality microphone. This
will do more to improve the quality of the product
than any other piece of equipment. Good quality
units are sold by Shure, Sony, Sennheiser, Electro-
Voice, and other companies. Expect to pay $80 to
$250 for a good mike. Second, get a good cassette
tape deck. Sony, Aiwa, Technics, Akai, Superscope,
and many other companies produce suitable units
for $250 to $600. Pick up a copy of High Fidelity or
another magazine aimed at the stereo equipment
niarket. Look at the ads. You will get.a feel for what
is available. Any unit with Dolby circuitry (especially
the new Dolby C), with wow and flutter under
.08%, and push-button controls, is fine. Auto-
reverse is also nice. But if you use auto-reverse, use
tapes that don’t have plastic lead-in sections at each
end.

One other item is useful: a graphic equalizer. It can be
“tuned” to filter out low rumbles, such as the church’s
heater or air conditioner. This makes it easier to play
in an automobile, too. The mid-range frequencies,
where the human voice operates, are “punched out”
far more effectively. Expect to pay $200.
