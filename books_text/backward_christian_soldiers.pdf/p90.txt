76 GACKWARD, CHRISTIAN SOLDIERS?

had to fail. They must fail. The Apocalypse
alone can cure the world’s ills. Man even at his
best, even as a Christian, can never do so. You
can never make people Christian by Acts of
Parliament. You can never christianize society.
It is folly to attempt to do so, 1 would even sug-
gest that it is heresy to do so (p. 108).

Here is his constant theme: men are sinful; the
world is fallen; therefore, perfection is impossible.
As he told Carl Henry; the cultural mandate was
given to Adam before the Fall; we live as in the days
of Noah. What he conveniently neglects—and he
could not conceivably be ignorant of the passage —~ is
that God gave the same cultural mandate to Noah,
after the Flood (Gen. 9:1-7), It should be obvious
why Dr. Lloyd-Jones conveniently neglects this
passage: it spells the doom of his entire misinterpretation of
the Bible. We cannot escape the moral burden of the
cultural mandate —what I have called the dominion
covenant—just because of man’s ethical rebellion.
We are the sons of Noah,

Christian reconstruction is supposedly impossible.
However, we can work as Christians for reform. He
calls statist wealth-redistribution “legitimate reform.”
He then appeals to the tradition of Abraham
Kuyper. I find his conclusions most illuminating,
especially in regard to the similarities drawn by
Lloyd-Jones between the political careers of the
Netherlands’ Kuyper and Britain’s first radical
Prime Minister, Lloyd-George:

Nevertheless, government and law and order
