252 BACKWARD, CHRISTIAN SOLDIERS?

the kind of time horizon God has in mind for His
covenanted community, including the sons and
daughters of today’s faithful remnant. The covenant
stretches actoss generations. This is what gives signifi-
cance to hile triumphs; they can accumulate over
time to produce extensive results. The compounding
effect requires time, but if you have enough time—
and if your time horizon encompasses all the time
you have—then you do not need large percentage in-
creases each year in order to achieve your goals. A
little each year goes a long way, if you have enough
time.

The radicals have grasped this. Marx was con-
vinced that there was a march of progress over time,
that the proletarians would inevitably win, and
therefore it is worth sacrificing a lifetime to produce
the pamphlets and books that would strengthen the
proletariat over its long-term struggles. Marx had a
vision of history which stretched back to the hypo-
thetical ancient communist agricultural com-
munities. Fime was important to him—he thought
the timetable for revolution was much shorter than it
was— but time was not a threat to him. He could set-
tle for minor victories. So could Lenin. Tivo steps for-
ward and one step back ig a perfectly reasonable
strategy, if you think you have time on your side.

The conservatives have adopted a philosophy of
continuity in some areas of their lives—family,
business, church growth, etc.—but not in political
affairs. As they lose faith in time, their commitment
to continuity will be reduced in whatever areas of ul-
timate concern that they have. Until they become
