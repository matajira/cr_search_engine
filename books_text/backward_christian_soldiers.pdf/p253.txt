HOW MUGH TIME? 239

could be replaced with far better rails than the
British possessed, and at a far lower expenditure of
capital. Throughout the intervening years, the
builders had the extra capital to use for other pur-
poses. They “built cheap,” and let technology come
up with the better product later on. The same thing
has happened in our era with computer technology.
It is not sensible to “buy ahead” when you buy a
computer; increased needs in the future should be
purchased in the future, when prices will be far
Jower and performance will be greater.

But what about the long-run builder? Is he
foolish? It depends on whether or not we really have
a lot of time remaining to us— collectively, as a race;
nationally; or geographically, where we are building
our structures. Maybe there were Canaanites who
went to considerable expense just prior to the exodus
to build family estates that would last 500 years. Not
too smart, in retrospect; they were just increasing
the capital value of the Hebrews’ property. If we ex-
pend huge quantities of long-term capital, and dis-
cover that it is blown away by short-term forces of
history, then we have wasted our capital. However,
if we blaw away our capital on short-run projects,
only to discover that we have run out of money a
long time before the end appears, then we have also
wasted our resources. It is imperative, then that we
make accurate assessments concerning the time re-
maining to us.

It should be obvious that in the twentieth century,
very few Christian groups think that we have a lot of
time remaining. This has drastically influenced the
