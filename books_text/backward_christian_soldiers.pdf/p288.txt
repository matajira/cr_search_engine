274 BACKWARD, CHRISTIAN SOLDIERS?

back door by using such terms as ‘biblical
principles,” or “biblical ethics,” or “the moral stand-
ards of the Bible.” But if the principles are morally
binding, they must be regarded as laws.

If there is no neutrality anywhere in the
universe, and biblical standards apply universally,
then all academic disciplines of the modern univer-
sity are to be governed by biblical revelation and
biblical law. On this point, see the book which I
edited, Foundations of Christian Scholarship (Ross
House Books, P.O. Box 67, Vallecito, CA 95251).

LIBERATION THEOLOGY Popular among liberals
in all denominations since its development by a Har-
vard professor in the late 1950’s, liberation theology
adopts the language of socialist wealth redistribu-
tion, Marxist revolutionary rhetoric, and out-of-
context Bible quotes. It has become extremely
popular in Latin America among radical Roman
Catholic clerics. Not all of its adherents are openly
revolutionary. Some of them are vague. Others are
chicken. Baptist theologian Ronald Sider is probably
best classified as a non-revolutionary liberation theo-
logian (as of early 1984). His book, Rich Christians in
an Age of Hunger, was co-published by the liberal
Roman Catholic Paulist Press and the neo-
evangelical InterVarsity Press. The best refutation of
liberation theology by a Protestant (or anyone else,
for that matter), is David Chilton’s Productive Chris-
tians in an Age of Guilt-Manipulators (2nd ed., Institute
for Christian Economics, 1982). It is also the best in-
troduction to Christian economics,
