‘THE THREE LEGS OF CHRISTIAN RECONSTRUCTIONS STOOL 145

trial in 1925 and after the failure of Prohibition. The
liberal denominations have lost their influence
markedly since the 1960's. Roman Catholicism has
been rent asunder by a series of extraordinary
changes that began theologically in the early 1950's
(higher criticism”) and institutionally with Pope
John XXII’s call for church reform.

But another series of events have begun to rally
Christians, luring them back into the arenas of cul-
tural and political conflict. Thousands of fundamen-
talists have been intellectually encouraged by the
publication of anti-evolutionary books and materials
since the early 1960’s, The legalization of abortion on
demand by the U.S. Supreme Court in Ree » Wade
(1973) has given Christians of many denominational
traditions a cause, and groups are cooperating on an
ad hoc basis in order to bring an end to the slaughter
of the innocents. The cause of human life has tran-
scended theological disputes that once made co-
operation improbable.

As. Christians have begun to recognize the
religious impulse of modern humanism, they have
seen that there are battle lines drawn between the
kingdom of God and the kingdom of Satan —battle
lines that affect every area of life. The reigning phi-
losophy of neutrality has at last been challenged by
Christian leaders (as Marxists challenged it a cen-
tury ago and philosophical relativists challenged it
two generations ago), The implications of this new-
found, Bible-based presuppositionalism are becom-
ing clearer to a growing minority of thinking Chris-
tians. The intellectual compromises with humanism
