122 BACKWARD, CHRISTIAN SOLDIERS?

judge in terms of standards. An act is right or
wrong, acceptable or prohibited. If Christians are to
judge the angels, then they must do so within a
framework of morality designed by God and re-
vealed to man. The Corinthian Christians were to
stay out of pagan law courts precisely because there
is no neutral civil law. Civil law, like church law, is
governed by religious presuppositions concerning
morality. Religious civil law may be defended in
terms of a philosophy of universal neutralism, but
such an argument is itself intensely religious and un-
neutral, for there are irreconcilable conflicts between
biblical law’s grounding in God’s revelation and any
other law-order which is not grounded in His revela-
tion, The judgment of this world by the saints
testifies to the absence of neutral law and neutral
lawyers or judges. It is the saints who are the judges,
not the self-professed neutralists. There will be no
neutrality on the day of judgment. It will be the
universal rule of God’s law which prevails.

GOOD LAW, EXPERIENCED JUDGES The Corin-
thians were to seek out the most competent judges
within the membership of the church. They were to
elevate these men to the seat of authority. These
men, not pagan judges, were to be preferred by the
members in the settlement of disputes.

Was Paul forever closing the door to an expansion
of godly rule? He was not a defender of ecclesiocracy.
He acknowledged the civil ruler’s role as a minister of
God. So what did he have in mind? How could the
influence of the gospel be legitimately restricted to
