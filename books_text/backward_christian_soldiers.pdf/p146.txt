182 BACKWARD, CHRISTIAN SOLDIERS?

nevertheless succeeded in reshaping the history of
mankind. Their ignorance did not keep them from
outdistancing India, China, and the other ancient
civilizations by the end of the medieval period.
There was a cumulative effect of the vast number of
successive additions of family capital: agricultural,
technological, educational, and moral. Line upon
line, precept upon precept, a body of moral capital
was built up, and it produced a new civilization.

CONCLUSION However important theology may
be, it is the. application of that theology to specific in-
stances of daily living that makes the difference cul-
turally, Theology is not simply an affair of the educa-
tional specialists. Flourishing theology is always
practical theology. Theology has implications for
every sphere of human existence. It is basic to the
successful outworking of God’s dominion covenant
(Gen. 1:28) that people begin to apply the truths they
have-learned, especially in family affairs. If theology
is untranslated into the little things of life, then it is
truncated theology —cut off at the root. If the con-
struction of ever-more finely honed theological for-
mulations does not lead to altered family, church,
business, or school operations, then it is dead
theology. It was not the incredibly erudite debates of
late medieval scholasticism that built medieval cul-
ture. Indeed, these debates among the schoolmen
were the sign of the strangulation of medieval cul-
ture, the end of the line for the medieval world.
Theological scholarship, apart from concrete ap-
plications, is dead scholarship that leads nowhere. If
