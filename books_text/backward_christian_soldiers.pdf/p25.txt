IMPENDING JUDGMENT 11

Whole cultures disintegrated, and the rise of Bolshe-
vism and Nazism brought decades of additional deva-
station. We still live under the shadow of that great
war, Yet not everything was lost. Not all signs of prog-
ress ceased. Men forged ahead, especially in the area
of technology. But, on the other hand, there is the
hydrogen bomb, technology’s highly efficient threat to
the world of technology. And what if technology geis
the cost of producing such a weapon down to the level
where an Idi Amin can buy or steal one?

The prophets of doom and the prophets of contin-
uing progress can both look plausible for a while,
There will come a time in the life of any given nation
or culture, however, when the implications of its his-
torical development become sufficiently obvious, so
that the majority of men can see them. When civili-
zations fall, men wail the loss, but they recognize it.
Even when the fall of a civilization takes centuries,
as it did in Rome, the later citizens can look back
and recognize the world they have lost. We cannot
put off the day of cultural reckoning forever. For a
man heavily invested on margin in the U.S. stock
market in October of 1929, the day of accounting
could not be delayed any longer. What resulted from
that collapse (i.e., the depression which began be-
fore October 1929, but had not been obvious) was
understood as a disaster by those living in the 1930's.
The pessimists of the late 1920's were proven correct;
the optimists were proven bankrupt.

PROPHETIC PREACHING ‘The prophets of the Old
Testament believed that there is a fixed relationship
