12 BACKWARD, CHRISTIAN SOLDIERS?

between the moral character of a nation and the ex-
ternal blessings or cursings visited by God on that
nation. They believed in the reliability of biblical
law. They knew that if people continue to cheat their
neighbors, commit adultery, break up the family,
and defy all lawfully constituted authorities, the land
will be. brought under judgment. They had no
doubts in this regard. They recapitulated the teach-
ings of Deuteronomy 28:15-68, warning their listen-
ers that God’s laws cannot be violated with impunity
forever.

Twentieth-century preaching has neglected the
outline of Deuteronomy 28. We find few pastors who
are willing to: stick their necks out and warn con-
gregations that modern society faces the same sort of
judgment that faced ancient Israel. They are unwill-
ing to follow the logic. of the covenant, namely, that
similar sins result in similar judgments. While today’s
religious leaders are sometimes willing to speak of
the impending judgment of God on the lawless
members of society —a society from which all Chris~
tians will have been removed by a supernatural act
of God—they are seldom ready to preach as the
prophets did. They do not warn their listeners, ag
Jeremiah did, that they, too, are a part of contempor-
ary society, and that they, too, are’ not immunized
against God’s wrath. If men have relied on the con-
tinuing profitability of today’s economy, and the con-
tinuing functioning of today’s bureaucratic struc-
tures, then they have put their capital at risk. They
have rested and are resting on weak reeds.

Where is the warning being sounded? Where are
