CAPTURING THE ROBES 6

conservative, but not on politics and economics,
Michael Novak writes: “Would you have guessed
that, of all professors at all the Bible colleges, divinity
schools, and seminaries in all fifty states, only 17 per-
cent would call themselves Republicans (Q. 42)?
Meanwhile, 62 percent call themselves either Demo-
crats (30%) or independent, closer to Democrats
(32%). Only 7 percent are pure independents. These
figures help explain how 56 percent voted for
McGovern over Nixon (34%), 66 percent for Carter
over Ford (28%), and 52 percent for Carter over
Reagan (30%) and Anderson (11%)” (p. 102).

“Eighty percent think the competition between the
U.S. and U.S.S.R. is fundamentally a struggle in
power politics, only 20 percent fundamentally a
moral struggle (p. 103)... . Seventy percent think
U.S. multinational corporations hurt poor countries
in the ‘Third World, 30 percent think they help (Q,
104). Thirty percent think the U.S. treats Third
World couniries fairly, 70 percent unfairly (Q, 105)”
(p. 104).

Are these all theological liberals? While only 27
percent believe in the inerrancy of the Bible, 64 per-
cent claim it is infallible in matters of faith and
morals. Some 59 percent say they have had a born-
again experience. Half claim they experience a
special closeness to God daily (p. 105). These are
people, in other words, who for the most part would
be considered theological brothers by the editors of
Christianity Today, Eternity, Christian Life, and virtually
all of the non-denominational magazines of America.
Three quarters of these professors think the Moral
