96 BACKWARD, CHRISTIAN SOLDIERS?

is, of course, not a suddenly awakened ‘thirst for
education’ but the desire for restricting the supply for.
those positions and their monopolization by the
owner of educational certificates.” What parents
really want for their children is money, other things
being equal. They want a “white collar union card.”

But who. will pay the freight? Rick alumni, if the
school is an older one, or a very: prestigious ivy
League school. The Federal government, if the Ad-
ministration is willing (as too many of them are) to
compromise their morals and their theology, and beg
for other people’s confiscated tax money. Besides, all
they have to give up is the right to mention God or
the Bible in the buildings constructed with Federal
dollars. A small price to pay, if you're an ad-
ministrator. Better a new gym and no prayers, than
no gym and the right to mention the name of God.
First things first, you know.

The Christian colleges have another source of
funds; the denomination, They can continue to keep
tuition levels low if church officers will use part of the
money collected in God’s name and the moral re-
quirement of the tithe to finance the politically lib-
eral professors in their tenured safety. Then the
steady indoctrination of the students can continue,
all financed by parents and church members who do
not share either the political outlook or the tenured
safety of the college’s faculty members.

The denomination ought to tell the college to
charge full-cost tuitions. Why subsidize a long-term
investment made by middle class and upper middle
class parents for their children’s financial futures?
