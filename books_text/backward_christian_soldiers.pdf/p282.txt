268 BACKWARD, CHRISTIAN SOLDIERS?

the founders have held all four. The first person who
put this system together publicly was Rousas John
Rushdoony. He was my mentor during the 1960's,
and while I was working .on the specific field of eco-
nomics, he was developing the overall framework.
The first comprehensive introduction to the Chris-
tian Reconstruction position. was Rushdoony’s The
Institutes of Biblical Law (Craig Press, 1973), in which
three of my appendices appear. The easiest introduc-
tion to the position is my book, Unconditional Sur-
render: God's Program for Victory (2nd ed., Geneva
Divinity School Press, 1983).

DOOYEWEERDIANISM A philosophy pioneered by
the Dutch Calvinist legal philosopher, Herman
Dooyeweerd (DOUGH-yeh-veerd), in the mid-
twentieth century. His major work is A New Critique
of Theoretical Thought. He argued with considerable
erudition (and appalling verbiage) that there is no
neutrality in any philosophical system, All philoso~
phies and outlooks rely on what he called pre-
theoretical assumptions about man, nature, law, and
God. His shorter book, Zn the Twilight of Western
Thought, presents a 3-part outline of Western philos-
ophy. He categorically rejected the idea that biblical
revelation can provide either the categories of philos-
ophy or the content of Christian philosophy. His sys-
tem therefore lends itself to various “common
ground” appeals to the universally logical mind of
man. Many of his younger followers have turned to
some variation of socialism, most commonly medi-
eval guild socialism, as an answer to the perceived
