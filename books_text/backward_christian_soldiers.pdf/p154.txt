140 BACKWARD, CHRISTIAN SOLDIERS?

for a broad market.

We have a broad position to defend—a position
which must be separated from the dying culture of
secular humanism, Yet at the same time, we have.
diverse strands in this Christian tradition, unique
elements provided by many Christian groups.. We
are offering a dying culture a-rival, comprehensive
culture, meaning a culture broad enough to conquer
every nook and cranny on earth. No single ecclesias-
tical tradition can provide everything needed to
replace the humanist system which is disintegrating
before our eyes.

Therefore, the Christian school, and the econom-
ics of the marketplace, provide the goal and the
means of co-operating. Christians cannot afford to
be too exclusive today. We simply don’t have the
funds to be hyper-exclusive. We also don’t have the
bodies. Like the chaplaincy, we have to put up with
diverse ecclesiastical traditions precisely because we
are at war, we expect to win, and we cannot ignore
help where it is offered,

Now, for those who do not think they are at war,
who would not expect to win if they did understand it,
and who cannot distinguish a Christian day school
from a government day school, ail of this may appear
silly. After all, they think they have to defend their
total institutional purity on questions of dress, drink,
prayer, architecture, flip-around collars, or whatever.
That’s what really counts before God. And if it means
shoving the next generation into the training camps of
secular humanism, well, then, that’s what’s necessary:
After all, not.many people can finance both their ec-
