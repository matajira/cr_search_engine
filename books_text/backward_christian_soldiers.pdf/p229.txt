THE COMPUTER 215

programs— the data-handling of existing “toy” com-
puters will escalate rapidly. They are “toys” only
because the programs are poor. Electronically, they
represent a revolution, today. Now that IBM,
Hewlett-Packard, and other computer giants have
entered the field once dominated by Radio Shack
and Apple, the market will expand, which means
that software will improve dramatically.

WORD PROCESSING The $1,000 typewriter is
really obsolete. By mid-1983, it was possible to buy a
very useful portable—well, at 20 to 25 pounds,
moveable—computer for under $1,800. These units
include a word processing program, a mailing list
program, and other software. Couple one of these
with a fast printer like the Epson FX-80 or FX-100
{in the $700 price range) and a letter-quality printer
{anywhere from $500 to $2,000), and you have a
powerful tool. Even less expensive systems are now
available: under $700 for everything. But you get
what you pay for. To spend less than $2,500 in 1984
is probably unwise. Some of the computers that are
quite powerful are the Kaypro II, the Zorba, and the
Micro Decision (Morrow Designs). They have units
in the $1,500 range and somewhat more powerful
models in the $2,800 range, plus the cost of the
printers. You can put sermons, outlines, quotations,
Bible references, information from huge, commer-
cially inexpensive data storage banks (UPI, Wall
Street Journal, and library information), and
anything else on a little plastic disk. You can recall
these data by topic, or date, or writer. You can for-
