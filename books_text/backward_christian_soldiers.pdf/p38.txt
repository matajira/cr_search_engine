24 BACKWARD, CHRISTIAN SOLDIERS?

The message of the conference was straightfor-
ward; it is the Christian’s responsibility to vote, to
vote in terms of biblical principle, and to get other
Christians to vote. There can be no legal system that
is not at bottom a system of morality, the speakers
repeated again and again. Furthermore, every sys-
tem of morality is at bottom a religion. It says “no” to
some actions, while allowing others. It has a concept
of right and wrong. Therefore, everyone concluded,
it is proper for Christians to get active in politics. It
is our legal right and our moral, meaning religious,
duty,

You would think that this was conventional
enough, but it is not conventional at all in the Chris-
tian world of the twentieth century. So thoroughly
secularized has Christian thinking become, that the
majority of Christians in the United States still ap-
pear to believe that there is neutrality in the uni-
verse, a kind of cultural and social “no man’s land”
between God and Satan, and that the various law
structures of this neutral world of discourse are all
acceptable to God. All except one, of course: Old
Testament law. That is unthinkable, says the mod-
ern Christian. God will accept any legal framework
except Old Testament law. Apparently He got sick of
it 2000 years ago.

So when the crowd heard what the preachers and
electronic media leaders were saying, they must
have booed, or groaned, or walked out, right? After
all, here were these men, abandoning the political
and intellectual premises of three generations of
Protestant pietism, right before the eyes of the
