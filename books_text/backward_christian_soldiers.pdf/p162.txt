148 BACKWARD, CHRISTIAN SOLDIERS?

make much difference in the front lines.

The confidence provided to modern fundamen-
talism since 1960 by the various creationist research
groups has been crucial. The “shame” of the Scopes’
debacle has been cleansed away. The evolutionists
are on the run intellectually today, not the crea-
tionists. The psychology has shifted from retreat to
victory.

BAPTIST DAY SCHOOLS The advent of the Ac-
celerated Christian Education (A.C.E.) program
and the Beka Books of the Pensacola Christian
School have produced thousands of new independ-
ent Christian day schools and church schools since
1965. These schools are molding the minds of the
next generation of Christian leaders. They are the
knife at the throat of the monopolistic humanist
schools, and the humanists know it. The one estab-
lished church in the U.S., the public school system,
is facing the defection of millions of students.

When a parent pulls his child out of a public school
and keeps him out éhrough high school, he has broken
institutionally and psychologically with the statist
order. The church that sets up such a school has
broadened its commitment to social change. It has
also gained an institution which is worth defending,
and the humanists are increasingly ready to attack.
Thus, the psychology of conformism and capitulation
is frequently changed. Pastors who were previously
unwilling to make a stand against humanism’s myth
of neutrality now must make a break. Their schools
need a reason to exist. The war against humanism is
