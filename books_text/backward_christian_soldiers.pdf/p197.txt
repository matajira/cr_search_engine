BRUSH-FIRE WARS 183

There are no more easily won cases, The enemy is
well-armed in this battle. Our people are poorly
armed, except when the very best, most prominent
defense attorneys have been hired. (The most prom-
inent of these attorneys are not always the best,
especially when their tiny organizations are fighting
hundreds of cases. Never hire an attorney to fight
your case if he has over five potential trial cases in
progress.) There are few of these men to go around.
They ask and receive high fees, too, or are forced to
raise the money from hard-pressed donors.

Yet the enemy has problems, too. First, the
religious traditions of the United States stand
against thern. So do the legal traditions. Second,
there are only so many top-flight prosecuting at-
torneys. The government lawyers at the local level
are not usually “the best and the brightest.” If they
were really good, they would be in private practice
making three times the pay. Third, the State still
faces the threat of jury trials, and these juries are
sometimes filled with people who are sick and tired
of being kicked around by bureaucrats, So the war is
not over. Christians and independent school sup-
porters have the principles on their side, and the civil
government has both the initiative and the money.

What we need is to take advantage of our greatest
strength: numbers. We have many schools and
churches that need their independence. If we could
get the State to commit more men and resources to
the fight, we would find that the quality of our op-
ponents would drop. Their best legal talent would be
tied up in other court battles.
