OPTIMISTIC CORPSES © 235

ual or financial capital. If the compounding process
begins, and continues over a long enough period,
the whole world can be influenced. The fact that we
have short lifetimes does not mean that we cannot
make long-run investments. In fact, this under-
standing encourages us to make these sacrifices to-
day. We plant cultural seeds for the church’s future.

We derive our meaning from God. Our work on
earth survives, if it is good work. It survives in
heaven (I Cor, 3:41-15), and it also survives on earth.
The Christian who is an optimistic future corpse
does not worry that his work will go the way of the
evolutionist’s work, to be overcome by impersonal
entropy. He does not worry about leaving behind a
life’s work that will be swallowed up in the horrors of
the seven-year Tribulation. He looks to God in faith,
knowing that Christ will deliver up a completed
kingdom to the Father (I Cor. 15:28).

We can plant a tree, and if it is cared for by those
who follow us, it will bear fruit. We can plant today,
knowing that there is sufficient time, this side of the
millennium, for it to mature. It could be cut down in
a war, as any good work can be at any time, but we
know it will not for certain be cut down in absolute
destruction during a Great Tribulation, That
Tribulation took place when Jerusalem fell to the
Roman legions in 69-70 a.p. (Luke 21:20-24). The
work of any godly man has a possibility of survival
into the distant future. The rate of growth need not
be large under such circumstances. Little by little,
line upon line, his capital investment can prosper
year by year. His spiritual successors can see to its
