SHEPHERDS AND SHEEP 139.

sessed is authority to be used. We can change our
Christian schools. We cannot change the govern-
ment schools, so there is no hope there. Protestants
who wouldn’t think of spending time and effort to
convert Notre Dame University to Protestantism
spend lots of time trying to mold a far more secular,
government-financed public school system. It makes
no sense. At least there is hope for Christian schools.
We must labor where there is hope.

CURRICULA Here is where the fight must be
made. Here is where our dollars must go. Here is
where little is being done. The minds who produce
the school curricula for the next generation of Chris-
tian youth will provide what has been missing for so
long: an integrated program, an_ intellectual
strategy. But to meet a market, these materials must
reflect the Christian contributions of many ecclesias-
tical traditions. We do not have enough potential
buyers today to finance a narrow, denominational
type of curriculum. Not those of us, at least, who are
in churches that did not long ago abdicate by allow-
ing secular humanists to write the denominational
school textbooks.

The broad Christian tradition, which is our alter-
native to humanism, will provide much material for
batiling the common enemy. The crying need for
good, conservative, accurate, principled, readable,
and literate curricula will cover many minor
disagreements. If love covers a multitude of sins,
think what the need for Christian textbooks will
cover. And to get the costs down, we have to write
