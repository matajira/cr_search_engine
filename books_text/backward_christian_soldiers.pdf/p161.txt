THE THREE LEGS OF CHRISTIAN RECONSTRUCTION'S STOOL 147

ship, has overwhelmingly been Presbyterian in this
century. The Lutherans are in second place. This
has been true since the 1500’s. J. Gresham Machen
was fundamentalism’s spokesman from 1923 until his
death in early 1937, defending biblical inerrancy and
attacking theological liberalism, yet Machen was not
a fundamentalist. He was a Presbyterian. Even the
eloquent William Jennings Bryan was a Presbyter-
ian, although more of a fundamentalist than
Machen was. Presbyterian Oswald T. Allis’ defense
of merrancy in God Spake By Moses was as respected
by fundamentalist educators as his critique of dis-
pensationalism was rejected (Prophecy and the Church).
Francis Schaeffer’s influence is obvious—another
Calvinistic Presbyterian. R. J. Rushdoony’s defense
of Christian education (Zntellectual Schizophrenia) and
his critique of humanist education (The Messianic
Character of American Education) have become
“testaments” of the independent Christian education
movement in the U.S. His testimony in court trial
after court trial as the expert witness for the defense
of Christian schools has made him prominent within
Baptist and fundamentalist circles. Some Arminian
Baptists have even complained publicly about his
prominence, but they cannot find anyone with his
education and eloquence to fill the gap. They have to
put up with him because they have no alternative. In
a war, you need to be concerned about how well your
partner shoots, not what he believes about the
predestined guidance for his bullets.

In short, Presbyterians supply the ammo. They
shoot, too, but there just aren’t enough of them to
