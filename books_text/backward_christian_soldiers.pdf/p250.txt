236 BACKWARD, CHRISTIAN SOLDIERS?

care and maintenance.

Those who view God’s history as a giant scythe
which will cut down all the works of Christians on
the. final day (or rapture) except for internal,
“spiritual” works, cannot plant cultural seeds with
the same confidence, and therefore the same en-
thusiasm, as those who view themselves. as future
corpses whose work is long-term capital that can sur-
vive, On the day of judgment, the garden produced
at last by Christian discipline and Christian capital
will not experience a silent spring. It will be a thing
of beauty, delivered to the Father by the Son as His
fulfilment of the dominion covenant (Gen. 1:28). His
people will share in His pride of workmanship. As
His stewards, they will have a part in its historical
fruitfulness. That fruitfulness will extend into the
New Heavens and the New Earth.
