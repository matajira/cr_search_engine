48 BACKWARD, CHRISTIAN SOLDIERS?

They must make restitution.

In every area of life, we must reconstruct: in
education, in medicine, in agriculture, in econom-
ics, in our occupations, in politics, in law enforce-
ment, in family relationships, in church life, in the
arts and sciences . . . in everything.

God has told us what we must accomplish, as in-
dividuals and as a nation, in order to fulfill our do-
minion assignment. There is no escape from this
assignment, from Adam’s day to the present. There
can be personal success or failure in carrying it out,
but no escape. God holds us responsible, as in-
dividuals and as a society. We have our marching
orders from ‘a God who has promised victory to His
people, in time and on earth. Victory can be achieved
only in terms of God’s righteousness, God's sacrifice at
Calvary, and God's standards of righteousness for every realm
of life.

Satan cannot win. Why not? Because he has
denied God’s sovereignty and disobeyed God’s law.
But Moses was told explicitly, God’s blessings come
only from obedience. Satan will not win because he has
abandoned God's tool of dominion, biblical law.

It is time for Christians to stop giving Satan credit
for more than he is worth. Christians must stop wor-
rying about Satan’s power, and start working to
undermine his kingdom. Contrary to a best-selling
paperback book of the 1970’s, Satan is not alive and
well on planet earth—alive, yes, but not well. His
troops are no better than their commander’s
strategy, and that strategy is flawed. They have been
winning only because of the rival army’s field-grade
