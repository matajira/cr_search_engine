MUMANISW'S CHAPLAINS 75

world. The end time is going to be like the time
of the Flood. The condition of the modern
world proves that what we must preach more
than ever is “Escape from the wrath to come!”
The situation is critical. I believe the Christian
people—but not the Ghurch—should get in-
volyed in politics and in social affairs. The
kingdom task of the church is to save men from
the wrath to come by bringing them to Christ.
This is what I believe and emphasize. The
main function of politics, culture, and all these
things is to restrain evil. They can never do an
ultimately positive work. Surely the history of
the world demonstrates that. You can never
Christianize the world.

This tends to be the answer of the older fun-
damentalism: escape from the wrath to come, forget
about Christian reconstruction. But what does he
expect Christian people to do? Of course, it is not
normally the task of the institutional church to get
into the political arena. But that isn’t the question.
What about Christian men and women in voluntary
political or other organizations? What can they ex-
pect to accomplish? Hardly anything, says the good
doctor, They are in a losing battle. As he wrote in his
1975 essay:

We are now back to the New Testament posi-
tion; we are like New Testament Christians.
‘The world can never be reformed. Never! That
is absolutely certain. A Christian State is im-
possible. All the experiments have failed. They
