HUMANISM'S CHAPLAINS 73

Ghristians to make money. Sadly, he refuses to spec-
ulate concerning the reasons for this tendency to ex-
ist (and exist it does: Deut. 8, 28). But men who
make money don’t appreciate being forced by State
bureaucrats to contribute money to the care and
maintenance of statist power, i.c., welfare programs
used for the purchase of votes by politicians, what
Rushdoony has called the politics of guilt and pity.
This, the good doctor argues, is an evil attitude on
the part of Christians. They don’t like to share their
wealth with the State. The State, by implication, has
a perfect right to the wealth of hard-working, thrifty,
risk-taking Christians who have prospered finan-
cially. This is called “the legitimate rights of people.”
It is also called Keynesianism, interventionism, stat-
ism, the “new economics,” political liberalism, the
New Deal, the welfare State, the corporate State, and
in the 1930’s was known as fascism. I: is theft with a
ballot box instead of a gun. It is the Christian liberal’s
rewriting of the sixth commandment: “Thou shalt not
steal, except by majority vote.” It is the economics of
most voters in Grand Rapids, Toronto, Wheaton,
Edinburgh, London, and especially Amsterdam.

He recognizes that Anglo-Saxon Protestant Non-
conformists— those opposed to an established State
church — have traditionally been political reformists.
These people were defenders of nineteenth-century
political liberalism: political equality, but with eco-
nomic freedom. He also recognizes that those de-
fending the idea of the cultural mandate (Gen. 1:28)
tend to be political reformers, as do the Marxist
“Hiberation” theologians. In his interview in Christianity
