260 BACKWARD, CHRISTIAN SOLDIERS?

the development of a movement in its early stages,
when it is learning to cope with the realities of life,
but sparse beginnings enable it to deal with growth
and success later on, when its principles become
more widely accepted.

GRASS-ROOTS ORGANIZATIONS “Organizations”:
The word is plural, not singular. The idea of
establishing a single grass-roots organization is pre-
posterous. It would be mowed down by the wide-
blade power mowers of the opposition as soon as its
sprouts were detected. Multiple organizations, on
the other hand, can affect changes in many places,
especially out of the way places where the opposition
does not have its wide-blade mowers available.

Men try out different types of grass in different en-
vironments. In one place it may be Bermuda grass (or
Bahamas grass, or even Switzerland grass). In others
it may be plain old crab grass. What it must not be is
unwatered, unfertilized grass that will wither when
the mid-day sun hits it. That is the grass which hu-
manists have planted, and as the State’s restraints on
freedom squeeze productivity out of the legal, visible
markets, the énd of cheap Federal fertilizers and cheap
water will lead to a change in ownership of the field.

What we have is exactly what we need: altemative
grass seeds, hidden from view in minor and seemingly
insignificant fields. We are steadily raising up new,
non-hybrid “seeds” that will survive the competition of
new blights and new environments. The hybrid seed
used by the State produces a lush lawn, but only
under limited environmental conditions and only by
