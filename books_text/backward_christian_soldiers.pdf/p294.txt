280 BACKWARD, CHRISTIAN SOLDIERS?

Bauer, P.T, a

Beatles, 51

bedpan, 69

Beka Books, 148

Bible, 3, 40, 263

Bible Belt, 57

Bible colleges, 2

biblical law, 12, 24, 28,
42, 44, 47 55, 72, U7,
123, 124, 250, 266,
272-74
European history,

129

blessings, 12, 45

“blood” (smell of), 25

blueprints, 18, 41, 65, 66,
69, 72

bond issues, 180

boot camp, 109

Britain, 3, 167

brush-fire wars, 189, 220

Bryan, William Jennings,
147

Buddy Holly, 5f

bugout fever, 102

Bunyan, John, 108-9

bureaucracy, 80, 135, 172,
175, 178, 179, 181, 182, 220

Burke, Edmund, 246

Busing, 180

128,

CBN, 220, 222, 255
Caesar, 39
Caligula, 36
Calvary, 48

Calvin, John, 2
Cambodia, 723
Canaan, 4, 237-38
capital, 9, 130-32, 151,
230, 238, 251
Carter, Jimmy, 23, 6f
cathedrals, 240
Catholicism, 16, 112, 126,
123 136, 144-45
centralization, 195, 137 225!
certification, 164, 167 180
Chalcedon, 26¢ -
Chamberlain, Neville,
chaplain, 99
chaplains, 140
childhood, 125
Chilton, David, 237
China, JH, 122
Chinese, 101
Christ, 10, if4
Christianity
broad position, 140
co-operation, 135, 140
future-oriented, 229
irrelevance, 7
irresponsibility, 7
power, @
responsibility,
retreat, 5, 17
secularized, 24
silent, 8
Western civilization &,
126
Christian Law Association,
92

56

46, 53

 
