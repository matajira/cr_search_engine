230 BACKWARD, CHRISTIAN SOLDIERS?

years from now, for they are convinced that civiliza-
tion, like themselves, has only about five good years
left. After them, the deluge.

Methuselah lived longer than any other man. He
died in the year of Noah’s flood. (He was 187 years
old when he begat Lamech, and 182 years later,
Lamech begat Noah [Gen. 5:26-27]. Then, 600
years later, the flood came [Gen. 7:11]. If you add
187, 182, and 600, you get 969 years, which was what
Methuselah lived [Gen. 5:27]. He therefore died in
the year of the flood.) More than any other man in
history, he had the most opportunities to say, ac-
curately, that he had at least five years remaining.
Also more than any other man in history, he had the
right to say, “After me, the deluge.” Yet life went on.

A new civilization lay ahead, through the waters of
the flood.

Those who assume, as Queen Elizabeth I assumed,
that they have five years left, are very short-sighted.
Elizabeth refused to name a successor, so her sur-
vivors named James I, who turned out to be a disas-
ter for England, as Otto Scott’s biography of him
demonstrates. Things do go on. They go on with or
without any individual. They go on with or without |
that individual’s legacy. The man who continually |
assumes that five years are ahead of him, so he can
safely defer a decision on who or what will survive
him, is a short-sighted man. Others will use his
legacy to their own advantage, but the less he leaves
with instructions for the perpetuation of his capital
or work, the more will remain for his heirs—spiritual, |
genetic, ideological, or bureaucratic~to dispose of

 
