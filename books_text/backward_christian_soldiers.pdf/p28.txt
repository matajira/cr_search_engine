14 BACKWARD, CHRISTIAN SOLDIERS?

The threat of judgment is spelled out clearly in
Deuteronomy 28. The reality of judgment has been
with us since 1914. The Soviet Union—the most con-
sistent humanist regime in history —has escalated its
pressures on the West, and since 1982 has been in a
position to launch a successful first strike against
America’s undefended missiles. Yet any pastor who
would dare to mention the wisdom of buying dehy-
drated food, gold coins, and a home in a small town
would be branded as an extremist, What is an ex-
tremist? A prophet. And you know what respectable
priests and rulers did to the prophets.

CONCLUSION If you are a pastor, and you don’t
think your congregation wants to hear this kind of
message, think about forming a new congregation.
It won't be difficult. Just start preaching like a
prophet of God, and the losers will leave, or toss you
out. Your income as a pastor is going to wipe you out
anyway; better seek alternative income now, while
you have the opportunity.

If you are a layman, and your pastor refuses to
preach like a prophet, find a new church, or do what
you can to get a new pastor. Being surrounded by
Christian lemmings (grasshoppers, in Aesop’s fable)
when the crisis hits will be unpleasant. You will need

friends who are better prepared than lemmings in
that dark day.
