216 BACKWARD, CHRISTIAN SOLDIERS?

mat them into paragraphs, and have the machine
shift them around. For $100 to $200, you can buy
spelling dictionaries that will tell you when you have
misspelled a word, or even when you have forgotten
to close a parenthesis, or close a quotation with a
quotation mark. (Microproof has 50,000 words: Cor-
nucopia Software, Box 5028, Walnut Creek, CA
94596: Grammatik has 38,000 words, plus the punc-
tuation mark checking device: Aspen Software, Box
339, Tijeras, NM 87059.) In short, a revolution in
newsletter writing, article writing, and book produc-
tion is here—for under $3,000.

Say you want to write up weekly sermons, You
put them into the computer, just by typing (which
allows you to type far faster, by the way). Then you
decide to update them, correct them, and put them
into a book. You just call back the originals, make
the corrections electronically, push a button, and get
copy out of your printer, Or you can locate a typeset-
ting company that allows your computer to talk
directly over the phone to their typesetting com-
puter. Presto: you can demand about a 30% dis-
count (they pay no one to type the manuscript into
the computer), and the total entry time is about an
hour, possibly less, Within days, you have in your
possession camera-ready copy, without spelling er-
rors (thanks to your electronic dictionary), and for-
matted by your professional typesetter. A book can
go from raw sermons to a finished product in less
than two months—possibly a month, if the book
printing company co-operates.
