CHURCH NEWSLETTERS §=201

target the audience. The newspaper ad is your
“shotgun.” The caller self-targets himself and calls
in. Those who are not interested drop out. Those
who are interested will probably call again. The goal
ts to use the messages to screen the audience. Those who
finally respond to an offer are far hotter prospects.
They represent what the sales industry calls “quali-
fied prospects.”

The recorded message is somewhat more personal
than the printed page. The human voice has a
greater degree of personalism. But people are not
going to walk in the church door just because of the
tape, unless the tape does something related directly
to church services, such as offer a brief summary
(with tantilizing questions) about next Sunday's ser-
mon. What people will normally respond to is the
offer of literature of some kind. They may hesitate to
schedule a visit with the minister, but they are will-
ing to take the next step: writing for a free tract,
newsletter, or whatever.

The person who responds to an offer has taken an
important first step. He has requested something.
You are not shoving anything down his throat. This
is very important. Your goal is to bring the person
into fellowship on a step-by-step basis. He needs to
know at each stage that he is initiating the next step.
Your job is to encourage him to take each step. But
you need to do this on a cost-effective basis. You
should not waste church resources.

With a mailing list, you can target your audience.
You can mail questionnaires. You can contact the
person to interview him, This enables you to get
