1B BACKWARD, CHRISTIAN SOLDIERS?

went down in a sea of needlessly shed blood, the
Christians grabbed the only life preservers they
thought were available: pessimistic eschatologies.
They took comfort from the fact that the ship had
sunk, not because they were safely sailing on a rival
ship, but because ali optimistic endeavors are sup-
posedly doomed, They had built no ship of their own
to compete with the Tiianic of secularism, so they
comforted themselves by clinging to theologies of
universal sunken ships.

There are those who parade a theology of ship de-
signing. They say that we ought to conquer the earth
by means of Christian institutions. They claim that
they have designs ready and waiting—cosmonomic
designs, certified for export by the Dutch board of
trade—but that they know, in advance, that there is
no market for such designs, no capital to begin con-
struction, and no hope of seeing them completed.
They feel that they have been faithful to the Bible by
merely proclaiming the hypothetical possibility of
the external kingdom of God on earth. They have
not bothered to get down to the blueprint stage, sim-
ply because they have not believed that their social
and economic designs could ever be implemented.
Allships, ultimately, are doomed, say these theologi-
ans of shipwreck.

Is it surprising, then, that the “dominies” in cleri-
cal robes are considered to be immune to criticism
by laymen within ecclesiastical organizations that
are based on a theology of shipwreck? After all, if all
secular ships must go down eventually, and all
Christian social institutions are equally doomed,
