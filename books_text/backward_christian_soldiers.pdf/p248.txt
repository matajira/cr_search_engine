234 BACKWARD, CHRISTIAN SOLDIERS?

left, and that they should assume that their organiza-
tions are not going to be left behind in a world with-
out the presence of other Christian workers, they
have to think about the future. Because they know
they will die, they can be optimistic about the future.
They know that other Christians will persevere.
They know that Christian institutions will survive to
serve as salt for a world civilization. Because they
will die, they think to themselves, they can build for
the earthly future of others who will also die.
Because their view of their own efforts is necessarily
short run—one lifetime, at most— their view of the
long-term effects of their efforts is inplicitly long run.

No one in this world gets out alive. None of us will
be raptured. No institution is left behind without
any possibility of extension into the future. God will
not pull the plug on history until the whole world is
brought under His institutional sovereignty, ‘There ts
hope for the kingdom of God on earth, precisely
because there is no hope for God’s people to escape
the sting of death. Postmillennialists can rejoice in
their own physical mortality. Their efforts can
multiply over time, long after they are dead and
gone. They are optimistic. They know, in principle,
that they are future corpses. There is no escape,
Once this is firmly in one’s mind, one can get to
work—work for the long haul. By God’s grace, the
results of such work will survive and prosper.

TO PLANT A TREE When men look to the future,
they can make minimal investments that can, if
given enough time, become major sources of spirit-
