teed, NOT 1948 51

be the parent figures of America. (Trivia question:
What did Ozzie do for a living?) The CBN commer-
cial stations rerun “Father Knows Best” in the after-
noon, or similar sitcom fare. Christian parents see
these bland, thoughtless series as a kind of Novocain
for their children’s minds, or even their own minds.
It takes them out of the 1980's.

In the Bible Belt the culture is still suffering from a
mixture of three decades of rebellion. The 1950's
style rebellion is almost cute: hot cars, cruising down
main street, cigarettes behind the gym. These are
the sorts of things that Fonzie does, and everyone
loves Fonzie. The kid who was the black sheep in
1957, the kid your parents wouldn’t have wanted you
to goof off with, is a folk hero of the 1970’s and
1980's —a throwback to “the good old days,” when sin
was essentially harmless. ‘The trouble is, it was that
“harmless” sin that served as a cultural wedge for the
late 1960’s. The same is true of the music. Buddy
Holly was cute; the early Beatles were a bit strange
looking, but cute. Nothing since 1965 has been cute.

Also present in the Bible Belt is the rebellion of the
1960's, such as pot smoking, illicit sex, loose lan-
guage (I mean loose—incoherent—not just foul).
Premarital sex, while frowned upon in Christian
circles, is no longer a cause for mental breakdowns
among parents. It is not exactly accepted, but it is
not universally condemned, either. Incredibly, mem-
bers of churches are not universally opposed to abor-
tion. Thus, we come to the sins of the 1970's, such as
having your daughter, “who made a mistake,” kill
her unborn child. In this case, the parents are as
