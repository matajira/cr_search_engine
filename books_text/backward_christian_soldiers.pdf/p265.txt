THE LONG, LONG HAUL 257

his soil, the businessman developmg a market, an
inventor developing a working model: here are ex-
amples of continuity. Since Christians are called into
God's service, they must adopt a program of con-
tinuity in their primary area of service.

The shortening of men’s time horizons as a result
of both premillennialism and amillennialism has
contributed to a decline in competence among Chris-
tian workers and an increase in reliance upon the
miraculous. If men do not believe that they have a life-
time to develop their skills and capital, let alone to
pass down both skills and capital to later genera-
tions, they must become dependent upon God’s
miracles to advance their causes. As men’s time
horizons shrink, their quest for “the big payoff” increases,
since only through such a discontinuity can they €x-
pect to advance themselves significantly in a brief
period of time. The man who has time can ex-
perience steady but slow increases in his capital—
however he measures his assets. The man who does
not have time cannot afford the luxury of continuity.

COVENANTS AND TIME. To count the costs, you
must be able to estimate time, Time is a significant
factor in assessing true cost. Try to get a home built
in half the time; it costs more money. The same is
true of almost any investment. The shorter the pay-
out period, the higher the risk involved. Short
delivery time schedules cost more.

When men covenant with an eternal God, they
must have some idea of what He expects them to ac-
complish in time and on earth. They must estimate
