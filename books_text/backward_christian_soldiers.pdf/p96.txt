‘82 BACKWARD, CHRISTIAN SOLDIERS?

same old Keynesianism. It is the status quo. So, us-
ing the language of anti-status quo, Dr. Lloyd-Jones
joined the ranks of the ordained chaplaincy of hu-
manist conformity. He was a Conformist’s confor-
mist, and he was therefore granted the right to use
the language of progressive reformism—so long as it
was not promoted in the name of Christianity, so
long as it abandoned any appeal to Old Testament
law, and so long as it abandoned hope.

Is it any wonder that leadership like this has pro-
duced generations of socially impotent Christians? Is
it any wonder that humanism, in the form of the
welfare State, has triumphed? In the realm of soci-
ety, the salt has lost its savor. We have been afflicted
with chaplains who have actively promoted savorless
salt. The sheep need better shepherds; they. need
shepherds who are not front men for political hu-
manism’s wolves.
