PASTORAL EDUCATION 169

St. Andrews University in Scotland, or the Univer-
sity of Manchester, or some other British university.
Go in, write your thesis, take your orals, and gradu-
ate. Get finished as fast as possible. If you think you
need to go to seminary, think again; anyone who
needs 2 conventional seminary education probably
shouldn’t be contemplating the ministry anyway.
Better to apprentice with a master church-builder or
Christian counsellor, and learn directly.
