6 BACKWARD, CHRISTIAN SOLDIERS?

remind Christians that without the sure foundation
of the belief in the creation, modern science could
never have arisen? He, not the local pastor, has an-
nounced: “The optimism and buoyancy of Western
culture is more an effect of the idea of the good crea-
tion than its cause.”

With the fading belief in the creation, we have
seen the coming of major intellectual crises in
science and the arts. Who has informed modern
Christians that without permanent standards of law
and truth, no progress or development is possible,
and that with the coming of relativism we have seen
the death of the idea of optimistic progress? Why
can't it be some internationally known evangelist
rather than a secularist like Gunther Stent, professor
of biology at the University of California, Berkeley?
His book, setting forth this idea, The Coming of the
Golden Age: A View of the End of Progress (1969), was
published for the American Museum of Natural
History. Why must the secularists do our work?

When we think of Christian scholarship, what do
we have in mind? A seminary? But seminaries limit
their efforts to instruction in the biblical languages,
evangelism, church history, or “practical” theology —
counselling, church budgets, visitation, etc. Apart
from the Wycliffe translation program, . there is
hardly a single explicitly Christian endeavor that has
impressed the secular world with its competence. We
are second rate, or third rate, and we know it. Why?
I contend that it is directly related to our stubborn
unwillingness to consider the whole counsel of God.
A book like R. J. Rushdoony’s institutes of Biblical
