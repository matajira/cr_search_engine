WHY FIGHT To Lose? 38

declared Christianity as the lawful religion. The first
Christian emperor of Rome had arrived. The perse-
cutions ended, Christians were brought into the civil
government. Constantine recognized the obvious:
there was no other social force in Rome stable
enough, honest enough, and productive enough to
match the Christian church. The Empire could no
longer do without these people. After almost 300
years, Christ had conquered Caesar. The power of Rome
had crumbled before the kingdom of God. God,
through the faithfulness of His people, had van-
quished His enemies.

CHRISTIAN SELF-GOVERNMENT The church had
suffered. It had been reviled, ridiculed, beaten
down. But over the years, Christians learned how to
deal with adversity. They had learned to deal with
reality. There was no Roman State to rely on for
justice or protection. They had to rely on God, on
themselves, and their church courts. They became a se-
cond government within the Empire. When the time was
ripe, they were ready to exercise leadership.

But what about today? Are Christians ready to ex-
ercise leadership in the high places of our world?
Where are the Christians? Almost invisible,

Why? Why are people who believe in the God of
Abraham, Moses, David, Elijah, and Christ almost
invisible in today’s culture? We live in a culture built
by Christians, from the days of Constantine to the
days. of America’s Founding Fathers, almost all of
whom were members in good standing in Bible-
believing churches. But we have very little say in to-
