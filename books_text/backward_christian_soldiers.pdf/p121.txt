THE STALEMATE MENTALITY 107

perpetual stalemate. They are no longer content to
remain bottled up on the Pusan peninsula, No one
needs a point man in a defensive operation anyway;
point men are offensive, They are used in breakouts,
They are used to “carry the war to the enemy.” They
are not used in defensive operations. By identifping
himself as a point man, Jerry Falwell has been forced, in-
stituttonally and financially, ta abandon the language of
premillennial dispensationalism, whether he still believes it
or not. He has adopted the language of victory.

Christians are rallying to support Falwell and
others like him who stand up and fight. In doing so,
they are steadily abandoning premillennialism,
psychologically if not officially. Older groups still
cling to eschatologies of defeat. These groups are
large, but they are dying. Their influence is shrivel-
ling up. People who have rallied for a battle do not
want to hear tales of inevitable defeat from their
commanders.

The kind of theology represented by Hal Lindsey's
books is fading. Those who believe Lindsey hope for
“peace in Pusan.” They pray for a continuing stalemate,
for they see persecution and external defeat as the
only alternative to stalemate. But those people are
not in the front lines of today’s religious battles.
“Bugout theology” does not produce armies, only refugees.
These people cannot serve as point men. Their
theology will not produce a sustained battle—the
kind of battle Christians are lining up for today.

A stalemate is not good enough. MacArthur said
it best: “There is no substitute for victory.”
