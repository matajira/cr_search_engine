186 BACKWARD, CHRISTIAN SOLDIERS?

letter network, over 150,000 letters of protest were
generated, and the IRS had to postpone the action
(although it is still pending). This indicates how the
newsletter industry is becoming a kind of
underground information network. The newsletters
are the modern equivalent of the Committees of
Correspondence during the American Revolution.
(Address: Temple Times, 2560 Sylvan Rd., East
‘Point, GA 30344).

There is no reason why a church can’t publish
several letters, each aimed at specific groups. There
is no reason for the pastor to publish a church news
report; that task should be delegated. The pastor
should specialize. His teaching ministry should in-
creasingly become a written and tape recorded
ministry, with graduates of the training sessions
leading the new ones. A newsletter program allows a
continual upgrading of any teaching ministry. The
newsletters become inserts for instructional packages
later on.

THE CLEARING HOUSE The church which adopts
my issues-oriented program of local evangelism can
use newsletters as an integral program of follow-ups.
This program is outlined in The Journal of Christian
Reconstruction (Winter, 1980-81): “Evangelism.” Send
$5 to: Journal of Christian Reconstruction, P.O.
Box 158, Vallecito, CA 95251. In this program, local
canvassers contact residents, block by block, neigh-
borhood by neighborhood, with a questionnaire
geared to specific topics of interest to activist Chris-
tians: abortion, national defense, inflation, etc.
