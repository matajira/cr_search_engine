ESCHATOLOGIES OF SHIPWRECK 17

merely subordinate authority, but by nature in-
ferior).

One of the reasons why Christians have adopted
the peculiar view of authority outlined above has to
do with the concept of victory. From Augustine to
Kuyper, or from Luther to Barth, expositors have
too often limited the promise of victory to the institu-
tional church, or even more radically, to the human
heart alone. Where a man’s heart is, there will be his
kingdom. If his hope of victory is limited to his
heart, then his concern will be drastically narrowed.
He will worry about his heart, his personal standing
before God, his own sanctification, and his relation-
ship to the institutional church. He will be far less
concerned about exercising disciplined authority in
the so-called secular realm. It is difficult psychologi-
cally to wage war on a battlefield which by definition
belongs to the enemy. An army which lacks confi-
dence is defeated before it takes the field. This is why
God commanded Gideon to announce to the Israel-
ites: “Whosoever is fearful and afraid, let him return
and depart early from Mount Gilead” (Judges 7:3).

A THEOLOGY OF SHIPWRECK What we have
seen, especially since the First World War, is a
retreat from victory by Christians. Precisely at the
time when humanism’s hopes of a perfectible earth
were shattered on the battlefields of Europe, the
Christians also gave. up hope. The Christians had
seen the technological victories of secularism, and
they had mentally equated these victories with
Christ's kingdom promises. When the secular ship
