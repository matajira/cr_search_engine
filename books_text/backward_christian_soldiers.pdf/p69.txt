1904, NOT 1943 55

They did not take the prophets’ warnings seriously,
either. But when judgment came, and Zion ceased to
be at ease, people then started examining their
hearts and God’s law to see what had happened to
them, The 1.C.E. is laying down a foundation of
critical materials, as well as construction materials, so
that after the signs of a crisis are obvious to the pub-
lic at large, there will be a few thousand Christians
who say to themselves first, and then to those around
them, “I knew this was coming. I was warned. I was
told of social, economic, and political alternatives,
The law of God teaches that these sorts of crises are
inevitable. If we want to reconstruct society, we need
biblical alternatives to secular humanism. This time,
we have to take God's word seriously.”

We need diesel fuel for our generators. The
publications of the I.C.E. serve as back-up supplies
of diesel fuel. There is little likelihood that our per-
spective will be taken seriously until this culture
works out its damnation without any fear or trembl-
ing. What these publications are designed to do is to
sit on shelves in 3-holed notebooks, gathering dust.
Most recipients probably ignore these materials to-
day. We are investing long-term when we send them
out, We want people to wake up on the far side of some dis-
aster, reach up to the dusty shelf, and start rereading our ma-
terials. They will build then on foundations laid
down today.

In a major crisis, every crackpot in the world will
be parading in the streets (or the mails), telling
anyone who will listen that “I told you this was com-
ing! You had better listen to me.” I will be among the
