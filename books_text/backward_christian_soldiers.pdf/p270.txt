256 BACKWARD, CHRISTIAN SOLDIERS?

Oregon. He used to help students obtain draft
deferments, as well as oppose the war in other ways.
He noticed only years later that almost nobody ever
offered to pay him for his assistance. It was assumed
by radical students that such assistance was a free
good, that it was somehow owed to the beneficiary.
This is the typical mind-set of the political liberal.

The same phenomenon affects the bulk of the
socialist-interventionist movements of our time.
With the notable exception of the Communists, the
Left has been generally unwilling to self-finance
their programs in this century. They much prefer to
get the State to finance them. This has been done,
too; the conservative rallying cry, “De-fund the
Left,” is valid. Ideologically radical organizations
have for years been granted millions of dollars, from
Planned Parenthood to the Legal Services Corpora-
ton.

But at some point, this dependence on the State
backfires. Sources of private funding dry up, since
everyone knows that the State is writing the checks.
For instance, the Left has not developed successful
direct-mail campaigns or mailing lists, unlike the
conservatives. When public opinion finally turns
against the religion of secular humanism, and voters
start cutting off the funds, these organizations will
lose access to perpetual funding. When the fiat
monetary unit finally goes the way of all flesh, what
will they use to pay their employees? ‘The govern-
ment supplies the money, but the money it supplies
is Federal money. What happens if Federal money
becomes worthless?
