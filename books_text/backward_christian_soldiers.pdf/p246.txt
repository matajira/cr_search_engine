232 BACKWARD, CHRISTIAN SOLDIERS?

final judgment follows. There will be no era of semi-
god status, mixed in with people who had not been
raptured as saints. The rapture means the end of
time in the amillennialist perspective. No earthly,
sin-influenced millennium follows the rapture.

In any case, whatever is left behind is not worth
much, compared with whatever follows. It is not
worth saving. It is not a down payment on the future
era of bliss. It is only the stained rags of life which we
are all trying to escape. What we leave behind, in
short, is bio-degradable trash. Our legacy will rot.

Christians seek immortality. They want to avoid
death. They are generally convinced that the end is
in sight, that there is “light at the end of the tunnel.”
The rapture draws nigh, Escape draws nigh. Im-
mortality draws nigh—an immortality which is not
stung first by death. Literally millions of Christians
believe that they, as members of the terminal genera-
tion, will experience this death-free way to immor-
tality.

OPTIMISTIC FUTURE CORPSES Only one tiny
group of Christians firmly believes that they will die.
In fact, they rejoice in the fact that there are more
years ahead for society than there are for themselves.
They know they must plan and build in terms of
their own death, They know that someone will read
their last wills and testaments, including institu-
tional last wills and testaments. They know that
there is no escape, that insofar as life is concerned in
our day, nobody gets out of it alive. These people are
called postmillennialists.
