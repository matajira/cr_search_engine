58 BACKWARD, CHRISTIAN SOLDIERS?

it contains the seeds of its own destruction. As it becomes
more consistent with its own presuppositions, it will
disintegrate.

‘We must be ready to pick up the pieces after the
disintegration. We must be ready to show others how
to do it. That is what the early church did for the col-
lapsing Roman Empire. We must be ready to do it
again.
