240 BACKWARD, CHRISTIAN SOLDIERS?

kinds of institutions they have constructed, and are
in the process of constructing. (Why these groups
continue to encourage pension fund programs and
annuities for their pastors is a mystery, but they do.
Maybe—just maybe~the administrators of the
funds think they can get access to the ministers’
money today, and avoid having to pay off later, in
the post-rapture period. Of course, the fund mana-
gers may decide to invest the funds in terms of this
operating assumption, praying fervently that Christ
returns in glory before the dollar goes belly-up and
the funds go bankrupt. It is my contention that a
majority of pension investors and annuity holders
living today will learn to their dismay that time has
run out for the dollar, not the dispensation.)

In medieval times, communities built cathedrals
that were expected to last for a thousand years, and
some of them have. Generations of local contributors
and craftsmen would add their money, goods, or ser-
vices to the long-run construction project. These
majestic buildings are no doubt being used today by
people who do not hold dear the religious beliefs held
by the builders, which is the best argument against
what they did, but at the same time, these structures
attest to the long-run vision they shared, their hope
for the future, and their willingness to sacrifice pres-
ent income for the sake of the beauty which many
generations after them would enjoy. If they built
their cathedrals for narrowly ecclesiastical reasons ~
a vision of the church and church worship—then
they may have erred, for the church in our day has
abandoned the kind of supernaturalism: that the
