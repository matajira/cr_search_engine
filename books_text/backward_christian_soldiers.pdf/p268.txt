28

SMALL BEGINNINGS

 

“Frustration is basic to reconstruction.”

 

For who hath despised the day of smail things?
(Zech. 4:10a)

One of the difficult things to imagine is a modern
proponent of political liberalism standing up to pass
the hat for some local social action project. What he
will attempt to do is to create a grass-roots pressure
group to promote the financing of the particular
project with local taxpayer funds, or better yet,
through Federal grants. The political liberal’s idea of
social action is action to increase the power of the
State over local affairs.

The political liberal wants to achieve his goals
through political action. His religion is the religion
of politics. He is skilled at gaining favors by the State
for pet projects. His answers for almost every prob-
