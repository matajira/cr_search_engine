FUNDAMENTALISM: OLD AND NEW 27°

way, who made the oft-quoted statement that “God
doesn’t hear the prayer of a Jew.” Ironically, the
Moral Majority got tarred with that statement by the
secular press, yet the man who made it had publicly
disassociated himself from the Moral Majority. He
has since disavowed the statement, but he certainly
said it with enthusiasm at the time. I was seated on
the podium behind him when he said it. It is not the
kind of statement that a wise man makes without a
lot of theological qualification and explanation.)

In checking with someone who had attended a
similar conference in California a few weeks pre-
viously, I was told that the same neglect of the rap-
ture doctrine had been noticeable. All of a sudden,
the word had dropped out of the vocabulary of
politically oriented fundamentalist leaders. Perhaps
they still use it in their pulpits back home, but on the
activist circuit, you seldom hear the term. More peo-
ple are talking about the sovereignty of God than
about the rapture. This is extremely significant.

MOTIVATION How can you motivate people to get
out and work for a political cause if you also tell
them that they cannot be successful in their efforts?
How can you expect to win if you don’t expect to
win? How can you get men elected if you teil the
voters that their votes cannot possibly reverse soci-
ety’s downward drift into Satan’s kingdom? What
successful political movement was ever based on ex-
pectations of inevitable external defeat?

The Moral Majority is feeling its political
strength, These people smell the blood of the politi-
