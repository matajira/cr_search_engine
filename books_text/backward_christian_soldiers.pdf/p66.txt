52 BACKWARD, CHRISTIAN SOLDIERS?

deeply involved in the sin as the children.

In the regions that see their rebellion on T-V., all
nicely sanitized by the censors and the sponsors,
Christian people can go about their business as if it
were still 1962, as if our military defenses were still
supreme, as if the Federal deficits were still in the $8
billion range, as if the public schools weren't in the
process of academic collapse, as if their pensions
were “as sound as a dollar,” and the dollar were “as
good as gold.” They think they can insulate them-
selves from the economic crises of New York City
and the moral crises of southern California, Their
culture’s walls still seem to be standing. If they can
just get the local 7-11 store’s manager to sell the soft-
core pornography from a stack behind the counter,
everything will be just fine.

The walls are in a state of disrepair. The universal
culture of rock music, with its sado-masochistic
lyrics, its call to homosexual experimentation, and
all the rest of the filth, is available to any child able
to reach up to the counter with $7.98, plus tax.

The public schools are the established churches of
the religion of secular humanism, yet millions of
Baptists in the South think of these schools as “our
schools,” and nothing short of a 2-and-10 football
season can get them up in arms. If someone were to
write a book on the South’s approach to education, it
might be called, Pigskins, Sheepskins, and Lambs to the
Slaughter.

SHORTING OUT The cultural insulation. of the
Christian world has worn through all along the line.
