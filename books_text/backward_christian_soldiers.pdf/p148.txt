15

SHEPHERDS AND SHEEP

 

“The battle for Christian reconstruction is many-
faceted, but there is a single “litmus test” to
separate the shepherds from the soon-to-be-
sheared sheep in this battle: their support,
financial, famllistilc, and verbal, of the Christian
day school movement.”

 

Any successful strategy of conquest must employ a
two-pronged attack: the grand design and specific
tactics. We need to participate in a co-ordinated pro-
gram of conquest. This is what Christians have
never succeeded in achieving. The various Christian
churches, not to mention independent groups not
directly connected to denominations, have always
arrogated to themselves almost total authority. They
have not been able to co-operate in the realm of first
principles, nor in the area of strategy. They have
thought they could “go it alone.”

The result has been fragmentation. This, how-
