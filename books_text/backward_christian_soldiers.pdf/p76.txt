62 BACKWARD, CHRISTIAN SOLDIERS?

Majority is politically and religiously harmful (pp.
405-6).

Ladd and Ferree make an interesting comparison
between prestige universities and liberalism, and be-
tween liberalism and the Ph.D.

In various publications including The Divided
Academy Ladd and Lipset demonstrated that
within academe, confounding a “class theory of
politics,” the “top” is more liberal than the “bot-
tom.” When one arrays faculty, for example, by
the intellectual standing of the college or uni-
versity at which they teach, one finds that with
every step up the institution-quality hierarchy
there is a greater measure of faculty liberalism.
Similarly, within any type of university or college,
professors with greater academic attainments —
measured, for example, in terms of levels of
scholarly publication—are consistently more
liberal than their less-attaining colleagues. . . .

One sees reflections of this signal relation-
ship in the political orientations of groups with
the theological faculty. Those whose career em-
phasis has been nearest the academic and
scholarly emphasis of the “main-stream” Amer-
ican professorial community appear consis-
tently more liberal in sociopolitical outlook
than those less involved in conventional aca-
demic work and attainment. For example,
theology faculty who hold the rank of Ph.D. are more
Liberal on every social and political issue measured in
the survey than are those with other academic
