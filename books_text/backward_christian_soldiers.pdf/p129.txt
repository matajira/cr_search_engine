WHAT KIND OF ARMY? 115

in a shooting war, you cannot win without them.

Ficld-grade officers—majors and colonels—are
numerous. (A seminary professor is a light colonel.)
Thats what Protestants have in large numbers.
However, they all have the idea that they are
generals, or if not actually generals, then at least
they are as good as today’s generals. They know for
sure that nobody is about to follow them into battle,
so it really doesn’t make much difference how good
or incompetent they are. Nobody recognizes that the
war is on, because it’s not a shooting war yet.

Second lieutenants are, as always, as expendable
as tent pegs, and not much more useful. They are
the deacons, seminarians, and elders in churches
that are one-man bands, They know they are un-
prepared, and so does everyone else. Noncommis-
sioned officers, such as Sunday School teachers, are
ignored by everyone. They are assumed to be in-
competent, but you need them in any bureaucratic
system. We supply them with weak or corrupted ma-
terials, give them no training, and send them out to
teach, Teach what? Well, whatever drivel pietistic
evangelicals have published, or whatever socialistic,
guilt-producing handbooks that have been issued by
the denominations liberals.

The troops sit passively, confused, unaware that a
war is in progress. They think their commanders are
on top of everything. They don’t even feel called
upon to exercise minimal leadership, which is just
what their superiors prefer.

We have no strategists, so far as I can determine.
How could we? We haven't won a major battle in
