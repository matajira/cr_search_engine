114 BACKWARD, CHRISTIAN SOLDIERS?

What kind of army functions without a chain of
command? None. Then what kind of army is the
church? A defeated army. An army which is told that
it must suffer defeat, that any sign of victory is an il-
lusion or else a lure into a subsequent defeat, that
victory must be the Devil's, will be a defeated army. Yet
this is precisely what modern Christians have been
told, and since they don’t like the rigors of battle,
and since they don’t like the discipline of a chain of
command, and since they really don’t trust the judg-
ment of their officers, they prefer to listen to stories
of defeat. Defeatism justifies their own softness. And
since they are guaranteed victory in the internal bat-
tles (they think), and since the external warfare is
simply allegorical (they think), they can dismiss as
ridiculous the idea that we really should be training
as cultural soldiers soldiers ready to do battle on a
multitude of battlefields.

THE SUPREME COMMANDER Jesus Christ is our
supreme commander, but He operates only through
His word, which is unquestionably a training man-
ual, However, He has many interpreters, and few
people see the Bible as a true training manual. There
are too many one-star generals in a peacetime army,
all building up their local empires, all jealously com-
peting against their peers, and most of them com-
pletely unprepared for a war, When the war comes,
both superiors and inferiors recognize which generals
are fit to lead, and the peacetime bureaucrats are rap-
idly removed from public scrutiny. Peacetime armies
cannot tolerate men like Patton, or even MacArthur;
