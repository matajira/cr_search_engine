294 BACKWARD, CHRISTIAN SOLDIERS?

handbook ever written on the subject. This hefty book

provides many workable plans for churches and Chris-

tian groups to resist unlawful government intrusions.
More than this, it offers a concrete strategy for

building a Christian culture. There is a wealth of ma-

terial here on taxation and exemption problems,

Christian school issues, and other legal matters which

are increasingly involving concerned Christians,
Some of the articles included are:

© James B. Jordan: “Rebellion, Tyranny, and Dominion
in the Book of Genesis.”

© Gary North and David Chilton: “Apologetics and
Strategy.”

* Gary North: “The Escalating Confrontation with
Bureaucracy.”

© Michael R. Gilstrap: “Citizens’ Paralegal Litigation
Tactics.”

*® Ray R. Sutton: “The Church as a Shadow Government”

* Otto J. Scott: “Tactics of Resistance Used by the
Protestant Reformers.”

© Lawrence D. Pratt: “Tools of Political Resistance.”

© and thirteen more!

Israel and the New Covenant by Roderick Camp-
bell, 350 pp. indexed, hardback, $12.95.

The main thesis of this long out-of-print classic is
that the future of Christendom is not to be read in
terms of impotence and apostasy, but of revival and
victory. In other words, this book deals with the
Christian conception of the future and with our duty
as Christians in view of that future. Mr. Campbell
presents an unusual approach, among present-day
writers, not only to the religious and ethical, but also
to the social and political problems of our times.
