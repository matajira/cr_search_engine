186 BACKWARD, CHRISTIAN SOLDIERS?

operate in terms of price, at least until they get on
national television,

One thing is sure: there will be greater demand for a
pastor's time than his supply of time, at zero price. His time
is a scarce resource. He has to allocate it. And given
the “finger-in-the-dyke” syndrome, he tends to
become a crisis-management man. He acts in terms
of crises. He.learns to allocate his time in terms of
the comparative catastrophe method.

LOW PRIORITY On this scale of measurement,
reading has a low priority. Settling family quarrels is
much higher up. Visiting dignitaries rate higher yet.
Counselling oil executives who tithe is still higher on
the list. (“Al souls are equal, but some. are more
equal than others.”) But reading is down-there at the
bottom, running neck-and-neck with catechism
classes, paper cup supplies, and the wife’s birthday.

Pastors are too often functional illiterates. This
doesn’t mean they can’t read. It means they don’t
read anything except the daily newspaper, overdue
bill notices, and articles under two pages long in
Christianity Today. They read only those items aimed
at people who have lost the ability to discipline them-
selves enough to tackle anything long, serious, com-
plex, or thought-provoking. Only those pastors who
really enjoy ideas, the way that Pentecostals enjoy
“new things,” Episcopalians enjoy prayer breakfasts,
and Presbyterians enjoy committees, are willing to
struggle with tough books.

Serious reading, like serious anything, takes
practice —systematic self-discipline on a long-term
