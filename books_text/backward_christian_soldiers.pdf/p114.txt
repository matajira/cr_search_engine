100 BACKWARD, CHRISTIAN SOLDIERS?

knock down the door. It would not be easy, but it
would be done.

PUSAN OR INCHON? In June of 1950, the North
Korean army attacked South-Korea. The poorly
equipped South Korean army, outnumbered two-to-
one, collapsed immediately. Half of its 65,000 men
were killed, wounded, or taken prisoner, President
Singman Rhee {age 75) and General Douglas
MacArthur (age 74), who had flown in from Japan,
watched the final rout from the front lines. Seoul,
the South Korean capital, fell in four days.
America flew in the Army’s 24th and 25th divi-
sions from Japan. Inexperienced, poorly trained,
and poorly equipped, they retreated for six weeks
until they were trapped on the southern tip of Korea,
the Pusan peninsula. Some American troops had
surrendered ‘at first, until they learned how few
prisoners the North Koreans took. It looked as if
MacArthur’s troops would be pushed into the sea.
Finally, the Americans and South Koreans dug in,
and a stalemate ensued. A chunk of land south of a
120-mile strip across the southern tip of Korea was
all that remained of free South Korea.. Now what?
MacArthur had an idea. Why not lauch an inva-
sion at the port of Inchon, 24 miles west of Seoul and
150 miles from the rear of the North Korean forces?
It was considered impossible to complete an am-
phibious landing at Inchon. MacArthur knew what
military experts believed, so he decided to attempt
it. The element of surprise was crucial. The ist
Marine Division was secretly shipped in from San
