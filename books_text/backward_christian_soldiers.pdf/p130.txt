116 BACKWARD, CHRISTIAN SOLDIERS?

this century. (When was the last time a theological
liberal was removed for heresy by any American
church, other than by the Missouri Synod Luther-
ans, in this century?)

We have a few tacticians who have specialized in
areas like starting Christian schools, or battling
against abortion, or setting up small activist or
publishing organizations, but their efforts are not co-
ordinated, and they seldom respect any single
leader. Even communications are lacking; the
groups seldom talk to each other.

If we have any generals, nobody salutes them,
especially bird colonels, who generally think that they
are the true generals.

THE WAR When war comes—persecution, a Soviet
victory, rioting in the streets, more visible attacks on
Christian schools, a gun confiscation law, an economic
collapse —then the generals will appear. There will be
leaders only when the followers see the strategic neces-
sity of following. When external conditions make man-
datory a chain of command, we will see its creation.
We already have a reliable Supreme.Commander,
He knows what has to be done to win. His enemies
cannot defeat Him or His troops. When commanders
who are capable of leading join forces with followers
who trust their judgment and who are willing to sacri-
fice for the sake of the war, we will see the light. And
books on the inevitability of external defeat will no
longer be best sellers. Psychological losers who don’t
understand the stakes of this war are the buyers of
such books. They will not survive the first volley.
