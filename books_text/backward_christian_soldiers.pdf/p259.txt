27

THE LONG, LONG HAUL

 

“Two steps forward and one step back is a
perfectly reasonable strategy, if you think you
have time on your side.”

 

One of the strange anomalies of modern social
and political life is the sharp contrast between what
conservatives believe and what they actually do, and
an analogous contrast between what revolutionaries
say they believe and what they actually do. Both
sides are inconsistent, and these inconsistencies go a
long way to explain why the radicals have been so
successful, and the conservatives have not.

Consider the radicals’ view of reality. Following
Marx, they believe that the institutions of modern
life are corrupt. The corrupt environment of man-
kind is what lies at the root of injustice in social
affairs. By reconstructing man’s environment, they
believe, they can produce a fundamental alteration
in the nature of man. All radicals share some version
