T74 BACKWARD, CHRISTIAN SOLDIERS?

the knowledge possessed by skilled illiterates. Econo-
mist Thomas Sowell has stated it well: “Although the
phrase ‘ignorant savage’ may be virtually self-
contradictory, it is a common conception, and one
with a certain basis. The savage is wholly lacking in
a narrowly specific kind of knowledge: abstract, sys-
tematized knowledge of the sort generally taught in
schools. Considering the enormous range of human
knowledge, from intimate personal knowledge of
specific individuals to the complexities of organiza-
tions and the subtleties of feelings, it is remarkable
that one speck in this firmament should be the sole
determinant of whether someone is considered
knowledgeable or ignorant in general. Yet it is a fact of
life that an unlettered peasant is considered ig-
norant, however much he may know about nature
and man, and a Ph.D. is never considered ignorant,
however barren his mind might be outside his nar-
row specialty and however little he grasps about
human feelings or social complexities.” (Knowledge
and Decisions [New York: Basic Books, 1980], p. 8.)

It is obvious that the training which is considered
basic for the pastorate is the training of the seminary
classroom, which is essentially bureaucratic and
rationalistic. The Bible, in contrast, describes the
criteria of the eldership as being essentially personal
and familistic (I Tim. 3:1-7). The same is true of
the office of deacon (I Tim. 3:8-13). Protestants,
especially the more “magisterial” Protestants—
Presbyterians, Anglicans, and Lutherans—have
stressed academic performance over apprenticeship.
The result has been the capture of the denomina-
