98 BACKWARD, CHRISTIAN SOLDIERS?

up church courts to handle disputes, as Paul had
commanded (I Corinthians 6), Their people received
justice.

They built strong families. They went out and
picked up babies who had been left to die. The au-
thorities were outraged. This was made illegal. So
the Christians violated the law. They kept taking
home exposed infants.

They took care of their own people. They used the
tithe to support the poor and sick among them. They
did not go permanently on the dole.

They worked hard. They became the most pro-
ductive citizens in the Empire. Yet they were perse-
cuted. They refused to honor the “genius” (divinity)
of the emperor. This was considered treason by the
Roman authorities. Nero used tar-covered Chris-
tians as torches at his parties.

For over two centuries the church was persecuted,
The persecution was intermittent. One generation
would suffer, many would renounce the faith, others
would compromise. The church would be “thinned
out.” Then, strengthened by resistance to persecu-
tion, the church would experience growth during
periods of relative toleration.

Finally, the emperor Diocletian came to the
throne, in 284. Inflation was rampant. He put on
price and wage controls. Shortages immediately ap-
peared. He imposed the death penalty for violators,
The economy began to collapse. He persecuted the
church. In 305, he gave up and abdicated—the first
emperor ever to do so,

In 312, Constantine came to. the throne. He
