WHY FIGHT TO LOSE? 41

for (or against) a particular bill. Does the Bible tell
him which way to vote? What if he is a businessman
who is considering borrowing money for a project.
Does the Bible give him instruction?

What if he is a judge who is about to sentence a
criminal? Does the Bible give him guidelines?

Humanists are convinced that the Bible should
not be used as a blueprint for society. As a matter of
fact, the vast majority of seminary professors, even
in Bible-believing seminaries, agree wiih the hu-
manists on this point. But if they are correct, then
what can we use to guide us in our search for right and
wrong? Our own imaginations? What we learn in
university classrooms taught by humanists? What
we read on the editorial page of the New York Times?

Uf not in the Bible, then where?

This is the question Christians have been avoiding
for a century.

A “KINGS X” FROM GOD? Christians take the
message of salvation to lost men. Why? To tell them
about the penalties of sin, and God’s grace in pro-
viding an escape from judgment, through faith in
Jesus Christ. Without a knowledge of sin, Paul
wrote to the church at Rome, there can be no knowl-
edge of the new life in Christ (Romans 7:9-12).
Then what do we tell men who are sinning in high places?
The prophets of Israel told kings right to their faces
what they had done, and what God was going to do
to them if they failed to repent. They were very
specific in their charges against the rulers of the day.
But if men can sin in high places, they must he sin-
