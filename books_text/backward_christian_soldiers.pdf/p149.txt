SHEPHERDS AND SHEEP 135

ever, is only part of the story. It is not just that
denominations have not cooperated well. {t is also
that the individual churches have fragmented inter-
nally, The centralists have tended to become full-
time bureaucrats seeking power, and the decentralized
pastors, teachers, and other Christian workers have
tended to go their own way, leaving the tedious
affairs of central administration to those with a taste
for it, Those with a taste for bureaucratic adminis-
tration seldom have a taste for creeds, theology, and
{nonhumanistic) imnovation, all of which are con-
troversial, and all of which tend to reduce the powers
of bureaucracies. Hierarchies have strangled culture-
aliering Christian innovation.

What is needed is a working jederalism, armong
Christian groups and within each group. What is
needed is decentralization, yet with sufficient will-
ingness on the part of the “eyes” to recognize the im-
portance of the “feet,” and with all acknowledging
the authority of the “head,” Jesus Christ (I Corin-
thians 12). We need multiple responsibilities governed.
by biblical revelation and the leading of the Holy
Spirit. What is needed is a vision of conquest so great that
we must co-operaie.

The cost of gaining co-operation institutionally
has proven prohibitively high for centuries.
Churches haven't co-operated very often. They
haven’t agreed on policy, organization, and the
assignment of tasks. Unquestionably, this has been
an institutional failure on the part of institutional
Christianity. The hands and feet have looked to an
earthly head, and the Bible teaches that there is no
