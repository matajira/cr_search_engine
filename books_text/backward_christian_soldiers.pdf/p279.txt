CONCLUSION 265.

tian Reconstruction Bible study is not to become any
sort of initiatory secret society. But a tightly knit,
highly personal, three-person group is just a good
way to spread the message.

These meetings will not eat up a person's time,
because members agree in advance never to con-
tinue the meetings beyond one hour. This allows
each member (along with spouses and other affected
persons) to plan his day in terms of a predictable
schedule for Bible discussions. If people indulge
themselves and spend additional hours in unplanned
discussions, someone will grow resentful: members
of the group, spouses, children of participants, or
others who are in some way dependent on the par-
ticipants, Warning: Christian Reconstruction Bible
studies already have enough people upset at the
basic idea of broad Christian responsibility; there is
no use in creating additional resentment.

Each discussion group member, after about six
months, should begin recruiting his own group.
Take what you have learned over half a year of study
and begin to teach others. Get the division of labor
going. Don’t rush into a leadership position until you
are fairly confident that you will not embarrass your-
self or the word of God. But don’t hesitate forever,
searching for perfection. Start small. Despise not the
day of smail beginnings.

It may be that you are not ready to begin a discus-
sion group. Maybe you just don’t have the time to
read more newsletters. Maybe Bible reading is a
chore for you. Perhaps you are just too busy making
aliving or whatever to devote time to a consideration
