SMALL BEGINNINGS 261

continually returning to the monopolistic hybrid
seed sellers. It is not a resilient variety of grass.

Grass-roots organizations are all around us. Not
one; many. They may co-operate with others for
limited ends, but they have their own timetables,
resources, and goals. They are competitive. Not all
will survive; some will. Those that do survive will
replace the existing structures of society, all over the
world. Humanism is a worldwide phenomenon; it
will collapse as a worldwide phenomenon, to be
replaced by numerous alternatives.

CONCLUSION The apparent ineffectiveness of
small, underfunded ideological or religious organiza-
tions is deceptive. All long-term social change comes
from the successful efforts of one or another struggling
organization to capture the minds of a hard core of
future leaders, as well as the respect of a wider
population. There is no other way to change a society.
The hope of stepping into power overnight without
planning is naive, let alone the hope of getting
financial support from the existing leadership.

The Hebrews of Joshua’s generation wandered in
the wildemess for 40 years until their parents died.
They had to prepare themselves mentally and organi-
zationally for the battle to come. They certainly did not
bother to court the favor of the king of Jericho, nor did
they worry too much that the Levites had not graduated
from fully accredited Baal Theological Seminary. If we
only recognized our wilderness condition for what it is,
we might not continue to make the mistakes in strategy
that the Hebrews of Joshua’s day didn’t make.
