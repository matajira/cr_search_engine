THE LAST DAYS 41

Now the Spirit expressly says that in latter
times some will depart from the faith, giving
heed to deceiving spirits and doctrines of
demons, speaking lies in hypocrisy, having
their own conscience seared with a hot iron,
forbidding to marry, and commanding to ab-
stain from foods which God created to be re-
ceived with thanksgiving by those who believe
and know the truth.

Was St. Paul talking about “latter times” which
would happen thousands of years later? Why should
he warn Timothy of events which Timothy, and
Timothy’s great-great-grandchildren, and fifty or
more generations of descendants, would never live to
see? In fact, St. Paul tells Timothy, “If you instruct
the brethren in these things, you will be a good min-
ister of Jesus Christ” (I Timothy 4:6). The members
of Timothy’s congregation needed to know about
what would take place in the “latter days,” because
they would be personally affected by those events. In
particular, they needed the assurance that the com-
ing apostasy was part of the overall pattern of events
leading up to the end of the old order and the full es-
tablishing of Christ’s Kingdom. As we can see from
passages such as Colossians 2:18-23, the “doctrines of
demons” St. Paul warned of were current during the
first century. The “latter times” were already taking
place. This is quite clear in St. Paul's later statement
to Timothy:

But know this, that in the last days perilous
times will come; for men will be lovers of them-
