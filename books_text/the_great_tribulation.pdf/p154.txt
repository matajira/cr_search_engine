438 THE GREAT TRIBULATION

of the drying of the Euphrates for a conquering army
is taken, in part, from a stratagem of Cyrus the Per-
sian, who conquered Babylon by temporarily turning
the Euphrates out of its course, enabling his army to
march up the riverbed into the city, taking it by sur-
prise. The more basic idea, of course, is the drying
up of the Red Sea (Exodus 14:21-22) and the Jordan
River (Joshua 3:9-17; 4:22-24) for the victorious
people of God. Again there is the underlying note of
tragic irony: Israel has become the new Babylon, an
enemy of God that must now be conquered by a new
Cyrus, as the true covenant people are miraculously
delivered and brought into their inheritance. The
coming of the armies from the Euphrates, of course,
represents the final siege of Jerusalem by the armies
of Titus; and it is certainly more than coincidental
that thousands of these very troops actually did come
from the Euphrates.

In verses 13-14 of Revelation 16, St. John records
the appearance of three unclean spirits proceeding
out of the mouths of the Dragon, the Beast, and the
False Prophet (the “Land Beast,” or leadership of
Israel, spoken of in Revelation 13:11; cf. 19:20). A
connection with the second Egyptian plague is estab-
lished here, for the multitude of frogs that infested
Egypt came from the river (Exodus 8:1-7). St. John
has combined these images in these verses: first, an
invasion from a river (v. 12); second, a plague of
frogs (in the Old Covenant dietary laws, frogs are
unclean: Leviticus 11:9-12, 41-47). Third, these
“frogs” are really spirits of demons, performing signs
in order to deceive mankind. There is a multiple em-
