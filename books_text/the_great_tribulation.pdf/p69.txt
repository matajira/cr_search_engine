5

THE COMING OF
THE NEW COVENANT

We have seen in the preceding chapters how the
message of Jerusalem’s approaching desolation is
central to the concerns of the New Testament. The
Book of Revelation is no exception to this. It specifi-
cally states, in the very first verse, that its concerns
are not with the far distant future and the end of the
world, but rather with “the things that must shortly
take place.” In the third verse its readers are warned
that “the time is near” for its prophecies to be ful-
filled. Both of these statements are repeated at the
end of the book as well (see Revelation 22:6, 10).
And its prophecies are clearly — though often in sym-
bolic form—directed against “the Great City .. .
where the Lord was crucified” (Revelation 11:8; cf.
14:8; 16:19; 17:18). Like the rest of the New Testament,
the Book of Revelation follows Christ’s example in
foretelling the destruction of Jerusalem in a.p. 70.

As I have explained at length in my commentary,
The Days of Vengeance, St. John wrote Revelation in
