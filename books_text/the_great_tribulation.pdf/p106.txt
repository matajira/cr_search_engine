90 THE GREAT TRIBULATION

angels. Why? Because he had already introduced
seven “angels,” or pastors, in Revelation 2-3. They
are the ones represented here, even if we grant that
the two sets of “seven angels” are not necessarily iden-
tical. They are clearly meant to be related to each
other, as we can see when we step back from the text
{and our preconceived ideas) and allow the whole
picture to present itself to us. When we do this, we
see the Revelation structured in sevens, and in re-
curring patterns of sevens. One of those recurring
patterns is that of seven angels (chapters 1-3, 8-11, 14,
15-16). Just as earthly worship is patterned after
heavenly worship (Hebrews 8:5; 9:23-24), so is the
government of the Church (Matthew 16:19; 18:18;
John 20:23); moreover, according to Scripture, there
are numerous correspondences between human and
angelic activities (cf. Revelation 21:17). Angels are
present in the worship services of the Church
(I Corinthians 11:10; Ephesians 3:10)—or, more pre-
cisely, on the Lord’s Day we are gathered in worship
around the throne of God, in the heavenly court.
Thus we are shown in the Book of Revelation
that the government of the earthly Church corresponds to
heavenly, angelic government, just as our official worship
corresponds to that which is conducted around the
heavenly throne by the angels. Moreover, the judg-
ments that fall down upon the Land are brought through the
actions of the seven angels (again, we cannot divorce the
human angels from their heavenly counterparts).
The officers of the Church are commissioned and
empowered to bring God’s blessings and curses into
fruition in the earth. Church officers are the divinely ap-
