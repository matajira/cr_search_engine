ALL GREATION TAKES VENGEANCE 127

While the primary significance of this plague is
symbolic, referring to the uncleanness of contact
with blood and death (cf. Leviticus 7:26-27;
15:19-33; 17:10-16; 21:1; Numbers 5:2; 14:11-19), there
are close parallels in the actual events of the Great
Tribulation. On one occasion, thousands of Jewish
rebels fled to the Sea of Galilee from the Roman
massacre of Tarichaeae. Setting out on the lake in
small, flimsy boats, they were soon pursued and
overtaken by the sturdy rafts of Vespasian’s superior
forces. Then, as Josephus recounts, they were mer-
cilessly slaughtered: “The Jews could neither escape
to land, where all were in arms against them, nor
sustain a naval battle on equal terms. . . . Disaster
overtook them and they were sent to the bottom,
boats and all. Some tried to break through, but the
Romans could reach them with their lances, killing
others by leaping upon the barks and passing their
swords through their bodics; sometimes as the rafts
closed in, the Jews were caught in the middle and
captured along with their vessels. If any of those who
had been plunged into the water came to the surface,
they were quickly dispatched with an arrow or a raft
overtook them; if, in their extremity, they attempted
to climb on board the enemy’s rafts, the Romans cut
off their heads or their hands. So these wretches died
on every side in countless numbers and in every pos-
sible way, until the survivors were routed and driven
onto the shore, their vessels surrounded by the
enemy. As they threw themselves on them, many
were speared while still in the water; many jumped
ashore, where they were killed by the Romans.
