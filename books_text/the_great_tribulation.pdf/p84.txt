68 THE GREAT TRIBULATION

3:3-4; cf. Revelation 1:16; 4:5), bringing pestilence
and plague (Habakkuk 3:5; Revelation 6:8), shatter-
ing the mountains and collapsing the hills (Habak-
kuk 3:6, 10; Revelation 6:14), riding on horses
against His enemies (Habakkuk 3:8, 15; Revelation
6:2, 4-5, 8), armed with a Bow (Habakkuk 3:9, i;
Revelation 6:2), extinguishing sun and moon
(Habakkuk 3:11; Revelation 6:12-13) and trampling
the nations in His fury (Habakkuk 3:12; Revelation
6:15). Habakkuk clearly interprets his imagery as a
prophecy of the military invasion of Judah by the
Chaldeans, God’s heathen instruments of divine
wrath (Habakkuk 3:16; cf. 1:5-17). Under similar im-
agery, St. John portrays Israel’s destruction at the
hands of the invading Edomite and Roman armies.

The White Horse

The Book-visions begin, as the Messages did,
with Christ holding a cluster of seven in His hand.
As the Lamb breaks each of the first four Seals, St.
John hears one of the four living creatures saying as
with a voice of thunder, “Come!” This is not spoken
as a direction to St. John to “come and see.” It is,
rather, that each of the living creatures calls forth
one of the Four Horsemen. The four corners of the
earth, as it were, standing around the altar, are call-
ing for God’s righteous judgments to come and de-
stroy the wicked—just as the apostolic Church’s
characteristic cry for judgment and salvation was
Maranatha! O Lord, Come!~and bring Anathema!
(Early Christian documents indicate that this phrase
from I Corinthians 16:22 was repeated in the closing
