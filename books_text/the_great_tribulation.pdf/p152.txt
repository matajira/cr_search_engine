186 THE GREAT TRIBULATION

Temple of Jerusalem and the Temple of the great
Latin god” (The Early Days of Christianity, pp. 555f.).
One passage from Tacitus, the Roman historian,
provides some idea of the chaotic conditions in the
capital city: “Close by the fighting stood the people of
Rome like the audience at a show, cheering and
clapping this side or that in turns as if this were a
mock battle in the arena, Whenever one side gave
way, men would hide in shops or take refuge in some
great house. They were then dragged out and killed
at the instance of the mob, who gained most of the
loot, for the soldiers were bent on bloodshed and
massacre, and the booty fell to the crowd.

“The whole city presented a frightful caricature
of its normal self: fighting and casualties at one
point, baths and restaurants at another, here the
spilling of blood and the litter of dead bodies, close
by prostitutes and their like—all the vice associated
with a life of idleness and pleasure, all the dreadful
deeds typical of a pitiless sack. These were so inti-
mately linked that an observer would have thought
Rome in the grip of a simultaneous orgy of violence
and dissipation. There had indeed been times in the
past when armies had fought inside the city, twice
when Lucius Sulla gained control, and once under
Ginna. No less cruelty had been displayed then, but
now there was a brutish indifference, and not even a
momentary interruption in the pursuit of pleasure.
As if this were one more entertainment in the festive
season, they gloated over horrors and profited by
them, careless which side won and glorying in the
calamities of the state” (The Histories, iii. 83),
