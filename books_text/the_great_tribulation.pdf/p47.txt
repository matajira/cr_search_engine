THE COMING OF THE ANTICHRIST 31

became an increasing subject of apostolic counsel
and pastoral oversight as the age progressed. Some
heretics taught that the final Resurrection had
already taken place (II Timothy 2:18), while others
claimed that resurrection was impossible (I Corin-
thians 15:12); some taught strange doctrines of ascet-
icism and angel-worship (Colossians 2:8, 18-23; 1
Timothy 4:1-3), while others advocated all kinds of
immorality and rebcllion in the name of “liberty” (IE
Peter 2:1-3, 10-22; Jude 4, 8, 10-13, 16). Again and
again the apostles found themselves issuing stern
warnings against tolerating false teachers and “false
apostles” (Romans 16:17-18; II Corinthians i1:3-4,
12-15; Philippians 3:18-19; I Timothy 1:3-7; II Tim-
othy 4:2-5), for these had been the cause of massive
departures from the faith, and the extent of apostasy
was increasing as the era progressed (I Timothy
1:19-20; 6:20-21; II Timothy 2:16-18; 3:1-9, 13; 4:10,
14-16). One of the last letters of the New Testament,
the Book of Hebrews, was written to an entire Chris-
tian community on the very brink of wholesale aban-
donment of Christianity. The Christian Church of
the first generation was not only characterized by
faith and miracles; it was also characterized by in-
creasing lawlessness, rebellion, and heresy from
within the Christian community itself—just as Jesus
had foretold in Matthew 24,

The Antichrist
The Christians had a specific term for this apos-
tasy. They called it Antichrist. Many popular writers
have speculated about this term, usually failing to
