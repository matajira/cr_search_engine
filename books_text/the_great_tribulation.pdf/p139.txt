6.

7.

ALL CREATION TAKES VENGEANCE 123

On the Euphrates, drying it up to make
way for kings of the east; invasion of
frog-demons; Armageddon (16:12-16).
On the air, causing storm, earthquake,
and hail (16:17-21).

Trumpets

4.
2.
3.
4.

5.
6.

7.

On the Land; % earth, trees, grass
burned (8:7).

On the sea; % sea becomes blood, % sea
creatures die, 4 ships destroyed (8:8-9).
On the rivers and springs, 4% waters
become wormwood (8:10-11),

Y% of sun, moon, and stars darkened
(8:12).

Demonic locusts tormenting men (9:1-12).
Army from Euphrates kills 4% mankind
(9:13-21).

Voices, storm, earthquake, hail (11:15-19).

Plagues on Egypt
1.

2.
3.

7.

Boils (sixth plague: Exodus 9:8-12).
Waters become blood (first plague:
Exodus 7:17-21).
Waters become blood (first plague:
Exodus 7:17-21).

4. Darkness (ninth plague: Exodus 10:21-23).
5.
6. Invasion of frogs from river (second

Locusts (eighth plague: Exodus 10:4-20).

plague: Exodus 8:2-4).
Hail (seventh plague: Exodus 9:18-26).

“A loud voice from the Temple” issues the com-
mand authorizing the Chalice-judgments (Revela-
