THE FOUR HORSEMEN 73

symbolizes His conquest of the nations after a.p. 70
with the Gospel. But that is not in view during the
breaking of the Seals. Here, Christ is coming against
His enemies in judgment. He is coming, not to save,
not to heal, but to destroy. The awful and terrifying
riders who follow Him are not messengers of hope
but of wrath. Israel is doomed.

The Red Horse

As the Lamb breaks the second Seal (Revelation
6:3-4), St. John hears the second living creature say-
ing, “Come!” In answer, a rider on a “blood-red”
horse comes forth, who is granted by God the power
“to take peace from the Land, and that men should
slay one another; and a great sword is given to him.”
This second rider, standing for war, shows how utterly
depraved man is. God does not have to incite men to fight
against cach other; He simply orders His angels to take away
the conditions of peace.

Tn a sinful world, why are there not more wars
than there are? Why is there not more bloodshed? It
is because there are restraints on man’s wickedness, on
man’s freedom to work out the consistent implica-
tions of his hatred and rebellion. But if God removes
the restraints, man’s ethical degeneracy is revealed
in all its ugliness. John Calvin wrote: “The mind of
man has been so completely estranged from God's
righteousness that it conceives, desires, and under-
takes, only that which is impious, perverted, foul,
impure, and infamous. The heart is so steeped in the
poison of sin, that it can breathe out nothing but a
loathsome stench. But if some men occasionally make
