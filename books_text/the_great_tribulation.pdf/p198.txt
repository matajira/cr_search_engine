182 THE GREAT TRIBULATION

But if we mix our salt with corruption, as Lot’s wife
did, then we become covenantally dead salt, cor-
rupted salt, and useless to God.

Salt was a required aspect of God’s sacrificial
system.

And on the second day thou shalt offer a kid
of the goats without blemish for a sin offering;
and they shall cleanse the altar, as they did
cleanse it with the bullock. When thou hast
made an end of cleansing it, thou shalt offer a
young bullock without blemish, and a ram out
of thy flock without blemish. And thou shalt
offer them before the Lorn, and the priests shall
cast salt upon them, and they shall offer them up
for a burnt offering unto the Lorp (Ezek.
43:22-24).

There must always be salt on the altar. Chris-
tians are that salt. In their sin-free resurrected bod-
ies, they will serve as eternal salt for God’s eternal
altar. There will always be a sacrifice on that altar,
just as surely as there will always be a Church, God’s
holy salt. On that fiery altar judgment will burn for
as long as the Church shall exist. There can be no
acceptable sacrifices without salt. God will not
tolerate salt-free sacrifices. He will preserve His
Church, for He will always preserve His altar. His
law is perpetual, His justice is perpetual, and His
judgment is everlasting, both blessings and cursings.
