‘THE FOUR HORSEMEN 77

and more comprehensive commission, is named
Death; and he is followed by Hades (the grave)—
both having been set loose by the Son of Man, who
unlocked them with His key (see Revelation 1:18).
Authority is given to him to bring four plagues upon
the four-cornered Land: “to kill with sword and with
famine and with death and by the wild beasts of the
Land.” This is simply a summary of all the cove-
nantal curses for apostasy in Leviticus 26 and Deu-
teronomy 28. Moreover, it parallels God’s listing of
His four basic categories of curses with which He
punishes ungodly and disobedient nations—“My
four severe judgments against Jerusalem: sword,
famine, wild beasts, and plague to cut off man and
beast from it!” (Ezekiel 14:21; cf. Ezekiel 5:17). At
this preliminary stage, however—and in keeping
with the “fourness” of the passage as a whole— Death
and the grave are given authority to swallow up only
a fourth of the Land. The Trumpet-judgments will
take a third of the Land (Revelation 8:7-12), and the
Chalice-judgments will devastate it all.

Conclusion

Perhaps the most significant obstacle to a correct
interpretation of this passage has been that commen-
tators and preachers have been afraid and unable to
see that it is God who is bringing forth these judg-
ments upon the Land—that they are called forth
from the Throne, and that the messengers of judg-
ment are the very angels of God. Especially vicious
and harmful is any interpretation which seems to pit
the Son of God against the court of heaven, so that
