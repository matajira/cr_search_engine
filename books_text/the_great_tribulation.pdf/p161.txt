ITISFINISHED! 146

quake, and so great” (cf. Matthew 24:21; Exodus
9:18, 24; Daniel 12:1; Joel 2:1-2}.

This was also the message of the writer to the
Hebrews. Comparing the covenant made at Sinai
with the coming of the New Covenant (which would
be established at the destruction of the Temple and
the complete passing of the Old Covenant), he an-
nounced that the “heavens and earth” of the Mosaic
economy were passing away, having been replaced
by Christ’s eternal Kingdom:

See to it that you do not refuse Him who is
speaking. For if those did not escape when they
refused Him who warned them on earth, much
less shall we escape who turn away from Him
who warns from heaven. And His Voice shook
the earth then, but now He has promised, say-
ing: Yet once more I will shake not only the earth, but
also the heaven [Haggai 2:6}. And this expres-
sion, “Yet once more,” denotes the removing of
those things that can be shaken, as of created
things, in order that those things that cannot be
shaken may remain. Therefore, since we receive
a Kingdom that cannot be shaken, let us show grati-
tude, by which we may offer to God an accept-
able service with reverence and awe; for our
God is a consuming fire (Hebrews 12:25-29).

St. John has made it clear that “the Great City” is
the Old Jerusalem, where the Lord was crucified
(Revelation 11:8; cf. 14:8); originally intended to be
“the light of the world, a City set on a hill,” she is
