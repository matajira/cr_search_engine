106 (THE GREAT TRIBULATION

ing in constant torment of the population.)

Second, this seems to refer in part to the actions
of Gessius Florus, the procurator of Judea, who for a
five-month period (beginning in May of 66 with the
slaughter of 3,600 peaceful citizens) terrorized the
Jews, deliberately seeking to incite them to rebel-
lion, He was successful: Josephus dates the begin-
ning of the Jewish War from this occasion.

Third, the use of the term fize is associated in
Scripture with power, and specifically with military
organization—the arrangement of the Israelite mili-
tia in a five-squad platoon formation (Exodus 13:18;
Numbers 32:17; Joshua 1:14; 4:12; Judges 7:11; cf. IT
Kings 1:9ff.). By God’s direction, Israel was to be
attacked by a demonic army from the Abyss.

During the ministry of Christ, Satan had fallen
to the earth like “a star from heaven” (cf. Revelation
12:4, 9, 12); and, St. John says, “the key of the well of
the Abyss was given to him. And he opened the well
of the Abyss.” What all this means is exactly what
Jesus prophesied during His earthly ministry: the
Land, which had received the benefits of His work
and then rejected Him, would become glutted with
demons from the Abyss. We should note here that
the key is given to Satan, for it is God who sends the
demons as a scourge upon the Jews.

The men of Nineveh shall stand up with
this generation at the judgment, and shall con-
demn it because they repented at the preaching
of Jonah; and behold, something greater than
Jonah is here. The Queen of the South shall
