THE BOOK IS OPENED 103

are becoming intensified, and more precise in their
application, St. John is building up to a crescendo,
using the three woes of the Eagle (corresponding to
the fifth, sixth, and seventh blasts of the Trumpet;
ef. Revelation 9:12; 11:14-15) to dramatize the in-
creasing disasters being visited upon the Land of
Israel. After many delays and much longsuffering by
the jealous and holy Lord of Hosts, the awful sanc-
tions of the Law are finally unleashed against the
covenant-breakers, so that Jesus Christ may inherit
the kingdoms of the world and bring them into His
Temple (Revelation 11:15-19; 21:22-27).
