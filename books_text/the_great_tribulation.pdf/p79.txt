‘THE COMING OF THE NEW COVENANT 63

the evil Beast of the pagan Roman Empire. These
Christians needed to understand history as some-
thing not ruled by chance or evil men or even the
devil, but ruled instead from God’s Throne by Jesus
Christ. They needed to see that Christ was reigning
now, that He had already wrested the world from
Satan’s grasp, and that even now all things in heaven
and earth were bound to acknowledge Him as King.
They necded to see themselves in the true light: not
as forgotten troops in a lonely outpost fighting a los-
ing battle, but as kings and priests already, waging
war and overcoming, predestined to victory, with the
absolute assurance of conquest and dominion with
the High King over the carth. They needed the Bibli-
cal philosophy of history: that all of history, created and con-
trolled by God's personal and total government, is moving in-
exorably toward the universal dominion of the Lord Jesus
Christ. The new and final age of history has arrived;
the New Covenant has come. Behold, He has conquered!
