142 THE GREAT TRIBULATION

the text of the sixth Chalice continues: “Blessed is the
one who stays awake and keeps his garments, Jest he
walk about naked and men see his shame” (cf. Rev-
elation 3:18, in the Letter to Laodicea: “I advise you
to buy from Me . . . white garments, that_you may clothe
pourself, and that the shame of your nakedness may not be
revealed”).

The symbolism of this is based on the punish-
ment for those Temple guards whe fell asleep while
on duty: their clothes were confiscated and burned.
Christ is rebuking the guardians of Israel for their
Spiritual sloth, warning that they are about to be
cast out of office when He comes in judgment. They
fell asleep, and now it is too late—the Temple is
going to be attacked and destroyed. Judgment and
destruction are approaching rapidly; there is no time
left to waste, and the churches must be awake and
on the alert.

St. John picks up the story again in verse 16: the
demons gather the kings of earth together “to the
place which in Hebrew is called Armageddon.” Lit-
erally, this is spelled Har-Magedon, meaning Mount
Megiddo. A problem for “literalists” arises here, for
Megiddo is a city on a plain—not a mountain! There
never was or will be a literal “Battle of Armageddon,” for
there is no such place. The mountain nearest to the
plain of Megiddo is Mount Carmel, and this is pre-
sumably what St. John had in mind. Why didn’t he
simply say “Mount Carmel”? Probably because he
wanted to bring both ideas together — Carmel because
of its association with the defeat of Jezebel’s false
prophets, and Megiddo because it was the scene of
