36 THE GREAT TRIBULATION

One of the Antichrists who afflicted the early
Church was Cerinthus, the leader of a first-century
Judaistic cult, Regarded by the Church Fathers as
“the Arch-heretic,” and identified as one of the “false
apostles” who opposed St. Paul, Cerinthus was a Jew
who joined the Church and began drawing Chris-
tians away from the orthodox faith. He taught that a
lesser deity, and not the true God, had created the
world (holding, with the Gnostics, that God was’
much too “spiritual” to be concerned with material
reality). Logically, this meant also a denial of the In-
carnation, since God would not take to Himself a
physical body and truly human personality. And
Cerinthus was consistent: he declared that Jesus had
merely been an ordinary man, not born of a virgin;
that “the Christ” (a heavenly spirit) had descended
upon the man Jesus at His baptism (enabling Him
to perform miracles), but then left Him again at the
crucifixion. Cerinthus also advocated a doctrine of
Justification by works—in particular, the absolute
necessity of observing the ceremonial ordinances of
the Old Covenant in order to be saved.

Furthermore, Cerinthus was apparently the first
to teach that the Second Coming would usher in a
literal reign of Christ in Jerusalem for a thousand
years. Although this was contrary to the apostolic
teaching of the Kingdom, Cerinthus claimed that an
angel had revealed this doctrine to him (much as
Joseph Smith, a 19th-century Antichrist, would later
claim to receive angelic revelation).

The true apostles sternly opposed the Cerinthian
heresy. St. Paul admonished the churches: “But even
