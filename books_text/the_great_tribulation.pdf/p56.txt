4

THE LAST DAYS

As we began to see in the preceding chapter, the
period spoken of in the Bible as “the last days” (or
“last times” or “last hour”) is the period between Christ's
birth and the destruction of Jerusalem. The early Church
was living at the end of the old age and the begin-
ning of the new. This whole period must be consid-
ered as the time of Christ’s First Advent. In both the
Old and New Testaments, the promised destruction
of Jerusalem is considered to be an aspect of the
work of Christ, intimately connected to His work of
redemption. His life, death, resurrection, ascension,
outpouring of the Spirit, and judgment on Jerusalem
are all parts of His one work of bringing in His Kingdom
and creating His new Temple (see, for example, how
Daniel 9:24-27 connects the atonement with the de-
struction of the Temple).

Let’s consider how the Bible itself uses these ex-
pressions about the end of the age. In I Timothy
4:1-3, St. Paul warned:
