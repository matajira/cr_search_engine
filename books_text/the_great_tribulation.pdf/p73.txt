‘THE COMING OF THE NEW COVENANT 57

of the Book are broken in order to reveal the Book’s
contents; but the breaking of the Seventh Seal initi-
ates the sounding of the Seven Trumpets (8:1-2),
The final vision of the Trumpets-section closes with a
horrifying scene of a great Vintage, in which human
“grapes of wrath” are trampled and the whole Land
is flooded with a torrent of blood (14:19-20). This
leads directly into the final section of Revelation, in
which St. John sees the blood from the Winepress
being poured out from the Seven Chalices of wrath
(46:1-21), It would seem, therefore, that we are
meant to understand the Seven Chalices as the con-
tent of the Seventh Trumpet, “the last Woe” to fall
upon the Land (cf. 8:13; 9:12; 11:14-15; 12:12). All of
these —Seals, Trumpets, and Chalices— are the con-
tents of the seven-sealed Book, the New Covenant.

But there is a crisis: St. John finds that no one in
all of creation —“in heaven, or on the earth, or under
the earth”—is able or worthy to open the Book, or
even to look into it. No one can fulfill the conditions
required of the Mediator of the New Covenant. All
previous mediators— Adam, Moses, David, and the
rest—had ultimately proved inadequate for the task.
No one could take away sin and death; for all have
sinned, and continually fall short of the Glory of God
(Romans 3:23). The sacrifice of animals could not
really take away sins, for such a thing is impossible
(Hebrews 10:4); and the high priest who offered up
the sacrifices was a sinner himself, “beset with weak-
ness” (Hebrews 5:1-3; 7:27) and having to be replaced
after his death (Hebrews 7:23). No one could be
found to guarantee a better covenant. With the pro-
