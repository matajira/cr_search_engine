16 THE GREAT TRIBULATION

lary; There was a “language” of prophecy, instantly
recognizable to those familiar with the Old Testa-
ment. As Jesus foretold the complete end of the Old
Covenant system — which was, in a sense, the end of
a whole world—He spoke of it as any of the prophets
would have, in the stirring language of covenantal
judgment. We will consider each element in the
prophecy, seeing how its previous use in the Old Tes-
tament prophets determined its meaning in the con-
text of Jesus’ discourse on the fall of Jerusalem.
Remember that our ultimate standard of truth is the
Bible, and the Bible alone.

The Sun, Moon, and Stars

At the end of the Tribulation, Jesus said, the uni-
verse will collapse: the light of the sun and the moon
will be extinguished, the stars will fall, the powers of
the heavens will be shaken. The basis for this sym-
bolism is in Genesis 1:14-16, where the sun, moon,
and stars (“the powers of the heavens”) are spoken of
as “signs” which “govern” the world. Later in Scrip-
ture, these heavenly lights are used to speak of
earthly authorities and governors; and when God.
threatens to come against them in judgment, the
same collapsing-universe terminology is used to
describe it. Prophesying the fall of Babylon to the
Medes in 539 8.c., Isaiah wrote:

Behold, the Day of the Lorp is coming,
Cruel, with fury and burning anger,

To make the land a desolation;

And He will exterminate its sinners from it.
