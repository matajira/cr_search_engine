JERUSALEM UNDER SIEGE 115

were assembling for the Feast of Unleavened Bread,
on the eighth of the month of Nisan, at the ninth
hour of the night [3:00 a.m.} so bright a light shone
round the altar and Temple that it looked like broad daylight;
and this lasted for half an hour. The inexperienced
regarded it as a good omen, but it was immediately
interpreted by the sacred scribes in conformity with
subsequent events.”

During the same feast another shocking event
took place: “The east gate of the inner sanctuary was
a very massive gate made of brass and so heavy that
it could scarcely be moved every evening by twenty
men; it was fastened by iron-bound bars and secured
by bolts that were sunk very deep into a threshold
that was fashioned from a single stone block; yet ¢his
gate was seen to open of tts own accord at the sixth hour of the
night [midnight]. The Temple guards ran and re-
ported the news to the captain and he came up and
by strenuous efforts managed to close it. To the un-
imitiated this also appeared to be the best of omens as
they had assumed that God had opened to them the
gate of happiness. But wiser people realized that the
security of the Temple was breaking down of its own accord
and that the opening of the gates was a present to the
enemy; and they interpreted this in their own minds
as a portent of the coming desolation.”

A similar event, incidentally, happened in a.p.
30, when Christ was crucified and the Temple’s
outer veil—24 feet wide and over 80 feet. high! —
ripped from top to bottom (Matthew 27:50-54;
Mark 15:37-39; Luke 23:44-47). The Talmud (Yoma
39b) records that in a.p. 30 the gates of the Temple
