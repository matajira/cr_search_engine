800 THE GREAT TRIBULATION

vened in order to render an authoritative ruling on
the issue of justification by faith (some teachers had
been advocating the false doctrine that one must
keep the ceremonial laws of the Old Testament in
order to be justified). The problem did not die down,
however; years later, the apostle Paul had to deal
with it again, in his letter to the churches of Galatia.
As St. Paul told them, this doctrinal aberration was
no minor matter, but affected their very salvation: it
was a “different gospel,” an utter distortion of the
truth, and amounted to a repudiation of Jesus Christ
Himself. Using some of the most severe terminology
of his career, Paul pronounced damnation upon the
“false brethren” who taught the heresy (see Galatians
1:6-9; 2:5, 11-21; 3:1-3; 5:1-12).

St. Paul also foresaw that heresy would infect the
churches of Asia Minor. Calling together the elders
of Ephesus, he exhorted them to “be on guard for
yourselves and for all the flock,” because “I know that
after my departure savage wolves will come in
among you, not sparing the flock; and from among
your own selves men will arise, speaking perverse
things, to draw away the disciples after them” (Acts
20:28-30). Just as St. Paul predicted, false doctrine
became an issue of enormous proportions in these
churches. By the time the Book of Revelation was
written, some of them had become almost com-
pletely ruined through the progress of heretical
teachings and the resulting apostasy (Revelation
2:2, 6, 14-16, 20-245 3:1-4, 15-18),

But the problem of heresy was not limited to any
geographical or cultural area. It was widespread and
