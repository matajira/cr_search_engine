118 THE GREAT TRIBULATION

upon the city, and upon themselves also.”

Israel’s idols, St, John says, are “of gold and of sil-
ver and of brass and of stone and of wood,” a standard
Biblical accounting of the materials used in the con-
struction of false gods (cf. Psalm 115:4; 135:15; Isaiah
37:19). The Bible consistently ridicules men’s idols as
the works of their hands, mere sticks and stones
which can neither see nor hear nor walk. This is an
echo of the Psalmist’s mockery of heathen idols:

They have mouths, but they cannot speak;
They have eyes, but they cannot see;

They have ears, but they cannot hear;
They have noses, but they cannot smell;
They have hands, but they cannot feel;
They have feet, but they cannot walk;
They cannot make a noise with their throat.

Then comes the punchline:

Those who make them will become like them,
Everyone who trusts in them
(Psalm 115:5-8; cf. 135:16-18).

Herbert Schlossberg has aptly called this reverse
Ssanclification—a process by which “the idolater is
transformed into the likeness of the object of his wor-
ship. Israel ‘went after worthlessness, and became
worthless’” (Idols for Destruction, p. 295). As the
prophet Hosea thundered, Israel’s idolaters “became
as detestable as that which they loved” (Hosea 9:10;
cf, Jeremiah 2:5).
