PUBLISHERS EPILOGUE 193

Sadly, pessimistic Christians who expect litile
but defeat for God’s people cling to faith in natural
law as a neutral common ground between Satan’s
supposedly expanding influence in history and the
church’s supposedly decreasing influence. They see
God’s Bible-revealed Jaw as a threat to their retreat
from historic responsibility, so they choose to preach
an undefined (and always undefinable) “neutral nat-
ural law” which lays no uniquely Christian civic re-
sponsibilities on them.

Conclusion

The judgment of God on Israel in a.p. 70 should
persuade us of the futility of escaping God’s progres-
sive judgments in history. In our day, potentially the
greatest blessings since Pentecost are facing us:
worldwide revival, the information revolution of
computerization, and a rediscovery of God’s revealed
law as a tool of godly dominion (Gen. 1:26-28). In
our day, also potentially the worst curses since the
fall of Jerusalem are facing us: biological terrorisma
that would kill millions of residents in a large city or
cities, nuclear terrorism, or an attack on the west’s
telecommunications systems. We need to understand
God's judgment. it mvolves blessing and cursing.

God’s blessing is definitive: the grace of salvation in
Ghrist. His blessings are also progressive: promise of
the coming seed (Gen. 3:15) and His provision of
clothing for them, Noah’s ark, the exodus from
Egypt, the return to the land under Nehemiah and

Ezra, the resurrection of Christ, and the expansion
