20° THE GREAT TRIBULATION

And the moon shail not give its light.
All the shining lights in the heavens

I will darken over you

And will set darkness on your land,”
Declares the Lord Gop (Ezekiel 32:7-8).

It must be stressed that none of these events liter-
ally took place. God did not intend anyone to place a
literalist construction on these statements. Poetically,
however, all these things did happen: as far as these
wicked nations were concerned, “the lights went out.”
This is simply figurative language, which would not
surprise us at all if we were more familiar with the
Bible and appreciative of its literary character.

What Jesus is saying in Matthew 24, therefore, in
prophetic terminology immediately recognizable by
his disciples, is that the light of Israel is going to be ex-
tinguished; the covenant nation will cease to exist.
When the Tribulation is over, old Israel will be gone.

The Sign of the Son of Man

Most modern translations of Matthew 24:30
read something like this: “And then the sign of the
Son of Man will appear in the sky. . . .” That is a
mistranslation, based not on the Greek text but on
the translators’ own misguided assumptions about
the subject of this passage (thinking it is speaking
about the Second Coming). A word-for-word ren-
dering from the Greek actually reads:

And then will appear the sign of the Son of
Man in heaven...
