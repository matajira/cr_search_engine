‘THE TERMINAL GENERATION 16

then crucified them—at the rate of 500 per day.

“Let Him be crucified! Let Him be crucified! His
blood be on us, and on our children!” the apostates had
cried forty years earlier (Matthew 27:22-25); and
when it was all over, more than a million Jews bad
been killed in the siege of Jerusalem; close to a mil-
lion more were sold into slavery throughout the em-
pire, and the whole of Judea lay smoldering in ruins,
virtually depopulated. The Days of Vengeance had
come with horrifying, unpitying intensity. In break-
ing her covenant, the holy city had become the
Babylonish whore; and now she was a desert, “the
habitation of devils, and the hold of every foul spirit,
and a cage of every unclean and hateful bird” (Reve-
lation 18:2).
