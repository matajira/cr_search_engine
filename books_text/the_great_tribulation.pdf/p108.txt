92 THE GREAT TRIBULATION

nacle and the Temple were made ready (Leviticus
9:24; II Chronicles 7:1). This fire, started by God,
was kept burning by the priests, and was carried
from place to place so that it could be used to start
other holy fires (Leviticus 16:12-13; cf. Numbers
16:46-50; Genesis 22:6). Now, when Gad’s people
were commanded to destroy an apostate city, Moses
further ordered: “You shall gather all its booty into
the middle of its open square and burn all its booty
with fire as a whole burnt offering to the Lorp your
God” (Deuteronomy 13:16; Judges 20:40; cf. Genesis
19:28). The only acceptable way to burn a city as a
whole burnt sacrifice was with God’s fire — fire from the
altar. Thus, when a city was to be destroyed, the
priest would take fire from God’s altar and use it to
ignite the heap of booty which served as kindling, so
offering up the entire city as a sacrifice. It is this
practice of putting a city “under the ban,” so that
nothing survives the conflagration (Deuteronomy
13:12-18), that the Book of Revelation uses to
describe God’s judgment against Jerusalem.

God rains down His judgments upon the earth in
specific response to the liturgical worship of His peo-
ple. As part of the formal, official worship service in
heaven, the angel of the altar offers up the prayers of
the corporate people of God; and God.responds to
the petitions, acting into history on behalf of the
saints. The intimate connection between liturgy and
history is an inescapable fact, one which we cannot
afford to ignore. This is not to suggest that the world
is in danger of lapsing into “non-being” when the
Church’s worship is defective. In fact, God will use
