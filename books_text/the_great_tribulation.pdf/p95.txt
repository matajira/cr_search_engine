7

VENGEANCE FOR
THE MARTYRS

For the first-century readers of Revelation, the
tribulations depicted in it were becoming all too real:
each church would soon know the anguish of having
some of its most forthright and able leaders im-
prisoned and executed “because of the Word of God,
and because of the testimony which they had main-
tained” (Revelation 6:9). For many Christians, all
across the empire, the coming months and years
would involve great distress, as families would be
separated and loved ones killed. When tragedy
strikes, we are all tempted to ask: Does God care?
This question is especially intense when the pain is
caused by vicious enemies of the faith bent on de-
stroying God’s people, and the injustice of the suffer-
ing becomes apparent. If Christians were truly the
servants of the King, when would He act? When
would He come to punish the apostates who had first
used the power of the Roman State to crucify the
Lord, and now were using that same power to kill
and crucify the “prophets and wise men and scribes”
