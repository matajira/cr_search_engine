42 THE GREAT TRIBULATION

selves, lovers of money, boasters, proud, blas-
phemers, disobedient to parents, unthankful,
unholy, unloving, unforgiving, slanderers,
without self-control, brutal, despisers of good,
traitors, headstrong, haughty, lovers of pleas-
ure rather than lovers of God, having a form of
godliness but denying its power. And from such
people turn away! For of this sort are those who
creep into households and make captives of gul-
lible women loaded down with sins, led away by
various lusts, always learning and never able to
come to the knowledge of the truth. Now as Jan-
nes and Jambres resisted Moses, so also do these
resist the truth; men of corrupt minds, disap-
proved concerning the faith (II Timothy 3:1-8).

The very things St. Paul said would happen in “the last
days” were happening as he wrote, and he was simply
warning Timothy about what to expect as the age
rushed on to its climax. Antichrist was beginning to
rear its head.

Other New Testament writers shared this per-
spective with St. Paul. The letter to the Hebrews begins
by saying that God ‘has én these last days spoken to us
in His Son” (Hebrews 1:2); the writer goes on to
show that “now once af the end of the ages He has ap-
peared to put away sin by the sacrifice of Himself”
(Hebrews 9:26). St. Peter wrote that Christ “was
foreknown before the foundation of the world, but
has appeared in these last times for you who through
Him are believers in God” (I Peter 1:20-21). Apostolic
testimony is unmistakably clear: when Christ came,
