THELAST DAYS ST

against one another, brethren, lest you be
judged. Behold, the Judge is standing at the door!
{ James 5:7-9).

The Apostle Peter, too, warned the Church that
“the end of all things is at hand” (I Peter 4:7), and
encouraged them to live in the daily expectation of
the judgment that would come in their generation:

Beloved, do uot think it strange concerning
the fiery trial which is to try you, as though
some strange thing happened to you; but re-
joice to the extent you partake of Christ’s suffer-
ings, that when His glory is revealed, you may
also be glad with exceeding joy... . For the
time has come for judgment to begin at the
house of God; and if it begins with us first,
what will be the end of those whe do not obey
the gospel of God? (I Peter 4:12-13, 17).

The early Christians had to endure both severe
persecution at the hands of apostate Israel, and be-
trayal by Antichrists from their own midst who
sought to steer the Church into the Judaistic cult.
But this time of fiery tribulation and suffering was
working for the Christians’ own blessing and sancti-
fication (Romans 8:28-39); and in the meantime
God’s wrath against the persecutors was building up.
Finally, the End came, and God’s anger was un-
leashed. Those who had brought tribulation upon
