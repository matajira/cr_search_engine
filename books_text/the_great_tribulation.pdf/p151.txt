IT Is FINISHED! 135,

it becomes darkened—which is, as we saw in our
study of Matthew 24, a standard Biblical symbol for
political turmoil and the fall of rulers (cf. Isaiah
13:9-10; Amos 8:9; Ezekiel 32:7-8). The primary sig-
nificance of this plague is still the judgment on
Israel, for (in terms of the message of Revelation) she
was the “throne” and “kingdom” of the Beast. More-
over, as we shall see, the people who suffer from the
Fifth Chalice are identified as suffering as well from
the First Chalice, which was poured out upon the
Land, upon the Israelite worshipers of the Beast
(Revelation 16:2).

It is also likely, however, that this judgment par-
tially corresponds to the wars, revolutions, riots, and
“world-wide convulsions” that racked the Empire
after Nero committed suicide in June 68. The great
19th-century scholar F. W. Farrar wrote in this con-
nection of “the horrors inflicted upon Rome and
Romans in the civil wars by provincial governors —
already symbolized as the horns of the Wild Beast,
and here characterized as kings yet kingdomless.
Such were Galba, Otho, Vitellius, and Vespasian.
Vespasian and Mucianus deliberately planned to
starve the Roman populace; and in the fierce strug-
gle of the Vitellians against Sabinus and Domitian,
and the massacre which followed, there occurred the
event which sounded so portentously in the ears of
every Roman—the burning to the ground of the
Temple of the Capitoline Jupiter, on December 19th,
A.D. 69. It was not the least of the signs of the times
that the space of one year saw wrapped in flames the
two most hallowed shrines of the ancient world—the
