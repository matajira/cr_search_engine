100 THE GREAT TRIBULATION

water became bitter because of the multitude of dead
and decaying fish (Exodus 7:21), The bitterness of
the waters is caused by a great star that fell from
heaven, burning like a torch. This parallels Isaiah's
prophecy of the fall of Babylon, spoken in terms of
the original Fall from Paradise:

How you have fallen from heaven,

O star of the morning, son of the dawn!

‘You have been cut down to the earth,

You who have weakened the nations!

But you said in your heart,

I will ascend to heaven,

I will raise my throne above the stars of God,
And I will sit on the mount of assembly,

In the recesses of the north.

1 will'ascend above the heights of the clouds;
I will make myself like the Most High.
Nevertheless you will be thrust down to Sheol,
To the recesses of the pit (Isaiah 14:12-15).

‘The name of this fallen star is Wormwood, a term
used in the Law and the Prophets to warn Israel of
its destruction as a punishment for apostasy (Deuter-
onomy 29:18; Jeremiah 9:15; 23:15, Lamentations
3:15, 19; Amos 5:7). Again, by combining these Old
Testament allusions, St. John makes his point: Israel
is apostate, and has become an Egypt; Jerusalem has
become a Babylon; and the covenant breakers will
be destroyed, as surely as Egypt and Babylon were
destroyed.
