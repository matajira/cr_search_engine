ITB FINIGHED! 149

this case, the seventh plague: Exodus 9-18-26). The
plague of hailstones also calls up associations with
“the large stones from heaven” that God threw down
upon the Canaanites when the Land was being con-
quered under Joshua (Joshua 10:11); as Deborah
sang, the very stars of heaven make war against the
enemies of God ( Judges 5:20).

A specific historical referent of this “hailstorm”
may have been recorded by Josephus, in his strange
account of the huge stone missiles thrown by the
Roman catapults into the city: “The stone missiles
weighed a talent and traveled two furlongs or more,
and their impact not only on those who were hit first,
but also on those behind them, was enormous. At
first the Jews kept watch for che stone—for it was
white —and its approach was intimated to the eye by
its shining surface as well as to the ear by its whizz-
ing sound. Watchmen posted on the towers gave the
warnings whenever the engine was fired and the
stone came hurtling toward them, shouting in their
native tongue: ‘The Son is coming!’ Those in the line of
fire made way and fell prone, a precaution that re-
sulted in the stone’s passing harmlessly through and
falling in their rear. To frustrate this, it occurred to
the Romans to blacken the stones so that they could
not be seen so easily beforehand; then they hit their
target and destroyed many with a single shot’” (The
Jewish War, v. vi. 3).

After considering various theories about the
meaning of this phrase, commentator J. Stuart
Russell observed: “It could not but be well known to
the Jews that the great hope and faith of the Chris-
