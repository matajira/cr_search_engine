10 THE GREAT TRIBULATION

the fulfillment of a process, and the other a decisive
event:

6. Worldwide evangelization. “And this Gos-
pel of the Kingdom shali be preached in the
whole world for a witness to all the nations, and
then the end shall come” (v. 14).

At first glance, this seems incredible. Could the
Gospel have been preached to the whole world with-
in a generation of these words? The testimony of
Scripture is clear. Not only coudd it have happened,
but # actually did. Proof? A few years before the de-
struction of Jerusalem, Paul wrote to Christians in
Colossae of “the Gospel which has come to you, just
as in all the world also it is constantly bearing fruit and
increasing” (Colossians 1:5-6), and exhorted them
not to depart “from the hope of the Gospel that you
have heard, which was proclaimed in all creation under
heaven” (Colossians 1:23). To the church at Rome,
Paul announced that “your faith is being proclaimed
throughout the whole world” (Romans 1:8), for the
voice of Gospel preachers “has gone out into all the
earth, and their words to the ends of the world”
(Romans 10:18). According to the infallible Word of
God, the Gospel was indeed preached to the whole
world, well before Jerusalem was destroyed in a.p.
70. This crucial sign of the end was fulfilled, as Jesus
had said. All that was left was the seventh, final sign;
and when this event occurred, any Christians re-
maining in or near Jerusalem were instructed to
escape at once:
