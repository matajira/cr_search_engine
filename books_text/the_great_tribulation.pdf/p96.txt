80 THE GREAT TRIBULATION

(Matthew 23:34) whom Christ had sent?

Thus the breaking of the fifth Seal reveals a scene
in heaven, where the souls of those who had been
slain are underneath, or around the base of, the altar
(Revelation 6:9-10). The image is taken from the
Old Testament sacrifices, in which the blood of the
slain victim would stream down the sides of the altar
and form into a pool around its base (“the soul
[Hebrews nephesh] of the flesh is in the d/ocd,” Leviti-
cus 17:11). The blood of the martyrs has been poured
out (cf. If Timothy 4:6), and as it fills the trench
below the altar it cries out from the ground with a
loud voice: “How long, O Lord, holy and true, dost
Thou not judge and avenge our blood upon those
who dwell on the Land?”

The Church in heaven agrees with the cherubim
in calling forth God’s judgments: How tong? is a
standard phrase throughout Scripture for mvoking
divine justice for the oppressed (cf. Psalm 6:3;
13:1-2; 35:17; 74:10; 79:5; 80:4; 89:46; 90:13; 94:3-4;
Habakkuk 1:2; 2:6). The particular background for
its use here, however, is again in the prophecy of
Zechariah (1:12): After the Four Horsemen have
patrolled through the earth, the angel asks, “O Lorp
of Hosts, how long wilt Thou have no compassion for
Jerusalem?” St. John reverses this. After his Four
Horsemen have been sent on their-mission, he shows
the martyrs asking how long God will continue to put
up with Jerusalem—how long before He destroys her
for her violent oppressions.

St. John’s readers would not have failed to notice
another subtle point: if the martyrs’ blood is flowing
