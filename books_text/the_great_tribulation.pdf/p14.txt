xiv) THE GREAT TRIBULATION

Mankind’s New Beginning

It is one of those oddities of recent intellectual
history that perhaps the most succinct and percep-
tive comment on the Christian view of history is pro-
vided by a secular Jew who teaches law at Harvard
University. In the Introduction to his book, Law and
Revolution: The Formation of the Western Legal Tradition,
published by Harvard University Press in 1983,
Harold J. Berman makes 2 crucial observation on
the centrality of the resurrection in Christian histori-
cal thought. He begins with an important insight in-
to the Hebrew attitude toward historical time:

In contrast to the other Indo-European peo-
ples, including the Greeks, who believed that
time moved in ever recurring cycles, the
Hebrew people conceived of time as continu-
ous, irreversible, and historical, leading to ulti-
mate redemption at the end, They also be-
lieved, however, that time has periods within it.
It is not cyclical but may be interrupted or ac-
celerated. It develops. The Old Testament is a
story not merely of change but of development,
of growth, of movement toward the messianic
age—very uneven movement, to be sure, with
much backsliding but nevertheless a movement
toward.

Berman then goes on to explain how Christianity
adopted this view of linear time, but added a key
new element:
