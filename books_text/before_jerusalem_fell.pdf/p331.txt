19

THE CONDITION OF THE
SEVEN CHURCHES

The final pro-Domitianic argument from the internal evidence
that we will consider is that which is drawn from the epistolary
preface to Revelation. It is averred by many that the Seven Letters
to the churches of Asia contain historical allusions demanding a late
date. Turning our attention again to the order of argument given by
Morris, we cite his fourth objection to the early date: “A further
indication is that the churches of Asia Minor seem to have a period.
of development behind them. This would scarcely have been possible
at the time of the Neronic persecution, the only serious competitor
in date to the Domitianic period. ” ' Guthrie also lists this as his fourth
argument, and confidently so. After expressing some hesitancy in
employing the Nero Redivivus myth, he notes of the present argument
that here “we are on firmer ground” due to “certain positive indica-
tions of internal conditions” indicated in the letters.*This line of
reasoning is cited as Swete’s first point in establishing the late date
from internal considerations;*it appears as the second argument in
Charles, Moffatt, and Mounce (among his minor arguments), and
third in Kiimmel.*

1, Leon Morns, The Revelation of St. John (Grand Rapids: Eerdmans, 1969), p. 87.

2, Donald Guthrie, New Tistament Introduction, 8rd. ed. (Downers Grove, IL: Inter-
Varsity Press, 1970), p. 954.

8, Henry Barclay Swete, Commentary on Revelation (Grand Rapids: Kregel, [1906]
1977), pp.¢-ci.

4, R. H, Charles, The Revelation of St. John, 2 vols. International Critical Commentary
(Edinburgh: T. & T. Clark, 1920) 1 :xciv; James Moffatt, The Revelation of Si. John the
Divine, in W. BR. Nicoll, ed,, Englishman’s Greck Testament, vol. 5 (Grand Rapids: Eerd-
mans, rep. 1980), p. 816; Werner Georg Kimmel, Introduction to the New Testament, 17th
ed., trans, Howard C. Kee (Nashville Abingdon, 1978), p. 469; Robert H. Mounce, The
Book of Revelation. New International Commentary on the New Testament (Grand.
Rapids: Eerdmans, 1977), p. 84,

318
