30 BEFORE JERUSALEM FELL

Source Documentation

We will cite only those authors who by their noteworthy and
scholarly labors merit a careful hearing. There are numerous lesser
works that promote a pre-A.D. 75 date that we could set forth as
defenses of the early date view; these are omitted as inconsequential.”
Where possible we will employ original documentation. Where this
has not been possible, we will note the sources from which we
discovered their positions. No secondary source that is at all of
dubious scholarly distinction will be given consideration. The num-
bers in braces [ ] that precede the immediately following list of
secondary sources will be used at the end of each source entry in the
catena below. Some entries in the catena will have multiple sources.

[1] Greg L. Bahnsen, “The Book of Revelation: Its Setting” (un-
published research paper, 1984), pp. 14f.

[2] Adam Clarke, Clarke’s Commentary on the Whole Bible, vol. 6 (Nash-
ville: rep. n.d.), p. 961.

[8] Frederic W. Farrar, The Early Days of Christianity (New York:
Cassell, 1884), p. 408.

[4] Arthur S. Peake, The Revelation of jokn (London: Joseph Johnson,
1919).

[5] John A. T. Robinson, Redating the Naw Testament (Philadelphia:
Westminster, 1976), p. 225.

[6] Philip Schaff, History of the Christian Church, vol. 1: Apostolic Christi-
anity (Grand Rapids: Eerdmans, [1910] 1950), p. 834.

[7] Henry B. Swete, Commentary on Revelation (Grand Rapids: Kregel,
[1906] 1977), p. citi.

[8] Milton S. Terry, Biblical Hermeneutics (Grand Rapids: Zondervan,
rep. 1974), pp. 24In, 467.

Catena of Early Date Advocates
The following listing is arranged in alphabetical, rather than

4], For example: Robert L, Pierce, The Rapture Cult (Signal Min., TN: Signal Point
Preas, 1986); Ed Stevens, What Happened in 70 A, D2 (Ashtabula, Ohio North East Ohio
Bible Inst,, 1981 }; Max R, King, The Spirit of Prophecy (Warren, OH: by the author,
1971 ); Ulrich R, Beeson, The Revelation (Birmingham, AL: by the author, 1956); Jessie
B, Mills, Sirwy of the Book of Revelation (Bonifay, FL by the author, n.d.).
