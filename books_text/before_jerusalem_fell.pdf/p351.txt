340 BEFORE JERUSALEM FELL

Although virtually every point made by House and Ice regarding
Revelation’s date already has been dealt with in the main body of
this work, a direct response to them may be of interest to the reader.
Having now come upon their book, Dr. Greg Bahnsen and I are
preparing a full, book-length response to it. However, here in just a
few pages, I will deal with the comments they make in their Chapter
12, particularly as they affect the date of Revelation, but also with
reference to a few related matters.

Preparatory Observations

One particularly frustrating aspect of the recent debate regarding
Reconstructionist views is the tendency of the opponents of Recon-
structionism to confuse issues. House and Ice’s opening statement
in Chapter 12 illustrates this problem: “The validity of the Christian
Reconstruction agenda is vitally dependent upon the last book in the
Bible, the book of Revelation.” By this they mean Revelation as
interpreted from “the preterist, postmillennial viewpoint.”2 This sim-
ply is not true, and for a number of reasons.

Preterism and Reconstructionism

First, in point of fact, it has only been in recent years of Reconstruc-
tionist thought that serious and sustained attention has been focused
on the Book of Revelation. Chilton’s commentary itself was not
published until 1987, with its forerunner, Paradise Restored, preceding
it by only two years. Earlier, in its “Symposium on the Millennium,”
The Journal of Christian Reconstruction did not even make reference to
preterism!*If “the validity of the Christian Reconstruction agenda”
were “vitally dependent” upon the preterist approach to Revelation,
this doctrine would have been dealt with much earlier in the develop-
ment of the recent resurgence of Reconstructionist thought.

That Reconstructionists began to devote considerable time, money,
and effort to the book of Revelation well over a decade after the
preliminary outline of their position was in completed form indicates
that their perspective is not governed by preterism. But House and
Ice’s perspective is surely governed by futurism, so they have targeted
this aspect of Reconstructionism as being primary to the Reconstruc-

2, House and Ice, Domizon Theology, p. 249.
3. Gary North, od,, The Journal of Christian Reconstruction 111-2 (Winter, 1976-1977),

passim.
