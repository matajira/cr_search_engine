48 BEFORE JERUSALEM FELL

translational problem as “very dubious.” 2 Moffatt (a vigorous late
date advocate) discounts the supposed problem with just one sen-
tence, stating that the proposed revisions are “ingenious but quite
unconvincing.” ° According to Barnes, Chapman “is frankly con-
temptuous” against the proposed reconstruction of Irenaeus.4 There
are, however, a number of noted scholars who have disputed various
parts of the common translation. Among these are J. J. Wetstein,
M. J. Bovan, S. H. Chase, E. Bohmer, James M. Macdonald, Henry
Hammond, F. J. A. Hort, Edward C. Selwyn, George Edmundson,
Arthur S. Barnes, and J. J. Scott. 5

Three of the major problems with the generally accepted transla-
tion will be dealt with below: (1) The referent of éwpcOn (“was
seen”). (2) The significance of the time reference: ovdé yelp mpd
TOMAob xpévov éwpéOn, ddd ayedov én wig Hpetépas yeveds
(no long time ago was it seen, but almost in our own time”). (3)
The overall internal confusion in Irenaeus suggested by the incom-
patibility of Irenaeus’s statements on Revelation. 6

The Referent of éwpadn

Indisputably, the most serious potential objection to the common
translation has to do with the understanding of éwod0n, “was seen. ”
What is the subject of this verb? Is it “him who saw the Apocalypse”

12, Robinson, Redating, p. 221.

18, James Moffatt, The Revelation of St. John the Divine, in W. R. Nicoll, od., Englishman's
Greek Testament, vol, 5 (Grand Rapids: Eerdmans, rep. 1980), p. 319.

14, Arthur Stapylton Barnes, Christianity at Rome in the Apostolic Age (Westport, CT
Greenwood, [1988] 1971), p. 167n.

15. J. J. Wetstein, Novum Testamenten Graecum, vol, 2 (1751), p. 746. M. J. Bovan, Revue
de Theologic et de Philosophie (Lausanne: 1887), 8. H. Chase, “The Date of the Apocalypse
The Evidence of Irenaeus,” Journal of Theological Studies 8 (1907): 431-434Hort noted the
significance of this article; see Henry Barclay Swete, Commentary on Revelation (Grand
Rapids: Kregel, [1906] 1977), p. cvi. E, Bohmer, Uber Verfasser und. Abfassungszeit des
Apokalypse, pp. 30%; cited in Moffatt, Revelation, p. 505. James M. Macdonald, The Life
and Writings of St. John (London: Hodder & Stoughton, 1877), pp. 169. Henry Ham-
mond, A Paraphrase and Annotations Upon the New Testament, 4th ed, (London: 1653), p.
857; cited in Peake, Revelation, p. 74n, Edward C, Selwyn, The Christian Prophets and the
Prophetic Apocalypse (London: Macmillan, 1900). George Edmundson, The Church in Rome
an. the First Century (London: Longmans, Green, 1918). Barnes, Christianity at Rome, pp.
167ff. Barnes cites also Sanday, from his Preface to F, J. A. Hort, The Apocalypse of St.
John: 1-H, and Hilgenfeld ag adherents to this view. J. J. Scott, TheApocalypse, or Revelation
of S. John the Divine (London: John Murray, 1909), p. 154.

16, There is another area where some scholars have deemed there to be a problem
with the common interpretation of Irenaeus’s statement, Taking the lead of Guericke, a
