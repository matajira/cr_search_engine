The Role of Internal Evidence 15

less compelling. Some of the evidences propounded by early date
advocates of higher critical persuasions are even based on anti-
supernaturalistic presuppositions ‘Early date evidences considered
to be the most significant set forth by several nineteenth century
scholars will be briefly mentioned for two reasons. We do this, first,
in order to provide some historical background to the debate, and,
second, as a means of illustrating the variety of avenues that have
been explored in this matter.

Macdonald settles upon six major lines of evidence."His argu-
ments are as follows: (1) The peculiar idiom of Revelation indicates
a younger John, before his mastery of the Greek language, a mastery
evidenced in his more polished Gospel from a later period. (2) The
existence of only seven churches in Asia Minor (Rev. 1) indicates a
date before the greater expansion of Christianity into that region. (3)
The activity of Judaizing heretics in the Church (Rev. 2, 3) should
be less conspicuous after a broader circulation of Paul's anti-
Judaizing letters. (4) The prominence of the Jewish persecution of
Christianity (Rev. 6, 11) indicates the relative safety and confidence
of the Jews in their land. (5) The existence and integrity of Jerusalem
and the Temple (Rev. 11) suggest the early date. (6) The reign of the
sixth emperor (Rev. 17) must indicate a date in the A.D. 60s.

Of these arguments, Milton S. Terry endorses numbers 1, 2, 4,
and 5; he also adds a couple of additional considerations: (7) There
is a lack of internal evidence in Revelation for a late date. (8) The
nearness of the events had no fulfillment beyond the dramatic events
of A.D.70.° F, W. Farrar allows for Macdonald’s arguments 5 and
6, and adds another: (9) It is easy to apply Revelation’s prophecies
to the Jewish War, Schaff allows for three of the above arguments:
Macdonald’s numbers 5 and 6, and Farrar’s additional argument
regarding the nature of the events of the Jewish War. Schaff also
expands on Macdonald’s argument 4 by reference to the existence of

4, Bg., John A.T, Robinson, Redating the New Testament (Philadelphia Westminster,
1976), passim; and Charles Cutler Torrey, The Apocalypse of John (New Haven: Yale:
1958), passin,

5, James M. Macdonald, The Lit and Writings of St. John (London: Hodder &
Stoughton, 1877), pp. 152-167.

6, Terry, Hermeneutics, pp. 240M.

7, Frederick W. Farrar, The Early Days of Christianity (New York: Cassell, 1884), pp.
#12,
