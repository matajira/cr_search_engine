The Contemporary Integrity of the Temple 179

readily. Barnes added to these the reference to one Fortunatus (a
friend of Paul in 54, cf. 1 Cor. 16:17), the selection of Claudis and.
Valerius (who were of the household of Claudius the Emperor,
according to Lightfoot) as messengers, and other such indications .°'

Third, in 1 Clement 5:1 we read: “But to pass from the examples
of ancient days, let us come to those champions who lived nearest
our times. Let us set before us the noble examples which belong to
our generation. By reason of jealously and envy the greatest and
most righteous pillars of the church were persecuted, and contended.
even unto death. Let us set before our eyes the good Apostles.”
Clement thereupon mentions the deaths of Peter and Paul, which
indisputably indicates that he is referring to the Neronic persecution.
The fact that he mentions the deaths of “the good Apostles” in “our
generation” suggests a very recent occurrence that is quite compat-
ible with a date around A.D. 69 or 70. And although possible, the
“generation” would be on the outside reach of a date of A.D. 96
(which would be close to thirty years after the events).

Furthermore, it is more than a little interesting that Clement
names a few of those who died in the Neronian persecution. In 1
Clement 5 he names Peter and Paul, but also in 1 Clement 6 we read
of the names of a couple of other martyrs now virtually unknown,
Danaids and Dircae. It is quite remarkable that he cites names of
those involved in the Neronian persecution that allegedly occurred
about thirty years previous to his own day, but that he is strangely
silent about the names of those who died in the Domitianic persecu-
tion — even though they are supposed to have been prominent mem-
bers of his own congregation!

In both sections five and six Clement devotes many sentences to
explication of these Neronian woes. But it is quite curious, on the
supposition of a Domitianic date, that in 1 Clement 1 he uses only
ten words (in the Greek) to refer to the Domitianic persecution, the
persecution through which he and many of his friends were allegedly
going. That reference reads: “by reason of the sudden and successive
troubles and calamities which have befallen us.” If the letter were
written sometime approaching or in early A.D. 70, however, then the
first, fifth, and sixth sections would ali speak of the Neronian persecu-

60, Ibid., pp. 194.
61, Barnes, Christianity af Rome, pp. 21388.
