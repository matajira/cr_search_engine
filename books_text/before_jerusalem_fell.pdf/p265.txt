The Looming Jewish War 251

against Judea because of the resultant disaffection and widespread.
mayhem.

The events of the year A.D. 66, however, should not be consid-
ered a judgment against the Jews. This is because the Jewish forces
actually (and mysteriously!) gained the upper hand against the troops
of the governor of Syria. Josephus records the retreat of Cestius in
haste and fear amid the rejoicing of the Jews:

There it was that Cestius stayed two days; and was in great distress
to know what he should do in these circumstances; but when, on the
third day, he saw a still greater number of enemies, and all the parts
round about him full of Jews, he understood that his delay was to his
own detriment, and that if he stayed the longer there, he should have
still more enemies upon him.

That therefore he might fly the faster, he gave orders to cast away
what might hinder his army’s march. . . . [But when his troops were
soon trapped in difficult circumstances by the Jews] the distress they
were at last in was so great, that they betook themselves to lamenta-
tions, and to such mournful cries as men use in the utmost despair:
the joyful acclamations of the Jews also, as they encouraged one
another, echoed the sounds back again, these last composing a noise
of those that at once rejoiced and were in a rage. Indeed these things
were come to such a pass, that the Jews had almost taken Cestius’s
entire army prisoners, had not the night come on, when the Remans
fled to Bethoron, and the Jews seized upon all the places round about
them, and watched for their coming out in the morning.

And then it was that Cestius, despairing of obtaining room for a
public march, contrived how he might best run away. . . . [But] the
Jews went on pursuing the Remans as far as Antipatris; after which,
seeing they could not overtake them, they came back and took the
engines, and spoiled the dead bodies; and gathered the prey together
which the Remans had left behind them, and came back running and
singing to their metropolis; while they had themselves lost a few only,
but had slain of the Remans five thousand and three hundred foot-
men, and three hundred and eighty horsemen. This defeat happened.
on the eighth day of the month Dius, in the twelfth year of the reign
of Nero... .*

Now the Jews, after they had beaten Cestius, were so much elevated.
with their unexpected success, that they could not govern their zeal,

58, Wars 2:19:7-9,
