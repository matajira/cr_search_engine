SUBJECT INDEX

Abaddon, 210,

Abomination of desolation, 130, 190-191,
349,851.

About to, (See: Revelation — expectation),

Abraham, 124, 188-189, 195,222, 225,
227,284,

Abyss, 146,247,802.
angel of, 247,

Agrippa (See also Herod), 293n.

Aggripina (mother of Nero), 216.

Abenobarbus (father of Nero), 217.

Alexandria, 68, 69,97,98, 157n, 178,
270.

Alphabets), 157, 198-194, 195.

Altar, 97, 189, 165, 169, 174, 175, 214,
265,266,267,268,270, 272,274,280,
284,292, 350.

Amillennial(ist): See: Millennial Views.

Angel(s), 184, 198,282, 286, 244, 248n,
320,828,
interpretation by, 146-149, 168.

Anthropology, 14n.

Antichrist, 846-47, 52,58,58, 79,80,
101, 107,201n, 206,209.

Apocalypse (See: Revelation).

Apocalyptic, x, 8,12, 14, 17, 25, 151,
158, 162, 168, 198,247, 276.

Apollo, 207,271-275.

Apologetio(s), 188, 191, 192.

Apostle(s), 22, 45n, 52,58,56,61,62,
68,64, 78,82,84,88,90,91,95, 101,
102, 106, 125, 144, 145, 179, 191, 227,
244,247,276,290,291, 822,

Arabia, 70.

Aramaic, 199, 208-210,

Archaeology, 149,92,285, 268, 273.

documentary evidence, 199, 235,

Archippus, 828,

Armageddon, 6, 7,210.

Artist, 271.

Asia, 45n, 54n, 61,62,63, 100, 108, 150,
160,211,266, 268, 269, 806, 820,824,
826,847.

Asia Minor, 24, 92,98, 115, 161, 211,
260, 261,267,279,287,8 18,821,828,

Assassin(ate), 145,215.

Astrology, 805.

At hand (Bee: Revelation — expectation),

Atheism, 249,

Athens, 272, 288,

Augustus (emperor), 74, 77,84, 154-159,
211,241,242,271,275, 280n, 302.
name used as oath, 267,
worship of, 262, 264, 266-269, 270, 278,

288,

Baalam, 329n, 380,

Babylon, 17, 116, 187, 211,241n, 286,

Bacchanalian Conspiracy, 280-281,

Banishment (Bee: John the Apostle
— banishment),

Beast (See also: Nero), 58n, 90-91, 116,
119n, 162, 218, 241n, 262,296.
appenrance, 146, 201, 254, 276.
authority, 168.
character, 219-215, 254.
color, 217, 276,
death/wound, 79, 217-218, 276,
generic, 204, 206, 254,277, 810-816,
heads, 119n, 146, 148, 149, 163-164,

206,218,254,276,301, 308,318-316,
horns, 146, 148,218,276,

397
