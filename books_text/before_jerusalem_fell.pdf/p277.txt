264 BEFORE JERUSALEM FELL

The Pre-Neronian History of the Emperor Cult

We should not only notice the slight hesitancy regarding the
emperor cult in these scholars, but also the rationale for such: (1)
Emperor worship is traceable as far back as Julius Caesar, almost a
century before Nero’s death. (2) Formal temples erected for the
worship of the emperor are known to exist as far back as Augustus’s
reign (c. 29 B.C.). '° (3) The method for the enforcement of emperor
worship under Domitian is unknown, despite the claims that only
beginning with Domitian could the slaying of non-participants have
existed (as per the Revelation evidence), ! (4) The first official impe-
rial evidence of the enforcement of emperor worship is after both
Nero and Domitian, in the reign of Trajan. These are serious prob-
lems besetting any confident employment of emperor worship in the
argument. Especially are these problematic in light of the quite
precise chronological observations supportive of the early date in
Revelation 17 (the enumeration of the “kings”) and the existence of
the Temple, which is known to have perished in A.D. 70. The
emperor cult argument is slippery, as we shall see. At this juncture
a brief survey of the history of the emperor cult will prove helpful in
illustration of the fact that Revelation’s evidence is not incompatible
with a pre-Domitianic date. *2

Julius
Apparently, Julius Caesar learned from Cleopatra “the political

advantage of the deifications of royalty — the Pharaohs of Egypt
having been accepted by their subjects as incarnate deities.”3 Earlier

10, Edward C, Selwyn, The Christian Prophets and the Prophetic Apocalypse (London:
Maemillan, 1900), p. 122,

11, As will be noted in the next major section, there is a great deal more substantial
evidences for a Neronic persecution of Christianity than for a Domitianic.

12, For fuller discussion of the development of the imperial cult see the following
Adolf Harnack, The Mission and Expansion of Christianity in the First Three Centuries (New
York: Putnam’s, 1908) 1:2:9. B. W. Henderson, The Lift and Principate of the Emperor Nero
(London: Methuen, 1908), pp. 347ff., 434ff. Herbert B. Workman, Persecution of the Early
Church (Oxford: Oxford University Press, [1906] 1980), pp. 94. Kenneth Scott, “The
‘Wentification of Augustus with Romulus-Quinnus,” Transactions and Proceedings of the
American Philotogical Association %82-105. Lily Ros Taylor, The Divinity of the Roman
Emperor (Middletown, Corm.: American Philological Association, 1981), passim. Kurt
Aland, A History of Christianity, vol.1: From the Beginnings tothe Threshold of the Reformation,
trans. James L, Schaaf (Philadelphia Fortress, 1985), pp. 18-22.

18, Arthur Weigall, Nero:Emperor of Rome (London: Butterworth, 1988), p. 110.
