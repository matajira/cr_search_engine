106 BEFORE JERUSALEM FELL

the Gospel was increasing by the hands of the Apostles, Nero, the
unclean and impure and wicked king, heard all that had happened
at Ephesus. And he sent [and] took all that the procurator had, and
imprisoned him; and laid hold of S. John and drove him into exile;
and passed sentence on the city that it should be laid waste.”8° This
ancient statement is clear and to the point.

Elsewhere in the Syriac tradition, we should note that “both of
the Syriac Versions of the Revelation give in the title the statement
that John was banished by Nero.”®+ Though the earlier canon of the
true Peshitta (or Syriac Vulgate) version of the fifth century did not
contain Revelation at all, the sixth and seventh century editions of
the Syriac New Testament did. In them The Apocalypse of St. John
agrees with a Neronic banishment for John.®° One version is “beyond
doubt”8’that of Thomas of Harkel (A.D. 616). The other most
probably is the edition prepared in A.D. 508 by Polycarpus, the
chorepiscopus of Philoxenus, Bishop of Mabbug, hence its designa-
tion as the Philoxenian version.” Their titles say:“written in Patmos,
whither John was sent by Nero Caesar. “8°

Andreas of Cappadocia

Andreas was bishop of Cappadocia (probably near the com-
mencement of the sixth century).®? He is known either as Andrew of
Caesarea or Andreas of Cappadocian Caesarea. He wrote a commen-
tary on Revelation which is still extant.

It is clear from reading him that he prefers a Dornitianic date for

88, Wright Apocryphal Acts 2:55.

84, Arthur 8. Peake, The Rewlation of John (London: Joseph Johnson, 1919), pp. 76-77.
See also Swete, Rewilation, p. C; Hort, Apocalypse, p. xix.

85. Bruce M. Metzger, The Text of the New Testament, 2nd ed. (Oxford: Oxford
University Press, 1968), pp. 68-72.

86. John Gwynn, ed., The Apocalypse of St John. in a Spriac Version Hitherlo Unknown
(Amsterdam: APA-Philo, [1896] 1981), p. 1.

87, Swete, Revelation, p. cxciv.

88. Metzger, Text, p. 70. See also Gwynn, p. iv. See all of chap. 6 for a detailed
analysis,

89, Stuart, Apocalypse 1:267,

90, Though his dates are difficult to pinpoint, it seems agreeable to most scholars
that he flourished in either the latter part of the fifth century or the earlier part of the
sixth, See Stuart, Revelation 1-267; Swete, Revelation, cxcix; Schaff, Encyclopedia 1:83; and
W. Smith and Henry Wace, Distionars of Christan Biography, Literature, Sects, and Doctrines
(Boston: Little, Brown, 1877-1888) 1154ff.
