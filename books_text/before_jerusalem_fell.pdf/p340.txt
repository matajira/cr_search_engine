The Condition of the Seven Churches 327

the two Epistles to Timothy. Deterioration has set in at Ephesus, and
at Sardis and Laodicea faith is dying or dead.”3 | Morns, Swete, and
others argue that the supposed magnitude of the spiritual decline
manifested in the churches, evidenced in John’s admonitions, de-
mands a period of time more readily available if John wrote during
Domitian’s reign, than if he wrote during Nero’s."It would seem a
reasonable expectation that the early fervency of a new-found faith
would wane only after the passing of various perils over an extended
period of time.

Despite all the seemingly credible assertions advanced toward
the establishment of the above argument, however, at least two
important counter considerations militate against any confident ac-
ceptance of them.

Time Required for Spiritual Decline

First, granting that there is “a marked deterioration”3*in the
churches, the whole question of the length of time necessary for such
a waning of faith lies at the heart of the situation. Though it is quite
reasonable to expect that a passage of time is best suited to a decline
of a newborn faith, surely the passage of time is not a sine gua non for
such. In fact, a classic lustration of a rapid decline is contained in
the New Testament itself.

In Galatians 5:7 Paul writes to the Galatians that initially “you
were running well.” The very purpose of Pauls letter, however, is to
deal with the rapid decline of the apostolic faith among those in the
congregation: “I am amazed that you are so quickly deserting Him
who called you by the grace of Christ, for a different gospel; which is
really not another; only there are some who are disturbing you, and.
want to distort the gospel of Christ” (Gal. 1:6-7). The inspired
apostle considers the congregation to be “deserting” Christ. And this
desertion of the faith was occurring “quickly.”

Consider also Paul’s concern over the multitude of troubles within
the church of Corinth, a church founded in A.D. 49 and to which he
wrote with heavy heart in A.D. 57. Indeed, Paul anticipated such
problems to be experienced among the churches virtually as soon as

31, Swete, Revelation, BD. c-ci.

“92, Morris, Revelation, p, 87; Mounce, Revelation, p. 34; Swete, Rawlation, pp. ci;
Guthrie, Introduction, p. 954.

88. Guthrie, Introduction, p, 954.
