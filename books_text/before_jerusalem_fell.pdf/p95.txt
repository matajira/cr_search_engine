Clement of Alexandria 79

that he was the first of the emperors to be pointed at as a foe of divine
religion.

Lactantius (c. A.D. 260-330) speaks of Nero’s demise after his
persecution of Peter and Paul. Interestingly, he observes that Nero
was a tyrant: “He it was who first persecuted the servants of God;
he crucified Peter, and slew Paul: nor did he escape with impunity;
for God looked on the affliction of His people; and therefore this
tyrant, bereaved of authority, and precipitated from the height of
empire, suddenly disappeared.”**

Sulpicius Severus (A.D. 360-420) writes:

As to Nero, I shall not say that he was the worst of kings, but that he
was worthily held the basest of all men, and even of wild beasts. It
was he who first began a persecution; and I am not sure but he will
be the last also to carry it on, if, indeed, we admit, as many are
inclined to believe, that he will yet appear immediately before the
coming of Antichrist. ... I content myself with the remark, that he
showed himself in every way most abominable and cruel... . He
first attempted to abolish the name of Christian, in accordance with
the fact that vices are always inimical to virtues, and that all good
men are ever regarded by the wicked as casting reproach upon them.*

In chapter 28 of the same work he continues by noting of Nero’s
hideous persecution that “in this way, cruelty first began to be
manifested against the Christians. ” He even associates Nero with the
prophecy of Revelation: “It was accordingly believed that, even if he
did put an end to himself with a sword, his wound was cured, and
his life preserved, according to that which was written regarding
him, — ‘And his mortal wound was healed, [Rev. 13:3] — to be sent
forth again near the end of the world, in order that he may practice
the mystery of iniquity.” Writing of St. Martin of Tours, Severus
states that “when we questioned him concerning the end of the world,
he said to us that Nero and Antichrist have first to come.”©” In this
Sacred History he reserves two chapters to a consideration of Nero’s
reign, and only three sentences to Domitian’s.

68, Eusebius, Exclesiastical History 2:25:1-3,

64, On the Death of the Persecutors 2.

65, Sulpicius Severus, Sacred History 2:28.

66. Sacred History 2:31. Although he asserts that John wrote Revelation under Domi-
tian,
67, Sulpicius Severus, Dialogues 14,
