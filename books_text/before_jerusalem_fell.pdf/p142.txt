128 BEFORE JERUSALEM FELL

in Revelation, such would not be the exclusive reference (cf. Rev.

5:9; 7:9; 11:9; 13:7; 14:6).

“The Land”

In addition, the Greek word for “earth” in Revelation 1:7 is y#,
which most usually means either: (1) “earth, globe” or (2) “land.””
Thus, upon purely lexical considerations, the term can be understood
as designating the Promised Land. As a matter of fact, literal transla-
tions of the Scripture lean in this direction. Robert Young's Literal
Translation of the Holy Bible translates it: “Lo, he cloth come with the
clouds, and see him shall every eye, even those who did pierce him,
and wail because of him shall all the tribes of the land. Yes! Amen!”?!
Marshall’s The Interlinear Greek-English New Testament concurs: “Be-
hold he comes with the clouds, and will see him every eye and [those]
who him pierced, and will wail over him all the tribes of the land.
Yes, amen.*2”

Desprez’s comments on this matter are most helpful:

The words 4 yi}, are not infrequently used in the Apocalypse in
connection with other clauses which qualify their meaning, making it
evident that no particular land is pointed out, but earth gener-
ally. . . . But the words in question are sometimes found qualified
by governing considerations which define and determine their mean-
ing, and this is always the case, when they are found in connection with
the governing clauses “they that dwell,” of katomxedvtec. Then they
have, and can have, only one meaning; then they refer only to one
land and to one people, and this land and this people must be the
land and the people of Judea.”

The significance of this translation of # yi} can be discerned from
spiritual-cultural situations, such as noted by Edersheim: “For, to the
Rabbis the precise limits of Palestine were chiefly interesting so far
as they affected the religious obligations or privileges of a district.

20. See Arndt and Gingrich, p, 156; Thayer, pp. 114-115. G. Abbott-Smith, A Manual
Crock Lexicon oft tu New Testament (Edinburgh: T, &T, Clark, 1987), p. 91.

21, Robert Young, The New Testament t Literal Translation of the Holy Bible (Grand
Rapids: Baker, [1898] rep.nd.), p. 167.

22, Alfred Marshall, The Interlincar Greek-English New Testament, 2nd od, (Grand Rap-
ids: Zondervan, 1959), p. 956,

28, P, 8, Desprez, The Apocalypse Fulfilled, 2nd od, (London: Longman, Brown, Greon,
Longmans, 1855), pp. 12-18.
