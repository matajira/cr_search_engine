The Role of Emperor Worship 263

undermine the arguments. For the emperor cult to serve as a dating
indicator capable of overthrowing the wealth of early date evidence
rehearsed heretofore, it must be demonstrated rather decisively that
the cult as presented in Revelation “is a post-Neronian phenomenon
that is “almost impossible” in the Neronian era.

It is more than a little interesting that some of the leading
exponents of this evidence — the leading late date evidence — are not
as fully persuaded themselves of the evidential value of the cult as
one would like, if their evidence is to be compelling proof. Morris,
who terms this evidence “the principal reason” for a Domitianic date,
is a case in point. He at first briefly presents the evidence for his
“principal reason” for the Domitianic date. He then concludes by
stating “on the score of emperor-worship Domitian’s reign is the
most probable by far.” But he seems to offer reason for hesitation:
“But dating this accurately is more difficult. Thus Julius Caesar had
been worshiped as a god during his lifetime, and, while Augustus
was more cautious, there were temples in his honor in some of the
provinces. . . . It is true that, from the time of Nero on, the cult
tended to grow in some areas and it is barely possible that the
references in Revelation could be understood of some period under
or after Nero.”®

Despite his own arguments in regard to the emperor cult, and.
despite the fact it serves as his first argument for a Domitianic dating
of Revelation, Guthrie’s hesitation is harmonious with Morris’s, and
for the same reasons: “No knowledge of any rescript or edict has
survived from the first century which enforced emperor worship. ...
[A]lthough the emperor worship presupposed in the Apocalypse
would well suit the later period of Domitian’s reign, there is no
conclusive evidence that it could not have occurred earlier.”"7 Even
as vigorous and as liberal a late date proponent as Moffatt speaks of
the cult evidence as “almost impossible” under Nero.’As Robinson
observes “the growth of the imperial cultus is again something which
it is almost impossible to date with confidence.”®

6, Leon Morris, The Revelation of 8. John (Grand Rapids: Eerdmans, 1969), p. 35.
7, Guthrie, Introduction, pp. 950-951,
8, Moffatt, Revelation, p, 317, Emphasis mine,
we John A. T. Robinson, Redating the New Testament (Philadelphia: Westminster,
, p. 236,
