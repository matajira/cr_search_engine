390 BEFORE JERUSALEM FELL

Harrison, R, K., 17m.

Hartwig, H. G., 84, 241n.

Hase, Karl August von, 84,

Hastings, James, 27,

Hausrath, 84, 200.

Hayes, D, A., 34, 38n.

Hemer, C. J., 319n.

Henderson, Bernard W., 25, 34,45,60,
72, 78n, 200,213n, 214, 229, 250n,
264n, 266n, 270n,271n, 272n, 275n,
278,280, 281n, 282n, 288, 298,295,
296n, 304, 809n, 31 in,

Hendriksen, William, 121n, 150n,204,
285, 825, 828n,

Hengstenberg, C. W., 46, 880n.

Hentenius, $4,

Herder, Johann Gottfried von, 85, 241 n,

Herrenschneider, J. 8., 35.

Hermann, Gebhardt, 200.

Hilgenfeld, Adolf, 27,85, 48n, 188,200,
304n,

Hill, David, 3n, 35.

Hillers, D, R., 199m, 200.

Hitzig, Ferdinand, 35, 199,200.

Hodges, Zane C., 196n.

Hoeksema, Herman, 186, 140, 153n,
164,

Holtzman, Heinrich Julius, 85,

Holizmann, O., 168,

Holweerda, 241 n,

Hort, F. J. A., 28,28,85,41,48,49, 50,
58,60, 69n, 80n, 81,95,98, 104, 116,
117, 188, 199,209, 225n, 240n, 288,
296, 297n, $21 n, $45, 846,

House, H. Wayne, 389-353.

Hovey, Alvah, 7n,

Howson, J. 8., 274n, 283.

Hug, Johann Leonhart, 35,91.

Hurst, John Fletcher, 255n,

Hurst, George L., 183.

Hurte, William, 35.

Ice, Thomas D., 889-353.
Immer, A., 85,

Jacobs, Henry E., 266n, 304n, 330n.

James, E, O,, 231n.
Jamieson, Robert, 96n.
Jastrow, Marcus, 198,
Jerome, D, Harmon, 265n,
Johnson, George, 255n.
Johnson, Paul, 278n,
Jones, William, 255n,
Julicher, 380n,

Kautssch, E.,, 194n.

Keble, John, 62n,

Keck, Leander E., 825n,

Kee, Howard Clark, 3n, 156n, 209n,
281 n, 260n, 268n, 280n301n.

Keil, C.F, 17m.

Keim, Theodor, 35, 296n.

Kenyon, Kathleen M., 236n, 287.

Kerr, Walter C., 7 in.

Kepler, Thomas S, 18, 18n.

Kiddie, Martin, 116, 119, 200,201.

Kittle, Gerhard, 127n.

Killen, W. D., 256n,

King, Max R., 80n,

Kirban, Salem., 6n,

Kitto, John, 82.

Klausner, Joseph, 21 in,

Klostermann, E,, 210n.

Kiijn, A. F, J., 1870.

Knibb, M. A., 156n, 247n.

Knight, George W. HI, ix.

Knowles, Louis E., 352n,

Koppe, Theodor, 35.

Krenkel, Max, 35, 200.

Kruger, Gustav, 209n.

Kimmel, Werner Georg, 8n, 168, 209n,
210n, 260n, 285,301,818, 319, 328n,
825,828, 348n.

Kurfess, A. M., 185.

Kurtz, Johann Heinrich, 85, 255n.

Lachmann, Karl, 87.

Ladd, George Eldon, 25,81, 82n, 145n,
158,204, 287, 288,

Lake, Kirsopp, 825n,

Lampe, F, A,, 84n,

Landsman, J. I., 188,
