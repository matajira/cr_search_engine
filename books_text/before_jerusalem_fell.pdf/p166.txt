152 BEFORE JERUSALEM FELL

fragment hypothesis theory), Moffatt comments that thisis “the one
passege . . . which appears to be a water-mark of the date.” ©

All that is required for determining the chronology indicated by
Revelation 17:10 is that we find a series of seven kings, five of whom
“have fallen,” the sixth of whom “is® still ruling, and the last of whom
was of but a brief reign. The one who “is” will be the king alive and
tuling at the time John wrote Revelation. Then, of course, the discov-
ery of the dates of his reign will serve as the frmini within which
Revelation must have been composed.

We provided ample demonstration above to show that the place
of the seven kings is the famed city of “seven hills,” i.e., Rome. And
given the contemporary expectation of the book, the obvious candi-
dates for fulfilling the role of the seven kings would have to he the
emperors of Rome, the line of the Caesars. It is an indisputable
historical fact that the Caesars were ruling at the time John wrote
Revelation, regardless of whether an early (pre-A.D. 70) or late (c.
A.D. 95) date be advocated.

Various Approaches

Though it seems certain that the line of the emperors isin view
in Revelation 17:10, nevertheless, several difficulties arise as to the
proper enumeration of the line of the Caesars. In regard to the
chronology, two particularly important questions arise: With whom
does the enumeration begin? And, are any of the Caesars to be
omitted?

Some scholars (¢.g., Dusterdieck, Bleck, Swete, Weigall, Morris,
and even Torrey and Robinson) '’ begin the counting of the emperors
with Augustus, in that he was the first official “emperor.” Some (e.g,
Dusterdieck, Gilmour, and Rist) in their overall enumeration ornit

16. James Moffatt, The Revelation of St. John the Divine,in W. R.Nicoll, ed., Englishman’s
Greek Testement, vol. 5 (Grand Rapids: Eerdmans, rep. 1980), p. 318.

12. Friedrich Diisterdieck, Critical and Exegetical Handbook to the Revelation of John, 8rd
ed., trans, Henry E. Jacobs (New York Funk and Wagnails, 1886), p. 48. Friedrich
Bleck, An Introduction io the New Testament, 2nd od., trans. William Urwick, vol. 2 (Ed-
inburgh: T, & T. Clark, 1870), p. 226, Swete, Revelation, p. 220. Arthur Weigall, Nero:
Emperor of Rome (London: Butterworth, 1933), p. 298. Leon Morris,The Revelation of St.
John (Grand Rapids: Eerdmans, 1969), p. 88. Torrey, Ralation, p, 60. John A. T.
Robinson, Redating the New Testament (Philadelphia; Westminster, 1976), p. 248.

18, Diisterdieck, Revelation, p. 49. 8, MacLean Gilmour, “The Revelation to John,”
in Charles M. Laymen, od.,7he Interpreters One Volume Communiary on the Bible (Nashville:
