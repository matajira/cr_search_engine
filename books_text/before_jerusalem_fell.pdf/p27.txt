10 BEFORE JERUSALEM FELL

in 1910 that Darling’s list of English works on the Apocalypse con-
tained nearly 54 columns.33 With Carpenter’s observation regarding
the literature of the Apocalypse, we are compelled to agree: It is
“perfectly hopeless to touch so vast a subject as this.”3*

Certainly Revelation has captivated the minds of both the intense
scholar and the part-time student alike. Although presumption and.
imagination have caused many a commentator to stumble in inter-
preting Revelation, nevertheless the book has commanded and will
continue to command the devout attention of those who love God
and His Word.

Interpretive Difficulty of Revelation

As noted, Revelation has historically generated an intensity of
interest unparalleled among any of the books of Scripture. Yet, at the
same time — as evidenced by the extreme diversity of the views on
Revelation — it has been a most difficult book to interpret. Or per-
haps the converse is true: because of the extreme difficulty of interpret-
ing Revelation, it has created an intense interest! As Chilton has
observed: “Many rush from their first profession of faith to the last
book in the Bible, treating it as little more than a book of hallucina-
tions, hastily disdaining a sober-minded attempt to allow the Bible
to interpret itself — and finding, ultimately, only a reflection of their
own prejudices, “3°

Too often such a situation is due to the temptations presented
by biblical scholars who gear their works for the popular market.
This seems to be especially true of dispensational theologians. For
instance, Charles Ryrie — an able scholar and probably the leading
dispensationalist theologian of the present day — has written of Reve-
lation: “How do we make sense out of all those beasts and thrones
and horsemen and huge numbers like 200 million? Answer: Take it
at face value.”*° Later he gives an example of the usefulness of his
“face value” hermeneutic in seeking the correct interpretation of
Revelation 9:1-12 (the locusts from the abyss): “John’s description
sounds very much like some kind of war machine or UFO.. Demons

88, Schaff, History 1:826,

84, W. Boyd Carpenter, The Revelation of St. John, in vol, 8 of John Charles Ellicott,
od., Elliott's Commentary on the Whole Bible (Grand Rapids: Zondervan, rep. n.d.), p. 532.

85, David Chilton, Paradise Restored (Tyler, TX: Reconstruction Press, 1985), p. 153.

86, Ryrie, The Living End, p. 87.
