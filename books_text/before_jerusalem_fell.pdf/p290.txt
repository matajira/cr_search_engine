The Role of Emperor Worship 277

As demonstrated heretofore, the Beast is representative of impe-
rial Rome, the seven heads being the first seven emperors .84 We have
also argued that the 42 month “war against the saints” represented.
the persecution of Christians by the beast Nero from A.D. 64 to 68."
The'role of the wounded head” will be treated in a major section yet
tocome."* At this juncture we will set forth a brief demonstration of
the relationship of the emperor cult to the Neronian persecution (in
the next chapter we will deal more in depth with the nature and
extent of the Neronian persecution).

Clarifying the Issue

We note here at the outset that a formal, legal relationship of
emperor worship to the Neronian persecution is not absolutely re-
quired by the prophetic message contained in Revelation. Two con-
siderations lead us to this statement. In the first place, even upon
purely secular (i.e., naturalistic, anti-prophetic) presuppositions the
ideas embodied in Revelation 13 can be perceived as subtly lurking
behind the persecution of Nero. For the very existence of the emperor
cult and its employment by Nero himself surely would suggest to the
mind even of a mere non-inspired enthusiast both the religious
incompatibility of the Christian faith in regard to the divine preten-
sions of the emperor, as well as the inexorable drift to deadly confron-
tation. After all, at an earlier date (c. A.D. 40-41) had not Caligula
madly proposed the erection of his image in the Temple of Jerusalem
to the bitter distress and excited consternation of the Jews — and to
the very brink of war? Thus, whether or not the emperor Nero
formally and legally demanded “worship or die,” the inevitable ten-
dency of the emperor cult, when coupled with the autocratic power
of the mad emperor Nero, must necessarily result in just such an
explosive confrontation. The Christian “man on the street” must
have feared just such a potential under Nero’s nefarious reign.

In the second place, it could be that the prophecy of Revelation
18 speaks of the underlying philosophical and spiritual issues engaged,
rather than the external publicly advertised and judicially sanctioned
ones. That is, Revelation 18 could very well provide a graphic spiritual
elucidation of the fundamental potentialities lurking behind the cruel

84, See Chap, 10,
85, See Chaps, 14 and 17,
86. See Chap, 18,
