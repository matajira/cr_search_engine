Additional External Witnesses 99

Stuart’s observation is quite reasonable — much more so than the
presumptive guesses handed down as assured observations. His com-
ment is especially reasonable since Origen does mention that Herod
beheaded John’s brother James. It may well be that Origen’s state-
ment depends not on an Irenaean tradition, but upon a Tertullianic
one, as suggested by Robinson.®! Or even better, perhaps, if our
analysis of the evidence from Clement of Alexandria be correct, it
could be that Origen picked up on his master, Clement, who seems
to teach that John was exiled under Nero.

It is of further interest that Origen calls this unnamed emperor
“the King of the Remans.” Ratton understands this reference to be
indicative of Nero:

Throughout the East the Julian Caesars were looked upon as a royal
line and hailed as Kings. ... Nero was the last of them. After him
came the successful generals raised to the purple by their legions.
They took the title of Caesar, but prefixed it to their own names.
They reigned by virtue of their leadership of the Army. The official
title of Domitian illustrates both these points - “Jmperator Caesar Domi-
fionus Augustus, "©

Be that as it may, we come again upon a widely-acclaimed late date
witness which is wholly unconvincing.

Victorious

Victorious (d. c, A.D. 304), bishop of Pettau (or Petavionensis),
is another of the mainstays of the late date argument from tradition.
Victorious’s relevant statement is found in his Commentary on the
Apocalypse at Revelation 10:11. He states that: “When John said
these things he was in the island of Patmos, condemned to the labour
of the mines by Caesar Domitian. There, therefore, he saw the
Apocalypse; and when grown old, he thought that he should at length
receive his quittance by suffering, Domitian being killed, all his
judgments were discharged. And John being dismissed from the
mines, thus subsequently delivered the same Apocalypse which he
had received from God.”

It is abundantly clear that Victorious, a pre-Eusebian witness,

61. Robinson, Redating, p. 228.
62, Ratton, Apocalypse, p, 29,
68. ANF 7:353.
