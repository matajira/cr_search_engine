18 BEFORE JERUSALEM FELL

the two general dating camps regarding Revelation does not neces-
sarily fall along conservative/liberal lines. Nevertheless, the division
between Revelation scholars also tends to fall into two general camps.
These, too, are usually classed as “late” (c. A.D. 95) and “early”
(pre-A.D. 70, generally determined to be between A.D. 64 and A.D.
70)?

New Testament scholars commonly divide the options on the
dating of Revelation between these two periods.*We should note,
however, that more precise dates than simply pre-A.D. 70 and c.
A.D. 95 have been suggested by scholars — although the demonstra-
tion of a pre-A.D. 70 date is the major issue. For instance, Guthrie
presents a three-fold classification based on the eras of three different
Roman emperors: Domitian, Nero, and Vespasian.® Kepler suggests
four different time-frame classifications: (1) late Nero, (2) between
Nero and A.D. 70, (3) Vespasian, and (4) late Domitian.’

Second, the interpretation of both is strongly influenced by the
date assigned by the interpreter. Although the time span separating
the two general camps among Revelation interpreters (about 30
years) is not as broad as that which separates Danielic scholars
(around 400 years), the catastrophic events separating the two Reve-
lation dates are of enormous consequence. Those events include most
prominently: (1) the beginnings of the Roman persecution of Chris-
tianity (A.D. 64-68); (2) the Jewish Revolt and the destruction of the

4, There are even some noted early date scholars that hold to dates during Claudius’s
reign in the mid-A.D, 40s (o.g., Zillig, Grotius, and Hammond), but this position is quite
rare. See William Milligan, Discussions on the Apocalypse (London: Macmillan, 1898), pp.
754 Still others hold a mid-50s date. See Milton 8, Terry, Biblical Hermeneutics, p. 241 n
for souree documentation,

5, For example, Robert H. Gundry mentions only two options: in the general era of
Nero and of Domitian; Gundry, Surty of the New Tistament (Grand Rapids: Zondervan,
1970), pp. 864-865. See also G. R. Beasley-MurrayZhe Hook of Revelation. New Century
Bible (London: Marshall, Morgan, and Scott, 1974), p. 87; Harry E. Boer, The Book of
Revelation (Grand Rapids: Eerdmans, 1979), p. 19; J. W. Roberts, The Revelation to John
(Austin, TX: Sweet, 1974), p. 9; Mounce, Revelation, pp. 32-33; Leon Morris, The
Revelation of St, John (Grand Rapids: Eerdmans, 1969), p. 94.

6. Donald Guthrie, New Testament Introduction, 8rd ed. (Downers Grove, IL: Inter-
Varsity Press, 1970), pp. 94987, 958ff, 961. It should be noted that the Neronic and
‘Vespasianic time-frames under consideration are very close, usually understood to be
separated by a period of from as early as A.D. 64 to around A.D. 70, Thus, they may
both be considered in the “early” time-frame,

7, Thomas 8, Kepler, The Book of Revelation: A Commentary for Laymen (New York
Oxford, 1957), p. 19.
