138 BEFORE JERUSALEM FELL

“what must happen soon”
New Testament in the Language of Today,
by William F. Beck

The translation under question (i.e., in Revelation 1:1, although
the other references cited should be kept in mind, as well) has to do
with the proper interpretation of the Greek phrase év tdyet. Taye
is the dative singular of the noun téyo¢. Lexicographers seem to be
universally agreed with the translators as to the meaning of the word.
According to the Arndt and Gingrich Lexicon," téyoc is used in the
Septuagint (and certain non-canonical writings) to mean “speed,
quickness, swiftness, haste.” In the prepositional phrase év 7éye1, the
word is used adverbially in the Septuagint and Josephus to mean
“quickly, at once, without delay.” The New Testament uses tdyo¢
in this manner, says Arndt and Gingrich, in Acts 10:33; 12:7; 17:15;
22:18, In Luke 18:8; Remans 16:20; 1 Timothy 3:14; Revelation 1:1;
and 22:6 this lexicon translates it “soon, in a short time. ” The various
entries proffered at the téyo¢ entry by Thayer’ include: “quickness,
speed” and “quickly, shortly, speedily, soon.” Thayer lists Revelation
1:1 and 22:6 with the “speedily, soon” entries. Abbott-Smith concurs;
for the Revelation 1:1 and 22:6 texts he offers: “quickly, speedily,
soon.”!? Hort translates it “shortly, soon.”!? Noted Greek scholar and
church historian Kurt Aland agrees, when he comments on the word
as it is used in Revelation 22:12:

In the original text, the Greek word used is tay, and this does not
mean “soon, “ in the sense of “sometime,” but rather “now,” “immedi-
ately.” Therefore, we must understand Rev. 22:12 in this way: “I am
coming now, bringing my recompense.” The concluding word of Rev.
22:20 is: “He who testifies to these things says, ‘surely I am coming
soon.” Here we again find the word tayv, so this means: I am
coming quickly, immediately. This is followed by the prayer: “Amen.
Come, Lord Jesus!”... The Apocalypse expresses the fervent wait-

 

10, W. F, Amdt and F, W, Gingrich, eds., A Greek-English Lexicon of the New Testament
and Other Early Christian Literature, 4th ed, (Chicago University of Chicago, 1957), pp.
814-815,

11, Joseph Henry Thayer, od., Greek-English Lexicm of the New Testament (New York:
American Book, 1889), p. 616,

12.G, Abbott-Smith, A Manual Greek Lexicon of the New Testament, Sri ed, (Edinburgh:
T. &T, Clark, 1950), p. 441.

13d, FA. Hort, The Apocalypse of St, John: I-HI (London: Macmillan, 1908), p. 6.
