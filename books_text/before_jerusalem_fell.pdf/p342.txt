The Condition of the Seven Churches 329
argument, states that the argument “could be disputed.”3°

The Problem with the Domitianic View

Second, it must be noted that except for the matter of time, the
Domitianic date is not necessarily any more conducive to the decline
than the Neronic. That is, late date advocates have John on the scene
with these seven churches for over twenty-five years, but still they
declined. It is not as if (on the late date view) the churches have been
left without apostolic oversight. Both the early and late date views
face the same “problem” in this regard.

There does not seem to be any compelling reason to reject the
early date of Revelation on the basis of the spiritual decline in certain
of the Seven Churches.

Conclusion

Although there are other arguments that have been drawn from
the Seven Letters, those presented are the leading ones. A careful
consideration of the merits of each of the arguments, however, dem-
onstrates their inconclusive nature. Not one of the arguments consid-
ered individually, nor all of them considered collectively, compel
acceptance of the Domitianic date of Revelation. This is made all the
more serious when their inconclusive nature is contrasted with the
wealth of other internal considerations for an early date, as rehearsed
heretofore in the present work.

The Seven Letters even have elements more suggestive of a
period of time prior to the destruction of the Temple. A major one
of these has been discussed previously: the presence of strong Jewish
elements in the churches. This feature bespeaks an early period of
Christian development prior to the cleavage between Jew and Chris-
tian, which was enforced by the complex of events associated with
both the Neronic persecution and the Jewish War (Rev. 2:9; 3:9).39

88, Guthrie, Introduction, p. 955,

39. See Chap. 18 above. An interesting and reasonable conjecture regarding the
derivation of the name “Nicolaitan” (Rev. 2:6, 15) has enjoyed wide currency, and is
algo subtly suggestive of the early date of Revelation in that it bespeaks an era prior to
the final separation of Christianity from Judaism. That is, that the name “Nicolaitan” is
intentionally derived from the Greek (vix@ and dadv)_ which means “conqueror of
people,” and as such reflects the Hebrew term “Baalam” (from 53 and DY ), which
means “destruction of the people.” This indicates John is giving a Greek designation to
the Hebrew word, as he does elsewhere in Revelation (e.g., 9: 11; 16: 16; cf:12:9; 20:2).
