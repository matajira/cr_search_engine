162 BEFORE JERUSALEM FELL

the vision of the Woman and the Beast he purposely transfers himself
in thought to the time of Vespasian (6 ig Zonv), interpreting past
events under the form of prophecy after the manner of apocalyptic
writers, "+

The Symbolic Nature of Revelation

Some scholars doubt the utility of the Revelation 17 kings list in
dating the book in that Revelation is preeminently a symbolic book.
For instance, J. P. M. Sweet argues that “John’s history, like his
geography and arithmetic, is spiritual (11:8); his hearers needed to
be told not who was reigning but his spiritual affiliations. The number
seven is symbolic - there were many more churches than seven — though
it can refer to actual entities. John ‘means to represent the Roman
power as a historic whole.” ”®

The first and most obvious problem with such a statement is the
fatal admission he makes: “though it can refer to actual entities.”
That being the case, the question arises: why not here? Beyond that
we should consider that the Christians of the era would think it
important to know not only the “spiritual affiliations” of the reigning
king, but also his identity — not the connotation only of the “king,”
but also his denotation. Their lives were literally on the line. Why would
they not need to know? What is so incredible with knowing the
identity of one’s enemies when promised the information? Besides,
the very passage in question is, as we have stated above, an explica-
tion of the symbolism that purports to elucidate the matter (Rev. 17:7).
Whereas in the illustrative verse alluded to by Sweet (ie., Rev. 11:8),
John clearly says the designation is “spiritual.” After John gives the
spiritual reference, even there he provides a clear, indisputable his-
torical geographic reference: The city that is spiritually called “Sodom
and Egypt” is “where also their Lord was crucified.”

All agree that the book makes a symbolic use of numbers. But
we must understand that it is the sovereign God of heaven and earth
who makes that usage. Is it necessarily impossible to find a direct
correspondence between the symbolic numbers and historic reality?
After all, both spiritual symbolism and historical-geographical reality
proceed forth from the same source: the One seated above the chaos

64, Ibid., p.221,
65, Sweet, Revelation, p. 257. Op. Beckwith, Apocalypse, p. 704-708.
