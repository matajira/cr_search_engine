Lrenaeus, Bishop of Lyons a

nounced by him who beheld the apocalyptic vision. For that was seen
no very long time since, but almost in our day, towards the end of
Domitian’s reign?

The éwpdOn (“that was seen”) is commonly considered to refer
back to the immediately preceding noun, dnoxdAvyic “Revelation”
or “apocalyptic vision”), in the preceding sentence. Irenaeus is af-
firming, it is argued, that John “saw” (ie., received by vision) the
prophecies of Revelation at a time fitting the late date theory of
composition: “no such long time ago,“ “almost in our own genera-
tion,” and, more precisely, “at the end of the reign of Domitian.”

As the external evidence section of the present study is developed,
additional ancient historical witnesses will be considered. But the
importance of this evidence found in Irenaeus’s work is universally
recognized and demands careful and lengthy consideration. How
shall early date advocates deal with such strong and forthright testi-
mony by this noteworthy ancient church father? As a matter of fact,
there are several considerations that tend to reduce the usefulness of
Irenaeus for late date advocacy. These will be brought forward in
some detail.

The Translational Problem

Certainly the two initial considerations in any judgment regard-
ing the interpretation of a crucial documentary witness are those of
textual certainty and translational accuracy. In that there are no
crucial questions regarding the integrity of the text of Irenaeus’s
statement raised from either camp in the debate, we can move
directly to consideration of the matter of translational accuracy.

On the matter of translation there has been a good deal of debate
on various aspects of the statement in question. In fact, “this transla-
tion has been disputed by a number of scholars.” !° According to
Peake and Farrar the problem of translational accuracy was first
broached by J. J. Wetstein in 1751.] ! We should note at the outset,
however, that most scholars doubt there is a problem of translation.
For instance, Robinson (an early date advocate) speaks of the alleged

9. ANF 1:559-560.
10, Robinson, Redating, p. 221.
11. Farrar, Early Days, p. 4498; Peake, Revelation, p. 78.
