The Looming jewish War 253

Now from the time of this official imperial engagement in the
Jewish War (early Spring, A-D. 67) until the time of the Temple’s
destruction and Jerusalem’s fall (early September, A.D. 70) is a
period right at the symbolic figure of 1260 days (or 42 months or 3!/2
years). Indeed, counting backward from early September, A.D. 70,
we arrive 42 months earlier at early March — in the Spring of 67!
Surely this figure cannot be dismissed as sheer historical accident.
Though the time-frame undoubtedly carries with it the foreboding
spiritual connotation associated with a broken seven (31/2 is one-half
of the perfect number 7), nevertheless, we are also driven to recognize
the providence of God in these historical affairs. In keeping with
divinely ordained symbol, in fulfillment of divinely inspired prophecy,
it did, as a matter of fact, take Rome 3’ /2 years to trample Israel and
the city of Jerusalem totally. Under the providence of God the
symbolic “broken seven” became the literal time-frame of Jerusalem’s
doom. Stuart surmises: “After all the investigation which I have been
able to make, I feel compelled to believe that the writer refers to a
literal and definite period, although not so exact that a single day,
or even a few days, of variation from it would interfere with the object
he has in view. It is certain that the invasion of the Romans lasted
just about the length of the period named, until Jerusalem was
taken, °

Thus, again, we have a time-frame that is wholly consistent with
historical circumstances associated with the Jewish War — a time
frame that lends further strength to the pre-A-D. 70 argument for
Revelation.

Revelation 13:5-7

In Revelation 13:5-7 the events are separated in time and geogra-
phy from the events of the Jewish War, but, as we will see, the
circumstances well fit the pre-A.D. 70 era. The passage before us
reads:

 

Rome when the capital of the nation and the strongest point of resistance fell. Titus
returned to Rome by 71 to celebrate a joint victory celebration with his father, Vespasian,
now the new emperor, See also Reicke, New Testament Era, pp. 2664. Walker, History, p.
30. C.F. D, Moule, The Birth of New Testament, 8rd ed, (New York: Harper & Row,
1982), pp. 172ff. Masterman,ZSBE41619,

68, Stuart, Apocalypse 2:218,
