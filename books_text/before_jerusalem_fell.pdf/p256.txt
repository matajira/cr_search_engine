242 BEFORE JERUSALEM FELL

a war at Zion in 66; after Nero’s death, the whole Roman Empire was
ablaze and at war during the year 69. The same Aome nows who
conquered the Jews, Vespasian, was soon able to restore the power
of the emperors, but upon a new foundation.””

This was spoken of by Origen as the “abundance of peace that began
at the birth of Christ. “2*Latourette states that “the internal peace
and order which Augustus achieved endured, with occasional inter-
ruptions, for about two centuries.”2?

Due to this famed, empire-wide peace, Christ’s prophetic refer-
ence to “wars and rumors of wars” (Matt. 246, 7), which were to
occur in His “generation” (Matt. 24:34), serves as a remarkably
significant “sign” (Matt. 24:3-8, 38) of the end of the Temple and the
Jewish age (Matt. 24:2, 3, 15-16). And as such they find expression
also in John’s version of the Olivet Discourse, ie., Revelation.”

Revelation 6:4

The same text indicates civil war in “the land”: “it was granted.
to take peace from the earth (lit., the land), and that men should slay
one another” (Rev. 6:4). Josephus is emphatic in his assessment of
the calamities that befell the Jews. He insists that the carnage wrought
by internecine strife in Israel wreaked more destruction upon them-
selves than that brought upon them by the Remans.*1 One citation
will suffice as evidence:

There were, besides, disorders and civil wars in every city; and all
those that were at quiet from the Remans turned their hands one
against another. There was also a bitter contest between those that
were fond of war, and those that were desirous of peace. . . . [I]nso-
much that for barbarity and iniquity those of the same nation did no

 

27, Reicke, New Tistament Eva, pp. 109-110.

28, Origen, Remane 1:3.

29. Kenneth Scott Latourette, A History of Christianity, nd ed., 2 vols. (New York
Harper & Row, 1975) 1:21, See also Joseph Ward Swain, The Harper History of Civilization,
vol, 1 (New York: Harper & Bros., 1958), pp. 15 14, Williston Walker, A History of the
Christian Church, 8nd ed. (New York: Scribners, 1970), p, 8, John Laurence von Mosbeim,
History of Christianity in the First Three Centuries, vol. 1 (New York: Converse, 1854), p. 11.

80. It is interesting that, John is the only writer of a canonical Gospel who omits
Christ's Olivet Discourse announcement of the destruction of the Temple and the end
of the age. It would seem almost certain that this is due to the fact that he had treated
it earlier in his Revelation, See earlier discussion,

81, Wars 4:3:2, 10. Cp. 4:6:10; 5:1:1,5.
