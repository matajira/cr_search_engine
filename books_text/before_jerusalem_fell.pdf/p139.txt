The Theme of Revelation 125

you also endured the same sufferings at the hands of your own
countrymen, even as they did from the Jews, who both killed the Lord
Jesus and the prophets, and drove us out.”

This consistent and constant witness against the Jews in the
canon of the New Testament continues into post-apostolic Church
history. Let us list a few of the sources where the idea is pressed by
the early Church fathers. We will quote the fathers occasionally to
illustrate the nature of the comments. The references are all taken
from Roberts and Donaldson’s The Anie-Nicene Fathers; parenthetical
page numbers are to this work. '°

Ignatius (c. A-D. 50-115) quite frequently drives home the point
of Jewish culpability regarding Christ’s death. In chapter 11 of his
Epistle to the Magnesians (ANF 1:64) he speaks of the “Christ-killing
Jews.” In chapter 11 of the Hpistle to the Trailians (ANF 1:71), he
speaks of the Jews as “those fighters against God, those murderers
of the Lord.” In the Epistle to the Smymacans, chapter 2 (ANF 1:87),
he says: “The Word raised up again His own temple on the third
day, when it had been destroyed by the Jews fighting against Christ.”
In chapter 2 of the spurious (but ancient) Epistle to Hero (ANF 1: 113),
the writer casts reproach upon those who deny Christ’s deity: “If any
one says that the Lord is a mere man, he is a Jew, a murderer of
Christ.” In the spurious (but ancient) Epistle to the Philippians, chapter
14 (ANF 1:119), we read: “If any one celebrates the passover along
with the Jews, or receives the emblems of their feast, he is a partaker
with those that killed the Lord and His apostles. ”

Justin Martyr (c. A.D. 100-165) plays the same theme of Jewish
liability in his First Apology “Jesus Christ stretched forth His hands,
being crucified by the Jews” (ch. 35, ANF 1:174). “And that all these
things happened to Christ at the hands of the Jews, you can ascer-
tain” (ch. 38, ANF 1:175). In his Dialogue with Trypho, chapter 72
{ANF 1:235), he writes: “the Jews deliberated about the Christ
Himself, to crucify and put Him to death.”

Trenaeus (c. 180-202) concurs in his Against Heresies, when he
says of the Jews: “[God] sent in Jesus, whom they crucified and God.
raised up” (3: 12:2, ANF 1:430), and “To the Jews, indeed, they
proclaimed that the Jesus who was crucified by them was the Son of

15, Alexander Roberts and James Donaldson, eds., The Anie-Nicene Fathers {ANF}, 10
vols. (Grand Rapids: Eerdmans, [late 19th ¢.] 1975).
