196 BEFORE JERUSALEM FELL

hers.’ | Tertullian sees in Gideon’s choice of 300 men a cryptic refer-
ence to the letter “T,” which signifies the sign of the cross. |

Of ancient cryptograms we should note that there are “countless
examples from classical and Hellenistic and indeed Rabbinic litera-
ture.”!3 Caird points out several specific examples of gematria in
rabbinic writings, '' while Eduard Reuss writes: “The mechanism of
the problem [i.e., the problem in Revelation 13: 18] is based upon one
of the cabalistic artifices in use in Jewish hermeneutics, which con-
sisted in calculating the numerical value of the letters composing a
word. This method, called geimatria, or geometrical, that is, mathe-
matical, [was] used by the Jews in the exegesis of the Old Testa-
merit. ”'° The point is clear: cryptograms were common among the
ancients, even among Christians. Hence, the gematria in Revelation
is not something created de xovo by John; rather, the idea involved a
familiar concept to the ancients.

The Textual Variant

Another introductory matter undoubtedly of significance in de-
termining the identity of this “666” is the matter of the textual variant
in the Greek of Revelation 13:18. Although both the strongest manu-
script evidence and intrinsic probability are supportive of the reading
“666,” '® there is some slight manuscript and historical evidence for
the number “616.”

Instead of éprjkovra, which is strongly supported by p”’ 8 A P 046

LL, Against Heresves 2:24:1 ff, written ca. 185,

12, Carn. ad. Mare, 3:4. Cited in Frederic W, Farrar, The Barly Das of Chnstianity
(New York: Cassells, 1884), p. 469 n, 1.

43, Ruble, “ape” TNT 1-462,

14.G, B, Caird, A Commentory on the Revelation of St. John the Dime (New York: Harper
& Row, 1966), p. 174, See the Babylonian Talmud: Yona 20a; Nazir & ; Sanhedrin 22°;
Uekin 12.

1, Eduard Reuss, History of Christan Theology in the Apostolic Age, cited in J. Stuart
Rossell, The Perouna: A Study of the New Testament Doctrine of Our Lord’s Second Coming, 2nd.
ed, (Grand Rapids: Baker, [1887] rep, 1983), p. 557.

16, The number 666 is accepted by the committees of all the major Greek New
Testaments, to wit: Eberhard Nestle, ed., Noun Testamentum Graece, 25th ed. (Stutigart:
Wiirttembergische Bibelanstalt, 1968), p. 688; R. V.G, Tasker, ed., The Greek New
Testament, Being the Text Translated in the Naw English Bible 1961 (Oxford: Oxlord University
Press, 1964), p. 896; The Textus Reveptus (London: Billing and Sons, 1967), p. 614; Kurt
Aland, Matthew Black, ef. ol, eds., The Greek Naw Tistament, 8rd ed. (Minster: West
Germany, 1975), p. 869; and Zanc ©, Hodges and Arthur 1,, Farstad, eds., The Greek New
Testament According to the Majority Test, Ind ed, (Nashville; Thomas Nelson, 1985), p. 765.
