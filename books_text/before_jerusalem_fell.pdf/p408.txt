Subject Index 399

Circumcision, 212,222,225, 227.
Circus, 288, 292.
Civil war (See: Rome - civil war; Jewish
War).
Classica/classical, 25,60, 104, 154.
Claudius (emperor), 18n, 158, 178, 216,
267,306.
another name for Nero, 104-105.
banishes Jews from Rome, 293, 293n.
worship of, 270-271.
Clementia Caesaris.
Cloud(s), 121, 122, 128, 180, 170.
Codex Sinaiticus, 88.
Coin(s), 149, 170,285, 272, 278,274,
Coming of Christ (See: Jesus
Christ — coming).
Commodus (emperor), 73, 77, 160.
Conquer/conqueror, 2n, 76.
Conservative (See: Orthodox),
Constantine (emperor), 101.
Copies (See: Manuscripts; Texts).
Corinth, 327-828,
Covenant (See aise: New Covenant; Old
Covenant), 142, 189, 339.
curse of, 148-144, 171, 212n, 249,
Crimes (See: Christians — crimes of).
Critio(s) (Gee aise: Higher criticism; Text
~ criticism), 21, 27n, 28, 60, 65, 114,
115, 156, 167,202.
Crown(s), 95, 184,
Crucifixion (See Jesus Christ —
erveifixion),
Cryptic, 187, 193n.
Cryptogram (See; Cematria),
Cult(ic), 288,256, 261ff, 272,279,
Culture, 280, 293n, 335,337.
progress of, 7, 336,
Curse, 123,

Date of Revelation (Sze: Revelation, date
of).

Day of the Lord (Gee 2/7: Tribulation),
234,

Decapitated (See: Beheaded).

Decius (emperor), 80.

Demas, 328,

Demon(a) (See also: Jerusalem — demons
and), 10, 198, 247, 248,305n.

Destruction (Ser: Tample — destruction of
Jerusalem — destruction of),

Devil (Bee also: Satan), 210, 222n, 249,

Devils (See: Demons).

Diadem, 278,276,

Diaspora (Jewish dispersion), 186, 211.

Disciple(s), 22, 207.

Dispensationaliam(Ser: Millennial views).

Divores of Israel, 2417, 350,

Document(s), 16, 19,27,47,58-59,94,
199, 288, 287, 289, 290,

Domitian (emperor), 18,20, 26,28,44,
47505258, 56,57,60,61,65, 73,
79,85,98,99, 100, 109, 116, 182, 144,
160,215,247,259,259, 271,285, 288n,
296,300,828,327,984, 343,348,
as second Nero, 73, 82, 288,
death of, 145, 180,218,

Nero Domitius, 48n-49n, 70, 104,
persecution of (Se: Persecution
—Domitianic).
pre-imperial authority, 66,
worship of, 262, 264, 267, 278-279, 283n,
Drama, 3,8,81, 118, 188, 134n, 142,
Dragon, 276.

Engle, 318n,

Earth (See also: Land; World), 76, 128,
141, 146, 156, 157, 162,218, 235,241,
244,302,

Earthquake, 170n, 188,319-322, 348,

Early-date (See Revelation, dateof).

Editor, 167-168, 189,

Egypt, 162, 170, 175, 188, 286, 264, 322,
330n.

Bight, significanceof, 816n,

Bight hundred, eighty-cight (Ser Jesus
Christ - number of Numbers
~ symbolic use of)

Bighth head of Beast (Sve also: Kings
— eighth king).

Elder(s), 52, 56,61,63,89, 94, 178.

Emblem(s) (See: Symbol).

Emperors (See individual names; Sec:
