274 BEFORE JERUSALEM FELL

Who is the heavenly saviour whose coming the peoples have awaited?
The emperor!

The official expression of this political philosophy is the classical coin.
On the obverse of the coin we see the portrait of the ruler, decorated.
with the marks and emblems of deity, and framed in titles of divine
dignity. For the ruler is the god who had become man. The reverse
of the coin usually depicts the most symbolically potent event in the
life of the ruler, his advent... . [I]t was in the age of the emperors
that the political advent philosophy reached its heyday. The first to
have the word ADVENTUS inscribed on the coins was Nero. A
Corinthian coin of Nero’s reign, from the year 67, has on the obverse
the type of the emperor in divine nakedness, adorned only with the
laurel wreath of Apollo, and on the reverse the flagship with the
imperial standard and above it the inscription ADVENTUS
AUGUSTI, the Arrival of the August One. The divine Apollo once
came by sea to the Greek mainland. The Roman emperor now makes
his entry into Greece by sea, in order that he may be worshiped as
Apollo incarnate.73

Thus, of Paul’s first Roman imprisonment, it can be noted that:

History has few stranger contrasts than when it shows us Paul preach-
ing Christ under the walls of Nero’s palace. Thenceforward, there
were but two religions in the Roman world: the worship of the
Emperor and the worship of the Saviour. The old superstitions had
been long worn out; they had lost all hold on educated minds. There
remained to civilised heathen no other worship possible but the
worship of power; and the incarnation of power which they chose
was, very naturally, the Sovereign of the world. This, then, was the
ultimate result of the noble intuitions of Plato, the methodical reason-
ings of Aristotle, the pure morality of Socrates. All had failed, for
want of external sanction and authority. The residuum they left was
the philosophy of Epicurus, and the religion of Nerolatry. But a new
doctrine was already taught in the Forum, and believed even on the
Palatine. Over against the altars of Nero and Poppaca, the voice of a
prisoner was daily heard, and daily woke in groveling souls the
consciousness of their divine destiny.”

In A.D. 67 Nero went to Greece where he remained for more

73. Stauffer, Christ and the Caesars, D. 38,
74. W, J, Coneybeare and J, 8, Howson, The Lift and Epistles of St. Paul, vol, 2 (New

York: Scribners, 1894), pp. 434-435,
