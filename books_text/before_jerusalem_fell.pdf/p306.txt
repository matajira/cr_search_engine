The Persecution of Christianity 293

The Significance of the Evidence for a Neronic Persecution

As the evidence for the Neronic persecution is scrutinized, we
must bear in mind that it clearly demonstrates, first, that Christians
were punished, and that they were punished as Christians. Both Taci-
tus and Suetonius make reference to the fact that those punished
were members of that hated religious sect. Suetonius mentions
Nero’s credit that the “Christians” were punished as members of a
“new and mischievous supers tition. ” Tacitus speaks of them as “Chris-
tians” and as “detested” by the populace and as “guilty” of criminal
activity. 33 Clearly the hated religious commitment of the Christians
marked them out as worthy of punishment in the minds of the
heathen populace.**

These Christians were not punished as Jews, as may have been
done by imperial confusion under Claudius when Jews were banished
from Rome because of “Chrestus” (Christ) .“It is clear that although
Rome had previously confused Christianity as a sect ofJudaism and.

88, The “crimes” of the Christians have nothing to do with the fire — Tacitus admits
that Nero looked for scapegoats. The “crimes” of the Christians had to do with their
aloofness from the “culture® of Rome. “The principles in which they gloried . . . forbade
them to recognise the national gods or the religion of the Roman people, or to take part
in any of the public religious ceremonies or spectacles, or in that worship of the genus of
Caesar . . .“ (Edmundson, Church in Rome, p. 187). Tacitus’s reference to the Christians
indicates they were thought to have a hatred for the human race: odt humani generis
Annals 15:44); see B. W. Henderson, Tie Life and Principate of the Emperor Nero (London:
Methuen, 1908), pp. 486-487, Ramsay wrote of this comment; “To the Remans genus
jumanum meant, not mankind in ggneral., but the Roman world - men who lived
according to Roman manners and laws; the rest of the human race were enemies and
barbarians. The Christians then were enemies to civilised man and to the customs and
laws which regulated civilised society. They were bent on relaxing the bonds that held
society together . . “ (William M. Ramsay,7% Church in the Roman Empire Before A.D.
170 [Grand Rapids: Baker, (1897) 1979, p. 286).

84, Some have argued that the name “Christian” was uncommon in Nero's day and
was only used proleptically by the second century historians Tacitus and Suetonius. But
these were men whe freely derived their historical research from contemporary sources.
Furthermore, Peter speaks of the Neronian persecution when he writes: “Ifa man suffer
ag a Christian let him not be ashamed, but let him glorify God in this Name* ( 1 Pet.
416), The name “Christian” was popularly employed in Antioch well before the 60s
(Acts 11:26) and was even familiar to King Agrippa (Acts 2628). Pliny’s correspondence
to Trajan also suggests that the name “Christian” was long known among imperial
authorities by A.D. 112,

85, Suetonius, Claudius 25:4: “Since the Jews constantly made disturbances at the
instigation of Chrestus, he expelled them from Rome.” That Christians were affected by
this banishment is evident from Acts 18:2. Obviously, the fact that many Christians were
Jewish confused the Remans into considering Christianity a Jewish sect.
