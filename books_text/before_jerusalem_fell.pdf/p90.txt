74 BEFORE JERUSALEM FELL

The Fear of Nero’s Return

Second, Nero was so dreaded by many that after his death there
began circulating haunting rumors of his destructive return. In fact,
“very soon after Nero’s death, there grew up a curious legend which
remains well-nigh unique in history, the legend that Nero would
return to earth again to reign. “3’The rumors can be found in the
writings of Tacitus, Suetonius, Dio Cassius, Zonara, Dion Chrysos-
tom, Augustine, and other ancient writers.”

In the corpus of the Sibylline Oracles Nero appears as a constant
threat to the world. Sibylline scholar J. J. Collins notes in this regard
that “there is the prominence of Nero as an eschatological adversary
throughout the Sibylline corpus.”3° Let us take a few pages to demon-
strate the pervasiveness of Nero in these alleged prophecies of folklore
quality. In the Jewish Sibylline Oracles (written “sometime after
A.D.70”)* there is a veiled reference to Nero*! that equates him
with the dreaded Beliar:

Then Beliar will come from the Sebastenoi [i.e., the
line of Augustus]

and he will raise up the height of mountains, he
will raise up the sea,

the great fiery sun and shining moon,

and he will raise up the dead. ...

But he will, indeed, also lead men astray, and he
will lead astray

many faithful, chosen Hebrews, and also other
lawless

men who have not yet listened to the word of
God.“

 

whitewash the tyrant” is not exactly true, Arthur Weigall in his classic study, Nero:
Emperor of Rome (London: Butterworth, 1988) portrays Nero as a victim of bad publicity.

87. Henderson, Nero, p. 419,

88, Tacitus, Histories 1:78; 2:8; Suetonius, Nero 37; Dio Cassius Xiphilinus 65:9; Zonara,
Annals 11:15-18; Dio Chrysostom, rations 21:9,10; Augustine, The City of Goa’ 20:19-3.
See also Sibylline Oracles, 4:119-124, 187-189; 5:33ff, 104-107, 189-154, 214-220, 861-
370; Ascension of Isaiah 42-4,

39, J. J, Collins, “Sibylline Oracles,” in James H, Charlesworth, od., Old Testament
Pseudepigrapha, 2 vols (Garden City, NY: Doubleday, 1983) 1:360.

40. Ibid., p. 860.

41, Ibid., p. 868, note j.

42, Sibylline Oracles 3:63-70; OTP 1:863.
