108 BEFORE JERUSALEM FELL

Domitian, Eusebius alleges in his Chronicon.” Arethas does not
appear to be satisfied with what Eusebius “alleges.” This is all the
more evident in his comments on Revelation 7:1 and 7:4 (see above);
there Arethas speaks his own mind. He then goes on to note that
Josephus records the fulfillment of the predictions in the seals. Stuart
saw these — and rightly, it would seem — to be compellingly sugges-
tive of a pre-A.D, 70 date for Revelation.

Theophylact

A much later witness is Theophylact, Metropolitan of Bulgaria
and noted Byzantine exegete (d. 1107). He gives evidence of a dual
tradition on John’s banishment. He puts Revelation “under Trajan,
but elsewhere gives a date which would bring it into the time of
Nero.” In his Preface to Commentary on the Gospel ofJohn, Theophylact
puts the banishment of John under Nero when he says John was
banished thirty-two years after the ascension of Christ®”: “év 7drpq
ti vijow éfdpiotog SiatedGv peta tpiaxovtadtio ét tg tot
Xptotob avedryeas.” In his commentary on Matthew 20:22 he
mentions John’s banishment under Trajan!

Conclusion

The above survey shows that the Domitianic date cannot be
certainly established from the external evidence. Indeed, when care-
fully scrutinized, the evidence even tilts in the opposite direction.
Thus, Guthrie’s statement does not appear to be well taken: “It
would be strange, if the book really was produced at the end of Nero’s
reign, that so strong a tradition arose associating it with Domi-
tian’s.”® The Domitianic evidence is less than compelling.

Trenaeus’s statement, the major evidence by far, is grammatically
ambiguous and easily susceptible to a most reasonable re-interpreta-
tion. The re-interpretive approach would totally eliminate him as a
positive late date witness. The evidences from Clement of Alexandria

96, Peake. Revelation, p. 77. Cp. Swete, Rawlation, p, ¢; and Charles, Revelation 1:xcii.

97. For the Greek, see Stuart, Apocalypse 1:269,

98, Among ancient writers only Dorotheus, bishop of Tyre in the sixth century, agrees
with such a late date for John’s banishment to Patmos; see his Synopsis de vita et morte
prophetarum. See Swete, Revelation, p. c; and Stuart, Apocalypse 1:269. It should be noted
that Dorotheus only says that the Gospel (not Revelation) was written at this time.

99, Guthrie, Introduction, p, 960,
