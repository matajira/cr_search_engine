The Persecution of Christianity 289

“evidence to justify this reputation is scanty.” Despite his employ-
ment of the Domitianic persecution as one of the major proofs of the
late date, Morris himself admits that the evidence for a general
persecution under Domitian “is not easy to find” !?!

The only non-Chnistian evidence for a Domitianic persecution of
Christianity is based on an ambiguous statement from Dio Cassius’
Roman History, a history produced quite sometime after the events.”
Dio states that Domitian’s cousin Flavius Clemens was executed and
his wife banished on the basis of the charge of “atheism,” a charge
that Dio equates with the practice of Judaism. Besides the ambiguity
of the statement itself (is this “atheism” really Christianity, as some
argue?), two-thirds of Die’s writing is preserved for us in an eleventh
century epitome and a twelfth century summary. Cary argues, and
Bell agrees, that the section dealing with Domitian was produced
“very carelessly. »23 Belf’s article in New Testament Studies provides an
excellent analysis both of the difficulty of establishing a general
persecution under Domitian and of the questionable utility of the
evidence from Dio Cassius.

Furthermore, it is remarkable that though Suetonius credited
Nero with the persecution of Christians, he makes no mention of
Domitian’s alleged persecution.” It would seem that since he viewed
the punishment of Christians as praiseworthy under Nero, that any
general persecution of them under Domitian would have deserved.
comment.

Thus, the documentary evidence for a general imperial persecu-
tion of Christianity under Domitian is deemed questionable by a
number of competent scholars. This fact alone should render this
second leading proof of a late date for Revelation suspect. Even a
good number of knowledgeable late date advocates doubt the useful-
ness of such an argument. Not only is the evidence suspect, but even
if accepted, it reveals a persecution inferior in every respect to the
Neronic persecution, as will be shown.

20, Bruce, Histoty,p.412,

21, Morns, Revelation, pp. 96-87.

22, His dates are A.D. 150-235.

28, Dio Cassius, Roman History, trans, Ernest Cary, 9 vols. Loeb Classical Library _
(Cambridge Harvard University Press, 1968) !nodii.

24, Nero 16:2.
