136 BEFORE JERUSALEM FELL

that these events which constitute the revelation must take place
shortly. That more than 1900 years of church history have passed.
and the end is not yet poses a problem for some. One solution is to
understand ‘shortly’ in the sense of suddenly, or without delay once
the appointed time arrives. Another approach is to interpret it in
terms of the certainty of the events in question. Of little help is the
suggestion that John may be employing the formula of 2 Peter 3:8
(with the Lord one day is as a thousand years’). ... The most
satisfying solution is to take the word in a straight-forward sense,
remembering that in the prophetic outlook the end is always immi-
nent."5

Morns (who probably would be classed as an amillennialist)
agrees with the premillennialist on this matter, although he takes the
route that seems to Mounce to be “of little help”: “Shortly is not
defined. . . . This could mean that the fulfillment is expected in the
very near future... . But speedily has a reference to His time not
ours. With Him one day is as a thousand years and a thousand years
as one day (2 Pet. iii. 8). It is also possible that the term should be
understood as ‘suddenly, i.e., not so much ‘soon’ as ‘without delay
when the time comes.’”®

Vincent’s work differs little from the type suggested by Morris’s
line of thought: “Expressions like this must be understood, not ac-
cording to human measurement of time, but rather as in 2 Pet. iii. 8.
The idea is, before long, as time is computed by God.”7 Hoeksema,
an amillennialist, agrees when he writes of Revelation 1:1 that “we
must remember ... that God’s measure of time differs from ours.”8
Swete, a postmillennialist, writes that “dv téyet... must be inter-

5. Robert H. Mounca, Tie Hook of Revelation. New International Commentary on the
New Testament (Grand Rapids: Eerdmans, 1977), pp. 64-65. Later, however, Mounce
makes an admiasion based on his view that muat be pair-did to a conservative biblical
scholar “It is true that history bas shown that ‘the things which must shortly come to
pase (1: 1) have taken longer than John expected” (p. 248), Were not bis numerous
expectations recorded in infallible Holy Writ? Were they merely the expectations of
“John the enthusiast,” or were they not the expectations of ‘John the divinely inspired
prophet” (see Rev. 1:1; 22:6, 20)? These were not incidental to his work, but repetitively
emphatic in it.

6, Leon Morris, The Reeelation of St. John (Grand Rapids: Berdmans, 1969), p. 45.

7, Marvin R, Vincent, Word Stuties in the New Testament, vol, 2: The Writings of John
(Grand Rapids: Eerdmans, [1887] 1985), p. 407.

8, Herman Hoeksema, Behold, He Cometh! An Exposition of the Hook of Revelation (Grand.
Rapids: Reformed Free Publishing, 1969), p. 9.
