282 BEFORE JERUSALEM FELL

The Jews responded to the favors of Rome (as varying as these
were under different local procurators) by offering “sacrifices twice
every day for Caesar, and for the Roman people.” lThis was
doubtless regarded by Rome as “a very fair equivalent” | to the
imposition of the Imperial Divinity’s inclusion in the Pantheon of
Rome’s subjects. In other words, it appeased the emperor’s expecta-
tion for some form of religious veneration by the Jews. |!*

At the outbreak of the Jewish Revolt (which became a full-fledged
war from Rome’s perspective when Nero commissioned Vespasian
to suppress it), however, this protective offering in honor of Caesar
was stopped. Josephus records the event:

And at this time it was that some of those that principally excited the
people to go to war, made an assault upon a certain fortress called
Masada. They took it by treachery, and slew the Remans that were
there, and put others of their own party to keep it. At the same time
Eleazar, the son of Ananias the high priest, a very bold youth, who
was at that time governor of the temple, persuaded those that offici-
ated in the divine service to receive no gift or sacrifice for any
foreigner. And this was the true beginning of our war with the
Remans: for they rejected the sacrifice of Caesar on this account: and
when many of the high priests and principal men besought them not
to omit the sacrifice, which it was customary for them to offer for their
princes, they would not be prevailed upon.

The effect of this decision as it reflected upon the Roman emperor
was that “its termination in the summer of A.D. 66 was tantamount
to official renunciation of his authority .”!* This was the focal event
that highlighted the extreme seriousness of the revolt of the Jews and
that brought Roman imperial forces into the picture. In a real sense,
the cessation of the Jewish sacrifices for the emperor resulted in the
death of those in “the land,” for a most gruesome and protracted war
was waged against rebellious Israel. }!”

112, Josephus, Wars 2:10:4, See also his Against Apion 2:5.

118, Henderson, Nero, p. 348,

114, One example exists of at least one emperor who felt it was not enough, The
emperor Gaius complained: “You offered sacrifices for me, it is true, but you offered
none to me,” in Philo, To Gaius 857.

115, Ware

116. Broce, History, p. 139,

117. This seems to be the idea involved in the second Beast’s killing those in the Land
who did not worship the image of the Beast (Rev. 13:15).

 
