4 BEFORE JERUSALEM FELL

Milo Connick states the case well when he writes: “The book of
Revelation has the dubious distinction of being the most misunder-
stood composition in the New Testament. Many readers don’t know
what to think of the writing, and others make altogether too much
of it. 56

Despite the very real difficulties associated with the book, how-
ever, it is “given by inspiration of God and is profitable” (2 Tim.
3:16). Thus, surely it is the case that Swete overstated the matter
when he wrote: “The key to the interpretation disappeared with the
generation to which the book was addressed... , and apart from
any clue to its immediate reference, it was little else but a maze of
inexplicable mysteries. “5’ Neither can we agree with Allen who de-
spairingly lamented that “the book is, and must remain for the most
part, unintelligible to the average reader.”5°

Causes of Difficulty

There is a variety of reasons that either independently or collec-
tively have caused the would-be interpreter to stumble. Foremost
among them seem to be the following (which, due to our main
purpose, will not be given extensive consideration):

First, unfamiliarity with its literary style. Revelation is considered
by most scholars to be of the literary genre known as “apocalyptic.”°?
This style is not unique to Revelation among canonical books — though
it is not used elsewhere in canonical literature to the extent it is in
Revelation. “Apocalyptic imagery may be found in Daniel, Ezekiel,

 

Berkeley Mickelson noted with mild understatement, “This is no small task” (Interpreting
tte Bible (Grand Rapids: Eerdmans, 1968] p. 280). G. R. Beasley-Murray comments that
“Revelation is probably the most disputed and difficult book in the New Testament” (G.
R. Beasley-Murray, Herschell H. Hobbs, Frank Robbins, Revelation: Thre: Viewpoints
(Nashville: Broadman, 19771, p. 5).

56, C. Milo Gonnick, The New Testament: An Introduction to Its History, Literature, and
Thought (Belmont, CA: Dickenson, 1972), p. 408.

57, Swete, Revelation, p. cxix.

58, Willoughby C, Allen and L, W, Grensted, Introduction to the Books of the New
Testament, 84 od, (dinburgh: T, & TT, Clark, 1928), p. 273.

59, The source of apocalyptic imagery, contrary to secularistic anthropologists, is not
firat century apocalypticism, but Old Testament era canonical prophetic imagery. The
first century apocalyptic movement itself grew up in a literary milieu dominated by the
Old Testament, Revelation is genealogically related to the Old Testament, not to
non-canonical mythology. See note labove

60, “There is only one other Apocalypse which may be compared with [Daniel], and
