A Response to House and Ice 351
Christ the Son of God.*!

Another ancient document that makes reference to the destruc-
tion of the temple based on Matthew 24:2-34 is the Clementine Homi-
lies. There we read:

But our Master did not prophesy after this fashion; but, as I have
already said, being a prophet by an inborn and every-flowing Spirit,
and knowing all things at all times, He confidently set forth, plainly
as I said before, sufferings, places, appointed times, manners, limits.
Accordingly, therefore, prophesying concerning the temple, He said:
“See ye these buildings? Verily I say to you, There shall not be left
here one stone upon another which shall not be taken away [Matt.
24:3); and this generation shall not pass until the destruction begin
[Matt. 24:34]... “ And in like manner He spoke in plain words the
things that were straightway to happen, which we can now see with
our eyes, in order that the accomplishment might be among those to
whom the word was spoken.®

In Clement of Alexandria’s Miscellanies, we read his discussion of
the Daniel 9:24-27 passage:

And thus Christ became King of the Jews, reigning in Jerusalem in
the fulfillment of the seven weeks. And in the sixty and two weeks the
whole of Judaea was quiet, and without wars. And Christ our Lord,
“the Holy of Holies,” having come and fulfilled the vision and the
prophecy, was anointed in His flesh by the Holy Spirit of His Father.
In those “sixty and two weeks,” as the prophet said, and “in the one
week,” was He Lord. The half of the week Nero held sway, and in the
holy city Jerusalem placed the abomination; and in the half of the
week he was taken away, and Otho, and Galba, and Viteilius. And
Vespasian rose to the supreme power, and destroyed Jerusalem, and
desolated the holy place.

G1. Origen, Against Celsus, 2:13 (See Roberts and Donaldson, Ante-Nicee Fathers,
4437), Origen further discusses the destruction of Jerusalem as a final act removing the
Jews forever from their former favor (422; See Roberts and Donaldson, Ante-Nicene
Fathers, 4506).

62, Though not written by a noted Church father, it is a late second century work
that touches on the matter before us. House and Ice boldly state that preterism is found
in “none of the early church writings” (p. 258). Yet, here is a work that shows early
consideration of the matter, apparently picking up on views current i7 that day.

68, Clementine Homilies, 8:15, See Roberts and Donaldson, Ante-Nicone Fathers, 8:241,
