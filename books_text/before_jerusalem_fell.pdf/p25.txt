8 BEFORE JERUSALEM FELL

noted a quarter century later: “The literature of the Apocalypse,
especially in English, is immense, but mostly impository rather than
expository, and hence worthless or even mischievous, because con-
founding and misleading,”2°

An illustration of the current severity of the problem is the
success of Hal Lindsey's publications. These often tend to be as
incautiously sensational as they are immensely popular. For instance,
it is difficult to conceive of anyone reading Revelation with even a
modicum of spiritual sensitivity who could be less than overawed at
the terrifying majesty of the revelation of the righteous wrath of
Almighty God as it is unleashed in all its holy fury upon His wicked.
enemies. Furthermore, it would seem that anyone reading Revelation
with any appreciation of literature as such could not but stand in
wonder at the intricately woven and multifaceted beauty of its struc-
ture and dramatic movement. Yet in Lindsey's works (which deal in
large part with Revelation), Revelation’s majestic splendor is reduced
to simplistic jingles. Cute headings such as “The Future Fuhrer”
(i.e., antichrist), “Scarlet O’'Harlot” (i.e., the Harlot of Revelation
17), “the Main Event” (i.e., the glorious Second Advent of Christ),
an so on, dot the pages. 24 Despite the caution urged by the histori-
tally illumined mind in regard to the failure of modern prognostica-
tors," Lindsey confidently asserts: “The information in the book
you’re about to read is more up-to-date than tomorrow’s newspaper.
I can say this with confidence because the facts and predictions in
the next few pages are all taken from the greatest sourcebook of
current events in the world. ”*° In a follow up work he confidently
sets forth his view that the 1980s may be the last generation of our

era.”

28, Philip Schaff, History of the Christian Church, 8 vols.’ (Grand Rapids: Eerdmans,
[1910] 1950) 1:826.

24, Lindsey, Lute Great Planet Earth, pp. 98, 122, 169,

25, See Wilson's analysis inArmageddon Now! Note J. A. Alexander’s warning in the
1800s in his article “The End is Not Yet” (reprinted in The Banner of Troth 88 (January,
197 1:1), A perfect illustration of unfounded confidence in this regard is A. W. Pink's
The Redeemer’s Return (Ashland, KY Calvary Baptist Church, [1918] rep.n.d.), pp. 315%.
Pink was certain that the beginning of World War I was the beginning of the end, Pink
later changed his views and suppressed this book, which was reprinted only after his
death,

26, Hal Lindsey, There% A New World Coming (Santa Ana, CA: Vision House, 1973),
p15,

27, Lindsey, Countdown to Armageddon, pp, 8, 12, 15,
