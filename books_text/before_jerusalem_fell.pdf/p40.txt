The Approach to the Question of Dating 23

is a most significant question. Extremely strong defenses of its apos-
tolic authorship, however, are available from such noted scholars as
B, B. Warfield, William Milligan, Henry B. Swete, Donald Guthrie,
and Austin Farrer, *"to name but a few.

Unity

Third, another very weighty consideration that has been vigor-
ously debated, but which will be assumed in the present research, is
the matter of the unity of Revelation. An array of approaches has
arisen as to Revelation’s original content and composition history,
including various emendations by the same writer and numerous
editions by later editors. These have been suggested in order to
explain some of its alleged disunity.

Furthermore, these matters do have a great bearing upon its
date. Moffatt has boldly asserted that “the Neronic date (i.e. soon
after Nero’s death) exerts most of its fascination on those who cling
to too rigid a view of the book’s unity, which prevents them from
looking’ past passages like xi. If. and xvii. 9f” '° Even as conservative
a scholar as Swete rebuts Lightfoot, Westcott, and Hort for their
support of the A.D. 68-69 date due to two presuppositions they hold,
one of which is the matter under consideration: “The unity of the
Book is assumed, and it is held to be the work of the author of the
Fourth Gospel. But the latter hypothesis is open, and perhaps will
always be open to doubt; and the former cannot be pressed so far as

 

Gospel and no claim to have known Christ personally, (4) There seem to be several
uncharacteristic emphases if by the apostle, e.g., God as Majestic Creator (instead of
Compassionate Father), Christ as Conqueror (instead of Redeemer), a sevan-foldness to
the Holy Spirit (rather than a unity), (5) There is a different range of thought, i.e, an
omission of characteristically Johannine ideas such. as life, light, truth, grace, and love.
(6) Linguistic style. (7) Doubt as to apostolic authorship among Eastern churches, All
of these and more are ably answered in the introductions and commentators to be cited
next,
17. B, B, Warfield, “Revelation,” in Philip Schaff, ed., A Religious Encylopedia: Or
Dictionary of Biblical, Historical, Doctrinal, and Practical Theology (New York: Funk and
Wagnalls, 1888) 3:2034ff Milligan, Apocalypse, pp. 1491. Swete, Revelation, pp. cxx ff
Guthrie, Introduction, pp. 932. Austin Farrer, The Revelation of St. John the Divine (Oxford:
Clarendon, 1964), chap, 1, Farrer’s demonstration of Johannine authorship is unique in
its exposition of the correspondence of the literary patterns between the Gospel and
Revelation. Farrer would not be classed as a “conservative” scholar.

18, James Moffatt, The Revelation of St, John the Divine, in W.R. Nicoll, of, Englishman's
Greek Testament, vol, 5 (Grand Rapids: Eerdmans, rep. 1980), p. 817.
