270 BEFORE JERUSALEM FELL

Now, one of these ambassadors from the people of Alexandria was
Apion, who uttered many blasphemies against the Jews; and, among
other things that he said, he charged them with neglecting the honours
that belonged to Caesar; for that while all who were subject to the
Roman empire built altars and temples to Caius, and in other regards
universally received him as they received the gods, these Jews alone
thought it a dishonourable thing for them to erect statues in honour
of him, as well as to swear by his name.“

His notorious plan to have his image erected in the Temple at
Jerusalem and the providential prevention of it is well-known, thanks
to Josephus.

Claudius

Suetonius and Tacitus both record the up and down position of
Claudius as a god: he was voted a god upon his death only to have
his enrollment among the gods annulled by Nero but later restored.
by Vespasian.47 Even during his life a temple was erected to him at
Colchester.”

Clearly then, the emperor cult had a prominent role in the
political and social life of the Roman empire from at least the times
of Augustus — well before Domitian, and even before Nero.* In fact,
“the student of the struggle of contending religions in the early
Roman Empire cannot neglect the history of the State cult, even if
he feels disposed to slight it as no true example of religion. The seer
of Patmos did not so misapprehend its force.”5° Even late date
advocates note that “the blasphemous title of divus, assumed by the
emperors since Octavian (Augustus = sebastos) as a semi-sacred title,
implied superhuman claims that shocked the pious feelings of Jews
and Christians alike. So did Se0¢ and O06 vidc that, as the inscrip-
tions prove, were freely applied to the emperors, from Augustus

45, Antiquities 1
46, Antiquities 18:
47, Suetonius, Claudius 45; Nero 9; 33; Tacitus, Annals 12:69,

48, Workman, Persecution, p. 40,

49, Helpful in pointing out the existence of emperor worship and the role of emperors
in various cults during the reigns of Caligula, Claudius, and Nero is E, Mary Smallwood,
od., Documents Illustrating the Principates of Gaius Claudius and Nero (Cambridge: University
Press, 1967), entries #124-168, pp. 48-58.

). B. W. Henderson, The Siudy of Roman History, 2nd ed. (London: Duckworth, 1921),
p. 102,

  

See also Eusebius, Ecclesiastical History 2:5-6.

 
