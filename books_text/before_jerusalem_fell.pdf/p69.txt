Trenaeus, Bishop of Lyons 53
along the lines of Chase’s:

But surely this rendering [i.e., the common rendering of Irenaeus] is
wrong. It should be “for he (St. John the writer) was seen .. . almost
in our generation toward the end of the reign of Domitian.” It is of
the Seer and his ability to declare the name of Antichrist that Irenaeus
is speaking. The misunderstanding about the meaning of the passage
is largely due to Eusebius, who after a reference to Domitian’s perse-
cution proceeds “in this (persecution) report [he] affirms that the
Apostle and Evangelist John, who was still living, in consequence of
his testimony to the divine word was condemned to dwell on the
island of Patmos,” and then he quotes Irenaeus in support of his
statement.”

Edmundson feels that Eusebius imparted this wrong historical data
as a result of reading too much into Origen’s comments on Matthew
20:22. That is, apparently Eusebius merely assumed that John was
exiled to Patmos under Domitian, based on Origen’s obscure com-
ment.*! Edmundson thus surmised that this led Eusebius astray in
his historical arrangement of the data at this point.

A further reason for Irenaeus’s emphasis is that “to say of one ‘he
was seen,’ meaning thereby he was still alive at a certain time, might
seem unusual, whether in Greek or English, as applied to an ordinary
man. When we consider, however, how much would be thought of
seeing this most aged apostle who had seen the Lord, there is nothing
unnatural in the use of such an expression. In fact this verb is applied
to him in precisely the same sense in the beginning of the chapter.”*

The evidence rehearsed above has not convinced everyone. Even
early date advocates such as Hort, Stuart, Guericke, and Robinson™
fail to endorse such a re-interpretation of Irenaeus. Stuart dismisses

30, Edmundson, Church in Rome, pp, 164-165. His reference to Eusebius is to his
Ezclesiastical History 3:23:1,

31, We will consider this statement from Origen later in this part of our work, It
should be noted here, however, that Origen does not mention the name “Domitian” in
his statement, Simcox suggests that Irenacus may have merely assumed Domitian used
banishment more than Nero (William Henry Simcox, The Relation of St, Jo/m the Divine.
‘The Cambridge Bible for Schools and Colleges [Cambridge University Press, 1898], p.
xl).

82, Macdonald, Lije and Writings, pp. 169-170.

88. On Hort's position, see Swete, Ravlation, p, cvi. Stuart, Apocalypse 1265, writes:
“And although the Avoraéie, in the passage of Irenaeus , has been differently inter-
preted by different critics (6. g. the ancient translator of Irenaeus renders it visum est, viz.
the beast; Wetstein applies the verb to John himself; Storr, to the name of the beast), yet
