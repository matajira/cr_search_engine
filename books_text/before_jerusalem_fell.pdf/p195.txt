The Contemporary Integrity of the Temple 181

It would seem that, at the very least, reference to the statement
in 1 Clement 41 cannot discount the possibility of our approach to
Revelation 11, in that the date of 1 Clement is in question. And as is
probably the case, Clement did write his epistle prior to the Temple’s
destruction.

The Alleged Silence of Early Christianity

It is objected by a number of scholars that, contrary to what we
might expect, early Christian literature did not make much of the
fall of Jerusalem and the destruction of the Temple. Consequently,
it is not a serious matter for John, writing in the A.D. 90s, to make
any room for the destruction of the city and Temple that occurred
in A.D. 70: “We should expect... that an event like the fall of
Jerusalem would have dinted some of the literature of the primitive
church, almost as the victory at Salamis has marked the Persae. It
might be supposed that such an epoch-making crisis would even
furnish criteria for determining the dates of some of the NT writings.
As a matter of fact, the catastrophe is practically ignored in the extant
Christian literature of the first century.”© Or, as put by another
scholar: “It is hard to believe that a Judaistic type of Christianity
which had itself been closely involved in the cataclysm leading up to
A.D. 70 would not have shown the scars — or, alternatively, would.
not have made capital out of this signal evidence that they, and not
non-Christian Judaism, were the true Israel. But in fact our traditions
are silent.”

At this juncture we will bring forth three points to establish our
thesis. We will begin by demonstrating the tenuousness of the asser-
tions of Moffatt and others regarding the first century evidence.
Then, we will cite several Jewish works of this era that show the
significance of Jerusalem’s fall to the Jewish mind. Finally, a long list
of sources from later (ante-Nicene) Christian tradition showing the
significance of the destruction of Jerusalem for apostolic and early
post-apostolic Christendom will be brought forward. Having done
this, it should become obvious that a silence on the matter in canoni-

65, James Moffatt, An Introduction to the Literature of the New Testament, 3 vols. (Ed-
inburgh: T, & T. Clark, 1911) 3:3.

66. Moule, Birth of the New Testament, ist od. (Cambridge: University Press, 1962), p.
128. In his third edition of the work (New York: Harper & Row, 1982), he has changed
his views on this matter,
