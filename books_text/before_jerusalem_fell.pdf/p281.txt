268 BEFORE JERUSALEM FELL

was represented as the deity incarnate in human form.”... At
Thera, for example, a pagan altar has been found which was dedi-
cated “to the almighty Caesar, the son of God.” .. . This divi flius
title was one of the most common and least conventional of what John
called blasphemias onomata.>

Archaeologists have in their possession a decree of the Synod of
the Province of Asia dated about 9 B.C. that has been preserved in
a letter of the proconsul to the cities of Asia.“ The decree commends
the celebration of “the natal day of the most divine Caesar [Augus-
tus] .”*’ This document notes very clearly that the emperor Augustus
was deemed to be the cause of Rome’s glorious condition:

[Whether the natal day of the most divine Caesar [Augustus] is to
be observed most for the joy of it or for the profit of it - a day which
one might justly regard as equivalent to the beginning of all things,
equivalent, I say, if not in reality, at any rate in the benefits it has
brought, seeing that there was nothing ruinous or that had fallen into
a miserable appearance that he has not restored. He has given another
aspect to the universe, which was only tm ready to perish, had not
Caesar — a blessing to the whole of mankind — been born. For which
reason each individual may justly look upon this day as the begin-
nings of his own life and physical being, because there can be no more
of the feeling that life is a burden, now that he has been born. ...

Resolved by the Greeks of the province of Asia, on the proposal of the
High-priest Apollonius... : Whereas the Providence which orders
the whole of human life has shown a special concern and zeal and
conferred upon life its most perfect ornament by bestowing Augustus,
whom it fitted for his beneficent work among mankind by filling him
with virtue, sending him as a Savior, for us and for those who come
after us, one who should cause wars to cease, who should set all things
in fair order, and whereas Caesar, when he appeared, made the hopes
of those who forecast a better future [look poor compared with the
reality], in that he not only surpassed all previous benefactors, but
left no chance for future ones to go beyond him, and the glad tidings

85, Moffatt, Revelation, pp, 807-809, Selwyn offers additional helpful insights into the
role of the Asiarch and Ashiarchess (the Asiarch’s wife) in promoting the imperial cult,
noting that these would eventually bring Christians “face to face with the imperial
cultus” ( Christian Prophets, p. 124).

86, Howard Clark Kee, The Origins of Christianity: Sources and Documents (Englewood,
Nd: Prentice-Hall, 1978), pp. 74-76.

87. Cited in Dud, D. 76.
