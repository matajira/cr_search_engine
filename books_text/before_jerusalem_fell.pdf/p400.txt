Modern Authors Index 391

Lane, William L., 81n, 288n.

Lange, John Peter, 9n, 46,

Langer, William L,, 144n.

Lardner, Nathaniel, 87.

Latourette, Kenneth Scott, 242,

Lawson, John, 87,91.

Laymen, Charles M., 325n.

Learsi, Rufus, 211 n, 235n,

Lebreton, Jules, 296n.

Lechler, Gotthard Victor, 35.

Lee, Francis Nigel, 35, 58,59, 103n.

Lewis, Greville P,, 18,

Lightfoot, Joseph B., 28,27,85,60,81,
86,88, 90n, 98, 94n, 177n, 179, 188,
228n, 295m, 322n, 324,325,828,

Lindsey, Hal, 6,8,347.

Lohse, Eduard, 278n,

Longnecker, Richard, 94n,

Licke, Gottfried C, F., 35, 804n,

Lathardt, Christoph Ernst, 35,

Luther, Martin, 11,

Macdonald, James M., 36, 48,49n51,
115, 118, 119n, 166, 288n, 252n, 804,
305, 380n, 344n.

McPherson, John, 255n.

Mahan, Mile, 255n.

Marivale, 214,

Marshall, Alfred, 128, 141.

Martineau, Russell, 159n, 168.

Masterman, B, W. G., 248, 258n,

Maurer, Christian, 127n,

Maurice, Frederick Denisen, 36.

MeHlale, John, 7n.

MeNeile, A. H., 168, 825n.

MeSorley, Joseph, 255n,

Meinardus, Otto F. A., 325n,

Merivale, Charles, 78, 296n.

Messenger, Ernest €.,296n.

Metzger, Bruce Manning, 106n, 155n,

156n, 187, 197n, 200,208, 247n, 333n.

Meyer, A., 168,

MGiffert, A. C., 168,
Michael, J. H., 825n,
Michaelis, John David, 36,
Michaelis, W., 825n,

Michaels, J, Ramsey, 81n, 288n.
Mickelsen, A, Berkeley, 18, 14n, 15, 16n,
Milburn, R, L,, 288n.,

Milik, J. ., 1990.

Miller, Andrew, 255n,

Milligan, William, 18n, 28,28, 169, 183,
200,204,

Mills, Jessie B., 80m,

Milman, Henry Hart, 2280.

M'llvaine, Charles Pettit, 36.

Moffatt, James, 8n, 28,48,44,48,61,
105n, 187,139n, 161, 152n, 160, 167,
181, 182, 188, 210, 260n, 261,268,
267, 268n, 271 n, 287,800, 315,318,
820, 828,824, 328, 880m,

Momigliano, A., 25,86, 228n, 272n.

Mommsen, Theodor, 36 228n, 296n.

Morgan, Charles Herbert, 36

Morns, Leon, 11, 18n, 26, 41n, 114, 196,
140, 152, 200, 201,208,204,208,
228n, 259, 260n, 261,268,285,287,
300, 310n, 818, 319,321, 822, 828n,
824,826,827, 880n.

Morrison, W. D., 228n.

Mosheim, John Laurence von, 55, 80n,
148n, 242n, 256n,207n.

Moule, ©, F. D., 25, 88n, 86, 173n, 1810,
182, 188, 220n, 226n, 227m, 228n,
258n, 288, 2960,

Moule, H.C. G., 825, 828n,

Mounce, Robert H., 4n, 17m, 18n, 48n,
in, 104, 105n, 185, 189, 141, 150,
158, 169, 175, 176,193n, 198n, 208n,
204,205,218, 222n, 228,259,261,
285,299,801,808,318, 319,820,821,
328n, 327n.

Moyer, Elgin §., 92n, 279.

Muilenburg, James, 183n.

Miller, Jac J., 825.

Muratori, L, A., 98.

Myers, J.M.,156n, 187.

Neander, John Augustus Wilhelm, 36
Nestle, Eberhard, 196n, 202,

Newbolt, M. B., 5.

Newman, Barclay M., Jr., 3n, 24,42,
