124 BEFORE JERUSALEM FELL

the ones who sought His death (John 11:53; Matt. 26:4; 27:1), who
paid to have Him captured (Matt. 26:14-15, 47; 27:3-9), who brought
false witnesses against Him (Matt. 27:59-62), who initially convicted
Him (Matt. 27:65-66), who turned Him over to Roman authorities
(Matt. 27:2, 11, 12; Acts 3:13), and who even arrogantly (and
disastrously!) called down His blood upon their own heads (Matt.
27:24-25). John even tells us in his Gospel that the Roman Procura-
tor, Pontius Pilate, sought to free Jesus, finding no fault in Him (John
18:38; 19; 12; cp. Acts 3:13). But the Jews demanded that the robber
Barabbas be released instead of Christ (John 18:39, 40), and that
Christ be immediately crucified (John 19:6, 15). They even subtly
threatened Pilate’s tenuous Roman procuratorship by affirming “we
have no king but Caesar” (John 19:14-15), suggesting that Pilate was
allowing Christ to supplant Caesar. And Jesus Himself, during the
course of these events, specifically pointed out to Pilate: “he who
delivered Me up to you has the greater sin” (John 19:11 ).

In Acts 2:22-23, 36, Peter laid the blame largely on Israel: “Men
of Israel, listen to these words: Jesus the Nazarene, a man attested.
to you by God with miracles and wonders and signs which God
performed through Him in your midst, just as you yourselves
know — this Man, delivered up by the predetermined plan and fore-
knowledge of God, you nailed to a cross by the hands of godless men
and put Him to death... . Therefore let all the house of Israel know
for certain that God has made Him both Lord and Christ — this
Jesus whom you crucified.” He does the same in a sermon in Acts
3:13-15a: “The God of Abraham, Isaac, and Jacob, the God of our
fathers, has glorified His servant Jesus, the one whom jou delivered
up, and disowned in the presence of Pilate, when he had decided to
release Him. But you disowned the Holy and Righteous One, and
asked for a murderer to be granted to you, but put to death the Prince
of life. ” He repeats this to the Jews in Acts 5:30 where he proclaims:
“The God of our fathers raised up Jesus, whom you had put to death
by hanging Him on a cross.”

Stephen, in Acts 7:52, declares the same fact as does Peter:
“Which one of the prophets did your fathers not persecute? And they
killed those who had previously announced the coming of the Right-
eous One, whose betrayers and murderers you have now become. ”
Paul concurs in 1 Thessalonians 2:14-15: “For you, brethren, became
imitators of the churches of God in Christ Jesus that are in Judea, for
