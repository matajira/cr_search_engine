Revelation Studies 7

short term. Both liberal and conservative theologians, as well as the
secular and scientific communities, have allowed their imaginations,
hopes, and fears to be captivated by the looming of that magic year,
the year 2000. Even dispensationalist historian Dwight Wilson has
lamented: “As the year 2000 approaches there will undoubtedly be
increased interest in premillenarian ideas and even more hazardous
speculation that this third millennium will be the Thousand Year
Kingdom of Christ.”9 In his philosophico-theological treatise on
futurology, Ted Peters dedicated his entire first chapter — “Toward
the Year 2000” — to a survey and analysis of the interest the year
2000 is already generating. Regarding the interest in the year 2000,
he notes with some perplexity: “It is a curious thing that as we
approach the year 2000 both the secular and scientific communities
are taking a millennialist perspective. . .. All this has given rise to
a new academic profession: namely, futurology. “2° Examples could
be multiplied to the point of exhaustion.

Eschatological inquiry should be a genuinely Christian concern
in that it is fraught with tremendous moral and cultural, as well as
spiritual, implications. Regrettably, prophetic studies have been so
dominated by a naive sensationalism that they have become a source
of embarrassment and grief to many in conservative Christendom.
No book has more trenchantly pointed out the ill-conceived sensa-
tionalism of the modern prophecy movement than Dwight Wilson’s
carefully researched and profusely documented Armageddon Now!”!

The only comfort to be derived from this lamentable situation is
that this generation is not the only one to suffer through such. This
seems to be what Justin A. Smith had in mind when late in the last
century he observed: “Perhaps there is no book of the Bible the
literature on which is in a certain way so little helpful to an expositor
as that of the Apocalypse.”** Or as church historian Philip Schaft

19, Dwight Wilson, Armageddon Now! (Grand Rapids: Baker, 1977), p. 18.

20, Petera, Futures, p. 9, In regard to futurology studies, see for instance: Alvin Tofller,
Future Shock (Toronto: Bantam, 1970); Paul R, Ehrlich, The Population Bom’ (New York:
Ballantine, 1968); John McHale, The Future of the Future (New York George Braziller,
1969); Robert Theobald, Beyond Despair (Washington: New Republic, 1976); Victor
Ferkiss, The Future of Technological Civilization (New York: George Braziller, 1974); Charles
A. Reich, The Greening of America (New York: Bantam, 1970).

21, Wilson, Armageddon Now, passim.

22, Justin A, Smith, Commentary on the Revelation, in Alvah Hovey, ed., An American
Comtnentory on the New Testament (Valley Forge Judson, [1884] rep.n.d.),p. 4,
