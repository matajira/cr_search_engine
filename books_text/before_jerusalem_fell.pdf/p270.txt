256 BEFORE JERUSALEM FELL

find not only particular personages (Nero), cultural structures (the
Jewish Temple), and historical events (the Neronic Persecution and.
the Jewish War) that harmonize well with the Neronic era, but even
time-frames for these that fill out the picture of the era of which John
wrote. It can be no other than in the mid- to late A.D. 60s.

 

W. Simpson, An Epitome of the History of the Christian Church During the First Three Centuries
and of the Reformation in England, 3rd ed. (Cambridge Macmillan, 1857), p. 67.

David Churchill Somervell, A Short History of our Religion (New York: Macmillan,
1922), p. 129,

David D. Van Antwerp, Church History, Sth od., vol. 1 (New York: James Pott, 1884),
pp. 27, 42,

B. F. Westeott, The Tuo Empires: The Church and the World (London: Macmillan, 1909),
pp. 324.

Edward Backhouse, Early Church History to the Death of Constantine, 8rd ed. (London:
Simpkin, Marshall, Hamilton, Kent, 1892), p. 11.

W. D, Killen, The Ancient Church: Its History, Doctrine, Worship, Constitution, Traced for the
First Three Hundred Years, ed. John Hall (New York Anson D, F. Randolph, 1883), pp.
147.
