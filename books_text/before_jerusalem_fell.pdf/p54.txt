Tie Approach to the Question of Dating a7

Eduard Wilhelm Eugen Reuss, History of the Sacred Scriptures of the New
Testament (Edinburgh: T. &T. Clark, 1884). [3,4,6]

Jean Reville, Rev. d. d. Mondes (Oct., 1863 and Dec., 1873). [3]”

Jd. W. Roberts, The Revelation to John (Austin, TX: Sweet, 1974).

Edward Robinson, Bibliotheca Sacra, vol. 3 (1843), pp. 532ff.

dohn A, T. Robinson, Redating the New Testament (Philadelphia: West-
minster, 1976).

J. Stuart Russell, The Parousia (Grand Rapids: Baker, [1887] 1983).

W. Sanday (1908).58

Philip Schaff, History of the Christian Church, 3rd cd., vol. 1: Apostolic
Christianity (Grand Rapids: Eerdmans, [1910] 1950), p. 834.

Johann Friedrich Schleusner.?

J. H. Scholten, de Apostel Johannis in Klein Azie (Leiden: 1871). [1]

Albert Schwegler, Das Nachapostol Zeitalter (1846). [3]

J.J. Scott, The Apocalypse, or Revelation of S. John the Divine (London:
John Murray, 1909).

Edward Condon Selwyn, The Christian Prophets and the Apocalypse
(Cambridge: 1900); and The Authorship of the Apocalypse (1900).

Henry C. Sheldon, The Early Church, vol. 1 of History of the Christian
Church (New York: Thomas Y. Crowell, 1894), pp. 112ff.

William Henry Simcox, The Revelation of St. John Divine. The Cam-
bridge Bible for Schools and Colleges (Cambridge: Cambridge
University, 1893).

D. Moody Smith, “A Review of John A. T. Robinson’s Redating the
New Tesiament,” Duke Divinity School Reciew 42 (1977): 193-205.

Arthur Penrhyn Stanley, Sermons and Essays on the Apostolic Age (8rd
ed: Oxford and London: 1874), pp. 234ff. [6]

Rudolf Ewald Stier ( 1869). [3]

Augustus H. Strong, Systematic Theology (Old Tappan, Nd: Revell,
[1907] 1970, p. 1010).

57. For soures documentation see Milligan, Apocalypse, p, 142,

58. Cited in Hort, Apocalypse, D. iv.

59, Cited in P. 8, Desprez, The Apocalypse Fulfilled, 2nd ed. (London: Langman,
Brown, Green, Longmans, 1855), p. 2.
