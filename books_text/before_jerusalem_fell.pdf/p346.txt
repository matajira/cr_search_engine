334 BEFORE JERUSALEM FELL

A Summation of the Early Date Argument

In attempting to demonstrate the proper dating of this most
influential book of our sacred canon, our investigation carefully con-
sidered both the external and internal witness of Revelation. Al-
though much weight has long been credited the external evidence,
especially that associated with Irenaeus, we noted that such a proce-
dure is in danger of quieting the voice of God in deference to the voice
of man. That is, when engaged from the perspective of an unflinching
commitment to Scripture as the Word of God, it should be the
procedure of Biblical Introduction to allow the most weight to the
Scripture’s seif-iestimony regarding its own historical composition. In
deference to common practice, however, and in light of the nature of
the present work as largely concerned with a rebuttal to the current
late date position, we began with an inquiry into the external consid-
erations of tradition.

The External Witness

In the portion of this study dealing with the external evidence,
we gave extensive consideration to the statement of Irenaeus regard-
ing Revelation’s date. There we noted that the commonly received.
interpretation of Irenaeus is not without ambiguity. The all-impor-
tant question in the matter is: Did Irenaeus mean to say that Revela-
tion was seen by John in Domitian’s reign? Or did he mean that Jota,
who saw the Revelation, was seen in Domitian’s reign? By the very
nature of the case, verbal inflection alone is incapable of resolving the
matter. More helpful are the contextual indicators available that
suggest Irenaeus meant the latter of the two options.

Even if this re-interpretive approach to Irenaeus fails, however,
we pointed out that Irenaeus was subject to error — even on matters
he claims to have heard from first-hand sources (such as when he
asserted that Jesus lived to be almost fifty years old). It is time for
biblical scholars and Church historians to consider afresh Irenacus’s
statement regarding Revelation. Especially is this the case since so
much weight is granted to his witness, despite its ambiguity.

Additional inquiry into the other major late date witnesses from
tradition turned up some rather surprising facts: The alleged evi-
dence from both Clement of Alexandria and Origen — the two most
important witnesses after Irenaeus — actually requires a reading of
the name “Domitian” into their texts at crucial points. Otherwise,
