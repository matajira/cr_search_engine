116 BEFORE JERUSALEM FELL

the Twelve Tribes, assumed intact in Revelation 7:4-8.°

As we seek to establish an early date for Revelation in this the
major portion of our study, honesty compels us to admit at the outset
that there are many good scholars of both schools of dating who agree
with the assertion of late date advocate Martin Kiddie: “There is no
direct evidence in REVELATION itself to indicate any precise date
for its composition.”g Guthrie admits the significance of internal
evidence in matters of Introduction, but, in the case before us, doubts
if Revelation offers any.'° Even early date advocate F. J. A. Hort is
not really persuaded that there are direct internal evidences leading
in this direction. Regarding those offered — such as those mentioned
above — he doubts whether we should “lay much stress upon them.”
He deems the positive internal evidences as merely “interesting.” |!

The critical determination of noted early date advocates of Hort’s
line of thinking is founded upon an evaluation that gives more weight
to discreet literary and psychological indicators than to what many
early date advocates deem to be direct statements of chronological
significance or objective historical allusions. Though these are, never-
theless, internal indicators (they have to do with self-witness, rather
than with tradition), they tend to be more subjective or atmospheric
than objective and concrete. For instance, Hort lists two “grounds for
asserting the Neronian date” that seemed to him to be “decisive”:

(1) The whole language about Rome and the empire, Babylon and
the Beast, fits the last days of Nero and the time immediately follow-
ing, and does not fit the short local reign of terror under Domitian.
Nero affected the imagination of the world as Domitian, as far as we
know, never did....

(2) The book breathes the atmosphere of a time of wild commo-
tion. .. . Under Vespasian, however, the old stability seemed to
return: it lasted on practically for above a century more. Nothing at
all corresponding to the tumultuous days after Nero is known in
Domitian’s reign, or the time which followed it... . It is only in the

8, Philip Schaff, Hisiory of the Christian Church, 8 vols. (Grand Rapids: Eerdmans,
[1910] 1950) 1:835m.
9, Martin Kiddie, The Revelation of St, John (New York: Harper and Bros,, 1940), p.
xxxvi. That he holds to a late date theory can be seen on p. xl of his work,
10, Donald Guthrie, New Testament Introduction, 8rd ed. (Downers Grove, IL: Inter-
Varsity Press, 1970), p. 957. Emphasis mine,
11, F. J. A. Hort, The A pocalypse of St. John: 1-117 (London: Macmillan, 1908), p. xxviii.
