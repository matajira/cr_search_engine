The Nero Redivivus Myth 303

throne are recorded to have employed the myth in quests for power.
In the pagan literature references to the expectation of Nero’s return
after his fall from power can be found in the writings of Tacitus,
Suetonius, Dio Cassius, Xiphilinus, Zonaras, and Dion Chrysos-
tom. ? Among Jewish Talmudists the myth surfaces in the tract
Gittin, 8 The Ascension of Isaiah (4:24), as well as in the Jewish
Sibylline Oracles. In Christian circles it is mentioned in books by
Lactantius, Sulpicius Severus, Jerome, and Augustine. '4 Several Sib-
ylline Oracles of various origins — Christian, Jewish, and pagan — use
the myth, as well. 5

Clearly the existence, spread, and influence of the Nero Redivious
myth cannot be disputed. It is a unique legend in all of political
history. But the questions that must here be dealt with are: Does
Revelation employ the myth? And if it does, does the employment of
it necessitate a late date for the composition of Revelation?

Early Date Response
if the Myth Be Accepted

Despite the confidence with which some late date advocates
employ the Nero Redivivus myth, it is of more than a little interest to
note briefly two intriguing facts. First, not all late date proponents
allow the argument as significant to the question of the dating of
Revelation. While establishing the arguments for the Domitianic date
for Revelation, Guthrie, a most able late date adherent, considers the
merits of the Nero Redivious argument, but discourages its endorse-
ment in the debate: “[I] t must be regarded as extremely inconclusive
for a Domitianic date. The most that can be said is that it may
possibly point to this.” '

Astonishingly, Mounce uses the myth as an evidence for the late
date of Revelation in his introduction to his commentary, but then

12, Taeitus, Histories 1:2; 2:8, 9; Suetonius, Nero 40, 57; Domitian 6; Dio Cassius, Roman
History 63:9:3; 66: 19:3; Xiphilinus 64:9; Zonaras, Annals 11:151-8; and Dion Chyrsostom,
Oratwru2t.

13, See Frederic W. Farrar, The Zarly Days of Christianity (New York: Gasscll, 1884),
p. 467,

14, Lactantius, On The Death of the Persecutors 2; Sulpicius Severus, Sacred History 2:28;
Jerome, Daniel 11:28; and Augustine, The City of God 20:19:3.

1B, Sibylline Oracles 3-630; 4: LSM; 5:3340; 8:68f6 ; 12:78; 13:890f.

16. Donald Guthrie, New Testament Introduction, 8rd. ed. (Downers Grove, IL: Inter-
Varsity Press, 1970), p. 954.
