320 BEFORE JERUSALEM FELL

an enrichment of the church would not be difficult to imagine.

Despite the prima facie plausibility of this argument it does not
carry sufficient weight to serve as an anchor for the late date theory.
Some suspicion is immediately cast on the argument when it is noted
that it is avoided by such noteworthy late date advocates as conserva-
tive scholars Swete and Guthrie, and such liberal proponents as
Charles and Moffatt.’The refusal of these scholars to make reference
to this argument is not necessarily destructive to the cause, of course.
But it is at least curious that such vigorous liberal and conservative
advocates do not deem it to be of merit.

The Nature of the “Riches”

We should note also that it may be that the reference to “riches”
made by John is a reference to spiritual riches, and not to material
wealth at all.

These riches and other goods in which the Laodicean Church and
Angel gloried we must understand as spiritual riches in which they
fondly imagined they abounded... . [T]his language in this appli-
cation is justified by numerous passages in Scripture: as by Luke xii.
21; 1 Cor. i:5; 2 Cor. viii. 9; above all, by two passages of holy irony,
1 Cor. iv. 8 and Hos. xii. 8; both standing in very closest connexion
with this; I can indeed hardly doubt that there is intended a reference
to the latter of these words of our Lord. The Laodicean Angel, and.
the church he was drawing into the same ruin with himself, were
walking in a vain show and imagination of their own righteousness,
their own advances in spiritual insight and knowledge.’

      
 

 

 

A good number of commentators suggest allusion here to 1 Corinthi-
ans 4:8 and Hosea 12:8. Additional passages such as Luke 18:11, 12;
16: 15; and 1 Corinthians 13:1 can be consulted as well. If this
interpretation of “riches” in Revelation 3:17 is valid, then the entire
force of this argument is dispelled. Surprisingly, this is even the view
of Mounce: “The material wealth of Laodicea is well established.
The huge sums taken from Asian cities by Roman officials during the
Mithridatic period and following indicate enormous wealth. ...
The ‘wealth’ claimed by the Laodicean church, however, was not

8, See references to their works cited above.
9. R. C. Trench, Commentary on. the Epistles to tu Seven Churches, 4th ed, (London:
Macmillan, 1883), p. 210.
