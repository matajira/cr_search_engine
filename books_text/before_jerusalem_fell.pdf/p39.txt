22 BEFORE JERUSALEM FELL

conviction as to the canonicity and significance of Revelation and is
based, as best the writer is able to discern, on the most compelling
of evidence.

Furthermore, in that Revelation is canonical Scripture it there-
fore possesses the attributes of Scripture, including absolute author-
ity, truthfulness, and inerrancy. Revelation’s authority is the author-
ity of the voice of the Living God and the Exalted Christ. The
truthfulness of the book, therefore, is impeccable. Consequently,
Revelation does not err in any of its assertions, prophecies, or impli-
cations.

This assumption will be shown to matter a great deal when the
actual argument for Revelation’s dating is begun, for the argument
will greatly stress Revelation’s internal witness. As will be shown, the
internal witness must be given the highest priority.

Authorship

Second, an assumption that is open to debate even among conser-
vative scholars but will not receive attention in the present research
is the Johannine authorship of Revelation. The position of the present
writer is that Revelation was written by the Apostle John, the son of
Zebedee, the disciple of Christ. This John is also held to be the
human author of the Gospel ofJohn and the three epistles ofJohn.

Now, of course, Revelation does not specifically designate the
author as “the Apostle John. ” The opening statements of Revelation
mention only that “John” wrote it without specifying which particu-
lar John. Thus, to assert that the writer was not the Apostle would
not be to deny our first assumption regarding its canonicity. Apostolic
authorship may be an indicator of canonicity, but it is not a sine qua
non of it. The New Testament includes several books not written by
the original Twelve Apostles: Mark, **Luke, the Pauline epistles,
James, Jude, and Hebrews.

Nevertheless, the present writer is well aware of the various
arguments against Johannine authorship. '© The matter of authorship

15. Even if we accept the widespread and very credible view of tradition that Mark
was writing under the direction of Peter, it remains that the author was Mark; in contrast
to the epistles of Peter, which were written by the apostle.

16, Among the more serious arguments against an apostolic authorship are the
following (1) The author claims to be a “prophet” and not an “apostle.” (2) The author
names himself; contrary to John’s writings. (8) There are no allusions to incidents in the
