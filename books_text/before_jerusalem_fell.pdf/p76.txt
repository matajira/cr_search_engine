60 BEFORE JERUSALEM FELL

dom and moderation. He is neither very original nor brilliant, but
eminently sound and judicious. “5° He is an extremely helpful witness
to many matters of historical significance for the understanding of
early Church history.

Unfortunately, however, “Second-century traditions about the
apostles are demonstrably unreliable.”5'And although generally reli-
able, Irenaeus’s writings are not without imperfection in matters
historical. Indeed, some very fine and reputable scholars of renown
discount his testimony that is s0 relevant to our debate. Robinson
notes that “despite this [the testimony of Irenaeus to a late date],
Hort, together with Lightfoot and Westcott, none of whom can be
accused of setting light to ancient tradition, still rejected a Domitianic
date in favour of one between the death of Nero in 68 and the fall of
Jerusalem in 70. It is indeed a little known fact that this was what.
Hort calls ‘the general tendency of criticism for most of the nineteenth
century, and Peake cites the remarkable consensus of ‘both advanced
and conservative scholars’ who backed it .”©° The Oxford University
classical historian B. W. Henderson agrees, and adds that

Irenaeus, the earliest extant authority, dates the [Revelation] under
Domitian. His date, however, is c. 180 A. D., and if the Apocalypse
enjoyed strange vicissitudes of neglect and esteem immediately after
Irenaeus, as with Caius, Hippolytus, and the author of the Murato-
rian fragment, it not improbably did before, especially when years
passed. ... Irenaeus’ testimony to its authorship is perhaps more
valuable than to its date. He abandons the task of interpretation in
despair and with it the internal evidence which here on the question
of date is more valuable than one piece of external evidence, not ‘a
generation’ only later but a century.)

Farrar, speaking of Papias’s statement regarding the fertility of
the vines in the millennium that is recorded by Irenaeus,®? makes a
relevant and noteworthy observation:

Experience shows that a story told second-hand, even by an honest

58, Schaff, History 2:750.

59. G. B, Caird,A Commentary on the Revelation of St. John the Divine (New YorkHarper
& Row, 1966), p. 4.

60, Robinson, Redating, pp. 224-225,

61, Henderson, Nero, p. 442,

62, Irenaeus, Against Heresies 5:33:3.
