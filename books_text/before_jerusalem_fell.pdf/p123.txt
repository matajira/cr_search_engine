Additional External Witnesses 107

Revelation. He frequently challenges, however, other interpreters of
his era who apply several of the prophecies of Revelation to the
Jewish War under Vespasian and Titus.gl At Revelation 6:12, for
instance, he writes: “There are not wanting those who apply this
passage to the siege and destruction of Jerusalem by Titus.” On the
interpretation of Revelation 7:1 he comments: “These things are
referred by some to those sufferings which were inflicted by the
Remans upon the Jews.” On Revelation 7:2 he observes: “Although
these things happened in part to Jewish Christians, who escaped the
evils inflicted on Jerusalem by the Remans, yet they more probably
refer to Anti-christ.” From such statements it would appear evident
that there were several (there are not wanting” and “some”) noted.
commentators who flourished in the sixth century (or before!) who
necessarily held to a pre-A.D. 70 date for Revelation.

Arethas

According to A. R. Fausset, “Arethas, in the sixth century,”
applies the sixth seal to the destruction of Jerusalem (70 A.D.),
adding that the Apocalypse was written before that event .“0°Like
Andreas, he wrote a commentary on Revelation. Desprez cites Are-
thas’s comments on several verses .94 On Revelation 6:12 Arethas
writes: “Some refer this to the siege of Jerusalem by Vespasian.” On
Revelation 7:1 he notes: “Here, then, were manifestly shown to the
Evangelist what things were to befall the Jews in their war against
the Romans, in the way of avenging the sufferings inflicted upon
Christ.” Of Revelation 7:4 we read: “When the Evangelist received.
these oracles, the destruction in which the Jews were involved was
not yet inflicted by the Remans.”

Stuart records some additional observations from Arethas’s com-
mentary worthy of consideration. 95 In his comments on Revelation
1:9, Arethas writes: “John was banished to the isle of Patmos under

91, See Stuart, Revelation 1:267; Desprez, Apocalypse, p. 7.

92, Some scholars, most notably Stuart (Apocalypse 1:268) and Fausset (Jamieson,
Fausset, Brown, Commentary) assign Arcthas to the sixth century. Others assign him
much later to ¢, A.D. 914, For example Swete, Revelation, p. cxcix (on the strength of
Harnack’s argument); and Kurt Aland, et. al.,7he Greek New Testament, 8rd ed. (London:
United Bible Societies, 1975), p. xxvii.

98. Fausset, in Jamieson, Fausset, Brown, Commentary 2:548.

94, Desprez, Apocalypse, p. 7.

95. Stuart, Apocalypse 1:268,
