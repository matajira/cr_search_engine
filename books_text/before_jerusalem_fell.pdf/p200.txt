186 BEFORE JERUSALEM FELL

because they did harm to the son of the heavenly
God. ...

Then when the Hebrews reap the bad harvest,

a Roman king will ravage much gold and silver.

Thereafter there will be other kingdoms

continuously, as kingdoms perish

and they will afflict mortals. But there will be

a great fall for those men when they launch on
unjust haughtiness.

But when the temple of Solomon falls in the illus-
trious land

cast down by men of barbarian speech

with bronze breastplates, the Hebrews will be
driven from their land;

wandering, being slaughtered, they will mix much
darnels in their wheat.

There will be evil strife for all men;

and the cities, violated in turn,

will weep for each other on receiving the wrath of
the great

God in their bosom, since they committed an evil
deed.”

Collins notes the reference to the Roman king and states that it is “an
obvious reference to the defeat of the Jews in A.D. 70”; he further
notes that the reference to Solomon’s Temple in verse 393 “refers to
the same event.”8 Here is a clear Christian reference — and as-
suredly an early one — to the destruction of Jerusalem as a vindica-
tion of Christianity and a judgment on the Jews for harming “the son
of the heavenly God.”

Second, the Jewish writers of this era (and shortly thereafter) feel
the pain and anguish of the loss of Jerusalem, a pain that cannot but
be useful to those who follow the One who prophesied its destruction,
Jesus Christ (Matt. 24:2, parallels).

2 Esdras is almost certainly to be dated about the year 100 in its
original form. This date is argued by such noted scholars as G. H.

85, Ibid, 1:3436
96. Ibid, 1 :344n,
