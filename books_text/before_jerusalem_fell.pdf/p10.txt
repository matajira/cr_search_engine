x BEFORE JERUSALEM FELL

If we do not understand the historical context (withtext”), we will
have trouble understanding the text itself. If we fail to understand
both text and context, we risk misapplying the text’s message in our
lives. In the case of no other book of the New Testament has an error
in dating led to more misinterpretations and misapplications than the
Bock of Revelation.

Third, there is no doubt that the intellectual attack on the
integrity of the Bible's manuscripts has been the most important
single strategy of covenant-breaking modern Bible scholars.” I refer
here to the academic specialty known as higher criticism of the
Bible.} A large part of this attack involves the dating of the Bible’s
original texts. The presupposition of all higher critics of the Bible is
that the biblical texts, especially the prophetic texts, could not possi-
bly have been written at the time that the texts insist that they were
written. To admit that they were written when the texts say that they
were written would be to admit that mortals, under the inspiration
of the Holy Spirit, can accurately predict the future. This would
destroy the most cherished assumption of the humanist: the sover-
eignty of man. If this ability to forecast the future actually exists, the
future is not only known to the revealer, it is foreordained by some-
thing beyond man’s power to alter. This points clearly to the absolute
sovereignty of God, and the humanist rejects this doctrine with all
his heart.’

Prophecy Fulfilled

In 1987, my publishing company, Dominion Press, published
David Chilton’s book, The Days of Vengeance: An Exposition of the Book

2, Writes Old Testament theologian Walter Kaiser: “For many it is too much to
assume that there is consistency within one book or even a series of books alleged to
have been written by the same author, for many contend that various forms of literary
criticism have suggested composite documents often traditionally posing under one single
author, This argument, more than any other argument in the last two hundred years,
has been responsible for cutting the main nerve of the case for the unity and authority
of the biblical message.” Walter Kaiser, Jr., Toward Old Testament Ethics (Grand Rapids,
Michigan: Zondervan Academie, 1983), p. 26.

8, See Oswald T. Allis, The Five Books of Moses (2nd od.; Phillipsburg, New Jersey:
presbyterian & Reformed, [1949); Allis, The Old Testament: lis Claims and. Its Critics
(Nuitey, New Jersey: Presbyterian & Reformed, 1972),

4, Very few Arminians (“free-will Christians”) discuss the topic of biblical prophecy
in terms of God's absolute sovereignty. They may enjoy discussing Bible prophecy; they
do not enjoy discussing the predestinarian implications of Bible prophecy.
