The Role of Internal Evidence 117

anarchy of the earlier time that we can recognise a state of things that
will account for the tone of the Apocalypse. ?

These observations are quite suggestive and will be dealt with
later. Yet despite Hort’s hesitancy at acknowledging positive, objec-
tive historical indicators in Revelation, his use of the literary and
subjective arguments is helpful to formulating the early date position.
Even early date advocates who recognize more objective historical
indicators within Revelation often make use of the subjective data as
well. For instance, Stuart considers the psychological implications of
a late date composition when he notes that “the fiery phantasy or
lively imagination everywhere exhibited in the Apocalypse, can with
more probability be predicted ofJohn at some sixty years of age, than
at eighty-five or ninety.” '° Robinson follows suit when he surmises
that “it is difficult to credit that a work so vigorous as the Apocalypse
could really be the product of a nonagenarian, as John the son of
Zebedee must by then have been, even if he were as much as ten
years younger than Jesus.”!*

Beyond such psychological implications, there are also the liter-
ary implications. Westcott states the older literary argument ably
when he writes:

The irregularities of style in the Apocalypse appear to be due not so
much to ignorance of the language as to a free treatment of it, by one
who used it as a foreign dialect. Nor is it difficult to see that in any
case intercourse with a Greek-speaking people would in a short time
naturally reduce the style of the author of the Apocalypse to that of
the author of the Gospel. It is, however, very difficult to suppose that
the language of the writer of the Gospel could pass at a later time in
a Greek-speaking country into the language of the Apocalypse. .. .

Of the two books the Apocalypse is the earlier. It is less developed
both in thought and style. The material imagery in which it is
composed includes the idea of progress in interpretation. ...

The Apocalypse is after the close of St. Paul’s work. It shows in its
mode of dealing with Old Testament figures a close connexion with
the Epistle to the Hebrews (2 Peter, Jude). And on the other hand it

12, Ibid, DP. XXVi, xxvii,

13. Moses Stuart, Commentary on. the Apocalypse, 2 vols. (Andover: Allen, Merrill, and
Wardwell, 1845) 1:280.

14, Robinson, Redating, p. 222,
