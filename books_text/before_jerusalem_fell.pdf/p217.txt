The Role of Nero Caesar 203

the Beast’s number in the unadulterated text does refer to Nero
Caesar (as seems evident from the evidence cited above); and if this
fact would be recognizable with a degree of effort by the original
recipients of the letter (as should be most likely if Revelation was
written to be understood by, rather than to taunt, the persecuted.
recipients); then it should be no mere coincidence that 616 is the
numerical value of “Nero Caesar” when spelled in Hebrew by trans-
literating it from its Latin spelling. This would seem satisfactorily to
explain the rationale for the divergence: so that the non-Hebrew
mind might more readily discern the identity of the Beast. Even
Guthrie, who rejects the Nero theory, grants that this variant gives
the designation Nero “a distinct advantage.”“* As Metzger writes:
“Perhaps the change was intentional, seeing that the Greek form
Neron Caesar written in Hebrew characters TO? 773 is equivalent
to 666, whereas the Latin form Nero Caesar DP V3 is equivalent
to 616.”45 Thus, rather than either being inconsequential to or over-
throwing the Nero theory of 666, the textual variant provides a
remarkable confirmation of the theory.

Objections to the Nero Theory

Despite the above evidences, the arguments have not convinced
all New Testament scholars.** A variety of objections is put forward.
by dissenters from the Nero theory. Before moving on to other brief
allusions to Nero as the Beast in Revelation, some of the leading
objections will be given due consideration. These will be stated first,
then returned to subsequently for a seriatim analysis.

(1) The earliest fathers were unaware of this designation, as
indicated particularly in that Irenaeus knew nothing of the Nero
theory, even with the 616 vanant. As Morris puts it: Irenaeus does
not “even include Nero in his list, let alone regard this as a likely
conjecture. “4’ In addition, Morris notes: “It is also to be borne in
mind that in the ancient world when Nero was a considerable fig-

44, Donald Guthrie, New Testament Introduction, 8rd. ed. (Downers Grove, IL: Inter-
Varsity Press, 1970), p. 959.

45. Mewger, Textual Commentary, p, 752,

46. Indeed, some, such as Mounce (Revelation, p. 264), are convinced on the basis of
the long standing debate that we cannot know the answer.

41, Morris, Revelation, p. 38.
