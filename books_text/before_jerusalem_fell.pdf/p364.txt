A Response to House and Ice 353

Conclusion

Space fails our responding to other aspects of the argument by
House and Ice. Perhaps I will develop them at more length in the
book-length response to their Dominion Theology. Yet I believe that if
anyone were to consider the few problems associated with their
Chapter 12, which I have noted above, he would quickly see that as
presented, the argument by House and Ice is fraught with miscon-
ception and error. Though they disparage employing the “debater’s
technique” of casting “doubt upon the reliability of the source,” I
must confess that as far as the “Reconstruction debate” goes, I
seriously question the reliability of House and Ice.

69, House and Ice note that theirs is but the first of several book-length responses to
Reconstructionism in the works (Dominion Theology, p. 9). Perhaps they were a little too
hasty in attempting to beat the others to the punch, It may be that the other responses
vill be a little more careful in their presentations and will require analysis from a different
perspective,
