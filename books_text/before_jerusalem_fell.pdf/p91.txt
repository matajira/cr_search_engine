Clement of Alexandria 75

Another passage found in Sibylline Oracles 4:115-124 teaches that
Nero had fled Rome to Parthia, from whence he would come to terrify
Rome.

Two impostors claiming to be Nero are mentioned in profane
history, one, in A.D. 69 and the other twenty years later.“Their
attempts to deceive and to gain power required the pervasive belief
in Nero’s being alive and in hiding.

Book 5 of the Sibylline Oracles is also a Jewish composition,
written for the most part sometime after A.D. 80. In this book “the
evil of Nero has the same three dimensions as the evil of Rome: he is
morally evil, he was responsible for the destruction of Jerusalem,
since the Jewish war began in his reign, and he claimed to be God. “4°
There we read:

One who has fifty as an initial will be commander,

a terrible snake, breathing out grievous war, who
one day will lay hands

on his own family and slay them, and throw every-
thing into confusion,

athlete, charioteer, murderer, one who dares ten
thousand things.

He will also cut the mountain between two seas
and defile it with gore.

But even when he disappears he will be destruc-
tive. Then he will return

declaring himself equal to God. But he will prove
that he is not.

Three princes after him will perish at each other’s
hands.

Later in the same book Nero’s return from Persia is envisioned.”
He is called

a savage-minded man, much-bloodied, raving non-

48. Tacitus, Histories 2:8,9; Dio Cassius, Rom History 649; Suetonius, Nero 57.

44, Collins, “Sibylline Oracles,” OTP 1:390.

45, Ibid.

4G, Sibpiline Oracles 5:28-35; OTP 1:393,

47, Sibylline Oracles 5:93-110. See Collins, “Sibyiline Oracles,” OTP 1:395, notes y and
2.
