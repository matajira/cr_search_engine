Copyright ©1989 by Kenneth L. Gentry, Jr., Th.D.

All rights reserved. No part of this publication maybe reproduced, stored
in a retrieval system, or transmitted in any form or by any means, except
for brief quotations in critical reviews or articles, without the prior, written
permission of the publisher. For information, address Dominion Press,
Publishers, Post Office Box 8204, Fort Worth, Texas 76124.

Unless otherwise noted, Scripture references are taken from the New
American Standard Bible, © 1960, 1962, 1963, 1968, 1971, 1972, 1973,
1975, 1977 by The Lockman Foundation. Used by permission.

The following publishers have generously given permission to use extended
quotations from copyrighted works:

eRedating the New Testament, by John A. T. Robinson ‘SCM Press Ltd.
1976. Published in the U.S.A. by The Westminster Press. Reprinted and
used by permission.

The Book of Revelation, by Robert Mounce ©1977 by Wm. B. Eerdmans
Publishing Co. Published in the U.S.A. by Wm. B. Eerdmans. Re-
printed and used by permission.

«The Relation of St. John, by Leon Morris © 1969 by The Tyndale Press.
Published in the U.S.A. by Wm. B. Eerdmans. Reprinted and used by
permission,

The Old Testament Pseudepigrapha, by James H. Charlesworth. Copyright
© 1983, 1985 by James H. Charlesworth. Reprinted by permission of
Doubleday, a division of Bantam, Doubleday, Dell Publishing Group,
Ine.

«New Testament History, by F. F. Bruce. Copyright ©1969 by F. F. Bruce.
Reprinted by permission of Doubleday, a division of Bantam, Doubleday,
Dell Publishing Group, Inc.

+The Birth of the New Testament by C. F. D. Moule. Copyright © 1981 by
Charles Francis Digby Moule. Reprinted by permission of Harper &
Row, Publishers, Inc.

Published by the Institute for Christian Economics
Distributed by Dominion Press, Fort Worth, Texas
Typeseiting by Nhung Pham Nguyen

Printed in the United States of America

ISBN 0-930464-20-6
