100 BEFORE JERUSALEM FELL

taught that John was banished by Domitian. That which is striking
about this traditional evidence, however, is that John, who was
doubtless well into his 90s, could be condemned to the mines:

Inevitably, therefore, when Domitian began his policy of persecution
in 96, St. John must have been somewhere between ninety and a
hundred years old. We are asked to believe that at that great age he
was able to stand the journey as a prisoner from Ephesus to
Rome - that is possible, for St. Polycarp stood it — to go through a
trial before the Emperor; to be scourged publicly and cruelly in the
Forum; to be banished to Patmos and to work under the lash in the
mines; and, after having endured all this, to return to Ephesus still
possessed of enough vigour to... reorganize the Churches of Asia
and to survive, in spite of all this activity, for several years more.**

This difficulty is similar to that expressed above regarding Clement
of Alexandria. Such difficulties tax to the very limit the credibility of
the reference.

The Acts of John

There is also possible evidence to be garnered from the apocry-
phal The Acts of John. In this work — which is mentioned by Eusebius,
Epiphanies, Augustine, and Photius — there is the establishment of
a Domitianic exile, to be sure. But the rationale for the exile is
suggestive of a prior publication of Revelation. And it could be that
John was banished twice, once under Nero and later under Domitian
(which would explain the two traditions of a Neronic and Domitianic
exile). In Zhe Acts ofJohn we read:

And the fame of the teaching of John was spread abroad in Rome;
and it came to the ears of Domitian that there was a certain Hebrew
in Ephesus, John by name, who spread a report about the seat of
empire [sic] of the Remans, saying that it would quickly be rooted
out, and that the kingdom of the Remans would be given over to
another, And Domitian, troubled by what was said, sent a centurion
with soldiers to seize John, and bring him. .. . [Later when John
appeared before Domitian, we read:] And Domitian, astonished at all
the wonders, sent him away to an island, appointing for him a set
time,

And straightway John sailed to Patmos.

64, Barnes, Christianity ab Rome, p. 166.
65, See ANF 8:560-562.
