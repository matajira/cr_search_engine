aT

THE CONTEMPORARY
INTEGRITY OF THE TEMPLE

Another noteworthy historical datum in Revelation is found in
Revelation 11 where we discover a reference to the Temple. Verses
1 and 2 of Revelation 11 contain the relevant temporal indicators:

And there was given me a measuring rod like a staff: and someone
said, “Rise and measure the temple of God, and the altar, and those
who worship in it. And leave out the court which is outside the
temple, and do not measure it, for it has been given to the nations;
and they will tread under foot the holy city for forty-two months.”

The Significance of Revelation 11

A good number of competent scholars have long recognized the
significance of this passage for the interpretation and the dating of
the book. Bleek notes the existence of the Temple as a significant
indicator “with tolerable clearness” of Revelation’s historical era: “As
to the time of writing, there are several statements which indicate this
with tolerable clearness, and to which we have already referred. In
the first division (ch. xi. 1-14) . . . Jerusalem and the temple are
spoken of as still standing.” ' Disterdieck writes with deep conviction
regarding Revelation 11:1 ff: “It is sufficient for chronological inter-
est, that prophecy depends upon the presupposition that the destruction
of the Holy City had not yet occurred. This is derived with the greatest
evidence from the text, since it is said, ver. 2, that the Holy City, ie.,
Jerusalem, is to be trodden down by the Gentiles. . . . This testi-
mony of the Apoc., which is completely indisputable to an unpreju-

1, Johannes Friedrich Bleek, An Introduction to the New Testament, 2 vols., 2nd ed.,
trans, William Urwick (Edinburgh: T. & T. Clark, 1870) 2:226.

165
