94 BEFORE JERUSALEM FELL

was written nearer A.D. 180.” Others believe Caius, Presbyter of
Rome, wrote it about the year A.D. 200."If written by Caius, it
should be noted that he may well have been a student of Irenaeus.”
But even if Caius did not compose it, it most certainly was drawn
up by a writer from the latter half of the second century, the very era
of Irenaeus.” As Schaff observes, it is “the oldest Latin church
document of Rome, and of very great importance for the history of
the canon, “4*

The witness of this manuscript virtually demands the early date
for Revelation. The relevant portion of the document states that “the
blessed Apostle Paul, following the rule of his predecessor John,
writes to no more than seven churches by name. ” Later we read:
“John too, indeed, in the Apocalypse, although he writes to only
seven churches, yet addresses all, “4* This ancient writer clearly teaches
that John preceded Paul in writing letters to seven churches. And it is
universally agreed among historians and theologians that Paul died.
before A.D. 70, either in A.D. 67 or 68.“This is a most important
piece of early evidence with which to reckon.

If the common late date interpretation of Irenaeus is accepted,
the Muratorian Canon records a contemporary tradition contrary to
and despite Irenaeus. If we adopt the most reasonable reconstruction
of Irenaeus and accept the clarifying of the ambiguity in Clement,
as presented heretofore, then we have a trio of harmonious evidences,
all from the same era.

Tertullian.

Quintus Septimius Florens Tertullian (c, A.D. 160-220), the first
major theologian to write in Latin, lived in Carthage and began
writing around A.D. 196. He is most famous for his Apology, but is

87, Lightfoot and Harmer, Apostolic Fathers, p, 298.

38, Tim Dowley, Eerdmans Handbook to ihe History of Christianity (Herts, England: Lion,
1977), p. 105, See also next note,

99, ANF 5:599,603.

40, FF, Bruce, Nee Testament History (Garden City, NY Doubleday, 1969), p. 366.

41, Schaff, History 1:76.

42, ANF 3:603. The seven churches addressed by Paul would be Rome, Corinth,
Galatia, Ephesus, Philippi, Colossae, and Thessalonica.

43, A.'T. Robertson, “Paul in International Standard Bible Encyclopedia, Ist od. (Grand
Rapids: Eerdmans, 1915) 3:2287; Richard Longenecker, The Mfmsiry and Message of Paul
(Grand Rapids: Zondervan, 1971), p. 86.
