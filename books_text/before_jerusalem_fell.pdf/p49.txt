32 BEFORE JERUSALEM FELL

Heinrich Béhmer, Die Offenbarung Johannis (Breslau: 1866). [1]°

Wilhelm Bousset, Revelation of John (Gottingen: Vandenhoeck, 1896).

Brown, Ordo Saeclorum, p. 679."

Frederick F. Bruce, New Testament History (Garden City, NY:
Doubleday, 1969), p.411. [5]

Rudolf Bultmann (1976). [5]>!

Christian Karl Josias Bunsen. [3]

Cambridge Concise Bible Dictionary, editor, The Holy Bible (Cambridge:
University Press, n.d.), p. 127.

W. Boyd Carpenter, The Revelation of St. John, in vol. 8 of Charles

Ellicott, ed., Ellicott’s Commentary on the Whole Bible (Grand Rapids:
Zondervan, rep. n.d.).

S. Cheetham, A History of the Christian Church (London: Macmillan,
1894) , pp. 244.

David Chilton, Paradise Restored (Tyler, TX: Reconstruction Press,
1985); and The Days of Vengeance (Ft. Worth, TX: Dominion Press,
1987).

Adam Clarke, Clarke’s Commentary on the Whole Bible, vol. 6 (Nashville:
Abingdon, rep. n.d.).

William Newton Clarke, An Outline of Christian Theology (New York:
Scribners, 1903).

Henry Cowles, The Revelation of St. John (New York: Appleton, 1871).

W. Gary Crampton, Biblical Hermeneutics (n. p.: by the author, 1986),
p. 42,

Berry Stewart Crebs, The Seventh Angel (Grand Rapids: Eerdmans,
1938),

Karl August Credner, Finleitung in des Neuen Testaments ( 1836). [1]

Samuel Davidson, Tze Doctrine of the Lust Things (1882); “The Book
of Revelation” in John Kitto, Cyclopedia of Biblical Literature (New
York: Ivison & Phinney, 1855); An Introduction to the Study of t/w New

“49, See also Bornhard WeissA Manual of Introduction to the New Tisiomet, trams, A. J.
K. Davidson, vol. 2 (New York: Funk and Wagnalls, 1889),p. 81 n,

50, Cited in 8, Cheetham, A History of the Christian Church (London: Macmillan, 1894),
p24,

51, See statement by C. H. Dedd in Robinson (5), p. 359.
