SELECT BIBLIOGRAPHY II; ANCIENT WRITINGS

MAJOR EDITIONS CONSULTED

Charlesworth, James H., ed. The Old Testament Pseudepigrapha. 2 vols.
Garden City: Doubleday, 1983.

Josephus, Flavius. The Complete Works of Flavius Josephus. Translated
by William Whiston. Grand Rapids: Kregel, 1977.

Roberts, Alexander, and James Donaldson, eds. The Ante-Nicene Fa-
thers. 10 vols. Grand Rapids: Eerdmans, (1885) 1975.

Schaff, Philip, and Henry Wace, eds. A Select Library of Nicene and
Post-Nicene Fat /w-s of the Christian Church (Second Series). 14 vols.
Grand Rapids: Eerdmans, (1890) 1986.

Smallwood, Mary E. Documents Mlustrating the Principates of Gaius Claudius
and Nero, Cambridge: University Press, 1967.

Warmington, E. H., ed. The Loeb Classical Library. Cambridge: Har-
vard University Press, various dates.

Wright, William. Apocryphal Acts of the Apostles. Amsterdam: Philo,
(1871) 1968,

374
