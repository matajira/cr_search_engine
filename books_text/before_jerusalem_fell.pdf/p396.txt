MODERN AUTHORS INDEX

Abauzit, Firmin, 81, 241n.

Abbe, 200.

Abbott, E, A., 168,

Abbott-Smith, G., 138, 142, 194n.

Adams, Jay E., 81, 168.

Adeney, Walter F., 4n, 13.

Aland, Kurt, 107n, 188, 139n, 190n, 196n,
201 n, 227n, 264n, 269n,

Albertz, 325n,

Alcasar, Luis de, 81.

Alexander, J. A., 8.

Alford, Henry, 44,46, 153.

Allen, Willoughby C.,14, 20.

Allis, Oswald T., 18.

Angus, 8., 227n, 296n, 297n,

Arndt, W. F.,127n, 188, 140, 148, 212n.

Aube, B., 81, 296n,

‘Auberlen, Karl August, 31,

Backhouse, Edward, 256n.

Bahnsen, Greg L., 80, 81.

Baljon, 168.

Barabas. Steven, 288n.

Barclay, William, 18, 260n,

Barker, Glenn W., 81 n, 288n,

Barnes, Albert, 43, 141,285.

Barnes, Arthur Stapylton, 31,4887,
100n, 177, 179n.

Barth, F., 168,

Bartlet, James Vernon,31, 188,

Batiffol, Pierre, 296n.

Baur, Ferdinand Christian, 27,81,200,
804n,

Beasley-Murray, G. R, 18, 14n, 18n, 150,
285,

Beck, William F., 138,

 

Beckwith, Isbon T., 12, 16, 158, 162n,
168, 210n, 261n, 265,266, 267n, 301n,
802n, 828n,

Beeson, Ulich R,, 30m,

Behm, J., 828, 325n.

Bell, Albert A., Jr., 81, 157n, 289,

Benary, 199, 200.

Ben-Gurion, David, 17in.

Benoit, P., 199n, 325n,

Ben-Sasson, H, HL, 21in,

Berkhof, Louis, 15, 19, 20.

Bernhard, Thomas Dehancy, 122, 180,
131n.

Berry, George Ricker, 141.

Bertholdt, Leonard, 81.

Beule, 214,

Beyshlag, Willibald, 81,200.

Bigg, Charles, 31.

Black, Matthew, 201 n.

Blass, Friedrich, 142n.

Bleek,Friedrich 31, 42, 152, 165,200,
304n.

Board, Stephen, 6n.

Boer, Harry E., 18n,

Béhmer, Heinrich, 92,48,

Boiasier, Gaston, 296n,

Bolton, 209,

Bonnard, P,, 325n.

Bousset, Wilhelm, 82,42, 168, 880n.

Bovan, M. J., 48,49,

Box, G. H., 156, 156n, 187, 189.

Brandon, 8, G. F., 210n, 226n, 227,
280, 244n,

Bright, John, 200.

Briggs, C. A, 168,

Bromiley, Geoffrey W,, 194n.

387
