294 BEFORE JERUSALEM FELL

had thus tolerated it as a religio licita under the umbrella of Judaism,
such was no longer to be the case. Many scholars note that Christian-
ity was first recognized as a separate religion and was increasingly
regarded as a religioillicita in the period beginning with Nero’s
opening persecution and ending in the destruction of the temple in
Jerusalem. ** Workman confidently asserts that

we can date with some certainty this distinction in the official mind
between Jew and Christian as first becoming clear in the summer of
64. The acquittal of St. Paul in 61 or 62 — an event we may fairly
assume as probable — is proof that in that year Christianity, a distinct
name for which was only slowly coming into use, could still claim
that it was a religio licita... still recognized as a branch of Juda-
ism... . At any rate, both Nero and Rome now clearly distinguished
between the religio lictia of Judaism and the new sect... . The
destruction of Jerusalem would remove the last elements of confu-
sion.”
The distinction having become evident, the situation which arose
was that “once Christianity presented itself in the eyes of the law and
the authorities as a religion distinct from that ofJudaism, its charac-
ter as a religioillicita was assured. No express decree was needed to
make this plain. In fact, the ‘non /icet’ was rather the presupposition
underlying all the imperial rescripts against Christianity. “3°

It is indisputably the case that Christianity was persecuted by
Nero Caesar. The evidence for a Domitianic persecution is immea-
surably weaker, and thus the argument for a Domitianic setting for
Revelation is also weaker.

Second, we learn from both pagan and Christian sources that
not only were Christians punished, but they were punished in huge
numbers. Not only so, but the Neronic persecution was more grue-
some and longer lasting in comparison to the alleged Domitianic
persecution. Tacitus speaks of an “immense number” (multitudo in-

36, Ramsay, Church in Roman Empire, pp. 251; Philip Schaff, History of the Christian
Church, 8 vols. (Grand Rapids: Eerdmans, [1910] 1950) 1:877-881; Herbert B. Work-
man, Persecution in the Early Church (Oxford: Oxford University Press, [1906] 1980), p. 22;
Sweet, Revelation, Dp. 28; Peake, Revelation, p, 94,

87. Workman, Persecution, D. 22,

88, Adolf Harnack,The Mission and Expansion of Christianity im the First Three Centuries,
2 vols, (New York: G, P. Putnam’s, 1908) 2:116,
