16

INTRODUCTION TO
DOMITIANIC EVIDENCES

Despite the wealth of compelling arguments for an early date
cited heretofore, late date advocacy persists among the majority of
scholars, even to the point of dominance in academic circles. Al-
though in the nineteenth century the evidence cited in defense of a
late date for Revelation derived almost solely from external consid-
erations, such is certainly not the situation in the current debate since
the early 1900s. ’ Current late date literature vigorously argues from
the internal evidence. In order the better to secure the early date
argument in terms of the internal evidence, we must address those
contrary arguments put forward by late date advocates.

Though there are a variety of approaches to the evidence arrayed
by late date advocates, it would appear that the modern case concen-
trates its focus upon four basic arguments. These arguments are
capably summarized by evangelical scholar and late date advocate
Leon Morris in his commentary on Revelation. The order of his
listing will be followed.

First, what Morris calls “the principal reason for dating the book
during” Domitian’s reign is that Revelation “contains a number of
indications that emperor-worship was practised, and this is thought
to have become widespread in Domitian’s day.”2 Second, “indica-

1, Terry could write in the late 1800s that no “critic of any note has ever claimed
that the later date is required by any internal evidence” (Milton 8, Terry, Biblical
Homencutics (Grand Rapids: Zondervan, rep. 1974], p. 240), Interestingly, Morris's
more recent late date commentary hardly even considers the external evidence at all!
(Leon Morris, The Revelation of St. John [Grand Rapids: Eerdmans, 1969], pp. 344).
Guthrie places it as his last argument (Donald Guthrie, New Testament Introduction, 8rd.
ed. [Downers Grove, IL: Inter-Varsity Press, 19701, p. 956).

2, Morris, Revelation, p, 36, See also Robert H, Mounce, The Book of Revelation. New
International Commentary on the New Testament (Grand Rapids: Eerdmans, 1977),

259
