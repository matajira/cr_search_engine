A Response to House and Ice 349

writings reflect Chilton’s views in their interpretation of Revelation?
If the A.D. 70 destruction of Jerusalem fulfilled so much of biblical
prophecy, then why is this not reflected in the views of the early
church? Why is it that adi of the early fathers, when referring to
Revelation and Matthew 24, see these as future events?”

And since they spend a good deal of space on the influence of Daniel
9:25ff on Matthew 24:15, surely they would include the handling of
Daniel 9 in this statement.“ After all, they attempt to distinguish
Luke 21:20-24 from Matthew 24:15 on this very basis: “In contrast,
the Matthew 24:15 passage has a context of its own which differs
from the Luke account. Matthew says, ‘when you see the abomina-
tion of desolation which was spoken of through Daniel the prophet
(not Luke), standing in the holy place . . .’ Comparison of the
description in Matthew and Daniel with the passage in Luke yields
differences, which prove that they are two separate events.” They even.
state: “One major reason Matthew 24 could not have been fulfilled
in A.D. 70 is that ‘the abomination of desolation’ (24:15) was not
accomplished in the destruction of Jerusalem.”5"Thus, on their own
analysis Daniel 9 should be no more preteristic than Matthew 24 and
should be no more heard of being interpreted preteristically in early
Christianity than it is.

It is here I begin to suspect that they have done ve7y little reading
in patristics, though they write with confidence as if they had. This
is a part of the problem that frustrates the early date advocate: among
popular writers urging the late date, there is frequent bold assertion
without adequate knowledge. However, let us note a few samples
that falsify such a claim.

As I have noted, there are references to the destruction of Jerusa-
lem in the context of Revelation studies in the ancient Church. I
pointed out that in his day, Andreas of Cappadocia had to respond
to comments made earlier by several Christian writers who applied
various of the prophecies of Revelation to the destruction of Jerusa-
lem.*® Also Arethas specifically interprets various passages in Revela-
tion in terms of the destruction of Jerusalem.*”

52, House and Ice, Dominion Theology, p, 258 (emphasis mine),
58, Ibid,, pp. 259, 287-290.

54, Ibid., p. 290 (emphasis mine).

55, Ibid., p.287.

56, See above, pp. 106-107.

57. See above, pp. 107-108.
