204 BEFORE JERUSALEM FELL

ure .. . this solution was apparently never thought of.”°

(2) The designation of 666 as a particular, historical individual
misses John’s point, according to some. “Merely to count up the
numerical value of the figures obtained from Nero Caesar would not
have answered the Apostle’s purpose, and could never have filled his
mind with the awe that is upon him in this verse.”*° Morris concurs
with his generic, rather than specific, designate. He writes, “It is
possible that such solutions are on the wrong lines and that we should
understand the expression purely in terms of the symbolism of num-
hers.”5° He sees the number 666 as falling short of the number of
Jesus’ name (which carries the value of 888) and of the number of
perfection (777). Thus, the number represents that “civilization with-
out Christ is necessarily under the dominion of the evil one. ”*!
Hendriksen and Torrance agree with Morris’s main point.”In es-
sence, these scholars view the number as more symbolic that crypto-
grammic,

(3) In that John writes to a Gentile church using the Greek
language, we should not expect that a Hebrew form of the name was
intended. According to Ladd: “No one has explained why John,
writing to a Greek-reading public, would have used the elaborate
symbolism of gematria with a Hebrew instead of a Greek form of the
name. “5*Richardson, Morris, Guthrie, Mounce and others concur
with Ladd.“

These, then, are the leading objections to the Nero theory regard-
ing the meaning of 666. Nevertheless, despite their being advanced.
by numerous fine scholars, these difficulties are not insuperable. A
brief rebuttal to them will suffice to enhance the positive evidence in
the theory's favor outlined above.

48, Tbid., p, 174, Cp. Mounce, Revelation, p. 265; Guthrie, Introduction, p.959,

49, Milligan, Discussions, p. 120.

50, Morris, Rewlation, p. 174.

51, tid,

52, William Hendriksen,More Than Conquerors (Grand Rapids: Baker, 1967), p. 182.
‘Thomas F. Torrance, The Apocalypse Teday (Grand Rapids: Herdmans, 1959), p. 86.

58. George Eldon Ladd, A Commentary on the Rewlation off Jokn (Grand Rapids; Eerd-
mans, 1972), p. 186,

54, Donald W, Richardson, The Revelation of Jesus Christ (Richmond: John Knox,
[1988] 1964), pp. 84-86; Morris, Revelation, p. 174; Guthtie, Introduction, p, 959; Mounce,
Revelation, p. 265.
