Irenaeus, Bishop of Lyons 63

accept necessarily Irenaeus’s authority as conclusive.

Irenaeus states matter-of-factly that Papias was “the hearer of
John.’™ Eusebius, however, provides information that contradicts
Irenaeus’s claim that Papias had heard John. Eusebius records a
statement by Papias that said, “If therefore anyone came who had.
been a follower of the Presbyters, I would ask him about the words
of the Presbyters.”©° According to Eusebius, and contrary to Ire-
naeus, Papias sought for those who had seen any of the “presbyters”
or apostles. Obviously, then, he was not one of them himself.

In other places Eusebius disputes the opinion that Revelation
was written by the Apostle John. 70 And this despite the fact Irenaeus
(who claims to have known Polycarp, who knew John) was certain
that the Apostle wrote it’1 For some reason, obviously compelling
to Eusebius, he felt justified in contradicting Irenaeus’s emphatic
statements regarding the Johannine authorship of Revelation.
Eusebius’s countering of Irenaecus’s witness in this area surely indi-
cates that this great chronicler of the Church did not conceive of
Trenaeus as above reproach on historical matters.

ITrenaeus’s Historical Errors
In Against Heresies we read a very unusual historical statement:

For how had He disciples, if He did not teach? And how did He teach,
if He had not a Master’s age? For He came to Baptism as one Who
had not yet fulfilled thirty years, but was beginning to be about thirty
years old; (for so Luke, who bath signified His years, bath set it down;
Now Jesus, when He came to Baptism, began to be about thirty years
old:) and. He preached for one year only after His Baptism: complet-
ing His thirtieth year He suffered, while He was still young, and not
yet come to riper age. But the age of 30 years is the first of a young
man’s mind, and that it reaches even to the fortieth year, everyone
will allow: but after the fortieth and fiftieth year, it begins to verge
towards elder age: which our Lord was of when He taught, as the
Gospel and all the Elders witness, who in Asia conferred with John
the Lord’s disciple, to the effect that John had delivered these things

68. Irenaeus, Against Heresies 5:33:4.

69, Eusebius, Ecclesiastical History 3:39.

70, Ibid., 3:24:17-18; 5:8:5-7; 7:25:78, 14,

71, See Irenaeus, Against Heresies 4142; 4:16:6; 421 :3; 4:28:2; 5:34:2, compare with
4:20:11; 5:26:1,
