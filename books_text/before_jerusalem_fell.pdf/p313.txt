18
THE NERO REDIVIVUS MYTH

Morris's third evidence for a Domitianic date for Revelation is
the ancient Nero Redivivus myth, which he briefly explains and confi-
dently employs: “Again, it is urged that the book shows evidence of
knowledge of the Nero redivious myth (e.g. xvii. 8, 11). After Nero’s
death it was thought in some circles that he would return. At first
this appears to have been a refusal to believe that he was actually
dead. Later it took the form of a belief that he would come to life
again. This took time to develop and Domitian’s reign is about as
early as we can expect it.”! Swete lists the myth as the first of the
“more definite” evidences for a late date: “There are other indications
of date which are more definite, and point in the same direction. (a)
It is impossible to doubt that the legend of Nero Redivious ig in full view
of the Apocalyptist in more than one passage (xiii. 3, 12, 14, xvii.
8)”

Form critic Moffatt boldly asserts that “the phase of the Nero-
redivivus myth which is represented in the Apocalypse cannot be
earlier than at least the latter part of Vespasian’s reign.”? In his
commentary on Revelation 17 he speaks strongly of the role of the
myth in interpreting the passage, when he notes that “the latter trait
is unmistakably due to the legend of Nero redivivus, apart from
which the oracle is unintelligible.”4 Charles, a fellow form critic, is
equally confident of the utility of the Nero Redivious myth in establish-
ing Revelation’s date: “The Nero-redivivus myth appears implicitly and

1. Leon Morris, The Revelation of St. John (Grand Rapids: Eerdmans, 1969), p. 87.

2, Henry Barclay Swete, Commentary on Rewlation (Grand Rapids: Krege!, [1906]
1977), pp. ci-cit.

8, James Moffatt, The Revelation of St, John, the Divine, in W. R, Nicoll, od., Englishman’s
Greek Testament, vol. 5 (Grand Rapids: Eerdmans, rep. 1980), p. 317.

4, Ibid., p. 450.

300
