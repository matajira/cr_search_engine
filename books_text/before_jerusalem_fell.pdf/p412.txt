Subject Index

John the Baptist, 234.

Jordan River, 245,

Judaism (Seeaise Christianity Jewish
background; Jews), 144, 167, 178, 181,
184, 189, 221,228,225>227, 229,235,
289,298,294, 329n.

Judaizers, 115,

Judas, 81,

Judea (See aim: Palestine; Land, the), 119,
124, 128, 148, 176, 184, 227, 288,240,
244,251,814,851.

judgment(s), 128, 130, 147, 174, 186, 188,
191, 238, 240,244, 251,336.

Julio-Claudian line of emperors, 73,99,
185-157,287.
cessation of, 311, 314-315.

Julius Caesar (Se: Caesar, Julius).

Jupiter, 145,265,267,269.

fig(s), 76,80,97,98, 106, 156, 157,
189, 160, 162, 170, 172, 278, 851.
eighth king, 302, 808, 310-816,
Julian emperors as, 71,72,99, 124,
184, 159-160,276.

seven kings, 146, 149, 151-159, 161,
164, 198,264, 310-816,385,

sixth king, 118, 146-164, 198, 908, 310-
816,852,

Kingdom, 158, 156, 168-164, 186, 206,
310,814,
of Christ/God, 127, 174, 207, 248n,
of Satan, 248n,

Jamb, 148, 254, 276.

Land, the (See also: Israel; Judea; Pales-
tine), 115, 128-181, 282-288, 240, 282,

Laodicea, 819-822, 326-829, 348,

Last daya (See: Latter Days).

Lateinos, 206,

Late-date (Ste: Revelation, date of).

Latter days (Last days), 185, 184, 207.

Latin, 11,46,49,54,94, 105, 195, 197n,
208.

Law(@), 6n, 184,

Legend (See: Nero - Legend).

Legion(s) (See: Rome — legions).

403

Liberal, 7, 17, 18, 24-28, 145, 151,166n,
167, 182, 210, 315,820.

Literal(ism) (See also: Hermeneutics;
Revelation — interpretation), 10-11,
15, 164n, 174, 175,

Locust(s), 10,91,247.

LXX (See: Septuagint).

Maccabee(s) (can), 17, 222n,

Magic, 18, 216.

Man/mankind (humanity), 5n, 70,81,
148, 172, 185,237,268,280,281, 298n,
$12,

Man of gin, 80.

Manuseript(s) (See aise: Texts), ix, 44,
51,58-59,98-94, 196-197, 198,202,
Marcus Aurelius (emperor), 78, 77, 160,

288n,

Mark, 22, 210.

Marquis de Sade, 78.

Mars, 272,

Martyrdom) (See ao specific names of
martyrs, 6.g., James, Paul), 42, 62, 80,
95,96,98, 105, 190,218,

Masada, 282,

Megiddo, 130.

Millennial views:
amillennial, 136.
dispensational, 5, 6n, 7, 10, 185, 147,

164n, 178, 174, 201n, 316n, 3394,
352n,
postmillennial, 196, 339-342.
premillennial (See also: Irenaeus —
premillennialism of), 60, 135, 136,
175, 205n, $41, 852n,

Millennium (See Thousand; Millennial
views),

Mishna, 178.

Mithras, 273,

Moon, 74, 76.

Moses, 180,

Mourn (See also: Weep), 121, 128, 180,

Mountain Gee a/so: Seven — mountains),
74,170.

Muratorian Canon, 60,87,88,98-94,
109, 335, $44,
