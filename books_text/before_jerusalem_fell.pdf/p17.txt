xviii BEFORE JERUSALEM FELL

B. Jordan, long-time friend, is also to be thanked for his careful
editing of the final manuscript for publication.

Each of these is to be thanked with deep appreciation for the
sharing of their valuable time and for encouraging me in this project.
Without their encouragement the undertaking would have been im-
mensely more difficult and the potential value of my labor much
diminished. Of course, the end product is the present writer's — he
alone is responsible for any deficiencies and inadequacies that may
be discovered within.

Although the goal of the dissertational inquiry was quite nar-
row — to ascertain the general time-frame of the composition of one
book in the New Testament — the scope of the research demanded
for a careful demonstration of the goal proved to be quite broad.
This was so for two reasons.

In the first place, the majority of current scholarship accepts a
late date for Revelation — a date of around A.D. 95 - which this
work seeks to refute. Consequently, there was a vast array of more
readily available scholarly material for the opposite position. Thus,
the establishment of our case was confronted with a sizeable range
of material for the contrary conclusion, which demanded sorting and
scrutinizing.

In addition, by the very nature of the case the determination of
the date of Revelation’s composition is quite a complex affair. It
requires engaging in an exegesis of critical passages, a diligent survey
of the voluminous scholarly literature on Revelation, an inquiry into
the apocalyptic literature of the era, and a laborious search through
the writings of both the early church fathers and the pagan Roman
historians. It is hoped that the profusion of research contained within
will not be without beneficial effect.

Nevertheless, despite the extensive and involved nature of the
research presentation, it is the conviction of the present writer that
the case for Revelation’s early dating is clear and compelling. The
extensive research gathered in the establishment of this date was not
sought for in a strained effort to create a case where there was none.
On the contrary, much of the material was employed with the
intention of demonstrating the precariousness of the contrary opin-
ion. Of course, whether or not the rebuttal to the majority opinion
and the positive establishment of the minority position are adequate
to the task is now left .to biblical scholarship to assess.
