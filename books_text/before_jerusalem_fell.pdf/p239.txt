The Role of jewish Christianity 225

with a crown of twelve stars on her head (Rev. 12:1 ff. ). Christians
are represented as worshiping in the Temple and ministering in
Jerusalem (Rev. 11:1-8).

The Interpretation of the Evidence

In light of such evidence, we can safely observe that “the Apoca-
lypse of John plainly belongs to the period in which Jews and
Christians still lived together.”!6 Robinson poses a question and
suggests a conclusion along these lines:

For is it credible that the references in Rev. 2:9 and 3:9 to those who
“claim to be Jews but are not” could have been made in that form
after 70? For the implication is that Christians are the real Jews, the
fullness of the twelve tribes (7:4-8; 21: 12), and that if these Jews were
genuinely the synagogue of Yahweh (as they claim) and not of Satan
they would not be slandering “my beloved people.” Even by the time
of the Epistle of Barnabas, which, unlike the book of Revelation,
clearly presupposes the destruction of the temple (16:1-4) and the
irrevocable divide between “them” and “us” (cf. 13:1, 1) taOrjxn etc
Hpac fj cig éxeivouc), such language is no longer possible. 17

As noted in Robinson’s quote, Barnabas, soon after the fall of Jerusa-
Jem (c. 100), posited a radical “us/them” distinction between Chris-
tians and Jews. This is in keeping with later, post-Temple Christian
practice. Ignatius (c. 107) writes: “It is absurd to speak of Jesus
Christ with the tongue, and to cherish in the mind a Judaism which
has now come to an end. For where there is Christianity there cannot
be Judaism.”!® Justin Martyr (c. 160) does the same: “For the
circumcision according to the flesh, which is from Abraham, was
given for a sign; that you may be separated from other nations, and
from us; and that you alone may suffer that which you now justly
suffer; . . . For you are not recognized among the rest of men by any
other mark than your fleshly circumcision. . . . For you have not the
power to lay hands upon 4s, on account of those who now have the
mastery. But as often as you could you did so. ” 9

16, Torrey, Apocalypse, D. 80.

17. John A.T, Robinson, Redating the New Testament (Philadelphia: Westminster,
1976), pp. 227-228, He notes that Hort in his commentary on Revelation 2:9 made this
same point (F. J. A. Hort, The Apocalypse of St. John: F-H! (London: Macmillan, 1908D.

18. Episite to the Magnesions 10,

19, Dialogue with Trypho the Jew 16, Emphasis mine,
