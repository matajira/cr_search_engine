316 BEFORE JERUSALEM FELL

head/king in Revelation 17:11: “And the beast which was and is not,
even he is an eighth.” Of course, there is no indefinite article in Greek,
but the omission of the definite article that clearly and repetitively
defined the chronological series of head/kings (“the five,” “the one,”
“the one to come”) vanishes before the eighth is mentioned. Thus, the
eighth is “an eighth, “ie., it refers not to any one particular individ-
ual, but to the revival of the Empire itself as the heads are beginning
to be replaced. The Roman Empire which will later revive its perse-
cuting relationship to Christianity in its revived form is arising from
ruin.

There is a very important sense in which the revival of the
Empire under Vespasian, was a revival under “an eighth,” who is
“of the seven.” It is the same Roman Empire that is brought to life
from the death of Civil War. John’s concern is particularly with the
contemporaneous events, ie. here the Roman Civil War that oc-
curred within the compass of the reign of the seven kings. The eighth
is beyond his most pressing and immediate concern (although it is
not unimportant), and thus is not specified and detailed. The fact
that this revival is of an eighth head, however, indicates the rapid
recovery of the Beast."That recovery will come shortly after the
demise of the original seven.

Conclusion

The revival of the Beast is a remarkable and significant aspect
of Revelation’s message. Although late date advocacy presents an
intriguing argument based on this phenomenon, in the final analysis
it fails of its purpose. Even if the Nero Legend were in John’s mind,
its seeds were planted early in Nero’s reign and its first appearance
as a powerful influence in civil affairs occurred in A.D. 69.

More importantly, a reasonable and persuasive case can be

63, Chilton has perceptively noted that the number eight is that of resurrection, for
Jesus was resurrected on the eighth day, ie,, Sunday. He alludes to its significance here
in showing the revival of Roman tyranny which is to come, See Chilton, Days of Vengeance,
P- 436, See also E, W, Bollinger, Tie Companion Bible (London: Samuel Baxter and Sons,
rep. 1970), appendix 10,

64, The dispensationalist recognizes the importance of the fall of Rome in Revelation,
But rather than seeing it as contemporaneus with the life of John and the original
recipients of his book, he sees it as the full of Rome a few hundred years later and followed
millennia later by a “revived Roman Empire.” See for instance John F, Walvoord, The
Revelation of jesus Christ (Chicago: Moody, 1966), p. 9.
