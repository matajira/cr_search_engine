The Role of Nero Caesar 205

The Early Fathers

The first objection proffered above is one of the two strongest
(the third being the other weighty one). It would seem most reason-
able to expect that since Irenaeus wrote within about one hundred
years of Revelation, he likely would have heard of the proper view.
At the very least, we would think, Irenaeus would recognize the true
view, though growing indistinct, as a theory to be given equal footing
with the solutions he does proffer. But, as a matter of fact, in his
lengthy treatment of the gematria in Against Heresies 5:28-30 (espe-
cially chapter 30), he provides at least three possible interpreta-
tions — and Nero’s name is conspicuously absent. Furthermore, no
early Church father suggests Nero’s name as the proper designation
of 666, even though various suggestions were given by such men as
Irenaeus, Andreas of Caesarea, Victorious, Hippolytus, Clement of
Alexandria, and others. Surely this is a potent objection for the
twentieth century interpreter.55 Even this objection, however, strong
as it is, is not fatal to the theory, and that on the following grounds:

First, the very fact that Irenaeus, writing just one hundred years
after Revelation, cannot be sure of the proper designation demon-
strates that the true interpretation, whatever it was, very quickly had
been lost. If this is true of Irenaeus in A.D. 180, it is certainly true
of the later fathers. Mounce suggests that “John intended only his
intimate associates to be able to decipher the number. So successful
were his precautions that even Irenacus some one hundred years later
was unable to identify the person intended. ”° Had Irenaeus offered
with conviction and assurance a specific alternative, the case against
the Nero theory would have been more seriously challenged. Interest-
ingly, Irenaeus suggests the hopelessness of determining the proper
understanding: “It is therefore more certain, and less hazardous, to
await the fulfillment of the prophecy, than to be making surmises,
and casting about for any names that may present themselves, inas-
much as many names can be found possessing the number men-
tioned; and the same questions will, after all, remain unsolved. “5”

55, Although it should not go unnoticed that the views of Irenaeus and others are not
adopted by medern commentators anyway.

56, Mounce, Revelation, p. 265. Interestingly, this is somewhat inimical to Mounce’s
premillennialism, Are we to believe that John told the first century church the name of
a twentieth or twenty-first century man?

57, Against Heresies 5:30:3
