Concluding Remarks 337

men” to Christ (John 12:31), then the Church can confidently seek
to bring “every thought captive to the obedience of Christ” (2 Cor.
10:5).

We also noted in the beginning of our inquiry that a serious
confusion as to the naturc and message of Revelation is partly respon-
sible for the cultural defeatism and retreatist pietism so influential in
twentieth century Christianity. There we observed that one reason
for confusion as to the Church’s future is due to a radical misunder-
standing of the date of the writing of Revelation. If Revelation is
inadvertently dated after the events it prophesies as future, the way
is opened to a radical misconstruing of its message. Indeed, not only
has the message been misread in such circumstances, but it has been
wholly inverted, placing in our future what really lies in our past.
Hence, the significance of the date of Revelation.
