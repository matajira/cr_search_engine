ANCIENT AUTHORS INDEX

Africanus, Julius, 352n,

Andreas of Cappadocia, 106-107, 109,
205,349,

Apollinius of Tyana, 70, 214, 267, 268.

Appian, 171, 213n.

Arethas, 54, 107-108, 108, 147, 885, 844,
349,

Aristophanes, 218n,
Augustine, 74,81, 100,808,
Aurelius, Marcus, 73, 7.

Barnabas, 157, 188, 184, 195, 225, 352n,

Caius, 60,94,

Cicero, Marcus Tullius, 149, 207, 265n.

Claudian, 149,

Clement of Alexandria, 42n, 48,44,68-
85,88,94,97-100, 108, 108-109, 190,
205,884-895,844,845, 851.

Clement of Rome, 78, 86,91, 102, 176-
181, 290,295,

Cyprian, 126,352,

Dio Cassius, 71, 74, 75n, 155, 265n,
278,275, 289, 208,809, 32in.

Dio Chrysostom, 74,80,215, 265n, 808,

Dorotheus, 44, 108n,

Epictetus, 71, 73,

Epiphanies, 42n, 44, 54, 100, 104-105,
108, 244, 885,reth344.

Eusebius, 42n, 48,44,46, 51n, 52n, 58,
58n, 61-68,65,66, 78, 79n, 82,88,
92,95, 100, 101-104, 108, 108,197n.

Flaceus, Aulus Persius, 71,

Harmatolus, Georgius, 92,

Hegissiupus, 190.

Hermas, 61,86-92, 109, 178, 212n, 885,
844,

Herodian, 71.

Hippolytus, 60, 104, 126, 205, 206n.

Horace, 149,

Ignatius, 102, 125, 184,212n, 228n, 225,
Irenaeus (See: Subject Index),

Jerome, 11,4844, 46n, 54,65,66,80,
81,88,95-97, 108-105, 109, 150,808.

John the Apostle (See: Subject Index).

Josephus, Flavius,130n, 188, 148, 155,
161, 171, 172,211,212n, 218n, 222n,
228n, 285,249-246,248-259, 270,
280n, 281n, 282,298, 312n, 818, 314,

Julianvs, Antonius, 228n,

Juvenal, 70,7182, 2110, 288n,

Lactantins. 79, 80,81, 126, 191, 214,
265n, 288n, 308.

Livy, 280n,

Lucan, Marcus Annaeus, 71,

Martial, Marcus Valerius, 71, 72n, 78,
82, 14, 160, 222n,

Martyr, Justin, 125, 184-185, 222n, 228n,
295.

Melito of Sardis, 190,288.

Origen, 43,58,67,69,48,85,88, 97-99,

108,242,384-385,344, 345,350, 352n,
Orosius, Paulus, 82, 88n, 291, 298, 819m,
Ovid, 149,207,

395
