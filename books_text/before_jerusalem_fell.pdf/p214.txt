200 BEFORE JERUSALEM FELL

Although wide-ranging scholarly consensus is certainly not the
sine qua non of truth, it should be noted that a good number of noted
scholars have accepted this identity as designating Nero. Milligan,
who considered the designation to be “impossible,”2'listed the follow-
ing scholars of his day as holding to the Nero postulate: Fritzsche,
Benary, Hitzig, Reuss, Ewald, Baur, Zeller, Hilgenfeld, Volkmar,
Hausrath, Krenkel, Gebhardt, Renan, Abbe, Reville, Sabatier,
Davidson, Stuart, Bleek, Beyschlag, Farrar, and Cowles. Other
scholars who have affirmed this view include: J. Stuart Russell,
Shirley Jackson Case, George Edmundson, B. W. Henderson, Arthur
S. Peake, Martin Kiddie, Charles C. Torrey, John Bright, Austin
Farrer, G. Driver, D. R. Hillers, Bo Reicke, J. P. M. Sweet, Bruce
M. Metzger, and John A. T. Robinson, to name but a few." Weigall
undoubtedly goes too far when he claims that “scholarship is pretty
well unanimous” on this identification.2* Henderson is a bit more fair
to the opposition when he states that the “number of the Beast’ is
now fairly generally admitted to be 666 because this = Neron kaisar
transliterated into Hebrew. “* In either case, Morris’s statement that
of all the solutions put forward “none has won wide acceptance”3‘
seems quite mistaken. “The most probable view still remains that
most generally accepted, that the writer intended Nero Caesar in
Hebrew letters.”3° Thus, “many are the solutions offered, some of

29, William Milligan, Discussions on the Apocalypse (London: Macmillan, 1893), p. 115.

80. Ibid,, p. 110.

81, Russell, Parousia, p. 557. Shirley Jackson Case, The Revelation of John: A History of
Interpretation (Chicago: University of Chicago, 1919), p. 819. George Edmundson, The
Church in Rome in the First Century (London: Longman’s, Green, 1918), pp. 165-166. B.
W. Henderson, Five Roman Emperors (Cambridge: University Press, 1927), p. 45. Arthur
8. Peake, The Revelation of John (London: Joseph Johnson, 1919), p. 326. Martin Kiddie,
The Revelation of St. John (New York: Harper, 1940), p. 261. Charles C. Torrey, The
Apocalypse of John (New Haven: Yale, 1958), p. 60. John Bright, The Kingdom of God
(Nashville Abingdon, 1963), p. 240. Austin Farrer, ie Revelation of St, John the Divine
(Oxford: Clarendon, 1964), pp. 158i. G, Driver, The Judean Scrolis (Oxford: Blackwell,
1965), p. 874, Hillers, “Revelation 18:18,” p. 65. See J. P. M. Sweet, Ravlation. Westmin-
ster Pelican Commentaries (Philadelphia Westminster, 1979) p. 218, note u, Bruce M.
Metzger, The Text of the New Testament, 2nd ed. (Oxford, 1968), p. 752, John A. T,
Robingon,Redating the Naw Testament (Philadelphia: Westminster, 1976), p. 235.

92, Arthur Weigall, Nero: Enyperor of Rome (Landon: Butterworth, 1988), p. 298.

88, B, W. Henderson, The Life and Principate of the Emperor Nero (Landon: Methuen,
1908), p. 440. Robinson calls it “far the most widely accepted. solution” (Robinson,
Redating, p. 295).

34, Leon Morns, The Revelation of St. John (Grand Rapids: Eerdmans, 1969), p. 174.

85, Peake, Revelation, p. 826, This conclusion was reached after twelve pages of
discussion,
