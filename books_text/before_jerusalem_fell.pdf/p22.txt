Revelation Studies 5

Modern Interest

More directly relevant to the current thesis, however, is the
modem interest in Revelation studies. Interest in Revelation among
Christians is basically of a two-fold nature. On the one hand, it is of
significant spiritual importance to Christians in that it is one book
among the sixty-six that compose the sacred, inspired canon of
Protestant Christianity. As one portion of that inerrant and authori-
tative revelation from God, it demands — equally with the remaining
books — the devout attention of the Christian so that the will of God
might be more perfectly known. Conservative Christendom insists
upon the plenary inspiration of Scripture; a logical (albeit often
overlooked) corollary to plenary inspiration is the “plenary signifi-
cance” of Scripture. That is, since all of the books of Scripture are
inspired of God, all are profitable (2 Tim. 3:16-17).11

On the other hand, it is of significant moral and psychological
importance to Christians in that God has created man to be naturally
inquisitive (Prov. 25:2). And especially is man inquisitive about the
future since, even though he is endowed with an immortal soul, he
is a creature enmeshed in time (Eccl. 3:1-11). Furthermore, the future
is an intrinsically moral concern because expectations regarding the
future impact on the priorities and values one holds in the present!
In that the current popular understanding of Revelation is predomi-
nantly dispensationalist in orientation, Revelation attains a height-
ened significance among Christians in regard to its importance for
eschatological study.

11. In this regard M. R. Newholt in The Bock of Unveiling (London: SPCK, 1952) has
observed: “The Revelation of St. John the Divine is an immensely important part of
Holy Scripture. It lifts our grasp of the Faith on to a plane which no other book can
reach, setting our life against the background of ‘the things that are not seen which are
eternal’, , St. John opens a door into heaven, he algo lifts the cover of ‘the bottomless
pit’; he reveals both celestial splendors and infernal horrors,” From another perspective,
John F, Walvoord, though a dispensationalist, notes the importance of Revelation in his
The Revelation of Jesus Christ (Chicago: Moody, 1966, p. 7): “In some sense, the book is the
conclusion to all previous biblical revelation and logically reflects the interpretation of
the rest of the Bible.”

12, A few samples from the prevailing dispensationalist viewpoint will serve to
illustrate the potential negate impact of this particular eschatology on cultural and social
involvement. Charles C. Ryrie has written: “This world is not going to get any easier to
live in. Almost unbelievably hard times lic ahead. Indeed, Jesus said that these coming
days will be uniquely terrible. Nothing in al! the previous history of the world ean
compare with what lies in store for mankind” (The Living End [Old Tappan, Nu: Revel],
1976], p. 21). If such is the case, why get involved?
