The Role of Nero Caesar 207

‘Titan’ by those who do now possess [the rule] .”°? The Roman
writers Cicero and Ovid have been produced as evidence of the sun’s
being called “Titan”? among the Remans. Remarkably Nero was
widely known to have adopted the attributes of the sun deity as his
own,

Titan was one of the old poetic names of the Sun, and the Sun was
the deity whose attributes Nero most affected, as all the world was
able to judge from seeing his colossus with radiated head, of which
the substructure of the base still remains close by the ruins of the
Colosseum. The mob which greeted him with shouts of ‘Nero-Apollo!’
were well aware that he had a predilection for this title.

Tt seems that Irenaeus at least may have been on the right path.
Third, there is the possibility that Irenaeus did not record the
Nero theory because of his predisposition to a futuristic interpretation
of Revelation generated by his premillennialism. With such a predi-
lection for futurism, he may not have deemed the Nero view worthy
of mentioning. He does seem a little perturbed that some have the
variant number in their texts and use it to offer various suggested.
names: “But as for those who, for the sake of vainglory, lay it down
for certain that names containing the spurious number are to be
accepted, and affirm that this name, hit upon by themselves, is that
of him who is to come; such persons shall not come forth without loss,
because they have led into error both themselves and those who
confided in them. ”© Could he have been just as disturbed by those
who suggested that the correct number indicated a name of the past,
and not of the future? He does give much attention to the future
coming and kingdom of Christ, and makes great use of Revelation
in that discussion.® He insists that “in a still clearer light has John,
in the Apocalypse, indicted to the Lord’s disciples what shall happen
in the last times, ”®’ He says that John only “indicates the number of
the name now, that when this man comes we may avoid him, being

62, Against Heresies 5:30:5. Victorious also records this view, Apocalypse 18,

68, Note by W. H. Rambaut, translator, in ANF 1:659.

64, Farrar, Early Days, p. 470. See also Seneca’s reference to Nero in terms of Apollo
in Ethelbert Stauffer, Christ and the Cassars: Historical Sketches, 8rd od., trans, K. and R.
Gregor Smith (Philadelphia: Westminster, 1955), p. 52.

65. Against Heresies 5:30:1.

66, Ibid, 5:25-35.

67, Ibid, 5:26:1
