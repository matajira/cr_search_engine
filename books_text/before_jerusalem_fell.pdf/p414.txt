Subject Index 405

rabbis regarding, 128-190.

Papias, 60,61, 90-98, 109,835,344,
John and, 63, 92n, 102, 108.

Polycarp and, 68, 102.

Parthia(ns), 75, 180, 807,812,

Passover, 125, 140,

Patmos, 42, 58, 68,69,88,95-96,98-101,
105, 106, 107, 109,218n, 270, 286,
385.

Paul the Apostle, 27,83,84,86,94, 102,
115, 117,144, 178, 179, 227, 229, 291,
805n, 322,826,327-828,
imprisonment, 274, 294, 325-326,
martyrdom, 83, 95-97, 103-104, 179,

218, 255,290,298,822,824,

the aged, 84,

Pas Romana, 241-242, 268,280.

Peace (See: Pax Romana),

Pella, 244,

Pentateuch, 24,

Pergamum, 266, 278.

Persecution of Christianity (See also:
Christiana, crimes of), 15,24-25,28,

79-80,95, 144, 168, 238, 240, 352n,

Domitianic, 8n, 81-88, 100, 101, 145,
180,260,278,280, 287-289,302,
318,347,

Jewish, 92, 115,175, 187,226, 227m,
289, 240n, 285, 296n.

late-date argument and, 260, 285-287,
348,

Neronic, 18, 77-88,95,96, 119, 145,
150, 177, 179-180, 209,218,240,
254,256, 260, 264n, 271,276-284,
285-299, 305n, 318,328, 329, 330,
335,347.

Neronic geographical extent, 78,82,
297-298,

Neronic: legality of, 277-278,281-284,
294-298, 296n.

Neronic: temporal length, 15,80, 144,
254-255,277, 294-295,

Persia, 75.

Pertinax (emperor), 160.

Pessimism (Sev: eschatology —
pessimistic),

Peter the Apostle, 22,88, 84,95-97, 102,
108-104, 144, 179, 191,284,255,290,
291,298,

Pharisees, 190, 226n, 234, 305n.

Philadelphia, 222.

Philosophy (-cr}, 78, nero

Phoenix, 178,

Pierved (See: Jesus Christ — pierced),

Pilate, }24, 159, 160.

Politics, 159, 189,209, 237, 267,272,
278,274,280,282,308, 807.

Polycarp:
age of, 100.

Trenaeus and (See: Irenaeus).

Jobn and, 46,57,62-63, 102,22,
Papias and, 61,92, 102, 108,
Smymaen Church and, 57, 322-826,

Polytheism, 267,

Pompeii, 194.

Pompey, 281.

Pontius Pilate, 281 n.

Pope, 81,87, 105, 178.

Poppasa (wife of Nero), 272,274.

Premillennialist) (See millennial views).

Presbyter(s) (see: Elder).

Presuppositions, 21, 118, 389.

Preterist (school of interpretation), 25,
26,27, 145, 150,289-240,889-848,
348-352,

Priest, 87, 174.

Jewish, 159, 176,217,282.
pagan, 266,267, 268, 269,809,312,

Principate/Princeps, 78, 160,811,

Prophecy(-ties), 6, 12,22,25,47,79,98,
104, 107, 115, 188, 184, 187, 140, 142,
161, 162, 166, 169, 174, 175, 184, 186,
¥91, 288,277, 348,849,350,352,
fulfillment of, 20-21, 184, 151,205,

284,242,244.946,247, 248,249,
250,258,316,839,349, 351,
pagan, 74, 76, 156,812,318,

Prophet(s) (ccc), 22n, 58,88, 105, 124,
180, 134, 182, 183, 289, 306.

Prophetic, 8, 7, 14, 15, 17, 19, 20, 118,
180, 136, 147, 174,

Prophetic movement, 6, 7.
