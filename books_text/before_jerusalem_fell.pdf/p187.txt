The Contemporary Integrity of the Temple 173

Rabbis, ‘bath God bestowed upon the world, and nine of these fall
to the lot of Jerusalem’ — and again, ‘A city, the fame of which has

gone out from one end of the world to the other.’ ‘Thine, O Lord, is

the greatness, the power, the glory, and eternity.’ This — explains the

Talmud —‘Is Jerusalem.’ In opposition to her rival Alexandria, which

was designated ‘the little,’ Jerusalem was called ‘tke great.’ “3°

By the time of the Exile Jerusalem had come to be known among her
people as The City in distinction from The Land:*” and this is usual also
in the Mishna, It is significant of the growth of her importance both
material and spiritual, and of the absence of other cities in the rest of
the now much diminished territory. Townships there were, and not a
few fenced ones; but Jerusalem stood supreme and alone as The
City

The most natural interpretation of Revelation 11, then, would
suggest that the references to the cultic structures have behind them
the literal Temple complex, for only Revelation clearly refers to
Jerusalem. Even recognizing that the part of the Temple complex to
be preserved has a spiritual referent,’ how could John be com-
manded to symbolically measure what did not exist with the idea of
preserving (in some sense) a part and destroying the rest? Why would
there be no reference to its being already destroyed in such a work
as this, a work that treats of judgment upon Judaism? When he
originally held to a late date for Revelation, Robinson asked himself
“Was it not strange that this cataclysmic event was never once
mentioned or apparently hinted at”4*in the books of the New Testa-
ment, particularly in Revelation and Hebrews? Moule came to have
the same concern. Where is there any reference to the rebuilding
of the Temple in Revelation so that it could be again destroyed (as
per the dispensationalist argument) ? Such a suppressed premise is
essential to the futurist argument. If there is no reference to a
rebuilding of the Temple and the book was written about A.D. 95,

89, Alfred Edersheim, Sketches of Jewish Social Lift (Grand Rapids: Eerdmans, rep.
1975), p. 82.

40. ze, 7:23; Jer, 32:24 Psa, 72:16; Ina, 46:6.

41, Smith, Jerusalem, 1:269,

42, See below.

43, Robinson, Redating, p. 10,

44. C, D, F, Moule, The Birth of the Now Testament (8rd cd.: New York: Harper &
Row, 1982), p. 175.
