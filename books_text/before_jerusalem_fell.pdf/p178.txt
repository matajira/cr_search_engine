164 BEFORE JERUSALEM FELL

is plain from the language of the angels. He tells us about these heads
that one is, that five have fallen, and that one is not yet, evidently
pointing to succession.”*’ Another argues similarly: “The seven heads
are best explained as referring to seven kings who represent seven
successive forms of the kingdom.

Various problems beset the view that the “kings” represent “king-
doms,” rendering it unfit as an adequate interpretive option. First,
the word given to help John understand the vision is “kings” (Baa-
Agic). This word never means “kingdom.” Second, as noted above,
the obvious allusion to Rome via the “seven hills” cannot be mis-
taken. To allow it to refer to something other than Rome would be a
cruel taunting of the original audience. Especially would this be so
since the angel declared that he was assisting in the interpretation!
Third, as noted in a earlier section of the present study, the expecta-
tion of the book is that of the events being “at hand” and “near”
(Rev. 1:1,3, 19; 3:10; 6:10; 22:6, 10,12, 20).

Conclusion

Revelation 17 points specifically to the present rule of a sixth
“king” in a succession of seven that rule from seven hills. In light of
the various considerations outlined above, it is obvious that a con-
vincing case can be made for a date sometime during the reign of
Nero, particularly in the latter years of his reign. Although this does
not specify the exact year of dating, it does clearly obviate a late date
for Revelation. And when this extremely strong piece of evidence is
combined with all that given heretofore and with the yet-to-come
internal evidence, the early date position approaches certainty.

67. Hoeksema, Behold, He Cometh, pp. 572, 878,

68, Walvoord, Revelation, p. 250, See also Ladd, Revelation, p, 229, It is a frequent
source of frustration that despite loud calls for a hermeneutic of “consistent liberalism”
by dispensational premillennialist, such a denial of this historically verifiable referent is
urged by them, For calls to liberalism in Revelation, see Walvoord, Revelation, p. 21; and
Charles C, Ryrie, The Living End (Old Tappan, NJ: Revell, 1976), p. 87.
