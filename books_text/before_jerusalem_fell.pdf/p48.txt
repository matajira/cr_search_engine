The Approach to the Question of Dating 31
chronological order, for easy reference.

Firmin Abauzit, Essai sur 1’ Apocalypse (Geneva: 1730). [1, 6]

Jay E, Adams, The Time is at Hand (Phillipsburg, NJ: Presbyterian
and Reformed, 1966).

Luis de Alcasar, Vestigatio arcani Sensus in Apocalypsi (Antwerp: 1614).
f6]

B. Aubé. [6, 3]

Karl August Auberlen, Daniel and Revelationin Their Mutual Relation
(Andover: 1857). [3, 8]

Greg L. Bahnsen, “The Book of Revelation: Its Setting” (unpub-
lished paper, 1984).

Arthur Stapylton Barnes, Christianity at Rome in the Apostolic Age (West-
port, CT: Greenwood, [1938] 1971), pp. 159ff-

dames Vernon Bartlet, The Apostolic Age: Its Life, Doctrine, Worship, and
Polity (Edinburgh: T. & T. Clark, [1899] 1963), Book 2, pp. 388ff.
(1)

Ferdinand Christian Baur, Church History of the First Three Centuries,
3rd ed. (Tubingen: 1863). [3,4, 6, 7]

Albert A. Bell, Jr., “The Date of John’s Apocalypse. The Evidence
of Some Roman Historians Reconsidered,” New Testament Studies
25 (1978):93-102.

Leonhard Bertholdt, Historisch-kritische Einleitung in die simmitichen kan-
onischen u, apocryphischen Schrifien des A, und N. Testaments, vol. 4
(1812-1819).

Willibald Beyschlag, New Testament Theology, trans. Neil Buchanan,
2nd Eng. ed. (Edinburgh: T. &T. Clark, 1896). [7]

Charles Bigg, The Origins of Christianity, ed. by T. B. Strong (Oxford:
Clarendon, 1909), pp. 30,48.

Friedrich Bleek, Vorlesungen und die Apocalypse (Berlin: 1859); and An
Introduction to the New Testament, 2nd cd., trans. William Urwick
(Edinburgh: T. & T. Clark, 1870); and Lectures on the Apocalypse,
ed. Hossbach (1862). [3, 4, 6]

48, Cited in Moses Stuart, Commentary on the Apocalypse, 2 vols. (Andover: Allen,
Merrill, and Wardwell, 1845) 2:277.
