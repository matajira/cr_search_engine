Testimonials Regarding
The Dating of Revelation

66 Here is a book some of us have been awaiting for years! Now that it is here
we can rejoice. Mr. Gentry convincingly demonstrates the fact the book of Revelation
was written, as it in so mony woys declares, prior to the destruction of Jerusalem in
AD. 70, It should receive a wide reading and ought to rattle many windows.99

—Jay E. Adams, Ph. D.,

Author of The Time Js at Hand and Professor,
Westminster Theological Seminary West,
Escondido, California.

64 thorough and outstanding statement of the case for the carly date of Revela-
tion, The book makes one aware of the evidence from within the book and from carly
church sources, and surveys the arguments of New Testament scholars of this century and
previous centuries concerning the question. No stone is left unturned to resolve the
question. 99

- George W. Knight II, Th.D.,
Professor of New Testament,
Covenant Theological Seminary,
St. Louis, Missouri.

66 The Rev, Kenneth Gentry has presented a powerful and convincing case for a
pre-A.D. 70 writing of the book of Revelation. He has demonstrated this from both the
internal and external witnesses, Hopefully this dissertation will be published and widely
read within Christian cirdes.99

-W. Gary Crampton, Th.D., Ph. D.,
Professor of Theology,

Whitefield Theological Seminary,
Lakeland, Florida.

662: would be an unhappy mistake to assume this work is a tedious, technical
treatment of the date of Revelation, The dating question affects the interpretation of
many passages, Gentry’s thorough treatment is thus not only valuable, but it leads the
reader through substantive passages of Relation with illuminating insights. 99

—Carl W. Bogue, Th.D.,
Visiting Professor of Theology,
Whitefield Theological Seminary,
Lakeland, Florida.
