352 BEFORE JERUSALEM FELL

As a matter of fact, several of the early fathers held a distinctly
preteristic interpretation of Daniel 9!°*

In Cyprian we have clear reference to Matthew 24 as referring
to Jerusalem’s A.D. 70 fall. In the entirety of Treatise 12 he is
dealing with testimonies against the Jews, including Christ’s prophe-
cies.

Surely it may not be stated, as do House and Ice: “Why is it that
all of the early fathers, when referring to Revelation and Matthew
24, see these as future events?”

Nero and Revelation

House and Ice write: “If Chilton could show that Nero is the ruler
spoken of in Revelation, then he would have a major victory for his
view. But he cannot. ”6 As I have shown in great detail many lines
of evidence converge upon Nero®: (1) His place as the sixth among
the Roman emperors, (2) his being followed by a seventh, brief
reigning emperor (Galba), (3) his name’s numerical value of 666, (4)
his living while the temple still stood, (5) the prominence of his
persecution in first century Christianity, and more. There is an old
adage: If the shoe fits, wear it. Nero’s footprints are all over Revela-
tion,

64, For a discussion of early interpretive approaches to Daniel 9, see Louis E.
Knowles,“The Interpretation of the Seventy Weeks of Daniel in the Early Fathers,”
Wesiminster Theological Journal 7:2 (May, 1945), 137-138. Actual references include: The
Epistle of Barnabas 16:6; Tertullian, Agawst the Jews 8 (despite being a Montanist and.
therefore premillennial!), Origen, Matthew 24:15; Julius Africanus, Chronography (relevant
Portions preserved in Eusebius, Preparation for the Gospel 10:10 and Demonstrations of the
Gospel 8); Euschius (Demonstrations 8); and Augustine in his 199th epiatle.

65, Cyprian, Treatises, 12:1:6, 15. See especially Roberts and Donaldson, Ante-Nicene
Fathers, §:507-511,

66, House and Ice, Dominion Theology, p, 258 (emphasis mine). In the final analysis,
however, one must wonder how their argument carries weight in light of the Plymouth
Brethren roots of dispensationalism. After all, it is the chief proponent of dispensational-
ism, Charles C, Ryrie, who defends dispensationalism from “the charge of recency” by
labeling such a charge a “straw man” and arguing from history asc “fallacy.” In addition
he writes: “The fact that something was taught in the firat century does not make it right
(unless taught in the canonical Scriptures), and the fact that something was not taught
until the nineteenth century does not make it wrong . . .* (Dispensationalism Today
[Chicago: Moody, 1965], p. 66).

67. Ibid, p. 259.

68. See above, chapters 10, 12, 14, 16, 17, and18.

 
