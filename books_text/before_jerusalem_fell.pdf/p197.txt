The Contemporary Integrity of the Temple 183

1 Clement, too, is oftentimes brought in at this point in the
argument as a first century Christian evidence that is silent on
Jerusalem’s demise. But because of the possible date of writing
argued above, 1 Clement cannot be considered as evidence in that it
was most probably written before Jerusalem’s fall.

With the dismissal of the New Testament canonical books and 1
Clement from consideration, the Moffatt and (early) Moule argu-
ment is virtually eliminated. But these are not the only early Chris-
tian works available to us. The Epistle of Barnabas is almost certainly
a first century Christian work. Lightfoot and Milligan date it between
70 and 79, as do Weizsacker, Hurst, and Bartlet.”! Schaff, Hilgenfeld,
Coxe, and Roberts and Donaldson date it “at the close of the first
century. “7' Reuss, Ewald, Wieseler, and Funk from 79 to 100.”
Robinson dates it between 75 and 100, and Frend “as early as” A-D.
100.7

In Barnabas 4:14 and 5:11 we read the following:

Moreover understand this also, my brothers. When ye see that after
80 many signs and wonders wrought in Israel, even then they were
abandoned, let us give heed, lest haply we be found, as the scripture
saith, many called but fav chosen... .

Therefore the Son of God came in the flesh to this end, that He might

sum up the complete tale of their sins against those who persecuted
and slew His prophets.

At Barnabas 13:1 we read of the distinction between the Christians
and the Jews: “Now let us see whether this people or the first people
bath the inheritance, and whether the covenant had reference to us
or to them.” In Barnabas 16:1 ff. we read of the demise of the Temple:

 

edition of this work, See also Cornelis Vanderwaal, Search the Scriptures, trans. Theodore
Plantinga, vol, 1; Genesis - Exodus (St. Catharines, Ontario: Paideia, 1978), p. 11.

71, See Joseph B, Lightfoot and J. R. Harmer, The Apostolic Fathers (Grand Rapids:
Baker, [1891 ] 1984), pp. 240-241, George L. Hurst, An Outline of the History of Christian
Literature (New York: Macmillan, 1926), p, 11, JamesMuilenburg, Tz Literary Relations
of the Epistle of Barnabas and the Teaching of the Twelve Apostles ( Marburg: Yale, 1921), p. 2.
For Milligan, see Schaff, History 2:678n.

72, Schaff, History 2:678; Coxe, ANF 1; 188; Roberts and Donaldson, ANF 1:185,

78, For bibliographic references see Schall, History 2:678n,

74, Robinson, Redating, pp. 818iI5 W. H. C, Frend, Te Early Church (Philadelphia:
Fortress, 1982), p. 87.
