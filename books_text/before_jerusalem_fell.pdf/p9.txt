PUBLISHER'S PREFACE
by Gary North

Ihave several reasons for wanting to see this book in print. The
first reason is my technical interest in the methods of dating primary
source documents on the basis of their internal evidence and external
evidence from other sources. The accurate dating of historical docu-
ments is crucial to our knowledge of the events of any period of
history. If we do not date our primary source documents accurately,
we cannot expect to gain an accurate understanding of history. There
have been too many ill-fated attempts to compare “contemporary”
events in different ancient societies based on inaccurate chronologies.
The pieces of the chronological jigsaw puzzle do not match, and
therefore must be damaged by the historian in order to jam them
together. My theory of chronology is simple: “If we don’t know when
something happened, we don’t know how or why it happened.”

The Bible is self-consciously an historical book. More than any
other foundational religious text in the man’s history, it claims to be
an historical book. Thus, Christians need to treat it as the historical
document it claims to be. Modern scholarship, even Christian schol-
arship, has too often refused to do this, especially with regard to the
Old Testament. For example, scholars prefer to accept as chronologi-
cal standards the various attempted modern reconstructions of the
historical texts of the non-historically minded Egyptians. They then
rewrite the events of Scripture, especially the events of the Exodus,
in terms of modern interpretations of pagan Egyptian texts. !

My second reason for publishing this book is that as a Bible
student, I want to know when a biblical book or epistle was written,
so that I can better understand the ethical message of the document.

1, Gary North, Moses and Pharaoh: Dommuon Religion vs, Power Religion (Tyler, Texas:
Institute for Christian Economics, 1985), Appendix A: “The Reconstruction of Biblical
Chronology.”
