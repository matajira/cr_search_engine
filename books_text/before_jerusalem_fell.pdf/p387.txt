Ancient Sources art

Philostratus. Life of Apollonius of Tyana.
Pliny the Elder. Natural History.
Pliny the Younger. Epistles,
—___ . Panegyricus.
Revelation of Paul
Seneca the Younger. Apocolocyntosis (or, Pumpkinification).
—___. On Clemency.
Severus, Sulpicius. Dialogues.
. Letters,
—_____ . Suered History.
Sibylline Oracles.
Suetonius. The Lives of the Twelve Caesars: Caligula.
—_____.. The Lives of the Twelve Caesars: Claudius,
. The Lives of the Twelve Caesars: The Divine Julius.
. The Lives of the Twelve Caesars: Nero,
. The Lives of the Twelve Caesars: Galba, Otho, Vitellius.
. The Lives of the Twelve Caesars: The Divine Vespasian.
Suidas.
‘Tacitus. The Annals.
—____ . The Histories.
Teaching of Addacus the Apostle.
Teaching of the Apostles.
Teaching of Simon Cephas.
Tertullian, Against Marcion.
—___ . Ann Answer to the jews.
. Antidote forth Scorpion’s Sting.
. Apology,
. Carm. adv, Mar.
. The Shows.
. On the Exclusion of Heretics,
. On Idolatry.
. On the Mantle,
. Orations.
—_____. To the Nations.
Theophylact. Commentary on John.
Victorious. Commentary on the Apocalypse.
Victor, Aurelius. The Epitome on the Lives and Characters of the Caesars.
