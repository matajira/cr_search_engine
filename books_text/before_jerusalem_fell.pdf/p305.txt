292 BEFORE JERUSALEM FELL

To the advantage of proving the Neronic persecution, the Chris-
tian testimony is well-supplemented by heathen historians. Roman
historian Tacitus, who was born during the early days of the reign
of Nero and who wrote under the reign of Trajan, gives a most
detailed and terrifying account of the beginning of the persecution:

But by no human contrivance, whether lavish distributions of money
or of offerings to appease the gods, could Nero rid himself of the ugly
rumor that the fire was due to his orders. So to dispel the report, he
substituted as the guilty persons and inflicted unheard-of punishments
on those who, detested for their abominable crimes, were vulgarly
called Christians. ...

So those who first confessed were hurried to the trial, and then, on
their showing, an immense number were involved in the same fate,
not so much on the charge of incendiaries as from hatred of the human
race, And their death was aggravated with mockeries, insomuch that,
wrapped in the hides of wild beasts, they were torn to pieces by dogs,
or fastened to crosses to be set on fire, that when the darkness fell they
might be burned to illuminate the night. Nero had offered his own
gardens for the spectacle, and exhibited a circus show, mingling with
the crowd, himself dressed as a charioteer or riding in a chariot.
Whence it came about that, though the victims were guilty and
deserved the most exemplary punishment, a sense of pity was aroused
by the feeling that they were sacrificed not on the altar of public
interest, but to satisfy the cruelty of one man.”

Suetonius credits as one of Nero’s positive contributions as em-
peror”! that he persecuted Christians: “During his reign many abuses
were severely punished and put down, and no fewer new laws were
made: a limit was set to expenditures... . Punishment was inflicted
on the Christians, a class of men given to a new and mischievous
superstition. “3° The evidence is from such sources and of such a
nature that the existence of a Neronic persecution of Christianity
cannot be denied.

30, Tacitus, Annals 15:44.

31, He states ter “I have brought together these acts of his, some of which are
beyond criticiam, while others are even deserving of no slight praise, to separate them
from his shameful and criminal deeds, of which I shall proceed now to give an account”
(Nero 19:3).

82, Suetonius, Nero 162,
