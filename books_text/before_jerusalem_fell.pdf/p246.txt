14
THE LOOMINGJEWISH WAR

As we press on in our argument, we move to a consideration of
the fact that Israel’s condition in Revelation bespeaks a time pre-A.D.
70, as well. This is especially evident in the portrayal of Israel’s
physical condition in the land.

Israel in the Land

In Revelation 7:1-8 we find an interesting temporary divine
protection of “the land” (yi})! where four angels are seen holding
back the winds of destruction:

After this I saw four angels standing at the four corners of the earth,
holding back the four winds of the earth, so that no wind should blow
on the earth or on the sea or on any tree. And I saw another angel
ascending from the rising of the sun, having the seal of the living
God; and he cried out with a loud voice to the four angels to whom
it was granted to harm the earth and the sea, saying, “Do not harm
the earth or the sea or the trees, until we have sealed the bond-
servants of our God on their foreheads.”

Then follows the sealing of the 144,000 from the Twelve Tribes of
Israel.

The language and the manner in which the whole thing is stated could
hardly more distinctly imply that the Jewish nation was still existing,
and occupying its own land, — a land exposed to some impending
desolation, from which the sealed, the one hundred and forty-four
thousand, were to be exempt. The twelve tribes are named, notwith-
standing so many of them had been lost, because the destruction
revealed in connection with the sealing was to overtake the whole land

1, For the proper understanding of y# as a reference to “the land” (i.¢., Israel), 560
earlier discussion in Chap, 8,

232
