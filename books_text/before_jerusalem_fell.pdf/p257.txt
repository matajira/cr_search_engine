The Looming Jewish War 243

wy differ from the Remans; nay, it seemed to be a much lighter thing
to be ruined by the Remans than by themselves.

It surely is not a mere accidental correspondence with history that
is indicated in the fateful war scenes in Revelation. Whereas war with
one’s enemy generally has the effect of unifying a people, Israels
situation was the exact opposite of this.

Revelation 6:5-6

Another extremely significant factor in the Jewish War (probably
one of the three leading factors of devastation, along with the assault
of the mighty Roman imperial forces and the internal civil strife) was
the horrible gravity of the famine that ravished Jerusalem’s belea-
guered populace. The famine is graphically depicted in Revelation
6:5-6: “And when He broke the third seal, I heard the third living
creature saying, ‘Come.’ And I looked, and behold, a black horse;
and he who sat on it had a pair of scales in his hand. And I heard
as it were a voice in the center of the four living creatures saying, ‘A
quart of wheat for a denarius, and three quarts of barley for a
denarius; and do not harm the oil and the wine.” Again Josephus
gives emphatic testimony to the role of famine during the War.“One
piece of evidence from Josephus will illustrate the matter:

But the famine was too hard for all other passions, and itis destructive
to nothing so much as to modesty; for what was otherwise worthy of
reverence, was in this case despised; insomuch that children pulled
the very morsels that their fathers were eating, out of their very
mouths, and what was still more to be pitied, so did the mothers do
ag to their infants... .“

Revelation 7:1-7

The protection of Jewish Christians in Jerusalem is indicated in
Revelation 7:1-7 where the well-known sealing of the 144,000 is
revealed. It has been shown already that this refers to the providen-
tial protection of those Christians ofJewish lineage who were “in the
land.” An extremely interesting and famous piece of tradition informs

82, Wars 4:3:2.

88. Ware §:10:2-5; 5:12:3; 6:8:1-5. It may even be that the reference to “the oil and the
wine” finds expression in the adulteration of the sacred oil and wine by the Jews
themselves; lars §:18:6.

84. Wars 5:10:5. See also Eusebius, Eectesvastical Hastory 3:6.

 
