44 BEFORE JERUSALEM FELL

Yet, despite a confident use of these witnesses by late date
proponents, we will demonstrate that a careful scrutiny of the mate-
rial reveals that the evidence is too diverse to lead to any assured.
conclusion as to this date. Moses Stuart (who late date advocate
James Moffatt claims provided one of only two pre-Alford works that
“retain any critical value” regarding Revelation) '* states well the
situation regarding John’s banishment, and thus of the date of Reve-
lation, when he writes: “Beyond the testimony ofJohn himself, there
is such a diversity of views, as serves to show that mere floating
reports and surmises were the basis of the views. Were not this the
case, how could there have been so great a variety of opinions about
asimple matter of fact?’15

Although our primary concern will be to provide an analytical
inquiry into the late date evidence from Irenaeus and Clement of
Alexandria, a survey of evidence from other early church fathers will
round out the evaluation of the external evidence. The evidence
provided in Part II of the present work is presented with a view to
demonstrating that: (1) Much of the late date external evidence is,
in fact, inconclusive at best. (2) There is some noteworthy early
evidence for a Neronic banishment ofJohn and a pre-A.D. 70 writing
of Revelation.

William Henry Simcox states that “there are statements in early
Christian writers which seem to show that the tradition on this point
was not absolutely unanimous.” '® The generally accepted dates-from
a few of the notable witnesses yield a wide range of diverse conclu-
sions, including a pre-Vespasianic date (Epiphanies, Theophylact,
the Syriac Revelation manuscripts), a Domitianic date (Irenaeus,
Jerome, Eusebius, Sulpicius Severus, Victorious), and a Trajanic
date (Dorotheus). But beyond these few church fathers there are
other historical witnesses, as well.

Let us, then, begin our inquiry into the various ancient sources
that lend themselves to the debate. Following separate treatments of
Irenaeus and Clement of Alexandria, the remaining survey will cover
the additional evidence in chronological succession.

14, Moffatt, Revelation, p. 888.

15, Moses Stuart, Commentary on the Apocalypse, 2 vols. (Andover: Allen, Mot-s-ill, and
Wardwell, 1845) 1:271.

16, William Henry Simeox, The Revelation of St. John Dive. The Cambridge Bible for
Schools and Colleges (Cambridge University Press, 1898), p. xiii.
