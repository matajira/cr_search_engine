222 BEFORE JERUSALEM FELL

The Jewish Character of
Christianity in Revelation
In Revelation there is quite suggestive evidence that the era in
which John wrote was one in which Christianity was still largely
affected by and strongly attached to the Jewish community.

The Evidence

In Revelation 2:9 and 3:9 two churches are warned that some
claim to be Jews, but are not:

I know your tribulation and your poverty (but you are rich), and the
blasphemy by those who say they are Jews and are not, but are a
synagogue of Satan (Rev. 2:9).

Behold, I will cause those of the synagogue of Satan, who say that
they are Jews, and are not, but lie — behold, I will make them to
come and bow down at your feet, and to know that I have loved you
Rev. 3:9).

John here indicates that at least two of the seven churches (Smyrna
and Philadelphia) are plagued by “those who say they are Jews. ”
That those who plagued them were racial Jews and undoubtedly of
the Jewish faith can be fairly assumed in that the Jews had distinctive
racial features and wore a distinctive cultic mark (circumcision).
The question naturally arises: Who would array themselves against
the Church, posing as racial Jews, who were not racial Jews?’ Appar-
ently these churches were being persecuted by Jews in these two

 

(London: Adam and Charles Black, 1970), p. 26. He documents the Jewish terminology
which Christians used of themselves (pp. 26ff:) and develops the Church's “self-
understanding” in chaps. 2 and 3.

6. Justin Martyr wrote “For the circumcision according to the flesh, which is from
Abraham, was given for a sign; that you may be separated from other nations, and from
us;... For you are not recognised among the rest of men by any other mark than your
fleshly circumcision” (Dialogue with Trypho the Jaw 16), Tacitus wrote of the Jews: “They
adopted circumcision to distinguish themselves from other peoples by this difference”
(Histories 5:5). See also Martial 7:82 and Tertullian, ATS Answer fo the Jaws 8, In the
post-Maccabean era circumcision attained immense importance among the Jews, /
Maccabees 1:18, 48, 60; Assumption of Moses 8:1; Josephus, Antiquities 12:241, They also
wore distinctive clothing (Num. 15), which had developed by this time into the prayer-
shawl with its tassels,

7. Interestingly for our thesis, in the two verses under consideration John uses the
Hebrew word for the devil (verevéc), rather than the Greek (6:¢fo40c). Commentators
deeming this fact noteworthy include Robert H. Mounce, The Book of Revelation. New
