The Contemporary Integrity of the Temple 189

A.D.” J, H. Charlesworth, in his editorial emendation to the article
by Rubinkiewicz, writes that: “our pseudepigraphon was written after
A.D. 70, because the author describes the destruction of Jerusalem
(cf. ch. 27). Hence, the apocalypse — that is the early Jewish stra-
tum — was composed sometime after A.D. 70 and before the middle
of the second century. “9° G. H. Box and J. I. Landsman concur.
L. Ginzberg places it in “the last decades of the first century.”97

It is important to bear in mind that: “the Apocalypse of Abraham
is one of the most important works written after the destruction of
the nation in A.D. 70. The importance of the apocalypse can be
compared to that of 2 Baruch or 4 Ezra, but our author analyzes the
causes of the destruction of Jerusalem from a different perspective:
The defeat was caused by the infidelity of Israel toward the covenant
with God and the opportunistic politics of some leaders. “9°In chapter
27:1-6 we read of this Jewish lamentation over Jerusalem:

And I looked and I saw, and behold the picture swayed. And from its
left side a crowd of heathens ran out and they captured the men,
women, and children who were on its right side. And some they
slaughtered and others they kept with them. Behold, I saw (them)
running to them by way of four ascents and they burned the Temple
with fire, and they plundered the holy things that were in it, And I
said, “Eternal One, the people you received from me are being robbed
by the hordes of the heathen. They are killing some and holding
others as aliens, and they burned the Temple with fire and they are
stealing and destroying the beautiful things which are in it. Eternal,
Mighty One! If this is so, why now have you afflicted my heart and
why will it be 50?”

Clearly this first century Jewish work despairs over the fall of Jerusa-
lem. Of course, it does not attribute it to the Jewish role in the
crucifixion of Christ, but it does illustrate again that the fall had a
tremendous impact on the minds and affections of post-fall Judaism.
This impact was not overlooked by the Christian tradition, as we

94, R, Rubinkiewiez, “Apocalypse of Abraham,” intétd, 1:688,

95. See ibid. 1:688.

96. G. H. Box and J. I. Landsman, The Apocalypse of Abraham (London: Pitman, 1918),
p.xv ff,

97. L, Ginzberg, “Apocalypse of Abraham, “in The Jewish Engelopedia (New York
KTAV, 1953-1968) 1:92,

98, Rubinkiewicz, “Apocalypse of Abraham,” OTP 1:685.
