172 BEFORE JERUSALEM FELL

written (apparently) from Egypt in the 90s."In this oracle Jerusalem
is spoken of thus:

He seized the divinely built Temple and burned the citizens and
peoples who went into it, men whom I rightly praised.

For on his appearance the whole creation was shaken and kings
perished, and those in whom sovereignty remained destroyed a great
city and righteous people... .*

For murder and terrors are in store for all men because of te great
city and righteous people which is preserved throughout everything
which Providence held in special place... .“

But now a certain insignificant and impious king has gone up, cast it
down, and left it in ruins with a great horde and illustrious men. He
himself perished at immortal hands when he left the land, and no
such sign has yet been performed among men that others should think
to sack a great city.3

Josephus sadly extols Jerusalem’s lost glory after its destruction:

This was the end which Jerusalem came to by the madness of those
that were for innovations; a city otherwise of great magnificence, and
of mighty fame among all mankind.*4

And where is not that great city, the metropolis of the Jewish nation,
which was fortified by so many walls round about, which had so
many fortresses and large towers to defend it, which could hardly
contain the instruments prepared for the war, and which had so many
ten thousands of men to fight for it? Where is this city that was
believed to have God himself inhabiting therein? It is now demolished
to the very foundations.”

He also records John of Gischala’s retort to Titus’s call (through the
captured Josephus) for the surrender of the city; John refused to
surrender Jerusalem because “it was God’s own city.”28

Edersheim reminds us that “Ten measures of beauty,’ say the

 

33, Sibyltine Oracles 5:150-154; OTP 1:396, Emphasis mine,
34, Sibylline Oracles 5:225-227; OTP 1:398, Emphasis mine.
35, Sibyllive Oracles 5:408-413: OTP 1.403, Emphasis mine.
86, The Wars of the fans 7:1
87. Ware 7:8:7. Emphasis mine,
88, Ware 62:1.

 
