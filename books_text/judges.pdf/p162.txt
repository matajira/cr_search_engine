Gideon: God’s War Against Baai 1s

15. And he came to the men of Succoth and said, “Behold
Zebah and Zalmunna, concerning whom you taunted me, saying
‘Is the palm of Zebah and Zalmunna already in your hand, that
we should give bread to your men who are weary?’ “

16. And he took the elders of the city, and thorns of the
wilderness and briers, and he made the men of Succoth ac-
quainted with them!

17. And he tore down the tower of Penuel and killed the men
of the city.

Gideon’s method of attacking Succoth reminds us of the way
Jericho was spied out, and also of Luz (Jud. 1:24). Now it is an
Israelite town that is treated as an enemy. The full complement
of the city’s rulers, 77 men, were scourged with thoms. They
would not “know” the Lord, so they were made to “know” the
curse, firsthand!

Gideon not only tore down Penuel’s tower, but he killed the
men (leaders probably) of that city. Why he was harder on
Penuel than on Succoth we do not know. He had his reasons,
and since he is not condemned, we trust they were good ones.
Perhaps it was because Penuel’s sin of trusting in their own
tower was more serious than Succoth’s.

Both Succoth and Penuel were cities in Gad. Gad had failed
to support Deborah (Jud. 5:17), and apparently was pretty weak
spiritually. All was not lost, however, for the Gadite town of
Jogbehah supported Gideon (Jud. 8:11).

Why did Gideon punish Succoth and Penuel when he did
nothing of the sort to Ephraim? The difference is all important.
Ephraim was seffish, and this is a sin; but Ephraim did fight on
the Lord’s side. Ephraim is in the position of a genuine Chris-
tian who has a habitual sin; he needed a rebuke, not full judg-
ment. Ephraim was in sin, not in open apostasy. Time would tell
which way he would go. Paul was able to rejoice when the
gospel was preached by contentious men (Phil. 1:15-19). Paul
knew that God would eventually deal with them. Jephthah
would deal with Ephraim by and by.

Succoth and Penuel, on the other hand, were faithless, and
this is apostasy. They did not fight on the Lord’s side, and since
neutrality is impossible, they were against the Lord. They were
God’s enemies, and they were treated as such.
