Othniel: Babylon Anticipated 55

versed, and there would be nothing left. Such was the threat.
(When it finally came to pass, at the exile, we find that the grace
and love of God is so great that He did not cast them off totally
after all.)

They were oppressed eight years. The number eight in Scrip-
ture frequently points to new beginnings. Man’s sin defiled his
first week, but God grants him a new opportunity on the eighth
day (the day circumcision was performed, and day of Christ’s
resurrection). > Possibly the deliverance in the eighth year here
was designed to show that God was giving Israel a second
chance. (By the way, it is not possible to construct a strict
chronology for the book of Judges, since various judges ruled
locally, and their rules overlapped. Totalling up all the numbers
gives us too many years, well more than the 480 years permitted
by the statement of 1 Kings 6:1. This makes it all the more likely
that the numbers in Judges, in addition to being historically ac-
curate, have theological significance.)

The deliverer God raised up was Othniel. The meaning of his
name is not absolutely known, but probably “God is Powerful”
or “Lion of God” is correct. Regardless of the literal meaning of
his name, Othniel certainly was a lion of the Tribe of Judah. He
was not, however, a descendant of Judah, but a converted
Kenizzite, as we have seen in our comments on Judges 1:11-15.
Since most of the tribe of Judah were bastards, at this time none
were eligible for public office in Israel. (We shall return to this
matter later in this commentary.) In his victory over this proto-
Babylon, we can see a foreshadowing of the Greater Othniel,
Who destroyed the ultimate Babylon in Revelation 19:11-16.

Babylon is still with us today, in Washington, D. C., as well
as in the Kremlin. Those who reject the salvation offered by the
Greater Othniel will windup in slavery to some modern Doubly-
Wicked bureaucrat or commissar.

After the oppression, there were forty years of peace.
Whether Othniel lived to judge this entire period we are not
told. We ought probably to envision the normal process of judg-
ing according to the pattern given by Jethro in Exodus 18. Most
civil judgments would be rendered by local elders over tens,

3, Ihave discussed this at greater length in my book, The Law of the Cove-
nant: An Exposition of Exodus 21-23 (Tyler, TX: Institute for Christian Eco-
nomics, 1984), p. 164.
