xvi Judges

Fall: Judges chapter 1

Decline: book of Judges

Judgment: capture of the Ark at the time of Samson,
Samuel, and Ruth (1 develop this in detail in chapter 12 of
this book)

Re-creation: the return of the Ark

Basic pattern No. 2c:
Creation: Samuel and David
Fall: Solomon, who breaks all the laws for kings (compare
Deuteronomy 17:16f. with 1 Kings 10:14ff., 26ff.; 11:1ff.)
Decline: the two monarchies
Judgment: the destruction of Jerusalem and the Exile
Re-creation: Ezra, Nehemiah

Throughout the Bible, there are smaller manifestations of
this pattern as well. Our concern in this third “secret” of inter-
pretation is to note the position of the book of Judges in the
overall sweep of redemptive history. Judges records the fall,
decline, and judgment of Israel, and also (in Samson and in the
last chapter) the beginnings of re-creation. This is an important
structure for understanding the book.

The fourth “secret” of interpretation is to pay close attention
to the specific details in the text. God does not waste words.
God has absolute superintendence of events, and every detail
recorded in the text is to be pondered for significance. Judges
9:53, for instance, does not say, “Someone threw a stone and it
hit Abimelech so that he was dying.” Rather, it says, “A certain
woman threw an upper millstone on Abimelech’s head, crushing
his skull.” Every detail is important, as we shall see in chapter 8
of this study: that it was a woman, that it was a stone, that it
was a millstone, that it hit and crushed his head.

Similarly, numbers are usually important as symbols in the
text. Ancient writers always used numbers symbolically, and it
strains credulity to think that the writers of the Bible did not do
so. People today don’t think of numbers symbolically, but in the
history of the world, modern man is a great exception on this
point. To be sure, the numbers are also literally true, but since
God superintends all events, we are certainly invited to consider
the deeper significance of the number patterns in the text.

The writings in the Bible are carefully constructed literary
