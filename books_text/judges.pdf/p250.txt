Samson the Nazirite 235

lined the chapter accordingly: Message (13:2-14); Messenger
(13:15-23).

The author of Judges wants us to contrast this scene with
Judges 6. Gideon makes the same offer, and God is willing to eat
with him. Gideon sacrifices a kid, an animal that symbolizes a
youth, just as Manoah offers a kid. Gideon is fearful after seeing
the Angel of the Lord, just as Manoah is. After the sacrifice, God
states that there is peace between them, affirming for us that this
was a Peace Sacrifice, and thus a communion meal. Here, how-
ever, God refuses to eat with Manoah, and thus with Israel. God
is frankly at war, not at peace, with a sinful Israel. What is
needed is a whole burt sacrifice, a confession of sin and judg-
ment. Communion can only come after the re-consecration of
person (whole burnt sacrifice) and works (grain offering). With
Gideon, God ignited the fire, ate the food, and vanished from
sight. Here, however, something more amazing happens.

While the kid is being bumed up, the Angel steps onto the
altar and goes up in flames! This is a picture of the substitu-
tionary death of Jesus Christ. This is the great turning point we
are needing at this time in history. Because God has sent his
Son/Angel to be the Substitute, history can turn around and
Israel can be delivered from the Philistine. Because Christ has
died, Samson can live. God’s holy Kid (Child) substitutes for the
kid (child) of Manoah and his wife. When the Angel goes up in
flames, it is a picture that God Himself will take the punishment
that men deserve!

The Angel will not give His Name. This shows that He is
God. The only Name He gives is “Wonderful,” and this is ex-
plained in the next verse in that “He performed wonders.” Won-
ders are miracles, the unexpected surprises that God humor-
ously brings into history in order to reverse the expectations of
both the wicked and the righteous. God did wonders in Egypt
(Ps. 78:12). God here does wonders again. There will be a new
exodus from this new bondage.

Because God is a God of wonders, we cannot put Him in
a box. We cannot have a “name” for Him that completely
describes Him, so that we can say we understand Him fully.
(The reason Jehovah’s Witnesses are so insistent on the name
“Jehovah” is precisely because they think they can control God
by using that name.) God is always a God of surprises, and the
