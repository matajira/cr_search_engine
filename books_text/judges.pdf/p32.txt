14 Judges

In order to drive this point home, the narrator says, “Now
the Lorp was with Judah .. .; but... .“ God was willing, but
man was faithless.

The plains were in the center of the land of promise. The con-
tinuing strength of the Canaanites here effectively divided Judah
and Simeon from the rest of the tribes. Over the centuries, this
isolation brought about cultural division, and caused more and
more trouble until finally the two kingdoms split from one
another. Thus do minor compromises grow into major troubles.

Jerusalem Lost
20. Then they gave Hebron to Caleb, as Moses had spoken;
and he drove out from there the three sons of Anak,
21. But the sons of Benjamin did not drive out the Jebusites
who lived in Jerusalem; so the Jebusites have lived with the sons
of Benjamin in Jerusalem to this day.

These two verses are also designed as contrasts. The aged but
faithful Caleb drove out the giants from Hebron, but the Benja-
mites did not drive the normal-sized Jebusites from Jerusalem,
even after Judah had conquered it for them. Unbelievers con-
tinued to live in the holy city.

Faithlessness was the reason. This was a bad start for Ben-
jamin, and their moral situation was to worsen until God saw fit
virtually to liquidate them (Judges 19, 20).

(The phrase “to this day” indicates that Judges was composed
before David conquered Jerusalem, and corroborates my thesis
that Samuel most likely wrote the book.)

In summary, Judah started well, but failed to follow through.
They compromised. All the same, as before we do have pictures
of Christ in this passage as well:

Philistine — Christ destroyed “all five” Philistine cities, five
being the number of preeminence and power. ! This is pictured
more fully by Samson later on, and the Samson story answers to
the failure of Judah here in Judges 1.

1, On the number five, see my book, The Law of the Covenant: An Exposi-
sion of Exodus 21-23 (Tyler, TX: Institute for Christian Economics, 1984), Ap-
pendix G, “Four and Five-fold Restitution;” and see discussion below on

iges 1:27.
