68 Judges

oppressed Israel totally and fully (10 + 7), as we saw above, so
Moab was totally and completely defeated. God had ordered
that Amalek be completely obliterated (Ex. 17:14-16; Dt. 25:19),
and so every neo-Sodomite and Amalekite warrior was faithfully
destroyed by Ehud. This faithful “response of man” led to
eighty years of blessing as “God’s evaluation.”

Ehud himself killed the enemy leader; Ehud’s men killed the
enemy’s troops. Thus it is with Christ also. He has dealt with
Satan, and we are called to deal with Satan’s legions. Revelation
19:11-16 pictures the Greater Ehud as He leads His army to vic-
tory by the use of the proclamation of the Word, the two-edged
sword that comes from His mouth.

God’s people emerged to rule over their enemies. They had
driven back the opposition, and the land rested eighty years,
two generations. This indicates that Ehud and his generation
must have been quite faithful in teaching the Truth to their chil-
dren, for it was not until the third generation that God again
chastised His people.

Finally, we must see that it was the Lord Who raised up the
enemy (v. 12); it was the Lord Who raised up the savior (v. 15);
and it was the Lord Who gave the victory (v. 28). We must ever
see the Lord at work in Scripture.

Shamgar: The Surprise Judge
31. And after him came Shamgar the son of Anath, who
struck down six hundred Philistine with an oxgoad; and he also
saved (yasha‘) Israel.

Shamgar is not an Israelite word, so it is possible, yea likely,
that he was a convert to Israel’s God and cause. He is said to be
the son of Anath, meaning either that he came from an environ-
ment of Anath-worship, or that his father’s name was Anath,
the same as that of the fertility goddess mentioned in Judges
1:33, In either case this again makes it likely that Shamgar was a
convert.

He lived in the southern part of Israel, where the Philistine
were. Thus, he was an heir to Ehud, working during the eighty
year period mentioned in v. 30. He probably labored toward the
end of that period, since he was a contemporary of Deborah
(Sud. 5:6), Under his Spiritual guidance, the southern part of
