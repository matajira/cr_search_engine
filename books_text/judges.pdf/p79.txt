Ehud: Sodom and Amalek Resurgent 61

53:3), but His “name” was changed when He was elevated to the
right hand of the Father (Acts 7:56), the seat of rule and author-
ity. (See Micah 5:2-3 for a prophetic commentary on this.)

Ehud was well thought of already. The tribute money was
sent by his hand to the oppressing ruler, Eglon. Indeed, Eglon
held Ehud in such esteem as to grant him a private conference
later on. This would have been possible only if Ehud had
already been a leader in Israel.

Ehud’s two-edged sword is noted here. The sword had no
crosspiece, for it sank completely into Eglon. It was apparently
the length of a short cubit, from elbow to knuckles. Ehud
bound it on his right thigh, so that it would be available to his
left hand. The sword is specifically noted as two-edged, calling
to mind Christ’s two-edged sword, the Word of God (Ps. 45:3;
Eph. 6:17; Heb, 4:12; Rev. 19:15).

After presenting the tribute, Ehud and his men retired, and
separated at the “graven stones” at Gilgal. These might refer to
idols set up by Egion at that historic site, in which case they
doubtless galled and goaded Ehud into action; or the reference
might be to the memorial stones set up by Joshua (Josh. 4:1-8,
20), which would also have called his purpose to his mind.
While it is not actually forbidden by Scripture, it is unlikely that
Joshua’s memorial stones would have been graven, on analogy
with Exodus 20:4 and 25. Thus, I think it most likely that Moab
had defiled Gilgal with idols.

Ehud left a force of men there, and returned to Eglon’s
abode. Gilgal was between Jericho and the Jordan, and Egion’s
army would have to retreat toward the Jordan, passing by the
trap set for them at Gilgal. There is also a snapshot of redemp-
tion here, in that the Lord’s army was told to wait while the
Lord’s anointed messiah struck the definitive blow. Christ
strikes the killing blow against Satan, and then His army moves
in to mop up Satan’s fleeing army.

Ehud asked for a private conference, saying that he had a
secret message for Eglon. That he was granted it again shows his
position as a magistrate in Israel. He announced that he had a
message for the king, one so important that it could only be
delivered in secret. This message was the Word of God, in its
negative aspect of judgment, though Eglon did not know it
then. The king ordered all his retainers to keep silence, that is,
