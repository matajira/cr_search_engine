INTRODUCTION

One of the best ways to communicate truth in such a way as
to grip the hearts and minds of the hearer is by means of story
telling. The Bible is full of stories, designed for just this pur-
pose. The whole theology of story telling could use a treatment
in itself. In this book, however, it is my intention to illustrate
such a theology, rather than to write it systematically.

God is Himself the Great Story Teller. Being God, He can
sovereignly superintend all events so as to bring His stories to
life. His stories really happened. The fact that they are told as
stories does not subtract one whit from their real historical
character. Still, what gives them their thrilling power is not only
that we know that they really happened in a certain year and at a
certain place, but because they speak to us today.

Why do good stories speak to us today? Because, as students
of literature would say, they embody universal characteristics,
and deal with universal problems, hopes, fears, symbols, and so
forth. This is exactly correct. Universal truths are not the same
as abstract generalities, however. It is precisely in the specific
events themselves that the most universal aspects of the stories
are seen.

Images of God in Judges

There are in Biblical theology certain great universals. They
derive from the fact that man is the image, the very symbol of
God. Thus, throughout the Bible marches The Seed. He is the
one bom of The Woman who will crush the head of The Ser-
pent, We shall meet him several times in the book of Judges. In-
deed, the crushing of the head of the enemy is one of the most
obvious themes in the book:

ix
