184 Judges

forces its basic message. While this type of numerology seems
strange to 20th century readers, it is only because they have not
been educated in it. It was normal for ancient, medieval, and
early modern writers to employ numerological structures, and
the Bible is no exception to this.)

The social system given in Exodus 18 had two aspects. One
was the ministry of the Levites. These were locally scattered
throughout Israel, and basically were the pastors of the local
churches. Their job was to train the people in the Law, so that
they would mature in self-government. The other aspect was the
system of judges and courts of appeals. There were judges over
10s, 50s, 100s, and 1000s, with a final court of appeal at the top
of the system. The whole thrust of this system was localistic. Im-
mediate, day to day decisions would be made by the local elders,
the “elders of the gate,” which were the elders over 10s sitting as
a group. Only hard cases would be appealed up the line, and
only the toughest of all would ever come to the judge of all
Israel, at the top (for an example, see 1 Kings 3:16ff.), Thus,
although there was a strong government over the whole nation
in one sense (defiance of the judge resulted in the death penalty,
Dt. 17:8ff.), yet this central government had next to nothing to
do with the day to day affairs of the citizens. It only existed as a
final court of appeals.

The tendency, however, is for sinful man to reverse this. In-
stead of local churches with courts of appeal, we get top heavy
denominational structures that tend to invade and consume
local ministries. Instead of local government, we get national
government, universal conscription, and heavy national taxa-
tion. Instead of local police, locally accountable, we tend to get
national police (not yet in America, though). This is because in
Baalistic humanism it is man, not God, who is king.

Since God is omnipresent, He can manifest His rule in every
place, and every place the same if the Levites (Church) are strong
and teaching the same Law (Bible) in every place. God’s om-
nipresence, manifest through the work of the Church, makes
for a civilization that is fundamentally uniform throughout,
without that uniformity’s being imposed by tyranny, so that
there is also rich diversity of local color.

When men want to play God, however, they can only impose
their will over a large area by using implements of force and
