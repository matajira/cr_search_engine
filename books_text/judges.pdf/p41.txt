Conquest, Compromise, Judgment, and Restoration 23

Lorp: What have you done? God always treats men as responsi-
ble, and His questions are designed to provoke self-examination
(“Adam, where art thou?“ “Simon Peter, lovest thou Me?”).
Thus it is here: What have you done? Think about it. Meditate
upon it. And repent.

Now the Lorp passes judgment (verse 3), in strict accord
with His threat articulated twice already (in Num. 33:55 and
Josh. 23 :13f .). The judgment is in line with the two indictments.
First, since the Canaanites are still in the land, “they will be in
your sides” (literally; the “as thorns” is added by the translators
to bring out the sense). This first judgment has a number of
ramifications. As the translators note, the Canaanites will be
thorns in their sides. But also, since Eve came from Adam’s
side, the prediction is that Israel will intermarry with the Ca-
naanites, which will lead to further woes. We can also imagine
them walking side by side with Israel, tripping them up. Oxen
are yoked side by side, so that for the Canaanites to be in the
side of Israel implies that they were now “unequally yoked
together with unbelievers” (2 Cor. 6:14).

Second, since the altars are still in the land, “their gods will be a
snare to you.” A snare lies on the ground, waiting to catch a bird or
small animal. If a man steps into a snare, he receives a wound in
the foot. The imagery here, thus, refers us to Genesis 3:15, where
the enemy is said to have his head crushed, while the righteous
receive wounds in the foot. Specific instances of foot wounds are
not found in Judges (though Jud. 5:31 alludes to Gen. 32:31, and
Jacob’s limp), but the crushing of the serpent’s head is a major
theme in the book (see the Introduction for more on this).

The snares will trip them up. Later on, in Judges 3:2, we see
that, as always, there is mercy mixed with this judgment; but
here the judgment is expressed as total. It is tripping that must
drive Israel, in desperation, to its knees.

These two sins and judgments (idolatry and adulterous
covenanting) form a theme in Judges. They recur as the core
description of Israel’s sin in Judges 3:6. The two appendices to
Judges explore each in depth. The first appendix deals with the
Levites’ failure to protect Israel from idolatry, and the conse-
quences of this. The second appendix deals with the Levites’
failure to protect Israel from adultery, and the consequences of
this. Also, the snare of idolatry is particularly explored in the
