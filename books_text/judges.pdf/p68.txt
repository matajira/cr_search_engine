50 Judges

assembly of the rulers, to ask their advice and consent. This we
find to have been repeatedly done by Moses, Joshua, and
Samuel.”

“Still another limitation to the authority of the Hebrew
judges was in the law itself. Their power could not be stretched
beyond its legal bounds.”

Wines sums it all up this way: “No salary was attached to the
chief magistracy in the Hebrew government. No revenues were
appropriated to the judges, except, perhaps, a larger share of
the spoils taken in war, and the presents spontaneously made to
them as testimonials of respect (Jud. 8:24; 1 Sam.9:7; 10:27). No
tribute was raised for them. They had no outward badges of
dignity. [This may be a bit extreme on Wines’s part, since there
probably was some type of robe of office, but Wines is certainly
right in what follows.] They did not wear the diadem. They were
not surrounded by a crowd of satellites. They were not invested
with the sovereign power. They could issue orders; but they
could not enact laws. They had not the right of appointing
officers, except perhaps in the army. They had no power to lay
new burdens upon the people in the form of taxes. They were
ministers of justice, protectors of law, defenders of religion, and
avengers of crime; particularly the crime of idolatry. But their
power was constitutional, not arbitrary. It was kept within due
bounds by the barriers of law, the decisions of the oracle, and
the advice and consent of the senate and commons of Israel.
They were without show, without pomp, without retinue, with-
out equipage; plain republican magistrates.” While Wines may
go a bit too far in rejecting all outward symbols of office, since
these were common and expected in this period of history, in the
main he is clearly correct. As we shall see, the history of the
period of Judges shows the regents of Israel gradually aggran-
dizing to themselves more and more of the trappings of power.
This will be our main focus of attention in chapter 9 of this
study.

Kings and judges are shepherds. The central section of
Judges, at which we have now arrived, is concerned with this.
Priests, prophets, ministers, Levites — these are guardians. The
two appendices of Judges deal with Levites, and their failure to
guard Israel properly. The point of the appendices (Judges 17-
21) is to show that the failure of these moral guardians was the
