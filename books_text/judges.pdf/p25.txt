Conquest, Compromise, Judgment, and Restoration 7

ing the promised land (Num. 13:28 ff.). No such fear stops faith-
ful Judah now, however.

The association of giants with Hebron tells that if we want to
have sanctuary, we have to destroy the giants. God gives no ref-
uge to those who do not war against sin.

The Story of Othniel and Achsah

11. Then from there he went against the inhabitants of
Debir. Now the name of Debir formerly was Kiriath-Sepher.

12, And Caleb said, “The one who attacks Kiriath-Sepher
and captures it, I will even give him my daughter Achsah for a
wife.”

13. And Othniel the son of Kenaz, Caleb’s younger brother,
captured it; so he gave him his daughter Achsah for a wife.

14, Then it came about when she came to him, that she per-
suaded him to ask her father for a field. Then she alighted from
her donkey, and Caleb said to her, “What do you wish for your-
self?”

15. And she said to him, “Give me a blessing: Since you have
given me the land of the south country, give me also springs of
water.” So Caleb gave her the upper springs and the lower springs.

Here is one of the famous, romantic love stories in the Bible,
found also in Joshua 15:13-19, as if to tell us that it is so impor-
tant that it should be told twice. The setting is Debir. Debir
means “word.” The city was formerly Kiriath-Sepher, which
means “city of books.” In other words, Debir was a large library
city. It was where the clay tablets were stored. It was the reposi-
tory for the philosophical books of the Canaanites, their genea-
logical records, their trading records, treaties, land ownership
documents, and much more. To destroy this place was to
destroy their entire civilization, as can well be imagined. Thus,
Debir was well guarded, for an entire civilization depended on
the preservation of its books.

Caleb and Joshua were the only two spies who had ad-
vocated conquering the land of Canaan (Num. 13, 14), and as a
result, they and they alone were allowed to enter the land. It was
the giants who had frightened the people away, and we can well
imagine what was on Caleb’s mind all those thirty-eight long,
wearying years of wandering in the desert: Just wait until I get
my hands on those giants! Thus, when Joshua offered to let
