Jephthah: Reaching for the Crown 201

God, to present your bodies a living and holy sacrifice, well-
pleasing to God.” Jephthah knew the Scriptures, as we have
seen, and he was filled with the Spirit. It is utterly inconceivable
that he would offer to kill a human being in exchange for a vic-
tory from God. Not only is this unthinkable in terms of the con-
science of the believer, it is also the case that Jephthah knew that
sinful man can never be a sacrifice acceptable to God. In fact,
that is one of the primary reasons why there is no human sacri-
fice in Scripture except for the sacrifice of the one perfect human
who ever lived, Jesus Christ. Moreover, just as the people
prevented Saul from killing Jonathan (1 Sam. 14:45), so priests
and people would not have stood idly by and allowed such an
abomination as human sacrifice to take place. (And it would
have been an abomination, not primarily because it would have
been a murder, but first of all because a sinful human being is
not an acceptable sacrifice to the Most Holy One.)

Rather, it is clear from the sequel that he had in mind some
permanent service to God that would prevent the person from
living a normal life. We find in Exodus 38:8 and 2 Samuel 22:2
that women did serve full time at the Tabernacle, and we know
from Leviticus 27 that such vows of service were possible; there
can be little doubt but that this was what Jephthah had in mind.
We find women serving the Incamate Tabemacle in Matthew
27:55-56 and Luke 8:2f., and we find women set aside on a
special roll to serve in the New Testament Church (1 Tim.
521-16).

But why the first person who comes “out of the doors of my
house”? There is a specific reason for this, and it tells us much of
what Jephthah had in mind, and why he mourned when it turn-
ed out to be his only child who came out. The doors of the
house in Scripture are symbols of birth. They correspond to the
“doorway” on the woman’s body, where the baby is bom.

This association of doorways with birth is common in the
Bible, but it may seem strange to us, so we should get some
verses in mind before proceeding further. In Genesis 18:10, it is
while standing in the doorway of her tent that Sarah hears she
will have a child. In 1 Samuel 1:9, Hannah is standing in the
doorway of the temple when she hears that she will have a child.
In 2 Kings 4:15 the Shunamite woman is standing in a doorway
when she is told that she will have a son. In a reverse twist, Eli
