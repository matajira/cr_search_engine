7

GIDEON: GOD’S WAR AGAINST BAAL
(Judges 6:1 - 8:28)

The story of Gideon and of his son, Abimelech, is not easy
to divide up into sections. There are several apparent concerns
in the text, which are woven together in such a way as to
overlap. The larger concerns include these:

1. Judgment for sin, oppression by Midian, Amalek, and
Ishmael, and deliverance.

2. God’s maturation of Gideon’ s faith.

3. Israel’s drift toward a Baalistic, statist order.

4. Judgment for sin, oppression from within the nation, and
deliverance,

5. The Lorp’s war against Baal.

There are two periods of oppression, the first under Midian,
and the second under the false king Abimelech.

Rather than give a list of all the interactions between God
and man in Judges 6-9, it is simpler to summarize by noting that
God judges Israel for her sin in Judges 6, Israel begins to repent,
and God raises up a deliverer. God interacts with Gideon in a
series of command/promises, to which Gideon responds each
time in faith. After the battle, Gideon passes a whole series of
judgments (evaluations) from the Lord: against the heads of the
enemy army, against two Israelite towns, and against the tribe of
Ephraim. Gideon’s final command, speaking for the Lord, is
that their king is God, not a man. In his old age, however, Gi-
deon begins to be unfaithful to this rule, and since Israel lusts
for a king, God gives them one. Even though nothing is said
about human repentance, God eventually does deliver Israel
from Abimelech, this time without a human deliverer. Rather,
God simply lets the evil destroy each other.

The following is an outline of the text:

au
