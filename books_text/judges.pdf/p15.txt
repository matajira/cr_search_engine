xviii Judges

Ill. Two Unlikely Judges:
A. Deborah, a woman (4:1-5:31)
B. Gideon, a youth (6:1-9:57)
1, Gideon’s triumphs (6:1-8:28)
2. Gideon’s fall and the beginning of the polemic
against kingship (8:29-9:57)

(Notice that Psalm 83:9-12 and Isaiah 9:1-4 put the Deborah
and Gideon stories together.)

 

IV. Two Compromised Judges:

A. Jephthah, the half-breed {10:1-12:15)
1. Jephthah’s sin of desiring the crown (11:1-40)
2. Jephthah’s righteous acts (12:1-7)

B. Samson, the Nazirite (13:1-16:31)
1, Samson’s birth (13)
2. Samson’s evangelistic work (14-15)
3. Samson’s fall (16)

(Notice that the story of Jephthah is bracketed with notices
about minor judges (10:1-5; 12:8-15}, which illustrate the tempta-
tion to kingship. Notice also that the Jephthah and Samson
stories are inversions one of another: Jephthah’s righteous acts
come after his fall, while Samson’s fall comes after his righteous
acts.)

V. Two Appendices
A. The Levites fail to guard the worship of Israel
(17-18) (See 3:7 -idolatry)
B. The Levites fail to guard the morality of Israel
(19-21) (See 3:6 - whoredom)

Such is the simplest way to outline the book. There is a sec-
ond way to do it, which brings out the two-witness aspect even
more fully. Beginning with Ehud, at least, each section intro-
duces a theme that is taken up by the next section, as follows:

Ehud and’ Deborah: In both stories we have deliverance
from the enemy by an assassinating hand. In both stories the
head of the serpent is crushed by the Messianic hero or heroine,
and then the armies of God follow after with a mopping up
operation. (Cf. 3:27 and 4:14), Ehud recaptured the “City of
Palm Trees,” and Deborah sat as judge under a palm tree.

Deborah and Gideon: In both stories we have deliverance by
