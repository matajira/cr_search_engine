5

THE LEVITES’ FAILURE: RELIGIOUS APOSTASY
Qudges 17-18)

We now arrive at the two appendices to the book of Judges.
These two stories happened early in the history, but their place-
ment here serves to bring out the fundamental causes of the dis-
orders of the period. We reach here the climax of the message of
Judges, which is that when the fathers and mothers of Israel fail
in their tasks, all goes wrong; and when the Levites fail to show
forth the true Husband of Israel to the Bride, then the Bride
goes whoring after other husbands.

Women need attention, so Paul says, “Husbands, love your
wives as Christ loved the Church” (Eph. 5:25). It was the job of
the Levites to manifest God’s Husbandly attentions to His
Bride. Just as an ignored women may seek attention elsewhere,
so Israel sought other lovers. She was in sin, but the Levites
were in greater sin, just as Adarn bore greater responsibility for
humanity’s fall.

The Lord was to be Husband and King, but Levi failed to
make His presence manifest. Thus, as the appendices say five
times, there was no king in Israel. The first appendix shows the
consequences of this failure in the area of worship, while the
second probes the consequences in social life.

The liturgical structure of Judges 17 and 18 is all in the back-
ground. The Command/Promise from God centers in two

areas. One is the second commandment, forbidding the worship
of God through graven images. The second commandment does
not prohibit religious art, or even the placement of religious art
and symbols in the place of worship. What it forbids is bowing
down to images, using them as magical means to communicate
with and manipulate God. By extension, it forbids the erection
of any competitive place of worship, at least during the Old
Covenant. There is only one Mediator between God and man,

2719
