280 Judges

and He is the very image of God Himself, the Son. All other
mediators are false.

The other area of Command/Promise has to do with the
Levites. They were to be the priest-guardians of Israel. When
they failed in their task, chaos ensued.

Nor does this passage- give an explicit evaluation or judg-
ment from the Lord. The reason for this is that the whole
history of the period shows God’s evaluation. Because of the
failure of ‘the Levites, Israel came under judgment time and
again. This passage does move to a climax of horror, though. It
comes when we find out that the apostate Levite was none other
than the grandson of Moses himself! (Judges 18:30.} So horrible
is this fact that the Jewish scribes took the letters MSH, which
ate the Hebrew consonants in the name Moses, and changed
them to read “Manasseh” (MNSH) after the apostate king of
that name. (In Hebrew, only the consonants are written; the
vowels are “understood,” and were not written into the text until
during the Middle Ages.) But in the margin, the scribes always
gave the real name: Moses. This, by the way, is what shows that
this event happened early in the period of the judges.

The following is an outline of Judges 17-18:

I. The Establishment of a False Sanctuary (17:1-6)
IL The Establishment of a False Priesthood (17:7-13)

ILL The Danite Migration (18)
A. The Spies meet with the Levite (18:1-6)
B. The Report of the Spies (18:7-10)
C. The Danites Steal the Idols (18:11-20)
D. The Danites Drive Micah Away (18:21-26)
E. The Conquest of Laish and the Full Establishment of
an Apostate Sanctuary (18:27-31)

The Establishment of a False Sanctuary
This story is a parody of the story of the establishment of

true worship at the exodus from Egypt. Virtually every detail
found here is also found there, but here it is perverted.

17:1. Now there was a man of the hill country of Ephraim
whose name was Micah,
