2

SPIRALS OF CHASTISEMENT
(Judges 2:6 - 3:6)

This second introduction to Judges is thematic rather than
narrative in character. Here again we can see the three-fold pat-
tern, although the command/promise of the Lord is presupposed:

L The Command/Promise of the Lord (Deuteronomy 6:4-9,
20-25)
IL. The Response of Israel (Judges 2:6-13)

A. The Failure of the First Generation (2:6-10)
B. The Failure of Later Generations (2:11-13)

IIL. The Evaluation of the Lord (2:14 - 3:6)
A. The Slavery-Deliverance Cycle (2:14-19)
B. Tuming Sins into Scourges (2:20 - 3:6)

The Command/Promise of the Lord
(Deuteronomy 6:4-9, 20-25)

The basic failure of the Israelites, as set forth in this passage,
was this: They did not pass onto their children a sense of loyalty to
the Lord. Since this was the fundamental command/promise that
they violated, we should have it before us before we look at the
passage itself. First of all, then, from Deuteronomy 6:

4, Hear, O Israel! The Lor is our God; the Lorp is one!

5. And you shall love the Lorn your God with all your heart
and with all your soul and with all your might.

6. And these words, which I am commanding you today, shall
be on your heart;

7. And you shall teach them diligently to your sons and shall
talk of them when you sit in your house and when you walk by the
way and when you lie down and when you rise up.

8. And you shall bind them as a sign on your hand and they

25
