10 Judges

Blessings for the Kenites
16, And the sons of the Kenite, Moses’ father-in-law, went
up from the City of Palm Trees [Jericho] with the sons of Judah,
to the wildemess of Judah which is in the south of Arad; and
they went and lived with the people.

The Kenites were part of the Midianites, descendants of
Abraham (Gen. 25:2). Moses had married into a Kenite tribe,
one presided over by Jethro. (Taking wives from outside Israelis
condemned when it involves idolatry, but when the woman con-
verts it is a form of evangelism, picturing the incorporation of
the gentiles into the Bride. We shall see this again when we get to
Samson. In this case, Jethro was a faithful worshipper of the
God of Abraham already, so no conversion was needed.)

Moses had persuaded part of Jethro’s family to go to the
land of milk and honey with Israel (Num. 10:29-32). Since
Moses was of the tribe of Levi, and Levi had no inheritance in
Canaan, it was necessary for the Kenites to associate with
another tribe. They chose to associate with the royal tribe,
Judah. They broke camp at Jericho (the City of Palm Trees, 2
Chron. 28:15), and moved into Judahite territory, and lived with
the people of God there.

Here is a miniature picture of salvation. Those outside Israel
can either join with God’s people and be saved, or war with
God’s people and be destroyed.

Hormah

17. Then Judah went with Simeon his brother, and they
struck the Canaanites living in Zephath, and utterly destroyed it.
So the name of the city was called Hormah.

Now we see Judah making good her bargain with Simeon.
The destruction of Canaanite Zephath was total, so that the
place was called Hormah. This is not the only “Hormah,” for we
read in Numbers 21:1-3 of a place that was also “devoted to
destruction,” and as a result was called Hormah.

Hormah means “placed under the ban, totally destroyed.”
To be placed under the ban is to be devoted to death. Just as the
Nazirite was devoted to God in life (for instance, Samson,
Samuel), so the banned person or city was devoted wholly to
