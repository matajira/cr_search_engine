Oo] Judges

Sodom, but the people lacked that faith, so they were back in
Sodom. It had been the prayers of Moses that had saved Israel
from Amalek, but the people lacked those prayers, so they were
oppressed again by Amalek. It had been the convictions of
Joshua that had destroyed Jericho, but the people lacked those
convictions, so Jericho had been recaptured by the enemy, and
resettled. In all this we must remember what verse 12 says: It was
the Lord who strengthened Eglon, raising him up to chastise
Israel. As always, it is not the visible enemy with whom we have
to do, but rather with the Lord. When He is pleased with us, the
enemy will vanish.

The Savior and His Methods

15. But when the sons of Israel cried to the Lorn, the Lorp
raised up a deliverer (yasha‘) for them, Ehud the son of Gera,
the Benjamite, a left-handed man. And the sons of Israel sent
tribute by his hand to Eglon the king of Moab.

16. And Ehud made himself a sword which had two edges, a
cubit in length; and he bound it on his right thigh under his
cloak.

17. And he presented the tribute to Eglon king of Moab.
Now Eglon was a very fat man.

18. And it came about when he had finished presenting the
tribute, that he sent away the people who had carried the tribute.

19. But he himself turned back from the idols which were at
Gilgal, and said, “I have a secret message for you, O king.” And
he said, “Keep silence.” And all who attended him left him.

Ehud, whose name possibly means “Strong,” was the savior.
He was of the tribe of Benjamin. When Rachel gave birth to
Benjamin, as she died she named him Ben-Oni, “Son of My Sor-
row” (Gen. 35:18). Jacob changed his name to Ben- Jamin, “Son
of the Right Hand.” In Hebrew this is a pun, for it can mean
either “I come from my father’s right hand, the hand of author-
it y and rulership ,“ or “I am right-handed.” Benjamin, however,
seems to have been left-handed, for we find that left-handedness
became a characteristic of his descendants (Jud. 20:16).

Ehud is a true Benjamite. He is left-handed, and though his
life commences in the sorrow of persecution, he is elevated to
the right hand in becoming a ruler. In this he portrays Christ,
the True Benjamite. Our Lord was the Man of Sorrows (Is.
