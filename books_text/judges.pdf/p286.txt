272 Judges

And he awoke from his sleep and said, “I will go out as at other
times and shake myself free.” But he did not know that the Lorp
had departed from him.

21. Then the Philistine seized him and gouged out his eyes;
and they brought him down to Gaza and bound him with bronze
chains, and he was a grinder in the prison.

Samson’s wife had worn him down until he told her his rid-
die. Now Delilah also wears him down and he tells her another,
more important secret. Samson should have remembered the
earlier incident, how this nagging was a sign of betrayal. He un-
derstood it then, but now, blinded by lust, he ignores the danger
signals.

She annoyed him “to death.” This is not just an hyperbolic ex-
pression. Proverbs 2:16ff. tells us that the adulterous woman
flatters with her words, but “her house sinks down to death and
her tracks lead to the dead.” Involvement with Philistia can only
lead to death.

When Samson said “my Strength will leave me” (v. 17), he
referred to the Spirit. After his head was shorn, we read in verse
20, “the Lorp had departed from him.” Samson knew where his
strength came from. It was not magic. It was a personal gift
from the Lord. God had graciously not withdrawn it yet, in
spite of Samson’s adultery. There was no magical tie between
Samson’s strength and his hair, but there was a spiritual connec-
tion in that God gives strength to those who are dedicated to
Him, and in Samson’s case, his dedicated head was the sign of
his separation to God. In fact, Samson was already in sin, and
God was about to pull away His Strength from him, but God
chose to do so at the same time as Samson’s head was shorn, so
that the outward sign would correspond to the reality.

By this time, the five lords have given up their hopes of
defeating Samson. Delilah, anxious for the money, sends for
them to come up one more time. Once Samson falls asleep (a
picture of his spiritual state), she has his head shaved, and then
she “afflicts” him. Thus, she displays the true nature of Philistia!
The reference to Samson’s “sleeping” on Delilah’s “knees” is an
allusion to his sinful sexual relationship with her, as can be seen
from the use of “kneel” in Job 31:10. The connection between
knees and legs is obvious, and we are thus back to the harlot of
