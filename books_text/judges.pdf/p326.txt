The Destruction of Benjamin 313

serve other gods” (whom you have not known),

14. Then you shall investigate and search out and inquire
thoroughly. And if it is true and the matter established that this
abomination has been done among you,

15. You shall surely strike the inhabitants of that city with
the edge of the sword, putting it under the ban fhormab] and all
that is in it and its cattle with the edge of the sword.

16. Then you shall gather all its booty into the middle of its
open square and burn the city and all its booty with fire as a
whole burnt sacrifice to the Lorp your God; and it shall be a
ruin forever. It shall never be rebuilt.

There are several things to notice here. First, the apostates
are referred to as “sons of Belial.” It is not known exactly what
“Belial” means, but it seems to mean “worthlessness.”
Deuteronomy 13 is the first time the expression is used, and the
second time is in Judges 19 and 20. This literary connection
shows us that the narrator of Judges intended his readers to see
the sin of the men of Gibeah along the lines of what is described
here in Deuteronomy 13.

Second, the sin was apostasy from the true God. Two details
in Judges 19 bring this out. First, the city ignored the Levite, and
offered him no hospitality. Second, when the sons of Belial
attacked the Levite, they were attacking God. The crime of
Gibeah was primarily social in character, at first glance anyway,
but it had a clearly religious root. Gibeah was an apostate city,
and came under the ban of Deuteronomy 13.

We notice that the law requires that everyone in the city, in-
cluding cattle and all booty, be utterly destroyed as a whole
burnt sacrifice to God.

Before returning to Judges 20, we need also to look at
Deuteronomy 20:10-18. Here we have the laws for normal war-
fare. When a faraway nation declared war on Israel, the
Israelite army would go to that city and defeat it. In such a case
the women, children, cattle, and goods of the city became spoil
for Israel. Such was not to be the case with the Canaanite cities,
however. They were to be utterly destroyed, “in order that they
may not teach you to do according to all their detestable things
which they have done for their gods.” Clearly, the action of the
Sodomites of Gibeah is in the category just described. Gibeah
had identified itself with the Canaanite culture, so it received the
