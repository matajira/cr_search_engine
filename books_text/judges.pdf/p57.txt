Spirals of Chastisement 39

19. But it came about when the judge died, that they would
turn back and act more corruptly than their fathers, in following
other gods to serve them and bow down to them; they did not
abandon their practices or their stubborn ways.

This passage deals with basic principles, which God used not
only in dealing with Israel but also in dealing with His people of
all ages. As a chastisement for their wickedness, God sold them
(v. 14) into the hands of their enemies. God had delivered them
from bondage in Egypt, but now He sells them back into bon-
dage. A return to slavery is in view. The passage emphasizes that
it is God Who is actively at work chastising His people, and
strong language is used: “wherever they went, the hand of the
Lorp was against them for evil” (v. 15). But at the same time, it
is stressed (v. 16) that it was also the Lorp Who raised up the
judges, who saved them. The word “saved” in Hebrew is pasha’
from which we get Joshua and also Jesus. It basically means “to
put into a large, open place,” which is what Joshua had origi-
nally done, and which is what each new Joshua (each judge)
would do,

These deliverances by God did not have the desired effect of
changing the culture. Such is the depravity of the human heart
that “they turned aside quickly” from the Lord, and “played the
harlot after other gods” (v. 17). The promise of the New Cove-
nant is that such declension will not occur as often, and that
the Church will respond more favorably to God’s deliverance in
Jesus Christ (it being a greater deliverance; cf. Jer. 31:31-33).
God has given greater Power (of the Holy Spirit) to the post-
Pentecostal Church. Indeed, after the Exile, which was the low
point of universal Church history (Is. 54:7-10), the children of
God were much more faithful than they had been before. We
never hear of Baalism again; rather, the problem becomes a
loyalty to the Law divorced from the Person of God, a perverse
loyalty called Pharisaism which made obedience the way of eter-
nal salvation instead of a response to it. But we should note that
by New Testament times, the Synagogue-Church had spread all
throughout the Roman Empire and the Persian as well, an
outflow of evangelism which was not characteristic of pre-exilic
Israel. The people did respond to Ezra, Nehemiah, Zechariah,
and Haggai; as their fathers had not responded to Elijah and
