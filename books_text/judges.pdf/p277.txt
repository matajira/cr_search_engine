262 Judges

want to believe that the Spirit worked with Samson by completely
bypassing his brain (which certainly would make Samson unique
in the history of the world!), we have to say that Samson knew
what was going on, and was in moral control of the situation.

Thus, in my opinion the parallels between Samson’s youth-
ful offer of marriage and his later involvement with Delilah are
not indications that Samson was in the same moral state.
Rather, they are inversions. In the first story, Samson
dynamically moves out to the gentiles to offer them the gospel;
in the second, Samson is seduced by the sins of the gentiles and
becomes blind to the gospel. In the first story, the girl hounds
Samson into giving up a secret, but Samson is in complete con-
trol of the situation and this is but a way of testing her, with the
result that he overcomes the Philistine; in the second, Delilah
hounds Samson into giving up a secret, but Samson winds up in
her control and power, with the result that the Philistine over-
come him. Finally, we should note that in the first story the
Spirit repeatedly rushes upon Samson mightily; while in the sec-
ond the Spirit departs from him, leaving him powerless. If Sam-
son had been in sin in the first story, the Spirit would also have
departed from him then.

In conclusion we should see that this story anticipates certain
interesting characteristics of our Lord’s ministry, which revolve
around what is sometimes called “the Messianic secret .“* Anyone
familiar with the gospels can remember that Jesus on several oc-
casions enjoined people not to tell what they had seen Him do.
Also, Jesus told parables that were secrets only for the inner cir-
cle of believers (Mark 4:11-12). Repeatedly the Jews asked Jesus
to say who He was, but He spoke to them in cryptic sayings, and
gave them no sign but the sign of Jonah. It was only at the resur-
rection that it was made unmistakably clear to all men who He
was.

Unquestionably, many things are involved in the “Messianic
secret,” but the story of Samson gives us one perspective on it.
By delaying the revelation of His secret, Jesus created intense in-
terest in it. Such an interest should have led to faith, when the
Jews realized that Jesus had indeed slain the lion of Satan and
made available once again the milk and honey of the kingdom.
Just as Samson’s wife was not impressed, however, neither were
the Jews impressed with Christ’s resurrection. Samson’s wife
