216 Judges

lords were killed, destroying all five heads of that culture. True
to form, their heads were crushed under tons of rock. More-
over, the chief priests of Dagon must also have been present,
and they also got their heads crushed. Once again the number
three shows up, pointing again to the fact that Samson’s
deliverance was a “beginning,” not the full seven-fold end.
There were many more than 3000 present, for the house was full
in addition to the 3000 on the roof. It must have been a very
large Temple, to have that many people on the roof.

Was Samson’s prayer a selfish one? Hardly. Bear in mind
that this is Israel’s messiah praying. Jesus also prayed destruc-
tion down upon His enemies from the cross, as we see in Psalm
69:20ff. Samson does indeed phrase his prayer in terms of ven-
geance for his two eyes, but as we have seen, Jesus was also
blindfolded at one point, and in His prayer, He prays, “May
their eyes grow dim so that they cannot see” (Ps. 69:23). The
prayer is in terms of God’s perfect standard of justice: eye for
eye. It was Israel’s messiah they had blinded, and Samson
knows that there is a price to be paid for attacking the Lord’s
anointed.

A careful translation of verse 30 brings out that Samson’s
Spiritual Strength had returned, and that Samson used all of
this Strength in pulling down the Temple. He asked to die with
the Philistine, showing that the Messiah would have to die
Himself in order to effect the destruction of His enemies. We
also see that he killed more at this time than at any other time in
his career. (Doubtless his total number over 20 years was higher
than the number killed in his last deed, but this was the greatest
number of any particular occasion.) His greatest and most
definitive victory came with his death. In all of this, he is a pic-
ture of the coming Messiah, Jesus Christ.

if all the people were killed, who got out to tell this story to
the author of Judges? Perhaps the boy who guided Samson to
the pillars. Maybe, being a servant, he was a Hebrew, and
maybe Samson warmed him. Maybe. We don’t know. Another
question: Was Samson’s death a suicide? Yes, in one broad
sense, but compare Matthew 27:50, where Jesus “gave up the
Spirit .“ We don’t regard volunteering for a suicide mission on
the battlefield as immoral, but as an act of self-sacrifice. That is
how we should understand Samson’s death.)
