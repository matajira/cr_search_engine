Introduction xi

written these stories, and had tried to make everything come out
just so, we would have had to engage in a litile judicious fiction
(and there is nothing wrong with that, as Jesus’ parables illus-
trate), But that is not what we have here. These were real flesh
and blood people, who really lived. Their lives were so ordered
by God, however, that everything did come out just so; and the
history of their lives was written by the author in such a way as
to bring out the universal meanings, without the need to distort
a single fact.

Keys to Interpretation

Who was this author of Judges? Christians confess that God
wrote this book, ultimately, as He wrote all of the Bible. I think
the most likely candidate for human authorship is Samuel. As
we shall see, one of the major themes in Judges is that there was
no human king in Israel. The people were supposed to recognize
the Lord as their king. When they did not, chaos ensued. It is
Samuel who made the great speech against the tyranny of
human kings in 1 Samuel 8, and it is very easy to believe that he
might have been moved by God to prepare Judges as a tract for
the times,

Judges, like all the so-called “history books” of the Old
Testament, is really a prophecy. Judges is numbered among
what are called the “Former Prophets.” These books were called
prophecies because the histories they recorded were regarded as
exemplary. The histories showed God’s principles in action, and
thus formed prophetic warnings to the people. If we read
Judges merely as a set of exciting stories, we miss this.

To get at the prophetic meaning, we need to know four
“secrets” of interpreting Biblical narratives. First, we have to
take seriously the universals, as mentioned above. The first
enemy who invades Israel in the book of Judges is Cushan-of-
Double-Wickedness from Ararn-of-Double-River. This is Meso-
potamia. What is the prophecy? If the people do not live right-
eously, the enemy will come from Mesopotamia. And so it was.
First Assyria conquered Northern Israel, and later Babylon con-
quered Southern Israel, so that even the idea of a two-fold
destruction came to pass.

Along these lines, we must confess with Genesis 1:26 that
man, both individually and corporately (at various levels), is the
