102 Judges

From their courses they fought against Sisers.
21. The torrent of Kishon swept them away;
The ancient torrent, the torrent Kishon.
O my soul, march on with strength.
22. Then the horses’ hoofs beat from the dashing,
The dashing of his valiant steeds [mighty ones].

More description of the batile is given here than was given in
Judges 4. The fact that the Canaanites took no spoil is picked
up again in verses 28-30. It is included here as an ironic under-
statement: The Canaanites lost far more than booty. The loca-
tion of the battle, as mentioned already, takes up from Manas-
seh’s failure to clear out the Canaanites in Judges 1:27.

The concept of the stars fighting in heaven, and of the stars
controlling the weather (bringing rain), was common in Baal-
ism. Here Deborah asserts that the stars are part of God’s heav-
enly host, and that their (angelic) control of the weather is for
the good of Israel. Baalism is impotent. The notion that those
who trust in the Baals have the stars and the weather on their
side is a lie.

Stars in Scripture are associated with angels (lob 38:7, Is.
14:13; Rev. 12:4). Storms, at least special ones, are also associ-
ated with angels (Ezk. 1; 10; Ps. 18:9-12; 104:2-4; Ex, 19:16 with
Heb, 2:2). Because of the influence of neo-Baalism (secular hu-
manism) in our modern culture, we tend to think that God,
when He made the world, installed certain “natural laws” or
processes that work automatically and impersonally. This is a
Deistic, not a Christian, view of the world. What we call natural
or physical law is actually a rough approximate generalization
about the ordinary activity of God in governing His creation.
Matter, space, and time are created by God, and are ruled
directly and actively by Him. His rule is called “law.” God
almost always causes things to be done the same way, according
to covenant regularities (the Christian equivalent of natural
laws), which covenant regularities were established in Genesis
8:22. Science and technology are possible because God does not
change the rules, so man can confidently explore the world and
learn to work it. Such confidence, though, is always a form of
faith, faith either in Nature (Baal) and natural law, or faith in
God and in the trustworthiness of His commitment to maintain
covenant regularities.
