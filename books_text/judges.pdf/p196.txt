The Minor Judges: Drifting toward Humanistic Kingship 179
C. Jephthah’s dynastic aspirations thwarted (11:29-40)
IV. Jephthah and the Ephraimites (12:1-7)

V. Minor Judges (12:8-15)
A. Ibzan, drifting toward monarchy (12:8-10)
B. Elon, not drifting (12:11-12)
C. Abdon, drifting toward monarchy (12:13-15)

The trend toward monarchy, which began with Gideon, con-
tinues here. The story of Jephthah, which is the heart of this
section, concerns the desire to set up a dynasty. It concerns, in a
sense, the seed to come. Instead of awaiting God’s time and
God’s Seed (Jesus Christ), Jephthah is interested in establishing
a dynasty. How God (graciously) thwarts his aspirations is the
central story in this section.

Bracketing this narrative at the beginning and at the end are
brief notices conceming minor judges, whose names are not ex-
actly household words. These men seem to have no importance
whatsoever, and seem to be included simply out of a desire for
completeness (or so that the total number of judges would be
twelve, which it is). Actually, however, the theme of aggrandize-
ment and of the tendency toward tyranny is what governs the
arrangement of the text here.

It is not the case that every paragraph of Scripture can stand
by itself with a clear, discernible message. Some parts of Scrip-
ture only make sense when taken with a larger section. Such is
the case here. Taken one at a time, or even as a group, the stor-
ies of these minor judges seem to have little purpose, but taken
in a larger context, these notices are meaningful. Let us take a
look at each one, first, and then return to the larger context.

Tola

10:1 Now after Abimelech died, Tola the son of Push, the
son of Dodo, a man of Issachar, arose to save (yasha‘) Israel;
and he lived in Shamir in the hill country of Ephraim.

2. And he judged Israel 23 years. Then he died and was
buried in Shamir.

Tola means “worm,” a strange name. But “tola” can also
refer to the scarlet-colored cloth made from a dye created by
crushing worms. Such would be a robe of honor, signifying the
