100 Judges

13, Then the remnant of the nobles came down;
The people of the Lorp came down to me as warriors.
14a, From Ephraim those whose root is in Amalek;
Following you, Benjamin, with your peoples.
14b, From Machir commanders came down,
And from Zebulun those who wield the pen of the scribe.
15a, And my princes of Issachar were with Deborah; as was
Issachar, so was Barak;
Into the valley they rushed at his feet.
15b, Among the divisions of Reuben
Great resolves of heart.
16a. Why did you sit among the sheepfolds [or saddlebags],
To hear the pipings for the flocks?
16b, Among the divisions of Reuben
Great searchings of heart!
17a, Gilead remained across the Jordan;
And why did Dan stay in ships?
17b. Asher sat by the seashore, .
And remained by its landings.
18, Zebulun was a people who despised their lives even to death,
And Naphtali also, on the high places of the field.

Both nobles and commoners came to fight (v. 13). Now we
have a list of those who fought. First is Ephraim. The associa-
tion of Ephraim with the area formerly inhabited by Amalek is
unclear, though this might be a figure of speech for fierce war-
tiors. There is no supporting evidence for such a conjecture,
however. Maybe it points to the fact that God’s people were
once His enemies, though now converted by grace. Seedy
origins are no reason not to join the Lord’s army.

Benjamin, tiny as a result of the war recorded in Judges 19-
21, came along with Ephraim as part of their force. Being small
is no reason not to join the Lord’s army.

That part of Manasseh located on the Mediterranean side of
the Jordan river is meant by “Machir.” They contributed com-
manders. This was an important battle for them, because
Taanach and Megiddo (v. 19) were part of the territory they had
failed to conquer in Judges 1:27.

Zebulun contributed some scribes, in addition to soldiers.
These scribes enrolled the men, and collected the required
atonement money (Ex. 30:12ff.). Every time the army of the
Lord was mustered, the men paid each a half shekel of silver to
