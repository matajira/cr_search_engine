2 Judges

Canaan Resurgent

4:1. Then thesons of Israel again didevil inthesight of the
Lorp, after Ehud died.

2. And the Lorp sold them into the hand of Jabin king of
Canaan, who reigned in Hazer; and the commander of his army
was Sisers, who lived in Harosheth-Hagoyim [Harosheth of the
Gentiles].

3. And the sons of Israel cried to the LoRD; for he had nine
hundred iron chariots, and he oppressed the sons of Israel
severely for twenty years.

Each enemy is significant: Babylon, Amalek, Sodom, and
now the Canaanites. Here the Lord delivers Israel (selling them,
the language of enslavement) into the hand of Jabin, king of
Canaan. Irony of ironies! The Canaanites, who had been
defeated once, now rule Israel. Those who were under the curse
as “slaves of slaves” now rule over the righteous, the proper
rulers of the world (Gen. 9:25 ff.).

“Jabin” is to Canaan what “Pharaoh” is to Egypt, a name
carried by all the rulers. He reigned in Hazer. Under Joshua, an
earlier Jabin had been liquidated (Josh. 11:1-15). The defeat of
the king of Canaan had been the climax of the conquest of Ca-
naan. Thus, Jabin’s city of Hazer had been totally devoted to
the Lord, just as the first city conquered had been (11:11 ff.). The
conquest of the land, from Jericho to Hazer, had been
bracketed by “hormahs,” cities totally devoted to God by means
of sacrificial fire. Hazer and Jabin had been the head of the Ca-
naanite city-states (11: 10), and so the capital city was devoted to
the Lord for destruction. Here is the theme of the crushing of
the head (political) of the serpent. Finally, we note from this
first conquest that iron chariots had not stopped Joshua (11:4ff.;
compare Jud. 1:19), and that the victory took place in connec-
tion with water: “So Joshua and all the people of war with him
came upon them suddenly by the waters of Merom, and
attacked them” (11:7), a clue to the means God would use to
deliver Israel from Jabin a second time.

Now Hazer had been rebuilt, and a new Jabin was on the
throne. The Israelites in the north had been asleep, and the
enemy had refortified himself. Each of the stories in Judges has
a particular meaning, and this one no less. To understand this
