Abimelech: Israel’s First King 161

directed against the Lord and against His servant Abel. Abel is
God’s favorite, so it will hurt God if Cain kills Abel. Besides,
Abel has also offended Cain, by being so pious.

Expiation, propitiation — these are theological terms that
have to do with the sacrificial atonement for sin, Cain’s action is
clearly sacrificial, even though he may not consciously have con-
ceived of it as such, God treats it as sacrificial. Let us note that
Cain goes out and builds a city, a culture, based on his misdeed.
It is a culture that has at its heart not the substitutionary death
of Christ, but the murder of man to propitiate the wrath of
man, A il humanistic societies are built on the sacrificial murder
of man. The continual murder of millions of innocent people
was the foundation of National Socialism in Germany and is the
foundation of International Socialism in China and the
U.S.S.R. (For a description, see the three volumes of The
GULAG Archipelago by Aleksandr Solzhenitsyn.) And we are
not saying too much to point out the millions of aborted babies
that seem to be the chief product of our humanistic American
society.

We shall return to the theme of human sacrifice as the foun-
dation of human society when we get to Jephthah. For now, the
curious reader might consider 1 King 16:34, where a ritual
human sacrifice seems to be the foundation of the rebuilding of
Jericho. And is it not Jesus Christ, sacrificed and resurrected,
Who is the foundation stone and cornerstone of the Church, the
New Jerusalem?

Abimelech’s rule in Shechem, and the restored Canaanite
culture of Shechem, are based on the human sacrifice of the
seventy sons of Jerubbaal the Baal Fighter. We shall see that
Abimelech is definitely a man of wrath who must propitiate his
wrath whenever it is aroused. This is not his last act of human
sacrifice.

The place where Abimelech was made king is important. Ac-
cording to Joshua 24:1, 24-26, this pillar was a monument
erected by Joshua as a memorial stone to remind the people of
their covenant with the Lord. It was to remind them that the
Lord was their King. Now, in a tremendous act of perversion,
the Lord is explicitly rejected at this very spot, and a murderous
humanistic king enthroned. Here in a capsule we see the
apostasy of Israel. To get the full weight of it, read Joshua 24 all
