The Destruction of Benjamin 305

Benjamin, they may punish them for all the disgraceful acts that
they have committed in Israel.”

11, Thus all the men of Israel were gathered against the city,
united as one man.

12, Then the tribes of Israel sent men through the entire tribe
of Benjamin, saying, “What is this wickedness that has taken
place among you?

13. “Now then, deliver up the men, the sons of Belial in
Gibeah, that we may put them to death and bum away this wick-
edness from Israel.” But the sons of Benjamin would not listen
to the voice of their brothers, the sons of Israel.

We begin to see the parallels between this last story in Judges
and the first. Just as the tribes went up against the Canaanites by
lot, so now they go up against Benjamin the same way. The Ben-
jamites are the moral and spiritual Canaanites here.

The offer of peace to Benjamin, which gives them the oppor-
tunity to choose whom they will serve, comes from the laws of
war given in Deuteronomy 20:10-11. As is so often the case, the
Benjamites stood with their sinful relatives against the Lord.
How many times in the Church do we see wives standing by re-
bellious husbands, or parents standing by apostate children.
Which is thicker, blood or God’s covenant? Such was the issue
then, and such is it today. Any church that has had to excommu-
nicate a sinner has seen other people march out, fists raised, be-
cause they would not break off their relationship with the ex-
communicant,

Israel says that the wickedness must be “burned” out (v. 13).
The Hebrew verb form used here is relatively rare, and is used
for sacred sacrificial fires and for the purging out of sinners by
the death penalty. When Benjamin refuses to purge out the
thugs of Gibeah, the result is the sacrificial torching of that city
(v. 40).

Preparations and the First Battle

14, And the sons of Benjamin gathered from the cities to
Gibeah, to go out to battle against the sons of Israel.

15. And from the cities on that day the sons of Benjamin
were mustered, 26,000 men who draw the sword, besides the in-
habitants of Gibeah who were mustered 700 choice men,

16. Out of all these people 700 choice men were lefi-handed;
each one could sling a stone at a hair and not miss.
