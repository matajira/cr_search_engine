AFTERWORD

THE POLEMIC AGAINST KINGSHIP IN
JUDGES AND THE AUTHORSHIP OF JUDGES

I want finally to tie together some matters dealt with briefly
in the Introduction and throughout this study. We are now in a
betier position to assess these, which have to do with the author-
ship and immediate purpose of Judges, and with the theme of
humanistic kingship.

It has become common in recent years to find commentators
taking the last two stories in Judges as arguments in favor of
some type of central state presided over by a human king. With-
out going into who says what (because generally not very much
is said), I should simply like to take up the matter in general
terms, and summarize here in one place my arguments against
this viewpoint.

First, Israel could not have had a king during this period,
because the king had to come from Judah, and virtually all of
the tribe of Judah were disqualified at this time. Not until the
tenth generation could Judah produce a king. Now, it might be
argued on the basis of this fact that the book of Judges shows
the terrible consequences of Judah’s sin, thus: “Suppose Judah
had not sinned? Then Israel might have had a king, and then
this horrible anarchy would not have come to pass. But, Judah
did sin, and so the anarchic nightmare of a decentralized state
was brought to pass in order to show the consequences of sin.”
Such an argument, however, goes against the greatness of grace,
for in 1 Corinthians 10:13 we read that “no temptation has over-
taken you but such as is common to man; and God is faithful,
who will not allow you to be tempted beyond what you are able;
but with the temptation will provide the way of escape also, that
you may be able to endure it .“ Thus, with or without a king,
Israel did not need to sin, and did not need to experience anarchy.

Second, the story of Gideon argues against monarchy. Gid-

329
