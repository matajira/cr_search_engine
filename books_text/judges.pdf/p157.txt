140 Judges

Zeeb, and they killed Oreb at the Rock of Oreb, and they killed
Zeeb at the Wine Press of Zeeb, while they pursued Midian; and
they brought the heads of Oreb and Zeeb to Gideon from across
the Jordan.

Once the battle was clearly won, Gideon summoned
Ephraim, that proud and surly tribe. These stationed themselves
along the Jordan and slew every Midianite who tried to cross.
(See my comments on Judges 3:28.) This was near Beth-Barah,
which apparently means “House of the Ford,” and is referred to
in John 1:28 as the place where John was baptizing; that is, a
place of judgment, either unto life or unto death.’

The Ephraimites captured and killed Oreb and Zeeb, the
commanders of the Midianite army. Oreb means “Raven” and
Zeeb means “Wolf,” both beasts that indicate the character of
the Midianite enemy. The places where they were killed became
landmarks. The Rock of Oreb reminds us of the rocks in which
the Israelites had been hiding (Jud. 6:2). Now the enemy tries to
hide in a rock, to escape their doom (compare Rev. 6:15). God’s
holy humor is full of irony.

The Wine Press of Zeeb reminds us of Gideon’s threshing in
the wine press (6:11). Gideon had been hiding from the Mi-
dianites; now the Midianite leader is hiding from him (and com-
pare Is. 63:1-6). This type of irony is deliberate in Scripture, and
serves to encourage the saints.

To bring out the theology of the crushing of Satan’s head,
the text calls attention to the fact that the heads of Oreb and
Zeeb were cut off, and brought as trophies to Gideon.

Judgment: Ephraim

8:1. Then the men of Ephraim said to him, “What is this
thing you have done to us, not calling us when you went to fight
against Midian?” And they contended with him vigorously.

2. But he said to them, “What have I done now in com-
parison with you? Is not the gleaning of Ephraim better than the
vintage of Abiezer?

3. “God has given the leaders of Midian, Oreb and Zeeb,

7. Some versions of the Greek NT say “Bethany” in John 1:28. This would
be the same place, though there is no particular reason not to stick with the
traditional reading, which is “Betha-Barah.”
