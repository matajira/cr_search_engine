234 Judges

logically, Manoah simply has nothing to do with this new birth
and new beginning. When the Angel appears to the woman, she
is once again alone in the field, Manoah not with her (v. 9).
Theologically speaking, the Miracle Child is begotten of God’s
Angel in the womb of the mother, and the first Adam’s lineage is
set aside. Physically, of course, this was only true of Jesus
Christ, and physically Manoah did beget Samson. The text,
however, emphasizes that it is God’s miracle that brings this to
pass, and thus stresses the total impotence of the fallen first
Adam.

No Peace with God

15. Then Manoah said to the angel of the Lorn, “Please let
us detain you so that we may prepare a kid for you.”

16. And the angel of the Lorp said to Manoah, “Though
you detain me, I will not eat your bread, but if you prepare a
burnt offering, offer it to the Lorp .“ For Manoah did not know
that he was the angel of the LoRD.

17. And Manoah said to the angel of the Lorp, “What is
your name, so that when your words come to pass, we may
honor you?”

18. But the angel of the Lorn said to him, “Why do you ask
my name, seeing it is Wonderful?”

19. So Manoah took the kid with the grain offering and
offered it on the rock to the LoRD, and He performed wonders
while Manoah and his wife looked on.

20. For it came about when the flame went up from the altar
toward heaven, that the angel of the Lorp ascended in the flame
of the altar. When Manoah and his wife saw this, they fell on
their faces to the ground.

21. Now the angel of the Lorp appeared no more to
Manoah or his wife. Then Manoah knew that he was the angel
of the Lorp.

22. So Manoah said to his wife, “We shall surely die, for we
have seen God.”

23. But his wife said to him, “If the Lorp had desired to kill
us, He would not have accepted a bumt offering and a grain offer-
ing from our hands, nor would He have showed us all these
things, nor would He have let us hear things like this at this time.”

With the final two questions of Manoah, there is a shift of
focus from the message to the Messenger. We might have out-
