136 Judges

What an encouragement this is to Gideon! The enemy
knows his name, the name of God’s anointed messiah, and is
terrified of it ! They know that God is with Gideon, and against
them. Gideon bows in worship.

Preparations for Battle

15b. He returned to the camp of Israel and said, “Arise, for
the Lorp has given the camp of Midian into your hands.”

16. And he divided the 300 men into three heads [compan-
ies], and he put trumpets and empty pitchers into the hands of
all of them, with torches inside the pitchers.

17. And he said to them, “Look at me, and do likewise. And
behold, when I come to the outskirts of the camp, it shall come
about that just as I do, so you shall do.

18. “When I and all who are with me blow the trumpet, then
you also blow the trumpets all around the camp, and say, “For
the Lorp and for Gideon.’ “

Gideon realizes that the prediction means that the Lord has
given His enemies into Israel’s hands. He divides his 300 men
into three companies, three “heads.” These heads of Israel will
crush the heads of the enemy. These three companies will go to
three places around the camp. Each man has a trumpet slung at
his waist. Each man carries an earthenware jar in his right hand
and a burning torch, inserted into the jar, in his left hand.

They are told to imitate Gideon. We need to see Gideon here
as the messiah, the anointed deliverer of Israel. Just as we are to
imitate Jesus Christ, the ultimate Messiah, they were to imitate
Gideon, doing as he did. The words they were to shout into the
camp were significant. It is not self-centered vainglory that caused
Gideon to order that his own name be shouted along with that
of the Lord. Rather, Gideon knew from the dream that his own
name was a terror to the Midianites. Thus, it was tactically im-
portant to use it. Moreover, again we must remember that Gid-
eon was the messiah at this point in history. Thus, we may para-
phrase the shout: “For the Lorp and for His Messiah.” Surely
that is what Christians of all ages have shouted into the sleeping
camps of the enemy.
