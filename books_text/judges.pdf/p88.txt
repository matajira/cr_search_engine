70 Judges

number arrived at (by Divine superintendence) is six, to show
that the work of the judge is to destroy the old Adam, in order
to give salvation. It is Christ, the Greater Shamgar, who
destroys the sinful “six” of Adamic humanity, and raises His
people into the “seven” of fulfillment and sabbath rest.

We are told that Shamgar “saved” Israel (vasha‘ again). This
means that he gave Israel living space by driving away the
enemy. Living space meant that southern Israel could pursue
God’s work of dressing and guarding the Garden in peace. Cul-
tural advance was possible.

Things were not so well in the north, however. Zebulon and
Naphtali were always under the threat of enemy invasion. They
were effectively “Finlandized” for much of history, so that they
were a people who walked and dwelt in darkness (Is. 9:1, 2).
Jabin was oppressing them even as Shamgar worked in the
south, and the judgess Deborah had had to leave her home in
Issachar (Jud. 5:15) and move to the middle (hill) country of
Ephraim. These people in the north, however, were about to see
a great light (Is. 9:2), the deliverer Barak, whose name means
“lightning,”
