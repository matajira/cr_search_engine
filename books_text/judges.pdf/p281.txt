Samson: Blindness, Judgment, Restoration 267

Yhwh answers Samson’s prayer. The strong man cannot deliver
himself. Though the lives of the Philistine appear to be in Sam-
son’s hands, 15:15, and ultimately in 16:30, Samson’s life and
death are in the hands of Yhwh, and Yhwh alone.”? Thus, both
stories bring us in the end to the fact of human impotence, and
God’s omnipotence.

Philistia Exposed

16:1. New Samson went to Gaza and saw a harlot there, and
went in to her.

2. When it was told to the Gazites, saying, “Samson has
come here,” they surrounded the place and lay in wait for him all
night at the gate of the city. And they kept silent all night, say-
ing, “At the morning light, then we will kill him.”

3. Now Samson lay until midnight, and at midnight he arose
and took hold of the doors of the city gate and the two posts and
pulled them up along with the bars; then he put them on his
shoulders and carried them up to the top of the mountain which
is opposite Hebron.

The first verse of this chapter has the same form as the for-
mula, “Now Israel played the harlot with some false god” (Jud.
2:17; 8:27, 33; and many other places in Scripture). At the end of
each deliverance, we read that the Judge judged Israel for x
number of years, and then he died, and then Israel went whor-
ing after some other gods. The same thing is seen here. Samson
delivered Israel. Then the text says he judged for 20 years. The
next thing we read is that Samson went whoring after a Philis-
tine prostitute. Samson as the anointed judge is a picture of
Israel as a whole. His failures are theirs.

There is a literary parallel between this verse and the first
verse of chapter 14, a parallel designed to bring out irony:

14:1 And Samson went down to Timnah, and he saw a
woman in Timnah....

16:1 And Samson went to Gaza, and he saw there a woman,
a prostitute... .

In the first story, Samson acted honestly, with pure motives.
Here the seeing has a different culmination.

3. Ibid, p. 8
