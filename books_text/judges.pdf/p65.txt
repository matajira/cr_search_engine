3

OTHNIEL: BABYLON ANTICIPATED
(Judges 3:7-1)

What were the judges? They were civil rulers and deliverers
of Israel. God is concemed with all of human life and society. It
is false to try to limit His interest only to the institutional
Church, though as the sacramental body of Jesus Christ, the
Church is the foremost earthly “institution.” The judges show us
God delivering His people from His and their enemies, in partic-
ular in social and political situations. According to Scripture,
the civil magistrate bears the sword of iron (as distinct from the
Sword of the Scriptures) as a threat to evildoers. A magistrate is
a minister of God, no less than a Church officer is, but the
magistrate is a minister of God’s vengeance, while the elder is a
minister of redemption. (See Remans 13.)

The judges were civil magistrates. Their normal work was to
act as magistrates for Israel, settling disputes (Ex. 18:21 ff.).
Their special work was to act as avengers for Israel, destroying
the enemies of God. This is still the duty of the magistrate to-
day: to settle disputes in court and to prosecute defensive war-
fare against aggressors. The book of Judges focuses in on the
exceptional work of vengeance and deliverance, because this is
what is important for the purpose of revealing and foreshadow-
ing the redemptive work of Christ.

In Scripture there are two offices or official works to which a
man may be called beyond his normal capacities as worker, hus-
band, and father. These two offices are those associated with
redemption and vengeance. Those called to these offices are or-
dained for the purpose. Ordination is by the Holy Spirit, as
represented by oil. In the Old Testament, the Levites and the
kings (the house of David) were ordained regularly by oil, as a
tite installing them in their official duties. We do not find such
ritual anointing in the case of the judges, however; rather, they

47
