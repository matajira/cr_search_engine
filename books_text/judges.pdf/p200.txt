The Minor Judges: Drifting toward Humanistic Kingship 183

known as wise and responsible citizens, and they were made judges
as a result. It may be that they simply rose in rank from being
elders over tens, to being elders over 50s, 100s, and 1000s until they
became the judge of all Israel (Ex. 18:21ff.). Because they were
elderly when they came to office, there is no particular message in
the fact that they only judged for short periods of time.

The three polygamists, Jair, Ibzan, and Abdon, must have
indulged this vice prior to becoming judges over all Israel, for
Abdon (for instance) could not have produced 40 sons and 30
grandsons in eight years. What this indicates is that the leaders in
Israel were drifting into a position of seeking special honors and
privileges, and of viewing themselves as above certain provisions
in the Law, particular the prohibition against polygamy. We see
the trappings, the effects, the manifestations of kingship, but
without the name of it. The powerful were beginning to act less
like servants of the people and of the Lord, and more like an aris-
tocracy. We may contrast Boaz, in Ruth 3 and 4, with this.

The following chart summarizes the message of the minor
judges, when seen in the context of the drift toward humanistic
kingship, which is one of the major themes of the second half of
the Gideon story, and of Judges 10-12:

Gideon 70 sons people sought to make him a
king

Abimelech made himself a king

Tola contrast: no regal manifesta-
tions

Jair 30 sons 30 donkeys put sons in charge of 30 cities

Jephthah sought to establish dynasty

Tbzan 30 sons 30 daughters sought to build up importance
of family through alliances

Elon contrast: no regal manifesta-
tions

Abdon 70 “sons” 70 donkeys extended dynasty to grandsons

(The numbers 3 and 7 in Scripture, by the way, connote ful-
ness and a sense of arrival, because the third day and the seventh
day are arrival points in history; cf. Num. 19:12. Thus, there
would have been a certain sense of satisfaction, a feeling of ac-
complishment, of having arrived, if a man had 30 sons, or” even
better if he had 70. Thus, the numerology of the passage rein-
