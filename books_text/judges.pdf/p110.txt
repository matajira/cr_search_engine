92 Judges

In Canaanite mythology, one of Baal’s wives (or consorts) is
Anath. Anath is the bloody goddess. Because of her fierce love
for Baal, she delights to drown Baal’s enemies in blood. There is
a parallel between some of the Anath poems and the Song of
Deborah, and the reason for it is that Deborah presents herself
(and thus, the Church) as the true Bride of the Lord, and she
shows the Bride rejoicing in the bloody destruction of the Lord’s
enemies, Anath is, then, but a cheap and perverted copy of the
true Bride. '

Scholars have often divided the Song of Deborah into three
stanzas, but have not always agreed on where the divisions
should be placed. There may be better ways to do it, but what
seems best to me, and most useful, is the following:

Stanza 1 (vv. 2-11): Introduction: the Situation
Stanza 2 (vv. 12-22): The Hosts of the Lord and the Batile
Stanza 3 (vv. 23-31): The Aftermath

The reader will notice that I have had the Song typeset in
such a way as to bring out the Hebrew parallelism in the text.
Usually in Hebrew poetry the same thing is said two times. This
is in order to forma testimony of two witnesses to the Word of
God, and is also for liturgical reasons. When used liturgically,
the leadér in worship says the first phrase, speaking the Word of
the Lord as His representative (which is why he must be a man),
and the congregation responds with the second phrase, the
response of humanity (the Bride).

For the most part the phrases in the Song of Deborah are
very short, creating a thythmic atmosphere of sharpness and ex-
uberance when read out loud. *

1, An in-depth discussion of the Anath theme in the Song of Deborah is
provided by Stephen G. Dempster, “Mythology and History in the Song of
Deborah,” Westminster Theological Journal 41 (Fall, 1978): 33-53.

2. The translation used here is for the most part the New American Stand-
ard Version, though I have made a few modifications at certain points in order
to make clear my own understanding of the Hebrew original. There are a
number of mistakes in the King James Version, by the way. The translators of
the KJV did the best they could with the knowledge at their disposal, but since
that time there has been some progress in the study of Semitic languages, and
certain obscure phrases in the Song can be given better translations.
