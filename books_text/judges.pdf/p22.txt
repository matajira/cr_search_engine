4 Judges

Bezek, and fought against him, and they smote the Canaanites
and the Perizzites.

6. But Adoni-Bezek fled; and they pursued him and caught
him and cut off his thumbs and big toes.

7. And Adoni-Bezek said, “Seventy kings with their thumbs
and big toes cut off used to gather up scraps from under my
table; as I have done, so God has repaid me.” So they brought
him to Jerusalem and he died there.

The first victory was over Adoni-Bezek. Adoni means Lord,
so this was the Lord of Bezek. It is thought that bezek means
“lightning flash” or even “sunrise.” If so, then Adoni-Bezek was
a Jovian figure, a picture of the Satanic ruler of this age.

This points to something that is fairly obvious in this para-
graph as a whole, which is its typical/symbolic character.
Adoni-Bezek ruled over seventy kings. The number seventy is
used throughout Scripture as the number of the nations of the
world (starting with the seventy nations listed in Genesis 10).
Adoni-Bezek is here presented as a type of the world-ruler, later
portrayed as a beast in Daniel and Revelation. The initial vic-
tory is over Satan; all else is a mopping up operation.

In connection with this, 10,000 men were slain. This is prob-
ably a round number. Its symbolic significance is apparent when
we remember that ten is the number for totality in Scripture.
Ten thousand indicates a total, complete defeat and liquidation
of the forces of Adoni-Bezek.

Perfect retribution is measured out against Adoni-Bezek.
According to the Biblical principle, “an eye for an eye” (Ex.
21:22-25; Lev. 24:17-22), just as he had done to others, so it was
done to him. Adoni-Bezek is forced to confess to the justice of
this: “As I have done, so God has repaid me.” On the last day,
every tongue will confess to the justice measured out by Jesus
Christ, the greatest son of Judah. Sadly, most commentators on
Judges present this as an act of unwarranted cruelty on Judah’s
part; but the Bible teaches it in principle, and the text says that it
was an act of Divine justice. Let us beware of criticizing God!

Why chop off thumbs and big toes? Well, just try to pickup
something with your fingers alone, and try to imagine what it
would be like to try and walk without your big toe (since your
foot is basically a flexible tripod). In order to symbolize his
destruction of the dominion of the seventy kings, Adoni-Bezek
