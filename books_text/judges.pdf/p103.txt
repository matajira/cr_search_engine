Deborah: A Mother for Israel 85

of Hazer and the house of Heber the Kenite.

18, And Jael went out to meet Sisers, and said to him, “Turn
aside, my master, turn aside to me. Do not be afraid.” And he
turned aside to her into the tent, and she covered him with a rug
[blanket].

19, And he said to her, “Please give me a little water to
drink, for I am thirsty.” So she opened a container of milk and
gave him a drink; then she covered him.

20. And he said to her, “Stand in the doorway of the tent,
and it shall be if anyone comes and inquires of you, and says, ‘Is
there anyone here?’ that you shall say, ‘No.’ “

21. But Jael, Heber’s wife, took a tent peg and placed a
hammer in her hand, and went secretly to him and drove the peg
into his temple, and it went through into the ground; for he was
sound asleep and exhausted. So he died.

22. And behold, as Barak pursued Sisers, Jae! came out to
meet him and said to him, “Come, and I will show you the man
whom you are seeking.” And he entered with her, and behold
Sisers was lying dead with the tent peg in his temple.

Sisers came to the tents of Heber, and went to that particular
tent which housed Jael, Heber’s wife. (Jael in Hebrew is not
composed of the words Jah and El, but is a word meaning
“mountain goat .“) Heber was at peace with Jabin, so that there
was some kind of treaty between them. When Jael slew Sisers,
she violated that treaty, and acted in disobedience to her hus-
band. Sisers would not normally have dared to approach a
woman’s tent, since to the ancient mind there was a parallel be-
tween the house and the human body (see for instance Eccl. 12).
Togo into the tent of another man’s wife was the same thing as
adultery. Practically speaking, there was no other reason why a
man would go into a woman’s tent. Thus, Jael had to come out
and invite him in, deceiving him with the words “fear not .“
What Jael was saying, in effect, was that in view of the extraor-
dinary circumstances, her husband would understand why she
was giving refuge to another man in her tent, and normal social
conventions could be set aside.

Symbolically, however, Sisers’s invasion of Jael’s tent points
to the rape of Israel by Jabin’s army. This is a theme that is
picked up in Judges 5:30, where one of the goals of Sisers’s war
is said to be the capture of Israelite women. The enmity between
