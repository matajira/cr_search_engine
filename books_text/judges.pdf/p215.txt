Jephthah: Reaching for the Crown 199

25. “ ‘And now are you any better than Balak the son of Zip-
per, king of Moab? Did he ever strive with Israel, or did he ever
fight against them?

Jephthah’s fifth argument is a subile (not so subtle, really)
reminder that Israel had whipped the tar out of Moab back in
the days of Balak (Num. 22-25, 31). Would Ammon like the
same treatment?

26. “ ‘While Israel lived in Heshbon and its villages, and in
Aroer and its villages, and in all the cities that are on the banks
of the Arnon, 300 years, why did you not recover them within
that time?

Jephthah’s sixth argument is that 300 years have passed since
Israel allegedly stole Ammonite land. They have lived in this
place for centuries. Something should have been said about it
before now!

27. “ ‘I therefore have not sinned against you, but you are
doing me wrong by making war against me; may the Lorp, The
Judge, judge today between the sons of Israel and the sons of
Ammon,’ “

28. But the king of the sons of Ammon did not listen to the
words which Jephthah sent him.

Jephthah closes by affirming that he is not the judge. It is not
Jephthah with whom Arnmon has to do. They are warring
against The Judge, Who is YHWH, God of Israel. Jephthah’s
true faith is seen here, for he knows full well Who the real Judge
of Israel is. Also, we see from this passage that Jephthah is fully
acquainted with the book of Numbers. He knew the Bible well.
He was not an ignorant man living in an ignorant age.

Ammon rejects the gospel, thus bringing judgment upon
themselves, just as later on the Philistine will bring judgment
on themselves by rejecting the marriage offer of Samson, the
Mighty Bridegroom.

Jephthah ’s Vow and Victory

29. Now the Spirit of the Lorp came upon Jephthah, so that
he passed through Gilead and Manasseh, then he passed through
Mizpah of Gilead, and from Mizpah of Gilead he went on to the
