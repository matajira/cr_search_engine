Spirals of Chastisement 27

in, to give us the land which He had swom to our fathers.

24. “So the Lorp commanded us to observe all these
statutes, to fear the Lorp our God for our good always and for
our survival, as even today.

25. “And it will be righteousness for us if we guard, observ-
ing all this commandment before the Lorp our God, just as He
commanded us .*

Here again the thought concems what and how to teach the
children (v. 20). At this point, however, the subject matter is not
the content of the Law itself (the character of God), but rather
the reason why the Law was given (the actions of God in
history).

Phase one of God’s mighty acts: He brought us out of
Egypt. The children are especially to be taught about God’s
mighty signs and wonders, so that they will know Him to be a
God of almighty power (so that they will not fear iron chariots).
Phase two of God’s mighty acts: He brought us into the prom-
ised land, and gave us His Law as a rule of life. The children
must understand that deliverance from Egypt was not by their
own law-keeping, but by God’s powerful action on their behalf.
The Law was given after salvation as a rule of life. Nevertheless,
keeping the Law is all-important, for it shows whether or not we
fear (are afraid of and revere) God. The Law is given for our
own good and for our survival (vv. 21-24).

In light of this, there is a temporal righteousness that
comes to the believer who keeps God’s Law. The language here
is interesting. Adam was told to guard the Garden (Gen. 2:15,
“keep” is “guard”). Adam failed to do so, and was cast out, and
new guardians were appointed (Gen. 3:24). Here, as the people
are restored to a kind of Edenic garden (Canaan, a land flowing
with milk and hone y), they are again told to “guard.” The means
of proper guarding, of proper stewardship, is by observing all
God’s commandments. (This interpretation is based on a literal
reading of the Hebrew of v. 25. If we take it idiomatically, we
get “and it will be righteousness for us if we are careful to
observe all this commandment. . . .” The thought is more
general, but does not conflict with the notion of guarding the
new garden.)

So, the children must know two things: the Lord, and what
