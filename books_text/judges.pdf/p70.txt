52 Judges

A similar argument is found in some commentators to the
effect that certain judges are omitted from the list of the faithful
in Hebrews 11:32. There are a lot of other people omitted from
this list, however, such as Hezekiah, Josiah, and Zerubbabel.
Also, those included in the list number Jephthah and Samson,
surely not perfect men. The omission of Ehud, Jael, Shamgar,
Ruth, etc. only shows that their histories are not quite as strik-
ingly relevant to the essential point of Hebrews 11. The omission
says nothing about the rightness or wrongness of Ehud’s decep-
tion. If Ehud’s deception caused him to be omitted from
Hebrews 11, why is Rahab included?

The anointing of civil leaders with the Spirit is first seen in
Numbers 11:24-29. The seventy elders were civil leaders; the
religious guardians were the Levites. Civil magistrates need the
Spirit no less than Church elders. There was a time in the Chris-
tian past when civil authorities were anointed by the Church.
Even today, though it does not mean much, prayers are offered
when a new magistrate takes office.

Moreover, we must keep in mind that God is a Unity, One
and Three. We cannot have a partial relationship with God, as if
we could experience only one of His atiributes or one of His
graces without experiencing the rest. If the judges partook of
the Holy Spirit, as they indeed did, then they partook of all of
His graces, not just of certain select ones. Scripture expects us
to keep this in mind. The graces of the Spirit are enumerated in
Isaiah 11:2 as wisdom, understanding, counsel, strength, knowl-
edge, and fear of the Lord. To say that Samson, for instance,
partook of the Spirit of strength, but not of the Spirit of
wisdom and of the fear of the Lord, is absurd. Samson’s failures
were real, and they are recorded for our instruction; but these
failures were not the characteristic expression of Samson’s life.
The characteristic expression of his life is found, in the fact that
the Spirit of the Lord came upon him, and thus he partook of
the qualities listed in Isaiah 11:2. Samson’s sins were the excep-
tions, not the rule.

This anointing with the Spirit shows us that the judges are
symbols or types of Christ, Who is the final Judge (Luke 4:18;
John 3:34). The narrative histories of the judges are not just in-
spiring stories of how God saved His people in times past,
though they are that; these stories are also freighted with sym-
