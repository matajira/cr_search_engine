Spirals of Chastisement 35

not much more than studying doctrine, so the modern humanist
worships his god in the same way. We don’t see humanists bow-
ing down to their gods, but we do see them studying them, lec-
turing about them, writing books about them. And we don’t see
Christians bowing down to the Lord either, but we do see them
studying Him, preaching about Him, and writing books about
Him,

Thus, there is indeed a big difference between ancient
religions and modern ones. Ancient man primarily worshiped
his gods, while modern man primarily studies his. This is true
both of pagans and of conservative, orthodox Christians.

Once we understand this, however, we can see that the op-
position between the Truth and the Lie is the same now as it was
then. Ancient pagans worshiped Nature, while modem pagans
philosophize about Nature. The belief is the same, however: the
belief that Nature is self-creating. Similarly, ancient believers
worshiped the Creator, while modern Christians tend mainly to
philosophize about Him, but the belief is the same.

So, what was Baalism? In ‘essence it was the ascription of
power to Nature: The universe has within itself the force of life.
The world as we know it is the result of the union of the ultimate
male and female principles of the universe, which maybe called
Baal and Ashteroth (or Astartes). (A similar goddess is
Asherah, mistranslated as “groves” in the King James Version.
The difference between the two goddesses is technical, and both
were expressions of the same religious principle.) Canaanite phi-
losophers believed, of course, that these ultimate forces were
impersonal, and that their union was not sexual; but the com-
mon people preferred to think of the matter mythically. The sun
god copulated with the original mud of the world, and the
animals and man resulted. How does such a myth differ from a
more sophisticated expression of the same principle, such as can
be found in any 20th century high school science textbook?
Once, we are told, there was a vast primordial sea. Then one
day, sparked by sunlight, an organic molecule appeared, which
evolved “to become our present world. A “male” principle, sun-
light, inseminates a “female” principle, the primordial sea, and
life is born.

The Baal-Ashteroth religion understandably was intimately
concerned with fertility. The Creator God of the Bible had prom-
