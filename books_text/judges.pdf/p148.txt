Gideon: God's War Against Baal UI

should not look for miracles in the sense of signs (the Bible is
our sign, telling us how to live), we should be looking all the
time to the eternally active God to bring things to pass. There is
much that we should be asking for, except that our Baalistic phi-
losophy of process causes us to think that it is no use asking for
it. We should take everything to God in prayer.

There are things in our lives that we have gotten used to, and
we think “Well, that’s just the way things are.” In reality, how-
ever, these things we have gotten used to are the way God is do-
ing things, and God can do things differently if He wants to.
There would probably be a great deal less chronic sickness
among us if we would stop treating sickness as a process and
start treating it as the action of God, correctable by Him. 2
Chronicles 16:12 condemns Asa for looking solely to the physi-
cians rather than to God for healing. James 5:14-15 tells us the
primary thing we should do in the case of sickness (without
despising the ministries of Luke the physician).

Baalism is rampant in America today, in the classroom, in
science, in social science (how to manipulate people by manipu-
lating processes), on the right (cycles of civilization), on the left
Grresistible force of dialectical materialism), etc. We as Chris-
tians must keep reminding ourselves that God is a Person, our
relationship with Him is personal, He is personally interested in
every atom of the universe, He govems all things by His per-
sonal actions, we are surrounded by angels, we can ask and He
will answer.

When God performs these miracles, exactly according to
what had been agreed upon beforehand, Gideon knows that
God will deliver Israel. Gideon knows that God is able to doa
miraculous event (deliver Israel by the hand of Gideon), and
Gideon knows that God is willing to do it, because God has
foretold it.

Beyond this anti-Baalist philosophy of miracle, is there any
symbolic meaning to the dew on the fleece? Possibly, though we
cannot be certain. Two possibilities present themselves, in view
of the fact that dew is a frequent symbol for blessing in the Bible
(Gen. 27:28; Dt. 33:13, 28). First, it may be that the fleece repre-
sents Israel. Thus, when God blesses Israel, God dries up the na-
tions round about, and they are unable to threaten her. On the
other hand, when God dries up Israel because of her sins, God
