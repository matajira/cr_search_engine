The Levites’ Failure: Religious Apostasy 281

2, And he said to his mother, “The 1100 pieces of silver which
were taken from you, about which you uttered a curse and also
spoke it in my ears, behold the silver is with me; I took it ““ And
his mother said, “Blessed be my son by the Lorp.”

3. And he returned the 1100 pieces of silver to his mother,
and his mother said, “I wholly dedicate the silver from my hand
to the Lorp for my son to make a graven image and a molten
image; now therefore I will return it to you .“

4. So when he returned the silver to his mother, his mother
took 200 pieces of silver and gave them to the silversmith who
made it into a graven image and a molten idol, and it was in the
house of Micah.

5. And the man Micah had a house of gods and he made an
ephod and teraphim [household gods] and consecrated one of
his sons, that he might become his priest.

6. In those days there was no king in Israel; every man did
what was right in his own eyes.

Just as Samson had been betrayed for 1100 pieces of silver, so
here the Lord is betrayed. This betrayal is fundamental to every-
thing that happens. This is the third betrayal for money in
Judges (cf. Jud. 9:4). (On 1100, see my comments on p. 29
above.)

When Micah stole the silver from his mother, she cursed the
thief. She pretty well knew who had done it, so she made sure to
tell Micah about the curse (see Leviticus 5:1). Afraid, he returned
it to her, and she undid the curse by use of the formula of blessing
at the end of verse 2, We notice that Micah makes none of the re-
quired restitution demanded by Leviticus 6:1-7, which was to add
20070 to what was stolen and offer a Trespass Offering. The resti-
tution was to satisfy the person who was robbed, and the sacrifice
was to satisfy God. Micah ignores the true God.

The mother is not named, indicating that we have to look
for the essence of motherhood operative in this paragraph. The
mother led her son into idolatry, and perhaps the writer of
Judges intends to say something to us by using the word
‘mother’ six times in this paragraph, six being the number of in-
adequacy. She stands in contrast to Deborah, the true mother in
Israel. She is a picture of the Israelite parent who fails to teach
her child about the Lord, and who brings Israel into sin (as we
discussed this in chapter 2 of this study).
