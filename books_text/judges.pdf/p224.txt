208 Judges

virginity because she is now being tom from any future family
life and from any future husband. Her arms will be empty.

She takes her companions. These are the other virgins who
would have attended upon her in her wedding (Ps. 45:14). Now
they join her in mourning what will never be. She says that she
wants to “walk and go down upon the mountains .“ This is a cur-
ious phrase, and conjures in our minds an insane woman with
wild hair screaming and running through the hills. Undoubtedly
this is the wrong notion, and the action here is symbolic in char-
acter, though what it might mean is difficult to say. I should like
to take a stab at it, though. We find that men pass, but the
mountains remain, so that the mountains are spoken of as eter-
nal or everlasting in Scripture (Gen. 49:26; Dt. 33:15). The dew
of heaven is seen to drop down first upon the mountains, and
then flow down to the plains where men live (Dt. 33:28; Ps.
133 :3; Prov. 3:20). The mountain, a high place, is where God
meets with man, and the gracious influences of His peace are
brought down the mountain to the nation by His representa-
tives. This was done most dramatically by Moses, who went up
and down the mountain several times (Ex. 19, 20, 24, 34).

Corresponding to this mountain home of God is the Taber-
nacle home of God. The high priest brings the gracious influ-
ences of God’s dewy peace from the enthroned Cloud out to the
people. We find that there were women who ministered at the
door of the Tabernacle (Ex. 38:8; 1 Sam. 2:22). These women
(perhaps) were involved in passing God’s messages to other
women who came to the door of the Tabernacle to talk with
them. We find (Jud. 11:40) that this was what Jephthah’s
daughter did for the rest of her life.

Thus, if we are right in our hypothesis, there is a correspond-
ence between her ministry of bringing God’s gracious words
from His Tabemacle to other women, and her action of “com-
ing down” on the mountains of Israel. At least in terms of the
way ancient people thought, this is a valid suggestion. If so,
then Jephthah’s daughter spent time on the mountain spiritually
preparing herself for a life of ministering God’s Word to other
women, and as part of that preparation she engaged in certain
dramatic/symbolic actions that both showed forth and also
psychologically reinforced to her the nature of her ministry
(“coming down” upon the mountains).
