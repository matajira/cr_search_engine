Conquest, Compromise, Judgment, and Restoration 9

result is that this small, scholarly publisher is now under the
tules of the Thor Power Tool Decision, and must pay inventory
taxes. Had this attack on P&R not been reversed (which it was),
I doubt if they could have lasted until 1990. Many Christian
publishers have already had to shut down. At the same time as
this, however, pornography is more and more rampant in our
society. Christians definitely need to recapture the City of
Books.

The second half of the story deals not with conquest but
with occupation — faithful occupation. Caleb had given Achsah,
and through her, Othniel, a south land, which would not have
been very well watered. Achsah asks her father for water, so that
the land would be fruitful, and Caleb gives it to her. Water is im-
portant in Scripture, in that the Garden of Eden was watered by
springs, which flowed together into the river of Eden. Here we
see the Edenic principle coming to the forefront, as it does so
often in the Bible. The family property of Achsah and Othniel
becomes a miniature Garden of Eden, fruitful and well watered.
Such is the promise to every faithful man and wife. Such also is
the promise for the Bride of Christ, for we may go to our
Heavenly Father and ask for whatever we need to carry out the
wonderful tasks He has given us.

The gift of springs of water, making the ground fruitful, is
specifically called a blessing. Blessings are not only of the invisi-
ble, moral sort; they are also physical, such things as make for a
good, productive, Godly life.

Human life is created to image the life of God (Gen. 1:26f.).
Thus, we should not be surprised to see some very general,
relatively more vague, images of the gospel in the stories re-
counted in the Old Testament. When a father sets a task for a
son, or gives a gift to a daughter, this images the way God has
acted toward His Son, and toward His daughter (the Church).
While it would be pressing matters to insist on a full-blown
typology here, there is certainly some imaging going on in this
story. Caleb wins Achsah by destroying the giants, j ust as Christ
won the Church. The Father gives the bride to the faithful
Groom. Finally, the bride (Church) asks for water (the Spirit),
and this additional blessing is given as well (Pentecost).
