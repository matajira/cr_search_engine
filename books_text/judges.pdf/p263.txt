248 Judges

unclean. The text does not say this, however. We ought to
assume that this Spirit-led man used a tool to scrape the honey
out into his palm. Giving the honey to his parents meant that
the land would again become a land of milk and honey once the
Philistine lion had been slain (and compare Ps. 19:5 and 10).
Why didn’t he tell his parents? Because he was ashamed
because he had broken his vow? There is no reason to think he
broke his vow, though. We find out later on why Samson did not
tell them. Under the guidance of the Spirit, Samson recognized
the meaning of the lion’s attack. He planned to use it against the
Philistine. In order for his riddle to be a real test of his wife, no
one else might know about it. The girl alone will know, and so
he will know exactly who betrayed him, if she does betray him.

A Riddle for the Philistine

10. Then his father went down to the woman; and Samson
made a feast there, for the young men customarily did this.

11. And it came about when they saw him that they brought
30 companions to be with him.

12. Then Samson said to them, “Let me now propound a rid-
dle to you; if you will indeed tell it to me within'the seven days of
the feast, and find it out, then I will give you 30 linen wraps and
30 changes of clothes.

13. “But if you are unable to tell me, then you shall give me
30 linen wraps and 30 changes of clothes.” And they said to him,
“Propound your riddle, that we may hear it .“

14, So he said to them,

“Out of the eater came something to eat;

“And out of the strong came something sweet.”

But they could not tell the riddle in three days.

Back in those days, a truly “macho” man was not only
physically strong, he was also clever. Contests of songs and rid-
dles were as important as contests of strength. Samson seeks to
defeat (and even convert) Philistia by words before going out to
battle against them. If they had realized the threat implied in the
tiddle, that the defeat of the Philistine lion would bring sweet-
ness to Israel, and that Israel would consume those who sought
to consume them, they might have repented.

Also, riddles were keys. The Sphinx guarded his territory by
means of a riddle. If a man could not tell the riddle, the Sphinx
