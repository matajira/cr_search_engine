Gideon: God's War Against Baal 23

Lorp had spoken to him; and it came about, because he was too
afraid of his father’s household and the men of the city to do it
by day, that he did it by night.

Here again, every detail God sees fit to include in the text is
important. It is important that Gideon use the second bull. It is
important that the bull be seven years old. It is important that
the altar be built on the stronghold. It is important that the
same bull that tears down the altar of Baal be sacrificed to the
Lord. If these details were not important, they would not be in-
cluded. We are not doing justice to the sacred text of God’s
Word unless we at least try to understand how all this fits
together.

God’s command came at night. We have already mentioned
the sunrise theme in Scripture in our comments on Judges 5:31.
God’s appearances at night are tokens that glory is coming.
Even in creation, there was evening first and then morning; but in
the world under sin, night signifies the darkness of sin’s domin-
jon. God’s revelations in the Old Covenant frequently came at
night (Gen. 15, for instance). Indeed, Zechariah 1 through 6 area
series of visions that grow darker and then brighter as the night
passes toward morning. And of course, Nicodemus came to see
the Sun of Righteousness at night (John 3; Mal. 4:2), After Gid-
eon’s victory, the sun rises upon him (Judges 8:13).

God’s command came the same night as He appeared to Gid-
eon. When fellowship with God is restored, reformation must
begin immediately, and it begins at home and in the home town.
God launches a direct assault against Baal. Baal’s altar and the
carved pillar of his wife Asherah must be wrecked, and God’s
altar must be put in their place. Gideon’s household had to
change sides in the great war of history. The fact that the altar of
Baal belonged to Joash indicates that Gideon had been brought
up in a Baal-worshipping household, though doubtless the Lord
was given some lip-service as well. The change of allegiance had
to be public, and God’s altar was to be built high up on the
stronghold, where all could see it as a public confession of faith.

Joash and his people had sought fertility and prosperity for
their land by worshipping fertility gods and goddesses. The
result had been virtual starvation. If they return to the Lord, the
true Giver of life, fertility, and prosperity, things will change.
