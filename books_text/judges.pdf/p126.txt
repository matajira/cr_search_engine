108 Judges

This contrast has been developed already.

Second, there are the two storms, one at Sinai, the other at
Megiddo. Yet, we ought not to see these as two disparate events.
God’s fierce storm rages throughout all history, ever destroying
His enemies, as seen in Revelation 16:12-19. In Revelation 16:18
the storm at Ar-Mageddon that destroys Babylon is described in
language taken directly from Exodus 19:16-18. This great storm.
of history did not destroy God’s people at Sinai because they
were under the blood of the Passover Lamb. This same storm
bypasses God’s people at the battle of Megiddo, because the
people were with Barak the Levite in the sanctuary of Kedesh-
Naphtali. But as for the enemies of God, Sisers and his army,
the great storm of God destroys them.

In this great storm it is Christ Himself, the Greater Barak,
who is the Lightning Bolt. Lightning is God’s sword to render
judgment on His enemies (Dt. 32:41). It is seen within His glory
cloud (Bzk.1:4, 13, 14). Lightning bolts are His arrows (Ps.
18:14; Zech. 9:14). When Christ comes in judgment, it will be
“just as the lighting comes from the east and flashes to the
west” (Matt. 42:27).

God’s great storm of judgment rages throughout all history.
Either we are safe in the sanctuary with Christ, or we are out-
side, exposed to His wrath.

Connected to the great storm of history is the great flood of
history. That ancient torrent was created in Genesis 1:2, and out
of it came the dry land (Gen.1:9). When man’s sin reached a
climax, the ancient torrent washed clean the land of the whole
earth in the Flood. The ancient torrent withdrew to let Israel
pass at the Red Sea and again at the Jordan, but swept away
Pharaoh and his chariots. Take your children to the ocean, and
show them the hungry sea seeking to devour the land, but being
restrained, as Job 38:8, 11 teaches. It is the grace of God that
keeps us safe; it is Christ Who still calms the sea. Those who are
sprinkled with the waters of baptism will not be drowned in the
ancient torrent. Ultimately the ancient torrent flows from God
Himself, and signifies His judgment, for the voice of the Lord is
as the voice of many waters (Ezk. 1:24; 43:2; Rev. 1:15; 19:6).

Third, there are two responses to the call. We are not sur-
prised to read a rollcall of the faithful tribes who came to fight
with Barak against Sisers. Praise for good works comes easy (or
