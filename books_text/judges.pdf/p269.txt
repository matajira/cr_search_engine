254 Judges

The fourth episode in each case is the climax of the story. In
Chapter 14, we have a whole series of conversations; in Chapter
15, a whole series of actions. In both cases “three characters are
involved: Samson as protagonist, the Philistine as antagonist,
and a third party, whom the Philistine draft to enable them to
gain advantage over Samson. The third party in 14:10-20 is the
Timnite; in 15:9-19 the Judahites assume this role.”? In each
case, “the Philistine succeed at what they had wanted. The
Timnite tells them the answer to the riddle; the Judahites deliver
over Samson to them. In each episode, at the point where the
Philistine appear to have the upper hand, the spirit of Yhwh
impels Samson to go to Ashkelon, where he smites (nkh) thirty
Philistine. In Ch. 15, when Samson arrives at Lehi, the spirit
comes upon him and he smifes a thousand Philistine !"4

Theologically, the passage moves from the verbal to the
physical, from word to act. This is liturgical order, from the dec-
laration of the Word, to the sacramental act. Here, of course,
both are negative. Samson did not tell his parents or his wife his
secret, until his wife prevailed upon him. At that point, the real
contest was joined. Privy to the truth at last, how would she re-
spond? Her negative response to the messiah’s secret sealed her
own doom, and the abuse of that secret by the Philistine sealed
theirs. The negative side of the sacrament follows: a series of
visitations of judgment in the second half of the story.

This same order is seen in the book of Revelation. In the first
half of the book, we have seven trumpets proclaiming the Word
of judgment against the nations and Israel. When they do not
repent, the second half of the book shows seven chalices of anti-
sacramental judgment poured out against them.

It is important to see that Samson was not ultimately con-
cealing the truth from the Philistine or his parents. That he had
killed the lion was gospel to Israel, and also to Philistia if they
would listen. Samson created a situation in which people greatly
desired to hear the truth, and then he told it. We should not
psychologize the text here, as if Samson had been brow-beaten
by his beloved wife into revealing something he really did not

3. J. Cheryl Exum, Salat of §: etry and Balance in the Samson
Saga,” Journal for the Study of the Testarvent 49 (1981):17. I am indebted
to Exum for this entire schema.

4, Ibid., p. 18.
