16

THE LEVITES’ FAILURE: MORAL DEPRAVITY
(Judges 19)

The second appendix to Judges has three sections. The first
(Jud. 19) is a true horror story, very carefully and dramatically
written, delineating the rape and murder of an Israelite woman
by a gang of thugs in a Benjamite town. The second part (Jud.
20) reveals the astonishing fact that the entire tribe of Benjamin
chose to stand with this gang, rather than punish them, and as a
result all Israel went to war against Benjamin. The third part
(Jud. 21) concerns the fact that the tribe of Benjamin was virtu-
ally wiped out, and the problem of rebuilding the tribe.

As was the case with the first appendix, the various com-
mands and promises of the Lord that are involved in this nar-
tative are not expressed here, but are found in the laws God
gave Israel at Sinai. We shall examine them as we come to the
sections they apply to. Also, God’s evaluation of these incidents
is not expressed as such, but events fall out in such a way as to
make that evaluation obvious.

Turning now to Judges 19, we find here a very carefully con-
structed narrative. If we read it as if we had never before heard
the story, we find a steady rise in tension in the way the writer
has set it out. It is set out as a nightmare. The Levite delays on
his journey until late in the day. As the sun goes down, we
become uneasy about what may happen. As the scene grows
darker, our sense of foreboding increases. No one invites the
Levite and his family in to spend the night. The old man who
does finally take them in urges them not to spend the night in the
square. Sure enough, a gang of Sodomites surrounds the house
at which they stay. And then comes the shocker: The Levite
kicks his wife out of the house for them to rape and murder. As
the day begins to break, the Sodomites leave her alone, and she
crawls back to the door. As the sun rises, she dies.

291
