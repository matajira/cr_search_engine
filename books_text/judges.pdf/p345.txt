332 Judges

Danite migration? Should aking have prevented it? Could any
Israelite king, being a constitutional and not an absolute
monarch, have prevented it? I doubt it.

How about the sin at Gibeah? Again, I don’t see how any
constitutional monarchy could have prevented this from hap-
pening. Well, then, it maybe said that a king could have taken
steps to punish it. To which I reply that the twelve tribes seem to
have done a pretty thorough job of punishing Benjamin without
the need of a king, and under the guidance of the True King via
His high priest, Phineas. How does this last story show that a
human king is needed to avenge wrongs? If anything, it shows
the effectiveness of the republican system.

Finally, the end of the period of the judges, which comes in 1
Samuel 8, is the people’s demand for the king who will be like
the kings of the nations. This is clearly and explicitly seen as a
rejection of the Lord’s kingship. It is hardly credible for an or-
thodox interpreter to assert that the Bible argues for human
kingship in one place, and condemns it in another.

But, that leads us to a question of how there could have been
any Davidic kings at all. Is it absolutely and positively wrong at
all times and seasons to have a human king? Clearly not. What
is required, however, is that such a human king be a viceroy to
the Divine king. The period of the judges established clearly the
primacy of God as King. Only when this was clearly and un-
mistakably established was it possible for a man to be placed at
the head of the nation as a viceroy to God over the state.
Human kingship must always be subordinate to Divine
kingship. In the providence of God (the disqualification of
Judah during this period), this was made plain by the period of
the judges.

The book of Judges does provide an important theological
canon for the interpretation of the books of Samuel and Kings.
Whenever Israel sinned, they were sold into slavery to a foreign
nation and oppressed until they repented and God raised up a
deliverer. The only time this did not happen was after the death
of Gideon when, as we saw, Israel was sold into the hands of a
king from her own midst. Later on in history, after the establish-
ment of a permanent monarchy in Jerusalem, God no longer
brought in foreign oppressors upon the people when they sinned.
Instead, He gave them bad kings. The story of Abimelech in the
