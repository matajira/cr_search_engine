216 Judges

only God’s judge, but also the place of judgment. Theirs is a
total revolt against the Divine order.

Jephthah says that he did call them, and that they did not
come. We see here just a bit about the actual battle, in that appar-
ently Jephthah was hard pressed at one point. Ephraim failed to
come when summoned. It is interesting to note that these men,
who present such a brave and threatening front now that the
batile is over, apparently were cowards when it came to the real
fighting. God had said that Israel was to learn to fight (Jud. 3:2),
and to fight by faith. Those who did not trust God and respect
His messiah not only refused to fight, but did not have the
courage to fight either. But like cowards and bullies of all ages,
Ephraim is now ready to fight.

Jephthah informs them that the battle was not his, but the
Lord’s. It was the Lord Who had delivered Ammon into his
hand, says Jephthah. Jephthah thus admonishes them, and in so
doing preaches the gospel to them, giving them an opportunity
to repent. He tells them that when they complain about the vic-
tory, they are complaining about the Lord. Rather, they should
give thanks for the deliverance.

Ephrairn, however, does not repent. Instead they go out of
their way to insult the Lord’s anointed and his men. Pointing to
Jephthah’s band, they call them and all Gilead “fugitives from
Ephraim.” They say that the Gileadites are fugitives from
justice, criminals. Ephraim says that they are justified in attack-
ing Gilead, since they are just a bunch of criminals. Clearly,
Ephraim struck the first blow, because they crossed the Jordan
to attack Jephthah, and in verses 5 and 6 we see them trying to
get back.

What we see here is unfortunately all too common in daily
life and in the Church. After the victory against the real enemy,
the righteous have to face complaining and insurrection from
within the Church itself. There is always something, it seems, to
criticize, and those offering the criticism are always people who
could not be bothered with the real battle. Did the Church
picket an abortion chamber? Did the clinic shut down? Wonder-
ful, but now be prepared for a revolt in the Church itself. The
people who were too busy to fight the Lord’s battle will now, to
justify themselves, attack the leadership in the Church. Be
prepared, as Jephthah was, to deal severely with such people,
