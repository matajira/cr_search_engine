162 Judges

the way through, and then consider what is done here.

Also, the Tabemacle of the Lord had formerly been pitched
next to this oak and pillar (Josh. 24:26). Unquestionably this
site is now the location of the house of Baal-Berith (Jud. 9:4).
Again we see the substitution that has been made.

Why did God allow Gideon’s 70 sons to be killed? Because
they had sinned against Him. They died as a result of their
father’s sin, for there would have been no Abimelech had Gid-
eon lived faithfully with one wife, and no tendency toward king-
ship if Gideon had effectively cut it off; but they died, as well, as
a result of their own sins, for Judges 8:27 says that Gideon’s
ephod became a snare to himself “and his household.” The 70
sons were compromised, just as Gideon was. They had fallen,
along with their father. This fits the basic Biblical pattern we set
out in the introduction to this study: re-creation, then fall, de-
cline, and judgment. The re-creation was the victory of Gideon
over Baalism in his house and the consequent cleansing of the
land, reestablishing Eden. The fall was the creation of the false
ephod. The punishment for man’s fall is death, and so it came to
pass here.

Yet, these 70 sons were doubtless much better men than their
half-brother Abimelech. Had they lived, and exercised influ-
ence, things would have been better for Israel. All Israel, how-
ever, had fallen into sin. Thus, the deaths of the 70 sons were, in
the providence of God, part of His judgment against Israel.
God gave Israel over into the hands of the worst of the lot.

Jotharn is here called the “youngest” of Gideon’s sons. This
connects with the theme established in Genesis of the younger
brother replacing and redeeming the older brother, the second
Adam replacing and redeeming the first Adarn. Here it is not re-
demption but vengeance that is in view. By pronouncing this
curse, Jotham avenges his other brothers. Throughout the Old
Testament, the fathers and older brothers sin and die, and
younger sons rise to replace them. All of this points to Christ,
the Redeemer and Avenger of His “older brothers,” those dead
in Adam.

The sins of Gideon and his sons brought this judgment upon
them, but the one who slew the sons was not acting as God’s
avenger, and thus was himself judged. This same pattern is seen
when God brings in the enemy to punish His people, and then
