Jephthah: Reaching for the Cro wn 213

Thus, the sealed gates of the city correspond to the sealed
status of the virgin. The gates of the city are opened to receive
the king and lord of the city, just as the virgin gives herself to her
lord on the wedding night.

Just as the serving women at the door of the Tabemacle gen-
erally signified the fruitfulness of God’s kingdom, so also the
special presence of virgins at the doorway of the Tabernacle was
a symbol of the sealed impregnability of God’s holy kingdom.
When God’s house was kept pure from moral defilement, a sealed
virgin, then also the land of Israel would be protected from
assault.’ If God’s house were defiled, or left unprotected due to
the failure of the Levite guardians to shield her, then so also the
nation would be unprotected.

When we see this we get a new dimension on what is found
in 1 Samuel 2:22, “Now Eli was very old; and he heard all that
his sons were doing to all Israel, and how they lay with the
women who served at the doorway of the tent of meeting.” Not
only was this an act of moral outrage, sure to bring the curse of
God; it was also a picture of how the priestly guardians of Israel
were failing to protect the virginal integrity of God’s holy bride.
They themselves were “penetrating the veil,” and leaving the
“gates of the city” wide open to attack. As a result, Israel was
defenseless against the Philistine, who ravaged the land and
even took the Ark into captivity, and who killed Eli’s sons (1
Sama, 4).

Thus, the fourth aspect of the ministry of Jephthah’s
daughter was this: She was an abiding sign to Israel that their
protection from the enemy came from their moral purity and
faithful virginity, as they awaited the coming true King. If they
feared attack, if they feared the rape of their cities and land,
they should act to protect the house of God, not to build up
their own. If they would guard the most holy things, they would
be safe, but if they looked to politics and the might of arms to
save them, they would be destroyed.

Conclusion

Jephthah had hoped to build his own house and dynasty.
This was not for purely selfish motives, but rather because like
all of the rest of Israel, he had come to believe that a strong visi-
