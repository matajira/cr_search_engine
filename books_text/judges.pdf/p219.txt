Jephthah: Reaching for the Crown 203

Jerusalem. The Church is built upon His substitutionary death.

There is more to all this, however, having to do with the
basic victory-housebuilding pattern in the Bible. First, a battle is
fought against the enemy. After the enemy is defeated, the
triumphant king returns and builds a house. The spoils of the
enemy are used to build this house, but the death of the first-
born is also required. The whole book of Exodus follows this
outline. Egypt is defeated, and its spoils are used to build God’s
house, and we notice that abundant blood sacrifices are made in
connection with the building of that house, in addition to the
Passover sacrifice which underlay it all and which substituted
for the death of the firstborn. Similarly, it is when David was
finally completely victorious, so that “the Lorp had given him
rest on every side from all his enemies,” that he moved to build
the Temple (2 Sam. 7:1ff.). This, of course, reaches its fulfill-
ment in the work of Christ, whose defeat of Satan was simultan-
eously a foundation-sacrifice, resulting in the creation of His
new house on Pentecost.

This is precisely the way Jephthah is thinking in this passage.
He wants to establish his dynasty. He has won the victory, with
God’s help, and now he offers to sacrifice his firstborn, the first
person “bom” from the doorway of his house after the battle.
He vows to sacrifice this person to the Lord, to perpetual Taber-
nacle service, in exchange for the Lord’s building of his house.

In itself this was not an unreasonable request. Notice .in 2
Samuel 7 that David wants to build God’s house, but God says
that this must wait for Solomon, God goes on, however, to
guarantee David a perpetual dynasty, to build David’s house (2
Sam. 7:12ff.). Why didn’t God do this for Jephthah? Because
Jephthah was not the one God had in mind, and Jephthah
should have known it. He was out on three counts. First, he was
a bastard. Second, he was not of the tribe of Judah. Third, God
had not chosen him to be a king, but to be a judge.

Thus, God acts to frustrate Jephthah’s design. Yes,
Jephthah will indeed consecrate to Tabernacle service the
firstborn of his house, but it will be his only daughter, and that
will end his dynastic aspirations.

(We might summarize here the parallels between Jephthah
and David. Both were exiled. Both gathered impoverished men
around them. Both harassed the enemy for years. Both defeated
