Spirals of Chastisement 37

ism” aspect). There used to be such a thing as “Christian hu-
manism,” which taught that man was lord of nature, but only in
submission to God. This is the true doctrine, although the term
“humanism” has come to be offensive to Christians, so we no
longer speak of “Christian humanism.”

So, for the ancient Baalist to bow before his idol was not an
act of submission, but an act of stimulation. What he believed
was the same thing modern secular humanists believe: that man
is the lord of Nature, and that there is no Creator God to whom
man is responsible.

Now of course, the Israelite children did not deny the exist-
ence of the Lord altogether. He had His place as well, perhaps
as the superintendent of the whole overall process. Israelite
thinkers began to look at Genesis 1 with new eyes. What did this
passage mean to express, they might ask, in its poetic frame-
work? Surely it is not to be taken literally. We should realize that
God is at work through evolution (that is, through Baal and
Ashteroth). God gave to Nature certain intrinsic powers, and
these powers are evolving and developing. Nature is a real,
though lesser, power. We must respect Nature. Look at all the
bounties Nature has given us. The Canaanites have understood
this better than we have, although they do not see clearly that
God is at work in Nature as well. The Canaanites are, on the
whole, not very sophisticated about it — sacrificing children to
stimulate Nature, for instance — although there is more to be
said for the Canaanite philosophers. We shouldn’t stop our ears
to what these people are saying to us. Remember: We are to
spoil the Egyptians of their goods, and surely Canaanite science
and philosophy are part of the spoils.

So they doubtless thought. Baalism was evolution, the belief
that Nature was the author of all life. Israel was sucked into
theistic evolution first, and then later on into full Baalism. (By
the way, spoiling the Egyptians means taking their fruits, not
their philosophical roots.)

The details of the Baal cult are not of much importance to us
now. It is the underlying philosophy of Baalism which is regnant
in American education and life today, and which is taught in the
science departments of almost all Christian colleges today, and
not just in science departments either. Scripture teaches that
God sustains life directly, not indirectly. There is no such thing
