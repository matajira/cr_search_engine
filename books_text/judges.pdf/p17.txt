XX Judges

Theology of Judges

In terms of its “theology proper,” the book of Judges pres-
ents God almost exclusively in two aspects. The first is as the
LoRD. God told Moses in Exodus 6:3, “I appeared to Abraham,
Isaac, and Jacob, as God Almighty, but by My name, LoRD, I
did not make Myself known to them.” This does not mean that
the patriarchs did not use the name YHWH or LoRD, but that
God had not made clear to them its meaning. God appeared to
the patriarchs as God Almighty, the God Who creates covenants
and makes promises. At the exodus, God appeared to Israel as
the Lorp, the God Who continues (“establishes”) His covenant
and who keeps His promises. Exodus 6:6-8 gives a detailed ex-
position of the meaning of the name LoRD: The Lorn is the one
who brings His people out of bondage (v. 6), who marries them
(v. 7), and who gives them the land promised to them (v. 8).

The name LoRD, then, has to do with God’s faithfulness in
the face of man’s faithlessness. It has to do with the land God
promised, and the conquest of that land. It has to do with God’s
marriage to Israel. It has to do with bondage and deliverance.
These are all major themes in Judges, and this is why “LoRD” is
the name for God used here. Thus, the book of Judges as a
whole is a large-scale exposition of the meaning of the name
Lorn.

The other term used to refer to God in Judges is “Angel of
the Lorp.” According to Exodus 24:20ff., the Angel is the one
who goes before the people, as Captain of the Lorp’s host, to
lead them into the land. The Angel of the LoRD, thus, appeared
to Joshua at the beginning of the conquest (Joshua 5:13 ff.). In
Judges, God manifests Himself as Angel when He judges the
people at Gilgal for their faithlessness in the conquest (Jud.
2:1-4), when He appears to Gideon to summon him to war for the
land (Jud. 6:11-22), and when he appears to the wife of Manoah
to announce the birth of Samson the deliverer (Jud. 13:3-21),

This material was originally presented to the Adult Sunday
School Class at St. Paul Presbyterian Church, Jackson, Missis-
sippi, in the summer and fall of 1978, while I was a student at
