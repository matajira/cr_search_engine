302 Judges

the Covenant, and meant that the land was torn up. To sym-
bolize this, the Levite sent the pieces of this woman’s body to
the twelve tribes of Israel. This dead woman now symbolized
the nation, hacked to bits by sin. Action to restore order was
needed. The Levite, who had proven no protector, at least
proved to be an avenger.
