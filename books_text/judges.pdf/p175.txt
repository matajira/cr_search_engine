158 Judges

three scattered appearances, its usage is limited to this chapter,
and it appears thirteen times here. The rest of the book of
Judges uses the words such as men, princes, elders, and judges
to denote leaders of a city. The point is, then, that in Shechem
we have a lingering outpost of Canaanite culture, baalistic in
character. The leaders, followers of the idol Bard, are called

In the light of this it is even more important to note that
Abimelech refers to his father not as “Gideon” but as “Jerub-
baal,” the Baal Fighter. He suggests that the Shechemites must
choose between him and the seventy sons of Jerubbaal the Baal
Fighter. As recorded in verse 2, he presents four arguments,
subtly:

1. “Centralized rule by one man is preferable to decentralized
tule by seventy men.” The true Godly system is one God, but
many diversified human rulers. The pagan system is one statist
tule, but as many idols and gods as you wish.

2. “Jerubbaal’s seventy sons will become rulers.” There is no
hard evidence that they had such aspirations, but Abimelech
suggests they do.

3. “You Shechemites are worshipers of Baal-Berith. Do
you want a family of Baal Fighters ruling over you?”

4. “I, on the other hand, am from your hometown. I am
related to you by blood. I understand and sympathize with your
situation. I am a worshipper of Baal-Berith just as you are.”
And from what we can see in verse 3, it was this last argument
that was most effective.

4. And they gave him 70 pieces of silver from the house of
Baal-Berith with which Abimelech hired worthless and reckless
fellows, and they followed him.

5. Then he went to his father’s house at Ophrah, and killed
his brothers the sons of Jerubbaal, 70 men, on one stone. But
Jotham the youngest son of Jerubbaal was left, for he hid him-
self,

6. And all the men of Shechem and all Beth-Millo assembled
together, and they went and made Abimelech king, by the oak of
the pillar which was in Shechem.

The destruction of the house of Gideon (Jerubbaal) was
financed by the temple of Baal-Berith. Thus, we must see this as
