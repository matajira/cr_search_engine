206 Judges

Jephthah’s mourning is cast in terms of the ripping of his
daughter from him, and the rending of his house. There is no
apparent joy in this for him.

He says she has brought him very low. The Hebrew word is
actually “caused me to bow down.” This word almost always is
used for the bowing down of an enemy in humiliation or in
death, though it is also used for acts of obeisance. Jephthah
states that he is being forced to bow the knee to God’s purposes,
and forsake his own. This is an act of confession, of yielding to
what God now makes clear, that he is to have no abiding house.
Also, Jephthah is “troubled.” Clearly, then, Jephthah has met
with a reversal of his expectations here.

Why didn’t Jephthah substitute a money payment for his
vow? These monetary substitutes are set out in Leviticus 27:1-8.
If a man dedicated a man or a woman to the Lord, such persons
could not be accepted as such because only Levites were permit-
ted into the courts to serve at the Lord’s house. Accordingly, a
monetary substitute was required. A woman was to be redeemed
for 30 shekels of silver (Lev. 27:4). Why didn’t Jephthah do
this? The few commentators who have addressed this particular
question have noted that there seems to be no explanation for it,
unless we assume that when Jephthah made his vow he mentally
excluded this “easy out .“ I think that the answer is to be found
in another direction.

Leviticus 27:28-29 says, “Nevertheless, any proscribed
[devoted] thing which a man sets apart [devotes or banishes] to
the Lorp out of ail that he has, of manor animal or of the fields
of his own property, shall not be sold or redeemed. Every pro-
scribed [devoted] thing is most holy to the LoRD. No proscribed
[devoted] person who may have been set apart [devoted or ban-
ished] among men shall be ransomed; he shall surely be put to
death.” The word translated “devoted” in these verses is the
same as the word Hormah, which we studied in chapter one. We
found that a “Hormah” was a site devoted to destruction, fired
from God’s altar, and called a whole bumt sacrifice (Dt. 13:16).
Thus, to “devote” something to God is the same as to offer it as
a whole burnt sacrifice. To set something apart as holy was one
thing, and such objects might be redeemed; but to set something
apart as devoted was something else, here called “most holy,”
and such could not be redeemed. Since Jephthah vowed to offer
