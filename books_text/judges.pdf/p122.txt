104 Judges

Meroz must have been in the immediate area of the battle
and feared reprisals should the Canaanites win. For their lack of
faith and commitment they were cursed. We are not told if the
Israelites destroyed the town, although the command probably
implied such action. It is the angel of the Lord, the captain of
the Lord’s host, who gave this command. The curse on Meroz
for its refusal to aid Israel was effective — no one today knows
where Meroz was located! The curse on Meroz is to be con-
trasted with the following verse, which blesses Jael for her ac-
tion in the same circumstances.

24. Most blessed of women is Jacl, the wife of Heber the Kenite;
Most blessed is she of women in the tent.
25a. He asked for water;
She gave him milk.
25b. In a magnificent bowl,
She brought him curds [buttermilk; yogurt].
26a. She reached out her hand for the tent peg,
And her right hand for the workman’s hammer.
26b. Then she struck Sisers, she smashed his head;
And she shattered and pierced his temple.
27a. Between her feet he bowed, he fell, he lay.
Between her feet he bowed, he fell.
27b. Where he bowed,
There he fell devastated.

The picture given in Genesis 3:15 is of the serpent’s head
crushed by the foot of the seed. Thus, the attention called to
Jael’s feet again causes us to see the Messianic aspects of this
event. Sisers’s humiliation is stressed in his bowing at her feet.
He was already asleep. We can only assume that in his death
spasm his body curled up into a bowed position.

We mentioned in chapter 4 of this study that “feet” can refer
to private parts, and in chapter 5 we showed how Sisers’s enter-
ing Jael’s tent also has sexual overtones. With this in mind, we
are in a position to see an added dimension in these verses,
which would not have been lost on the Song’s original hearers.
It is as if Sisers had been struck down in the process of trying to
carry out a rape. In fact, the first phrase of verse 27a could be a
graphic description of rape: “Between her feet he bowed, he fell;
he lay.” This is particularly evident in that the verb “lay” is the
