Abimetech: Israel’s First King 157

cretistic (combination) god composed of elements from Baalism
and the true faith. Its champion will prove to be the halfbreed
Abimelech. Its center will be the mixed town of Shechem. Syn-
cretism, mixtures of faiths, will be a concern of Judges 9. A
modern example of Baal-Berith religion is Mormonism. The es-
sence of the Mormon religion is fertility cult belief, with all hu-
manity descended from Mr. and Mrs. God, and multiple mar-
tiages (to bring forth many spirit children) the goal. Yet, this
modern Baalistic cult uses the language of the Bible, speaks of
Christ, the ten commandments, and so forth.

Israel forsook the Lord and His anointed one, whose name
was Baal Fighter. Being Baal worshipers, they did not like a
family of Baal Fighters. This is why the name Jerubbaal is used
in verse 35. Instead of selling them into the hands of a foreign
power, the Lord gave them over to Abimelech. The story of his
disastrous three-year rule over Israel is the “oppression” that
corresponds to the invasions described in the other stories in
Judges. They were worshipping a half-breed god, so the Lord
gave them into the hands of a half-breed man. They wanted a
humanistic king, so they got one. As always, God punished His
people by giving them what they wanted.

The King Enthroned—on Human Sacrifices

9:1. And Abimelech the son of Jerubbaal went to Shechem
to his mother’s brothers, and spoke to them and to the whole
clan of the household of his mother’s father, saying,

2. “Speak now in the hearing of all the leaders {baals] of
Shechem, ‘Which is better for you, that 70 men, all sons of Jer-
ubbaal, rule over you, or that one man rule over you? Also,
remember that I am your bone and your flesh.’ “

3. And his mother’s brethren spoke all these words on his
behalf in the hearing of all the leaders of Shechem; and their
hearts inclined after Abimelech, for they said, “He is our
brother.”

Abimelech spoke to his mother’s relatives, and they spoke
on his behalf to the rulers of Shechem. The rulers are called
“baals,” or lords. This is not an uncommon designation for
leaders and prominent men in Scripture, and usually it is neutral
in connotation. In Judges, however, it is not neutral. Except for
