178 Judges

English, !

Bucer spent the last few years of his life in England, where
his republican views were favored by the Puritans. Meanwhile,
Bucer’s close friend Peter Martyr Vermigli had also lectured
through Judges, and his lectures had been translated into
English and published in 1564. These made quite an impact, and
also constituted a strong attack on political absolutism. Richard
Rogers’s 1615 commentary on Judges notes especially Vermigli’s
work as its foundation. ?

Thus, from Bucer and Calvin on the one hand, and from
Vermigli on the other, a strong belief in limited government
based on Divine law, already present in the British world, was
reinforced. The Puritans readily received this heritage, and
made the book of Judges their own in the years of the English
Civil War. Later on, their descendants in America found in the
thirteen Israelite republics a model for their own thirteen states,
loosely united under one Constitution, but with no absolute
king, and a strictly limited federal government.

A full study of the impact of the book of Judges on Protes-
tant political thought needs to be written. I have been able only
to sketch it here, as an introduction to this chapter on the minor
judges.

Why mention Christian political thought here? Because to a
great extent it pertains to the section of Judges that we now take
up, which is chapters 10 through 12. The following is an outline
of this section:

I. Minor Judges (10:1-5)
A. Tola, not drifting toward monarchy (10:1-2)
B. Jair, drifting toward monarchy (10:3-5)

IL The Sin of Israel and the Lord’s Anger (10:6-16)
A. Northem tribes sold to Ammon.
B. Southern tribes sold to Philistia.

If, Jephthah and the Ammonites (10:17 - 11:40)
A, Jephthah’s halfbreed heritage (11:1-11)
B. Jephthah’s warning to Ammon (1J:12-28)

1, Geneva Ministries, 708 Hamvasy Lane, Tyler, TX 75701, has reprinted
some of the Deuteronomy sermons in modern English, however. A subscrip-
tion to these is available for a contribution.

2. Available in reprint form from Banner of Truth Publications (1983).
