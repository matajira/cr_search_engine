18

THE SALVATION OF BENJAMIN
(Judges 2D

The story before us is generally seen by commentators as an
example of the moral chaos that existed in Israel from the
earliest times until the reformation under David. Almost univer-
sally, Christian commentators (I exclude liberals, who see in this
story just an interesting folk tale) use verse 25, which says that
“everyone did what was right in his own eyes,” to provide a
moral evaluation of the entire final three chapters of Judges.
They contend that Israel went into excess of vengeance in
destroying all of Benjamin, that Israel’s vow not to permit their
daughters to marry Benjamin was an evil one, that the destruc-
tion of Jabesh-Gilead was unwarranted, and that the capture of
the daughters of Shiloh was a rape.

I believe that this interpretation misses the point. It is the
burden of this chapter to draw quite a different interpretation
from the record of the events. For reasons that seem sufficient to
me, and that I shall attempt to make clear to you, the reader, I
believe that the actions of Israel as recorded here were each right
and proper.

In the preceding chapter, I pointed out that this final section
of Judges corresponds in certain ways to the first chapter of the
book. Benjamin identifies itself with the Canaanites, in its moral
and spiritual behavior. As a result, Benjamin comes under the
condemnation of Canaan. When this is understood clearly, the
events of Judges 21 make more sense.

When Lot lifted up his eyes and considered the circle of the
Jordan, the heart of the land of Canaan, it was like the garden
of Eden in appearance (Gen.13:10). At this time, the iniquity of
the Canaanites at large was not yet full, and God was not ready
to judge them fully (Gen.15:16). There were, however, four
cities in the heart of Canaan, in the circle of the Jordan, that

315
