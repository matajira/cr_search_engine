The Song of Deborah 101

atone for the blood spilled in war, which money went to the
upkeep of the Tabernacle.*

Issachar was Deborah’s tribe, as verse 15 makes clear (“my
princes”). They were right there with Deborah, and with Barak.
As Barak led the attack, they were right behind him. They fol-
lowed the Lord’s anointed ones.

Now Deborah begins pointedly, and even sarcastically, to
ridicule the tribes who did not come to fight. Reuben, the
first-bom of Jacob, should have had the preeminence, but be-
cause he was unstable as water, doubleminded, he lost his birth-
right (Gen. 49:3-4). Reuben’s magnanimous resolutions became
empty deliberations. The first couplet and the third are identi-
cal, save that the resolves of Reuben have disintegrated into
searchings. They could not be stirred from lethargy. Brave talk,
when not followed up with brave deeds, makes a man an object
of scorn. The other tribes who failed to show up at least did not
make bold promises of support.

Gilead is trans-Jordanic Manasseh and Gad. They, with Dan
and Asher, were in the vicinity and should have showed con-
cem, but they were too busy with their day-to-day affairs to be
bothered with exterminating Canaanites. The Canaanite domi-
nation did not bother them very much, since they were not
peasants; so they did not come to help. Their coldness and com-
promise is stingingly recorded for all time.

Judah and Simeon are not included in this roll call because
they dwelt so far in the south that their participation was not ex-
pected. Also, they were busy, under Shamgar, with Philistine.
Levi is also not included, since it was not a political tribe but
was scattered throughout Israel; though of course, Barak was a
Levite.

Then Deborah returns to heap especial praise on the two
tribes who did the most: Zebulun and Naphtali.

19a. The kings came and fought;
Then fought the kings of Canaan

19b. At Taanach near the waters of Megiddo;
They took no plunder in silver.

20. The stars fought from heaven,

4, [have discussed this at length in my book, The Law of the Covenant
(Tyler, TX: Institute for Christian Economics, 1984), pp. 225f.
