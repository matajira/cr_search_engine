40 Judges

Isaiah and Jeremiah.

The prophecies of the Old Testament, and of Remans 11, in-
dicate that the Church will gradually go from strength to
strength, until all the world has been permeated with gospel in-
fluences. Thus, the history of steadily worsening apostasy and
declension seen in the book of Judges must be understood in
terms of the history of redemption (as we set it out in the Intro-
duction), and we are today not living at the same stage of his-
tory as they were. There is a decline and there is an expansion,
and we are living in the age of expansion (Matt. 28:18-20).

Nevertheless, the treachery of the human heart is a constant
factor in all ages. Even though the Spirit has been given in
greater measure to the Church, it remains a fact that all Chris-
tians tend to “tum quickly out of the way,” and ignore the
mighty acts God has wrought on their behalf. Moreover, the
fact of a general forward motion to history and a general expan-
sion of the gospel does not eliminate times of setback and apos-
tasy. Surely the 20th century is a time of great apostasy in the
Western world, and, just as surely, the sad history of the Judges
sheds light on our sorry condition.

We are told (v. 18) that the Lord was with the judge, and that
is was the Lord Who, through the judge, saved Israel. This is
important, for it shows that the judges are pictures of Jesus
Christ, Who is the Lord. When we look at the salvific actions of
the judges (not at their sins), we must see the Lord there also;
and where we see the Lord, we see Christ. The judges are types
of Christ not only because their actions symbolize His, and not
only in that they were anointed by the Spirit (making them
“messiahs” — anointed ones), but also and especially because
Christ was there with them directing and controlling their ac-
tions. We must see Him at work in this book of Judges. (More
about the nature of these judges in chapter 3 of this study.)

The principle of progressive declension is articulated once
again in verse 19. They not only turned away quickly (v. 17), but
they acted more corruptly than their forefathers.

What this passage points to in a way of principle is this. Cul-
ture is an effect, a product, of religion. Those who serve the
Lord will develop a Christian (Godly) culture, with the Chris-
tian benefits of liberty, mutual respect, and peace. Service to
other gods likewise produces cultures that are in line with those
