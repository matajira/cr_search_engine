204 Judges

the enemy and delivered Israel. The women sang and danced for
both. Both aspired to build a house, but David’s motives were
relatively more pure.)

Jephthah ’s Aspirations Thwarted

34, When Jephthah came to his house at Mizpah, behold,
his daughter was coming out to meet him with tambourines and
with dancing. Now she was his one and only child; besides her he
had neither son nor daughter.

35. And it came about when he saw her, that he tore his
clothes and said, “Alas, my daughter! You have brought me very
low, and you are among those who trouble me; for I have given
my word to the LoRD, and I cannot take it back.”

36. So she said to him, “My father, you have given your
word to the Lorp; do te me according to what has proceeded
out of your mouth, since the Lorp has avenged you of your
enemies, the sons of Ammon.”

37. And she said to her father, “Let this thing be done for
me; let me alone two months, that I may walk about and go
down on the mountains and weep because of my virginity, I and
my companions.”

38. Then he said, “Go.” So he sent her away for two months;
and she left with her companions, and wept on the mountains
because of her virginity.

39, And it came about at the end of two months that she
returned to her father, who did to her according to the vow
which he had made; and she knew no man. Thus it became a
custom in Israel,

40. That the daughters of Israel went annually to com-
memorate [talk with] the daughter of Jephthah the Gileadite
four days in the year.

The story of Jephthah is probably the hardest to understand
in the book of Judges. This is partly due to the prejudice of
most commentators, who have confused the matter horribly by
insisting that Jephthah killed his daughter. The main problem,
however, comes from the fact that all the basic elements in this
story presuppose that we are familiar with certain fundamental
ways of thinking that were common in the ancient, medieval,
and early modern world, but that are completely unknown to-
day. We have already mentioned the victory-housebuilding pat-
tern, the correspondence between doorways and birth, and the
