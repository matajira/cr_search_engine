xiv Judges

some are not really well supported, or not credible, that is fine.
The important thing is to engage in the interpretive discussion,
and strive for a fuller understanding of the prophecies before
us.

The second “secret” is to keep an eye on the interaction be-
tween God and man. We ask three questions:

1. What is God’s Word of promise and command?
2. What is man’s response (rebellion or faithfulness)?
3. What is God’s Word of evaluation (judgment or blessing)?

Every Biblical narrative contains all three elements, at least
by implication. Sometimes the Word of promise/command is
not expressed, because it is contained in the Law, which is the
background for all the later books of the Bible. Every promise is
a command, for the faithful man knows that he needs to pursue
the blessing in the promise; and every command is a promise,
for God will always bless those who submit to His commands.
We then come to man’s response. Men are either faithful or re-
bellious — sometimes a mixture of the two. Then, third, we come
to God’s evaluation or judgment, which entails either curse or
blessing.

This threefold action underlies every narrative in Scripture.
Adam was given a command/promise. He rebelled. God came
to judge him. Humanity as a whole is given a command/promise
from God. Human history as a whole is the response of human-
ity. The Last Judgment is the final evaluation made by God.
Abram was given a command: move to Canaan. Abram obeyed.
After he arrived in Canaan, God met him and blessed him — and
gave him his next orders, which Abram obeyed, and God
blessed him and then gave him his next orders, which he obeyed,
etc., etc.

The third “secret” is to take note of the larger covenant-
historical context of the book. The Bible presents one basic
story over and over again, with variations each time, designed
for our instruction. This is the story of creation, fall, decline,
judgment, and re-creation. This pattern happens in three very
large historical sweeps during the Old Covenant. The first occur-
rence is the creation of the world, the fall of Adam, the decline
recorded in Genesis 6, the judgment of the Flood, and the re-
