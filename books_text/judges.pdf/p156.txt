Gideon: God's War Against Baal BY

ture. (See, for instance, 1 Thess. 5:5-8.) The camp of God’s
enemies is asleep to the real issues of life. Their cultures are stag-
nant, with no social or scientific progress. Into these stagnant,
sleeping cultures comes the gospel, which shakes them up, caus-
ing discord. (Compare Matt. 10:34-36 and Luke 12:51-53.)

2. The trumpet is the herald of judgment. The trumpet an-
nounced the judgment of Jericho (Josh. 6:20} and will an-
nounce all judgments of God (Rev. 8:2). It is a message of judg-
ment that is thrown into the camp of the ungodly.

3. The light is the witness of Truth (lohn 3 :19; 1 John 1:5-7;
2:9-10; etc.). It is the light of Truth that is shone into the camp
of the wicked, but men love darkness more than light. Light
blinds them, and it also shows up their sins and filth.

4. The sword is the proclamation of the Word, and especially
of the gospel: The Lord and His Messiah (Is. 11:4; 2 Thess. 2:8;
Heb. 4:12; Rev. 19:15). The proclamation of the Word throws
the sleeping camp of the wicked into constemation.

All we have to do is stand fast and preach the full gospel of
judgment and salvation, and God will destroy the enemy, by
causing the enemy to self-destruct. The self-destructive
character of evil men is represented here, as the enemy kills itself
off, but since this is the theme of the entire ninth chapter of
Judges, we shall postpone discussion of it until we get there.

This accomplished victory is the final stage of the upbuilding
of Gideon’s faith. Now all Israel fall in line behind him, to mop
up what is left of the enemy as they flee. The theology here is the
same as we have already seen in Judges: The first and definitive
blow is struck by God Himself, and then the armies of the right-
eous are called in to finish mopping up the remnants of the
enemy. We confess that Christ Jesus has won the definitive vic-
tory, and now we as His Church follow Him, privileged to put
down all His enemies on the earth.

Judgment: Oreb and Zeeb

24, And Gideon sent messengers throughout all the hill
country of Ephraim saying, “Come down to meet Midian and
take the waters before them, as far as Beth-Barah and the Jor-
dan.” So all the men of Ephraim were summoned, and they took
the waters as far as Beth-Barah and the Jordan.

25. And they captured the two leaders of Midian, Oreb and
