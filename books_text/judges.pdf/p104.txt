86 Judges

Satan and the woman takes the form that Satan wants to
possess the bride, and raise up his own wicked seed through her
(as in Gen. 12, 20, and 26). Thus, it is fitting that here the
woman crushes the serpent’s head.

Sisers was thirsty, and asked for water. She gave him milk,
or as Judges 5:25 indicates, a kind of buttermilk or yogurt. This
would make him sleepier than mere water would have. It is also,
again, an essentially womanly act to give milk. Just as Deborah
had fed Israel with the milk of her words for years, so Jael gives
milk to Sisers; but that milk which strengthens the righteous is
poison to the wicked (compare 1 Cor. 11:30 and 2 Cor. 2:16).

So convincing was her deception that Sisers asked a further
favor of her, that she misdirect anyone looking for him. He
wanted her to act as his guardian. This request builds the humor
of the situation: He was asking Israel’s guardian to guard him.
He did not in the least anticipate what was about to happen to
him.

Once Sisers was sound asleep, Jael drove a tent peg through
his head. Her faith in God calmed her nerves for this frighten-
ingly gruesome task. It was the woman’s job to setup tents, and
she would have known how to drive a tent peg from years of ex-
perience. Christ has crushed Satan’s head definitively, in His vic-
tory on the cross. Christ’s people are called to join with Him in
this victory, and the promise is that we too shall crush Satan’s
head, in union with Christ (Rem. 16:20). We who live after the
cross reflect the work of Christ, but those who lived before the
cross anticipated His work. Jael, then, is a prophetic picture of
Christ, the ultimate Seed of the Woman.

Barak was pursuing Sisers, intending to kill him, when Jael
beckoned him to come and see her work. Thus was Barak
brought face to face with the fulfillment of Deborah’s prophecy.

Commentators have not been very nice to Jael. Most seem to
be squeamish about some aspect of what she did, some criticiz-
ing her betrayal of the “laws of hospitality, * other lighting on
her out-and-out deception and lie to Sisers, “fear not .“ Most
give her credit, like Rahab, for having identified with the cause
of Israel, but all seem to feel that somehow she should have
done something other than what she did.

What are the charges against Jael? We may list them as fol-
lows:
