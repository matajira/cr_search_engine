Gideon: God’s War Against Baal Di

He also prepared a kid. This was also a generous sacrifice. A
whole kid would be valuable enough under ordinary cir-
cumstances, but after seven years of Midianite pillage, after
they had left “no sustenance in Israel as well as no sheep, ox, or
donkey,” we can well imagine that a kid would be precious in-
deed. The kid is a young goat, and just as all the sacrificial
animals symbolize humanity in its various aspects, the kid is
especially connected to youth. Thus, it is fitting that it was a kid
that Gideon offered.3

The meal had three parts: drink (broth), bread, and meat.
This corresponds to the Peace Sacrifice of the Mosaic Law. To
understand fully what happens here in the story of Gideon, we
must have some understanding of the Peace Sacrifice. In the
Peace Sacrifice a meal was shared among the offerer, the officiat-
ing priest, and the Lord. Some of the characteristics of this meal
were as follows:

1, The offerer eats part of the sacrifice (Lev. 7:15-17).

2. Some of the meat is given to the Lord, Who gives it back
to the priest to eat (Lev. 7:28-34; 10:14f.; 21:22).

3. The fat and certain organs are burnt up, tumed into
smoke, as food for the Lord (Lev. 3:11, 16; 22:25).

4. The unleavened bread is burnt up as food for the Lord,
while leavened bread is eaten by offerer and priest (Lev.7:11-14).

5. Wine is poured out for God to drink, while the par-
ticipants also drink wine (Num. 15:1-10).

6. Some examples of Peace Sacrifices are found in Genesis
18:1-8, Genesis 31:54, Exodus 18:12, and Exodus 24:1-11. Pass-
over is a variant of the Peace Sacrifice.

The unhewn rock formed a temporary altar (Ex. 20:25), and
at the Lord’s command Gideon poured out the broth for the
Lord to drink, Then the Lord touched the bread and meat with
his rod, which speaks of judgment, and all went up in flames.
God showed thereby that He was willing to eat a meal with His
people once again. Communion was restored.

God had told Moses, “You cannot see My face, for no man
can see Me and live” (Ex. 33:20). Gideon, realizing fully now
just Whom he has been conversing with, is struck with fear for

3, Ibid., pp. 1908, 2724.
