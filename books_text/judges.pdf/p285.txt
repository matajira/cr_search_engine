Samson: Blindness, Judgment, Restoration 27h

them, he would have known she was betraying him. She kept
him thinking that this was just a game.

The Philistine believed in magic, and they believed
Samson’s strength was magical. Samson has fun ridiculing this
belief in magic. He recommends fresh undried cords, never be-
fore used ropes, and binding his hair in a loom. Each time she
tries these, he breaks free, showing the stupidity of magic. For
Samson, it is just a game.

But it is more than that. We see Samson playing around just
a bit with his vow. In the first binding, it is seven cords that must
be used. This has to do with his seven locks of hair, and with the
seven-fold Spirit he depends upon. In the third binding, he tells
her to do something with his seven locks of hair, again toying
with his vow. We see Samson falling through compromise.

The message for Israel? No, magic does not exist, and you
need not fear it. But you must fear compromise. Involvement in
playing around with evil people will drag you down, until you
fall totally, as did Samson. You may get by with messing around
for a while, but there are always men lurking in the inner room,
waiting to destroy you when you are exposed.

15. Then she said to him, “How can you say, ‘I love you,’
when your heart is not with me? You have deceived me these
three times and have not told me where your great strength is.”

16. And it came about when she pressed him daily with her
words and urged him, that his soul was impatient to the point of
death.

17. So he told her all that was in his heart and said to her, “A
razor has never come on my head, for I have been a Nazirite to
God from my mother’s womb. If I am shaved, then my Strength
will leave me, and I shall become weak and be like any other

18. When Delilah saw that he had told her all that was in his
heart, she sent and called the lords of the Philistine, saying,
“Come up once more, for he has told me all that is in his heart.”
Then the lords of the Philistine came up to her, and brought the
money in their hands.

19. And she made him sleep on her knees, and called for a
man and had him shave off the seven locks of his head, Then she
began to afflict him, and his strength left him.

20. And she said, “The Philistine are upon you, Samson!”
