286 Judges

Sidonians, quiet and secure; for there was no ruler humiliating
them for anything in the land, and they were far from the Sidon-
ians and had no dealings with anyone.

8. When they came back to their brothers at Zorah and
Eshtaol, their brothers said to them, “What do you report?”

9. And they said, “Arise, and let us go up against them; for
we have seen the land, and behold, it is very good. And will you
sit still? Do not delay to go, to enter, to possess the land.

10. “When you enter, you shall come to a secure people with
a spacious land; for God has given it into your hand, a place
where there is no lack of anything that is on the earth.”

The spies return. Their report is just like the report of
Moses’ spies, except that they encourage Dan to make the move.
The text stresses that the people of Laish had no defence, in
order to point out the cowardice of the Danites. (Laish was in-
deed part of the land Israel was supposed to conquer, Joshua
13:1-7, but it was not part of Dan’s alloted territory.) They
wanted the blessings of the Covenant apart from the conditions
of the Covenant, which involved faithfulness to the Lord and
also holy war.

11. Then from the family of the Danites, from Zorah and
from Eshtaol, 600 men armed with weapons of war set out.

12. And they went up and camped at Kiriath-Jearim in
Judah. Therefore they called that place Mahaneh-Dan to this
day; behold it is west of Kiriath-Jearim.

Again we are reminded of Samson, who was from Mahaneh.
Dan (Jud. 13:25), in order once again to make the contrast be-
tween that mighty man and these Danites.

13, And they passed from there to the hill country of ~
Ephraim and came to the house of Micah.

14, Then the five men who went to spy out the country of
Laish answered and said to their kinsmen, “Do you know that
there are in these houses an ephod and teraphim and .a graven
image and a molten image? Now therefore, consider what you
should do.”

15, And they turned aside there and came to the house of the
young man, the Levite, to the house of Micah, and asked him of
his welfare.
