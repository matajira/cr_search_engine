The Minor Judges: Drifting toward Humanistic Kingship 185

violence. To get a uniform culture, they have to impose it from
above, and this works to nullify all local diversity. In a Biblical
society, the larger government sets only general policy, and
serves as a court of appeal; but in a humanistic state, the larger
government sets all policy, specific as well as general, thus
destroying local diversity, and there is no court of appeal
because all local courts are manifestations of the central court.

The bottom line is that man was never created to be omni-
present. Men are by nature local; they can only be one place at a
time, and they can only effectively govern a small number of
things. A wise man can manage a court of appeals, for only a
few things are appealed. No man, however, can rule all the
details of a whole civilization. Whenever men try to do s0, it is
because they are usurping God’s prerogatives, and they become
tyrants.

Did Israel learn from Samuel’s waming (assuming Samuel
wrote Judges)? No, and Samuel reiterated the warming in cap-
sule form in his sermon recorded in 1 Samuel 8. It is not wrong
to be like the nations in having a king, but it is wrong to want a
king who is like the kings of the nations. A king “like all the na-
tions” would create a standing army and a forced draft; he
would make himself rich by taking over large land holdings; he
would take their daughters for his own purposes; he would con-
fiscate their traditional family property; he would tax them
heavily and take a tithe (making himself God); he would take
their servants and their best animals; and finally he would make
them all his slaves. This would all be done in the name of effi-
cient central government.

This was the warning Israel refused to heed. This same warn-
ing was given us by our Puritan forefathers, who knew first
hand what they were talking about. We have not heeded it
either. If the book of Judges has a message for us it is this:
When God is once again King in his Church, and the Levites are
doing their job, and the people are obeying Him, then the tyrant
will be gone. The first part of Judges shows that God can take
care of foreign tyrants. The second part warns that we will also
face internal tyrants as well.
