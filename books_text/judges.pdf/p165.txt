148 Judges

there from his spoil.

26. And the weight of the gold earrings that he requested
was 1700 shekels of gold, besides the crescent ornaments and the
pendants and the purple robes which were on the kings of Mid-
ian, and besides the neck bands that were on their camels’ necks.

27. And Gideon made it into an ephod, and placed it in his
city, Ophrah, and all Israel played the harlot with it there, so
that it became a snare to Gideon and his household.

28. So Midian was subdued before the sons of Israel, and
they did not lift up their heads any more. And the land was un-
disturbed for 40 years in the days of Gideon.

The men of Israel do not look with the eyes of faith, and
thus fail to see that it is the Lord Who delivered them, and it is
the Lord Who should rule them. Moreover, they want Gideon to
establish a dynasty. Thus, it is clearly their desire to establish
some form of humanistic kingship that will be perpetual. They
do not want Gideon merely for a judge. They want a king on a
throne with a dynasty. They are putting their trust for safety and
security not in the Lord but in the principle of a centralized
state.

Gideon seems to take the Lord’s rebuke (assuming our inter-
pretation of verses 18-21 is correct), for he rejects the crown
offered to him, and also rejects the notion of a dynasty. Note
that in verse 22 we find explicitly stated the principle that the
savior is the lord. Those who separate Christ as Savior from
Christ as Lord are completely out of line from Scripture at this
point. Gideon’s reply is sound: The Lord saved you, so the Lord
must be your king.

This verse initiates the theme of humanistic kingship, which
will dominate the next chapter and the two appendices at the
end of Judges. What happens next is a small picture of the
larger problem discussed in Judges 17-21. Gideon does well to
reject human kingship, but hi’ next action undermines the
whole social order, and makes humanistic statism inevitable.
When men are religiously faithful, then the Lord truly is king,
and even if we have a human king (such as David) it is no threat
to liberty. But when men depart from the Lord, then they will
have humanistic statist rulers soon enough. So it was in the time
of the judges, and so it is today. The appendices to Judges show
that it was the religious failure of the Levites that undermined
