142 Judges

to your army?”

7. And Gideon said, “Just for that, when the Lorn has given
Zebah and Zalmunna into my hand, then I will thrash your flesh
with the thorns of the wilderness and with briers .“

8. And he went up from thereto Penuel, and spoke similarly
to them; and the men of Penuel answered him just as the men of
Succoth had answered.

9. So he spoke also to the men of Penuel, saying, “When I
retum safely, I will tear down this tower.”

While the larger army was mopping up at the crossing of the
Jordan, Gideon’s guerilla band was in hot pursuit of the kings
of Midian and their remaining 15,000 men. We invite the reader
to meditate on what it means that they were “weary, yet pursu-
ing.” There surely is a lesson here for each of us.

Having had no chance to eat, though they could drink at the
Jordan, Gideon asked for bread; not meat, just bread. Deuter-
onomy 23:3-4 tells us that God had cursed Moab and Ammon
because they did not give bread to hungry Israel as they came
out of Egypt. Jesus makes the same point in Matthew 25:34-40.
It is important to realize that Midian was often allied with Moab
and Ammon, as in the story of Balaam and Phineas, which is in
the background of the story of Gideon (Num. 22-25; Dt. 23:4),

Thus, when Succoth refused to help God’s people, they were
identifying themselves with the Moabites, Ammonites, and Mi-
dianites. Indeed, they were bold to say so: They wanted to take
no risks with the Midianite kings still on the loose. They had no
faith or trust in God. Spiritually they were Midianites, and
deserved to be treated as such. Like good liberals, they wanted
to have peace by having detente with the enemy.

The expression “is the palm ... already in your hands?”
refers to the practice of chopping off the hands of the enemy. It
was the “hand” of Midian that oppressed Israel (Jud. 6:1, etc.),
and to symbolize victory, the hand of the enemy leader would be
cut off his corpse. Just as in death his head would be cut off (as
we have seen), so also his hand would be cut off, that he might
no longer bear a sword against God and His people.

Part of the irony here is the names God gives to the leaders
of Midian. Zebah means “Victim,” and Zalmunna means
“Shade Denied,” that is, “Protection Denied.” These were prob-
ably not their real names! The writer has given them these
