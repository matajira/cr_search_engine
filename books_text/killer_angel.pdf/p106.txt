KILLER ANGEL

eration. She often spent Planned Parenthood money
for her own extravagant pleasures, for instance. She
invested organizational funds in the black market. She
squandered hard-won bequests on frivolities. And she
wasted the money she had gotten “by hook or by
crook” on her unrestrained vanities.

Because of her wastrel indiscretions, she was qui-
etly removed from the Planned Parenthood board
several times, but the organization found that it simply
could not survive without her. In the end, Planned
Parenthood was forced to take on the character and
attributes of its founder. “The love of moneyis the root
of all evil” (see 1 Tim. 6:10). Violence and greed are
inseparable (see Prov. 1:8-9). Thus, Planned Parent-
hood’s evil agenda of violence to women and children
cannot be cut loose from the deep tap root of avarice
and material lust that Margaret planted.

Sexual immorality, theft, adultery, covetousness,
greed, malice, wickedness, deceit, lewdness, lascivi-
ousness, arrogance, blasphemy, pride, ruthlessness,
and folly are all related sins (see Mark 7:21-22). They
commonly coexist (see Rom. 1:29-31). Certainly they
did in the tortured concupiscence of Margaret Sanger.
And they still do, in the organization that honors her
as pioneer and champion.

By the time she died on September 6, 1966, a week
shy of her eighty-seventh birthday, Margaret Sanger
had nearly fulfilled her early boast that she would
spend every last penny of Slee’s fortune. In the process,
though, she had lost everything else: love, happiness,
satisfaction, fulfillment, family, and friends. In the
end, her struggle was for naught.

96
