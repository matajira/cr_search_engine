PART THREE

 

No Little People

We often hear of a man becoming a criminal
through a love of low company. I believe it is much
commoner for a man to become a criminal through
a love of refined company. There is a kind of people

who cannot stand poverty because they cannot
stand ugliness. These people might rob or even
murder out of pure refinement.

—G. K. Chesterton!
