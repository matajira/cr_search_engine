KILLER ANGEL

Kropotkin’s subversive pragmatism, and communist
lectures on Bakunin’s collectivistic rationalism. But
she was especially interested in developing close ties
with the Malthusians.

Thomas Malthus was a nineteenth-century cleric
and sometime professor of political economy whose
theories of population growth and economic stability
quickly became the basis for national and interna-
tional social policy throughout the West. According
to his scheme, population grows exponentially over
time, while production only grows arithmetically. He
believed a crisis was therefore inevitable—a kind of
ticking population time bomb that he believed threat-
ened the very existence of the human race. Poverty,
deprivation, and hunger were the evidences of this
looming population crisis. He believed that the only
responsible social policy would be one that addressed
the unnatural problem of population growth—by
whatever means necessary. Every social problem was
subordinate to this central cause. In fact, Malthus
argued, to deal with sickness, crime, privation, and
need in any other way simply aggravates the problems
further; thus, he actually condemned charity, philan-
thropy, international relief and development, mis-
sionary outreaches, and economic investment around
the world as counterproductive.

In his magnum opus, An Essay on the Principle of
Population, published in six editions from 1798 to 1826,
Malthus wrote:

All children born, beyond what would be

required to keep up the population to a
desired level, must necessarily perish, unless

50
