PART FIVE

 

To Be or Not to Be

What I complain of is the shallowness of people
who only do things for a change and then actually
talk as if the change were unchangeable. That is
the weakness of a purely progressive theory. The
very latest opinion is always infallibly right and
always inevitably wrong.

—G. K. Chesterton!
