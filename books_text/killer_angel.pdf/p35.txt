PART TWO

 

Whence? What? Whither?

There is a tradition that jumping off a precipice is
prejudicial to the health; and therefore nobody does
it. Then appears a progressive prophet and
reformer, who points out that we really know
nothing about it, because nobody does it. And the
tradition is thereby mocked—to the peril of us all.

—G. K. Chesterton!
