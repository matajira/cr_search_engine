KILier ANGEL

7. Ibid., 481.

8. Ibid., 488.

9. William H. Bradenton, The Comstock Era: The
Reformation of Reform (New York: Laddel Press, 1958),
276.

10. Gringer, Sanger Corpus, 489.

CHAPTER SIX

J. Illustrated London News (May 14, 1927).

2. Allan Chase, The Legacy of Malthus: The Social
Costs of the New Scientific Racism (New York: Knopf,
1977), 7.

3. Paul Johnson, A History of the English People
(New York: Harper and Row, 1985), 276.

4. Ibid.

5. Germaine Greer, Sex and Destiny (New York:
Harper and Row, 1984), 309.

6. Daniel Kevels, In the Name of Eugenics (New
York: Penguin, 1985), 110.

7. Ibid.

8. Illustrated London News (February 14, 1925).

9. Ibid.

10. G.K. Chesterton, Eugenics and Other Evils (Lon-
don: Cassell, 1922), 54.

CHAPTER SEVEN

1, Hlustrated London News (September 27, 1919).

2. Illustrated London News (January 9, 1909).

3. Linda Gordon, Woman’s Body, Woman’s Right
(New York: Penguin, 1974), 204.

4. Ibid.

5. Margaret Sanger, The Pivot of Civilization (New
York: Brentano’s, 1922), 101.

6. Ibid., 108.

7. Ibid., 123.

114
