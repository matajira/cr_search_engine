ACKNOWLEDGMENTS

 

Lam riding my pen on the shuffle, and
it has a mouth of iron.

G. K. Chesterton!

ILAIRE BELLOC, PERHAPS THE MOST PROLIFIC
H curmudgeon of this century, once quipped,

“There is something odd book writers do in their
prefaces, which is to introduce a mass of nincompoops of
whom no one has ever heard, and say, my thanks is due to
such and such, all in a litany, as though anyone cared a
farthing for the rats.”

Needless to say, Belloc did not place high stock in
either gratitude or accountability. His fierce self-assur-
ance and autonomy as an author was defiantly unflap-
pable. I would hope that I know better.

A number of friends and fellow-laborers encouraged
me to pursue this project—and at the same time helped to
support the work of the King’s Meadow Study Center so
that I could. David and Pam Ferriss, Mike and Debbie
Grimnes, Jerry and Cindy Walton, Steve and Marijean
Green, Bill and Robin Amos, Bill and Sharon Taylor, Jim
and Gwen Smith, John and Marye Lou Mauldin, Steve and
Karen Anderson, and Bill and Dawn Ruff have all been
selfless supporters from the beginning. Dale and Ann
