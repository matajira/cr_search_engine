KILLER ANGEL

He met Margaret at a party in White Plains in 1900
and immediately fell head over heels in love. He was a
tall, dark-haired man with intense coal-black eyes and
a thin-set mouth turned down like an eagle’s. Now
almost thirty and entirely dedicated to his work, he
had sorely neglected the social side of his life for several
years. But he was smitten by the girlishly slim, red-
headed beauty he met that day.

He courted Margaret with a single-minded zeal,
promising her devotion, leisure, and a beautiful
home—the fulfillment of her most cherished dreams.
He plied her affections with flowers, candy, jewelry,
and unremitting attention. As for her part, she was
willingly—even enthusiastically—courted.

Within just a few months they were married,

The Sangers settled into a pleasant apartment in
Manhattan’s Upper East Side and set up housekeep-
ing. But housekeeping appealed to Margaret even less
than teaching or nursing. Though she busied herself
collecting pots, pans, and dishes, she quickly grew
restless and sullen.

Her doting husband tried everything he could
think of in a determined effort to satisfy her restless
and unresolved passions. He sent her off for long
vacations in the Adirondacks. He hired maids and
attendants. He bought her expensive presents. He
even designed and built an extravagant home in the
exclusive Long Island suburbs. Nothing seemed to suit
his temperamental bride.

In short order they had three children, two boys
and a girl. Like so many before and since, Margaret
thought that having babies might bring her the fulfill-

18
