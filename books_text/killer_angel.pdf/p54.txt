KILLER ANGEL

At first, the ploy seemed to work. Together they
enjoyed the enchantments of the chattering salons, the
quaint artists’ colonies, and quirky galleries that dotted
the Left Bank in those pre-holocaust halcyon days. They
were awed by the magnificent fountains which even
today fall with hallowed delicacy into the framing space
of the Place de la Concorde. They gawked as blue hues
crept out from behind the Colonades in the Rue de
Rivoli and through the grillwork of the Tuileries. They
marveled at the low elegant outlines of the Louvre—a
serious metallic gray against the setting sun. They
strolled under the well-tended branches that hung
brooding over animated cafes, embracing their conver-
sations with tender intimacy. They reveled in the sight
of the long windows that opened onto iron-clad balco-
nies in marvelously archaic hotels, while gauzy lace
curtains fluttered across imagined hopes and wishes and
dreams. Romance wafted freely in the sweet cool breezes
off the Seine—and they embraced it deeply and passion-
ately.

They took an apartment in a wonderful eighteenth-
century building replete with high ceilings, ornamented
plaster bas-relief across one wall, huge shuttered win-
dows, antique furniture, and loads of dusty old books.
They surrounded themselves with all the odd trappings
of an ex-patriot’s existence.

On their tight budget they could not afford the
typical Grand Tour initiation to the city—sitting in the
chic cafes along the Champs-Elysées for hours sipping
champagne at twelve dollars a glass or buying leather at
Louis Vuitton at a thousand dollars per garment or
snatching up two-hundred-dollar scarves at Hermes or
eating at the Epicurean five-star Bristol Hotel at more

44
