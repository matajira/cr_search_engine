KILuer ANGEL

Not satisfied even with this kind of extreme lascivi-
ousness, she also began an unusual and tempestuous
affair with Havelock Ellis.

Ellis was the iconoclastic grandfather of the Bohe-
mian sexual revolution. The author of nearly fifty books
on every aspect of concupiscence from sexual inversion
to auto-eroticism, from the revolution of obscenity to
the mechanism of detumescence, from sexual peri-
odicity to pornographic eonism, he had provided the
free love movement with much of its intellectual apolo-
gia.

Much to his chagrin, however, he himself was sexu-
ally impotent. Thus, he spent his life in pursuit of new
and ever more exotic sensual pleasures. He staged elabo-
rate orgies for his Malthusian and Eugenicist friends; he
enticed his wife into innumerable lesbian affairs while
he luridly observed in a nearby closet; he experimented
with mescaline and various other psychotropic and psy-
chedelic drugs; and he established an underground net-
work for both homosexual and heterosexual
extemporaneous encounters.

To Margaret, Ellis was a modern-day saint. She
adored him at once, both for his radical ideas and for his
unusual bedroom behavior. Their antics are beyond the
pale of decent discussion and somehow manage to tran-
scend the descriptive capacities of pedestrian prose.
They are best left unexamined.

But the inculcation of animal instinct was not the
only perversity they conjured together. The two of them
began to plot a strategy for Margaret’s cause. Ellis em-
phasized the necessity of political expediency—he be-
lieved that she would need to shortly return to New York
in some sort of triumphant display of faux courage and

62
