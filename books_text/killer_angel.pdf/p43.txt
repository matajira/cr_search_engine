4

MADONNA

 

Clichés are things that can be new and already old.
They are things that can be new and already dead.
They are the stillborn fruits of culture.

—G. K. Chesterton!

had close connections with revolutionaries the

world over: Bolsheviks in Russia, Fabians in
England, Anarchists in Germany, and Malthusians in
France. She lectured all across the American heart-
land, drawing large crowds, discoursing on everything
from the necessity of free love to the nobility of incen-
diary violence, from the evils of capitalism to the
virtues of assassination, from the perils of democracy
to the need for birth control.

E MMA GOLDMAN WAS A FIERY RENEGADE WHO

33
