Human Weeps

As her organization grew in power and prestige,
she began to target several other “ill-favored” and
“dysgenic races,” including “Blacks, Hispanics, Amer-
inds, Fundamentalists, and Catholics,” 5 Tt was not
long before she set up clinics in their respective com-
munities as well. Margaret and the Malthusian
Eugenicists she had gathered about her were not par-
tial; every non-Aryan—ted, yellow, black, or white—
all were noxious in their sight. They sought to place
new clinics wherever those “feeble-minded, syphilitic,
irresponsible, and defective” stocks “bred unhin-
dered.” Since by their estimation as much as 70 per-
cent of the population fell into this “undesirable”
category, Margaret and her cohorts really had their
work cut out for them.

But they were more than up to the task.

In 1939, Margaret designed a “Negro Project” in
response to requests from “southern state public
health officials”. —inen not generally known for their
racial equanimity.® “The mass of Negroes,” her pro-
ject proposal asserted, “particularly in the South, still
breed carelessly and disastrously, with the result that
the increase among Negroes, even more than among
Whites, is from that portion of the population least
intelligent and fit.” The proposal went on to say that
“Public Health statistics merely hint at the primitive
state of civilization in which most Negroes in the
South live.”

In order to remedy this “dysgenic horror story,”
her project aimed to hire three or four “Colored Min-
isters, preferably with social service backgrounds, and
with engaging personalities” to travel to various Black

73
