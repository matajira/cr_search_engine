I]

Root AND FRUIT

 

The advantage of being a sentimentalist is that you
only remember what you like to remember.

—G. K. Chesterton”

UST AS A NATION'S “HEAD” DEFINES THE CHARACTER
and vision of that nation, so an organization’s
“head” defines the character and vision of that or-
ganization. This is a very basic Biblical principle. It is the
principle of “legacy.” It is the principle of “inheritance.”
The Canaanite people were perverse and corrupt.
They practiced every manner of wickedness and repro-
bation. Why were they so dissolute? The answer, accord-
ing to the Bible, is that their founders and leaders passed
evil onto them as their legacy, as their inheritance (see
Gen. 9:25; Lev. 18:24-25; Amos 1:3-12).
Similarly, the Moabites and the Ammonites were a
rebellious and improvident people. They railed against

101
