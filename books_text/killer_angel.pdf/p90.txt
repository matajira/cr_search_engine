KILLER ANGEL

minded, and criminal classes. Billions of dol-
lars are expended by our state and federal
governments and by private charities and phi-
lanthropies for the care, the maintenance, and
the perpetuation of these classes. Year by year
their numbers are mounting. Year by year
more money is expended . . . to maintain an
increasing race of morons which threatens the
very foundations of our civilization.’

She was especially distressed by the dim prospects that
democratic suffrage afforded her dystopic plans to imple-
ment a universal system of inhuman humanism:

We can all vote, even the mentally arrested.
And so it is no surprise to find that the mo-
ron’s vote is as good as the vote of the genius.
The outlook is not a cheerful one.*

Jf there was little for her to cheer about in America,
there was even Jess on the international scene. Europe,
decimated by the Great War, was desperate to reverse its
dramatic decline in population, while the developing
world was no less desperate to stoke the hopeful fires of
progress with aggressive population growth. Despite the
fast start of her various enterprises, her message was falling
on increasingly deaf ears.

By convening dozens of like-minded “neo-Malthu-
sian pioneers” from around the world, she was hopeful
that together they would be able to circle the wagons, to
“develop a new evangelistic strategy,” and ultimately to
reverse the tide of public opinion and public policy—
and thus “to keep alive and carry on the torch of neo-
Malthusian truth.”

80
