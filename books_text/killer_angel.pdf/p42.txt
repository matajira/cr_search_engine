KILLER ANGEL

It was bad enough that Margaret had become en-
tirely enamored with Debs and his comprehensive
dogma of revolution, but then when Margaret fell under
the spell of the militant utopian Emma Goldman, Wil-
liam’s husbandly concern turned to extreme disap-
proval. Margaret had gone from an arch-typical
material girl to a revolutionary firebrand almost over-
night. And now she was taking her cues from one of the
most dangerous and controversial insurrectionists since
the bloody Reign of Terror during the French Revolu-
tion.

It was just too much. William began backpedaling
furiously. He steered clear of his radical associations.
And he tried desperately to pull his wife back into a more
conventional social orbit. Now that the revolution had
moved beyond parlor fantasies and arm-chair bombast
and had invaded the inner sanctum of his home and
family, its horrific disruptiveness became all too obvious
to him.

To Margaret’s way of thinking, however, he had
become a traitor to the cause. She was now a true be-
liever, and nothing and no one could possibly be allowed
to interfere with its progress among men and nations.
Thus, the paranoia of fanaticism sorely stigmatized him
in her eyes.

And her new attachment to the steely determinism
of Emma Goldman only reinforced that perversely held
taint.

32
