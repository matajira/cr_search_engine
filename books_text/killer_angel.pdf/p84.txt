KILLER ANGEL

: . 8
enclaves and propagandize for birth control.” Her
intention was as insidious as it was obvious:

The most successful educational approach
to the Negro is through a religious appeal.
We do not want word to go out that we want
to exterminate the Negro population and
the Minister is the man who can straighten
out that idea if it ever occurs to any of their
more rebellious members.”

Of course, those Black ministers were to be care-
fully controlled—mere figureheads. “There is a great
danger that we will fail,” one of the project directors
wrote, “because the Negroes think it a plan for exter-
mination. Hence, let’s appearto let the colored run it.”
Another project director lamented:

1 wonder if Southern Darkies can ever be
entrusted with . . . a clinic. Our experience
causes us to doubt their ability to work ex-
cept under White supervision.

The entire operation then was a ruse—a manipu-
lative attempt to get Blacks to cooperate in their own
elimination.

Sadly, the project was quite successful. Its geno-
cidal intentions were carefully camouflaged beneath
several layers of condescending social service rhetoric
and organizational expertise. Like the citizens of Ham-
lin, lured into captivity by the sweet serenades of the
Pied Piper, all too many Blacks across the country

74
