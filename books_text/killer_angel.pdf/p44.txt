KILLER ANGEL

She made her living selling her Anarchist magazine
Mother Earth and by distributing leaflets on contra-
ception and liberated sex. Known as the “Red Queen
of Anarchy,” she was baleful and brutal. But she was
brilliant—and she was more than capable of commu-
nicating that brilliance to vast throngs in her political
rallies. Her spare, spartan appearance proved an apro-
pos guise for her mechanistic dogma of dystopic dis-
ruption.

Margaret was completely taken by her erudite dis-
cussions of philosophical profundities and ideological
certainties. She hung on Goldman’s every word and
began to read everything in Goldman’s wide-ranging
library of incendiary literature, including the massive,
seven-volume Studies in the Psychology of Sex by Hav-
elock Ellis, which stirred in her a new lust for lust.

Goldman discipled the young reformer, introduc-
ing her to the concupiscence of Ibsen, Tolstoy, Vol-
taire, and Kropotkin. She taught her the grassroots
mobilization tactics of the great revolutionary cabals
of France, Austria, Poland, and Russia. She tutored her
subversive impulse with the Enlightenment cate-
chisms of Rousseau, Babeuf, Buonarroti, Nechayev,
and Lenin, She reacquainted her with the subversive
strategies of the Radical Republicans during the Re-
construction subjection of conquered territory fol-
lowing the American War Between the States. She
schooled her in the verities of Humanism—the fan-
tastic notions of the self-sufficiency and inherent
goodness of man, the persistent hope of perfectability,
and the relativity of all ethical mores. She desensitized
her to the most extreme ideas and the most perverse

34
