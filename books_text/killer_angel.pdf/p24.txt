KILLER ANGEL

After a thoughtful pause her father rejoined, “Well,
well, so that’s the idea. Then why didn’t you just say so?
Always say what you mean, my daughter, it is much
better.”

In spite of Michael’s concerted efforts to undermine
Margaret’s young and fragile faith, her mother had her
baptized in St. Mary’s Catholic Church on March 23,
1893. The following year, on July 8, 1894, she was con-
firmed. Both ceremonies were held in secret—her father
would have been furious had he known. For some time
afterward she displayed a zealous devotion to spiritual
things. She regularly attended services and observed the
disciplines of the liturgical year. She demonstrated a
budding and apparently authentic hunger for truth.

But gradually the smothering effects of Michael’s
cynicism took their toil. When her mother died under
the strain of her unhappy privation, Margaret was more
vulnerable than ever before to his fierce undermining.
Bitter, lonely, and grief-stricken, by the time she was
seventeen her passion for Christ had collapsed into a
bitter hatred of the church. This malignant malevolence
would forever after be her spiritual hallmark.

Anxious to move away from home as soon as she
could, Margaret was willing to go anywhere and try
anything—as long as it was far from Corning. After a
quick, almost frantic search, she settled on Claverack
College. A small and inexpensive co-educational board-
ing school attached to the famed Hudson River Insti-
tute, Claverack was a Methodist high school housed in
an imposing wooden building on twenty picturesque
acres overlooking the Hudson Valley. Not known for its
academic rigors, the school was essentially a finishing
school for protean youth.

14
