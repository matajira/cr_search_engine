KILLER ANGEL

and nomadic rootlessness was telling. Hospital work
proved to be even more vexing and taxing than teaching.
She never finished her training. In later years, however,
she would claim to be a trained and practiced nurse.
Nearly forty pages of her Autobiography were devoted to
her varied, often heroic, experiences as a seasoned vet-
eran in professional health care.” But they were little
more than Margaret’s well-realized fantasies.

In fact, her actual exposure to medicine was almost
nonexistent: she never got beyond running errands,
changing sheets, and emptying bedpans. Like so much
else in the mythic fable of her rise to prominence, her
career as a nurse was little more than perpetrated fraud.

Determined to escape from the harsh bondage of
labor and industry, she once again began to cast about
for some viable alternative. She finally resorted to the
only viable course open to a poor girl in those seemingly
unenlightened days when the Puritan work ethic was
still ethical: she married into money.

16
