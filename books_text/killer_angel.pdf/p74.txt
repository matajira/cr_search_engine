KILLER ANGEL

Next, she decided to open an illegal, back-alley birth
control clinic. Papers, pamphlets, and speeches could
only do so much to usher in the revolution. Following
her Malthusian and Eugenic instincts, she opened her
clinic in the Brownsville section of New York, an area
populated by newly immigrated Slavs, Latins, Italians,
and Jews. She targeted the “unfit” for her crusade to
“save the planet.”

But there would be no victory for Margaret Sanger
in this venture. Within two weeks, the clinic had been
shut down by the authorities. Margaret and her sister,
Ethel, were arrested and sentenced to thirty days each in
the workhouse for the distribution of obscene materials
and the prescription of dangerous contraband and dele-
terious medical procedures.

Predictably, Margaret was undeterred. As soon as
she was released, she founded a new organization, the
Birth Control League, and began to publish a new maga-
zine, The Birth Control Review. She was still intent on
opening a clinic, but her time in jail had convinced her
that she needed to cultivate a broader following before
she made another attempt at that. She thought that
perhaps the new organization and magazine would help
her do just that. And, she was right—the organization
and the magazine were the inauspicious beginnings of
the international empire she would later dub with the
innocuous-sounding moniker, Planned Parenthood.

Though she was now drawing severe public criticism
from such men as the fiery popular evangelist Billy
Sunday, the famed Catholic social reformer John Ryan,
and the gallant former president Theodore Roosevelt,
Margaret was gaining stature among the urbane and
urban intelligentsia. Money began to pour into her of-

64
