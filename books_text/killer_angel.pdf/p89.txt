9

A NEw WorLD ORDER

 

Civilization is only one of the things that men
choose to have, Convince them of its uselessness
and they would fling away civilization as they
fling away a cigar.

—G. K. Chesterton?

neo-Malthusian and birth control conference at the

tiny Hotel McAlpin in New York. She had grown
increasingly concerned that societal, civic, and religious
pressure might snuff out her nascent Eugenic ideals. As
she asserted:

] N 1925, MARGARET HOSTED AN INTERNATIONAL

The government of the United States deliber-
ately encourages and even makes necessary by
its laws the breeding—with a breakneck ra-
pidity—of idiots, defectives, diseased, feeble-

79
