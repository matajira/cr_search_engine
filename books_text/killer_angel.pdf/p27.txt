2

THE WINTER OF HER
DISCONTENT

 

The special mark of the modern world is not that it is
skeptical, but that it is dogmatic without knowing it.

—G. K. Chesterton!

ILLIAM SANGER WAS NOT EXACTLY RICH,
W but he was financially secure—and that was

close enough for Margaret. He was a young
man of great promise. An up-and-coming architect
with the famed McKim, Mead, and White firm in New
York City, he had already made a name for himself
while working on the plans for the resplendant Grand
Central Station and the landmark Woolworth tower
in midtown Manhattan.

17
