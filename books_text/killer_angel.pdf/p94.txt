KILLER ANGEL

They even mandated that each national affiliate be
willing to overcome any legal obstacles that might impede
the overarching Planned Parenthood agenda of Eugenic
cleansing through various forms of legal challenges, popu-
lar protests, and acts of civil disobedience. At times that
might mean merely sidestepping the law: in the Philip-
pines where abortions are illegal, Planned Parenthood
offers “menstrual extractions” instead—despite the fact
that the procedures are, for all intents and purposes, tech-
nically the same. At other times clear violation of the law
is perpetrated: in Brazil, where sterilization is illegal,
Planned Parenthood performs as many as 20 million pro-
cedures every year in its field clinics.!°

According to one internal directive issued from the
London office:

Family Planning Associations and other non-
government organizations should not use the
absence of the law or the existence of an unfa-
vorable law as an excuse for inaction; action
outside the law, and even in violation of it, is
part of the process of stimulating change.!!

Though these ideas were more than alittle radical, their
careful presentation and prudent institutionalization—
under the ever watchful management of Margaret and the
other neo-Malthusians—eventually paid off. And it paid
off in huge dividends.

Ultimately, most of Planned Parenthood’s neo-
Maithusian ideas found their way into some of the most
significant political, cultural, and social programs of the
twentieth century as modern presuppositional tenets of
an aggressive and universal politically correct ortho-
doxy. Unlikely support for the ideas sprang up every-

84
