INTRODUCTION

school left-wing extremism. It has weighed in with so-
phistocated lobbying, advertising, and back-room
strong-arming to virtually remove the millennium-long
stigma against child-killing abortion procedures and
family-sundering socialization programs. Planned Par-
enthood thus looms like a Goliath over the increasingly
tragic culture war.

Despite its leviathan proportions it is impossible to
entirely understand Planned Parenthood’s policies,
programs, and priorities apart from Margaret Sanger’s
life and work. It was, after all, originally established to
be little more than an extension of her life and world-
view.!

Most of the material from this project has been
drawn from research that J originally conducted for two
comprehensive exposés of that vast institutional cash
cow. Entitled Grand Mlusions: The Legacy of Planned
Parenthood, the first book has gone through twelve
printings and two editions since it was first published in
1988.°° The second book, entitled Immaculate Decep-
tion: The Shifting Agenda of Planned Parenthood, details
the remarkable changes the organization has made over
the last decade.! ? They gave wide exposure to the tragic
proportions of Sanger’s saga. From the beginning of
those massive projects, though, I felt that a shorter and
more carefully focused biographical treatment was war-
ranted. Little has changed in the interim—except that
the monolithic reputations of Sanger and her frighten-
ingly dystopic organization have only been further en-
hanced,

It is therefore long overdue that the truth be told. It
is long overdue that the proper standing of Margaret
