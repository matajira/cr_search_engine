Roor aNnb Fruit

policies, the organization makes every attempt to
mock, belittle, and undermine Biblical Christianity.
Bad seed brings forth bitter harvest. The legacy con-
tinues.

* Deceit. Throughout her life, Margaret Sanger devel-
oped a rakish and reckless pattern of dishonesty. She
twisted the truth about her qualifications as a nurse,
about the details of her work, and about the various
sordid addictions that controlled her life. Her auto-
biographies were filled with exaggerations, distor-
tions, and out-and-out lies. She even went so far as
to alter the records in her mother’s family Bible in
order to protect her vanity. Today, Planned Parent-
hood faithfully carries on her tradition of disinfor-
mation. The organization continually misrepresents
the facts about its lucrative birth control, sex educa-
tion, and abortion enterprises. Bad seed brings forth
bitter harvest. The legacy continues.

A recent Planned Parenthood report bore the slogan
“Proud of Our Past—Planning the Future.”” If that is
true—if the organization really is proud of its venal and
profligate past, and ifit really is planning the future—then
we all have much to be concerned about.

“Those who plow iniquity and those who sow
trouble harvest it. By the breath of God they perish, and
by the blast of His anger they come to an end” (Job
4:8-9).

105
