THe Winter oF Her DiscontENT

of well-planned social subversion and disruptive anti-
establishment protest that gripped her.

John Reed, who would later gain fame as a propa-
gandist for the Bolsheviks in Soviet Russia, was a
passionate speaker who exuded confidence. He also
had a knack for vivid, compelling prose. He described
with heroic idealism a kind of ideological crusade bent
on irreverently overturning the privileged status quo.
Appealing to her romantic extremism, he painted a
lucidly resplendent picture of adventurous anarchy
akin to some pre-deluvian epoch.

Margaret was wowed. The ideas and ideals of
Marxism had never seemed to her to be particularly
relevant to the real world. But in the hands of a com-
pelling presence like Reed, they came alive to her.
Before long, she could think of little else. She was
completely radicalized. She suddenly shed her bour-
geois habits and took to Bohemian ways. Instead of
whiling the hours away in the elegant shops along Fifth
Avenue, she plunged headlong into the maelstrom of
rebellion and revolution.

She read voraciously for the first time in her life.
John Spargo had just translated Marx’s Das Capital
into English. Lincoln Steffens had published The
Shame of the Cities. Jacob Riis released his classic, How
the Other Half Lives. Upton Sinclair was shaking the
establishment with raging indictments like The Jungle.
And George Fitzpatrick produced War, What For?
Each became an important factor in the development
of her newfound interests.

And each became an important part of William
and Margaret's lifestyle, too—their apartment quickly

21
