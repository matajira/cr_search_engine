KILLER ANGEL

Having convinced an entire generation of scientists,
intellectuals, and social reformers that the world was
facing an imminent economic crisis caused by un-
checked human fertility, Malthusian thought quickly
turned to practical programs and social policies.

Some of these managerial Malthusians believed
that the solution to the imminent crisis was political:
restrict immigration, reform social welfare, and
tighten citizenship requirements. Others thought the
solution was technological: increase agricultural pro-
duction, improve medical proficiency, and promote
industrial efficiency. But many of the rest felt that the
solution was genetic: restrict or eliminate “bad racial
stocks” and gradually “help to engineer the evolution-
ary ascent of man.”

This last group became the adherents of a malevo-
lent new voodoo-science called Eugenics. They
quickly became the most influential and powerful of
all the insurgent ideologists striving to rule the affairs
of men and nations. In fact, for the rest of the twentieth
century they would unleash one plague after an-
other—a whole plethora of designer disasters—-upon
the unsuspecting human race.

The Eugenicists unashamedly espoused an elitist
White Supremacy. Or to be more precise, they es-
poused an elitist Northern and Western European
White Supremacy. It was not a supremacy based on
the crass ethnic racism of the past but upon anewkind
of “scientific” elitism deemed necessary to preserve
“the best of the human race” in the face of impending
doom. It was a very refined sort of supremacy that
prided itself on rationalism, intellectualism, and pro-
gressivism.

54
