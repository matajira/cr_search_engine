THE MARRYING KIND

course. And she went out of her way to prove it; she
flaunted her promiscuity and infidelity every chance
she could get.

She was still terribly unhappy, but at least now she
was terribly rich, too.

Immediately, Margaret set herself to the task of
using her new wealth to further the cause. She opened
anew clinic—this time calling it a “Research Bureau”
in order to avoid legal tangles. Then she began to
smuggle diaphragms into the country from Holland.
She waged several successful “turf” battles to maintain
control over her “empire.” She campaigned diligently
to win over the medical community. She secured mas-
sive foundation grants from the Rockefellers, the
Fords, and the Mellons. She took her struggle to
Washington, testifying before several congressional
committees, advocating the liberalization of contra-
ceptive prescription laws. And she fought for the in-
corporation of reproductive control into state
programs as a form of social planning. With her al-
most unlimited financial resources, she was able to
open doors and pull strings that had heretofore been
entirely inaccessible to her.

Margaret was also able to use her newfound wealth
to fight an important public relations campaign to
redeem her reputation—which, despite her success,
bore the taint of radicalism and social disruption.
Because of her Malthusian and Eugenic connections,
she had willingly become closely associated with the
scientists and theorists who put together Nazi Ger-
many’s “race purification” program. She had openly
endorsed the euthanasia, sterilization, abortion, and

91
