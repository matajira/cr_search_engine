Edgar Cayce: From Diagnosis to Gnosis 219

whole eternity to fear, One cannot hold both positions. Each side
knows it. Or at least the consistent representatives do.

If we are to believe Dr. Cerminara, the doctrine of reincarnation
has an important social and political corollary: the irrelevance of institu-
tional forms. “Marriage as an institution is, by the reincarnationist
view, less sacrosanct than many people think. If society wishes to
make marriage indissoluble, well and good; if not, again well and
good. Cosmic law will not be thwarted by either system —if man fails
to meet an obligation in one existence, he will irrevocably be called
to task in another. The outer forms which man sets up are almost as
arbitrary and almost as unimportant as the rules he devises for gin
rummy. In the last analysis it matters very litte what rules are set up
for any game, because through the forms and conventions of all of
them it is skill and honesty in playing which are their intrinsic
value.”80

On the one hand, this is radically antinomian. It denies that
there is any form of social order that is dictated by the limits and
needs of mankind. But this is consistent with the humanism of karma:
man is ultimately limitless, Therefore, by becoming radically anti-
nomian, the reincarnationist opens up the possibility of radical au-
thoritarianism. There is no ethical principle, no higher law, to which
men can appeal other than social convention. Men must conform, in
a Dewey-like fashion, to the rules of society. The Moloch state, as
Rushdoony has called it, is set free from the restraining features of
godly law. Men are left defenseless to play the institutional games of
the totalitarian regime. The worst, as Hayek has said, will tend to
get on top of such a coercive state system.*!

Cayce’s movement has proven to be quietistic and retreatist,
waiting for Atlantis and California earthquakes. They wait for
China to become the new cradle of Christianity, as predicted.® (Un-
fortunately, China did not become democratic in 1968, as pre-
dicted.®) They wait for the Soviet Union to change its present lead-
ership, for a prophecy told them, ‘out of Russian comes again the
hope of the world.”** Not from Bolshevism, but from a reawakening
of freedom: Russia's future will be crucial. So they wait, and hope,

80. Tbid., p. 154.
81. F. A. Hayek, The Road to Serfdom (Chicago: University of Chicago Press,
1944), ch. 10.
82. Furst, Jeus, p. 333.
83. Stearn, Prophet, p. 90.
84. Furst, Jesus, p. 332.

  
