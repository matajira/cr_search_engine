412 UNHOLY SPIRITS

Duke University, 120, 197
Dyrbal, Karl, 95, 97

Eastern mysticism, 29
Eastern religion, 271
Eckhart, Meister, 63
economic growth, 274, 276, 279
ecumenicism, 189
Eden, 101, 211
education, 20, 196-97, 229, 249, 254
efficiency, 297
Egypt, 211-12, 214
1897, 293
Einstein, Albert, 144
Eisenbud, Jule, 110-16
Eliade, Mircea, 154-55, 275, 276,
336-37, 342, 344, 3470
Elisha, 68-69
elite, 37-38, 340-41, 361, 372
elitism, 357
Ellul, Jacques, 297
endless quest, 330
end of era, 11, 13, 17
Engels, Frederick, 37, 306
Enlightenment, 13, 368
environmentalism, 278, 283, 348
envy, 279-80
episternology, 17, 119, 145, 262
Esalen, 171
escapist religion, 27, 333, 376,
380, 393
eschatology, 380-83, 386-91
ESP, 88-89, 95, 11, 112, 118,
120, 165-74, 355
Establishment, 399
ethics
biblical theme, 68
dominion &, 38, 158-59, 276
fall of man, 61-62
goal, 334
karma, 215-16
ritual vs., 136-37
see also morality
evangelism, 249
Eve, 62
evidence
authority &, 39

ignored, 47
improved, 48
Ockham, 31
scientific, 46
evolution
ancient idea, 292
centralization, 368
Darwinian, 46, 133, 340
deification, 85, 133
elite, 361
intellect, 332
judgment vs., 65, 133
leap of being, 5, 148, 318
magic, 133
man-directed, 133, 148
man’s role, 133
mind, 133
New Age, 86
politics, 364
purposeful, 340
relativism, 224
self-transcendence, 133
spacemen, 306-9
statism, 377
transmutation, 340
UFO's, 305-6
varieties, 328
existentialism, 136-37, 138, 149
experience, 31
experiments, 118
eyeless sight, 92-93, 168-69
eyes, 241

facts, 39-43, 50, 57, 286
factuality, 40-41, 67

faith, 40

faith healers, 268-70

faith (loss), 23-4, 43, 255
fakery, 260, 267

fall of man, 61-62, 71, 334, 335
false teeth, 165

familiars, 183

Faraday cage, 84n, 91, 113, 123
fascism, 224

fashions, 7

fate, 30, 173, 280

Fate, 250

Fatima, 294
