244 UNHOLY SPIRITS

phenomenon. The key word, of course, is “known.” [f setence is unable
to explain ut, it is not known. The public, slowly but surely, is becoming
aware that orthodox science is involved in a sleight-of-hand trick far
more comprehensive than anything dreamed of by peasant healers.
Peasant healers sometimes fake miracles; orthodox scientists who
face the facts of the demonic are constantly faking non-miracles.
“Give us time; we can explain this,” they say, paraphrasing the
“Amazing Randi’s” challenge: “Give me time, and I can duplicate
this.” Then orthodox scientists scurry away, hoping that both the
unexplained phenomena and the public’s memory of their challenge
will somehow fade away.

Arigo was again convicted of practicing medicine without a
license in 1964. This time he was sent to jail. Predictably, he began
healing prisoners, The public kept coming to him, and the jailers
looked the other way. He even was given a key to allow him to come
and go when he wished. The world-wide effects of the occult revival
were making themselves felt in Brazil in 1964-65. The Roman Cath-
olic Church, like a growing minority of formerly orthodox scientists,
was involved in a holding operation called “redefinition.” If an occult
practice could not be stamped out, or at least suppressed enough to
keep the public from making it into a fad, it was redefined as “para-
psychological” in nature. Parapsychology was seen more and more
as a legitimate subdivision of scientific investigation, thereby sanc-
tioning the observation of occult abilities. Somehow, by calling de-
monic possession “parapsychological behavior,” the church’s leader-
ship hoped to justify their retreat from the fray. This was neutraliz-
ing the old hostility to witchcraft.” In Arigo’s case, the authorities
were growing tired of trying to suppress him, He had become one of
Brazil's half a dozen most widely recognized citizens. He was front-
page news, year after year. He had treated people from every station
in life. He was paroled in June of 1965, after having spent seven
months in jail.

The following summer, Puharich led a well-equipped team back
to Congonhas do Gampo. They watched, charted, and recorded
events for three days, involving about 1,000 patients. With a tape re-
corder, they took down each of Arigo’s 965 diagnoses. They checked
to see if the patient had brought along his own physician’s written
diagnosis. In 545 cases, they had. Out of these 545, Arigo had made
diagnoses in accord with the physicians in 518 cases, or a total of

30. Ibid., p. 62.
