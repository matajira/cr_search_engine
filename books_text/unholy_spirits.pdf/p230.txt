224 UNHOLY SPIRITS

Cayce’s philosophy is so thoroughly at one with the underlying pre-
suppositions of modern thought: relativistic, man-centered, hostile
to Christian dogma, universalistic, messianic. His is a system of
process philosophy, so deeply ingrained in Western thought since Dar-
win. “Now is Truth such a thing that those who have been followers
of Mohammed have all the truth? Have those who have been the fol-
lowers of Moses, the law-giver, all of the truth? Or hasn’t it been,
rather, a growth in our individual lives; and what may be truth for
one individual may not, in the experience of another individual, an-
swer at all? Does that make the other any less true for the other in-
dividual... . Then if this be true, it is possible that truth is a
changeable thing—is a growth.”®

Truth is totally man-centered, “the essence with which an in-
dividual builds faith, hope and trust.” It is, in “theclogian” Paul
Tillich’s popular phrase, an “ultimate concern.” Man constructs his
own truth, his own God: “[W]hat you hold before yourself, to create
that image you worship—that is what will develop you always up-
ward, and will continue to enable you to know truth.”! Cayce then
came to a doctrine of truth and God which is essentially Hegelian-
fascist- Marxist: “Truth, then, being a growing thing; truth, being a
thing that will develop you; is a something that is entirely in action!
That’s what God is! For in every movement that has ever been, there
has been a continual upward development— upward toward that
which is Truth. . . . What is prayer but simply attuning yourself to
that which you are seeking assistance through? That's all prayer is
—the attunement to that very same thing; and that becomes Truth when
i becomes an action, When it goes into action, to you it becomes
Truth. . . . You go on whichever way your standard is set.”?

Hegel writing of the movement of the dialectic, Marx writing of
the unity of theory and practice, or Mussolini writing of the neces-
sity of a life of pure action: the evolutionary thrust leads to total, un-
compromising relativism. No wonder Cayce’s cosmology—on the sur-
face so archaic—could have attracted so many modern followers. It
holds out that most ancient of modern heresies: the desire to worship
oneself as God. Man, made in the image of God, wishes to worship
an image rather than the Creator. The second commandment pro-
hibits this practice, but Cayce asks: “Why not? Because if you make

99. Reader, pp. 31-32.
100. Ibid., p. 33
101, Bbid., p. 34.
