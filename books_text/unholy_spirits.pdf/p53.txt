The Crisis of Western Rationalism 47

dox scientists, including Darwin himself, when Wallace became a
convinced spiritualist. What Prof. Irvine wrote of Darwin, he could
have written about most rationalistic scientists of the late nineteenth
century: “The supernatural interfered with the aesthetic symmetry of
his ideas, The Deity had become an epistemological inconvenience.”#*

Students of Witchcraft

One of the last representatives of this naively confident older hu-
manism who still writes on the topic of witchcraft with real authority
is Rossel Hope Robbins. He was still teaching on university cam-
puses in the late 1960's. His important book, The Encyclopedia of
Witchcraft and Demonology (1959), announces in no-nonsense terms his
presuppositions regarding the witchcraft phenomena of the late
Middle Ages: “I have selected what F conclude to be the most signifi-
cant for the total picture of witchcraft presented here: a colossal
fraud and delusion, impossible because the ‘crime’ of witchcraft was
an impossible crime. The present entries could have been doubled or
trebled, but the reader would not necessarily be that much richer or
wiser.” Indeed, when one begins with a religiously held conviction
that a particular kind of phenomenon is impossible, every last bit of
evidence offered to the contrary will be reinterpreted (or ignored) in
order to force it into another mosaic of interpretation — hallucination,
torture, self-delusion, desire for notoriety, etc. Robbins is only echo-
ing sixteenth-century skeptics, such as Johann Weyer, a Protestant
physician, who blamed the confessions of witches on their confusion.
He was not alone in his opinion. Robbins lists several pages of the
titles of tracts dealing with witchcraft from the early modern period,
and half a dozen were skeptical. Robbins writes in the same tradi-
tion of the two great historians of witchcraft in the last century, bath
totally skeptical: Joseph Hansen and Henry C. Lea. They deny any
supernatural reality to the witchcraft phenomena for the same rea-
son that they deny a personal God who manifests Himself in human
time: the natural realm is ours and ours alone, by definition.

At the other end of the spectrum are the totally credulous. Mon-
tague Summers is the classic example of a scholarly historian of the
occult— witchcraft, vampires, werewolves— who believed almost
every word in every document charging these crimes against certain
individuals. Looking at the same documents that Lea and Hansen

48. William Irvine, Apes, Angels and Victorians: The Story of Darwin, Huxley and
Evolution (New York: McGraw-Hill, 1959), p. 113.
