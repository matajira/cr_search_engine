ds This the End of the World? 393

panel was very well received.2° DeMar subsequently taught a course
on the Christian basis of civil government on Rev. Tilton’s satellite
network, It, too, was well received.

Mr. Hunt sees that if this fusion of theological interests takes
place, then the day of unchallenged dominance by the old-time dis-
pensational eschatology is about to come to an end. A new funda-
mentalism is appearing.”? If the New Agers and New Dealers also
tecognized what Mr. Hunt has scen, they would be even more con-
cerned than he is. The implicit and 60-year-old alliance between
fundamentalism’s escapist religion and the humanists’ power religion
is about to break up. The dominion religion of orthodox Christianity
is going to challenge both of them. The false peace of this collapsing
alliance, whose cornerstone has been the public school system, is be-
ing shattered by hard-core Christian activists who are tired of sitting
in the back of humanism’s bus.

It is not that the “positive confession” charismatics may be about
to become New Age humanists; it is that they may be about to be-
come Christian reconstructionists and postmillennialists. It is not
simply Robert Tilton who constitutes Mr. Hunt’s problem. It is also
David Chilton.”

Conclusion

A paradigm shift is in progress inside American Protestant fun-
damentalism, It is a paradigm shift which above all involves the re-
thinking of the concept of dominion. Second, it involves a lengthen-
ing of the time perspective of Christians. Third, it involves a re-
thinking of the present-day authority of biblical law. This paradigm
shift is not yet recognized, except by those who are self-consciously

26. To give a picture of what the new technology offers “the little guy,” Rev.
Robert Thoburn was one of the pane] members. He was a former delegate to the
House of Delegates in Virginia, and the founder of the most financially successful
for-profit Christian school in America. Thirty days before the conference, he de-
cided to write a paperback book, he Christian in Politics. He sat down in front of an
IBM PC computer, wrote several chapters a day, and his son, a professional typeset
ter, typeset it fram this computer's disks. The book was sent to the printers 10 days
after he began, and 3,500 copies were available at the conference. The total invest-
ment was under $5,000, not counting the equipment. He sold the books and had his
money back before the printer even sent the bill.

27, Backward, Christian Soldiers?, ch. 4: “Fundamentalism: Old and New.”

28. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
‘Texas: Dominion Press, 1985); Days of Vengeance: A Comunentary on the Book of Revelation
(Ft. Worth, Texas: Dominion Press, 1986).
