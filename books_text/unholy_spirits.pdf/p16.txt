10 UNHOLY SPIRITS

happiness rock concert, on the Isle of Wight in the English Channel,
in August 1970. Then it was all over.

Economies on Campus

Third, the economic recession that was first revealed by the stock
market in the summer of 1969 stretched into 1970 and then into 1971.
The days of easy jobs for college graduates ended. Even more impor-
tant for the campus, the 1964 prediction by New York University’s
president Allan Cartter came true: that in 1969, the boom in college
hiring would end. It did. A glut of Ph.D.’s appeared, overnight.
From the spring of 1969 until the present (and far into the foresee-
able future), newly graduated Ph.D.’s in almost every field have not
been able to get full-time college teaching jobs, let alone tenured
positions. The non-tenured junior faculty members saw the hand-
writing on the wall, and there was no time left for love-ins with
undergraduates — not during normal class hours, anyway, and not in
groups.

It is now common for people in southern California who hold
earned Ph.D.’s from major universities to take part-time jobs teach-
ing nearly illiterate junior college students. For this they are paid
$25 per classroom teaching hour. Deduct travel time, class prepara-
tion time (reviewing old class notes, reading the assigned textbook),
exam-grading time (especially if the teacher gives essay exams), and
the instructor discovers that he is working for about $6 per hour. So
much for the value of the Ph.D. This is the financial return on an in-
vestment of the extra half decade to a decade of a person’s youth that
it takes to earn a Ph.D. This shift in market rewards ended the visi-
ble radicalism of the professors in the fall of 1970. It has never
returned. “They ain’t marchin’ anymore.”

We Cant Go Home Again

Many of the changes wrought philosophically and morally by the
counter-culture are still with us. The New Age movement has
become respectable and bureaucratized. The psychedelic baby did
not exactly eat the cybernetic monster; it just grew up and bought an
IBM PC. The hippies got haircuts, but a significant proportion of
them have not abandoned their world view. The Weathermen went
underground, but they still await a favorable time to begin terrorist
attacks. LSD was out by 1970, but cocaine is in. Cocaine has become
the preferred drug of stockbrokers everywhere. Standards have
