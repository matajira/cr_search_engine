Edgar Cayce: From Diagnosis to Gnosis 223

Conclusion

Edgar Cayce was a man who saw auras, he said; read people’s
minds, he said; received visions and revelations throughout his life,
he said; and was not an occultist, he said. He did go into something
like 25,000 trances over the years, and the records of over 15,000 of
them are still available for inspection by researchers. He has been
the subject of dozens of books. There seems to be no serious evi-
dence available that he faked the trances; if he did, the motive is ob-
scure. Some of his prophecies and diagnoses — of a minutely detailed
kind—did turn out as he had said. These served as the bedrock for
the construction of a humanistic, occult cosmology: reincarnation,
vibrations, evolution, monism, pantheism, etheric planes of exist-
ence, and so forth. What is central to the Cayce movement is his cos-
mology, not the “signs and wonders” that served as the magical valid-
ation of the later philosophy.

Max Weber, the great German sociologist, defined charismatic au-
thority as that possessed by some unique personality because of cer-
tain magical qualities or demonstrations—the Bible’s signs and won-
ders —“by virtue of which he is treated as endowed with supernatural,
superhuman, or at least specifically exceptional powers... .”%
Because of these powers or qualities, he is considered a leader. His
rule is characterized by hostility to all other forms of authority,
whether bureaucratic (rational) or traditional. He comes with the
words, “You have heard it said. . . . but I say unto you... . .” Toa
very great extent, this classification applies to Cayce. He possessed
the unique abilities, and once he had established a small following,
primarily confined to his family and close friends, he was then able
to take the traditional language of Christian orthodoxy —“you have
heard it said”—and transform the meaning into the commonplace,
but seemingly revolutionary, interpretations of gnostic mysticism —
“but I say unto you.” Given the enormous popularity of Cayce-
related materials, it would seem that as a charismatic prophet, he
was quite successful.

The reasons why the movement has spread are varied. First, it is
basic to the post-1964 times: anti-establishment, inquisitive, curious
about the unexplained, interested in new avenues of power, desper-
ate to establish new forms of community, Yet at the same time,

98. Max Weber, Exonomy and Society (New York: Bedminster Press, [1924] 1968),
p. 24.
