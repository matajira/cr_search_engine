The World of a Sorcerer 153

tional. !°! When an untrained man faces the nagual, his zonal may die;
he may die. The sorcerer must discipline his rational side not to col-
lapse in the face of control by the irrational, Don Juan offers this cru-
cial bit of instruction: “The goal of a warrior’s training then is not to
teach him to hex or to charm, but to prepare his ¢ena/ not to crap out.
A most difficult accomplishment.”!? But apart from a man’s en-
trance to this magical time, he cannot become a sorcerer. Sorcerers
are beyond the limits of time.

Castaneda’s description of his training is significant: “In my ex-
perience with don Juan I had noticed that in such states [of altered
consciousness] one is incapable of keeping a consistent mental
record of the passage of time. There had never been an enduring
order, in matters of passage of time, in all the states of nonordinary
reality I had experienced, and my conclusion was that if I kept my-
self alert a moment would come when I would lose my order of se-
quential time. As if, for example, I were looking at a mountain at a
given moment, and then in my next moment of awareness I found
myself looking at a valley in the opposite direction, but without re-
membering having turned around.”! He very properly suspected
hypnosis as the cause of his altered perceptions. But hypnosis really
cannot explain anything. It is the deus ex machina used by scholars
to avoid explanations.

Access to such magical timelessness inescapably involves self-
transcendence. “Do you know that you can extend yourself forever in
any of the directions I have pointed to?” asked don Juan. “Do you
know that one moment can be eternity? This is not a riddle; it’s a
fact, but only if you mount that moment and use it to take the total-
ity of yourself forever in any direction.” This, of course, is the
nagual’s time. It is, in the words of the title of Castaneda’s second
book, a separate reality. “For the negual there is no land, or air, or
water. .. . So the nagual glides, or flies, or does whatever it may
do, in nagual’s time, and that has nothing to do with tonal'’s time. The
two things don’t jibe.”!5 The goal is the abolition of time. “ ‘There’s no
future!’ he exclaimed cuttingly. ‘The future is only a way of talking.
For a sorcerer there is only the here and now, ”!% Everything that

101. Tales of Power, p. 159.
102. Ibid., p. 174.
103. Ixtlan, p. 239.
104. Tales of Power, p. 17.
105. Ibid., p. 192.
106. Ibid., p. 206,
