The Crisis of Westen Rationalism AL

least object to the kind of authority that is involved in the idea of irrational-
ism. And that chiefly for two reasons. In the first place, the irrationalism of
our day is the direct lineal descendant of the rationalism of previous days.
The idea of pure chance has been inherent in every form of non-Christian
thought in the past. It is the only logical alternative to the position of Chris-
tianity, according to which the plan of God is back of all. Both Plato and
Aristotle were compelled to make room for it in their maturest thought.
The pure “non-being’ of the earliest rationalism of Greece was but the sup-
pressed “otherness” of the final philosophy of Plato. So too the idea of pure
factuality or pure chance as ultimate is but the idea of “otherness” made ex~
plicit. Given the non-Christian assumption with respect to man’s auton-
omy, the idea of chance has equal rights with the idea of logic.

In the second place, modern irrationalism has not in the least encroached
upon the domain of the intellect as the natural man thinks of it. Irrational-
ism has merely taken possession of that which the intellect, by its own ad-
mission, cannot in any case control. Irrationalism has a secret treaty with
rationalism by which the former cedes to the latter so much of its territory
as the latter can at any given time find the forces to control. Kant's realm of
the noumenal has, as it were, agreed to yield so much of its area ta the phe-
nomenal, as the intellect by its newest weapons can manage to keep in con-
trol. Moreover, by the same treaty irrationalism has promised to keep out
of its own territory any form of authority that might be objectionable to the
autonomous intellect.

The very idea of pure factuality or chance is the best guarantee that no
true authority, such as that of God as the Creator and Judge of men, will
ever confront man. If we compare the realm of the phenomenal as it has
been ordered by the autonomous intellect to a clearing in a large forest, we
may compare the realm of the noumenal to that part of the same forest
which has not yet been laid under cultivation by the intellect. The realm of
mystery is on this basis simply the realm of that which is not yet known,

This “secret treaty” between the scientific phenomenal realm and
the personalistic noumenal realm has one major purpose: to shave God
out of the universe.

Heisenberg and Indeterminacy

This treaty is breaking down in our era. The “not yet known’—
pure randomness—has today reasserted itself with a vengeance in
modern science and philosophy. Heisenberg’s scientific principle of
indeterminacy in physics is first cousin to psychological and philo-

40. Cornelius Van Til, The Defense of the Faith (rev. ed.; Philadelphia: Presbyter-
jan & Reformed, 1963), pp. 124-26.
