Invaders from... ? 317

wise navigators of the cosmos.””? He goes on to say, “if you take the
trouble to join me in the analysis of the modern UFO myth, you will
see human beings under the control of a strange force that is bending
them in absurd ways, forcing them to play a role in a bizarre game of
deception.”7!

Vallee claims to have in his files records of 2,000 cases, not sim-
ply of sightings, but of actual, reported contacts. He claims that it is
normal for the contacts to take place between 6:00 p.m. and 10:30
P.M., no matter where the contact supposedly has taken place. The
peak number of sightings per people actually awake is 3 a.m.” It is
his belief that these appearances are staged. The question is, of
course, “staged by whom?”

Many of the contactees displayed traits that are found in other
kinds of occult phenomena. One of them is by trance. Another is
by some form of automatic writing, A third is peering into some kind
of crystal. The contactees are told to go to some valley and take a
crystal, point the crystal towards the sun, hold their minds still and
wait.”3 Again, the reappearance of quartz or other kinds of crystals
as a means of contacting higher consciousness or higher beings is a
familiar one in other kinds of occult manipulation. Jean Dixon’s
crystal ball or don Juan’s crystal stones are examples.

The fourth means of gathering information about UFO contacts
is through hypnosis. The most famous example of this was made
into a television movie and was based on the 1961 experiences of Mr.
and Mrs. Barney Hill. A book about them by John G. Fuller ap-
peared five years later, The Interrupted Journey (1966). The Hills had
been suffering from psychological problems. When hypnotized by a
psychiatrist, they gave a story about having been carried onto a
spacecraft and given certain information. What is odd is that the in-
formation given to the wife concerning certain stellar formations
turned out to be accurate, even though at the time scientists could
not have verified the information. Three years after the book was
published, Marjorie Fish identified nine of these stars, which are
part of the constellation Reticulum. A triangle of background stars
completed the drawing; she identified them only in 1972. Prior to
1969, the position of these three stars had been erroneously esti-

70. Ibid., p. 20.

71. kdem.

72; Hynek and Vallee, Edge of Reality, p. 8.
73. Messengers of Deception, p. 33.
