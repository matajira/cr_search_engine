60 UNHOLY SPIRITS

of God (Isa. 45:5-12), despite the fact that He is not responsible for
sin. This is also a humanly unanswerable antinomy, and the apostle
Paul specifically called attention to it, and then denied that man, the
creature, has any right to answer it (Rom. 9:19-22). It is an antinomy
in every existing philosophical system, for all systems fragment on
the antinomy of Iree will and predestination, personal responsibility
and unbendable cause-and-effect law. The Christian system presents
a God who is sovereign and man who is responsible. All other exist-
ing systems, as Van Til has demonstrated so well, rest on the presup-
position of a chance universe: chance out of chance returning unto
chance. But there is no zone of ultimate contingency that can serve
man as a “neutral safety zone” for the testing of God’s word. There
are no zones of existence outside His control. “The king’s heart is in
the hand of the Lorn, as the rivers of water: he turneth it whitherso-
ever he will” (Prov. 21:1). Similarly, “A man’s heart deviseth his way:
but the Lorn directeth his steps” (Prov. 16:9), The theme of the hea-
venly potter and his earthly clay—out of which man was created —
appears in both the Old and the New Testaments. “O house of
Israel, cannot I do with you as this potter? saith the Lorp. Behold,
as the clay is in the potter’s hand, so are ye in mine hand, O house of
Israel” (Jer. 18:6). It is this image that Paul uses in establishing the
doctrine of the sovereignty of God (Rom, 9:20-21).

In direct contrast to the biblical view of man and God, the occult
systems, from the magical sects of the East to the gnostics of the early
church period, and from there unto today’s preachers of the cosmic
evolution and irresistible karma, one theme stands out: monism.
There is no Creator-creature distinction. We are all gods in the mak-
ing. Out of One has proceeded the many, and back into One are the
many travelling. Eastern mystics, philosophical Hegelians, and fol-
lowers of the overrated ‘Teilhard de Chardin are all agreed on the
reality of ultimate monism. It is such a convenient doctrine, for it
denies any eternal separation of God and His creation, and therefore
it denies any eternal separation of saved and lost. It denies any ulti-
mate distinction between good and evil, past and present, structure
and change.

In the revival of mysticism, which is invariably monistic,
whether Eastern or Western, the popular culture has produced many
documents that aid the cause, but one of the most successful is Her-
mann Hesse’s pseudo-Eastern book, Siddhartha, Published in 1951, in
the late 1960's it became a touchstone among members of the
