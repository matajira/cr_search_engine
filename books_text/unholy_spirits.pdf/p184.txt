178 UNHOLY SPIRITS

thousands of years; Tryon based his novel on universal themes in
folklore and actual historical practice.) In other instances, moody
children, especially teenage children, seem to come upon their pow-
ers suddenly. Gerard Croiset fits neither of these categories. In his
book on Croiset, Pollack fails to mention anything about the man’s
childhood, but in a 1964 article in True Magazine, he does provide
some basic information, Croiset’s parents were Jewish theater peo-
ple, always on the road. He had no home life. At the age of eight, he
was placed in a foster home. He was to go through six sets of foster
parents over the next five years, until he finally ran away at the age
of thirteen,

Croiset had been an introverted child who lived in a world of his
own fantasies. He was a prodigious daydreamer. He was sickly, and
he suffered from rickets. When he spoke of imaginary playmates and
imaginary places, people no doubt thought him to be a trifle imma-
ture. When he began to describe people and places he had never
seen, but describe them accurately, adults grew concerned for his
sanity. He began to predict events on the basis of his fantasies, and
they would occasionally turn out to be true. “These daydreams
which Gerard didn’t understand, together with his telepathic rela-
tions with people, were the first signs of his paranormal abilities,”
wrote Pollack.?!

Unlike David Hoy, Croiset had little education. He had failed at
several occupations by the time he was out of his teens. In 1935, he
suffered a nervous breakdown at the age of 26. He joined a spiritual-
ist group at one point, but quit shortly thereafter. He says that he
does not believe in spiritualist explanations for odd phenomena. It
was only as his reputation grew as a psychometrist (object-handling
psychic), from 1937-40, that he began to believe that he had some-
thing unique to offer the world.

Croiset’s abilities may be questioned by professional skeptics, but
police departments throughout Europe are not so skeptical, and fami-
lies who have been helped in locating lost children or lost property are
not skeptical. The chair tests have also added to his reputation. He
has become a prophet in his own time. To know in advance when and
where a body will float to the surface of a canal is a startling sort of
knowledge. There is little doubt that the man’s talents are unique.

Modern scientists, not being able to deal with such phenomena
in terms of the categories of pure rationalism, have preferred to ig-

 

 

jue (April 1964), p. 125.
