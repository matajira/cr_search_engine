Escape from Creaturehood 341

which the whole of creation, at the end of terrestrial time, will find its
accomplishment, consummation and exaltation, Man is capable of
anything. His intelligence, equipped from the very beginning, no
doubt with a capacity for infinite knowledge, can in certain condi-
tions apprehend the whole mechanism of life. The powers of human
intelligence, if developed to their fullest extent, could probably cope
with anything in the whole Universe. But these powers stop short at
the point where the intelligence, having reached the end of its mis-
sion, senses that there is still ‘something other’ beyond the confines of
the Universe. Here it is quite possible for an analogical conscious-
ness to function, There are no models in the Universe of what may
exist outside the Universe. The door through which none may pass
is the gateway to the Kingdom of Heaven.”

In this universe, man can cope with anything it has to offer.
Man, in good Kantian fashion, stands reverently at the door to the
Kingdom of Heaven, making sure that nothing over there bothers us
over here. If something there gets into the Universe, however, it can
be coped with, The mind of man will see to that.

What we are witnessing is the coming of an elite corps of mu-
tants. Some have come before us: Jesus, Confucius, Mohammed. An
international secret society of these mutants now exists. Salvation is
coming —~ metaphysical transformation through the techniques of ap-
plied mutation. Pauwels and Bergier are optimistic, for they promise
gnostic salvation. Man’s very mind is the agent of salvation; salva-
tion comes through knowledge (Socrates’s famous dictum): “We are
on the side of the invaders, on the side of the life that is coming, on
the side of a changing age and changing ways of thought. Error?
Madness? A man’s life is only justified by his efforts, however feeble,
towards better understanding. And to understand better is to
become more attached. The more I understand, the more I love; for
everything that is understood is good.” ,

Gnostic Salvation

The ideal of exhaustive knowledge is placed before mankind. If
mankind, or an elite secret society of mutants, can attain exhaustive
understanding, sin and the effects of sin will be erased from the uni-
verse. Man will be lord of creation. Nothing human is foreign to the
alchemist, and nothing understood can be evil. This is the ancient

27. Ibid., p. 341.
28. fbid., p. 46.
