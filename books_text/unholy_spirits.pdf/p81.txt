3
PARANORMAL SCIENCE SINCE 1960

We have largely evicted superstition from the physical universe, which
used to be the dumping ground of the miraculous. . . . But we have great
ground to rejoice that sctence ts now advancing into this domain more rap-
idly than ever before, and that the last few years have seen more progress
than the century that preceded. The mysteries of our psychic being are
bound ere long to be cleared up. Every one of these ghostly phenomena will
be brought under the domain of law. The present recrudescence here of an-
cient faiths in the supernatural is very interesting as a psychic atavism, as
the last flashing up of old psychoses soon to become extinct.

G. Stanley Hall (1910)!

Science no longer holds any absolute truths. Even the discipline of
bhystcs, whose laws once went unchallenged, has had to submit to the in-
dignity of an Uncertainty Principle. In this climate of disbelief, we have
begun to doubt even fundamental propositions, and the old distinction be-
tween natural and supernatural has become meaningless. I find this tre-
mendously exciting.

Lyall Watson (1973)?

The official position of the vast bulk of scientists in the academic
world as recently as 1959 was far closer to G. Stanley Hall’s presenta-
tion of the “facts” than Lyall Watson’s. Indeed, the majority of ten-
ured physical and biological scientists today would probably prefer

to be categorized as followers of Hall. But the naive confidence of

Hall, writing as he did prior to the discovery of the principle of in-

determinacy by Werner Heisenberg and prior to the development of

1. G. Stanley Hall, “Introduction,” Amy Tanner, Studies in Spiritism (New York:
Appleton, 1910), p. xxi.

2. Lyall Watson, Supernature: A Natural History of the Supernatural (Garden City,
New York: Anchor Press/Doubleday, 1973), Introduction, p. ix.

75
