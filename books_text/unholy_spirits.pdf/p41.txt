The Crisis of Western Rationalism 35

The Loss of Confidence

David Hume, in the middle of the eighteenth century, provided
the classic answer of the skeptic: natural law really does not exist as a
force independent of man’s mind. Natural law is nothing more than
the agreement among men that certain actions follow necessarily
from prior actions. Cause-and-cffect relationships, in other words,
are nothing but conventions. The sensation of pain when I thrust my
finger into boiling water may have no relation to that water. I may
experience pain cach time, but experience is not the same as rigor-
ous mechanical law. We simply call certain events effects of prior
events (the causes). Nominalism, that is, the denial of the existence
of metaphysical forms which order nature, had grown to maturity in
the philosophy of Hume. Whirl once again became king, despite the
fact that men naively think that the laws or conventions of their
minds relate in some way to a hypothetically lawful universe out
there beyond our senses. Law becomes convention. As a result, con-
fidence turns into skepticism.

Men do not normally choose universal skepticism, but Hume’s
arguments seemed to make it impossible to avoid such a choice.
Hume’s arguments were useful in refuting dogmatic theology, so his
skepticism could be used against eighteenth-century Christianity,
but the price paid for this anti-Christian weapon soon proved to be
too high, Such was the conclusion of the philosopher who still stands
as the philosopher of the modern world, Immanuel Kant.

Hume's ideas, Kant later wrote, awoke him from his dogmatic
slumbers, but the decade of his conversion to Humean skepticism
(1763-72) led him to search for certainty, and certainty clearly was
not to be found in the contingent rules of human experience. On
that, Hume and Kant were in agreement. First comes dogmatism,
then skepticism, and finally, Kant said, truly critical certainty.
“Scepticism is thus a resting-place for human reason, where it can
reflect upon its dogmatic wanderings and make survey of the region
in which it finds itself, so that for the future it may be able to choose
its path with more certainty. But it is no dwelling-place for perma-
nent settlement. Such can be obtained only through perfect certainty
in our knowledge, alike of the objects themselves and of the limits
within which all our knowledge of objects is enclosed.”* To find a
priori certainty, Kant concluded, men must be humble. They must

34. Trmnanuel Kant, Crifigue of Pure Reason, A 761 (B 789), p. 607.
