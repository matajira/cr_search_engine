Escape from Creaturehood 355
out.”85 He quotes John Yepes (St. John of the Cross; d. 1591):

For the soul courageously resolved on passing, interiorly and exteriorly,
beyond the limits of its own nature, enters illimitably within the super-
natural, which has no measure, but contains all measure imminently within
itself. To arrive there is to depart hence, going away, out of oneself, as far as
possible from this vile state to that which is highest of all. Therefore, rising
above all that may be known and understood, temporally and spiritually,
the soul must earnestly desire to reach that which in this life cannot be
known, and which the heart cannot conceive; and, leaving behind all actual
and possible taste and feeling of sense and spirit, must desire earnestly to
arrive at that which transcends all sense and all feeling. . . . The more the
soul strives to become blind and annihilated as to all interior and exterior
things, the more it will be filled with faith and love and hope.

The Void

This distinctly Eastern view of reality—the void—is a common
feature of all monistic systems of mysticism. Van Til calls the process
the “integration into the void.” With reference to modern psychology,
he writes: “The real reason why modern psychology has left no room
for responsibility is found in the fact that it has taken the whole of the
human personality in all its aspects, self-conscious and sub-
conscious, and immersed it in an ultimate metaphysical void. Man
cannot be responsible to the void.”*? It should surprise no one to
learn that the founders of modern psychology, Freud and Jung, were
both keenly interested in occult subjects, that Freud once remarked
that he regretted not having devoted more of his life’s work to a study
of ESP, and that Freud was convinced at one stage of his career that
cocaine would be a wonder drug for psychological therapy. Salva-
tion is in the void, beyond the categories of rationality, beyond the
pressures of created reality.

The void need not be empty to function as a void. It may not be
Buddhist Nirvana—the abolition of all existence — but if it is devoid
of structure or meaning, it is a void. The experiences of the so-called
higher consciousness remove men from the realms of created reality

55. Ibid,, p. 194

56. {bid., pp. 145-46.

57. Cornelius Van Til, Psychology of Religion, Volume IV of In Defense of the Faith
(Phillipsburg, New Jersey: Presbyterian & Reformed, [1961] 1971), p. 73.

58. Nandor Fodor, Freud, jung, and Occultism (New Hyde Park, New York: Uni-
versity Books, 1971)
