Edgar Cayce: From Diagnosis to Gnosts 221

auras are visualized vibrations.“

Nevertheless, the most important teaching connected to the vi-
bration theory is that God is a vibration. Understanding this fact, the
trance voice informed its listeners, is the key to one’s personal salva-
tion. This is the soteriology (doctrine of salvation) of vibrations, “For
without passing through each and every stage of development, there
is not the correct vibrations to become one with the Father. . . . Then
in the many stages of development throughout the Universe or in the
great system of the Universal Forces, each stage of development is
made manifest through the flesh — which is the testing portion of the
Universal Vibration. In this manner, then, and for this reason, all
are made manifest in flesh and there is the development through
aeons of time and space, called Eternity.”*! Vibrations, in short, are
the key to evolution. They are also related to astrological influences:
“Each planetary influence vibrates at a different rate of vibration. An
entity entering that influence enters that vibration: (it is) not neces-
sary that he change, but it is the grace of God that he may! It is part of
the Universal Consciousness, the Universal Law.”? Nevertheless, it is
not the planets that determine life; it is the will of man. The voice was
very specific on that point: “But let it be understood here, no action of
any planet or any of the phases of the sun, moon, or any of the hea-
venly bodies surpass the rule of man’s individual will power. . . .”®
God, the Great Vibration, is secondary at best, probably tertiary,
and possibly not half so powerful as the color green.

Garbled Messages

One problem faced by all occult mediums is that the messages
trom “the other side” are often incoherent. It is as if the wisdom of
the spirit world gets short-circuited from time to time, leaving the re-
cipients of this wisdom with the vague feeling that the spirits have
been drinking steadily for several hours. Yet the language is always
very profound, totally self-assured. For example:

First, the continuity of life. There is no time; it is one time; there is no
space; it is one space; there is no force; it is one force; other than all force in
its various phases and applications of force are the emanations of men’s

90. Agee, ESP, p. 69.

91. Furst, jesus, p. 57.

92. Idem.

93. Agee, ESP, p. 54; cf. Appendix B on astrology in Furst, Jesus.
