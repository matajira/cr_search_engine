The Biblical Framework of Interpretation 69

opened the eyes of the young man; and he saw: and, behold, the
mountain was full of horses and chariots of fire round about Elisha.
And when they came down to him, Elisha prayed unto the Lorp,
and said, Smite this people, I pray thee, with blindness. And he
smote them with blindness according to the word of Elisha” (II Ki.
6:16-18). Blindness among the enemies was also the method of free-
ing Lot from Sodom (Gen. 19:11). It was not that the angels used
their chariots of fire or great numbers in order to impede the plans of
the Syrian army, but their presence was meant to assure Elisha of his
own safety.

One of the difficulties in describing various kinds of angels is that
the biblical writers would see them in visions, and they themselves
were well aware of the non-literal nature of some segments of these
visions — the candlesticks of the Book of Revelation, for example, or
even the great chain which binds Satan (Rev. 20:1). The idea of
chains and spirits may make fine reading in Dickens’ A Christmas
Carol, but this should not be confused with the spiritual nature of
God’s bondage of demons. Therefore, Danicl’s description of the
cherubim ~ four-faced creatures, part beast and part human, with
four wings— may reflect only God’s means of revealing the majesty
and power of His angels (Ezk. 10:10-21). John’s vision of the sera-
phim is analogous: four beasts —“like a” lion, calf, man, and eagle,
each with six wings—crying “Holy, holy, holy, Lord God almighty”
(Rev. 4:7-8). When children, or adults for that matter, sing the
hymn, “Holy, Holy, Holy,” they probably do not spend a lot of time
thinking about the original source of the lyrics. These could con-
ceivably be the permanent shapes of these various angels, but in the
references to their appearances before men (outside of visions), they
take less shocking shapes. Obviously, one would be unlikely to enter-
tain these visionary angels unawares.

There are some Old Testament references that indicate that God
takes the form of men-angels from time to time. These revelations of
himself are usually called theophanies. We are told that God walked
in the garden of Eden (Gen. 3:8), that He appeared in human form
before Abraham at Mamre (Gen. 18:1-2, 22), and that He wrestled
through the night with Jacob, alterward providing him with his new
name, Israe] (Gen. 32:24-28). The most famous theophany in the
Bible, the appearance of God before Moses in a burning bush, actu-
ally states that “the angel of the Lorp appeared unto him in a flame
of fire out of the midst of a bush” (Ex. 3:2), yet the voice clearly
