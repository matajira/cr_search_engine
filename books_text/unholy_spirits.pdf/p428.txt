422 UNHOLY SPIRITS

Robbins, Rossel H., 47
Roberts, Oral, 269-70
Robespierre, 7

rodents, 97

rods (dowsing), 93-94, 98
RolRer, Products, 171-2
Rolling Stone, 9

Rolling Stones, 8-9, 11
Roman Empire, 13, 378, 399
ropes, 161

Rose, Ronald, 251-52
Rosemary's Baby, 7

Roszak, Theodore, 366
routines, 135, 136, 137, 155
“Rubber Soul,” 6

Ruppelt, Edward, 305
Rushdoony, 2, 67, 155, 189, 377
Russia, 87-94

Ryzl, Milos, 112

 

sacrifice, 67
Sagan, Carl, 302
Sage, Wayne, 116-17, 118, 119
Sale, Kirkpatrick, 366
Salk, Jonas, 366
salvation
Cayce, 216-18, 221
consciousness, 354
from history, 346
gnostic, 216-48, 335, 341-43
humanistic, 63
reincarnation, 254
self-salvation, 37, 217-18, 225, 301,
335
UFO's, 303, 305, 316
vibrating god, 221
void, 355
Salza, Andrew, 109-10
Sanderson, Ivan, 54
sanitation, 248, 256
Satan
angel of tight, 50
appearances, 330
backward-looking, 370-71
centralization, 369
compact, 295
fall of, 70

limited knowledge, 58.
religion, 71
ruler, 40
society of, 376-77
teleportation, 160
Satanism
escape from finitude, 335
magic, not ethics, 334-35
quest for power, 282-83
Sauvin, Perre, 107
Sceva, 73
Schoeck, Helmut, 279-80
scholarships, 276-77
scholasticism, 32
schools, 363, 375, 385
schizophrenia, 30
Schmidt, Helmut, 116, 121
Schuller, Robert, 392
science
anti-teleology, 165
arbitrariness, 242
autonomy, 324, 326
coherence (loss of}, 144
conspiracy, 114-15
creation doctrine, 132
dilemma, 326
division of labor, 123, 336
“empirical,” 324
evidence, 39, 118
facts, 239
journalism, 246
journals, 245
laws, 43
New Age, 82-87
open-minded, 324
optimism, 22
phenomena, 321
progress, 247
public’s skepticism, 326
Reformation, 336
relativism, 144-45
revolutions, 23-24, 122-23, 242-43,
247
rigor as religion, 119
signals, 118-19
skepticism, 179
strategy, 100
