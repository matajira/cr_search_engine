20 UNHOLY SPIRITS

absolutely accurate term, “multiversity,” was trying to produce qual-
ity mass education. “With his talent for achieving a consensus, the
chances are that he will succeed. If he fails, the future will look bleak
not only for his own university but for all higher education in Amer-
ica.” You would be hard-pressed to find a more prophetic statement
in the annals of popular journalism, for Kerr did indeed fail to gain
consensus, and so did his bureaucratic counterparts on the nation’s
prestigious university campuses. The fabric of education was torn to
shreds institutionally, and a decade later, the leading educational
theorists were still in shock. Ironically, the original article had been
called, “The Exploding University of California.” The author had
been speaking of size, but the phrase was really appropriate just one
semester later.

The second article, “I Ama U. C. Student: Do Not Fold, Bend
or Mutilate,” appeared two years later (June 18, 1966). No longer
was Berkeley the smug center of institutional conservatism, the place
where “no important reforms are necessary.” All that was long gone.
“Berkeley is surely the leader. It is the most serious and committed to
change. Berkeley is, as they say, ‘out of sight.’ It is the model of the re-
bellious enclave within the affluence of mid-century America.”
Shortly thereafter, Clark Kerr was dismissed by the Board of Reg-
ents of the University, He left with his sense of humor, disappearing
from the public’s hostile eye with this final occupational epitapb: “I
am leaving this job just as I entered it: fired with enthusiasm.”

Yet this was not the final act for Berkeley. Again it would serve as
a model, for after 1970 a new series of weird phcnomena appeared:
pseudo-Eastern mystics, outright occultism and witchcraft, and the
Jesus (Freaks) People. There was also a revival of fraternities and
sororities, which had fallen on hard times, 1965-70. There was a shift
from hard drugs to marijuana and cheap wine, and an almost total
internalization of concern—personal salvation, grade point average,
professional training, grad school. The Old Grad, class of 63, would
have been more comfortable on campus in 1973 had he returned to
his alma mater.

Pessimism and Soctal Disorder

Nevertheless, there had been a subtle psychological shift: the op-
timism of technocratic liberalism, so fundamental to university life
from 1945-64, had become subducd. The older optimism concerning
the benefits of liberal arts training, the possibilities of future employ-
