Invaders from... ? 289

the time, he was sheltered in a concrete capsule 60 feet below the
ground, While he was trying to clear up the problem, other Air
Force personnel on the surface reported seeing a UFO— an uniden-
tified flying object ~- high in the sky. It had a bright red light, and it
appeared to be alternately climbing and descending. Simultan-
eously, a radar crew on the ground picked up the UFO at 100,000
feet... . ‘When the UFO climbed, the static stopped, stated the
report made by the base’s director of operations.” Hynek reported
that they saw it land ten to fifteen miles south of the base, but when a
team was sent to investigate, their radio contact was drowned in
static. Another UFO was spotted in the vicinity.?

When communications are disrupted at a nuclear missile base,
and when the base’s radar spots a vehicle close to it at 100,000 feet,
we need to take such phenomena seriously, What if a whole radar
screen full of such blips should appear, all headed toward SAC bases
and missile sites? There is little doubt at the probable result. World
War III would begin.

It came close on October 5, 1960. A formation of unidentified fly-
ing objects was picked up on U.S. radar screens at the early warning
station at Thule, Greenland. These objects appeared to be headed for
the U.S.A. Their origin seemed to be from the Soviet Union. The
Strategic Air Command in Omaha was alerted. The B-52 crews
were racing toward their planes at airfields all over the world. To
confirm, SAC broadcast a signal to Thule. No answer. “Suddenly
the mysterious blips on the radar screen changed course and disap-
peared.” Later, the U.S. government issued an official explanation
for the loss of communication: an iceberg had cut the submarine ca-
ble to the U.S. (Our survival communications link is dependent on a
“submarine cable”?) No explanation for the blips on the radar screen
was offered until three members of Parliament in Britain demanded
one. The Air Force replied that radar signals had actually bounced
off the moon and had been misinterpreted. The story was (predic-
tably) buried in the New York Times on page 71.*

If our radar signals are that easily misinterpreted, we have a ma-
jor high-risk problem. If they are not that easily misinterpreted, then
what caused the radar to give this sort of signal? Do we have another

3. Reprinted in Jay David (ed.), The Flying Saucer Reader (New York: Signet,
1967), pp. 213-14.

4. John Keel, UFOs: Operation Trojan Horse (New York: Putnam's, 1970), pp.
13-14,
