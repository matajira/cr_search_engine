The Biblical Framework of Interpretation 7

was their place found any more in heaven. And the great dragon was cast
out, that old serpent, called the Devil, and Satan, which deceiveth the
whole world: he was cast out into the earth, and his angels were cast out
with him. And I heard a loud voice saying in heaven, Now is come salva-
tion, and strength, and the kingdom of our God, and the power of his
Christ; for the accuser of our brethren is cast down, which accused them
before our God day and night. And they overcame him by the blood of the
Lamb, and by the word of their testimony; and they loved not their lives
unto death. Therefore rejoice, ye heavens, and ye that dwell in them. Woe
to the inhabitants of the earth and of the sea! for the devil is come down unto
you, having great wrath, because he knoweth that he hath but a short time
(Rev. 12:7-12).

The Book of Job indicates the nature of Satan’s accusing tongue.
After the crucifixion of Christ, that tongue could no longer be wag-
ged before the throne of God. The brethren overcame him, with the
cooperation of Michael and the angels, through the blood of the
Jamb. This was an historical event. It did not take place in some
misty pre-Adamic period, nor did it take place in Old Testament
times.’ The ethical rebellion did take place, obviously, before the
Fall of man, although we are never told just when. (My own guess is
that it was on the morning of the seventh day, since God pronounced
his blessing upon creation on the sixth day, and Satan is certainly a
created being. But this is mere speculation.)

The results of that ethical rebellion have been applied steadily to
Satan and his host over time. The religion of Christianity, like that of
orthodox Judaism, is a religion of time. Understandably, the essence
of Satan's religion is a denial of time, for time seals his doom and is
an ever-present reminder to him that he has been vanquished and
will ultimately suffer total, eternal defeat (Matt. 25:41; Rev. 20:14).
Time must either be denied as being real or else be extended infi-
nitely in each direction, thereby denying the final judgment. Again
and again, we find in occult speculation, whether Eastern or West-
ern, ancient or modern, a denial of the reality of linear, bounded time
—the uniquely Western tradition which was first presented in its

16. The passage usually cited to show that the event was prior to New Testament
times is Tsaiah 14:12-15. This interpretation is as old as the early church fathers. But
the Hebrew word translated “Lucifer” merely meant “morning stat” or “bright star,”
and its position in a passage dealing with the coming destruction of Babylon in-
dicates that, at best, it is only analogous to Satan’s fall. Verse 16 clearly refers to “the
man that made the carth to wemble. . . ." Cf. J. A. Alexander, Commentary on the
Prophecies of Isaiah (Grand Rapids: Zondervan, [1865] 1974), pp. 295-98.
