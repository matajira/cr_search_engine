156 UNHOLY SPIRITS

Well, look around. People are busy doing that which people do. Those
are their shields. Whenever a sorcerer has an encounter with any of those
inexplicable and unbending forces we have talked about, his gap opens,
making him more susceptible to his death than he ordinarily is; I've told
you that we die through that gap, therefore if it is open one should have his
will ready ¢o fill it; that is, if one is a warrior. If one is not a warrior, like
yourself, then one has no other recourse but to use the activities of daily
life to take one’s mind away from the fright of the encounter and thus to
allow one’s gap to close, . . . At this time in your life, however, you can no
longer usc those shields as effectively as an average man. You know too
much about those forces and now you are finally at the brink of feeling and
acting as a warrior. Your old shields are no longer safe.1"8

When danger approaches, Castaneda is told to write. Take
notes, write, concentrate: here is his old rationalistic activity, and it
is a very Western activity. His notcbook will act as his shield. It is the
only one he has left. His mind has been shattered by what he has
seen, whether in visions or in the phenomenal world. He has left the
world of Western rationalism, and he is in danger of destruction by
real beings that wield tremendous power.

Western man is progressively losing his shields. Since 1965, loss
of faith in traditional nineteenth-century philosophical mechanism
has been obvious. People are steadily learning about the paranormal
universe around them, even as Elisha’s servant learned to see the
angelic host around him (II Kings 6:16-18). An Associated Press
story released in May 1974 is symptomatic; and the reaction of estab-
lishment scientists, clinging desperately to the world of simplistic
ninetcenth-century thinkers, is typical—and futile:

NEW YORK -— The scientific elite were somewhat taken aback on be-
ing informed that amid all the madern technological advances, a new, na-
tional study shows people are believing more and more in the active reality
of the devil.

When told of it, participants in a recent mecting in San Francisco of the
Amcrican Assn. for the Advancement of Science “were absolutely shocked,”
says Clyde Nunn, a social researcher who reported the findings to them.

“Et didn’t fit their presuppositions,” he adds. “It was mind-blowing for
them.”

He said the fact that Americans are increasingly convinced of the devil's
existence runs counter to the scientific community’s general assumption of

N15. Castaneda, Separate Reality, pp. 216-17.
