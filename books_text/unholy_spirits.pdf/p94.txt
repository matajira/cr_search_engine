88 UNHOLY SPIRITS

official Soviet publication (and that means any legal Soviet publica-
tion), to the very early 1960’s, old-fashioned scientific orthodoxy
reigned supreme. By 1965, as in the United States, the dam had
broken. The scientists of the Soviet Union were tentatively being
offered a whole new world to explore. Soviet scientists, bit by bit,
began to make their peace with the hitherto banned data of the oc-
cult. No doubt such a shift in scientific opinion was easier for Soviet
scientists than it has been for Americans: the state paid them to
change their minds, and in the Soviet Union the discovery of eternal
truths inside previously prohibited facts is a familiar process. The re-
vision of encyclopedias in the Soviet Union is literally a way of life.

Telekineses and Telepathy

In 1959, reports hit the USSR (but not the USA) that the U.S.
Navy had been experimenting with telepathy. The submarine Naut:-
lus had supposedly been trying ESP techniques (or magic, or not-
yet-knownness) as 2 means of communication between underwater
points, something thought to be impossible by presently known
scientific techniques. Leopold Vasiliev, a highly respected Soviet
physiologist (who had for years been running a highly secret labora-
tory for parapsychological research), warned fellow scientists that
the USSR should not fall behind the USA in the field of paranormal
science. He stated himself forthrightly: “The discovery of the energy
underlying ESP will be equivalent to the discovery of atomic
energy.” Like any well-salaried employee of a messianic state, Vasi-
liev saw his opportunity and he took it. Call attention to the national
enemy (the United States), point to a secret new discovery that holds
out the promise of almost unlimited power, and make certain that
the authorities bankroll it, since it just happens to be one’s own area
of expertise. In the United States, this technique is called “grants-
manship.” Vasiliev got what he wanted: access to research money
and, perhaps even more important, access to the hitherto closed
pages of the academic journals. A special ESP laboratory was estab-
lished under his direction at Leningrad University.

By far the most prominent human subject of Soviet research into
telekinesis — mind over matter —has been “Nelya Mikhailova” (Nina
Kulagina), a Leningrad housewife of peasant background. Over forty
top Soviet scientists have tested her abilities. After two to four hours

36. [id., p. 7.
