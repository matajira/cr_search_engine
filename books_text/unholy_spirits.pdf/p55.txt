The Crisis of Western Rationalism 49

serious intellectual investigation; it is the first step in any scientific or
historical enquiry. Yet at the same time, the researcher must be
humble. The data can fool him, especially data provided by “true be-
lievers,” confidence men, and serious but overenthusiastic research-
ers on the fringes of orthodox science. The data may be of “phenom-
enal” rather than “noumenal” origin; it may be a question of simply
not having enough data or not having discovered some new set of
rules that can deal with these perplexing data. But in some cases, the
data will truly reflect their supernatural origin. The question which
faces each man who encounters such data should therefore be: What
should I do about my encounter with the supernatural? And if you
are not a Christian, and therefore not the beneficiary of special pro-
tection, the answer is twofold: 1) run; 2) repent and be converted.
