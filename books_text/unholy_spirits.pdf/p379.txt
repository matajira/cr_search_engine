Escape from Creaturchood 373

This does not mean that there will not be competition and even
social conflict among the various groups that seek control. No doubt
there will be. This also does not mean that the United States is uot
going to suffer judgment. It may have to suffer judgment as a pre-
lude to national repentance, if it is to play a positive role in the proc-
ess of worldwide Christian reconstruction. But we need to recognize
that there are two kinds of judgment in the Bible: judgment unto res-
toration and judgment unto destruction. With respect to God's
covenanted people, the Bible always speaks of the first form of judg-
ment, unto restoration, It never speaks of judgment unto historic obli-
vion when it speaks of the remnant of the faithful. Read Isaiah chap-
ters one and two for the outline of prophetic preaching concerning
judgment. “And I will restore thy judges as at the first, and thy coun-
sellors as at the beginning: afterward thou shalt be called, The city of
righteousness, the faithful city. Zion shall be redeemed with judg-
ment, and her converts with righteousness” (Isa. 1:26-27). Could
anything be plainer? “Restore,” “redeemed”: Isaiah’s language is
crystal clear. Isaiah then goes on in the second chapter to describe
this spiritual restoration. The church of Jesus Christ is the lawful inhenitor
of these promises, for Paul calls the church “the Israel of God.” “And as
many as walk according to this rule, peace be on them, and mercy,
and upon the Israel of God” (Gal. 6:16).

To put it as clearly as 1 can: Armageddon happens to the enemies of
Christ's church, not to the faithful. The conquest of “Canaan” is what
God’s people accomplish, by means of the gracious intervention of
God, not by His physical presence, but spiritually, as His people
work out their salvation with fear and trembling (Phil. 2:12). In
short, God-fearing people conquer the world by preaching the gos-
pel, exercising discipline under God in terms of His law, and re-
deeming—buying back— modern civilization. It takes time, but
time is on our side. It is not on Satan’s side. God owns the world, not
Satan.

This leads us to the question of competence. Whose world- and-
life view is more oriented toward dominion? Whose tool of dominion
—whose law-order—has greater authority, God’s revealed law or the
“gero-blueprint” program of the New Agers? Whose kingdom is
promised in the Bible to be the victorious one, in time nd on earth,
before Christ returns visibly to execute final judgment? 'The answer,
contrary to those who preach “eat, meet, retreat, and wait for the
