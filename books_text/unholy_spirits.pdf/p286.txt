280 UNHOLY SPIRITS

The efficacy of demonic magic is strong in these non-Christian
cultures. The fear of magic is pervasive. Thus, the threat of its use
against the truly successful man causes men with talents to conceal
them from their fellows. Men become secretive about what they
own. They prefer to attribute any personal successes to luck or fate,
both impersonal. “Institutionalized envy . . . or the ubiquitous fear of
it, means that there is little possibility of individual economic
achievement and no contact with the outside world through which
the community might hope to progress. No one dares to show any-
thing that might lead people to think that he was better off. Innova-
tions are unlikely. Agricultural methods remain traditional and primi-
tive, to the detriment of the whole village, because every deviation
from previous practice comes up against the limitations of envy.”*

Furthermore, Schoeck writes: “It is impossible for several fami-
lies to pool resources or tools of any kind in a common undertaking.
It is almost equally impossible for any one man to adopt a leading
role in the interests of the village.”!° While Schoeck does not discuss
it, the problem of institutionalized envy and magic for the establish-
ment of republican institutions in primitive cultures is almost over-
whelming. Once a chief's link to traditional authority is officially de-
nied by secular educators in a culture, who is to lead? If a man can-
not point to a long tradition of authority or his semidivine status as a
leader, who is to say who is to lead? Whoever dares to proclaim him-
self as leader had better be prepared to defend his title not merely at
the polling booth, but from envious magic. In a culture in which the
authority of traditional rulers has been eroded by Western secular-
ism and Western theories of individualism and democracy, the ob-
vious alternative to traditional authority is military power.

In April 1975, the tiny, poverty-stricken African “nation” of Chad
had a military coup. Months later, the news media briefly picked up
the story of the deposed leader, Ngarta Tombalbaye, who had been
executed by the military. He had imported Haitian witch doctors—
one of Haiti’s few products that can compete successfully in the in-
ternational division of labor—and had used Radio Chad to broadcast
what were purported to be spirit messages. Men in every station of
life were told to return to their tribal villages and participate in initi-
ation rites from Chad’s Stone Age past. (Chad’s Stone Age, chrono-
logically speaking, ended a couple of centuries ago.) From 1962,

9. Ibid., p. 47.
10. Ibid., p. 48.
