142 UNHOLY SPIRITS

atives— Jesus Christ, the Virgin Mary, Our Lady of Guadalupe—
indicate his contempt for their lack of visible power. But they also in-
dicate how frivolous was his relatives’ worship of these occasional
church visitors. He contrasts their worship of the traditional Mex-
ican deities with the worship demanded by Mescalito, the god of
peyote. “If they were real protectors they would force you te listen,”
he argues. “If Mescalito becomes your protector you will have to
listen whether you like it or not, because you can see him and you
must take heed of what he says. He will make you approach him
with respect. Not the way you fellows are accustomed to approach
your protectors.”5! Power demands respect.

The problem with power is always the same: the user is simul-
taneously subjected to it, The man who wields the scientific power of
the modern world must have a theory of the transmission of power.
If causes have effects, then by becoming an intermediary cause a
man must admit that his decisions are also effects of prior causes. If
he denies that he is necessarily determined, then he must also deny
his own power to determine certain effects. Similarly, if a magician
uses the power of an ally or demon to produce certain effects, he in-
evitably places himself under the power of the ally. At the very least,
he is subjected to a rigorous series of rituals that must be used when
calling forth occult power. To command power~ any power—is to
acknowledge the sovereignty of the source of that power, whether
God, demons, natural law, random variation, or whatever. Men will
serve that which they believe to be sovereign,

At the very end of his training, Castaneda is warned of his new re-
sponsibilities by his teacher: “The fate of all of us here has been to
know that we are the prisoners of power. No one knows why us in par-
ticular, but what a great fortune.”5? Simply and less enthusiastically
put, “We are dregs in the hands of those forces.”*> Ghosts, spirits,
mysterious forces: these are the gods of the world of the sorcerer.

Since the attainment of the gnosis was basic to the attainment of
power, and power involved Castaneda in a new and fearful world,
what kinds of power did he actually see or experience that would
seem to have made the bargain worthwhile? As a result of the vari-
ous drugs, he did achieve several nonrational experiences, a few of

51, Separate Reality, p. 64.

52. Tales of Power, p. 280.

53, Ixtlan, p. 88.

54. Separate Reality, pp. 232-34.
