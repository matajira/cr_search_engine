256 UNHOLY SPIRITS

Arigo, Terte, and all the rest: the lack of sanitation. These healers
have an almost philosophical commitment to unsterile operations.
This is not to say that people are actually infected. Indeed, from the
reported cases of infection —virtually none—there is far less infec-
tion in one of these primitive clinics than in a typical North Ameri-
can hospital, with its staph infections and other horrors. The bodies
of those treated are not infected, but what is infected is the attitude
of the public toward standards of cleanliness. The lack of sanitation
is a visible testimony against the law of cause and effect in the medi-
cal realm, The relationship between cleanliness and health is sym-
bolically rejected by these healers. Those peasants who are treated
by them remain in a kind of microbiological bondage to their primi-
tive culture. They remain dependent upon the charismatic healers
who alone seem to be able to provide efficacious treatment within the
framework of the traditional culture to which these people are cling-
ing. These static cultures are kept static, in part, by the very success
of these healers. As one Western physician remarked during a televi-
sion documentary on a group of doctors who use light aircraft to
bring Western medicine to African tribes: “I’m always the second
doctor called in for consultation.”

Agpaoa normally treats 30 to 50 people per day, seven days a
week. His absolute maximum was Arigo’s average day: 300.5 His
operations generally take as “long” as five minutes, five times as long
as Arigo’s. His trance state, like Terte’s, affects only his arms and
hands.” When the power comes upon him, he says, there is a tingl-
ing sensation—a phenomenon which is reported as preceding num-
erous forms of demonic activity. Then he goes to work.

Occasionally, one of these healers will use a razor blade, but nor-
mally they use only their bare hands to enter the body. Juan Blanche,
as reported earlier, uses the index finger of an onlooker to make the
slit in the flesh—eight inches away from the body. Then they enter
the cavity of the body with their hands.* Typically, the healer begins
by massaging the flesh for a few moments, When it opens, he inserts
his hands and makes the necessary “repairs.” Sometimes they may
use a pair of old scissors to cut the internal organs; other times, most

 

54. Sherman, pp. 24, 58, 61, 192.
55. Tbid., p. 59.

56. Ibid., p. 163.

57. Ibid., p. 68,

58, Thid,. pp. 54, 288.
