104 UNHOLY SPIRITS

His scholarly papers were suppressed, time after time, by the Royal
Society, although the Linnean Society was willing to publish some of
them. Too much carbon dioxide, he found, affected plants the way
that too much liquor affects men: they swayed, passed out, and
finally revived.” One instrument, the crescograph, measured in-
credibly minute growth rates among plants, and Bose claimed that
by merely touching a plant it is possible to retard its growth rate in
some cases.

The authors quite properly identify what Bose’s work involved:
“an amalgamation of the wisdom of the ancient East with the precise
scientific techniques and language of the modern West.”® This is one
of the most important of ali occult goals: the fusion of rationalism
and irrationalism, of mysticism with science. It is, in the words of
C. 3. Lewis’ fictional demon Screwtape, the goal of discovering the
materialist magician: the man who believes in occult forces and
scientific power, but not in God. When he appears, “the end of the
war will be in sight.”

In the 1920’s, at the time Bose was continuing his researches,
Soviet scientist Alexander Gurwitsch conducted a series of experi-
ments with the roots of onions. He thought it possible that some
form of energy linked onion plants. He built an apparatus that ex-
posed portions of the roots of two onions to each other by means of a
glass tube. After three hours of exposure, he said he was able to de-
tect significant increases (twenty-five percent) in the growth rate of
the exposed section of the “receiving” onion’s root.7! He tried yeast as
a receiver. It increased its growth rate by thirty percent.”2 Several
European studies claimed to have duplicated Gurwitsch’s findings,
but the American Academy of Sciences denied finding any change in
growth rates, Gurwitsch was forgotten.

Half a century later, Soviet scientists revived Gurwitsch’s discred-
ited theory. At the Institute of Automation and Electrometry, scien-
tists placed identical tissue cultures into hermetically sealed vessels
separated by a wall of glass and then introduced a lethal virus into
one chamber. The second colony was unaffected. But when quartz
was substituted for glass, the second colony died. Then they used

 

68, fhid., p. 112,

69. Zbid., p. I.

70. G. 8. Lewis, The Sornotape Letters, ch. 7, p. 33.
71. Ostrander & Schroeder, Psychic Discoveries, p, 69.
72. Ibid., p. 412.
