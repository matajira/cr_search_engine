Index

suppression, 260
Weber on, 226-27
within reason, 321
science of mind, 172
Scientology, 347
scissors, 235, 236
Scofield, C. 1., 382, 391
Scot, Reginald, 228
Scotland, 381
Screwtape, 89-90, 104, 326-27
Scruggs, Earl, 103
secrecy, 280-81
secret knowledge, 183
Secret Life of Plants, 101
secret societies, 62, 123, 358, 396
secrets of the ages, 292
seeds, 253
Self-Realization Fellowship, 105
self-transcendence, 33
353, 369
seminaries, 12
Serios, Ted, 110-16
service, 141, 379, 399
shadows, 152, 159
Shafarevich, Igor, 370, 371-2
Shaman, 155
Shapley, Harlow, 302
SHC, 52-56
Sheldrake, Rupert, 123
Sherman, Harold, 257, 259-60, 262-63
shields, 155-56, 159, 163, 248, 260, 331
shrimp, 102-3
Siberia, 294
Siddhartha, 8, 60-61, 207
Sider, Ronald, 12
Sidgwick, Henry, 46
signals, 118-19
silence, 258
silly women, 48
Silver Bridge, 167-68
Singer, Gregg, 2
skepticism, 35, 89, 111, 179, 240, 253
skeptics, 48, 301, 302-303
Slotkin, J. $., 125
“smoke,” 134
snakes, 194
social change, 21

 

423

Social Darwinism, 368
socialism, 2, 347-8, 370, 371
Society for Psychical Research, 44-45,
46-47
Sodom, 68
solar system, 27
Solzhenitsyn, A., If
something for nothing, 271
sorcerer, 65, 126, 128-30, 139, 144-45,
279
sorcerer’s apprentice, 98, 101, 152
sovereignty, 39, 286, 333
Soviet Union, 87-94
spaceman, 307-8, 328
Spangler, David, 465, 366
spark of divinity, 190
Sparks, Bertel, 197n
spells, 65
spirits, 101-8
spontaneous human combustion,
52-56
Spurgeon, G. H., 381
Stamp Act, 369
“Starman,” 301
state, 66-67, 377
statistics, 118-22, 239
Stepanic, Ravel, 111
Sternfels, Florence, 110
Stevenson, Adlai, 5
stimulus & response, 119, 120
Stone, W. Clement, 172
“stop the world,” 148
“straight,” 353-54
subconscious, 113-14
suffering, 344
Summers, Montague, 47
supernatural
astrology, 85
belief in, 120
evidence, 31
world views, 172
supernaturalism
defined, 31, 44
hostility w, 32
surgeons, 260
swamp gas, 309
Sweden, 299

 

 
