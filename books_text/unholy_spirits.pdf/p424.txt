418 UNHOLY SPIRITS

movies, 245, 257, 260
multiverse, 150-52
multiversity, 20, 25
Mumler, William, 108
Miinsterberg, Hugo, 195-96
music, 6-7, 8-9, 11, 19, 103
mutants, 344

mutation, 340

Myers, John, 109
mysteries, 48, 62

mystery, 40, 41, 76, 146
mysticism, 27, 60-61, 335
myth, 50

nagual, 145-46, 149-50, 152-53
Naisbett, John, 365
Napoleon, 7
nature
autonomy, 76
conquest of, 37, 130-31
Hall vs. Watson, 75-77
laws of, 33
magic, 282
man over, 285-86
oneness with, 115
nature/freedom, 33-34, 37-8, 130-31,
142, 297
nature/grace, 30-33
Nautilus, 88
Nebel, Long John, 193
necromancy, 65
neo-evangelicals, 12-13
Netherlands, 174-80
networking, 376
neutrality, 60
New Age
bureaucratized, 10
elitism, 372
evolution, 86n
evolutionists, 368
higher consciousness, 318
key doctrine, 133
leap of being, 318
old view, 371
optimism, 388-89
philosophy, 359
politics, 366, 369

“possibility thinking,” 387
Renaissance, 372
revival, 6
science, 82-87
socialism &, 371
West vs., 26
world view, 5
New Alchemy Institute, 365
New Christian Right, 12, 14
New Deal, It, 367
New Heavens, 376
New Left, 14
New World Education Fund, 365
New World Order, 189, 191-92, 340
new year, 276
New York City, 299
Mew York Times, 194, 201, 240-41,
257, 289
newspapers, 257
Newton, Isaac, 22, 27, 45
Newtonianism, 28, 33, 41-3, 144,
304, 310, 311
nihilism, 61
Nikolaiev, Karl, 90-91
1965
Brazil, 244
Castaneda’s change, 137
cute witches, 66
inflation, 7
Jeane Dixon, 184
loss of faith, 156
new beliefs, 223
paranormal science, 82
Project Blue Book, 303
rise of counter-culture, 3
tise of occultism, 3
“Rubber Soul,” 6, 249
Soviet science, 88
Soviet telepathy, 90
Velikovsky, Immanuel, 246
Watts riot, 7
Nisbet, Robert, 5, 14, 258
Nirvana, 355
Nixon, Richard, 9, 188, 197n, 240
noise, 50
nominalism, 35
