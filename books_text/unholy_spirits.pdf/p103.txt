Paranormal Science Since 1960 97

consulting a pendulum, was led to visit the Cheops Pyramid. He
was permitted a rare overnight visit. While inside the pyramid, he
noticed that animals that had died—rodents, cats—had been placed
in trash cans, yet after considerable periods of time these carcasses
did not decay. Supposedly he had been told of this phenomenon in a
vision or revelation he received while consulting with the pendulum.
This feature of the pyramid was apparently confined to the pharaoh’s
chamber, exactly where Dyrbal was later to place his razor blade
(after reading about Bovis’s discovery). Bovis returned to France,
and he constructed a scale model. Sure enough, organic matter is
preserved for long periods of time. No refrigeration is needed.

The most striking fact of these reports is the repeatability of the
phenomena. Dyrbal had to wait for years for the Czech government
to grant him his patent. He had to show that the razor-repairing
effect is truly scientific. Dyrbal’s theory is incomprehensible: the
shape of the pyramid creates certain resonating effects that somehow
dehumidify the water molecules in the blades’ steel structure. Any-
way, in 1959, patent number 91304 on the pyramid was issued. You
can buy models throughout the Western world. (If I were you, I
wouldn't, as this book will make clear later on.)

More obviously occult psychological effects are produced by the
pyramid shape. Some firms make “pyramid tents” that are used for
personal meditation. By placing one’s head close to the apex (not too
hard in a small tent), the meditating mystic sometimes achieves a
“higher consciousness.” Furthermore, one company now offers a
kind of prayer kit (magic kit) built around the pyramid. Prayers or
requests are written on certain colored sheets of paper, with colors
matching the type of request. The paper is placed inside the pyramid
(made of cardboard), and a chant is made over it. After incubating
for an unspecified time, the paper is removed and burned, in order
to “liberate” the wish. Rewards, according to specifications, are sup-
posed to follow.*” People who are the products of the rationalistic
American public school system are buying these devices.

Some people report tingling sensations in their fingers when they
raise them close to the apex inside a pyramid tent. This is strikingly
parallel to the tingling sensations in the fingers reported by table-

55. Lbid., pp. 150-52. Cf. Peter Tompkins and Christopher Bird, The Secret Life of
Plants (New York: Avon, 1974), pp. 310-11

56. fbid., pp. 159-60.

57. Ibid., pp. 160-62.
