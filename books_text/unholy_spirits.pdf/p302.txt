296 UNHOLY SPIRITS

elves, sprites, “little people,” and similar mythological beings in
Western folklore.) John Keel says that he received over 2,000 clip-
pings of sightings in March and April of 1966, yet the national news
media ignored this huge “flap.”2” In 1973, another wave took place.
This led Vallee to shift his approach, and to begin to discuss the psy-
chology and theology of the contactees rather than the scientific basis
for believing in UFO's.

I have a second question; Why should it be that if occult forces
underlie the manifestation of spacecraft or creatures supposedly
from outer space, they chose to reveal these “vehicles” on a continu-
ing basis only after 1946? Why was it that prior to 1947, there were
only occasional references to such creatures from outer space, and
only occasional references to the spacecraft that supposedly brought
them to earth? In other words, it is the question of timing. Why is it
that now rather than before that we should be deluged with stories
about creatures from the beyond? And when I say “beyond” in this
case, I mean from outer space. Stories of creatures from below are as
old as civilization and anti-civilization: ghosts, spirits, departed
souls, etc. But why ouéer space? And why now?

The Technological Imperative

One obvious answer is that after 1945 and the advent of the
atomic age, the general public became much more aware of the pos-
sibilities of destruction by unknown technology. The faster that tech-
nology has advanced, and the more specialized it has become, the
less it is understood even by the elite guild of scientists, let alone by
the general population. We are not really sure exactly how modern
technology works, but we know some powerful new force has been
given to mankind by the elite scientific planners of the world, and
that this power can be used both for good and for evil. There are
widespread disagreements among politicians and even voters con-
cerning which uses are good and which uses are evil. This has cre-
ated political unrest. To mention only a few areas of rabid scientific
and political disagreement: national defense policy, atomic power
for producing electricity, petrochemicals and the proper disposal of
their waste products, and genetic engineering.

There is something else, something more important: the growing
awareness that technology changes very rapidly and that technologi-

27. Keel, Trojan Horse, p. 21,
