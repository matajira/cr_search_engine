126 UNHOLY SPIRITS

but it all seemed too esoteric — the pastime of the idle educated. The
LSD counter-culture was half a decade away.

Castaneda knew something about the use of peyote in the relig-
ious practices of the Indian tribes of the American Southwest. In a
town close to the Mexican border, he managed to obtain an intro-
duction to an elderly Yaqui Indian, Juan Matus (the Yaqui equiva-
jent of “John Smith”!) who is called don Juan by Castaneda. For
some reason—a very specific reason, the old man confessed years
Jater—he took an interest in Castaneda. He allowed him to visit him
at his house in the desert. As it turned out, don Juan was a brujo—a
sorcerer. The old man had been drawn to Castaneda because he said
it had been revealed to him that the younger man would make an ex-
cellent initiate into the secret wisdom. The revelation was quite cor-
rect; Castaneda has written seven provocative books telling of his
dozen years of off-and-on initiation:

The Teachings of Don Juan: A Yaqui Way of Knowledge (1968)
A Separate Reality: Further Conversations with Don Juan (1971)
Journey to Ixtlan: The Lessons of Don Juan (1972)

Tales of Power (1974)

The Second Ring of Power (1977)

The Eagle's Gift (1981)

The Fire from Within (1984)

Unless Castaneda is exposed as a total fraud, these books are
likely to become classics ~ not classics in Yaqui magic, but classics in
their capacity as primary-source documents of the American
counter-culture of the mid-1970’s, Are they fraudulent? Some re-
viewers have said so, though not a majority of them. The first book
got through the editorial screening process of the academically
proper University of California Press, while the third was accepted
as a doctoral dissertation at UCLA.? Furthermore, by the time Ixtlan
was accepted as a dissertation, Castaneda had become a best-selling
author and a cult figure of the youthful drug subculture. His com-
mittee was not interested in taking unnecessary chances, yet they
gave him his Ph.D. Professional anthropologists believed that it rang
true, As primary-source documents of the mind and world of the

1. Time Magazine, 1973, reprinted in Daniel C. Noel (ed.), Seeing Casteneda: Reac-
Hons to the “Don Juan” Writings of Carlos Castaneda (New York: Putnam's, 1976), p. 94.
2. Ibid., p. 100.
