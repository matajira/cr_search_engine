340 UNHOLY SPIRITS

tion. Not Darwin’s antiseptic evolution of random mutations and
randomly changing environments, but purposeful evolution. We are
headed for Teilhard’s “omega point,” that Hegelian monist resolution
point of all progress. A new stage in man’s evolutionary process is
dawning: “A revolution is taking place before our cyes—the unex-
pected remarriage of reason, at the summit of its victories, and intui-
tion. For the really attentive observer the problems facing contem-
porary intelligence are no longer problems of progress. The concept
of progress has been dead for some years now, Today it is a question
of a change of state, of a transmutation. From this point of view
those concerned with the domain of the interior life and its realities
are in step with the pioneering savants who are preparing the birth
of a world that will have nothing in common with our present world
of laborious transition in which we have to live for just a little while
longer."

What these defenders of alchemy are saying is significant. They
are focusing their attention not on the everyday labors of the com-
mon man, nor on the steady accumulation of scientific knowledge,
but rather on the radically discontinuous event which will bring forth a new
world. They have abandoned the idea of progress, as have so many of
the intellectuals of our day, especially the younger men whose
careers were closely linked with the occult explosion and Vietnam
protests of the post-1965 era. Not progress but mutation. Yet not
random mutation, either. Mutation which is directed by a new elite of
illuminated ones.

Activism toward the world has no meaning for the authors, theirs
is an interior world. The next stage of history will stem from the
affairs of the psyche or the transcendent states of consciousness —
beyond consciousness. The formal activism of the alchemist has once
again become internalized and passive. Thus it must always be. if
men are not passive before God, then in principle they cannot be active toward
the world, Only for brief periods of time can they borrow or steal the
spiritual capital of future-oriented Christianity. Modern autono-
mous man has killed the faith in progress, yet he is called upon to
create a new world: “Man can have access to a secret world —see the
Light, see Eternity, comprehend the Laws of Energy, integrate within
himself the rhythm of the destiny of the Universe, consciously appre-
hend the ultimate concentration of forces and, like Teilhard de Char-
din, live the incomprehensible life that starts from ‘Point Omega,’ in

26. Ibid., pp. xxii-xxiii,
