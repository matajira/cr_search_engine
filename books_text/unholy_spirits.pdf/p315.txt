Invaders from... ? 309

prisoned Man’s spirit for fifteen centuries and stifled progress by the
Scriptures. Today, dazzled by the wonders of Science, we watch the
world drift to suicide and suspect that our materialist philosophy
may be wrong, inspiring us to search again for greater, nobler
Truth,”

Hynek’s Conversion

The difficulty with dismissing such nonsense as this is that it is so
popular. Another difficulty is that it is based on measurable phenom-
ena. The philosophy, of course is based on nothing except occult
speculation and the desire to sell lots of books, but the actual phe-
nomena of Unidentified Flying Objects do exist. One of the most re-
spectable scientific explainers of UFO phenomena was Northwest-
ern University astronomer J. Allen Hynek. Hynek had been em-
ployed as the scientist and public relations officer to explain away
UFO's for the Air Force. Originally the Air Force project which in-
vestigated UFO phenomena was called Project Sign. It was initiated
in September of 1947, and on February 11, 1949, it became Project
Grudge. Then in the summer of 1951 through late 1969, it was called
Project Blue Book. J. Allen Hynek studied these phenomena for the
Air Force for 22 years. He later claimed that he played no role dur-
ing the “Project Grudge” phasc, when the Pentagon treated UFO's
with ridicule.

Again and again, the Air Force called him forward to give expla-
nations for essentially unexplainable phenomena. In March of 1966,
he offered as a possible explanation for a particular incident burning
swamp gas rather than lights from an outer space vehicle. One
report had him say “marsh gas.”*? From that point on, he became
known as Swamp Gas Hynek. He had not actually said that this was
the explanation, but only that some people might have seen swamp
gas. He had been pressured into saying something premature by an
Air Force public relations officer.

Vallee’s comments are important for understanding the shift in
opinion that had taken place over the last few months— paralleling
the wave of incidents that began in early 1966. The press jumped on
Hynek, criticizing him for casting doubts on the word of a local

50. Drake, Gods and Spacemen of the Ancient Past, pp. 27-28.

51. J. Allen Hynek, The UFO Experience, p. 2.

52. “Marsh Gas in Michigan,” in Jay David (ed.), 7he Flying Saucer Reader (New
York: Signet Books, 1967).
