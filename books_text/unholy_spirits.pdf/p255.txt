Demonic Healing 249

Christians must understand the nature of a society like Brazil. It
may be able to telescope its progression from primitive religion to
Western science and religious skepticism to post-1965 paranormal
science and parapsychological fascination. It may be able to skip the
rationalist phase. Skeptical rationalism may never get a strong
enough foothold, even among the intellectual elite, to secularize the
culture. This fact is extremely important, for it has made it very
difficult for Christian evangelism by denominations that have a
puritan-like commitment to education and rational techniques.
Long-term economic growth is made far less likely; mass inflation
will be the substitute. The foundations of progress are simply not
present, for the primary foundations are matters of faith and atti-
tude.’ The only Western churches that have access to these people
are those that hold very similar conceptions of the universe. Writes
Wilson: “In Brazil and elsewhere in Latin America the appeal of
Pentecostalism, which in America and Europe we should regard as a
conversionist movement, may have much less to do with the specific
elements which have been significant in its spread in Protestant soci-
eties, and more to do with those thaumaturgical aspects which are a
part of its inheritance. ‘I'hus we find that in missionary activity . . .
the ostensible configuration of doctrine, organization, and practice
that is offered, is not accepted as a whole: certain elements are more
readily embraced than others. In cultures with strong indigenous re-
ligious traditions it is entirely expectable that the appeal of any mis-
sionary denomination which includes thaumaturgical elements
should be precisely these, rather than other features of its teachings,
activities, or organization.”¥

In short, it is very difficult to deliver societies in occult bondage
by means of evangelism that is based on similar manifestations of oc-
cult healing. The Protestant ethic, which was firmly grounded in a
de-emphasis on charismatic signs and wonders, and placed great
emphasis on self-discipline, education, thrift, and material uplift,
cannot be implanted readily in the hearts and minds of the newly
“converted” population. Those in bondage may stay in bondage, or
at best, may not experience the fruits of total redemption from occult
power.

37. P. T. Bauer, Dissent on Development (Cambridge, Mass.: Harvard University
Press, 1972).
38. Wilson, Magic, p. 121.
