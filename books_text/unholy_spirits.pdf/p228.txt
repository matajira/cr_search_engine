222 UNHOLY SPIRITS

endeavors in a material world to exemplify an ideal of a concept of the crea-
tive energy, or God, of which the individual is such a part that the thoughts
even of the individual may become crimes or miracles, for thoughts are
deeds and applied in the sense that these are in accord with those principles
as given. That [which] one applies will be applied again and again until that
oneness, time, space, force, or the own individual is one with the whole, not
the whole with such a portion of the whole as to be equal with the whole.*

Got that? If so, then perhaps you are ready for the voice’s answer
to a question concerning the Sons of the Highest in Atlantis and the
second coming of souls to earth:

There was, with the will of that as came into being through the correct
channels, of that as created by the Creator, that of the continuing of the
souls in its projection and projection — sce? While in that as was of the offs-
pring, of that as pushed itself into form to satisfy, gratify, that of the desire
of that known as carnal forces of the sense, of those created, there continued
to be the war one with another and there were then — from the other sources
(worlds) the continuing entering of those that would make for the keeping
of the balance, as of the first purpose of the Creative Forces, as it magnifies
itself in that given sphere of activity, of that that had been given the ability
to create with its own activity — see?

Frankly, I am perfectly willing to admit that I simply do not see.
It is way, way beyond my powers of comprehension. When one
worker asked the sleeping Cayce in 1932, “How can the language
used in the readings be made clearer, more concise and more direct?”
the voice was concise and direct: “Be able to understand it better.”%
Doris Agee nevertheless gives us hope: “Just as you found when you
first encountered Shakespeare or Chaucer, familiarity with the partic-
ular ‘shape’ of the language will make you more comfortable with it.””

I never did get used to Chaucer's 600-year-old English, but at least
there are translators and commentators for Chaucer. So far, at least,
Gayce’s 1932 English is without a translator for a considerable portion
of his revelations. But it is not really a translation problem. Those
who made up the messages did not wish to communicate anything of
substance. Why should they? They themselves have no substance.

94. Reader, p. 159.

95. Furst, jesus, p. 36.
96. Agee, ESP, p. 28.
97. Idem.
