124 UNHOLY SPIRITS

ting to which all people may apply, and which operates under open
competitive rules. Scientific guilds may resemble secret societies, but
they are subject to scientific criticism, and they go through public rev-
olutions from time to time. What they do is not “hid under a basket.”

In the case of people who possess unique powers, can these
powers be explained as highly developed skills?

1. Do other people possess similar abilities?

2. Is there some common religious or philosophical thread

linking those who possess it?

. Did these abilities appear overnight?

4, Were the circumstances of this overnight appearance
linked to New Age or occult training?

5, Can these abilities be transferred to others without
respect to initiation or profession of faith?

6. Can the program of training be successfully adopted by
people of many religious backgrounds or professions of
faith?

7. Do these abilities involve the possession of special
knowledge or power that are manifested only intermit-
tently? Under what conditions?

8. Is there any legitimate biblical-spiritual reason for these
powers to be limited to a handful of possessors?

ws

As in the case of science, the normal operating biblical principle
of institutional life is the division of labor (I Cor. 12). Unique gifts
are occasionally given to people, but only in a corporate setting,
under the discipline of the church, and for the edification and benefit
of the church as a corporate body. Why should some gift to an indi-
vidual be given by God? If this unique gift ~ prophecy, healing, spe-
cial knowledge, unique power— is not straightforwardly brought to
the service of the institutional church in its legally corporate status,
then it is highly suspect. It does not meet the criterion of the division
of labor. If the manifestation of the gift is outside the normal corpor-
ate guidelines that are established for the church in the Bible, then
the gift is guilty until proven innocent. If it is given only in the dark,
or only in a special physical environment (location), or only to peo-
ple who are part of an inner circle other than the eldership, then it is
guilty until proven innocent.
