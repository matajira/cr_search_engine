212 UNHOLY SPIRITS

in this age. They also had the impression that what is remote and far distant
is more holy; hence their cult of the “barbarians,” of Indian gymnosophists,
Persian magi, Chaldean astrologers, whose approach to knowledge was felt
to be more religious than that of the Greeks. In the meiting-pot of the Em-
pire, in which all religions were tolerated, there was ample opportunity for
making acquaintance with oriental cults. Above all, it was the Egyptians
who were revered in this age... . The belief that Egypt was the original
home of all knowledge, that the great Greek philosophers had visited it and
conversed with Egyptian priests, had long been current, and, in the mood
of the second century, the ancient and mysterious religion of Egypt, the
supposed profound knowledge of its priests, their ascetic way of life, the re-
ligious magic which they were thought to perform in the subterranean
chambers of their temples, offered immense attractions.

That Hermes-Jesus and RaTa-Cayce had once worked together
on that greatest of all magical and occult projects, the Great Pyra-
mid, no doubt sent a wave of anticipation down the spines of Cayce’s
followers. Here was Gayce, RaTa reincarnate, launching yet
another work of mysterious power and mysterious knowledge.”
They would be a part of it. Hugh Lynn Cayce may not have ac-
cepted the mumbo-jumbo of reincarnation, preferring instead to see
the venture inward as a psychological operation, but his followers do
not seem to have been equally sophisticated, The stories of the rein-
carnations, especially concerning Jeshua-Jesus, are referred to in
numerous Cayce-related volumes.

The Master, Jesus, was a man. Until the very end of his reincar-
nation cycles, Jesus was a fallen man, When he went into the wilder-
ness, he went “to meet that which had been His undoing in the be-
ginning.”®* What we have in the life of Jesus of Nazareth, therefore,
is a fine ethical example: “In that the man, Jesus, manifested in the
world the Oneness of His will with the Father, He becomes—from
man’s viewpoint — the only, the first begotten of the Father. Thus he
is the example to the world, whether Jew, Gentile or any other
religious force.” Jesus is the humanistic example of the good life,
even the final good life which overcomes all our former and im-
perfect lives.

56. Frances Yates, Giordana Bruno and the Hermetic Tiadition (New York: Vintage,
[1964] 1969), p.5.

57. I can’t stand it any more! I have to say it! “RaTa, RaTa . .. RaTa, RaTa,
ring, ring, ring.” There. I feel a lot better. Now let’s get back to our story.

58. Furst, Jesus, p. 172.

39. Ibid, p.173.
