352 UNHOLY SPIRITS

an illusion; existence is maya. The first step at transcending reality
is control of the body. Hatha yoga techniques are the outward form
of an inner system of philosophy. The goal of yoga is the liberation of
man from the illusion of existence, and physical culture is the first
step in this “liberation.” Mental purification and spiritual awakening
come from the manipulation of the body. This, of course, is almost
the mirror image of Christianity, which teaches that regeneration
precedes the acts of men, for “the natural man receiveth not the
things of the Spirit of God” (I Cor. 2:14). The Christian doctrine of
the Fall of man is ethical; the Indian is metaphysical. The Christian
responds outwardly to a change in his inward condition; the yogi at-
tempts to change his inward perception of reality — illusion—by ma-
nipulating his body. The Indian mystic is trying to escape from cre-
ated reality, and various methods are used: asceticism, libertinism,
withdrawal from society, meditation, immersion in work. The goal,
however, is the systematic distortion of perception.

One of the first things to be distorted, as mentioned earlier, is the
sense of time. This is also equally true of the hallucinogenic drugs.
Aldous Huxley, in his Doors of Perception (1954), comments on his
altered sense of time:

And along with indifference to space there went an even more complete
indifference to time.

“There seems to be plenty of it,” was all I would answer, when the inves-
tigator asked me to say what I felt about time.

Plenty of it, but exactly how much was entirely irrelevant. I could, of
course, have looked at my watch; but my watch, I knew, was in another
universe. My experience had been, was still, of an indefinite duration or al-
ternatively of a perpetual present made up of one continually changing
apocalypse.

Plenty of time: for the Eastern mystic, yes; for the Western thinker,
no. “Time is money,” wrote Ben Franklin. But even more to the
point are St. Paul’s words, just two sentences prior to his statement
on drunkenness: “See then that ye walk circumspectly, not as fools,
but as wise, Redeeming [buying back] the time, because the days
are evil” (Eph. 5:15-16), Time is the one truly unrenewable resource
in man’s life, but only in a world of linear time and unthout reincarnation.

44. Aldous Huxley, The Doors of Perception (New York: Perennial Library, [1954]
1970), p. 21.
