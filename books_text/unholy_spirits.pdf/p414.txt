408

Augustine, 72, 173, 273
aura, 98, 220
Australia, 251-52, 267
authority, 32, 39, 40, 333, 379
automatic writing, 113, 317
autonomy
apostasy, 39
cosmos, 86
creation vs., 334
death of culture, 28¢
despair, 43
determinism &, 29
ethical, 283, 328
fascism, 140
fool’s dream, 333
“humility,” 36, 63
impotence, 287
irrationalism, 39
natural world, 31
nature's, 76
noumenal &, 44
occultism, 397
Ockham, 32
power &, 34, 38
relativism, 145
restraints on, 378
science, 45
scientific, 324, 326
technique, 297
trust, 285
universe, 333
Averroism, 32n

Baal, 67
Backster, Cleve, 102
Bailey, Alice, 225
Bakunin, Michael, 366
Banfield, Edward, 348-49
barbarism, 372

barbers, 172

barter, 362

Bateson, Gregory, 366
Bauer, P. T., 277-78
Beach Boys, 8, 180-81
Beard, Charles A., 24
Beatles, 6, 19, 23, 349
Becker, Carl, 24

UNHOLY SPIRITS

Belgian Committee Para, 78, 81

Belk, Henry, 181, 236, 271

belly, 150

Benedictines, 274

Berkeley, 4, 6, 19-20

“Bewitched,” 66

Bible
Cayce 196, 203-4
chariots of fire, 328
humility and dominion, 57
Ockham vs. 3t
philosophy and, 39
prohibiting occultism 65
witchcraft vs., 397

Bigfoot, 330

Billington, James, 361

biofeedback, 171, 350

birth control, 116

Bittencourt, Sen., 230-31

blackboard, 323

black magic, 56

Blanche, Juan, 252, 256

bluegrass music, 103

blueprint, 366, 373

Bob Jones University, 168

Boeing 747, 107

bondage, 198-99, 230, 256,
267, 284-85, 333

bookkeeping, 331

books, 18-19, 100, 144

borders, 298

Bose, J. C., 103-4

Bovis, Antoine, 96-97

Bradwardine, Thomas, 31

brain, 133, 171

Brazil, 168, 231-233, 249

Breakfast Club (Los Angeles), 259-60

bridge, 167-68

broken bones, 261

Brown, Charles B., 54

Brown, Frank, 83-84

brio, 126

Bruno, Giordano, 306

Bucke, R, M., 354-55

Buddha, 344-45

Buddhism, 64

buffer, 138
