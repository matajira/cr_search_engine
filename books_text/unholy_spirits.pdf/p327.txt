Invaders from... ? 321

with the same problem. He said that we do not know as much about
the tools we use as the primitive savage knows about the operations
of his tools. We use sophisticated equipment even though we know
next to nothing about it. What modern rational science proclaims is
that “if one but wished one could learn it at any time. Hence, it means
that principally there are no mysterious incalculable forces that
come into play, but rather that one can, in principle, master all
things by calculation. This means that the world is disenchanted.”®
This was one of Weber’s constant themes: the disenchantment of the
world. It is basic to all Western rationalism.

What UFO's represent, along with psychic healing and other oc-
cult phenomena, is a threat to this world view. It represents a re-
enchantment of the world. It represents a break with Western rational-
ism and Western science, The very words “occult phenomena” are
considered illegitimate by definition—an implicit breaking of the
treaty. Phenomena by definition cannot be occult, In principle, phe-
nomena are capable of being explained through rational calculation.
This is basic to the faith of Western rational science.

The Dilemma of Empirical Science

Scientists seem to recognize that studying anything which does
not conform to this rule borders on the heretical. Science is supposed
to be open minded, within reason. Science is supposed to be willing to
investigate anything, #f relevani. Science is supposed to be empirical,
but only when the facts are real. Here is twentieth-century science’s di-
Jemma: the measurable manifestations of the phenomenal realm are
beginning to depart from any known set of scientific rules. The
counting devices of science cannot deal with the phenomena, for the
phenomena keep violating the rules of calculation. A few scientists—
perhaps a hundred of them in 1975, worldwide, what Vallee calls
“the Invisible College” — keep on investigating the occult, or UFO's,
in the hope that they will discover some overarching theory, some
new means of integrating knowledge. They believe that they are in-
vestigating anomalies that are comparable to other oddities in
nature, other “not yet explained” facts. They admit that these facts
do not conform to the presently known laws of phenomenal science.
Only a tiny handful of scientists on the very fringe of the fringe are

85. Max Weber, “Science as a Vocation” (1918), in H. H. Gerth and C, Wright
Mills (eds.), From Max Weber: Essays in Socinlogy (New York: Oxford University
Press, 1946), p. 139
