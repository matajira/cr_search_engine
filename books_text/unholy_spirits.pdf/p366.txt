360 UNHOLY SPIRITS

Konko-Kyo Church of Izuo
The Love Project

The Mandala Society, Inc.
The New Age Press
Ontological Society
Rosicrucian Fellowship
Servers of the Great Ones
Sivananda Yoga Center

Sri Aurobindo Society
Technoeracy, Inc.

‘Temple of Peace

‘Temple of Understanding
‘Theosophical Order of Service
Thomas Jefferson Research Genter
United Nations Association
United World

Women’s Universal Movement, Inc.
World Family

World Institute Council
World Union

You Institute

This list is taken from the 1974 Directory (8th Annual Edition).
Hopefully, it provides a general sense of what the ICG was really all
about. Southern California is undoubtedly the congregating point in
the United States for such groups as thesc, and it is nice to know
that modern man’s hunger for community is still alive. The problem
for the rest of us is the kind of community these groups have in
mind, In the Spring-Summer, 1973 issue of The Coordinator, the
editorial, “A Model for Mankind,” informs us:

Mankind is indeed being born and we of this time have the great
privilege of being present at the creation. We must do everything we can to
ensure that the new infant is not misshapen at birth but is well-formed and
healthy so that it can have the maximum opportunity for surviving and
growing to realize its full potential in the universe.

Mankind, it should be noted, is an androgynous being, having both
male and female characteristics in one. It will grow both through an in-
creasing complexity-consciousness of each cell and by individual cell
multiplication. However, for Mankind to survive and flourish, a cure for
the cancer of unlimited population growth must be found. When Mankind
is ready to expand autward into space a new phase of growth can start, but
