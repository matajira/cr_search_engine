Paranormal Science Since 1960 89

of intense concentration, Nelya can perform the following wonders
(sometimes): spin a compass needle from several feet away (a terrific
talent, especially when you're lost); move various objects around a
table — or off; separate the yolk from the white of an egg that is sus-
pended in a saline solution, and then put them back together. All of
these activities produce intense physiological reactions in Nelya, in-
cluding a heart pulsating as fast as 240 beats per minute. A measura-
ble magnetic force field is created by her during these experiments
—focused on the object of her concentration, These experiments
have been conducted under rigorous test conditions and recorded on
film. There are no known explanations.”

One of the key signs of occult powers as distinguished from re-
markable but natural powers is the repeatability factor. Like virtually
all psychics, Nelya never knows for certain if her powers are going to
appear. Unlike American establishment scientists, who dimly grasp
the significance of repeatability, the Soviet scientists who are in-
volved in paranormal scientific research put up with the seemingly
random display of these experimental oddities. American scientists
dismiss such phenomena as being unreal, which they assuredly are
not, or as purely random events needing no explanation, which they
certainly do need. But this basic skepticism has kept most of them
away from the dark paths of occult power.

The Soviets, demonic to the core in their official materialism,
have now embarked on an equally demonic journey into the realms
of power that are anything but materialistic. Screwtape, the myth-
ical demon of C. 8. Lewis’ Screwtape Letters, is having his devilish
wish come true behind the Iron Curtain. Writing to his nephew, the
neophyte demon Wormwood, Screwtape sets forth basic policy:
“Our policy, for the moment, is to conceal ourselves. Of course this
has not always been so. We are really faced with a cruel dilemma.
When the humans disbelieve in our existence we lose all the pleasing
results of direct terrorism, and we make no magicians. On the other
hand, when they believe in us, we cannot make them materialists
and skeptics. At least, not yet. I have great hopes that we shall learn
in due time how to emotionalise and mythologise their science to
such an extent that what is, in effect, a belief in us (though not under
that name) will creep in while the human mind remains closed to
belief in the Enemy. The ‘Life Force,’ the worship of sex, and some
aspects of Psychoanalysis may here prove useful. If once we can pro-

37. Ibid., pp. 67-76.
