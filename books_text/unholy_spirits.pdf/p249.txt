Demonic Healing 243

dox science as wedges (or crowbars) to break open the hard shell of
establishment orthodoxy.

The overwhelming problem with this approach is in the very na-
ture of the tools they are using, including the assumptions about the
universe that are accepted by all supposedly reasonable participants
in scientific research. If the subject studied does not, by its very na-
ture, fall within the post-Kantian laws of science, what good are the
operating assumptions of post-Kantian science in determining what
factors are involved in the observed phenomena? Orthodox scientists
conclude that there must be some flaw in the observational capacities
of paranormal researchers. They are being tricked by sleight-of-
hand artists. They have not studied enough cases. The cases are not
repeatable. The anomalies are a product of the inherent randomness
of the universe. The researchers are liars and involved in a massive
conspiracy against their colleagues. The researchers are mad. The
paranormal researchers reply that the orthodox leaders are blinded
by prejudice, unwilling to examine the facts, suppress evidence, lie
about paranormal researchers, and are too stodgy and wrapped up
in pre-1965 concepts of what constitutes the possible to be accurate
judges. Yet both groups claim allegiance to the increasingly shaky
canons of post-Kantian rationalism, Yet it is precisely this—the
operating presuppositional foundation of all modern science — that is
being undermined by the realization that men like Arigo do exist,
and that neither orthodox medicine nor paranormal science can
make any sense out of the facts. The hard reality of the facts are, like
a knife, jabbing their way into the eyes of the scientists. The ortho-
dox scientists act like Arigo’s patients: they seem oblivious to what is
taking place. The paranormal scientists understand that what is
happening is impossible, but they also claim that there is no discom-
fort involved. They are self-deluded. Arigo’s knife is cutting away
the public’s faith in the world of the scientists. The public is coming
to believe that scientific rationalism cannot account for the miracu-

 

lous events that are being reported in book after paperback book,
and even in that Establishment organ of conventional wisdom, the
Reader's Digest. The public, feeling no pressure to publish in respec-
table scientific journals, and not having gone through the mind-
directing program of scientific initiation, sees that Arigo’s knife is
cutting out the heart of science’s most cherished presupposition: that
the rational, categorizing mind of man can give an account, at least
in principle, assuming sufficient facts are available, of any known
