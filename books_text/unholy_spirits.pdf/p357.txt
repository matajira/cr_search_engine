Excape from Creaturchood 351

Perception and Responsibility

The problem with drunkenness is the problem with pot is the
problem with LSD: the deliberate altering of the human con-
sciousness merely for the sake of alteration, or for psychological
escape, is a form of cultural and religious rebellion. “And be not
drunk with wine,” St. Paul writes, “wherein is excess, but be filled
with the Spirit” (Eph. 5:18). Paul could advise Timothy to “use wine
for thy stomach’s sake” (I Tim. 5:23), so he was not issuing a general
prohibition on what we might call social drinking. But he did con-
demn drunkenness. Why? Because drunkenness is the deliberate
distortion of a man’s perception of God's created reality. It is a denial
of the creation to be drunk — the twisting of God’s revelation of Him-
self through his creation. In short, man is forbidden to indulge in the
distortion of his mental faculties merely for the sake of escaping nor-
mal reality. (This is not to imply that anesthetics are illegitimate, for
these make the physician’s task easier in operating and increase the
likelihood of a man’s restoration to health. Health, after all, is nor-
mal for a godly, progressive society, i.e., normative: Ex, 23:25).

Each man is personally responsible for his actions. This is the
starting point of all human action. Each man is responsible before a
sovereign God who has created reality. Each man is therefore re-
sponsible in terms of his lawful sphere of influence. From him to
whom much is given, much is expected (Luke 12:48). Therefore, for
any person to assess his own gifts and responsibilities, he must strive
to perceive reality accurately. One of the most important economic
developments of all time is the double-entry bookkeeping ledger, It
enables men to keep an accurate record of their liabilities and assets.
Without a free market which can assess the economic value of scarce
economic resources, there can be no rational economic calculation
and therefore no rational economic planning, Unless men are trying
to conceal something, they do not deliberately make false entries in
their business ledgers. The use of hallucinogenic drugs, yoga tech-
niques of meditation, and other self-transcendence devices is com-
parable to making deliberately false entries into a ledger. It reduces
men’s ability to make accurate assessments of their actions in rela-
tion to a real external reality.

That yoga techniques should be successful in altering men’s
perception should come as no surprise. The philosophy of Indian
monism, like that of Zen Buddhism, is to deny all reality. Reality is
