Invaders from... ? 315

Vallee had scientific training and cannot be dismissed as a crank.
This book is simply a careful summary of numerous sightings of air-
borne vehicles—sightings that cannot be easily dismissed. A later
book, Messengers of Deception (Bantam, 1980), is much more impor-
tant. It is subtitled, UFO Contacts and Cults.

Vallee claims to have had access to over 2,000 documented cases
of reported contacts with UFO creatures. These files indicate that
there is much that conventional science cannot explain. He under-
stands that scientists have deliberately suppressed evidence regard-
ing UFO sightings. He says he was present when French astrono-
mers erased a magnetic tape on which their satellite tracking team
had recorded I data points on an unknown flying object which was
not an airplane, a balloon, or a known orbiting craft.6 He admits
that in 1967 his interests had begun to shift. In that year, the Penta-
gon gave $512,000 to Professor Condon to begin his studies. Vallee
began to ask himself new questions: “Why is it, I wondered, that the
‘occupants’ of UFOs behave so much like the denizens of fairy tales
and the elves of ancient folklore? Why is the picture we can form of
their world so much closer to the medieval concept of Afagonia, the
magical land above the clouds, than to a description of an extraterres-
trial planetary environment? And why are UFO's becoming a new
religious form? I spent a year researching this angle, and emerged
with a greater appreciation for the pychic aspects of the phenomenon.
I could no longer regard the ‘flying saucers’ as simply some sort of
spacecraft or machine, no matter how exotic its propulsion.”

Vallee continues: “In my spare time, I pursued my UFO studies,
trying to find some pattern in the global distribution of sightings.
What I deduced from the data was of remarkable significance: The
phenomenon behaved like a conditioning process. The logic of conditioning
uses obscurity and confusion to achieve its goal while hiding its
mechanism. I could see a similar structure in the UFO stories. ] am
beginning to perceive a coherent picture of the ‘flying saucer’ phe-
nomenen for the first time, now that I am pursuing the idea that
UFOs may be a pyschological control system, and now that I am
aware of their link to human consciousness. I stil] think there is a
genuine technology at work here, causing the effects witnesses are
describing. But I am not ready to jump to the conclusion that it is the

 

65. Messengers of Deception: UFO Contacts and Cults (New York: Bantam, [1979]
1980), p. 5
66. tbid., p. 6.
