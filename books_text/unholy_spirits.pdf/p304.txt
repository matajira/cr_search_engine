298 UNHOLY SPIRITS

it to ourselves, But the damnable things keep coming back anyway.”®

Tt is my thesis that personal occult creatures whose goal is to con-
fuse men, delude men, and keep them in a form of bondage, can do
so by manifesting themselves in a form which is acceptable to man.
In other words, we are back to that being described by Screwtape in
his letter to Wormwood, the “materialist magician.” When men are
willing to believe in supernatural forces but not in God, then man-
kind is very close to the end—either the final battle, or at the very
least, the end of the anti-Christian world of humanism.

Not Physical Phenomena

Kenneth Arnold, the pilot whose adventure started it all, studied
UFO's from 1947 until 1955. In that year, he issued a report saying
that he thought that the vehicles were not airships at all, but some
form of living energy.

But the story is stranger than just the oddity of the physical man-
ifestations of the “vehicles.” If these invaders are fram Mars, asks
Keel, why is it that they seem to know exactly where state borders
are in the United States? Time and again in the “UFO flaps” — mass
sightings of numerous UFO’s— the sightings cluster within the bor-
ders of one or two states. This was a common phenomenon in
1966-67. On August 16, 1966, for example, there were hundreds of
sightings in Arkansas, in a belt running from north to south. There
was not a single sighting in Oklahoma, Mississippi, Tennessee, or
Louisiana, all bordering states. Minnesota and Wisconsin, far to the
north, did participate in this “flap,” mostly in Minnesota, with a few
sightings in South Dakota, right on the border with Minnesota. Keel
comments: “Certainly if the UFOs were meteors or other natural
phenomena, they would also be reported in adjoining states. Cross-
state sightings are not as common as skeptics would like to believe.”>4
Another question Keel asks is this; Why were over 20% of the 1967
and 1968 sightings on a Wednesday, but only 7% on Tuesday?

Mystery airplanes are also often sighted in UFO “flap” areas.
They fly without identification markings, usually low to the ground,
and at night the pilot's cabin is illuminated. They often appear in the
midst of bad weather. In short, they violate all the rules of sane

30. Keel, Trojan Horse, p. 32.
31, Ibid, p. 42.
32, Idid., p. 20.
33, Ibid. p. 19.
