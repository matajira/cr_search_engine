Escape from Creaturehood 347

ings~and by the Church of Scientology. An important part of the
initiation of shamans (medicine men) in primitive cultures is the out-
of-body experience of flight.** In short, however such experiences
occur in the perception of the actors, astral projection is a traditional
means of transcending both time and space. It is a denial of the bind-
ing nature of man’s normal faculties. Where it exists, Western con-
cepts of privacy are made most difficult. Nevertheless Western tech-
niques of spying and electronic communication are a lot more reli-
able and in the long run, more of a threat to privacy. Technology has
made possible the equivalent power of astral projection, but it has
placed such power in the hands of third-level bureaucrats instead of
an initiated elite of adepts. The West has democratized, or at least
bureaucratized, the ecstatic dreams of the shamans of the world, and
has added videotape recordings as well.

“Time Is Money”

The West is concerned with reducing the burden of time. The
communications revolution, the computer revolution, the data-
retrieval networks, the highway, the jet plane, and numerous other
features of Western culture have provided men with savings in time
undreamed of by the masses. But there has been a difference: the
burden of time has been recognized as ultimately unyielding to the
manipulations of men, Each saving of time has a cost; technology
can reduce this cost, but it cannot reduce it to zero.

The quest for zero-cost reductions in time is demonic. It denies
that man faces fundamental limits as a creature. Western man may
not be satisfied with running a hundred meters in 9.9 seconds. He
admits that he cannot safely predict how low this time restraint will
fall. But he affirms what the occultist will not admit: that the time it
takes a man to travel one hundred meters cannot and will not be re-
duced to zero. The fact that Western man counts the cost accurately,
i.e., can calculate the weight of each burden, enables him to reduce
the burden through effective planning, discipline, and above all,
capital formation. The socialist or communist, who affirms the relig-
ion of the occult — that the limits of scarcity are not natural, but are
man-created—is less rational, cannot calculate economically, and
threatens economic productivity whenever he takes over the reigns
of political power. Socialist economics is the demonic philosophy of

39, Eliade, Shamanism (Princeton: Princeton University Press/Bollingen, [1964]
1974), pp. 127-44.
