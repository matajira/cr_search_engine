306 UNHOLY 5PIRITS

their rulers can do can release them from the thermonuclear trap.
They therefore seek solace, comfort, and assistance from beyond the
skies. They do not turn to God; they turn to little green men.

Old Faith, New Framework

It should not be thought that belief in outer space creatures is
new. That belief goes back for hundreds of years, perhaps thousands
of years. In the Renaissance, scientists so-called and philosophers so-
called believed in multiple universes on multiple planets beyond the
stars. The ancient Platonic idea of plenitude was combined with the
discoveries of the Copernican revolution to produce a new religious
faith in the plurality of worlds.*7 This was true of the magician-“scien-
tist” Giordano Bruno, and it was true of many of his contemporaries.
It was true of some of the crackpots of nineteenth-century rational-
ism, some of whom became quite influential. One example is Charles
Fourier, the French socialist and certifiable nut who believed that the
planets are alive and copulate with one another. Yet Frederick
Engels, the co-founder with Karl Marx of Communism, praised
Fourier as a man who, despite his mysticism (!!!), was an important
social scientist who was characterized by his “sober, bold and system-
atic thinking, in a word, social philosophy.”®

What distinguishes the modern concern about flying saucers
from the mystical occult belief in multiple worlds that are inhabited
by many forms of beings is today’s confidence in technology. The
faith in technology as a means of linking up creatures between the
worlds was not as common in Renaissance society. One new thesis
which is unique to the latter decades of the twentieth century is the
thesis of popular writer Eric Von Daniken that gods from outer
space, meaning space travellers, came to earth thousands of years
ago, mated with our foremothers, and produced a new race of which
we are the heirs. They taught them science or at least preliminary
foundations of scientific thought. They showed them technological
wonders and then they left, Mankind, therefore, made a major leap
of being as a result of contact from outer space creatures. He argues
this thesis in several books: In Search of Ancient Gods, Chariots of the
Gods?, The Gold of the Gods, and most tauntingly, Gods from Outer Space,

47. Arthur R. Lovejoy, The Great Chain of Being: A Study in the History of an Idea
(New York: Harper Torchbooks, [1936] 1965), ch. IV,

48. Cited by Igor Shafarevich, The Socialist Phenomenon (New York: Harper &
Row, [1975] 1980), p. 204.
