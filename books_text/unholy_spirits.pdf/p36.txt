30 UNHOLY SPIRITS

sarily infinite number of abstract forms, no one was sure. ‘The in-
ability of classical philosophers to reconcile this fundamental dual-
ism led to the disintegration of classical culture. Eastern mystery
cults spread over the Hellenistic and Roman worlds. Total imper-
sonal Fate battled with total impersonal Chance for control of the
universe. Astrology flourished, was banned, and still flourished;
chaos cults were everywhere. Men could no longer make sense out
of their world. Christianity replaced classicism’s fragmented cul-
ture.® But the lure of Greek philosophical speculation — the logic of
the hypothetically autonomous human mind — was nearly irresistible
to Christian philosophical apologists. They incorporated aspects of
Greek wisdom, and therefore Greek dualism, into their defenses of
the orthodox faith. The result was intellectual schizophrenia — philo-
sophical syncretism. Christian philosophers attempted to combine
irreconcilable systems: Greek philosophy and biblical revelation.

Thus was born the so-called nature/grace framework. The truths of
autonomous human reason, whether Platonic (as in the early Mid-
dle Ages), Neo-Platonic and mystical (same era), or Aristotelian
(late Middle Ages), were understood as autonomous truths. These
autonomous truths were believed io require the grace of God’s reve-
Jation to complement and extend them, but they are independent
(autonomous) truths nonetheless. Nature (meaning philosophical
speculation) and grace (theology) were to be reconciled by means of
a synthesis of Christian theology and Greek (Aristotelian) philoso-
phy. Late medieval scholasticism, the great attempted synthesis, was
finalized by Thomas Aquinas (d. 1276). This scholastic synthesis
was effectively challenged within a century.

 

Dualism: Logic us. Faith

In the fourteenth century, the synthesis was attacked from two
sides. William of Ockham is deservedly famous for the assertion of
*Ockham’s razor,” a hypothesis that complex explanations thal at-
tempt to describe any natural process should be abandoned when-
ever a “simple” explanation suffices. He and his followers used this
hypothesis to shave grace out of the universe. If logic or observation
accurately describe a particular event, then men must not appeal to

 

 

25, Charles Norris Cochrane, Christianity and Classical Culture: Studies in Thought
and Action from Augustus to Augustine (2nd ed.; New York: Oxford University Press,
{1944] 1957).

26. Tbid., pp. 44-45
