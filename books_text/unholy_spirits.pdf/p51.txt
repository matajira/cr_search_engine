The Crisis of Western Rationalism 45

~-as mental telepathy, hypnotism, and apparitions.47 They were
talking as though they might be willing to allow certain aspects of
Kant’s noumenal “something-in-itself” to creep across the treaty’s
boundary markers. While such odd phenomena were believed to be
wholly explainable by rational laws, given enough study, the desire
of the spiritualists to relate these odd phenomena to affairs of the
noumenal realm— phenomena who have somehow dropped in for a
chat from the noumenal realm—was quite properly understood by
the rationalists as a violation of the post-Kantian secret treaty. The
unknown is supposed to stay unknown until modern science has an
acceptable way to explain it; any time the unknown becomes known,
it is supposed to become completely subject to the comprehensive a
prior’ laws of the autonomous human mind. That is the rule, the ra-
tionalists insist. No exceptions.

No faithful nineteenth-century Kantian rationalist ever believed
that the unknown was actually invading the realm of the known and
disrupting its deterministic operations, What he resented was the
obstinacy of the ghost hunters in clinging to their fantasy that there
couéd conceivably be such an invasion. To acknowledge the possibil-
ity of such an invasion was tantamount~ indeed, identical—to say-
ing that there is a crucial flaw in the only philosophical tool which
the humanist possesses which can be used to keep God locked out of
the phenomenal realm. A defect in this tool means that the crack in
the wall between the phenomenal realm of scientific and comprehen-
sive knowledge and the fresently unknown might allow God to sneak
back in to control the destiny of the universe and to judge it on the
final day. If any force has impact in our world, it just has to be a phe-
nomenal force, that is, a force that operates in terms of rigorous
scientific law, even if we have not yet discovered that law.

Rationalism’s mutual defense treaty with irrationalism against
God was being ignored by the spiritists. Their denial rankled the
minds of naive Newtonian rationalists. The naive Newtonians did
not understood that Newton had personally rested his case for a
coherent universe on God's providence, and that when Kant
destroyed men’s faith in Newton's providentialism, he thereby in
principle destroyed the foundation of predictable science. If God
does not hold the universe together, then who does? Man? Man’s
mind? The a priori categories of man’s mind? What a feeble founda-

47, Nandor Fodor, Encyclopaedia of Psychic Science (New Hyde Park, New York:
University Books, 1966), pp. 350-51.
