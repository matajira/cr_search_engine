ds This the End of the World? 387

program of comprehensive redemption,’® The dominion covenant
requires men to subdue the earth to the glory of God (Gen. 1:28;
9:1-17). His people still must accomplish this task before He comes
again to judge their success. They have been given sufficient time; they
must redeem it.

“Sufficient time”: it really does matter what eschatology a person
holds. It colors what a man believes that God has called His people
to do. Eventually, the question of eschatology makes itself felt, for
better or worse. Though many Christians think the subject can be
safely ignored, it cannot be. There is never a choice of “eschatology
vs. no eschatology”; it is always a question of which eschatology.

Rev. Wilkerson is still proclaiming the older fundamentalist
paradigm of the external historical defeat of Christians, a view which
has hamstrung efforts at Christian dominion in the United States for
almost a century. It is now being replaced by a new paradigm, the
paradigm of dominion. The escapist religion is going down with the
power religion’s sinking ship. The dominion religion is in the early
stages of replacing both.

Dave Hunt’s Coming Holocaust

Dave Hunt is a careful student of the cults and the occult. He
understands the very real threat which New Age philosophy poses.
He also recognizes the extent to which Christian leaders have un-
thinkingly adopted New Age “possibility thinking” and “think and
grow rich” techniques. But his analysis is colored from start to finish
by his belief that we are nearing the end of the so-called “Church
Age,” that Jesus is about to call His people into heaven at the Rap-
ture, and that today’s visible apostasy is a nearly inescapable one-
way street to judgment. He holds the premillennial-dispensational
belief that the church of Jesus Christ will lose in its confrontation
with Satan, until Jesus returns physically to run the whole show, In
short, he thinks we redeemed people are a bunch of losers and incompetents,
and no maich for Satan and his followers. He regards Christians as people
who were called by God to a world-conquering task (Mat. 28:18-20),
who were given the Holy Spirit (Acts 2:1-21), and then were sent on
a task which God knew from the beginning that they could not ac-
complish without Christ’s presence in the flesh. Obviously, if you
begin with a low view of the church, and a high view of Satanic

18. Gary North, “Comprehensive Redemption: A Theology for Social Action,”
The Journal of Christian Reconstruction, VIII (Summer 1981).
