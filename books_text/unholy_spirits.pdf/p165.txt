The World of a Sorcerer 159

of the world over the weaker. Temporarily, one view may prevail.
But this view can be defeated by yet more powerful forces. Nothing
is stable; nothing is perpetual. This is why each man is ultimately
autonomous if he is a “warrior.” He makes his own reality.

This view destroys the division of labor, Men cannot cooperate
successfully, It is no accident that don Juan was a kind of hermit,
that he had no contact with Yaqui institutions. He was a witch, and
a witch takes this view of power very seriously. He cannot cooperate,
except as an initiator initiates a successor. He must remain almost
invisible. As Castaneda told the interviewer, “To weasel in and out of
different worlds you have to remain inconspicuous.”1!9 (This is espe-
cially true if you are a 50-year-old man who is selling millions of
books to people under 30 who think you are one of them.)

The essence of his initiation was deception. Don Juan lied to
him, misinformed him, frightened him, blew his mind with dope,
and generally manipulated him. The world of a sorcerer is a world of
continual flux, especially flux with respect to everything one says. In
short, it is a world of shadows and lies, yet always close to a very real
world of intensely powerful beings who hate man, and who serve
him only to trap him later on. Man is promised to be able to trans-
cend the limits of ordinary mortals with their ordinary reality. The
result is backwardness, hermit-like existence, meaninglessness, and
impotence. The true power which is made available to man through
social cooperation is renounced in favor of the warrior’s autonomous
existence. Let us not be deceived: don Juan was a poverty-stricken
drifter— without community, without nation, without friends, and
without a fortune in book royalties. Castaneda got those, for he was
still a Western man.

Conclusion

What are we to make of Castaneda’s books? Are they works of
fiction? One reviewer concluded that whether or not these events
took place is irrelevant: “What difference does it make whether one
believes in astrology or I Ching or sorcery? As though a vote in some
cosmic ballot is going to establish its truth or utility. The real issue is
what it says, whether the disturbance it causes in the normal ways of
knowing will lead to more imaginative ways.”!” No doubt about it:
our shields are down. If don Juan is correct, it makes all the differ-

119. Seeing Castaneda, p. 86.
120, Los Angeles Times (June 8, 1975).
