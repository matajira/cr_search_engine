ds This the End of the World? 379

day, however, the residue has been eroded away to a great extent.
This humanist civilization has spent its spiritual capital, and its
checks are bouncing. The decay of humanism has led to the revival of
occultism. What we are witnessing is occudt revival and cultural disinte-
gration. What we may very well be witnessing is huwnantst civilization’s
dying gasp.

The Christian answer is not terror, retreat, and hopelessness. It
is not a program of “eat, meet, and retreat.” It is not waiting for the
Rapture. The answer is a systematic, well-financed, decentralized
program of comprehensive Christian reconstruction, Every area of
life must be called back from the rot of humanism and the acids of
occultism. The possession and long-term maintenance of authority in this
world must be seen by Christians as the product of regenerate men’s self-confi-
dence under God and in conformity to His moral and civil laws, as revealed in
the Bible. Confidence in the earthly victory of God’s people is our
motivation;? biblical law is our tool of dominion;3 profitable service
is our program. Reason flourishes in a framework of biblical revela-
tion. It disintegrates when men attempt to operate in terms of their
own hypothetical neutrality and autonomy.

We see before us the poison fruits of a self-professed autonomous
Western rationalism which is running wild. The sons and daughters
of twentieth-century rationalism have become, if not occultists, then
certainly people who tolerate the world of occultism and who are in-
capable of successfully challenging it. The thin cord of Western ra-
tionalism cannot, apart from Christian values, support the weight of
today’s occult-laden civilization. If men insist on power apart from
God-ordained meaning, then they will seek power any way they can.
If rationalism works, fine; if occultism works, fine; if a fusion of the
two works, fine. But ultimately, the Bible informs us, none of this
works. Power cannot be attained in the long run by those who seek it
and it alone.*

This ‘has not been understood by twentieth-century fundamen-
talists. They saw the power of “the power religion,” and they

2. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
Texas: Dominion Press, 1985).

3. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
Texas: Institute for Christian Economics, 1985); R, J. Rushdoony, Institutes of
Biblical Law (Nutley, New Jersey: Craig Press, 1973).

4. Gary North, Moses and Pharaoh: Dominion Religion versus Power Religion (Tyler,
‘Texas: Institute for Christian Economics, 1985).
