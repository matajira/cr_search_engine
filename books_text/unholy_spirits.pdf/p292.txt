286 UNHOLY SPIRITS

name that all others should conquer nature. He was no shrinking
violet. Yet the result of his faith was a kind of impotence. All money,
he admitted, slipped through his fingers. “Every human affection
that He had in His heart—and that heart aches for Love as few
hearts can ever conceive — was torn and trampled with such infernal
ingenuity in His intensifying torture that His endurance is beyond
belief.” As he says, his children died, his wives drank themselves to
death or went mad.” In short, his life was a disaster. But he invari-
ably capitalized his name and spoke of himself in the third person, as
if he were a god.

Crowley was at least consistent. He knew the origin of his faith,
for he spoke of “Chaos, the All-Father." There is no law in the
world, no truth, except for his own writings. He had to believe in
“Gods,” for these are nothing more than the laws of nature, “and thus
their ‘Wills’ are immutable and absolute.”28 Yet there is no law, ex-
cept for his famous dictum: “Do what thou wilt shall be the whole of
the law.” There is no devil. There are demonic hosts who can serve
men.” These demons are fallible, and they can be manipulated
emotionally, Every fact is autonomous; there is no law-order that
stands in relation to man.% Yet the magician is nonetheless sover-
eign: “For there is no power either of the firmament or of the ether,
or of the earth or under the earth, on dry land or in the water, of
whirling air or of rushing fire, or any spell or scourge of God which is
not obedient to the necessity of the Magician!” And to achieve this
absolute sovereignty over nature, the magician must submit entirely
to the invisible forces of secret intelligence ~ demonic spirits who are
not infallible, as he informs us: “It is equally necessary that the
muscles with which he manipulates the apparatus of divination must
be entirely independent of any volition of his. He must lend them for
the moment to the intelligence whom he is consulting... .””
Chaos, in Crowley’s world, is indeed king, the All-Father. His is the
world of terror and destruction: “There is a Magical Operation of

22. Ibid., p. 127.

23. Ibid., p. 42.

24. Tbid., pp. 36, 62, 144, 256-57.
25. Ibid., p. 24n.

26. Ihid., p. 193.

27. Ibid., pp. 167-69.

28. Ibid., p. 164

29. Ibid., p. 65.

30. Tbid., p. 164.
