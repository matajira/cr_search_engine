328 UNHOLY SPIRITS

escape for millions. So is the doctrine of reincarnation, a form of per-
sonal evolution. So is the doctrine of scientific man, the master of
technology. The technological society is a Western development, and
it grew out of biblical presuppositions. Its strength is also its weak-
ness. Men believe in their own autonomy and forget about God and
His law. But then they refuse to believe in demons. What Satan
wants is the coming of the materialist magicians: people who believe
in him but not in God.

This is the threat. The chariots of the gods from outer space re-
place the chariots of fire in the Bible. Men believe that they can pre-
serve their metaphysical autonomy as sons of the ancient spacemen.
No God created them or their world. (They never seem to ask them-
selves: “Who created the spacemen?”) By preserving their metaphy-
sical autonomy, people believe that they can safely preserve their eth-
ical autonomy. They can safely continue to forget God. They adopt
the ancient teachings of Eastern mysticism in the name of a coming
higher technology, They use occult communications techniques in
order to “keep the channels open” with high-tech space visitors: auto-
matic writing, trances, ESP, clairvoyance, yogic meditation, and so
forth.9? You would think that these visitors could at least provide
contactees with cheap transistor radios equal in efficiency to a Japan-
ese $20 import! They don’t.

For scientists, there is the time-worn Darwinian doctrine of evo-
lution through natural selection, or the “new, improved” version
offered by Harvard professor Stephen J. Gould, the “rapid, macro-
evolutionary leap” version. For the less sophisticated, there are the
popular science books on UFO's. And for the easily misled, there are
books by contactees, with their messages of cosmic evolution and im-
minent leaps of being. The difference between Prof. Gould’s vision
of evolution and the First Intergalactic Church of Cosmic Vibra-
tions’ version has more to do with style than the reliability of evi-
dence. The goal is the same: to evolve into God and out of eternal
danger.

99, Wilson and Weldon, Close Encounters, pp. 180-81

100. Stephen Jay Gould, “Evolution: Explosion, Not Ascent,” New York Times
(Jan. 22, 1979}; Gould, Ever Since Danvin (New York: Norton, 1977); The Panda's
Thumb (New York: Norton, 1980). Cf. Steven N. Stanley, The New Evolutionary Time-
table (New York: Basic Books, 1982). An altcrnative explanation for these supposed
“macro-cvolutionary leaps” is found in Rupert Sheldrake, The New Science of Life: The
Hypothesis of Formative Causation (Los Angeles: Tarcher, 1981)
