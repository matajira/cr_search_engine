118 UNHOLY SPIRITS

random change there is stability; the laws of probability. Random
changes operate, says modern science, by the fixed laws of probabil-
ity. These are the test of coherence in any series of events. It is a rig-
ged test, however.

Paranormal science experimenters have found that statistical
probability is a one-way street within conventional scientific circles.
It is allowed to screen out paranormal phenomena, converting them
into random events within a normal distribution. When these para-
normal patterns persist, establishment scientists either blame the ex-
periment’s control procedures or, in a last-ditch effort to sweep their
universe clear of noumenal influences, appeal to luck—good old
lady luck. (You can lead an establishment scientist to a significant
deviation, but you can’t make him swallow the paranormal camel.)
As one commentator summarizes the situation: “All scientific insight
rests on some reasonably invariant relationship. The structure of an
experiment must be such that when A is done in the lab, B occurs.
Psychic research seemed to hypothesize that, when all known factors
are controlled, any score deviating from chance would be by defini-
tion due to ESP—which continued to mean telepathy to parapsy-
chologists, luck to critics.”

Every time a parapsychologist discovers a significant deviation
from normal events, he is tempted to think that he has shown that
the prevailing scientific world view is flawed, and that evidence of its
flawed nature now exists. “Meaningless lucky event,” reply the es-
tablishment critics. In short, “Results based solely on odds against
chance could simply not be accepted as definitive proof, they had to
be confirmed in turn by a repeatable laboratory experiment under
controlled conditions which would show that a signal had traveled
from one point outside the mind of a human being to some point
within. No such experiment had ever been accomplished.” The
scientific guild still prefers to have something concrete to measure,
even when, in Heisenberg’s indeterminate world, the very act of
measurement distorts the experiment’s results. The protection of
phenomenal science is the highest priority of the establishment scien-
tist; this is the very essence of establishment science.

This is why no such measured experiment could be devised and
still remain paranormal; as soon as some “signal” is discovered, if
such a signal exists, then it immediately becomes absorbed into the

94. Sage, “ESP? p. 57.
95. Ibid., pp. 57-58.
