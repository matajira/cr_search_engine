188 UNHOLY SPIRITS

crazy. [Not because she likes to peer into crystal balls, you under-
stand, which for Washington is not all that weird, but because she
was predicting a Truman victory, which involved the essence of
craziness as defined by Washington, namely, arguing against the
polls.} Let's try it again with the cards, to see if I still get the same vi-
brations.”** She did, and Truman won.

My favorite Jeane Dixon prophecy about politics was an-
nounced in the Los Angeles Times (May 11, 1973, Part IV): “Dixon:
Watergate Will Help Nixon.” Speaking before the Mary and Joseph
League, she prophesied that Watergate “may not help the Presicent’s
image now, but it will later.” Watergate “will put Nixon in a position
to right a great wrong for the United States. . . . This will come to
pass.” What else will come to pass? National unity. “I feel that Presi-
dent Nixon will unite us as we have not been united for a long time
and, strange as it seerns, Watergate will be the catalyst.” It was, too:
the whole country was united against Nixon, and fifteen months later,
he resigned in disgrace. Her crystal ball must have been a little
cloudy that day.

Crystal balls, cards, vibrations, voices, visions: What is left? As-
trology. She claims that she never uses astrology. Not that she thinks it
is nonsense. Quite the contrary; she was taught the art, she says, in
her teens by a Jesuit priest (a statement which will be readily be-
Heved by all Dominicans), The problem with astrology, according to
her, is that it takes too much time. There are more efficient ways of
divining the future. Instead of constructing complex astrological
charts, Mrs. Dixon has found a short cut, vibrationally speaking: in
reading for Miss Nichols and other strangers, Jeane almost invari-
ably asks their birthday. “I do that for the rising and setting signs,”
she explains, “because it helps in my meditation to see which direc-
tion they’re going. I don’t ask for the minute of their birth, because I
don't want to be influenced by what their horoscope charts would
say. I just like to know their rising and setting signs, so that I can
pick up their correct vibrations.”#”

Jeane Dixon is, if nothing else, a vibrant woman. How is it that
a crystal-gazing, card-consulting, vision-receiving reincarnation-
believing wife of a divorced man could be regarded by everyone, in-
cluding the Roman Catholic Church, as an orthodox and upright

45, Ibid, p. 83.
46. Ibid., p. 26.
47. Ihid., p. 94.
