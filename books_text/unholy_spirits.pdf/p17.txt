Introduction i

changed . . . for the worse.

In entertainment and popular literature, magic and witchcraft
have fused with fantasy and science fiction to give us a whole new
range of occult options: Dungeons and Dragons, wizardry, Conan
the Barbarian, and an endless number of Saturday morning cartoon
shows for children displaying the wonders of the occult, “Ghostbust-
ers” was the biggest money-making movie in 1984. The Rolling
Stones’ facial lines are rapidly becoming middle-aged wrinkles, but
they are still able to draw huge audiences all over the civilized world.
Jumping Jack Flash is still jumping. The beat goes on.

The former youth culture also has lowered its standards, by rais-
ing the age limits of acceptable toleration. These days they don’t
trust anyone over 50; before, it was anyone over 30. And a job at
Merrill Lynch doesn’t look so bad any more, either, especially if the
Dow Jones industrial average keeps going up. But will it? And if it
doesn’t, and the economy begins to unravel, what will our former
flower children do? Even more to the point, what will the Weather-
men do? We are sitting on top of a social volcano. It is inactive, not
dead.

The Shattered Foundations

There have been significant changes since 1970. The Soviets
have caught up with the West and have surpassed us in terms of
armaments, Solzhenitsyn’s Gulag Archipelago has at last brought real-
ity into the thinking of the West’s intellectuals concerning Soviet civi-
lization. The U.S. economy has been through the inflation-recession
wringer several times, and the rapidly escalating government and
private debt load threatens Western economies everywhere. The
New Deal welfare programs are geriatric and shuffling with the aged
gait of the octagenarian. The Great Society’s war on poverty is now
known to have increased poverty.®

The self-confidence of the liberals is gone. The self-confidence of
the economists has gone with it. France’s Jean Francois Revel’s re-
markable book, How Democracies Perish (Doubleday, 1984), is an epi-
taph on modern democracy’s inability to defend itself against dedi-
cated, relentless Communist totalitarianism, and it has won rave
reviews from Democrats everywhere. We are at the end of an era.

8. Charles Murray, Lasing Ground: American Social Policy, 1950-1980 (New York
Basic Books, 1984).
