The Biblical Framework of Interpretation 57

The Growing Literature of Occultism

There is such an immense and growing quantity of information
available on occultism, written from so many different viewpoints,
that investigators are likely to give up in despair. How is it possible
to assimilate all of this material? Obviously, it is not possible, any
more than it is possible to assimilate all the economic data or chemi-
cal data or any other kind of data that might present itself to a ser-
ious researcher. The quest for exhaustive knowledge is illegitimate.
Men are creatures with creaturely limitations, That they are not
God, with God’s comprehensive and authoritative knowledge of all
factuality, should not disturb men, Yet the ideal of exhaustive knowl-
edge has placed modern scientific man on an endless treadmill, a
kind of modern quest for the holy grail (itself a magical image). Man
thinks that if he does not know everything exhaustively, he cannot
know anything confidently. On the presuppositions of the pretended
autonomy of human thought, this is precisely the case. Since any
fact in the universe can conceivably influence any other fact, in or-
der to know any fact truly, a man must understand all facts exhaust-
ively. Yet no man or society can know all the facts of the universe ex-
haustively. So, then, where can we go to inform ourselves of the eter-
nally valid principles of interpretation for the facts of the occult
realm? I go to the Bible.

The Bible teaches man humility, but it also teaches man confi-
dence: humility before God and confidence under God that the earth
can be mastered by man to the glory of God. God, as the creator and
sustainer of all facts, provides man with the necessary framework of
interpretation that enables man to know facts accurately enough to
accomplish his assigned tasks. The key is the interpretive framework
rather than the quantity of raw data. In fact, facts are never “raw”;
they are always interpreted facts, since they exist in terms of God’s
sovereign plan. There is no brute factuality; there is only God-
ordained factuality. Therefore, man can have confidence im his in-
tellectual labors if he subordinates his investigative labors to the in-
terpretive framework provided to him in biblical revelation. Because
of the existence of revelation, man need not know every fact —in this
case, facts about oceultism—in order to have accurate knowledge
concerning the world. More time spent in study, more facts, better
facts, and more attention paid to literary style no doubt would im-
prove people’s knowledge of occult phenomena, but as limited crea-
