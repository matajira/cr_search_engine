The World of a Sorcerer 151

When he came back to normal, he felt a loss. “I longed for the
‘unknown’ where my awareness was not unifted.”® He had experi-
enced a form of pseudo-transcendence. He had attained a higher
consciousness, His own multiple reality had entered into new reali-
ties of perception. He longed to attain other perceptions like this
one. As the book ends, he apparently recaptures the state of altered
consciousness.

What is man in this perspective? He is simply a cluster of percep-
tions held together by we know not what. “At death, however, they
sink deeply and move independently as if they had never been a
unit.”% Man is dispersed to “the vastness.”®*! There is no threat of
judgment, for there is no guilt, “because to isolate one’s acts as being
mean, or ugly, or evil was to place an unwarranted importance on
the self,” as Castaneda summarizes don Juan’s teaching.” There is
no second death, These words are no doubt as comforting to the
minds of modern men as they were to Indian mystics a thousand
years ago. When it comes to questions concerning final judgment,
man-— the creator and sustainer of the rational universe —is not to
worry, for to worry about such questions is to place “unwarranted
importance” on his insignificant self. Strategically professed humility
is the escape hatch of ancient as well as modern man.

“One,” says don Juan, “can arrive at the totality of oneself only
when one fully understands that the world is merely a view, regard-
Jess of whether that view belongs to an ordinary man or to a
sorcerer.”®? Such a world is indeterminate, and an indeterminate
universe does not hand out final judgments. The appeal of such a
cosmology to modern man should be obvious. Man the perceiver-
creator is safe. The perceived unity of the world is an illusion—a
convenient illusion, as well as an inexplicable one —so there are no
ultimate questions worth asking. Since man constructs his universe,
he is not answerable to anyone else.

Don Juan, an “uneducated” Indian of the Southwest, has the
same view of the world and man’s role in it that Immanuel Kant
struggled to attain by means of rigorous logic. Man makes up the
rules and serves as the game's only umpire. “The tonal makes the

89. Bbid., p. 263.
90. Ibid., p. 266.
91. Fxtlan, p. 155
92. Ibid., p. 183.
93. Tales of Power, p. 240.
