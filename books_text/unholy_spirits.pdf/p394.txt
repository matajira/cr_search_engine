388 UNHOLY SPIRITS

power, you will be skeptical — even hostile—to those Christians who
preach victory prior to Christ’s coming.

Mr. Hunt is not so outspoken as Rev. Wilkerson. He does not
categorically announce as some sort of Spirit-filled prophet that
America will not repent. He writes concerning his message of the
coming Armageddon: “If the world would take these warnings seri-
ously and repent, God might withold His judgment. He has done so
in the past, as in the case of Ninevch, which repented when Jonah
warned of coming destruction.” But he reminds us that “Armaged-
don isn’t going to go away just because we all determine to think
positively.”!9

 

“Positive Confession” and Eschatology

He then attacks Robert Tilton, whose satellite television network
is gaining the support of thousands of charismatic Christians. About
2,000 churches receive Rev. Tilton’s broadcasts, and the number is
growing rapidly. He started it in late 1982. As a member of the charis-
matic “positive confession” movement— sometimes called “name it
and claim it”~ Rev. Tilton’s message is optimistic. God does not in-
tend for His people to be poor and sick. (Rev. Tilton has therefore
adopted the principle underlying one of the aphorisms that has gov-
erned my own life: “It is better to be rich and healthy than it is to be
poor and sick.”) Mr. Hunt points out that the language used by other
“positive confession” ministers is similar to the man-deifying lan-
guage of the New Age “positive thinking” theology. There is no
doubt that his accusation can be documented, and that some of these
leaders need to get clear the crucial distinction between the imputed
human perfection of Jesus Christ and the non-communicable divinity
of Jesus Christ. This Creator-creature distinction is the most impor-
tant doctrine separating the New Agers and orthodox Christianity.

But then Mr. Hunt makes a very revealing statement. He im-
plicitly associates New Age optimism with an optimistic eschatology.
He recognizes (as few of the “positive confession” leaders have
recognized) that they have become operational postmillennialists.
They have abandoned the mind-set of premillennial, pre-
tribulational dispensationalism, even though they have not made
this shift known to their followers, who still profess faith in dispensa-
tionalism. He sees clearly that a new eschatology is involved in

19. Dave Hunt and ‘I. A. McMahon, The Seduction of Christianity: Spiritual Discem-
ment in the Last Days (Rugene, Oregon: Harvest House, 1985), p. 216.
