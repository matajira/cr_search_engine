338 UNHOLY SPIRITS

istic, evolutionary philosophy was condemned by the Roman Catho-
lic Church. He was far more Buddhist or Hindu in his religious per-
spective than Christian.

The Coming “Leap of Being”

Alchemy follows the lead of magic, arguing for the principle that
“as above, so below.” All being is at bottom one, or monistic. It is
therefore man’s task to bridge the gap between his own temporarily
limited being and God’s eternal being. (The fact that Luther was a
supporter of alchemy therefore comes as a shock, for his theology
mitigates against the monism that undergirds alchemy.) With its
roots in gnosticism, alchemy was self-consciously committed to the
divinization of man.2! Pauwels and Bergier wax eloquent about the
alchemist’s quest: “The ‘Great Work’ is done. The alchemist himself
undergoes a transformation which the texts evoke, but which we are
unable to describe, having only the vaguest analogies to guide us.
This transformation, it seems, would be, as it were, a promise, or
foretaste, experienced by a privileged being, of what awaits human-
ity after attaining the very limits of its knowledge of the earth and its
elements: its fusion with the Supreme Being, its concentration on a
fixed spiritual goal, and its junction with other centers of intelligence
across the cosmic spaces. Gradually, or in a sudden flash of illumina-
tion, the alchemist, according to tradition, discovers the meaning of
his long labors. The secrets of energy and of matter are revealed to
him, and at the same time he glimpses the infinite perspectives of
Life. He possesses the key to the mechanics of the Universe. He es-
tablishes a new relationship between his own mind, which from now
on is illuminated, and the Universal Mind eternally deepening its
concentration.”22

The manipulation of metals brings on the sudden flash of illumi-
nation, the higher consciousness which is the goal for all occult groups.
This key phrase — higher consciousness — serves as a kind of talisman
among radical humanist groups. Consciousness-raising within
Women’s Liberation, the new consciousness of Esalen and the other

20. Eliade, Forge, pp. 190-91. Eliade relies on the study of Lutheran alchemy writ-
ten by the Lutheran scholar, John Warwick Montgomery, “Cross, Constellation,
and Crucible: Lutheran Astrology and Alchemy in the Age of the Reformation,”
Transactions of the Royal Society of Canada, I Ser. 4 (Junc 1963), Sect. II, pp. 251-700.

21, It is the thesis of this book that man’s self-deification is the very heart of the
religion of Satan—the temptation to be as God.

22. Pauwels and Bergier, Moming, p. 137.
