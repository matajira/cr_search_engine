300 UNHOLY SPIRITS

really deliberate. Maybe they are meant to foster the belief that the
objects are real and mechanical.”*

Their operators have never really come out in the open, though
human “contactees” have repeatedly been promised that the “space
brothers” are about to reveal themselves. (Gypsy Rose Lee success-
fully used a similar technique to maintain interest by observers.)
Furthermore, there are too many kinds of spacecraft. It is as if a
stellar civilization designed hundreds of kinds of craft, and then sent
them to play games with earthlings—and only in great numbers
after 1945. They appear to people selectively. They appear on radar
screens selectively.

What about speed? The vehicles accelerate too rapidly; they vio-
late physical laws. They do not seem to get hot despite their speed.
Sometimes they are completely silent; sometimes they are noisy —
clanging, Model T noisy. They change shape, disappear, pass
through clouds without moving the clouds. One spacecraft will split
into two spacecraft.*”7 The occupants do not splatter against the wall
when ships travelling 18,000 miles per hour suddenly change course
at a 90-degree angle. Hynek and Vallee put it thusly: “If UFOs are
indeed somebody else’s ‘nuts and bolts hardware,’ then we must still
explain how such tangible hardware can change shape before our
eyes, vanish in a Cheshire cat manner (not even leaving a grin),
seemingly melt away in front of us, or apparently ‘materialize’ mys-
teriously before us without apparent detection by persons nearby or
in neighboring towns. We must wonder, too, where UFOs are ‘hid-
ing’ when not manifesting themselves to human eyes.”®® In short,
nothing adds up in the Newtonian account book.

Problem: if they are not physical phenomena, how can they be
phenomena at all? If they do not conform to physical laws, then
what is the truth? Are the laws incorrect? (Crisis!) Are the UFO’s an
illusion? If so, then how can we trust our powers of observation?
(Crisis!) Is our knowledge of the true physical laws incorrect?
{Slightly smaller crisis, but still a crisis.) Is the universe governed by
noumenal sorts of events? (Greatest crisis of all!) What are we to
believe?

36. Ibid., p. 64.

37. Clifford Wilson and John Weldon, Clare Encounters: A Better Explanation, pp.
38-41,

38. Edge of Reality, pp. xii-xiii
