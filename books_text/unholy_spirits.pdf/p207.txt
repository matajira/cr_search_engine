Edgar Cayce: From Diagnosis to Gnosis 201

pensatory, listing all available therapeutic drugs. There was no bal-
sam of sulphur. Then, in the attic, they stumbled across an old de-
funct catalogue, put out fifty years before. They dusted it off, opened
it, and there found balsam of sulphur.”!8

Even more eerie was his prescription, “oil of smoke.” Again, no
catalogue listed it. No one had heard of it. They took another read-
ing. The name of a Louisville drugstore was given. Ketchum wired
the store for a bottle. The reply: never heard of it. “We took a third
reading. This time a shelf in the back of the Louisville drugstore was
named, There, behind another preparation—which was named—
would be found a bottle of ‘Oil of Smoke,’ so the reading said. I wired
the information to the manager of the Louisville store. He wired
back, ‘Found it.’ The bottle arrived in a few days. It was old. The
label was faded. The company which put it up had gone out of busi-
ness. But it was just what he said it was, ‘Oil of Smoke.’” This story
was related to Professor Miinsterberg, or so Sugrue reports.’ It is
unlikely that Ketchum would make himself appear to be a fool in
front of a Harvard professor. Cayce did possess astounding powers.
Or, to put it more accurately, astounding powers possessed Cayce.

Ketchum related some of the Gayce stories to a convention of
homeopathic physicians at a 1910 convention. He was asked to put
his thoughts down on paper by Dr. Krauss, who then read it before a
Boston meeting of the American Association for Clinical Research.
The New York Times picked up the story and ran it on October 9,
1910. These were before the days of the “new journalism” or “inter-
pretive journalism”; hence, it was run as a straight news story. Other
papers picked up the story from the Times (some things never
change), and an army of reporters descended on Hopkinsville.”
Local physicians began crying for Ketchum’s scalp, but he backed
them down successfully. It was the first and last time that Cayce was
to receive national attention from the media. Only with the publica-
tion of Sugrue’s book in 1942 did the nation have the opportunity to
hear the. story of Edgar Cayce during his lifetime. Cayce finally
broke with Ketchum over the question of his using his readings to do
a bit of gambling with fees paid into a supposed hospital fund. He
moved to Selma, Alabama for a time. He tried to find oil by means

18. Jess Stearn, Edgar Cayve— The Sleeping Prophet (New York: Bantam, [1967]
1971), p. 105.

19. Sugrue, River, p. 21.

20. H. L. Cayce, Venture Inward, pp. 36-39.
