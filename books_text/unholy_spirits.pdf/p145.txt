The World of a Sorcerer 139

He has to be, so to speak, the master of his choices. He must fully under-
stand that his choice is his responsibility and once he makes it there is no
longer time for regrets or recriminations. His decisions are final, simply be-
cause his death does not permit him time to cling to anything.

Castaneda may be inserting’bits and pieces of Western philoso-
phy into the old man’s mouth, but the basic themes are repeated over
and over in various contexts. The themes of death and detachment,
coupled with total affection for earthly life, pervade his teaching. At
the end of the fourth volume, Castaneda quotes the old man: “This
earth, this world. For a warrior there can be no greater love.” This
same perspective undergirds the widely publicized Church of Satan
in San Francisco, ran by Anton LaVey: “Life is the great indulgence
—death, the great abstinence. Therefore, make the most of life—
HERE AND NOW!"* Don Juan’s philosophy is simple enough,
and it is exceedingly modern: “Life in itself is sufficient, self-explana-
tory and complete.”55

Escape from Meaning

Total detachment is supposed to give a man a lust for life. A uni-
verse in which death lurks—death that literally stalks a man —is sup-
posed to promote the philosophy of life. Life is everything, yet it is
nothing. This is the vital attitude that don Juan calls “controlled folly.”
“But we must know first that our acts are useless and yet we must
proceed as if we didn’t know it. That’s a sorcerer’s controlled folly.
This perspective must be used with everyone else at all times. The
sorcerer is an actor; he does not let those around him know that he
thinks that they are irrelevant. “Once a man learns to see he finds
himself alone in the world with nothing but folly.”*’ In short, “every-
thing I do in regard to myself and my fellow man is folly, because
nothing matters.” The modemism of don Juan’s outlook is striking.
A meaningless universe must be dealt with in terms of a philosophy
of ultimate meaninglessness and the concomitant quest for power.

32, Ibid., p. 151.

33. Tales of Power, pp. 284-85.

34. Anton Szandor LaVey, The Satanic Bible (New York: Avon, 1969), p. 33. By
late 1972, this book was in its eighth printing.

35. Tales of Power, p. 59.

36. Separate Reality, p. 77.

37. Ibid., p. 81.

38. ibid., p. 80.
