148 UNHOLY SPIRITS

The goal, then, is to “stop the world,” to alter our perception of
the world. “To change our idea of the world is the crux of sorcery,” he
tells Castaneda. “And stopping the internal dialogue is the only way
to accomplish it, The rest is just padding.””? The rest—drugs, bodily
exercises, secrecy, special foods —is mere padding. The goal is a higher
consciousness. At last, he reveals the sorcerer’s secret:

We, the luminous beings, are born with two rings of power, but we use
only one to create the world, That ring, which is hooked very soon after we
are born, is reason, and its companion is talking. Between the two they con-
coct and maintain the world,

So, in essence, the world that your reasen wants to sustain is the world
created by a description and its dogmatic and inviolable rules, which the
reason learns to accept and defend.

The secret of the luminous beings [living beings] is that they have
another ring of power which is never used, the will. The trick of the
sorcerer is the same trick of the average man. Both have a description; one,
the average man, upholds it with his reason; the other, the sorcerer, upholds
it with his wl. Both descriptions have their rules and the rules are
perceivabte, but the advantage of the sorcerer is that will is more engulfing
than reason.

The suggestion that I want to make at this point is that from now on you
should let yourself perceive whether the description is upheld by your reason
or your will. I feel that is the only way for you to use your daily world as a
challenge and a vehicle to accumulate enough personal power to get to the
totality of yourself.

The true prestidigitation of the sorcerer is intellectual and philo-
sophical. It is the same trick employed by the humanist. From a po-
sition which insists that man is no better than an animal, that it is a
crime to think of oneself as superior to the animals, we come to man
as the sustainer of the universe, man as the total being. What mat-
ters to a warrior “is arriving at the totality of oneself.”*! Man is the
focus. Man is central once again. It is the same sleight-of-brain trick
that modern evolutionists indulge in: man, the product of chance,
the product of slime, somehow becomes man the mind-endowed dis-
continuous leap in the continuous evolutionary chain. Man-the-
director-of-evolutionary-processes somehow emerges from man-the-

79. Tales of Power, p. 22.
80, Zbid., p. 101.
81. Mbid., p. 13.
