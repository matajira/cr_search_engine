370 UNHGLY SPIRITS

man” of the American Revolution who was probably most responsi-
ble for getting it started with his agitation in Boston and his creation
the Committees of Correspondence was Sam Adams, who was 43 in
1765. These men were unknown to the political leaders of Britain,
and certainly unknown to King George IT, yet they changed the
course of history. In 1788, the same could have been said of Robes-
pierre, Danton, Marat, and the other obscure men of the French
Revolution, yet within a year, they had begun the process which was
to overthrow European civilization by 1815.

Igor Shafarevich is a human rights protester in the Soviet Union.
He is a well-known mathematician, and he contributed an essay to
Alexander Solzhenitsyn’s collection of essays, From Under the Rubble
(1974), In his brilliant history of socialism, he points out: “At the mo-
ment of their inception, socialist movements often strike one by their
helplessness, their isolation from reality, their naively adventuristic
character and their comic, ‘Golgolian’ features (as Berdyacv put it).
One gets the impression that these hopeless failures haven’t a chance
of success, and that in fact they do everything in their power to com-
promise the ideas they are proclaiming. However, they are merely
biding their time. At some point, almost unexpectedly, these ideas
find a broad popular reception, and become the forces that deter-
mine the course of history, while the leaders of these movements
come to rule the destiny of nations.””* What he writes concerning
early socialist sects can be applied quite well to all successful revolu-
Uonary groups: they begin small, they look foolish, and eventually
they take over society. Most fail; some win.

Satan Can't Go Home Again

The question is, which of today’s “fringe groups” will win? Here I
must indulge in a bit of prophecy. Prophecy is the proper application
of fixed biblical principles to future situations. Time will tell if my
prophecy is correct. I begin with the biblical presupposition that Azs-
tory is Linear. Time does not go back. History does not repeat itself, al-
though historians repcat cach other. Whether we are talking about
God’s kingdom or Satan’s attempted kingdom, the church or the
satanic host, they do not go backwards. They may atternpt it. Chris-
tians may join self-proclaimed “primitive” churches, or “first-century”
churches, but they are fooling themselves. ‘fime marches on, God’s

76. Igor Shafarevich, The Socialist Phenomenon (New York: Harper and Row,
[1975] 1980), p. 129.
