62 UNHOLY SPIRITS

sical. It involved his whole being, body and soul, and the curse of
God in response to this rebellion penalized both body and soul, in-
cluding man’s mind. Nevertheless, the Fall itself was not a metaphy-
sical “fail from Being.” It was not some lack in man’s being which led
to his downfall; it was willful rebellion. The curse of God on man in-
volved the creation in which man was now to labor (Gen. 3:15-17);
his world would now be a world of scarcity. But it was not a flawed
environment that was man’s downfall, although Adam blamed Eve,
and Eve blamed the serpent, and they all, by implication, blamed
God for their plight. The Fall was ethical, and therefore the restoration
has to be ethical — not a “leap of being” by mystical means, nor by en-
vironmental manipulation, such as through Marxist revolution.'?

The restoration of man and the creation is promised (Isa. 65, 66;
Rom. 8; I Cor. 15). This is the work of God in calling His people unto
Him { John 17). Restoration is not to be achieved by “secret wisdom”
through initiation into “mysteries.” The words of Jesus in the Ser-
mon on the Mount forbade secret wisdom to His followers. It there-
by forbade mystical secret societies. “Ye are the light of the world. A
city that is set on an hill cannot be hid. Neither do men light a can-
dle, and put it under a bushel, but on a candlestick; and it giveth
light unto all that are in the house. Let your light so shine before
men, that they may see your good works, and glorify your Father
which is in heaven” (Matt. 5:14-15). Restoration, both ethically and
externally, stems from the grace of God—another doctrine ana-
thema both to the modern world and to occult philosophy. Restora-
tion is not the product of men’s neutral reason, nor is it the product
of their own unaided, self-initiated works (Eph. 2:8-9).

 

The Incarnation

The message of the first chapter of the Gospel of John is quite
straightforward: God Himself walked this earth in human flesh dur-
ing a specific period of human history. He appeared in time and on
earth, one person in two natures: perfect humanity and full divinity,
but without confusion of these two natures. The promise given to the
people of God is that each man,while retaining his own personality,
will be conformed to the image of Jesus Christ on the final day
(I Cor. 15:42-50). In short, redeemed man does not become divine; he

12. Gary North, Marx's Religion of Revolution: Regeneration Through Chaas (rev. ed;
Tyler, Texas: Institute for Christian Economics, 1988).
