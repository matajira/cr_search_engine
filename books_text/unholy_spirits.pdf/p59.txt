The Biblical Framework of Interpretation 53

rapidly reduced to a handful of greasy ashes. Paradoxically, manimate
objects nearby escape relatively unharmed.” So much for definitions.
Specialists in forensic medicine are the scientists who are most
likely to encounter the phenomenon, Desperate police officials can-
not explain these phenomena, so they expect the local coroner to
take the responsibility. The specialists prefer to decline the honor.
They would really prefer to deny the phenomena. In 1861, J. L. Cas-
per dismissed the whole thing in his Handbook of the Practice of Forensic
Medicine. His language is all too familiar to anyone who has re-
searched any aspect of paranormal science. The odd phenomena
tend to stay the same, and so do the scientific reasons for rejecting
the evidence. “It is sad to think that in an earnest scientific work, in
this year of grace 1861, we must still treat of the fable of ‘spontaneous
combustion’ . . . the very proots of whose existence rest on the testi-
mony of people who are perfectly untrustworthy nonprofessionals.”?
The phenomenon has been around for a long time. The descrip-
tion does not change very much. Here is a summary account by one
French researcher, published in the Texas Register (Nov. 7, 1835):

Spontaneous combustion commences by a bluish flame being seen to
extend itself, by little and little, with an extreme rapidity, over all the parts
of the body affected. This always persists until the parts are blackened, and
generally until they are burned to a cinder. Many times attempts have been
made to cxtinguish this flame with water, but without success. When the
parts are touched a fattish matter attaches itself to the finger, and still con-
tinues to burn. At the same time a disagreeable smell, having analogy to
burnt horn, spreads itself through the apartment.

A thick smoke escapes from the body and actaches itself to the furniture,
in the form of a sweat, unctious to the touch. In many cases the combustion
is arrested only when the flesh has been reduced to a cinder and the bones to
powder, Commonly the feet and a portion of the head are not burned.
When the combustion is finished an incinerated mass remains, which is
difficult to believe can be the whole of the body. All this may happen in a
space of an hour and a half, It is rather uncommon for the furniture around
it to take fire; sometimes even the clothes are not injured.®

The “Fortean” (disciples of that curious investigator of the cur-
ious, Charles Fort) periodical Pursuit publishes accounts of odd facts
that conventional scientists refuse to deal with. Ivan T. Sanderson

7. Quoted by Larry Arnold, “Human Firebulls,” Science Digest (Oct. 1981), p. 90
8. Mysterious Fires and Lights, p. 187.
