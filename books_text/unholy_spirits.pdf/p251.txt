Demonic Healing 245

96% . The researchers, being men of good faith, assumed that in the
27 discrepancies, the physician was correct and Arigo was wrong.
These diagnoses, remember, took about 30 seconds each. Movies
were made of the operations. More photographs were taken.*! Still,
they needed more data. Always more data were required. Paranor-
mal scientists are forever fearful of the standard charge: insufficient
observations to permit statistically significant correlations. If one in-
stance of a knife being poked in someone’s eye does not convince the
orthodox scientists, then perhaps 500 instances will. If he thinks you
have faked one set of films, possibly 500 sets will convince him.

Another expedition would be needed. This time, the questions to
be asked would be restricted even more —so restricted, in fact, as to
be virtually irrelevant in making any sense out of the observed data:
“Paramount in the plans was the study of those aspects of Arigo’s
work which could be explained and related to accepted modern med-
ical theory and practice. . . . The ‘voice’ that told Arigo his amaz-
ingly correct diagnoses would be considered, but the statistics on the
actual diagnoses and treatment would come first.”®? As for the Karde-
cist explanation of Arigo’s non-miracles, that he was a vessel for a
God-given energy, or higher form of energy, it could be ignored
completely. “hese concepts were interesting, but utterly useless for
the medical researchers, who needed more statistical information on
the hard, observable facts. It is one thing to be convinced by direct
observation. It is another to articulate the facts in a form acceptable
to the editors of a scientific journal, who must have precedents and
previously documented material to fall back on.”

This hope in getting published was naive. Here we find the old
“egg before the chicken, chicken before the egg” routine: no prece-
dents can be set without access to the journals, but the journals re-
quire precedents. The thousands upon thousands of cases of impos-
sible events compiled by Charles Fort and his followers never crept
into respectable journals. The idea of these journals is to keep out overly
disturbing precedents. One can never be sure just where a precedent
may lead, so editors handle them very carefully. Anyone who doubts
that outright suppression goes on should read Alfred de Grazia’s The
Velikovsky Affair, which demonstrates how members of major univer-
sities, including Harvard, pressured Velikovsky’s publisher, Mac-

 

31, Dbid., p. 168.
32, Tbid., p. 174.
33. Ibid., p. 195.
