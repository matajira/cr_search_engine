416 UNHOLY SPIRITS

Koch, Kurt, 4, 267-8
Koestler, Arthur, 121-22

Kolchak, 66

kooks, 56

Krishnamurti, 356

Krogman, Wilton, 54-55

Kuhn, Thomas, 12, 23-24, 242, 312

laboratory, 261
lake of fire, 72
Lammers, Arthur, 202, 203
language, 145, 205-6
Lanham, Richard, 125
Latal, Gerald, 269
LaVey, Anton, 139, 282
law
biblical, 158, 379
chance &, 39
convention, 35
deadening, 155
faith in, 132
God’s, 158, 373
humanistic, 377
large numbers, 121-22, 167
natural, 33-34, 75, 302
Newtonian, 44
progress &, 158, 276
scientific, 45
law of large numbers, 121-22, 167
Layne, Al, 198-200
Lea, Henry C., 47
leaf, 99
leap of being
Alchemy, 338-39,
Darwinian, 133, 148
magic, 342
New Age, 371
spacemen, 306-7
“supernature,” 85
theology of, 62-64,
Timothy Leary, 318
UFO's, 292,
See also divinization, self
transcendence, Gen. 3:5
Leary, Timothy, 125, 278, 318, 350
Leek, Sybil, 206
Leff, Gordon, 31-32

Left Hand Path, 282-83
Lewis, C. 8.
death of, 3
inner ring, 397-99
materialist magician, 89-90, 326-7
nature/freedom, 37-38
power, 130-32
Renaissance, 4
Screwtape, 89-90
liberalism
hypocrisy, 171
older, 14, 19, 24
pessimism, 11, 20-21
pragmatism, 5
technocratic, 23
liberals, 348, 358
liberation theology, 12
light switch, 170
limiting concept, 37
Lindsey, Hal, 382-83, 395
“little green men,” 306, 323
“ttle people,” 196
lizard, 183
Loch Ness Monster, 330
Lodge, Oliver, 46
Lomax, Louis, 257-58
Lord’s prayer, 390
lower class, 155
LSD, 10, 24, 349, 350, 351, 353
luck, 118, 280
lucky days, 65
Luther, Martin, 338, 400
lying, 134

Macdonald, Margaret, 381, 389
macrocosm, 335, 337
magic
Castaneda, 129
division of labor, 283
golden age, 275-76
Middle Ages, 21
moral flux, 283
poverty, 284
perfection, 135
quest for power, 133
rationalism, 130
revolution, 276
