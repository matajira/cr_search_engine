28 UNHOLY SPIRITS

The neo-pagan mysticism and sun worship of Kepler, however, did
not accompany the spread of his mathematical world view.

‘The success of Descartes, who had experienced an ecstatic illum-
ination in 1619 which had informed him that mathematics is the sole
key for unlocking the scerets of nature, and then the even greater
success of Newton at the end of the century, had elevated mathemati-
cal logic to a position of pre-eminence.?! The capacity of mathematics
to describe the regularities of certain natural and physical processes
convinced many philosophers, most notably Hobbes, that mathemat-
ics is a universal tool which could be used to create a science of man
and society.

Blinded by the dazzling success of Newton in physics and astron-
omy, a success which was vastly greater than the crude measuring
devices of his era could record, men hesitated to inquire into the ap-
parent absurdity of Newtonian science. Why should mathematical
reasoning, an abstract mental skill and even art, be found to corre-
spond to the mechanical processes of the observed world? Why
should such a mind-matter link exist? As the Nobel-prize winning
physicist Eugene Wigner has put it, such a finding is utterly unrea-
sonable.” But the correlation exists.

The Christian knows why the correlation exists. It exists because
man i is made in the image of God, Man has been assigned the task of
ing dominion over the carth as God’s lawful subordinate
(Genesis 1:27-28). Because God exhaustively and perfectly under-
stands His creation, men are able analogously (though not perfectly
and exhaustively) to understand the creation. The creation is not
lawless, for it was created by an orderly Creator who sustains it by
His providential sovereignty. In short, ours is a personal universe. We
are persons made in God’s image, so we can understand our world.
‘The world was not a product of random events, nor are our minds
the product of random evolution. The world was created by Gad.
Mathematics, too, is God-given, and can be understood and de-
fended only as a product of a Creator God.™ Thus, there can be and
is a correspondence between the logic of mathematics and the opera-
tions of the external world.

exert

 

 

 

21. Ibid., p. 105.

22, Eugene Wigner, “The Unreasonable Effectiveness of Mathematics in the Na-
tural Sciences,” Gummunications on Pure and Applied Mathematics, XII (1960), pp. 1-14.

23. Vern 8. Poythress, “A Biblical View of Mathematics,” in Gary North (ed,),
Foundations of Christian Scholarship: Essays in the Van Til Perspective (Vallecito, Califor-
nia: Ross House, 1976).
