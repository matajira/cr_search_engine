90 UNHOLY SPIRITS

duce our perfect work— the Materialist Magician, the man, not using,
but veritably worshipping, what he vaguely calls ‘Forces’ while denying
the existence of ‘spirits’ then the end of the war will be in sight.”*

It is hardly surprising that outside the Iron Curtain countries,
the scientists who are most deeply involved in the study of the para-
normal are psychologists: parapsychologists. Many of them are deeply
Freudian or Jungian. Indeed, Freud himself began investigating
psychic phenomena at the end of his career, and Jung built much of
his analytic structure around occult themes and powers. But the
Soviets are officially materialists; they have no time for such bour-
geois theories as found in Freudian psychology, a fact which un-
doubtedly has made their psychological research far more concrete.
What the Soviets are after, purely and simply, is power.

Take telepathy. Telepathic experimentation never died out in the
USSR, but since 1965, there has been a renewed interest in tele-
pathic phenomena, The most famous telepaths in the USSR are
Karl Nikolaiev, an actor, and Yuri Kamensky, a biophysicist. They
send wireless messages without any form of apparatus. Mental mes-
sages, both visual and numerical. Emotional messages, in which one
emotional experience is transmitted from one to the other over many
miles. Kamensky is the “sender.” These experiments are rigidly con-
trolled by scientists who, initially, are often quite skeptical of the
whole operation. For example, objects are placed randomly in sealed
boxes, and the boxes are then opened randomly by Kamensky. Kam-
ensky then focuses his attention on the contents of the particular box.
Then, 1,500 miles away, Nikolaiev records his mental impressions of
what the object is. He is correct in an astonishing number of cases.
Electrodes attached to Nikolaiev’s head recorded the moment of his
response to the “message” each time Kamensky “sent” one. By com-
paring the times in each laboratory, scientists found that Nikolaiev
received his messages within a matter of seconds after they had been
sent—or at least the responses on the charts indicated a brief delay.
These experiments, which were conducted from 1966 on, were fully
approved by the Soviet academic guild. Yet the 1956 Sovtet Encyclope-
dia had announced: “Telepathy is an antisocial, idealistic fiction
about man’s supernatural power to perceive phenomena which, con-
sidering the time and the place, cannot be perceived.”39

38. G. 8. Lewis, The Screwtape Letters (New York: Macmillan, [1943] 1969), Letter
VIL, pp. 32-33.
39. Ostrander & Schroeder, Psychic Discoveries, p. 20.
