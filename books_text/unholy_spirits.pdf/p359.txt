Escape from Creaturehood 353

What Huxley found was meaningful irrelevance. “I looked down by
chance, and went on passionately staring by choice, at my own
crossed legs, Those folds in the trowsers—what a labyrinth of end-
lessly significant complexity! And the texture of the gray flannel—
how rich, how deeply, mysteriously sumptuous!” He stared, seem-
ingly for an immensely long time, at a chair.“ He had the sense of
being separate from himself.*? Yet for all of these mindless experi-
ences, he calls for more of the same —or better. Alcohol and tobacco
cannot be prohibited, but they are not safe. “The universal and ever-
present urge to self-transcendence is not to be abolished by slam-
ming the currently popular Doors in the Wall. The only reasonable
policy is to open other, better doors in the hope of inducing men and
women to exchange their old bad habits for new and less harmful ones.
Some of these other, better doors will be social and technological in
nature, others religious or psychological, others dietetic, educational,
athletic. But the need for frequent chemical vacations from intolerable
selfhood and repulsive surroundings will undoubtedly remain.”

We need a new, non-habit-forming drug, cheaply synthesizable,
less toxic than present ones. “And, on the positive side, it should
produce changes more interesting, more intrinsically valuable than
mere sedation or dreaminess, delusions of omnipotence or release
from inhibition.”#

A well-known fact about the use of LSD is that the user needs
someone “straight” in the same room with him, or at least someone
who is chained to the perception of reality. Psychologist Paul Stern
writes: “Ordinarily one does not trip alone, but needs at least one
other person who officially functions as guide or as a sort of safety
net.” In a philosophical sense, Hualey wants to find a new hallucin-
ogen which can, in and of itself, serve as a rational, “straight” guide.
No mere dreaminess for him, We need meaning—the educated,
well-read man’s kind of meaning. But this desire is absolutely im-
possible to fulfill.

“In general,” writes Stern, “drug users tend to overestimate the
stimulus value of hallucinogens. Drugs do not ‘cause,’ in any mean-
ingful sense of the term, the so-called hallucinogenic experience, as

45. Ibid., p. 30.
46, Ibid., p. 53.
47. Ibid., p. 60.
48. Ibid., p. 64.
49. Ibid., p. 65.
50. Paul J. Stern, Zn Praise of Madness (New York: Norton, 1972), p. 86.
