Escape from Creaturehood 361

that is primarily for coming generations. Our immediate task, in the child-
hood of mankind, is to work for a universal consciousness.

This is a very revealing editorial statement. First, it displays the
sense of drama: we are present at the creation of a new mankind. A
great metaphysical event is about to take place: the autonomous self-
liberation of Mankind. We are participating in the creation-event,
just as pagan chaos festivals are supposed to allow men to do. Second,
we are to see that the new infant is not misshapen. In other words,
man is about to take control of man, or as C. S. Lewis has warned,
some men are about to take control of all the others. This is a common
theme in New Age groups: the need for the “illuminated ones” to take
charge of the forces of mystical evolution. Evolution is not to be a
democratic process. This is as true for the higher consciousness crowd
as it is for the rationalistic central planners with their computers.

Third, the editor refers to society as an androgynous being, sim-
ultaneously male and female. The image of the androgyne goes back
to pagan religious symbolism. It is one of the most important ele-
ments of alchemy, East and West. The unity of opposites will pro-
duce the Philosopher’s Stone, the transformation of man and nature.
Prof. Molnar’s discussion of this symbol is relevant to any discussion
of the politics of transcendence: “In Theosophy, the androgyne figure
is the symbol of the union of divine and human nature, and hence of
the soul purified in view of ascent to unity in God. The androgyne is
thus the archetype of man, symbolizing the end of passion, the final
reabsorption of creation. . . . Through androgyne or other forms of
Hermetic union the manipulator gains access to magic knowledge and
power in order to contemplate nature’s work and to complete it in a
shorter time than nature would be able to do. Paracelsus was among
those who assumed that nature has a certain direction not decipher-
able except by the true knower, and that this direction pointed toward
the primordial — undivided —state. The task of the Hermetic was to
help nature achieve this end.” It also appeared from time to time in
nineteenth-century revolutionary movements. Prof. Billington com-
ments: “Fhe idea that androgyny (the state of Adam before the fall)
was the only truly hberated human condition had, since [Jacob]
Boehme, been a central concept within the occult tradition.”

64. Thomas Molnar, Ged and the Knowledge of Reality (New York; Basic Books,
1972), pp. 89-90.

65. James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith (New
York: Basic Books, 1980), p. 485.
