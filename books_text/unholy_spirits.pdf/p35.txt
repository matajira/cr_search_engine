The Crisis of Western Rationalism 29

The atheist rejects this explanation. Thus, the correlation be-
tween mathematics and the natural world is unexplainable and ulti-
mately unreasonable for him. Nevertheless, without faith in this cor-
relation, modern science becomes impossible. Cartesian and New-
tonian science rests on this correlation between mind and matter.
Because this correlation between mind and matter exists, the atheis-
tic followers of Descartes and Newton immediately faced a fearful
intellectual problem: What becomes of the free will of “autonomous” man in
an impersonal, mathematically deterministic universe? In a world of cosmic
impersonalism and determinism, what becomes of the human mind?
Is man’s mind simply a ghost in the cosmic machine? Doesn't the
very certainty of mathematical logic spell the end of human freedom,
since man is undoubtedly a part of this universe? Is man ultimately a
machine?

The Hypnotic Lure of Greek Philosophy

These questions, or ones analogous to them, have been with man
from the beginning. Men ask themselves this question: How can we
make sense of a constantly changing world? Answer: only by means
of fixed standards of measurement or comparison. This raises
another question: Where do these fixed standards come from? And
another: How do these fixed standards interrelate with the flux of life?
For thousands of years, men have grappled with these questions.

The ancient Greck philosophers struggled with these dualisms
—structure vs, change, law vs chaos, determinism vs. freedom—in
terms of the so-called form/matier framework. The world was under-
stood as the product of eternal conflict: abstract (but real) metaphy-
sical forms partially subduing raw, chaotic matter. Another varia-
tion of this approach had matter imitating form. The ultimate form
was understood as monistic in nature, the ultimate One. Oué of one
came many, that is, diversity. (This is a basic theme of the New Age
movement today. It is also the basic theme of Eastern mysticism and
ancient pagan occultism.}

As to which had priority, abstract fixed form or fluctuating mat-
ter, Greek philosophers differed. How the two were linked together,
or how one or the other was not swallowed up by the other, or how it
is possible to compare infinite quantities of raw matter or the neces-

24. Herman Dooyeweerd, én the Tiwilight of Western Thought; Studies in the Pretended
Autonomy of Philosophical Thought (Philadelphia: Presbyterian & Reformed, 1960), pp.
39-42.
