392 UNHOLY SPIRITS

Robert Schuller and Norman Vincent Peale. There is no doubt that
some of them have not come to grips with the Bible’s teaching on
Christology: that Jesus Christ in His incarnation was alone fuily
God and perfectly human.* Some of them have verbally equated
Christian conversion with becoming divine. This is unquestionably
incorrect, At conversion the Christian definitively has imputed to him
Christ’s perfect humanity (not His divinity), which he then progressively
manifests through his earthly lifetime by means of his progressive
ethical sanctification, But their confusion of language is a testimony
to their Jack of theological understanding; they mean “Christ's perfect
humanity” when they say “Christ’s divinity.” Those who don’t mean
this will eventually drift away from the orthodox faith.

Unquestionably, getting one’s doctrine of Christology straight
(‘ortho”) is more important than getting the doctrine of eschatology
straight. Postmillennialism with a false Christology is as perverse as
dispensationalism with a false Christology. But a false Christology is
independent of eschatology. Mr. Hunt implies that the poor wording
of the “positive confession” charismatics’ Christology reflects their es-
chatology. It doesn’t. It simply reflects their sloppy wording and their
lack of systematic study of theology and its implications, at least at
this relatively early point in the development of the “positive confes-
sion” movement's history.

Mr, Hunt is well-read. He understands theology. This is why I
find it very difficult to believe that Mr. Hunt really believes that a
Robert Schuller-Robert Tilton sort of alliance is likely. Mr. Hunt
understands far better than most observers what is really taking
place. Indeed, it has already begun: bringing together the postmil-
lennial Christian reconstructionists and the “positive confession”
charismatics, with the former providing the footnotes, theology, and
political action skills, and the latter providing the money, the au-
dience, and the satellite technology.” It began when Robert Tilton’s
wife read Gary DeMar’s God and Government in late 1983, and then
persuaded her husband to invite a group of reconstructionists to
speak before 1,000 “positive confession” pastors and their wives at a
January 1984 rally sponsored by Rev. Tilton’s church. The all-day

24. For the political implications of this creed, see R. J. Rushdoony, The Founda-
tions of Social Order: Studies in the Creeds and Cuuncils of the Early Church (Fairfax,
Virginia: Thoburn Press, [1968) 1978), ch. 7: “The Council of Chalcedon: Founda-
tion of Western Liberty”

25. Gary North, Backward, Christian Soldiers? (Tyler, Texas: Institute for Christian
Economics, 1984), ch. 16: “The Three Legs of Ghristian Reconstruction’s Stoul”
