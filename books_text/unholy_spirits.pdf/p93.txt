Paranormal Science Since 1960 87

Soviet Experiments

If G. Stanley Hall had ever heard about the existence of the book
by Sheila Ostrander and Lynn Schroeder, Psychic Discoveries Behind
the Iron Curtain (1970), he would never have believed a word of it.® If
Joseph Stalin had ever heard of it, he would have had a copy trans-
lated; within six weeks, the book would no longer have been true.
This book, by 1975, had become the Bible of paranormal science, al-
though it is written in a journalistic fashion by nonscientists. It went
through five printings as a hardback in 1970, and by mid-1974 had
gone through ten printings as a paperback. To say that it was a best-
seller does not do it justice. Watson cites its reports over and over in
his own best-selling book. The book fully deserves its wide circula~
tion, for it tells the story of impossibilities within an impossibility —
paranormal studies financed by the most rationalistic political em-
pire in the history of man,

Stalin had prohibited investigations into the occult (although cer-
tain forms of telepathy, oddly enough, were not completely banished
as scientific facts). Yet within a decade of his death, the Soviet
Union’s scientific community began to be hit with a series of mini-
revivals, Upper-echelon members of the scientific establishment
began to examine phenomena that had previously been banned as
not only nonexistent but un-Marxist. Here, in the most thoroughly
rationalistic and bureaucratized state in man’s history, was a scienti-
fic guild that was permitting the study of what scientists throughout
the world had long called occultism. Yet such studies were being
made in the name of Marxism-Leninism, that is, materialism. The
Soviets were slowly discovering Supernature. By the late 1960's,
scientists in the Iron Curtain nations had become the most heavily
subsidized paranormal scientists in the world. Though no scientist
would have wanted or dared to admit it, total rationalism was steadily
being fused with supernaturalism, that is, the old irrationalism.

What kinds of phenomena are under investigation? Mind over
matter (telekinesis), telepathy, and extrasensory perception (the
Western phrase given to the unknown and the theoretically impossi-
ble). Sight without eyes. Hypnosis-induced reincarnation. Precogni-
tion. Bioplasmic bodies (auras). Prophecy and astrology. From the
late 1950’s, when hardly a word of such research could filter into any

35. Sheila Ostrander and Lynn Schroeder, Psychic Discoveries Bekind the Iron Curtain
(New York: Bantam, 1970).
