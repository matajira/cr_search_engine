184 UNHOLY SPIRITS

There is power here. Power to give information and power to direct
and control personal development, That power is personal: being to
being.

The doors of perception swing both ways.

There are problems associated with voices. Take the case of one
Robert F. Roy, a self-professed “student and disciple of demonology,”
accused burglar, arsonist, and murderer. He admitted to the murder
of a mother of nine children, Police in Camden County, New Jersey,
said that he informed them that an inner spirit seemed to be telling
him “to kill an elderly female.” He led them to a hidden recess in a
wall of the Berlin Hotel, where he was living, and produced a
12-gauge shotgun which he said was the murder weapon. The
woman had indeed been cut down by a shotgun blast. There is no
guarantee that the voices, whatever they are, are reliable, factually
or ethically. Yet psychic after psychic relies upon some form of spirit
voice to guide him in his mystical revelations.

Jeane Dixon

“As I touched her hand I saw the death symbol over her. It was
high above the ground, I saw life on the ground around her, and
thus knew that if she would keep her feet on the ground she could
elude danger. It was a sort of inner voice that said, ‘Six weeks.’ This
voice comes to me frequently, and I always listen to it.”?” Jeane
Dixon, like Peter Hurkos, gets messages from a voice. These voices
get around.

Mrs. Dixon is undoubtedly the most prominent American psy-
chic of our time. Ruth Montgomery’s bestselling introduction to her
powers, A Gift of Prophecy (1965), was almost prophetically timed to
coincide with the initial detonation of the occult explosion. It appealed
directly to the literary tastes of those who take seriously the in-depth
articles of the Ladies Home Journal. Unlike Peter Hurkos, who feels
burdened by his talent, Jeane Dixon (as interpreted by Ruth Mont-
gomery) seems to revel in her gift. Being a prophet may not be easy,
the book informs us, but it certainly is spiritually rewarding, and it
certainly can make a person the envy of her neighbors. The under-
lying message of the book is that while everyone is noi equally gifted
—contrary to David Hoy—it certainly is nice for those who are,
especially those wha Do a Lot of Good with their gift.

36. Philadelphia Bulletin (May 30, 1974).
37. Ruth Montgomery, A Gift of Prophecy (New York: William Morrow, 1965), p. 19
