160 UNHOLY SPIRITS

ence in the world whether people believe in astrology or sorcery. It
made all the difference to Castaneda’s training and his safety during
that training. The question concerning the actual historicity of don
Juan. may not be crucial; what-is crucial is whether the universe con-
veyed in the writings of Castaneda is true or not.

If these are works of pure fiction, totally unrelated to any con-
crete historical individuals named don Juan and don Genaro, then
Castaneda is one of the great writers of fiction in our era. But if he is
a writer of fiction, he is also a master of folklore —the greatest that
UCLA has ever produced. His reconstruction of the sorcerer’s image
of this world is striking in its power and its correlation with what we
know of other primitive beliefs. As we might paraphrase that old
slogan, if don Juan did not exist, it has certainly been useful to in-
vent him. The teachings of don Juan ring true. The reader has en-
tered the mental universe of a primitive sorcerer. What we have
found is a remakably modern thinker.

Did Castaneda really experience these events? I think he did.
Were these events historical events, that is, events that could have
been photographed or recorded on tape (neither medium was per-
mitted Castaneda in his research)? Some, but not all. He suspected
hypnosis,!2! Furthermore, after having experienced a particular
nonordinary experience, the disappearance and reappearance of his
car, be was told by don Juan that don Genaro had not really moved
his car from “the world of ordinary men,” but that he had “simply
forced you to look at the world as sorcerers do, and your car was not
in that world. Genaro wanted to soften your certainty.”!% The
nagual’s time is not the ¢onal’s time.

This is not to say that such events were absolutely impossible. If
Satan could transport himself and Christ to the top of the temple,
then the Christian who takes the Bible seriously cannot safely say
that teleportation by occult means is categorically impossible. But
the ability of men to distinguish fact from fiction is limited. The
apostle Peter, upon his miraculous release from prison, was con-
vinced that what was happening to him was in fact some sort of vi-
sion. Only when he was outside the city’s gate was he sure that an
historical event had taken place (Acts 12:9-11). The apostle Paul’s de-
scription of one man’s heavenly vision (no doubt his own) is open to
this same confusion: he did not know whether he was in the body or

121. Castaneda, Ixtlan, p. 239.
122, Ibid., p. 155.
