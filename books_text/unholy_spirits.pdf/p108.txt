102 UNHOLY SPIRITS

or angelic creatures who control the nature spirits that are said by
clairvoyants to be everywhere at work nurturing plant life.” This
perspective, needless to say, is pure animism. “Divina contributed
detailed descriptions of the messages she said she received directly
from the devas, of which she described whole hierarchies responsible
for every fruit and vegetable, for every flower and weed.”®! (In Silent
Spring, Rachel Carson informed us that our insecticides are killing
off the rebins, but she neglected to tell us that we may also be inter-
fering with the Intercosmical Association of Devas and Wood Spirits
Union.)

‘There was a theology undergirding all this growth, There almost
always is: radiations, vibrations, and most important, man, the new
god. “Man appeared to have the role of a demigod; by cooperating
with nature he might find no limit to what could be achieved on this
planet.”&

What was not known to me at the time I wrote the earlier version
of this book was that Findhorn, the little Scottish town where the big
tomatoes grow, has become perhaps the most important single ex-
porter of New Age occultism. Constance Cumbey calls it the Vatican
City of the New Age movement.® It was founded in 1962. “Esoteric
lore, from Tibetan Buddhism through UFOlogy, was actively pur-
sued by the Findhorn initiates and would-be initiates.” And to think:
it all started with gardening. But, then again, as the residents of
Findhorn seem to understand, so did Satan’s temptation of Eve.

Scientific Experiments

The “scientific” interest in talking plants seems to have begun
with Cleve Backster. Back in 1966, Mr. Backster, an internationally
famous polygraph (lie detector) expert, hooked up one of his plants
to a polygraph device. He noticed that the device recorded signifi-
cant responses whenever he directed a mental threat toward the
pliant, such as burning one of its leaves. In a now-famous and fre-
quently cited experiment—frequently cited in paranormal science
books, that is—Backster placed brine shrimp on plates, which in

60. Tompkins & Bird, Secret Life, p. 381

61. Tbid., p. 382

62. Ibid., p. 379.

63. Constance Cumbey, The Hidden Dangers of the Rainbow: ‘The New Age Movement
and Our Coming Age of Barbarism (Shreveport, Louisiana: Huntington House, 1983),
p. 51
