ds This the End of the World? 389

“positive confession,” a dominion eschatology. He includes a subsection,
“An Emerging Eschatology,” in his final chapter. He does not like
what is emerging, but at least he recognizes it.

He calls the “positive confession” movement “a new gospel.” But
he calls it this in the subsection on eschatology. Why the section on
eschatology? Is he calling into question the orthodoxy of historic
postmillennialism? What he conveniently fails to mention is that his-
toric postmillennial eschatology is in no way connected with “posi-
tive confession” doctrines. It is not a new gospel. It is about 1800
years older than the doctrine of the premillennial, pretribulation
rapture which Mr. Hunt holds, which as we have seen was first an-
nounced by a 15-year-old girl, Margaret Macdonald, who did so
during an ecstatic trance at a private prayer meeting in Scotland in
1830. Some of the greatest leaders and theologians of the Bible-
believing church in the United States have been postmillennialists,
including most of the theologians of Princeton Theological Seminary
before the liberals captured it in 1929: Charles Hodge, A. A. Hodge,
Archibald Alexander, B. B. Warfield, Oswald T. Allis, and J.
Gresham Machen. Machen led the intellectual counter-offensive
against modernism in the 1920’s and 1930’s.% Dispensational schol-
ars also fail to remind their readers that Dr. Allis, who wrote what
they acknowledge as probably the definitive defense of the Mosaic
authorship of the Pentateuch, The Five Books of Moses, also wrote the
definitive refutation of dispensationalism, Prophecy and the Church.

Mr. Hunt misleads his readers: first, by failing to mention the
long tradition of postmillennial optimism in the history of Protestant
orthodoxy, and second, by equating the optimism of historic post-
millennialism with New Age optimism. How does he accomplish
this? By placing the following paragraph immediately after his para-
graph criticizing the humanistic gospel of self-esteem:

‘There are many groups representing seemingly widely divergent points
of view about whether the world can be saved, and if so, how. There is one
point, however, upon which even those who sccm to be opposed to each
other find agreement. This otherwise-surprising unity is expressed in the
growing opposition from many quarters to the traditional fundamentalist
view that the only hope for this world to be saved from destruction is
miraculous intervention by Jesus Christ. Increasing numbers of Christian

20. J. Gresham Machen, Christianity and Liberalism (New York: Macmillan, 1923),
reprinted by William B. Eerdmans, Grand Rapids, Michigan.
