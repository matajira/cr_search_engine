Escape from Creaturehood 337

the opus magnum [great work] was at once the freeing of the human
soul and the healing of the cosmos. In this sense alchemy is a contin-
uation of Christianity. In the eyes of the alchemists, observes Jung,
Christianity saved man but not nature. It is the alchemist’s dream to
heal the world in its totality; the Philosopher's Stone is conceived as
the Filius Macrocosmt who heals the world, whereas, according to the
alchemists, Christ is the Saviour of the Microcosm, that is, of man
only. The ultimate goal of the opus is Cosmic Salvation; for this rea-
son, the Lapis Philosophorum is identified with Christ.”5

Chinese alchemists searched for the “divine cinnabar,” or drink-
able gold, which would produce eternal life —the elixir of youth, in
other words.'6 The Philosopher’s Stone was seen as the elixir of im-
mortality.’ The alchemist was concerned with the transcending of
time, and he was therefore the inheritor of ancient paganism, with
its ritual attempts to return to the primordial chaos, the Time before
time. ®

Louis Pauwels and Jacques Bergier, the authors of the enor-
mously successful book, The Morning of the Magicians (1960), believe
in alchemy. Their book supposedly heralds a new dawning of an age
of alchemy. They, too, agree with Jung about the religious impulse
of alchemy. “For the alchemist, it must never be forgotten that power
over matter and energy is orfly a secondary reality. The real aim of
the alchemist’s activities, which are perhaps the remains of a very old
science belonging to a civilization long extinct, is the transformation
of the alchemist himself, his accession to a higher state of conscious-
ness, The material results are only a pledge of the final result, which
3s spiritual, Everything is oriented towards the transmutation of man
himself, towards his deification, his fusion with the divine energy,
the fixed center from which ail material energies emanate. The
alchemist’s is that science ‘with a conscience’ of which Rabelais
speaks. It is a science which tends to exalt man rather than matter;
as Teilhard de Chardin puts it: ‘The real aim of physics should be to
integrate Man as a totality in a coherent representation of the
world. 719 Interestingly enough, they quote Teilhard, whose mon-

15. Mircea Eliade, The Forge and the Crucible: The Origin and Structures of Alchemy
(New York: Harper Torchbooks, [1956] 1971), p. 225.

16, Ibid., pp. 109-1.

17. Ibid., pp. 124, 167.

18. fbid., pp. (74-75.

19, Louis Pauwels and Jacques Bergier, The Morning of the Magicians (New York:
Avon, [1960] 1973), p. 118.
