The Biblical Framework of Interpretation 63

finally attains perfect humanity, but not divinity. The Divine once
became a man, and only once. Because of Christ’s atoning work on
the cross—a judicial act of sacrifice which satisfied God’s own stand-
ards of justice—some men can be saved (John 17; Rom, 5). But no
man can become God. The attempt to become God, or a part of
God's being, is wholly illegitimate and constitutes the essence of re-
bellion. The Creator-creature distinction is everlasting.

The doctrine of the incarnation asserts that God took on flesh.
The doctrine of monism asserts that out of God flesh and matter ap-
peared, and eventually they must return to (or into) God. Monism
implies that God is somehow incomplete. This is the specific teach-
ing of Hegel, for example. We are needed to complete God’s being
and to restore Him to his pre-temporal wholeness. Man is therefore
a part of the process of God's salvation. God is saved by man. It is
man’s efforts to be reunited with God that are crucial in the process
of time, The emphasis of monism, while officially theistic, is none-
theless humanistic. A very fine example of just this kind of reasoning
is found in a fragment of the writings of Meister Eckhart (1260-1328),
the German mystic. Heretical to the core, Eckhart’s monism led him
into the wildest kinds of philosophical rambling. Yet he was consis-
tent in his presentation of the theology of monism, as indicated by
the following passage: “God’s divinity comes of my humility, and this
may be demonstrated as follows. It is God’s peculiar property to
give; but he cannot give unless something is prepared to receive his
gifts. If, then, I prepare by my humility to receive what he gives, by
my humility [ make God a giver. Since it is his nature to give, I am
merely giving God what is already his own. It is like a rich man who
wants to be a giver but must first find a taker, since without a taker
he cannot be a giver; for the taker, in taking, makes the rich man a
giver. Similarly, if God is to be a giver, he must first find a taker, but
no one may be a taker of God’s gifts except by his humility, There-
fore, if God is to exercise his divine property by his gifts, he well may
need my humility; for apart from humility he can give me nothing
~without it I am not prepared to receive his gift. That is why it is
true that by humility I give divinity to God.”8

Where this approach to “humility” is present, either in the ex-
plicit thinking or implicit presuppositions of would-be autonomous
man, the resistance to occultism is socially and culturally close to

13, Meister Eckhart, trans. Raymond Bernard Blakney (New York: Harper &
Row, 1941), pp. 233-34,
