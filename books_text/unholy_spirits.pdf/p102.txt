96 UNHOLY SPIRITS

you don’t think so, write to the president of Gillette. (He knows but
won't say.)

The Cheops Pyramid has fascinated both historians and occult-
ists for generations. Herodotus was duly impressed. So are all the
rest of us. How could such structures have been built in the middle
of a desert? Something like 2.5 million massive blocks of stone, some
weighing perhaps twenty tons, each averaging 2.5 tons, were used to
build the Cheops structure. ‘There are no traces of carbon on the
inner walls. How did the workers and priests light the interiors?
What instruments could have been used during the so-called bronze
age to form perfectly measured blocks? How did they level the mas-
sive site to a deviation of only half an inch? Questions like these go
on and on, to the total confusion of the theorists, who are themselves
hard-pressed to manufacture hypotheses as rapidly as the Egyptians
constructed the pyramids.

The pyramids had religious significance architecturally. Like the
great tower described in Genesis 11, they were built to be staircases
to heaven. The 267th pyramid text of the Fifth and Sixth Dynasties
reads: “A staircase to heaven is laid for him [the pharaoh], so that he
may mount up to heaven thereby.” The theological significance
should be obvious: man, by means of his own powers, shall attain
divinity, or near divinity. This was explicitly the teaching of Egypt-
ian religion concerning the pharaoh, himself the descendant of the
sun god,

Occultists and cultists have, for over a century, devoted great
efforts to interpreting the significance of Cheops’ dimensions. It
seems that in both inches and centimeters, it is possible to see impor-
tant historical events foretold in the “key” dimensions of the Cheops
Pyramid. A whole prophetic literature has grown up around these
three-dimensional studies. I choose not to go into detail here, but I
can report that prophecies based on these dimensions, if properly in-
terpreted, have achieved extraordinarily high correlation rates, but
only so long as the fulfillments of the possible predictions are investi-
gated retroactively. (Years ago | read a similar study, written by a pyra-
mid skeptic, of the Washington Monument.)

The most important figure in the revival of pyramid occultism,
apart from Dyrbal, is a French occultist, Antoine Bovis. Bovis, after

53, Charles F, Pfeiffer and Harold F. Vos, Wycliffe Historical Geography of Bible
Lands (Chicago: Moody Press, 1967), p. 68.
54. Toth & Nelson, Pyramid Power, pp. 97-99.
