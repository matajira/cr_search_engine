Demonic Healing 251

aged man who claimed to be suffering from a blood clot was seated
on the operating table. Terte’s hand began to massage the man’s
chest area. Within ten seconds, according to Ricks, a small, black
object began to appear on the chest—actually coming out through
the chest! Yet there was no opening visible to Ricks, who was stand-
ing on the other side of the operating table. The clot fell into Terte’s
hands. Ricks said it was the size of his thumb. The man claimed that
he felt nothing.

The next patient had an acute case of appendicitis. ‘lhe, choir
sang for a while, then stopped, Terie said his prayer, and commenced.
He washed his hands and the patient’s body with rubbing alcohol —
the only condescension to Western standards of cleanliness—and
began to massage the man’s abdomen. A one-inch hole appeared. A
few seconds later, the appendix rose up through the small opening.
Terte lifted the organ and placed it in Ricks’ hand. There was almost
no sign of blood. By pressing on the opening with his hand, Terte
closed the wound. A few seconds later, Ricks reported, there was no
sign that there had ever been an opening.

Ricks watched the third operation from a distance of a few inches.
‘Terte made an incision on the back of a patient and drew out long
strands of foul-smelling flesh. As always, the patient felt no pain.

‘This phenomena of internal organs or objects located inside the
body passing through the flesh, even apart from openings larger
than the pores, is relatively common with primitive forms of de-
monic healing. Ronald Rose, in his study of Australian aborigines,
records a conversation he had with one native concerning an alleged
healing. Rose is convinced that such operations involve trickery:

When the old Birri full-blood from Bowen (Queensland), Harry
Monsell, told me about seeing a doctor suck a stone about the size of a hen’s
egg from the body of a woman who was suffering from abdominal pains, I
put it to him that the doctor had deceived them,

“Oh, no,” he said firmly. “That same old doctor E saw once fix up a man
who had a chest pain. He said the man had got a porcupine [echidna or
spiny anteater] spike in him. The doctor sang a song, and I saw with my
own eyes the quill come out of the body of the man.”

“Do you mean that it seemed to come directly out of his flesh?”

“Yes.”

“Did the doctor perhaps pull it out?”

“No. He just sang and it came up. When it came right out, it fell down
onto the ground,”
