250 UNHOLY SPIRITS

The Philippine Healers

Arigo was unrivaled as a demonic healer. His powers were the
least disputed, most consistent, utterly baffling, and most widely
witnessed of any healer on record. Yet what Arigo did for two dec-
ades in Brazil differs only in degree from what other demonic healers
in primitive cultures have performed for centuries. When we exam-
ine the healing arts of another widely publicized group of peasant
spiritists, the Philippine wonder healers, we find the same basic pat-
terns, both theologically and methodologically. Their abilities have
been challenged more successfully, since some instances of fakery
have been detected, and far fewer people have been treated by any
one of these men. Nevertheless, in the aggregate, a large number of
films, photographs, and first-hand accounts have been assembled
that demonstrate that successful operations are being performed
which are impossible to explain in terms of known biological and
physical principles,

One of the earliest “psychic” healers in the Philippines whose
work was observed and reported by American visitors is Brother
Eleuterio Terte, Gert Chesi’s New Age book, Faith Healers in the Phil-
ippines, says that Terte performed the first Philippine “bloody opera-
tion” in 1945.59 Fate, a popular magazine of occult and “Fortean”
anomalies, ran an April 1960 article which dealt with Terte’s powers.
Two moving picture producers recorded several of his bare-hand
operations. His techniques were analogous to Arigo’s, although they
were far more explicitly religious in nature. He uses a female
medium in a trance state to add to his power; she, rather than Terte,
is visibly entranced. J. Bernard Ricks, a clairvoyant and self-
professed medium, journeyed to Terte’s tiny village in 1963 to see
him in action, and his report appeared in Fate in the sare year. The
editors of the magazine decided to include this essay in a compilation
published in 1965, The Strange and the Unknown, an anthology gleaned
from the pages of Fate. Again, notice the date of publication: 1965.

According to Ricks, villagers in the little town of 400 gather
together in order to form a choir. They sing hymns before each
operation, supposedly in order “to stir up the vibrations.” After the
hymns, Terte recited the Lord’s prayer and went to work. A middle-

39. Gert Chesi, Faith Healers in the Philippines (Worgl, Austria: Perlinger Verlag,
1981), p, 10.
