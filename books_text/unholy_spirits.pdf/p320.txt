314 UNHOLY SPIRITS

will be revealed as naive, pretentious, and false, Worse; it will leave
men at the mercy of forces that they cannot control by scientific
means, Scientists may worry about the Bomb, or the potential
plague threat of a newly created test tube bacteria, but the idea that
there are forces in this world that in principle cannot be explained by
repeatable cause-and-effect laws scares them even more. They are
men clinging to a dying paradigm —a paradigm which was never
fully accepted by residents in the West, and which was never ac-
cepted at all by residents in primitive cultures, They are fighting for
survival, not just as guild members who are about to be replaced,
but as members of a guild which as a whole cannot survive if the evi-
dence of an invasion from the noumenal is taken seriously.

If the saucers are in fact operated by Kittle green men from the
stars, scientists will find a way to cope with the problem, But con-
ventional scientists smell trouble. The evidence points to something
other than men from Mars. Men from Mars are in principle no
different from amoebas from Mars, and scientists have been almost
religious in their pursuit of evidence of life forms on the planets (as
evidence of Darwinian evolution). No, what bothers them is that the
stories associated with UFO phenomena indicate that we are dealing
with noumena: invasions from a world beyond mathematical cause
and effect. Such forces do not belong here. They point to even
greater forces, the traditional forces of heaven and hell, of fmal judg-
ment. These are far more serious than invaders from Mars.

Tronically, the very best statement from a conventional scientist
concerning the question of UFO’s was made by Condon. In a dis-
paraging speech on UFO's in April of 1969, Condon revealed his
contempt for the UFO topic, yet in doing so he identified the proper
answer to the question, “What are UFO's?” He said: “Perhaps we
need a National Magic Agency (pronounced ‘enema’) to make a
large and expensive study of aé/ these matters, including the future
scientific study of UFOs, if any.” We do not need such an agency,
but we do need this identification of the “phenomena”: magic.

Vallee’s Studies

One of the writers who has done the most to gain a hearing for
the question of UFO's is the French writer Jacques Vallee. He wrote
a best-selling book called Anatomy of a Phenomenon, published in 1965.

64. Cited by Keel, Tigjan Horse, p. 46. Keel is somewhat sympathetic to Gondon,
