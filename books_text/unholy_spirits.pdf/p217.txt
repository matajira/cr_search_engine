Edgar Cayce: From Diagnosis to Gnosis on

million, B.c.5*

Later on, Atlantis was formed (300,000 B.c.). The Master took
on a human form at this point— 700,000 years after the creation of
the five races. He was called Amilius, or Adam. Here is the heart of
Cayce’s soleriology—his doctrine of salvation. The Master, as
Amilius-Adam, fell ethically.

Question: When did Jesus become aware that he would be the Saviour
of the world?
Answer: When he fell in Eden.*

Eden, as later readings seem to imply, was Atlantis. Then the
Master began his series of incarnations. He subsequently became,
among others: Enoch, Melchizedek, Joseph (son of Jacob and
Rachel), Joshua, Zend (father of Zoroaster), Asapha or Alfa (an
Egyptian), and finally Jesus.®* He was incarnated for a total of 30
times, which the voice affirmed as a very low number to achieve per-
fection.* The Master's real name was Jeshua. He was also Hermes,
the builder of the Great Pyramid of Egypt. Amazingly enough, his
partner in this important task was the priest RaTa, who was an early
incarnation of . . . Edgar Cayce!*4 The identification of Jesus with
Hermes is extremely significant in occult circles. The Egyptian god
Thoth was identified as Hermes by the Greeks, also referred to as
Hermes Trismegistus (thrice great). The importance ascribed to
Hermes Trismcgistus by carly medicval magicians, astrologers, and
occultists carmot be overstressed. The development of Renaissance
magic around the legends of Hermes has been studied in an ex-
haustive fashion by Frances Yates. Hermes was understood as the
real founder of the magical arts. Miss Yates makes a shrewd obser-
vation concerning the “cult of Egypt” in the second century, when the
legend of Hermes originated. It fits the Cayce cult quite well:

The men of the second century were thoroughly imbued with the idea
(which the Renaissance imbibed from them) that what is old is pure and holy,
that the earliest thinkers walked more closely with the gods than the busy
ralionalists, their successors. Hence the strong revival of Pythagoreanism

1. Ibid., pp. 32-35, 40-41.
2. fbid., p. 23.

3. Idem

4, Ibid. p. 71.

3. Tbid., pp. 78-79.
