398 UNHOLY SPIRITS

pline. But all these would not satisfy us if we did not get in addition
the delicious sense of secret intimacy,.”* Being a member of some in-
ner ring allows us to use the word “we” in contrast to “them,” the out-
siders. As he says, “your genuine Inner Ring exists for exclusion.
There'd be no fun if there were no outsiders.”>

The only inner circles worth joining are the church or profes-
sional associations. Membership is based on shared beliefs, open en-
try, and public standards. Pass a test, and you get in. Make a public
profession of faith, and you get in. This is not the “inner ring” that
Lewis talks about. These rings are open to the public; they increase
the reign of morality and public service. He was speaking rather of
the inner ring which draws you away from the rules that govern the
outside world. Year by year, your loyalty shifts to members of the
ring rather than to general principles of righteousness. “Of all pas-
sions the passion for the Inner Ring is most skillful in making a man
who is not yet a very bad man do very bad things.”6 This is a major
theme in his novel, That Hideous Strength. To save your integrity, you
must get out.

The quest of the Inner Ring will break your hearts unless you break it.
But if you break it, a surprising result will follow. If in your working hours
you make the work your end, you will presently find yourself all unawares
inside the only circle in your profession that really matters. You will be one
of the sound crafismen, and other sound craftsmen will know it. This group
of craftsmen will by no means coincide with the Inner Ring or the Impor-
tant People or the People in the Know. It will not shape that professional
policy or work up that professional influence which fights for the profession
as a whole against the public, nor will it lead to those periodic scandals and
crises which the Inner Ring produces. But it will do those things which that
profession exists to do and will in the long run be responsible for alt the
respect which that profession in fact enjoys and which the speeches and
advertisements cannot maintain. And if in your spare time you consort
simply with the people you like, you will again find that you have come
unawares to a real inside, that you are indeed snug and safe at the centre of
something which, seen from without, would look exactly like an Inner
Ring. But the difference is that its secrecy is accidental, and its ex-
clusiveness a by-product, and no one was led thither by the lure of the
esoteric, for it is only four or five people who like one another meeting to do

4. G.S. Lewis, “The Inner Ring,” in Lewis, The Weight of Glory and Other Addresses
(New York: Macmillan, 1980), p. 100.

5. Ibid., p. 104.

6, Ibid., p. 103.
