234 UNHOLY SPIRITS

The physician who had diagnosed the cancer was immediately
called to the scene. He found no hemorrhaging. The “patient” claimed
that she felt completely relieved. He took the tumor for examination,
and the woman subsequently recovered.'* There was no possible
medical explanation for the tumor’s removal, her survival, and the
disappearance of all traces of cancer. Arigo had performed a miracle,
according to the family.

The lines lengthened in front of his door. He would treat people
for hours, yet after he was finished, he claimed that he remembered
nothing. He wrote long, complicated medical prescriptions in a mat-
ter of half a minute, while staring blankly into space. He operated
with a pair of scissors, a knife, and perhaps some tweezers. The pre-
scriptions were sometimes for long-forgotten drugs, yet on other oc-
casions they were so new that they were not yet imported into Brazil.
The best pharmaceutical firms were used.!5 This later was an impor-
tant point raised by his defense lawyer in opposition to the charge
brought against Arigo of witchcraft. As his biographer writes: “In
order to prove witchcraft, it was necessary to prove that a defendant
had personally distributed concoctions of roots and herbs. This was
clearly a thing Arigo never did.” Yet these weird prescriptions,
often of abnormally high dosages, did not work when any other phy-
sician imitated them, They worked for Arigo and his immediate pa-
tients, but not for anyone else. His ministry was non-repeatable.

Carlos Paranhos da Costa Cruz was a dentist in Belo Horizonte,
and had his office in the same building as the British consul, H. V.
Walter. He reported the story of one of Arigo’s operations to Walter,
who was to become a frequent observer at Arigo’s “clinic.” Sonja,
Cruz’s sister-in-law, had cancer of the liver. This, at least, was the
diagnosis of several physicians, including her own father. She was
considered inoperable. In desperation, she, Cruz, and her father
journeyed to Congonhas do Campo. She got in line, and when she
came to Arigo, he asked no questions. He told them she had a tumor
on her liver. He insisted on performing an operation, The descrip-
tion of what followed is nothing short of fantastic:

Within minutes, Sonja was placed on newspapers on the floor of Arigo’s
small room. Arigo brought some cotton and several instruments, including

14, Ibid. pp. 20-21.
15. Ibid., p. 75.
16. Sid, p. 125.
