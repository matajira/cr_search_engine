Abraham, 69
aborigines, 161, 251-52, 267
abortion, 12, 374, 385
above-below, 82, 335, 338
accidents (trains), 117
action, 140, 224, 348
acupuncture, 100, 240
Adam, 39
adepts, 342
advertising, 171
Agpoa, Tony
operations (5 minutes), 256
sanitation, 256
skeptics, 262
trance, 256
agriculture, 280
Ahab, 384
airplanes (mystery), 298-99
alchemy, 123, 336-39, 372
Allis, O. T., 389
“ally,” 134, 141-42, 154
Ambix, 336
American Revolution, 394
Anabaptists, 12
Andrews, Donald Hatch, 187
androgyne, 360-61
anesthetics, 351
angels, 68-70
animals = mankind, 140, 148, 283
animism, 101-8, 127, 130, 135-37
antinomianism, 219, 286
apostasy, 39
appendix (removal), 251
a priori, 36, 45
Aquinas, 30, 32n

INDEX

Arigo, 228-49, 396-97
arrest, 239
bondage of, 230
division of labor, 246
dreams, 230
drugs, 234
ballucinations, 229
headaches, 230
hospital, 246
knife, 229
possession, 236, 238
sanitation, 248
scissors, 235, 236
security police, 235
spiritism (anti-), 230
spiritism (pro-), 233
trance, 231
tumor-removal, 233-34, 234-35
visions, 230
voice, 229-30, 245
Aristotle, 30, 32n, 41, 59
Arlington House, 1-2
Armageddon, 373, 386, 394
army, 281
Arnold, Kenneth, 295, 298
astral body, 100, 112
astral projection, 98, 143, 149
161, 345-7
astrology, 65, 77, 82, 85, 188
astronomy, 77-82
Athens, 83, 169
Atlantis, 204-5
atomic age, 296
atomism, 135, 154
atonement, 63

407
