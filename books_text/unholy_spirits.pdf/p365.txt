Escape from Creaturehood 359

In 1979, the Unity-in-Diversity Council took over the ICC. We
can still profit from a review of the older organization’s activities,
since it was an important clearing house in an important region in
the period of the initial “coming of age” of New Age philosophy. In
the mid-1970's, the ICC sponsored annual consciousness celebra-
tions and other activities. It published a quarterly magazine, The
Cooperator, It involved itself in educational activities, supplying any
and all schools with materials. Its description of itself sounds quite
progressive:

The International Cooperation Council (ICC) is a non-profit coor-
dinating body of autonomous individuals and groups, each seeking in its
own unique way to contribute constructively to the global transformation of
our time, Based on the principle of “unity in diversity among all peoples,”
ICC is an experiment whose goal is to foster the emergence of a new
universal man and civilization serving the well-being of all mankind.
Utilizing the methods and knowledge of modern science in concert with
valid insights of religion, philosophy, and the arts, the creative activities of
ICG cover a spectrum from the search into man’s inner nature ta dynamic
social action, More than one hundred and twenty-five groups are now
cooperating with ICC.

What groups they are! Here is a partial listing:

Army of Parapsychology and Medicine

Ananda Meditation Retreat

Aquarian Arcane College

Association for Humanistic Psychology

Astara, Inc.

Astro Consciousness Institute for Self-Enlightenment and Peace
Avatar Meher Baba Group

The Boston Visionary Cell

Buddhist Publication Society

California Institute for Asian Studies

Center for the Study of Democratic Institutions
Center for the Study of Power

Church of Essential Science

Committee on Cosmic Humanism

Esperanto League for North America

Foundation for Universal Understanding

Inner-Space Interpreters Service

Institute of Human Engineering

International Association of Educators for World Peace
