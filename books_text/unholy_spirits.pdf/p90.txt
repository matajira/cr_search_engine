84 UNHOLY SPIRITS

rectly overhead Evanston. “This was the first piece of scientific evi-
dence to show that even an organism living away from the ocean
tides could be influenced by the passage of the moon.”

Watson goes on to record other well-known animal responses
that are influenced by lunar rhythms: the running (spawning) of the
California grunion fish, the movement of a particular variety of
worms, and May flies. Even rainfall has been found to be abnor-
mally heavy just after full and new moons, and Sctence, a prestigious
establishment periodical, has published the data on this unexplained
statistical phenomenon.?’ In addition to lunar rhythms, there are
solar rhythms, We are learning that sun spots may influence some
aspects of the earth’s weather, They also may influence earthquakes.
Changes in human blood serum have been attributed to sun-spot ac-
tivity. Even the speed of chemical reactions, says Giorgio Piccardi,
the director of the Institute for Physical Chemistry in Florence, Italy,
is influenced by sun spots. Watson summarizes his experiments with
bismuth oxychloride (a colloid), which forms a cloudy precipitate
when poured into distilled water. Three times a day he and his assist-
ants carried out this simple test, until they had 200,000 separate
results. Sure enough, over a ten-year period they found that the
reaction took place more rapidly during sun-spot activity.” (Here is
a typical test procedure of orthodox science.) These tests have been
replicated by other scientists throughout the world.”

What is mind-boggling to the average reader, assuming he pays
much attention to what he is reading, is not that sun spots have some
kind of influence over bismuth oxychloride, but that any highly
placed scientist would conduct a decade-long experiment of this
nature. What if— perish the thought— the results had been negative?
‘Ten years, a ton of bismuth oxychloride, and a zillion gallons of dis-
tilled water—right down the proverbial drain. And the positive
nature of the experiment has fascinated only parapsychologists and
scientists of the odd, themselves highly suspect in the eyes of ortho-
dox scientists. Finally, there is the question of financing. “Who
cares?” is too easy. “Who paid?” is more to the point. Even more as-

26. Idem.

27. Bid., p. 27.

28. Ibid., pp. 52-53.

29, The effect was inhibited when the solution was shielded by a copper screen,
for whatever that is worth: Bysenck and Nias, Astrology, p. 137.

30. Idem.
