200 UNHOLY SPIRITS

something called clarawater to the man, only there was no such
product listed. Cayce was put under again and the voice came up
with the formula. Yet at no time could Cayce remember any of
this.!* His voice would give remarkably detailed anatomical descrip-
tions, and they were sometimes confirmed by physicians, yet Cayce
read nothing other than the Bible and simple Sunday school litera-
ture. He had not received training in anatomy by sleeping on his
grammar school textbooks, however advanced they would seem in
today’s educational context. In 1902, Cayce also developed a game
for adults which he called “Pit.” People liked it, so he sent a sample to
Parker Bros, He received their thanks, several free sets, and a letter
stating that it had been copyrighted by them and it would henceforth
be illegal for him to produce them. So much for Edgar Cayce, busi-
nessrman, 5

Word finally got out to the press about Cayce’s diagnoses. This
was in 1903.'* The medical authorities immediately moved in on Al
Layne, who agreed to quit practicing until he graduated from a
school of osteopathy. Cayce’s voice again departed. Now, however,
he had learned that he could put himself into a trance, and with a
“control” agent, he could still have himself temporarily cured. A local
physician agreed to act as an assistant, but was surprised when
Cayce’s chest turned red, then returned to normal, and upon awak-
ening, his voice was normal.!”

Cayce worked with some young physicians in Bowling Green,
then returned to Hopkinsville, where he teamed up with a doctor of
homeopathy, Wesley Ketchum, who quietly used Cayce to diagnose
a hundred cases over the years. It was Dietrich’s testimonial on
behalf of Cayce that helped to reduce his initial skepticism. But the
occult nature of Cayce’s power became increasingly blatant. On one
occasion, he had prescribed something called “balsam of sulphur.”
Years later, Ketchum described the scene: “One day, there were a
couple of doctors and druggists in Cayce’s [photographic] studio on a
complicated case, for which Cayce had prescribed balsam of sul-
phur. They began scratching their heads, Nobody had ever heard of
it. One of the druggists, an elderly man named Gaither, was con-
vinced there was no such thing. They pored over a copy of the dis-

14, Sugrue, River, pp. 137, 145-46.

15. Ibid., p. 126.

16. H. L. Gayce, Venture Inward, pp. 33-35.
17, Sugrue, River, p. 146,
