138 UNHOLY SPIRITS

ordinary world are no longer a buffer for him; and that he must adopt a
new way of life if he is going to survive. The first thing he ought to do, at
that point, is to want to become a warrior, a very important step and deci-
sion, The frightening nature of knowledge leaves one no alternative but to
become a warrior.

By the time knowledge becomes a frightening affair the man also real-
izes that death is the irreplaceable partner that sits next to him on the mat.
Every bit of knowledge that becomes power has death as its central force.
Death lends the ultimate touch, and whatever is touched by death indeed
becomes power.

A man who follows the paths of sorcery is confronted with imminent an-
nihilation every turn of the way, and unavoidably he becomes keenly aware
of his death. Without the awareness of death he would be only an ordinary
man involved in ordinary acts. He would lack the necessary potency, the
necessary concentration that transforms one’s ordinary time on earth into
magical power.®

It is obvious that no man can live in constant fear of death, and
don Juan did not advise Castaneda to enter a life of worry. But some
other emotion had to be substituted for fear. The next step in the
path toward magical power is total detachment. “The idea of imminent
death, instead of becoming an obsession, becomes an indifference.
. .. Detach yourself from everything.”® Understandably, a man
detached in this manner is not bound by earthly conventions.

It is at this point that don Juan displays another of the important
intellectual and philosophical premises he shares with modern man.
Don Juan is an existentialist.» I think it is likely that a student could
pass off the following quote as if it were part of a formerly unpublished
letter from Martin Heidegger, the German existentialist philosopher:

Only the idea of death makes a man sufficiently detached so he is in-
capable of abandoning himself to anything. Only the idea of death makes a
man sufficiently detached so he can’t deny himself anything. A man of that
sort, however, does not crave, for he has acquired a silent lust for life and
for all things of life. He knows his death is stalking him and won't give him
time to cling to anything, so he tries, without craving, all of everything.

A detached man, who knows he has no possibility of fencing off his
death, has only one thing to back himself with: the power of his decisions,

29, Castaneda, A Separate Reality: Further Conversations with Don Juan (New York:
Pocket Books, [1971] 1974), pp. 149-50.

30. Ibid., p. 150.

31. Ibid.
