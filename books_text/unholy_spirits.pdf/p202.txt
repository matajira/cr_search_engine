196 UNHOLY SPIRITS

some sort of legitimate, though unheard-of, abilities.) Normal: that
was one term that had never fit young Edgar.

As a child, he claimed to be able to see “little people” — compan-
ions in the fields. Unlike most children, Cayce never doubted that he
had. At the age of thirteen —the age most frequently associated with
poltergeist activity — he was reading his Bible through for the twelfth
time, Not a good student, he was at least faithful in this activity, He
experienced a vision of a lady with wings. She asked him what he
wanted, and he said he wanted to be of service to people. (Part of the
appeal of the Cayce story to a jot of people is that he was a perpetual
do-gooder. He always seemed so sincere.) She granted him his wish,
he later said.* This incident cannot be dismissed lightly; the next day
Cayce demonstrated a most remarkable ability. He was a poor stu-
dent. His teacher, who happened to be his uncle, had told his father
that the boy was hopeless, and his father, like so many fathers before
him, was determined to beat a little knowledge into his son. He
began to drill him on his spelling lesson. Blank, as always. He grew
furious, striking the boy. Edgar later claimed that at this point, he
heard the lady’s voice: “if you can sleep a little, we can help you.” He
asked his father’s permission to doze for five minutes, Granted. He
put his spelling book behind his head, dozed off for five minutes, and
when his father returned, he knew every word in the book, including
the page number and the line on which the word appeared.

He brought home his geography book, slept on it, and the next
day had it completely memorized. He did this with every school
book he had. His teacher could hardly believe it. His father was dis-
turbed; this was too spooky. Yet he was proud of his son. (Some of
Cayce’s classmates, when interviewed several decades later, still re-
called how they had resented his effortless learning technique.5) Just
before his son’s graduation from school (from the seventh grade, ac-
cording to Hugh Lynn Cayce; from the sixth, according to legend®),
his father had bragged to former Congressman Jim MacKenzie that
his son could memorize anything by sleeping on it. MacKenzie was
understandably skeptical. A test was proposed by MacKenzie. Years
before, he had delivered a speech favoring the repeal of the tax on

4, Ibid., pp. 23, 45-46.

5, Harmon H. Bro, Edgar Caywe on Religion and Psychic Experience (New York:
Paperback Library, 1970), p. 24.

6. Hugh Lynn Cayce, “Introduction,” H. L. Cayce (ed.), The Edgar Cayce Reader
{New York: Warmer Paperback Library, [1969] 1974), p. 8
