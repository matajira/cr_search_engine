182 UNHOLY SPIRITS

Hurkos became famous, like Croiset, for his investigations of
criminal cases. He worked on many of them, with varying degrees of
success. One police officer who believes in Hurkos was police chief
Robert White of the Palm Springs police department, Hurkos had
helped his department locate the body of a missing youth, Stephen
Gallagher. He had told them where to search for the body (Decem-
ber, 1968); the following September, they located it very close to the
location described by Hurkos. Even more ecrie, however, was the
way in which Hurkos presented the information. Chief White recon-
structs that original meeting:

“T had heard a lot about Peter Hurkos,” said Chief White. “I knew of his
reputation on the Boston Strangler case, and I was interested in meeting
him. While he was in my office he asked whether we had any complicated
cases we were trying to solve. I told him yes, as a matter of fact we had sev-
eral, I asked Captain Richard Harries to bring in some of the folders of un-
solved cases. He brought in several,

“Peter Hurkos laid his hand on the top folder, which happened to be the
Gallagher case, and began verbally telling us almost verbatim what was in
the report. Both my captain and I were amazed. We had never seen Peter
Hurkos before. He had no way of knowing what was in that folder. The
case was a year old, and it hadn’t been in the news recently. Yet Peter began
telling us about it exactly as though he were seeing through that folder.

“He said it was the case of a missing boy. He named the correct number
of persons involved — threc. He identified the vehicle they were driving. He
told us there had been a little ‘party’ and there were lots of narcotics in-
volved. This also was correct. The three boys had been off on a little ‘trip’
on L§D.”*

Hurkos is able to perform before large groups. He is a profes-
sional psychic. His gifts are not limited to dark halls and seances
filled with credulous people. He can do his work on television as well
as on a Las Vegas stage. In August of 1969, he appeared on The
David Frost Show, Columnist Leonard Lyons handed him a sealed
box and asked him to identify its contents. Not even Frost knew
what was inside. His biographer writes:

Peter began rubbing his hands over the box. His reading, as many will
remember, went something tike this:

“I see wires, yes. . . wires and hooks . . . steel hooks shaped like this

30. Fbid., pp. 110-11.
