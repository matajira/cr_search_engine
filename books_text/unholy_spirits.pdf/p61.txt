The Biblical Framework of Interpretation 55

around it are harmed.

July 1, 1952, St. Petersburg, Florida: Mrs. Mary Reeser, a sixty-
seven-year-old widow, was visiting in her room with her neighbor,
Mrs. P. M. Carpenter. When she left her that evening, Mrs. Reeser
was seated in her armchair by the window, dressed in a rayon night-
gown, slippers, and a housecoat, and was smoking a cigarette. The
next morning, a Western Union messenger failed to raise her by
knocking at her door to deliver a telegram. Concerned about her
normally light-sleeping neighbor, Mrs. Carpenter started to open
the door. The brass doorknob was hot. She cried out, and two house
painters ran to see what was wrong. Together they broke into the
house. Although both windows were open, the room was hot. In
front of an open window were some ashes; a chair, an end table, and
Mrs. Reeser. All that remained of her were a few pieces of charred
backbone, a shrunken skull the size of an orange, and a wholly un-
touched left foot, still in its slipper. Her room was generally un-
affected, except for some melted wax candles and melted plastic fix-
tures. From four feet above the floor was the soot. The clock had
stopped at 4:20 a.M., but when plugged into an unmelted wall out-
let, it started running again. There were no embers and no smell of
smoke. Mrs. Reeser had weighed 175 pounds the night before; now
only ten pounds remained.

The FBI was called in. The case received lots of publicity locally.
Result: no explanation. Professor Krogman happened to be visiting
friends nearby and volunteered to study the case. His conclusion:
spontaneous human combustion. But he had never seen a head
shrunken by fire. The skull should have exploded, not shrunk, Said
Kregman: “Never have I seen a skull so shrunken or a body so com-
pletely consumed by heat. This is contrary to normal experience and
T regard it as the most amazing thing I’ve ever seen. As I review it,
the short hairs on my neck bristle with vague fear. Were I living in
the Middle Ages, I'd mutter something like ‘black magic!’ ”

Not living in the Middle Ages, he can only mutter “spontaneous
human combustion.” But what comfort is that? The phenomenon ex-
ists. It exists in the twentieth century. People want explanations for
phenomena; if they are denied explanations, they prefer to ignore
the phenomena. This is why the major news services refuse to pick
up stories like the death of Mary Reeser~or did in 1952. In earlier
