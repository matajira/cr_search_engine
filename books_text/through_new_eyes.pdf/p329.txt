Bibliography H7

Edersheim, Alfred. The Temple: lis Minisiry and Services, As They Were at
the Time of Christ. Grand Rapids: Eerdmans, 1972. Evangelical.

Fairbairn, Patrick. The Typology of Scripture. New York: Funk and
Wagnalls, 1876, Classic evangelical study of typology; see Davidson.

Farrer, Austin. A Rebirth of Images: The Making of St. John’s Apocalypse.
Gloucester, MA: Peter Smith, (1949) 1970. Fascinating study of
Biblical symbolism. Quasi-evangelical,

. The Revelation of St. Jokn the Divine. Oxtord: Claren-
don Press, 1964, A follow-up study to Rebirth of Images.

Frame, John M. The Doctrine of the Knowledge of God. Phillipsburg, NJ:
Presbyterian and Reformed Publishing Co,, 1987. The most im-
portant work in evangelical epistemology in several decades.

Haran, Menahem. Temples and Temple-Service in Ancient Israel. Oxford:
Clarendon Press, 1978. Liberal perspective.

Hough, R.. E. The Ministry of the Glory Cloud. New York: Philosophical
Library, 1955. A valuable redemptive-historical study. Evangelical.

Institute for Basic Youth Conflicts. Character Sketches: From the Pages of
Scripture, Hlustrated in the World of Nature. Three volumes. Oak
Brook, IL: Institute for Basic Youth Conflicts, 1976, 1978, 1982.
Evangelical.

Jaki, Stanley, The Road of Science and the Ways to God. University of
Chicago Press, 1978. A Christian critique of Kuhn’s subjectivism.

Jordan, James B. “The Bible and the Nations (A Syllabus).” Biblical
Horizons, P.O. Box 132011, Tyler, TX 75713.

—. “Christianity and the Calendar (A Syllabus).” Bibli-
cal Horizons, P.O. Box 132011, Tyler, TX 75713.

. Covenant Sequence in Leviticus and Deuteronomy. Tyler,
TX: Institute for Christian Economics, 1988.

. “The Death Penalty in the Mosaic Law: Five Ex-
ploratory Essays.” Biblical Horizons, P.O, Box 132011, Tyler, TX
75713.

—___.. “Dragons’ and Dinosaurs in Biblical Perspective.”
Biblical Horizons, P.O. Box 132011, Tyler, TX 75713.

—_-__.. “From Glory to Glory: Degrees of Value in the Sanc-
tuary.” Biblical Horizons, P.O. Box 132011, Tyler, TX 75713.

—_______. “Hexameron: Theological Reflections on Genesis
One (A Syllabus).” Biblical Horizons, P.O. Box 132011, Tyler,
TX 75713.
