266 TuroucH New Eves

symbolic dimensions. An interesting example of this, alluded to
above, is found throughout the Gospel of John. In John’s Gos-
pel, Jesus is presented as the fulfillment of the Tabernacle.
Before looking at this, let us consider the relevance of it. If
Jesus Christ is God’s true Tabernacle, then we who are in Christ
are in that true “body politic” house of God. Moreover, since the
cosmic heavens and earth are also imaged in the Tabernacle, if
Christ is the true Tabernacle, then all the cosmic heavens and
earth must also come to be in Christ, so that “in Him all things
hold together” (Colossians 1:17). By presenting Christ as the true
Tabernacle, John is not simply giving us snapshots of redemp-
tion in the narrow sense; he is also presenting us with a world-
view, a new universe. As Paul puts it in Colossians 1, the Second
Person of the Trinity was the center of the first universe, and the
God-man Christ Jesus is the center of the new universe (Colos-

sians 1:15-20):

t. Creation:
a. And He is the image of the invisible God,
b. The frsthorn [i.e., captain] of all creation.
c.. For in Him all things were created,
(i) in the heavens and on earth,
{2) visible and invisible,
(3) whether thrones or dominions or rulers or author-
ities,
d. all things have been created through Him and for Him.

2. Restoration:
a. He is also the head of the body, the Church:

b. And He is the beginning, the firstborn [i-e., captain]
from the dead, so that He Himself might come to have
first place in everything.

c. For alt the fullness was pleased to dwell in Him and
through Him to reconcile all things to Himself, having
made peace through the blood of His Cross; .

d. through Him, whether things on earth or things in the
heavens.
