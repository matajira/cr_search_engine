308
4.
4.

6.

10.

12,
13.

16.

Notes To Paces 223-228

Judges 13; 1 Samuel 1. On the decline and on the simultaneity of Samuel's and
Samson’s births, see Jordan, Judges, esp, pp. 229-230, 238-238,

Samson was not the fool he is often mistaken for. An analysis of his shrewd re-
vival methods is found in Jordan, Judges, chaps. 12-14.

Lucius Nereparampil, Destroy This Temple: An Exegetica- Theological Study on the
‘Meaning of Jesus’ Temple-Logion in Jn 2:19 (Bangladore, India: Dharmaram [Col-
lege] Publications, 1978), pp. 67-73.

. [have treated the theology of the Saul narratives (1 Samuel 9-15) at length in

two essays: James B. Jordan, “Saul: A New Adam for Israel,” and “Saul: A
Study in Original Sin? The Geneva Papers 2:9 ( July, 1987), 11 ( July, 1988).

. It would be interesting to make a full study of the typological parallels, and

differences due to the maturation of history, between the Mosaic wilderness
wanderings and David's wilderness experience. Notice for instance that David
begins by getting showbread from the Tabernacle (1 Samuel 21:1-6), which we
should associate with manna in the wilderness. Saul becomes a new Pharaoh
pursuing David. Like Moses, David must fight Amalekites (1 Samuel 30).

. “And therefore I have sworn to the house of Eli that the iniquity of Elis house

shall not be atoned for by sacrifice or offering forever.” I believe this refers speci-
fically to the High Priest’s sacrifice on the Day of Atonement (Leviticus
16:1i-14), not to other sacrifices. In other words, the house of Eli was demoted to
the status of ordinary Auronic priests.

On the textual problems with this verse, where Ahimelech and Abiathar seem
to have been transposed, see commentaries on 1 Chronicles.

. This seems to be implied by 1 Chronicles 16:37-40, where only Zadok is men-

tioned as ministering at Gibeon. First Chronicles 24:5 says that both Ithamar-
ites and Eleazarites served as both officers of the sanctuary (Tabernacle) and
officers of God (the Ark?), Thus, if there was a distinction of duties in David's
time, it may not have been very rigid.

See Jordan, “Saul; A Study in Original Sin.”

On the tribal republics, sce E. C. Wines, The Hebrew Republic (originally pub-
lished as Commentary on the Laws of the Ancient Hebrews, vol. 2; reprint, Rt. 1, Box
65-2, Wrightstown, NJ 08562: American Presbyterian Press, n.d.).

. See James B. Jordan, Sabbath Breaking and the Death Penalty: A Theological In-

vestigation (Tyler, TX: Geneva Ministrics, 1986), pp. 47-49.

. Gehenna or the Valley of Hinnom was “a deep narrow glen to the south of Jeru-

salem, where, after the introduction of the worship of the fire-gods by Ahaz, the
idolatrous Jews offered their children to Moloch (2 Chronicles 28:3;
miah 7:31; 19:2-6), In consequence of these abominations, the valley was
polluted by Josiah (2 Kings 23:10); subsequently to which it became the com-
mon lay-stall of the city, where the dead bodies of criminals, and the carcasses
of animals, and every other kind of filth was cast, and, according to late and
somewhat questionable authorities, the combustible portion consumed with
fire. From the depth and narrowness of the gorge, and, perhaps, its ever-burn-
ing fires, as well as from its being the receptacle of all sorts of putrefying matter,
and all that defiled the holy city, it became in later times the image of the place
of everlasting punishment. . . . In this sense the word is used by our Lord,
Matthew 5:29-30; 10:28; 23:15, 33... .” “Gehenna,” in John McClintock and
James Strong, Cyclopedia of Biblical, Theological, and Ecclesiastical Literature (Grand
Rapids: Baker Book House, [1867-87] 1981) 3:764.

See Chapter 12, note 14 (on p, 302 above), on the gutter at the base of the altar
as perhaps the symbolic equivalent of Gehenna.

 
