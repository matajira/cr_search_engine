Eden: The World of Transformation 163

to Heaven, and also of the Church as a body politic. The same
thing is true of mountains, which symbolize nations and people
(Isaiah 41:14-16; Jeremiah 51:25, 42; Zechariah 4:7; Matthew
21:21-22).16 God’s people are His Mountain, a Mountain that
grows and fills the earth (Daniel 2:35).

This imagery is absolutely fundamental to Biblical revela-
tion, We have to consider each passage in context to see what it
is saying, of course; but we need to be alert to symbolism and
imagery. The Bible uses these images to express its worldview,
according to each’ stage of history. To an examination of these
stages of history we must now turn our attention, beginning
with the world of Noah.
