The Purpose of the World

Everything in creation bears some analogy to God: All the
world has been made with God’s stamp on it, revealing Him.
Creation is His temple, heaven His throne, earth His footstoal.
Thus Scripture finds analogies to God in every area of creation:
inanimate objects (God the “rock of Israel,” Christ the “door of
the sheep,” the Spirit as “wind,” “breath,” “firc”), plant life
(God's strength like the “cedars of Lebanon,” Christ the “bread
of life”), animals (Christ the “Lion of Judah,” the “lamb of
God”), human beings (God as king, landowner, lover; Christ as.
prophet, priest, king, servant, son, friend), abstract ideas (God
as spirit, love, light; Christ as way, word, truth, life, wisdom,
righteousness, sanctification, redemption). Even wicked people
reveal their likeness to God, with, of course, much irony — see
Luke 18:1-8.7

Similarly, Bavinck wrote that God

is compared to a lion, Isaiah 31:4; an eagle, Deuteronomy
32:11; a lamb, Isaiah 53:7; a hen, Matthew 23:37; the sun,
Psalm 84:11; the morning star, Revelation 22:16; a light, Psalm
27:1; a torch, Revelation 21:23; a fire, Hebrews 12:29; a foun-
tain, Psalm 36:9; the fountain of living waters, Jeremiah 2:13;
food, bread, water, drink, ointment, Isaiah 55:1; John 4:10;
6:35, 55; a rock, Deuteronomy 32:4; a hiding place, Psalm
19:14; a tower, Proverbs 18:10; a refuge, Psalm 9:9; a shadow,
Psalm 91:1; 121:5; a shield, Psalm 84:11; a way, John 14:6; a
temple, Revelation 21:22, etc.®

23

All this can be boiled down to a simple fact: The universe
and everything in it symbolizes God. That is, the universe and
everything in it points to God. This means that the Christian
view of the world is and can only be fundamentally symbolic. The
world does not exist for its own sake, but.as ‘a revelation of God.

Man Reveals God

Genesis 1:26 tells us that man was made as the preeminent
and particular image of God: “Let Us make man in Our image,
after Our likeness.” Man, then, is the special symbol of God. As

Bavinck put it,

Something of God is manifest in each creature, but of all crea-
tures man is endowed with the highest degree of excellence.
