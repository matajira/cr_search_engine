186

i0.

11.

‘TuroucH New Eves

Abram: (Genesis 12:18), Abimelech blamed Abraham
(Genesis 20:9), Abimelech blamed Isaac (Genesis 26:10),
Laban blamed Jacob (Genesis 31:26), Pharaoh blamed
Moses (Exodus 10:28), and Saul blamed David (1 Samuel
20:31). Herod and Pilate tried to shift the blame to each
other. Pilate washed his hands, and then put a sign on the
Cross blaming God and the believers. The Jews blamed
the Christians (Acts 5:28),

. God humiliates the false gods of the enemy. By implication the

false gods of Egypt and Philistia were humiliated when
Abram and Isaac were delivered. Rachel sat on Laban’s
gods (Genesis 31:34), and Jacob buried them (Genesis
35:4).8 God judged all the gods of Egypt (Exodus 12:12).
The Ark humbled Dagon of the Philistines (1 Samuel 5:3),
and then destroyed him (v. 4). Nebuchadnezzar and
Darius were converted and renounced their false gods
(Daniel 4; 6:7, 26-27). Christ defeated Satan, and the
ascended Christ destroyed the Temple, which had become
an idolatrous abomination.

. God's people depart with spoils. Noah brought the true wisdom

of the old world with him (Genesis 8:20-22). Abram left
Egypt with spoils (Genesis 12:16). Lot, of course, barely
got out of Sodom alive, but Abraham received large gifts
from Abimelech (Genesis 20:16), Jacob had nothing when
he went ta Mesopotamia, but came back extremely rich
(Genesis 32-33), Israel spoiled the Egyptians (Exodus
12:35-36). The Philistines sent the Ark back laden with
gold (1 Samuel 6:17-18), David inherited Saul’s Kingdom.
The Jews came back from Babylon with much spoil
(Zechariah 6:10-11). Between 30 and 70 a.p., the Church
spoiled the Old Covenant. She now spoils the world,
bringing all into the kingdom,

Finally, God's people ave installed in the Holy Land. This is, of
course, the goal of the exodus. Sometimes the people are
brought out of bondage, but reject the Kingdom, as in the
cases of Lot and of Israel in the wilderness. Eventually,
though, they come into the new world.

Installation in the new land means setting up worship,
building God’s house out of some of the spoils, and setting
up a priesthood. (This “Victory-Housebuilding Pattern”? is
actually what we are calling the establishment phase that
