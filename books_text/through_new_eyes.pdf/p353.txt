SUBJEC

T INDEX

 

astral bodies/imagery, 1, 3, 12, 20, 45,
46, 53H., 189, 270

astrology, 67

astronomy, 53

Assyria, 83, 178

Aaron's rod, 217

Abel, 81

Abiathar, 225

abomination, 244

Abram, Abraham, 20, 58f., 61, 82, 88,
90, 139, 154, 178, 182.

abyss, 145f., 228

acacia, 91f.

Adam, 15, 122, passim
Israel's king as, 228

adoption, 305

Ahimelech, 225

Agur, 299

Alexandrian exegesis, 17

allegory, 50f.

almond, 86

altar, 98, 147f., 158f., 161, 190, 234, 252,
285
Elijah’s, 236
Faekiel’s, 158f., 235f., 301f.

analogy, 24, 32

angels, 2, 31, 46, 55, 1058, 173, 302,
304

animals, 1, 3, 12f., 95ff., 134, 137, 269

anticipation, 198

Antiochus Epiphanes, 245

appearance, 12, 46

Ararat, 174

Ark of Covenant, 14, 90, 183, 217, 223f.,
226, 253, 269

Ark of Noah, 150, 170ff.

Armenia, 150

army, 204f., 222, 235

art, 134

Babylon, 62f., 65, 178
Bach, J. S., 13, 21
baptism, 72, 136, 267, 311
bastards, 221
bdellium, 73£., 295
beasts, 13, 96

Beast of Revelation, 15f.
Beersheba, 223f.
Beethoven, 21
Being, 22, 29, 291
bells, 216f.
benedicamus, 1286.
Bethel, 191
Bible, heavenly blueprint, 47
birds, 100, 102

see dove, eagle
blame, 185f.
blood, 174
blue, 45, 46
blueprints, 41.
body, human, 216f.
body politic, 24f., 2134.
boundary, 46, 136, 213, 300
bread, 82, 84
bride, assaulted, 184
burning bush, 74, B4f.

Cain, 81, 125, 128, 169

Carmel, 235f.

catechisms, 30

cedar, 82, 91f., 227

chariot, Divine, 231, 244, 252
see cloud-chariot

cherubim, 46, 49, 61, 75, 99, 101, 105,
Ill, 12, 113, 148, 206, 207, 231, 244,
252, 264

chiasmn, 55

Christmas tree, 93

chronology of Bible, 291

church, 264

341
