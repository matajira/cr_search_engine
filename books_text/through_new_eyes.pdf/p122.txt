io ‘THroucH New Eves

Now, imagine a child growing up in this situation. Let us
assume that this child was born while Israel was encamped at
Sinai. For nearly forty years, these are the only conditions he
has ever known. If this man.were a scientist, he might come up
with some scientific “natural laws.” For instance, he might be-
lieve that it is a law of nature that while some fabrics wear out,
fabrics worn by human beings do not. While some leather ob-
jects wear out, shoes worn by human beings do not. Perhaps this
is because human beings have a “restraining aura” around them-
selves that prevents wear. He might also formulate a “natural
law” that says that the gravitational and tidal forces of the sun
and moon prevent the fall of manna every seven days, while pro-
viding a doubling of manna the day before. Concentrations of
“cosmic rays” cause the quality of the manna provided on the
day before the cessation to be different, more “concentrated,” so
that it lasts twice as long without rotting.

It is easy for us to see that these explanations would not be
valid’ Our present world order, however, is the same. The “laws”
that govern natural processes are actually just God’s. current
ways of doing things.

This brings us to the involvement of angels in running the
natural world. It is in the area of weather that the Bible shows
angels running the world. The passages that show this are in
Psalm 104 and the book of Hebrews. Speaking of God, the
Psalmist says:

He lays the beams of His. upper chambers in the waters; He
makes the clouds His chariot; He walks upon the wings of the
wind; He makes the winds His messengers, flaming fire His
ministers (Psalm 104:3-4),

The author of Hebrews explains that these are references to
angels, “And of the angels He says, ‘Who makes His angels
winds, and His ministers a flame of fire’” (Hebrews 1:7).

This means that at least sometimes angels are involved in
running the weather, and carries with it an implication that
angels run other things in the world also. God, of course, is con-
currently running the world, but angels are also involved, at least
sometimes. Thus, for instance, if you pull the watch off your
arm and drop it into your lap, what causes it to fall? And to fall
