310 Nores To Pacgs 245-255

5. Sadly, many have completely overlooked the first fulfillment of this prophecy in
the ‘inter-testamental era. Commentators who are aware of the immediate
Restoration fulfillments of Ezekiel 38-39, and who sce Ezekicl’s Temple as
relating to the Restoration first of all, include Matthew Henry, Matthew Poole,
Adam Clarke, and E. W, Hengstenberg. See Matthew Henry’s Commentary (nu-
merous editions); Matthew Poole, A Commentary on the Holy Bible (London: Ban-
ner of Truth Trust, [1685] 1962); Adam Clarke’s Commentary (numerous edi-
tions); E. W. Hengstenberg, The Prophecies of Ezekiel Elucidated, trans. A. C.
Murphy and J. G. Murphy (Minneapolis, MN: James Publications, reprint:
1976). See also Ralph Woodrow, His Truth is Marching On: Advanced Studies on
Prophecy in the Light of History (Riverside, GA: Ralph Woodrow Evangelistic
Association, 1977), pp. 32-46.

6. See the works in note 5 above, and also Katheryn Pfisterer Darr, “Phe Wall
Around Paradise: Ezekielian Ideas about the Future,” Vatus Zéstamentum 37
(1987):271-279,

7. Note how our interpretation eliminates all the problems that plague so many
approaches to these chapters. The sacrifices were indeed set back up in
Zechariah’s time. These chapters in Ezekiel de not point to restored sacrifices in
a millennium, nor do these chapters have to be “totally spiritualized” to refer
only to the New Testament situation. Like Leviticus, they refer to actual sacri-
fices, and like Leviticus, they have typological relevance to the New Testament
church. :

8. On the problem of nations and empires in Biblical perspective, see Gary North,
Healer of the Nations: Biblical Principles for International Relations (Fort Worth, TX:
Dominion Press, 1987), and James B. Jordan, “The Bible and the Nations (A
Syllabus)” (available from Biblical Horizons, P.O. Box 132011, Tyler, TX
75713).

9. On the detailed predictions in Daniel 11 and 12 I have found Adam Clarke's
Commentary to be of greatest help, though I da not agree with every detail of his
analysis.

10. The establishment of “chief priests” was one of the changes David made in the
original Mosaic system.

11. Ihave made a very detailed study of these visions in a series of thirteen lectures
available on cassette from Biblical Horizons, P.O. Box 1320i1, Tyler, TX
75713, An expanded version. of this survey of Zechariah 1-6, focusing on the
bronze mountains, is James B. Jordan, “Thoughts on Jachin and Boaz”
(available from Biblical Horizons, P.O. Box 132011, Tyler, TX 75713).

12. For this imporlant insight, I am indebted to David L. Petersen, Haggai and
Zechariah 1-8: A Commentary (Philadelphia: Westminster Press, 1984), pp.
194-202. “Zechariah’s concerns here involve meta-ritual. How can the whole
temple ritual system be sct in motion again if everyone is unclean? The high
priest cannot bear anyone’s guilt since he, or at loast his clothing, is soiled.
‘What happens in this vision of Zechariah mukes the restoration of the ritual
system possible, and this by means of an ad hoc cleansing ritual in the divine
council, The normal purification rituals are impossible and therefore not in-
voked,” p. 201.

13. The idea of the “ten lost tribes” is wholly mythical. For conclusive proof we
need only call attention to Luke 2:36. The northern tribes were carried off by
Assyria, and joined up with the southem exiles later on.

 
