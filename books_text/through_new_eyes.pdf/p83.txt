Rocks, Gold, and Gems 7

The gods of other nations are like-little rocks without much
to them. Israel would be foolish’to forsake the True Rock, be-
cause in time God would destroy these pebbles.

And He will say, “Where are their gods,
The rock in which they sought refuge?”
(Deuteronomy 32:37)

When Samuel was born, his mother Hannah celebrated
God as her Rock: “There is no one holy like the Lord, Indeed,
there is no one besides Thee, nor is there any rock like our God”
(1 Samuel 2:2):

David also celebrated God as his Rock:

The Lord is my Rock and my Fortress and my Deliverer; my
God, my Rock, in whom I take refuge. . . . For who is God
besides the Lord? And who is a Rock, besides our God? . . .
The Lord lives, and blessed be my Rock; and exalted be God,
the Rock of my salvation (2 Samuel 22:2-3, 32, 47; cf. 2 Sam-
uel 23:1-5).

The Psalter abounds in references to God as our Rock:

To Thee, O Lord, I call;
My Rock, do not be deaf to me (Psalm 28:1),

Incline Thine ear to me, rescue me quickly;

Be Thou to me a Rock of strength,

A stronghold to save mc.

For Thou art my Rock and my Fortress (Psalm 31:2-3).

I will say to God my Rock, “Why hast Thou forgotten me?
Why do-I go mourning?” (Psalm 42:9)

O come, let us sing for joy to the Lord;
Let us shout joyfully to the Rock of our salvation
{Psalm 95:1; cf. also 61:2; 62:7; 71:3; 89:26; 92:15; 94:22),

Isaiah also delights to call God the Rock of Israel:

For you have forgotten the God of your salvation
And have not remembered the Rock of your refuge
(Isaiah 17:10).
