136 Turouen New Eves

so the water used on Moriah flowed from springs on Zion,° Yet
we are told that someday “the mountain of the Lord’s house will
be established as the chief of the mountains and will be raised
above the hills” (Isaiah.2:2), What does this mean?

First, the true Mountain of God is in heaven, not on earth,
and thus not approacliable by man. It is always high, as the
heavens are high above the carth.

Second, man’s earthly sanctuary, while it starts high, is to
grow and develop in glory during history. During the infancy of
humanity, the sanctuary is protected with the swaddling clothes
of the mountains roundabout (compare Galatians 4:1-7). Once
maturity has been attained, then the holy mountain stands forth
as the greatest of the mountains.

A third aspect of this prophecy is seen in that Jesus left Zion
and Moriah behind, and transferred His Kingdom to the Mount
of Olives, which was the highest mountain in the area.!!
Spiritually, though, the mountain of His Kingdom is a ladder to
heaven, whose top breaks through the firmament to the Throne
of God. (Revelation 21:10; 22:1).

Mountain symbolism is found in all world religions. The
Hindus had Mount Meru; the Japanese, Fujiyama; and the
Greeks, Olympus.?? It is also found throughout Scripture. Thus,
Abraham offered Isaac on Mount Moriah (Genesis 22:2}; Moses
received the law on Mount Sinai (Exodus 19-24); Elijah defeated
Baal on Mount Carmel (1 Kings 18) and received his commis-
sion renewed on Mount Sinai (1 Kings 19); Jesus preached His
definitive sermon on a mount (Matthew 5), was transfiguredon
a mount (2 Peter 1:16-18), and gave his final, great commission
on a mountain (Matthew 28:18-20).

Beyond this, we find that Christians “are the light of the
world; a city set on a hill cannot be hid,” a reference to Jerusalem
on Mount Zion, and also a symbol of the righteous person (Mat-
thew 5:14). Believers are God’s people-mountain, and someday
“the mountain of the house of the Lord will be established as the
chief of the mountains, and will be raised above the hills; and all
the nations will stream to it” (Isaiah 2:2). God’s holy mountain
grows until it fills the whole world (Daniel 2:34-35). This can be
so because the mountain symbolizes not only the individual
human person but also the Church:
