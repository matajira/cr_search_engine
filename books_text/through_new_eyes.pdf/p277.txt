The New World 265

is a far more powerful social force than mere capital punish-
ment, according to the spiritual insight of the New Covenant.®
Of course, when the Church influences society, then Godly pun-
ishments are sct up in society as well. All the same, the most im-
portant fulfillment of the Old Testament penalties lies in the
sanctions exercised by the elders of the Church.

The Restoration of Type and Symbol

The Jews of this period had almost completely perverted the
law.. As we have seen, they were not keeping the moral require-
ments of the law. Neither did they. understand the symbolic
aspects. When Jesus told the Jews that if they destroyed the
‘Temple He would raise it up in three days, referring to His
body, they were utterly confused (John 2:19-21). Similarly, when
Jesus talked with Nicodemus and referred to the water-cleansing
rites of the Old Covenant as a means of resurrection and new
birth, Nicodemus was confused. Jesus expressed amazement:
“Are you the teacher of Israel, and do not understand these
things?” (John 3:10).

In the first century, the Jews had rejected Biblical symbolism
and typology, and were divided into two groups. The Pharisees
had turned symbolism into moralism, and were keeping the law
as a means of salvation, The Alexandrian Jews had replaced
Biblical typology with allegories grounded in Greek philosophy.
Asa result, neither group was able to recognize Christ when He
came to them.

It was the task of Jesus and the writers of the New Testament
to restore true Biblical symbolism and typology, and to show
how the Old Testament revealed Christ. It is for that reason,
among others, that John writes his Gospel as a “tour through the
Tabernacle”; that Paul explains that the Tabernacle and Temple
were symbols both of the individual believer and of the corporate
church (1 Corinthians 3:16; 6:19); that the author of Hebrews
expresses shock that his hearers do not understand the Old Tes-
tamerit symbolism and typology (Hebrews 5:12); and that Jesus
had to explain the typology of the Old Testament to the two men
on the road to Emmaus (Luke 24:27).

Thus, when Jesus came to be the true Prophet, He first of all
had to restore the Old Covenant, both in its moral and in its
