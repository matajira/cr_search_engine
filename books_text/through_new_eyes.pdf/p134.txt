122 ‘TuroucH New Eves

Romans 1:21, speaking of all men and thus pointedly of
Adam and Eve, says “for even though they knew God, they did
not glorify Him as God, or give thanks.” Man was created on the
sixth day of creation week. He was made in the middle of the
day, after the animals. Before that first day was over, God
brought various animals to Adam for Adam to name. The next
day was the sabbath, the time when Adam was to come before
God and give thanks, glorifying God as God.

It is important to reflect on what it meant for Adam to name
the animals. These were not new names resulting from a work of
restructuring. Rather, Adam was coming to grips with things
the way they already were. To put a name on something is a way
of laying hold on it. We cannot deal with things we cannot
name. Thus, it was not labor in the strict sense for Adam to
name the animals. Adam was simply taking hold of the
creation.

Before beginning to work with the creation, Adam was to
give thanks to God, affirming His sovereignty. Adam was not to
give thanks to God empty-handed. Rather, it was with God’s
creation in his hands that Adam was to render thanks to God.
This involved the dedication of his future works to God. Thus,
Adam’s future works would involve moving the creation from
glory to glory, by restructuring and redistributing it. Adam’s six-
fold rite for life, thus, was as follows:

1, Adam was to lay hold on the cosmos.

2. Before working with it, Adam was to give thanks to God
for His gift of the cosmos,*

3. Adam would then break down and restructure that portion
of the cosmos within his grasp. He would give new names
to the results (e.g., Genesis 4:17).

4, Adam would then distribute his works to others. A tithe (10
percent) would be given to God, on the sabbath day of
judgment, for God’s evaluation. The rest Adam would
either keep for himself or distribute to others through giv-
ing or trading. Just as directions for use accompany things
we buy today, so Adam would have given directions to
those who received his gifts or bought his wares.
