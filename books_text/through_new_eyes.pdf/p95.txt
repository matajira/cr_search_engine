Trees and Thorns 83

In Jonah 4, Assyria is pictured as a suddenly sprouting plant
that would provide shade for Israel. Jonah had been reluctant to
preach to Nineveh, fearing that God would convert those people
and thereby raise them up as a powerful nation. He knew that
Israel deserved judgment, and that God had threatened to take
the Gospel to another nation, thereby raising it up as a weapon
to punish Israel (Deuteronomy 32:21). Sure enough, the people
of Nineveh repented at the preaching of Jonah, and Jonah was
horrified. In spite of her sins, Jonah loved wayward Israel and
hated to see the Gospel taken from her to the Gentiles (compare
Paul, Romans 9-11). Jonah went outside the city to wait and see
what God would do.

So the Lord God appointed a plant and it grew up over Jonah
to be a shade over his head to deliver him from his discomfort.
And Jonah was extremely happy about the plant, But God ap-
pointed a worm when dawn came the next day, and it attacked
the plant and it withered. And it came about when the sun
came up that God appointed a scorching east wind, and the
sun beat down on Jonah’s head, so that he became faint and
begged with all his soul to die, saying “Death is better to me
than life” (Jonah 4:6-8).

The plant represented Assyria. Its sudden sprouting represented
the conversion of Assyria. Such a converted nation would be
sure to bless Israel, in terms of the Abrahamic covenant (Genesis
12:3), and thus would provide shade for Jonah (Israel). In time,
however, the'serpent would attack the roots of Assyria, and that
nation would apostatize, and would indeed become a threat to
Israel, as Jonah had feared. Israel would experience the scorch-
ing heat of God’s wrath.?

God did promise to restore Israel, however. The promise
was made through Hosea, and again employed arboreal, or tree-
like, imagery:

I will be like the dew to Israel; he will blossom like the lily and
he will strike his roots like the cedars of Lebanon, His shoots
will sprout, and his beauty will be like the olive tree, and his
fragrance like the cedars of Lebanon. Those who live in his
shadow will.again raise grain, and they will blossom like the
vine. His renown will be like the wine of Lebanon. O Ephraim,
