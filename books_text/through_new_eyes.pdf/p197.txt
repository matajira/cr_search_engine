a

The World of the Patriarchs

while actually defending Isracl. Jesus protected and saved
His holy bride by drawing Satan’s fire to Himself.

. Very often, God's people are enslaved during the sojourn outside the

land. Jacob was virtually enslaved by Laban, and Laban
regarded him as his slave.’ Israel was enslaved in Egypt.
Israel was virtually enslaved at the beginning of the
Babylonian captivity. Jesus was dragged before Pilate and

cast into prison.

. God brings blessings upon His people during the captivity, but

plagues the tyrant, either progressively or as part of the deliverance.
Abram acquired wealth in Egypt (Genesis 12:16), but
Pharaoh’s house received plagues (Genesis 12:17). Despite
persecution, Isaac became wealthy in Philistia (Genesis
26:12-17). God made Jacob wealthy in Mesopotamia, but
gradually decapitalized Laban (Genesis 31:5-12). Israel
multiplied in Egypt (Exodus 1:12), but Egypt was plagued.
The Ark brought plagues on the Philistines during its cap-
tivity (I Samucl 5:6-6:1), David gathered an army and
wealth while in exile, but Saul was plagued by demons
(1 Samuel 16:14; 22:2; 1 Samuel 25), The Jews prospered
in Babylon, and important Jews were found at court, while
Nebuchadnezzar was driven insane (Daniel 4). The work
of Christ on the Cross redeemed. the world, ensured our
blessing, and destroyed Satan, while apostate Israel was
plagued by demons during Christ’s ministry and through-
out the book of Acts.

. God miraculously intervenes, often with vistons to the pagan lord, in

order to save His people, Noah’s Ark and Flood were miracles,
as we have seen. God's glory appeared to Abram in Ur (Acts
7:2). God appeared to Abimelech to deliver Sarah (Genesis
20:6-7). Angels came to save Lot, and worked miracles (Gen-
esis 19:11). God appeared.to Laban and ordered him to
leave Jacob alone (Genesis 31:24). The miracle of Passover
saved Israel from Egypt (Exodus 12). The plagues ori Philistia
were miraculous (1 Samuel 5-6). God appeared to Neb-
uchadnezzar and converted him, causing him to favor God’s
people more than before (Daniel 4), God sent Pilate’s wife
a vision (Matthew 27:19), God raised Jesus from the dead.

. Very often the serpent tries to shift blame and accuses the righteous

man of being the cause of his difficulty. Thus, Pharaoh blamed

185
