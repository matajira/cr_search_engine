144 Turoucn New Eyes

heaven, as earth, and as both together, they were images of
God’s house. Since the human person is a temple and a taber-
nacle, they were also images of the individual human being
(i Corinthians 6:19; 2 Corinthians 5:1). The human community
is also a temple and a tabernacle, so that they were images of the
body politic as well as of the human individual (1 Corinthians
3:10-17), Because of this, they portrayed the True Man, Jesus
Christ, as well as His Church (John 2:19).

We shall expand on all this as we proceed, but it is necessary
for us to take note of it here as we begin. Some matters that are
relatively obscure in Genesis 2 are made clearer by comparison
with later world models (always bearing in mind that the later
models are more glorified than the earlier ones). The four rivers
that flowed out of Eden are simply a curiosity, for instance, until
we associate them with the four corners of the earth, and the
four corners of the altar, and the four corners of the cross. Thus,
even in our initial study of the first world, we shall draw on later
world models to help us understand the images presented com-
pactly in Genesis 1 and 2.

The Three-Decker Universe

Bearing in mind that the Bible generally uses the language of
appearance in describing the world, we can see the proper sense
in which the Bible presents a triple-decker universe. The second
commandment prohibits bowing before any image made in the
likeness “of what is in heaven above or on the carth beneath or in
the water under the earth” (Exodus 20:4b). This three-part
cosmos is {undamental.to Biblical imagery and symbolism.

In Genesis 1:9, we read that the waters were gathered “into
one place.” This seems to be a reference to the oceans of the
world, which in fact are continuous with one another, so that all
the continents are in reality large islands in this one vast ocean.
Except for a few isolated lakes, all the bodies of water on the
earth are one large sea, and so the “one” gathering can also be
called “seas” (plural).

The sea level establishes the limit of the land. Thus, the sea
is always “below” the land, and since the sea goes down and
down, it clearly stretches into’an abyss. Moreover, the land is
clearly in a visual sense “founded” on the seas, “established” on
