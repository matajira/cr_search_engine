178 TuroucH New Eyes

enforcing the post-Flood polity. Having failed at Babylon,
Nimrod went out and created Nineveh and Assyria (Genesis
10:10-11). Thus, both of these two mighty hunter-conqueror cul-
tures were created by one man, who was carefully carrying on
the policy of Gain. Both Babylon and Nineveh are to be seen as
extensions of Enoch, the city of Cain.

God’s judgment on the Tower of Babel, however, was accom-
panied as always with a new announcement of salvation. All the
things that man had sinfully tried to seize at Babel—land, name,
priestly influence—God announced that He would bestow upon
Abraham.

They had wanted land, “lest we be scattered abroad over the
face of the whole earth” (Genesis 11:4). God, however, scattered
them (11:8), and gave land to Abram: “Go forth from your coun-
try, and from your relatives and from your father’s house, to the
land which I shall show you” (Genesis 12:1).

They had wanted a name: “And let us make for ourselves a
name” (Genesis 11:4b). God, however, confused their languages,
so that they could not understand one another’s names (11:7),
and gave a great name to Abram: “And I will make you a great
nation, and I will bless you, and make your name great” (Gen-
esis 12:2),

Finally, they had wanted to be religious leaders. Their tower
was to reach to heaven. They would be the points of contact be-
tween other men and “god” (Genesis 11:4). God, however,
prevented their tower-building (11:8) and set up Abram and his
seed as the priestly nation: “And so you shall be a blessing; and I
will bless those who bless you, and the one who curses you I will
curse. And in you all the families of the ground shall be blessed”
(Genesis 12:2-3).15

This brings us to the world of the Patriarchs, which we must
now consider.
