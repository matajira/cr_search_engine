FIFTEEN

THE WORLD OF
THE TABERNACLE

 

Psalm 102 tells us that the heavens and the earth eventually
wear out, and have to be changed:

Of old Thou didst found the earth; and the heavens are the
work of Thy hands, They themselves will perish, but Thou
dost endure; and all of them will wear out like a garment; like
clothing Thou wilt change them, and they will be changed
(Psalm 102:25-26).

Whether the psalmist has in mind the physical heavens and
earth, or the social polity of the world, is not immediately clear.
Perhaps his language is designed to encompass both,! The prin-
ciple he articulates, however, clearly applies to both; and it ap-
plies to the Abrahamic heavens and earth. They waxed old as
the people grew into a nation. Eventually there were too many
people to be ruled by simple clan structures. The people began
to break out of the seams of the old heaven-and-earth garment,

God acted to change the garment. For one thing, once the
people were reduced to slavery, the distinction between the
blood line of Jacob and the multitudes of servants in the nation
broke down.’ All were servants now. When Israel came out of
Egypt, we do not find an aristocracy of true-blooded Israelites
dominating a plebeian class made up of the descendants of the
servants, as probably would have been the case had Gad not put
the nation through the crucible of enslavement. The result of
this change was that government by patriarchs shifted into gov-
ernment by elders (Exodus 3:16; 4:29). Men of discernment
rather than men of blood came to hold power in Israel.

197
