230 Trrroucn New Eyes

Diagram 16.2
Zion as a World Model

Temple and Palace
(Garden)

  
 
      
    

Jerusalem
(Eden)

Mount Zion
(World)

 

Gehenna
(Abyss)

North — South

This is the meaning of the King, and of the King’s palace being
associated with the Temple on Mount Moriah (between the
Temple and the Edenic city on Mount Zion). There is a parallel
between the king’s palace-garden and bride, and God’s Temple-
garden and bride (Israel). The Song of Solomon shows us Adam
and Eve restored to the garden. The book abounds in garden or
paradise imagery. We see the bride tempted to unfaithfulness.
We see much talk of trees and fruit, and the husband feeding the
bride. Also, there is a great deal of architectural imagery in Can-
ticles, comparing the human body to God’s Temple,’”? Thus,
Christian expositors have usually seen Canticles as a parable of
Christ and the Church, though it is also a celebration of the res-
toration of marital love.1®

The Temple, of course, is the major symbolic polity change.
As the Tabernacle symbolized the political cosmos of Israel in
the Mosaic era, so the Temple symbolized the political cosmos
