256

Polity:

Symbol:

Societal:

Church:

State:

TrroucH New Eves

records, the loss of Levitical cities,
the full establishment of the. non-
Levitical synagogue.

The Kingdom constitution was
nullified, since there was no longer
an independent kingdom and army.
The Mosaic establishment also no
longer applied in many reéspects,
since the Jews were under overar-
ching impcrial laws. Note that God
left them to apply the wisdom of
these systems to constantly chang-
ing circumstances. They were
maturing, becoming more “adult,”
and thus were left to make their own
applications. Also, witness-bearing
becomes an important new duty.

Priests and Levites at Temple, lay-
leaders at synagogues.

World emperor who protected God's
people, or who chastised them. Im-
perial overseers, such as Nehemiah,
and local Jewish prince, such as
Zerubbabel, over the land.

The Temple in Jerusalem, but only
as a rude representation of the vi-
sionary Temples of Ezekiel and
Zechariah.
