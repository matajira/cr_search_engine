SEVENTEEN

THE WORLDS OF EXILE
AND RESTORATION

 

‘The Davidic heavens and earth had hardly gotten under way
before it fell into sin. The Kingdom was split, and the Temple
was raided. Thus, both social and symbolic polities were
changed. In time, the fabric of the Davidic covenant began to
wear thin, It was no good trying to put a patch on it; a new gar-
ment was needed.!

The new garment consisted of a World Imperial order, with
Israel under the protection of (or at the mercy of) a world
emperor. Within Israel the synagogues, which had previously
had Levites as local pastors, were now run by laymen. The
Restoration Temple, the symbolic polity, was nowhere near as
glorious as Solomon’s, but what it symbolized was a far more
glorious and powerful Spiritual presence.

All of this had been anticipated in the centuries before the
new covenant came into being. First, in terms of symbolic polity,
the loss of Temple-glory matched the loss of the Davidic house
when the Kingdom split. This anticipated the relatively less glo-
rious Restoration Temple.

Second, in terms of local “holy convocations,” while Levites
continued as local pastors in Judah, in Northern Isracl there
were very few Levites, Most of them moved to Judah as a result
of persecution (2 Chronicles 11:13-14). Thus, God raised up
prophets; and these prophets set up theological seminaries, the
“schools of the prophets,” to-train local pastors.? The synagogues
of the faithful (the “remnant”) continued to meet on sabbaths
and new moons (2 Kings 4:23), but their pastors were laymen
trained and ordained by the prophets. Thus, while the Levites

241
