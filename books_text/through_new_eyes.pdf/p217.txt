Benjamin
EPHRAIM,

Manasseh

Levites
(Gershon)

The World of the Tabernacle

Diagram 15.1

The Israelite Camp

Asher

DAN Naphtali

Leviles
«Merari)

 

 

Tent of Meeting

 

 

Gad

Levites
(Kohath)

REUBEN Simeon

sould

205

Issachar
JUDAH

Zebulun,

Meredith G. Kline has written that God’s “house-building,
as depicted in Exodus, is of two kinds. There is first the structur-
ing of the people themselves into a formally organized house of
Israel.”*2 This took place in Exodus 18-24, with Jethro’s reorgan-
ization of the nation, the giving of the Ten Commandments, and
the giving of the social laws of the Book of the Covenant. Then,

having narrated the building of this living house of God’s
habitation, the Book of Exodus continues with an account of
the building of the other, more literal house of Yahweh, the tab-
ernacle,... Though a more literal house than the living
house of Israel, the tabernacle-house was designed to function
as symbolical of the other; the kingdom-people-house was the
true residence of God (a concept more fully explored and spiri-
tualized in the New Testament). The Book of Exodus closes by
bringing together these two covenant-built houses in a sum-
mary statement concerning Yahweh’s abiding in the glory-
cloud in his tabernacle-house “in the sight of all the house of
Israel” (40:34-38).13

This brings us to a consideration of the Tabernacle as the

symbol for the Mosaic establishment. There are five aspects of
the Tabernacle we wish to consider. First, the Tabernacle was a
