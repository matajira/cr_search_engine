286 TuroucH New Eyes

When the man went out toward the east with a line in his hand,
he measured a thousand cubits, and he led me through the
water, water reaching the ankles. Again he measured a thou-
sand and led me through the water, water reaching the knees.
Again he measured a thousand and led me through the water,
water reaching the loins. Again he measured a thousand; and it
was a river that I could not ford, for the water had risen,
enough water to swim in, a river that could not be forded (Eze-
kiel 47:3-5),

Again, this is not a picture of instant Kingdom, but of gradual
growth and devclopment.

Jesus said the same in His parables. Let us consider the
Kingdom parables of Matthew 13. In the Parable of the Wheat
and the Tares, Jesus makes it clear that the wicked will always
coexist with the righteous in this world (Matthew 13:24-30), but
this parable does not say that they remain at equal strength. No,
rather Jesus immediately tells the Parable of the Mustard Seed,
which says that the Kingdom starts small but gradually grows to
become the largest of all garden trees, so that the birds, the na-
tions of the world, rest supported by its branches (Matthew
13:31-32). Jesus follows this up by comparing the Kingdom of
God to leaven, which gradually leavens a limp of dough (v. 33).3

Thus, the Bible pictures the continuing growth of the King-
dom after its establishment by Jesus Christ. Of course, theolog-
ians have debated how far this will go—whether or not there will
be a one-thousand year golden age, and the like.‘ It is not my
purpose here to get into this question, but simply to make the
point that the Kingdom is growing from glory to glory.

The Growth of the Kingdom in Church History

Since we live in an age of setback, it is not always apparent
to us that the Kingdom has, in fact, grown. But, if we take a
look at the Kingdom in the year 300, we find it suffering in pre-
Constantinian tribulation. A few centuries later, the Church was
wrestling the tribes of Northern Europe into the Kingdom; while
in the East, Christianity experienced a real golden age, and what
we. call “Nestorian” Christians had influence throughout India
and China. A few centuries later, after the high “Middle” ages
and the Protestant Reformation, Christianity greatly discipled
