Eden: The World of Transformation 145

the flood (Psalm 24:2). Suppose all the land of the earth were
connected, so that the bodies of water were separated. In that
case—a mirror image of the real world—we would say that the
seas were borne up by the land. The reverse is the case, how-
ever: Each island of Jand, however large, is bounded by the sea.
Thus, in imagery we have a three-decker universe: sca at the bot-
tom, then land, and finally heaven. (See Diagram 12.1.) The
three-decker world is referred to in Exodus 20:11, Psalm 146:6,
Nehemiah 9:6, and Revelation 10:6.. This visual three~decker
world becomes a symbol for a three-decker moral world: hell,
earth, heaven.

Diagram 12.1
The Three-Decker World

« Firmament-Heaven ,

ADAAL a AA

Sea

We have come to the wider symbolic structures established
by the wording of Genesis 1. We saw that there are two heavens
in Genesis t: the highest heaven, created on Day One, and the
earthly sky-heaven, the firmament, established on Day Two.
The sky-heaven is an image, a symbol, a reminder of the highest
heaven. By implication, the same thing is true of the sea, or
abyss. The “deep,” the “abyss” of the sea points beyond itself to
The Abyss, the place where the devil and the wicked will spend
eternity. This Ultimate Abyss did not yet exist in Genesis 1,
however, because neither angels nor men had yet sinned, and
that is why it is not mentioned in Genesis 1. Once the Ultimate
Abyss was established, however, the ocean-abyss became an im-
