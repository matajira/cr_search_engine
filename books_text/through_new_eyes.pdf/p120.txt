108 Txroucu New Eves

This is not the Biblical view, Christianity teaches that God is

intimately active in running His universe ail the time. He is not

n “absentee landlord.” There are no impersonal natural forces
at “work” in the cosmos. Bavinck writes that

after the creation of the world God did not leave the world to it-
self, looking down upon it from afar. The living Ged is not to
be pushed to one side or into the background after the creation
issues from His hand.?

From the Biblical perspective, a miracle occurs when God
does something differently from the way He usually does it. As
Auguste Lecerf has written,

The constant relations which we call natural laws are simply
“divine habits”: or, better, the habitual order which God im-
poses on nature, It is these habits,.or this habitual process,
which constitute the object of the natural and physical sciences.
The miracle, in its form, is nothing but a deviation from the
habitual course of natural phenomena, provoked by the inter-
vention of a new factor: an extraordinary volition of God.?

Poythress goes straight to the heart of the matter:

The Bible shows us a personalistic world, not impersonal law.
What we call scientific law is an approximate human description
of just how faithfully and consistently God acts in ruling the world
by speaking. There is no mathematical, physical, or theoretical
“cosmic machinery” behind what we see and know, holding
everything in place. Rather, God rules, and rules consistently.

Anmiiracle, then, is not a violation of a “aw of nature,” and not
even something alongside laws of nature, but is the operation of
the only law that there is— the Word of God. What God says is
the law (see Psalm 33:6).*

The Bible tells us that God actively “works all things after the
counsel of His will’ (Ephesians 1:11). In a particular way con-
cerning the Church it can be said that “there are varieties of
effects, but the same God who works all things in all” (1 Corin-
thians 12:6). It is true of all men, however, that “in Him we live
and move and have our being” (Acts 17:28).
