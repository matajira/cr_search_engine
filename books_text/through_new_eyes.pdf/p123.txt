Angels Ww

at a rate we can describe by a “gravitational constant”? Well,
first of all, the eternally active God caused it to go down at that
rate, according to His provisions in the Noahic covenant. Sec-
ond, it is likely that gravity-angels either pulled or pushed it
down at that rate.

We need to face the fact that this way of thinking secms
“primitive” or “childish” to us, but that is only because of the sec-
ular propaganda we have absorbed. God's world is a friendly
world, run by Him personally and by His angels. Investigations
of “natural” processes are really investigations of how God’s
stewards run His house.

Before the rise of modern secularism, Christian theologians
spoke more frcely about this kind of thing. Let me just call atten-
tion to some of John Calvin’s remarks on the prophecy of Eze-
kiel. Calvin takes note of the fact that the angelic cherubim who
drive God’s cloud-chariot have four faces: the faces of man,
eagle, ox, and lion. Calvin. does not hesitate to say that:

by these heads all living creatures are represented to us: for
although trees, and the sea, and rivers, and herbs, and the air,
and stars, and sun, are parts of the universe, yel in living
beings there is some nearer approach to God, and some clearer
display of His energy: for there is motion in a man, jn an ox, in
an eagle, and in a lion. These animals comprehend within
themselves all parts of the universe by that figure of speech by
which a part represents the whule. Meanwhile since angels are
living creatures we must observe in what sense God atiributes
to angels themselves the head of a lion, an eagle, and a man:
for this seems but little in accordance with their nature. But He
could not better express the énseparable connection which exists in the
motion of angels and all creatures. We have said that angels arc not
called the powers of God in vain; now when a lion either roars
or exercises its strength, it seems to move by its own strength,
so also it may be said of other animals. But God here. says that
the living creatures are in some sense parts of the angels though
not of the same substance, for this is not to be understood of
similarity of nature but of effect. We are to understand, there-
fore, that while men move about and discharge their duties,
they apply themselves in different directions to the objects of
their pursuit, and so also do wild beasts; yet there are angelic mo-
trons underneath, so that neither ‘men nor animals move themselves, but
their whole vigor depends on a secret inspiration.7
