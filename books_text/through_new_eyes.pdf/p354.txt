342

city gate, 88
City of Palm Trees, 91
city-states, 175f.
clean and unelcan, 13, 100ff., 200f., 207
cleansing, 214
clothing, 35
cloud-chariot, 43, 49
see chariot, Divine
common grace, 125
concurrence, 109, 298
confessions, ecclesiastical, 34, 36f., 293
conquest, 190f.
Constantine, 287
constellations, 59ff., 212
see zodiac
constitutions, civil, 34, 36f., 293
contradictions, apparent, 14
cornerstone, 72, 106, 148
cosmic order, 266.
council member, 138ff.
courtyard, 207, 211
covenant regularities, 109
covenant sequence, 130f.
creation week, 45
creationism, 291
creeping things, 12
cross, 90, 148
cud chewing, 101f.
culture, 118
curtains, 113, 207, 214f,
Cush, 150f.
cutting off, 202, 305f.
cypress, SIF.

Daniel, 135, 250

Danites, 187

darkness, 42f.

David, 19f., 71, 86, 135, 148, 154, 183
genealogy of, 307

day, 1
Day of Atonement, 268, 308
Day of the Lord, 54, 64, 168f., 302
days of Genesis 1, Off

deacons, 141

death penalty, 264

Deborah, 56, 88, 91

deception, 184

dedication, 122

deep sleep, 199

deism, 107

demiurge, 19

design of creation, 9, 13, 30

TuroucH New Eves

dinosaurs, 99, 291

distribution, 19

dolphin, 214f.

dove, 100, 173

drunkenness, 303

dust, 102, 189, 215
see soil

eagle, 101

Eden, 73, 75, 81, 90, 92, 118
homeland, 152ff,

Egypt, Egyptians, 63, 65, 85

elders, 141, 202f., 291

Eleazar, 225

Eh, 225, 308

Elijah, 88, 233f.

Elim, 91

Elizabeth, 199

encroachment, 113

Enoch, 170

ephah, 253

ephod, 14

Ephraim, 221

equinoxes, 55, 61

establishment, 16B

Esther, 92, 251

Eucharist, 126

evaluation, 120

evangelism, 189

 

evolution, 10
excommunication, 202, 264, 305f,
exodus, 168, 304
exodus pattern, 18247,
Mosaic, 204
expulsion, 169
eye, 216
Ezekiel; 244, 309

famine, 184

Feast of Tabernacles, 211

festivals, 54, 203

fig tree, 65, 87, 92

firmament, 41ff., 54, 147, 161, 207, 171

firstborn, 188, 213, 266

fish, 100, 102
see dolphin

five-fold action, 118

flags, 55

Flood, 100, 150, 154, 1708., 204, 272,
298, 303

foodland, 73, 75
