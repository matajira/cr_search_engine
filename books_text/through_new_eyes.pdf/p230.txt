2g Turoucn New Eyes

Socictal: The Ten Commandments, and the
Mosaic Law as a whole, both as it
symbolized society (Tabernacle and
sacrifice) and as it legislated for soci-
ety (the “case laws” of Exodus and

Deuteronomy).
Polity: Separation of Church and state.
Church: Priests at Tabernacle, Levites at
synagogues.
State: Local elders, with judges at the top.

Symbol: The Tabernacle complex.
