Diagram 15.4
Environments

Place Symbol Occupant Guardian — Furniture Tabernacle Boundary
Garment Shield

Highest Most God Cherubim Ark 1/2 Inner Veil
Heavens Holy (High Priest). Mercy Tabernacle
Seat Curtain
Firma- Holy God Priests Altar V2 Outer Veil
ment Place (High Priest) Lamp Tabernacle
Heavens (Cherubim) Table Curtain
Garden Court- — (Priests)' Levites Laver Tent Court
yard (Priests) Altar Curtain Curtain
Eden Canaan . Israel Israel Dwellings Red River &
Army (Lev.11)? Ramskin Boundary?

World Warld Gentiles Gentiles Dwellings Dolphin Four
Corners of
the Earth

Notes:

‘Adam and Eve were living in the garden, but were kicked out. The temporary
and partial privilege of the priests to dwell there pictured man’s eventual return
to that estate.

The dwellings of the Israelites had to be clean. Dead animals defiled them.
Leprosy defiled them. They had to be cleansed annually of all old leaven before
Passover.

3The importance of these boundaries is hightighted by the huge attention given
them in Joshua 15-22. Note also the symbolic boundaries in Ezekiel 48.

This information is taken from James B. Jordan, “From Glory to Glory: Degrees
of Value in the Sanctuary,” and summarizes the material. presented there.
