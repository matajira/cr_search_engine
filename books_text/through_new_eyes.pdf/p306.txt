294 Notks to Paces 45-59

9. R. Laird Hareis, Gleason L. Archer, Jr, and Bruce K. Waltke, eds., Theological
Wordbook of the Old Testament, 2 vols. (Chicago: Moody Press, 1980) 2:861, no. 2217.

10. In addition to the works of Meredith Kline, noted above, a valuable study of
the glory-cloud is R. E. Hough, The Ministry of the Glory Cloud (New York:
Philosophical Library, 1955). Also see David Chilton, Paradise Restored, chap. 7,
and Chilton, Days of Vengeance.

11. Richard M. Davidson, Typology in Scripture: A Study of Hermeneutical TYPOS
Structu‘es (Berrien Springs, MI: Andrews University Press, 1981), pp. 115-190.

12, The best introduction to the study of typology is Davidson, referenced above.
Davidson refines and perfects the perspective found in Patrick Fairbairn’s nine-
teenth-century study, The Typology of Scripture, 2 vols. (New York: Funk and
Wagnalls, 1876). Davidson is an Adventist, but the peculiarities of that position
seldom interfere with the value of his five-hundred-page study. The student
may wish to look at the following studies as well: G, W. H. Lampe and K, J.
Woollcombe, Essays in Typology, Studies in Biblical Theology 22 (London: SCM
Press, 1957); Francis Foulkes, Yhe Acts of God: A Study of the Basis of Typology in the
Old Testament (London: The Tyndale Press, 1958); R. T, France, jesus and the Old
‘Testament (Downers Grove, IL: Inter-Varsity Press, 1971); Moisés Silva, Has the
Church Misread the Bible (Grand Rapids; Zondervan, 1987). An excellent popular
introduction to the typological reading of the Bible is $. G. DeGraaf, Promise
and Deliverance, 4 vols., trans, H, Evan Runner and Elizabeth W. Runner
(St. Catherines, Ontario: Paideia Press, 1977).

13. Jean Daniéiou, From Shadows ta Reality: Studies in the Biblical Typology of the Fathers,
‘trans: Wulstan Hibberd (Westminster, MD: Newman Press, 1960), p. 1.

{4. Tbid., p. 57, “in an unpublished thesis, Fr. Delcuve has shown that [Philo’s]
Allegorical Commentary on the Laws was entirely a symbolical interpretation of the
Aristotelian theory of knowledge,” p. 58.

15. Ibid., p. 38.

16. Herbert Schlossberg, Zdols for Destruction (Nashville: Thornas Nelson, 1983),

17, Notice how the description of Jesus Christ in Revelation 1 matches the des
tion.of His bride, New Jerusalem, in Revelation 21-22. The bride has been
remade in the image of her Husband.

 

 

 

Chapter 5—Sun, Moon, and Stars

{. Judges 5:31; t Thessalonians 5:1-11; Genesis 15:12; 32:23-30; Exodus 12:29;
2 Samuel 23:4; Isaiah 60:1-3; Zechariah 1-6; Malachi 4:1-2; Luke 1:78; John
3:2. See James B. Jordan, “Christianity and the Calendar,” Chapters 2 and 3.
Available from Biblical Horizons, P.O. Box 132011, Tyler, TX 75713.

2. See James B. Jordan, Judges: God's War Against Humanism (Tyler, TX: Geneva
Ministries, 1985), pp. 107, 144, 236.

3, There may be an intended association between the east-west motion of the ris-
ing sun and the east-west motion of God’s glory as it enters His house. Notice
the imagery of Deuteronomy 33:2; Judges 5:4; Ezekiel 43:1-5.

4, M. Barnouin, ‘Remarques sur les tableaux numériques du libre des Nombres,”
Reoue Biblique 76 (1969):351-64; Barnouin, “Recherches nurériques sur la généal-
ogie de Gen. V,” Revue Bibligue 77 (1970):347-65; Barnouin, “Les Recensements
du Livre des nombres et 'Astronomie Babylonienne?” Vitus Testamentum 27
(1977):280-303; English translation, “The Censuses of the Book of Numbers
and Babylonian Astronomy,” available for $5.00 from Biblical Horizons, P.O.
Box 132011, Tyler, TX 75713 cf, also Gordon Wenham, Numbers: An Introduction
and Commentary (Downers Grove, IL: Inter-Varsity Press, 198i), pp. 64-66; and
Wenham, Genesis 1-15 (Waco, TX: Word Books, 1987), pp. 133-34.
