 

There dwells the Lord our King,
Tae Lorp Our RIGHTEOUSNESS
(Triumphant o’er the world and sin),
The Prince of Peace;

On Zion’s sacred height,

His Kingdom still maintains;

And glorious with his saints in light,
Forever reigns.

—Thomas Olivers
“The God of Abrah’m Praise,” stanza 7

 
