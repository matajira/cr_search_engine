18.

Noves ro Pacgs 192-202 305

In light of the conversion of Egypt, there is no reason to think that Potipherah
was a heathen priest, The Joseph ‘who was too pure to become involved with
Potiphar’s wife would hardly have joined himself to a heathen woman.

Chapter 15—The World of the Tabernacle

1.

Notice that God’s people are said to continue and be established after God has
changed the heavens and earth. This could apply to the Resurrection and the
new physical universe, but more likely applies to an in-history covenant
change.

. As mentioned in the previous chapter, there were so’ many servants that the

Hebrews were given the entire land of Goshen in which to dwell. Most of these
“servants” were adopted freemen, which is what the expression “born of the
house” means (Genesis 14:14). On this see James B. Jordan, The Law of the Cove-
nant: An Exposition of Exodus 21-23 (Tyler, TX: Institute for-Christian Eco-
nomics, 1984), pp. 76-84, The retinue of the patriarchal sheikdom are referred
to in Genesis 24:2; 26:19, 25, 32; 32:16.

. It can be argued that the entire Tabernacle and sacrificial systern flowed from

Passover, At Passover, God claimed the firstborn sons of Israel as.His priests,
‘They fell into sin at the Golden Calf, and the Levites were substituted for them.
Also at Passaver, God put blood on the houses of the Israelites and spared thern
His plague. These protected houses can be correlated to the Tabernacle, itself a
symbol of the nation of protected houses. Thus, the nalion-under-blood, and
the Tabernacle as its symbol, were created as Passover. All the new sacrifices
added to the original burnt offering had to de with Tabernacle-access, All the
laws of cleanness had to-do with Tabernacle-access. ‘Thus, all of this stemmed
from Passover.

. Sec James B. Jordan, Sabbath Breaking and the Death Penalty: A Theological Investi-

gation (Tyler, TX: Geneva Ministries, 1986); and Jordan, “Christianity and the
Calendar (A Syllabus)" (available from Biblical Horizons, P.O. Box 132011,
Tyler, TX 75718).

. This is the tendency, if nat the actual position, of the “theonomic” writings of

R. J. Rushdoony and Greg Bahnsen. See Rushdoony, Institutes of Biblical Law
(Nutley, NJ: Craig Press, 1973); Bahnsen, Theonomy and Christian Ethics (Nutley,
NJ; Craig Press, 1977). For a critique, see Vern S. Poythress, Understanding the
Law of Moses forthcoming). It should be noted that the “theonomic” positions of
Rushdoony and Bahnsen are much more theologically sophisticated and
moderate than the radical “Mosaic theocracy” views spouted by certain militant
Anabaptists at the time of the Reformation and by certain extreme wings of the
Puritan movement a century later. Modern “theonomists” do not believe in so-
cial reform via political revolution.

. I have explored some of the ramifications of this in James B. Jordan, “The

Death Penalty in the Mosaic Law: Five Exploratory Essays” (available from
Biblical Horizons, P.O. Box 132011, Tyler, TX 75713).

. Some have argued that “cutting off” is the same as execution, but this is impos-

sible. Leviticus 18:29 states that all the aborninations of Leviticus 18 are pun-
ished by “cutting off,” but in Leviticus 20, these same crimes are discussed, with
a variety of punishments. Only a few are capital offenses. Clearly, then, “cut-
ting off” does not mean cxecution, It probably implies that God will deal with
the sinner—God will cut him off—but since the officers of the Church know
that God has set His face against the person, excommunication must be the
