FOURTEEN

THE WORLD OF
THE PATRIARCHS

 

We have stated that there is always a decline that partially
explains the need for a new covenant. It is also true, however,
that even if man had not sinned there would have been advances
from glory to glory. Thus, the coming of a new covenant is not
wholly to be explained by the failure of the previous one. Also
involved is the fact of human maturation, so that what was once
appropriate and fitting at a certain stage of childhood now must
be superseded. As children grow, we have to keep getting them
new shoes and new clothing, partially because the old ones are
wearing out, but also because the child has outgrown them.

This explains why God never simply calls His people back to
the previous covenant. The prophets come and tell the people
that they have broken the covenant and remind them of their
duties in terms of the old covenant; but when covenant renewal
comes, it is never simply a return to the old ways. Rather, it is a
renewal of the old ways in a new form, a form appropriate to the
times and to the stage of growth.

As we move into the period of the patriarchs, it will be help-
ful to set out a rough overview of covenantal history. After the
Flood, God re-created the world with the Noahic covenant. With
the sins of Ham and: then Nimrod, the world order was threat-
ened, and God took advantage of the opportunity to set aside a
new (Edenic) dand with the Abrahamic covenant, designating
one nation, the Hebrews, to be priests to the rest. That nation of
priests fell into sin in Egypt, and God took the opportunity to re-
create the (Garden) sanctuary with the Mosaic covenant, setting
aside the Levites and Aaronic priests to guide the Israelite na-
tion. Next, just as God planted a Garden in Eden and then

481
