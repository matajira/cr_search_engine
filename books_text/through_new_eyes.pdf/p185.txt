The World of Noah 473

This exodus was a wholly miraculous event. A few moments’
meditation will show just how extraordinary it was. First, it in-
volved a miracle for all the animals to come to Noah and enter
the Ark, We should consider this parallel.to the movement of
God’s people out of Egypt to Mount Sinai, remembering that
animals are images of humanity.

Second, it involved a miracle for these animals to be at peace
with one another. During the wilderness wanderings, God per-
formed many miracles, miracles of judgment, to force the people
to keep peace and to put down rebellion (cf. esp. Numbers
11:1-3; Numbers 16-17).

Third, feeding and caring for these animals involved some
kind of miracle. Jt would not have been possible to carry on the
Ark enough food to feed all these animals for a whole year. God
fed Israel with manna.in the wilderness, and it is easy to imagine
that something similar happened here. Then again, perhaps
many of the animals hibernated for the whole year,

Thus, the exodus transition was accomplished by a whole
series of miracles, While Noah and his family were sustained on
the Ark, the angels were busy remaking the world. They were
burying animals to make oil, and plants to make coal, and in
many other ways preparing a new world for humanity.

A detailed study of the Flood will reveal many re-creation
motifs at work.? The subsiding waters revealed the land, just as
in the creation week. The dove hovering over the water recalls the
Spirits hovering at creation, and the dove-Spirit hovering over
our Lord at His baptismal inauguration of the New Covenant.

Establishment

With the resting of the Ark, we have a transferring of the
world model to the world. We shall see this again at Mount
Sinai, when the configuration of that mountain is transferred to
the configuration of the Tabernacle. Here with Noah we find
that the triple-decker Ark becomes the model for a new triple-
decker world. The waters recede, and the world is made anew
after the image of the model.

The arrival of the Ark is like the arrival of Israel in Canaan.
God gave the world anew to Noah, telling him to be fruitful and
multiply in the new creation (Genesis 8:16-17). God promised
