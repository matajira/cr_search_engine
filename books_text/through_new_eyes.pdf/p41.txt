THREE

SYMBOLISM
AND WORLDVIEW

 

The study of symbolism is seen by some as a curiosity, rather
far removed from the central matters of life. According to them,
anyone who spends time studying Biblical imagery: and sym-
bolism may well be getting into a “dangerous” area. Persons who
engage in an “overly symbolic” interpretation of Scripture are to
be regarded with suspicion. What matters is the study of reality;
symbolism is secondary.

This attitude betrays the influence of the Greek view of the
world. According to the Greeks— and actually all pagans—the
world was not made by God, Rather, the world, or the raw ma-
terial of the world, has always existed. This always-existing stuff
just és, and so it is called “Being.” This “Being” stuff is like a blank
slate. It is silent and meaningless “raw material.” It does not bear
the impress of any Creator, and it does not joyfully shout His
name (Psalm 98:4-9).

How did our present world come about, then? Well, the an-
cients believed that a designer or maker came along, often called
a “demiurge.” This demiurge imposed order on the primeval raw
material. He imposed meaning, structure, and symbol on the
neutral, always-existing world. Human beings, according to the
pagan view, are like little demiurges: We go through life impos-
ing meaning and structure and order on the world. Modern phi-
losophy, especially after Immanuel Kant, has taken an even
more radical view. The modern view is that there is no demiurge,
and that the universe is really ultimately chaotic. Whatever order
and meaning there is in the world has been imposed by human
beings, and by no one else. We create our own worlds by generating
our own worldviews. All meaning, all symbols, are man-made.!

29
