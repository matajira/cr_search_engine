Symbolism and Worldview a

And not only so. Just as everything in creation is a general
symbol of God, so also man is the special symbol, for man and
man alone is created as the very image of God (Genesis 1:26).
Each individual human being, and the race as a whole (Genesis
1:27), symbolizes God in a special way. What is this special way?
Theologians have debated the issue, and no one will ever fully
understand it (since to do so we should have to understand fully
the nature of the God whose symbol we are). All the same, this
much can be said: Man is the only symbol that ts also a symbol-maker.
The first part of Genesis | is the context in which it is then said
that man is the image of God. God has been presented as one
who determines, creates, evaluates, names, takes counsel among
Himself, etc. These things are what man uniquely images.

Symbols vary. This is ultimately because, as theologians
would say, in the opera ad- extra of God, one or the other of the
Persons and/or attributes of God is always highlighted. What
does this mean, in simpler language? Well, the opera ad extra are
the works that God does outside of Himself. Some of these works
are more particularly the work of the Father, some of the Son,
and some of the Holy Spirit; though in every case all three Per-
sons are active, because “all of God does all that God does.” Also,
some of these works more particularly show God’s wrath, others
His grace, others His forbearance, others His jealousy, and so
forth; yet in a general way, all of God's attributes are present in
each of His actions.?

In a-general way, everything in creation points to all three
Persons of God, and to all of God’s attributes, if we could but see
it. But in a special way, each item particularly discloses one or
another of His attributes and/or one or another of the Persons.

How are we going to read these symbols? By guesswork?
Happily, we have the Bible to teach us how to read the world.
The Bible lays out for us the primary symbol of God (man), and
four classes of secondary symbols: animals, plants, stones (non-
living things), and stars (heavenly bodies). The Bible also dis-
cusses angels, though whether angels should be regarded as, like
man, special symbols (images) of God, is a matter of theological
debate. Since each of these signifies God, it also signifies man, as
well, The Bible teaches us how to interpret these symbols, as we
shall see in later chapters in this book. Some symbols are verbal
