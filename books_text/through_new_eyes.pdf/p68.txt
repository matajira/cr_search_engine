56 ‘Turoucu New Eves

to Abraham in Genesis 15:5, reiterated in Genesis 22:17, 26:4,
and Deuteronomy 1:10. Christians “appear as stars in the world
in the midst of a crooked and perverse generation” (Philippians
2:15). The fact that Abraham’s descendants were to be like stars
implies that they would not only be positioned in the heavenlies
(Ephesians 2:6), but also that they wauld be ruders over the gentiles.
Now, it is neither possible nor desirable to separate these
aspects of astral symbolism. The sun, moon, and stars mark time
as clocks. Since they mark time, they govern time. Positioned in
the heavenlies, they signify governments, ruling day and night.
Since they mark time, they can be seen to signify the duration of
earthly governments, so that as we shall see the fall of sun,
moon, and stars is a symbol for the fall of earthly governments,
Let us now took at these things in more detail.

The Sun
The sun, ruler of the sky and of the day, is used to symbolize
the Lord in Psalm 84:11, “The Lord God is a sun and shield.”
Similarly, a familiar passage in Isaiah says,

Arise, shine, for your light has come, and the glory of the Lorp
has risen upon you. For behold, darkness will cover the earth,
and deep darkness the peoples; but the Lord will rise upon you,
and His glory will appear upon you. And nations will come to
your light, and kings to the brightness of your rising (60:1-3).

God is like the sun, and when He comes, He glorifies His
people so that they also shine. So Deborah could pray, “Let those
who love Him [God] be like the rising of the sun in its might”
(Judges 5:31b), a prayer answered a few years later in Gideon,
and then again in Samson, whose name means “Sun.” Psalm 19
reflects on this: The sun is like a bridegroom, like a strong man.
The reference here, first of all, is to Samson, the bridegroom of
Judges 14-15. But beyond this we see the Messianic Judge of all
the earth, who is to come and bring His Word (wv. 7-11). When
John saw that One, “His face was like the sun shining in its
strength” (Revelation 1:16b).3

Night gives way to day, and this is an image of the coming of
the Kingdom. If Nicodemus met with Jesus by night, this was in
part a reflection of the condition of history at that point, for the
