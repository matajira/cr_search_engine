The World of the Tabernacle 499

In making His people a nation, God gave them social laws as
part of the Mosaic law. There is a good deal of misunderstand-
ing about the Mosaic law in Christendom today. The three most
common errors about the law are that it was harsh, was impossi-
ble to obey, and is irrelevant to us today.

Against the first misconception, that the law was harsh, we
have to say that our God is a God of love. God never gave any
mean, harsh, unreasonable, or cruel laws. God’s laws, even
those thundered from Sinai, were loving, joyous, peaceable,
patient, kind, good, faith-filled, gentle, longsuffering, temper-
ate, and spiritual. If they seem harsh to us, it is either because
we have misinterpreted them, or because we are still looking at
them from a secular humanistic perspective. We dare not, how-
ever, judge the Bible by our own modern standards.

Against the second misconception, that the laws were so
tough, so demanding, and so stringent that nobody could ever
keep them, we must say that this is not so. The Bible tells us that
Zacharias and Elizabeth “were both righteous in the sight of
God, walking blamelessly in all the commandments and require-
ments of the Lord” (Luke 1:6). Clearly, the law could be kept,
and was kept by many godly people. True, they were not perfect,
but they kept the law by bringing sacrifices to cover their sins.

Galatians 4:1 says that the people in the Old Covenant were
like children, and Galatians 3:24 says that the law was like a
tutor for children, The law, then, was a “simplificd accommo-
dation” for. children. We expect more from adults than from
children. Adults have greater responsibilities and are more ac-
countable than children. Thus, the New Covenant law is actu-
ally much tougher to obey, because it makes so many demands
on our inward attitudes.

Why do people think the Mosaic law was hard to keep? In
general, it is because they do not know what the law really com-
manded, and because they have the Mosaic law confused with
the rabbinical traditions of Judaism. The rabbinical traditions
were a “heavy yoke” (Matthew 15:1-20; Mark 7:1-23; Acts 15:10;
Matthew 23:4). Jesus called the people back to the Mosaic law,
making it His own, and in doing so said that He was offering an
“easy yoke” (Matthew 5:20-48; 11:29-30). We should, then,
briefly look at the Mosaic law.
