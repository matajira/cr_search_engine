10,

ll.

12,
13.

14,

Notes To Paces 148-158 - 301

. On the cross, see James B, Jordan, The Sociology of the Church (Tyler, TX:

Geneva Ministries, 1986), pp, 212-218.

. See C. S. Lewis, The Discarded Image (New York: Cambridge University Press,

1964), pp. 2B, Gl, 140i; and Daniel J. Boorstin, The Discoverers (New York:
Random House, 1983), pp. 94, 98, et passim.

. Some useful insights, and some questionable ones, into the location of Eden in

the north are provided by B. F. Pocock, “North and South in the Book of Gen-
esis,” in J. H. M. Beattie and R. G. Lienhardt, Studies in Social Anthropology:
Essays in Memory of E. E. Evans-Pritchard by his former Oxford Colleagues (Oxford:
Clarendon Press, 1975), pp. 279-284.

. Had God been coming from Jerusalem to Ezekiel, He would have come from

the west. The “north” here must, thus, point back to Ararat and Eden,

. Zechariah’s prophecy concerns the evangelization ‘of the Northern empires,

Babylon and Persia, under Daniel and Esther, Egypt, to the south, would not
be converted at this point in history.

. Useful reflections on this complex subject will be found in Klaas Schilder,

Heaven; What Is ft?, trans. and condensed Marian M. Schoolland (Grand Rap-
ids: Eerdmans, 1950); Schilder, Christ and Culture, trans. G. van Rongen and W.
Helder (Winnipeg: Premier Pub., 1977); Henry R, Van Til, The Gatuinistic
Concept of Culture (Grand Rapids: Baker Book House, 1959); and Francis Nigel
Lee, The Central Significance of Culture (Phillipsburg, NJ: Presbyterian and Re-
formed Pub, Co, 1977),

See George Ernest Wright and Floyd Vivian Filson, The Westminster Historical:
Allas to the Bible, rev. ed. (Philadelphia: Westminster Press, 1956), p. 105. Also
“Zion,” in John McClintock and James Strong, Cyclopedia of Biblical, Theological,
and Ecclesiastical Literature (Grand Rapids: Baker Book House, [1867-87] 1981)
10:1100-1102, Zion’s elevation was 2550', while Moriah’s was 2440’,
McChintock and Strong, “Olivet,” 7:353-355. Olivet was 2665’ above sea level.
As we saw in Chapter 7, olive was the wood of the Most Holy Place. Jesus’ descent
from Olivet to Jerusalem at the triumphal entry may be seen as a descent from
heaven (Matthew 21:1), Jesus walked out of Zion and cursed it and Moriah
from Olivet (Matthew 24:1-3). It was on holy Olivet that He offered His high
priestly prayer (Matthew 26:30). From Olivet He ascended into heaven (Acts
1:12). See further our remarks in Chapter 7,

See Boorstin, chap. 10.

Notice also the association of mountains and men in Revelation 17:9-10, “The
seven heads [of the beast] are seven mountains on which the woman sits, and
they are seven kings.”

The Hebrew for “mountain of God? is hart, while “hearth” is ‘ari, The two
words are obviously quite different, but in spite of this the following Bibles
translate them both as “hearth”: The New American Standard Version, the
New International Version, the Jerusalem Bible, and the New King James
Version. The old King James sidesteps the problem and just translates both
words as “altar.” The assumption that the far is to be changed to “hearth” is
based solely on supposition and the LXX, and is utterly unsupportable, even
though occasionally. advocated by conservatives; e.g., Patrick Fairbairn, 4n
Exposition of Ezekiel (n.p.; National Foundation for Christian Education,
[reprint] 1969), p. 475.

This mistransfation in otherwise conservative translations of the Bible
(which are not known for following the LX!) is all the more peculiar because
so many conservative commentaries on Ezekiel have strongly maintained that
Aarel is not to be tampered with, and that verse 15 clearly and unmistakably
