ACKNOWLEDGMENTS

 

From 1980 to 1988, we lived in Tyler, Texas, and were mem-
bers of Westminster Presbyterian Church, which became Good
Shepherd Episcopal Church in 1987, This book is dedicated to
our friends of those years, a group of godly people: who have
borne with grace the “slings and arrows of outrageous fortune.”
You have our love.

Part of this book was written while I was employed by West-
minster Presbyterian Church as administrator of Geneva Minis-
tries, then a ministry of the Church. After I left that position,
Dr, Gary North of the Institute for Christian Economics provided
me with a living by commissioning me to do some background.
studies in Leviticus. Because of his generosity and-support of my
labors, I was able in my spare time to complete the present book.
Many thanks, Gary.

Parts of this book have seen more informal circulation else-
where in the past. I have noted where this is the case. In 1987,
Geneva Ministries put out two sets of tapes, each with a work-
book, that gave in lecture form the gist of much of this book.
The series “Rocks, Stars, and Dinosaurs” concerns basic world-
view symbolism; the series “The Garden of God” concerns the
transformations of the world in history, focusing on the Garden
of Eden, Tabernacle, Temple, etc. The material in the present
book supersedes what is found in those tape sets.

I must also thank the following people who read the manu-
script and provided encouragement and sound advice: Messrs.
Michael Hyatt and George Grant of Wolgemuth & Hyatt,
Publishers.

xttt
