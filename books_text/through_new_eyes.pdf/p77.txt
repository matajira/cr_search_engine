Sun, Moon, and Stars 65

points, I believe, to something particularly Jewish: the sacrificial
system. If they will not accept the blood of Jesus Christ, the final
Sacrifice, then they themselves will be turned into blood. They
will become the sacrifices. That is what the prophesied war is all
about. That is what the destruction of Jerusalem in a.p. 70 was
all about.

But Joel is issuing a warning. Those who listen can escape.

And it will come about that whoever calls on the name of the
Lorp will be delivered; for [just as Obadiah has already told
you] “on Mount Zion and in Jerusalem there will be those who
escape” [Obadiah 17], as the Lorp has said, even among the
survivors whom the Lorp calls (2:32).

Just as Isaac escaped death on Mount Moriah, because of
the substitute ram that God provided (Genesis 22:13), so those
who trust in the Lamb of God will escape the destruction of
Jerusalem in a.p. 70. Such is Joel’s warning, reiterated by Peter
on the day of Pentecost,

It is also reiterated by John. Prophesying of this same event,
the destruction of Jerusalem, John writes,

And I looked when He broke the sixth seal, and there was a
great earthquake; and the sun became black as sackcloth made
of hair, and the whole moon became like blood; and the stars of
the sky fell to the earth, as a fig tree casts its.unripe figs when
shaken by a great wind (Revelation 6:12-13).

The fig tree is a standard symbol for Israel, especially in this
context (Matthew 21:19; 24:32-34; Luke 21:29-32). Both sack-
cloth and blood remind us of the Levitical system, the blood for
sacrifices, and the sackcloth for the mourning associated with
leprosy and uncleanness.?

In this way, the astral symbols are given peculiar coloring de-
pending on the context. The Babylonians worshipped the stars,
and so they are extinguished. The Egyptians worshipped the
sun, so God darkens it. The Jews continued to maintain the sac-
rifices, so the moon is turned to blood.

To round out this discussion, we need only look at two more
passages, briefly. After promising the coming of the Spirit and
