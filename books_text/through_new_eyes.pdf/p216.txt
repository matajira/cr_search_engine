204 Turoucu New Eyes

The synagogue was the place of sabbath day worship, and
came into existence with the Mosaic Exodus. It is unclear
whether or how the Hebrews kept the sabbath day before Sinai.
God rested on the seventh day, after completing the world.
There is a sense in which the new world, begun at the Flood,
was not completed until Moses, since there was no new garden-
sanctuary until that time. Perhaps God did not reinstitute the
sabbath in its fullness until then. Given the hebdomadal (seven-
day) patterns in Genesis, it is reasonable to infer that the Patri-
archs worshipped on the sabbath (e.g., Genesis 7:10, 12). There
is little evidence to suggest that the sabbath, at least in its Mosaic
form, was binding on the Noahic nations.'9

It is.surely doubtful, however, that the Hebrews were able to
observe the sabbath during the Egyptian bondage. Thus, the
sabbath as a day of rest, festivity, and worship became a distinct
and wonderful blessing of the Mosaic exodus. The book of Ex-
odus is organized in terms of a passage from slavery to sabbath.
The Book of the Covenant, Exodus 21-23, starts and ends with
sabbath laws.*! Deuteronomy 5:15 says that the reason for keep-
ing the sabbath was to memorialize not only the creation, but
also the exodus. Thus, the creation of the synagogue parallels
the establishment of the weekly sabbath.

The Symbolic Polity

In the wilderness, God had his holy army, the militia, camp
around Him in a special symbolic array. It is important for us
to consider this, because it correlates to certain features of the
design of the Tabernacle, the symbol of the body politic. The
Book of Numbers shows Israel as God’s host, His army (Num-
bers 1:2-3). When Israel encamped, the Aaronic priests were
positioned on the east side of the Tabernacle, as guardians of
the door (which was on the east). Around the Tabernacle on the
other three sides were the three groups of Levites. As an outer
ring of warrior guards were the twelve tribes, carefully po-
sitioned on the four sides (see Numbers 2, 3; also'see Diagram
15.1). In this way, the army clearly formed a human taber-
nacle for God’s dwelling (cp. Exodus 23:17; 34:23; Deuteron-
omy 16:16).
