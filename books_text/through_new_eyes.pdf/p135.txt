Breaking Bread: The Rite of Transformation 123

5. Adams works would then be evaluated. Adam would eval-
uate his own works, and so would other people. That por-
tion given to God would be evaluated by Him, the part for
the whole.?

6. The works of the unfallen Adam would be enjoyed by all,
particularly by God, for whom they would be a savor of
sweet incense.

There are two aspects of this I should like to call attention to.
The first is that this process takes place in time. Thus, what is
“good? at an early stage of history may not still be “good” later. A
drawing by a child may be evaluated “very good” by adults, but
the same crudities from the hand. of an adult would not be given
the same evaluation. It is important to affirm the eschatological
character of the good, because it helps to explain the fact that the
products of human work do not endure.

It also explains why each stage of the Old Covenant was
good and wonderful at the time, but yet needed to be superseded
later on. The New Testament speaks disparagingly of the Old
Covenant, using such phrases as “weak and worthless elemen-
tary principles” (Galatians 4:9), “milk for babes” (Hebrews 5:13),
and the like; but only in comparison with the New Covenant. In
1400 B.c., the Mosaic Covenant was the most wonderful thing in
the world (Deuteronomy 4:6-8). But what is good for a child is
not necessarily still good for an adult, and it is perverse to cling
to childish things (Galatians 4:1-11; 1 Corinthians 13:11).

The second aspect of this, which also pertains to the fact that
human works do not in themselves endure, is that man’s six-fold
action is an act of glorification, Man is God’s agent for the glori-
fication of the world. The world was created glorious, but is to
become more glorious progressively under the hand of man.
“Glory” is a difficult concept to describe, but clearly it has to do
with the revélation of God. We know that God is fully revealed,
and thus fully glorified, in all that He has made, Yet, the work of
man is to reveal God even more, and bring Him even more
glory. This is a theological paradox, called sometimes the “prob-
lem of the full bucket.” If God is fully glorious, how can the crea-
tion add to His glory? If God is fully revealed in creation
(Romans 1:19-20), how can He become more fully revealed?
This is a mystery, but it is also. clearly the truth.
