192 TuroucH New Eves

Behold, how good and how pleasant it is

For brothers to dwell together in unity!

It is like the precious oil upon the head,

Coming down upon the beard, Aaron’s beard,

Coming down upon the edge [mouth, top] of his robes.
It is like the dew of Hermon,

Coming down upon the mountains of Zion;

For there the Lord commanded the blessing:

Life forever!

Thus, the mist of God’s cloud upon His mountain is parallel
to the oil poured on Jacob's stone, It was a symbol that at the top
of the holy mountain, the ladder to heaven, was the glory-cloud of
God. Jacob performed this ritual, which seems so strange to our
symbol-impoverished twentieth-century minds, in the sure and
certain confidence that someday God’s glorious cloud, His heav-
enly Kingdom, would come to earth, The cloud and the oil rep-
resent the Holy Spirit, the Bond of unity in the Kingdom. The
descent of the cloud on the mountain, and of the oil on the pillar,
find their typological fulfillment with the descent of the Spirit on
our Lord at His baptism, and upon the Church at Pentecost.16

History and Decline

For a while, God’s priestly nation was faithful to Him. They
prospered despite tribulation. They had successful ministries
with the Philistines, who were anxious to be led in worship by
Abraham and Isaac (Genesis 21:22-34; 26:26-33). As a climax,
God converted the Pharaoh of Egypt, and put Joseph in charge
of the whole world.!? When Joseph married the daughter of the
priest of Heliopolis, we see a unification between the older
Noahic Gentile priesthood and the new Abrahamic special
priesthood.!8 During these years, the nation grew larger and
larger, so that while only seventy from Jacob’s immediate family
went down into Egypt (Genesis 46:27), yet the number of people
in the nation was so great that they had to be given the entire
land of Goshen, the best of that Edenic land, to dwell in (Genesis
47:6; cf. 13:40).

After a prosperous season in Egypt, however, the people
lapsed into idolatry (Joshua 24:14), God raised up a tyrant to
scourge them, and thus put them into a crucible to restructure
