 

Round each habitation hovering,

See the cloud and fire appear

For a glory and a covering,

Showing that the Lord is near;

Thus deriving from their banner
Light by night and shade by day,
Safe they feed upon. the manna
Which he gives them when they pray.

—Jehn Newton

 
