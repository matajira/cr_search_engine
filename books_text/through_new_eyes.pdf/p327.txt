BIBLIOGRAPHY

 

This bibliography is by no means complete. Many other
works were used in the present book, and they are found. in the
notes. Here I have simply listed those works I believe would be
of greatest help for those wishing to follow up some of the themes
in this book. My own previous studies naturally figure promi-
nently in this list, since they form much of the background to the
present work. All of my studies are available from Biblical
Horizons, P.O. Box 132011, Tyler, TX 75713.

Some of the works referred to here are written from an or-
thodox Christian perspective, while others, though containing
useful information, are not. I have used the term “liberal” to
denote works that assume that the Bible is not the infallible
Word of God, but the product of human authors. I have used the
term “evangelical” to denote works that hold to the Christian
view of the inerrancy of Scripture.

Albright, W. F. Archazology and the Religion of Israel. Baltimore: Johns
Hopkins, 1946.

Allen, R. H. Star Names: Their Lore and Meaning. New York: Dover
Pub., 1963.

Bavinck, Herman. Our Reasonable Faith. Translated by Henry
Zylstra. Grand Rapids: Baker Book House, (1956) 1977.

. The Doctrine of God. Translated by William Hendriksen.
Edinburgh: The Banner of Truth Trust, (1918) 1977.

Beattie, J. H. M., and Lienhardt, R. G. Studies in Social Anthropology:
Essays in Memory of E. E. Evans-Pritchard by his Former Oxford Col-
leagues. London: Oxford University Press, 1975.

Berkhof, Louis. Systematic Theology. Fourth Edition. Grand Rapids:
Eerdmans, 1949.

35
