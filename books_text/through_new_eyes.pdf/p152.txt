HO TrroucH New Eves

’ challenge God. Amos asks God to forgive Israel, because Jacob
is so small (Amos 7:2), As Heschel aptly puts it, Amos does not
say, “Thy will be done,” but “Thy will be changed.” And in the
case of Amos the Lord concedes. He repents: “‘It shall not be,’
said the Lord” (Amos 7:3). The pivotal role of the prophet as
one who stands in the council of the Lord and who becomes a
partner in the unfolding of God’s covenant plans of judgment
and salvation is crucial for understanding the way in which
the New Testament people as a whole may be considered
prophetic people,1!

John the Forerunner was the greatest prophet of the Old
Covenant, according to the testimony of Jesus, yet the least in
the heavenly kingdom of the New. Covenant would be greater
than he (Matthew 11:7-14), As a Council member, John was
privy to more information than any-other prophet ever had been
(John 1:33), yet matters were not completely clear even to him
(Matthew 11:3). Living under the Old Covenant, before the veil
was torn and heaven reopened, John’s access to the Council was
limited and partial.

It would not always be so, however, God had given Joel to
prophesy that when the New Covenant arrived, everyone would
be made full-time Council members:

And it will come about after this that I will pour out My Spirit
on all flesh; and your sons and daughters will prophesy, your
old men will dream dreams, your young men will see visions,
and even on the male and female slaves will I pour out my
Spirit in those days (Joel 2:28-29).

Not just men but women also; not just adults of full strength but
also youths and the aged; not just free but also slaves; not just
Israel but all flesh — all would be privileged to sit with the Coun-
cil. All would have a voice in the decision-making processes,
though each according to his station.

Jesus prophesied the same:

No longer do I call you slaves; for the slave does not know what
his master is doing; but I have called you friends, for all things
that I have heard from My Father I have made known to you
(John 15:15).
