COLOPHON

 

The typeface for the text of this book is Baskerville. Its creator, John
Baskerville (1706-1775), broke with tradition to reflect in his type the
roundet, yet more sharply cut lettering of eighteenth-century stone in-
scriptions and copy books, The type foreshadows modern design in such
novel characteristics as the increase in contrast between thick and thin
strokes and the shifting of stress from the diagonal to the vertical strokes,
Realizing that this new style of letter would be most effective if cleanly
printed on smooth paper with genuinely black ink, he built his own
presses, developed a method of hot-pressing the printed sheet to a
smooth, glossy finish, and experimented with special inks. However,
Baskerville did not enter into gencral commercial use in England until
1923.

 

 

Substantive editing by Michael S. Hyatt
Copy editing by Barbara Sorensen
_ Cover design by Kent Puckett Associates, Atlanta, Georgia
Typography by Thoburn Press, Box 2459, Reston, Virginia 22090
Printed and bound by Maple-Vai! Book Manufacturing Group
Manchester, Pennsylvania
Cover Printing by Weber Graphics, Chicago, Illinois
