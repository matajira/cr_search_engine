Angels 107

Jesus affirms that men, in their transfigured glory, are “like the
angels in heaven,” and “neither marry nor are given in marriage”
(Matthew 22:30). Note that men are not said to become angels
(contrary to popular mystical presentations, such as the film “It’s
a Wonderful Life”), Rather, men become like the angels.

In one sense, maturation in glory can never end, either for
angels or for men, since there are always new depths and heights
to God for us to appreciate and reflect. In another sense, how-
ever, there is a point at which maturity is reached; and: in that
sense angels serve as models for men.

Thus, the original creation purpose of the earth was to grow
into heaven-likeness, and for man was to grow into angel-like-
ness. This natural process of growth and development, built into
the Old Creation, was forever wrecked by the rebellion of man.
As a result, the process of maturation took the world downhill in
the direction of wilderness degradation (Genesis 4-6), and man
into the likeness of beasts and creeping things (Romans 1:23;
Psalm 135:18; 49:12).

In the New Creation, inaugurated at the Resurrection of
Jesus Christ, the world and humanity are restored to their origi-
nal program. The developmental processes of the Old Creation
are reestablished in the Spirit, on the basis of the heaven-
attainment of Christ.

Cosmic Gontrollers

Angels run the world for God. This is one of the most diffi-
cult aspects of the Biblical worldview for modern men to under-
stand, and so we should take a closer look at it. The modern
view of the world is that the cosmos is run by natural forces,
sometimes called natural laws. The expression “natural law” is a
holdover from earlier, more Christian times. The notion of a
“law” requires a personal lawgiver, and also a personal agent to
obey the law. What modern people mean by “natural law” is bet-
ter termed “natural forces.”

‘At this point, most modern people are deists. They believe
that God created the universe (billions of years ago), winding it
up like a clock, and then leaving it to run itself. Occasionally
God interferes in these natural processes, and they call this a
“supernatural” event, or a “miracle.”
