Birds and Beasts a7

Scripture depicts God with strikingly concrete imagery. He is
described in varying contexts as a father, husband, judge, king,
warrior, winnower, husbandman, smelter, builder, teacher,
healer, harvester, giver of rain, leader of the blind, wayfarer
and stranger, as well as the shepherd of a flock. It is therefore
not surprising to find God also portrayed with imagery drawn
from the world of fauna.

As an eagle that stirreth up her nest,

Hovereth over her young,

Spreadeth abroad her wings, taketh them,

Beareth them on her pinions. (Deuteronomy 32:11)

I bore you on eagle's wings. (Exodus 19:4)

He will cover thee with His pinions,
And under His wings shalt thou take refuge. (Psalm 91:4)

God is also depicted as a lion, as a leopard, and as a bear lying
in wait for its prey (Isaiah 31:4; Hosea 5:14; 11:10; 13:7, Lamen-
tations 3:10); and the wrath of the Lord is compared to that of a
she-bear bereaved of her cubs (Hosea 13:8; 2 Samuel 17:8; Isa-
jah 59:11), In Balaam’s orations, God is to Israel “like the lofty
horns of the wild ox” (Numbers 24:8; 23:22). Now clearly, God
is not being compared to the animals themselves. It is rather
the deeds of God that are described and compared to the deeds
of specific animals in particular circumstances.

Similarly, Israel is portrayed as being an unfaithful wife, a wild
vine, God's servant, God’s beloved, a bride, a vineyard, as well
as, from the world of fauna, sheep, a wild ass, well-fed
stallions, fatted cows of Bashan, a turtledove, an untrained
calf, and a worm (Micah 2:12; Jeremiah 2:24; 5:8; Amos 4:1;
Psalm 74:19; Jeremiah 31:18; Isaiah 41:14), By means of such
concrete imagery the concept of God and His.covenant with
Israel was related to the life experiences of the Israelites, nota-
bly those embodied in the world of nature.*

God groups animals with men in certain special ways that in-
dicate a closer analogy between men and animals than between
men and any other aspect of the earthly creation. Both men and
animals stand under the penalty of capital punishment for
murder (Genesis 9:5). More specifically, as regards the cattle,
