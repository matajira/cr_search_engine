136 ‘Trroucn New Eves

mother and cleave to his wife; and they shall become one flesh”
(v. 24).

At this point it will be well to note that the act of making a new
creation is simultaneously an act of generating a new covenant.
Marriage is a covenant, according to Malachi 2:14. Covenant-
making in the Bible entails dividing and restructuring, and that
is what we have seen here. In the remainder of this book we shall
iook more fully at God’s acts of restructuring and initiating new
covenants in history. The point here is that human society is or-
ganized by covenant, by acts of separation and restructuring.

Eve is now part of the Garden, and it will be Adam’s task to
guard her as he guards the Garden. This brings us to his priestly
task.

Man as Priest

Systematic theologians generally locate man’s priestly work
in the area of worship. As the Presbyterian Westminster Shorter
Catechism puts it, “Christ executeth the office of a priest, in His
once offering up of Himself a sacrifice to satisfy divine justice,
and reconcile us to God; and in making continual intercession
for us” (Question 25), Without contradicting the insights of sys-
tematic theology, our purpose here must be to uncover the Bibli-
cal imagery surrounding the priesthood.

Just as man’s kingly task proceeds from scientific examina-
tion to aesthetic transformation, so his priestly task has two
stages. First of all, as a priest, man establishes or recognizes pre-
existing boundaries, and then he acts to guard those boundaries.
Adam was to recognize the boundaries of God’s holy Garden,
just as he recognized the names of God's animals. Then Adam
was to protect those boundaries against invasion.

In human society, it is actually a priestly task to establish and
enforce boundaries. In the Church, those boundaries are estab-
lished by baptism and safeguarded by excommunication. In the
state, which also has a priestly role, the boundaries are political
and are safeguarded by military force. Property boundaries are
established by covenanting acts among people in society, and are
safeguarded by the state using police powers.

Preeminently, of course, the priest guards the house of God.
The most elaborate picture of the measuring function is found in
