152 Turoucn New Eves

when Adam turned the Garden over to him. The armies of God
in Zechariah 6 take judgment and then cleansing blessing to the
north, reestablishing it first. Judgment also proceeds to the
south, but without cleansing at this point in history.®

The Three Environments

The Bible tells us that God planted a garden in Eden, on the
east side of that land (Genesis 2:8). This establishes three envir-
onments on the earth: Garden, Eden, World. (See Diagram
12.5.) Men would proceed from the Garden and Home in the
north downstream toward the lands of the south. They would be
motivated to do so by the fact that there were good minerals in
the southern lands, as we are told that in Havilah (Arabia) there
were gold, bdellium, and onyx. Similarly, in another symbolic
picture, men would follow the four rivers out to the four corners
of the earth.

Diagram 12.5
Three Human Environments

 

 

 

 

 

 

 

World

 

 

Let us consider the three environments. The land of Eden
would be Adam’s initial home. It would be the place where he
slept, where his children were reared, and so forth, Home is
where man returns when his work is done.
