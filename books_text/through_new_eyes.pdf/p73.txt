Sun, Moon, and Stars él

Second, clearly the Bible is opposed to the abuse of the constella-
tions for idolatrous or astrological purposes, to tell fortunes and
the like (Deuteronomy 18:9-13; Isaiah 8:19-20; 44:24-25; 47:8-15).

Third, 1 know of no evidence to support the notion that the
precession of-the equinoxes and the 2,000-year-long periods of
time that they introduce (Age of Pisces, Age of Aquarius) are
used by any Biblical passage to structure either history or proph-
ecy. As a way of marking time, the precession of the equinoxes is
part of God’s universal clock system, but it seems. to have no
special symbolic significance.

Fourth, I find no Biblical evidence to support the popular no-
tion that the twelve signs of the zodiac are a map of Bible history
and prophecy, beginning with Virge and ending with Leo. This
has been the most common evangelical/fundamentalist use of
the zodiac in popular literature. Abraham’s analysis of the stars
is explained this way: Abraham looked at the cycle of the con-
stellations and received a picture of prophetic history and of the
coming Redeemer.‘ I can find, however, no foundation for this
approach to the zodiac in Scripture.

Fifth, it is a fact, however, that the four faces of the cherubim
in Ezekiel and Revelation correspond to the four central con-
stellations in the zodiac, and to the four tribes of Israel that were
positioned north, south, east, and west of the Tabernacle in the
wilderness (Numbers 2:1-34). The Lion is Leo, Judah (Genesis
49:9), The Bull is Taurus, Ephraim (Deuteronomy 33:17). The
Man is Aquarius, Reuben, “unstable as water” (Genesis 49:4).
The Eagle is Scorpio, Dan. (This last identification is more diffi-
cult until we understand two things. First, Scorpio was also
drawn as an Eagle in the ancient world, according to R. H.
Allen.’ Second; the scorpion is linked with the serpent, and Dan
is the serpent [Genesis 49:17; Luke 10:17-19].)

With this paradigm in mind, it is possible to draw a diagram
of the twelve tribes in the wilderness, and link the other tribes
with the other zodiacal signs by going to the right and left of each
of the four major (cherubic) signs. A correlation of these signs
with the prophecies of Jacob and Moses in Genesis 49 and Deu-
teronomy 33 would prove most interesting, but we have no time
for it here.8
