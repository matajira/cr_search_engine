Sun, Moon, and Stars 59

association with astral numbers. At any rate, Barnouin’s thesis is
a complex one, but certainly also a challenging one.*

The second alternative view of Genesis 15:5 relates to the
constellations of the zodiac, and we shall defer consideration of it
until after we examine Genesis 37:5-10.

To conclude our look at Abraham, let me point out that
when God made the covenant with Abraham in Genesis 15, He
did so by burying thé sun and making it “very dark” (Genesis
15:17). This implies that if the “seed” is to be like the stars of
heaven and dust of earth, it will have to be a new heavens and a
new earth,

Joseph’s Stars

When Joseph saw twelve stars bowing down before him,
could this have been the constellations of the zodiac, and not in-
dividual stars? In this section, we want to focus on the posszbility
(please note) that the twelve signs of the zodiac may have been
designed by God as twelve portraits of humanity, and that they
may correlate with the twelve tribes of Israel (which clearly were
twelve portraits of humanity).

This is not a new idea. Synagogues dug up in Israel have
been found to have tiled mosaics showing the twelve signs of the
zodiac, and Josephus relates this to the twelve iribes.5 Before
looking at this, however, let us get before us what the Bible says
about the constellations.

The constellations are referred to-several times in Scripture,
without any implied criticism, simply as if they were part of the
created order of things. An example is Job 38:31-33, where God
says to Job,

Can you bind the chains of the Pleiades, or loose the cords of
Orion? Can you lead forth a constellation (or, the zodiac) in its
season, and guide the Bear with her sons? Do you know the or-
dinances of the heavens, or fix their rule over the earth?

This is an interesting passage, for it speaks of the chains and
cords of the astral signs. After all, everyone who has ever learned
the constellations knows that they don’t in the least resemble
what they are supposed to look like. It takes a lot of imagination
to tie together the stars in these symbols. Who, then, devised
