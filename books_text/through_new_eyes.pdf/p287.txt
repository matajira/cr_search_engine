The New World 275

The often controversial gift of other tongues was bestowed at
Pentecost, According to the New Testament (1 Corinthians
14;20-22), one of the primary meanings of this gift, if not the
only one, was as a sign of judgment upon Israel. The Gospel was
going to the Gentiles and would be preached in new languages.
If Israel was to hear it, they would have to hear it in other
tongues. Indeed, the New Testament would be written not in
Hebrew but in.Greek! Such languages would sound like drunken
speech (Isaiah 28:7-10; Acts 2:13-15), but would actually com-
municate judgment. Throughout the book of Acts, the Gospel
went to the Jew first and then to-the Gentile. Finally, in Acts
28:28, Paul declared to the Jews that full judgment was coming
upon them, and that the Kingdom had been taken from them.!?

Finally, after the elimination of the competition in a.p. 70,
the new kingdom stood forth in glory. The former heavens and
earth were done away, and the new had fully come.

Yet, while the initial coming of the Kingdom was in three
stages, there is yet a fourth and final stage to come. After a.v.
70, God made it clear who the true heirs of the Old Covenant
really were. All the same, the Church still exists in conflict in this
world; and no matter how glorious the kingdom may become,
she will still experience difficulty and death, and will still coexist
with unbelievers. Only with the Second Coming of Christ will
the Kingdom be finally come in all its fullness.

Typology

It remains only to note that all the different heavens and
earth, all the different establishments of the Old Covenant are
typological of the New. There is instruction for the Church in
every aspect of the Old Testament. The book of Revelation,
which deals largely with the destruction of old Jerusalem, begins
with letters to seven churches. The message to these churches is
this: You are the true heirs of the Old Govenant, but watch out.
If you commit the same sins as Jerusalem, you will be punished
as Jerusalem is about to be punished. So take heed!

Each of these seven churches was a true and separate church
existing in Asia Minor. In the providence of God, however, each
church was in a different spiritual state. These seven states corre-
spond to seven stages of Old Covenant history. (We have only ex-
