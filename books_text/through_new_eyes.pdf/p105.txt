Trees and Thorns 93

Conclusion

Certainly the Bible enables us to view trees through new
eyes! But this is not really new for Christians. Each year, most
Christians set up a tree in their homes. The Christmas tree has
its origins in the medieval paradise tree, decorated with apples,
and the North European Christmas light, a treelike stand decor-
ated with boughs and candles.* The stylized fruits (balls and or-
naments) of our Christmas tree, and its electric lights, still speak
of glory and beauty, and point us to the nativity of Christ, the
Tree of Life.
