u2 TaroucH New Eves

As the Nicene Creed says that the Holy Spirit is the “Lord
and Giver of life,” so Calvin seems to have felt that life and
energy were communicated to men and animals by the angelic
agents of the Spirit. Calvin’s interpretation of Ezekiel 1 may not
be correct at this particular point, but his overall worldview is in
accord with the Bible’s.

Angels and Men

Now that we have a fuller understanding of the service of
angels, we can see once again how they model life for humanity.
We are to do God’s will on carth as it is done in heaven, to act in
this world as heavenly people. The angels—perfect servants of
God —are thus models for us.

There are three particular things the Bible shows angels do-
ing as models for us. First, the Bible shows angels praising God,
ascribing holiness to Him, around His throne. The seraphim of
Isaiah 6 sing “holy, holy, holy” to Him, as do the cherubim in
Revelation 4. When the cherubim start up this chant, the
twenty-four angels of the second rank take it up, and also wor-
ship God (Revelation 4:8-11).8 In Revelation 4, humanity is ab-
sent, still excluded from heaven, In Revelation 5, however, we
watch the Son of Man, the Lamb of God, ascend to the throne
next to His Father. Now at last the redeemed multitude also can
join the heavenly choir, The cherubim and twenty-four chief
angels begin a new song (Revelation 5:8-10), which is taken up
by the whole angelic host (Revelation 5:11-12), and finally by hu-
manity and all creation (Revelation 5:13-14).

Second, these praises constitute the hymnic throne of God.
The cherubim form His throne in the Tabernacle, as He sits on
their outstretched wings with His feet on the Ark, the footstool
(1 Samuel 4:4; 2 Samuel 6:2; 2 Kings 19:15; Isaiah 37:16; Psalm
80:2; 99:1). When this throne becomes His chariot, we find
again that He is seated above the wings of the cherubim (1
Chronicles 28:18; Ezekiel 1:4-28). Just so, God is enthroned on
the praises of Israel (Psalm 22:3; 148:13), and His throne was
carried. on the shoulders of the Levites (Numbers 4:15; 7:9; 2
Samuel 6:3-7, 13). God’s hosts thus labor for Him as His ser-
vants. By holding Him up, they proclaim Him King.

    
