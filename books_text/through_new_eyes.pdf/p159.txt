Eden: The World of Transformation 147

perfection. Additionally, because of sin, the sea reminds us of
the Abyss, the opposite of heaven, where impenitent sinners will
reside forever.

The Four Corners of the Earth

One of the most familiar symbols of Scripture is that of the
four corners of the earth. Isaiah 11:12 says that God will “gather
the dispersed of Judah from the four corners of the earth,” and
Ezekiel 7:2 says that “the end is coming on the four corners of
the earth.” In Revelation 20:8 the wicked are gathered from the
four corners of the earth.

To understand this imagery it is helpful to recall that the
Bible pictures the world as a house:

Where were you when I laid the foundation of the earth? Tell,
if you have understanding, who set its measurements, since
you know? Or who stretched the line on it? On what were its
bases sunk? Or who laid its cornerstone? (Job 38:4-6)

The world is like a house, the firmament like its ceiling, and
the mountains like the sky’s pillars, so that the collapse of the
mountains is associated with the rending of the firmament-
ceiling (Isaiah 34:3-4; Revelation 6:13-14). As we have men-
tioned, the Tabernacle and Temple were world models, the
world being conceived as:a cosmic house.

A complementary image used in Scripture for the world is
that of the altar. Altars must be made of earth (Exodus 20:24-25)
and have four corners, and these figure the four corners of the
earth (cp. Revelation 7:1 and 9:13). Thus, the fire on the bronze
altar figured the judgment that must come upon the world; and
the sacrifice spoke of the fact that the fire must come either upon
a substitute, or upon humanity itself. In the same way, the in-
cense burning on the altar of incense spoke of the universal duty
and privilege of men to stand upon the world and pray to God.
Thus, when the Bible speaks of the “four corners of the world,” it
reminds us of the world as house and altar. The house imagery
sees the world as a container for men, while the altar imagery
sces the world as a platform for men. Both images are used
throughout Scripture. In terms of the world as house, judgment
means the collapse of the house, the shaking of its mountain-
