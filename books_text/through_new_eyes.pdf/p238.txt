226 Turoucn New Eves

Ithamar to Eleazar. We have also shifted from Tabernacle to
‘Temple. In social polity, we have shifted from judges and seers
to kings and prophets (as discussed in Chapter 11). In the king-
dom we now have a small professional army that protects the
king and that serves as a buffer against attack while the militia is
called up. To support the legitimate needs of the palace and the
king, we have a system of taxation; and the nation is divided
into tax districts that do not correspond to tribal boundaries
(1 Kings 4:7-19).

All of this gradually came into place. To summarize the tran-
sition: During Samuel’s judgeship, the Ark was at Kiriath-
jearim and the Tabernacle and High Priest were at Shiloh. Dur-
ing Saul’s kingship, the Ark was at Kiriath-jearim, the Taber-
nacle at Nob, and the High Priest in the wilderness with David.
During Davic’s reign, the Ark was in Jerusalem with Abiathar,
and the Tabernacle was at Gibeon with Zadok.!! Under Solo-
mon, the Ark was re-enthroned in the Temple with Zadok as
High Priest. Putting all this together indicates that God refused
to put the Ark back into a House until the line of Eli was out of
the way, and the transition to the new priesthood was completed. _

As we saw in the previous chapter, God sets up His new so-
cial polity, the human house, before setting up His new symbolic
polity, the physical house. Thus, when Samuel made Saul king,
he wrote a new constitution: “Then Samuel told the people the
ordinances of the kingdom, and wrote them in the book and
placed it before the Lorn” (1 Samuel 10:25). It would be inter-
esting to have a copy of this constitution, this new law which was
placed before the Lord, that is, in the Most Holy Place along
with the Mosaic Law. We do not have it, however.

‘To me this is most significant. Samuel’s new constitution was
a transformation of the Mosaic Law for the New Covenant. God
has left us the Mosaic Law, but has also shown us that it must be
applied by transformation to new covenantal situations. Such an
application must be made by wisdom, and thus wisdom literature
comes into focus during the Davidic establishment. The books of
Proverbs, Psalms, Ecclesiastes, and Song of Solomon were writ-
ten either largely or exclusively by Solomon and David; and Job
was probably added to the canon at this time. This fact strikes a
blow against any simplistic and legalistic attempt to impose the
