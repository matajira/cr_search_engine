190 THroucH New Eves

Eden. Edenic imagery is reinforced by the attention given to
wells of water in connection with the patriarchal ministry (16:14;
21:19, 25, 30; 26:15, 18-32). This is no accident, Abraham’s
evangelistic ministry was a call to people to make a spiritual
pilgrimage back to God.

In Adam’s Garden there was a holy mountain, a grove of
trees (cp. Genesis 23:17), a well of water, and also a woman.
Significantly, it is in connection with wells that the patriarchs
found their wives. Rebekah and Rachel were both found at wells
(Genesis 24:11-45; 29:2-10), and Moses found Zipporah at a well
(Exodus 2:15). Jesus presented Himself as the True Groom to a
Samaritan divorcee and adulteress at a well (John 4:6-26).

The altars spoke of the coming sacrifice of Jesus Christ on
God’s holy mountain. The trees spoke of God’s glory and shade,
ladders to heaven. The springs spoke of spiritual nourishment, a
nourishment offered by the nation of priests to the Gentiles (see
especially Isaac’s ministry in Philistia, Genesis 26:12-33).

The patriarchs dug wells, built altars, and planted trees
(Genesis 21:33); and everything suggests that these things were
done together in grove settings.1° They created open-air
sanctuary-gardens. They did not build a house for God. In our
survey of the eleventh step in the “Exodus Pattern” above, we
noted that in patriarchal times, after éach exodus there was an
altar built; while in subsequent eras, a house was built after each
exodus. The contrast is important. In the house, all the various
materials are organized together: laver, altar, wooden boards
(trees). The house structure is an appropriate analogue for a na-
tion, The Hebrews were not an organized nation in the age of
the patriarchs, however. Thus, neither a portable house-tent nor
a permanent temple would have been appropriate. Once the
people progressed from glory to glory, into a full-fledged nation,
then the altar-tree-spring garden would progrcss into the glory
of Tabernacle and Temple.

The places Abraham made as his ministry headquarters
were Shechem, Bethel, and Hebron (Genesis 12:6-8; 13:18).
Jacob later made these the sites of his ministry also (Genesis
33:18, 20; 35:1, 6, 7; 27; 46:1). These were the key sites initially
captured by Joshua when he conquered Canaan (Joshua 7:2;
8:30; 10:3). Thus, Abraham and Jacob were engaged in a “shadow
