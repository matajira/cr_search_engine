The World of the Temple 293

These were the preliminary judgments. The depth of the decline
came when Israel was defeated by Philistia and the Ark was taken
into captivity. God also slew the High Priest and his sons (1 Sam-
uel 4), This was the full judgment on the Mosaic establishment.

The final judgment on the Old Covenant is always simultan-
eously an announcement of a new one, as we have seen. It was
at this juncture of history that God intervened to raise up messiahs
to restore the nation. Two men were miraculously born of barren
wombs, both of whom would be Nazirites, special warrior priests,
all their lives, These two men were Samson and Samuel.* Both
men entered into their ministries at about the age of twenty, the
age of citizenship and military service (Numbers 1:3).

As we have already seen in Chapter 14, the Ark plagued
the Egyptians and made an Exodus out of Philistia with spoils
(1 Samuel 5-6). This was the initial defeat of Philistia, but it took
twenty years for Samson and Samuel.to rebuild the nation to the
point where they could inflict a major military defeat on the
Philistines. For twenty ycars Samson caused the Philistines to
appear ridiculous by making sport of them, while Samuel toured
the country teaching the Bible and raising up a new, righteous
generation.> At the end of his career, Samson killed all five
princes of the Philistines, and most of the Philistine priesthood
and nobility (Judges 16:23, 27). With their leadership destroyed,
the Philistines were casily defeated by Israel at the battle of Miz-
pah (1 Samuel 7:9-11).

The crucible of enslavement to Philistia (and remember,
Philistines were Egyptians, Genesis 10:13-14) had the effect of
rending the fabric of the Mosaic establishment. The Israelite
judge, Samson, had to spend most of his time in hiding, while
Samuel had to lay low. As a result, the system of judgeship in
Israel tended to break down. Also, during the captivity, the Phil-
istines removed all the weapons of the Israelite militia, The bat-
tle of Mizpah was won only by a miracle (1 Samuel 7:10). Even
after this victory, Israel was still dominated by Philistia, though
not enslaved; and she still had no weapons (1 Samuel 13:19-22).

As a result of this situation, the people demanded a king.
Their fabricated pretext was that Samuel's sons were not per-
forming very well as judges. We notice, however, that these
young men were judging in Beersheba, on the border of Israel,
