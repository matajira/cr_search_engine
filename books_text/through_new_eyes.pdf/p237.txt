The World of the Temple 225

death, and as a “temple” in His resurrection.® Thus, what Israel
experienced under God’s judgment. was a kind of death, and
under His grace, a kind of resurrection. The Temple would be a
glorified Tabernacle, a-resurrection body if you will.

The Building of the Kingdom

The empty Tabernacle was moved to Nob (1 Samuel 21:1)
and later to Gibeon (1 Chronicles 16:39-40). The priests main-
tained it as a vacant house for God. Once Saul became king,
perhaps God would have moved back into a tent. But Saul fell
from grace almost as soon as he was crowned, and the Philistines
continued to oppress Israel.’ ‘This corresponds to the wilderness
period of Israel after she rejected God's offer to conquer Canaan,
and David's wilderness wanderings illustrate this. After David
defeated the Philistines, and Israel could settle down once again,
God moved back into a tent; but even when David moved the
Ark to Jerusalem, he set up a separate tent for it, and did not
move the Tabernacle there (1 Chronicles 16:1, 37-38).

God also smote the priesthood. Because of their sins, the High
Priest, Eli, and his two sons were killed the day the Ark was cap-
tured (1 Samuel 4:11-18 —In spite of his sins, righteous Eli was more
upset over the capture of the Ark than at the death of his sons).
God swore that the line of Eli, the line of Aaron’s son Ithamar,
would no longer serve as High Priests (1 Samuel 3:14).9 Saul in
his demonic. fury slew all the priests at Nob, including Eli's
grandson Ahimelech (1 Samuel 22:11-19). The son of Ahimelech,
Abiathar, escaped to David, and wandered with him in the wil-
derness until he came to the throne (1 Samuel 22:20). During
David's reign there were two High Priests, Abiathar of Ithamar’s
line, and Zadok of Eleazar’s line (1 Chronicles 24:3).1¢ Abiathar
conspired against Solomon, and Solomon deposed him from
being High Priest (1 Kings 2:26-27). This left Zadok as sole
High Priest, and finalized the transfer of the High Priesthood.

Now, when there is a change of priesthood, there is of neces-
sity a change of law (Hebrews 7:12). The gradual changes in the
priesthood during the century between the Ark’s removal from
the Tabernacle and its re-enthronement in the Ternple were ac-
companied by gradual changes in the law. It is easiest to look
first of all at the big picture, however. We have shifted from
