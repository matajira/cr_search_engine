The World of the Patriarchs 183

having taking hold of bread, broke: it and gave it a new name:
His body. What is seen very simply and basically in such acts of
restructuring is seen at large in the exodus pattern.*

The following are some of the most important exoduses in
the Bible:

1.
2.

Noah's removal from captivity in the Old World to the New,

Abrasn’s removal from death in Babylon to life in Ganaan
(Genesis 11:27-12:5).

. Abrams deliverance from captivity in Egypt to life in Canaan

(Genesis 12:6-13:18).

. Lot’s deliverance from Sodom (Genesis 19:1-16), God’s

offer of life at The Mountain (19:17-19), and Lot’s death in
the wilderness (19:30-38).5

. Abraham's deliverance from danger in Philistia (Genesis

20).

. Isaac’s deliverance from danger in Philistia (Genesis 26).

7. Jacob’s deliverance from enslavement in Mesopotamia

10.

i.
42.

13,

(Genesis 31).

. Israe!’s deliverance from enslavement.in Egypt (Exodus

1-15).

. The Ark of God, taken captive by Philistines, defeats their

gods and is returned, laden with spoils (1 Samuel 5-6).

David’s sojourn in the wilderness and Philistia, and then
his return to the land (1 Samuel 21-2 Samuel 2).

Israel’s return from Mesopotamia, after the exile.

Jesus’ “exodus at Jerusalem’ (Luke 9:31); His renunciation
of Jerusalem and the Temple, and His crucifixion outside
the walls; the new Kingdom of the Mount ‘of Olives.

The removal of the Church from Jerusalem before her de-
struction in 70 a.p. (Matthew 24:16-18; Acts 1-28).

When we remember that the Bible regards the Philistines as
a sub-group of the Egyptians (Genesis 10:13-14), we see that
there are basically two avenues of éxodus in the Old Testa-
ment: those from the North (Babylon, Mesopotamia) and those
from the South (Egypt, Philistia), All of these find their fulfil-
