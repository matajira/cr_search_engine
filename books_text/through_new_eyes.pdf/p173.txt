Eden: The World of Transformation 161

Second, we are now to the point of summarizing the concept
of a ladder to heaven. Any ladder to heaven is of necessity a
model of heaven and earth, with heaven at the top, and the
earthly sanctuary, the gate of heaven, lower down or at the bot-
tom. We can also say that any heaven and earth model is also a
ladder to heaven. For instance, the Tabernacle of Moses: The
Israelite citizen was admitted to the forecourt or gateway. of the
Tabernacle courtyard, where he offered sacrifice. He was not
allowed to ascend the Holy Mountain of the Bronze Altar; and
thus he was not to go farther back into the courtyard than the
Altar. The Altar as Holy Mountain ascended up to the firma-
ment. The Laver of Cleansing thus signified the heavenly sea of
Genesis 1:7 (and not, N.B., the cosmic or Gentile sea of the
waters below). The Holy Place had to do with the visible or fir-
mament-heaven, God’s outer court, while the Most Holy had to
do with the Highest Heaven, the very Throne of God (see
Diagram 12.10). The Bible speaks of both the highest heaven and
the firmament-heaven as tents or tabernacles of God (Job 36:29;
Psalm 18:11; 19:4; 104:2; Isaiah 40:22), We shall, of course, look
at the Tabernacle.in more detail in Chapter 15; for now we only
wish to see it and the Temple as models of heaven and earth, and
thus as ladders to heaven.

Ladders to heaven and models of heaven and earth speak of
two related things. It is relatively easy for us to see that they spcak
of environments. Heaven is an environment, and so is the earth.
The Tabernacle and Temple, with their courtyards, were envir-
onments— physical environments. A special tree, an altar, a mon-
ument pillar, a holy mountain — these are physical environments.

Each of these, however, pictures ‘a social or human environ-
ment, We have seen that the heavenly host has to do very often
with rulers or with saints. In terms of the political arrangement
of a nation, the heavens are the rulers and the earth is the ruled
(Isaiah 13:13; 34:4). In terms of the spiritual polity of the world,
God’s people are positioned in the heavens (Philippians 3:20;
Ephesians 1:20; 2:6; Hebrews 12:22-23).15 We have seen that
trees speak of people, and that an environment of trees, such as
the Temple and Tabernacle, speaks of a body politic. We have
seen that the Temple and Tabernacle were symbols both of the
righteous individual, and thus of Jesus Christ, the True Ladder
