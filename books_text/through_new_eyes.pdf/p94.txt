82 THroucH New Eves

day of creation, when God said, “Let the earth sprout grass,
herbs yielding seed, fruit trees bearing fruit after their kind”
(Genesis 1:11). Although there are other things we eat as well,
such as leaves and tubers, the Bible takes special notice of grain
(seed) and fruit. Grain and fruit are processed into bread and
wine. Melchizedek gave bread and wine to Abram (Genesis
14:18). Joseph encountered a baker and a cupbearer in prison,
and eventually he replaced them both (Genesis 40; 41:53-57;
44:2-5).1 This, of course, carries over into the bread and wine of
Holy Communion,

After the fall of man, plants provided not only food but also
medicine. The Bible signifies this by saying that fruit is for food,
and leaves for healing:

And by the river on its bank, on one side and on the other, will
grow all kinds of trees for food. Their leaves will not wither,
and their fruit will not fail. They will bear every month because
their water flows from the sanctuary, and their fruit will be for
food and their leaves for healing (Ezekiel 47:12; cf. Revelation
22:9; Psalm 1:3).

In terms of Biblical symbolism these trees are the saints, fed by
the spiritual waters. of the sanctuary, and healing the nations of
the world by the Gospel.

Genesis 2 says that trees provide beauty; and after the fall,
part of that beauty is shade from the burning sun. The Bible
refers to this in several striking symbolic passages. In Ezckiel
17:23, Israel is pictured as a great tree whose shady ministry at-
tracts. the birds of the nations:

On the high mountain of Isracl I shall plant it, that it may
bring forth boughs and bear fruit, and become a stately cedar.
And birds of every wing will dwell under it; they will dwell in
the shade of its branches.

The cedar here is Israel, because the Temple on Mount Moriah
was made of cedar wood (1 Kings 6). The birds are the nations.
Jesus repeated this parable in Matthew 13:31-32, but spoke of a
mustard tree, showing that the kingdom was going to shift from
the Old Covenant cedar to a new planting (cf. Mark 4:30-32;
Luke 13:18-19).
