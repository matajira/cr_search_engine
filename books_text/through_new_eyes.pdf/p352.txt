340

Kimball, William R., 295

Kline, Meredith G., 43, 45, 170, 172,
205, 216, 293-307 passim, 318

Kuhn, Thomas §., 291, 318

Latourette, Kenncth §., 313
Lecerf, Auguste, 108, 298, 318
Lee, Francis Nigel, 301, 306, 319
Leithart, Peter, 313

Lewis, C. S., 301, 302, 319
Lovejoy, Arthur O., 291, 319
Luther, Martin, 18, 292, 304

  

Martin, Emest L.., 295, 296
McClintock, John, 301, 308
Mendenhall, George E., 13, 291, 299
Meyers, Carol F., 296

Moore, Joan André, 295

Murphy, Roland E., 299

Murray, Iain, 313

Murray, John, 311

Nereparampil, Lucius, 224, 307, 308
Neusner, Jacob, 306

Newton, John, 40, 68

North, Gary, 291, 299, 300, 302, 310, 319

Olivers, Thomas, 180, 196, 220, 240,
258, 303

Ollenburger, Ben C:, 292

Owen, John, 302

Parmcelee, Alice, 296

Petersen, David L., 310

Philo of Alexandria, 50, 294

Pink, Arthur, 268, 311, 319

Pinney, Roy, 297

Pocock, D. F., 301

Poole, Matthew, 310

Poythress, Vern ., 108, 298, 305, 306,
31, 319

Robertson, Q. Palmer,’ 299, 319

THRoucH New Eves

Rodriguez, Angel M., 306, 307

Rosenstock-Hucssy, Eugen von, 313,
315, 319

Rossi, Vincent, 3, 291

Rushdoony, Rousas J., 291, 292, 305,
319

Sauer, Erich, 306
Schilder, Klaas, 301, 319

Schlossberg, Herbert, 51, 294, 319
Schmemann, Alexander, 288, 292, 320
Schochet, Elijah Judah, 96, 296, 297
Seiss, Joseph A., 295

Silva, Moisés, 294, 320

Spencer, Duane E., 295
Shewell-Cooper, W. E., 296

Stone, $, J., 282

Strong, James, 301, 308

Sutton, Ray R., 299, 320

Taylor, Paul S., 297
‘Terry, Mitton S., 320

Vandervelde, George, 139, 300

Van Til, Cornelius, 24, 288, 292, 320
Van Til, Henry, 118, 298, 301, 320
Van't Veer, M. B., 312

Venantius Fortunatus, 80

Weiser, Francis X., 296
Wenham, Gordon J., 294, 320
Whitcomb, John G., 291, 320
Williams, William, 166

Wines, E. C., 308

Wolfe, Tom, 293

Wolff, Hans Walter, 307, 320
Woodrow, Ralph, 295, 310, 320
Woolley, Paul, 320

Wright, David P., 302
Wright, George E., 301
Weight, Ruth V., 296

Young, Edward J., 291
