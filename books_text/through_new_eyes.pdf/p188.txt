176 Turoucu New Eyes

men who dominated the whole carth; while after the Flood, the
political heavens were the priest-kings of the seventy separated
nations. Some of the priest-kings were. godly, and some were un-
godly; but whatever the case, we have moved into a new heav-
ens and carth, with nations and city-states. This would continue
to be the situation in the world at large until the time of Israel's
exile, when we move into the world-imperial stage of history.
(See Diagram 13.2.) For as long as Israel remained a nation,
however, the Gentile world was also organized into nations with
city-state capitals.

 

 

 

Diagram 13.2
Polities
Pre-Flood World NoahicWorld — f Exilic World
Heavens Mighty Cainite men Priest-kings World emperors
who dominate: who rule: (Daniel 2, 7)
Earth aone-worldstate; many separate | many nations gathered
all people nations under one ruler

 

 

 

 

 

 

Third, as just noted, the name of God in use among the na-
tions was “God Most High.” God Most High was regarded as
Possessor of heaven and earth, a name of international signifi-
cance. Melchizedek, priest of God Most High (Genesis 14:18),
blessed Abram with these words: “Blessed be Abram of God
Most High, Possessor of heaven and earth; and blessed be
God Most High, who has delivered your enemies into your
hand? (14:19-20). The name God gave to the Hebrew Patriarchs
was “God Almighty,” the God who has the ability to fulfill His
promises (Genesis 17:1; 28:3; 35:11; 48:3), Melchizedek, how-
ever, used the Gentile name. Note also what Abram said to the
king of Sodom: “I have sworn to the Lord, God Most High,
Possessor of heaven and earth, that I will not take a thread or a
sandal thong or anything that is yours” (14:22-23), Abram iden-
tified his own God, the Lord, with the one known among the
Gentiles as God Most High, Possessor of heaven. and carth.
