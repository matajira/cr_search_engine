124 THroucH New Eves

The progressive revelation and glorification of God in history
does not take place by a process of unveiling what is hidden, but
by transforming what is already revealed. This is the mystery of
time, of growth, of history. It means something amazing, how-
ever: that even in the simplest of human actions, God's glory can
be enhanced and His Person revealed more fully.

This sccond aspect also gives perspective to the transitory
nature of human works, The great paintings of the Reformation
era are darkening and cracking with age. Many have been de-
stroyed in wars. Of Bach’s five great Passions, only two are ex-
tant, All our works are like castles of sand. Thus, it is sometimes
argued that human work in the creation has meaning only in
that it trains men: Adam himself is progressively transformed
and glorified through the six-fold action. While this touches an
important truth, the problem is with the word “only.” By itself,
the notion that human labor exists only to train men reduces the
value of work only to the subjective dimension. The objective
foundation needed is the confession that human labor, if it is ul-
timately worthwhile, progressively reveals and glorifies God.
Even if the artifact does not itself endure, like the crude sketches
of a child, the revelation of God and glorification of the creation
is cumulative.

Corruption and Restoration

Unfortunately, this process of glorification was corrupted.
The sin of Adam lay precisely at the second step of his rite. He
refused to give thanks to God, because he could not do so. With
the forbidden fruit in his hand, Adam could not give praise to
God. Thus, Adam’s original sin entailed (among other dimen-
sions) the failure to glorify God as God (by restructuring the cre-
ation along His desired lines), and the failure to give thanks (by
expressing dependence upon God and gratitude for what God
had given him), Thus, the six-fold action designed for man’s
good was corrupted. In Gain we see this fleshed out.

1, Cain laid hold of the creation to restructure it into the city
of man.

2. Cain did not give thanks, or express dependence and
gratitude, to God or to anyone else.
