The New World 273

Diagram 18.1
The New Jerusalem

   
   
 
   

Heaven,

Church

(firmament)

New Jerusalem

 

   

Mountain

i sig (World) es

  
    

Gehenna

wa

History

The coming of the new creation was in three phases. The
first phase was in Jesus Christ alone. During the years of His
earthly ministry, He was the Kingdom. His Disciples followed
Him, and experienced a foretaste of His Kingdom; but before
Pentecost, the Kingdom did not come to them. Only then were
they clothed with power from.on high.

The new Kingdom could not be-envisioned by the Disciples.
They and the rest of the Jews believed that Jesus would simply
restore the glories of the Davidic monarchy in an imperial form.
This was a logical vision for them to hold, in terms of the devel-
opment of history, but it was an error. Just as the Hebrews in
Egypt could not have envisioned the Tabernacle, and just as the
Israelites of Samuel’s day could not have envisioned the Temple,
so the Jews of Jesus’ day could not have envisioned the New
