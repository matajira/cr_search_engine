The World as God's House 43

minion of darkness and that of the Spirit here on the first day,
this is not a conflict between evil and good. Darkness was not an
environment of evil, nor a symbol of sin, at this stage, Rather
the idea is that the Spirit was beginning the work of glorifying
the creation.

The hovering Spirit manifested the presence of the Triune
God in creation. It is the Spirit who proceeds eternally from the
Father and also from the Son (in two different ways, according
to the properties of each spirating Person). Once the world has
been created, it is the Spirit who proceeds out of eternity into
time, and makes manifest the presence of the other two Persons.
The Father and the Word send the Spirit into the creation as ini-
tial Light-bearer.

Genesis 1:3 records the creation of the cloud of glory, also
called Shekinah Glory: “Let there be light.” Light implies trans-
mission from some source, with the possibility of shadowing.
Moreover, the light was not constant, but alternated with darkness
for three days before the creation of the sun. Thus, there had to
be some local source of this created light, and it was not.the sun.

As Meredith G. Kline has shown at length, the visible mani-
festation of God’s throne-environment in the creation is always
the work of the Spirit. Thus the hovering Spirit in Genesis 1:2
corresponds to the hovering cloud-chariot of God elsewhere in
the Bible, as in Deuteronomy 32;10-11, where the glory of God
hovered over Israel in the wilderness.5 Until God spoke from
His throne and said, “Let there be light,” there was no visible
manifestation of glory in connection with the Spirit.

When God’s glory-cloud appears later in the Bible, we find
that it consists of such basic heavenly phenomena as light,
clouds, lightning, thunder, blue sky, and the like. Here in Gen-
esis 1:2-3 is the explanation of this. God first created heaven, and
then sent His Spirit to hover over the earth. Proceeding from
heaven, the Spirit brought the heavenly pattern into the cosmos.
With the creation of light, the Spirit manifested God’s presence
as a cloud of glory. In the design of God, this glory was repro-
duced in the firmament heavens made on the second day, and
then was further reproduced in successive stages, on the.earth.
(See Diagram 4.1.) “The heavens declare the glory of God in the
special sense that they are a copy of the archetypal Glory of God.”6
