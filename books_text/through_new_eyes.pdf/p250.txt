238

Grant:

Promise:

Stipulations:

Sacramental:

Societal:

Polity:
Church:

State:

Symbol:

Turoucu New Eves

The city of Jerusalem, as capital of
the Kingdom.

God will not forsake the House of
David.

Slight changes in the worship sys-
tem reflecting the change from
Tabernacle to Temple. Also, certain
sacrifices were paid for by the King,
acting as Chief Layman (e.g., 2
Chronicles 35:7).

The new constitution of the King-
dom, in particular the rule that the
King hearken to the prophet.

Priests at Temple, prophets and
Levites at synagogues.

Elders and judges, with King at the
top.

The Palace/Temple complex.
