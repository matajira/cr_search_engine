Eden: The World of Transformation 159

While the altar in the Tabernacle did not have this shape, the
statement in Ezekiel clearly expresses the theology of the altar (see
Diagram 12.8). When God appeared on Mount Sinai, the top
was covered with fire and smoke (Exodus 19:18). We can hardly
fail to see the visual association of this with the burning sacrifices
on the bronze altar, and the incense on the golden altar. More-
over, altars for sacrifice were generally built on the tops of moun-
tains before the Tabernacle was set up (cf. Genesis 22:9), and
during the interregnum between the dissolution of the Taber-
nacle and the building of the Temple (cf. 1 Samuel 9:12). Thus,
the association of altar with holy mountain is fairly pervasive.

Diagram 12.8
The Altar of Ezekiel 43:13-17

The Hearth

 

The Mountain of God
{Garden—Sanctuary)’ | Gutter (v. 17)

 

 

 

 

 

 

 

 

 

(Holy Land)
(World) Gutter (v. 14)
u {Gehenna) Lt
Conclusion

First, with ali this information before us we can construct a
symbolic picture of the original world (see Diagram 12.9). Above
the earth was the firmament-heaven, a picture of the highest
heaven. Below the earth was the sea, a picture of the Abyss. Ris-
ing out of the center of the earth was the Holy Mountain, froma
which flowed four rivers to carry Spiritual influences to the four
corners of the earth. Stationed at the top of the pyramid was
man, God’s agent for world transformation. From this vantage
point, man could look up and see the heavenly blueprint, and
then come down the mountain to work with the earth, making it
like heaven.
