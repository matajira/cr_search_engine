200 Trroucn New Eves

What about all those sacrifices, you may ask? There were the
Burnt, Meal, Peace, Thank, Votive, Sin, Reparation, “Heave,”
and “Wave” Offerings, for starters. Some used salt, and some did
not. Some used oil, arid some did not. Some required a lamb;
others, oxen; others, birds. Leavened bread was used with
some, unleavened with others. Some parts of the animal were
burned up, others given to the priests, and others were eaten by
laymen. These things differed for each sacrifice. It was an awful
lot of detail to master. The Israelite citizen, however, never
offered any sacrifices himself. Only the priests were allowed to
do the sacrifices, and they did them every day. They soon be-
came familiar with all these details.

Compare the details of the complicated sacrificial system with
the details of auto repair, and it suddenly becomes clear just how
simple the priest’s job was. How many different kinds of cars are
there? Add on the fact that they change from year to year. Now
consider all the different parts and aspects that can go wrong.
Next time you take your car in, look at all the volumes of “Chil-
ton” auto repair manuals that your mechanic keeps on hand,
and compare their size and detail with the book of Leviticus. If
your mechanic can learn to fix cars, and enjoy it, obviously the
priests of Israel had no trouble managing the sacrificial system.

What about the sabbath? Wasn’t that a burden? No, it was a
time of rest. But weren't they forbidden to cook on the sabbath?
No, they kept the sabbath as a feast. But weren’t they forbidden
recreation on the sabbath? No, the Bible nowhere says this. Well
then, what did they do? They went to church to worship God at
the synagogue (Leviticus 23:3), and relaxed the rest of the day.
The sabbath was not an “impossible burden.”*

What about all those cleansing rules in Leviticus 11-15? Well,
in the first place, becoming unclean only meant one thing: You
were not permitted to go into the forecourt of the Tabernacle
and bring a sacrifice. Since most forms of uncleanness only
lasted a day or a week, it was no real burden to be unclean. Sec-
ond, if you were seriously unclean, you could make other people
unclean for a few hours (until sundown) if you touched them;
but again, that was only a matter of concern if the other person
were on his way to offer a sacrifice. At the most, being unclean
was an inconvenience.. Of course, if you were unclean for
