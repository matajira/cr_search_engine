 

It is the glory of God to conceal a matter, but the glory
of kings is to search out a matter.

— Proverbs 25:2

 
