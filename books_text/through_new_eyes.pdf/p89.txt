Racks, Gold, and Gems a7

sun, and His legs like pillars of fire,” The rainbow around His
head means that Christ looks through it to see the world, always
reminded of the Noahic covenant.

The rainbow encircles God’s throne, but so, too, do the gemi-
stones. In Revelation 21 we have a vision of the New Jerusalem,
the city built around God’s throne (Revelation 22:1). Jerusalem
is an architectural replica of God's glory-home:

The foundation stones of the city wall were adorned with every
kind of precious stone. The first foundation stone was jasper;
the second, sapphire; the third, chalcedony; the fourth,
emerald; the fifth, sardonyx; the sixth, sardius; the seventh,
chrysolite; the eighth, beryl; the ninth, topaz; the tenth,
chrysophrase; the eleventh, jacinth; the twelfth, amethyst.?
And the twelve gates were twelve pearls; each one of the gates
was a single pearl.

In other words, the city was encircled with gemstones: stones of
the land for Israel, and pearls from the sea for Gentiles.* These
foundation stones have already been associated with the names
of the Apostles (Revelation 21:14), just as the High Priest's
twelve stones had written on them the twelve tribes of Israel.
‘Thus, these stones represent people. (See also Isaiah 54:11-12.)

We are God's house of gemstones. The righteous people in
the Church are likened to gemstones by Paul in a famous tem-
ple passage:

According to the grace of God which was given to me, as a wise
masterbuilder I jaid a foundation, and another is building
upon it. But let each man be careful how he builds upon it. For
no man can lay a foundation other than the one which is laid,
which is Jesus Christ. Now if any man builds upon the founda-
tion with gold, silver, precious stones, wood, hay, straw, each
man’s work will become evident; for the day will show it, be-
cause it is revealed with fire (1 Corinthians 3:10-13a).

Paul is writing to pastors, who will continue building the house
whose foundation he has already laid. Paul says that the Church
is built up of saints, who are like gold, silver, and gemstones,
but also includes Satan’s agents, who are like wood, hay, and
stubble. God puts the wicked into the Church so that they may
