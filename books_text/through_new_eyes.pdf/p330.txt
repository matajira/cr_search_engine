38 Turoucn New Eyes

. Judges: God's War Against Humanism. Tyler, TX:
Geneva Ministries, 1985.

—_____.. The Law of the Covenant: An Exposition of Exodus 21-23.
Tyler, TX: Institute for Christian Economics, 1984. Out of print.
Photocopies available from Biblical Horizons, PO. Box 132011,
Tyler, TX 75713.

—_____. Primeval Saints: Studies in the Patriarchs of Genesis.
Biblical Horizons, P.O. Box 132011, Tyler, TX 75713.

—_____, editor. The Reconstruction of the Church. Christianity
and Civilization 4. Tyler, TX: Geneva Ministries, 1985.

. Sabbath Breaking and the Death Penalty: A Theological In-
vestigation, Tyler, TX: Geneva Ministries, 1986.

—______. The Sociology of the Church. Tyler, TX: Geneva
Ministries, 1986.

——____. “Thoughts on Jachin and Boaz.” Biblical Horizons,
P.O, Box 132011, Tyler, TX 75713.

—_______.. Food and Faith: The Mosaic Dietary Laws in New Cove-
nant Perspective. Biblical Horizons, P.O, Box 132011, Tyler, TX
75713.

Keel, Othmar, The Symbolism of the Biblical World: Ancient Near Eastern
teonography and the Book of Psalms. Translated by Timothy J.
Hallett. New York: Seabury Press, 1978. Liberal perspective, but
valuable, highly illustrated study.

Kik, J. Marcellus, An Eschatology of Victory. Phillipsburg, NJ: Presby-
terian and Reformed Pub. Co., 1971. Evangelical study in
prophetic symbolism.

Kline, Meredith G. Images of the Spint, Grand Rapids: Baker, 1980.
Evangelical study in Biblical symbolism.

. Kingdom Prologue. 3 volumes. By the author (Gordon-
Conwell Theological Seminary, South Hamilton, MA), 1981,
1983, 1986. Evangelical study in Genesis, focussing on literary
structures and symbolism at many places.

. The Structure of Biblical Authority. Revised Edition.
Grand Rapids: Eerdmans, 1972. Evangelical study of literary
structures in the Bible.

Kuhn, Thomas S. The Structure of Scientific Revolutions. Second Edition.
University of Chicago Press, 1970.

Lecerf,.Auguste. An Introduction to Reformed Dogmatics. Translated by
André Schlemmer. Grand Rapids: Baker Book House, (1949)
1981, Evangelical.
