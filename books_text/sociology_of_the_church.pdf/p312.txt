298 The Sociology of the Church

clude those who must be chastised for sin, The sabbatical assem-
blies in the Old Testament are principally noted as the great solemn
feasts, and as certain great occasions in the history of the people
(e.g., the dedication of the Temple, the assembly of Jehoshaphat,
ihe crowning of Joash, the great gatherings under Hezckiah, and
the assembly under Nehemiah). Finally, it would seem from the
New ‘lestament (1 Cor. 11:10; Heb. 12:22f.) that the angels, though
a different people of God and though in a different organization
(edah), join in the sabbatical assembly (gakal) of the people of God.

Il is important to note that these distinctions between civil and

 

  

sabbatical organizations and assemblies arc not to be found in the
terms themselves. The Old Testament does not speak of “church
and state” as we do. Rather, one must examine in context what
“the organization” or “the assembly” is said to be doing in order to
know if a civil or sabbatical activity is intended.

The Old Testament usage of dah and gahal is primarily sab-
on for this is not far to seck. Man’s primary and
all-determining relationship in life is his God-ward relationship.
The severing of that relationship in the Fall was the wellspring of
all other disabilities that have come upon the human race, The
restoration of that relationship, and its maintenance, is thus of
central and all-determining importance for the life of man, Thus,
although the people of God function in all areas of life, the civil in-
cluded, it remains that there is a primacy to the sabbatical that

     

is determinative of all else.

Tt remains only to note that the New ‘Testament notion of
“church” — ekklesia ~ involves ail of these Old Testament ideas. The
notions of the church as the people of Ged in the broad sense and
as the general ¢dah or community of saints arc very close, and in
certain passages a clear distinction cannot be drawn.! (Indeed,

 

1. In fact, let me stress here that f am not claiming a sharp distinction between
‘dah and gakal in the Hebrew. I do believe, however, that the evidence warrants
seeing a general or vague distinction in meaning, Some terms are very precise in
meaning (¢.g., “propitiation”) and some are very vague (e.g., driving “preity
fast”), Languages need both vague and precise terms, and terms in between. It is
easy to make a mistake by trying to read too much precision into a relatively
vague term. All | am claiming is that ¢dak generally focuses on government and
organization, while gakal generally focuses on an actually assembled group.

  

 
