218 The Sociology of the Church

ment, we come up initially with a “church in the round” design,
Since we don’t want people staring at cach other in worship, gen-
erally the church in the round is not a complete circle, The cross
shape casily lends itself to this, particularly if we modify it by ex-
panding the center into a square or circle, We thus retain the
gathering of people around the Word and Sacraments, which is
both practical and theologically satisfying.

VEY

CHURCH IN THE
ROUND

ee

MODIFIED MODIFIED
CRUCTRORM CRUCIFORM

   

Posture and Gesture

T mentioned the act of crossing oneself above. I am not neces-
sarily recommending this, but I am saying that there is no reason
to reject it out of hand. Christianity does not separate the soul
from the body, but teaches the resurrection of the body, and
affirms that we worship God in the whole person, which includes
bodily movement, As Romano Guardini put it, “The man who is
moved by emotion will kneel, bow, clasp his hands or impose
them, stretch forth his arms, strike his breast, make an offering of
something, and so on. These elementary gestures are capable of
