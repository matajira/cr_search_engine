13
A LITURGY OF MALEDICTION

Malediction is the opposite of benediction, and means “curse.”
In the Book of Revelation, “seven stars” are seen in the right hand
of the enthroned Jesus Christ (1:16,17), and these are identified as
the “angels of the seven churches” (1:20). Since the letters are ad-
dressed to these angels (2:1,8,12,etc.), the angels clearly are rulers
in the church on earth. Probably each was the presiding elder
(later called “bishop”) for all the congregations in his city, accord-
ing to the pattern set out in Exodus 18:21.

This unmistakably sets up the theology of the Book of Revela-
tion. Sadly, the point is almost universally missed, and it is
assumed that the word “angel” as used in the remainder of the
book has reference only to heavenly, spiritual beings. We have to
remember, however, that the Christian church, particularly dur-
ing sacramental public worship, exists “in the heavenlies” (Eph.
2:6; Heb. 12:22-24). Thus, John’s being caught up to heaven on
the Lord’s Day (Rev. 1:10; 4:1) is at least analogous to the position
of the church during worship each Lord’s Day.

The Book of Revelation as a whole is organized as a worship
service, and is a model for us. Jesus taught us to pray, “Thy will
be done on earth as it is in heaven.” Thus, the heavenly model is
to be reproduced on earth. In light of this, the seven angels who
sound trumpets in Revelation 8 are to be connected with the pro-
clamations of the angels (officers) of the seven churches. Either
what is pictured is the actual work of church officers, or else it is
the heavenly model that earthly church officers are to emulate.
Either way, it is of immediate practical import for the church,

279
