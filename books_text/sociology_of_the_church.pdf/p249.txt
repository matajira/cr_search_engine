God's Hospitality and Holistic Evangelism 235

Covenant church is that found in Exodus 18, with the elders over
tens (houses), and fifties (local churches) and hundreds (the
churches in a city), etc. This seems, indeed, to have been the pat-
tern in the early church.

Early on, however, the church departed from this familistic
structure, The higher elders (over hundreds, thousands, myriads;
that is, bishops, archbishops, and patriarchs) were to function as
advisors and shepherds to the younger, lower ranks of elders. In the
event of a judicial case appealed to them, the elders would sit to-
gether as a court, for adjudication is a joint power, There would
be little legislation in the church, for the Bible was the legislation,
and there would be little administration, for the Spirit was the Ad-
ministrator. Soon, however, in naiveté perhaps, the church
adopted the imperial form of the Roman empire. Bishops became
monarchs, not shepherds. This is the imperial stage of the church,
and it continued down to the Reformation. These monarchs tended
to replace the Bible and Christ as the Law and King of the
church.

The Reformation broke with the imperial form and substi-
tuted the bureaucratic form of the church. Instead of familistic
elders over tens, the elders sat as bureaus, boards, and commit-
tees, ruling over the churches. Or else the pastor acted as dictator.
Instead of being courts of appeal, presbyteries and synods became
ruling bodies in a legislative and bureaucratic sense, again tend-
ing to replace Scripture with church laws.

This bureaucratic form of the church is thankfully dying now.
Churches are instinctively returning to cell groups, meeting in
homes of elders, and in small groups.

The bureaucratic form of the church turns rulers from foot-
washers into distant dictators, The result is that people do not
really know any of the elders, and suspicion abounds as to what
the elders are doing. This is aggravated when the board of elders
becomes close-mouthed and sccretive. The problem, however, is
in the structure. Rule in the church is to be by means of footwash-
ing (hospitality) as much as by giving orders (Mark 10:42-45;

8. See Mark 10:42-45; John 13:1-17.
