How Biblical Is Protestant Worship? 213

+

 

 

 

 

 

 

 

 

v

Neither of these diagrams is designed to show what an aerial
view of the pre-deluvian world might have looked like, Rather,
the diagrams are a symbolic form, showing the basic shape. Later
in Scripture, the same picture recurs, with water flowing from a
rock, or from the Temple, or the headwaters of the Jordan flowing
from the great rock at Caesarea Philippi, The theological con-
tinuity among all these pictures lies in their symbolic form.

Notice in the two diagrams how the fundamental cross or X
form produces a square, The square fills out the space that is fun-
damentally defined by the cross. The cross has a center, and it has
four extensions, which are either the corners of the square, or the
centers of the sides of the square, The Bible repeatedly uses this
fundamental shape to portray the kingdom of God.

At the center is the initial sanctuary. Adam and Eve would fol-
low the four rivers out, extending dominion along their lines, and
branching out to fill and cultivate the whole world. One of the
most common ways of portraying the cross as the center of the
world, with influences spreading everywhere, is the labyrinth
design. Here the four rivers of influence are shown “curving”
around the world in ever expanding squares, until the whole
(square) world is transformed.

>

 

 

«_—_
LABYRINTH
