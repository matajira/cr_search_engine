A Liturgy of Malediction 281

tory of the state was very temporary (less than three days). Thus,
the state cannot really threaten the church at all. All the same, for
the good of society, the church should resist the jurisdictional en-
croachments of the state.

Before the hearing, a special service was held to ask God to
confound the wicked efforts of the conspirators. After the reading
of several passages of Scripture (2 Chronicles 26:1-23; Acts 4:1-31;
Psalm 59), interspersed with the singing of several appropriate
psalms (Ps, 2, 72, 79, 80, 83, 94), the following form was used:

Presiding Elder: “Dearly Beloved, our Lord Jesus Christ
has assured us that His church is built upon Himself, the
Rock, and that the gates of hell shall never prevail against it.
To His church He has committed the keys of the kingdom of
heaven, saying ‘whatever you shall bind on earth shall have
been bound in heaven, and whatever you shall loose on
earth shall have been loosed in heaven,’

“In the Book of Revelation, the office-bearers of the
church are called the angels of the churches, and in the
eighth chapter of that Book, these office-bearers are shown
blowing the trumpets of the Word of God. As. the
office-bearers proclaim the Word of God, signified by these
trumpets, and as the people of God pray for salvation, sig-
nified by incense that ascends to heaven, God is faithful and
pours out fiery wrath upon His and their enemies on the
earth.

“Tonight we bring before you the names of,
who have attacked the church of Jesus Christ. We ask you to
join with us in praying that God will pour out His wrath
upon them, and upon all in alliance with them in this sinful
act. When I have prayed God to deal with them, I shal! ask
the other elders to join me in solemn Amen, and then I shall
ask the congregation to join with us in solemn Amen. Let us
pray.

(praying) “Almighty and Most Terrible God, Judge of all
men living and dead, we bring before you _____
(here name thc persons being cursed), who have brought an
