36 The Sociology of the Church

various stages, he will evaluate what he and others have done.
Finally, he is to consume or enjoy it.”

This is the Christian worldview, and it also proclaims the
death of Christ. Because of human sin, it was necessary for God
to lay hold on man, break and restructure him, and send him
back into the world. Only thus could God give man a positive
evaluation and enjoy him in common sabbath rest. This Christ
accomplished for us, Even though not a bone in His body was
broken, yet He experienced the curse of the covenant, which is to
be ripped in half and devoured by the lower creation. As in all the
Old Testament sacrifices, His blood was separated from His
flesh.° Thus, the bread is broken. Similarly, before Jesus gave the
cup to us, He drank it Himself, and this is explained as His death:
“Father, if it be possible, let this cup pass from Me.” Accordingly,
while the cucharist does not recrucify Christ, nor extend the ac-
tion of His death, it does “proclaim the Lord’s death until He
comes” (1 Cor. 11:26).

This proclamation is not only to men, but also to God. The
covenant memorials were given by God for man to use to remind
Him to keep the covenant. It is not as if God forgets and must be
reminded, but that for man’s own good God requires us to remind
Him. The proclamation is made to men, but unless men add their
“amen,” thus returning the proclamation to God, the proclama-
tion is not salvific. This amen-proclamation to God is almost cer-
tainly what is in view in 1 Cor, 11:26,3! Thus, the rainbow was es-

 

29. I have developed the “six-fold action” as a worldview in an essay, “Chris-
tian Piety: Deformed and Reformed.” The Geneva Papers (New Series) No. 1
(September, 1985); available for $4.00 from Geneva Ministries, Box 131300,
Tyler, TX 75713.

30. This is why the bread is eaten, and then the wine drunk, as two separate
actions.

31. Leon Morris asserts that katangeflo in 1 Cor. 11:26 can only refer to the
proclamation of the gospel to men. See The first Epistle of Paul to the Corinthians.
Tyndale New Testament Commentaries (Grand Rapids: Ecrdmans, 1958), p.
162. While | am reluctant to differ with so eminent a scholar, Morris's assertions
at this point betray the weakness of leaning too heavily on word studics to do
theology. By itself, katangello means “show, proclaim.” We have to look at context
or theology to determine to whom the proclamation is being made. In the light of
the Biblical theology of covenant and covenant memorials, it surely stands to rea~

  
  
