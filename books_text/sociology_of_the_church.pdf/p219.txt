Rethinking Worship: Some Observations 205

corporation into His transfigured body, on the other side of His
death as payment for our sins, that is our restoration. The em-
phasis must be on resurrection from the dead, not on incarnation
as such,

A third general difficulty with the current liturgical movement
is that there is sometimes not enough respect paid to the regula-
tive principle of worship. We are to worship God only in the ways
He has “commanded” us. As evangelicals come to appreciate their
catholic heritage, they need to assess that heritage from the stand-
point of the Bible. The Bible has things to say on such subjects as
the placement of visual images in the environment of worship, the
use of vestments, the use of incense, the value of an ecclesiastical
calendar, preferable forms of architecture, the use of musical in-
struments, and so forth. The essays in this section are devoted to
exegetical investigations of some of these matters.
