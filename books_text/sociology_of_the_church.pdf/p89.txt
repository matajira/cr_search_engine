Sociology: A Systematic Approach 5

visibly united, There is one Lord Jesus Christ, one audible Word
making Him visible, one pair of sacraments making Him visible,
and one conception of special officers making Him visible. More-
over, speaking of the church in general, we see her united against
sin, abortion, homosexuality, pornography, united in favor of the
family, family property, etc, There is organic visible unity to the
church insofar as Christians share a common witness against evil
and act for Christ. There is unity visible in the cooperation of the
churches, There is unity visible in local interrelationships among
churches, including common Eucharistic worship on occasion.
There is unity visible in the cross-fertilization of the parachurch
organizations. In view of all this, much of the grief expended over
the lack of unity in the church has been misplaced. It is due again
to a failure to distinguish the various aspects and dimensions of
the church.

The Father has never failed to answer the Son’s prayer in John
17, It is wrong to act as if He has. We must confess unity by faith
(not by sight), and on that basis work for greater visible manifes-
tations of it,

The question in the problem of denominationalism is the
question of shell unity. It is a genuine problem, because it pre-
vents the church from functioning in a proper way as a govern-
ment (since various denominations do not recognize one
another), and as a local ministry (since the churches in a location
will be of varying denominations). The proliferation of shell di-
versity is due to at least three factors. First, with the Reformation
there was a great burst of understanding with respect to the Scrip-
ture. God and His Word being infinite, and man being finite, it is
understandable that different men would see and appreciate
different aspects of Christianity. This diversity would not have
sparked disunity except that, being sinners, men tend to fight over
their differences in perception. If this were the only factor in-
volved in shell diversity, we might call for a council and synthesize
all the apprehended truths.

Second, communication is an important factor in shell disunity.
Differing theological traditions often use words or key phrases in
slightly different ways. Skill and effort is needed, as well as a gra-
