How Biblical Is Protestant Worship? 2u1

years. That is, the Bible not only teaches us what is essential (esse)
for the worship of the church, but what is good for her well being
(bene esse) and her full being (plene esse). Second point: Conser-
vative Protestantism has generally rejected the opportunity to
grow and develop Biblically, in spite of loud affirmations of Bibli-
cal rigorism. I maintain Biblical rigorism as my position, but Bib-
lical rigorism is not the same as cultic minimalism (which is influ-
enced by Western stoic philosophy, and unfortunately is all too
often the posture of Reformed and Anabaptist worship).

The Act of Crossing Oneself

Throughout all the centuries of the Christian church, the cross
has been a prominent symbol of the faith. It is probably the most
prominent “mere symbol” in the church, once we have excluded
the sacramental signs of Holy Baptism and Holy Communion.
The cross has been used in three distinct but interrelated ways: as
an architectural design, as a symbol, and as an action. Churches
were built in a cruciform shape. A cross was put on the front wall
and on the steeple. People crossed themselves to invoke the pro-
tection of the covenant God.

The Reformers did not object to the act of crossing oneself,
provided it was not done superstitiously. They recognized that it
might simply be an external bodily action that accompanies an
inward prayer for protection. To be under the sign of the cross is
to be under the blood of Christ, under the protection of His
wings. A man in distress might pray, “Lord Jesus Christ, protect
me from harm, for I am Your child, under Your protection,” and
he might cross himself as an external physical act while he thus
prays.5

This may make little sense to a modern man, however. Under
the influence of Greek philosophy, primarily Stoic asceticism and

3. Thus Martin Bucer, “This sign [of the cross] was not only used in the
churches in very ancient times: it is still an admirably simple reminder of the
cross of Christ.” Bucer writes with respect to making the sign of the cross as part
of the rite of holy baptism. In Bucer, of. cit., p. 90.
