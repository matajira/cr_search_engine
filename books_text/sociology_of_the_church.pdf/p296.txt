282 The Sociology of the Church

attack upon the integrity of Your holy government on the
earth. We as Your anointed office-bearers now ask that You
place Your especial curse upon these people, and upon all in
alliance with them, We ask You to pour out the fire of Your
wrath upon them, and destroy them, that Your church may
be left in peace, and our time free to pursue the advance-
ment of Your Kingdom. We ask that You visibly and swiftly
vindicate the government of Your only Son, Jesus Christ our
Lord, Whe lives and reigns with You and the Holy Spirit,
ever one God, world without end. Amen.

(addressing the other elders) “Elders —__.___
(here say the names of the elders), do you join with me in in-
voking the wrath of God upon these people? If so, answer
Amen.”

Elders: “Amen.”

Presiding Elder; “Congregation of the Lord Jesus Christ,
do you join with us in asking God to visit His wrath upon
these people? If so, answer Amen.”

Congregation: “Amen.”

The next day, when the hearing was held, representatives of
the church attended, explaining that the church was not under the
Jurisdiction of the Texas Employment Commission, and that our
appearance should be regarded as a “special appearance,” and a
courtesy. The Texas Employment Commission agreed, and stated
that the church is not under its jurisdiction, The enemies of the

church were put to confusion.
We dare not expect that every situation will work out as easily,

but we should confidently ask our God to fight for us in these and

like battles.
