208 The Sociology of the Church

tants will take no notice at all of her. If Roman Catholics chant the
psalms in precise translations, Protestants will either versify the
psalms (losing much in the process), or stop singing psalms alto-
gether.’ If Roman Catholics keep Christ nailed to the cross, Prot-
estants will war against the cross as a symbol altogether. If Roman
Catholics have processions and street dancing, Protestants will re-
ject all bodily expression in worship. If Roman Catholics ritually
sanctify wedding rings during the “sacrament of matrimony,” then
Protestants (the Puritans and Anabaptists at least) will say it is
sinful to wear a wedding band at all. And so it goes.

This is not Biblical fasting from an abuse. It is a perversion.
Instead of getting their theology and practice from Scripture,
Protestants have too often gotten them from reacting against
Rome. As a result, many of the things that the Bible teaches
about true worship have been lost in many Protestant churches.

The Regulative Principle

The Reformers taught that “nothing should be introduced or
performed in the churches of Christ for which no probable reason
can be given from the Word of God.”? The general rule on this is
that we must have Biblical warrant for what we do in special wor-
ship—warrant consisting of principle, precept, or example.
Rather rapidly, however, this sound and salutary principle was re-
duced to the slogan “whatever is not commanded is forbidden,” a
simplistic formula that is a long way from the principles of the
protestant Reformers. There is a lot of difference between Bucer’s
“probable reason” and “commanded.”

This simplistic version of the regulative principle is hard to ap-
ply. First of all, no one is able to apply it without modifying it,
because we find no Biblical command for church buildings, pews,
etc. Second, in its simplistic form the principle is almost always

1. Ofcourse, singing versified psalms in the vernacular is a vast improvement
over listening to them sung in incomprehensible Latin.

2. Martin Bucer, Censura, trans. by E. C. Whitaker in Martin Bucer and the Book
of Common Prayer. Alcuin Club Collections No. 55 (London: SPCK, 1974), p. 42f
