64 The Sociology of the Church

is more interested in the “Church at Tyler, Texas” than He is in
the “Association of Reformation Churches.” The local church of
which I am a member has ties in both directions, but our primary
concern should be with the former. Once we get rid of the Greek
notion of the primary of the intellect (and thus of dogma), we can
see clearly that the Bible teaches the importance of local connec-
tions over ideological (denominational) ones.

Schism and the “Visible” Church

The distinction between the Church Visible and the Church
Invisible has a long, confusing, and often unhelpful history. The
Bible does not speak in these terms, and this has rendered the dis-
cussion highly problematic. The distinction is used to safeguard
some important truths, but the question may justly be put as to
whether these same truths might be better safeguarded in some
other way.

Our concern is not directly with the Church Invisible, how-
ever conceived, Rather, we are concerned with the nature of the
visibility of the church. How does what is visible te God become
visible to man? Earlier in this essay we discussed three forms of
visibility corresponding to the three aspects of the church: moral
and dominical visibility (people of God), gathered visibility, and
institutional visibility (sacraments, officers, buildings, political in-
fluence, etc.}.

We must now ask the question: What is the mainspring of
visibility? It is common for people to think that the church's
source of visibility is historical institutionalization. In this view,
the church is a visible institutional empire or bureaucracy that
flows down through history from the time of Christ, or Abel, to
the present. Christ is unaltcrably committed to this institution.
He died for it, He must always revive it. Outside of it there is no
ordinary possibility of salvation. ‘To disrupt it is the awful sin of
schism, The extreme view associates salvation simply with incor-
poration into the institution. The mild view holds that it is sinful
for a local church to leave the institution. Ifa separation occurs, il
is “they” who left the true institution, and it is “we” who preserve

 

 
