132 The Sociology of the Church

3. Itis a fact that the church is brought into being by the work
of the Spirit, who proceeds from eternity into time at every mo-
ment of time. Thus, it is not historical succession that matters,
but Spiritual procession. This explains how churches with a long
history can apostatize, and new churches arise. Ultimately it is
faithfulness, not history, that counts. The Bible tells us that the
new birth is not by blood (historical succession) but by the will of
God (the Spirit). Now, because God’s plan involves historical
maturation, historical succession has a real, though secondary,
importance. We baptize our children, confessing that they are
outside the Kingdom by birth, but that Ged is placing them into
the Kingdom by baptism. This creates an apparent historical suc-
cession, but a succession in which there are a continual series of
complete breaks. Each new generation is born dead in trespasses
and sins. Each new generation must be brought into the King-
dom, by baptism initially and by perseverance thereafter. Thus,
succession is only a visible effect, not a cause. Spiritual procession
is the cause.

Claims of apostolic succession by themselves, then, are not
only meaningless, they can easily become idolatrous, substituting
temporal continuity for the discontinuous new-creating work of
the Spirit, According to the Creed, only the Spirit is the “Lord and
Giver of life.”

Thus, we should not be surprised when it turns out to be rela-
tively new churches that are the true heirs of the wealth of the
past. It is what we should expect, when we realize that our God
“makes all things new.”!?

 

 

 

he goes to another church, and the new church gleefully rejoices in the apportun-
ity to shaft her sister and takes the man in without making any investigation
whatsoever.

12, It might be objected thar the analogy between the needed new birth of each
new generation of men and the “making new” of the church is a questionable
analogy. In fact, though, there is no such thing as “church” apart from God and
people. The word “church” refers to people under the aspect of a certain quality.
“Thus, the measure of a given church is in terms of what its people are, as in Rev-
elation 2 and 3. If the people are not born again, the church is dead, and Christ
has departed. Thus, the principle of historical discontinuity (death and resurrec-
tion) applies both to individuals and to churches, as 1 have set it forth.
