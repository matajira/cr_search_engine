God's Hospitality and Holistic Evangelism 247

and Who gives life. The Spirit came to be with the manna and
water in the wilderness, with the Passover meal, with other Old
Covenant meals, and He comes to be with the Lord’s Supper today.

What this means is clear enough. The children ate the manna
and drank the water. Indeed, there was nothing else to eat or
drink. The passage in 1 Corinthians 10:1-5 associates this with
baptism: all those baptized in the Old Covenant were entitled to
eat the Lord’s Supper (Note that it does not say that all, including
children, were circumcised in the Red Sea crossing, but that they
were baptized. This is a proof text for infant baptism.) This does
not mean that all were saved, for “with most of them God was not
well pleased; for they were laid low in the wilderness.” Those who
were initially included in the historical administration of the cove-
nant by baptism did not all persevere in faith so as to attain to the
eschatological fulfillment of the covenant. At any rate, we can see
that the Lord has invited the children to His table; do we dare to
turn them away, as the disciples did, and received Christ’s rebuke
(Matt. 19:13-15)?

Slaves, including those not personally converted, also ate the
Passover in the Old Covenant. All purchased slaves were circum-
cised when they became part of the master’s household, according
to the express command of God, (Ex, 12:44; Gen. 17:12f). The act
of circumcision made the slave into a covenant member, in the
same class as the “native of the land” or Israelite (Ex. 12:48; Lev.
15:29), able to partake of the Passover, which no foreigner could
partake of (Ex. 12:43-45).

A newly purchased slave would net even know the Hebrew
language, let alone be inwardly converted, It would take time to
teach him Hebrew, and then to explain the covenant of God to
him. Notice, however, that the slave was circumcised in ignor-
ance, and admitted to the Lord’s Table in ignorance.

This scems strange to modern Americans because of the in-
fluence of individualism. The Bible however, is covenantal, not
individualistic. The household is included in the decision of the
covenant head, and it is only as the members of the household
mature that they are expected to continue in the covenant on their
own. Under the influence of humanistic individualism, however,
