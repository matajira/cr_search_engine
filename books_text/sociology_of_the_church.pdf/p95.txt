Sociology: A Systematic Approach 81

the sacramenta] sanctuary. The determination of what to do with
tithe money is to be made by the special officers, not by the person
paying the tithe, Gifts above the tithe may lawfully be given to
parachurch organizations, and local churches may choose to give
part of the tithe they receive over to parachurch groups.

It is best to play the parachurch tune by ear. In the area of
Bible translation, Wycliffe has done a good job over the years.
(Questions might be raised nowadays, unfortunately, because of
the rise of “dynamic equivalence.”)” There is really no crying
need for each denomination to have its own Translation Society,
Missionaries can be trained by Wycliffe and sent out by the
churches, if desired. The same can be said of other groups.

The major advantage that accrues to the church from the mul-
tiplicity of diaconal organizations is this: They tend to break down
artificial barriers and work for cross-pollenization and unity
among the churches. If our long term goal is the unification of the
whole institutional shell of the church, it is to our advantage to
work with existing parachurch and diaconal organizations, and
add them to the melting pot, rather than set up new ones.

In summary, my point is this: Given the Biblical understand-
ing of the church as the people of God, there is nothing wrong in
principle with parachurch organizations. The independence of
these organizations is a problem no different from that of denomi-
nationalism. The only serious issue in the problem of parachurch
organizations is whether the organization in view is doing an
effective, Biblical job or not. Hf the matter were debated on this
material ground, rather than on formal abstract grounds, more
would be accomplished. If some parachurch group is inadequate,
it is not because it happens to exist at all, but because its theology
and practice are truncated. Independent mission boards should
be examined in the same way.

Conclusion

The main point of this essay has been to examine the notion of
the institutional church. The Reformation started out assuming

22, On “dynamic equivalence,” see Jakob van Bruggen, The Future of the Bible
(Nashville, Thomas Nelson, 1978).
