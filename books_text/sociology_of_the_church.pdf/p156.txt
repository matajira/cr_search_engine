142 The Sociology of the Church

Thus, the “international” catholic church began indeed to take
upon itself the trappings of a civil empire. And at the same time
“the secular state itself— starting, as it were, from the opposite end
—strove for its own exaltation and quasi-religious glorification.”¢
Thus, imperial thinkers began to speak of the corpus retpublicae mys-
ticum, the “mystical body of the commonweal.” This mystical body
of the state was gathered into the person of the King, just as the
(political) mystical body of the church was gathered into the per-
son of the Pope.

The result, at the end of the Middle Ages, was that there were
two statist orders in competition with one another. There was the
largely statist order of the Papal Monarchy, which also controlled
the church catholic, and there were the statist orders of the kings
of Northern Europe, who claimed religious prerogatives, Into this
mess came the Protestant Reformation.

The Reformation

Luther provided a convenient way for the princes of Germany
to do what they had always wanted to do: take over the visible
power of the church. Luther so stressed the personal and charis-
matic aspect of the gospel, over against the institutional side, that
his movement fitted nicely with the designs of the princes. At the
same time, from a political point of view, Luther and his followers
needed the help of “godly princes” in order to protect them from
Papal threats.

Conflicts in Germany over the reformation eventually led to
the formulation cutus regio, etus religio: whoever reigns, his religion.
The faith of a given region would be determined by the religion of
the ruling prince. At this point, Lutheranism in Germany had
become pretty much wholly statist in character, in terms of any
real independent power for the church. Lutheran acquiescence in
the power of the state has continued to be a problem for Christian-
ity in Germany down to the present day, and accounts for the pas-
sivity of the Lutheran churches in the face of Nazism.

6. Bbid., p. 207.
