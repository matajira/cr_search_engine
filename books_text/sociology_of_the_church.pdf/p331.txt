General Index

Envy, 19
Episcopalianismn, 15ff.

Esau, 98

Esse of worship, 211

Euchatist (sce Lord’s Supper)
Evangelism, 250ff,

Excellence, promoted, 16.
Exclusion, principle of, 1014
Excommunication, 131n., 147, 194, 222

 

Falwell, Jerry, 181.

Familism, 187f., 2304.

Fasting from abuses, 207, 271

Five, meaning for army and
architecture, 215f.

Flesh, 96, 114

Foctal communion, 249

Food
sign of communion, 237f.
worship and, 31, 38

Football religion, 229

Formality, 202

Four Spiritual Laws, 137ff.

Fourfold action (see Sixtold action)

Frederick II, 182

Fringe, 126ff.

Garb (see Clothing)

Gate of city, 39, 110

Genevan gown, 267f., 271

Gentiles, saved in Old Covenant (see
Annotated Table of Gontents,
chapter 3), 303f,

Gesture, 218¢f

Glorification, 87

Glossolalia, 170f.

Gnosticism, 38, 127

Graham, Billy, 154

Gregory VI, Pope, 139

Grisbrooke, W. Jardine, 266

Guardini, Romano, 208f.

Hands uplifted, 219
Havilah, 86, 87f., 92, 93, 101, 109

317

Healing, 283ff.

Healing services, 290ff.

Health, wue, 2886.

Heave Offering, 268f.

Heaven as model for earth, 85f

Heidelberg Catechism, 144

Henry IV, Holy Roman Emperor, 139

Henry VUI, England, 143

Hierarchy, principle, 17.
elders arranged in, 42f., 62f., 231f.,

235, 2551.

History, “Waves” of Church History,
58ff.

Hodge, Charles, 148

Hooper, John, 267f.

Hospitality, 221f., 2508.

House church, 236

Houschold gods, 252

Hymnody, 225

Incamationalism, 2031f.
Incorporation, 1877
Individualism, 234, 285
Institutionality, 67f.
Invitation, 23687.

Image of God, 4Iff.
Irrationalism, 171ff.

Isaac, 94, 978,

Ishmael, 938.

Israel and the nations, 838!

Jacob, 98
Jerusalem, destruction of, 12
Jesus, exemplified both catholicity and
integrity, 15
Jethro, 99, 1008
Jews, 1751f.
Ashkenazik, 177
Joseph, 988.
Jubilee, 102
Judgment, Divine, 10
Jurisdiction, church and state, 192
Justinian, 4, 191
