Rethinking Worship: Some Observations 203

Americans tend to try to make everything informal, and then find
formality awkward. In fact, God is both Abba, Father, and also
the God of Hosts, the King of Kings. There are times to approach
Him informally, as a child crawls into its father’s lap; and there
are times to approach Him formally, making a public presenta-
tion before the King. A healthy church life requires a balance of
both approaches, Americans have no trouble calling each other by
their first names; rather, we have trouble being formal. Other cul-
tures have the opposite problem.

A second general difficulty I find in some of the new evangeli-
cal appreciation of formal worship is the theological error of incar-
nationalism, It is assumed that the physical incarnation of Jesus
Christ is what validates the physical, earthly creation and its con-
sequent use in worship, This puts the cart before the horse. It is
the goodness of God’s original creation that made possible the in-
carnation, The same kind of error crops up more perniciously
when certain evangelicals speak of salvation. They write as if it is
our incorporation into Christ’s incarnation that saves us, and that
the church is a continuation of that incarnation, This is funda-
mentally wrong. Our salvation comes in the death and resurrec-
tion (transfiguration) of Christ. His incarnation was merely the
necessary prelude to His work. It is not the incarnation but the
resurrection that transforms men and the creation,

Incarnationalist language seems the rage in some quarters of
evangelicalism today, but it is very dangerous. Writing on the festi-
val of the nativity from within the Roman Catholic licurgical move-
ment, Father Louis Bouyer warns against identifying our new birth
with Christ's incarnation: “Nothing could be more foreign to the
whole teaching of St. Paul and St. John than such an idea of pattic-
ipation in Christ’s birth. Ic is on the Cross, in the Blood and water
flowing from His side, that the Church is bom of Christ . . . as Eve
was born of Adam. This is the teaching of St. Augustine and of all
the Fathers. ... To the mind of the Fathers, the Incarnation can
only be called redemptive in one very definite sense: in the sense
that it was an incarnation in a flesh which must undergo death, so
that the death of Adam should die in the death of Christ.”*!

11, Bouyer, Liturgical Piety, p. 201f. All italics his.
