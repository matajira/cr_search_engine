viii The Sociology of the Church

My parents were not particularly impressed with public school
education, and sent us to the parochial Roman Catholic school. T
attended grades 2 through 6. This was in the 1950s, and I can re-
call discussions in class over whether we “non-catholics” could
possibly be saved. he Sisters of the Sacred Heart were quite
faithful to then-current dogma, and firm on the point that no one
could be saved apart from the specific ordinances of the Roman
Catholic Church, Pre-Vatican If Romanism was my earliest ex-
perience with the sectarian mindset; unfortunately, it was not my
last.

My father would not have a television in our home when we
were younger, and it was not until I was in high school thal we got
one. Entertainment in our home centered around the phono-
graph, and my father had a large collection of church music:
Gregorian Chants, Eastcrn Orthodox services, passions and ora-
torios by Bach, sacred music by Couperin, Charpentier, Lully,
and Lalande, and so forth. While we kept a critical distance from

 

all these, we sought also to appreciate their contribution,

After we acquired a television, we used to watch Billy
Graham's Crusades when they were aired, and I was influenced
by his ministry, Just after graduating from high school, I read
Graham's World Aflame, and for the first time came to grasp clearly
justification by grace apart from works. After that, my interest in
our Lutheran church waned, because the current pastor was
rather liberal, though [ continued to direct the choir and in other

 

ways assist with the liturgy.

In college, I was active in a number of conservative political
organizations (Young Americans for Freedom, Intercollegiate
Studies Institute), and also in Campus Crusade for Christ, Dur-
ing my sophomore year, Francis Schaeffer's first books appeared,
and [ devoured them, } soon was listening to Schaeffer's tapes,
and was also moving on to the works of E. L. Hebden Taylor,
Herman Dooyeweerd, Cornelius Van Til, and Rousas John
Rushdoony. Since all these men were Reformed or Presbyterian,
1 soon became oriented in that direction, though as a good Gam-
pus Crusader, I did my studies from a New Scofield Reference
Bible.
