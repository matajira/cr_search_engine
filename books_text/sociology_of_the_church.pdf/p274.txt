260 The Sociology of the Church

Lewis Bulkeley, has referred to this as the “helmet haircut.” Each
time I go to a pastors conference of the Gothard Institute, I am
struck by the cloned appearance of the crowd, because virtually
all the men have “Yeppie” helmet haircuts, The hair is blow-dried
into a seemingly perfect mold.

Third, there is the conservative presbyterian minister’s garb.
This means a black or very dark blue suit, a white shirt, and a
conservalive necktie; rather like the kind of clothing worn by
lawyers.

Now, this is what people expect their pastors to dress like, if
not every day, at least in the pulpit — and it most certainly is dis-
tinctive clerical garb.

There are three aspects to these clerical costumes: high quality,

      

conservatism, and distinctiveness. First, people want their pastors
to dress well, whatever dressing well means to them. For people in
lower social and economic brackets, dressing well means dressing
loudly; for more upper class types, it means dressing in severe,
tasteful dark suits. In other words, people want to see their clergy
adorned in fine clothing.

The second aspect is no Jess important, though more subtle.
Clerical garb generally lags behind the latest styles. There seem to
be several reasons for this. One is that the leading clergy are
always older men, whose tastes were set in youth, and who are
thus naturally disposed to the forms and fashions of an earlier
time. After a number of gencrations, this conservative tendency
can result in a “clerical costume” that actually had been the public
fashion generations earlier.

A second reason for conservative garb is that most churches
keep their pastors poor, This is no myth; it is reality, and has been
for centuries. A result of it is that clergymen cannot afford a lot of
clothing, and so tend to avoid fashion and fad, going for clothes
that are less likely to go out of date. A black suit, white shirt, mod-
erate necktie — these never go out of style, because they are never
really in style; but white shoes, green suits, very wide or very nar-

 

row lapels, very wide or very narrow neckties, very wide or very
narrow belts, etc. — these do not last more than a few years before
they change. Lack of funds keeps clerical garb conservative.
