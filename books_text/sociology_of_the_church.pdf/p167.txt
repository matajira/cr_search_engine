Conversion 153

From my experience, and from my understanding of the Bible
and of Christianity, there are four kinds of conversion experi-
ences. First, for a person totally outside the faith, there is an ini-
tial conversion experience when that person comes to Christ for
the first time, This kind of conversion has become the norm for
everyone, unfortunately, even though it applies to relatively few
Christian people.

Secondly, there is daily conversion. Each day, and many times
during the day, we have to turn from sinful tendencies, and turn
back to Christ. These “little turnings” are so many daily conver-
sions. By magnifying the initial conversion experience, modern
evangelism does not say enough about daily conversion.

Third, there are what I call “crisis conversions.” There are
crisis points in every Christian’s life. At these crisis points, the
Christian needs to reaffirm his or her faith by making a major
break with some problem that has crept up, and make a major
turn toward Christ.

Fourth, there are what I called “stage conversions.” By this I
don’t mean conversions that are merely put on for show. Rather, I
mean that God brings Christians through various stages of growth
and maturity, and at each stage it is necessary for the Christian to
come to a fuller understanding of what it means to be a Christian.

Now, I don’t think enough justice is done to this matter of
stages of life. As a person grows, his understanding of himself, of
the world, and of God will change, because he is himself chang-
ing. His understanding grows wider, and embraces more factors
of life. He becomes aware of things he was not aware of before.
Moreover, his understanding grows deeper, and more profound.
Learning to adjust to a spouse, and then co children; learning to
adjust to authorities on the job, and learning how to relate to sub-
ordinates; learning how to manage money; etc.—aill of these
things cause a person to deepen and widen his understanding.
Hopefully, they cause a person to become more and more wise
and stable.

These changes of understanding happen slowly and gradually,
without our being aware of them. One day, however, we wake up
and realize that we have changed. I am not the same person I was
