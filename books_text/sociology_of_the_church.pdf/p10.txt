x The Sociology of the Church

ognize the sacramental/institutional church as having a certain
kind of preeminence in the kingdom.

‘The essays in this volume all in one way or another reflect my
background and struggles in this area. To paraphrase C. 8.
Lewis, I have been dragged “kicking and screaming” into a higher
view of the institutional church over the past decade. I feared that
to take a high view of the church would result in my taking a lower
view of the other aspects of Christ's Kingdom. I also feared that
adopting a high view of the institutional church would move me in
a sectarian direction, and cut me off from any real sympathy and
understanding for the present condition of American Christen-
dom. I hope that I have managed not to let either of these things
happen.

Americans in general do have a low view of the sacramental
church, and certainly tend to separate social action from liturgy.
As I have written elsewhere, “To say that the root of our problems
is religious is to say a great deal, but also to say rather little. . . .
If this confession only amounts to the notion that religious zdeas

 

 

underlie any given culture, then the affirmation is [not particu-
larly] radical. For to discuss religion only in terms of ideas or doc-
trine is to reduce religion to an ideclogy.” A true presuppositional-
ist will not fall into the trap of the “primacy of the intellect and
doctrine,” but will recognize that social renewal must flow from
the whole life of the Christian. “The practice of the Christian faith
is most concentrated in the activity of the church. This is for the
obvious reason that it is in the church that men devote themselves
most rigorously to the practice of the faith.”

A true Christian social theory, I have come to see, means rec-
ognizing that the whole life of the church constitutes the nursery
of the Kingdom of God. American Christians tend to isolate piety
and prayer to the individual realm, leaving the social realm to
political action. For there to be real reformation in our time, piety
and social action must be integrated, and the Biblical way to do
that is by a recovery of corporate worship and life, a recovery of the in-
suitutional church as a government and as a place of public worship.

3, fbid., pp. viirviii
