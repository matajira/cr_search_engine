Reconstructing the Church 25

stead of below them. Third would be at least one, preferably two
or three, paraphrases of the psalm in the form of rhythmic hymns.
The “Genevan jig” tune, with complete harmony and the words
printed between the staves for several stanzas, might be included,
as well also as a Gelineau version, and other hymnic renderings as
well, such as those of Isaac Watts and those found in The Trinity
Hymnal and The Book of Psalms for Singing. Such hymnic renderings
ought to make use of the greatest of church music, such as the
Lutheran chorales and the great Anglican tunes of the 19th cen-
tury. Such a project should command the support of all churches
interested in genuine reconstruction, but where is the money for it
now? A genuinely usable and catholic psalter is nowhere to be
found.}7

Worship

Evangelicalism needs a return to formal and Biblical worship.
Worship is a public act, performed on the surface of God’s true
altar, the world, before His throne. Man’s chief end is to glorify
and enjoy God, and worship is done for God’s pleasure. It is
man’s highest privilege to dance before the throne of the King of
kings, to make a public ritual affirmation of the primacy of God.

Public worship is also done for the edification of men. To
“edify” is to build up, as we see in the word “edifice,” which means
building. God’s appointed pastors oversee and organize worship,
because they are in charge of overseeing the building of the edifice
(1 Cor, 3:10-15; and 14:26), At the same time, edification does not
mean “good feelings.” We are not to worship as we “feel led,” but
as God requires.

The basic regulation of worship is found in John 4:24, “in
Spirit and in truth.” “Truth” refers not just to ideology but pri-
marily to covenant faithfulness. The Hebrew words that lie in back of
the New Testament word for “truth” have to do with faithfulness,
reliability, trustworthiness, sureness, (One of them is the word

17, For further remarks on this, see my essay “Church Music in Chaos,” in
James B, Jordan, ed., The Reconstruction of the Church, Christianity & Civilization
No. 4 (Tyler, TX: Geneva Ministries, 1985).
