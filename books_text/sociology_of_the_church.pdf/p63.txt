Reconstructing the Church 49

not a special ruling function.

A deacon is an assistant and an apprentice elder. Joshua was
Moses’ deacon. Elisha was Elijah’s deacon, The twelve were Jesus’
deacons, until they ascended to eldership, and then they selected
other trainees under them. A deaconness is an assistant to the
elders, but never an apprentice.

Conclusion

This is only the beginning of an agenda. Other matters need
to be thought through as well.®° For instance:

Church Architecture. The Bible shows us that the Spirit makes a
glory environment visible around the throne of the Lamb. Histor-
ically, the architectural model seen in the Tabernacle, the Temple,
and in the Book of Revelation has been viewed as the norm for
church architecture, Curiously, evangelicals, who make the most
out of doing things the Bible’s way, pay the Icast attention to these
architectural examples.

Children and Catechisms. Protestants historically have kept their
children from Christ's Table. This entire matter is being re-
thought today. Moreover, we need to ask if the best way to teach
the faith to children is by their memorizing a catechism consisting
of little more than a series of definitions of terms (for instance,
“What is justification?”). Biblical pedagogy for children seems to
consist of two things: stories and proverbs. In my opinion, cat-
echizing, while important, has historically assumed far too large a
place in the Christian education of children, though it should not
be done away with altogether.

Church and State. The Biblical model is the interrelationship of
avenger and sanctuary. One of the social functions of the church
in society is to act as a restraint on the state. Today, however,
church buildings are no longer regarded as sanctuaries. The
church needs to recover the concept that her courts are real, her
property inviolable,

39, Some of these matters are taken up in this book’s companion volume,
James B. Jordan, ed., The Reconstruction of the Churck. Christianity and Civiliza-
tion No. 4 (Tyler, TX: Geneva Ministrics, 1985)
