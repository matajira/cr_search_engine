Reconstructing the Church 27

the center of visual attention in the church, for the glory-
environment of the Spirit is established around Christ, Who is
specially present in Word and sacrament.

The essence of worship, according to Romans 12:1, is for us to
offer ourselves as living sacrifices, Leviticus 9:15-22 shows us the
proper liturgical order of sacrifice: confession, consecration, and
communion. First comes the sin offering, which means worship
must open with an act of confession of sin. After the sin offering
comes the whole burnt offering and the cereal offering, which are
acts of consecration: of self and works, respectively. Last comes
the peace offering, which is the sacrifice of communion, a meal
shared with God.

In terms of the dialogue of Truth, God speaks to us each time,
encouraging us to the triple act of sacrifice. First, we are exhorted
by the minister to confess sin, and then we do so (hopefully pray-
ing together a prayer provided for the occasion). The sanctuary
—God’s corporate people — must be cleansed by the sprinkling of
blood before worship can be offered, and we affirm thar by the
blood of Christ it has been so cleansed, once and for all.

Second cornes the synaxts or service of the Word. Passages of
Scripture are read (Old Testament lesson, Epistle, Gospel,
Psalm), and then comes the sermon, This is all designed to lead us
to the second act of sacrifice: the Offertory. ‘The Offertory is not a
“collection,” but the act of self-immolation (in and through Christ)
of the congregation. In union with Christ, and not apart from
Him, we offer ourselves (“whole burnt sacrifice”) and our tithes

gifts (“cereal sacrifice”) to God. In the early church, the bread

 

and wine for communion were also brought forward at this time,
along with tithes and other gifts. Thus, the offering plates are
brought down front to the minister, who holds them up before
God (“heave offering”) and gives them to Him. God then gives the
offering back to the elders to use in His name. Then comes the
long prayer, the prayer “for the whole state of Christ’s church,”
(“incense offering”), which also is part of the Offertory, With this

 

prayer, the synaxis is over.
Now begins the third act of sacrifice, the eucharist (“thanksgiv-
ing”) or Lord’s Supper. Prayers are offered, and the people are ex-
