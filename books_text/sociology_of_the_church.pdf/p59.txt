Reconstructing the Church 45

in the way these questions are asked?

I believe that the hidden assumption is this: It is assumed that
the human calling to serve God as prophet, priest, and king is
more universal than sexual differentiation, Protestants especially
start from the assumption of the “universal priesthood of all be-
lievers,” and from this it follows that “both men and women are
priests” in this sense,

This in my opinion obscures the issue. Suppose we were to
say, “No, it is not true that both men and women are prophets,
priests, and kings. Rather, only men are prophets, priests, and
kings; women are prophetesses, priestesses, and queens.” If we
phrase our canon in that fashion, we are asserting that the differ-
entiation of humanity into male and female must totally qualify
the notion of office or function.

On the basis of what is said in Genesis 2 and 3, we have to
think in this latter fashion, Man was given his calling to dress and
to guard the garden before the woman was created. The woman
was then brought to be his helper. She also dresses and guards the
garden, but as a woman, not as a man. She guards and dresses in
a way different from the way a man guards and dresses.

Let us return to our questions. May women prophesy? It
seems so, There are prophetesses in both the Old and New Te:
ments, and while they are few in number, nothing in the text indi-
cates anything unusual about them. In the Bible (as opposed to
systematic theology), a prophet is simply one who speaks for
another, in terms of God’s council, Thus, the first reference to a
prophet in the Bible is to Abraham, who is said to speak to God on
behalf of Abimelech (an activity generally seen as priestly by sys-
tematicians). May a woman speak for her husband? Certainly.
This being the case, it is certainly proper for a woman to speak to
the whole church on behalf of God, the heavenly Husband.

But, a prophet prophesies not only as a representative of the
Father/Husband/Son, but also as a symbol thereof; while the
prophetess prophesies simply as a representative, as the Mother/
Bride/Daughter.

May women be judges? It seems so. Deborah is the premier

 

ae

example here, Does that mean that a woman may exercise au-
