ANNOTATED TABLE OF CONTENTS

Preface ....... 00.6.0 eevee es ere eects eee sete ee Vit

Part I: Building Blocks for Reconstruction

 

Introduction . : : :
Brief overview of the essays that follow

  

1, Reconstructing the Church: A Conservative Ecumenical Agenda ......3
Louisville, Nebraska; Justinian and the Nika Revolt
True and False Churches, 5
The true Christian loves the Bible
Satan steals the Word
Liberal Churches do not always totally negate the Word
Conservatives also negate the Word in various ways
Dispensationalism eliminates the relevance of large parts of it
Pentecostalism substitutes experience
Bapto-presbyterianism tends not to teach or preach it
Little of the Word enters worship structure
Catholicity and Integrity, If
Openness to other Christian traditions
Commitment to mutual recognition of discipline
Balance of catholicity and integrity
Striving for catholicity only, or integrity
Muddling through
Solution: voting and non-voting members
What Might We Learn from Episcopatianism?, 15
They dominate our culture
Promotion of excellence and scholarship
Not afraid to recognize and use hierarchy
Biblical view of how God works with both elite and masses
Polemic against envy
Respect for tradition
High view of visible church, ritual, and symbol
Problem: lack of real discipline

321
