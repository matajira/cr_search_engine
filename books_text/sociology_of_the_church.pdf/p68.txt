54 The Soctology of the Church

by Christ.” But what does this mean, practically? Most followers
of the Belgic Confession would argue that Baptists do not practice
baptism as it is taught in the Bible. The Bible, however, nowhere
speaks directly to the issue of infant baptism, “Well,” it may be
replied, “the whole Bible makes it clear that infant baptism is to be
practiced. Since Baptists reject this, they are not ‘pure’ in their ad-
ministration.” But what about Reformed churches that use grape
juice for Communion? How “pure” are they, since the Bible
“makes it clear” that wine is to be used? And what about Re-
formed churches that do not have Communion each week, when
the Bible “makes it clear” that weckly Communion is the rule?
How “pure” are they?

The hard fact is that there is no abstract formula that can be
used as a procrustean measure of whether a given church is true
or false. In practice, such Reformed churches demand of other
churches that they be as pure as they need to be to be regarded as
pure enough by such Reformed churches. How pure do you have
to be? As pure as we say you have to be.

‘This is not helpful. The Bible nowhere gives us any “marks” of
the true church, at least not in the sense of abstract formulae. We
need something that is a little more practically relevant to the
question, especially in a time of ecclesiastical chaos such as we are
in today.

 

Wave, Particle, and Field

Modern physics has contributed to linguistics and to philoso-
phy the notion of three basic perspectives available to man as he
views existence.® The “Particle” view focuses on a discrete thing,
im terms of its identifiable features. The “Particle” view answers
the question: What are the characteristics of this thing?

The “Field” view focuses on the interrelationship of the thing
with other things. It answers the question: How does this thing
relate to other things?

6. The best overall theological treatment of this is Vern 8. Poythress, Philoso-
phy, Science, and the Sovereignty of Gad (Phillipsburg, NJ: Presbyterian and Re-
formed Pub. Co., 1976).
