Annotated Table of Contents 327

Priests (Seed), 119
Officers represent Christ
Baptism and circumcision
Genealogical principle: Christ’s “succession”
Summary, 121
All are priests, yet the distinction between cult and culture is still important
The church and the world: dynamics are diffcrent in the New Covenant
Covenant theology and dispensationalism share same error

Part I]: Thoughts on Modern Protestantism

Introduction . 2.0.00... 06. c eee ees rs 125
The meaning of the word “protestant”
Fundamental difference between Roman Catholic ecclesiology and that of
Orthodoxy and Protestantism
The Fringe, 126
Small faithful churches are not on the fringe but in the center of the church
Glamourous worship is not the issue; gnosticism had gorgeous worship
The True Church Syndrome, 127
Historical succession is not determinative for finding a “true church”
‘The true valuc of history
Example: Anglican worship less historically faithful than Presbyterian
The Facts, 130
1, We live in a theocracy today
2. The church of Jesus Christ is unified today
3. The church is brought into being by the procession of the Spirit, not by
historical succession
New faithful churches are truc heirs of the past
Encouraging Words, 133
Encouraging signs from some scholars within mainline churches
Such signs do not yet indicate that the churches themselves are changing
David's Band, 134
Tt is better to be with David's band fighting evil, than to remain
comfortably with Saul
The Problem, 135
Evangelicals are not acting like the true catholics they are

4. The Three Faces of Protestantism . . 137
Current view divides protestantism into magisterial and anabaptist
branches
Problem: ignores third branch, the Reformed Catholic branch
Current view sees everything in statist terms
Church and State in the Middle Ages, 138
Pagan view of king as head of religion
