2

THE SOCIOLOGY OF THE CHURCH:
A SYSTEMATIC APPROACH

In this essay we are concerned with a variety of problems that
can generally be put under the umbrella of the “sociology” of the
church. That is, we are not concerned directly with a (sacramen-
tal) theology of the church, but with how the church relates to
other spheres and institutions of life in this present world. We
shall be concerned first of all with an investigation of the nature of
the church in her threefold manifestation; the people of God, the
special government of Christ, and the institution of organized
public worship. We shall be concerned with how we are to recognize
the church in these various aspects or manifestations. Then we
shall turn our attention to the institutional church in its relation-
ship to itself, to other “denominations,” and to the parachurch
phenomena. To assert that the church should not be divided into
denominations, and that parachurch organizations should not ex-
ist, is of little help in trying to discover why God has brought it to
pass, and how the matter should be understood and resolved.

The Three-fold Nature of the Church

Without too much difficulty we can see that the church has
three aspects. First and foremost, the church is a specific govern-
mental body with a specific governmental power, I say first and
foremost, because this specific power is nothing other than the
power to admit to or expel from the sacramental Body of Jesus
Christ. It is at the sacraments that Christ is specially present, and
manifests His special and central rule on the earth. The special

51
