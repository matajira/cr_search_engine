16 The Sociology of the Church

essay on Episcopalianism. I offer it as an example of the kind of
openness we need to have if we are going to make significant prog-
ress toward the reconstruction of the church and of our culture.

Some of us like to believe that our American Christian culture
is based on Presbyterian and Baptist values, Obviously this is to a
great extent true. The fact is, however, that both in Britain and in
America, the dominant religious group has been Episcopalian.
Like it or not, the Episcopalians have exercised more effective
social dominion than have the rest. The strengths of Presbyterians
and Baptists have been harnessed, monitored, directed, and over-
seen by Episcopalian rulers in both nations.

Why. is this? Why are the Episcopalians, as a group, the
strongest, and that in spite of the fact that after the War of Inde-
pendence they were associated with despised loyalists? I should
like to isolate what I regard as certain key factors, at which points
Fpiscopalians differ from Presbyterian and Baptist groups. All
three have a heritage of Calv: ic or Augustinian orthodoxy (in
soteriology and the doctrine of God), and thus all three far surpass
all other churches in dominion (counting the Methodists, for now,
as a variant of Episcopalianism in this regard). The Episcopalians
(as distinguished from Methodists here) have been on the top,
always. There is something different about Episcopalians that
brings this about. What is it?

I believe that the salient factors are three: the promotion of ex-
cellence, the respect for tradition, and a certain primacy of the in-
stitutional church,

First of all, it is my impression that the Episcopal churches,
more so than any others, are careful to advance and promote their
best men. If this is true in their church, it will also be true in their
society at large. If one looks to see who the big name theologians
of Episcopalianism are, they are frequently bishops. The Episco-
palians identify, promote, protect, and prosper their best men.
‘They provide large salaries, good homes, secure retirements. For
their scholar-bishops, they provide domestic servants and s
taries, so that the man of the cloth is free from ordinary worries
and duties and can devote his time to pastoral and literary work.

 

  

 

  

 

Te-
