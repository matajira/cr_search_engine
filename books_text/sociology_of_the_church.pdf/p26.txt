12 The Sociology of the Church

churches is not the rule.

Similarly, we have on occasion been forced to declare certain
of our members excommunicate from the church. These people,
almost without exception, simply go down the street and join
another church. Do the pastors of these churches phone us up and
ask us about it? No. Never. Not once. Indeed, we have taken it
upon ourselves, on occasion, to write letlers or phone other
churches when we hear that they have taken in excommunicated
people, but we have seldom received any recognition.

Now, what is interesting is this, Recently, a presbyterian
church in our town split. Who was right in the center of causing
the trouble and the split? A couple of people excommunicated
from another church. Also, recently, a presbyterian church in a
nearby town also split. Who was right in the center of that split?
Again, it was a couple of people excommunicated from another
church. There is a price to be paid, it seems, lor despising the gov-
ernment of other churches,

 

 

  

In our early days, a man came to us from another denomina-
tion. He had been excommunicated. He said it was because he
had come around to Calvinistic doctrine that he had been perse-
cuted. Instead of checking out his story, we believed him. Within
six months we had had to excommunicate him also. Then we
checked up on him, and found out that the real reason his former
church had excommunicated him had nothing to do with doc-
trine! We had to learn the hard way. The next man who came to
us with that story was sent back to his former church, not a pres-
byterian church, to set things right. Initially he was very angry
with us, but after a couple of months he did go back and make his
peace with his former congregation, and was enabled to transfer
in peace to a presbyterian church.

Third, internally we need to work out a balance between catholicity and
integrity, There are generally four ways to resolve the tension be-

 

tween the two,

(1) A church may strive exclusively for catholicity. In my
opinion, this is what happens when the Lord’s Supper is held, and
the officiant invites everybody who thinks he is a Christian to par-

   
