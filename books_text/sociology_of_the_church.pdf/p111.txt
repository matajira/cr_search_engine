Sociology: A Biblico-Historical Approach 97

that is in the New Covenant. Theologically, symbolically, Ishmael
and all those living outside Eden are in the “flesh.” They are in the
“world” in the bad sense. They are exiled. All the same, they can
be saved by the influences that flow downstream from Eden.
Thus, when Paul uses Ishmael as a picture of those living under
the curse because of sin, he does so in terms of the theological/
symbolic aspect. He does not imply that Ishmael was unconverted
in the moral/Spiritual sense.

Those who live in Havilah are to bring their tithes and offer-
ings to Eden. Accordingly, we read of the sons of Abraham in
Isaiah 60:4-9 that they will bring the wealth of all nations to the
sanctuary. In verse 6 we read of Midian, Ephah, and Sheba (sons
and grandsons of Keturah, Gen. 25:2-4), and in verse 7 of Kedar
and Nebaioth (Ishmael’s first two sons, signifying the whole of his
descendants, Gen. 25:13).

Isaac, Jacob, and Joseph

As we continue, now, with the lives of the Patriarchs, there are
three other passages to note, The first is Genesis 26. Here we have
the story of a second threat to the seed people from the king of the
nations, Abimelech. After God delivers His peculiar possession,
we find a story of conflict between Isaac and the Philistines. Fi-
nally, we find that the Philistines are converted to the Lorp, and
make covenant with Isaac, in fulfillment of the Abrahamic prom-
ise that all nations would bless themselves in the seed. What is
beautifully set out in this chapter is the relationship between
Edenic water and downstrcam influences. The whole focus in ver-
ses 12-33 is on wells of water, Isaac’s peculiar task in life is to dig
wells, to provide water for others. That is what it means to be a
priest to the nations. Initially, the Philistines in their depravity try
to stop Isaac. They fill the wells with “dust,” which is an act of kill-
ing the well (26:15 with 3:17, 19). In verses 18-22, Isaac digs two
wells, which the Philistines fight over, and then finally a third
well, which he enjoys in peace. This connects to the third-day res-
urrection theme that runs all through Scripture." Then, Isaac

10. For a discussion of the third-day theme in Scripture, sec my Sabbath Break-
ing and the Death Penalty.
