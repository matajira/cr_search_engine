322 The Sociology of the Church

The Bible, 23
Self-attesting canon
Copyrighted Bibles, and need for a chureh Bible
Restoring the original order of books in the Bible
Restoring the Bible to centrality in worship
Warship, 25
Command performance
Affirmation of the primacy of God
Worship in truth = covenant faithfulness, dialogue of truth
Worship in Spirit = heavenly environment
Living sacrifices and the order of offerings (Leviticus 9)
Basic order of synaxis and eucharist
Goals of the Reformation:
Biblical regulation of worship, sidetracked by minimalism
Old Catholic forms, sidetracked by anti-catholicism
Congregational participation, sidetracked by quictism
Discussion of Savoy Conference: Puritans and Anglicans
Worship and Ceremony, 30
Creeping ceremonialism today
Principles of ceremony:
Priesthood of believers, whole-personed participation
Heavenly pattern, ceremony in Revelation
Performative language
The Lord’s Supper, 33
Knowing and doing
Consequences of doing without knowing, and knowing without doing
The six-fold action
Proclaiming the Lord's death: to God the Father
‘The Memorial Name—a reminder to God
Worship as response to Truth, never silent
The Keys of the Kingdom: Word and Sacrament, 39
Cherubim, the gate of Eden, and the keys

   

 

Relation of Word and sacrament,

 

ign and seal
Two or three wimesses: Word, sacrament, image
Memorial *thereness and thatness”
Memorial as continuing miracle
Procession of the Holy Ghost gives model for relationship of Word
and sacrament
Church Rulers, 41
The prophetic council
Stars, angels, and elders
Order of worship in the Book of Revelation: actions of the angels
The Woman Question, 44
Women prophesy as wornen, not as men
