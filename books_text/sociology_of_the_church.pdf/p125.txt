Sociology: A Biblico-Historical Approach lt

heaven and earth, Finally, that duality would also be overcome
when the world was transfigured, and man would have a new
Spiritual body capable of movement in both heaven and earth.”
In Christ, all of this came to pass. For that reason, there is no
longer any separate earthly sanctuary, for all the earth has been
cleansed so as to become possible sanctuary contact-points. This
may not be visibly true yet, but it is officially the case. What is the
sanctuary for earth? It is heaven.%

This means that there is no place on earth for us to go to wor-
ship God. Thus, we have to go into heaven to worship Him. And
that is just what we do.

In the Old Creation, there was a duality between the High
Priest and all the other priests. The same duality existed between
Israel as a nation of priests, and the “priesthood of all believers”
possessed by believers living downstream. Where is that duality
today? It is the duality that exists between Christ and the rest of
us priests, as follows:

1, Christ is living totally in the sanctuary land of heaven.
We live in an “already but not yet” sanctuary on earth, both
downstream and in sanctuary. In worship, however, we are
enabled to join Christ in the fulness of sanctuary.

2, Christ is living in perpetual sabbath rest and joy. We live
in an “already but not yet” perpetual sabbath on earth, given
full rest in Christ but still having our own work to do, In wor-
ship, however, we are enabled to join Christ in the fulness of
sabbath rest.

23. This motif of the progressive “coalescence of culture and cultus” has
received attention throughout church history. Of particular usefulness is Klaas
Schilder, Heaven: What Is [t?, Wrans. and condensed by Marian M. Schoolland
(Grand Rapids: Eerdmans, 1950), Schilder reflects at length upon the duality of
heaven and earth, and the eschatological joining of the two in the new heavens
and earth. He also points to the transfigured “perpetual sabbath” as another ex-
ample of the merging of work and worship.

24. For liturgical and social! reasons it is important that certain places be set
apart as sanctuaries, for worship and for refuge. The point is that any place may
be thus set apart, at the decision of men, because ail places have judicially been
declared sanctuaries by God.

  
