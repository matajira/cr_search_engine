110 The Sociology of the Church

need of redemption. They were like slaves (4:7). Compared to the
New Covenant believer, they were like Ishmaclites (4:21ff.), In
spite of his very real privileges, the Jew was still essentially excluded
under the Old Covenant. New Covenant belic re now included
and restored to the privileges of being guardians of the Garden
(given the keys to the gate of the Garden, the keys of the Kingdom.)

‘Thus, the Pauline perspective is this: Under the Old Cove-
nant all men were saved by faith. Yet within the category of those
saved by faith, sons of Abraham, there was a division of labor and
privilege. Some were of the circumcision, and some of the uncir-
cumcision, Those of the circumcision had greater privilege, and
their position displayed the nature of salvation. Thus, salvation
came to the gentile by virtue of his faith in the Secd of the circum-
cised line. When we get to the New Covenant, we do not find that
gentiles are now incorporated fully into Israel. Rather, we find
that the entire system is done away, and a new creation comes.

There is more we have to say about this, but let us ask a cou-
ple of questions here. Was Israel the church of the Old Covenant?
Yes and no. Yes in the sense that it was the heart and core, the
focus of the church, Yes in the sense that it was definitively the
church. Yes in the sense that it was centrally (in a very literal, geo-
graphical sense) the church. But no in the sense that the popula-
tion of circumcised Israel was the outer limit of the community of
the redeemed.

Did the church begin at Pentecost? In the New Testament
sense of “one new man,” yes. In the sense of the community of the
redeemed, no.

 

 

The New Creation

To understand the sociological arrangement of the new crea-
tion in Christ, we have to remind ourselves of man’s original task,
which our Lord in essence completed. It was to overcome all bar-
riers in the world by glorifying all of it. The world would go from
Garden/Eden/Downstream to being all “New Jerusalem”
throughout (cf. Rev, 21:22). All the dualities in the world would
be “overcome” by glorification, leaving only the first duality of
