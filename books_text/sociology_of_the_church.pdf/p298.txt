284 The Sociology of the Church

With the completion of the work of redemption and the close of
the canon of Scripture, no such sign-hcalings are needed. This ap-
proach assumes that the symbolic dimension is special, and adven-
titious to the “brute fact” of healing itself. In fact, the symbolic
dimension is the primary one.

Secondly, following on this, rationalistic orthodoxy has down-
played the Biblical evidence that indicates that the coming of the
New Covenant is the coming of an age of healing, and that a heal-
ing ministry is part of the normal work of the church. The tend-
ency in rationalistic orthodoxy is to eliminate as much as possible
the mystical or non-rational clement in faith and worship. This
tendency is nowhere better seen than in the “Zwinglian” view of
the presence of Christ in the Holy Eucharist, advocated by virtu-
ally all evangelical and Reformed groups for several centuries
now.? In view of this, it is no wonder that the healing ministry has
all but disappeared from such churches.

Predictably there has been a reaction against the rationalism
of orthodoxy, and we sce it in the cultivated non-rational experien-
tialism of the charismatic movement. Unfortunately, the charis-
matic movement tends to err as much on the one side as orthodoxy
does on the other. Not infrequently, hard theological reflection is
seen as damaging to the irrational work of the “Spirit,” and all too
often, irrational experiences are sought for their own sake, and in
terms of what we must call (with severe frankness) a pleasure prin-
ciple, The massive quest for pleasurable ecstatic experiences is part
and parcel of a uniquely American kind of Christianity, which is
of a piece with the American “fun and games ethic” as a whole.

All the same, charismatic theologians note rightly that the
New Covenant is a time of healing, and that the Bible indicates a
certain expectation that God will heal] the diseases and afflictions
of His people. Such promises were found in the provisional ad-
ministration of grace to Israel under the Old Covenant (as in Ex.
15:26; Dt. 7:15; Prov. 4:22). It is particularly with the ministry of
Christ and His disciples, however, that we find abundant healings

2. Fora corrective to this view, sce Ronald Wallace, Calvin's Doctrine of the Word
and Sacrament (lyler, TX: Geneva Ministries, 1982).
