100 The Sociology of the Church

as those who maintain the throne sanctuary and hold property in Eden. The
circumcision and then rejection of Ishmael forms a type of the cre-
ation and fall of man. Ishmael, the son of the “flesh,” is fallen, and
must be cast out.'?

Circumcision, of course, entailed the shedding of blood, and thus
substitutionary in nature. The circumcision of the male children
covered representatively for the females. Since circumcision con-

   

stituted Israel as a nation of priests, and since women could not be
priests, they were not circumcised in any fashion. Rather, they were
saved by being connected to those who were circumcised.'3 They
were saved by faith in what the blood sacrifice of circumcision en-
tailed, Why could women not be priests? Because a priest is a
guard, and Adam’s task was to guard his woman. The woman is
not the guarder, but the one guarded. Because this is the heart of
what il means to be a priest, no woman was ever a priest.'*

Similarly, those outside of Israel who were converted to the
true religion were not circumcised, for they were not members of
the priestly nation, But they were saved by putting their faith in
what the circumcision of the seed meant. Ultimately, circumcision
pointed to the death of Christ, the cutting off of the Seed. This
was the only circumcision that ever saved anybody, and all those
thus saved are saved by faith in it.

This means that there is a sociological difference between the
circumcision of the Old Covenant and the water baptism of the
New Covenant. We shall explore this problem in the section of
this chapter dealing with the New Covenant. For now, let us get

 

more evidence before us.

When Moses lived with Jethro, the Godly priest of Midian, he
did not circumcise his sons. Commentators have puzzled over
this, and generally ascribed it to sin on Moses’ part. Together with
this goes the presumption that Jethro was a heathen, This will

12. In other words, Ishmael remains in the position of Noah or Shem, while
Tsaac is in the new position in which Abraham had been placed,

13. See my study of this in The Law of the Covenant (Tyler, TX: Institute for
Christian Economics, 1984), Appendix F.

14. Sec my fuller remarks on this on pp. 44-49, above,

15. See discussion in The Law of the Covenant, Appendix F.
