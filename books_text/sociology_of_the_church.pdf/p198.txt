184 The Sociology of the Church

when such a novice is the spokesman for the New Christian
Right!

Christian Zionism is blasphemy. It is a heresy. Christians have
no theelogical stake whatsoever in the modern State of Israel. It is
an anti-God, anti-Christ nation. Until it repents and says “blessed
is He Who comes in the Name of the Lord,” it will continue to be
ander the wrath of God. The modern State of Israel permits the
persecution of Christians and Christian missionaries, We must
pray that God will change the hearts of Jews, as of all other pag-
ans, to receive Christ. But to support the enemies of the Gospel is
not the mark of a Gospel minister, but of an anti-Christ.

Tve been pretty hard on Jerry. Somebody needs to be. This
kind of thing is inexcusable, and needs to be repented of. A couple
of years ago I wrote an essay defending Falwell against a some-
what liberal critic.? What I have said here does not change what I
wrote then, because Falwell’s critic was wrong; but I have cer-
tainly come to take a dimmer view of Mr. Falwell since, His tram-
pet is giving forth an uncertain sound. He needs to clean it out.

Messianic Judaism

In recent years, a large number of Jewish young people have
turned to Jesus Christ as their Lord and Savior. Many of these
young people have formed “Messianic Synagogues,” and have ar-
ticulated here and there various theologies of “Messianic
Judaism.” For many, Messianic Judaism is simply a way of keep-
ing some Jewish cultural traditions while becoming Christian,
and there is nothing wrong with this. It is proper for Christians of
various tribes and tongues to give expression to the faith in a vari-
ety of cultural forms.

Unfortunately, for some, Messianic Judaism is seen as an
alternative to historic Christianity. his is due to the influence of
pop-dispyism. After all, if the Millennium is right around the cor-
ner, and Jewish culture will be imperialistically triumphant dur-

7. See my essay, “Che Moral Majority: An Anabaptist Critique?”, in James B
Jordan, ed. The Failure of the American Baptist Culture. Christianity and Civilization
No. 1 (Tyler, TX: Geneva Ministries, 1982).
