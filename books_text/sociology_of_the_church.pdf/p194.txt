180 The Sociology of the Church

and the promise “those who bless you, I will bless,” have bcen set
aside, until we re-enter “prophetic time.” Thus, the Jews have no
right to the land during the Church Age, and also there is no par-
ticular blessing for Gentiles who treat the Jews with especial favor.

Fourth, dispensational theologians are most strict on the point
that the Church is a “new people,” composed as one body in Christ
of both Jew and Gentile, During the Church Age, the distinction
between these two is not to be felt in the Church. Thus, dispensa-
tional theology is, by implication, opposed to the kind of stand-
point articulated in many “Messianic Jewish” groups.

What I am setting forth is standard, consistent dispensational-
ism. As far as 1 am concerned, dispensationalism is sorely wrong
in its prophetic view, but it is at least orthodox in its view of salva-
tion and blessing. Blessing comes to the Jews when they repent
and accept Christ; until then, they are under God’s curse. How
can it be otherwise? All blessings are in Christ. This is the teach-
ing of orthodox Christianity, and Darby and the early dispensa-
tionalists were orthodox Christians on this point, as far as I can
tell.

Jerry Falwell and Christian Zionism

My description of dispensationalism may seem rather strange,
because this is not the teaching of Hal Lindsey, of the modern
Dallas Theological Seminary, or of other modern dispensational-
ists. I call these people “pop-dispies,” for short. In contrast to the
dispensational system, these people hold that God presently has two
peoples on the earth: the Church and Israel. The consistent dis-
pensational system teaches that there are no prophecies whose ful-
fillment takes place during the Church Age, because the Church
exists outside of prophetic time, but modern pop-dispies teach
that the reestablishment of the nation of Israel in 1948 was a fulfill-
ment of prophecy.

Consistent dispensationalism teaches that God is dealing with
His “heavenly” people today (the Church), and that during the
Church Age, God has “set aside” His apostate “earthly” people
(Israel), Pop-dispies, on the contrary, hold that even though apostate,
