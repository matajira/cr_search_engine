126 The Sociology of the Church

mony that was corrupting her. For the Reformers, it was really
the Roman Church that was in exile from her true Head.

There is an important difference between the ecclesiology of
the Roman Catholic Church and all other churches at just this
point. While the earthly head of the church in the Old Covenant
was located in earthly Jerusalem, in the New Covenant the Head
of the Church is located in heavenly Jerusalem, and He is mani-
fested whenever and wherever a local assembly draws into heaven
for worship. Our Head is visible to us in the mystery of bread and
wine, Accordingly, the seven churches of Asia Minor arc shown in
Revelation 1-3 as seven separate lampstands, not as branches of
one stand, Christ is in their midst, but there is no necessary politi-
cal union among them.

Writing of Eastern Orthodoxy, which has preserved the view
of the carly church, Timothy Ware states, “For Rome the unifying
principle in the Church is the Pope whose jurisdiction extends
over the whole body, whereas Orthodox do not believe any bishop
to be endowed with universal jurisdiction. What then holds the
Church together? Orthodox answer, the act of communion in the
sacraments.”* There are several Eastern churches, each “auto-
cephalous” (having its own head), that are in fellowship with one
another and with the Ecumenical Patriarch in Constantinople.
Although there are differences over how this works out in prac-
tice, the principle held by the Eastern churches is the same as that
held by the protestant.

The Fringe

People who look only on the outward, sociological appearance
tend to think of small, conservative denominations as on the “fringe”
of the church. Nothing could be farther from the truth. It is true
that such a denomination as the Association of Reformation
Churches, to which I belong, is on the fringe of the heretical,
gnostic, modernist “mainline” churches in America, but it is also
true that the ARC is smack-dab in the center of historic catholic

4. Timothy Ware, The Orthodox Church (Middlesex: Penguin Buoks, rev, ed
1983), p. 250.
