140 The Sociology of the Church

separation of church and state.

During all these centuries there were scores of drop-out move-
ments. Some of these remained within the church, but argued
that the church should avoid the “world” and especially that the
church should “follow Christ's example and live in poverty.” The
imperial theologians found it very convenient to support the
theology of the sects, for an impoverished and powerless church
was exactly what the kings of northern Europe wanted. They
wanted no earthly institution to compete with their own,?

In the later Middle Ages, unfortunately, the Papacy came to
function more and more like an imperial monarchy. The ideal had
always been a universal, catholic church with its headquarters in
Rome, It was harder and harder for people to believe in this vi-
sion when the Papacy was acting more and more like a state in it-
self, Naturally, the imperial and statist thinkers of the North (and
in Italy, too) took every opportunity to point out Roman incon-
sistencies, and to maintain that their struggle was not with the
“pure” church but with the Papal perversion of it.

Emst Kantorowicz has described the shift in catholic thought
that accompanied the drift into Papal statism.? In the early church
and in the early Middle Ages, the corpus mysticum or “mystical
Body” was the sacrament of the Lord’s Supper, around which the
church was gathered. The center of the church’s earthly dominion
was the sacrament, which she administered in Christ's name.
Christ was the head of the church, and He made Himself present
and active through proclamation and sacrament. The church's
earthly power was the power to excommunicate from Christ. The
church was corpus Christi, the body of Christ, centered around His
mystical body.

The first shift away from this early and biblical way of think-
ing came when the term corpus mysticum (mystical body) came to
be used for the church instead of for the sacrament. In a subtle

 

2. The drama of these three forces: catholic, imperial, and sectarian is
described cogently in the recent celebrated novel by Umberto Eco, The Name of
the Rose—a fascinating book

3. Ernst Kantorowicz, {he King’s Tivo Bodies: A Study in Medieval Political Theory
(Princeton: Princeton University Press, 1957), pp. 1938.
