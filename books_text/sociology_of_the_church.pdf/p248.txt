234 The Sociology of the Church

The fact that she chose to stand with her husband rather than with
the church is an illustration of what it means to choose the old
family over the new. During worship, in times of judgment, the
husband never speaks for his wife. When joining the church, both
the man and the woman must answer the questions. Both must

 

answer the questions pul to thern when they bring a child for bap-
tism. ‘Che wife must take the sacrament directly from Christ’s rep-
snitatives, not through the mediation of her husband.

Since the church restores the world, however, after worship is
completed the natural family is . Afte
that takes piace in worship, the principle that the husband is head
of the wife is secure. Indeed, it is precisely the breaking down and
rebuilding action of the liturgy that secures the order of the natu-
ral family, and thus restores Biblical familistic culture.

‘The baptistic worldview of American evangelicalism does not
perecive that the family structure as such is dead and must be re-
newed in the Kingdom. While evangelicals are very concerned
about the farnily, its structure is not related to the specific work of
the church. The church is seen as dealing with individuals, but

re:

  

 

r the transformation

 

lores

 

 

not seen as taking hold of the family as such and transforming it.
Baptistic evangelicalism thus tends to separate the natural family
from the foundation and reinforcement of the new family, the
church.? Books on Christian family life abound, yet few if any
refer to God’s new family as the foundation for the restoration of
the natural family. The natural family is simply enjoined to keep a
bunch of rules — good in themselves — apart from the transforming
life of the Kingdom. As a result, pressures and expectations are
place on the natural family that it cannot bear, and rampant
divorce is the present-day result in American evangelicalism.
The restoration of the natural family in the Kingdom

 

  

in the organization of the New Covenant church (Acts
Rom, 16:5, 10, 11; 1 Cor. 1:11, 16; Col, 4:15; 1 Tim. 5:13; 2 Tim.
1:16; 4:19; Philem. 2). The logical pattern for organizing the New

  

 

7, On American evangelicalism, sce James B. Jordan, ed., The Failure of the
American Baptist Culture, Christianity and Civilization No. 1 (Tyler, TX: Geneva
Ministries, 1982)
