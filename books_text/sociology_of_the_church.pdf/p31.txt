Reconstructing the Church 17

Is anything like this ever done in Baptist and Presbyterian cir-
cles? I dare say not. To my knowledge, there has never been, in
the entire history of Presbyterianism, a man who was set aside to
be a scholar and writer. Without exception, Presbyterians load
their best men down with detail and trivial tasks, so that they ac-
complish little. Their best thinkers are made teachers in theologi-
cal institutions, where they are made to spend their days going
over basics with young, immature men just out of generally
worthless college educations. The rest of their time is taken up
with committce meetings and administrative tasks. It is a wonder
that any of them ever get any writing and research done. It is no
surprise that the most brilliant of them, Cornelius Van Til, sel-
dom was able to get his writings into polished English style—~he
had no time for it.

We can contrast this with the armies of scholars maintained by
Rome, and the small cadre maintained in Episcopalian circles.
The difference is marked, and points to the fundamental differ-
ence between these two groups. The catholic party (Roman and
Anglican) is frankly elitist. It strives to convert and control the
elite in society, and it arms its best men for that task, giving them
time for reflection and writing. The evangelical party (Presbyter-
ian and Baptist, especially the latter) is infected largely with the
heresy of democracy, and believes (wrongly) that the conversion
of society comes with the conversion of the masses.

Americans (evangelicals) like to believe the myth that society
is transformed from the “bottom up” and not from the “top down.”
This flies squarely in the face both of history and of Scripture.
The history of Israel, as recorded in Scripture, is not a history of
revivals from the bottom up, but of kings and their actions. Good
kings produced a good nation; bad kings a bad nation. The order
is always seen from the top down, though of course with real feed-
back from the bottom up.

‘This is no surprise. From Genesis 3 onwards, society is likened
to a large man, with a head and hands and feet. The head obvi-
ously governs the rest of the members. To destroy the body, you
crush the head. This is seen over and over in the book of Judges.
Sometimes the head is literally crushed, as with Sisera and Abim-
