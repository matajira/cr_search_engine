94 The Sociology of the Church

Isaac, Yet, he was saved by the covenant, by faith in it. Similarly, the
Covenant of Grace was made first and foremost with Jesus Christ,
and we are saved by faith in it. In that sense, it is made with us
also, In a systematic theological sense, the covenant was indeed
made with Ishmael, but in a redemptive historical sense it was
not, The covenant was specifically made with the seed-throne
people, but its benefits were extended downstream to the nations.
This will become clearer as we proceed.

When Abraham was old, he took another wife, after securing
the seed line by getting a replacement for Sarah. It is important to
sec this. Isaac takes Rebekah into Sarah’s tent (Gen. 24:67).
Keturah is not the replacement for Sarah, in the redemptive theo-
logical sense. The seed line is separated from Abraham, and con-
tinues on down through history. There is a new bride, and she is
the bride of Isaac, not of Abraham. Isaac and Rebekah are now
the maintainers of the Eden sanctuary. Abraham has bowed out.8
He has moved downstream, and his new sons will be downstream
people.

Their names are given in Genesis 25:2-4. In 25:5, we read
that Abraham “gave all he had” to Isaac, but in verse 6 we read
that he gave “gifts while he was still living” to his other sons. Thus,
“all he had” must refer to the covenant and the Edenic throne-
sanctuary privileges and blessings. There were other capital assets
that Abraham could give to his other sons. In verse 6 we also

 

 

ing the Medieval separation of nature and grace using different terminology. See
the fine discussion in Henry Van Til, The Caluinistic Concept of Culture (Grand
Rapids: Baker, 1959), chapter 16. Biblically speaking, “common grace” is the
crumbs that fall from the (wholistically sacramental) Table of the Lord. Indeed,
Matthew 15-21-28 is a fine ilustration of the whole thesis of this chapter, that the
gentile dogs could be saved by placing trust in the benefits given to the world
through the children of Israel.

8. The book of Genesis contains a sustained implicit critique of patriarchy, be=
ginning in 2:24, When Isaac married, he moved away from Abraham, and he
took his essential inheritance (the covenant promise) with him. Similarly, it is
when Jacob is sent to procure a wife that the covenant is passed to him (Gen. 27
with 28:1-7). After that point, the text is concerned with Jacob and the covenant
line, and little more is said concerning Isaac. Biblically speaking, then, a sizeable
chunk of inheritance is passed on at the point of marriage; sons did not have to
wait for their parents to die in order to obtain inheritance.
