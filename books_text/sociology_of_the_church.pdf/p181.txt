The Effective Church Splitter’s Guide 167

church you just left. Never offer to start a new church. Try to
work it around so that one of them suggests it. Remember, when
it finally happens, it will be “the leading of the Lord.” At that
point, get with the denomination you have already contacted, and
get your church underway.

When soliciting sheep from the old church, never come right
out blatantly and invite them to go with you. Instead, just give
them a lot of pious brother-talk: “I just phoned to let you know,
brother, that some of us are starting a new work, brother, and I
just wanted you to hear it from me rather than from someone else,
brother.” This kind of indirect solicitation works best, and nobody
can complain against it. Who dares accuse you of stealing sheep!

The reason for going through all the legalities is that you want
your “transition” to be “peaceful.” You could just go out and start a
new church, and ignore an excommunication from the old one.
That puts you under a cloud, however. Make it peaceful, and
legal, even if it takes a little longer.

Now you have gotten all the really holy people out of the false
church. These are the real seekers after truth, the truly inflexible
people of God. Now the fun begins, because at last you have a
real, godly church.

Or do you?

At any rate, it is really fun being a wolf.
