70 The Sociology of the Church

wield power. As a result, it attracts men who want to play god by
wielding power, The power of God being withdrawn, the shell is
the helpless prey of these vultures. They entrench themselves, and
the outer shell of the institution becomes corrupt.

‘Third, the time comes when people begin to seek God once
again, and He lets Himself be found by them. As a result, Christ
is in their midst. They enter into communion with the Blessed
Trinity. They may grow up within an historical shell, or they may
grow up alongside of one. ‘he history of the “remnant” in Old
“lestament illustrates this sociological factor. How this “remnant”
should relate to the older shells will be taken up below.

The process we have just delineated has repeated
less times in history, even in the period before Christ. Yet theolog-
rally have not been able to formulate a theological ra-

 

 

 

elf count-

 

 

jans gen
tionale for this movement, due to a preoccupation with the “Parti-
cle” view and ignoring the “Wave” view.

All the same, there are certain legitimate questions that have
to be asked about the recognition of the church as a “Particle.” If
the shell is corrupt, docs that mean that the organic heart is cor-
rupt (officers, sacraments, Word)? We have to say, first, that men
cannot corrupt the aments. They can fail to administrate
them, and thus corrupt the sacramental order, but even if the offi-
cer doing the sacraments is a thorough renegade, the sacraments
remain God’s work, and are not corruptible by man. The heart of
the Donatist controversy was over this issue, and the church
determined from Scripture that the power and authority to ad-
minister the sacraments lies in the office itself, and in God’s work,

 

not in the person of the office-bearcr. Thus, we submit to the
office, not to the person, and it is the office and not the person who
administers the sacraments.

All the same, churches do become corrupt, and men should
separate from them. On what basis do we determine the corrup-
tion of the church? Which is more corrupt, the Baptist church that
makes Christ visible each week in the sacrament, but that re! $
the sacrament to children, or the Reformed church that admini:
ters baptism at least to children, but that makes Christ sacrarmen-
tally visible in the Supper only four times a year? As we noted

 

 
