God’s Hospitality and Holistic Evangelism 231

conformity to their own character (law), and by means of an order
in which the Father begets the Son, and the Father and the Son
send the Spirit. They are joined in being, but also joined in a
covenant bond, which has only been broken once, when the Son
on the cross cried out, “My God, my God, why hast Thou for-
saken me?”

Mankind, the image of God, reproduces this pattern at the
created level in the family. Right in the Garden of Eden, God es-
tablished the family and its boundaries (Gen. 2:24). The family is
a covenant bond, which includes personal (love and communica-
tion) and structural (law and hierarchy) aspects. Ephesians
3:14-15 states that all human families derive their name, that is
their character, definition, and interpretation, from God the
Father. Human culture is an outworking of religion, and the out-
working of the Trinitarian faith is a familistic culture.

Many of the basic powers of society are given by God to the
family: children and their rearing, property, inheritance, and care
of the poor.® The plan of salvation, covenantally administered, is
administered familistically, so that the sign of the covenant is ad-
ministered not individualistically but by households.

The state and the church are different from the family, and
have powers and duties that the family does not have. The state
has the power of the sword and the church has the power of the
sacraments (binding and loosing). Both state and church, however, are
seen in Scripture as organized by households. It seems that in the Patriar-
chal era, when all of society was organized by households, the

5. Louis Berkhof writes: “The subsistence and operation of the three persons
in the Divine Being is marked by a certain definite order. There is a certain order
in the ontological ‘Trinity. In personal subsistence the Father is first, the Son sec-
ond, and the Holy Spirit third, It need hardly be said that this order does not per-
tain to any priority of time or of essential dignity, but only to the logical order of
derivation, . . . Generation and procession take place within the Divine Being,
and imply a certain subordination as to the manner of personal subsistence, but
no subordination as to the possession of the divine essence is concerned.” System-
atic Theology (Grand Rapids: Eerdmans, 1941), pp. 88f.

6. On the powers of the family, sec Rousas J. Rushdoony, “The Family as
Trustee,” in The fournal of Christian Reconstruction, 1V;2(1977);8-13; and Rush-
doony, Instituies of Biblical Law (Nutley, NJ; Graig Press, 1973), pp. 159-218.
