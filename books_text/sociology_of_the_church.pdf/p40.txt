26 The Sociology of the Church

“amen.”) Jesus said, “I am the Truth,” and He is more than a mere
intellectual ideology. Truth involves discipleship ( John 8:31f.), so
that we are commanded to “do” the truth (John 3:21; 1 John 1:6).

Truth is presented as a dialogue between man and God. God
speaks first, and man returns speech to God. God speaks His
Word to man in more than one way: The Word is read to us,
taught to us, preached to us, made visible to us in the Supper,
sprinkled upon us in baptism, embodied to us in the lifestyle of
Godly men and women. Then, we return Gud’s Word to Him, by
listening, submitting to baptism, cating the Supper, singing and
praying Scripture, and so forth. This is the dialogue of Truth at
the heart of life, before the Throne, and it flows out into all of life.

The second clement in true worship is Spirit. If we read John
4:24 in its context (verses 20-26), we realize that it is talking about
environment. Worship in Spirit means worship in the environment
established by the Spirit.!® In the Old Covenant that was Mount
Zion. In the New Covenant, it is wherever Jesus Christ is present.
Worshipping in Spirit does net mean (a) worshipping internally,
or (b) worshipping enthusiastically, or (c) worshipping with my
spirit, Rather, it means worshipping in the glorious environment
of heaven itself.

This is made clear in Hebrews 12:22f. The Spirit brings
heaven to earth during the time of worship (compare Acts 2), and
we arc taken up into this heavenly environment (compare Revela-
tion 4 and 5). We are present not only with other Christians (“the
assembly of the Firstborn who are enrolled in heaven”), but also
al array,” as well as the departed

 

with “myriads of angels in fe
saints (“spirits of just men made perfect”). This is the environment
of worship, and it is described throughout the book of Revelation.
The slain Lamb and the Book in the center of the scene mean that
Scripture and sacrament should be prominently displayed at

 

18, The Spirit proceeds from the Father and the Son outward to manifest a
glory-environment around the Godhead. This glory is called heaven, and also is
seen as a cloud. It is architecturally modeled in the Tabernacle, in the Temple,
and in the world itself considered as an altar under a canopy of sun, moon, and
stars, For an introduction to this, see Meredith G. Kline, Jnages of the Spirit
(Grand Rapids: Baker, 1980).
