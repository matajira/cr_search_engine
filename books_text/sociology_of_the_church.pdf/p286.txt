272 The Sociology of the Church

day to set distance between ourselves and Roman Catholic cus-
toms. After 400 years, people in our churches are not still caught
up in Medieval superstitions. If you pastors wear a collar, and a
surplice and stole in worship, nobody is going to think that you
and you alone can mediate between him and Christ. It simply is
not a problem today. Thus, pastoral concern about Medieval in-
fluences is a non-issue in the discussion of vestments today.

Priestcraft?

I think we have a problem in letting Roman Catholic theology
tell us what a priest is and was. In traditional Roman Catholic
theology, a priest “mediates” between God and man in the sense of
having power to negotiate between the two, in some sense. This is
more than mere representation. Representation, we may say, is
purely oficial. A representative speaks for the people to God, and
for God to the people, but a representative does not have any
power to alter the covenantal arrangement. He has no power in
himself; he is only a speaker.

Roman theology is vague at the point, but clearly gives to the
priest more than merely a speaker's role. The Roman Catholic
priest has in some sense the power to bind things on earth, in the
assurance that God will hearken to him and bind the same things
in heaven. Taking Matthew 16:19 more literally, and as a com-
mand, the protestant “priest” knows that he may only bind on
earth what he knows (from the Bible) has already been bound in
heaven,

Now, the question is this: What was the priest of the Old
Testament like? I think protestants very often assume that that the
Old Testament priest was like a Roman Catholic priest, and that
some great change in administration has come with the New
‘Testament, so that now we have the “priesthood of all believers.”
This, I believe, is an error, Rather, the Old Testament priest was
never a “mediator,” but only a representative. He spoke to Gad for
the people, and to the people for God, but he never had any
power to negotiate. Moreover, there was a priesthood of all
believers in the Old Covenant just as there is in the New—after
