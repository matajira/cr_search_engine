160 The Sociology of the Church

Sickness is a crisis that generally causes people to reassess
their lives—leading to what we are calling conversions (renewed
faith in Christ), The sacrament of Unction was designed to pro-
vide a place for pastoral ministration in this time of need. While
protestants again don’t call this a sacrament, protestants do often
obey James 5:15 and anoint the sick.

But how about the daily conversions, and the crises that come
from time to time, and the hidden “stage” changes that we under-
go? The old church set up the confessional to provide pastoral
care for this: the sacrament of Penance. People would come to the
pastor and talk over their problems in the confessional box. It is a
little enough known fact, but the Protestant Reformers tried to re-
tain the practice of confession in the church, because they saw it
as a healthy way to minister to the people (see James 5:16). Prot-
estants generally have not worked out a good way to deal with
this, but the rise of the modern counselling movement in protest-
ant circles is an attempt to help people with the crises and needed
conversions of life.

Food for thought? I think so.

Along these same lines, one protestant substitute for the con-
fessional, in America at least, has been the rededication service.
By having a week of special meetings annually, the Baptistic
churches provide an opportunity for persons in crisis, or who have
moved to a new stage of maturity, to externalize this crisis in a
ritual of rededication to Christ. Unfortunatcly, the Baptist
theology of conversion often comes into play here, and people
tend to think that they were not “really” Christians until the day
they “walked the aisle.” All the same, this is another way in which
the church has provided opportunities for people to handle the
crises and changes of life.

Rather than ridicule these customs (Catholic and Baptist), we
Reformed Christians ought to ask whether or not there is some-
thing to be learned from them. What regular means do we pro-
vide in our churches for people to approach, with ease, their
pastors and ask for serious counselling? Both the confession box
and the rededication service provide situations wherein people
can feel free to discuss their problems and change their lives, Un-
