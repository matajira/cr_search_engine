3

THE SOCIOLOGY OF THE CHURCH:
A BIBLICO-HISTORICAL APPROACH

In this essay we are concerned to investigate the relationship
of Israel to the nations under the Old Covenant, and how this re-
Jationship compares with that of the church to the world under the
New Covenant. My purpose is to challenge directly the assump-
tion that Israel were the only saved people during the Old Cove-
nant period. I assert that while Israel alone were priests, God gen-
erously redeemed many other people before the coming of Jesus
Christ. It is the burden of this essay to demonstrate this thesis
from the Scriptures, although perforce this investigation must be
rather tentative and preliminary.’

Looking at the Old Covenant we find, as we usually do, that
the situation was much more complex then than it is today. There
es/sacraments and rites of initiation

 

 

were many different sacri
(cleansing, circumcision, cutting the nails and hair of a war-bride,
etc.). Now there is but one of each. Sociologically, there were
Israclites, nations, Levites, priests, and the High Priest, not to
speak of Nazirites and others. Now there are just special officers
{elders}, general officers (laymen), and unbelievers. A considera-
tion of how things were then, and how they differ now, should

 

help us to avoid mistakes in our practice and organization.
Before looking at the matter in detail, let us take a general

1, Throughout this chapter there are references to “sabbath enthronement,”
The reader is referred to my study, Sabbath Breaking and the Death Penalty: A Theo-
logical Investigation (L'yler, TX: Geneva Ministries, 1986) for more on this.
Chapter 2 of that study repeats a great deal of what is contained here, but with a
focus on the sabbath rather than on the sociology of the church.

 

83
