Reconstructing the Church 47

lacks something, something needed for his kingly task. God pro-
vides what man lacks: a helper fitted for him, a queen.

Second of all, God teaches man about the priestly, guarding,
sacramental eating task. He brings an animal to man, for man to
guard against. The animal assaults the wife, offering a demonic
substitute for the sacrament. The man guards the wile, rejects the
animal, and has a sacramental meal with God, feeding his wife.
From this, the man learns that he lacks something, something
needed for his priestly task. God provides what man lacks: a robe
of judicial authority.

Of course, this is not what happened. Man failed the priestly
task. He stood by and permitted his wife to interact with the ser-
pent, He failed to guard her, or the garden.* He permitted her to
partake of the table of demons. He received instruction from her
mouth, and food from her hand, the reverse of the proper order,

Now, the important thing to note at this point is that the
woman was not present when the man entered into the kingly
task, She was brought in to help him with it, making her a queen.
But, when the test regarding the priestly task came about, it was
precisely in terms of whether or not the man would guard his wife.

We have to note that the Bible repeatedly says that Eve was
deceived (1 Tim 2:14; 2 Cor. 11:3). She was not constitutionally
created to be able to guard the garden, and she is not blamed for
the fall. But, when Adam is called on the carpet, he advances
from failing to guard his wife, to attacking her openly, In this,
Adam totally reverses the relation he should have, and becomes
the precise antithesis of what he was to symbolize: God's relation
to His Bride.

Are women pricsts then? No, at least not in this ultimate, spe-
cial sense. But what about the “priesthood of all believers" What
the Reformers meant by this phrase is that any person can and

35. For an extended defense of this interpretation, see James B. Jordan, “Re-
beltion, Tyranny, and Dominion in the Book of Genesis,” in Gary North, ed.,
Tactics of Christian Resistance, Christianity and Civilization No. 3 (Tyler, TX:
Geneva Ministries, 1983).

36. The connection between the woman and the garden, as environments for
man, runs all through the Song of Songs.
