Sociology: A Biblico-Historical Approach 87

ren wilderness. The world already had an initial, natural glory.
The Garden was the most beautiful, glorious place in the world. It
was the sanctuary, where God and man would meet on the sab-
bath day, By letting man meet with Him on the sabbath, God was
promising that man would someday finish his labors, as God had
His, and enter fully into God’s own rest (Gen. 2:1-3),

Thus, like any good father, God gave Adam a head start. The
Garden was, partially at least, a special model of heaven on earth.
Outside the Garden was the land of Eden. Part of man’s project
would be to extend this Garden into Eden, glorifying all of Eden
into Garden. Simultaneously, however, man would be glorifying
the Garden itself, moving it from glory to glory, according to the
degrees of glory (2 Cor. 3:18). How would he do this? By taking
hold of the creation, giving thanks for it (thus orienting his labors
toward God), breaking down the creation and reshaping it, and
then sharing the fruits of his labors with others, who would then
evaluate them and enjoy them.? For instance, man would take
grain, break it down into flour, mix it with other things, and bake
bread. He would engage in the same general process turning
grapes into wine. He would also extract gold and jewels from
rock, to adorn the Garden.

Now, in order to do this he would have to make a trip down-
stream to Havilah, where the gold was. Thus, the glorification of the
Garden would not be possible without assistance from the down-
stream areas. Neither the Garden nor Eden was self-sufficient. It
would not be possible to glorify one part of the world (Garden)
without beginning to reprocess and glorify other parts. Glorifica-
tion would be a holistic process.

Just as Havilah would contribute gold to the Garden, so the
Garden would contribute things to Havilah as well. In particular,
the heaven-model would be brought by Adam's descendants from
the Garden into other lands. And, along with that, seeds from the
Garden would be planted in other lands. In timc, new Trees of

3. Lhave discussed this sequence of actions elsewhere in this volume; see pp.
35-36. See also my study, “Christian Piety: Deformed and Reformed,” The Geneva
Papers WA {available for $4.00 from Geneva Ministries, Box 131300, Tyler, TX
75713)

 
