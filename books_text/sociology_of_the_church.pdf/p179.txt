The Effective Church Splitters Guide 165

 

In either scenario (simple or tough), you need to make a prac-
tice of spending some time with the church’s rulers. Give them ad-
vice, Try to find arcas of “looseness” or “harshness” where the rul-
ers already have strong opinions, and advise them to change their
ways. They won't heed you, and this is fodder for the future. You
can then honestly say that you tried to work things out, but it was just
impossible.

Another benefit of spending time advising the elders is that it
makes you look like one of the boys. You look like a leader in the
eyes of the people, and soon you will have a group for which you
can be spokesman.

In time (about a year to eighteen months), you should be in a
position to make a move (this is simple scenario). You simply
bring your malcontent group with you, and tell the rulers that
these people and you don’t think that the church is doing right,
and that they want to start a new church, and they have, surpris-
ingly, chosen you as the leader. If the elders are wise, they may
just let you go. They don’t want malcontents in their midst either.
If the elders arc less experienced (which we assume here), they
may be very angry. ‘They'll accuse you of “sheep stealing” and
other bad things. You need to be able to say with a “clear con-
science” that you never sought this honor; rather, these poor un-

 

loved people sought you out.

Now, the elders will realize and be angry that you have sprung
this on them overnight. You need to be able to say “I warned you
about such and such a problem.” That’s why you need to spend
time with the elders beforehand, “warning” them about the prob-
lems that you have perceived (and are stimulating) in their nice,
warm, cozy church.

No church splits without hostility, because people feel be-
trayed, and because a church covenant is kind of like a marriage
covenant. All the same, now is the time for you to put on the ma-
ture act, and admonish everyone to “separate in peace.” That may
be a farce, but you want it to look as if you were the peaceloving
one.

Now let’s go to the tougher scenario. In this church the elders
have had experience with guys like you before. As you chafe
