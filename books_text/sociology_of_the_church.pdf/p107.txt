Sociology: A Biblico-Historical Approach 93

Priest of the throne land. His descendants would be a nation of
priests, who would maintain the Eden sanctuary on behalf of the
all the downstream nations. Who would it be? The first candidate
was Lot, but God removed his name from consideration (Gen.
13). The second candidate was Abraham’s chief servant, but he
was not to be the one either (Gen. 15). The third candidate was
Ishmael, but he was set aside (Gen. 17), Rather, it was Isaac, the
miracle-born son of the resurrected dead womb of renamed par-
ents, who would be the seed, and whose descendants (half of
them) would maintain Eden and the sanctuary.

Third, Abraham needed influence, When Pharaoh attacked
him, God magnified Abraham in his eyes (Gen. 13). When Che-
dorlaomer and his men captured Lot, we find Abraham’s influ-
ence magnified again (Gen. 14), When Abimelech attacked, God
so magnified Abraham that only his intercessory prayers could
avert God’s curse on Philistia (Gen. 20). Here we see Abraham as
a priest to the nations. Accordingly, the Philistines make covenant
with Abraham (Gen. 21:22ff.), which is a sign of their conversion.
Finally, we see how great Abraham became, and how great his
Edenic influence, in his dealings with the Hittites in Genesis 23.

Ishmael

Now, let us trace this Eden/Havilah duality in the lives of the
Patriarchs, When Ishmael was cast out, it was not because he was
reprobate. In fact, Genesis 17:20-21, God promised to bless Ish-
mael, though the covenant would be established with Isaac. In
Genesis 21:20, we read that “God was with the lad,” and there are
other clearly salvific promises throughout this passage. Ishmael
was regenerate, elect, saved, but he was not to be the Eden sanc-
tuary priest.? The covenant was not made with him, but with

7. Our presuppositions have everything to do with how we interpret texts such
as these. The modern evangelical view that God saved virtually nobody during
the Old Covenant period causes most evangelical commentators to assume that
Jonah’s Nineveh was not really converted, that Joseph’s Pharaoh did not really
bow the knee to God, etc. Similarly, in terms of this prejudice, the promises
made to Ishmael are seen as only “common grace.” This whole matter of common
grace is highly problematic, since it really is only a backdoor way of reintroduc-
