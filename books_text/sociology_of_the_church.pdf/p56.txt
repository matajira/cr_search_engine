42 The Sociology of the Church

stand it until a man explained it, for “how will they hear without a
preacher?” (Acts 8:31; Romans 10:14). It is an amazing truth that
God involves men in His work. He normally gives not just two
witnesses, but three. ‘lhe fact that the saints, in their persons, arc
1 is an important theme in

 

a revelation of God and of the gos
2 Corinthians (especially 2:14 - 4:6).
In a general way, all members of the royal priesthood are

 

church rulers. All sit on the Divine council. ‘hat council is seen
initially in Genesis 1:26, and had then only three members. Had
Adam persevered, he and Eve would have been the next two. We
see in Genesis 18:17-33 that Abraham was a member. The Biblical
word for council-member is “prophet.” A prophet is a member of

 

the council who brings the decisions of the council to men, or who
prosecutes the covenant lawsuit against men before the council.
Abraham is called a prophet for just this reason in Genesis 20:7.
In meetings of the council on carth, all have a voice, but in differ-
ent ways and at different times. Thus, in certain weighty matt
of doctrine, or embarrassing matters of ethics, only elders meet in
the council (Acts 15). At church council meetings where judgment
must be passed, only men may speak (1 Cor. 14:34 in the context
of y, 29). At other meetings, women may speak, but need to have

   

a sign of authority on their heads (1 Cor 11:5).

This is th al work of the council, and because il is a
heavenly council, its members are called “stars” (Phil. 2:15).
There arc, however, also special members. ‘hese are the guard-
ians of the church, the elders. They are called stars in Revelation
1:20, and also called angels. Why angels? Because they have taken
the place of the cherubim at the door of Eden, and they have the
special use of the keys committed to them. (Indeed, in the Eastern
Church liturgy, there is a hymn that begins, “Let us who mystically

 

 

 

represent the Cherubim. . . .”)
According to Exodus 18:21, the elders of the church are sup-
posed to be arranged in hierarchical ranks.* The angels of Reve-

34, For some odd reason presbyterians, who make: the most fuss about elder
tule, generally ignore the Biblical prescriptions tor the hicrarchical organization
of the eldership. The Reformers, however, were not opposed to bishops. See
