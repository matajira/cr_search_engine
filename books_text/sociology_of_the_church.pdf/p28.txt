14 The Sociology of the Church

Moreover, anybody who professes Christ can be not only a
communicant but also a voting member of the church. The latter
tends to dissolve integrity by opening the possibility of many peo-
ple making decisions in the church who are not aware of sound
doctrine. The check on this comes in the special officers (elders),
who are supposed to know the faith more perfectly, but this again
is compromised when “ruling elders” are elected only on the basis
of being notable persons, and not on the basis of Spiritual and
doctrinal maturity. We might pursue this, but it should be clear
that catholicity and integrity arc working against onc another in
this system. The more catholic we are, the more diluted we be-
come, and the more integrity we try to have, the more exclusive
we become.

(4) The fourth option is to combine a strong commitment to
integrity with a strong commitment to catholicity. Here, integrity
is committed to the province of the special officers, and anyone is
permitted to come to the Table of the Lord whe (a) has been bap-
tized, (b) professes Christ as Savior and Lord, and (c) is under
some ecclesiastical government. Such church members may be
very ignorant of the doctrine of the church, and may be in consider-
able error, but as long as they are willing to listen to the preached
voice of the Master, they are permitted to share at His Table.’

This position makes a distinction between voting and non-
voting members. Children, people who are new to the faith, people
who have not come to a knowledge of various fundamentals of the
faith, and persons under chastisernent for some sin clearly will not
be permitted to vote in the selection of elders to govern the church.®

‘Phe guardians of orthodoxy in the church are not the people at
large, but the special officers. Their integrity is in turn guarded by

 

 

 

7. don’t intend this to be taken in some simplistic scnse. A person is initially
admiited to the Table based on these three qualifications. Should he show himself
in moral sin, or an avid advocate of some perverse doctrinal viewpoint, discipline
would be in order.

8. Biblically speaking, the age of voting, of coming into the assembly, is 20;
see Numbers 1:3. As regards new converts, Biblical data indicates that back-
ground should be taken into account; see Deuteronomy 23:3-8. The power of the
New Covenant is such that, ] believe, it is not necessary to wait several genera-
tions; but perhaps the wait of a sabbath period of six years would be advisable.
