216 The Sociology of the Church

50, or 2:5. I am not aware of the occurrence of this shape in the
Tabernacle, but if we form a brigade of ten battalions, again ar-
ranging them five in a rank, two battalions deep, we get another
square 100 by 100.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

eoocoooc0h q
eo0000coedr
eo0000cdogd
oeooo00ccod q
oooco0ooo0ng
PLATOON
12 COMPANY
ti
A —
BATTALION peicaox
BY tt

Of course, the Bible repeatedly tells us that God’s house is
built of people, that we are living stones in His temple, etc. It is
interesting, however, to sce how this was signified to the people of
old. The arrangement of the army in groups of five explains the
prominence of the number five in the dimensions of the Taber-
nacle. Five seems tu be the number for preeminence and power,”
Here again, to sum up, we see the humaniformity of the house of
God, and thus the appropriateness of the use of the cross shape in
church architecture.

2, See the discussion of this in my book, The Law of the Covenant: An Exposition of
Exodus 21-23 (Tyler, TX: Institute for Christian Economics, 1984), pp. 261ff.
