Christian Zionism and Messianic Judaism 177

are not Jews at all; They are Khazars.3 The Khazari race seems to
lie behind the Ashkenazik Jews of Eastern Europe. This kind of
assertion can, of course, be debated. The real problem in the dis-
cussion is the notion that Jewishness is a blood or racial phenome-
non, It is not.

Biblically speaking, a Jew is someone who is covenanted into
the people of the Jews by circumcision, for better or for worse.
When Abraham was commanded te circumcise, he was told to
circumcise his entire household, including his 318 fighting men
and his other domestic servants (Gen. 14:14; 17:10-14), Competent
scholars imagine that Sheik Abraham’s household probably in-
cluded at the very least 3000 persons. These servants multiplied
as the years went by, and Jacob inherited them all (Gen. 27:37).
Although only 70 from the loins of Jacob went down into Egypt,
so many servants went along that they had to be given the whole
Jand of Goshen in which to live.

All these people were Jews, but only a small fraction actually
had any of Abraham’s blood in them. Later on we see many other
people joining the Jews; indeed, the lists of David’s men include
many foreigners, of whom Uriah the Hittite is but the best known,
What this demonstrates is that covenant, not race, has always
been the defining mark of a Jew (as it also is of a Christian). Gen-
ealogical records were kept for the immediate family, of course,
since the Messiah had to be of the actual blood of Abraham, and
later of David; but this could not have applied to more than a
fraction of the total number of people.

Thus, the Jews are those who claim to be Jews, who are cove-
nanted with the Jews. The Khazari converted to Judaism in the
Middle Ages, and they are Jews, British-Israelite rightist non-
sense to the contrary.* (Of course, modern Zionists do not under-

 

3. On the Khazars, see Arthur Koestler, The Thirteenth Tribe (New York: Ran-
dom Housc, 1976.)

4, British-Israelitism claims that the Anglo Saxon people are the true Jews,
and thus inherit the covenant promises by means of race alone. This weird,
stupid idea is promoted by the Armstrong cult, but also crops up in right wing
Christian cireles. For a fine analysis and refutation of this viewpoint, see Louis F.
I’ Boer, The New Phariseeism (Columbus, NJ: The American Presbyterian Press,
1978).
