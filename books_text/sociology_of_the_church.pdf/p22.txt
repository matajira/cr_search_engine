8 The Sociology of the Church

By these two means, dispensationalism undermines the prac-
tical authority of much of Scripture as thoroughly as any liberal
theology docs. Giving lip service to inerrancy is certainly inconsis-
tent under these circumstances. Are all dispensational preachers
and churches, then, anti-Christian? Of course not. Just as we saw
with liberalism and neo-orthodoxy, many churches are far better
than their official theories, And, happily, in recent years, dispen-
salional theologians have become more sophisticated, and are
willing to admit that Christians should pray the Lord’s Prayer,
and take other pre-Pentecostal portions of Scripture more seri-
ously as authoritative for the present day.

Pentecostalism also all too often has little use for Scripture.
The emphasis in the movement as a whole is upon direct, mystical
experiences with God (roughly defined). The stimulation of
glands has priority over the reformation of life. This is most pro-
nounced in the various healing cults and “name it and claim it”
sects, which are all over the airwaves today. This is nothing more
than medicine man religion, and scarcely Christian at all. It has
little more relation to Christianity than do the “cargo cults” of
Polynesia.> Not all charismatics are this bad, of course, but the
tendency is there in all tov many of them. ‘The effect is that the
Word of God is rendered null and void.

The third conservative group that negates the Bible is the
bapto-presbyterian group. These do so by means of their preach-
ing and liturgy. That may seem a strange charge, but the fact is

 

 

   

   

3. The cargo cults make airplanes out of wood and sacrifice to them, in the
hopes thar the planes in the sky will be drawn back down to them, and give them
more cargo (as they did during World War II)—cargo like liquor, good food,
clothing, etc, This is all mixed up with “Jesus,” as a regult of confusion with white
missionary endeavors. ‘The pentecostalists endulge in self-stimulation in order to
try to attract the blessings of their version of the Holy Spirit, and consequent
cargo (such as a new Eldorado, lots of money, a new woman, ete,). Te watch
American cargoism in action, simply tune in your “Christian” radio or television
station. To read about Polynesian cargoism, two studies are: Edward Rice, John
Frum He Gome: Cargo Cults and Cargo Messiahs in the South Pacific (Garden City, NY:
Doubleday, 1974); and the chapter on “Commodity Millennialism” in Bryan R.
Wilson’s outstanding Magic and the Millennium: A Sociological Study of Religious
Movements of Protest Among Tribal and Third-World Peopley (New York: Harper &
Row, 1973)

  

 
