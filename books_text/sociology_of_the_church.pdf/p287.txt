Triumphalistic Investiture 273

all, anyone could pray to God.

Thus, I assert that the New Testament elder has the same
basic position as the priest of the Old Covenant, as a representa-
tive of God and of the people. Let me explain further how I think
that works out. First, as God’s special representative, the elder has
a formal role of speaking God’s Word to the congregation. Sym-
bolically, this is the ministry of the Groom to the Bride, and thus
no woman may ever take up this role (1 Cor. 14:34; 1 Tim. 2:12). I
believe that the elders may delegate the role of liturgical officiant
to any man in the church, but not to a woman. Possibly then we
should entertain the notion that any man may wear liturgical
vestments while he performs the role of officiant. If we say this,
then the tab collar would be a sign of the office of elder or over-
seer, and worn only by them, while liturgical vestments might be
worn by any man who conducts worship.

Second, as a representative of the people to God, the elder is
not in a position of being sole representative. Under both the Old
and the New Covenants, any believer may approach God on his
knees and be heard at any time. This does not eliminate the fact
that there arc also special times appointed by God fur the
ceremony of public worship, and that there are certain persons
appointed by God to oversee (and normally to lead in) the public
ceremony of command-performance worship. These were the
priests of the Old Covenant, and the elder-bishops of the New.

‘There is a difference: Under the Old Covenant, public formal
worship was highly restricted. Only the high priest might come
into the Holiest Place, and then only once a year, not without
blood, ete. Now, however, every Christian may come into the
Holiest Place (heaven itself) in public worship. The Old Cove-
nant priest represented an absent congregation, kept away by the
cherubim with flaming sword. The New Covenant priest (elder)
represents a present congregation, readily admitted to God’s
throne room,

Well, then, why have representatives at all? As one corre-
spondent wrote me, interacting with the original publication of
this essay, “It would seem that the New Testament pattern would
be not one man representing (symbolically) between God and the
