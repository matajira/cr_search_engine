God's Hospitality and Holistic Evangelism 251

ineffectual, The gains to the church from this method are mini-
mal. That is not to say that God never blesses visitation evangel-
ism, but that it is not a very strong way to witness.

Now, if I have a neighbor family over to my house, I have the
opportunity to display Christian hospitality to them from the mo-
ment they cross my threshold. I am in control of the situation, and
it is a Christian environment. They observe Christianity in ac-
tion. They eat my food. They observe the devotions conducted at
my table. Without invading their privacy, I can explain Christi-
anity to them. And even if I do not give them the Gospel with a
direct verbal appeal, it is set before them unmistakably in all that
they experience while in a Christian home. The advantages of this
method of evangelism are obvious.

Of course, this means that I must have my Christian house-
hold in order. Probably the main deterrent to hospitality evangel-
ism, and hospitality in general, is the fact that the Christian fam-
ily sees itself as too disorderly and not a good witness. An untidy
house with a sloppy housekeeper will effectively keep the covenant
head of the home from inviting people over. Bickering parents,
undisciplined children, poor leadership by the father, are all too
often found in Christian homes as well as in pagan ones. The
Christian household, however, must analogically reflect the order
found in the kingdom of Christ. Christians must honestly face up
to the disorder in their own lives and homes, for judgment begins
at the house of God. Then hospitality will be a real possibility.
Most children will act up when company is visiting the home,
because the children are made to feel insecure by the attention the
parents are giving to outsiders. The issue is not whether children
act up or not, but whether the outsider will see Christian parents
handling the problem in a Christian manner (e.g., giving extra
love to the kids). The churches must double their efforts to raise
up orderly Christian homes, as a prelude to hospitality in general
and hospitality evangelism in particular.

Since elders should be the leaders in the church in her imita-
tion of God, no one should be an elder who is not given to hospi-
tality. The diaconate, the apprenticeship for the elders, is charac-
terized by “waiting on tables,” or training in hospitality. Because
