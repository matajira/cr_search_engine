xii The Sociology of the Church

Gary North, Michael Gilstrap, Craig Bulkeley, Robert Dwelle,
and David Chilton.

ee

As a creature, man in intrinsically incapable of knowing any-
thing exhaustively, but as a creature of time he inescapably knows
things progressively. According to sin and grace, knowledge
either progresses or regresses, but never remains the same. This
elementary observation implies as a significant corollary that
man’s understanding of the church of God must increase, and to
some extent alter, over time.

As a sinner, man has an inbuilt tendency to misunderstand
and pervert the revelation of God. Even as regenerate, men still
have this tendency, The Christian man, thus, may easily be and
often is misled with respect to his understanding of the church.
For this reason, the progressive corporate sanctificalion of the
church in history dictates reevaluation and alteration in the
church’s self-concept.

Thus, because of man’s creaturcliness and sinfulness his un-
derstanding of the precise nature and definition of the church of
God requires an ever-sharpening focus. We
pect to set forth a description of the church that will be valid in all
of its particulars for all time, and we cannot expect the men of
previous generations to have done so either. Just as the individual
Christian, as he grows in grace over the years, acquires an in-
creasing understanding of his uniqueness and definition under
God, his name if you will, so also does the church. Just as the in-
dividual may and inevitably will have to correct some erroneous
self-evaluations over the years, so also the church.

This volume seeks only to set out some lines of thought along
which, it scems to me, the church could profitably reflect in seek-
ing to resolve some of the problems currently facing her. The par-
ticular problems addressed here are those that concern the
church’s structure and relation to the other aspects of creation and
society, It is in this broad sense that we are concerned with the
“sociology” of the church, though inevitably such a concern draws

   

 

annot, therefore ex-

 
