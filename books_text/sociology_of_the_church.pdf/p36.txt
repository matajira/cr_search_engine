22 The Sociology of the Church

comes something more than merely a collection of people, and it
transcends their differences. Not until the Episcopal church began
ordaining women and homosexuals, and openly denying the
faith, did any schism come.

This makes for a strong church, if a rather closed one, There
are a lot of analogies to the Jews here, not least in the failure of
either group to evangelize for itself, (Elites seldom feel any need to
evangelize.) Provided the various theologies tolerated in the
church are each basically orthodox, and in line with the historic
creeds, there is no problem with having a strong church. The
problem comes when liberalism creeps in, and of course the Epis-
copal churches today have rotted out as much as any others have.

Doubtless Episcopalian readers have been amazed at how I
have described their church. Doubtless if I were an Episcopalian
rather than a Presbyterian, the grass would look greener on the
other side. Doubtless what I have written here is more an occasion
to sct out some of my own thoughts than it is an accurate descrip-
tion of Episcopalianism. We ought, therefore, in closing to look at
the glaring problem in Episcopalianism,

That problem is the lack of discipline in that body. Do Episco-
palians ever declare anyone excommunicate? (Nobody else does
either, but for different reasons.) Episcopalianism has been tied to
the cultural clite, with the result that Episcopal churches often can
become little more than religious country clubs. The cart (the
elite) begins to pull the horse (the church), This is the danger and
corruption of Episcopalianism.

The answer to this problem is seen only in the Roman Catho-
lic church, That body alone has retained a ministry to all levels of
society. The result is that no particular cultural group controls it.
A second result is that there is no reticence about disciplining
apostates,

Clearly, the reconstruction of the Christian church must take a
catholic (though reformed) approach. The point of this essay is
that there are things in evangelical protestantism today, which is
basically Presbyterian, that prevent this wholistic type of ministry.
In particular, if we want to capture the leadership of society, we
have to take seriously thuse things that enabled the Episcopalians,
