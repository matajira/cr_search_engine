Thoughts on Modern Protestantism 127

orthodoxy. It is the “mainline” churches, with their blatant denial
of the fundamental articles of the faith, that are on the fringe of
the historic church. (It rather prejudices the discussion, doesn’t it,
to refer to these groups as “mainline”?)

But what about the fine liturgies in these churches? Isn’t that
in the middle of historic orthodoxy? Not at all, The mere perform-
ance of outward rites is not a criterion of orthodoxy or catholicity.
The gnostics of the ancient world had really nice liturgies, and so
do modern gnostics. Gnostics use a lot of language in common
with true Christians. It is only to the superficial eye that gnostics
appear to have a connection with the historic church.

In fact, the performance of the cudfus of worship for its own
sake is the essence of mystery cult religion.6 In Christianity, it is
Truth that calls men together to worship. Worship is a response to
Truth, Thus, the meagre “widow's mite” worship of an evangelical
church is acceptable in God’s eyes, while the glories of a gnostic cult
only add to the damnation of its participants. This is no reason, of
course, not to work to enhance the glory of true Christian worship,
but we must not forget that liturgical splendor is in itself no criterion
for the selection of a church—Thomas Howard to the contrary!”

Thus, while liturgically the humble evangelical church may
appear on the fringe, it is not, for Truth rather than glory is the
first criterion of catholic orthodoxy.

The True Church Syndrome

The fact that visible orthodoxy is seriously impaired in “main-
line” churches, as well as in the Roman Catholic and Eastern

5. Gnosticism is the great counterfeit of Christianity. Gnosticism replaces the
facts of the history of creation and redemption with philosophical ideas. ‘he Apos-
tles' Creed, in that it simply recounts history, is the premier anti-gnostic dacu-
ment of the church. ‘I'he most famous modern gnostic was Kar! Barth, and his
followers are legion

6. On this, see Alexander Schmemann, Introduction to Liturgical Vheology (New
York: St. Vladimir's Seminary Press, 1966), and my essay on “Christian Piety:
Deformed and Reformed,” The Geneva Papers (New Series), No. 1,

7, In a recent Christianity Today interview, Howard explained that his basic
reason for joining the Roman Catholic Church was its institutional and liturgical
glory and fulness.
