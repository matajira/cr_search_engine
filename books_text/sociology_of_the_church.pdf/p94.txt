80 The Sociology of the Church

(2) Parachurch officers must never demand the same degree
of respect or submission as the elders of the institutional church,
This heinous offense is committed by some parachurch organiza-
tions, where an almost monastic degree of submission is required.
Not even the institutional church can require such submission,
though there is nothing wrong with a special temporary vow of
special submission for the purpose of special discipleship. Para-
church organizations should be thought of as businesses with em-
ployees, not as churches with officers.

(3) Teaching conducted in parachurch has no official weight.
It has only the weight of an informal Bible study. The teaching
conducted in the institutional church has official weight, which
docs not mean that the people are required to accept it willy-nilly,
but does mean that they are required to take it with utmost ser-
iousness. The churchmember is to be a Berean (Acts 17:11), study-
ing Scripture to make sure these things are true, but giving them
special weight, Also, the teacher in the institutional church has a
right to expect a special power from the Holy Spirit in his teaching
that the parachurch teacher cannot claim.

We conclude that parachurch evangelistic and teaching orders
are not in themselves wrong. Many such organizations have
sprung up because the institutional church has fallen down on the
job. The proper corrective to present abuses is not to preach the
parachurch organizations out of existence as an evil, but to so out-
shine them that they wither away as their members are drawn into
the institutional church. A couple of years ago the leader of a large
parachurch organization became a Reformed Christian. Suddenly
he dissolved his entire organization, “because parachurch is
wrong.” Instead of turning the organization into a Reformed
group, working with the churches for good, he dropped the whole
thing, leaving his sheep to scatter. Such is the effect of hyper-
institutional thinking on the church and on the people of God.

“May the tithe be used to support parachurch organizations?”
Thave deait with this at length elsewhere." All the tithe is owed to

 

 

 

21, James B. Jordan, “Tithing: Financing Christian Reconstruction,” in Jor-
dan, The Law of the Covenant: An Exposition of Exodus 21 - 23 (Tyler, TX: Institute
for Christian Economics, 1984)
