Part III
RETHINKING WORSHIP: SOME OBSERVATIONS

A tremendous liturgical awakening has begun in the 1980s
among the evangelical and Reformed churches. Publishers have
found a ready market for such books as O Come, Let Us Worship by
Robert G. Rayburn of Covenant Theological Seminary, and two
studies by Robert G, Webber of Wheaton College.? I find myself
in complete sympathy with this movement toward the restoration
of formal public worship, as should be obvious from my remarks
in chapter 1 of this book. Evangelical Christians are the truc heirs
of the catholic heritage, and should begin taking full advantage of
it.

There have been three liturgical movements in the Western
church. The first was the Reformation. At the time of the Refor-
mation, the primitive rites of the church had become almost
totally obscured with overlays of devotional picty that were too
often superstitious. Moreover, there was absolutely no congrega-
tional participation in worship. The service was said silently or
mumbled under the breath by the priest.? What was said was in
Latin, and was thus incomprehensible. There was no proclama-
tion of the Word in the liturgy. The people seldom if ever com-
municated, and then children were excluded, and all the people

1. (Grand Rapids: Baker, 1980).

2. Worship: Old and New (Grand Rapids: Zondervan, 1982); and Warship is @
Verb (Waco, TX: Word, 1985).

3. On the rule that the service be said in silence, sec Louis Bouyer, Euckarist
(Notre Dame: Notre Dame University Press, 1968), pp. 3714.

199
