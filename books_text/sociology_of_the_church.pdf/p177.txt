6
THE EFFECTIVE CHURCH SPLITTER’S GUIDE

You're a pastor, and you've just left your latest church. It is
blown up, and in shreds—but who can blame you for that? You
tried, but the people just weren’t holy enough. Now you've gone
to another town, and joined a local church. You're temporarily
out of the ministry, Your work is ahead of you.

You've got to be careful at this point. Discernment is needed.
In your new church there are bound to be some areas of sin and
looseness that you can exploit, The task before you is to be judi-
cious in selecting just which issues to make noise about. And you
have to do it fast, else when you leave you will not have credibility.

Maybe some of the people, even leaders, of this new church
smoke. Of course, maybe you smoked back in your old church, so
a frontal attack on smoking would not be a good tactic to use. You
might wind up embarrassed, You have to discern quickly whether
it will or won't be credible to say that “the people here smoke too
much.” Of course, who knows how much “too much” is? That’s the
beauty of vague, generalized discontent.

Let’s look at some things you might select to express “grief”
over, shortly after you first arrive. Do people go to see “too many”
movies? Do they have “too many” parties and get-togethers? Or,
equally juicy, do they “not have enough” fellowship? Do the elders
visit the people “enough”? Is there “too light” or “too serious” an at-
titude among the peuple? Are they “too loose” or “too intolerant”
on the sabbath? (This is an especially good one, for every church
on earth is either too loose or too intolerant on the sabbath. You
can have a grand time exploiting this one.)

163
