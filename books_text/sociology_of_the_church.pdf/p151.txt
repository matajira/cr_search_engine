4
THE THREE FACES OF PROTESTANTISM

It is commonplace nowadays to say that the protestant Refor-
mation had two branches. These are called the “magisterial” and
the “anabaptist” branches. What is meant by this is that some of
the Reformers (the “magisterial” ones) looked to the newly emerg-
ent nation states of Europe to promote the Reformation against
the Catholic Church, while others (the anabaptists) were opposed
to the state as well as to the Roman Church.

This way of looking at the Reformation sees it in terms of
church-state relations, Some Reformers wanted to put the church
under the state, while others wanted to drop out of society.
Because the issue of church-state relations is so important in our
own time, we should consider whether or not these are the only
two options available to us.

In fact, this model of the Reformation is not correct, and is
very misleading. From an anabaptist perspective (which is more
and more common nowadays), it might be useful to divide the
Reformers into “anabaptists and everyone else,” but looking at the
problem historically, such a perspective is of little value.

In fact, there were three major trends in the protestant refor-
mation, if we look at it in terms of “sociology.” It is the purpose of
this all-too-brief essay to set out these three trends, and to show
why it is important for the church today to reflect on this matter.

The three faces of Protestantism were, and are, the imperial
or nationalistic face, the sectarian or drop-out face, and the catho-
lic face, The Reformers can fairly easily, though roughly, be
divided into these three groups. There were drop-out anabaptists;

137
