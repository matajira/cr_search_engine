Sociology: A Systematic Approach 55

The “Wave” view focuses on how the thing moves and changes
in time. It answers the question; How much can this thing change
and still remain itself?

There are hierarchies of particles, waves, and fields. That is,
large particles may be seen as composed of smaller particles, large
waves of smaller waves, and large fields of smaller fields,

An important phrase in the preceding paragraph is “seen as.”
We are talking about different ferspectives on a given matter, Each
perspective is valid, but needs to be balanced by others. The idea
that there is only one valid perspective is an effective denial of the
doctrine of the Trinity (denying diversity).

The People of God

Let us apply this first of all to the church as the people of God.
What are the identifying marks of the people of God, considered
as “Particles”? They may be summarized by the moral law and the
fruit of the Spirit. The people of God live a holy life in their call-
ings. They show forth visibly that they are God's people, and not
His enemies. In terms of the “Field” perspective, the people of
God exist in a “Field” with one other group, the enemies of God.
That is, in the “Field” of the world there are two groups: God’s
people and God's enemies. In terms of the “Wave” view, we notice
that the people of God are more or less faithful at different times.
Sometimes they are making progress in subduing all things to
Christ. Sometimes they are being driven back because of their
own sin or because of persecution. They make mistakes and cor-
rect them; they sin and repent; they triumph and go forward.
Sometimes the people of God are so weak morally that it is hard to
tell them apart from the world, but then a revival comes and they
“Wave” into greater visibility,

We said that there are hierarchies among these things, Con-
sidering the church as the people of God, there are various parti-
cles that come into view, such as Christian individuals, familics,
businesses, clubs, schools, states, churches, etc. There are also
various “Fields” that come into view. As noted, the basic “Field” is
people of God versus enemies of God, but the people of God also
