PREFACE

My grandfather, William Henry Jordan, was a Methodist
minister. His father before him, and his before him, were also
Methodist clergymen, and before that, the Jordans were Episco-
pal ministers. My father, Howard S. Jordan, did not go into the
ministry, but instead became a Professor of French Literature,
specializing in the Jansenist period (Pascal, Molitre, Racine).

My mother, born Sarah Kathleen Burrell, came up in the
Southern Baptist Church. She met my father at Salem College, a
Moravian school in Winston-Salem, North Carolina. He was
Head of the Foreign Languages Department there at the timc.
When they were married, they joined the Moravian Church,

In 1949, the year I was born, my father moved to Athens,
Georgia, to assume headship of the Department of Modern For-
eign Languages. There being no Moravian Church there, my
parents became part of the First Methodist Church, where my
father taught a men’s Sunday School class. The Sunday School for
young children amounted to very little, so my parents sent my
brother and me to Sunday School at First Baptist Church.

My parents were distressed at the increasing liberalism in the
Methodist church, and when a Lutheran mission started up a
couple of blocks from our home, they began going there. They
soon joined, and I was baptized at Holy Cross Lutheran Church
(then part of the United Lutheran Church of America, now the
Lutheran Ghurch of America). My father served for years as an
elder, or councilmember as it was termed. My brother and I were

   
 

 

also confirmed in the Lutheran Church.

vii
