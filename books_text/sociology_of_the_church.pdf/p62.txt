48 The Sociology of the Church

should approach God without having to go through any mediator
except Christ alone. In terms of what they meant, they were right.
But, what they should have called it was not ‘universal priest-
hood,” but “universal Bridehood.” The privilege of approaching
God is not a priestly privilege, bur is the privilege of the Mother/
Bride/Daughter.

All the same, women do perform priestly tasks. They do guard
the home. They do instruct their children (and informally they can
instruct men). They do prepare meals and serve them. Are these not
“priestly” tasks? Certainly, but we have to make two distinctions.

The first is the same one we have already made concerning
prophecy and rule. Women are never priests, but pricstesses. A
priestess can only guard under the authority of a priest.

Second, we have to distinguish between the general and the
special. There is a special meal, and special office, in the church.
In connection with these, the priestly task must be performed in
an exclusively masculine fashion, in order that the relationship
between God and His Bride may be set out clearly.

Having noted this, we may now go back and assert the follow-
ing propositions:

Both men and women may perform the task of prophecy in
both the general and special areas. Women may be teachers.

Both men and women may perform the task of ruling in both
the general and special areas, Women may be magistrates.

Both men and women may perform the task of guarding in the
gencral area, but only men may perform the task of guarding in
the special area. Women may not be elders.

How about women as deacons? Impossible, because to be a
deacon you have to be a man. How about deaconnesses, then? No
problem. Both in the Old and in the New Testament, certain
women are sct aside to assist the elders with certain tasks (Ex.
38:8; 1 Sam. 2:22; Jud. 11:40;3? Matt.27:55-56; Luke 8:2-3; Rom.
16:1; Phil. 4:2-3; 1 Tim. 3:11;38 1 Tim. 5:3-10), This, of course, is

37. On Jephthah’s daughter, see my book Judges: Ged’s War Against Humanism
(Tyler, ‘Texas: Geneva Ministries, 1985).

38. If this verse referred to wives, it would not be imbedded in the section on
deacons. It clearly, in my opinion, refers to deaconnesses.

 
