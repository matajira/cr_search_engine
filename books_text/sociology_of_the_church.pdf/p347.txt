Annotated Table of Contents 333

Covenant Bonding, 244
The aspect of Divine life man was created to participate in is covenant life
Sin separated man from God's covenant community life
Consequently, men are separated from one another
When restored to covenant life and cornmunity with the Three Persons of
God, men are also restored to community with other men
Threshold crossings are marked with blood to signify covenant death and
resurrection
Thus, covenant bonding is a resurrection phenomenon
The idea of community is inseparable from that of resurrection life
Men are to bring their whole households with them into the kingdom
Infants belong at the table with their parents
Children ate the manna in the wilderness, which signified Christ
Newly purchased slaves were circumcised into the covenant, apart
from faith
American Christianity is overly individualistic on this point
Baptism is God's claim on children
Sarnson was considered part of the covenant community even in the
womb
In an age of family breakdown, the church can be a healing force if she
recovers this vision
Analogical Hospitality, 250
Going into the home of a non-Christian is not the ideal way to share the
gospel
Christian hospitality is the best method
To witness to unbelievers in an alien environment (a pagan home) takes
training and skill
Failure to understand this has produced truncated gospel tracts
Summary and Applications, 253
The modern church has confused preaching and teaching
Worship services do not belong on broadcast media
End Note on Office in Scripture, 254
Three-office view not found in Scripture
All elders have the same office and powers, though not all the same
responsibilities
The Gutenberg revolution tends toward equality among elders
The Biblical offices are: ruler in the state and ruler in the church
Deacons are apprentices and assistants to elders
The age of ordination to the cidership is 30
‘The age of separation to the diaconate is 25

12, Triumphalistic Investiture ...... : eee : 259
Clerical Garb, 259
Americans expect their pastors to dress in special ways
