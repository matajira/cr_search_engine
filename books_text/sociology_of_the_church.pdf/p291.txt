Trumphalistic Investiture 277

also arrays His bride in glory and beauty. All of us could wear
glorious apparel, but it would be pretty expensive. (This, though,
is why people wear their “Sunday best” to worship.) In worship,
the officiant represents the people, the Bride. Let him be gloriously
arrayed, then, This is part of the public witness of the church be-
fore the watching world.

Presbyterians, with their heritage of postmillennial triumphal-
ism, should be in the forefront of restoring splendor to the church,
which is His Throne and Bride. Let the nations tremble!

9. Practically speaking, we should be careful not to introduce
too much, but bring the people along. In the area of vestments, a
simple white alb or surplice (the white robe of the Bible) and a col-
ored stole (the strip of cloth that represents the “easy yoke” of
Christ’s service, the colors variable with the liturgical season)
should be sufficient. But we should not be afraid to think about
more glorious apparel at some later time, The Bride is most cer-
tainly and triumphantly to be adorned, and this is most aptly in-
dicated by the vestments worn by the officiant while he leads in
worship.

Conclusion

The church needs more governmental and institutional visi-
bility in America, as she confronts the state and attempts to safe-
guard Christian liberties. The reintroduction of clerical garb and
liturgical vestments would be very helpful along these lines, in my
opinion. The use of such special clothing is supported by Scrip-
ture, and does not contradict historic protestant teaching on the
subject, when we take historic protestantism in context.
