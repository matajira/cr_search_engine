262 The Sociology of the Church

more common in both Catholic and Protestant circles, though one
sees the ring collar sometimes in both circles also.) Recently, due
to the influence of the Liturgical Movement, the medieval black
has been supplemented by shirts of various colors.

The ring collar makes a particularly appropriate uniform be-
cause the “dog collar” is the mark of a slave. The clergy are to
scrve God's people as servant priests to the royal priesthood.
Those who are to rule, must do so by becoming servants, said our
Lord (Mark 10:43-44).

Vestments

Properly speaking, a vestment is a special garment, generally
some form of cape, worn during the celebration of Holy Com-
munion by the officiating elder (priest, presbyter, minister, eldcr,
pastor, bishop, you-name-it). What we usually think of as vest-
ments —the black robe (cassock), with a white shirt over it (sur-
plice), and with a colored strip of cloth around the neck and down
the sides (stole)—are not properly speaking vestments but are
called “choir” atlire. For our purposes, however, let’s just stick
with ordinary, non-specialist Janguage, and call them all vest-
ments. For our purposes, a vestment is a special costume worn by
the elders during worship, but not every day.

Why wear special clothing? Because the elder, during the wor-
ship service, carries out a symbolic role. When he prays for the
people, he symbolizes Christ as the Head of the church, praying
to the Father. Thus, his garments of glory and beauty remind the
congregation that the prayer offered up during worship is not only
the prayer of a sinful man, but it is also the prayer of Christ before
the Father. Our own prayers would not be heard if they were not
offered in union with those of the Son (see Revelation 7:3, 4).

Second, when the elder reads Scripture and proclaims the
Word to the congregation, he symbolizes Christ the Husband of
the church, instructing the Bride. Here again, vestments of glory
and beauty serve to remind us that we are not listening to the
mere opinions of men, but to the very Word of God.

Thus, vestments remind us, and reinforce to us at a deep psy-
chological level, that the man conducting the service is not just
