302 The Sociology of the Church

Scripture does not use “church” and “kingdom” in this way. In
Scripture, “church” and “kingdom” have the same referent (the
same object in mind), but entail different senses (different aspects
of this object). “Church” focuses on the community, ‘kingdom’ on
the rule of the Lord. Both terms have a broader “people of God”
and a narrower “sabbatical” use.
