Reconstructing the Church 29

Second, the Reformers wanted a return to Old Catholic
forms, as they understood them. A reading of the liturgies they
wrote shows this.?) Though all of the Reformers tended to over-
react against anything that reminded them of Italo-Papal imperial
oppression, they were not so “anti-catholic” as to reject the early
church. Soon, however, sectarian reaction against anything that
“smacks of Rome” overwhelmed their concern.

Third, the Reformers wanted participation in worship from
the whole priesthood of all believers. They wrote dialogue litur-
gies in which the people had many things to say and sing. They
had their congregations singing, for instance, the creeds, the Ten
Commandments, and the Lord’s Prayer. Soon, however, the
strength of the Medieval devotional tradition reasserted itself—
the “low mass” tradition in which the people only sat and watched
and listened, while the minister did everything. This Medieval tra-
dition was the essence of the Puritan view of worship. In worship, the
Puritans departed from the desires of the Protestant Reformers.

It is important to understand that although the Puritans did
uphold the theology of the Reformers, they rejected the Refor-
mers’ views on worship at some crucial points. After the Puritan
Revolution failed and Charles II came to the English throne,
there was a conference at Savoy between Puritan Presbyterian
churchmen and the newly restored Anglican bishops. It is very in-
teresting to note what the Presbyterians proposed. They wanted
“40 omit ‘the repetitions and responsals of the clerk and people,
and the alternate reading of Psalms and Hymns, which cause a
confused murmur in the congregation’ : ‘the minister being ap-
pointed for the people in all Public Services appertaining to God;
and the Holy Scriptures . . . intimating the people’s part in pub-
lic prayer to be only with silence and reverence to attend there-

 

goes on to note that, in rejecting such things as prayerbooks, kneeling, the Chris-
tian year, and weekly communion, “they were not in fact reverting to Calvin, but
departing from him, though . . . it is doubtful whether they realised this.”
Packer, “The Puritan Approach to Worship,” in Diversify and Unily. The Puritan
and Reformed Studies Conference Papers (Kent: PRSC, 1963), pp. 4, 5.

21, See the collection in Bard Thompson, Liturgies of the Western Church (New
York: Gollins World, 1961).
