Conversion 155

ever, we needed to deepen our faith. We went through a crisis. We
had a conversion.

Now, the problem comes in the notion that this experience is
the one and only conversion for one’s whole life. If we think that way, we
always look backwards to that conversion. We want to recapture
the simplicity of that initial warm experience of the love and ac-
ceptance of God, and this is a mistake. It freezes faith at an imma-
ture level, and prevents us from pressing on to maturity. People
influenced by this way of thinking tend to want to recover the ex-
periences of their late teen years,

(To take a parallel example, we see this most commonly in the
way people retain a strong, often binding affection for whatever
kind of music they listened to in their late teens. People who danced
to Lawrence Welk’s “champagne music” were horrified when their
teenagers liked the Beatles. Now the Beatles generation has its
own children, and they are horrified at modern punk rock. The
beatnik generation, which came in between, still clings to the
sounds of off-beat folk music. There is nothing necessarily wrong
with some of this music, and there is nothing wrong with an occa-
sional nostalgia for childhood, but there can be a real problem
when this nostalgia becomes an intransigent refusal to mature.

(Continuing this parenthesis: America is a strange culture. It
glorifies youth, and it provides most people with the means to sur-
round themselves with youthful fictions. Women at 30 years of
age, after bearing children, want to be as slim and weightless as
they were at age 18, a manifest impossibility, Similarly, the phono-
graph record and the cassette tape enable people to continue the
experience of late teen years via music. Thus, that this kind of in-
transigent nostalgia is present in the arca of faith is no surprise,
but it is regrettable. We are called to press on to maturity—in
every area of life.)

Thus, I appreciate the “Campus Crusade” type of college con-
version experience. I think it is healthy for many young people,
and I don’t think it harms anyone. (After all, if the reprobate don’t
persevere in the faith, that is their fault.) The problem is in mak-
ing this kind of youthful experience the norm for mature Chris-
tian faith.
