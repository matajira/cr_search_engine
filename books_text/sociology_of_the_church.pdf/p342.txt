328 The Sociology of the Church

Early church separated church from emperors
Ambrose and Theodosius
Same battle later on in northern Europe
Gregory VII and Henry IV
Papacy developed in imperialistic direction
Kantorowicz’s discussion of corpus mysticum and corpus Christi
By the time of the Reformation, two large statist powers contending for
the church
The Reformation, 142
Lutheran acquiescence to the imperial state: cutus regio, eius religio
English Reformation also overly statist
Calvin, Bucer, and Reformed Catholicism
Imperial manipulation of the reform
Example from the Heidelberg Catechismn
Post-Reformation Developments, 145
Protestantism identified with nationalism
Pictist and Puritan movements maintained a catholic emphasis
Protestant loss of governmental aspect of the church
America, 147

 

Denominationalism
Refusal of churches to honor one another's discipline
Sectarianism and nationalism
Conclusion, 148
Need to recover a vision for a Reformed Catholic International Church,
based on mutual recognition

. Conversion. ... bees pee eeee ASL

 

Some reflections on “testimony giving” as itis done today
The modern testimony ritual
What is Conversion?, 152
1, Conversion from outside the faith
Daily conversions: turning to Christ anew daily
. Crisis conversions: new appreciation of Christ as a result of a crisis
Stage conversions: maturation and the stages of life
Why college-type testimonies don’t fit older people
Parenthesis on American Youth Culture
The Abundant Life, 156
What feels “abundant” varies with maturity
Reactions, 157
Problems with the neo-Puritan critique of campus evangelism
The Four Spiritual Laws, 157
Appreciation and critique
The Sacramental System, 159

wen

-

Designed to minister to crises and stages
Need for protestants to come up with equivalent ministry methods
