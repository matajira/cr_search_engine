PREFACE
(1988)

And further, by these, my son, be admonished: of making many
books there is no end; and much study is a weariness of the flesh.

(Ecclesiastes 12:12),

Of revising many books there is no end, while the author is still alive.
(Gary North).

O death, where is thy sting?
(I Corinthians 15:55a).

I have been hoping for over a decade to find time to update
this book. It finally dawned on me in late 1987 that-not only am
T unlikely to find the time to update this book extensively, it
probably should not be updated extensively. It was written origi-
nally as a secondary source document, a hard-core Christian
analysis of Marx’s thought. It is more likely to serve in the future
as a primary source document. It will gain sales not because it
is a book about Marx but because it was my first full-length book.
I have a book-buiying audience today that I did not have when
this book was first published. People who try to understand what
my contribution to the Christian Reconstruction movement has
been will find this book useful in their task. Many of the themes
that I write about today were already a part of my thinking at
the age of 24 to 25, when I wrote the bulk of this book.

When T wrote it in 1966-67, the Christian Reconstruction
movement did not yet exist. If we think of Christian Reconstruc-
tion as a theological system based initially on “the four
P’s” — predestination, pronomianism (biblical law), postmillen-
