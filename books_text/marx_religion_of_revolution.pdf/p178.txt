104 Marx’s Religion of Revolution

State, opposition means death by slow starvation. The old prin~
ciple: who does not work shall not eat, has been replaced by a
new one: who does not obey shall not eat.”?!! The Marxists, for
that matter, cannot even answer the issue raised by Engels: “If
man, by dint of his knowledge and inventive genius, has subdued
the forces of nature, the latter avenge themselves upon him by
subjecting him, in so far as he employs them, to a veritable
despotism independent of all social organization.”#!2

Dialectics: Society os. the Individual

Thus, we are brought full circle. The “nature-freedom” scheme
reasserts itself once again. Either man is controlled by an irra-
tional nature or by a despotic, ultrarational social system of
man’s own creation. Walter Odajnyk describes the Marxian
view of man—a creature always dominated by ‘his environ-
ment, yet somehow the master of his own fate: “He is now
something in between a free being and a machine responding to
the laws governing its operations—a sort of elaborate IBM
machine, which has a degree of operational independence.”?!>

In a lengthy passage near the end of the third volume of
Capital, Marx dealt with the problem as well as he could; he did
his best, but he failed:

In fact, the realm of freedom does not commence until the point is
passed where labor under the compulsion of necessity and of external
utility is required. In the very nature of things it lies beyond the sphere
of material production in the strict meaning of the term. Just as the
savage must wrestle with nature, in order to satisfy his wants, in order
to maintain his life and reproduce it, so civilized man must do it, and
he must do it in all forms of society and under all possible modes of
production, With this development the realm of natural necessity

 

QI1, Trotsky, The Revolution Betrayed (1937), p. 76, quoted by Hayek, The Road to
Serfdom, p. 119. [He may not have said it, Hayek’s citation is incorrect. But the
observation is correct: obedience to the civil government is basic to personal
survival if the state is the sole employer.] Cf, Abram Harris, “The Social Philosophy
of Karl Marx,” Ethics, LVIII (April, 1948), pt. IT, p. 32.

212. Engels, “On Authority,” Selected Works, 2, p. 377,

213, Odajnyk, Marxism and Existentialism, p. 116.
