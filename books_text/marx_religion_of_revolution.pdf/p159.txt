The Cosmology of Chaos 85

Robert G. Tucker’s evaluation seems perfectly justified: “Thus,
for Marx, the communist revolution is the means of attaining
not material abundance (though that, in his view, will come too)
and not justice in the distribution of goods, but the spiritual
regeneration of man.”!99 Marx held, unquestionably, a religion
of revolution. Regeneration through total chaos was his goal, and
the proletariat would serve as the priestly class in this ritual so
that the whole society might be freed from its alienation,

Diatectics: Ethics vs. Metaphysics

It was pointed out earlier that Marx’s doctrine of alienation
‘was a substitute for the Christian doctrine of the fall of man. In
spite of this apparent affinity for a Christian cosmology, Marx’s
system must be linked more.with pagan ancient religion than
with Old Testament messianism. This is not really contradic-
tory, since the cosmologies of the ancients were equally substi-
tutes for the idea of the fall. The similarities and distinctions
between the pagan and Hebrew-Christian views have already
been hinted at previously. The ancients believed in a metaphysical
fall from chaos into the bondage of order and law. Marx asserted
(or so his language indicates) that the fall was originally psycho-
Jogical but that man’s alienation became reciprocal with private
property at a later date. By changing the environment, man
would regain his pre-alienation state, but with his modern tech-
nology intact.

The Christian view is that the fall was ethical; the universe,
including man, was cursed as a result of the fall — the alienation
between man and the Creator — but the fall itself was ethical,
Because man is alienated from God, he is also alienated from his
fellow men, since they are made in God’s image. The restoration
of man and his civilization is not to. be accomplished, therefore,
by a flight from law, but by a return to covenant obedience in
terms of Biblical law. Regeneration is to come through faith in
Christ’s sacrificial atonement on the cross; this is God’s grace to
the individual. Any social reconstruction that should result from

159, Tucker, Philosophy and Myth in Karl Marx, p. 24.
