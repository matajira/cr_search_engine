The Cosmology of Chaos 83

can the narrow horizon of bourgeois right be crossed in its
entirety and society inscribe on its banners: From each according
to his ability, to each according to his needs!”'5*

Marx’s basic authoritarianism was perceived by his anar-
chist rival, Michael Bakunin. Bakunin had once been able to
cooperate with Marx, since they shared an opposition to the
bourgeois state, but Bakunin later broke with Marx. He did not
share Marx’s faith in a total state’s ability to abolish the state
forever: “I hate communion [communism — apparently. a typo-
graphical error] because it is the negation of liberty and because
humanity is for me unthinkable without liberty. I am not a
communist, because communism concentrates and swallows up
in itself for the benefit of the State all the forces of society,
because it inevitably leads to the concentration of property in the
hands of the State, whereas I want the abolition of the State, the
final eradication of the principle of authority and patronage
proper to the State, which under the pretext of moralizing and
civilizing men, has hitherto only enslaved, persecuted, exploited,
and corrupted them. I want to see society and collective or social
property organized from below upwards, by way of free associa-
tion, not from above downwards, by means of any kind of
authority whatever.” 5+

Demythologizing Marx

Some contemporary scholars — e.g., Lichtheim, Halle, Mayo,
Schumpeter — want to play down Marx’s revolutionary fervor,
especially the cal] to revolution which he made in his later career.
The problem with this interpretation is providing an explanation
for his obvious delight with the Paris Commune of 1871. This
“aberration” in the so-called “mature” Marx indicates that his

153. Zaid. The “ability-needs” slogan was Morelly’s (1760).

154, Quoted in E, H. Care, Michael Bakunin (London: Macmillan, 1937}, p. 341.
How one can be against private property and alsa properly managed by the state
is not very clear, but Bakunin certainly saw the latent totalitarianism present in the
Marxian system, and for this reason his comments are useful. What exactly he
meant by this statement, “I am a collectivist, but not a communist,” is a mystery
(iid.). [He was a syndicalist, a philosophy of economic ownership that has. no
economic theory.]
