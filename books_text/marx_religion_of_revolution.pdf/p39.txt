Preface xxxix

were they? Two bourgeois writers who were converted to revolu-
tionary socialism in their mid-twenties. Both were sons of suc-
cessful bourgeois fathers, and Engels grew steadily richer over
the years because of his skills in managing his father’s industrial
textile mills. It never seemed to bother Lenin that he had no
consistent Marxist explanation for the historical fact regarding
the workers that he could not deny: the. Social-Democratic con-
sciousness of proletarians does not develop by itself. “This con-
sciousness could only be brought to them from without. The
history of ail countries shows that the working class, exclusively
by its own effort, is able to develop only trade-union conscious-
ness... . The theory of Socialism, however, grew out of the
philosophic, historical and economic theories that were elabo-
rated by the educated representatives of the propertied classes,
the intellectuals. The founders of modern scientific Socialism,
Marx and Engels, themselves belonged to the bourgeois intelli-
gentsia.”5 The obvious question is: Why? It has no obvious
Marxist answer,

Marx and Engels also predicted the initial successes of this
proletarian revolution in nations that had adoped modern indus-
trial capitalism. So, where have the only successful indigenous
Communist revolutions taken place? In rural Third World na-
tions and in nations that were only in the very early stages of
industrialism (e.g., Russia}. Who have their ideological recruits
been? First and foremost, intellectuals in industrial countries
who have themselves recruited no proletarian followers but who
have strongly influenced a small army of other intellectuals who
are basically favorable to Marxist humanism, or who are at least
unfavorable to the efforts of the enemies of Marxist tyrannies.57
Second, highly educated bourgeois intellectual activists in rural
nations who have succeeded in recruiting dedicated peasant
followers, In short, nowhere have the theories of Marx and

36. Lenin, What Is To Be Done? Burning Questions of Our Movement (Now York:
International Publishers, [1902] 1943), pp. 32-33.

37. Jean Francois Revel, How Democracies Perish (Garden City, New York:
Doubleday, 1984), Part Four.
