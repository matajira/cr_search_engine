The Myth of Marx’s Poverty 247

As far as I know, as I write this subsection, in late July of
1988, I seem to be the first investigator to search out even this
minimal data on wage levels and the cost of living in Paris during
the 1840’s in order to compare Marx’s income with the average
workers, I am surely not the only person bright enough to do
this. What we are suffering from is a combination of laziness on
the part of scholars of Marx’s life, plus an element of dignified
silence: to discuss such matters would lead to the overthrow of
the myth of Marx’s poverty. It throws serious doubt on Marx’s
lifelong self-posturing as the Prometheus figure of the European
proletariat. Only if someone finally turns up evidence that he
really did donate 5,000 francs to the Belgian workers in early
1848 should we find reserves of compassion for poor Karl.

His years of serious financial hardship began in 1848, but
by this time his philosophy of dialectical materialism and eco-
nomic communism had already crystallized in his mind. In
short, his philosophy of life had been developed in his years of
remarkable prosperity. He became the self-appointed “voice of
the prolctarians” before he suffered the self-inflicted financial
hardships of proletarianism. Unlike proletarians, he never held
a steady job after 1844, and that job in Paris had lasted less than
a year.

Marx returned to Cologne in 1848, and in June he began
publication of still another paper, the Neue Rheinische Zeitung. The
following February saw him brought to trial and subsequently
acquitted of the charge of subversion. In May he published the
inflammatory “red issue” — literally printed in red ink — since
he was about to be expelled anyway. He left for France, but was
expelled three months later (August 1849). From there he trav-
eled to London, which, along with Switzerland, was the home
of most nineteenth-century radicals after the revolutions of 1848-
1850. He was to spend most of his remaining life in London, the
city of exiles,

Self-Imposed Poverty

It was in the 15-year period from 1848 to 1863 that Marx
gained his reputation for poverty, a reputation he earned by his
