xii Marx’s Religion of Revolution

history of Western philosophy.? It is equally basic to modern
science.!° Human freedom is defined in the West in two irrecon-
cilable ways: 1) power over one’s totally determined environment
by means of reason (phenomenal); 2) autonomy from one’s totally
determined environment by means of an escape from reason
(noumenal). Principles of ethics and ethical decision-making are
always to be confined to the realm of the scientifically and
logically undetermined. But if the undefined and undefinable
drawbridge — intuition, will, or praxis—across the undefined
and undefinable moat that separates ethics from history can in
fact be lowered, what will then protect the noumenal from the
phenomenal, and vice versa? On the one hand, what will keep
man’s ethics and decisions from becoming totally determined
by impersonal cause-and-effect forces - for example, the world
hypothesized by psychological behaviorism? On the other hand,
if the drawbridge does somehow link the two realms, what is to
protect the phenomenal realm of cause and effect from being
disrupted by the invasion of the noumenal’s irrationalism?!!
There are no humanist answers that hold up to logical scrutiny
by other humanists. This is why a philosopher as good as Stephen
Toulmin has struggled with the topic of the relation between
reason and ethics.!2

 

9. Herman Dooyeweerd, A New Critique of Theoretical Thought, 4 vols. (Philadel-
phia: Presbyterian & Reformed, 1953-58).

10, Sidney Hook (ed.), Determinism and Freedom in the Age of Modern Science (New
York: New York University Press, 1958).

11. In the worldview of modern physics, the law of large numbers protects the
visible world from statistically significant invasions by the apparently non-physical,
purely statistical reality of the subatomic universe. Religiously logical men have
placed great faith in this statistical barrier for three generations. Today, specialists
design computer chips and circuits that are also in part designed by computer
networks, producing systems that no one can fully comprehend. The designers
never mention publicly the possibility that the realm of the noumenal — or even
aspects of the world of occultism— might hide in the “crevices” of computer-
designed circuits and logic, waiting to create havoc that will then be defined away
in terms of inescapable randomness, also known as “glitches.” Everything in the
noumenal must be impersonal, irrational, and random. But what if this is an
incorrect assumption?

12. Stephen Toulmin, An Examination of the Place of Reason in Ethics (Cambridge:
At the University Press, 1958).
