278 Marx’s Religion of Revolution

bourgeois revolution (disguised),
xxiii
consciousness, 80
dialectics &, 93-94
expropriated, 90-91
misery of, 129-134
new age, 99
Schumpeter én, 8-59
Social Democrats, xxxix
social progress, 51
Prometheus, xix, xx, 9, 36
Protagoras, 32
Peychedelic Rangers, 23

quantum physics, x, xv
Quigley, Carroll, Ixiv

radicalism, 26
randomness (see also’ chaos,
irrationalism)
computers &, xii
religion &, xiii
science &, xv
standard, xvi
rationalism, xi, 53, 56-58
rationing, 207
Reagan, Ronald, xxix
reason, 53
relativism, 53, 55-56
religion of revolution, xvii-xviii, 81, 85
representation, xxii
Revel, J. F., 164n
revolution
authoritarian, 106-107
cleansing, 18, 72, 80-81
contradictions, 51
economic (USSR), 204
economics, 50
inevitable, 62, 69, 97
locomotives of history, xviii, 169
logic & xvii
midwife, 73
new humanity, 72

permanent, xviii, 91

political act, 73

politics, 100

proof of, 112

reason &, 98

religion of, xvii-xviii

resolution, 70

Socialiam &, 73

static world &, 160
Ricardo, David, 149-153
Riis, S. M., 17
Robbins, Lionel, 177-178, 196
Roche, John, Ixix, lxxi
Rodbertus, 161
Répke, Wilhem, 179n
Rosicracians, 77
Rothbard, Murray, 102-103, 143n
Rousseau, J. J., 41
Rucker, Rudy, ix
Ruge, Arnold, ly, lvi, 10
Rithle, Otto, 14-15, 16-17
Runciman, Stephen, 77
Rushdoony

Godhead’s unity, 41

Institutes, xxiv

knowledge, 57

open universe, 107

Religion of Revolution, xxvi
Russia, 94-95, 165, 193n
Russian Revolution, xxi

Samuelson, Paul, 152, 154
sanctions, xi
Satan
empires, 173
omniscience, 163
power, 166
society of, xxv
Saturanlia, 75
Say’s Law, 137-139
scarcity, 103,
Schaafsma, Tjeerd, ix
Schumpeter, Joseph
