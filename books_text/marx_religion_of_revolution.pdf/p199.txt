The Economics of Revolution 125

means simply that more constant capital is being used in the
production process. As more and more machinery and raw mate-
rials are added, the percentage of human labor power involved
in the process necessarily falls. In other words, there is propor-
tionately less human labor power available for exploitation, and
thereforé profits must fall, since living labor is the only source of
capitalism’s profits.

Capitalism, in Marx’s system, is caught in a fundamental
contradiction: capitalists are impelled to act in a fashion which
will ultimately destroy their very mode of existence. In order to
increase their profits, they must increase production; in order to
increase production, they must add constant capital; and the
addition of constant capital increases the organic composition
of capital, thus causing a fall in the rate of profit. This tendency
for the rate of profit to fall can be offset by other tendencies which
can temporarily compensate for the fall, but ultimately the prof-
its of capitalism must decrease to a level incompatible with the
maintenance of the system.*+

Capitalist Accumulation

The scramble for profits motivates the capitalist to expand
the size of his industry and thus take advantage of the economies
of scale, Up to a point, increased plant capacity can achieve
lower cost per unit. After this point is reached, costs per unit
will rise at such a rapid rate that it will not pay the capitalist to

38, Algebraically, the organic composition of capital is expressed by the fraction
C/C+V. As C rises, the fraction approaches the value of one, or 100 percent
constant capital. The labor factor, V, therefore carries a smaller weight in the
fraction, As V gets smaller, the source of capitalism’s profits dries up. For a full
discussion of this “law” or tendency, see Capital, 3, ch. 3. The rate of profit is
expressed by the fraction S/C+ V. As C increases, if S (surplus value) and V (wages)
remain constant, the value of the fraction clearly decreases, since its denominator
is increasing because of the increase of C. The rate of profit is therefore falling.
‘This assumes, of course, that S is constant, or at least not rising fast enough to offset
the rate of the rise in C,

34, The offsetting tendencies are discussed in Capital, 3, ch. 25. For a critical
discussion of the “law” of Marxian analysis, see David McCord Wright, The Trouble
With Marx (New Rochelle, New York: Arlington House, 1967), ch. 5.
