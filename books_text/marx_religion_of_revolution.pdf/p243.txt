Conclusion. 169

mistic view of the future. The vision of time that a society shares
is very important for understanding how it operates. If you think
you are running out of time you will do certain things; if you
think you have all the time in the world, you will do different
things. Your vision of the future influences your activities in the
present,

Communism’s Confidence

One of the great advantages that the Soviet Union has
enjoyed in its confrontation with the West is that Communism
appears to offer Soviet leaders a doctrine of linear (straight line)
time. It gives them confidence about. the future. They believe
that the forces of history are on the side of international Gommu-
nism, This self-confidence is an illusion because Marxism’s opti-
mism is an illusion. Marxism is a publicly optimistic religion
with deeply pessimistic roots.

Karl Marx professed faith in linear time, and so do his
followers. Ultimately, Marxism is pessimistic and cyclical, as are
all pagan religions. Marx explained mankind’s history in terms
of revolution. “Revolutions are the locomotives of history,” he wrote.
Problem: what will serve as the engine of progress after the final
Communist revolution? Revolutions will cease. What then be-
comes the basis of human progress?

To understand the Communists’ lack of any answer, you
must understand Marxism’s doctrine of the fall of man. All
religions have such a doctrine; you just have to look for it more
carefully in humanist religions. Marx wrote that mankind is
alienated. This is the equivalent of being under a curse. This
theme of human alienation is the heart of Marx’s psychology,
economics, and humanist theology. Human alienation is the
basis of all of man’s conflicts, Marx wrote. How can mankind
overcome this alienation? By revolution. But how can revolution

9, Karl Marx, “The Class Struggles in France, 1848 to 1850" (1850), in Karl
Marx and Frederick Engels, Selected Works, 3 vols. (Progress Publishers, [1969]
1977), 1, p. 27. Italics in original. Reprinted in Karl Marx and Frederick Engels,
Collected Works (New York: International Pubilishers, 1978), 10, p. 122,
