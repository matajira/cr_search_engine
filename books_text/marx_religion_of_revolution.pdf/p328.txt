254 Marx’s Religion of Revolution

Marx, in short, felt he was unable to live comfortably on an.
income greater than that enjoyed by ninety-eight percent of his
countrymen —in a nation which, per capita, was the wealthiest
in the world. Incredibly, one biography puts it this way: “But
his anxieties only really ended in 1869, when Engels sold his
share in the cotton mill and was able to make Marx a definite,
if moderate, yearly allowance.” That is how history gets re-
written.

Conclusion

So what does all this mean? Perhaps not a great deal. But
at least we can now place the myth of Marx’s poverty in its
proper perspective. He was poor during only fifteen years of his
sixty-five-year career, in large part due to his unwillingness to
use his doctorate and go out to get a job. His economic opinions
had been formed, at least in their essentials, before this poverty
set in, and the final culmination of his system, Das Kapital,
published in 1867, was completed in the years of high income.
His own life seems to stand as a testimony against the validity
of his doctrine of economic determinism. The philosopher-
economist of class revolution — the “Red Doctor of Soho” who
spent only six years in that run-down neighborhood — was one
of England’s wealthier citizens during the last two decades of his
life, But he could not make ends meet.

In one respect, at least, things have not changed very much
since the middle years of the last century. You can still find far
more self-proclaimed Marxists on the bourgeois college campus

64. Baxter’s figures appear in Economic History Review, XX (April 1968), p. 21.

65. Nicolaievsky and Maenchen-Helfen, Karl Marx, p. 254.

66. At his death, Marx’s estate was valued at about £250, consisting primarily
of his books and furniture. Payne, Marx, p. 500. Payne’s comment is only too
accurate: “In spite of Engels’ generosity he was continually in debt. Although he
spent most of his waking hours thinking about money, he had very little under-
standing of the risk attached to borrowing. He would sign bills of exchange at high
interest and wonder how he had brought himself to sucha pass when the bills fell
due. He was improvident and oddly childlike in financial matters. He had no gift
for making money and none for spending it” (p. 342).
