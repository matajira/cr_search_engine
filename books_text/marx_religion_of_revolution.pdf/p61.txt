Preface isi

ify a person’s thinking. The best example of such a change is
my view of the biblical covenant; until Ray Sutton made his
monumental breakthrough in late 1985, there had never been a
clear exposition of the Bible’s five-point covenant model. It
was not that any one of the five points was new to my thinking;
it was that for the first time, an author had shown that these five
points are inherent in the biblical covenant, and also in a specific
order. Nevertheless, the language, theology, and categories of
Marx’s Religion are not substantially different from what I would
write today. This is another reason why I have. decided not to
revise it extensively,

This Book’s Style

The style of Afarx’s Religion of Revolution does reflect the
academic environment in which it was originally written. A wise
graduate student does not write with the same sort of visible
confidence that a financially independent author-publisher can
safely adopt. 1 was granted my Ph.D four years after the book
appeared. Today, I no longer worry about what a book editor
will think, let alone what college professors will think.’ But
back in 1968, I was at least to some degree under the self-
imposed restraints of the petrified hand of academic discourse.
In this one area, I can sympathize with Marx, who wrote anony-
mously in 1842 regarding his essays on Prussian censorship: “I
am humorous, but the law bids me write seriously. I am auda-

 

subjective value, because God evaluates both objectively and subjectively. I have
presented my position in The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4. I have also abandoned Mises’s
commitment to pure apriorism; we need both economic theory and historical facts
in our formulation of hypotheses, just as we need subjectivism and objectivism in
value theary: North, “Economics: From Reason to Intuition,” in Gary North (ed.),
Foundations of Christian Scholarship: Essays in the Van Til Perspective (Vallecite, Califor-
nia: Ross House Books, 1976), ch. 5.

99. The five points are transcendence, hierarchy, ethics, oath, and succession.
Its acronym is THEOS.

100. See, for example, my Foreword to Ian Hodge's Baptized Inflation: A Critique
of “Christian” Kemasianism (Tyler, Texas: Institute for Christian Economics, 1986),
which I regard as my classic polemical piece on Christian academia.
