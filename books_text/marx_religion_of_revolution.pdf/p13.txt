Foreword xiii

Fran Lebowitz, a pop culture New York “celebrity intellec-
tual” of no identifiable importance, once remarked: “Random-
ness scares people. Religion is a way to explain randomness.”
It is equally easy to argue the opposite. Religion scares people.
Randomness is the way that post-Kantian man can explain away relig-
ion,'* Increasingly as this century staggers toward the Big Some-
thing (it knows not. what) of the Year 2000 and beyond —a
tradition of crystal ball-gazing that began as far back as 1788,
when the French revolutionary and pornographer, Restif de la
Bretonne, who also coined the word “communism,” wrote L’Année
2000! — the irrationalism behind Kant’s brilliant mystification
is becoming clearer. The concept of the noumena! has been a
very effective way for self-proclaimed autonomous man to deal
with his perfectly justified sense of guilt and his equally justified
sense of impending doom, both temporal and eternal.

Free Will os. Predestination

No one escapes this continuing contradiction: determinism
(personal or impersonal) vs. responsible decision-making. At
best, we can only abandon thinking about it too deeply, either
out of principle or out of intellectual laziness. In Christian theol-

13, The Portable Curmudgeon, edited by Jon Winokur (New York: New American
Library, 1987), p. 234.

14. For a detailed discussion of this thesis, see my book, Gary North, Untaly
Spirits: Occultism and New Age Humanism (Ft. Worth, Texas: Dominion Press, 1986).

15, Fritz Baade, The Race to the Year 2000 (London: Cresset, 1962); Herman Kahn
and Anthony J. Weiner (eds.), The Year 2000: A Framework for Speculation on the Next
Thirty-Three Years (New York: Macmillan, 1967); Daniel Bell (ed.), Toward the Year
2000: Work in Progress (Boston: Houghton Mifilin, 1968); V. Kosolapov, Mankind and
the Year 2000 (Moscow: Progress Publishers, [1973] 1976); Andrew M. Greeley,
Religion én the Year 2000 (New York: Sheed & Ward, 1969); Foreign Policy Associa-
tion {ed.), Toward the Year 2018 (New York: Cowles Education, 1968); Desmond
King-Hele, The End of the Twentieth Century? (New York: St. Martin's, 1970); Council
for Environmental Quality, Global 2000 Report fo the President: Entering the Twenty-First
Century, 3 vols. (New York: Pergamon, 1981); Global 2000: Is There Still Time?
(Oklahoma City: Southwest Radio Church, 1984).

16. James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith
(New York: Basic Books, 1980), p. 7. See also Robert A. Nisbet, “The Year 2000
and All‘That,” Commentary (June 1968), pp. 60-66.
