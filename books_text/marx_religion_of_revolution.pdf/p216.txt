142 Marx’s Religion of Revolution

ings, certainly is important in any economic system, even if the
state or the “association” does the saving. However, Marx may
have had in mind only the idea that fractional reserve banking
would cease under socialism. He ridiculed all non-specie metal
monetary systems (a fact which may come as a surprise to many
readers), since he believed that unbacked paper credit or cur-
rency is a basic fraud of capitalism. Since all fractional reserve
banking is based upon an expansion of currency and credit
beyond the available gold and silver reserves, the system must
be condemned.” His criticism went far deeper than this, how-
ever; Marx argued that under communism no money would exist
at all. Money is the very symbol of the evils of capitalism — the
very sign of alienated production — and one of the glories of full
communism would be the abolition of money.

Here is the central flaw of all socialist systems: how can the
allocation of scarce resources take place in a society devoid of
money? See the appendix on “Socialist Economic Calculation”
for a more exterided discussion of this problem. There would be
no money and no debt; debt is a form of economic slavery, and
it could never exist in the new society.” It followed from this, in
Marx’s mind, that bankers are nothing more than “honourable
bandits.”®° Merchants’ capital is simply “a system of rob-
bery. . . .”8! None of this will exist in the world beyond the
Revolution.

Thus, Marx’s system contains multiple theories concerning
the breakdown of capitalism. The falling rate of profit is one
cause, and another is the supposed contradiction between produc-
tion and consumption. There is overproduction of both capital
goods and consumer goods; simultancously, there is a shortage
of the basic necessities for the masses of society. The overexpan-
sion of credit is a third cause. Wherever Marx looked, he saw
contradictions, all of which pointed to the inevitable coming

78. Ibid., 1, p. 144; 8, p. 537. [/0id., 1, p. 128; 3, p. 454.)

79, {id., 1, pp. 827-29; 3, p. 703, [Ibid., 1, pp. 754-55; 3, p, 598]
80. Zoid, 3, p. 641. [/bid., 3, p. 545.}

81. Ibid., 3, p, 389. {U5id., 3, p. 331]
