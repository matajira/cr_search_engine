x Marx’s Religion of Revolution

innate dualism of autonomous human thought is reflected in the
inherent dualism of nature, or vice versa. Others are popularized
versions of physical theory that link medern subatomic quantum
physics to Eastern mysticism: Fritjof Capra’s The Tuo of Physics
(1975) and Gary Zukav’s The Dancing Wu Li Masters (1980). We
even see best-selling author-lecturer Tom Peters write a book
called Thriving on Chaos (1987),3 an approach to corporate man-
agement seemingly very distant from his enormously popular In
Pursuit of Excellence and his recommended “management by walk-
ing around” techniques.

Why? Why all this concern about the creativity of chaos,
even to the point of arguing that.chaos underlies physical real-
ity —if in fact there really is an underlying physical reality,
something which several versions of modern quantum physics
deny? Because there is a continuing philosophical tension between chaos
and order, Qne side will dominate men’s thinking for a period,
and then the other will dominate. It has always been thus in the
mind of the covenant-breaker.5 God-defying man has always
seen the origins of cosmic order in an original chaos. The So-
cratic and post-Socratic classical rationalists saw things this
way: the world order in dialectical tension between the ceaseless
flow of Heraclitus’ stream of history and the permanent order of
Parmenides’ timeless logical principles. So does modern science.
Writes English physicist P. C. W. Davies: “If the organized
activity of the universe is slowly disintegrating in. obedience to
the second law of thermodynamics, we would expect that as we
look to the early moments of the universe, we would see more
rather than less order. Yet the evidence is just the opposite. The
primeval cosmos was not orderly at all but chaotic.” As Van

3 Tom Peters, Thriving on Chaos: Handbook for a Management Revolution (New
York: Knopf, 1987).

4, Gary North, Js the World Running Down? Crisis in the Christian Worldoiew (Tyler,
‘Texas: Institute for Christian Economics, 1988), ch. 1.

5, It may even be thus in the very brain of man, and not just his mind: a left
side-right side division.

6. P. C. W, Davies, “Order and Disorder in the Universe,” The Great Ideas
Today (Chicago: Encyclopedia Britannica, 1979), p. 49.
