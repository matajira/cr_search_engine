236 Marx’s Religion of Revolution

the ranks of bourgeois society. It was only to be expected that
Heinrich (Herschel) Marx, a relatively successful lawyer, would
want his son to do well in the world of “affairs.” He provided for
young Karl a thoroughly liberal humanistic education, first in
the Trier Gymnasium, and then at the University of Bonn and
then at Berlin. ,

Shortly before his graduation from the Gymnasium, Marx
wrote an essay called “Reflections Of A Youth On Choosing An
Occupation,” in which he exhibited such liberal and bourgeois
sentiments as this one: “The main principle, however, which
must guide us in the selection of a vocation is the welfare of
humanity, our own perfection.”’ How did the young man who
could write these words in 1835 become the philosopher of class
revolution a decade later?® One thing is certain: his “conversion”
to revolutionary communism was not the product of any grind-
ing personal poverty.

He had gone first to the University of Bonn, but his time had
been spent more in drinking and dueling than in study, a situ-
ation which was typical for those young men who had aspirations
of entering the state’s official bureaucracies upon graduation.
Marx’s father therefore insisted that Marx transfer to the more
academically rigorous University of Berlin; Marx did so at the
beginning of his second year of college. We know relatively little
about the life of Karl Marx over the succeeding five years. He
piled up many bills, received continual financial support from
his parents (his father died in 1838), and spent much of his time
in the so-called Professors’ Club or Doctors’ Club, a group of
about thirty youthful members which met in the Cafe Stehely.
It was here and in his extracurricular reading, not in the class-
room, that he received most of his education.?

7. Loyd D. Easton and Kurt H. Guddat (eds.), Writings of the Young Marx on
Philosophy and Society (Garden City, New York: Doubleday Anchor, 1967), p. 39.
[Collected Werks, 1, p. 8.]

8. One answer is that he entered into a compact with Satan: Richard
Warmbrand, Marz and Satan (Westchester, Illinois: Crossway, 1986), ch. 2.

9. This, however, is generally the case when very bright young men enter
college. Textbooks bore them. Classroom lectures bore them. European lectures are
