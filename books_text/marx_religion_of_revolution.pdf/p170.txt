96 Marx’s Religion of Revolution

What, then, becomes of the all-encompassing theory of capi-
talist development? What happens to the inevitable sweep of
man-made history on the march? Marx answered one critic.of
his Russian compromise in this fashion: “He feels himself obliged
to metamorphose my historical sketch of the genesis of capital-
ism in Western Europe into an historico-philosophic theory of
the marche genérale [general path] imposed by fate upon every
people, whatever the historic circumstances in which it finds
itself, in order that it may ultimately arrive at the form of
economy which will ensure, together with the greatest expansion
of the productive powers of social labour, the most complete
development of man. But I beg his pardon. (He is both honour-
ing and shaming me too much.)?!83

It is almost beyond belief! The system is.now nothing more
than “an historical sketch of the genesis of capitalism in Western
Europe.” The system, as anything more than a general survey
of European history, is now officially gutted by its author! It is
fantastic that so much labor and energy, so much poverty and
sickness, should have been self-imposed by Marx in order to
bring forth such a pitiful mouse.

Inevitability and. Universality

G.D. H. Cole, who was either unfamiliar with this letter or
else did not take it seriously, sees Marx’s universal history as a
powerful, but unfortunately inaccurate, hypothesis. He made a
very significant observation: “They could have rested content
with a formulation of the law of development limited to the
particular civilization which they were trying to influence.
Whether, formulated in this narrower way, their theory would
have exercised as powerful a speil as it has in fact exercised may
be doubted; for its universalism was undoubtedly not the least
of its attractions and played a large part in converting it from a
rationalistic doctrine into a belief which could be held with the
intensity of a religion.”!

189, Marx to the Editor of the Otyecestvenniye Zapishy, late 1877; Correspondence,
p- 354,

190, Cole, The Méaning of Marxism, p. 82; cf. p. 209,
