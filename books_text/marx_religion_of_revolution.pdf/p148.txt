14 Marx’s Religion of Revolution

wealth, and freedom; this age had been lost.'?° Orthodox He-
brew and Christian theology explains this transition in terms of
an ethical fall into sin; man opposed the law of God and was
punished for his disobedience, The fall, in other words, was an
ethical rather than a metaphysical phenomenon. The ancients,
however, saw the fall as a metaphysical event, the world is in
bondage to law and scarcity, and by returning to chaos for a
period, society participates in that pre-temporal age of plenty.
There is the hope that the Golden Age itself might be restored.
Only through participation in the pre-temporal chaos event can
society be rejuvenated; only through ritualistic participation can
the link be established. between cosmic time and the present
reality.

In all but the Biblical cosmology, the creation was seen as
the imposition of order upon a chaotic matter. Thus, in the
festivals and other rituals of chaos, society was thought to have
access to that vital matter which existed before form was im-
posed to stifle its free action. Roger Caillois has explained this
pagan cosmology, focusing his attention on the festival: “It is a
time of excess. Reserves accumulated over the course of several
years are squandered. The holiest laws are violated, those that
seem at the very basis of social life. Yesterday’s crime is now
prescribed, and in place of customary rules, new taboos and
disciplines are established, the purpose of which is not to avoid
or soothe intense emotions, but rather to excite and bring them
to climax. Movement increases, and the participants become
intoxicated. Civil or administrative authorities see their powers
temporarily diminish or disappear. This is not so much to the
advantage of the regular sacerdotal caste as to the gain of secret
confraternities or representatives of the other world, masked
actors personifying the Gods or the dead. This fervor is also the
time for sacrifices, even the time for the sacred, a time outside
of time that recreates, purifies, and rejuvenates society. . . . All

126. CF Hesiod (8th century B.G.}, The Works and Days (Ann Arbor: University
of Michigan Press, 1959), lines 109-201, for an account of the five ages of man: from
Gold to Iron, (Richmond Lattimore translation).
