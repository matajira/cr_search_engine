The Economies of Revolution 129

a conclusion which, had it been a forecast a century ago, Marx
would have rejected as utter bourgeois nonsense: “The studies
of trends of both general and market concentration have yielded
useful negative conclusions. It is clear now, as it was not clear
before, that there is no inevitable historical force at work. that
must produce, over any extended period of time, an increase in
the per cent of economic activity accounted for by the largest
firms either in American manufacture or in the economy as a
whole.”

Increasing Misery of the Proletariat

This is one of the more familiar themes in Marx’s economic
analysis. It is familiar in the sense that it has become a cliché
within Marxist circles; it is not familiar in the sense that anyone
is really certain as to what Marx meant exactly by the phrase.
Like so many of his teachings, this one was set forth by Marx
only in scattered places, and never in any systematic fashion.
Commentators are forced to sift through many seemingly contra-
dictory passages in their attempt to find some semblance of order
in his idea of “increasing misery.”

One view which many scholars (especially the more vocifer-
ous critics) have argued is that Marx meant that the increasing
misery is to be absolute under capitalism: things must inevitably
get worse for the proletariat as capitalism develops. There can
be no question about the fact that Marx did write several pas-
sages which definitely teach just such a doctrine. For example,
in the Communist Manifesto (1848), he and Engels wrote: “The
modern labourer, on the contrary, instead of rising with the
progress of industry, sinks deeper and deeper below the condi-
tions of existence of his own class. He becomes a pauper, and

44, Edward S. ‘Mason, Economic Concentration and the Monopoly Problem (Camb-
ridge, Massachusetts: Harvard University Press, 1957), pp. 42-43. Solomon Fabri-
cant pul it this way; “All the doubts that can be raised [concerning the data} do
not destroy, rather they support, the conclusion that there is no basis for believing
that the economy of the United States is largely monopolistic and has been growing
more monopolistic.” See his essay, “Is Monopoly Increasing?” Joumel of Economic
History, XIII (1953), p. 93.
