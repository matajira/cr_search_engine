Bibliography 263

Commentaries on the Marxian System
(alphabetical by author)

Bohm-Bawerk, Eugen von. Shorter Classics of Bohm-Bawerk, vol.
I. South Holland, Ill: Libertarian Press, 1962. This book
includes his famous essay on the contradictions within the first
and third volumes of Capital.

Bober, M. M. Karl. Marx’s Interpretation of History. New York:
Norton, 1965. A reprint of the 1948 edition of this important
study. Bober stays away from peculiar or original views of
Marx, and this is the strength of the book. It summarizes the
Marxian system.

Carew Hunt, R. N. The Theory and Practice of Communism. Balti-
more: Penguin, 1964. Originally published in 1950, this is
perhaps the best one volume introduction to Marx’s thought.
The first half deals with Marx and Engels, while the second
half covers later contributions to Marxism: Lenin, Stalin,
post-Stalinist thought. Like Bober, Carew Hunt does not offer
any startling theses about Marx; he simply comments intelli-
gently on the system as a whole, clarifying and expanding the
implications of Communism,

Cole, G. D. H. The Meaning of Marxism. Ann Arbor: University
of Michigan, 1964. A reprint of the 1948 study. Cole tries to
summarize Marxism, while reworking the original Marxian
ideas so that the serious criticism made by hostile commenta-
tors will not be so telling. He does not make a total apology,
however; where he believes Marx to be clearly in the wrong,
he says so. It is a very ponderous book to read; Cole’s style of
writing does not lend itself to quick skimming. Nevertheless,
a serious student must read it.

Cornforth, Maurice. Marxism and the Linguistic Philosophy. New
York: International Publishers, 1965. Cornforth, one of the
most influential of America’s Marxists philosophers, contin-
ues his studies of the Marxian philosophical approach, The
