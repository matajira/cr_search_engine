90 Marx’s Religion of Revolution

We have examined Marx’s doctrine of revolution as a cosmo-
logical and philosophical theme. This, however, does not explain
the historical setting in which Marx placed the inevitable chaos,
nor does it give much light on the whole problem of revolution-
ary tactics. It is to this aspect of the revolutionary format that
we must turn.

Young Marx vs. Old Marx?

Marx never set forth his outline of the Revolution in any one
place. Like most of his theories, this one is found throughout his
works, and it is not altogether clear as to exactly what he thought
the Revolution would be.'* As to how it was to come into being,
he never decided; in fact, conflicting statements concerning the
proletarian uprising have been used as a means for distinguish-
ing the younger Marx from the “mature Marx.” The year 1850
is usually seen as the point of transition.!®”

In his early years, Marx firmly believed that the forces of
material production in capitalism would bring the Revolution
to a head. The exploited proletariat class would rise up against
the bourgeoisie and take possession of the means of production,
the state, and the other organs of bourgeois control. He never

 

Tf Marx had been uninterested in any natural dialectic, why was he so taken
with Darwin's theory of the eternal struggle in nature? In this regard, I wish to
quote from a letter to me from Walter Odajnyk, author of Marxism and Existentialism
(1965}, dated 21 Sept. 1966: “I know that Western commentators — for some
curious reason — have attempted to saddle Engels with the sole responsibility for
the somewhat embarrassing natural dialectics of Marxism, but I would maintain
that it is a logical development of Hegelianism into the material-natural sphere.
Mars, for obvious reasons, was more interested in the consequences of an upside-
down Hegelianism in the socioeconomic sphere, and to Engels fell the natural
order.” As Odajnyk goes on to say, Marx was alive at the time of the writing of
both Anti-Dihring and Dialectics of Nature. In reference to the former, Schumpeter
writes: “It cannot be denied however that Marx wrote part of ch. x and shares
responsibility for the whole book.” Capitalism, Socialism and Demecracy, p. 39n. The
Soviet Marxists of course, have always accepted Engels’s views as basic to Marx:
ism. [Gf Gary North, fs the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1988), pp. 59-61.]

166. Meyer, Marxism: The Unity of Theory and Practice, p. 80.

167. Lichtheim, Marxism, pp. 122-29,
