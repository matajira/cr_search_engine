116 Marx’s Religion of Revolution

Once Marx had accepted the validity of the “common sub-
stance” hypothesis, he began to draw certain conclusions: “We
have seen that when commodities are exchanged, their exchange
value manifests itself as something totally independent of their
use-value. But if we abstract from their use-value, there remains
their Value as defined above. Therefore, the common substance
that manifests itself in the exchange value of commodities, when~
ever they are exchanged, is their value.” Apart from the diffi-
culty of understanding his own jargon, Marx faced an immediate
problem: “How, then, is the magnitude of this value to be
measured? Plainly, by the quantity of the value-creating sub-
stance, the labour, contained in the article.”!§ “The quantity of”
labour,” he went on to say, “is measured by its duration,” but
this necessarily must assume that all human labor is homogene-
ous. This he was willing to admit: “The labour, however, that
forms the substance of value, is homogeneous human labour,
expenditure of one uniform labour-power.”!” He continued in
this vein:

The total labour-power of society, which is embodied in the sum total
of the values of all commodities produced by that society, counts here
as one homogeneous mass of human labour-power, composed though
it be of innumerable individual units. Each of these units is the same
as any other, so far as it has the character of the average labour-power
of society, and takes effect as such; that is, so far as it requires for
producing a commodity, no more time than is needed on an average,
no more than is socially necessary. The labour-time socially necessary
is that [which is] required to produce an article under normal condi-
tions of production, and with the average degree of skill and intensity
prevalent at the time.!@

What about skilled labor? “Skilled labour counts only as
simple labour intensified, or rather, as multiplied simple labour,
a given quantity of skilled [labour] being considered equal to a

15, Capital, 1, p. 45. [Capital, 1, p. 38.]
16. ibid. [7oid.}

17, Hbid., 1, pp. 45-46, [Zbid., 1, p. 39]
18, Bbid., 1, p. 46. [Ibid., 1, p. 39.)
