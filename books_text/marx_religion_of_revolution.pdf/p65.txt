Preface ixv

tion and the Aspen Institute for Humanistic Studies! demon-
strates the providence of God in history: He still allows the
Egyptians to be spoiled by the righteous every so often.

The book offers an arsenal of information that is damaging
to the humanist intellectual Left, for it exposes Marxism’s non-
rational origins as no book had previously done. Billington traces
the roots of both revolutionary Marxism and revolutionary Na-
tional Socialism back to two major yet long-ignored strands of
late eighteenth- and early nineteenth-century European culture:
Germanic occultism and the new profession of journalism. The
book surveys the rise of the modern revolutionary faith from the
era of the French Revolution to the Russian Revolution of 1917,
It lacks only a companion volume documenting in detail the
financial and political connections between the revolutionaries
and the secret societies; the book is already nearly flawless in
establishing the ideological connections. (I have never seen a
more competent job of proofreading, either; the book contains
virtually no typographical errors.)

Billington began researching this book at about the time
that Marx’s Religion of Revolution came into print. He describes
the academic environment of that disruptive era on campus: “As
a university-based historian during the early years of this study,
my ‘method’ was to ignore professorial debates and to spend
my time with old books and new students. The experience gave
me an unanticipated sense of ‘relevance.’ I was repeatedly struck
in the depths of libraries with precedent for almost everything
that was daily being hailed as a novelty from the rooftops out-
side.”!05 While Billington was being struck by historical paral-
Jels, students were striking outside.

I will go so far as to say that it is impossible today to
understand nineteenth-century Europe if you have not read Fire
in the Minds of Men, or if you are not thoroughly familiar with the
long-ignored primary sources that serve as the foundation of his
book. You could safely skip any other single book written about

104, Fire, Acknowledgments, p. vii.
105, ibid, pH.
