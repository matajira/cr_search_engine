126 Marx’s Religion of Revolution

invest any more capital into the production plant itself. Marx;
however, virtually ignored this latter possibility; he generally
took the attitude that the economies of scale are, for all practical
purposes, unlimited. He never bothered himself with the very
real problern of optimum plant size.

In his frantic search for profits, the small businessman will
inevitably be crushed, Marx thought. The small businessman
cannot afford to invest the huge sums of capital necessary to
increase his company’s production. This being the case,the small
competitor cannot lower the costs of his products without suffer-
ing losses, and he will be driven out of business. Marx gave no
attention to the possibility that many kinds of economic activity
may be more suited to the smaller enterprise than to a huge,
complex, highly bureaucratic, industrialized establishment. The
area of personal services is one example, and the service indus-
tries have shown a tremendous capacity for growth in this cen-
tury. It is true, of course, that ours is the age of huge firms, but
much of this growth has been the result, not of higher efficiency,
but of political intervention and the state’s inflationary policies.*6

 

35, For a survey of the literature dealing with the growth of the service industries
in the United States, see William Regan, “Economic Growth and Services,” Journal
of Business, XXXVI (1963). Cf George Stigler, Trends in Employment in the Seruiée
Industries (A Study by the National Bureau of Economic Research [Princeton, New
Jersey: Princeton University Press, 1956}). Ini 1870, something like 20 percent of
the American working force was employed in the service industries; by 1950, the
figure was over 50 percent, Most of this change was not due to a falling off of
employment in manufacturing industries, but due rather to the decline of the
number of those connected with agriculture. See Stigler, Service Industries, pp. 5-6.

36. [On the effects of state sponsored monetary inflation, see my book, An
Introduction to Christian Economics (Nutley, New Jersey: Craig Press, 1973), chaps.
1-6, The expansion of the money supply’ directly by the state, or indirectly by
state-licensed commercial hanks, redistributes wealth. Those who first gain access to
the newly created money spend it into circulation; those who receive it late, as it
“trickles down” through the economy, are forced to reduce their consumption
because of the higher prices induced by the injection of new credit money. This
monetary inflation eventually sets the “boom-bust” trade cycle into operation:
Ludwig von Mises, Human Action: A Treatise on Economics (New Haven: Yale Univer-
sity Press, 1949), ch. 20; Murray N. Rothbard, America’s Great Depression (Princeton,
New Jersey: Van Nostrand, 1963).]
