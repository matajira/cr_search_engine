The Cosmology of Chaos 37

In contrast to Hegel, who conceived of human alienation as
a spiritual-inteliectual problem, Marx saw it as a social and
productivity phenomenon. It is human material labor, not intel-
lectual labor, which is alienated, and the cure for the problem
should not be sought in the realm of thought.9 Marx absolutized
the sphere of human labor, and it is not surprising that he should
have found the solution to the alienation question in that.same
sphere.

Alienation

With this framework of humanism supporting his thinking,
Marx’s revolt against the society in which he found himself is
easy to understand. Industrialization was transforming a rural
civilization into an urbanized society, and the transition was not
an easy one. While the horrors of the so-called Industrial Revolu-
tion have undoubtedly been overemphasized, there was natu-
rally a great deal of personal and social strain involved in the
process of urbanization. Men who have worked as farmers all
their lives, for example, find it difficult to adjust to the produc-
tion methods of the factory system. The religious rootlessness
of urban life as compared with rural traditional culture is an-
other familiar theme, and necd not be reviewed here. Marx
realized the impact which urban life would have on the conserva-
tive tendencies of a formerly agricultural Europe, and he wel-
comed the transition: “The bourgeoisie has subjected the country
to the rule of the towns. It has created enormous cities, has
greatly increased the urban population as compared with the
tural, and has thus rescued a’considerable part of the population
from the idiocy of rural life.”*!

39, For a comparison of the alienation concept as held by Hegel and the
Marxian concept (plus related topics), see Herbert Marcuse, Reason and Revolution:
Hegel and the Rise of Social Theory. Cf, Frite Pappenheim, The Alienation of Modern
‘Man (New York: Monthly Review Press, 1959}, pp. 83-84.

40. Ci. Sidney Pollard, “Factory Discipline in the Industriat Revolution,” Eeo-
nomic History Review, Second Series, XVI (1963), pp. 254-71.

41, Marx, Manifesto of the Communist Party, in Selected Works, 1, p. 112. [Gollected
Works, 6, p. 488.]
