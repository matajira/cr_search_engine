154 Marx’s Religion of Revolution

for the fact that it involved the problem of relating classical value
theory to capitalism’s price mechanism,

The attempted revisions of Marx’s system are a testimony
to at least two things: (1) the absolute contradiction in the
original Marxian economic system; and (2) the unwillingness of
Marxist scholars to face the basic truth. Since they cannot re-
solve the problem, they try to argue that Marx was never con-
cerned with such matters or that such matters, even if present
in Marx’s mind, were not fundamental to his outlook, in spite
of the fact that Marx considered them to be of enormous impor-
tance. Scholarship, in this century, has not always been faithful
to the idea of rigorous truth — even a truth defined by the canons
of secular thought. As Samuelson advises: “Marxolaters, to use
Shaw’s term, should heed the basic precept valid in all societies:
Cut your losses!”

Gottfried Haberler, one of America’s most respected econo-
mists, has offered this evaluation of BOhm-Bawerk’s efforts: “In
my opinion Béhm-Bawerk’s is to this day the most convincing
and lucid analysis of the Marxist theory of value, price, capital
and interest. , . . Bohm-Bawerk’s criticism, which goes, of course,
beyond the demonstration of an internal contradiction to show-
ing the basic flaws of theory, is altogether convincing and has
never been refuted.”!!° No more fitting compliment could be
paid to a master economist by one of his peers; it is a long-
deserved memorial to a brilliant logician after a half century of
garbled “refutations” and open vilification by those whose Marx-
ist presuppositions have interfered with their reasoning pro-
cesses,!11

109. Samuelson, of. cit., A.E.R. (1957), p. 892.

110. Haberler, “Marxist Economics in Retrospect and Prospect,” in Milorad
M. Drachkovitch (ed.), Marxist Ideology in the Contemporary World — lis Appeals and
Paradoxes (New York: Praeger, 1966), p. 115.

IIL. Ina letter to me dated 4 March 1967, Professor Hans Sennholz writes: “It’s
an indication of the incredible shallowness of contemporary thought that respected
economists can deny the Marxian contradictions of fact and reality. Surely Marxian
error is as important today as it was 85 years ago when Bohm-Bawerk wrote his
rejoinders. If Marx were right for the ‘aggregate’ then in my belief capitals of equal
