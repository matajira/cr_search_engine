liv Marx’s Religion of Revolution

have warned economists that something was fundamentally wrong
with the labor theory of value. Yet Marx lived out the labor
theory of value during the final sixteen years of his life, He
substituted frantic intellectual activity for meaningful intellec-
tual production.

Raddatz has recognized the fragmented nature of Marx’s
legacy: “The fact that Marx’s life’s work remained fragmentary,
therefore, cannot be laid at the door of external circumstances.
Since, apart from his great polemics or works of criticism and
shorter inflammatory writings, everything remained uncompleted,
the question arises whether this was due to some fundamental
tendency.”®? Marx was endlessly rewriting pieces that were more
than a month old. His son-in-law Paul Lafargue records that
Marx could not bear to publish anything that was Iess than
perfect. (What he needed, I can say with confidence, was the
unbreakable schedule imposed: by four newsletter essays per
month, plus book publishing deadlines. What he needed was
ownership of a profit-seeking publishing firm. These eliminate
such perfectionist tendencies.) Yet he left behind a mouniain of
notebooks and jumbled papers.*? And out of this jumble many
academic reputations have been constructed!

Except for The Economic and Philosophical Manuscripts of 1844
and selected parts of The German Ideology — the key transitional
document in Marx’s thinking — the items that remain most im-
portant from his file of unpublished manuscripts are the pieces
that he wrote late in his career in order to repair his economic
system’s visible inconsistencies, and then failed to publish be-
cause his answers created more problems than they solved. As I
mentioned in Marx’s Réligion two decades ago, the fact that he
got to the end of Volume 3 of Capital without defining “class” is
significant. He started to define that crucial term, but the manu-
script ends two paragraphs later. The manuscript then sat on his
shelves for well over a decade, gathering dust. Mises is correct:
“Significantly the third volume breaks off after a few sentences

82. Raddatz, Karl Marx, p. 237.
83. idem.
