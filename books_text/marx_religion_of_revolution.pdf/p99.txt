The Cosmology of Chaos 25

sophical necessity. History, in Hegel’s scheme, is dynamic, non-
cyclical, linear; all historical facts are therefore unique. History
is developing to a point at which there will be an ultimate
reconciliation of the many with the unity of the one. Unity and
diversity wili be transcended, and subjective knowledge and
objective knowledge will become one supreme form of knowl-
edge. Unfortunately, this will not happen in time, since the
historical process is eternal.?

The final resolution of this dualism serves as a backdrop — a,
limiting concept — for Hegel’s view of history. History, in short,
is the self-conscious development of the Spirit, and man is only
a means in that development. Man is driven by “the cunning of
history.” True freedom for man therefore consists “in submitting
to the inner necessities which are gradually working themselves
out in social institutions and not in attempting to force matters
by revolutionary action.” !®

The incredible subtleties of Hegel’s triadic scheme of his-
torical and logical development have baffled the best of philo-
sophic minds. Some have gone so far as to deny that any “thesis-
antithesis-synthesis” formula exists in Hegel’s system.'! Others,
taking a more moderate approach, admit that the triadic form
of reasoning was present in the system, but that no simple
gerieralization can be. made about the way in which Hegel used
it.” In any case, it is probably safe to say that Hegel saw the
process of history as the reunification of the Spirit from its
alienated condition; it is a dialectical process whereby historical-
logical contradictions are overcome by discontinuous “leaps” or
syntheses. Louis J. Halle has put it this way: “History, for Hegel,
is the dialectical process by which God overcomes his alienation.
Replace ‘God’ with ‘Man’ and this is what history is for Marx

9, Berlin, Karl Mars, pp. 46-56.

10. Sidney Hook, From Hegel to Marx (Ann Arbor: University of Michigan Press,
(1950) 1962), p. 78,

11, Gustav E. Mueller, “The Hegel Legend of ‘Thesis-Antithesis-Synthesis,’”
Journal of the History of Ideas, XTX (1958), pp. 411-14

12, J.N, Findlay, Hegel: A Re-examination (New York: Collier, 1962), pp. 69-71.
