250 Marx’s Religion of Revolution

ona whirlwind European tour.*?

Finally, in 1863, Engels was able to scrape together £125,
and possibly more ~ the record is unclear ~ for Marx’s relief.#
It was on this occasion that Engels criticized Marx openly, the
only time he ever did so. In January, Engels’s “wife” died, and
he wrote to Marx in despair. Marx replied with two brief sen-
tences of regret and then launched into a description of his own
financial woes. Engels was infuriated, told Marx so, and Marx
apologized — possibly the only time in his adult life that he
apologized to anyone outside his immediate family. So, Engels
sent him the money, and the two partners were reconciled.

In late 1863, Marx’s mother died. His share of the inheri-
tance, minus the advance, came to something less than £100.
He collected this early in 1864. It was enough, as one biography
puts it, to mitigate “at least the worst of Marx’s distress.”® Then
came the deluge. An obscure German follower, Wilhelm Wolff,
one of the original eighteen conspirators of the 1846 League of
the Just, died and left Marx the staggering (by 1864 standards)
sum of £824.5' Marx later dedicated Das Kapital to Wolff. In
September, Engels was made a full partner in his father’s firm,
and may have been less resentful than usual when Marx de-
manded an additional £40, which he insisted was owned to him
by Engels (who was executor of Wolff's estate).>3 Thus, in one
year Marx was the recipient of almost £1000.

When I first began looking into Marx’s finances (prior to the
publication of Payne’s revealing biography), I began to wonder
just how much this money amounted to in terms of purchasing

47, Payne, Marx, p. 330.

48, Ibid., pp. 339-40. The Nicolaievsky biography reports that Engels actually
paid Marx 350 pounds in 1863, although I am inclined to doubt this figure.
Nicolaievsky and Maenchen-Helfen, Kar! Mara, p. 253.

49. Ibid, p. 346.

50. Nicolaievsky and Maenchen-Helfen, Karl Merz, p. 258.

51. Payne, Marx, p. 354.

52. Berlin, Karl Mars, pp. 247-48.

53. Payne, Marz, p. 354.
