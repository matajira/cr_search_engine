52 Marx’s Religion of Revolution

Philosophy — class philosophy—is one of the tools which
the proletariat can use in its war against the fetters of capitalist
production, and after the revolution, philosophy will cease to be
‘divided, since all classes other than the proletariat will be dis-
solved. In other words, philosophy is nota mere tool of the mind,
a means for merely comprehending the world; philosophy is a
weapon. “Just as philosophy finds its material weapons in the
proletariat, so the proletariat finds its intellectual weapons in
philosophy.”®

Dialectic of History: Law vs. Flux

At this point, it would probably be wise to return to the
original problem which was raised in the. early pages of this
chapter. The dilemma: which confronted Marx was the one
which has confounded all secular thinkers who have considered
the problem of philosophy: how can we relate the flux of history
to general laws which are permanent and which regulate the
flux? If we are to understand (and influence) history, then we
need standards of evaluation by which we can examine, explain,
catalogue, control, and to some extent predict history. Marx
turned to the historical process itself; and specifically to the
economic and social elements of history, in his search of those
laws of development. Not logic, not military or political history,
not church history, but economic history is supposedly the key
which unlocks the closed door of the mysteries of the past,
present, and future. He thought that he had discovered a pattern
of development in economic history, a pattern which could be
used to predict the future of mankind.

Popper's Critique
It is this “historicist” methodology — the attempt to explain
history by means of laws inherent in history itself— which has

80. Marx, “Contribution to the Critique of Hegel's Philosophy of Right,” in Kar!
Marx: Early Writings, p. 59. [Gollected Works, 3, p. 187.] This essay was originally
published in 1844. Cf, Adam Schaff, “Marxism and the Philosophy of Man,” in
Fromm (ed.), Socialist Humanism, p. 348.
