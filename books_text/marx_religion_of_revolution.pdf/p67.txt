Preface lxvii

(That Communist Karl employed a household servant who had
literally been given to his wife by his mother-in-law is more than.
a little ironic. Gouldner properly refers to this as a feudal gift.!!)

Payne also shows that Engels’s subsidy to Marx in his later
years made Marx a rich man, contrary to received academic
opinion, (Payne’s book and Marx’s Religion were the first books
to reveal this fact, as far as I am able to determine.) Payne did
not endear himself to professional historians by making them
appear by comparison to be lazy, since previous biographers had
not done their homework regarding Marx’s illegitimate son or
his income level. Payne also started out with two additional
strikes against him: he was not a professionally certified histo-
rian, and he wrote so many excellent books. Financially success-
ful amateur historians — that is, market-certified historians rather
than taxpayer-supported or donor-supported historians — are
too often resented by unpublished and unknown scholars who
write the book reviews for non-profit and generally unread pro-
fessional journals,

The second book is Francis Nigel Lee’s massive 1,177-page
study of Marxism, Communist Eschatology (Craig Press, 1974).1!!
It is by far the best study of Marxist philosophy ever published.
It also contains an excellent 100-page chronological table of the
history of the world, 130 pages. of footnotes, and 88 pages of
bibliography. Unfortunately, it lacks an index, which in a world
of lazy and harried scholars is nothing short of catastrophic for
any academic book’s influence. The book immediately sank with-
out leaving a trace, much as mine did and Payne’s did. The fact
that under-funded Craig Press had published it was partly res-
ponsible. The fact that the original manuscript had.served as one
of his two doctoral dissertations did not help, either. Doctoral
dissertations tend to be the equivalent of chloroform in print.
But the plain fact is, thousand-page conservative Christian books
that deal with the history of ideas do not sell well these days, or

110, Gouldner, Fhe Tivo Marxisms, p. 277.

II, It has the lively subtitle, A Christian Philosophical Analysis of the Post-
Capitalistic Views of Marx, Hngels and Lenin,
