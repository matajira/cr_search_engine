Preface XRV

tion, he was no better than others of his day. He was to go in for
economics later, basing his thought on the classical and rather naive
labour theory of value, but it was not as an economist that he would
achieve the topmost heights of distinction. As a political analyst he
was surely not as good as his contemporary of lesser fame, Walter
Bagehot; as a social philosopher he was inferior te Alexis de Tocqueville.
His development of the sociological view that men’s concepts reflect
the material circumstances of their productive lives — this certainly
would entitle him to an important place in the history of human
thought. But it is hardly commensurate with the magnitude of his
influence.

Marx was extraordinary, I conclude, not as a man of action or as
an academic thinker, but as one of the great visionaries of history. It
was the Karl Marx who saw an immense and enthralling vision of
human society, the Karl Marx who on the basis of that vision created
a compelling myth of human society —this is the Marx who was
extraordinary among his contemporaries. He had more of St. Paul in
him than of the social scientist or the empirical scholar. His mission,
too, began with a vision on the Road to Damascus.

T asked myself: Is Halle correct? Concerning Marx's intellec-
tual attainments, he is generally correct: Marx was not a distin-
guished scholar. Concerning the influence of Marx’s religious
vision, he is also correct, although I am unaware of any Damascus-
type experience. He did lose his youthful commitment to liberal
Christianity almost overnight, in between his graduation from
the Gymnasium and his early years as a college student.* This has
been a familiar pattern in the West for well over a century.
Admittedly, few students write poems like Marx’s 1841 poem,

3. Louis J. Halle, “Marx's Religious Drama,” Encounter, XXV (Oct., 1965),
p29.

4. The evidence of his youthful Christianity is found in an 1835 school essay,
“On the Union of Believers with Christ According to John 15:1-14.” His radically
anti-Christian unpublished one-act play, Oulanem, was made available in English
in Robert Payne’s 1971 collection, The Unknown Karl. Marx (New York: New York
University Press, 1971). It is undated, but it was an carly effort, Oulanem, writes
Payne, is an anagram: oulanem = Manuelo = Immanuel = God (p. 63). Im-
manuel is the New Testament word meaning “God with us” (Matt, 1:23). Both of
these documents are now are available in Volume 1 of the Collected Works (New
