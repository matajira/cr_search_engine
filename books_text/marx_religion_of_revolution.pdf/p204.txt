130 Marx’s Religion of Revolution

pauperism develops more rapidly than population and wealth.”
Again, in Value [Wages], Price and Profit (1865), we read: “The
general tendency of capitalistic production is not to raise, but to
sink the average standard of wages, or to push the value of labour
more or less to its minimum limit.”*© Certainly, it would not be
deliberately misleading to argue, as many have done, that Marx
did believe that the condition of the workers was clearly going
to decline absolutely.”

On the other hand, many commentators have taken the
position that Marx actually taught a doctrine of relative increas-
ing misery, i.e., that the standard of living might be rising
somewhat even for the working class, but rising far more slowly
than production would warrant. Most of the wealth would go
either into the capitalists’ accumulation or into their personal
consumption budgets. This has been the view of many Marxists
since the time of Karl Kautsky; it is apparently the view of an
increasing number of non-Marxists scholars.** Perhaps the most
explicit statement of the relative increasing misery thesis is found
in Wage-Labour and Capital (1847): “If capital is growing rapidly,
wages may rise; the profit of capital rises incomparably more
rapidly. The material position of the worker has improved, but

45. Marx and Engels, The Manifesto of the Communist Party (1848), in Selected
Works, 1, p. 119. [Collected Works, 6, p. 495]

46. Marx, Value [Wages], Price and Profit (1865), in Selected Works, 2, pp. 74-75.
[Collected Works, 20, p. 148]; Cf. Capital, 1, pp. 707-9 {Capital 1, pp. 644-46}.

47. Some of those who have argued this way out are M. M. Bober, Karl Merx’s
Interpretation of History (New York: Norton, [1948] 1965), pp. 213-21; G. D. H. Gole,
The Meaning of Marxism (Ann Arbor: University of Michigan Press, [1948] 1964),
pp. 113-18; John Kenneth Turaer, Challenge to Karl Mars (New York: Reynal &
Hitchcock, 1941), ch. 4; Joseph Schumpeter, Capitalism, Sociatism and Democracy
(New York: Harper Torchbooks, [1942] 1962), pp. 34-35,

48. M, Dobb, On Marsism Today (London: The Hogart Press, 192), p. 105
Ronald L, Meek, “Marx’s ‘Doctrine of Increasing Misery’,” Science and Society,
XXVI (1962), pp. 422-41; Thomas Sowell, “Marx’s ‘Increasing Misery Doctrine’,”
American Economic Review, L. (1960), pp. 111-20. Sowel} argues that Marx did hold
to the absolute increasing misery doctrine before 1850 or so, but in the context of
this chapter, I have tried to indicate that he also wrote in terms of it after 1850. Cf.
Sowell, ibid., p. 113; Abram L. Harris, “The Social Philosophy of Karl Marx,”
Ethics, INTL (April, 1948), pt. 11, pp. 24-27.
