Marx was unfortunately averse to describing how his utopia
was supposed to work. Nevertheless, one can still infer from his
many indirect references to the communist society that some
sort of democratic procedures would be constructed through
which the goals of society could be formulated. After this is done,
scientists would devise rational comprehensive planning proce-
dures to implement these goals. Since this planning, to be mean-
ingful and scientific, must obtain control over all the relevant
variables, Marx consistently foresaw it as centralized and com-
prehensive. The commonly owned means of production would
be deliberately and scientifically operated by the state in accor-
dance with a single plan. Social problems would henceforth be
resolved not by meekly interfering with a competitive market
order but by taking over the whole process of social production
from beginning toend. . . .

This comprehensive or engineering model of planning will
be shown to be the only completely coherent notion of planning
advanced in the literature of radicalism, but even it is fundamen-
tally flawed. The social engineering approach mistakes the econ-
omy for the rather mechanical process by which an individual
technician solves a given problem, when the economic system is
actually more like the overall social process of scientific discov-
ery. Science and the market are not limited to the solving of given
and well-defined problems by known procedures. They also in-
volve the very process of conceptualizing the problems and dis-
covering the procedures. The notion of comprehensive planning
represents the nineteenth century’s boldest attempt to apply its
mechanistic view of science to society to yield a program for
radical change. But it can no longer serve in a century that is,
for good reasons, abandoning that view of science,

Don Lavoie*

“Lavoie, National Economic Planning: What Is Left? (Cambridge, Massachusetts:
Ballinger, 1985), p. 19.
