The Economics of Revolution ~ 133

of men would face abject poverty and deprivation, while at the
same time their presence within the economy would help to
make conditions worse for those of their fellow proletarians who
happened to be employed. The problem with this whole thesis,
other than the fact that such an army has never materialized,
has been pointed out by the Fabian socialist scholar G, D, H.
Cole:

But Marx nowhere explained why, if the capitalist class managed to
rise to power, in most countries, not by. catastrophic revolution over-
throwing the previous ruling class, but rather by a gradual process of
encroachment and adaptation of the established social structure, in-
creasing misery should be the means to the conquest of power by the
‘proletariat, whereas increasing prosperity had been the weapon of the
bourgeoisie. Yet the view is plainly paradoxical; for, on the face of the
matter, the increase of misery would be much more likely to weaken
and dispirit a class than to aid it in the prosecution of the class
struggle. . . . In effect, if Marx had been right, the probable outcome
would have been the collapse of Capitalism under conditions in which
the proletariat would have been too weakened by its misery success-
fully to establish an alternative system. In these circumstances, if there
had been no other aspirant to the succession, a collapsing Capitalism
would have been likeliest to be succeeded, not by Socialism, but by
sheer chaos, and by the dissolution of the entire civilization of which
Capitalism had been a phase.*

Cole, of course, favored a gradual transition to socialism, and
thus was hostile to Marx’s openly revolutionary approach. But
whatever Marx “really meant” by the doctrine of increasing
misery, it seems safe to say that it is no longer a tool of economic
analysis in the contemporary Marxist critique of capitalist soci-
ety. Even the idea of relative increasing misery fails to explain
the great advances made by members of the working class in
improving their standard of living over the last 100 years. Capi-
talism simply has not brought deprivation to Western workers.
A good Fabian would give the credit to the role of the state and
trade unions in forcing capitalism into reforms. A good free

54. Cole, The Meaning of Marxism, pp. 113-14,
