4 Marx’s Religion of Revolution

the Catholic—is to begin right now joint actions to reconstruct
society and to advance, through successive stages, to the creation
of a society where both ideologies will be put to the test. So why
not make the experiment?”® There are many reasons why the
experiment should not be made, and some of them are discussed
at length in this study.

Marx, like the Devil, must be given his due. He created a
vast, compelling intellectual structure, perhaps the greatest of
the post-Hegelian attempts to bind together the contradictions
of man’s self-proclaimed autonomous reason. The attempt failed,
but we should be willing to acknowledge his éfforts. No modern
historian or social thinker can fully escape the influence of Marx’s
intellect, as Raymond Aron has argued.’ In a sense, this book
takes a stand against all those who think that all of Marx’s labors
were silly. Some of his ideas were silly, and they deserve to be
treated as such. For this reason I cannot agree with one critic of
this study who wrote: “You should remove the quality of sarcasm
from your writing when you write about a great historical figure
like Marx.”® In the history of scholarship, there has been no
more sarcastic, vitriolic writer than Karl Marx, and since he
established the precedent, who am I to depart from it? But on

 

article, “Dialogue: Christ and Marx,” Nawsweek (Jan. 6, 1967), pp. 74, 76. Cf.
Christopher Wren, “Can Christians Talk to Communists” Look (May 2, 1967),
pp. 36ff [Since I wrote this, the movement known as Liberation Theology has
appeared. The bibliography on this influential movement is large and growing. The
primary publishing outlet in the United States is Orbis Books, The major institu-
tional promoter is the Maryknoll religious order.]

6. Santiago Alvarez, “Towards an Alliance of Communists and Catholics,”
World Marsist Review, VIII (June, 1965), p. 47. See also the report on the conference
held between Catholics and Marxists in the spring of 1965: Walter Hollitscher,
“Dialogue Between Marxists and Catholics,” World Marxist Review, VITE (August,
1965), pp. 53-58. Hollitscher’s statement is to the point: “Humanistic tendencies
must be upheld without prejudice — that is the common ground on which atheists
and believers can mect for joint action” (56).

7. Raymond Aron, The Opium of the Intellectuals (New York: Norton, 1962), p.
105,

8, [Let it be known in 1988: the critic was Howard Sherman, a syndicalist who
calls himself a Marxist,]
