The Economics of Revolution 153

G. D. H. Cole has argued that Marx’s theory of value was
not an explanation of prices at all! It was merely a theory of
capitalist exploitation.!°’ He fails to mention that Marx’s theory
of exploitation was written only in terms of a theory of capitalist
prices. It would have surprised Marx to have learned that all of
his time spent in computing price data, poring over statistics in
the British Museum, and formulating his theory of exchange — a
system based on the pricing mechanism — was spent in vain.
Cole’s argument is certainly unique.

Perhaps the most startling revision is Sweezy’s most recent
contribution. Not only was Marx not talking about a theory of
prices, he was not really interested in explaining the capitalist
economy in terms of classical economic theory: “The first nine
chapters of Capital, it is now widely recognized [! ? !], are not
primarily concerned with exchange value or prices in the sense
of either classical or neoclassical economics but rather with what
today might be called economic sociology.” Poor Marx; he
actually imagined that his system was a total one. He thought
that he had constructed a theoretical framework which demon-
strated all the contradictions of capitalism, whether economic
or social. He actually believed that he was an economist who
used the very presuppositions of Adam Smith and Ricardo in
order to demonstrate with their own intellectual tools that capi-
talism is doomed. But his followers have proven to their own
satisfaction that Marx really had not accomplished this, even
that he had not attempted to do so; he was a sociologist primar-
ily, and not a classical economist. Odd, under these circum-
stances, that Marx was so concerned in volume 3 with his
hopeful demonstration of the basic validity of the economic
outline of volume |. For a sociologist who was not supposed to
have been interested in price theory, he certainly struggled for
many pages with a problem which would not have arisen except

107, Cole, The Meaning of Marxism, p. 210.
108, Paul Sweezy, “Professor Cole’s History of Socialist Thought,” American
Economic Review, XLVI (1957), p. 990.
