The Biography of a Revolutionary 13

brilliant. economist, was in poverty and continual debt through-
out his life."

[The following section on Marx’s upper middle-class income
and his squandering of it was, as far as I am aware, the first
analysis ever published that referred to statistics regarding in-
come levels of Marx’s day. In the same year that this book first
appeared, 1968, Robert Payne’s biography of Marx also revealed
this fact. It also discussed in greater detail Marx’s purchase of a
nice home and other luxuries.!3 I expanded my discussion of this
story in a 1971 essay, which is reprinted as Appendix C. Even
today, very few students know about this side of Marx’s life. The
historians have simply not done their homework or are predis-
posed to remain silent about their findings for ideological rea-
sons.]}

Marx’s hostility to bankers and capitalists in general, and
Jewish moneylenders in particular, may have stemmed in part
from his own inability to make ends meet. In 1866, only two
years after his £1150+ windfall, he was searching for a loan at 5
percent; he was then paying out 20-30 percent.’ His illnesses
were expensive, and his attempt to keep his three daughters in
bourgeois luxury also drained his finances, but this kind of debt
is remarkable. As he wrote to Dr. Kugelmann in 1866, “I am
faced with a financial crisis in the immediate future, a thing which,
apart from the direct effects on me and my family, would also
be disastrous. for me politically, particularly here in London,
where one must ‘keep.up appearances.’”!6 London’s revolution-
ary circles were apparently afflicted with a severe dose of “bour-
geois affectations,” and Marx was no exception. It is ironic that

 

12. 1 am using the terms “radical” and “liberal” in the same sense as Robert
A, Nisbet uses them in The Sociological Tradition (New York: Basie Books, 1966), pp.
9-16, and as he uses them in his The Quest for Community (New York: Oxford
University Press, 1953).

13. Payne, Marx, pp. 350-59.

14, American Opinion (April 1971).

15, Letter to Kugelmann, 13 Oct, 1866: Letters t» Kugelmann (New York: Interna-
tional Publishers, 1934), p. 42.

16, Ibid.
