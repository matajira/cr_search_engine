Bibliography 265

Marxian socialists from whom Marx drew many of his eco-
nomic ideas,

Hook, Sidney. From Hegel t@ Marx, Ann Arbor; University of
Michigan, 1962. A reprint of the 1950 edition in which Hook
traces the development of left-wing Hegelianism which culmi-
nated in the Marxian system. The chapters on Feuerbach are
especially important.

Hook, Sidney. Towards an Understanding of Karl Marx. New York:
John Day, 1933. One of Hook’s early analyses of Marx. It
pictures Marx as basically a pragmatist, and it is as much a
reflection of Hook’s views at the time as it is of Marx’s. Still,
itis a well-written piece.

Lenin, V. L. The Teachings of Karl Marx. New York: International
Publishers, 1964. A short introduction to the Bolshevik view
of Marx, published first in 1917.

Lichtheim, George. Marzism: An Historical and Critical Study. New
York: Praeger, 1963. Probably the most important recent
study of Marxism. Lichtheim offers several new theses con-
cerning the development of the system. He distinguishes Marx’s
contributions from Engels’s later additions; he divides Marx’s
own thought into chronological periods; and he tries to argue
that after 1850 Marx was not really interested in revolution.

Marcuse, Herbert. Reason and Revolution: Hegel and the Rise of Social
Theory. Boston: Beacon Press, 1964. This book throws much
light on the Hegelian conception of alienation, and it contrasts
this with the idea of alienation which the young Marx held.
Both emphasized the salvation of man in terms of labor: Hegel
saw the process as mental labor; Marx saw it as material,
social labor.

Marxism and Alienation: A Symposium, New York: American Insti-
tute for Marxist Studies, 1965. Included are several essays on
the general theme of human alienation as it relates to art,
philosophy, and the American social order. The bibliography
