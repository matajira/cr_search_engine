XXX Marx’s Religion of Revolution

targets were almost always the writings of his socialist rivals, and
usually very obscure rivals at that, not. Adam Smith, David
Ricardo, John Stuart Mill, or other important advocates of clas-
sical economics. He wrote notes of criticism on the classical
economists, but these were not published in his lifetime: Theories
of Surplus Value. Marx never provided any blueprints regarding
the operation of the communist society to come. He offered no
program for building a new society after the revolution, except
for the famous ten points of the Communist Manifesto (1848). He
never again brought up the subject of the transition from capital-
ist to socialist to Communist society. Ten points in a pamphlet
do not a civilization build. He then stole this phrase from Morelly’s
Code de la Nature (1755-60): “From each according to his ability,
to each according to his needs!”! This is a slogan, not a pro-
gram. Lenin played the same game when he wrote that a Com-
munist society is simply one which combines political power and
electricity,!! one which gives equal pay to all workers and can
be run by simple bookkeepers,'? oné in which gold will be used
for public lavatories.’ Marx and Lenin could produce. slogans

10. Marx, Critique of the Gotha Program (1875), in Marx and Engels, Selected
Works, 3 vols. (Moscow: Progress Publishers, 1969), 3, p. 19.

11. “Communism is Soviet power plus the electrification of the whole country.” V. L.
Lenin, “Communism and Electrification” (1920), in The Lenin Anthology, edited by
Robert C. Tucker (New York: Norton, 1978), p. 494.

12, “Accounting and control —these are the chief things necessary for the organ-
izing and correct functioning of the first phase of Communist society. Alf citizens are
here transformed into hired employees of the state, which is made up of the armed
workers. All citizens become employees and workers of ene national state ‘syndicate.’
All that is required is that they should work equally, should regularly do their share
of work, and should receive equal pay. The accounting and control necessary for
this have been simplified by capitalism to the utmost, till they have become. the
extraordinarily simple operations of watching, recording and issuing receipts, within
the reach of anybody who can read and write and knows the first four rules of
arithmetic.” Lenin, State and Revolution (New York: International Publishers, [1918]
1932), pp. 83-84.

13. “When we.are victorious on a world scale I think we shalt use gold for the
purpose of building public lavatories in the streets of some of the largest cities in
the world.” Lenin, “The Importance of Gold Now and After the Complete Victory
of Socialism” (1921), in The Lenin Anthology, p. 515.
