160 Marx’s Religion of Revolution

were not always constantly in flux, a fact which Marx readily
admitted in other places!!2

Supply and demand are dynamic factors; even at those points
where market prices do equal cost prices, there is no cessation
of economic forces.!2? At the point of perfect balance, there could
be no profits in Marx’s system of analysis, since costs and market.
prices are identical. Then why should he criticize the capitalist
system? Capitalism uses the entrepreneur to forecast the future
state of the market; if all capitalists were to succeed, all profit
would disappear. Marx, therefore, had nothing but contempt for
the very men whose function tends toward the conquest of uncer-~
tainty. He never saw capitalism for what it is: the response of
sinful and limited men to make the best of an uncertain, imper-
fect and falien world, Marx demanded an economic paradise
where there would be no scarcity, no uncertainty, and no capital-
ist entrepreneurship. It is only this kind of world which can
dispense with profits. Marx wanted heaven on earth, or more
accurately, he wanted an escape from time and the curses which
time has brought. His vision of socialism ultimately required a
static universe in which there would be no change whatsoever,
or at least where all change could be accurately predicted and
controlled. Because the capitalist system failed to meet this
requirement, he rejected it as the creation of alienated mankind,
a temporary period which would come to an end with the Revo-
lution. He castigated the capitalist for deviating from the utopian
conception of a perfect world.°8

Conchision

Marx began with the assumption that the labor theory of
value is operative in capitalist economic affairs. A good must
contain an equal quantity of human labor with any other good
if an exchange is to take place. Prices, therefore, should be in
direct proportion to the quantities of labor contained in the

126, Jbid., 3, pp. 150, 190, 230. [/4id., 3, pp. 126, 161, 195,]
127, See Bohm-Bawerk’s remarks in “Unresolved Contradiction,” pp. 280-85,
128. Cf. Bober, Kar! Marx’s Interpretation of History, ch, 14,
