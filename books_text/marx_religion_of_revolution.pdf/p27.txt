Preface xxvii

that this same goal is inherent in many forms of modern human-
ism.® I recognized immediately that Marxism is in fact a ration-
alist recapitulation of this ancient religious impulse. Then I read
Halle’s essay. I saw immediately that he was correct about Marx
the religious visionary, but incorrect about the nature of Marx’s
vision. I concluded that what was needed was a self-consciously
biblical refutation of the major works of Marx and Engels, since
no such study existed.” The. free market disagreed with me.

6. R. J. Rushdoony, The Religion of Resolution (Victoria, Texas: Trinity Episco-
pal Church, 1965).

7. It was published as part of Craig Press’s University Series: Historical
Studies, The year after Marx’s Religion appeared, Craig Press published two addi«
tional books on Communism in this same series, James D, Bales’s Communism and
the Reality of Moral Law and Francis Nigel Lee's Communion Versus Creation. Wedge,
the nec-Dooyeweerdian publishing company in Toronto, published Johan van der
Hoven’s Karl Marx: The Rools of His Thought in 1976, A mote unreadable academic
book can hardly be imagined. I cannot resist mentioning briefly a book published
‘in 1980 by InterVarsity Press: Professor Klaus Bockmuehl's The Challenge of Marx
ism. It is also short. It is a nice, pleasant, earnest, riarrowly focused, cautious
academic discussion of the atheist character of Christianity’s most important world-
wide religious rival. Imagine, if you can, a book on the philosophy and history of
Marxism that does not discuss the following academically unpleasant topics: the
Soviet secret police system, the mass starvation of the early 1920's, Stalin’s murder
of his leading rivals, his forced coilectivization of agriculture and destruction of 6
million kulak peasants, the resulting famine of the early 1930's, his purges of the
Yate 1930’s in which 20 million co 30 million people died, his terrorism, the history
of the concentration camps (the Gulag}, Communism’s-philosophy of worldwide
domination by military conquest and illegal subversion, actual Soviet military
expansion, the Soviets’ financing of international terrorism, Coramunist takeover
of foreign nations’ arts, media, academia, and governments, the Soviet’ arsenal of
nuclear and chemical weapons, the Communist wars of liberation, Soviet disinfor-
mation programs, or the international Soviet spy network, It also does not mention
Mao’s equally ruthless execution of some 60 to-100 million Chinese. It does not
discuss the failure of socialist economic planning. Rather than mentioning Solzhe-
nitsyn's years in the Gulag, his expulsion from the USSR, or the government’s
systematic suppression of his works, Bockmuehl focuses on this “horror story”: the
government insisted “that Alexander Solzhenitsyn replace the capital ‘G’ in God
in one of his manuscripts with a small letter before publication” {p. 111). Ob, the
ruthlessness of Communism! This is whai I call the anti-Communism of the faculty
lounge. It is not that it is technically incorrect; itis that it is irrelevant. If you want
to read about “the challenge of Marxism,” read some victims’ prison memoirs, such
as. Solzhenitsyn’s three volumes of The Gulag Archipelago (New York: Harper &
Row, 1974-79), or Vladimir Bukovsky’s To Build a Castle (New York: Viking, 1978),
or Armando Valladeres’s Against All Hope (New York: Knopf, 1986).
