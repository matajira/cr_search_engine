The Economics of Revolution 151

“cut-throat” competition of these “ruthless” industrialists — the
vision which captivated Marx in volume i — now appears in a
modified form: sharcholders of one happy company can receive,
automatically, their share of the average profits. Bohm-Bawerk
exploded this aggregate argument forever: there is no such na-
tional sum of common profit from which each capitalist cuts his
share.!“° It is a totally static conception of profit.

Volume I vs. Volume 3

Marx admitted only that “The foregoing statements are
indeed a modification of our original assumption concerning the
determination of the cost-price of commodities.”"' It was more
than a modification; it was a total refutation of his earlier posi-
tion. He had argued before that the value of a commodity and
its price had to be equal; his theory of surplus value was offered
precisely as a solution to the problem of capitalism’s profits
arising in an economy where equal values must be exchanged for
equals. Yet in volume 3 he offered as a mere modification the
statement that “the price of production may vary from the value
of a commodity. . . .*!? Béhm-Bawerk pinpointed the issue,
since he was unwilling to permit Marx to escape from an abso-
lute contradiction as if the later revision were merely a modifica~
tion of the earlier: “There are two possible alternatives. The first
alternative is that a permanent system of exchange is really
established whereby goods are exchanged at values which are in
proportion to the labor that the respective goods represent, and
whereby, furthermore, the magnitude of the surplus proceeds to
be derived from production is really determined by the quantity
of labor expended. If that alternative obtains, then any equaliza-
tion of the ratio of surplus proceeds to capital is an impossibility.
The second alternative is that such an equalization does take
place, If that alternative obtains, then products cannot possibly

100. Shorter Classics, 1, pp. 230-35.
101. Capital, 3, p. 194. [Capital, 3, p. 164.]
102. Ibid., 3, p. 194. [Tbid., 3, p. 164.)
