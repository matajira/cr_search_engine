The Economies of Revolution 145

problem. No one in the decade ever succeeded in carrying off the.
prize.

Profit: No Consistent Explanation

The issue which faced Marx was simply this: his definition
of the rate of surplus value did not conform to his explanation
of the rate of profit. He had defined surplus value as that addi-
tional labor time in the production process over and above the
labor necessary to produce the laborer’s minimum subsistence
wage. The capitalist appropriates the value of this extra labor,
and this is the sole source of his profits. The rate of surplus value
was defined as the surplus value divided by the wage: s/v. The
rate of profit is something completely different. It was defined
by Marx as the ratio between the surplus value and the total
capital invested, including the constant capital: s/c+v. In other
words, the capitalist calculates his return not in terms of surplus
value as such, but in terms of the profits of his industry in
comparison to his overall capital outlay. Obviously, if he em-
ploys only one man to run a multi-million dollar machine, he can
extract his profit only from the surplus living labor time contrib-
uted by that one man; the capitalist would be out of business
very quickly if Marx’s theory were correct. This raises a distinct
problem, as Bohm-Bawerk, pointed out with such devastating
effect.

Consider, B6hm-Bawerk said, Marx’s favorite example: an
industry exists in which the workers earn their salaries in the first
six hours of labor, yet they are forced to work an additional six
hours for the capitalist. The rate of surplus value is s/v, or 6
hrs./6 hrs., or 100 percent. We know, however, that different
industries have different. organic compositions of capital. One
industry may be labor intensive, with 20 c (constant capital) and

85. A list of these essays is found in Bohm-Bawerk’s 1896 essay, which is
generally translated as Karl Marx and the Close of His System. Tam using a more recent
edition, “Unresolved Contradiction in the Marxian Economic System”; in The
Shorter Classics of Bihm-Bawerk (South Holland, Iliinois: Libertarian Press, 1962),
vol. 1, p. 2i0n. [Libertarian Press is now located in Spring Mills, Pennsylvania.]
