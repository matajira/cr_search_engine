20 Marx’s Religion of Revolution

reduced to the rational. That is to say if the natural man is to
make any intelligible assertions about the world of ‘reality’ or
‘fact’ which, according to him is what it is for no rational reason
at all, then he must make the virtual claim of rationalizing the
irrational, To be able to distinguish one fact from another fact
he must feduce all time existence, all factuality to immovable
timeless being. But when he has done so he has killed all indi-.
viduality and factuality as conceived of on his basis. Thus the
natural man must on the one hand assert that ali reality is
non-structural in nature and on the other hand that all reality is
structural in nature, He must even assert on the one hand that
all reality is non-stricturable in nature and on the other hand
that. he himself has virtually structured all of it. Thus all his
predication is in the nature of the case self-contradictory.””

Dialectics (Dualism) in Humanist Philosophy

‘In Greek philosophy, the dualism between law and “brute
factuality” appeared as the “form-matter” controversy (or the
“appearance-reality” dualism). Externally existing forms (Ideas)
were the basic reality in nature, and these metaphysical forms
were to be used as the standards by which order could -be
imposed upon a recalcitrant fluctuating matter. These meta-
physically existing forms were the philosophical corollaries of
raw matter which was in total flux; absolutely static laws were
to regulate a fluctuating matter which was ruled completely by
chance,

During the Middle Ages, the dilemma shifted somewhat;
the dualism was seen as a conflict between nature and grace.
Thomists and later scholastics (including post-Reformation
Protestants) divided the reasoning faculty of man into two com-
partments: natural reason was said to be sufficient for an under-
standing of natural events, while revelation was needed for a
soul-saving understanding of spiritual and supernatural
phenomena. Greek philosophical categories were still the foun-

2. Comelius Van Til, The Defense of the Faith (rev. ed.; Philadelphia: Presbyte~
rian and Reformed Publishing Co., 1963), pp. 126-27.
