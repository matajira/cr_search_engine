40 Marx’s Religion of Revolution

process, to this labour. The relation of the worker to work also
produces the relation of the capitalist (or whatever one likes to
call the lord of labour) to work. Private property is, therefore, the
product, the necessary result, of alienated labour, of the external
relation of the worker to nature and to himself. Private property is
thus derived from the analysis of the concept of alienated labour;
that is, alienated man, alienated labour, alienated life, and es-
tranged man.” In order to make his position absolutely clear,
he added: “We have, of course, derived the concept of alienated
labour (alienated life) from political economy, from an analysis of
the movement of private property, But the analysis of this concept
shows that although private property appears to be the basis and
cause of alienated labour, it is rather a consequence of the latter,
just as the gods are fundamentally not the cause but the product
of confusions of human reason. At a later stage, however, there
is a reciprocal influence.”*

Private property, in other words, was not the cause of man’s
alienation; originally, man’s alienation caused the establishment
of private property. Marx never again mentioned the original
cause of man’s alienated condition, so we must rely on this early
essay for our knowledge of his thoughts on the ultimate source
of man’s plight.

Once private property is seen as a result-of alienated produc-
tion, one of the central flaws in Marx’s system is revealed. If the
original cause is psychological rather than economic, then there
is no guarantee that the coming revolution will permanently
wipe out alienation merely because it destroys private property.
If the “fall into sin” of man is psychological, then how can the
“salvation” of man be assured by a social revolution? This whole
issue will be discussed in detail in relation to Marx’s linear
concept of history.

The Division of Labor
Under a system of alienated production, Marx argued, man

45, Bottomore translation, “Alienated [Estranged] Labour,” EPM, in Karl Marx,
Early Writings, p. 131. In the Straik edition, pp. 116-17, [Collected Works, 3, p. 279.]

46. Ibid. [Collected Works, 3, pp. 279-80.]
