Soviet Economic Planning 219

“The tolkach thrives in an economic soil watered by shortages and
fertilized by unrealistic targets.”**

Alec Nove’s summary of the “centralization-decentraliza-
tion” problem is to the point: “While centralized planning over-
burdens the organs charged with carrying it [the plan—G.N.]
out, decentralization — the obvious remedy — proves completely
unworkable so long as planners’ instructions are the principal
criterion for local decisions, The modest attempt to devolve
authority to territorial economic organs, in 1957, was inevitably
followed by renewed centralization, Within the system as it is,
only the center is in a position to know the needs of industry and
of society at large, since these are not transmitted by any eco-
nomic mechanism to any territorial authority. The latter is there-
fore unable to foresee the effects of its decisions on the economy
of other areas, and, in the circumstances, decentralized decision
making must lead to intolerable irrationalities. . . . Thus de-
centralization is both indispensable and impossible.”

Basic Practical Problems

In the previous section, we have looked at several important
problem areas of the Soviet economy, but the discussion has
been confined primarily to the theoretical problems of central
planning versus decentralized planning. With this broad perspec-
tive in mind, it is now relevant to examine some of the actual
practices of Soviet firms in their day-to-day activities, These are
perennial problems which are usually discussed by all critics of
Soviet economic institutions.

First, there is the question of the so-called “safety factor.”
Managers deliberately understate the productive capacity of
their plants in all reports to higher planning authorities. Their
motives are easy enough to understand; if their goals are set too
high by the central planners, then they will not be able to meet
the output goals. Consequently, they try to see to it that their

68. Jbid., p. 175.
69, Nove, “Prospects,” op. cit., p. 318.
