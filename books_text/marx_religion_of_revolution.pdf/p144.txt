70 Marx’s Religion of Revolution

Revolution

All roads in Marx’s works lead to the Revolution. Whether
we examine his theory of human alienation or his philosophy of
history, whether we look at his theory of knowledge or his eco-
nomic analysis, whether politics is the theme or the development
of science, all contradictions and difficulties are to be resolved
by the coming of the Revolution. It is the beginning of a truly
human history; it is the end of fettered production and exploited
labor. It is the discovery of human freedom combined with
absolute omnipotence and total control over nature. No more
absurdities like the division of labor will exist, no more. warfare,
no more conflict between men or within man. Without the ideal
of the Revolution as its goal, Marxism would be little more than
a huge mass of economic and historical material — interesting,
perhaps, but hardly the basis of a mass movement. When com-
bined with the idea of total revolution, it becomes a new religion,
or more accurately, a very ancient religion in new, pseudoscienti-
fic garb.

Was Marx primarily a scientist, or was he a religious prophet?
This debate has divided scholars for over haifa century, and it
is unlikely that it will be resolved in the near future. T. B.
Bottomore, a sociologist whose works are in the Marxist tradi-
tion, is one of those who think that Marx is best understood as
a scientist; naturally, the Soviet Marxists agree with this evalu-
ation.!!5 Robert C. Tucker, Louis J. Halle, Erich Fromm, Karl
Léwith, and many others see him as a semi-religious figure,
especially as an Old Testament prophet type. Léwith’s description
is typical: “He was Jew of Old Testament stature, though an
emancipated Jew of the nineteenth century who felt strongly
antireligious and even anti-Semitic, It is the old Jewish messian-
ism and prophetism ~- unaltered by two thousand years of eco-
nomic history from handicraft to large-scale industry — and
Jewish insistence on absolute righteousness which explain the
idealistic basis of Marx’s materialism. Though perverted into

115. Bottomore, “Karl Marx: Sociologist or Marxist?” Science and Society, XXX
(1964), pp. 11-24.
