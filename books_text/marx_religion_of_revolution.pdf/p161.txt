The Cosmology of Chaos 87

In contrast to this view stood ancient paganism. It was man
who would accomplish this shattering of the foundation. It was
man who, by ritual participation in the cosmic time of pre-
creation, would restore the Golden Age. It was man who would
abolish time and restore eternity to earth. To abolish time and
time’s curses: here was the goal of the pagan world. Eliade
writes: “The wish to abolish time can be seen even more clearly
in the ‘orgy’ which takes place, with varying degrees of violence,
during the New Year ceremonies. An orgy is also a regression
into the ‘dark’, a restoration of the primeval chaos, and as such
precedes all creation, every manifestation of ordered form. The
fusion of all forms into one single, vast, undifferentiated unity is
an exact reproduction of the ‘total’ mode of reality. I pointed out
earlier the function and meaning of the orgy, at once sexual and
agricultural; at the cosmological level, the ‘orgy’ represents chaos
or the ultimate disappearance of limits and, as time goes, the
inauguration of the Great Time, of the ‘eternal moment’, of
non-duration. The presence of the orgy among the ceremonials
marking the periodic divisions of time shows the will to abolish the
past totally by abolishing all creation.” S!

Escape from History: Cycles

This, it should be apparent, is the very essence of the Marx-
ian faith. The proletarian class “can only in a revolution succeed
in ridding itself ofall the muck of ages and become fitted to found
society anew.”'©2 What Marx wanted was an escape from his-
tory. The history of all hitherto existing societies has not been a
truly human history, since alienation has dominated them all.
As he wrote in the Preface to the Critique of Political Economy: “The
bourgeois relations of production are the last antagonistic form

161. Mircea Eliade, Pattsrus in Comparative Religion, p. 399; cf. 400-7 for a full
discussion of the subject, “The meaning of the carnivalesque orgy at the end of the
year is confirmed by the fact that the chaos is always followed by a new creation of
the cosmos. All these seasonal celebrations go on to a more or less clear symbolic
repetition of the creation.” Zbid., p. 400.

162, Marx and Engels, German Hdealogy, p. 86. [Selected Works, 1, p. 40. Collected
Works, 5, p. $2.]
