The Economics of Revolution 115

each of the exchanging parties values the other’s goods more
than he values his own. Far from some common element being
present, it is the essence of exchange that the exchanged goods
be unequal in the eyes of the potential traders. It does no good
to reply, as one economics professor scrawled on the first draft
of this manuscript, “you are talking psychology, not economics.”
Tt was the very essence of the post-classical revision that one
must provide a cogent explanation for economic affairs in terms
of human action and human decisions. Naturally the explanation
is “psychological,” since the foundation of economic reasoning
is centered on men’s decisions to act in the sphere of economics.
Any explanation which does not take account of psychological
causation in economic affairs is subject to the fallacy Marx was
always concerned with, the “fetishism of commodities,” i.e, as-
cribing to economic events a life of their own apart from the
human and social relations which make the events possible.
Marx’s explanation of the exchange phenomenon is a classic
case of “economic fetishism”: he looked at the commodities
instead of trying to explain the phenomenon in terms of the
economic actors. The idea of some metaphysical equality in the
exchanged items is wholly superfluous, and any conclusions
drawn from it can hardly fail to be irrelevant at best, and
probably wrong and highly misleading.’*

14. For an early exposition of the subjective nature of economic exchange and.
the necessary inequality involved in these psychological judgments, see Carl Menger,
Principles of Economies (Glencoe, Hlinois: Free Press, [1871] 1950), ch. 4. Gf, Eugen
von Béhm-Bawerk, The Positive Theory of Capital (4th ed.; South Holland, Mlinois:
Libertarian Press, [1921} 1959), Book III, pp. 121-256. [Libertarian Press is now
located in Spring Mills, Pennsylvania.] Marx hinted at the cruth involved in this
type of analysis in Capital, 1, p. 97 [], p. 85], but he nevertheless rejected Condil-
lac’s very similar discussion of exchange: 1, p. 177. [At this point, I am inserting a
later reference in place of the first edition’s recommended list of strictly aprioristic
economics books by Mises, Kirzner, and Rothbard. For a discussion of the episte-
mological problems found in modern economic thought, sce my essay} “Economics:
From Reason to Intuition,” in North {ed.), Foundations of Christian Scholarship: Essays
in the Van Til Perspective (Wallecito, California: Ross House, 1976). There is a basic
antinomy that manifests itself in the division of the “schools” of economic thought:
a posteriori reasoning (empirical, inductive, statistical) vs.a priori reasoning (logical,
deductive, “psychological”). The only valid reconciliation of this antinomy is
explicitly biblical-revelational. Biblical law provides the guidelines for men’s eco-
nomic theories and economic actions.]
