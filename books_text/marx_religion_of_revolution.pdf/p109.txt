The Cosmology of Chaos 35

ones, not dogmas, but real premises from which abstraction can
only be made in the imagination. . . . These premises can thus
be verified in a purely empirical way.”®? Socialism begins, how-
ever, with the presupposition that God’s existence is not a valid
philosophical issue; if He did exist then man and nature could
not exist, since they would owe their origin to God, and dy
definition man and nature are autonomous! Marx always began
with empirical premises only in the sense that he assumed, 4
priori, that all concrete, visible phenomena are self-sustaining,
self-creative, and totally autonomous.

Man Produces Man

The doctrine of creation is central to all philosophical sys-
terms, and Marxism is no exception. Marx’s whole perspective
was based upon the idea that Awman creative activity is the ultimate
foundation of all social existence. This is one of the central
themes in Marxism, and it can be found in the theoretical
volumes of Capital just as easily as it can be gleaned from his
earlier writings, although perhaps not in such a blatant fashion.
Production is the sphere of human, existence from which all other
temporal spheres are viewed; in this sense, it is the Marxist’s
intellectual “Archimedean point.” Again and again, Marx re-
turned to the production theme: “As individuals express their
life, so they are. What they are, therefore, coincides with their
production, both with what they produce and with how they
produce.” If this is true, then his materialistic conception of
history has its theoretical justification: “The nature of individu-
als thus depends upon the material conditions determining their
production.”3+ Human labor, in fact, actually defines mankind
as a species: “Indeed, labor, Jije-activity, productive life itself, ap-
pears in the first place merely as a means of satisfying a need — the

32, Marx and Engels, The German Ideology, p. 31, These manuscripts were not
published in the authors’ Hfetimes. [Selected Works, 1, pp. 19-20. Collected Works, 5,
p3l]

33, Ibid., p. 32. [Selected Works, 1, p. 20. Collected Works, 5, pp. 31-32.]

34, Ibid, [Selected Works, 1, p. 20, Collected Works, 5, p. 32]
