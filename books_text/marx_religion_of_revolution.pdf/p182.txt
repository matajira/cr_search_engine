108 Marx's Religion of Revolution

own God, and he has God’s very power of creation; man is totally
creative. Yet Marx’s blueprint for action was a call to total
destruction. Here, too, is the philosophy of the chaos cults of the
ancient world, Godless man has a passion for destruction, even
as the Bible declares: “. . . all they that hate me love death”
(Prov. 8:36b). This passion is not part of the biblical heritage,
and for this reason the attempt to link Marx to the Old Testa-
ment prophetic tradition is really erroneous. Though Marx's
system may, in certain instances, resemble in a superficial way
the biblical cosmology, on the whole it stands in open contrast
to orthodoxy. Halle writes that “Marxism met the city man’s
need for a new body of belief. It met the need for a religion of the
industrial age.”*2! To some extent, this is true enough, but it
misses the point; the cult of chaos has met the emotional needs
of apostate men for countless generations. Marx’s contribution
was his clothing of this revolutionary cult with language of
Germanic logic and contemporary secular science,

The Bible affirms a wholly divergent cosmology. Man is not
his own creator; he did not create himself “ex nihilo” — out of
nothing. Man is a creature who must operate under law, and he
lives in a universe which also operates under law. Because he is
under God’s law, man can stand over creation as God’s viceger-
ent. Marx, however, could not admit that man’s authority is
derivative; like the selfproclaimed autonomous men at the Tower
of Babel, he announced the creative power of man apart from
God: “And they said, Go to, Jet us build us a city and a tower,
whose top may reach unto heaven; and let-us make us a name
[i.e., define ourselves without reference to God], lest we be
scattered abroad upon the face of the whole earth” (Gen. 11:4).
“Ye shall be as gods,” the tempter promised (Gen. 3:5), and
Marx believed the promise. In affirming the powers of man for
total creation, he launched the forces of absolute destruction.

221. Halle, “Marx’s Religious Drama,” Encounter (Qct., 1965), p. 37. For a brief
but interesting discussion of the appeal of Marxism as an idealogy, see Mihaly
Csikszentmihaly, “Marx: A Socio- Psychological Evaluation,” Modem Age, X1 (1967),
pp. 272-82.
