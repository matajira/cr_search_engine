12 Marx’s Religion of Revolution

modeled after the standard catechisms of the churches of the
era, but Marx rejected the idea. The Communist League hoped
that it would be ready to influence the masses in the coming
revolution, which all the members expected to begin almost
momentarily. The revolution came too soon, however; the tract
did not appear until February of 1848, just as the uprisings were
beginning. The next month saw Marx’s expulsion from Belgium.

Marx and his family arrived in London, and he was not to
depart from the British Isles for any extended length. of time for
as long as he lived. It was in London that he did his research for
Das Kapital, laboring for long hours in the British Museum each
day. He held his only job in Britain as a correspondent and
analyst for Dana’s New York Daily Tribune. It was in England
that he and Engels organized, in 1864, the International Work-
ing Men’s Association (the First International). After his publi-
cation of The Civil War in France (1871), in which he defended the
Paris Commune of 1871, he became known as England’s “Red
Doctor,” yet he never manned a barricade or fired a rifle at
government troops; stodgy England never was able to muster
up a revolution for him.

For all of his vitriolic attacks on bourgeois institutions and
bourgeois morality, Marx was the antithesis of his ideal revolu-
tionary, at least in his private life. He remained legally married
to the same woman all his life, and the two were devoted to each
other. [This, despite the fact that Marx in 185] fathered an
illegitimate son by his wife’s lifetime maid, Helene Demuth.!°]
Though he was hardly a competent breadwinner, he apparently
was successful as a father, at least in the eyes of his children,"
Yet of the three daughters who survived death in childhood, two
(including Eleanor) committed suicide. But perhaps most
important of all was the fact that Karl Mars, the radical’s most

10. [The youth’s name was Fred Demuth, Robert Payne, Marx (New York:
Simon & Schuster, 1968), pp. 532-45, His name was pronounced “DEEmoth,” p.
537]

1}, See the note by Eleanor Marx Aveling on her father’s love for his family in
Erich Fromm, Marx's Concept of Man (New York: Ungar, 1961}, pp. 248-56.
