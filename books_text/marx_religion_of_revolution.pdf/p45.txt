Preface xlv

Marx was not yet a Marxist. Sidney Hook dates Marx’s first
appearance as a Marxist —an expositor of historical material-
ism—with The German Ideology (1845), an unpublished manu-
script co-authored with Engels. Part of this manuscript: actu-
ally appears in Hess’s handwriting.5*

Marx had read essays by Engels in 1843, which the latter
submitted to Marx as editor of two short-lived radical news-
papers, and which Marx published. (The second, co-edited by
Arnold Ruge and Marx, the Deutsch-Franzésische Jahrbiicher, pub-
lished in Paris, lasted only for one issue before being confiscated
at the borders by the Prussian authorities in February 1844.)5”
By 1844, Marx had also been converted to communism, though
not the “scientific” Marxist version, which began to take shape
only in 1845. It was in 1844 that the long collaboration between
Marx and Engels began. Hess had been the catalyst.

In the widely read, notoriously pro-Marx biography by Franz
Mchring, Hess’s influence is downplayed; Mehring even goes so
far as to write: “Both Marx and Engels co-operated with Hess
on numerous occasions during the Brussels period, and at one
time it appeared as though Hess had completely adopted their
ideas.”5* He makes it appear as though they were Hess’s teach-
ers, when in fact it had been the other way around, at least in
the early stages (1842-44). This Marxist rewriting of history is
understandable, since Marx and Engels concentrated their fire
on Hess’s ideas in the section on “True Socialism” in the Commu-
nist Manifesto (1848), despite the fact that Hess had adopted
many of their views on political economy.® This attack on a
former friend and teacher was typical of Marx and Engels from

55. Sidney Hook, Revolution, Reform, and Social Justice: Studies in the Theory and
Practice of Marxism (Oxford: Basil Blackwell, [1975] 1976), p. 58.

56. Hook, From Hegel to Mars, p. 186.

87. Franz Mebring, Karl Marx: The Story of His Life (Ann Arbor: University of
Michigan Press, [1933] 1962), pp, 58, 62.

58, Ibid, p. 112.

59. Hook, From Hegel io Marz, p. 186. For details of Engefs’s attack on Hess at
the October 23, 1847, meeting of the executive committee of Paris Communist
League's District Authority, see Hammen, The Red “fers, pp. 163-64.
