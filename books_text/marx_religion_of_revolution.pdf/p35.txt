Preface XXXV

to give orders and he tolerates no. opposition.” This is the
essence of the society of Satan: a system of bureaucratic control
that attempts to overcome the leader’s lack of omniscience and
omnipresence by means of top-down centralized power. It has
been the characteristic feature of Lenin, Stalin, Mao, and subse-
quent Communist dictators. It is inherent in the Communist
system.

Bakunin’s Warning

Michael Bakunin, the revolutionary anarchist and rival of
Marx in their battle for. control over the International Working-
men’s Association,?” accurately prophesied in 1869 what would
be the legacy of Marx’s theory of Communism:

The reasoning of Marx ends in absolute contradiction. Taking into
account only the economic question, he insists that only the most
advanced countries, those in which capitalist production has attained
greatest development, are the most capable of making social revolu-
tion. These civilized countries, to the exclusion of all others, are the
only ones destined to initiate and carry through this revolution. This
revolution will expropriate either by peaceful, gradual, or by violent
means, the present property owners and capitalists. To appropriate all
the landed property and capital, and to carry out its extensive eco-
nomic and political programs, the revolutionary State will have to be
very powerful and highly centralized. The State will administer and
direct the cultivation of the land, by means of its salaried officials
commanding armies of rural workers organized and disciplined for this
purpose. At the same time, on the ruins of the existing banks, it will
establish a single state bank which will finance all labor and national
commerce.

Tt is readily apparent how-such a seemingly simple plan of organi-
zation can excite the imagination of the workers, who are as eager for
justice as they are for freedom; and who foolishly imagine that the one
can exist without the other} as if, in order to conquer and consolidate
justice and equality, one could depend of the efforts of others, particu-

26. Cited by Raddatz, Karl Marx, p. 66.
27. Paul Thomas, Karl Marx and the Anarchists (London: Routledge and Kegan
Paul, 1980), pp. 249-340.
