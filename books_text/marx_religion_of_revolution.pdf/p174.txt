100 Marx’s Religion of Revolution

labor, ultimately abolishes political institutions. . . .”!%° If the
division of labor gave rise to classes, and if class power was
exercised by means of the state, then the abolition of the division
of labor would naturally eradicate the need both for classes and
the state. If a class can be said to exist, it is only the proletarian
class, and therefore there will be no need for an instrument of
class oppression, since there is no rival class to suppress. “When,
in the course of development, class distinctions have disappeared,
and all production has been concentrated in the hands of a vast
association of the whole nation, the public power -will lose its
political character. Political power, properly so-called, is merely
the organized power of one class for oppressing another. . . .
In place of the old bourgeois society, with its classes and class
antagonisms, we shall have an association, in which the free
development of each is the condition for the free development of
all, "199

Marx deliberately limited his definition of a class to include
little more than a group’s economic function in the production
system. He limited the idea of the state to that of an arm of
oppression of a ruling class. Thus, he could assert that “a vast
association of the whole nation” would replace the state. The
unity of the new society will abolish the division of labor and
therefore it will abolish the dichotomy between individual inter-
ests and the general, communal interests. No state is needed in
this schema. :

Engels may have been bothered by the idea of “the associa-
tion,” although he used it on occasion. In his dialogue against
the anarchists, “On Authority” (1873), he denied that it is
possible to have an organization without a division between
leadership and subordination. “Yes,” he paraphrased their re-
ply, “but here it is not a case of authority which we confer on
our delegates, but of a commission entrusted!” And he replied, quite
appropriately: “These gentlemen think that when they have

198, German Idealogy, p. 416. [Collected Works, 5, p.380.]
199, Manifesto of the Communist Party, Selected Works, 1, p. 127. [Collected Works, 6,
p. 506.]
