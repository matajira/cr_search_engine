BIBLIOGRAPHY

An English-language edition of the complete works of Marx
and Engels is being published by Lawrence & Wishart of Lon-
don (1975-). Nothing like it existed when I wrote this book. This
publishing venture has no date of completion that I know about.
It will take many years. Reprinted here is my 1968 bibliography,
unchanged.

Original Writings by Marx and Engels
{alphabetical by book title)

Basic Writings on Politics and Philosophy, edited by Lewis S. Feuer.
Garden City, New York: Doubleday Anchor, 1959. This col-
lection stresses the. political side of Marx, in addition to his
philosophy of history, It deemphasizes the economic writings,
and it takes little notice of the early manuscripts dealing with
human alienation,

Capital: A Gritique of Political Economy. New York: Modern Li-
brary, n.d. This is a reprint of the Charles H. Kerr edition of
volume I of Capital, 1906. In this volume, Marx presented his
basic critique of the capitalist economic system.

Capital: A Critique of Political Economy. Chicago: Charles H. Kerr
& Co., 1909. The second volume of Capital, entitled The Process
of Circulation of Capital. The least important of the three vol-
umes, assembled after Marx’s death by Engels,

Capital: A Critique of Political Economy. Chicago: Charles H. Kerr
& Co., 1909. Volume three of the study, The Process of Capitalist
Production as a Whole was also assembled by Engels. It attempts

237
