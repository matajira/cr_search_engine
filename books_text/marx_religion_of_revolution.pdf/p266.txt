192 Marx's Religion of Revolution

The Bureaucratic Impulse

The problem of planning has been sketched by Mises in his
study, Bureaucracy (1945). There are two forms of management
or administration. The first form is the type associated with
private capitalistic production. It is the decentralized type, since
its primary requirement is that each level of the organization
must produce a profit. It is left to the local managers to make
the decisions which will produce that profit. This does not mean
that no general rules are set down by the highest level; this level
is the area of the greatest entrepreneurial power. But so long as
the managers produce profits, they can be left to themselves
without too much danger. There is therefore considerable flexi-
bility for local decision-making. The second form of management
is the state bureaucracy. The situation here is totally different.
The state’s bureaucracy is limited by fixed appropriations. Flexi-
bility at the lower levels must be curtailed if the total structure
is to stay within its fixed budget. The bureaucratic planners
must see to it that each dollar that is allocated for a specific
purpose does, in fact, reach its destination. If the state’s pro-
grams are to be brought to fruition, then there cannot be very
much latitude in what is permitted to the bureaucracy’s subsidi-
aries. The nature of the type of control is determined, in other
words, by the source of the bureaucracy’s operating funds, Pri-
vate businesses are not faced with fixed appropriations; they can
suffer losses or make profits, but they do maintain a far higher
degree of local flexibility. The state’s bureaucracy is not subject
to the whims of the free market, since it does not operate on the
basis of profit and loss. Hence, its decisions must be fixed in
advance as much as possible and its activities must be executed
according to the preconceived plan. Uncertainty is reduced, but
so is the freedom of movement.” The two managements are very
different, and their rules of conduct are not interchangeable,
This is why each must be restricted to its proper realm.*

35. Mises, Bureaucracy (New Rochelle, New York: Arlington House, [1945] 1969).
[Reprinted by Libertarian Press, Spring Mills, Pennsylvania]

36. In so far as socialistic planning becomes the basis for producing a nation’s
goods, local industries will resemble less and less the sketch of private bureaucracies
