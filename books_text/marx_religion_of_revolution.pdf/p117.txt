The Cosmology of Chaos 43

production (since man, in the Christian framework, is more than
a machine), it does demand that the division of labor be accepted
as a positive social benefit.5!

Marx, however, was utterly hostile to the Christian idea of
the calling: “In a communist society there are no painters but
at most people who engage in painting among other activities.”5*
The division of labor is personified for Marx in the distinction
between mental and physical labor: “Division of labor only
becomes truly such from the moment when a division of material
and mental labor appears.”*4 Since private property and human
alienation are reciprocal, we find that the division of labor and
private property are also reciprocal: “Division of labor and pri-
vate property are, moreover, identical expressions. . . .”=+ Hu-
man alienation, private property, and the division of labor are
all aspects of the same detestable condition of man in capitalist
society.

 

Dialectics: Man Into Commodity

Man, under capitalism, has become a mere commodity: “The
demand for men necessarily governs the production of men, as of every other
commodity. . . . The worker has become a commodity, and it is
a bit of luck for him if he can find a buyer.”°° Marx saw what a
totally secularized, urbanized, and “rationalized” capitalist pro-
ductive system was doing to those workers who labored under it,
Instead of blaming the rootless secularism which was dehuman-

51, One of the most profound descriptions of the division of labor is found in St.
Paul’s first epistle to the Corinthians, chapter 12. In it, he describes the distribution
of spiritual gifts to the Christian church in terms of a body: there are hands and
feet, eyes and ears, and each has its special function. [Cf Gary North, The Dominion
Covenant: Genesis (rev. ed.; Tyler, Texas: Institute for Christian Economics, 1987),
chaps. 8, 10.]

52. Marx and Engels, The German Ideology, p. 432. [Collected Works, 5, p. 394.]

53, bid., p. 43. [Collected Works, 5, pp. 44-45.]

54. Ibid., p. 44. [Collected Works, 5, p. 46.]

55. “Meaning of Human Requirements,” EPM, p. 159. [Collected Works, 3, p.
317]

56. “Wages of Labor,” EPM, p. 65. [Collected Warks, 3, p. 235.]
