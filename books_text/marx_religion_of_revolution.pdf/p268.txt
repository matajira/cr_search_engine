194 Marx’s Religion of Revolution

forgotten is that the method which under given conditions is the
cheapest is a thing which has to be discovered, and to be discovered
anew, sometimes almost from day to day, by the entrepreneur, and
that, in spite of the strong inducement, it is by no means regularly the
established entrepreneur, the man in charge of the existing plant, who
will discover what is the best method. The force which in a competitive
society brings about the reduction of price to the lowest cost at which
the quantity salable at that cost can be produced is the opportunity for
anybody who knows a cheaper method to come in at his own risk and
to attract customers by underbidding the other producers. But, if
prices are fixed by the authority, this method is excluded. Any im-
provement, any adjustment, of the technique of production to changed
conditions will be dependent on somebody’s capacity of convincing the
S.E.C. [Supreme Economic Council - G.N.] that the commodity in
question can he produced cheaper and that therefore the price ought
to be lowered. Since the man with the new idea will have no possibility
of establishing himself by under-cutting, the new idea cannot be proved
by experiment until he has convinced the S.E.C. that his way of
producing the thing is cheaper. Or, in other words, every calculation
by an outsider who believes that he can do better will have to be
examined and approved by the authority, which in this connection
will have to take over all the functions of the entrepreneur.°?

The results of such a system can be safely predicted in
advance. It will create a nation of frightened bureaucrats who
fear all change because it forces them to make deci-
sions — decisions which may result in more than financial losses
if the supreme political authorities decided to make an example
of the bureaucrat’s error of judgment. It was Lange, against
whose theory of economic planning Hayek is arguing, who had
to admit that “the real danger of socialism is that of a bureaucratization
of economic life,” and on this point Hayek was in full agreement.
Hayek’s conclusion seems inescapable:

38. F. A. Hayek, “The Competitive ‘Solution’,” Economica, VII, New Series
(1940); reprinted in Hayek, Individualism and Economic Order (University of Chicago
Press, 1948), pp. 196-97,

39. Lange, On the Economic Theory of Socialism, p. 109.
