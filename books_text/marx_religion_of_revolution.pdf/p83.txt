The Biography of a Revolutionary 9

time was spent in long philosophical and political discussions in
the coffee houses and other meeting places of the little group. In
spite of his glowing letters home to his father which were filled
with details describing his supposed academic progress — the
quantities of work which he claimed to have accomplished are
positively staggering — he seemed to advance very slowly, if at
all, toward his law degree.® Even his associates in the group
implored him to hurry on his doctoral dissertation. Finally, in
1841, he submitted his dissertation to another university, the
University of Jena, and he was awarded his degree in philosophy
{not in law) in the same year. [The dissertation was titled,
“Difference Between the Democritean and Epicurean Philosophy
of Nature,” an appropriately narrow topic for a dissertation. It
is even less interesting than its title indicates. The reader should
note how far removed its subject matter was from anything
Marx wrote subsequently. He later paid to have it printed as a
book. The English-language. version is 72 pages long. It has
never played an important role in Marxism or anything else, but
the final sentence of the foreword to the printed version is impor-
tant, for it reveals Marx’s hatred of both God and authority:
“Prometheus is the most eminent saint and martyr in the philo-
sophical calendar.” Immediately preceding this, he had quoted
in Greek from Aeschylus’s Prometheus Bound: “Be sure of this, I
would not change my state of evil fortune for your servitude,
Better to be the servant of this rock than to be faithful boy to
Father Zeus.”7]

In 1842, he began to labor at the only type of paying job he
would ever hold, that of a journalist. He began to write for the
Rheinische Zeitung, one of the liberal papers of the day. It was a
small paper, but at least it offered the possibility of rapid ad-

6. An example of these letters is reproduced in Otto Rithle, Karl Marx: His Lift
and Work (New York: New Home Library, 1943), 15-24. Many of his biographers
take these letters seriously, but his father remained somewhat skeptical. For a more
reasonable account of what probably took place in these university days, see
Leopold Schwarteschild, Kart Marx: The Red Prussian (New York: Universal Li-
brary, 1947), ch. 3, (This letter is reprinted in Marx and Engels, Collected Works, 3
(New York: International Publishers, 1975), pp. 10-21,]

7. Collected Works, 1, p. 31n,
