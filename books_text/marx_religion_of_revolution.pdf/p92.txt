2
THE COSMOLOGY OF CHAOS

Both for the production on a mass scale of this communist conscious-
ness, and for the success of the cause itself, the alteration of men on
a mass scale is necessary, an alteration which can only take place in
@ practical movement, a revolution; this revolution is necessary,
therefore, not only because the ruling class cannot be overthrown in
any other way, but also because the class overthrowing it can only
in a revolution succeed in ridding itself of all the muck of ages and
become fitted to found society anew.

Marx and Engels (1845-46)!

It is always a difficult task to deal with an individual like
Karl Marx, for a number of reasons. Not the least of these
problems is the fact that Marx was a synthesis figure: he was the
inheritor of the revolutionary Jacobin tradition of the French
Revolution; he was of major importance in his development of
some of the ideas of classical political economy; he was one of
the founders of economic history, sociology, and social science
in general; and he was the most famous of the radical left-wing
followers of Hegel. Above all, he was the co-founder (along with
Frederick Engels) of “scientific socialism” or Communism, a

1, Karl Marx and Frederick Engels, The German Ideology (London: Lawrence
& Wishart, [1845-46] 1965), section on Feuerbach, p. 86. [Selected Works, 3 vols.
(Moscow: Progress Publishers, 1969), 4, p. 41, Collected Works (New York: Interna-
tional Publishers, 1976), 5, pp. 52-53. Reminder: the first Arabic numeral after the
title or publication date indicates the volume number.]

i8
