56 Marx’s Religion of Revolution

systematic truth only if truth actually exists. Otherwise, man has
no grounds for saying that giant strides in knowledge are, in fact,
being made. As with Hegel, the idea of eternal truth is merely a
presupposed limiting concept, an intellectual backdrop, for Marx
and Engels. As Engels wrote: “If mankind ever reached the stage
at which it could only work with eternal truths, with conclusions
of thought which possess sovereign validity and an unconditional
claim to truth, it would then have reached the point where the
infinity of the intellectual world both in its actuality and in its
potentiality had been exhausted, and this would mean that the
famous miracle of the infinite series which has been counted
would have been performed,”®

He was able to ward off the charge of total relativism only
be appealing to the concept of the relative exactness of the natural
sciences, Some of their truths are eternal, he claimed, thus effec-
tively negating what he had just written.® Even here he was
forced to admit that “eternal truths are in an even worse plight
in the third, the historical group of sciences.” The conclusion
was inescapable: “. . . knowledge is here essentially relative,
inasmuch as it is limited to the perception of relationships and
consequences of certain social and state forms which exist only
at a particular epoch and among particular people and are of
their very nature transitory. Anyone therefore who sets out on
this field to hunt down final and ultimate truths, truths which
are pure and absolutely immutable, will bring home but little,
apart from platitudes and commonplaces of the sorriest kind.”*!

Dialectics: Rationalism vs. Irrationalism

Van Til has argued that total rationalism must always have
total irrationalism as a corollary. If man, on the presuppositions
of rationalistic, autonomous human thought, can claim to know
anything, he must claim to know everything, If all facts are

88. Ibid., p. 100. [Collected Works, 25, p. 81.]
89, Hid, p. 101. [Collected Works, 25, p. 81,]
90. bid., p. 102. [Collected Works, 25, p. 82.]

Q1. Itid.,-p. 103. [Gollected Works, 25, p. 88]

 
