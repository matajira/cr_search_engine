xviii Marx’s Religion of Revolution

healing the class warfare that is the direct outcome of the eco-
nomic antitheses of society in history. Out of the systematic
production and ownership relations of capitalism has come bour-
geois civilization, the mode of production’s “superstructure.”
Out of capitalist production methods and institutions also has
come the industrial proletariat, a social class which will inevita-
bly rise up and destroy the many internal contradictions of
bourgeois civilization by ending it. Therefore, out of industrial
order comes social chaos, and out of that chaos comes the next
(and final) phase of civilization, communism. Marx wrote-in
1850: “Revolutions are the locomotives of history.”®> Somehow the
violent individual social revolutions must end when the proletar-
ian class imposes Communism’s system of permanent revolution, the
permanent transvaluation of values. “Their ery must be: The
Revolution in Permanence.”2”

Marx held to the orderly Newtonian worldview of physical-
natural cause and effect, but he resurrected and baptized an
ancient tradition of social chaos. His worldview was a strange
mixture of Western linear history (Augustine), Western utopian-
ism (communism), scientific rationalism (Newton), eighteenth-
century classical economics (the labor theory of value and the
cost of production theory of value), atheism (dialectical material-
ism), and pagan cyclical history (the chaos festivals). This is why
there are so many competing interpretations of “the meaning of
Marxism.” I argue throughout this book that it is the last
element — the pagan chaos religion—that has been most ne-
glected by scholars and disciples, yet this is the fundamental
doctrine of Marxism. Marx was above all an atheist, and this
atheism culminated in a war against God and all traces of God
in Western civilization. He offered a cosmology of social chaos

26. Marx, “The Class Struggles in France, 1848 to 1850” (1850), Collected Works
(New York: International Publishers, 1978), vol. 10, p. 122, Also reprinted in Marx,
and Engels, Selected Works, 3 vols. (Moscow: Progress Publishers, 1969), vol. 1, p.
277.

27. Marx and Engels, “Address of the Central Authority to the [Communist]
League” (1850), Collected Works, vol. 10, p. 287; Selected Works, vol. 1, p. 185.
