10 . Marx’s Religion of Revolution

vance for a young man who held a doctor’s degree. He began in
April; by October, the young Marx was editor. Unfortunately,
the young man displayed a trait which was to mark him through-
out his career: an unwillingness to compromise in the face of
overwhelming odds. In March of 1843, the Prussian government
ordered it suppressed after April 1, Marx resigned as editor on
March 17.

Interestingly enough, at this stage of his career, he was
actually opposed to communism as an economic and philosophi-
cal system. But within a year he and another young German
intellectual, Frederick Engels, were converted to a crude sort. of
communism, The catalyst in this metamorphosis was Moses
Hess, the “communist rabbi,” as Marx often called him. Marx
later went far beyond Hess in his devotion to the revolutionary
cause, and he constructed a far more thorough critique of capi-
talist society, but the role played by Hess at this early stage of
Marx’s development cannot be over-estimated.®

Another opportunity to enter into the world of journalism
presented itself shortly thereafter. Marx took his young bride,
Jenny von Westphalen, to Paris, where he and his old “Young
Hegelian” associate, Arnold Ruge, set out to edit Deuisch-
Franzbsischen Jahrbiicher [German-French Yearbooks]. The first edi-
tion was published in February of 1844; it was to be the last, as
well. The two men quarreled, and the breach was never healed.
Many of the copies were confiscated by the Prussian government
when issues were sent into Prussia. In the Yearbooks two of
Marx’s important early essays appeared: the “Introduction to a
Critique of the Hegelian Philosophy of Law,” and his reply to

8. Hess had originally encouraged the publishers of the Rhcinische Zeitung to
hire the young Marx. Hess is described by Isaiah Berlin as a communist “mission-
ary,” later a Marxist and a Zionist, who spent most of his efforts in gaining
adherents to the new faith. See Isaiah Berlin, Karl Mara: His Life and Environment
(8rd ed; New York: Oxford University Press, 1963), pp. 72-73. For a more detailed
analysis of Hess’s thought, see Sidney Hook, From Hegel to Marx (Ann Arbor:
University of Michigan Press, [1956] 1962), ch. 6, {See also Shlomo Avineri, Moses
Hess: Propket of Gorarunism and Zionism (New York: New York University Press,
1985). Avineri is the author of an influential book on Marx, The Social and Political
Thought of Kart Marx (New York: Cambridge University Press; 1968).]
