The Cosmology af Chaos 41

discovers that his very life forces are being robbed from him. The
source of his immediate difficulty is the existence of the division
of labor, The division of labor is the essence of all that is wrong
with man’s present condition; for Mars, it is contrary to man’s
existence as a fully creative being. It pits man against his fellow
man; it creates class divisions; it destroys the unity of the species,
That Marx should oppose the division of labor with such vehe-
mence is not too surprising. Mankind, in the Marxian perspec-
tive, is god; theologically; one cannot permit the Gedhead to be
divided, In his treatment of French Revolutionary thought, Rush-
doony elaborates on this question: “Tenth, humanity is the true
god of the Enlightenment and of French Revolutionary thought.
In all religious faiths one of the inevitable requirements of logical
thought asserts itself in a demand for the unity of the Godhead.
Hence, since humanity is god, there can be no division in this
godhead, humanity. Mankind must therefore be forced to unite.
Since Enlightenment philosophy was monistic, this means an
intolerance of differences as unessential. National and racial
differences, instead of being God-given and possessing richness
and dignity to be respected, are to be obliterated. The goal is
not communion but uniformity.”47

Following an analysis remarkably similar to Rousseau’s,
Marx argued that the division of labor gave rise to social classes;
therefore, to eliminate these economic classes — themselves an
outward manifestation of man’s alienated condition — mankind
must abolish the. division of labor.® Anything that leads to
divisions in mankind’s unity must be eliminated, by definition.

One of Marx's diatribes against the division of labor is found
in volume 1 of Capital: “Some crippling of body and mind is
inseparable even from division of labour in society as a whole.

 

47, R. J. Rushdoony, This Independent Republic (Nutley, New Jersey: Craig Press,
1964), p. 142. [Reprinted by Thoburn Press, Fairfax, Virginia, in 1978.)

48, Jean Jacques Rousseau, Discourse on the Origin of Inequatily, in G. D. H. Cole
(ed.), The Social Contract and Ditcourses (London: Dent, 1966), esp. pp. 195-208. Cf.
Robert A, Nisbet, “Rousseau and Totalitarianism,” fournal of Politics, V (1943), pp.
93-114. [Reprinted in Nisbet, Tradition and Revolt; Historical and Sociological Essays
(New York: Random House, 1968), ch. 1.]

 
