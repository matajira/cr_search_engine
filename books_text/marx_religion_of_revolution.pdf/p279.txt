Soviet Economic Planning 205

tarily wish to grow?”!! It is a question which the leaders of many
“underdeveloped” countries ought to ask themselves."

Another extremely important factor in the growth of Soviet
industrial production was the ability of the Soviet planners to
borrow from Western technology. This, as Gerschenkron points
out, is the advantage which all underdeveloped nations possess,
but the USSR has made use of it to an exceptional degree. After
World War II, this massive theft of Western production methods
slowed, but it is still going on in many cases.!* Bergson concurs,
arguing that Soviet borrowing has been on an “unprecedented
scale.”!* It is Gerschenkron’s belief that technology became a
retarding factor after World War IT, since the USSR had begun
to catch up with Western Europe’s basic technological methods,
leaving less room for Soviet borrowing. He is not followed in
this by all scholars, however, since other factors are now present,
such as an educational system which concentrates on turning
out technologists, engineers, and theoretical scientists.'® The

11. Murray N. Rothbard, Man, Economy and State, 2 vols. (Princeton, New Jersey:
Van Nostrand, 1962), 2, p. 837. [Reprinted by New York University Press, 1979.]

12. For a discussion of the whole question of economic growth, see the essay by
Colin Clark, originally published in The Intercollegiate Review, now distributed by the
National Association of Manufacturers, “Growthmanship”: Fact and Fallacy (1965).
P. T. Bauer, Economic Analysis and Policy in Underdeceloped Countries (Durham, North
Carolina: Duke University Press, 1957); Bauer and Basil S. Yamey, The Economies
of Underdeveloped Countries (University of Chicago Press, 1957). [P. T. Bauer, Dissent
on Development: Studies and Debates in Development Economics (Cambridge, Massachu-
setts: Harvard University Press, 1972); Equality, the Third World and Economic Delusion
(Cambridge, Massachusetts: Harvard University Press, 1981); Reality and Rhetoric:
Studies in the Economics of Development (Cambridge, Massachusetts: Harvard Univer-
sity Press, 1984).]

13. Alexander Gerschenkron, Economic Backwardness in Historical Perspective (Cam-
bridge, Massachusetts: Harvard-Relknap Press, 1961), p. 293.

14. Abram Bergson, The Real National Income of Soviet Russia Since 1928 (Cam-
bridge, Massachusetts: Harvard University Press, 1961), p. 293.

15. Gerschenkron, Beonomic Backwardness, p. 262.

16. Jan S. Prybyla, “Soviet Economic Growth: Perspectives and Prospects,”
Quarterly Review of Business and Feonamics, IV (1964); reprinted in Morris Bornstein
and Daniel R, Fusfeld (eds.), The Soviet Economy: A Book of Readings (Homewood,
Mlinois: Irwin, 1966), pp. 308-9.
