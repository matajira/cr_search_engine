The Cosmology of Chaos 61

different and conflicting possible decisions, shows by this pre-
cisely that it is not free, that it is controlled by the very object it
should itself control. Freedom therefore consists in the control
over ourselves and over external nature which is founded on
knowledge of natural necessity; it is therefore necessarily a prod-
uct of historical development.”9?

This, however, raises another issue, the question of elites. If
men generic, species mankind —are.to take control of cir-
cumstances, then we must, as Engels put it, gain “control over
ourselves.” C. S. Lewis, in one of the most important novels of
the century, That Hideous Strength, has one of the characters in the
book raise precisely this question: “Man has got to take charge
of Man. That means, remember, that some men have got to take
charge of the rest - which is another reason for cashing in on it
as soon as one can. You and I want to be the people who do the
taking charge, not the ones who are taken charge of.”®? Marx did
not care to deal with this problem, but modern day analysts of
the Soviet and satellite nations have been forced to reckon with
it; the “classless” society has created a new class of political
elites.

Dialectics: Controller or Controlled?

The most fundamenta! problem, nevertheless, is philosophi-
cal: if man is to take control of his universe, and if chance is to
be defeated, how can man do this if he is determined by that
very chance universe? This was the dilemma Feuerbach had not

97. Engels, Anti-Dithring, p. 128. [Collected Works, 25, p, 106.1

98, G. S. Lewis, That Hideous Strength (New York: Macmillan, 1946), p. 42. This
book is a theological 7994. It is available in paperback as the third book in the
trilogy of Perelandra and Out of the Silent Planet.

99. Milovan Djilas, in The New Glass (New York: Praeger, 1957), blames Stalin
rather than the socialist economic system as such for the tyranny of Communism.
He cannot offer a solution for the problem of bureaucratic elites, except for an
appeal (o a vague “democratic socialism.” Hayek’s Road to Serfitom (University of
Chicago Press, 1944) has disposed of that particular hope. The socialist planning
board will inevitably become a part of a “new class.” T. B, Bottomore, a Marxist
sociologist, has also called attention to the issue of clites in “Industry, Work, and
Socialism,” in Fromm (ed.), Socialist Humanism, p. 397,
