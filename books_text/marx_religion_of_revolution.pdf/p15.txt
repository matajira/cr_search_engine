Foreword x

damental presuppositions that cannot be questioned without
making human thought impossible. There are pre-theoretical
assumptions that support and legitimize all subsequent reason-
ing within every philosophical system.'® There are always certain
philosophical questions that are “off limits” in any system. For
example, Marx’s number-one “off limits” question was this cru-
cial one: What was the origin of man? He wrote: “Who begot the
first man, and nature as a whole? I can only answer you: Your
question is itself a product of abstraction. , . . Now I say to
you: Give up your abstraction and you will also give up your
question.”!9 In other words, “Don’t ask! Obey me!”

The Presence of the Irrational

The defenders of absolute scientific determinism can usually
be pressured intellectually into admitting the presence of mys-
tery—the “unknown” — within the realm of supposedly un-
breakable physical cause and effect. There is always some trace
of the random event, some physically undetermined and indeter-
minate event, in physical processes. For example, the unpredict-
able appearance of a physically uncaused event can produce perma-
nent alterations in aggregate physical systems. This makes no
sense, of course, but it is nonetheless basic to modern quantum
mechanics, and therefore to all modern physical theory.°

In some eras, popular discussions of “the way the world
works” have tended to emphasize the “scientific,” meaning the
popular science of the textbooks, where the physically uncaused
and the irrational are politely left unmentioned, The nineteenth
century is a good example of such an era. But this suppression
of the irrational! cannot last forever. The received truths regard-
ing the inherent rationality of the universe subsequently come

18. Herman Dooyeweerd, In the Tioilight of Western Thought: Studies in the Pretended
Autonomy of Philosophical Thought (Philadelphia: Presbyterian & Reformed, 1960).

19, Kasl Marx, “Private Property and Communism,” Economic and Philosophie
Manuscripts of 1844, in. Collected Works (New York: International Publishers, 1975),
vol. 3, p. 305.

20. Nick Herbert, Quantum Reality: Beyond the Naw Plosics (Garden City, New
York: Anchor Press/Roubleday, 1985). Cf. Gleick, Chaas,
