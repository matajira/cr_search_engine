206 Marx's Religion of Revolution

real threat to Soviet growth is not technological, but institu-
tional; they must learn how to implement their technology effi-
ciently before they will be able to match Western standards of
consumption and production,

Campbell’s comments on the whole issue of borrowed tech-
nology in the Soviet command economy are enlightening: “But
the interesting thing is that, despite this advantage in being able
to borrow technology, Soviet progress in productivity does not
seem to have been exceptional. The rate of increase in resource
productivity does not seem to differ much from that achieved in
other countries. This obviously implies that exceptional growth
should be attributed more to the ability of Soviet command
planning to mobilize resources — i.c., to accumulate capital, to
educate on a mass scale, to move people from low productivity
occupations such as agriculture to high productivity ones such
as industry, and to force increases in participation rates — than
to any special ability to use resources efficiently and increase
their productivity.”!?

Coercion, even more than borrowed technology, is the key
to Soviet economic growth. It should never be forgotten, as
Campbell points out, “It was in the name of industrialization
that the totalitarian terror machine was perfected.”!8 The extent
of this coercion is reflected in the standard of living of the Soviet
people during the four decade era of rapid industrialization.

Standard of Living

In 1921, the Soviet government was forced to revert to some
measure of private ownership in the areas of agriculture and
small scale industry in order to regain the economic losses of the
War and the “war communism” period of 1917-1921. The extent
of those losses was staggering; production in 1921 had fallen to
some 20 percent of 1914 output!!9 For seven years, the economy
experienced a surprisingly large rate. of growth, so that by 1928,

17. Campbell, Soviet Economic Power, pp. 128-29,
18. bid., p. 26.
19. dbid., p. 14.
