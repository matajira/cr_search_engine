The. Economies of Revolution 157

Modern economic theory sees profit as the result of entrepre-
neurial planning. This theory has been expounded forcefully by
Frank H. Knight in his monumental Risk, Uncertainty and Profit
(1921).""® It offers the argument that pure profit stems from the
ability of some entrepreneurs to forecast the state of the market
more accurately than their competitors. They are thus able to
reap an excess of income over capital, wage, and interest expen-
ditures. Profit, in other words, stems from the fact of uncertainty.
Without this entrepreneurial function — the task of guessing ac-
curately about the future and planning-accordingly — there could
be no profits under capitalism’s free market.'!’ The manager’s
task is merely to carry out the decisions made by the entrepre-
neurs, While the function of management is in part entrepre-
neurial {just as the entrepreneur’s function is in part manage-
rial), the managers do not perform the basic task in a profit-
seeking establishment.

Under this theory, the entrepreneur’s success is directly de-
pendent upon his ability to predict the future and plan for it,
The least successful at the job will be forced out of business
through the endless competition of the open market. In this view,
all societies need entrepreneurs; so long as men are not omnis-
cient concerning the future needs and wants of the population,
the entrepreneurial function must be performed by someone,
The free market, with its incentives of profit and loss, has been
the most successful in meeting the desires of the public at the
least possible expenditure. So far, it has proven to be the most
efficient means of stimulating men to bear the risks of economic
forecasting and planning.

Marx Ignored the Entrepreneur

Marx wrote, in regard to profit, that “the rate of profit of the
individual capital is determined, not by the market price of a

116, Frank H. Knight, Rist, Uncertainty and Profit (New York: Harper Torch-
books, [1921] 1964}, esp. chaps. 8-10.
117. CE Ludwig von Mises, Human Action, pp. 286-307,
