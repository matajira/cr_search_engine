76 Marx’s Religion of Revolution

law that lies like a dead weight on customs, and to recreate the
state of absolute spontaneity,” 52

Secret Societies

In a secular form, this tradition of ritual chaos and secrecy
has been preserved in the secret societies which are common to
all cultures. This is one of the reasons why they have served so
often as sources of revolutionary and conspiratorial activity,
especially in the history of Europe.'? One of the most blatant
statements of the philosophy of chaos was made recently by Jeff
Nutall, editor of the underground My Own Mag, and the princi-
pal mouthpiece for William Burroughs (author of Naked Lunch,
an underground favorite): “Still, Burroughs, all of us~ we're
decaying men, for God’s sake. We're all decaying, clearly. Play-
ing around with drugs, playing around with every possible sex-
ual deviation. Really, previous ages of decadence would look
upon the modern avant garde with amazement and admiration.
We've outdone the lot of them. But the curious, impressive thing
is that so many artists are able to go through these things as
intelligent men —not as totally unprincipled men. If you go
through these things to some purpose, it can be even noble. It’s
as if, with your own rot, you refucl and invigorate — you fertilize
this very scorched earth for those yet to come,”'*

132, bid., p. 46, Cf. Eliade, Patterns in Comparative Religion (New York: Sheed
and Ward, 1958), pp. 398, 402.

133. While they are out of favor in today’s historical circles, Nesta Webster's
studies contain solid information dealing with the activities and the development
of Europe's secret societies. Some of the connections that she establishes between
historical events and certain conspiratorial groups are probably questionable on
methodological grounds, but much of what she has to say is very important.
Historical scholarship always has difficulty in treating secret. societies, since they
leave few written documents and many of the public data are deliberately mislead-
ing. Three of her most important works are Secret Societies and Subversive Movements
(Hawthome, California: Omni Reprint, [1923] 1964); World Revolution (Waco,
Texas: Owen Reprint, [1921] 1964); The French Revolution (London: Constable,
1919). [Vastly superior is James LL. Billington’s masterpiece, Fire in the Minds of
Men: Origins of the Revolutionary Faith (New York: Basic Books, 1980).]

134, Barry Farrell, “The Other Culture,” Li (Feb. 17, 1967), p. 99,
