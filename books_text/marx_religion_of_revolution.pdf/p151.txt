The Cosmology of Chaos 7

Throughout history and in all societies, we find men who
swear allegiance to this demonic philosophy, whatever form it
may take, Marx was prefigured by such persons as Mazdak, the
revolutionary communist in 5th century (A.D.) Persia, a man
who very nearly succeeded in over-throwing the society in which
he lived.'95 In the Assassins of 11th-century Arabia (the name
is derived from the same root as hashish, which was an integral
part of Assassin ritual), a sect of revolutionary Moslems, the
same basic perspective was present, and it was imported into the
West by Bogomils and Cathars, the dualist sects of the medieval
world.'9* The whole list of rationalist secret societies in 18th-
century Europe ~ Illuminati, Rosicrucians, Grand Orient Ma-
sonry — all contributed to the same revolutionary tradition, though
in the name of enlightened and liberated humanity.

The culminating point in this tradition was the French Revo-
lution. It was here that secular humanism, revolutionary fervor,
and the secret societies fused into one overwhelming move-
ment.!87 Marx was the inheritor of this tradition, especially of
the plot of Babeuf to overthrow the government in 1795. Marx
acknowledged his respect for Babeuf's efforts.!"8 In fact, Marx’s
diagram for conspiratorial, revolutionary action which he set

135, Cf, article on “Mazdak” in James Hastings (ed.), Encyclopedia of Religion and
Ethics (New York: Scribners, 1915), pp. 508-10.

136. Steven Runciman’s The Medieval Manichee (New York:
gives an account of some of these sects,

187, The role played by the secret societies is not some wild thesis in the mind
of Mrs. Webster; liberals have profoundly acknowledged the part which these
societies took. Cf. Una Birch, Secret Societies and the French Revolution (London: John
Lane, 1911), See also-Charles William Heckethorne, The Secret Societies of All Ages
{2 vol.; New Hyde Park, New York: University Books, [1897] 1965) for a sympa-
thetic treatment of these movements,

138. Marx and Engels, The Communist Manifesto, in Selected Works, 1, p. 198.
[Collected Works, 6, p. 514.] Cf. David Thomson, The Babeuf Plot (London: Paut,
1947); Atbert Fried and Ronald Sanders (eds.), Socialist Thought (Garden City, New
York: Doubleday Anchor, 1964), pp. 43-71; Edmund Wilson, 7 the Finland Station
(Garden City, New York: Doubleday Anchor, 1953), pp. 69-73. For a defense of
Babeuf by one of his contemporaries, sec Filipo M. Buonarroti, Babexf’s Conspiracy
fr Equality (London: Hetherington, 1836). Finally, see J. L. Talmon, The Origins of
‘Totalitarian Democracy (New York: Praeger, 1960), pp. 167-255.

 

king Press, 1961)
