34 Marx’s Religion of Revolution

tory; neither Marx nor the consistent Christian thinker can do
this. One cannot challenge one’s god, and man is Marx’s god:
“Who begot the first man, and nature as a whole? I can only
answer you: Your question is itself'a product of abstraction.”

In the passage following this last’ section, Marx set forth
some incredibly obscure arguments which were-to show that the
whole issue of human origin is illegitimate. Then he made this
point: “Since, however, for socialist man, the whole of what is called
world history is nothing but the creation of man by human labour,
and the emergence of nature for man, he, therefore, has the
evident and irrefutable proof of his selfcreation, of his own ori-
gins. . . . [T]he quest for an alien being, a being above man and
nature (a quest which is an avowal of the unreality of man and
nature) becomes impossible in practice. Atheism, as a denial of
this unreality, is no longer meaningful, for atheism is a negation
of God and seeks to assert by this negation the existence of man.
Socialism no longer requires such a roundabout method; it be-
gins from the theoretical and practical sense perception of man and
nature as essential beings.”

Socialist man does not even need to assert his own being by
denying God; he just ignores God from the start. One stands on
one’s own two feet; how the feet got there or how the foundation
upon. which the feet are resting got there, one never bothers to
ask. In fact, one should not ask it; the question is a product of
abstraction. One who has been forced to read the fantastically
abstract discussions found throughout Marx’s writings can only
wonder why, at this particular point, Marx shied away from
abstract thinking.

Karl Marx always prided himself on remaining on “neutral”
ground philosophically. He always asserted that he was rigor-
ously empirical and scientific. As he wrote in The German Ideology
(1845-46): “The premises from which we begin are not arbitrary

30. Zbid., p. 145. {Collected Works, 3, p. 305.]
31, Here I am using the Bottomore translation of EPM, in Marx, Early Writings,

pp. 166-67. The passage appears in the Struik edition, p. 145. [Collected Works, 3,
Pp. 305-6.
