Socialist Economie Calculation 197

of the Soviet Union’s research program in cybernetics. Unless
there is a radical reform in planning methods in the USSR in the
near future, Glushkov estimates that the planning bureaucracy
will have to grow 36-fold by 1980, requiring the services of the
entire population!*5 If the central planners of the Soviet Union
persist in the idea that every nail and screw of all factories under
construction or already built must be known to them in advance,
then there is no hope for them. There has to be a reform, Leon
Smolinski reports on the discussion of the problem made by
Glushkov: “This attempt is utopian. As V. M. Glushkov has
recently shown, it implies that the central planners would have
to consider several quintiilion relationships among the various
products, probably the largest integer ever considered in eco-
nomic analysis. Glushkov adds that even if high-speed electronic
computers performing 30,000 operations a second were har-
nessed to that task, it would require one million computers
working without interruption for several years. And, of course,
the economy. would not remain frozen, waiting for the computa-.
tions to be completed.”#

The other problem is hardly mentioned: what guarantee
would the planners have that the data supplied by the various
data-gathering centers are, in fact, accurate? Total central plan-
ning, in short, is a silly dream of deluded thinkers. It cannot be
done.

Conclusion
In the final analysis, the problem comes down to this: men

45. Reported by Leon Smolinski, “What Next in Soviet Planning?” Foreign
Affaits, XLIT (1964); reprinted in Morris Bornstein and Daniel R. Fusfeld (eds.),
The Soviet Economy: A Book of Readings (Homewood, Ilinois: Irwin, 1966), p. $29.

46. Ibid, p. 335, [In retrospect, the idea of a computer that processes data at
Only 30,000 operations per second is laughable. But the speed of the computers is
not the main problem. The problem is the inability of men, in the absence of a
competitive pricing system based on private ownership, to quantify all the aspects
of potential consumer demand that go into making a centrally planned production
decision. In any case, the Soviet Union is a society that cannot allow large numbers
of computers to go into the hands of the public. Even if computers were available,
where would Soviet citizens gain access to reliable data?]
