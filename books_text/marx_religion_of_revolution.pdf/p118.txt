44 Marx’s Religion of Revolution

izing the culture of the 19th century, he blamed capitalism’s
division of labor.

This is precisely what one might expect; Marx, like so many
secular philosophers, divinized one aspect of the social order:
production. Every aspect of man’s life was viewed by Marx from
this one perspective. “As individuals express their life, so they
are,” he wrote.>” Man therefore expresses his very being in his
own productive activity. Under capitalism, however, the prod-
ucts of his labor do not belong to him, but to another man, the
capitalist. This is the source of man’s alienation. He concluded,
“as the result, therefore, of the fact that more and more of his
products are being taken away from the worker, that to an
increasing extent his own labor confronts him as another man’s
property. . . .”58 Estranged labor turns the products of his own
hands into alien, hostile creations that stand in opposition to
him; it tears him from his very species life, It finally results in
an estrangement between man and man.°9

While the emphasis on the separation between men did not
occur very often in Marx’s subsequent writings, the. idea that
man’s labor actually confronted him as an alien force appeared
in the later volumes of Capital A laborer is like a complex
machine which is slowly wearing out; the profits of the capitalist
stem ftom the exploitation (not a very neutral term for a professing
“scientist”) of his employees’ life forces. Under alienated produc-
tion, a peculiar phenomenon occurs, according to Marx: the
more material wealth society produces, the more inhuman it
becomes, and the less of one’s own life can a person possess. “The

57. German Ideology, p. 32. [Selected Works, 1, p. 20. Collected Works, 5, p. 31.]

58. “Wages of Labor,” EPM, p. 67, [Collecied Works, 3, p. 237]

59. “Estranged Labor,” EPM, p. 114. [Collected Works, 3, p. 277.]

60. Capital, 1, pp. 339, 384, 3968, 462, 625; Capital, 3 (Charles H. Kerr Co.
1909), pp. 102, 310, 848, Moscow’s Foreign Languages Publishing House has
produced an inexpensive three-volume set of Capital, and this edition has been
issued by International Publishers in New York City, I amt using the older Kerr
edition throughout, however, since most Hbraries have this set if they have any.
Also, it is the set generally referred to in most-pre-1965 scholarly works on Marx
by Americans. [International Publishers edition: Capital, 1, pp. 310, 349-50, 360-61,
423, 570-71; Capital, 3, pp. 85, 264, 814-15.]
