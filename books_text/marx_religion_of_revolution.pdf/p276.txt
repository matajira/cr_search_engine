202 Marx’s Religion of Revolution

Economic Growth

‘There can be no question of the fact that Soviet industrial
production has increased vastly over the past five decades. How-
ever questionable their official statistics happen to be (especially
the summary figures), by all possible measuring devices the
Soviets have been able to attain high rates of growth in the areas
of heavy industry and military armaments. Does this fact call
Mises’ basic theory into question? Does the Soviet experience
refute the argument that socialist economic planning is inconsis-
tent with the rational allocation of scarce resources?

The answer is difficult to ascertain. Mises was dealing with
a problem of pure economic theory, and he had in mind a purely
socialist economy, The Soviet Union has never attained such a
stage of purity in its economic relations. The presence of such
institutions as small privately owned agricultural units testifies
to elements of “latent capitalism” in the USSR, and the use of
money to facilitate economic exchanges is also a deviation from
pure socialism ~or at least pure Marxian socialism, To the
extent that the Soviets use a system of centralized economic
planning, the answer is no, their experience does not refite
Mises. The waste, inefficiency, and general misallocation of scarce
resources under the Soviet system are legendary. In fact, the very
reluctance (or inability) of the Soviet leaders to allow total
collectivization would indicate at least a partial realization on
their part of the basic argument made by Mises: an absolutely
socialistic, completely centralized, moneyless economy is an in-
tellectual abstraction incapable of being put into practice.

Nevertheless, the fact of Russia’s stupendous economic growth,
or at least the growth of the statistical indices of industrial
output, is still something which demands an explanation. Esti-
mates vary widely among Western scholars, but Abram Bergson’s
figures are at least somewhat representative. He guesses that the
gross national product of the Soviet Union has grown at a rate
of some 4.5 percent per annum, and 5.2 percent if we exclude the
war years, from 1928 to 1960 inclusive.? Given the basic weak-

2. Abram Bergson, The Economics of Soviet Planning (New Haven, Connecticut:
Yale University Press, 1964), p. 316,
