xviil The Beret of Revelation

discover who the beast is or was. If he has not yet appeared, then
the last days mu st also be ahead of us, unless we have actually
entered into them. If he has already appeared, then the last days
are over.

This book identifies the prophesied beast beyond any reason-
able doubt. This much I will tell you now: it is not Henry Kissin-
ger.

If all of the potential buyers of The Beast of Revelation were to
discover in advance that it is not filled with prophecies about
brain-implanted computer chips, tatoos with identification num-
bers, cobra helicopters, nuclear war, and New Age conspiracies,
most of them would not buy it. Customers of most Christian
bookstores too often prefer to be excited by the misinformation
provided by a string of paperback false prophecies than to be
comforted by the knowledge that the so-called Great Tribulation
is long behind u 3, and that it was Israel’s tribulation, not the
church’s. (For biblical proof, see David Chilton’s book, The Great
Tribulation.) 12 They want thrills and chills, not accurate Bible
exposition; they want a string of “secret insights,” not historical
knowledge. Like legions of imaginative children sitting in front of
the family radio back in the 1930’s and 1940’s who faithfully
bought their Oval tine, tore off the wrapper, and sent it in to receive
an official “Little Orphan Annie secret decoder,” fundamentalist
Christians are repeatedly lured by the tempting promise that they
can be “the first ones on their block” to be “on the inside” — to
be the early recipients of the “inside dope.” And that is just exactly
what they have be en sold, decade after decade.

Nine-year-old children were not totally deceived in 1938, They
knew the difference between real life and make-believe. Make-
-believe was thrilling; it was fun; it was inexpensive but it was not
real, The decoded make-believe secrets turned out to provide only
fleeting excitement, but at least they could drink the Ovaltine.
Furthermore, children eventually grow up, grow tired of Ovaltine,
and stop ordering secret decoders.

12, David Chilton, 7% Great Tridulction (Ft, Worth, Texas: Dominion Press, 1987).
