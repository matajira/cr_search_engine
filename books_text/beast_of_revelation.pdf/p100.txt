66 The Beast of Revelation

than a year performing as a musician and an actor in the Grecian
festivals. The response of the Greeks is given by Arthur Weigall,
as he comments upon the history of Rome written by Dio Cassius:
“Soon Nero was actually deified by the Greeks as ‘Zeus, Our
Liberator.’ On the altar of Zeus in the chief temple of the city they
inscribed the words ‘to Zeus, our Liberator’ namely Nero, for ever
and ever; in the tzmple of Apollo they set up his statue; and they
called him ‘The new Sun, illuminating the Hellenes,’ and ‘the one
and only lover of the Greeks of all time.“w

When Nero returned to Rome from Greece in A.D. 68, he
returned to the t ‘iumphant praise of the city as he entered the
Palace and Apollo’s Temple on the Palatine. Dio Cassius records
the scene thus: “The city was all decked with garlands, was ablaze
with lights and reeking with incense, and the whole population,
the senators themselves most of all, kept shouting in chorus: ‘Hail,
Olympian Victor! Hail, Pythian Victor! Augustus! Augustus! Hail
to Nero, our Hercules! Hail to Nero, our Apollo! The only Victor
of the Grand Tour, the only one from the beginning of time!
Augustus! August us! O, Divine Voice! Blessed are they that hear
thee.’”

During the Roman Civil Wars, begun with the death of Nero
in June A.D. 68, the emperor Vitellius even offered sacrifices to the
spirit of the deceased Nero. To better secure his own emperorship,
Emperor Vespasian, who overthrew Vitellius, had to make the
effort to check this Nero cult.

Conclusion

The appearance of emperor worship in Revelation is evidence
that we are on the right track in specifying the Roman empire as

40, Arthur Weigall, Nero: Emperor of Rome (London: Thornton Butterworth, 1933),
p. 276,

41, Dio Cassius, Rorsan History 62:20:5.

42, Ibid. 654,
