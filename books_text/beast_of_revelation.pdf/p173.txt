The Historical Evidence (1 141

have agreed, although later he changed his opinion.!? Because of
its assumed early date and apostolic connection, The Shepherd
tended to be used as if it were an inspired book. Interestingly, it
is found in one of the earliest, complete Greek manuscripts of the
Bible, the Codex Sinaiticus. This seems to demonstrate The Shep-
herd's early and widespread respect as high authority, and even
as Scripture by some.

Moreover, there are those who argue for a date prior to A.D.
85. Arthur S. Barnes and John A. T. Robinson argue most vigor-
ously for this time-frame.'’ And the evidences they suggest are
quite reasonable. Let us summarize them:

First, since the book is deemed at least quasi-scriptural by
Trenaeus, Clement of Alexandria, Origen, (the early) Tertullian,
Eusebius, and Jerome, we should expect a very early date. For a
work to be deemed inspired it likely would had to have been
written by an associate of the apostles, and probably very early,
perhaps pre-A.D. 80.

Second, Irenaeus lived in Rome for awhile and just 20 years
after Pius’s death. It is highly unlikely that he would have viewed
The Shepherd as “Scripture” if written in his own era and location.

Third, after initially accepting it as scriptural, Tertullian (A.D.
160-220) later discredited the book. It seems likely he would have
mentioned its recent authorship in his arguments against it had it
been written in the era A.D. 140-150. But he does not.

Fourth, the Muratorian Canon’s view (A.D. 170-200) cannot
be right, for several reasons. (1) It identifies Hermas as the brother
of bishop Pius of Rome. But as a foster child sold into slavery in
Rome (Vision 1:1: 1), it is remarkable that Henna-s rower mentions his
alleged brother Pius, bishop of Rome. (2) Nowhere in The Shepherd ig
there any indication that there exists a monarchical episcopate.
Hermas speaks, instead, of “the elders that preside over the

“72, Tertullian, Orations 16.

13, Arthur 8, Barnes, Ciristianity at Rome in the Apostolic Age (Westport, CT
Greenwood, [1938] 197 1), pp. 212ff; and John A. T. Robinson, Redeting the Naw
Testament Philadelphia Weatminater Preas, 1976), pp. 819-820.
