8 The Beast of Revelation

reference to a rebuilding of the temple and the book was written
about A.D. 95, how could the readers make sense of its prophecies?
John definitely speaks of the temple as still standing.

It is a matter of indisputable historical record — confirmed in
both archaeology znd ancient literature — that the temple in Jerusa-
Jem was destroyed in August, A-D. 70, by the Roman general
Titus. And yet in Revelation 11:1-2 we read of the temple standing
while John wrote. John looks to its filure destruction. Hence, John
must have written prior to A.D. 70,

We need to realize how obvious it is that this temple is Herod’s
temple. The cone usion is so strong that this passage has played
prominently in the various liberal critical theories of Scripture.
These (erroneous) liberal approaches view Revelation as a hodge-
podge collection of older and newer traditions from both Jewish
and Christian sources. These traditions, it is alleged, were strung
together, rather disjointedly, by one or more editors. For instance,
James Moffatt viewed this particular section of Revelation as a
pre-A.D. 70 Jewish fragment, and claimed that this is “widely
recognised by cri ics and editors.”® Thus, the particular “tradi-
tion” preserved in Revelation 11:1-2, he alleges, had to have been
written by some Jewish zealot during the Jewish War against
Rome before the temple was destroyed.

Obviously the presence of this temple in Revelation llis a
remarkable fact, ven if the liberals handle it wrongly. It is an
indicator of the ¢ arly date for Revelation, not for a patch-work
view of Revelation’s composition.

Objections
There are sewral objections that have been raised against the

position outlined above. Let us briefly mention and respond to
these.

6, James Moffatt, The Revelation of St. John the Divine, yol, 5 in W. Robertson
Nicoll, od., The Expositor’s Greek Testament (Grand Rapids: Eerdmans, rep. 1980), pp.
207ff,, 414, ete,
