Objections to the Early Date 169

cate James Moffatt wrote: “The blasphemous title of dim, as-
sumed by the emperors since Octavian (Augustus = sebastos) as a
semi-sacred title, implied superhuman claims which shocked the
pious feelings of Jews and Christians alike. So did theos [god] and
theou faios [son of god] which, as the inscriptions prove, were freely
applied to the emperors, from Augustus onwards.”4

The appearance of emperor worship in Revelation is held by
many late-date theorists as the strongest evidence for a date during
the last year of the reign of Domitian (A.D. 81-96). It is true that
Domitian required people to address him as “Lord and God.”
Certainly the emperor cult was prominent in his reign. Yet when
the historical evidence is scrutinized, there is abundant testimony
to emperor worship at various stages of development well before
both Domitian and Nero. Indeed, there are such clear statements
of so many aspects of the emperor cult that it is surprising that
this argument is used against the early date. That it is deemed “the
principal reason” (Morris) that makes it “almost impossible”
(Moffatt) for the early date view to stand is wholly incredible.

Persecution in Revelation

Second, Morris discovers “indications that Revelation was
written in a time of persecution.” This evidence is felt to accord
“much better with Dornitian.”"5 W. G. Kimmel is quite confident
that “the picture of the time which the Apocalypse sketches coin-
cides with no epoch of the primitive history so well as with the
period of Domitian’s persecution.”* Morris, Kimmel, and a num-
ber of other scholars list this as among their leading arguments for
the A.D. 95-96 date.

Again, in effect, I have already spoken to this matter in
Chapter 5. I agree that it seems clear enough that in Revelation

A Tid, p. 429.

5, Moms, Revelation, p. 36.

6. W.G. Kummel, fntradiction othe New Testament, trans. Howard Clark Kee,
17th ed. (Nashville Abingdon, 1973), p. 828.
