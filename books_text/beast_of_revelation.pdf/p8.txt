x The Beast of Revelation

(Luke 21:20-24), yet it took place long ago. It took place after the
New Testament writings were finished but long before you or I
appeared on the scene.

The fact is, t1e vast majority of prophecies in the New Testa-
ment refer to this crucial event, the event which publicly identified
the transition fro m the Old Covenant to the New Covenant, and
which also marked the triumph of rabbinic Judaism over priestly
Judaism, Pharisee over Sadducee, and the synagogue system over
the temple. So c2ntral was the destruction of the temple to the
future of both Christianity and Judaism that Jesus linked it sym-
bolically to His death and resurrection:

Then answered the Jews and said unto him, What sign shewest
thou unto us, seeing that thou doest these things? Jesus answered
and said unto them, Destroy this temple, and in three days I will
raise it up. Ther said the Jews, Forty and six years was this temple
in building, and wilt thou rear it up in three days? But he spake of
the temple of his body (John 2: 18-21),

Dating The Book of Revelation

“But,” you may be thinking to yourself, “John wrote the Book
of Revelation (th2 Apocalypse) in A-D. 96. Everyone agrees on

1, The Sadducee s:ct of Judaism disappeared, since it had been associated with
the priests who offcia ed at the temple, Herbert Danby, whose English translation
of the Mishnah is still considered authoritative by the scholarly world, both Jew and
gentile, commented on the undisputed triumph of Pharisaism after the fall of Jerusa-
Jem (which lives on aa Orthodox Judaism), “Until the destruction of the Second
Temple in A.D, 70 they had counted as one only among the schools of thought which
played a part in Jewist national and religious life; after the Destruction they took the
position, naturally anc almost immediately, of sole and undisputed leaders of such
Jewish life «s survived. Judaism aa it has continued since is, if not their creation, at
least a faith and a reli gious institution largely of their fashioning; and the Mishnah
is the authoritative record of their labour. it comes about that while Judaism
and Christianity alike venerate the Old Testament as canonical Scripture, the Mishnah
marks the Passage to J adaism ag definitely as the New Testament marks the passage
to Christianity.” Herb :t Danby, “Introduction,” Te Misimat (New York: Oxford
University Press, [1933] 1987), p, xiii. The Mishnah is the written version of the
Jews oral tradition, while the rabbis’ comments on it are called Gemara. The Talmud
contains both Mishna h and Gemara. See also R, Travers Herford, The Pharives
(London: George Allen & Unwin, 1924),
