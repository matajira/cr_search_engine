Publisher’s Preface © 1989 by Gary North
Copyright ©1989 by Kenneth L. Gentry, Jr, Th.D.
Second printing with corrections, 1994,

All rights reserved, No part of this publication maybe reproduced,
stored in a retrieval system, or transmitted in any form or by any
means, except for brief quotations in critical reviews or articles,
without the prior, written permission of the publisher. For infor-
mation, address D sminion Press, Publishers, Post Office Box 8204,
Fort Worth, Texas 76124,

Unless otherwise noted Scripture quotations are from the New
American Standard Bible, ‘The Lockman Foundation 1960, 1962,
1963, 1968, 1971, 1972, 1973.

Published by the Institute for Christian Economics
Distributed by Dominion Press, Fort Worth, Texas
‘Typesetting by Nhung Pham Nguyen

Printed in the Un’ ted States of America

ISBN 0-930464-2 |-4

Library of Congress Catalog Card Number: 93-134579
