General Index

133,

Oath, 59,60,

Octavia (wie of Nero), 17,42,

Octavia, The, 42, 158,

Octavian (Se Augustus),

Offerings, 123.

Oil (burning oil), 144-145, 165.

Old Covenant (Sx also: Covenant), x,
xiy, xy, xvi, 120,

Old Testament, ix, xxvi, xxx, 5, 26, 88,
89,112, 113,

Olivet Discourse, 99, 116, 115n.

Olympian, 60,66,

Omen, 15, 16.

One hundred, forty-four thousand,
98,188-134,

Ongen, 140, 141, 149, 156-157, 158, 166,
187.

Orosius, Paulus, 49n,

Orthodox{y) (See ais: Conservative
Evangelical), 88, 188, 172n, 188.

Otho (emperor), 72n, 75,108,

Ovid, 13.

Palace, 16,39,66.

Palestine, 83,94, 100, 168,
rabbis regarding, 94.

Papias, 142-149, 147, 187.

John and, 142,
Polycarp and, 142,

Parthia(ns), 66,72, 126,

Patmos, 47, 81, 144, 146, 147, 166, 161,
168, 165.

Paul, the Apostle, xix, xxiv, xxxvi, 19,
45,48,49,81, 125, 182, 148, 161, 177,
178, 179, 180,
favorable attitude toward Rome, 16,

48-49,
martyrdom, 52, 53, 68, 125, 126, 148,
144, 165, 170.
Pella, 100, 121.

205

Pergecution ofChristianity (Ss abe: Chris-
tianity - crimes of}, 44,85, 169-171.
Domitianic, 62, 64-55, 126, 144, 160,
164, 165, 170, 171.

Jewiah, 49,182,185.

late-date argument and, 169-171,

Neronic, 9, 13, 18, 43, 44, 46, 47-66,
68, 82, 85, 124, 195, 126, 144, 160,
164, 170-171, 176, 180, 181, 183,
185.

Neronic: impact, 48-60,62-63.

Neronic: severity, 50-62,66.

Neronic: temporal length, 48, 52, 63-
55.

Perseverance, 47,68,81.

Pessimism, 188, 184,

Peter, 52,58,91, 125, 126, 144,165, 170.

Pharisees, x, 95.

Philadelphia,

Philosophy, xiii, xix, 10,60, 109.

Philostratus, 42, 158,

Phoenix, 125,

Pierced (See: Jesus Christ — pierced).

Pilate, 91, 109,

Plague(a), 40n, 181, 177.

Pliny the Elder, 18,42,48, 122, 158,

Pliny the Younter, 42.

Politics, 10, 17, 18, 39,49-44,53,63, 64,
1024, 187, 178, 180, 186.

Polycarp, 40n, 150, 178-178,

Pompeii, 81.

Pope, 45.

Poppaea, 17,6.

Premillennialiat) (Se: Millennial views).

Priest.
high, 128.

Jewish, 109,120.
pagan, 58,69,60.

Properties, 18.

Prophecy, ix, x, xii, xvi, xvii, xxi, xxili,
xxiv, 9, 4, 6, 22, 25, 24,44, 70, 71, 72,
78, 114, 118, 120, 121, 148, 145, 172,

 
