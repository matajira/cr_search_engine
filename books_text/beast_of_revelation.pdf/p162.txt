130 The Beast of Revelation

second stage, towerd the end of Christ's ministry, the Great Com-
mission (Matt. 2(:28ff.; Acts 1:8) commanded a worldwide out-
reach to all nations. This was, however, only dimly understood
by the early origir al (Jewish) Christians. The difficulty they faced
in accepting the full implications of the Great Commission is
witnessed in Acts 10, 11, and 15. Even in this early post-
commission Christianity, the ministry continued to gravitate to-
ward the Jews.

Furthermore, the earliest Christians even engaged in Jewish
worship observances; bile focusing on and radiating their minis-
try from Jerusalem.’ Not only so but they frequented the temple,*
attended the synagogues,'and designated themselves as the true
heirs of Judaism.’One modern writer discusses the matter of
Jewish Christians worshiping as Jews and as Christians: “[Jesus’]
disciples, however, were faithful at first in their observance of both,
as Acts unobtrusively recounts ..., so that their special teach-
ing and customs cffered no occasion for them not to be considered.
Jews. Indeed, they had not separated themselves publicly nearly
as much as had the Essenes. Only after A.D. 70 did the require-
ments for membership in Judaism become more stringent.””

We can expect, then, that the earlier the date of a Christian
book, the more Jewish it might appear. The question then arises
as to whether or not Revelation has a strongly Jewish air to it. If
it does, this may |e supportive evidence for the pre-A.D. 70 date,
as opposed to an A.D. 95 date.

The Jewish Character of Christianity in Revelation
As a matter cf fact, in Revelation there is overwhelming evi-

2, Acts 2:1 21:262411.

 
  

3, Acta 2-5,
4, Acts 2:46; 3:19; 6; 2621.
5. Acts 13:5, 145 14: 84,7, 19,2@ 198; 221924113 2611,

6. Gal, 327-29; 6:16; Phil. 33

7. Leonhard Gopp:lt, Apostolic and Post-A postelic Times, trang, Robert A, Guelich
(London Adam and Charlea Black, 1970), p. 26.
