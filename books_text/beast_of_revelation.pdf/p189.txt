The Historical Evidence (2) 137

things in the Apocalypse. He seems also to have seen the Apoca-
lypse . . . in the island.”

Needless to say, early date advocates find the use of Origen
questionable, in that it is not at all clear that he had in mind
Domitian as “the King of the Remans.” Indeed, many late-date
advocates even admit that this “leading evidence” is based on
presumption! R.H. Charles, for instance, writes: “Neither in Clem-
ent nor Origen is Domitian’s name given, but it may be presumed
that it was in the mind of these writers.”7 H. B. Swete and
Mounce agree.'®

Thus we come again upon a widely acclaimed late-date wit-
ness which is wholly unconvincing. Additional arguments against
the reading of Origen as a late-date witness may be garnered from
the following material on Clement of Alexandria. This is due to
the fact that Clement was not only the precursor and teacher of
Origen, but is equally nondescript.

Clement of Alexandria

As we continue our survey of the evidence from tradition, we
come to Clement of Alexandria (A.D. 150-215). Clement was a
learned scholar of much prominence in early Christianity. The
evidence from Clement almost universally is cited by late-date
advocates as supportive of their view. Clement’s statement is found
in his Who is the Rich Man that shall be Saved?, Section 42: “Hear a
story that is no mere story, but a true account ofJohn the apostle
that has been handed down and preserved in memory. When after
the death of the tyrant he removed from the island of Patmos to
Ephesus, he used to journey by request to the neighboring districts
of the Gentiles, in some places to appoint bishops, in others to
regulate whole churches, in others to set among the clergy some
one man, it may be, of those indicated by the Spirit.” The critical
phrase here is “after the death of the tyrant he [John] removed

17. Charles, Revelation, D. xciii, Emphasis added.
18, Swete, Raelatiom, p. xcix, n, 2; Mounce, Rewlation, p, 32.
