The Identity of the Beast 9

hesitated.

As he considered his dire circumstances and the approach of
certain death, he is recorded to have lamented: “What an artist
the world is losing!”3"Finally, when he learned that the Senate
had voted to put him to death by cruel and shameful means, he
secured the assistance of his secretary Epaphroditus to run a sword
through his throat. His suicide occurred at the age of81 on June
9, A.D. 68. With his death the line of Julius Caesar was cut off,
and for the first time an emperor of Rome was appointed from
outside Rome.

Conclusion

The view to be presented in this work is that the Emperor
Nero Caesar is the Beast of Revelation specifically considered and
that Rome is the Beast generically considered. As has been shown
in our quick survey of his life, Nero was a horrible character in
Rome’s history. Church historian Philip Schaff speaks of him as “a
demon in human shape.”3' As will be shown in the pages to follow,
he was the very one whom John had in mind when he wrote of the
Beast whose number is 666.

The view I have presented and will be defending is contrary
to what the vast majority of Christians believe today. Almost
certainly you have been taught a radically different view at some
point in your Christian journey. You may even been tempted to
scoff at its very suggestion at this point. Nevertheless, I challenge
you to bear with me as we wade through the evidence on this
matter in Revelation. I am convinced that you will find the evi-
dence quite persuasive.

As we begin our interpretive journey through this issue, may
we bear in mind the exhortation of Paul who wrote: “Let God be
found true, though every man be found a liar” (Rem. 3:4). May

35, Suetonius, New 49,
36, Suetonius, Nero 49,
87. Schall, History 1:379.
