xxxvi The Beast of Revelation

The Same Argument the Liberals Use

By interpreting Jesus’ promise that He would soon return in
power and judgment against Israel as if it were a promise of His
second coming a t the rapture, dispensationalists are caught in a
dilemma. They teach that Paul and the apostles taught the early
church, in Dave Hunt’s words, to “watch and wait for His immi-
nent return,” yet Jesus has delayed returning physically for over
1,950 years. How can we escape the conclusion that the apostles
misinformed the early church, a clearly heretical notion, and an
argument that iberal theologians have used against Bible-
believing Christians repeatedly in this century? But there is no way
out of this intellectual dilemma if you do not distinguish between
Christ’s coming : n judgment against Israel in A.D. 70 and His
physical return in final judgment at the end of time.

Contrary to Dave Hunt, with respect to the physical return of
Jesus in judgment, the early church was told just the opposite: do
not stand around watching and waiting. “And while they looked
steadfastly toward heaven as he went up, behold, two men stood
by them in white apparel; Which also said, Ye men of Galilee, why
stand ye gazing up into heaven? this same Jesus, which is taken
up from you into heaven, shall so come in like manner as ye have
seen him go into reaven. Then returned they unto Jerusalem from
the mount called Olivet, which is from Jerusalem a sabbath day’s
journey” (Acts 1::0-12).

Those who prefer figuratively to stand around looking into the
sky are then temrted to conclude, as Dave Hunt concludes, that
the church today, by abandoning pre-tribulational dispensational-
ism — as if more ‘han a comparative handful of Christians in the
church’s history had ever believed in the pre-tribulational rapture
doctrine, invented as recently as 1830 — has “succumbed once
again to the unbiblical hope that, by exerting godly influence upon

31. Hunt, Whatever Happened to Heaven?, Dp, 55.
32, Dave McPherson, 7h Great Rapture Hoax (Fletcher, North Carolina: New
Puritan Library, 1983),
