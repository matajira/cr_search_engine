The Importance of the Date of Revelation 83
tion or theological bias.

Early Date Advocates

Holding to an early date for Revelation does not, however,
prove one is in defiance of “historical facts.” This should be evident
in the list of names of those who have held to an early date. An
appeal to venerated scholarship cannot settle the issue, to be sure.
But the very fact that a good number of astute biblical scholars
hold to a minority position should at least forestall too hasty a
dismissal of that position.

We herewith list a number of noted scholars who have dis-
counted the late-date for Revelation in favor of an earlier date.
Some of the following are noted liberal scholars, some orthodox.
The historical facts of the matter are not necessarily determined
by a particular school of thought. In fact, that some of the scholars
are liberals is quite remarkable in that the liberal view usually
tends to push the dates of biblical books to a later, not an earlier,
period,

We list these names in alphabetical, rather than chronological,
order: Jay E, Adams, Luis de Alcasar, Karl August Auberlen,
Greg L. Bahnsen, Arthur S, Barnes, James Vernon Bartlet, F. C.
Baur, Albert A. Bell, Jr., Willibald Beyshclag, Charles Bigg, Fried-
rich Bleek, Heinrich Bohmer, Wilhelm Bousset, F. F. Bruce, Rudolf
Bultmann, W. Boyd Carpenter, David Chilton, Adarn Clarke,
William Newton Clarke, Henry Cowles, W. Gary Crampton, Berry
Stewart Crebs, Samuel Davidson, Edmund De Pressense, P. S.
Desprez, W. M. L. De Wette, Friedrich Dusterdieck, K. A. Eck-
hardt, Alfred Edersheirn, George Edmundson, Johann Gottfried
Eichhorn, G. H. A. Ewald, F. W. Farrar, Grenville O. Field,
George P. Fisher, J. A. Fitzmeyer, J. Massyngberde Ford, Her-
mann Gebhardt, James Glasgow, R. M. Grant, James Comper
Gray, Samuel G. Green, Heinrich Ernst Ferdinand Guerike, Henry
Melville Gwatkin, Henry Hammond, H. G. Hartwig, Karl August
von Hase, B. W. Henderson, Johann Goitfried von Herder, Adolf
Hilgenfeld, David Hill, F. J. A. Hort, H. J. Holtzmann, John
