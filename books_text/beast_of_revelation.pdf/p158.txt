126 The Beast of Revelation

read of the names of a couple of other martyrs now virtually
unknown, Danaids and Dircae. It is quite remarkable that he cites
names of those involved in the Neronian persecution which alleg-
edly occurred about thirty years previous to his own day, but that
he is strangely silent about the names of those who died in the
Domitianic persecution — even though they were supposed to be
prominent members of his own congregation! In both sections five
and six of his letter, Clement gives many sentences to explication
of these Neronian woes. But it is quite curious, on the supposition
of a Domitianic date, that in / Clement 1 he uses only ten words (in
the Greek) that supposedly refer to the Domitianic persecution,
the persecution through which he and many of his friends were
allegedly going! That reference reads: “by reason of the sudden
and successive troubles and calamities which have befallen us.”

If, however, the letter were written sometime approaching, or
in, early A.D. 70, then the first, fifth, and sixth sections would al!
speak of the Neronian persecution. In the course of its long history
the city of Rome had never witnessed so many “sudden and
successive troubles and calamities” among its population gener-
ally, and for the Christians particularly, than in Nero’s rule. His
era eventually issued forth in the chaotic and destructive Year of
the Four Emperors,

Tacitus introduces Rome’s history after the death of Nero
thus:

The history on which I am entering is that of a period rich in
disasters, terrible with battles, torn by civil struggles, horrible even
in peace. Four et nperors failed by the sword; there were three civil
wars, more foreign wars and often both at the same time. There
was success in i he East, misfortune in the West. Ilyricum was
disturbed, the Gallic provinces wavering, Britain subdued and
immediately let 30. The Sarmatae and Suebi rose against us; the
Dacians won fame by defeats inflicted and suffered; even the
Parthians were almost roused to arms through the trickery of a
pretended Nero. Moreover, Italy was distressed by disasters un-
mown before or returning after the lapse of ages. Cities of the rich
fertile shores of Campania were swallowed up or overwhelmed;
