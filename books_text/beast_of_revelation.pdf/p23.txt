Publisher’s Preface av

else’s — on “The Jim and Tammy Show.” He does have a weekly
radio show and a weekly satellite television show.) Hunt’s promo-
tional copy announces: “Today, a growing number of Christians
are exchanging the hope for the rapture for a new hope . . . that
Christians can clean up society. ...” The promise — unfulfilled,
I might add — of the back cover is that this book will show old
fashioned dispensationalists “how we lost that hope [the rapture]
and how it can be regained.” The success of his books proves that
there are still buyers of the old literature who love to be thrilled
by new tales of the beast. This means, of course, that they do not
want to hear about the biblical account of the beast of Revelation.
They much prefer fantasy.

This Book Is About Hope

If the rapture is just around the corner, then the beast and the
antichrist are in our midst already, preparing to take advantage
of every opportunity to deceive, persecute, and tyrannize the world
generally and Christians in particular. This would mean that all
attempts by Christians to improve this world through the preach-
ing of the gospel and obedience to God’s Word are doomed. There
would be insufficient time to reclaim anything from the jaws of
inevitable eschatological defeat. This is precisely what dispensa-
tionalists believe, as I hope to demonstrate in this subsection.

Dave Hunt assures us that the cultural defeat of the church of
Jesus Christ is inevitable. Our task is to escape this world, not
change it. Those who teach otherwise, he says, “mistakenly believe
that the church is in this world to eliminate evil, when in fact it is
only here as God’s instrument of restraint. It is not our job to
transform this world but to call out of it those who will respond
to the gospel.”!© In short, he views the church’s work in this world
in terms of his view of the church’s only hope: escape from the irials
and tribulations of life. We are to call men out of this world, spiritu-
ally speaking, so that Jesus will come back in the clouds and call

16, Hunt, Whateer Happened to Heavn?, pp. 268-69.
