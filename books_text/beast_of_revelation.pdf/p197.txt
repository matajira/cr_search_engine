The Historical Evidence (2) 165

Contradictory Assertions

In the second place, Eusebius contradicts himself in his writ-
ings on the banishment ofJohn. It is clear in his Ecclesiastical History
that he believes John was banished under Domitian. But in Evan-
gelical Demonstrations 3:5, he speaks of the execution of Peter and
Paul in the same sentence with the banishment of John. This
clearly implies the events happened together, Thus, it indicates
that when he wrote Evangelical Demonstrations, he was convinced of
a Neronic banishment ofJohn.

Thus, again we discover that one of the leading witnesses from
tradition for the late date of Revelation is not all that solid a piece
of evidence.

Jerome

As a number of late-date proponents argue, Jerome seems to
regard John as having been banished by Domitian.“Due to its
context, however, this evidence may not be as strongly supportive
as many think. The context tends to confuse the matter by giving
evidence of Jerome’s confounding of two traditions. In his Against
Jovinianum we read that John was “a prophet, for he saw in the
island of Patmos, to which he had been banished by the Emperor
Domitian as a martyr of the Lord, an Apocalypse containing
boundless mysteries with the future. Tertullian, moreover, relates
that he was sent to Rome, and that having been plunged into ajar
of boiling oil he came out fresher and more active than when he
went in.”*

As shown above, the reference from Tertullian strongly sug-
gests a Neronic date. Thus, Jerome’s evidence seems confused and
is indicative of two competing traditions regarding the date of
John’s banishment, and, hence, the date of Revelation.

Conclusion
I cannot see how the external evidence can be used with much

42, See Swete, Charles, Mounce, Moffatt, Warfield, and Tenney.
43, Jerome, Against Jovinionum 1:26,
