Introduction 5
Yet there are legitimate

Reasons for Revelation’s Difficulty

Undoubtedly, a major reason for Revelation’s difficulty of
interpretation is due to its literary form. It is not written as simple
historical narrative or didactic instruction. Rather, it is a book of
fearsome visions and strange symbols, employing imagery drawn
from Old Testament prophecy and ancient culture. Its literary
style understandably makes Revelation a difficult work, particu-
larly for modern, Western Christians.

Much of the data concerning the Beast in Revelation is ren-
dered difficult because of the symbolic style John employs. The
terrifying visual appearance of the Beast in Revelation 13:1-2 is
obviously symbolic, but why does John employ such imagery? The
death and resurrection of the Beast in Revelation 13:3 indicate
something quite dramatic, but what? The rendering of the Beast’s
name in numerals in Revelation 13:18 is a unique feature in
Scripture, but what does it mean? The Great Harlot in Revelation
17:1-6 is quite mysterious and is somehow connected to the Beast,
but who is she and how does she relate to the Beast?

Beyond the widely recognized difficulty of the style of Revela-
tion, there is an equally serious matter that eonfronts the would-be
interpreter. The issue to which we refer is the question of the date
of the writing of Revelation.

Basically there are two possibilities regarding the date of Reve-
lation’s composition which are open to evangelical Christian
John may have written Revelation prior to the destruction of the
Temple, which occurred in A.D. 70. Or possibly he composed it
a generation later, around A.D. 95-96, in the last days of the reign
of the emperor Domitian. The second view is the majority opinion
among contemporary commentators. The first view is the convic-
tion of the present writer and a growing minority of biblical
scholars.

Few Christians-in-the-pew realize the importance of this mat-
ter for the interpretation of Revelation. This is partially due to the
