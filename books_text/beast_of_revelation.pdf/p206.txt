174 The Beast of Revelation

able to admit the presence of an element which the late-date school
proffers as a leading proof for its position! Beyond these two initial
problems, however, there are significant and reasonable possibili-
ties available to hand which wholly undermine the Nero Redivious
argument for a late date.

Despite the intriguing correspondences between the Nero Redi-
views myth and some of Revelation prophecies, the two are not
related. An extrenely strong case can be made for an interpreta-
tion of the relevant passages that has nothing whatsoever to do
with the Nero Re divious myth. In addition, this interpretation is
more appropriate, not only in regard to one of the major events of
the first century, hut also to the theme of Revelation. The interpre-
tation of which I speak is given in Chapter 7 above, on the revival
of the Beast. What John is speaking about is not a myth, but the
historical phenomena associated with the death of Nero, the near
demise of Rome, 2nd its reestablishment under Vespasian.

Late-date proponent James Moffatt is particularly interesting
at this point. He attempts to hold to the best of both worlds: (1)
He vigorously aiserts that the Nero Redivious myth appears in
Revelation 13 and 17. He urges that its appearance is helpful for
establishing the 1 ate date for Revelation, in that its highly devel-
oped form is not possible until Domitian’s reign (A.D. 81-96).”
(2) But then he a so adopts the interpretation of Revelation 13 and
17 that we suggest! That is, that the death wound and revival of
the beast make reference to the Roman Civil Wars of A.D. 68-69.
Notice his comments on Revelation 13:3: “The allusion is .. . to
the terrible convulsions which in 69 A.D. shook the empire to its
foundations (Tat Hist. i. 11). Nero’s death with the bloody inter-
regnum after it, was a wound to the State, from which it only
recovered under Vespasian. It fulfilled the tradition of the wounded
head... . The vitality of the pagan empire, shown in this power
of righting itself after the revolution, only added to its prestige.”2"

21. Mofatt, Reselation, p. 317.
22, Ibid, p. 430.
