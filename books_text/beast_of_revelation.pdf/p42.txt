Introduction 7
OUYr Purpose

The main purpose of the present work is to ascertain the
identity of the Beast of Revelation. I believe that the evidence is
there to identify clearly the very name and historical circumstances
of the Beast. I also believe that as the reader considers the material
to be set forth before him he will find the identification not only
persuasive, but surprisingly easy and startling.

An auxiliary purpose will be to provide the reader with a
synopsis of some of the arguments for the proper dating of Revela-
tion. As will become evident, the matter of dating is all-important
to the identity of the Beast. The concerned student should consult
the more extensive and technical research provided in my doctoral
dissertation, Te Date of Revelation: An Exegetical and Historical Argu-
ment for a Pre-A.D. 70 Composition. That dissertation, submitted to
Whitefield Theological Seminary in Lakeland, Florida, in March,
1988, is also published by the Institute for Christian Economics
- under the title Before Jerusalem Fell: Dating the Book of Revelation
(1989).
