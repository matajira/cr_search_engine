Objections to tie Early Date 175

Here a vigorous late-date advocate and Nero Redivivus enthusi-
ast admits that the references allude to the Roman Civil Wars and
Rome’s revival under Vespasian! This is a telling admission.” If
the references in question can be applied to the Roman Civil Wars
of A.D. 68-69, how can these same references point to Nero Rediuivus
and demand an A.D. 96 date for the book?

If the verses in Revelation can properly be understood as
making reference to the earth-shaking historical events of the era,
why would any commentator be driven to employ a myth to make
sense of the passages? And this being the case, how can the myth
be used as a major chronology datum from the internal evidence?

From our observations, it is obvious that the Nero Redivivus
myth cannot be used with any degree of success to establish a late
date for Revelation. There is good reason to doubt that it even
appears in Revelation! The doubt is so strong that some late-date
advocates refuse to employ it.24 The presumed evidence based on
this myth cannot undermine the facts derived from the docu-
mented historical matters by which we have established its early
date.

The Condition of the Seven Churches

Fourth, the historical situations of the seven churches, to which
Revelation is addressed (Rev. 1:4; 2; 3), seem to suggest a late
date. Since these are historical churches to which John wrote, the
letters may be expected to contain historical allusions which would
be helpful in dating. As Morris states it, the “indication is that the
churches of Asia Minor seem to have a period of development
behind them. This would scarcely have been possible at the time
of the Neronic persecution, the only serious competitor in date to

23, Interestingly, Mounce does the same thing On page 34 of his work, he
employs the myth to demonstrate a late date for Revelation, but in hia commentary
at Revelation 18 and 17 he opta for the revival-of-the-Empire interpretation (Ravla-
tion, pp. 216, 253).

24, For example, Guthrie, Zntraduction, pp, 955-064,
