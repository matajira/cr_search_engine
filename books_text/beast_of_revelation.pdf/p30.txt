xxii The Beast of Revelation

They adopt the pietistic platitude: “You don’t polish brow on a sinking
ship.” Many pessimistic pretribbers ding to the humanists’ version
of religious freedom; namely Christian social and political impo-
tence, self-imposed, as drowning men cling to a life preserver.?

Removing Illegitimate Fears

David Chilton shows in The Great Tribulation that Christians’
fears regarding some inevitable Great Tribulation for the Church
are not grounded in Scripture. Kenneth Gentry shows in this book

that the beast of Revelation is not lurking around the corner.

Neither is the rapture. Thus, Christians can have legitimate hope
in the positive earthly outcome of their prayers and labors. Their

sacrifices today will make a difference in the long run. There is

continuity between their efforts today and the long-term expansion

of God’s civilizat on in history (“civilization” is just another word.

for “kingdom”). Jesus’ words are true: there will be no eschatologi-

cal discontinuity, no cataclysmic disruption, no rapture in between
today and Christ’s second coming at the final judgment:

Another parable put he forth unto them, saying, The kingdom
of heaven is likened unto a man which sowed good seed in his field:
But while men slept, his enemy came and sowed tares among the
wheat, and went his way. But when the blade was sprung up, and
brought forth fruit, then appeared the tares also. So the servants
of the householder came and said unto him, Sir, didst not thou sow
good seed in thy field? from whence then bath it tares? He said
unto them, An enemy bath done this. The servants said unto him,
Wilt thou then that we go and gather them up? But he said, Nay;
lest while ye ga ‘her up the tares, ye root up also the wheat with
them. Let both grow together until the harvest: and in the time of
harvest I will say to the reapers, Gather ye together first the tares,
and bind them in bundles to burn then-x but gather the wheat into
my barn (Matt. 13:24-31).

The apostles did not understand the meaning of this parable.

29. David Schnitiger, Christian Reconstruction from a, Preivibulational Perspectice (Okla-

homa City: Southwest Radio Church, 1986), p. 7.
