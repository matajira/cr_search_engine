54 The Beast of Revelation

dreadful persecution ceased but with the death of Nero. The

empire, it is well known, was not delivered from the tyranny of
this monster until the year 68, when he put an end to his own
life.”2” At that tine the empire was embroiled in civil war and

could not afford to be distracted by the Christians.

But for a few days, this represents a period of 42 months! How
significant! Not only does Nero’s name fit the number of the Beast,
but his persecution lasted the very time required by the Beast’s
“war with the saints.”

A Common Objection

Most commentators agree that Revelation definitely breathes
the atmosphere of violent persecution. But the question arises:
Which persecution? The Neronic or the Domitianic? Although a
number of comm rotators argue that the persecution background
of Revelation is that of Domitian, this view is not supported by the
evidence.

Unfortunate] for those who claim a Domitianic persecution
background for Revelation, there is a good deal of debate as to
whether Domitian even persecuted Christians! George E. Ladd is
a capable New Testament scholar who believes Revelation was
written during Domitian’s reign. Nevertheless, he warns against
the use of evidence drawn from the persecution motif for proving
that John wrote the book under Domitian: “The problem with this
theory is that there is no evidence that during the last decade of
the first century there occurred any open and systematic persecu-
tion of the church .“2*Reginald H. Fuller also argues for a Domi-
tianic date of Revelation, but he advises that “there is otherwise
no evidence for the persecution of Christians in Asia Minor” under
Domitian.” Leon Morris also laments: “While later Christians

22, von Mosheim, Historica! Commentaries 1:138,139,

23. George Eldon Iadd, A Commentary on the Revelation of John (Grand Rapids:
Eerdmans, 1972}, p. 8.

24, Reginald H. Fuller, A Critical Introduction to the Naw Testament (Letchworth:
Duckworth, 1971), p. 137.
