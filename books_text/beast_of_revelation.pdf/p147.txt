The Architectural Evidence ig

of the material temple to Christ. (2) That very temple to which
they pointed was destroyed in A.D. 70 in a manner which precisely
fulfilled the terms of the prophecy.

Note that in Luke21 :5-7a the disciples pointed out the actual
features of that historical structure: “And while some were talking
about the temple, that it was adorned with beautiful stones and
votive gifts, He said, ‘As for these things which you are looking at,
the days will come in which there will not be left one stone upon
another which will not be torn down. And they questioned Him,
saying, ‘Teacher, when therefore will these things be?” The proph-
ecy that follows the disciples’ remarks was definitely spoken by the
Lord as the historical temple stood! In Luke’s record of the Olivet
Discourse, Christ specifically speaks of the dismantling of the
temple and destruction of Jerusalem in terms which form the basis
of those in Revelation 11.

A little further into the context, we read in Luke 21:24 “and
they will fall by the edge of the sword, and will be led captive into
all the nations; and Jerusalem will be trampled underfoot by the Gentiles
until the times of the Gentiles be fulfilled.” Compare this to
Revelation 11 :2b, which reads: “it has been given to the nations;
and they will tread under foot the Holy city for forty-two months.” In
these two passages the correspondences are so strong, they surely
bespeak historical identity, rather than mere accidental similarity:

Luke 21:24 { Revelation U:2
Jerusalem = the holy city
Gentiles (céine) = nations (ciknesin)
trampled underfoot (paioumene) = tread under foot (patesousin)
It is evident that these verses in both John’s Revelation and

Luke’s Gospel look to the same events."And these events were
literal occurrences that happened to historical institutions and

5, This may explain why John’s Gospel is the only Gospel which does not record
Christ’s Olivet Discourse, which pointed to the destruction of the Temple. See
Matthew 24 Mark 13; Luke 21.
