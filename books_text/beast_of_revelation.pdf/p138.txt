106 The Beast of Revelation

the beast that caries her, which has the seven heads and the ten
horns” (Rev. 17:7). What follows is the angel’s exposition of the
vision. He is not providing John with more difficult material, but is
explaining the confiing aspect of ihe vision. This experience, then, is
reminiscent of an earlier situation in which an “elder” (an angel
of some sort) explains to John the vision of the “great multitude”
in Revelation 7:9, 18, 14. In both Revelation 7 and 17 the angelic
explanation is provided as a felp to the interpretation of a vision.

The difficulty of the vision requiring “wisdom” is that the
interpretation of she imagery involves a two-fold referent. “The
seven heads are [1] seven mountains on which the woman sits, and
they are [2] seven kings” (w. 9-10a), This feature would doubtless
escape the interpreter without the angelic explication. It would.
appear, therefore, that the expression “here is the mind which has
wisdom” is introducing the interpretation of a vision. Consequently,
it is the case that he who follows the angelic interpretation “has
wisdom.” To argue that the following statements become more
difficult would go contrary to the stated purpose of the angelic
explanation. Therefore, we may expect to be able to easily under-
stand the statements in Revelation 17:9, 10. And these statements
point to the reign of Nero.

Identifying the First King

Many scholars argue that the line of the emperors of Rome
officially began with Augustus, whom I have numbered the second
king. Technically his may be true, in that Julius Caesar was not
legally an emperor. But the historical evidence still points to the
legitimacy of starting the count with Julius Caesar. As a matter of
fact, Julius Caesar did claim the title /mperator, according to Ro-
man historian Suctonius.© Furthermore, the subsequent rulers of
Rome, who were technically emperors, called themselves by the
name “Caesar,” indicating a contittuity with Julius Caesar.

But even more persuasive is the fact that records from the era

6, Suetonius, Julius 76.
