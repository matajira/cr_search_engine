38 The Beast of Revelation

John originally wrote it in Aramaic, a cognate language to He-
brew2?

Second, in fact there are other very Hebraic names in Revela-
tion. For instance, the words “Abaddon” (Rev. 9:11) and “Arma-
geddon” (Rev. 16: 16) are Hebrew words which are given Greek
equivalents. The Hebrew word “Satan” is used by John, but is
interpreted into Greek as “the devil” (Rev. 12:9). Other Hebrew
words appear, as well: “Amen” is said to mean “truthfully” (Rev.
3:14}. The Hebrew “hallelujah” is not even translated into a Greek
equivalent (Rev. 19:1, 3, 4, 6). How natural, it would seem, to
adopt a Hebraic s pelling for the basis of the cryptogram.

Third, Asia Minor was well populated by Jews. As a matter
of fact “long bef ore the Christian era the Jews had formed a
considerable factcr in the population of the Asian cities.”2° Indeed,
the Jews “were a notable part of the population of Alexandria.
They were strongly rooted in Syria and Asia Minor. ... Few
cities of the empi’e were without their presence.”® The audience
could well have t cen composed of at least a significant minority
ofJews.

And why shculd John not use an Hebraic riddle? Was not
John himself a Jew? Was not he, the writer of Revelation, sent “to
the circumcised” (Gal. 2:9)? Despite the brevity of each of the
Seven Letters, in t hem are prominent allusions to Jewish situations
(Rev. 2:9, 14; 3:9). In the book itself are very definite allusions to
Jewish matters, such as the twelve tribes of Israel (Rev. 7 and 14),

Conclusion

The role of N2ro Caesar in Revelation is written large. As all
roads lead to Rome, so do they ali terminate at Nero Caesar’s
Palace. The facto-s pointing to Nero in Revelation are numerous

27, Charles C, Torny, The Apocalypse of John (New Haven: Yale University Press,
1958), pp. 27-58.

28 Swete, Raelation, p, Ixvi.

29. Williston Walke, A History of the Christion Church, 3rd ed. (New York Scrib-
ner’s, 1970), p. 16.
