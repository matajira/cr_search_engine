The Historical Evidence (2) 161

discover Nero’s name is known even today, whereas Domitian’s
is forgotten. Go to your local library and try to check out some
books on Domitian. Probably you will not find any. But you very
likely will find several on Nero. Nero gained himself great fame as
a tyrant burned into the memory of history.

But there is more!

The Contextual Difficulty

To further compound the problem for late-date employment
of Clement there is the difficulty which surfaces in the context of
his famous statement. The context following the critical statement
is more easily believable if John were about thirty years younger,
as he would have been in A.D. 65-66 as opposed to A.D. 95-96.
Let us consider it and see if you agree.

In connection with his returning from banishment under the
“tyrant,” Clement informs us ofJohn’s activities, which are wholly
incredible if by a man in his 90s. I will cite the passage again:
“When after the death of the tyrant he removed from the island of
Patmos to Ephesus, he used to journey by request to the neighbor-
ing districts of the Gentiles, in some places to appoint bishops, in
others to regulate whole churches, in others to set among the clergy
some one man.”3" In further illustration of his activities, Clement
immediately added to the account a story in which John, disturbed
by a young church leader’s forsaking of the faith, chased him on
horseback “But when he recognised John as he advanced, he
turned, ashamed, to flight. The other followed with all his might,
forgetting his age, crying, ‘Why, my son, dost thou flee from me,
thy father, unarmed, old? Son, pity me.”

This is quite strenuous missionary activity for a man who by
that time had to be in his 90s! And the fact that he is said to have
forgotten his “age” does not indicate he may have been ninety.
Paul calls himself “the aged” while nowhere near that old (Philem.

85, Clement of Alexandria, Who 1g the Rich Man that Shall be Saved? 42,
36. Tid.
