18 The Beast of Revelation

most of Rome, broke out. Although he was out of Rome at the
time, suspicion was cast upon Nero for causing the fire. Many
were convinced | hat since he deplored the ugliness of Rome he
intended to destroy it to make room for more of his own building
projects.”? In order to turn attention ii-em himself, he falsely
accused Christians of having started the fire and punished them
for being “given t> a new and mischievous superstition.”?!

Nero was a lover of music, theater, and the circus, vainly
fancying himself to be one of the world’s greatest musicians, actors,
and charioteers.** Suetonius records that “while he was singing
no one was allowed to leave the theatre even for the most urgent
reasons, And so it is said that some women gave birth to children
there, while many who were worn out with listening and applaud-
ing. .. feigned death and were carried out as if for burial.”* He
even virtually abandoned direct rule of Rome for a two year visit
to Greece (A.D. € 7-68) in order to appear in their music festivals,

Nero’s Death

Disgusted wit: his absence from Rome, his excesses in Me, and
enormous political abuses, a revolt against Nero began in Gaul.
But it was quickly put down. Shortly thereafter the revolt broke
out anew under C ‘alba in Spain in A.D. 68."Tom with indecision
as to what to do in such pressing circumstances, Nero hesitated
in acting against Galba. When the revoh had gathered too much
strength he talked of suicide, but was too cowardly and again

28. Philip Schaff, Fstory of the Christian Church, 7vols., 8rd ed. (Grand Rapids:
Eerdmans, 1910) 1 :379; B, W. Henderson, The Lift and Principate of Neo (London:
Methuen and Co, 1903), p. 237,

29, Suetonius, Nero 33; TacitUs, Amals 15:38

80. TacitUs, Annals 15:44.

31, Suetonius, Nero ‘6.

32, Suetonius, Nero 23-25. See Miriam T. Griffin, Nero: The End oftz Dynasty (New
Haven: Yale University Preas, 1984), ch. 9,

83, Suetonius, Nero 23.
34, Suetonius, New <0
