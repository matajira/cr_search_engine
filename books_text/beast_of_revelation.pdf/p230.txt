198

69-70>78,77, 182, 184.

heads, 10, 11, 12, 13, 42, 68, 69, 70,
78,76, 102-110, 174.

horns, 12, 106.

identity, 8, 7, 9-20, 21, 28, 82, 85, 48,
44, 182, 184, 185.187

image, 65, 66.

mission, 10,

name, 5, 7, 10,82, 0, 153.

number of (See al: Six hundred, sixty-
aix), 5,9, 10, 19, 20-89,54.

relevance, 10, 28, 29, 32, 50, 66-77,
194,

revival, 5,44>68, 6-77, 174, 185.

specific referent, 11, 12, 14, 19,56,57,
67,69-70, 77, 182, 184.

war of (See also: Persecution -
Neronic), 43, 47-36, 67,70,188.

worship (See alsa: :mperor worship),
57-87, 168.

Beasts (Ste also: Anim:}; Beast of
Revelation), 41,46,51, 160,

Beheaded, 17,68.

Bible (or Scripture) (Sv als: Revelation),
xiv, xviii, xxi, xxiii, cov, 8, 4,5, 1 In,
19,26,34,36,89, 9), 99,94, 112, 125,
129, 138, 141, 173, 176, 182, 185.
present author’s cor nmitrnent to,

84,118,

Biahop, 36, 124, 145, ‘50, 164, 157, 161,
168, 164, 178.

Biahop of Rome, 121, 140, 141.

Blasphemy, 10,82,47, 181, 169.

Blood, 42,49,60,71,76,91,96, 144, 171.

Bottomless pit (See: Abyss).

Britien, 72, 126,

Britannicus, 15.

Burning ail (See: Oil).

Burrus, 16, 17,

 

Caesar, title of rank, (See individual
emperors), 48, 60, 61, 71, 106, 107,

‘The Beast of Revelation

108, 145, 171.

Caesarean family (Sx: Julio Claudian).

Caius (emperor), (Se: Gaius).

Caligula (emperor, AKA “Gaius”), 16,
61,63,64,104,
worship of, 61-62,64,65,67.

Canon, 91, 143-144,

Cassius, Die, 42, 48, 66, 66, 107, 159,
172.

Catastrophe, 71.

Charioteer, 18,51,68.

Christ (See: Jesus Christ).

Christian(s), xiii, xvi, xvii, xix, xxiii, xxv,
xuxi, xxxiv, 4,5, 10, 14, 18, 19, 29, 82,
43, 44, 47, 48, 49, 60, 51, 64, 86, 87,
98, 107, 120, 121, 125, 180, 188, 184,
185, 136, 187, 144, 158, 159, 160, 168,
176, 181, 184, 187,

Christianity, x,3, 4,6,26,38,49,50,52,
63, 65, 56, 69, 77, 85, 86, 116, 117,
181, 182, 135, 139, 157, 170, 182.
confused with Judaism by Remans,

49, 136,
crimes of alleged, 51.
history (Se: History — church).
Jewish origin, 86, 121, 129-197, 181,
186-187.
literature, 82,45,55, 109,180, 185.
separation from Judaism, 49, 190, 137,
181, 187.
Chronology, 69,76, 189.
Church(es) (See alse: History — church;
Tradition), 18, 14, 85, 182, 154, 161,
167, 175,178, 181.
at Rome, 43, 50, 51, 123-128, 143,
144,

Fathers, xvi, 36,87,82,92, 181, 185,
136, 151, 159, 163.

unity, 154,

universal, xi, xili, xiv, xvi, xxv, xvi,
xxix, 3, 11, 26, 46, 62, 59, 64, 86,
111, 112, 113, 120, 121, 125, 129,
