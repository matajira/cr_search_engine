The Relevance of the Beast 23

a short time,” “shortly,” “very quickly,” and “without delay.”* If
you look up Revelation 1:1 in any modern translation you will find
that the idea clearly exhibited is that of the very near occurrence
of the events of Revelation. This term also occurs in Revelation
2:16; 3:11; and 22:6, 7, 12, 20. Even a cursory reading of these
verses unavoidably leads to the conclusion that John expected
these things to happen “shortly” or “quickly.”

Another term John uses is eggus (pronounced engus), which
means “near” (Rey, 1:3; 22: 10). In Revelation 1:3 we read: “Blessed
is he who reads and those who hear the words of the prophecy,
and heeds the things which are written in it; for the time is near
(eggus].” When used of spatial relationships it means “near,” “close
to,” “close by.” When used of temporal relationships it signifies
“near,” “soon.”3 This term literally means “at hand.”4 Its import
in our context is clearly that of temporal nearness. The events
bracketed by these statements were expected, by the holy apostle
John, to begin taking place at any moment.

The final term we can note is mello, which means “about to”
(Rev. 1:19; 3:10). When found in both of the verb forms appearing
in Revelation 1:19 and 3:10, this term means “be on the point of,
be about to.”6 A number of Bible translations confuse the matter
when they translate the word properly in Revelation 3:10 but
improperly in Revelation 1:19. According to Young’s Literal Transla-
tion of the Bible, Revelation 1:19 reads: “Write the things that thou
hast seen, and the things that are, and the things that are about ta
come [redo] after these things.”© The leading interlinear versions

2, William F, Arndt and F. Wilbur Gingrich, A Greek-English Lexicon of the New
Testament and Other Early Christian Literature (Chicago: University of Chicago Press,
1957), p. 814,

3. bid, p 213.

4, The word is derived fiom the compounding ofen (in, at) and gus (limb,
hand). See Joseph H. Thayer, A Greek-English Lexicon of the New Testament, Ind ed.
(New York: American Book Co., 1889), p. 164,

5, Amdt-Gingrieh, Zericon, p, 502 (-b),

&. Robert Young, Young’s Literal Translation of the Holy Bible, Ind ed. (Grand
Rapids: Baker, rep. 1898), p. 167 (New Testament),
