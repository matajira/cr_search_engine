The Historical Evidence (2) 151

Thus, the long-standing tendency to rely heavily on Irenaeus is
not unreasonable. The strength of the reliance on Irenaeus is
clearly indicated in A. S. Peake’s commentary “In deference to
our earliest evidence, the statement of Irenaeus, the Book was
generally considered to belong to the close of Domitian’s reign.”>
More recent scholarship tends to rely heavily upon Irenaeus as
well.”

Frenaeus’s Statement

The evidence from Irenaeus is found in Book 5, Chapter 30,
Paragraph 8 of his Against Heresies. Although originally composed
in Greek, today this work exists in its entirety only in Latin
translation. Thankfully, however, the particular statement in ques-
tion is preserved for us in the original Greek twice in Eusebius’s
Ecclesiastical History, at 3:18:83 and 5:8.6.

Trenaeus’s crucial statement occurs at the end of a section
dealing with the identification of “666,” which he applies to the
Antichrist, in Revelation 13, That statement is generally translated
into English as follows: “We will not, however, incur the risk of
pronouncing positively as to the name of Antichrist; for if it were
necessary that his name should be distinctly revealed in this
present time, it would have been announced by him who beheld.
the apocalyptic vision. For that was seen no very long time since,
but almost in our day, towards the end of Domitian’s reign.”8 The
late-date advocate argues that this serves as compelling evidence
that John “saw” the Revelation “at the end of the reign of Domi-
tian.”

How shall early date advocacy deal with such strong and
forthright testimony by this noteworthy ancient church father? As
a matter of fact, there are several problems that arise which tend.

6, Arthur 8, Peake, The Revelation of john (London: Joseph Johnaon, 1919), p. 70.

7. Mounce, Revelation, p. 32; Sweet, Revelation, p. 21; Guthrie, New Testament
Introduction, pp. 956-957; Kummel, Introduction tothe New Testament, pp. 466-467;
Hendriksen, More Than Conquerors, pp. 19-20.

8, Coxe, in Ante-Nicene Fathers 1:559-560,
