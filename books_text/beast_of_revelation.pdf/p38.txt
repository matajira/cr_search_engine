INTRODUCTION

Blessed is he who reads and those who hear the words of the prophecy,
and heed the things which are written in it, .. (Rev. 1 :3a),

Revelation is one of the most fascinating and intriguing books
of the Bible. Evidently this has always been the case, for it was one
of the most widely circulated of New Testament books in the early
centuries of Christianity.! Following upon these early Christian
centuries, the Middle Ages and the Reformation era experienced.
an explosion of commentaries on Revelation.*

Interest in Revelation shows no sign of slackening today. In
fact, as the magic year 2000 looms ever nearer, we are witnessing
a reinvigorated interest in prophecy in general, and Revelation in
particular, Check any Christian bookstore and you will discover a
vast selection of books on eschatology and Revelation.

One of the most interesting questions debated in regard to
Revelation is that of the identity of the Beast of Revelation 13 and
17. This terrifying enemy of God and His Church has fascinated
both the Christian and non-Christian mind down through the
ages.

Who is this nefarious personage?

What is his role in biblical eschatology?

Does his foreboding visage haunt our future?

1, Robert Mounce, The Hook of Raviation (Grand Rapids: Eerdmans, 1977), p.
36; Walter F. Adeney, Nav Testament, vol. 2 of A Biblical introduction (London: Methuen,
1911), p, 461,

2, Henry B, Swete, Commentary on Revelation (Grand Rapids: Kregel, [1911] 1977),
p. cxcvii.

3
