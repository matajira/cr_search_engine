The War of ihe Beast 49

showed himself an enemy of the divine religion.”? Sulpicius Sev-
erus (A.D. 360-420) concurs: “He first attempted to abolish the
name of Christian.”*

As an imperial persecution (as opposed to the Jewish persecu-
tions witnessed in Acts) it had the effect of removing early Christi-
anity’s protected status as a teligio licita “legal religion”). Until
this time Christianity was assumed to be a sect of Judaism and
thus protected under the umbrella ofJudaism as a “legal religion.”
In his classic study on persecution, Workman confidently asserts
that

we can date with some certainty this distinction in the official mind
between Jew and Christian as first becoming clear in the summer
of 64, The acquittal of St. Paul in 61 or 62 - an event we may fairly
assume as probable —is proof that in that year Chris-
tianity, a distinct name for which was only slowly coming into use,
could still claim that it was a religiolicita.. . stilirecognized as a
branch of Judaism. ... At any rate, both Nero and Rome now
[in A.D. 64] clearly distinguished between the religiolicita of Juda-
ism and the new sect. ... The destruction of Jerusalem would
remove the last elements of confusion."

This protected status during Christianity’s infancy was vitally
important in that it gave apostolic Christianity time to spread and
gain a solid footing in the Empire. From the time of the Neronic
persecution, however, Christianity would be distinguished from
Judaism and would be exposed to the unprovoked cruelty of
Rome.

That this persecution was against Christians as such may be
proved not only from Christian but pagan sources. In his Annals
Roman historian Tacitus (A.D. 56-117) points to those who were

3. Eusebius, Ecclesiastical History 2:25:3.

4, Sulpicius Severus, Sacred History 2:28. See also Tertullian (A.D, 160-220), On
the Made 4; Apology 5; Paulus Orosius (A.D. 385-415), The Seven Books of History
Against the Pagans 7:2.

5. Herbert B, Workman, Persecution in the Early Church (Oxford Oxford University
Press, [1906] 1980), p. 22,
