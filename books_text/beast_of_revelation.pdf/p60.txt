26 The Beast of Revelation

Church (Matt. 18:20}, to believers at death (John 141-3),“to
God in heaven to receive His kingdom (Dan. 7:18), and in judicial
judgment upon men in history (Matt. 21:40, 41; Rev. 2:5). But
to which sort of “coming” do the verses mentioned above from
Revelation refer?

The references in Revelation to His coming have to do with
His coming in judgment, particularly upon Israel. This is evident in
the theme verse of Revelation found in Revelation 1:7: “Behold,
He is coming with the clouds, and every eye will see Him, even
those who piercec! Him, and all the tribes of the earth will mourn
over Him. Even so. Amen.” This cloud-coming of Christ in judg-
ment is reminiscent of Old Testament cloud-comings of God in
judgment upon ancient historical people and nations (Pss. 18:7-15;
1048; Isa. 19:1; Joel 2:1, 2; Hab. 1:2ff; Zeph. 1:14, 15).

Furthermore, it is obvious that this coming is a judgment
coming focusing upon first century Israel. Revelation 1:7 says He
is coming upon “those who pierced Him.” It states that as a
consequence all “the tribes of the earth [or Land)’ will mourn.
The New Testament is emphatic in pointing to first century Israel
as responsible for crucifying Christ (John 19:6, 15; Acts 2:22-23,
36; 3:13-15; 5:30; 7:52; 1 Thess, 2:14-15).!6

Jesus even told the Jewish leaders that they would personally
witness this judgment-coming (Matt. 26:64). This coming (Matt.
24:30)” was to occur in His generation (Matt. 2480,3* cp. Matt.
23:31-36). It was to be witnessed by men who stood and listened

14, Here again the Greek word used ig entomai,
bin Matthew 2l::0 the Greek word ig the aorist tense form of the Greek verb
erchomai,

16, Early post-apostalic Christianity continued this theme of pointing to the Jews
as the ones who pierce! Him, See Ignatius (A.D. 50-115), Magnesians 11 and Trallians
11, Justin Martyr (A. 100-165), First Apology ch. 35, ch. 38, and Dialogue with
Typlo 72, More detailei information on Revelation 1:7 maybe found in Chapter 9,

17, The Dan, 7:13 context - upon which Matt. 2430 and 2664 are based — refers
to the Ascension of Christ to take up His kingly rule. The dramatic, historical

judgment-experience or witness of the fact of His having ascended is the destruction
of the Temple, which « ent is in view in these and related passages,
