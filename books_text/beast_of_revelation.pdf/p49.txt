The Identity of the Beast 16

a vicious man from a noted, but cruel, Roman family. The entire
family was “notorious for instability, treachery and licentious-
ness” '° Nero’s father is spoken of as “hateful in every walk of
life." Nero’s famous, conniving, and ill-fated mother was Agrip-
pina, the sister of Emperor Gaius (also known as “Caligula”) and
niece of the emperor Claudius.

Nero was born on December 15th, A.D. 37, just nine months
after the death of Emperor Tiberius, under whom Christ was
crucified. He was born with bright red hair, as was common to his
lineage (the name “Ahenobarbus” meant “red-beard”) .”"Nero
was born feet first; among a superstitious and pagan people this
was considered to be an evil omen. This omen did not go unnoticed
by Roman historians of the day.* Many astrologers “at once
made many direful predictions from his horoscope.”!* On the day
of his birth even Nero’s father predicted that this offspring could.
only be abominable and disastrous for the public.'5

Nero’s cruel character evidenced itself quite early. At twelve
years of age and upon having been adopted by the emperor
Claudius Caesar, Nero began to accuse his brother Britannicus of
being a “changeling,“ in order to bring him into disfavor before
the emperor. At the same time he even served as a public witness
in a trial against his aunt Lepida, in order to ruin her.!

Agrippina, Nero’s mother, plotted and schemed to secure
Nero a high position in imperial Rome. Upon the death of the wife
of the emperor Claudius she began to make her moves. She
arranged Nero’s marriage to the daughter of Claudius, labored to
get the Roman law changed to allow her to marry Claudius (her

a Arthur Weigall, Nero: Emperor of Rome (London: Thornton Butterworth, 1933),
pA,

11, Suetonius, Nero 5,

12, Weigall, Neo, p. 25.

13. Suetonius, Gaius 24,

14, Ibid.

45, Suetonius, Nero 6,

16, Suetonius, Nero 7.
