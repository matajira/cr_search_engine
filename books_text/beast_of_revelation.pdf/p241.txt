General Index 209

Vietim(s), 58, 128, 124, World, xxv, 42, 44, 63, 64, 71, 73, 103,
‘Victorinus, 149, 150-151, 166, 187. 122, 190, 159.

Vipers (Gee Snake). end of (See also: Eschatology), 44, 90,
Virgil, 13, 184,

Vision(s), 5, 40, 41, 85, 103, 105, 139, Wrath, 89,90.
141, 142, 151,152, 15.

Vitellius (emperor), 66, 72n, 108. Xipbilinus, 172.

War (See: Beast - war of; Rome - civil “Year of the Four Emperors,” 72, 74,
var; Jewish War), 186.

Weep (See als: Mourn), 95,

‘Whitefield Theological Seminary, 7. Zealot, 118,137.

Wisdom, 29, 102, 105-106. Zeus, 66,

Woman (See a/so: Harlot), 18, 106. Zonara, 169,172.

Word of God (See ate: Bible), 47,81.
