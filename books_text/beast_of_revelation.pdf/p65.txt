‘The Number of the Beret 31

theorder of the alphabet? The first nine letters represented the
values of 1-9; the tenth through nineteenth letters were used for
tens (10, 20, 30, etc.); the remaining letters represented values of
hundreds (100, 200,300, etc.).$

Due to this ancient phenomenon of the two-fold use of alpha-
bets, riddles employing numbers which concealed names were
common. This phenomenon is called a “cryptogram” by modern
scholars. Among the Greeks it was called isopsephia (“numerical
equality”); among the Jews it was called gimairiya (“mathemati-
cal”).* Any given name could be reduced to its numerical equiva-
lent by adding up the mathematical value of all of the letters of the
name.

Archaeologists have discovered many illustrations of crypto-
grams as graffiti on ancient city walls that have been excavated.
One example has been found in the excavations at Pompeii. There
the Greek inscription reads: “philo es arithmos phi mu epsilon” (“I
Jove her whose number is 545”). Zahn notes of this example that
“The name of the lover is concealed; the beloved will know it when
she recognises her name in the sum of the numerical value of the
8 letters phi mu epsilon, i.e., 545 (ph= 500 + m= 40 +e = 5). But
the passing stranger does not know in the very least who the
beloved is, nor does the 19th century investigator know which of
the many Greek feminine names she bore.”6

2, For Greek, see W. G, Rutherford, The First Grek Grammar (London Macmillan
1935), pp. 143% For Hebrew, see E. Kautzsch, ed., A. E. Cowley, trane., Gesenius’
Hebrew Grammar, 28h ed. (Oxford: Clarendon Press, 1946), p. 30.

8, For readily available evidence of these values in Hebrew and Greek, the reader
mnay consult the appropriate letters at their entries in Francis Brown, 8, R, Driver,
and Charles A, Briggs, eds.,A Hebrew and English Lexicon of the Old Testament (Oxford:
Clarendon Press, 1972) and G. Abbott-Smith, A Manual Greek Lexicon of the New
‘Tetoment (Edinburgh T. & T, Clark, 1950).

4, See Alfred Edersheim, Stetches of Jewish Socal Lif (Grand Rapids: Eerdmans,
[1876] 1972), pp. 289-290, Examples of Hebrew cryptograma can be found in the
ancient Jewish Talmud at Sentedrin 22a, Yoma 20a, and Nazir 5a,

5. Cited in Oskar Ruble, “Aritkmeo” in Gerhard Kittel, ed., Theological Dictionary
of the New Testament, trans, Geoffrey W. Bromiley, vol. 1 (Grand Rapids: Eerdmans,
1964), p. 462,
