Xaxiv The Beast of Revelation

over the long ha ul with those who are willing to forego present
consumption for : he sake of future growth.

Furthermore, dispensationalists insist, the beast is coming,
and so is the antichrist. That horror is just around the corner. The
Great Tribulation is imminent. Nothing can stop it. Nothing will
resist its onslaught. Nothing we leave behind as Christians will be
able to change things for the next generation. It is all hopeless. All
we can legitimately hope for is our escape into the heavens at the
rapture.

It is no wonder that American Christians have been short-run
thinkers in this century. They see failure and defeat in the immedi-
ate future, relieve J only (if at all) by the rapture of the church into
heaven. This is Dave Hunt’s message. He sees no earthly hope for
the church apart b-em the imminent return of Christ.

But such a view of the future has inescapable practical impli-
cations, although more and more self-professed dispensationalists
who have become Christian activists, and who have therefore also
become operatior al and psychological postmillennialists, prefer to
believe that these implications are not really inescapable. If the
“Church Age” is just about out of time, why should any sensible
Christian attend college? Why go to the expense of graduate
school? Why beccme a professional? Why start a Christian univer
sity or a new business? Why do anything for the kingdom of God
that involves a <apital commitment greater than door-to-door
evangelism? Why even build a new church?

Here, admitt :dly, all dispensational pastors become embar-
rassingly inconsistent. They want big church buildings. Perhaps
they can justify this “worldly orientation” by building it with a
mountain of long-term debt, just as Dallas Seminary financed its
expansion of the 1970’s, They are tempted to view the rapture as
a personal and institutional means of escape from bill-collection
agencies. A perscn who really believes in the imminent return of
Christ asks himself: Why avoid personal or corporate debt if
Christians are about to be raptured out of repayment? Why not
