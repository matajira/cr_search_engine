200

Domitian (emperor), ?, 64-66,69,69,82,
97, 102, 108, 198, 139,144, 147, 148,
151, 152, 155, 156-157, 158, 161, 162,
163, 164, 165, 166, 168, 173, 174, 176,
179, 181, 188.
aa a second Nero, 150.
persecution of (See Persecution —

Domitianic).

Drama, 22.

Dragon, 40,57.

Earth (See aso: Land; World), 26,68, 70,
71, 72, 74, 76, 77, 88, 90, 93-95, 98,
117, 188,

Earthquake, 176, 177.

Early-date (See: Date ¢ f Revelation).

Egypt, xxi, 118, 121, 1°7.

Hight, significance of,"6-7.

Fight hundred, eight -eight (See: Jesus
Christ - number off also: Numbers
— aymbolic use of).

Eighth head of Beast ‘Se alo: Kings —
eighth), 12, 68, 75, “6.

Elder(s), 124, 141, 154-155,

Emperor(a) (See als individual names),
14, 16, 16, 19, 82,13, 44, 46, 60, 53,
66, 67, 69, 61, 62, 53, 69, 72, 74, 75,
76, 104, 106, 107, 109-110, 145, 147,
158, 160, 169, 183, 186.

Emperor worship (See is: Beast -
worship of), 67-67, 71, 185,
principle argument br late-date,

168-168,

Empire, Roman (See Rome — empire).

Endurance (See: Perseverance).

Epaphroditua (Nerd’a secretary), 19,69,

Ephesus, city of (See eke: Seven
Churches), 58,64, ‘161, 179,

  

 
 

church in, 148n, 175-180.
John in, 146, 157, 1:8.
Epictetus, 42,

Epiphanies, 121, 145, 48, 187.

The Beast of Revelation

Episcopate (See also: Bishop),

Eschatology, ix, xiii, xvi, xxv, xxvii, xxdi,
zor, 3, 4, 25,86, 159, 183, 184.

Fesenes, 180,

Eusebius, 48, 49, 52n, 68, 99, 100, 107,
121, MO, 141, 149, 159-160, 168-165,
166, 187.

Trenaeus and, 151, 163, 164,
on banishment ofJohn, 147, 165,
on authorship of Revelation, 164-165.

Exthanos, 36.

Evangelical (See clso: Conservative or-
thodoxy), 5, 10, 18, 77, 121, 128, 167,
168,

Exile (See John the Apostle —
banishment).

Eye-witness, 27, 155, 156.

Faith, 4,47,50, 182, 160, 170, 179-180,

Fate, 65.

Fire/fiery, 18,51,58,72, 119, 127, 144.

Flaccus, Aulus Persius, 42.

Flavian, 74,

Form criticism (See Higher Criticism),

Forty-two months, 47, 48, 64, 66, 111,

112, 115, 120, 121, 185,

Fortune, 65,

Fragment hypothesis (St: Higher
Criticiam),

Future (Sve also: Eschatology; Prophecy),
ix, xxx, xy, 4, 61, 75, 86, 11, 116,
165, 182, 188, 188,

Gaius (emperor); (See: Caligula).

Gatba (emperor), 18, 72n, 73, 104, 108,
186,

Gamaliel I, 136.

Gaul, 18,72, 126,

Genius, 58,63.

Gentile(s), xxxi, 87, 115, 157, 161.

Geography, 12, 108, 113,

Gimatriya, 31.
