General Index

God, the True, xv, xvi, xxi, 3, 20, 22,
26, 40n, 43,45,47, 52, 61,81, 89,90,
91,92, 97,98, 100, 111, 112, 116, 117,
120, 128, 125, 196, 188, 160, 161, 177,
182, 188.
is time perspective, 24,25,

Gods or goddesses (See also individual
namea), 18, 51, 57, 58, 59, 60, 61, 62,
64,65,72, 127, 169.

Golden House, 64,

Gospel (message), xxxiii, 18, 146, 164,
179.

Gospel (book), 91,95, 115, 155.

Grammatico-historical (See: Interpreta-
tion).

Great Commission, 190,

Great Revolt (Se: Jewish War).

Great Tribulation (S: Tribulation).

Greece, 18,65,66.

Greek, xv, 22, 26n, 30,31, 34,36,37,40,
42, 60, 71, 76, 93, 94, 97, 103n, 104,
117, 120, 126, 182, 184, 141, 151, 152,
i.

Harlot, 5, 106, 113,

Har-Magedon (Sw: Armageddon),

Heads (S#: Beast — heads),

Heaven, xxii, xaiii, oviii, 26, 52,
86, 113, 117, 121, 160,

Hebrew, 30, 34, 35, 37-39, 132n, 134,

Hebrews (se: Jews).

Hegesippus, 117,

Herod the Great, 111, 112, 114, 118, 128,

Herodian, 42,

Hercules, 68,66,

Hermas (Se: Shepherd of Hermas).

High priest (Sw: Priest — high).

Higher criticiam, xii, 118, 167, 172.

Hippolytus, 9:

History, xvii , Xxvili, 00d, xxxii, 9,
18, 25, 30, 62, 70, 71, 82, 83, 85, 86,

  
 

201

104, 118, 115, 116, 119, 121, 142, 161,
168, 169, 175, 185, 186.
Christian view of, 26,90, 118, 188,
Church (Gee alse: Tradition), xii, xxvi,
5, 13,96,98,48,49>62,84, 85,85,
86,91-92,96, 116, 118, 121, 125,
185, 1388, 147, 498, 154, 158, 168,
164, 179, 186-187, 188,
Jewish, 27,38,94,96.
Roman, 19,57,66,69,72-76,96, 102ff,
174, 177.
Historians,
Church, 19, 27, 43-46, 60-61, 53-64,
55, 62, 71, 100, 140, 148, 154, 157,
170, 182.
Jewish, 27,43,62,72,74,107.
Roman, 13, 16, 32, 41-43, 46, 49-60,
§1-62,55,58,59,62,65, 66,71, 72,
74, 106, 107, 108-109, 122, 126, 181,
182, 158, 170.
Holy City (See: Jerusalem - Holy City).
Holy Spirit, 25-26,95, 157, 167.
Homosexual, 17,41,46,
Horace, 18,
Horns (Se: Beast - horns),
Horoscope, 15.
Human race (See also: Man/mankind),
27,42,45,61,60,117.

Wolatry, 10.

Ignatius, 26n, 40n, 92, 116, 135.

Images (Se: Idolatry; Beast - image).

Tmminence (S:: Revelation —
expectation),

India, 42,

Tnerrancy (See also: Bible), 85.

Infallible (S+ also: Bible), 185.

Inscription, 59, 145,

Inspiration, (Sz also: Bible), 84-85, 141,
198.

Interpretation (Sw alse: Revelation -
interpretation), xvi, xvii, xxi, 4, 6, 12,
