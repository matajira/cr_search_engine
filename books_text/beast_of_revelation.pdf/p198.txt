166 The Beast of Revelation

credence by late-date advocates. Irenaeus’s statement, the major
evidence by far, is grammatically ambiguous and easily susceptible
to a most reasona 2le re-interpretation, which would eliminate him
as a late-date witness. The evidence from Origen and Clement of
Alexandria, the second and third most significant witnesses to the
Dornitianic date, are more in the mind of the modern reader than
in the script of the ancient text. The important references from
both of these two fathers wholly lack the name “Domitian.” Vic-
torinus is a sure witness for the late date, but his requires incred-
ible implications. Eusebius and Jerome provide us with conflicting
testimony.
