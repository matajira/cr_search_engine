4
THI: CHARACTER OF THE BEAST

And the beast which I saw was like a leopard, and his feet were like
those of a bear, and his mouth like the mouth of a lion, And the dragon
gaue him his power and his throne and great authority (Rev. 13:1-2).

In Revelation 13 the one behind the 666 riddle is specifically
designated a “beast.” The word for “beast” in Greek is therion, a
term frequently used of “wild animals,” of “dangerous animals.”*
Therion ig often used of the wild, carnivorous animals employed in
the cruel Roman arenas.” Because of its natural association, the
term is often quite aptly used figuratively of persons with “a
‘bestia? nature, beast, monster.”

Not only is t1e name “Beast” employed by John in this pas-
sage, but he even symbolically represents this fearsome being with
horrible, beastly imagery. This Beast is a compound of such feared
and destructive carnivores as the leopard, bear, and lion. Almost
all commentators agree that this vision of the Beast is reflective of

1, William F, Am it and F, Wilbur Gingrich, A Greek-English la-icon of the Naw
Testament and Other Early Christian Literature (Chicago: University of Chicago Press,
1957), p. 361, In Lev, 266 the beasts of the land are symbolic of evil; in Lev, 2622
God promises their re :um to plague Israel and to bereave her of her children if she
ig unfaithful to the covenant. Messianic blessedness vanquishes the evil beaata (Isa.
116-9; Ezek, 34:25).

2. Josephus, The Wars of the Javs 7:38; Martyrdom of Polyearp 24; 3ff.5 11:18,
Ignatius, Romans 4:1ff; 53; Smpmaens 42; Diognetus 7:7; Hermas, Visions 3:2:1.

8, Amdt-Gingrich, Lexie, p. 361.

40
