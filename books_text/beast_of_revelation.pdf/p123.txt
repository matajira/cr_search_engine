The Thematic Evidence ot

27:2, 11, 12; Acts 3:18), and even called down His blood upon their
own heads (Matt. 27:24-25). John even tells us in his Gospel that
the Roman procurator, Pontius Pilate, sought to free Jesus, finding
no fault in Him (John 18:38; 19:12; ep. Acts 3:13). But the Jews
demanded that the robber Barabbas be released instead (John
18:39, 40) and that Christ be immediately crucified (John 19:6,
15). They even threatened Palate’s tenuous Roman procuratorship
by affirming “we have no king but Caesar” (John 19:14-15),
suggesting that Pilate was allowing Christ to supplant Caesar. And
Jesus Himself, during the course of these events, specifically painted
out to Pilate that “he who delivered Me up to you has the greater
sin” (John 19; 11), This should settle the matter of culpability, but
there is more - much more.

In Peter’s Pentecostal sermon at Jerusalem in Acts 2:22-23,
36 the blame is laid wholly on Israel: “Men of Israel, listen to these
words: Jesus the Nazarene .. . you nailed to a cross by the hands
of godless men and put Him to death. .. . Therefore let all the
house of Israel know for certain that God has made Him both
Lord and Christ — this Jesus whom you crucified.” He does the
same in his next sermon in Acts 3:13-15a “The God of Abraham,
Isaac, and Jacob, the God of our fathers, has glorified His servant
Jesus, the one whom you delivered up, and disowned in the pres-
ence of Pilate, when he had decided to release Him. But you
disowned the Holy and Righteous One, and asked for a murderer
to be granted to you, but put to death the Prince of life.” He
repeats this to the Jews in Acts 5:30 where he proclaims: “The God
of our fathers raised up Jesus, whom jou had put to death by
hanging Him on a cross.”

Stephen, in Acts 7:52, declares the same truth: that the Jews
were the “betrayers and murderers” of Christ. Paul concurs in 1
Thessalonians 2:14-15 when he speaks of “the Jews, who both killed
the Lord.Jesus and the prophets, and drove us out.”

This consistent and constant witness against the Jews in the
canon of the New Testament continues into post-apostolic Church
history. I will quote a few samples from the early fathers to
