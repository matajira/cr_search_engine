xvi The Beast of Revelation

His church out o! this world, literally speaking.!”

His view is exactly the same as that of House and Ice, who
make it plain that Christians are working the “night shift” in this
world. (And we all know how far removed from the seats of
influence all “night shift” people are!) They write: “The dawn is
the Second Coming of Christ, which is why he is called the
‘morning star’ (2 Peter 1:19), Our job on the ‘night shift is clarified
by Paul in Ephesians 5:1-14 when he says we are to expose evil
(bring it to light), not conquer it... .”@

The Right Hand of Glory

This anti-dominion perspective conveniently ignores the “pas-
sage of passages” that dispensationalist authors do their best to
avoid referring to, the Old Testament passage which is cited more
times in the New Testament than any other, Psalm 110, What few
church historians have recognized is that it was also the church
fathers’ most cited passage in the century after the fall of Jerusa-
lem." (Dispensat ionalists keep citing unnamed early church fa-
thers in general for support of their thesis that the early church
fathers were all premillennialists — an assertion disproved by one
of their own disciple .)?° Psalm 110 may be the dispensationalists’
least favorite Bible passage, for good reason.

The Lorp said unto my Lord, Sit thou at my right hand, until

17, For a Bible-based explanation of what “this world” means, see Greg L.
Bahnsen, “The Persor, Work, and Present Status of Satan,” Journal of Christian
Reconstruction, T (Winter 1974), pp. 20-30, See the extract I provide in my book, /
the World Running Dour? Crisis in the Christian Worldsiao (Tyler, Texas: Institute for
Christian Economics, 1388), pp, 220-22,

18, House and Ice, suminion Theology, p. 172,

19, David Hay, Glor: at th Right Hand (Nashville, Tennessee Abingdon, 1973),

20, In a 1977 Dana; Seminary Th.M. thesis, Alan Patrick Boyd concluded that
the early church fathe's were both amilJenniat and premillennial, and he rejected
then Dallas professor Charles Ryrie’s claim that the early church fathers were all
premillennialist, Boyd, “A Dispensational Premillennial Analysis of the Eschatology
of the Post-Apostolic 1 ‘athers (Until the Death of Justin Martyr) .“ Gary DeMar
summarizes Boyd’s fin lings in his book, The Debate Over Christian Reconstruction (Ft.
Worth, Texas: Dominion Press, 1988), pp. 96-98, 180n.
