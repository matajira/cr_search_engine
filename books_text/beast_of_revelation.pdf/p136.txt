104 The Beast of Revelation

and the present reign of the sixth one. It is also anticipatory of a
coming seventh king, who will immediately follow the sixth. Who-
ever these kings are, the sixth one is indisputably alive and in
control as John wries, while in exile for his faith (cp. Rev. 1:9).

Second, history documents for us that Nero was the siath emperor
of Rome. Nero reigied in Rome from October 13, A.D. 64, to June
9, A.D. 68 — well before the 4-D. 90s required by the late-date
theory. He was the sixth ruler to bear the name “Caesar.” The list
of the Caesars is as follows:

1, Julius Caesar (49-44 B.C.)

2, Augustus Ciaesar (81 B.C.-A.D. 14)

38. Tiberius Caesar (A.D. 14-37)

4, Gaius Caesar, also called “Caligula” (A.D. 37-41)
5. Claudius Caesar (A.D. 41-54)

6. Nero Caesar | A.D. 54-68)

Nero fits the bill perfectly in terms of the enumeration of the
emperors of Rome. But there is more.

Third, John says of the seventh king that he “has not yet come;
and when he comes, he must remain a little while” (Rev. 17:10).
In my enumeration this seventh king must be reckoned with. It is
a remarkable and indisputable fact of Roman imperial history that
upon Nero’s death by suicide in the summer of A.D. 68, the empire
was cast into a leadership turmoil and struggle. The next ruler to
appear after Nero was Galba. And Galba reigned only seven months!
His rule lasted from June, A.D. 68, to January 15, A.D. 69.

By almost any standard, Galba’s brief rule of seven months
was a “little while. ” The Greek term in Revelation 17:10 for “little”
is oligon. We get o 1c term “oligarchy” (which means “a rule by a
few”) from this word. When Galba’s rule is compared to any of
the preceding six emperors (see the listing above), it is obvious
that his is indisput ably the shortest imperial reign theretofore.

This evidence from Revelation 17 fits the reign of Nero, then,
