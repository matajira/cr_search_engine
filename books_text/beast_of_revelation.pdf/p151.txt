The Architectural Evidence 119

The Preservation of a Portion of the Temple

A major objection to our view is that Revelation seems to
indicate that the temple in view will be partially preserved, while
history shows clearly that the temple was leveled to the ground.
How are we to understand the commands for John to measure the
temple but not to measure its court? That is, how are we to
understand this partial preservation of the temple (that which is
“measured”) in Revelation if the first-century temple is meant?

The proper understanding of the passage requires that we rec-
ognize a mixture of the figurative and literal, the symbolic and.
historical. This is true in virtually every interpretive approach to
the passage, even the attempted alleged literalistic hermeneutic of
dispensationalism. This may be why dispensationalist John F.
Walvoord is prone to agree that “careful students of the book of
Revelation will probably agree with Alford that chapter 11 ‘is
undoubtedly one of the most difficult in the whole Apocalypse.”7

In preparation for commenting on Revelation 11, Walvoord
writes that “the guiding lines which govern the exposition to follow
regard this chapter as a legitimate prophetic utterance in which
the terms are taken normally.” By “normally” he means “literalis-
tically.” Interestingly, Walvoord is conspicuously silent on the
matter of John’s literally scaling the walls of the temple, with a
physical reed in hand, and his gathering the worshipers together
to measure them. And he fails to interpret verse 5 as demanding
that literal fire issue forth from the mouths of the two Witnesses."
Even fellow premillennialist (though non-dispensationalist) Robert
Mounce notes: “The measuring of the temple is a symbolic way
of declaring its preservation.” °

It is apparent that there is a mixture of the symbolic inter-

7. Walvoord, p. 176
8. bid.
9, Ibid, p, 180.
10. Robert Mounce, The Book of Reatin (Grand Rapids: Eerdmans, 1977), p.
219.
