6 The Beast of Revelation

fact that most of the popular literature on Revelation either ignores
the issue of datirg altogether or too lightly glosses over it.‘Even
the more technical modern commentaries on Revelation tend either
to appeal to a majority opinion on the matter or parrot the
insufficient evidence for the popular, but erroneous date of Revela-
tion. Yet the matter of Revelation’s date of composition is crucial
to the correct unclerstanding of the book.

Despite the majority opinion, I believe that two bold claims
regarding the date of the writing of Revelation can be made with
conviction,

The first is that a misapprehension of the date of its writing
can literally turn Revelation on its head, rendering its proper
exposition impossible. Whereas the problem of the style of Revela-
tion renders the exposition of its details difficuli, the adoption of a
wrong date for its writing renders its specific meaning impossible. If
Revelation prophesies events related to the destruction of the
Temple in Jerusalem in A.D. 70, then to hold to a date of composi-
tion after that event would miss John’s whole point. I firmly believe
this has been done by the majority of Revelation commentators in
this century.

My second claim is that Revelation provides more concrete
internal information pointing to its date of composition than does
any other New Tt stament book. As we will show, the major reason
for dating the book more than a quarter of a century too late is
due not to internal indications within Revelation itself, but to
church tradition. It is a most unfortunate state of affairs when the
self-witness of a b blical book takes the back seat to an inconsistent
tradition arising more than a century after its writing.

4, See Charles Caldwell Ryrie, Revelation (Chicago: Moody Press, 1968), pp. 7-8;
William Hendriksen, fore haa Conquers (Grand Rapids: Baker, 1967), pp. 19-20;
John F, Walvoord, Th: Revelation of Jerus Christ (Chicago: Moody Press, 1966), pp.
13-14; Hernan Hocksema, Behold He Comah! (Grand Rapide: Kregel, 1969), p. ’33.

5, See George Eldon Ladd, A Commentary on the Rezlatica of John (Grand Rapids:
Eerdmans, 1972), p. 8 G, R, Beasley-Murray, Tt Book of Rewiation (Grand Rapids:
Ferdmans, 1978), pp. 37-28; Alan F, Johnson, aviation (Grand Rapids Zondervan,

yD. 14,
