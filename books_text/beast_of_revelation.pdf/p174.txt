142 The Beast of Revelation

church” ( Vision 2:4:3). (8) In Vision 2:4:2ff. Hermas is told in his
vision to write twc books and to send one of them to Clement, who
in turn “was to send it to foreign cities, for this is his duty.” This
implies Clement's role as a subordinate secretarial figure. Yet, in
about A.D. 90 Clement was appointed Bishop of Rome.

Where Does This Leave Us?

If The Shephert! was written somewhere around A.D. 85 con-
sider the following: Certain allusions in it show its awareness of
Revelation. Thus, Revelation influenced the writing of The Shepherd
in the late A.D. 80s! Furthermore, The Shepherd was certainly
written somewhere around Rome, for it mentions Clement of
Rome (Vii-ion 2:4).

For John’s R:velation to have been written, to have been
copied (laboriousl y by hand), to have made its way to Rome by
the 80s, and to have influenced the writing of Tze Shepherd, would
be strong evidence that Revelation existed a good deal of time
before A.D. 85+. It would, thus, be evidence against a date of ca.
A.D. 95 and compatible with a pre-A.D. 70 date.

Papias of Hierapolis

Papias, Bishor of Hierapolis (A.D. 60-180), is reputed to have
been a disciple of the Apostle John and a friend of Polycarp. As
such he would be an extremely early and valuable witness to
historical matters of the sort with which we are dealing. Unfortu-
nately, none of his books is in existence today. There is, however,
an important piece of evidence purportedly from Papias that is
quite revealing. Late-date advocate H. B. Swete deals with this
evidence in his treatment of the Apostle John’s extreme longevity.
Swete notes that two ancient manuscripts, one from the seventh
century and one from the ninth, cite a statement by Papias which
says John the Apostle and his brother, James, were martyred by
the Jews. Of this statement Swete observes: “With this testimony
before us it is net easy to doubt that Papias made some such
statement... .B it ifPapias made it, the question remains whether
he made it under some misapprehension, or merely by way of
