The Relevance of the Beast 25

give careful, spiritual attention to his words (Rev. 2:7, 11, 17, 29;
3:6, 18, 22), John is deeply concerned with the expectant cry of the
martyrs and the divine promise of their soon vindication (Rev.
6:10; cp. Rev. 5:3-5). It would be a cruel mockery of their circum-
stances for John to tell them that when help comes it will come
with swiftness — even though it may not come until two or three
thousand years later. Or that the events are always immi-
nent — though the readers may never experience them. Or that
God. will send help soon — according to the way the Eternal God
Measures time,

Christ’s Comings

Perhaps one of the contextual matters that causes the most
confusion is that in several of the passages before us reference is
made to Christ’s “coming” (Rev. 2:16; 3:11; 22:7, 12, 20). “Behold,
Jam coming quickly” resounds in these verses. Surely we do not
believe the Second Advent came in the first century, do we?

Here is where a good deal of unnecessary confusion arises.
Actually there are a number of ways in which Christ “comes.” It
is true that He will come at the end of history, bringing about the
resurrection and the judgment (Acts 1:11; 1 Thess. 4. 13ff; 1 Cor.
15:20-26) ."' But Scripture also teaches that Christ comes to His
people in other ways.’? He comes to us personally in the Holy
Spirit (John 16:16, 18, 28),'° in fellowship by His presence in the

11, A view currently gaining popularity teaches that the totality of the Second
Advent of Christ occurred in the firat-century (bringing about the resurrection,
rapture, and judgment) and that history will continue forever, This view is not
supported by any creed or any council of the Church in history. All creeds and
councils that touch upon the subject of eschatology view history as coming to a final
conclusion. It should be noted that much of the literature promoting this view is fom
the anti-creedal sect of the Campbellites. See Max R. King, The Spirit of Prophesy
(published by author, 1971), pp. 100-102,124,261-262, ete,

12, For an excellent discussion of this, see Roderick Campbell, israel and tke New
Covenant (Tyler, TX. Geneva Mb-Aries, [1954] 1983), ch. 8,

18, It should be noted that the Greek word occurring in John 1618, 28 ig erctomsi,
wi mean “come.” It is also the word found in Rev, 1:7; 2:5, 16; 3:11; 1615; 22:7,
2 .
