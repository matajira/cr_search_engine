The Architectural Evidence U7

matter. In his Epistle to the Magnesians 10 we read: “It is absurd to
speak of Jesus Christ with the tongue, and to cherish in the mind
a Judaism which has now come to an end.” With the demise of
the temple, Judaism is incapable of worshiping in the manner
prescribed in the Law of God; it “has now come to an end.” This
is used by Ignatius to enhance the role of Christianity against that
of now defunct Bible-based Judaism.

dustin Martyr wrote his The First Apology of Justin about A.D.
147. In this work we read at Chapter 32: “For of all races of men
there are some who look for Him who was crucified in Judea, and
after whose crucifixion the land [i.e. Israel] was straightway sur-
rendered to you as spoil of war.” In chapter 53 he writes: “For
with what reason should we believe of a crucified man that He is
the first-born of the unbegotten God, and Himself will pass judg-
ment on the whole human race, unless we had found testimonies
concerning Him published before He came and was born as man,
and unless we saw that things had happened accordingly- the
devastation of the land of the Jews.”

In the fragments of the works of Melito of Sardis (written
about A.D. 160-180), we read of his words against the Jews: “Thou
smotest thy Lord: thou also hast been smitten upon the earth. And
thou indeed liest dead; but He is risen from the place of the dead,
and ascended to the height of heaven,”

Hegesippus, in the fragments of his Commentaries on the Acts,
writes (A.D. 170-175): “And so he suffered martyrdom; and they
buried him on the spot, and the pillar erected to his memory still
remains, close by the temple. This man was a true witness to both
Jews and Greeks that Jesus is the Christ. And shortly after that
Vespasian besieged Judaea, taking them captive.” He ties in the
persecution of Christ’s apostle James to the destruction of Jerusa-
lem,

Clearly, early Christianity made much of the fall of Jerusalem
and the Jews. Furthermore, where is there any reference to a
rebuilding of the temple in Revelation so that it could be again
destroyed (as per the dispensationalist argument)? If there is no
