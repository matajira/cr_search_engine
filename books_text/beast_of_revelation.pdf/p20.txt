xxii The Beast of Revelation

The years passed by. No beast. No antichrist. Few book sales.
Scrap the topic! Try something else. Why not books on nutrition?
Presto: Salem Kirban’s How Juices Restore Health Naturally (1980).
Oh, well. Better a glass of fresh carrot juice than another book on
the imminent appearance ofJesus or the antichrist.

Nevertheless, a stopped “clock of prophecy” is always good
news for the next wave of pop-dispensational authors: more chances
to write new books about the beast, 666, and the antichrist. There
are always more opportunities for a revival — a revival of book
royalties. After al, a sucker is born every minute, even when the
“clock of prophecy” has again ceased ticking. The next generation
of false prophets can always draw another few inches along the
baseline of their reprinted 1936 edition prophecy charts. They can
buy some new springs for a rusted prophetic clock. These stopped.
clocks are a glut on the market about every ten years. Any fledgling
prophecy expert can pick one up cheap. Clean it, install new
springs, wind it, make a few modifications in a discarded prophecy
chart, and you’re in business! Example: as soon as Salem Kirban
retired, Constance Cumbey appeared.

(I give little credence to the rumor that “Constance E, Cum-
bey” is the pen name adopted by Mr. Kirban in 1983. I algo have
real doubts about the rumor that the woman who claims to be
Mrs, Cumbey is in fact a professional actress hired by Mr. Kirban
to make occasional public appearances. Nevertheless, it is remark-
able that Mr. Kirban’s name appeared on no new books after
1982, the year before Mrs, Cumbey’s Hidden Dangers of the Rainbow
appeared. Could this be more than a coincidence? It is also strange
that “Mrs. Cumbey” seems to have disappeared from public view
ever since the second book with her name on it failed to make it
into Christian bookstores. Is it possible that “Mrs, Gumbey” was
fired by Mr. Kirban when the book royalties faded to a trickle and
there was no further demand for her public appearances? I realize

 

sella few books, Thus, t 1 dispensationalism known to most buyers of prophecy books
is the dispensationalisr of the ticking clock, however erratically it may tick.
