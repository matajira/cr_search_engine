The Ecclesiastical Evidence 133

this stage were argumentatively presenting themselves as the true
Jews. This must be at an early stage of Christian development
when Christianity still understood and presented itself as true
Judaism.

Revelation 7

This primitive conception of Christianity is strongly reaffirmed
later in Revelation. John speaks of Christians as the true Jews, the
fullness of the Twelve Tribes of Israel (Rev. 7:4-8; 14: If I’; 21: 12).
Revelation 7:4-8 is particularly instructive:

And I heard the number of those who were sealed, one hundred
and forty-four thousand sealed from every tribe of the sons of
Israel: from the tribe ofJudah, twelve thousand were sealed, from
the tribe of Reuben twelve thousand, from the tribe of Gad twelve
thousand, from the tribe of Asher twelve thousand, from the tribe
of Naphthali twelve thousand, from the tribe of Manassch twelve
thousand, from the tribe of Simeon twelve thousand, from the tribe
of Levi twelve thousand, from the tribe of Issachar twelve thou-
sand, from the tribe of Zebulun twelve thousand, from the tribe of
Joseph twelve thousand, from the tribe of Benjamin, twelve thou-
sand were sealed.

It is true that an element of symbolism is involved here. If
nothing else, the perfect rounding of numbers along with the exact
and identical count in each of the tribes speak of a symbolic
representation. Furthermore, the number “1000” is frequently
used in Scripture as an indefinite, yet significantly large, number
{Pss, 90:4; Dan. 7:10; 2 Pet. 3:8; Heb. 12:22).

Despite the obvious symbolism, however, the symbols must
be founded upon some historical designation. And, of course, the
“twelve tribes of Israel” is the long-standing historical configura-
tion of the Jewish race.'® In light of this, it would seem that two
possible interpretations easily lend themselves to consideration:

16. Cp. Mate. 19:28; Luke 22:30; Gal. 616; James 1:1; 1 Pet, 29.
16. See Gen, 35:22ff; 46:80; 49; Ex. 1:18; Num. 1; 2; 13:44;263* Dent, 27:1 1ff;
33.688; Josh. 13-22; Judges 5; 1 Chron, 2-8; 12:24; 27:16; Ezek. 48; Acts 267.
