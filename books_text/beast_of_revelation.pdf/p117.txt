The Importance of the Date of Revelation 85

however.

The reader should note that this part of the book is a conden-
sation and popularization of a fuller, more technical doctoral
dissertation. ‘In the larger work will be found much fuller exegeti-
cal and historical argumentation.

The Significance of the Issue

Tf the earlier date for Revelation be adopted, an interesting
result presents itself to the interpreted Most of the judgment
visions in Revelation (chs. 4-19) could easily be applied to the
historical turmoil which came to a head shortly after John wrote.
The fulfillment of the majority of its prophecies would then apply
to the very beginning of Christianity, rather than to its conclusion.
Contained in Revelation might be prophetic allusions to the first
Roman persecution of Christianity (A.D. 64-68), the Jewish War
with Rome (A.D. 67-70), the death of Christianity’s first persecu-
tor (Nero Caesar, d. A.D. 68), the Roman Civil Wars (AD.
68-69), and the destruction of Jerusalem and the temple (A.D.
10).5

Tf such were the case, then the fulfillment of many of Revela-
tion’s prophecies would be subject to documentation from history.
Furthermore, the book would then be intensely relevant to the
suffering churches to which John addressed it (Rev. 1:4, 11; chs.
2-3; 22: 16). Revelation’s initial purpose would have been to steel
infant Christianity against the tribulation into which it was enter.
ing (Rev. 1:9; cp. 2:10, 22; 3:10; 6:9-11). In addition, John would
also be explaining to the early Christians and to us the spiritual
and historical significance of the destruction of Jerusalem and the
temple and the demise of Judaism. Such a preparation of first-
century Christianity would be of immense practical and spiritual

4. See footnote 2 above,

5, Adherents to this view include David Chilton, The Days of Vengeance: An
Exposition of the Book of Revelation (Fort Worth, TX Dominion Press, 1987); Cornelius
‘Vanderwaal, Search dle Scriptures, trans. Theodore Plantinga, 10 vols. (St. Catherine,
Ontario; Paideia Press, 1978), vol. 10; and Philip Schatf, Hi-story of the Ciristian Church,
3rd ed, (Grand Rapids: Eerdmans, 1910), vol. 1,
