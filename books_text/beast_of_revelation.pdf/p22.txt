xxiv The Beast of Revelation

does not seem to deter any particular decade’s reigning paperback
prophets or their gullible disciples.

Once a particular prophecy expert’s predictions begin to be
perceived as being embarrassingly inaccurate, another expert ap-
pears with a new set of prophecies. Christians who become tempo-
rary followers oft hese false prophets become ominously similar to
the misled women described by Paul: “For of this sort are they
which creep into houses, and lead captive silly women laden with
sins, led away wi ch divers lusts, Ever learning, and never able to
come to the knowledge of the truth” (II Tim. 3:6-7). Eventually,
these frantic (or twill-seeking) victims become unsure about what
they should believe concerning the future. Everything sounds so
terrifying. Christi ins become persuaded that personal forces be-
yond their control or the church’s control — evil, demonic
forces — are about to overwhelm all remaining traces of righteous-
ness. How, after all, can the average Christian protect himself
against mind con trol and memory transfer, let alone head trans-
plants, assuming such things are both technically and culturally
possible and imminent? (The fact that such things are not techni-
cally possible in t 1¢ time period claimed for them never seems to
occur to the buyers of paperback prophecy books.)

A steady stream of this sort of material tends to reduce the
ability of Christians to reason coherently or make effective long-
terrn decisions. Sensationalism becomes almost addictive. Sensational-
ism combined with culture-retreating pietism paralyzed the funda-
mentalist movement until, in the late 1970’s, fundamentalism at
last began to change. That transformation is nowhere near com-
plete, but it surely has begun. Fundamentalists are at last begin-
ning to rethink th :ir eschatology. They are less subject to uncon-
trolled spasms pre duced by rapture fever. The back cover promo-
tional copy on V’katever Happened to Heaven? reveals that Dave
Hunt is aware of the fact that his version of pop-dispensationalism,
like Hal Lindsey’s, is fading rapidly. (Mr. Lindsey largely disap-
peared from public view about the time he married wife number
three. Gone are the days of his guest appearances — and everyone
