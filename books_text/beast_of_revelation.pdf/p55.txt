2
THE RELEVANCE OF THE BEAST

The Revelation ofJesus Christ, which God gave Him to show to His
beond-servants, the things which must shortly take place; and He sent
and communicated it by His angel to His bond-sertant John (Rev.
1:1).

One of the most important clues for the proper understanding
of Revelation is at the same time one of the most overlooked and
neglected. This clue is also a significant key for opening up to us
the identity of the Beast. We are speaking of the stated expectation
ofJohn in regard to the time of the fulfillment of the prophecies.

The truth of the matter is that John specifically states that the
prophecies of Revelation {a number of which had to do with the Beast) would
begin coming to pass within a very short period of time. He clearly says
that the events of Revelation were “shortly to take place” and that
“the time is near.” And as if to insure that we not miss the
point - which many commentators have! — he emphasizes this
truth in a variety of ways. Read the following passages and see if
you agree.

Emphasis on the Expectation
Fist, John emphasizes his anticipation of the soon occurrence
of his prophecy by strategic placement of the time references. He
places his boldest time statements in both the introduction and
conclusion to Revelation. It is remarkable that so many recent
commentators have missed it literally coming and going!

al
