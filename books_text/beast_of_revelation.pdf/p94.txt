60 The Beast of Revelation
dedicated by Tiberius in Rome to the Numen Augusti.!#

BeckWith notes that on his death the Senate voted Augustus
among the gods and that a temple was erected in the Palatine area
of Rome. Furthermore “his worship spread rapidly in both the
Asian and western provinces, so that the Jewish philosopher Philo
{ea. 20 B.C.-A.D. 50) could say, that ‘everywhere honors were
decreed to him equal to those of the Olympian gods.’”45

Archaeologists have in their possession a most interesting
decree of the Synod of the Province of Asia, which is dated about
9 B.C. This decree is preserved in a letter of the proconsul to the
cities of Asia:

[Whether the nz tal day of the most divine Caesar is to be observed.
most for the joy of it or for the profit of it - a day which one might
justly regard as equivalent to the beginning of all things, equiva-
lent, I say, if not in reality, at any rate in the benefits it has
brought, seeing that there was nothing ruinous or that had fallen
into a miserable appearance that he has not restored. He has given
another aspect t the universe, which was only too ready to perish,
had not Caesar — a blessing to the whole of mankind - been born.
For which reasoa each individual may justly look upon this day
as the beginning of his own life and physical being, because there
can be no more of the feeling that life is a burden, now that he has
been born. ...

Resolved by the Greeks of the province of Asia, on the proposal of
the High-priest Apollonius .. .: Whereas the Providence which
orders the whole human life has shown a special concern and zeal
and conferred u 30n life its most perfect ornament by bestowing
Augustus, whom it fitted for his beneficent work among mankind
by filling him wi th virtue, sending him as a Savior, for us and for
those who come after us, one who should cause wars to cease, who
should set all things in fair order, and whereas Caesar, when he

14, Scullard, Gracchi, p. 242,

15, Isbon T, Beckwith, The Apocalypse offohn: Studies in Introduction (Grand Rapids:
Baker, [1919] 1967), p. 199,

 
