The Historical Evidence (1) 143

expressing his conviction that the prophecy of Mc. x. 39 had found
a literal fulfillment. Neither explanation is very probable in view
of the early date of Papias. He does not, however, affirm that the
brothers suffered at the same time: the martyrdom ofJohn at the
hand of the Jews might have taken place at any date before the
last days of Jerusalem.”!*

If these two pieces of data are in fact from Papias (as Swete,
Lightfoot, and other competent scholars are inclined to believe),
they provide interesting evidence. For those who hold that John
wrote Revelation this would be strong external evidence for its
pre-A.D. 70 composition.

The Muratorian Canon

Sometime between A.D. 170 and 200 someone drew up a list
of canonical books. This list, known as the Muratorian Canon, is
“the oldest Latin church document of Rome, and of very great
importance for the history of the canon.” " The witness of this
manuscript, which is from the very era of Irenaeus and just prior
to Clement of Alexandria, virtually demands the early date for
Revelation. The relevant portion of the document states that “the
blessed Apostle Paul, following the rule of his predecessor John,
writes to no more than seven churches by name” and “John too,
indeed, in the Apocalypse, although he writes to only seven churches,
yet addresses all.”!

The writer of the Canon clearly teaches that John preceded
Paul in writing letters to seven churches. Yet, church historians
are agreed that Paul died before A.D. 70, either in A.D. 67 or 68.”
This is clearly taught by Clement of Rome (di. A.D. 100) in J

14, Swete, Revelation pp. clxxix-chact.

16. Schaff, History 1:76,

16, The seven churches addressed by Paul would be Rome, Corinth, Galatia,
Ephesus, Philippi, Colossae, and Thessalonica.

17. A. T. Robertson, “Paul, the Apostle” in James Orr, ed, The Intemational
Standard Bible Engolopedia (Grand Rapids: Eerdmans, [1929] 1956), 3:2287; Richard
Longenecker, The Ministry and Message of Pau! (Grand Rapids: Zondervan, 1971), p.
6,
