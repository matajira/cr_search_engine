Publisher’s Preface xi

ple is their silence regarding William Everett Bell’s 1967 New
York University doctoral dissertation, “A Critical Evaluation of
the Pretribulation Rapture Doctrine in Christian Eschatology,”
which has been reprinted by Bell. Major books deserve full-scale
refutations in books, not simply brief negative book reviews in the
in-house, small-circulation journal. Any philosophical, theological,
or ideological system that is not defended intellectually and pub-
licly by its academic spokesmen, decade after decade, despite a
growing mountain of cogent criticisms, is close to the end of its
influence. Its brighter, younger recruits will drift away or else be
recruited by the critics. Eventually, the defending institutions will
drift theologically, as formerly dispensational Talbot Theological
Seminary did after 1986. A defensive mentality, a “form a circle
with the wagons” mentality, cannot be sustained forever. If a
movement does not move forward, it either stagnates or moves
backward culturally. If a movement adopts a view of time which
says that cultural progress in term’s of the movement’s worldview
is unsustainable, and that only “upward” movement or “inward”
movenent is truly significant, then the movement has drunk the
eschatological equivalent of Jim Jones’ Kool-Aid. This analytic
principle applies equally well to the New Age mystic’s quest for
inner escape or the dispensationalist’s rapture fever. This is why
dispensationalism is dying.
Bible-believing Christians need an alternative

The Last Days

This book is about the last days. It is not about the end times.
The last days are different from the end times. The last days are
not in the present or in the future; they are in the past. Still
confused? So are millions of other Christians. The confusion stems
from the fact that Christians have jumped to the conclusion — a
wholly erroneous conclusion — that the “last days” spoken of in
the New Testament refer to the last days of the church (or to the

8. Gary North, Uncondi tionat Surrender: God’s Program j?? Victory (8rd cd.; Tyler,
‘Texas: Institute for Christian Economics, 1988),
