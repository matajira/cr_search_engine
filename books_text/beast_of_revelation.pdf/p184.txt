152 The Beast of Revelation

to reduce the usefulness of Irenaeus for late-date advocacy. Three
of these will be brought forward?

First, the trar slational problem. The most important matter
facing us is the proper translation of Irenaeus’s statement. The
phrase “that was seen” or “it was seen” (as it is often translated)
is the crucial matter in this statement. This statement is commonly
considered to refer back to the immediately preceding noun from
the preceding sentence, which means either: “apocalyptic vision”
or “Revelation.” Fut as John A. T. Robinson has observed regard-
ing the commonly accepted translation: “This translation has been.
disputed by a number of scholars.”!® Compounding this problem
are several contextual matters and a certain internal confusion in
Irenaeus regarding the incompatibility of his statements on Reve-
lation.

Second, the subject of Aeorathe. Indisputably, the most serious
potential objectio 1 to the common translation has to do with the
understanding of the Greek verb Acorathe, “was seen.” What is the
subject of this verb? Is it “he who saw the Revelation” (ie., John)
or “Revelation” itself? Either one will work grammatically because
Greek is an inflected language that has no need of separate pro-
nouns (although it does have them). The verb endings often serve
in lieu of a prono 1n and with an implied subject. The verb before
us is found in the third person singular form. Considered alone
and divorced from its context, it may be translated either “it was
seen” or “he was seen.” Hence the reason for our inquiry. Which
of the two antec sdents — “he who saw” (i¢., John) or “Revela-
tion” — “was seen” almost in Irenaeus’s time and near the end of
the reign of Domitian?

Let us paraphrase the possible translations of this statement
in order to clarify our question. Did Irenaeus mean: “the Revelation
was seen in a vision by John almost in our own generation”? This

9, Other problem: are discussed in my Byfire Jerusalem Fell: Dating the Book of
Revelation (Tyler, TX: 1 nstivate for Chriatian Economies, 1989).

10, John A, T, Ro2inson, Redating the New Testament (Philadelphia Westminster
Press, 1976), p. 221,
