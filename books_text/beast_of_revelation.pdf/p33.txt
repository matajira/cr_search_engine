Publisher's Preface Xx

adopt the outlook of “eat, drink, and be merry, for tomorrow we
will be rescued by God’s helicopter escape”?

The Helicopter Man

Dave Hunt does not want to become known as “Helicopter
Hunt,” but that really is who he is. His worldview is the funda-
mentalists’ worldview during the past century, and especially since
the Scopes “monkey trial” of 1925,”but its popularity is fading
fast, just as the back cover copy of his book frankly admits. No
wonder. Christians today are sick and tired of riding in the back
of humanism’s bus. They are fed up with being regarded as
third-class citizens, irrelevant to the modern world. They are
beginning to perceive that their shortened view of time is what has
helped to make them culturally irrelevant.

The older generation of American fundamentalists is still being
thrilled and chilled in fits of rapture fever, but not so much the
younger generation. Younger fundamentalists are now beginning
to recognize a long-ignored biblical truth: the future of this world
belongs to the church of Jesus Christ if His people remain faithful to His
Word. They are beginning to understand Jesus’ words of victory
in Matthew 28; “And Jesus came and spake unto them, saying,
All power is given unto me in heaven and in earth. Go ye therefore,
and teach all nations, baptizing them in the name of the Father,
and of the Son, and of the Holy Ghost. Teaching them to observe
all things whatsoever I have commanded you: and, 10, I am with
you alway, even unto the end of the world. Amen” (w. 18-20).
They have at last begun to take seriously the promised victory of
the church’s Great Commission rather than the past horror of
Israel’s Great Tribulation. They are steadily abandoning that
older eschatology of corporate defeat and heavenly rescue.

In short, Christians are at long last beginning to view Jesus
Christ as the Lord of all history and the head of His progressively
triumphant church rather than as “Captain Jesus and His angels.”

30. George Marsden, Fizidamentatisom and American Culture: The Shaping of Twenticth-
Centary Brangrlitalism, 1870-1925 (New York Oxford University Press, 1980).
