2 The Beret of Revelation

high station in Rome.!®

Roman histovian Tacitus (A.D. 55-117) spoke of Nero’s “cruel
nature” that “put to death so many innocent men.” Roman
naturalist Pliny :he Elder (A-D. 23-79) described Nero as “the
destroyer of the human race” and “the poison of the world.”?
Roman satirist Juvenal (A.D. 60-140) speaks of “Nero’s cruel and
bloody tyranny.”8 Elsewhere he calls Nero a “cruel tyrant.”
Nero go affected the imagination that the pagan writer Apollonius
of Tyana (ea. 4 13, C-A.D. 96) specifically mentions that Nero was
called a “beast”: “In my iravels, which have been wider than ever
man yet accomplished, I have seen many, many wild beasts of
Arabia and Ind a; but this beast, that is commonly called a
Tyrant, I know not how many heads it has, nor if it be crooked of
claw, and armed with horrible frogs. . .. And of wild beasts you
cannot say that hey were ever known to eat their own mother,
but Nero has gorged himself on this diet.”

Among the ancient pagan written traditions exhibiting a ha-
tred and mocker{ of Nero are those by such Roman and Greek
writers as: Suetorius, the writer of The Octavia, Pliny the Younger,
Martial, Statius, Marcus Aurelius, Aulus Persius Flaccus, Vulca-
cius, Epictetus, Marcus Annaeus Lucan, and Herodian. '® Nero
scholar Miriam T. Griffin analyzes the presentation of Nero in the
ancient tragedy 7 te Octavia: “Nero is, in fact, the proverbial tyrant,
robbed of any personal characteristics, a mere incarnation of the

10. ibid. 33-35. See also Die, Roman History 61:1:2; Ascension of Isaiah 41; Sibyline
Oracles 5:20; 12:82,

LL. Histories 47; 48.

12 Pliny, Natural History 7:45; 2292,

18, Juvenal, Satire 7225.

14, Satire}0:3068

15, Philostratus, Lif} of Apollonius 438,

16, Suetonius, Domition 14; Th Octaoia; Pliny the Younger, Panegyricis 53; Juvenal,
4:38; Martial, Epigrom 7:21; 21:33 Statius, Silue 2:7; Marcus Aurelius, 3:16 Aulus
Persius Flaccus in Suetonius’s On Poets — Aulus Persius Flacus; Vulcacius, Life of
Cassius 8:4; Capitolinus 28:10; Epictetus, 4:5,17; Marcus Annaeus Lucan in Sue
tonius’s On Poets - Lacon; Herodian, 3:4, Historiat Augusta; Martial, Book of Spectactes 2,
