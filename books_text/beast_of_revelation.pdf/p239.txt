General Index

penology, 48, 90n,

people of, 42, 62, 58,59, 66, 72, 107,
126, 186, 156-157,

Senate, 19,45,57,69,60,61,66.

senator(a), 41,42,,

Sacrifice(s), 51,58,59,64,65,66,123,
cessation of Jewish, xiv, xv, xvi, 200i,
2.
for Rome,
Saint(s), 47, 60, 62, 64, 66, 68, 98-99,
188.
Sardis, 179-18.
Satan (See alv: Devil), xxviii, xxix, 11,
38, 129, 131, 182.
Savior, 58,60,64,
Scripture (See: Bible).
Seal(s) (noun), 146.
Seal (verb), 97-09,188,182,
Sebastos (sebastenoi), 64, 169.
Second Advent/Coming (Sse: Christ -
coming) .
Seer (Sa: Prophet).
Semitic (See Hebrew language).
Senate of Rome (See: Rome - senate).
Seneca, 16, 17,68, 109.
Septimontinm, 13.
Septuagint, 93.
Serpent (Sz: Snake).
Servant(s), 21,22,43,98, 138,169.
Seven (number), 22,38.
churches (See aie: individual entriea),
10, 13, 22, 24, 86, 191, 143, 176
180,
heads (Se: Beast - heads).
Kinga (See: Kinga).
letters, 38,61,89, 144, 178, 180.
mountains, 12, 18, 102K.
Severus, Sulpicius, 44, 49, 62, 109, 136,
160-161,172,
‘Shepherd of Hermas, 40n, 139-14.

207

aa early-date evidence, 199-142, 147,
187,
ae scripture, 140-141,

“shortly,* 21,22, 28>77,85, 101.

Sibpitine Oracles, 13,32, 42n, 43, 107, 109,
122, 158, 172,

Sinaiticus (See: Codex Siniaticus).

Six hundred, sixteen See also: Numbera
— symbolic use of}, 36,59.

Six hundred, sixty-six (See also: Numbers
- symbolic use of), 9, 11,29-39,40.
interpretation (alleged impossibility),

11,37.
interpretation (history of, 9, 11, 87.
interpretation (principles of), 9-11,32-
8

Trenaeus on, 36-87, 151, 153, 155.
Nero theory of, 19,29-39,70,184,
Nero theory objections to, 36-89, 150

Sixth head of beast (See also: Kings -
sixth), 78, 185.

Slaughter, 27.

Slave(ry) (See& servant), 27, 59n, 65,
141,

‘Smyma (city of}, 61, 178.
church in, 181, 182, 178-179,

Snake(s), 45.

Social, 29,68, 188.

Sodom, 118,121.

Sodomy (See: Homosexual).

“Son of perdition,” 45

“Soon,” (See a/v: Revelation - expecta-
tion), 10,21,28,28, 184, 186,

Spain, 18.

Spirit (See Holy Spirit).

Sporus, 17,41.

Stative, 13,

Stephen, 91,

Stoning, punishment by, 90,95.

Suetonius, 16, 17, 18, 19, 92,4142, 46,
49,55,58,59,62,69,71, 73, 74, 106,
