The Historical Evidence (1) 7

involved was not yet inflicted by the Remans,”2”

In his comments on Revelation 1:9, Arethas says: “John was
banished to the isle of Patmos under Domitian, Eusebius alleges
in his ChronicOn. “3° Clearly Arethas is not satisfied with what
Eusebius “alleges.” This is all the more evident in his comments
on various passages in Revelation.

Theophylact

A much later witness is Theophylact of Bulgaria, a noted
Byzantine exegete (d. 1107). He also gives evidence of a dual
tradition on John’s banishment. A. 8. Peake observed in this
regard: “Theophylact also puts it /i.e., Revelation] under Trajan,
but elsewhere gives a date which would bring it into the time of
Nero.”8! In his Preface to Commentary on the Gospel of John, Theophy-
lact puts the banishment of John under Nero when he says John
was banished thirty-two years after the ascension of Christ. In his
commentary on Matthew 20:22 he mentions John’s banishment
under Trajan, the second emperor after Domitian!

Conclusion

The above survey shows that early church tradition was not
uniformly set against the early date of Revelation, as some have
implied. Indeed, when carefully scrutinized, the evidence even tilts
in the opposite direction. Thus, Guthrie’s statement does not
appear to be well founded: “It would be strange, if the book really
was produced at the end of Nero’s reign, that so strong a tradition
arose associating it with Domitian’s.”=

There are some early witnesses that strongly hint at a pre-A.D.
70 dating for Revelation, such as The Shepherd of Hermas and Papias.
Other sources are even more suggestive of a Neronic banishment

29. Cited in P. S. Desprez, Me Apocalypse Fulfilled, 2nd ed. (London Longman,
Brown, Green, Longmans, 1855), D. 7.

30. Stuart, Apocalypse 1:268,

31. Peake, Revelation, p. 77.

82. Guthrie, Introduction, D. 957.
