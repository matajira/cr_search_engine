82 The Beast of Revelation

argues that John composed his work around A.D. 95-96, in the
last days of the principate of Domitian Caesar, who was assassi-
nated September |8, A.D. 96.

My Position

The position |: will set forth in the following pages is that of
an early date prior to A.D. 70,1 somewhere in the time-frame of
late-A.D, 64 (after the initial outbreak of the Neronic persecution)
to A.D. 67 (prior to the Jewish War with Rome). For too long,
popular commentaries have brushed aside the evidence for the
early date for Rex elation. Despite the majority opinion of current
scholarship, the evidence for an early date for Revelation is clear
and compelling.

Almost invariably the major reason for the dismissal of the
early date for Revelation is due to one statement by an early
church father naried Irenaeus. Other supportive evidences for a
late-date are brought into the discussion later. initially, however,
almost all commentators begin with and depend upon Irenaeus’s
statement in his lute second century worked entitled Against Here-
sies? Pick a Revelation commentary off your shelves and see for
yourself.

There is one particularly frustrating aspect of the debate for
early date advocates. When one mentions that he affirms a date
for Revelation prior to the destruction of the temple, a predictable
response all too often heard is: “Aren’t you aware that all scholars
agree it was written at the end of the first century?” Or, if talking
with a seminarian, the reply might be: “Don’t you realize Irenaeus
clearly settled this question?” In such encounters the early date
proponent is deerned intellectually naive and historically misin-
formed. He is thought to be throwing objective evidence and
assured conclusioris out the window on the basis of sheer presump-

1, I would like to thank Dr, George W. Knight III for his suggestions on the
following order of arrangement, which difer from my dissertation’s order.

2, One notable exception ia Leon Morna in his The Revelation of St. John (Grand
Rapids; Eerdmans, 1969),
