154 The Beast of Revelation
Trenaeus’s statement, this time with more of the context:

These things were said by the writer [iz., Irenaeus] referred to in
the third book o! his treatise which has been quoted before, and in
the fifth book he discourses thus about the Apocalypse ofJohn and
the number oft 1¢ name of the Antichrist. “Now since this is so,
and since this number is found in all the good and ancient copies,
and since those 1 vho have seen John face to face testify, and reason
teaches us that the number of the name of the beast appears
according to the numeration of the Greeks by the letters in it... .
[Heresies 5:30:1]” And going on later [Heresies 5:30:3] he says
concerning the same point, “We therefore will not take the risk of
making any pos tive statement concerning the name of the Anti-
christ. For if it had been necessary for his name to have been
announced clear ly at the present time, it would have been spoken
by him who also saw the Revelation; for it was not even seen a
long time ago, but almost in our own generation towards the end
of the reign of Di smitian.” ®

Notice should be made of the personal knowledge which is
emphasized by Irenaeus. It seems clear that the verb “was seen”
is but the dim reflection of his preceding statement’s more expan-
sive and precise statement: “those who have seen John face to face
testify.” In fact, the very same Greek verb (/eorathe, “seen”) is used.
in both statement 3! Surely it speaks ofJohn in both instances,

Fifth, the intent of Irenaeus. Still further, the proposed re-
interpretation of [renaeus is characteristic of Irenaeus's thought.
By this I mean that Jrenaeus constantly emphasizes the organic and tving
unity of the Church’s life. According to church historian Philip Schaff,
Trenaeus’s work sought to demonstrate that “the same gospel
which was first orally preached and transmitted was subsequently
committed to writing and faithfully preserved in all the apostolic
churches through the regular succession of the bishops and el-
ders.”8 This being the case, the most natural interpretation of

12, Translation by Lake.
18. Philip Schaff, History off the Christian Church, 8rd cd. 7 vols. (Grand Rapids:
Eerdmana, [1910] rep. 1950) 2:733.
