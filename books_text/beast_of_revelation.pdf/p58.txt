24 The Beast of Revelation

of the New Testament concur.’ This is surely the proper transla-
tion of the verse.

The Obvious Ie Painful

Unfortunately, right in the very first verse of Revelation certain
commentators begin straining to reinterpret the obvious. There
are various maneuvers used to get around this and the other terms:
Some understand these terms as indicating that whenever the
events do start coming to pass, they will occur with great speed,
following one up »n the other with great rapidity.? Others view
them as indicating that such events as John prophesied are always
imminent.? That is, the events are always ready to occur, though
they may not act ually occur until thousands of years later. Still
others see John’s references as a measure of God’s time, not
man’s." That is, ‘ohn is saying that these events will come to pass
“shortly” from God’s perspective. But, then, we must remember that
“a day with the Lord is as a thousand years” (2 Pet. 3:8).

Each of these approaches is destroyed by the very fact that
John repeats and varies his terms as if to dispel any confusion.
Think of it: If these words in these verses do not indicate that John
expected the even ts to occur soon, what words could John have used to
express such? How could he have said it more plainly?

Another detri nent to the strained interpretations listed above
is that John is writing to historical churches existing in his own
day (Rev. 1:4, 11; 2-8). He and they have already entered the
earliest stages of “the tribulation” (Rev. 1 :9a). John’s message
{ultimately from Christ, Rev. 1:1; 2:1; 22:16) calls upon each to

7. George Ricker Berry, The Interlincar Greek-Engltth New Testement (Grand Rapids:
Zondervan, rep. 1961 }, p. 625; Alfred Marshall, The Interlinear Greek-English New
Tistoment, 2nd ed, (Gre nd Rapids: Zondervan, 1959), p. 959; Jay P, Green, Sr., The
Jnierivecr Bible, 2nd ed, (Grand Rapids: Baker, 1983), p, 927.

8. John F., Walvoord, The Recelation of Jesus Christ (Chicago: Moody Press, 1966),
p. 35.

9. Robert H. Mou 1ce, The Book of Rela (Grand Rapids: Eerdmans, 1977),
p. 6&5.

10, Leon Morria, Th: Ravdation of St join (Grand Rapids: EFerdmana, 1969), p. 45,
