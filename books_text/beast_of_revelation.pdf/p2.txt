Other books by Kenneth L. Gentry, Jr.

The Christian Case Against Abortion, 1982, rev. 1989
The Charismatic Git of Prophecy: A Reformed Response to Wayne Grudem,
1986, rev. 1989
Ti Christian and Alcoholic Beverages: A Biblical Perspective, 1986, rev. 1989
Before Jerusalem Fed: Dating the Book of Revelation, 1989
House Divided: The Break-up of Dispensational Theology
{with Greg. L. Bahnsen), 1989
The Greatness of the Great Commission:
The Christian En'erprise in a Fallen World, 1993
He Shall Have Dominion: A Postmillennial Eschatology, 1992
Lord of ite Saved: Getting to the Heart of the Lordship Debate, 1992
Gods Law in the Modern World: The Continuing Relevance of Old
Testament Law, 1993

Contributions to:

David L. Bender, cd., The Welfare State: Opposing Viewpoints, 1982.
Gary North, cd., Theonomy: An Informed Response, 1992
