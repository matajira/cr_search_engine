276 Healer of the Nations

Comprehensive Responsibility

Fangless snakes are fangless because they preach a fangless
theology of world-retreat and institutional surrender. They refuse
to recognize the power of God’s law and the power of the Holy
Spirit. Lacking confidence in God’s covenant, they flee the very
idea of Christian responsibility outside the walls of home and the
local church. They create a vacuum of responsibility, and human-
ists gratefully rush in to fill it, usually with tax dollars confiscated
by the Christian majority, who dutifully re-elected politicians who
use the modern principle of “theft by ballot box.”!

The issue here is commitment to the gospel —Christ’s compre-
hensive gospel.2 This gospel calls all people to repentance. It then
calls them to redeem their time by working to work out the impli-
cations of their Christian faith with fear and trembling (Philip-
pians 2:12). This gospel influences every area of life, All the world
is in sin; all the world is in darkness. The gospel shines into every
area of darkness. It illuminates everything.

“I have come as a light into the world, that whoever believes in
Me should not abide in darkness” (John 12:46).

No more darkness— political darkness, economic darkness,
educational darkness, or any other form of darkness. This re-
quires comprehensive light. This is why Christian reconstruction
of every human institution is mandated by God. Christians dare
not flee their comprehensive responsibilities:

“You are the salt of the earth; but if the salt loses its favor, how
shall it be seasoned? It is then good for nothing but to be thrown
out and trampled underfoot by men. You are the light of the
world. A city that is set on a hill cannot be hidden. Nor do they
light a lamp and put it under a basket, but on a lampstand, and it

1. Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth,
Texas: Dominion Press, 1987), ch. 3.

2, Gary North, Is the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1987), Appendix C: “Compre-
hensive Redemption: A Theology for Social Action.”
