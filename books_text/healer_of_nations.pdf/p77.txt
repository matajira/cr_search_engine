All Nations Under God 63

huge propaganda effort would be necessary to achieve this goal.
Professor Carrol! Quigley comments: “That the chief obstacle to
this union was to be found in men’s minds was perfectly clear to
Curtis. To overcome this obstacle, he put his faith in propaganda,
and the chief instruments of that propaganda, he said, must be
the churches and the universities.”* The main book promoting
this vision was Clarence Streit’s Union Now (1939), a former
Rhodes Scholar.

My vision is similar, yet radically different. To stifle all plans
at creating a humanist world government, we need to starve the
beast. We need to limit its sovereignty. The answer to the sover-
eignty of a central world empire is not a system of autonomous
nations that face the onslaught of a concerted Communist empire;
the answer is a bottom-up covenanted federation that is openly
Christian in its public confession, but which does not allow the
higher levels of civil government to tax people directly.

The other safeguard against the tyranny of empire is the right
of secession. This principle is not honored in our era of central-
ized nationalism, but Jeroboam’s example is valid. While seces-
sion is a disrupting event, to be avoided as much as ecclesiastical
schism, unjustified tyrannical taxation is also disrupting, as
Rehoboam’s legacy indicates. The right of lower magistrates to
rebel against higher magistrates is basic to Christian liberty. The
top-down bureaucratic hierarchy, except during a formally
declared shooting war, is satanic.

Humanist Internationalism

In international relations, the humanists’ version of God’s
universal kingdom is either the creation of an empire or else the
creation of some sort of an alliance system that has many of the
markings of empire. We sometimes call this internationalism.
Humanism’s internationalism is based theoretically on the shared

18. Carroll Quigley, The Anglo-American Establishment (New York: Books in
Focus, 1981), p. 283. The manuscript was finished in 1949, but was not published
in the author's lifetime.

19. John Calvin, Institutes of the Christian Religion, Book IV, Chapter 20.
