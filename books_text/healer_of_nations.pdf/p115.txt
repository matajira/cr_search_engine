Nations With Rival Covenants Are Always at War 101

This imitation is legitimate, if done covenantally, meaning as
a lawful representative of God. Men are required to think God’s
thoughts after Him, though as creatures. They are to image God.
Thus, civil governments must impose sanctions. This is one of the
functions of all three covenantal governments: church, civil gov-
ernment, and family, The question is: Do they impose sanctions
as hypothetically autonomous (self-law) agents or as delegated
sovereigns of God? Another question is this: Under what condi-
tions may they legitimately seek to impose military sanctions on
other nations, as Israel imposed sanctions on Canaan? These
questions are more appropriate for a Biblical Blueprint on war,
but it is clear that national self-defense is legitimate in New Testa-
ment times. God establishes certain nations to serve as headquar-
ters for international misssions at certain points in history, and
Christians living in these nations may legitimately call for military
action to defend these bases from attack.

What is the proper response of nations that are threatened by
invasion or other coercive pressures from abroad? When one na-
tion threatens another, or many others, what should the potential
victims do? The typical response is to beat plowshares into
swords. Victimized nations either prepare for war or else they
pretend that the threat to their independence will go away.

We are presently living in the midst of perhaps the major con-
frontation between Christ and Satan since Calvary. When the for-
mer Communist spy Whittaker Chambers defected from the
Communist Party in the late 1940’s and went before Congress to
confess and to identify several networks of Communist spies in the
U.S. government, he wrote a letter to his children explaining why
he had done what he had done. It is included as the Introduction
to his magnificent 800-page testimonial, Witness (1952). His words
should ring in the ears of every Christian: “For in this century,
within the next decades, will be decided for generations whether
all mankind is to become Communist, whether the whole world is
to become free, or whether, in the struggle, civilization as we
know it is to be completely destroyed or completely changed. It is
