268 Healer of the Nations

tying a person’s hands behind his back, putting a tire soaked in
diesel fuel around his neck, and setting it on fire. The Marxist
African National Congress revolutionaries are using this tech-
nique against Christian blacks in South Africa. In January of
1987, U.S. Secretary of State George Shultz had a much-
publicized meeting with ANC president Oliver Tambo. Every-
thing was cordial. Christians might consider sending used tires to
George Shultz as a symbolic gesture.)

Christian parents in the United States are being sent to jail for
teaching their children at home. Their children are being sent to
foster homes.

Ghristians in Cuba live under the tyrant who was trained in
Catholic schools, and who turned against the Church—a familiar
pattern among humanists, from Judas to Rousseau to Castro,

No one is safe. The Communist noose is getting tighter. Yet
the Church’s main stronghold, the United States, is sleepwalking.
It thinks of all this as distant. It sits, hypnotized, in front of the
flickering colored shadows of the television, watching reruns for
haif the year—reruns of shows that were not worth watching the
first time. The vast majority of their children are in humanist-
controlled government schools. (See Robert Thoburn’s book in
the Biblical Blueprints Series, The Children Trap.) They do not ex-
ercise the right that millions of Soviet Christians would give what
little they own in order to possess: the right to send their children
to a Christian school.

Until the churches begin to think of themselves as members of
the institutional, international body of Christ, little will be done.

But signs of positive change are taking place. A growing minority
of Christians are cooperating in a battle against abortion. They are
setting up Christian schools. They are getting involved in politics
as Christians. They are beginning to reject the myth of neutrality,
which is the first step in returning to Biblical law as the only God-
ordained standard of righteous action. All this is tentative. These
are the first steps of cultural toddlers. But even these few uncertain
steps have frightened the humanists, who correctly suspect that
they are on their last legs culturally, doddering rather than toddling.
