God's Legacy of Progressive Peace 131

their conspicuousness. It is not only bad form, it is bad diplom-
acy, for them to win ‘victories’ Such ‘triumphs’ are hollow, for they
are bound to injure domestically the government with which the
diplomat has to deal, and make future relationships and negotia-
tions more difficult, needlessly.”>

Contrast this view of a diplomat with Jesus’ Great Commis-
sion. Jesus gave the job of kingdom ambassador to the disciples.
He was and is God's representative to mankind on earth and in
heaven. His disciples are under His discipline, and are responsi-
ble to their Commander-in-Chief.

The same should be true of a nation’s international am-
bassadors. They owe allegiance to the nation’s political head.
They should be hired and fired based on the decision of political
authorities, not on the basis of their years inside a closed bureau-
cracy. Any interference with this system of personal accountabil-
ity disrupts the chain of command (in this case the authority of the
voting public) and eventually creates disastrous situations. There
must be fterarchical accountability. There must be an unbroken
chain of command in government, whether in Church or State.

An ambassador should be polite. He should be diplomatic.
But he should not be a professional diplomat. The ambassador
should owe his job directly to the Prime Minister or President. He
must be fully accountable to a political representative of the vot-
ers. Today, this is seldom the case. The diplomat is protected by
Civil Service, or by the “old boy network,” or by other formal re-
strictions against his being fired at will, so he cannot be fired by a
political representative except for gross malfeasance in office. Un-
til this is changed, there is no hope to restore sanity to foreign pol-
icy. An ambassador must represent the political representatives of
the people. A diplomat represents the professional foreign policy
establishment.

The problem with Western foreign policy is that it is con-
ducted by diplomats for the sake of those elitists who benefit from

5. Henry M. Wriston, Diplomacy in a Demacracy (New York: Harper & Brothers,
1956), pp. 3-4.
