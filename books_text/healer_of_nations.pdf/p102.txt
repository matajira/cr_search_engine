88 Healer of the Nations

The Stoic philosopher Epictetus stated the case for natural law in
terms of man as a divine being: “When a man has learnt to under-
stand the government of the universe and has realized that there is
nothing so great or sovereign or all-inclusive as this frame of
things wherein man and God are united . . . why should he not
call himself a citizen of the universe and a son of God?”8

Here is a doctrine of potentially divine mankind. Natural man
is seen as being the heir of God, a citizen of the universe. This
theory rests on the pagan idea of the chain of being. Men and God
are participants together in common being, meaning ultimately
that men can become divine. This is Satan’s old lure: “You will be
like God” (Genesis 3:5b). Yet there are Christian scholars who still
cling to natural law theory as the best intellectual defense against
modern paganism, never admitting that natural law theory is an
invention of ancient pagan imperialism.

Natural law theory was devised by a few Greek scholars who
in general had lost faith in politics. They were products of a
regional civilization in which politics was the heart of religion.
The world of the polis was under attack in every sense: spiritually,
militarily, and politically. These philosophers did not give specific
content to natural law. The idea served as a hypothetical intellec-
tual backdrop, a hoped-for means of uniting all men in a coherent
universe —a dream that would have been utterly foreign to Greek
political speculation prior to the fourth century, 3.c. Before,
Greeks had been uninterested in the “barbarian” world, the world.
outside the city-state. The collapse of that local world forced upon
a handful of philosophers the intellectual problem of finding
meaning in a world far different from anything earlier Greek phi-
losophers had imagined possible.

The Roman Republic remained indifferent to Greek philoso-
phy, but as it began to expand, taking on the characteristics of the
empire, Stoicism began to appeal to Rome's thinkers. Stoicism
“appeared to suggest, in the field of relations between states, a sys-

8. Discourses, Tsix; cited in ibid., p. 80.
