110 Healer of the Nations

a kind of intelligence and adaptability. In the last few thousand
years, we’ve made astonishing cultural and technical advances. In
other areas, we’ve not made so much progress. For example, we
are still bound by sectarian and national rivalries.”

Recognize that “intelligence and adaptability” are code words
for evolution, meaning man-directed social, political, and eco-
nomic evolution. “Sectarian and national rivalries” are code words
for religious differences and nationalism. But Sagan is optimistic.
He sees a new world a’ comin’. Some people might even call it the
humanist New World Order. “It’s clear that sometime relatively
soon in terms of the lifetime of the human species people will iden-
tify with the entire planet and the species, provided we don’t de-
stroy ourselves first. That’s the race I see us engaged ina contest
between unifying the planet and destroying ourselves.”*

Back in the 1950’s, the slogan was: “Peace in the world, or the
world in pieces.” It is the same religious pitch: the unification of
mankind ethically and politically (the humanist one-world order)
is necessary if mankind is to survive as a species. Men must be
pursuaded to affirm very similar moral, political, and economic
goals. Divisive creeds and opinions need to be educated out of
people, preferably by means of compulsory, tax-financed schools.
Diversity of opinion concerning these humanistic goals must not
be tolerated, for they are the basis of “sectarian and national
rivalries.” Mankind must not be allowed to reveal differences of
opinion on fundamentals. Mankind’s unified godhead is at stake.

Now, there are three ways to achieve this unity: persuasion
(“conversion”), manipulation, and execution. The first approach
takes forever, or at least it seems to take forever in the opinion of
humanists. It also eats up lots of resources. It takes teams of “mis-
sionaries.” People just never seem to agree on these humanistic
first principles. They bicker. They battle. They refuse to be per-
suaded. Mankind reveals its lack of agreement on religion and
ethics. This, you understand, must not be tolerated.

If you cannot persuade men to cooperate, either by force of

14. U.S. News and World Report (Oct. 21, 1985), p. 66.
