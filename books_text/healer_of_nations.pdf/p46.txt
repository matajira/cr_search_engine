32 Healer of the Nations

cal boundaries are basic to every map of the world in man’s his-
tory and future. “And He has made from one blood every nation
of men to dwell on all the face of the earth, and has determined
their preappointed times and the boundaries of their habitation”
(Acts 17:26).

The Nations of Mankind

There ‘are 70 nations listed in Genesis 10. “These were the
families of the sons of Noah, according to their generations, in
their nations; and from these the nations were divided on the
earth after the flood” (Genesis 10:32). The number 70 is repeated
frequently in the Bible as the number of the nations. At the feast
of the tabernacles in the seventh month, beginning with the fit
teenth day, the priests of Israel began a week of sacrifices. For
seven days, a descending number of bullocks were sacrificed: 13,
12, 11, 10, 9, 8, and 7, totalling 70 bullocks, Then, on the eighth
day, one final bullock was sacrificed (Numbers 29:12-36). These
totalled 71 sacrifices for the 70 nations of the world, plus Israel.
God’s atonement for the whole world was manifested ritually in
the Old Testament sacrificial system.

When the Israelites captured the Canaanitic king, Adoni-
Bezek, after the death of Joshua, he confessed that he had slain 70
kings ( Judges 1:7). This presumably was his way of saying that he
had conquered the world—an assertion of his sovereignty.

The point is, the nations of the world existed prior to the scat-
tering at the Tower of Babel in Genesis 11. Mankind was supposed
to spread across the face of the earth. This is God’s method of sub-
duing the earth to His glory. The one river of the Garden of Eden
became four rivers flowing out from the garden (Genesis 2:10),
and therefore down from the garden, which indicates its status as a
mountain location (Genesis 2:10-14). This pointed to man’s re-
sponsibility of following them to “the four corners of the earth.”
The garden’s originating river revealed mankind’s unity, while the
four rivers flowing outward pointed to mankind’s future geo-
graphical and cultural diversity.

There are many nations, but only one mountain of Zion: “Great is
