All Nations Under God 75

Consider what Paul was doing. He was going through the
Roman Empire preaching the kingdom of God. The Jews were
correct in charging the disciples with preaching another King,
Jesus. That is exactly what they were doing. And in the end,
pagan Rome fell to Christianity. The triumph of Christianity did
mark the end of the pagan Roman Empire. Christ triumphed
over Caesar in history through His people.*?

Was Paul an isolationist? Hardly. He was an ambassador of
Christ’s international world order. He warned his listeners of the judg-
ment to come. Christ is King of kings and Lord of lords. This is the
message of God’s internationalism. This message repels humanist
internationalists as much as it repels humanist isolationists. It
means that God will destroy their pretensions of autonomy.

What the isolationist does not fully understand is that there is
a war on. This war is a war between Christ’s kingdom and the
kingdom of Satan. Christ’s kingdom is revealed in earthly king-
doms, and so is Satan’s kingdom. There can be no permanent
peace treaty between these two kingdoms. Thus, there can be no
permanent peace treaty between the national representatives of
the rival kingdoms. There can only be temporary cease-fire agree-
ments concerning certain aspects of the perpetual conflict
(Chapter Four). The quest for permanent peace treaties is legiti-
mate, but only between nations that are official, covenanted rep-
resentatives of Christ’s kingdom. The quest for permanent peace
on any other basis is an illusion. Such a peace treaty is either a
disguised attack on the pagan co-signer or a disguised surrender
to the pagan co-signer. There is no third possibility. There is no
neutrality in history, and there is no peace until the final judgment.

There can be no perfect peace in history because sin always
burdens mankind this side of the final judgment. Nevertheless,
there can and will be progressive peace in history as the gospel re-
duces the rule of sin in history (Isaiah 32; Jeremiah 32), and as
God’s covenantal blessings pour out on mankind (Deuteronomy
28:1-14). I discuss this in Chapter Five.

42. Ethelbert Stauffer, Christ and the Caesars (Philadelphia: Westminster Press,
1955).
