300 Healer of the Nations

treaty’s ratification by the Senate. The House offered Madison’s
resolution when it voted the money required by the treaty:

. .. when a treaty stipulates regulations on any of the subjects
submitted by the Constitution to the power of Congress, it must
depend for its execution as to such stipulations on a law or laws to
be passed by Congress, and it is the constitutional right and duty
of the House of Representatives in all such cases to deliberate on
the expediency or inexpediency of carrying such treaty into effect,
and to determine and act thereon as in their judgment may be
most conducive to the public good.”*

The study goes on to say that “This early precedent with re-
gard to appropriations has apparently been uniformly adhered
to.”25 However, because Congress has found itself incapable of
controlling spending in this century, it has lost control of the na-
tional government. It has therefore lost its ability to restrict Presi-
dential power, Centralization has continued as a result. Centrali-
zation is the inevitable result of big spending by the nation-state.

What is desperately needed is a Constitutional provision to
distinguish between a state of war and defensive operations
against a formally undeclared war against the nation. Peace must
not be defined as a condition of undeclared hostilities. The mili-
tary encirclement of the United States by the Soviet Union is an
act of war, declared by Lenin and all his successors. But because
the Soviets have not launched a nuclear first strike against the
U.S., the government pretends that we are not in fact losing a com-
prehensive war, including a military war. Trading with the enemy
goes on. The President should have the authority to suspend
diplomatic relations, including trade relations, with any nation that
he deems as a military threat to the U.S., subject to the approval of
Congress, meaning a two-thirds vote of both houses of Congress —
what it would take to override his veto. In fact, he probably pos-
sesses this authority now, and it should be exercised against the
USSR and Nicaragua, just as it has been exercised against Cuba.

24, Ibid., p. 488. A similar resolution was adopted by the House in 1871.
25. Idem.
