266 Healer of the Nations

are buying a service, we care about its quality, not the theology of
the person who delivers it,

There are exceptions. We do not want to subsidize evil. If a
physician practices abortion, Christians should seek out another
physician to heal them. But if your spouse were bleeding to death
at the side of the road, and a known abortionist offered to save his
life, would you refuse his assistance? No, you would take it. And
after your spouse recovered, you would again be found marching
in the picket line in front of his office,

We need to-look at the skills of Christian brothers in the same
way. Each Christian group “brings something to the table,” as the
slang of business says. They bring something to Gad’s table, as
the language of the Lord’s Supper says. It is our Gad-assigned
task to seek out the positive contributions of every Christian
group in order to discover what others possess in abundance that
each of us is lacking. Our job is to imitate their strengths without
sacrificing any of our own.

The division of labor is institutional and international within
the Church International. There are whole denominations that
see better than others. (Presbyterians, for example.) There are
denominations that walk better than others. (Baptists, for exam-
ple.) There are denominations that prosper economically more
than others. (Episcopalians, for example.) There are denomina-
tions that tithe more than others. (Mennonites, for example.)
There are denominations that show enthusiasm more than others.
(Pentecostals, for example.) There are denominations that have
persevered under oppression longer than others. (Eastern Ortho-
doxy, for example.) There are denominations that have captured
pagan cultures better than others. (Roman Catholicism, for ex-
ample.) They all bring something unique and valuable to God’s
table.

On the other hand, how many Christians would want to study
Christian philosophy at a Pentecostal seminary, study missions at
a Presbyterian seminary, study youth work at an Episcopalian
seminary, study military strategy at a Mennonite seminary, study
church cooperation at a Baptist seminary, study cultural transfor-
