Conclusion 255

Beating Something With Something Better

Christians possess the Bible and the Holy Spirit. They have
the law of God and the power of God at their disposal. They have
the doctrine of the covenant in all its God-given authority. Yet
they ignore all this, and instead proclaim Jesus as Lord of all the
Church, but not of all the earth; Jesus as sovereign Master of the
family, but not the civil government; Jesus as the Healer of a rem-
nant but not healer of the nations.

They send out missionaries, but not to baptize nations. They
send out pamphlets, but not handbooks for exercising godly rule.
The send out physicians of the body and the soul, but not of the
body politic. They have turned the world over to the devil by
default (and sometimes in the name of New Testament theology},
and then have vainly sought to persuade the devil’s power-holding
representatives to allow equal time for Jesus. Why should they
allow equal time for Jesus? Does Jesus intend to allow equal time
for Satan in eternity?

Christians have denied the covenant. They have denied with
all their heart, mind, and soul that Jesus intends them to disciple
the nations, including their own nations. They have denied the
greatness of the Great Commission.3? They understand the conse-
quences of such covenantal failure, but they have hoped in a last-
minute rescue by God’s cavalry. They have denied the compre-
hensive redemption offered at Calvary, and instead hope in a
supernatural deliverance in the midst of the international failure
of Christianity.

Did God intervene miraculously to bail out Jesus as He walked
toward Calvary? Did He intervene to save Stephen from the
stones of his adversaries (Acts 7)? No. Then why should He bail
out today’s Christians, who show none of Christ's courage in the
face of death, and none of Stephen’s taste for verbal confrontation
with the Christ-hating rulers of his day? If God refused to bail out
those who, in the midst of crisis, have preached the Church’s vic-

39, Kenneth L, Gentry, Jr, “The Greatness of the Great Commission,” Jour-
nal of Christian Reconstruction, VII (Winter, 1981).
