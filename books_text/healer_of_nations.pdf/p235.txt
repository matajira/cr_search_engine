Government Foreign Aid Disinherits the Faithful 221

on the ground that such a policy is a condition of economic devel-
opment. . . . At the same time the continued low living standards
and the persistent economic difficulties of centrally planned econ-
omies in underdeveloped countries serve as justification for con-
tinued aid.”2

The formerly underdeveloped nations of the Far East that are
now noted for their rapid economic growth have received little
government foreign aid from the West: Hong Kong, Taiwan
(Free China), Singapore, South Korea, and Malaysia. What they
have received is access to Western consumer markets. More than
any other single factor, the primary cause of economic develop-
ment of backward nations has been their commercial contacts
with the West. The farther away one gets from Western contacts,
the poorer the nations become.?

Contact with the West raises hopes among those people in
backward nations who are willing to sacrifice present consump-
tion for future income, This will initially be a minority of the pop-
ulation. As success proves the point—that contact with the West
and adopting Western economic techniques brings prosperity —
Western attitudes begin to penetrate the general population.
Their own future-orientation, thrift, and hard work have brought
them prosperity. Hong Kong is the classic example. A tiny area
south of China, a region with no known natural resources, over-
populated by Western standards, Hong Kong is so competitive
that manufacturers all over the Western world cry for tariff bar-
riers and import quotas against the “unfair competition” of this
tiny crown colony.*

They have adopted the Protestant work ethic without Christi-
anity, while the West was abandoning the work ethic, having
already abandoned the Protestant work ethic, But God is faithful:
He rewards with external blessings those who are externally obe-

2. P. T. Bauer, Dissent on Development: Studies and Debates in Development Economics
(Cambridge, Massachusetts: Harvard University Press, 1972), pp. 106-7.

3. Ibid., pp. 300-2.

4. PT. Bauer, Equality the Third World and Economic Delusion (Cambridge,
Massachusetts: Harvard University Press, 1981), ch. 10: “The Lesson of Hong
Kong.”
