God Created the Nations 35

God and God’s law. The individual, institution, or association ini-
tiates projects; the State only serves as a kind of “night watchman”
to see to it that each person abides by Biblical law in seeking his
various personal goals.

The principle of localism would be affirmed. Local courts
would handle most cases. Only the hard cases would be accepted
by the appeals courts. Local laws would not be overturned unless
they could be proven to be in opposition to a Biblical principle or
in opposition to the agreed-upon covenantal (constitutional)
terms of the next level of civil government. Just as in today’s hu-
manist nations, the supreme court can overturn a piece of legisla-
tion that violates the national constitution or common law prece-
dent, so a supreme court would overturn a constitutional provi-
sion that violates Biblical law. The Bible is sovereign, not some
human compact. However, to keep the supreme court from be-
coming absolutely sovereign, a combination of other civil author-
ities could overturn the court. For example, if the United States
were a truly Biblical commonwealth, the combined votes of (say)
three-quarters of all the members of each of the two houses of
Congress, plus the President, would be able to overturn a decision
by the U.S. Supreme Court. There should never be a unitary, ab-
solutely final, earthly court of human appeal.

3. Common Biblical Law

The Bible as the Word of God would be the final standard of
justice. All laws at every level of government would be judged in
terms of the Bible. The national constitution (written or unwrit-
ten) would be officially subordinate to the Bible. The courts would
render judgment in terms of the Bible. A body of legal precedent
would build up over the years, but precedents would always be
subjected to the decisions of juries regarding the proper applica-
tion of the civil code to circumstances. The Bible would be
declared the supreme law of the land, and it would be taught in
public gatherings on a regular basis (Deuteronomy 31:10-13).

4. Judgment by Citizens
The judges in Exodus 18 were to be men of good character.
There were to have been a lot of judges—far more than an elite
