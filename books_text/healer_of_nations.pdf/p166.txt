152 Healer of the Nations

There is no escape from citizenship in one of the two supernat-
ural commonwealths. One is born into Satan’s or adopted into
God's. This supernatural citizenship is the model for earthly citi-
zenship, either godly or satanic. Men are citizens of two common-
wealths: supernatural and earthly, There is a continuing struggle
throughout history between each side to increase the geographic
range of authority of the respective supernatural commonwealth,
This battle is primarily ethical. Each side seeks to extend through
its covenant the number of those who swear allegiance to the
respective supernatural commander.

The nature of this struggle has been concealed by the doctrine
of natural law. The quest for ethical neutrality in terms of
common-ground principles between the two kingdoms has been a
familiar feature of Christian scholarship since the early centuries
of the Christian church. As men’s faith in natural law has de-
clined, so has the myth of neutrality,

The basis of citizenship in the two supernatural kingdoms is
covenantal, So is the basis of earthly citizenship. As time goes on,
the nations of the world will be divided more and more in terms of
the rival covenants. The hope of discovering neutral terms for
these national covenants is fading, except in academic Christian
circles. The humanists of the West know better. The humanists
who rule in Communist societies know best of all.

Citizenship will progressively include and exclude members of
the earthly commonwealths in terms of the rival supernatural cov-
enants. As we approach the final judgment, the rival camps will
be more consistent in their imposition of the respective covenant.
Satan’s forces will grow weaker and lose territory, and Christ’s
forces will steadily replace them.

The basis of legitimate civil rule is ethical. Humanists substi-
tute power and intrigue for ethics. This creates resistance and
suspicion. Steadily in history, ethics replaces power and intrigue
as the basis of lawful civil rule.

In summary:

1. Christians are citizens of heaven.
2. Christians are covenanted to heavenly citizenship.
