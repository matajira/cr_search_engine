God's Legacy of Progressive Peace 125

there would be fewer and fewer enemies willing and able to raise
the battle cry against God-protected Israel. It means the same today.

Peace is therefore seen in the Bible as the ability militarily to wage
war successfully, if necessary. But what about turning swords into
plowshares? If peace really means preparation for war, how can
swords be converted into plowshares? They cannot be, until the
enemies of God have died on the field of battle, or else have retreated or sur-
rendered. Christian international relations can seek military disar-
mament only if Christians affirm the legitimacy of, and work to-
ward, national and international covenantal commitment to God.
Only if nations as nations affirm the covenant of Christ can inter-
national relations progressively attain peace.

In short, if there were no such thing as a Christian nation,
there could be no such thing as Bible-defined international peace
in this world. We would be condemned to a world of endless mili-
tary conflict. But we are not condemned to this. Jesus said very
clearly that when we hear wars and rumors of wars, the end is not
at hand. “And you will hear of wars and rumors of wars. See that
you are not troubled; for all these things must come to pass, but
the end is not yet” (Matthew 24:7).

What those who cite “swords into plowshares” fail to recognize
is that the Bible also calls on the enemies of God to launch a
doomed offensive against the protected nation. The Bible calls
God’s enemies into a losing battle. The Bible teaches plowshares
into swords . . . first.

Plowshares Into Swords
We are familiar with the phrase, “swords into plowshares.” It
applies to covenantally faithful nations. But the reverse phrase
also appears in the Bible, “plowshares into swords.” It applies
to covenantally rebellious nations. This is the message of the
prophet Joel:
Proclaim this among the nations: “Prepare for war! Wake up

the mighty men. Let all the men of war draw near. Let them come
up. Beat your plowshares into swords and your pruninghooks into
