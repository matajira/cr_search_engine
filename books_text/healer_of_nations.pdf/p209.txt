Businessmen Make the Best Diplomats 195

replace God’s bottom-up decentralized kingdom.

Christianity can prosper only if it adheres to God’s revealed
law in every area of life. When Christian businessmen do this,
they will prosper. Eventually, they can carry the story and the
skills of the gospel to foreign nations that may hate God but who
want the economic fruits of righteousness. Perhaps the most suc-
cessful social export of the United States in the twentieth century
is the Rotary Club.* Why wasn’t it the Christian church? Because
Christians have lost the Biblical vision of victory in history.

The first step in reducing the influence of our humanist ene-
mies in foreign policy is to cut drastically the budgets and the size
of the government-operated diplomatic corps of Western nations,
and replace them with thousands of businessmen who will serve
as unofficial, unpaid diplomats. Then it will be our job as Chris-
tians to raise up the skilled businessmen and businesswomen nec-
essary for this world-transforming task. There is no shortcut to
success.

It is imperative that the elected representatives of the nation
establish guidelines regarding valid trade policies. There should
be no trade whatsoever with nations inside a hostile, expansionist
empire, except to get Christian missionaries inside the country. It
is not the task of businessmen to determine which nations are le-
gitimate trading partners and which are not, any more than it is
the task of professional salaried diplomats to make this determina-
tion. Once the decision is made, however, businessmen rather
than foreign service bureaucrats should spearhead both contacts
and contracts between residents of the nations,

In summary:

1. Christians are to become Biblical pragmatists.

2. Christians are to become skilled workers in the affairs of
life.

3. Otherworldly Christianity resents this vision of full-scale
Christian responsibility in this world.

4. The humanists who now are in control agree with these

34. The classroom opinion of conservative sociologist-historian Robert
Nisbet.
