Nations With Rival Covenants Are Always at War il

reason, or an appeal to self-interest, or a moral appeal, then you
have only two choices remaining: manipulation or execution,
Either you confuse the bickering factions by means of an endless
process of shifting international alliances, thereby gaining their
cooperation under a unified (but necessarily secret) elite of plan-
ners, or else one faction must eliminate all rivals by force: you kill
your opponents or make them slaves. There is no third alterna-
tive, given the false doctrine of the ethical unity of man, Man is in
principle ethically unified, this theology proclaims; therefore, any
visible deviations from this hypothetical unity must be sup-
pressed, one way or another.

Détente

The West’s political and economic leaders are humanists.
They have adopted the soft-core humanism of Western liberalism.
They are not dedicated to fighting an all-encompassing war

, against evil. They are dedicated to pursuing universal peace
through endless peace treaties. They want to achieve in history
what God says is opposed to His plan in history: a stalemate
between good and evil, between covenant-keeping nations and
covenant-breaking nations. %

The Soviets know better, being hard-core humanists. They
use their opponents’ naive view of history to their advantage.
They promise endless treaties, in order to lure their enemies into
complacency. From the day they signed the Treaty of Brest-
Litovsk in early 1918 after they had surrendered to Germany,
Lenin knew exactly what he was doing. He began to break the
treaty, imitating Adam's decision in the garden. Broken treaties
are the heart of man’s rebellion. “Yes, of course, we are violating
the treaty; we have violated it thirty or forty times. Only children
can fail to understand that in an epoch like the present, when a
long, painful period of emancipation is setting in, which has only

15. Gary North, Backward, Christian Soldiers? A Manual for Christian Recon-
struction (Tyler, Texas: Institute for Christian Economics, 1984), ch. 11: “The
Stalemate Mentality.”
