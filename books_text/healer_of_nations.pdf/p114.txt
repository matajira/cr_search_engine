100 Healer of the Nations

business, etc. And it culminates in wars between nations.

We must never forget where wars come from: sinful hearts.
We must also not forget that God takes sides in these wars, as He
says repeatedly in the Bible. The Lord of hosts (angelic armies) is
on the side of those who remain faithful to His covenant.

War and Peace

Perhaps the most famous Western definition of war was pro-
vided by the Prussian scholar, Karl von Clausewitz, who said in
his posthumously published book, On War (1832), that “War is a
mere continuation of policy by other means.”! What I argue in
this book is the other side of this coin: foreign policy is war conducted
by other means. It is not a war in the sense that Clausewitz meant it:
an act of national force to compel our enemy to do our will. God,
not man, is the primary agent of compulsion in history, bringing
His comprehensive historical decree to pass. King Nebuchadnezzar
announced the absolute sovereignty of God in history with far
greater assurance than most modern Christians possess today: ‘All
the inhabitants of the earth are reputed as nothing; He does accord-
ing to His will in the army of heaven and among the inhabitants of
the earth. No one can restrain His hand or say to Him, ‘What have
you done’” (Daniel 4:35)? But this war is nevertheless a real con-
flict. Christians are God’s ambassadors who come in His name with
a peace treaty that requires unconditional surrender to the Great
King.? As ambassadors, they are peaceful warriors. The war be-
tween the two supernatural kingdoms never ends in history.

International relations are always governed by questions of
war and peace, Nations seek their goals in the international scene.
A few nations will always be on the offensive. They seek to impose
their will on other nations by imposing sanctions (though not ne-
cessarily military sarictions): blessings and cursings. They imitate
God in this respect.

1. Chapter One, Observation #24, On War, edited by Anatol Rapoport (New
York: Penguin, [1968] 1982), p. 119.

2. Gary North, Unconditional Surrender: God's Program for Victory 3rd ed.; Ft.
Worth, Texas: Dominion Press, 1987).
