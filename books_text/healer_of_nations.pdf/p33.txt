Introduction i9

his legs because mankind might someday have wings. I am not in
favor of cutting off my legs. I do favor cutting off the U.S. State
Department, however. In our present diplomatic situation, noth-
ing really is better than something. As Congressman Lawrence
McDonald remarked to me about a year before a Soviet pilot shot
down Korean Airlines flight 007 on which he was traveling: “If the
failures of the State Department were simply the result of stupid-
ity, the United States would win a diplomatic victory occasionally,
just on the basis of randomness. We never do.”

It is true that the world I describe as the Biblical standard is
presently nowhere visible on earth (Greek: ou = no; topos = place).
But Jesus Christ also is nowhere visible on earth. Is the Church
International therefore an institution that proclaims a utopian
faith? Humanists think so; Christians disagree. They know that
Christ is present covenantally with His people. They eat the
Lord’s Supper in His presence. They understand a fundamental
Biblical principle: what goes on in heaven is to serve as an ethical model
for what should go on in earth. Christians have been commanded by
Jesus Christ to pray: “Thy kingdom come. Thy will be done in
earth, as it is in heaven” (Matthew 6:10, King James Version).
When Christians stop believing this prayer, they become cultur-
ally irrelevant. When Christians stop praying this prayer, they
come under the curses of God in history. (Does your church still
pray this prayer publicly on a regular basis? If not, it is time for
you to find another church, or to start praying and working to
change your church.)

If Christians remain unaware of what the Bible says about
what this world should be and should do, they will not possess a
motivating vision of the Biblically attainable future. Understand,
what this book presents is not a program to attain what in princi-
ple cannot be attained in history. Instead, it presents God’s re-
quired blueprint for what must and will be attained by covenant-
ally faithful people in history. Humanists will resent this. World-
retreating Christians will also resent this.

To get from here to there will require a world crisis of institu-
tionalized humanism on a scale unmanageable by those who pres-
