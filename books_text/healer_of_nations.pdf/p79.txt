All Nations Under God 65

Yet this, in a real sense, could never be enough. No matter how
this nation strove to isolate itself in past generations, it could never
suppress or deny an impulse toward the world. In one age, this im-
pulse expressed itself through missionaries; in another age,
through philanthropy, medical care, deeds of charity; and, most
recently, through massive international aid and assistance.

There is a reason why this impulse has asserted itself. Our
Founding Fathers, obviously, built a home for one nation. Yet the
idea to which they and this nation were committed—the idea of
human freedom —was, is and can only be universal.

We are bound as a people, in the deepest sense, to live by this
commitment with a boldness, a confidence, and a clarity of vision
matching those who led us to national life.

He recognized the transition: from Christian internationalism
(missions) to humanitarian internationalism (philanthropy) to
statist internationalism (government-to-government foreign aid
programs) to the humanist one-world State. The problem, of
course, is the need for some sort of shared religious principles:
“There must be shared values and goals—a comparable com-
prehension of the nature of man and his place in the universe,
specifically with reference to freedom, justice, opportunity, dignity
and the rule of law.” Question: The rule of whose law, God’s or
self-proclaimed autonomous man’s?

The humanists expect a new human consciousness to appear
in our era, thus making possible the creation of a new global com-
munity. Zbigniew Brzezinski, who served as President Carter's
head of the National Security Council, speaks of this development
in terms of the New Age slogan, “Toward a Planetary Con-
sciousness.” He assured us in 1970 that “it would be wrong to con-
clude that fragmentation and chaos are the dominant realities of
our time. A global human conscience is for the first time begin-
ning to manifest itself. This conscience is a natural extension of
the long process of widening man’s personal horizons. In the
course of time, man’s self-identification expanded from his family

24, Ibid., pp. 147-48,
25, Tbid., p. 135.
