God's World Government Through Biblical Law 89

tem which could be used to justify Roman expansion,”® From the
beginning, natural law theory was an invention by an elite of
pagan Greek intellectuals to comfort themselves in the midst of
their collapsing civilization, and it was then used as an intellectual
cover for other pagan philosophers who sought universal reasons
to justify an expanding Roman Empire. Yet it is this makeshift in-
tellectual system, conceived by pagans in despair, and adopted by
tyrants who persecuted the Church (the best example is the
Roman emperor-philosopher Marcus Aurelius, under whose
tyranny Justin Martyr died) that has captured the minds of Chris-
tian philosophers, from the early church until the present.

Today, few people still believe in natural law. Christian
scholars are among a handful of philosophers who still accept the
idea. The humanists have, for the most part, recognized what
Darwin’s Origin of Species did to natural law. In a world of pur-
poseless change governed by purposeless chance mutations,
meaning a world of constant evolution through impersonal natu-
ral selection, nature is no longer regarded as a source of man-
comforting perpetual moral truths. Man is seen as either the vic-
tim of meaningless nature or else the potential master of nature
through scientific planning, but there is no room left for the har-
monious outworkings of natural moral law in Darwin’s dog-eat-
dog universe. Christians who still defend the morally empty box
of natural law theory have failed to recognize the obvious. They
still cling to an idea that originally was a makeshift intellectual
proposal offered by a handful of God-hating Greek cultural
defeatists which was then taken up by power-seeking Roman
apologists for bloody empire. It is time for Christians to abandon
natural law theory.

The Law of God and the Heart of Man

But if there is no common natural law, what holds man’s social
world together? If men of different religions do not agree on phi-

9. Gerardo Zampaglione, The Ides of Peace in Antiquity, translated by Richard
Tumn (Notre Dame: Indiana: University of Notre Dame Press, [1967] 1973), p. 139.
