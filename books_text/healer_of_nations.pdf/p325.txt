 

BIBLIOGRAPHY

Abraham, Larry. Call It Conspiracy. Seattle, Washington: Double A
Publications, 1985.

Allen, Gary. Say “No!” to the New World Order. Seal Beach, Califor-
nia: Concord Press, 1987.

Barron, Bryton. Inside the State Department: A Gandid Appraisal of the
Bureaucracy. New York: Reflection Book, Comet Press, 1956.
Brown, Anthony Cave and MacDonald, Charles B. On a Field of
Red: The Communist International and the Coming of World War IT.

New York: Putnam’s, 1981.

Bouscaren, Anthony Trawick. Soviet Foreign Policy: A Pattern of Per-
sistence. New York: Fordham University Press, 1962.

Burnham, James. The Suicide of the West: An Essay on the Meaning
and Destiny of Liberalism, New York: John Day, 1964.

Cohen, Warren. The American Revisionists: The Lessons of Interven-
ton in World War I. Chicago: University of Chicago Press, 1967.

Dawson, Christopher. The Judgment of the Nations. New York:
Sheed and Ward, 1942.

Department of State. Freedom From War: The United States Program
Sor General and Complete Disarmament in a Peaceful World. Depart-
ment of State Publication 7277, Disarmament Series 5, Office
of Public Services, Bureau of Public Affairs (Sept. 1961).

Doenecke, Justus D. The Literature of Isolationism: A Guide to Non-
Interventionist Scholarship, 1930-1972. Colorado Springs, Col-
orado: Ralph Myles, Publisher, 1972.

3it
