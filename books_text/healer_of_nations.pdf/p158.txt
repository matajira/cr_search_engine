144 Healer of the Nations

incorruptible, and we shall be changed. For this corruptible must
put on incorruption, and this mortal must put on immortality. So
when this corruptible has put on incorruption, and this mortal has
put on immortality, then shall be brought to pass the saying that is
written: “Death is swallowed up in victory.” “O death, where is your
sting? O Hades, where is your victory?” (1 Corinthians 15:50-55).

Covenant-breakers also become in eternity what they are in
principle throughout history: ethical rebels. At the resurrection,
covenant-keepers and. covenant-breakers are separated forever.
Perfect righteousness has no fellowship with perfect wickedness;
only history’s imperfect righteousness and imperfect wickedness
permits partial cooperation.?

Paul writes of a final transformation of Christians’ sin-cursed
bodies. This takes place when Christ returns in judgment. It is an
instantaneous transformation, accomplished “in a moment, in the
twinkling of an eye.” Paul writes elsewhere:

For the Lord Himself will descend from heaven with a shout,
with the voice of an archangel, and with the trumpet of God. And
the dead in Christ will rise first. Then we who are alive and remain
shalt be caught up together with them in the clouds to meet the
Lord in the air. And thus we shall always be with the Lord
(1 Thessalonians 4:16-17).

The Triumph of God's People in History

Liberals may not like such language of people being caught in-
to the air at Christ’s coming, but Bible-believing Christians must
affirm this future event. This is not some sort of symbolic event.
This will really take place as described.

The problem is, Christians have neglected too many of the im-
plications of these passages of final transformation. They have not
recognized that as time goes on, Christians mature in Biblical
faith, and they are progressively to manifest as “through a glass,
darkly” the final ethical perfection of the post-resurrection world,
Because of this universal, progressive, external ethical maturity,

1. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
TX: Institute for Christian Economics, 1987), ch. 2.
