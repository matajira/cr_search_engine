Missionaries Make the Best Ambassadors 161

The Doctrine of Representation

We can find the answer in two New Testament accounts of the
same event. They seem to be conflicting accounts, but they are
not. Matthew 8 records that a centurion came to Jesus and asked
Him to heal his servant. It says that “a centurion came to Him,
pleading with Him” (v. 5). But Luke 7 says that Jews came to
Jesus in the name of the centurion, and then the centurion’s
friends came to Him (vv. 3, 6). There is no evidence that the cen-
turion ever actually spoke with Jesus. Is there a conflict here?
Does the Bible contradict itself?

No. The centurion spoke with Jesus through representatives.
Also, the centurion represented his sick servant in his request that
Jesus heal him. The centurion understood the doctrine of repre-
sentation. He even went so far as to say that Jesus did not have to
enter his house in order for the servant to be healed (Luke 7:6b).
Then he said, “But say the word, and my servant will be healed”
(v. 7:7b). He understood Jesus’ authority. He was implicitly
testifying to Jesus’ position as God’s representative, for he com-
pared Jesus’ authority to his own position as a representative of
Caesar:

“For I also am a man placed under authority, having soldiers
under me. And I say to ane, ‘Go,’ and he goes; and to another,
‘Come,’ and he comes; and to my servant, ‘Do this,’ and he does it.”
When Jesus heard these things, He marveled at him, and turned
around and said to the crowd that followed Him, “I say to you, I
have not found such great faith, not even in Israel!” (Luke 7:8-9;
emphasis added).

The Jews spoke to Jesus in the name of the centurion. His
friends also spoke in his name. The centurion spoke to Jesus in
the name of Caesar (above him) and in the name of his sick ser-
vant (below him). Understand, he was publicly subordinating
himself to Jesus’ authority, despite his official position as Caesar's
lawful representative. This took great faith, as Jesus publicly
affirmed to the crowd. The centurion recognized that Jesus spoke
in the name of God the Father (above Him) and could therefore
