All People Are Citizens of Two Worlds 147

ancient classical democracy, clan family (brotherhood), Renais-
sance nation-state, totalitarianism, and modern democracy.

Ancient classical (Greece and Rome) citizenship was based
on membership in the family. Only males could participate in the
city-state because only males could participate in the family relig-
ious rites. Citizenship was through family membership. Women
were excluded because upon marrying, a woman moved to her
husband's family. Slaves were excluded. Foreigners were ex-
cluded. Citizenship was achieved either by birth or adoption.
There was no other way. If you were not a citizen, you could not
enter into a court of law. Contracts were valid only between citi-
zens. Foreigners were considered outside “humanity.” (The best
study of classical citizenship is Fustel de Coulanges’ The Ancient
City, published in 1864).

Clan families have been similar. Membership was by birth or
perhaps adoption. A man is a brother; outsiders are “others.”
Ethical rules governing those inside the clan are different from
those governing those outside. The Mafia is the obvious modern
example of clan citizenship. It is international in scope, but only
men of certain regional background (Sicily, southern Italy) are
eligible for full Family membership. (Inheritance is through the
mother, not the father: “Where was your mother or grandmother
born?”) Membership is provisional. Only the man who has “made
his bones” — murdered someone —is initiated fully (“adopted”) into
the Family. Murder is the rite of passage, the basis of inclusion.5
This is the ultimate earthly exclusion of another from member-
ship in any earthly citizenship.

The Renaissance nation-state that developed after the six-
teenth century is based on geographical residence. Those living
under the jurisdiction of the king are eligible for exercising politi-
cal power. Usually, membership within the oligarchy was based
on birth: royal or noble line. It could be attained through inter-

5. I was given this information by a man whose grandmother was born in the
region of Calabria, Italy, and who was subtly invited into the Mafia when he be-
came of age. He turned down the offer.
