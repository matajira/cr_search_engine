160 Hreater of the Nations

Lord’s Supper. But Christians should not believe that this periodic
covenant renewal applies only to the local church; it applies to
every faithful church when the Lord’s Supper is taken. The Lord’s
Supper has meaning and manifests publicly God’s imparting of
forgiveness and healing because communing men enter into the
presence of God as Judge, and also into spiritual presence in the
heavenlies with all other communing (covenant-renewing) Chris-
tians, in heaven and on earth. This is the covenantal basis of spiri-
tual Church unity, even though individual churches disagree in
history. There is only one body of Christ. There is only one bap-
tism and one Holy Communion. This unity is real because it is
covenantal. (If the covenantal unity of bread and wine is not truly
a sign and means of unity, then what other continuing act or sym-
bol throughout history testifies to the unity of Christ’s body, the
Church?)

Similarly, covenant renewal applies to all levels of civil
government that are formally covenanted to God: local, regional,
national, and international. There is no reason to limit the appli-
cation of church covenant sanctions to any one congregation.
Neither is there any reason to limit the application of civil cov-
enant sanctions to any one town, region, or nation. God’s law
flows out to all nations. The sanctions apply to all nations. This
was Jonah’s message to Nineveh, and it is Christ’s message to the
world. The disciples of Christ are to disciple the nations (Matthew
28:19).

We have already seen in Chapter Three that God’s law is the
legal basis for uniting the Christian nations of the world. The
God-assigned task of Christians is to press steadily the legal
claims of Christ in their own nations, as well as in other nations
(church missions}. Christians serve as ambassadors of Christ to
the nations. Our goal is to win the nations to Christ covenantally.
Nations are to be baptized. This raises the question: How are na-
tions to be baptized? We baptize people. How can nations be bap-
tized? We cannot understand this process unless we first under-
stand the doctrine of representation.
