Missionaries Make the Best Ambassadors 159

the same time, as we saw in Chapter Two, in a seriously Christian
international world, the national witness-ambassador of a Chris-
tian nation invites the representative leaders of pagan nations to
enter into a peaceful alliance--though not yet a covenant—with
Christian nations, thereby giving time for missionaries from
Christian nations to preach the gospel to the pagan nation’s peo-
ple. Then, steadily, as the gospel brings converts into the Church,
the once-pagan nation is transformed. Upon becoming officially,
covenantally Christian, it is then invited to enter the family of
Christian nations on a covenantal basis. It is not asked to subordi-
nate itself to any other nation, but it is asked to subordinate itself
to the Christian nations’ covenanted appeals court system. This is
Christian internationalism, and it is the standard of foreign policy
set forth in the Bible.

Baptizing the Nations

If the Word of God is the tool (sword) of dominion, then the
Church of Jesus Christ preaches an international Word as well as
a national and local Word. The Word of God does not cease flow-
ing from God’s mountain just because it reaches a national
border. The church is international, for it is called Jerusalem. It is
the center of the earth, just as the sacrifices in the Temple were the
ritual center of the earth. At the center of worship in Israel was
the law of God. It was written on the two copies of the covenant
legal document that were deposited in the ark of the covenant (Ex-
odus 20; 34:28; 1 Kings 8:9), which is the reason it was called the
ark of the covenant. It rested in the holy of holies of the Temple (i
Kings 8:6), At the center of worship today is the Communion
Table, where all Christians unite together to renew their covenant
with God and with each other at the throne of God.

Covenant renewal requires men to take a public oath promis-
ing to submit to the ethical terms of the covenant and also to the
sanctions of the covenant—cursings and blessings. These oaths
are to be taken in the three covenant institutions that God has es-
tablished: church, family, and civil government. Covenant re-
newal in the local church takes place each time members take the
