192 Healer of the Nations

What does it profit indeed? And if a person possesses the
knowledge of how to do business, and this knowledge is the only
means in man’s history by which the vast majority of people who
have adopted this way of life have escaped the threat of starvation,
what does it profit you if you never tell the starving masses of the
world how to escape starvation? Prof. Bauer is correct: it is con-
tact with the West that stimulates economic growth in backward
societies. The fewer the contacts, the poorer the societies.”

We are to give food to people in order to give them time and
energy to learn the skills of growing food and feeding themselves.
This means we must tell them of the liberation offered by Christ,
and the economic liberation offered by Christ’s social order.”

What does the State Department tell them? Morton Black-
well, a former White House aide under President Reagan, relates
this story:

In the spring of 1980, in a briefing at the U.S. embassy in
Guatemala Gity, an embassy staff member stressed to me that the
United States must force Guatemala to break up all large farms
and to nationalize all banks and the export industry.

T responded, “It sounds to me as if you are saying we must
force Guatemala to adopt Marxism in order to save it from
Marxism.”

The briefer, one Arnold M. Isaacs, replied haughtily, “I am
describing the policy of the United States.”

And Mr. Isaacs, whom I met in Guatemala in 1980, is still
going strong in the State Department. Today he’s in Foggy Bottom
itself™ at the State Department's Board of Examiners, interview-
ing and evaluating people who have applied to join the Foreign
Service.32

29. P. T. Bauer, Dissent on Development: Studies and debates in development economics
(Cambridge, Massachusetts: Harvard University Press, 1972), pp. 300-2.

30. Gary North, Liberating Planet Earth: An Introduction to Biblical Blueprints (Ft.
Worth, Texas: Dominion Press, 1987), ch. 9.

31. Foggy Bottom is the place in Washington, D.C. where the State Depart-
ment is locaced.

32. Morton C. Blackwell, “Can the United States Have a Winning Foreign
Policy?” The Constitution (Feb. 1987), p. 8.
