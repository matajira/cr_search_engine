102 Healer of the Nations

our fate to live upon that turning point in history.”3

And these words: “There has never been a society or a nation
without God. But history is cluttered with the wreckage of nations
that became indifferent to God, and died.”*

The Judgment of God in History

What should be the goal of Christian foreign policy in a world
of military pressures? Clearly, the goal is to prepare the way of the
Lord internationally. Christian nations are to act and ta speak
prophetically, just as David did as Israel’s delegated agent when
he confronted Goliath. They must speak in confidence, knowing
that they come in God’s name as His designated representatives.

Then David said to the Philistine, “You come to me with a
sword, with a spear, and with a javelin. But I come to you in the
name of the Lorp of hosts, the God of the armies of Israel, whom
you have defied. This day the Lorn will deliver you into my hand,
and I will strike you and take your head from you. And this day I
will give the carcasses of the camp of the Philistines to the birds of
the air and the wild beasts of the earth, that all the earth may know
that there is a God in Israel. Then all this assembly shall know that
the Lorn does not save with sword and spear; for the battle is the
Lorn’s, and He will give you into our hands” (i Samuel 17:45-47).

He made his point clear: the sovereignty of God and not the
sovereignty of earthly weapons is the basis of national victory.
God is sovereign, not man. The delegated representative institu-
tions of God are sovereign, not the delegated representative insti-
tutions of man.

Note, however, that David did not go empty-handed to fight
Goliath. He went with staff, sling, and stones. He did not wear
Saul’s armor, but he was not a proponent of unilateral disarmament.

The goal of foreign policy is to conduct the earthly war of God
against enemy nations, but to do so if possible without resorting to
armed conflict. The goal is long-term peace through the public

3. Whittaker Chambers, Witness (New York: Random House, 1952), p. 7.
4, [tid., p17.
