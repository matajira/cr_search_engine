288 Healer of the Nations

picket abortion clinics, then they might as well pack their bags for
a one-way trip to the concentration camp, or the firing squad.
The Communists are not playing around; they are playing for
keeps.

Until Christians begin to take seriously Church unity, all talk
about reforming foreign relations will remain speculative and
hypothetical to a fault,

Until Christians recognize that the humanist leaders of hu-
manist nations are at war with the God of the Bible, they will con-
tinue to be unsuccessful politically. They will not believe that they
have been anointed by God to come before this civilization and
confront it with God’s covenant lawsuit, just as Jonah confronted
Nineveh. They will not have the courage to do this, nor will they
know how to prosecute it.

Covenant and Social Order

The Biblical covenant is the basis of social order. When men
break this covenant, they should expect the judgment of God.
This is what Christians should expect. God’s judgment has two
aspects: blessing and cursing.® Thus, if Christians do their work
faithfully, the judgments of God in history will transfer the inher-
itance, including capital and political power, to Christians as the
lawful heirs (Proverbs 13:22b),6 and do so relatively rapidly, per-
haps within a few generations. On the other hand, if Christians
continue to drift along with contemporary humanist culture,
refusing to challenge it in every area of life, allowing humanists to
speak in the name of all people in terms of the doctrine of perma-
nent political pluralism, then they will find themselves under the
general or common cursings that God is about to bring on hu-
manist civilization. In such a case, it may take ten generations be-
fore the transfer of the inheritance approaches completion.

This is why Christians need to prepare themselves for the

4, Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987).

5. Ibid., ch. 4,

6. Tbid., ch. 5.
