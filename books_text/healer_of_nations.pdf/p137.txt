God's Legacy of Progressive Peace 123

“Do not think that I came to bring peace on earth. I did not
come to bring peace but a sword. For I have come to ‘set a man
against his father, a daughter against her mother, and a daughter-
in-law against her mother-in-law.’ And ‘a man’s foes will be those of
his own household’” (Matthew 10:34-35).

Christian foreign policy must begin with this view of peaceful
relations among nations. The humanist’s peaceful world is a
world at war with God.

Swords Into Plowshares

The phrase, “swords into plowshares,” was adopted as the title
for a book on humanist internationalism back in the late 1950's.
The book closes with a ringing declaration of confidence in the
United Nations Organization, an organization that has proven to
be so hopelessly inept, so overstaffed with overpaid and under-
worked bureaucrats, so impotent in dealing with the fundamental
international issues of our era, that political liberals have begun to
regard it as another god that has failed. I shall never forget photo-
graphs in the newspaper of U.N. members from Africa dancing
on their desks when Red China was admitted over the objection of
the United States. I cannot resist quoting the book’s conclusion, if
only as a reminder of how naive humanists are. It serves as a
reminder that Christians should not be overawed at their intellec-
tual opposition. “The United Nations, for all its imperfections and
inadequacies, is an achievement of the first magnitude. It is a
symbol of the urge to civilization. It is a repository of decent hopes
and progressive aspirations, It is a center for the consideration of,
and a mechanism for the effectuation of, such plans as govern-
ments can agree upon to improve the common human lot and
safeguard the common human destiny. It is an instrument for the
mobilization of as much good will and good sense as the political
leaders of divided humanity can muster for dealing with the criti-
cal issues of our time. As such, it draws into its proceedings the
representatives of virtually every state which can contrive to obtain
membership. The United Nations can be, and is being, abused,
