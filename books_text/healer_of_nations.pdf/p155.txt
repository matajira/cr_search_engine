All People Are Citizens of Two Worlds 141

death of the old Adam physically, but it comes in a much-
preferred form.)

Christians are citizens of two countries: heaven and earth.
Anti-Christians are also the citizens of two countries: hell and
earth, although I know of no Bible verse that says this as explicitly
as Philippians 3:20 says it regarding Christian citizenship. Surely,
covenant breakers are dead in trespasses and sins. Surely the
wrath of God abides on them in principle now: “. . . and he who
does not believe the Son shall not see life, but the wrath of God
abides on him” (John 3:36b). It does not require a great leap of
faith to conclude that covenant-breakers are citizens of hell, in the
same way that redeemed covenant-keepers are citizens of heaven.

The dividing issue regarding a people’s supernatural citi-
zenship is the covenant-renewing supper. With whom do men
partake in their communion meal? With God or with demons? Is
their communion holy or unholy? This is what Paul asks in 1
Corinthians 10 and 11. He warns us, “You cannot drink the cup of
the Lord and the cup of demons; you cannot partake of the Lord’s
table and of the table of demons” (1 Corinthians 10:21). People
partake of these meals on earth and in history. They are this-
worldly events as well as other-worldly. The two meals are at war
with each other; the two supernatural kingdoms are at war with
each other.

This points to éhe fact of history: the earth is a battlefield be-
tween two rival forces, the followers of God and the followers of
Satan, This battle is primarily etfical in nature. Two rival law-
orders are involved: Christ’s and Satan’s. There can be no ethical
neutrality; therefore, there can be no judicial neutrality. Ethical
neutrality is a myth. So is natural law (Chapter Three).

God has already established the basis of citizenship in His king-
dom: ethical perfection. Only Jesus Christ has (or can) achieve
this perfection in history. Thus, the basis of the Christian's citi-
zenship in heaven is God’s imputation of Christ’s perfect human-
ity (though not His divinity) to those whom He graciously
redeems. God the Father declares them “not guilty” because of the
work of His Son in history (Chapter Four).
