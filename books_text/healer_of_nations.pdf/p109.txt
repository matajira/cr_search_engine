God's World Government Through Biblical Law 95

of victory are generally righteous.! Few people believe this.

The humanists retain faith in the possibility of man-designed,
State-enforced world government. Some of the humanists who be-
lieve this are Marxists. They have faith in military force, system-
atic subversion, and terrorism. Others are people who think that
various treaties and other international agreements can be ham-
mered out by representatives of major nations. They see interna-
tional law as the key to establishing world government. They also
implicitly understand the structure of the covenant.

In their book, World Peace Through World Law, published in 1958
by Harvard University Press, Grenville Clark and Louis Sohn
proposed the transformation of the United Nations Organization
into a world government. They suggested a six-point program.
They insisted: “First: It is futile to expect genuine peace until there
is put into effect an effective system of enforceable world law in the
limited field of war prevention” (p. xi). Second, a constitutional
prohibition of the use of violence by any nation against another is
required. Third, “World judicial tribunals to interpret and apply
the world law against international violence must be established
and maintained . . .” (p. xii). Fourth, a permanent world police
force must be established. Fifth, the complete disarmament of all
nations is essential. “Sixth: Effective world machinery must be cre-
ated to mitigate the vast disparities in the economic condition of
various regions of the world, the continuance of which tends to
instability and conflict” (p. xii).

To restructure this list according to the Biblical covenant
model, the authors were suggesting: 1) a constitution, established
by the United Nations (source of law); 2) world judicial tribunals
(hierarchy); 3) statutes (law); 4) a permanent world police force
that can impose sanctions; 5) wealth redistribution to benefit the
poor nations at the expense of the richer nations (inheritance/
disinheritance). Their fifth point, disarmament, could be placed
under point four of the Biblical covenant, sanctions: the transfer

11. Gen, Douglas MacArthur's administration of Japan in the post-World War
Tl era is one exception, according to rulers and ruled.
