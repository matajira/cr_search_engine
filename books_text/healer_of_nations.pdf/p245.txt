Government Foreign Aid Disinhertts the Faithful 231

Summary

The humanists in power have sought to transfer the inherit-
ance of Christians and externally honest people to State-approved
failures who have repeatedly broken God’s laws. They have done
this in the name of charity, humanitarianism, national self-inter-
est, and sound banking practice. Whatever the excuses, the poli-
cies have been wicked. Thus, they have also been impractical and
economically suicidal. Biblical Christianity is practical. Cove-
nant-breaking is impractical. When the judgment of God comes,
Christians had better remind themselves and those in power that
God is in control, that He governs by a hierarchy, that He gov-
erns in terms of His law, that He brings judgments in history, and
that the wealth of the wicked is in the long run stored up for the
just.

The covenant-breakers have been given enough rope to hang
themselves with. It is said that Lenin said that if the Communists
announced that they would hang all capitalists tomorrow, they
would trip over each other today to sell Lenin the rope. What
even Lenin did not predict is that they would sell it for long-term
credits at below-market interest rates.

The satanic deception of the West is about to be exposed in a
wave of crises. The wicked will not keep the inheritance of the
righteous. But it may take the righteous several generations of
covenantal faithfulness after the crisis to earn back what is right-
fully theirs as adopted sons of God. They have allowed wicked
people to act as representatives of the Evil One, and transfer their
inheritance to those under God’s curse. They will pay dearly for
their silence.

In summary:

1. The wealth of the wicked is stored up for the righteous.

2. Covenant-keepers are supposed to inherit the earth.

3. During periods of rebellion, covenant-breakers appear to
triumph.

4. God eventually cuts them down.

5. Satan fights this inheritance transfer process in history.

6. He deceives Christians into believing that there is no ethical
