Missionaries Make the Best Ambassadors 163

the unbelieving husband is sanctified by the wife, and the unbe-
lieving wife is sanctified by the husband; otherwise your children
would be unclean, but now they are holy” (1 Corinthians 7:14).
Holiness means sanctification: to be set apart. Obviously, Christi-
anity does not teach regeneration through marriage. It teaches
regeneration by grace through faith in Jesus Christ (Ephesians
2:8-9), Therefore, to speak of the unbelieving spouse’s being sanc-
tified by the believing spouse points to the existence of a legal, ex-
ternal, covenantal bond. The external blessings given by God to
the believing spouse flow to the unbelieving spouse.

So it is with nations. Unbelieving residents and citizens re-
ceive blessings because of the believing residents and citizens in
their midst. The classic Biblical example is Sodom: as few as ten
righteous men in the city would have preserved it from God's fiery
judgment (Genesis 18:32). The believers would have served as rep-
resentatives of God to the Sodomites.

We still see traces of the original covenant with God in the tak-
ing of oaths. An oath, like baptism, is a covenantal act. (We might
also say that baptism is a Biblical oath.) It is not an ecclesiastical
covenantal act, but rather a civil covenantal act. Those who
testify in a United States civil court are asked to swear: “I promise
to tell the truth, the whole truth, and nothing but the truth, so
help me, God.” A witness places his left hand on a Bible as he
affirms that he will abide by this oath. Only in the twentieth cen-
tury have courts made provisions for those who refuse to swear
with their hands on a Bible, but the civil sanctions against perjury
still apply, Bible or no Bible. The President of the United States
still swears his oath of allegiance to the U.S. Constitution with his
left hand on an open Bible, right hand raised. This is a civil cove-
nantal act, even though no one comments on it publicly. (It is a
theological embarrassment to humanists.) The President places
himself and the nation publicly under God’s covenant sanctions:
blessing or cursing.

As baptized Christians grow in number, as they seek to extend
the dominion of Christ’s kingdom in history, they begin to affect
politics, Steadily, they begin to take over the operations of the civil
