122 Healer of the Nations

“For He must reign till He has put all enemies under His feet. The
last enemy that will be destroyed is death” (1 Corinthians 15:25-26).

Thus, the search for perfect peace is legitimate, so long as we
bear in mind that it cannot be attained in sinful history:

“We have a strong city; God will appoint salvation for walls and
bulwarks. Open the gates, that the righteous nation which keeps
the truth may enter in. You will keep him in perfect peace, whase mind is
stayed on You, because he trusts in You. Trust in the Lorp forever,
for in YAH, the Lorn, is everlasting strength. For He brings
down those who dwell on high, the lofty city; He lays it low. He
lays it low to the ground. He brings it down to the dust. The foot
shall tread it down — the feet of the poor and the steps of the needy”
(Isaiah 26:1b-6; emphasis added).

Again, the issue is faith, not walls; ethics, not weapons. Perfect
faith brings perfect peace; and we are to seek perfect faith. We are
commanded to be perfect (Matthew 5:48), even though we in-
evitably sin in history: “If we say that we have no sin, we deceive
ourselves, and the truth is not in us” (1 John 1:8). The perfect hu-
manity of Jesus is our standard; we are told to pursue it, to be
conformed to the image of God’s Son (Romans 8:29). The search
for peace goes on, but Christ alone brings perfect peace through
perfect victory. He gives it definitively to His people when they are
converted. He brings it progressively in history. He fulfills it
finally at the last judgment.

Jesus did not come to bring us peace in this world. He brings
us peace with God which assures us of conflict with God’s enemies. Paul
wrote to the church in Rome regarding the coming judgment of
their enemies, “And the God of peace will crush Satan under your
feet shortly. The grace of our Lord Jesus Christ be with you.
Amen’ (Romans 16:20), Ethical peace with God brings ethical
warfare with the anti-Christian forces of this world. This is why
Christ is Prince of peace, but only on His own terms, with peace
defined as the victory of Christ’s people in history. Anything less
than this victory is the world’s peace, not Christ’s, and He came to
destroy this world’s peace. Citing Micah 7:6, Christ said:
