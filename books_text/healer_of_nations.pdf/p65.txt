All Nations Under God Si

that do not honor human boundaries. Another major area of dis-
pute is disputes over boundaries. Each ascending level of civil
government must be weaker, dealing with fewer and fewer issues.
Localism is Biblical, but so is the appeals court system of Exodus
18. To assume that a nation is the final boundary for every dispute
is to create an incentive for war, just as the same assumption
brings conflict to provinces, counties, and townships.

To Inherit the Earth

Christ’s victory at Calvary in principle reclaimed the owner-
ship of the whole earth from Satan, and it legally transferred this
certificate of ownership to God’s people.® The certificate of owner-
ship is the New Testament itself. The New Testament is a cove-
nant: a legal document. It assigns the inheritance to God’s
adopted sons (John 1:12). The boundaries of this nation of nations
in principle are the whole earth. Though sin will restrict a perfect
working out in history of these boundaries, the goal of Christians
all over the world should be to work toward this goal: the creation
of a formally covenanted confederation of Christian nations under
God. God’s kingdom must triumph in history over Satan’s king-
dom. Christ’s nation of nations must triumph over Satan’s empire
of empires.

Christ’s work on Calvary is the legal foundation of Christian
internationalism. Without it, the world would still belong to
Satan. Humanist internationalism and humanist nationalism are
both attempts to deny the work of Christ at Calvary. This is why
Christians should recognize the legitimacy of a search to create an
international confederation of covenanted nations that confesses
as a unit that Jesus Christ is both Savior and Lord of nations.?
This will not be a one-State world, but it will be a nation of cove-
nanted nations. It will be the historical fulfillment of Christ’s
prophecy to the Pharisees.

8. Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth, Texas: Dominion
Press, 1987), ch. 5.

9. Gary DeMar, Ruler of the Nations: Biblical Blueprints for Government (Ft.
Worth, Texas: Dominion Press, 1987), pp. 5458, 93-95.
