 

Subject Index

covenant-keepers, 150
foreign policy, 29
foreign service, 171
God's nation, 157
governments, 26,
Jonah, 74
missionaries, 37
national leader, 8-9
President, 177, 214
silent authority, 31
victory, 105
residents, 163
resistance, 59, 206
responsibility, 85, 97, 177, 217, 276-77
resurrection, 2, 46
retreat, 272
Revel, Jean, 106
revival
Constitutional revision &, 301
denial of, 77
International relations &, 60
major one needed, 20, 33-34
national restoration, 8
‘West's need, 41
revolution, 213, 228
Rhodes, Cecil, 182-83
Rhodes Scholarships, 183
rich and poor, 253
right reason, 90
robes, 289
Robin Hood, 222
Rockefeller, David, 224-25
Rockefeller, Nelson, 203, 64-65
Rockefeller Panel, 134, 239
Rohatyn, Felix, 224
Roman Empire, 30, 75, 89, 206
Roman Republic, 88
Ropsevelt, Franklin, 116-17
Rotary Club, 195
Rothbard, Murray, 74
Rothschild, Lord, 183
Round Table Group, 181, 184, 185
Rousseau, 268

331

Rushdoony, R. J.

Biblical history, 2

source of law, 87

unified godhead, 67, 70-71
Ruskin, John, 182-83
Russia, 116, 117, 167-68

SALT Il, 177
sacraments, 73
sacrifice, 205
sacrifices, 32
Sagan, Carl, 109, 110
salvation, 45, 143
salvation by law, 109
sanctification, 39, 162-63
San Diego, 50
Satan
Adam &, 83
colonialism, 169
crushed, 122
deceptions, 218
empire, xii, 10, 14, 40, 71, 209
empire vs. kingdom, 47
equal time for, 93, 200, 255
first revolutionary, 72
goal of victory, 237
hierarchy, 72
imperialism, 167
kingdom of, 23, 75, 91
leaven, 61
missionaries vs,, 37
representatives, 219
vs. Satan, 209-10
scarcity, 69
Schaeffer, Francis, 200, 201, 235-36

" schism, 265

schools, B5

Scott, Otto, 184-85
screening, 150
secession, 58-59, 63, 72
secret societies, 150
self-government, 76, 82
Senate, 208
