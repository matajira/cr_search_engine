60 Healer of the Nations

Christian federation downward. Covenant law is binding, and
local governments must respect the decisions of the authorized ap-
peals court of the civil government. The central government does
have the power of the sword, and it is exercised within geographi-
cal boundaries. Local governments, however, do have the legal
right to specify their original terms of entering into the covenant.
We should not expect Christian nations to enter into covenant
with one another without qualifications. It will take centuries of
experience to increase mutual trust. Also, local civil governments
will retain the lion’s share of tax revenues, and also retain local
military units. The local militia, not state, national, or interna-
tional armies, will be the fundamental military unit. In this sense,
modern nationalism is both temporary and perverse. We need to
return to something closer to medieval feudalism: international in
scope, yet primarily local in taxation and authority.

The creation of a bottom-up international Christian con-
federation will take time, probably centuries. Today, there are few
explicitly Christian nations, and the United States is no longer
officially one of them, although it still is covenantally bound by
the terms of its original Christian confession, and therefore is re-
garded by God as being in rebellion against the inescapable terms
of His covenant. The United States is suffering from national
apostasy, as are all of the nations of Europe. Christian interna-
tionalism is many decades if not centuries down the road.

I am sketching what the Biblical model for international rela-
tions might look like after a worldwide Christian revival. A blueprint
alone is not the answer to today’s immediate problems; a blue-
print is not even a house site, let alone a construction crew. Today,
Christians should do what we can to resist every attempt of the
humanists to build their pagan one-world order. But we also need
to know where God says that we ought to be headed. We need to
recognize humanism’s perverse imitations of Biblically valid ar-
rangements. Just because humanism’s imitation is perverse, we
should not conclude that the Bible’s standard is also perverse.
When it comes to fighting the humanists’ one-world order, let us
never forget: “You can’t beat something with nothing.”
