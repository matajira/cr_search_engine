What the Church Can Do 265

But if Christ’s perfection as a God-incarnate human being is
imputed to an individual, then it is also imputed to the Church
International. The Church is Christ’s body; therefore, Paul says,
“there should be no schism in the body, but that the members
should have the same care for one another” (1 Corinthians 12:25).
This body is unified in principle; it is also diverse in principle, just
as God the Trinity is both unified and diverse.

What is true in principle concerning a person’s status as a God-
redeemed person is to be manifested progressively in his lifetime,
and manifested finally at the last judgment. He is saved, is being
saved, and will be saved, all by the sovereign grace of God.

What is true in principle concerning the Church International's
status as a God-redeemed institution is to be manifested progres-
sively in history, and manifested finally at the last judgment. It is
unified and diverse, is becoming unified and diverse, and will be
revealed as unified and diverse, all by the sovereign grace of God.

The problem for the Church International is that it has over-
emphasized diversity at the expense of unity. It has sinned. It has
needed creedal precision, but it has not needed schism. Like Solo-
mon, who was born of a marriage established through adultery
and murder, so the Church has achieved greater theological preti-
sion through schism. The wisdom of Solomon was not a justifica-
tion for adultery and murder. The increasing theological precision
of the Church is no justification for schism.

“Can You Shoot Straight?”

If you were a soldier in the front line, sitting in your trench,
and you saw the enemy’s troops coming over the ridge 500 yards
in front of you, would you ask the man at your left about his theol-
ogy or his ability to shoot straight?

Tf you were suffering from a brain tumor, and you heard about
a physician who specializes in operating on the brain, would you
consult him about his theology or his former patients’ rate of
survival?

There is such a thing as common grace. Pagans have been
given skills by God so that they can serve His people. When we
