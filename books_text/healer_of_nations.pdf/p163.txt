All People Are Citizens of Two Worlds 149

terms of the covenant. The question must then be raised: What
about inclusion in the civil covenant?

Over time, men lose faith in the doctrine of natural law. They
recognize that ethics can never be neutral, which means that law
can never be neutral. Laws include and exclude certain types of
behavior. Thus, as the earthly, visible, and institutional develop-
ment of Satan’s kingdom and God’s kingdom takes place in his-
tory, it becomes more and more difficult for anyone to make deci-
sions consciously outside the two kingdoms. As men seck to live
lives consistent with their covenantal status as either covenant-
keepers or covenant-breakers, this is reflected in political life.
Those who become dominant politically, either through con-
spiracy or power (the humanist ideal) or through steady conquest
through preaching, example, and godly service (the Christian
ideal), will eventually seek to exclude their rivals from participat-
ing in political affairs.

Exclusion is inherent in any process of inclusion. By defining
legal citizenship, a society necessarily must impose some principle
of exclusion. For example, in the United States anyone who has
been convicted of a felony is prohibited from voting for the rest of
his life. This is a pagan recognition of the covenantal status of citi-
zenship. Christians should seek to overturn this law constitu-
tionally. Anyone who has’ repented publicly (“word”) and has
made restitution to the victim (“deed”) should be restored to full
citizenship. This would make visible politically Christ’s process of
ethical and judicial restoration. But the “church” of modern de-
mocracy is often unforgiving.

Elites in Political Life

The elitism of modern democracy is the product of a group of
conspirators who have successfully closed membership to the seats
of real power. This is the satanic principle of “the inner ring,” as
CG. §. Lewis has called it.6 Members initiate new members

6. C. S. Lewis, “The Inner Ring,” in Lewis, The Weight of Glory and Other Ad-
dresses (New York: Macmillan, 1980).
