What the State Can Do 297

The United Nations has always served as a major spy center
for the Soviet Union, for its accredited representatives possess
diplomatic immunity. The highest-level Soviet spy ever to defect
to the U.S. was the Under Secretary General of the UN. When he
first was sent to the UN in 1966 as chief of the Security Council
and Political Affairs Division of the Mission, he reports, “I had a
staff of more than twenty diplomats. I soon discovered that in fact
only seven were real diplomats; the remainder were KGB or
GRU [military intelligence—G.N.] professionals under diplo-
matic cover.”20 Of course, a lot of their time was spent spying on
the real Soviet diplomats.

Shevchenko says of the required loyalty oath to the UN:
“Every Soviet national who takes the organization’s oath must
commit perjury. Before an individual’s candidature is submitted
by the Soviet Union to the Secretariat's Office of Personnel, that
individual undertakes an obligation to do his or her best in the in-
terests of the Soviet Union and to use his or her prospective job to
achieve this purpose.”24

It is time to get out of the UN.

The Structure of Authority

Every foreign policy official in every nation must serve solely
at the pleasure of the person or agency that is constitutionally em-
powered with responsibility for national foreign affairs. There
must be no legal restraints on hiring or firing by the head of State.
In the United States, this means the President. If he wishes to re-
tain the services of specialists, then this is his decision. National
policy must be conducted by the authorized agent, with constitu-
tionally authorized consent of the elected representatives in cer-
tain specified areas. The conduct of foreign policy, like the con-
duct of military affairs, must be lodged in one person. The kings
of Israel and Judah conducted foreign policy as monopoly pre-
rogatives, and the same centralization has existed ever since.

20, Tbid., p. 131,
21, Iid., p. 221.
