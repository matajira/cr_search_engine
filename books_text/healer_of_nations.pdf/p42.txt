28 Healer of the Nations

was over taxation (1 Kings 12). Jeroboam then created a new wor-
ship system based on golden calves, in order to keep his people
from going to Jerusalem to worship (1 Kings 12:25-33). His tax re-
bellion created a new civic nation, which God subsequently judged
as a separate covenantal entity: for example, by the Assyrian cap-
tivity (2 Kings 17). Israel and Judah possessed a common race,
common language, and common verbal confession in God and
God's law (though not common ritual), but they were not one na-
tion after Jeroboam’s revolt. They did not share in taxes, geogra-
phy (boundaries), and citizenship requirements.

Tribute as God's Judgment

The distinction in the Bible between tribute imposed by a con-
quering pagan nation (2 Kings 15:19-20) and taxes imposed by the
king (1 Samuel 8) indicates the civic limit of nationhood. Tribute
is what is paid temporarily to another nation. It does not mark the
creation of a new nation, for there is no common language, bor-
ders, confession of faith (oath), or citizenship. An invading nation
could impose tribute only in its capacity as a God-authorized
scourge, but it was only femporarily authorized by God to bring His
judgment against His nation in history. As Isaiah prophesied con-
cerning temporarily victorious Assyria over the rebellious north-
ern kingdom of Israel: “Woe to Assyria, the rod of My anger and
the staff in whose hand is My indignation. I will send him against
an ungodly nation, and against the people of My wrath. I will
give him charge, to seize the spoil, to take the prey, and to tread
them down like the mire of the streets” (Isaiah 10:5-6). It was God
who had scattered the nation of Israel, not Assyria, yet Assyria ar-
rogantly boasted of her own sovereign might:

By the strength of my hand I have done it, and by my wisdom,
for I am prudent, Also I have removed the boundaries of the peo-
ple, and have robbed their treasuries; so I have put down the in-
habitants like a valiant man. My hand has found like a nest the
riches of the people, and as one gathers eggs that are left, I have
gathered all the earth (Isaiah 10:13-14a).
