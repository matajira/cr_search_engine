8 Healer of the Nations

licly (covenantally) at their meetings, “I’m an alcoholic.” There is
no true release from bondage by means of the AA program,
though there is valid and desirable day-by-day release. Of course
it is better to live next door to a sober honest pagan than a drunk.
It is best to live next door to a sober, honest Christian.

In any case, we have yet to find a way to convert a whole soci-
ety of rebellious, broken, sick people by preaching common hu-
manist principles of restoration without God. Once a formerly
Christian society has become universally rebellious, the only way
to restore the nation to spiritual health and the peace of God is
through a national crisis accompanied by Christian revival.

The crisis is coming. Is revival coming, too?

Heal My Nation

What would you tell the political leader of a nation who came
to you with this story?

“My nation is in shambles. Our enemies have five times as
many nuclear weapons as we do. My country’s people drink too
much. Millions of them are on hard drugs. We are headed for a de-
pression. Everyone is in debt up to his ears, especially the govern-
ment. The nations that owe us money are about to default. Our
allies are threatening to leave us. I want to get my nation’s life back
together. What should I do?”

If you are a Christian, you would tell him the same thing you
would tell the individual whose life is in shambles. The first thing
he needs to do is accept Jesus Christ as his personal sin-bearer be-
fore God, the Savior, Lord, and Master of his life. Then you
would tell him that he needs to join a Bible-believing church, get
baptized, and take the Lord’s Supper on a regular basis. Then he
needs to read the Bible to discover and apply in his life the princi-
ples of Christian living.

You would tell him that he needs to do this because he is his
nation’s representative before God. He needs to serve as a model, He
needs to go before God in the name of his nation the way that
Abraham went before God to plead for his nephew Lot's city,
