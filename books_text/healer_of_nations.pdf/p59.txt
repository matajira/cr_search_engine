All Nations Under God 45

ing from man (Acts 17:25). “And He has made from one blood
every nation of men to dwell on all the face of the earth, and has
determined their preappointed times and the boundaries of their
habitation” (Acts 17:26).

If we were not all sons of God, we would not all be responsible
before God as rebellious sons. We would not be sons of Adam. We
would not need forgiveness. But every person on earth is born in
sin, as the covenantal heir of Adam, and therefore all men need
redemption. Thus, we are all sons of God by birth—disinherited
sons. It is foolish to deny the universal Fatherhood of God and the
universal brotherhood of man. There is a universal Creator-
Father, and He has disinherited His rebellious children. All men
really are brothers . . . just like Cain and Abel.

What God does is to adopt rebellious people back into His
covenantal family. “But as many as received Him [ Jesus Christ],
to them He [the Father] gave the right to become children of God,
even to those who believe in His name, who were born, not of
blood, nor of the will of the flesh, nor of the will of man, but of
God” (John 1:12-13). Salvation is by adoption, We are adopted
and therefore we have become the sons of the inheritance. So, there
are two kinds of sons: disinkertted sons and adopted sons. The
dividing mark is not blood; the dividing mark is ethics. God in His
sovereign grace imputes (declares judicially)? to adopted sons that
they now and forevermore possess the righteousness of Jesus’ per-
fect humanity (though never Christ’s divinity), which removes
forever from these once-disinherited sons God’s prior imputation
of Adam’s sin (Romans 4:8).3 Christians have become reconciled
sons of God (Romans 5:10-11).

The Quest for Spiritual Unity

The quest for spiritual unity among all redeemed men is not
only legitimate; it is required by God. Jesus prayed: “I do not

2. John Murray, Redemption Accomplished and Applied (Edinburgh: Banner of
Truth, 1961), pp. 123-25.

3. John Murray, The Imputation of Adam's Sin (Nutley, New Jersey: Presbyter-
ian & Reformed, [1959] 1977), ch. 4.
