262 Healer of the Nations

And this commandment we have from Him: that he who loves
God must love his brother also (1 John 4:21).

Christians preach the coming of the kingdom of God. What is
this kingdom? Paul said that “the kingdom of God is not food and
drink, but righteousness and peace and joy in the Holy Spirit”
(Romans 14:17). “Therefore let us pursue the things which make
for peace and the things by which one may edify another”
(Romans 14:19).

Precision Through Division

But here we encounter the first problem: the pursuit of two
goals that have long divided Christians. We are to pursue both
peace and edification. But as we have sought to edify and clarify,
we have found that others do not see things our way, One Christian’s
clarification is another Christian's proof of heretical deviation.

The Bible is a complex book. It baffles the best and the bright-
est. Its message of salvation is clear enough for children to grasp,
Jesus said repeatedly, but it is also sufficiently complex as to
divide the greatest minds in the history of the Church.

The progress of the creeds, which is one of the best pieces of
evidence for the progress of the Church in history, has also come
at the expense of unity. In fact, warfare — literal and figurative —-
has been a major motivation for improving the creeds. They have
served Christians as intellectual and even cultural weapons
against heretical enemies, and these enemies have sometimes not
been readily identifiable as non-Christians. In fact, the creeds
have always come as a means of exclusion as well as inclusion.
The writers have, in effect, drawn lines in the dirt and have an-
nounced: “Step across that line and you're out of the game.” Then
the others draw their lines in the dirt and say the same thing.
Millions upon millions of people have stepped across each other's
lines for about 2,000 years. If we were to take each other's excom-
munications seriously, all of us are out of the game, and always
have been.

Still and all, the lines in the dirt harden. Unlike lines in the
