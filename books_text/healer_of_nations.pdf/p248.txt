234 Healer of the Nations

them useful to covenant-keepers until the day of eternal wrath
begins.! The gospel is designed to extend the dominion of covenant-keepers
as Goa’s authorized representatives on earth, not to create the basis of a
permanent cease-fire agreement between covenant-keepers and
covenant-breakers until Jesus comes to judge the world. The gos-
pel is not the manifesto of a stalemate religion.?

Twentieth-century Christians have become even less confident
about the power of Christ's gospel to transform society. They have
lost the vision of international victory that used to motivate Chris-
tian missions programs. The vision of the international kingdom
of God that captured the minds of evangelists in the early Church
(Mark 16:15), continued through the Middle Ages, and even
lasted well beyond the Protestant Reformation,? faded rapidly
with the coming of Darwinism. The growing cultural inferiority
complex of Christians combined with the growing cultural superi-
ority complex of Darwinists to create a perverse coalition against
consistent, world-transforming Christianity, which preaches the
existence of progressive earthly manifestations of the kingdom of
God in history, in every area of life.

A Defensive Mentality

Christians, especially conservative Protestant Christians,
have adopted a defensive mentality. They see the theological
savages all around them, and they want to “form a circle with the
wagons.” First, they want the boundaries of church walls to form a
barrier, and they are willing to confine the effects of the gospel in-
side those walls in order to placate the savages (temporarily), who
deeply resent such an invasion of their culture. Second, they trust
in national boundaries to protect them from foreign-based un-

1. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987), chaps. 7, 8.

2. Gary North, Backward, Christian Soldiers? A Manual for Christian Reconstruction
(Tyler, Texas: Institute for Christian Economics, 1984), ch. 11: “The Stalemate
Mentality.”

3. J. A. De Jong, As the Waters Cover the Sea: Millennial Expectations in the Rise of
Anglo"American Missions, 1640-1810 (Kampen, Netherlands: J. H. Kok, 1970).
