Introduction 7

If you are a Bible-believing Christian, you would see this as an
opportunity to share the gospel with him. You would tell him that
the first thing he needs to do is accept Jesus Christ as his personal
sin-bearer before God, the Savior, Lord, and Master of his life.
Then you would tell him that he needs to join a Bible-believing
church and be baptized. He needs to take the Lord’s Supper, pre-
ferably each week. He also needs to read the Bible to discover and
apply in his life the principles of Christian living.

But what if he replied, “I want to get my life back together, but
leave out all this Jesus stuff. That's a lot of nonsense”; what then?

Would you spend a lot of time with him in an attempt to find
workable answers to his problems— answers that are acceptable to
him in the midst of his crisis—but without ever mentioning his
sin, Jesus, repentance, the church, the Bible, or Biblical princi-
ples of righteousness? If so, why? Isn't your task to get him to face
his real problem, his ethical rebellion against God? Why not re-
mind him of the external judgments of God in his life? Why not
tell him of God's plan of salvation—God’s comprehensive salvation?+
Why mislead him into thinking that there is some common, uni-
versally acceptable humanistic formula for successful, God-
honoring living apart from Jesus Christ?

No such formula exists today or ever has existed. It is a myth.
Yet, Christians today desperately want to believe in this myth, for
they believe that the existence of a formula for neutral civil gov-
ernment enables them legitimately to transfer the power and re-
sponsibility for exercising civil judgment to God's covenant-
breaking enemies.

Now, it is true that non-Christians can be partially restored ex-
ternally and visibly to a better outward way of life. Alcoholics
Anonymous has proven this. The AA program enables full-time
drunks to become full-time sober citizens. But mere sobriety does
not bring people permanently into favor with God. For the re-
mainder of their lives, AA members introduce themselves pub-

4. Gary North, Is the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1987), Appendix C.
