What the State Can Do 309

Communist leaders. No more publicized meetings between high
Western officials and high Soviet officials. Would Churchill have
posed with Hitler for photographs in 1942? “Don't you know
there’s a war on?”

Sanctuary should be given to defectors. The Chinese govern-
ment on Taiwan offers a fortune in gold to any pilot who defects
by flying his jet to Taiwan. So should Korea. So should Japan.

Summary

George Washington ran the Department of State with six peo-
ple. All of them were loyal to the U.S. That was a long time ago.

Modern foreign policy has become humanistic, bureaucratic,
secretive, and elitist. It has also become internationalist. It seeks
to accommodate the growth of the Soviet Union, and most impor-
tant, to confuse Americans about why the diplomats are so
accommodating.

The goal of Christian international relations should be to
transfer most of these activities to realms outside the direct au-
thority of the State. It is to substitute missions and business for
diplomacy in the vast majority of cases.

The way to achieve this goal is to transform the State Depart-
ment. This can be done through a program of systematic budget
cuts, the enforced publishing of documents, rewards and bounties
for those who will expose.Department cover-ups, transfers, salary
cuts, early retirement, the establishment of new hiring standards
—linguistic fluency rather than bureaucratic examinations— and.
the introduction of careful security checks.

‘The goal is to shrink the State Department back to its pre-1933
size, though perhaps with some minimal elasticity because of the
greater number of nations in the world today. Those Foreign Ser-
vice Officers needing better information abroad could ask local
businessmen and missionaries.
