190 Healer of the Nations

officers, including the Commandants of West Point.)

There would be this requirement for an identified enemy na-
tion to be granted trading rights with a Christian nation or a cove-
nanted group of Christian nations: the enemy would agree to
allow Christian missionaries to enter the nation and establish
churches without any interference, and that agreement would
have to be honored. Like skid row bums who are required to listen
to a sermon before they are handed their bowl of soup, so will the
identified enemy nations be required to allow missionaries to
preach the gospel to their people in order to gain access to Chris-
tian markets. We will find out how hungry they really are for the
products of the free market.

Businessmen as Diplomats

We need an army of skilled, practical people to take the
message of freedom to the whole world. They need not be profes-
sionally trained diplomats. In fact, they must not be professionally
trained diplomats, for the humanists long ago captured the key
training institutions. What we need are people who can offer the
pagan world what the pagans want: the prosperity that Western
capitalism has produced. They want Western science and technol-
ogy. Christians must learn and never forget the history of Western
science and technology: it was the product of Christianity.®

The businessman understands the ways of market competi-
tion, The free market is God’s way to expand the honest man’s
wealth. The free market grew out of Christianity, and only out of
Christianity. This proven fact of history” outrages most human-
ists and their Christian allies in theologically compromised Chris-
tian colleges, whose faculty members all attended humanist-

25. Susan Huck, “Lost Valor,” American Opinion (October 1977); “Military,”
ibid., (July/August 1980).

26. Stanley Jaki, The Road of Science and the Ways to God (Chicago: University of
Chicago Press, 1978); Science and Creation: From eternal cycles to an oscillating universe
(Edinburgh and London: Scottish Academic Press, [1974] 1980).

27. Max Weber, The Protestant Ethic and the Spirit of Capitalism (New York:
Scribner's, [1904-5] 1958). See also Gary North, “The ‘Protestant Ethic
Hypothesis,” Journal of Christian Reconstruction, I (Summer, 1976), pp. 185-201.
