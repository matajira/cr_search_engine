198 Healer of the Nations

Thus, to make a covenant with a pagan foreign nation, the
Christian nation would have to affirm formally the equality of the
pagan nation’s god with the God of the Bible. This is why all
covenants between pagan nations and Christian nations are
abominations.

In the ancient world, warfare between cities necessarily in-
volved warfare between the gods of those cities. To establish a
peace treaty between cities, the two had to establish an alliance
between their respective gods. A sacrifice was required, exactly as
the sacrifice of Jesus Christ is necessary to establish a permanent
peace treaty between God and rebellious men, Fustel de Coulanges
wrote in 1864 of these ritual treaties: “When a war did not end by
the extermination or subjection of one of the two parties, a treaty
of peace might terminate it. But for this a convention was not su-
flicient; a religious act was necessary. Every treaty was marked by
the immolation of a victim. To sign a treaty is a modern expres-
sion; the Latins said, strike akid. . . . The ceremony of the treaty
was always accomplished by priests, who conformed to the ritual.
... These religious ceremonies alone gave a sacred and in-
violable character to international conventions.”!

For several generations, Christians have avoided thinking
deeply about the theological aspects of treaties. One way that they
have avoided such intellectual problems is by refusing to acknowl-
edge the legitimacy of the idea that any nation can be identified as
a Christian nation. If no nation can ever be identified covenant-
ally as a Christian nation, then Christianity has nothing to say
one way or the other concerning the theological distinctions be-
tween permanent covenants and impermanent alliances among
nations, or concerning proper and improper treaties (covenants).
Such treaties are assumed to operate in terms of a hypothetical
“law of nations” or “national self-interest,” both of which are hypo-
thetically neutral theologically and covenantally. The myth of
neutrality again rears its ugly head in the affairs of Christian men.

1. Numa Denis Fustel de Coulanges, The Ancient City: A Study on the Religion,
Laws, and Institutions of Greece and Rome (Garden City, New York: Doubleday
Anchor, [1864] 1955), pp. 207-8.
