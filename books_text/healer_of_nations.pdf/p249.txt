Conclusion 235

pleasantness: cheap foreign imports, low-wage immigrants fleeing
Communism or socialism, Communist terrorists, and Commu-
nist troops. (Borders do not restrain the AIDS lentivirus: judg-
ment is still coming.) They cling to humanist nationalism—
founded on the two-fold myth of permanent pluralist politics and
unbiased judicial neutrality —as fiercely as drowning men grasp-
ing at life preservers. But humanistic nationalism is a life pre-
server with a leak in it, the product of a splintered sixteenth-
century church and eighteenth-century Enlightenment ideology.

There can be no “equal time for Jesus” in a pluralist society,
for Jesus demands covenantal obedience rather than parity with
Satan. So covenant-breakers who have control over the legisla-
tures and courts always strive to keep the effects of the gospel
tightly controlled. In any case, humanist nationalism is being
overwhelmed historically by humanist internationalism, in the
form of the Communist world empire, Humanist nationalism is a
weak reed to lean on.

Christians have lost a vision of earthly victory. They have no
vision of a world progressively transformed by the gospel, or na-
tions brought under Christ's covenant, one by one, or the Church
of Jesus Christ speaking with one voice, as it did in Acts 15, or a
confederation of Christian nations welcoming newly converted
nations into the commonwealth of redeemed mankind. They have
no confidence in the Bible as a reliable intellectual weapon against
the self-certified humanists who occupy the seats of influence and
power in every nation. They view their earthly labors as histori-
cally futile, to be swallowed inevitably by the triumph of anti-
Christian forces throughout the world. They are afraid even to
announce the covenant of Christ as morally binding on their own
nations, let alone on the whole world.

They have become confused by the inescapable choices that
God’s providential history imposes on them. They have accepted
the humanists’ lie that Jesus Christ has no legitimate authority
over the civil affairs of men, or none that men can ever perceive in
history. They write: “There have been times of very good govern-
ment when this interrelationship of church and state has been
