TABLE OF CONTENTS
Editor’s Introduction 6.0.2.6... 606: s cece cece seen eens ix

Part I: BLUEPRINTS

 

 

 
  
 
 

Introduction. ..........0 000s eee eee ee
1. God Created the Nations .......
2. All Nations Under God ......... 6.60 c cece eens 43
3. God’s World Government
Through Biblical Law ...........0.00000005 81
4. Nations With Rival Covenants
Are Always at War... 6.0. cece cece eee ee 99
5. God’s Legacy of Progressive Peace . 121
6. All People Are Citizens of Two Worlds . 139
7. Missionaries Make the Best Ambassadors . 155
8. Businessmen Make the Best Diplomats . 195
9, Alliances Are Not Covenants .............555+ 197
10. Government Foreign Aid

Disinherits the Faithful .. 217

Conclusion... ........: eee e eee eee

 

 

Part Il; RECONSTRUCTION

 
 
 
 

11. What the Church Can Do..............000005 259
12. What the Individual Christian Gan Do ......... 275
13. What the StateCan Do ....... 2 cece e eee ees 285
Bibliography ............ StL
Scripture Index 315
Subject Index....... . 319
What Are Biblical Blueprints? ..........60..e 0 sees 335

vil
