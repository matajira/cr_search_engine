Nations With Rival Covenants Are Always at War 105

to Me every knee shall bow, every tongue shall take an oath. He
shall say, ‘Surely in the Lorn I have righteousness and strength. To
Him men shall come, and all shall be ashamed who are incensed
against Him’” (Isaiah 45:22-24).

Until all knees are bowed before Christ, the war will go on.

Peaceful Warriors

Warfare in the Bible is covenantal. So is peace. Redeemed
men are at peace with God through the covenant. He has sworn
an oath in His own name that He will honor His covenant. “I
have sworn by Myself; the word has gone out of My mouth in
righteousness.” Redeemed men therefore rely on a totally sover-
eign God as their absolutely sovereign ruler. He has promised to
bring victory to His people— victory over indwelling sin, public
victory at the end of time, and public victory as God’s lawful repre-
sentatives even in the midst of the earthly war. Isaiah promises in
his great passage on millennial peace:

Then justice will dwell in the wilderness, and righteousness re-
main in the fruitful field. The work of righteousness will be peace,
and the effect of righteousness, quietness and assurance forever,
My people will dwell in a peaceful habitation, in secure dwellings,
and in quiet resting places (Isaiah 32:16-18).

Peace is said specifically to be the work of righteousness. God
promises His covenantal sanction of blessing to covenanted na-
tions that are faithful to the terms of the covenant, His revealed
law. Peace can be achieved only alongside the extension of right-
eousness in history. It is a gift to the faithful.

Thus, Christians can and must claim peace as their goal, but
only on God’s terms. It must be the product of covenantal faithful-
ness throughout the world. If armies are not to cross borders,
there must be a covenantal peace offensive. This peace offensive is
the preaching of the gospel. It must not be the false promised
peace of perpetual coexistence with evil. It is the Limited but growing
peace that God grants to victors in the spiritual wars of life.
