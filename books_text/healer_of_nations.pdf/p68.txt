54 Healer of the Nations

of kings.” The argument went something like this. “The king holds
an office established by God. There is no earthly sovereign over
the king, He answers only to God. God holds him accountable,
but no other institution of government holds him accountable.
The king, and only the king, possesses this unique authority
directly under God.”

In 1688, the Glorious Revolution swept James II from office
and established the sovereignty of the British Parliament. Parlia-
ment immediately adopted a very similar view of its authority. No
human institution could challenge the absolute sovereignty of
Parliament, William Blackstone, in his legendary Commentaries on
the Laws of England (1765), wrote concerning Parliament’s sover-
eignty: “The power and jurisdiction of Parliament is so transcen-
dent and absolute, that it cannot be confined, either for causes or
persons, within any bounds. . . . It hath sovereign and uncon-
trollable authority in the making, confirming, enlarging, restrain-
ing, abrogating, repealing, reviving, and expounding of laws,
concerning matters of all possible denominations, ecclesiastical or
temporal, civil, military, maritime, or criminal: this being the
place where that absolute despotic power, which must in all gov-
ernments reside somewhere, is entrusted by the constitution of
these kingdoms. . . . It can, in short, do everything that is not
naturally impossible; and therefore some have not scrupled to call
its power, by a figure rather too bold, the omnipotence of Parlia-
ment. True it is, that what the Parliament doth, no authority
upon earth can undo.”

The American Revolution began a decade later: a Biblical
reaction to such humanistic arrogance. Nevertheless, Parliament
did not soon abandon its assertion of total authority, despite its
steady loss of actual authority. As late as 1915, the distinguished
British legal scholar (and defender of limited government) A. V.
Dicey made this statement in the first chapter of his monumental
work on Constitutional law: “The principle of Parliamentary sov-

11. Cited by A. V. Dicey, Introduction to the Study of the Law of the Constitution (8th.
ed.; Indianapolis, Indiana: Liberty Classics, [1915] 1982), pp. 4-5.
