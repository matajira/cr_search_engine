All People Are Citizens of Two Worlds 143

Eternal Citizenship: Inclusion Requires Exclusion

Some people are included in God’s eternal kingdom, while
others are forever excluded. This eternal separation will take
place when God judges all people at the last day. He will judge
them as members of nations, for He is the Judge of the nations
(Matthew 25:31-35).

What must be understood is that this covenant structure is
truly covenantal. Many people have interpreted such verses as
Matthew 25:31-35 as referring exclusively to individual salvation,
but God’s covenant encompasses more than simply individual
souls. It also involves institutional salvation, meaning restoration.
It involves each of the three covenantal institutions ordained by
God: church, State, and family. To restrict the meaning of salva-
tion of the human soul is to misread Scripture.

God judges between two kinds of nations. This division re-
flects the two kinds of people who dominate the nations in history.
Paul writes that there are only two kinds of people in this world:
covenant-keepers and covenant-breakers.

Covenant-breakers are those who “are the enemies of the cross
of Christ, whose end is destruction, whose god is their belly, and
whose glory is in their shame—who set their mind on earthly
things.” Covenant-keepers are those who can honestly say that
their “citizenship is in heaven,” and who publicly acknowledge
that they “eagerly wait for the Savior, the Lord Jesus Christ, wha
will transform our lowly body that it may be conformed to His
glorious body, according to the working by which He is able even
to subdue all things to Himself.”

There is a final transformation of covenant-keepers at the final
judgment. What they are in principle in history they become in
fact in eternity.

Now this I say, brethren, that flesh and blood cannot inherit
the kingdom of God; nor does corruption inherit incorruption.
Behold, I tell you a mystery: We shall not all sleep, but we shall all
be changed—in a moment, in the twinkling of an eye, at the last
trumpet. For the trumpet will sound, and the dead will be raised
