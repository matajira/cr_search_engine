All Nations Under God 67

fools. The stairway to heaven was not physical; it was metaphysical.
It had to do with questions of metaphysics: the underlying nature
of the universe. And what the Tower symbolized was that man is
the connection between heaven and earth. The Tower would serve as a
religious center that would enable them, as they thought, to storm
the gates of heaven and seize the Tree of Life, from which men
were excluded (Genesis 3:24). This is the goal of every form of
pagan works-religion, and it was their goal as well. The actual
technique they used was magic. (Occultism and magic underlay
the Nazi ideal, too.)2¢

We need to notice also that they wanted to make themselves a
name. They did not want to be given a name by God, or wear His
name. They wanted to make a name for themselves, to glorify
themselves. They wanted to define themselves by themselves in
terms of themselves. God told Moses that His name is “I am whoT
am” (Exodus 3:14). The people of the Tower wanted to announce,
“We are who we are.” They wanted to be God.

Magic proclaims, “As above, so below.” Man tries to manipu-
late the creation and even God. Magic affirms a continuity of being.
Man and God are both part of the same chain of being.*! There-
fore, mankind seeks unity. As Rushdoony says, if man is god,
then this godhead must be unified — unified not just ethically, but
metaphysically, at the core of humanity’s being.*? In principle,
man and God are one, this humanistic theology teaches.

This unity of God and man is a religious presupposition of
most paganism. Historically, we call this monism. The underlying
reality of man’s unity must become a political, historical reality.
Without this political unity, mankind is not fully developed,
meaning fully evolved. Thus, humanism’s internationalism is

30. Nicholas Goodrick-Clarke, The Occult Roots of Nazism: The Ariosophists of
Austria and Germany, 1890-1935 (Wellingtonborough, Northamptonshire: Aquarian
Press, 1985).

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), pp. 36-39.

32. R. J. Rushdoony, This Independent Republic: Studies in the Nature and Meaning
of American History (Fairfax, Virginia: Thoburn Press, [1964] 1978), p. 142.
