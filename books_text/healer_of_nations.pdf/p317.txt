What the State Can Do 303

FBI. The other agency would be responsible for classifying it or not.
There should be a tracer on every such document. Some general
or admiral would become responsible for improper withholding of
data, should it be shown to be improper. His career for improper
classification of documents would be on the line. This way, he
would not be tempted to classify a document as top secret just be-
cause some bureaucrat ‘n the State Department wants to cover his
own flanks, A military officer is directly under the President and
other officers. If a piece of paper is vital to the nation’s security, let
the military or the FBI be its protector. The crucial requirement is
that no bureaucrat within the State Department should be allowed
to cover up the secret deals made by the State Department.

It takes 25 years for many documents to be published. Some
never are published. Some are published in edited versions. The
Yalta Conference papers are the best example. This quest for
secrecy is a bureaucratic religion. It is the breeding ground of con-
spiracy, and international conspiracy at that. Teams of editors
from outside the Department should be given access to most files
immediately, with classified files to be made public or transferred
to another agency within six months, Any Foreign Service Officer
who balks at opening the files is demoted or fired. He can become
a third-level clerk in the embassy in Chad.

There is no doubt that there is an available pressure group
that would back such a move: historians. Conservative, libertar-
ian, and Marxist historians all would like to get their hands on
these files. Let them. Come one, come all: hurry, hurry, hurry, get
on board every historian’s dream of a lifetime, namely, access to
documents never before seen, and which will make lifetime repu-
tations for those fortunate historians who get access to them first.
Offer access to foreign historians, too.

One requirement would be made: a photocopy. of every cited
document would have to be made, and the author would be re-
quired to pay for a microfiche of these documents. This would
cost a couple of hundred dollars. The microfiche cards would be
placed on file in the State Department and the Library of Con-
gress, so that critics could come in and check out the context of
