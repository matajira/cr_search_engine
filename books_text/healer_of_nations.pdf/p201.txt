Businessmen Make the Best Diplomats 187

This message placed the United States as a potential military
defender of the territorial sovereignty of nations in this hemis-
phere, although the language of conflict is absent. Monroe’s
speech produced no negative response by Congress, and his for-
mulation of U.S. foreign policy was continued by John Quincy
Adams. Monroe’s Doctrine was clearly an act 0. rgional isolation-
ism, It was an extension of George Washington’s Farewell Address
isolationist policy, which President Adams referred to in his
March 15, 1826 message to the House of Representatives, and in
which he also quoted extensively from Monroe's 1823 speech.2 He
reminded Europe to stay out of our affairs, just as the United
States had stayed out of Europe's. If our neutrality is not
respected by Europe, “we might be called in defense of our own
altars and firesides to take an attitude which would cause our neu-
trality to be respected. . . .”2

Such an “armed and dangerous’ isolationist outlook horrifies
the Anglophile internationalists who write today’s textbooks.
“Without the British fleet,” the Rockefeller Panel insisted,
“Monroe’s declaration to protect the hemisphere against Euro-
pean imperialisms would have been merely rhetorical, a promise
incapable of being made good.” But, the report complains, this
fact was somehow forgotten through most of the nineteenth cen-
tury. It was Britain, not God, who protected the United States,
and American statesmen forgot this key fact of international rela-
tions: “The American statesmen of those years liked to think that
they were favored by providence, not by human arrangements.”

The idea that human agreements come as part of God’s provi-
dence never occurs to them. That God in His grace might provi-
dentially favor a covenantally faithful nation is an obscene
thought to the humanist. God’s visible blessings testify to the ex-
istence of a covenant; this covenant also warns of a coming judg-
ment: God’s cursing of a covenantally rebellious nation. The

21. Ibid., pp. 903-5.
22. Ibid., p. 904.
23. Prospect for America: The Rockefeller Panel Reports (Garden City, New York:
Doubleday, 1961), p. 10,
