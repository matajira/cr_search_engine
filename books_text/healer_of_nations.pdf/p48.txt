34 Healer of the Nations

Establishing a Christian Nation

God establishes nations, kingdoms, and all other units of civil
government. Men, as God’s delegated sovereigns in history (Gen-
esis 1:26-28), create civil governments as agents of God, but not as
original creators.

What would be the universal features of a Christian nation?
The same five features that we see in every government. The
Christian nation would be fully aware of what God requires. It
must be stressed from the outset that the creation of such a nation
could be accomplished only as a result of the widespread work of the
Holy Spirit, not through some bureaucratic, top-down, coercively
imposed order on a non-Christian majority by a Christian minor-
ity. This covenantal transformation of a nation must be the sover-
eign work of God, with men as delegated agents, not the work of
men apart from the outpouring of the Holy Spirit. (Because there
has been so much confusion about what the Biblical Blueprints
Series is really advocating, I suggest that you reread the last two
sentences three times.)

J. A Common View of God

All citizens would acknowledge the sovereignty of the
Trinitarian God of the Bible. Only He would be publicly wor-
shipped. Only He would be called upon publicly in times of na-
tional crisis, Only He would be given public praise in times of na-
tional deliverance. His Word, the Bible, would be acknowledged
as the source of the nation’s law-order.

2. A Common System of Courts

There must be ways of settling public disputes. A Christian
nation would follow the example of Exodus 18 and establish an ap-
peals court system. Men would be free to do as they please unless
they violated a specific piece of Bible-based legislation or a specific
Biblical injunction that the Bible says must be enforced by the
civil government. Government is therefore a bottom-up structure,
with the individual operating as a lawful sovereign agent under
