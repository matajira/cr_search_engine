 

What the State Can Do 307

Broadcasting

The next step would be to begin a coordinated counter-propa-
ganda effort. Propaganda is a pejorative word used to describe
our enemies’ public announcements. Every nation uses it. The
question is: Is it true? The Soviet Union uses lies—disinforma-
tion? — as no other nation in history has used them. It is a nation
based on lies, The West must counter lies with truth, and then tell
the truth daily about the Soviet Union.

Broadcast more shows into the Soviet Union. Forget about
jazz broadcasts, except as lures to increase the propaganda rat-
ings. Play suppressed classical Russian music. News broadcasts,
yes. Even news unfavorable to the West. True news, in other
words. We can afford to tell the truth. The Soviets cannot deal
with truth.

News on Sovict deaths in Afghanistan should be daily offerings
— or whatever other up-to-the-minute embarrassment that would
create resentment of the. Soviet people against their leaders.

Offer history lessons on the Russian past. Read literature
from the pre-Bolshevik era, Read Solzhenitsyn’s novels, Read all
three volumes of The Gulag Archipelago. Read samizdat literature:
the underground typewritten literature written by Russians,

Then focus on the Soviet Union’s suppressed nationalities.
Read their suppressed national literature, stressing religious his-
tory and any heroes who fought for liberty against Soviet tyranny.
Let representatives of the various emigré groups select the liter-
ature and supply the announcers.

Let Russian Orthodox Church services be broadcast around
the clock, or close to it. Let Bible readings be beamed in around
the clock.

Let radio broadcasts into Islamic nations give the death totals in
Afghanistan day by day. “The Soviets are killing Muslims” should
be the full-time refrain of U.S. broadcasts into the Middle East, day
after day. Have interviews with the victims of Soviet aggression.

29. Richard H. Shultz and Roy Godson, Dezinformatsia: Active Measures in
Soviet Strategy (New York: Pergamon-Brassey’s, 1984).
