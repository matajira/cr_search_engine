332 Healer of the Nations

separation, 143, 144
serpents, 177, 275
sewage, 50
Shultz, George, 229, 268
shoebox evangelism, 280
Singlaub, John, 205
slaves, 147
sleepwalking, xii, 82
Smith, Adam, 17
Snoot, Dan, xiii, 182
socialism, 50
Sodom, 163, 209
Sohn, Louis, 95-96
Solomon, 211, 265
Solzhenitsyn
appeasement, 246
courage, 258
dogs & wolf, 212
humanism, 38-39
Kennan’s failures, 242
loss of Communist faith, 39
repentance, 39
surrender of West, 246
warning to West, xii
U.S. diplomats, 107
Western intellectuals, 38-39
sons of God, 45
South Africa, xi, 183, 267-68
sovereignty
international, 203
national, 27-28, 31, 55, 203
Parliamentary, 54
sovereignty of God
absolute, 44, 55
acknowledgement of, 139
over all things, 44-45
over individuals, 107
over nations, 29, 34, 40, 45, 100,
105
spies, 297
stalemate, I11, 234
Stalin, Joseph, 184, 212
standards, 85

state
authority of, 4, 5
covenant, 5, 27
neutrality of, 5, 7
neutrality myth, 6
outward peace, 108
savior, 278
salvation, 108-9
State Department
budget cuts, 304-6
civil service, 305
exams, 306
files, 302-4
growth of, 15
Publication, 7277, 14
security risks, 290-91
states imitate church, 4
statism, 4, 17
Streit, Clarence, 63
strength, 126, 129
surrender, xii, 103, 114, 118, 158, 246
Superbowl, xii
Sutton, Ray, xi, 20-21, 26
Switzerland, 31
sword, 20, 29, 30, 58, 123, 156

Taft, Robert, ix
‘Tambo, Oliver, 268
tariffs, 279
taxation, 27-28, 180
taxes, 61
teaching, 211
‘Tehran Conference, 16
temple, 33, 47
tenacity, 247
theocracy
defined, 56
hostility to, 96, 199-200
international, 56
personal, 199
Schaeffer vs., 201
natural law or, 92
Third World, 222
