Government Foreign Aid Disinherits the Faithful 225

sentence during a 1980 television documentary, “Well, I have to
say that having been in this business now for 33 years, I find one
has to be very pragmatic and flexible about these things, and that
relations with governments regardless of the political label that’s
attached to them depends to a large extent on people and human
relationships, and just because a country is technically called
communist doesn’t mean that a capitalist institution such as the
Chase Bank can’t deal with them on a mutually beneficial basis,
and indeed we do deal with most of the so-called communist coun-
tries of the world on a basis that has worked out very well, I think,
for both of us.”!* Such remarkable language! The Soviet Union is
just a “so-called communist” country.

The television broadcast also observed: “Private citizen David
Rockefeller is accorded privileges of a head of state. He is un-
touched by customs or passport officers and hardly pauses for
traffic lights. Rockefeller is the supreme example of how multina-
tional companies do business.”!5

In the super-secret journal of the US-USSR Trade and Economic
Council (Oct./Nov. 1977), General Electric Corp president Reginald
H. Jones warmly admitted that his firm’s business dealings with
the Soviet Union stretched back to 1922. He also affirmed: “It is
our experience that the Soviets are meticulous in observing every
contractual provision, once a contract has been signed, and they
expect the same from a supplier. This has a great deal of positive
significance for a supplier, particularly as it affects the specified
terms of payment” (p. 17).

The Communists are wise as serpents. They pay their bills on
time. They therefore get the external blessings of God to that ex-
tent: cooperation from the biggest corporations and banks in the
West.

Yet the $80 billion never seems to get repaid. So Western
banks make more loans to them, at interest rates below the free

14. Transcript, “Bill Moyers’ Journal: The World of David Rockefeller,” pro-
duced by WNET, New York, p. 12. Public Broadcasting System air date: Feb. 7,
1980.

15. Bid., p. 19.
