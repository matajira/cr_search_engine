Alliances Are Not Covenants 211

him while we strengthen our home defenses.

Something received must be accompanied by something given
up. The price the dictator pays is continued openness to mission-
aries. If he shuts the door to the gospel, he takes the risk of sinking
or swimming alone. He must learn that Christianity is not the
threat to him that the pagan empire is. If he hates God more than
he fears the empire, let him fall. Every nation that refuses to ac-
cept missionaries from a Christian nation should be regarded as
having broken its treaties with the Christian nation, Any continu-
ing alliance is established on a “strictly business” ad hoc basis.

We must make judgments regarding the reliability of our
allies. If they are generally reliable for the Christian nation’s interests,
then Christian foreign policy should refrain from pressuring them
to conform to our outlook and practices at the expense of their
perceived national interest. Only if we have our own house in
order can we in good conscience offer assistance to an ally regard-
ing his ethical weakness. Again, Matthew 7 is the place to begin
discussing making good judgments:

“Or how can you say to your brother, ‘Let me remove the speck
out of your eye’; and look, a plank is in your own eye? Hypocrite!
First remove the plank from your own eye, and then you will sce
clearly to remove the speck out of your brother’s eye” (Matthew
7:4-5).

We must teach by example, giving our allies confidence that
we take God’s law seriously, and that Ged therefore takes us seri-
ously. Nations, like people, are more easily persuaded by success
than by shouting. Imitation is entered into more enthusiastically
than necessary submission. Japan’s post-War revival economic-.
ally is proof that they took Western capitalism far more seriously
than the mixed-market, part-socialist capitalist West took it.

Most of this “speck-removing” process should be private rather
than public, quiet rather than loud. Missionaries, businessmen,
technicians, medical care specialists, teachers, and other privately
financed professional servants should spearhead peaceful change.
Free trade is also a good teacher. Solomon hired Hiram of Tyre to
