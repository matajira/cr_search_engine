6 Healer of the Nations

Healer of the Nations will not be a popular book within most
Christian circles, for it pushes the denial of the myth of neutrality
into the “prohibited” zone of the nation-state. I have gone to the
Bible to see what is required for international relations. I have
assumed that the Bible, not John Locke, is the proper Christian
standard. I have relied on the Bible, not President George Wash-
ington’s Farewell Address, as the final court of appeal. What will
be shocking to many conservatives is that I have assumed that the
Bible is alone authoritative, the final court of appeal, even when it
conflicts with the U.S. Constitution. I have begun with this pre-
supposition: the Bible is always the supreme law of every land,
the standard by which God judges all nations in history and at the
end of time, and this God-established fact should be publicly
affirmed, nation by nation, in history, as well as at the end of
time.

Because the Bible is the standard across borders and through-
out history, in heaven and in earth, it is sovereign. History is the
progressive working out of God’s decree. God has decreed that
there will be a progressive conforming in history of every human
institution to the requirements of His Word. God’s kingdom will
progressively be established visibly in history. Many Christians
do not believe this. No humanist believes this. Therefore, there
has been a working alliance—philosophically, politically, histori-
cally, culturally—between many Christians and all humanists.
This book is a direct challenge to this long-term alliance within
the field of international relations.

How to Be Healed

If a person came to you and said the following, what would
your say in reply?

“My life is in shambles. I drink too much. I just got fired from
my job. I am in debt up to my ears. I can’t pay my mortgage.
We're going to lose our home. My wife is threatening to leave me.
My teenage daughter is running around with a hoodlum. I want to
get my life back together. What should I do?”
