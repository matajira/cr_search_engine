Bibliography 313

Pipes, Richard. Survival Is Not Enough: Soviet Realities and America’s
Future. New York: Simon & Schuster, 1985.

Quigley, Carroll. The Anglo-American Establishment. New York:
Books in Focus, 1981.

——--—___ Tragedy and Hope: A History of the World. in Our
Time. New York: Macmillan, 1966.

Radosh, Ronald. Prophets on the Right: Profiles of Conservative Critics
of American Globalism. New York: Simon & Schuster, 1975.

Revel, Jean Francois. How Democracies Perish. Garden City, New
York: Doubleday, 1984.

Rockefeller Panel Reports. Prospect for America, Garden City, New
York: Doubleday, 1961.

Rushdoony, Rousas J. The Biblical Philosophy of History. Phillips-
burg, New Jersey: Presbyterian & Reformed, (1969) 1979.

. The One and the Many: Studies in the Philosophy of
Order and Ultimacy. Fairfax, Virginia: Thoburn Press, (1971)
1978.

———_-___. The Nature of the American System. Fairfax,
Virginia: Thoburn Press, (1965) 1978.

Scott, Otto. The Other End of the Lifeboat. Part 1. Chicago: Regnery,
1985.

Smoot, Dan. The Invisible Government. Boston: Western Islands,
(1962) 1977.

Solzhenitsyn, Aleksandr. “Misconceptions about Russia Are a
Threat to America.” Foreign Affairs, Vol. 58 (Spring 1980), pp.
797-834,

———_—____. Solzhenitsyn at Harvard. Washington, D.C.: Ethics
and Public Policy Genter, 1980.

. Solzhenitsyn: The Voice of Freedom. Washington,
D.C.: American Federation of Labor and Congress of Indus-
trial Organizations, 1975.

Sutton, Antony. The Best Enemy Money Can Buy. Billings, Mon-

tana: Liberty House, 1986.
