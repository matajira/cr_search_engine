God's Legacy of Progressive Peace 137

A Christian nation is not to seek to impose its national will on
other nations. It can legitimately offer to help other nations pre-
pare to defend themselves. It is not the task of a Christian nation
to “save another nation from itself.” If God is about to bring a
pagan nation under judgment, and that nation will neither repent
before God nor appeal to God’s representative nation or nations
for assistance, then it should be allowed to fall to the invader. The
empire will eventually overextend itself. The righteous nation will
not perish. The problem comes when righteous people live in un-
righteous nations. Sometimes, they are carried into captivity for a
while.

We need ambassadors who understand the theological nature
of the confrontation, and who will press the claims of Christ.
Diplomats can negotiate the details with covenanted allies, pagan
allies, and even neutrals. They should not be allowed to negotiate
with hostile nations. They do not possess the required skills.

We must strengthen our allies and weaken our enemies. We
must be guided by a vision of victory. We must work day and
night for victory. Nothing else can succeed. There is no comprom-
ise with evil. There is no substitute for victory.

In summary:

1. The goal of international relations is peace with God, not
peace with Satan,

2. International relations must face the fact that nations seek
to impose their will on other nations, just as God seeks to impose
His will on His enemies.

3. Christian nations must trust in God, not weapons.

4. We are not to go unarmed into battle, however: David and
Goliath,

5. Christian foreign policy is to seek the surrender of the na-
tions of the world to God through an international covenant.

6. Peace is the fruit of a previous victory, though not usually a
military victory.

7. Perfect peace comes only at the last judgment.

8. Perfect peace is a legitimate goal in history, but impossible
to attain.

9. Perfect peace, perfect victory, and perfect humanity are
