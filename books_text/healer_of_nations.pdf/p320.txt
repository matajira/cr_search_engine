306 Healer of the Nations

ing two competing offices, there will be less likelihood of success-
ful “stonewalling” by bureaucrats. The two security check pro-
grams will be in competition.

The Foreign Service Examinations

In order to reduce the power of the primary screening tool in
the selection of the foreign policy bureaucracy —the Foreign Ser-
vice examinations, written and oral—the President must unilater-
ally establish a new rule by Executive Order: any required written
examination would have to be taken every five years by all exist-
ing senior Foreign Service personnel. Thus, the ability of those
who have already passed the exam to reduce the number of com-
petitors by making future exams more difficult would disappear.
If passing the exam really is vital for effective Foreign Service, then
any Foreign Service expert who cannot pass the same exam given
to applicants must be regarded as incompetent, and would have
to be fired until he can pass the exam. (He would also lose his
seniority in the interim.) Under this rule, we would learn, much
to our “amazement,” that Foreign Service experts now on the gov-
ernment payroll would discover almost overnight that the written
exam really is not that important, and can safely be dropped.

Counter-Attack

A whole series of steps is absolutely necessary. First, we must
break diplomatic relations with every Communist country. This
symbolically announces that we regard them as declared enemies
of the West, and not fit to place their buildings on our soil. An em-
bassy building is legally foreign territory in a nation; they should
not have access to any such sanctuary.

This will serve notice to captive peoples that the United States
has had enough. It will also indicate to Communist leaders that
we have at last begun to take seriously their declarations of per-
petual war against the West.
