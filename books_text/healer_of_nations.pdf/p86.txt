72 Healer of the Nations

not all-powerful, all-seeing, and all-knowing. He must rule his
followers indirectly, unlike God who is totally sovereign over his-
tory. Satan must rule through a top-down chain of command. He
needs bureaucracy in order to maintain his personal control, in
contrast to Christianity’s decentralized, bottom-up appeals court
system.%® Satan’s need for bureaucracy manifests itself whenever
Satan attempts to develop his alternative to a Biblically valid
Christian internationalism that manifests God’s kingdom.

These two organizational principles are at war throughout his-
tory: Satan’s empire and Christ’s kingdom. Satan would prefer to
centralize power at the top of the hierarchy, while Christ, as abso-
lute Sovereign, can safely allow decentralized power in the hands
of individuals. Christ does not fear a “palace revolt”; Satan does.
Satan was the first revolutionary, and his empires are always
threatened by revolution from below. He will not allow secession
in history by his subordinates; Christ does allow it, and did allow
it in the garden of Eden. Secession has a price, but it is basic to
the preservation of freedom. The disappearance of the doctrine of
the legal right of political secession marks the advent of tyranny in
our age. That modern man cannot mentally imagine a world that
would honor this fundamental political right testifies to the suc-
cess of the theology of political centralization in modern times.

A Symbol of Unity

Holy Communion is not simply a symbol of unity for the
church of Jesus Christ; it is a covenantal reality. It is as real as a
prayer to God. Prayers are symbolic representations of man’s sub-
ordination to God. Prayers are also events that do invoke the
response of a real God in history. Thus, when Christians take
communion, they testify to the existence of a one-world Christian
order, and they also empower themselves to participate in the cre-
ation of this one-world Christian order.

The United Nations was designed to fulfill a similar purpose
for the humanist internationalists. It has failed in this purpose,

38. Ray R, Sutton, That You May Prosper, op. cit., pp. 90-51.
