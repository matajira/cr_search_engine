Government Foreign Aid Disinherits the Faithful 223

accounts for under 1 percent of the recipients’ national income—
too small to measure statistically.? Aid to smaller nations has a
greater impact, of course, but so do business investments.

Thus, foreign aid hurts taxpayers in the “donor” nations, hurts
the private sector in the recipient nations, strengthens the power
of the State in both donor and recipient nations, and cannot be
shown to increase per capita wealth significantly.

Foreign aid is simply a way for Satan to postpone briefly the
triumph of covenantally faithful people over covenant-breakers.
But as a symbolic gesture to evil, it suits his purposes.

The Banking Crisis

Beginning in the 1970's, the large multinational banks began
to loan huge quantities of OPEC oil money deposits to the Third
World. This money, along with the deposits of national residents,
has led to about a trillion dollars, worldwide, in loans to Third
World nations. Estimates of bad loans now run as high as 50 per-
cent, though no one really knows. About 40 percent of the trillion
dollars in loans are in Latin America, which makes this money
unlikely ever to be repaid. What everyone knows is that in early
1987, Brazil, in debt to the West by over a hundred billion dollars,
suspended payments on this debt. Other nations may follow
Brazil’s lead. Eventually, there will be a default. These nations
cannot repay. The key questions are these: When, under what
form, and how will the banks cover the losses?

Citicorp, the largest bank in the United States, and about
tenth largest in the world {the top four banks are Japanese), ad-
mitted in early 1987 that $3 billion of its $15 billion portfolio of for-
eign loans are unlikely to be repaid. As a result, it “wrote down”
(admitted to a loss of) $2.5 billion in the second quarter of 1987,
This may only be the beginning of major problems for the bank.
(This is the “Wriston bank.” Walter B. Wriston masterminded its
growth in the late 1960’s and 1970’s by emphasizing foreign loans
over domestic loans. He is the son of Council on Foreign Rela-

9. P. T. Bauer, Equatity the Third World and Economic Delusion, p. 101.
