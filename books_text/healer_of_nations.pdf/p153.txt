I._Transcendence/Immanence

6
ALL PEOPLE ARE CITIZENS OF TWO WORLDS

For many walk, of whom I have told you often, and now tell
you even weeping, that they are the enemies of the cross of
Christ: whose end is destruction, whose god is their belly, and
whose glory is in their shame—who set their mind on earthly
things. For our citizenship is in heaven, from which we also
eagerly wait for the Savior, the Lord Jesus Christ, who will trans-
form our lowly body that it may be conformed to His glorious
body, according to the working by which He is able even to sub-
due all things to Himself (Philippians 3:18-21).

The first point of the Biblical covenant is the doctrine of the
true transcendence of God. God is wholly distinct from His crea-
tion, yet wholly present with it, All the events of history must be
interpreted in terms of God and God’s sovereign plan for history.
The world centers around God. The world is theocentric.

Men are always in the presence of God. He is everywhere.
But His presence is always mediated by the covenant. It is not
enough that people are always in God’s presence; they must
acknowledge Him as sovereign. People are required to make a
choice in life between two declared sovereigns in the universe:
God and Satan. They must make a covenant. There is no escape
from the covenant, It is never a question of covenant vs. no cove-
nant. It is always a question of which covenant. We are born phys-
ically into Satan’s covenant, our legacy from Adam, Whether im-
plicitly or explicitly, we affirm his covenant by natural birth. Only
by the grace of God are we adopted into God’s family (John 1:12).

139
