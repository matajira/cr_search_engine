128 Healer of the Nations

ing up offensive weapons. They were to avoid displays of great
wealth. They were to study the law of God all of their days, in
order to keep from becoming arrogant. It is arrogance in the face
of God that is the sure path to destruction.

Arms control is a legitimate goal in a Christian com-
monwealth. Weapons should be defensive. The enemy must know
that an attack will be met with overwhelming resistance, but that
if they leave us alone militarily, they will be left alone militarily.
(They will never be left alone evangelistically; they must live with
that pressure until their conversion or their self-destruction.) The
Bible is confident that the Word of God is sufficiently powerful to
bring down kingdoms. Offensive weapons are not the tools of do-
minion. The gospel and Biblical law are the tools of dominion.?

The call for organized, formal mutual disarmament is non-
sense. Each nation is responsible for its own policies. Disarma-
ment schemes in the years between World War I and World War
II benefitted Germany and Japan’s war plans, not the West's. The
tyrants built up their war machines, and the democracies let their
military forces rust. Yet as it turned out, the Axis powers lost the
War. This should not be ignored. They gained too much confi-
dence in their weapons. They went on the offensive, and lost.

Is unilateral disarmament appropriate? No, but unilateral
shifting from an offensive strategy and arsenal to a defensive strat-
egy and arsenal is not only legitimate, it is required by God. Hu-
manists refuse to accept this, which is why the West continues to
rely on a policy of nuclear retaliation against civilian populations
— Mutual Assured Destruction (MAD)—rather than on a space-
based defense that would make intercontinental ballistic missiles
strategically obsolete. MAD is not a defense policy, but rather a
revenge policy.

The Bible clearly teaches arms control. Arms control is to be
self-imposed on the king of a righteous nation, Covenantal faith-
fulness must become the foundation of a successful military

2. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Insti-
tute for Christian Reconstruction, 1987).
