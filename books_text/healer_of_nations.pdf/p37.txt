I. Transcendence/Immanence

1
GOD CREATED THE NATIONS

And in the days of these kings the God of heaven will set up a
kingdom which shall never be destroyed; and the kingdom shall
not be left to other people; it shall break in pieces and consume all
these kingdoms, and it shall stand forever (Daniel 2:44).

The first point of God’s covenant is His transcendence over
the world and His presence with the world. He is distinct from the
world, yet He is present with everyone throughout history and
eternity. The doctrine of the sovereignty of God is fundamental.
God created the world and presently sustains the world (provi-
dence). He will judge the world at the last day.

Thus, we see the transcendence of God over history. The king-
dom of God is supreme; the kingdom of Satan is doomed. The
kingdoms of autonomous man will all fall. Like men, men’s king-
doms are mortal. They are true kingdoms, for God has raised
them up for His purposes, They are also mortal, for God tears
them down for His purposes. This was God’s warning to Nebu-
chadnezzar in the dream that Daniel interpreted for the king
(Daniel 2). “He removes kings and raises up kings” (Daniel
2:21b). He is transcendent over kings and kingdoms.

Nevertheless, He is also present with all men. They cannot
escape the creation’s testimony to His presence. “For since the cre-
ation of the world His invisible attributes are clearly seen, being
understood by the things that are made, even His eternal power
and Godhead, so that they are without excuse” (Romans 1:20). He

23
