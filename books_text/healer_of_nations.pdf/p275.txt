What the Church Can Do 261

Church, other than this increase in disunity, that stands as a
greater condemnation of the Church. Our creeds have improved
(at least until the mid-seventeenth century). Our charitable giving
has improved. Our missions have improved. Our technical means
of communicating the gospel have improved. More people can
hear the gospel in one evening because of television satellite
broadcasts than could have heard it in a century of missions.
These improvements have not been straight-line phenomena, but
there has clearly been general improvement.

The exception? The Church’s failure even to pursue the ethi-
cal and institutional goal of Church unity. Here the Church has
been a public failure. The pagan world uses it against Christ. “If
the gospel were clear, you people wouldn't be at each other’s
throats all the time!” The Church has been such a failure for so
Jong in this area of its ministry that Christians seldom even dis-
cuss disunity as a major area of failure. They praise local church
independency and denominational independency as if independ-
ency were God's preferred way. They look at this awful failure
that keeps getting worse, and call it a blessing, “Praise God! We
Christians give divided testimony to the world!”

Until there is visible evidence that the churches of this world
are heading toward cooperation, we know that the hoped-for mil-
lennial blessings of God are not close at hand. There will be no
worldwide comprehensive blessings without worldwide compre-
hensive obedience by Christians. We are told to be at peace with
each other, to love one another. Evangelicals turn to the Gospel of
John as their most effective witnessing tool. What did John tell us?

“A new commandment I give to you, that you love one
another; as I have loved you, that you also love one another. By
this all will know that you are My disciples, if you have love for
one another” (John 13:34-35).

“This is My commandment, that you love one another as I have
loved you” (John 15:12).

For this is the message that you heard from the beginning, that
we should love one another (1 John 3:11).
