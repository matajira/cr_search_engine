278 Healer of the Nations

is not practicable. There is no way for a people to speak in the
counsels of the nations except through that political authority that
has control over the inner processes of its life. This is a question of
the inevitable association of responsibility with power. To conduct
foreign policy means, at bottom, to shape the behavior of a nation
wherever that behavior has impacts on its external environment.
This is something only a government can do. For that reason, only
a government can speak usefully and responsibly in foreign
affairs.3

In other words, there was no valid American foreign policy
until, say, sometime around 1889, the year arbitrarily designated
by Richard W. Leopold as the transition year between old
fashioned U.S. foreign policy and modern U.S. foreign policy.*

This is nonsense, pure but not simple. It is the attempt of
State-worshipping humanists to grab international relations away
from Christian missions and free market international trade. It is
the assertion that civil government is the only proper institution
for shaping “the behavior of a nation wherever that behavior has
impacts on its external environment.” It is faith in the State as
savior, which alone possesses the “political authority that has con-
trol over the inner processes of its [a nation’s} life.”

It is imperative that individual citizens and people operating
through voluntary associations recapture international diplomacy
from the State and the State’s self-certified foreign service profes-
sionals. If Christians refuse to do this, then we will continue to be
burdened by the foreign service monopolists. We cannot expect to
fight something with nothing.

Missions at Home and Abroad

Individual Christians must begin to support missionaries, for-
eign exchange students, the translation of Christian literature into
foreign languages, and international charities that send food and

3. George Kennan, Realities of American Foreign Policy (Princeton, New Jersey:
Princeton University Press, 1954), pp. 42-43.

4, Richard W. Leopold, The Growth of American Foreign Policy: A History (New
York: Knopf, [1962] 1969), p. viii.
