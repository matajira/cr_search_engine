216 Heater of the Nations

2. Biblical covenants have five points.

3. To make Biblical a covenant among institutions, the repre-
sentatives must formally profess faith in the trinitarian God of the
Bible.

4, In the ancient world, a treaty between cities always in-
volved a ritual bond between the gods of the cities.

5. Modern Christians avoid thinking about such issues.

6. They assume that no nation can be covenantally Christian.

7. They implicitly accept the myth of neutrality.

8. Every institution must be brought into conformity with the
Bible’s requirements for it.

9. There will be no peace on earth until this is accomplished in
history.

10. Peace between nations with rival gods is therefore a cease-
fire, not Biblical peace.

11. National leaders must distinguish covenanted friends from
enemies, and temporary professed allies and neutrals from perma-
nent enemies.

12. Temporary alliances with temporarily friendly non-Christian
allies can be used to spoil Satan’s household.

13. The principle is “divide and conquer.”

14, We must teach by example.

15, We must work to weaken our enemies,

16. We are at war.

17. We must cease capitulating to Soviet demands.
