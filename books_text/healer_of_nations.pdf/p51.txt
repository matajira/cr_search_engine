God Created the Nations 37

treaties. Such treaties are valid only between or among Christian
nations. The means of securing the legal basis of such treaties is
missionary activity. Christians who are citizens in a Christian na-
tion must send out representatives to preach the gospel to all men.
They must send out missionaries to non-Christian societies who
will represent various church governments, but who would also
represent the particular Christian nation as @ guest in the foreign na-
ton. He would have to learn to operate in terms of two legal sys-
tems—a very difficult skill. His job is the conquest of Satan, but
not the subjection of the particular foreign nation to the nation of
his earthly citizenship. At most, he would work toward the inte-
gration of that nation into a covenanted federation of Christian
nations.

We now have some idea of what a Christian nation should be.
What does the Bible teach about international relations?

Christians Must Be Consistent

The establishment of a godly foreign policy must be part of a
program of comprehensive redemption. It is our responsibility as
Christians to seek to reform every area of life. No area of life is
outside of God’s two-fold judgment: cursing or blessing. No
aspect of life is religiously neutral. Thus, for the formerly Chris-
tian West to continue to conduct its foreign policy on the assump-
tion of the myth of neutrality is suicidal. The more consistent hu-
manist systems—the empires of history~will always seek to
swallow up those less consistent humanist societies that believe
that a permanent peace treaty with evil is possible and desirable.
God tells us what is in the hearts of empire-builders: rape. God will
sometimes permit this because of the faithlessness of His people:

I have likened the daughter of Zion to a lovely and delicate
woman. The shepherds with their flocks shall come to her. They
shall pitch their tents against her all around. Each one shall pasture
in his own place. Prepare for war against her. Arise, and let us go

10. Ultimately, this means an International Church government: see Chapter
Eleven.
