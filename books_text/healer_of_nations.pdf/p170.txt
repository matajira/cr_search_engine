156 Heater of the Nations

for I am gentle and lowly in heart, and you will find rest for your
souls. For My yoke is easy and My burden is light” (Matthew
11:28-30). A bondservant is under a hierarchy. The bondservant
to Christ is under Christ's hierarchy. His sign of bondage is a
yoke: the yoke of the covenant.

God’s Holy Mountain

The mountain is God’s dwelling place, His house. It is the
place where He teaches us His ways. Out of Mt. Zion goes God’s
law. It is the covenant law of God that subdues the nations. This
process of subduing the nations begins with self-government
under law, but it does not end there.

What is this mountain? Clearly, it is the institutional church.
The institutional church preaches God’s Word. Out of the institu-
tional church flows the law of God. The passage does not teach
that the institutional church will control the world. On the con-
trary, it is the people who obey the law that flows out of the church
who will subdue the earth. It is God’s Word that brings Christians
victory over His enemies. His Word is His sword. Jesus warned:
“Repent, or else I will come to you quickly and will fight against
them with the sword of My mouth” (Revelation 2:16). Again,
“Now out of His mouth goes a sharp sword, that with it He should
strike the nations. And He Himself will rule them with a rod of
iron. He Himself treads the winepress of the fierceness and wrath
of almighty God” (Revelation 19:15). The power of God’s Word in
history is awesome.

The nations will come into the New Jerusalem, the Church,
the true mountain of God (Revelation 21:9-27). This process has
already begun, It began at Pentecost. Pentecost was the first pub-
lic manifestation of the arrival of God's international kingdom.
Devout Jews had come to Jerusalem “from every nation under
heaven” (Acts 2:5b). The Word of God flowed out to them
through the preaching of the gospel in every tongue. The Holy
Spirit began His work of international healing. Like the oil that
was mixed in with the peace offerings of the Old Testament (Lev-
iticus 7:13), so the Holy Spirit was poured out on these represen-
