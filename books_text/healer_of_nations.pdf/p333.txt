SUBJECT INDEX

Abel, 45
abortion, xii, 268
Abram, 197, 209
acid rain, 50
accountability, 131
Acheson, Dean, 239
Adam

covenant &, 83, 93

fall of, 83

God’s claims on, 24

heirs of, 45

judge, 136

king, 47

tested God, 200-1
Adams, John Quincy, 187
adoption, 45, 49, 146, 230
African National Congress, 268
AIDS, 235, 242, 249, 284
Afghanistan, 66, 104, 241
Alcoholics Anonymous, 7-8
alliances

peace, 159

confusing, 111

covenants and, 197-216

entangling, 11

temporary, 14, 208
ambassadors

allegiance of, 134

Christ's, 164-67, 233

Christians as, 46, 233

one-third, two-thirds, 134

model, 158

official, 170-71

Christians as reconcilers, 233

witnesses, 157-59

see also missionaries

American Legion, 57
American Revolution, 54
anarchy, 74, 169
antinomianism, 86
appeals, 61
appeasement, 246

ark of covenant, 159
Armey, Richard, 293
arms control, 127-29
Assyria, 28, 218
Assyrians, 237

Adantic community, 168
Aurelius, 89

banking crisis, 223-27
baptism, 159-64
Barron, Bryton, 16, 302
battlefield, 141
Bauer, P. T., 192, 220-21
Beelzcbub, 91, 209
Benjamin (tribe), 58-59
Bible
authoritative, 6
answers to life, 7
complex, 262
legal standard, 35
nation-state &, 34
oath in court, 163
standard of God's judgment, 6
Western Civilization, 108
Biblical law
covenant &, 93
creation, 176
jurisdiction, 82
kingdom, 81

319
