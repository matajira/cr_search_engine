264 Healer of the Nations

Having then gifts differing according to the grace that is given to
us, let us use them: if prophecy, let us prophesy in proportion to
our faith; or ministry, let us use it in our ministering; he who
teaches, in teaching; he who exhorts, in exhortation; he who gives,
with liberality; he who leads, with diligence; he who shows mercy,
with cheerfulness (Romans 12:3-8).

For as the body is one and has many members, but all the
members of that one body, being many, are one body, so also is
Christ. For by one Spirit we were all baptized into one body—
whether Jews or Greeks, whether slaves or free—and have all been
made to drink into one Spirit. For in fact the body is not one mem-
ber but many. If the foot should say, “Because I am not a hand, I
am not of the body,” is it therefore not of the body? And if the ear
should say, “Because I am not an eye, I am not of the body,” is it
therefore not of the body? If the whole body were an eye, where
would be the hearing? If the whole were hearing, where would be
the smelling? But now God has set the members, each one of them,
in the body just as He pleased. And if they were all one member,
where would the body be? But now indeed there are many mem-
bers, yet one body. And the eye cannot say to the hand, “I have no
need of you”; nor again the head to the feet, “I have no need of
you” (1 Corinthians 12:12-21).

Notice that Paul’s verbs are all in the present tense. He is not
speaking of a future millennial age. He says that the Church of
Jesus Christ is a body. We are members. We may not be strong
members, or members marked by dexterity. We may be the
equivalent of arthritic members, gnarled and stiff, but we are
members.

The Doctrine of Imputation

Christians know that their only hope of eternal life is that
Christ’s righteousness Aas been imputed to them already. God looks
at Christ’s perfect righteousness as a human being, and declares,
“T declare as the Judge of history that your righteousness belongs
to this formerly lost sinner, I declare him ‘not guilty’ in My court.
of eternal law.” This declaration of “not guilty” comes as a result of
Christ’s perfect humanity, His perfect walk before God in history.
