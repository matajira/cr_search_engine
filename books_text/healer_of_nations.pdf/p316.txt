 

302 Healer of the Nations

Publishing the Files

If Christians should begin to capture political power, they will
have to call the State Department to full accountability. All
treaties should be published in one set of documents with compre-
hensive documentation. They should be put on laser disk, with a
computer program to search that disk, The State Department has
always resisted the perfectly reasonable demand that a complete
published set of international covenants be made available to
Congress, let alone the general public. What Bryton Barron com-
plained about a generation ago is still a problem: “With respect to
the first category of treaty. information, it is unfortunate that there
is no corivenient collection of United States treaty texts which is
complete. The Department of State has never provided the coun-
try with such a collection. Only by piecing together the thousands
of separate prints of treaties and agreements that have been
brought into force through the years, many now out of print, scat-
tered in several series, and confusingly numbered, can you hope
to have anything like a complete collection, Probably not more
than a dozen individuals or libraries in the country have attempted
so difficult a task.”27

There should be no significant time lapse between the signing
of any agreement and its publication. The Congressional Record is
typeset, printed, and delivered to each Congressman’s office over-
night. Yet it takes decades to get treaties and other State Depart-
ment documents into print.

The State Department should have no top secret status for any
document older than two years old, unless initialed by the Presi-
dent of the United States or the Secretary of State. This means
that no top secret document older than two years would remain.
inside the State Department unless initialed by the President or
the Secretary of State. If such clearance is sought by an official in
the Department for any other document, then the document
would be transferred to an appropriate military service or to the

27. Bryton Barron, Inside the State Department: A Gandid Appraisal of the Bureau-
cracy (New York: Reflection Book, Comet Press, 1956), pp. 112-13.
