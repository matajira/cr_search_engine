Introduction 3

look at the Church International as if it were a model for interna-
tional relations. They see no connection between the Church model
and the State model. The first possesses the God-established monop-
oly of the sacraments, while the second possesses the God-established
monopoly of the sword (violence). But if there is no connection,
then why did Jesus speak of the Church International as a nation
that inherits the kingdom of God? International relations among
churches within this inheriting nation serve as God’s designated
model for relations among nation-states.

Christians know that a local church is based on a covenantal
bond among members. ‘This covenant is based on a public confes-
sion that Jesus Christ is Savior and Lord. Some Christians also
understand that a denomination is also a church based on a cove-
nant. But they restrict the idea of the covenant to the church and
the family: baptism and the marriage vow. They usually stop
short of arguing that nation-states are told by God to become
forthrightly, openly, covenanéally Christian, in the way that all indi-
viduals are told by God to become forthrightly, openly, covenantally
Christian. Somehow, for some reason, civil governments are sup-
posed to remain forever covenantally neutral. Old Testament
Israel is not an appropriate model in New Testament times, we
are told by Christian authorities.» The New Testament nation-
state model is by definition (whose, it is never said) necessarily
secular. The State is therefore always to remain a strictly neutral
covenantal institution—a covenant with no god in particular,
meaning a covenant that answers only to the self-proclaimed god of this
world, autonomous (self-law) man.

This is the common faith of modern Christians. This is also
the common faith of modern humanists, who over a century ago
captured almost every Western nation-state. Even those Chris-
tians who argue against the myth of neutrality in general make
this exception: the State. This ts the baptized humanist theology of polit-
tcal pluralism through natural law. This is the politics of hypothetical
covenantal neutrality. It is the impotence-producing Christian
heresy of our age.

3. Meredith G. Kline, The Structure of Biblical Authority (rev. ed.; Grand Rap-
ids, Michigan: Eerdmans, 1975), ch. 3.
