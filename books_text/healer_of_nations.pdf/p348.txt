334 Healer of the Nations

war
ancient, 198
covenantal, 106
declaration of, 227, 300
diplomacy &, 194
diplomatic relations, 115
final stages, 107
foreign policy &, 100
new scale, 181
peace &, 125
in progress, 40
sanctions, 101
shooting, 227, 228
theological, 106
United States, 117-18
warfare
ancient, 198
continual, 199
peace &, 202
regional, 169
war's origin , 99-100
Warsaw Pact, 203
Washington, George, 6, 11-15, 187
watchman, 249
weakness, 126
wealth-redistribution, 219
welfarism, 219

West
collapse, 3
contacts with, 221
escape religion, 246
fearful leaders, 106-7
prosperity, 190
retreat of, 132
surrogates, 228
Western civilization.
Bible &, 108
white man’s burden, 168
will to resist, 207
work, 193
work ethic, 221
world government
Christian, 44
Wriston Committee, 305
Wriston, Henry, 130-31, 181, 184,
224, 290
Wriston, Walter, 223

Yalta Conference, 16, 184, 296
yoke, 155-56
yoked, 26

Zion, 33, 37-38, 155, 157-58, 218,
274, 283-84
