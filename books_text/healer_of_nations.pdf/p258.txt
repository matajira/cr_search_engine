244 Healer of the Nations

have to get back to a much simpler form of life, a much smaller
population, a society in which the agrarian component is far
greater again in relation to the urban component— until these ap-
preciations become widespread and effective—I can see no
answer to the troubles of our time.” Spoken like a man who spent
his life behind a typewriter—and toward the end, an electric type-
writer—in air-conditioned luxury, sipping sherry rather than tap
water. Yet this man was the primary State Department intellec-
tual for at least five decades.

He says that we face one of two catastrophes: nuclear war with
the Soviets, or ecological catastrophe in the next five decades. He
neglects another: political conquest by the Soviets, which he dis-
misses as unlikely because the Russians have suffered reversals
everywhere. “The Russians are not in a good position to take ad-
vantage of our great weaknesses today.” He concludes: “Com-
pared to the dangers which confront us on the ecological and
demographic front, the possibility of Soviet control of Western
Europe, even if one thought that this was a likely thing to happen
(which I don’t) would strike me as a minor catastrophe. After all,
people do dive in the Soviet Union. For the mass of people there,
life is not intolerable.”2”7 He should read What to Do When the Rus-
stans Come: A Survivor's Guide (Stein & Day, 1954), by Jon Manchip
White and Robert Conquest, the scholar whase book on Stalin's
purges of the 1930's is the definitive work, The Great Terror, That
was a decade in which 20-30 million people did not survive the
politics of the Soviet Union. China lost perhaps 60 million in the
1950's. Cambodia continued this political tradition in the 1970's,
and Ethiopia continues it today. But Kennan worries about smog.

Western Europe is decadent, he says, “far too addicted to its
material comforts.” Pornography is everywhere. “This betrays a
terrible lack of self-confidence and a total confusion of values.”?6

26. Iid., p. 8.
27, Ibid., p. 12.
28. Ibid., p. 8.
