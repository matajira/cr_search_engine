4 Healer of the Nations

among nation-states. This follows from one of modern humanism’s
most dangerous presuppositions: that the State! is the central in-
stitution in every area of life. It was also a major presupposition of
ancient humanism,? It is a demonic belief.

The Church International

Thave begun this book by quoting Matthew 21:43. Jesus spoke
of a nation that would inherit the kingdom of God. Was He speak-
ing of a particular nation-state? Or was He speaking of the totality
of those throughout history who profess faith in Jesus Christ? Was
He speaking of a particular nation-state (such as Israel had been)
or the Church International? Obviously, it was the latter.

This raises another very important issue: the Biblical defini-
tion of the word “nation.” I discuss this in Chapter One. Since the
Church International is called a nation, relations among the vari-
ous national and regional churches should serve as the Biblical
model for relations among sovereign nation-states. The Church
International is he nation of nations in New Testament times. It is
therefore the appropriate model for international relations.

Any failure of the Church International to resolve its internal
differences will necessarily have repercussions in relations among
other nations. If churches are in perpetual conflict with each
other, unable to find peaceful ways to conduct the affairs of inter-
national ecclesiastical institutional order, then we should expect to
see analogous disruptions among nation-states. Since nation-
states have no other legitimate model for the successful working
out of disputes, how can they be expected to achieve lasting
peace? It is a case of the blind (or at least the pathetically near-
sighted) leading the blind into a ditch. (See Chapter Eleven for
details.)

Christians have failed to understand this point. They do not

1. I capitalize the word “State” when I am referring to the covenantal institu-
tion of civil government; I do not capitalize it when I am referring to a regional
political unit called a state.

2. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy (Fairfax, Virginia: Thoburn Press, [197t] 1978), chaps. 3-5.
