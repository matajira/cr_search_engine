Introduction 17

pages are devoted to U.S. foreign policy prior to 1889. He says in
the Preface that the foreign policy experience derived from the
years 1889 to 1945 “is much more significant than that gained be-
tween 1775 and 1889.”% His view is representative of twentieth-
century political humanism.

What happened in the 1890’s to change American foreign pol-
icy? Darwinism. Charles Darwin’s Origin of Species (1859) had
blown away men’s earlier naive faith in a providential, orderly
world—a faith held by Christian philosophers, conservative hu-
manists, and natural law theists. The older faith in a social world
operated by magnanimous, harmony-producing forces that lead
inevitably to what George Washington, following John Locke and
Adam Smith, had called “the natural course of things,”2! had been
shattered. Educated men increasingly turned to a new religion,
which in fact is a very ancient religion: faith in the State as the
sovereign agent that alone possesses sufficient power to bring
peace and harmony to this dog-eat-dog Darwinian world.”

During the 1890's, the United States government began to ex-
pand into every area of economic life. This centralization came at
the expense of private activities and responsibilities. In the field of
international relations, no one before this era had perceived a
need for the United States government to send official representa-
tives to every nation or to seek alliances, agreements, and ar-
rangements with every nation. People assumed that private inter-
ests would be the basis of the vast bulk of international relations.
But in the twentieth century, men have lost faith in the power and
importance of government-unregulated activities. Nowhere has
skepticism regarding private activities been more deeply held than
in the area of international relations. In place of international re-
lations in the broadest sense, we have seen the creation of a vast
network of intergovernmental relations.

20. Richard W. Leopold, The Growth of American Foreign Policy: A History (New
York: Knopf, [1962] 1969), p. viii.

21. “Farewell Address,” Messages and Papers, p. 215.

22, Gary North, The Dominion Covenant; Genesis (2nd ed., Tyler, Texas: Insti-
tute for Christian Economics, 1987), Appendix A: “From Cosmic Purposeless-
ness to Humanistic Sovereignty.”
