132 Healer of the Nations

continued trade with the enemy.® The economic deal-doers have
taken control of the policy-making organizations, so the diplomats
are paid to avoid confrontations with the Communists. For exam-
ple, the U.S. Department of Commerce secretly helped to estab-
lish a highly secretive non-profit, tax-exempt foundation in the
mid-1970’s, the US-USSR Trade and Economic Council, located
in New York City.’ Step by step, decade by decade, Western dip-
Jomats and Western businessmen have worked hand in hand to
strengthen the Communist bloc nations.

Diplomats are specialists in softening rival positions, trading
with the other side. The result has been a steady retreat by the
West in the face of danger for over two generations. If we count
1933-38, it has been almost three generations.

An ambassador is to represent the national interests of his
country. He is not a professional negotiator. Negotiators are
useful professionals, but only when a nation is negotiating with its
allies. They should not be let anywhere near a self-identified
enemy. We should not trade with the enemy, so we need to keep
specialists in trading—economics or politics—completely out of
the picture.

This is not some strange view of foreign policy. Professor
Richard Pipes of Harvard, a specialist in the Soviet Union, has
pointed out, first, that in the United States, the Constitution des-
ignates the President as the person responsible for conducting for-
eign policy, not the Department of State. Second, the Department
of State is concerned with diplomacy, not foreign policy: “The
belief that the State Department is the proper instrument of for-
eign policy derives from the fallacious view that foreign policy is
synonymous with diplomacy — which, as has been pointed out, is
not the case. The Department of State is the branch of govern-
ment specifically responsible for diplomacy in all its aspects, and

6. Antony C. Sutton, The Best Enemy Money Can Buy (Billings, Montana: Lib-
erty House Press, 1986).

7, Joseph Finder, Red Carpet (Ft. Worth, Texas: American Bureau of Eco-
nomic Research, [1983] 1987), pp. 254-60. This book was originally published by
Holt, Rinehart and Winston.
