Businessmen Make the Best Diplomats 177

the first half of Jesus’ command: “Therefore be wise as serpents
and harmless as doves” (Matthew 10:16b). Harmless as doves,
yes, but never wise as serpents!

There is a quiet alliance between humanists and responsibility-
denying Christians against those Christians who want to follow
Christ’s Great Commission to preach the gospel, disciple the na-
tions, and baptize them, bringing them under the requirements of
God's covenant law. This alliance has been in effect for almost two
thousand years. But as humanists have become more consistent
ethically with their God-denying presuppositions, a growing
number of responsibility-fleeing Christians have begun to see
more clearly what it means to live under God-denying laws and
God-denying rulers. They are becoming fearful of where the hu-
manist West is headed. They at last have begun to see that the
weak-kneed humanists of the West are no match for the murder-
ous humanists of the Communist empires. Yet it is foreign policy,
above all, that is dominated by the weak-kneed humanists who
are ready at a moment's notice to bow the knee to the Soviets. The
highest-level members of the Council on Foreign Relations and
the Trilateral Commission determine U.S. foreign policy.! These
unelected representatives in fact represent the United States to
the world. They are, in the words of one of their most respected
leaders, an invisible government.”

Christians were horrified in 1979 when an American President
kissed the cheeks of the Soviet Premier at the signing of the SALT
Tl treaty (which the U.S. Senate later refused to ratify), for they at
last began to understand just where some future President will be
asked to kiss symbolically some future Soviet Premier, unless our
leaders get some backbone. The President represents his nation,
just as the Soviet Premier represents his. The doctrine of repre-
sentation is part of God’s inescapable covenant structure (point
two: hierarchy).

1, Larry Abraham, Call it Conspiracy (Seattle, Washington: Double A Publica-
tions, 1985),

2. Henry M. Wriston, Diplomacy in a Democracy (New York: Harper &
Brothers, 1956), p. 108.
