30 Healer of the Nations

membership. It does not possess the sword. The sword is exer-
cised within specified geographical boundaries. The Church has
no geographical boundaries. It cannot legitimately impose taxes
on all those within a particular geographical area, for it is not a
geographically defined institution.

Humanist Nationhood

The modern humanist defines the nation in terms of political
power, That geographical and legal entity which possesses
supreme political power is defined as the nation. The nation is to-
day incorrectly identified with the State. Conservative sociologist
Robert Nisbet writes: “Like the family, or like capitalism, the
State is a complex of ideas, symbols, and relationships. Unlike
either kinship or capitalism, the State has become, in the contem-
porary world, the supreme allegiance of men and, in most recent
times, the greatest refuge from the insecurities and frustrations of
other spheres of life.”* It is seen by many in the modern world as
possessing redemptive power.®

In classical Greece, the nation was a closely knit association
based on common religious rites, civil government, limited
boundaries (the city), a common language, common laws for citi-
zens only, and a common racial heritage. The empire of Alex-
ander the Great shattered this view of the nation in the fourth cen-
tury, B.c. His empire included many races, laws, and languages.
it was not based on shared religious rights, but on power. The
Roman Empire was equally diverse, and the only common relig-
ious rites were those based on the sovereignty of Rome’s power.
This is why the early Church was persecuted: it denied Rome's
divinity.° The breakup of the Roman Empire in Western Europe
led to local kingdoms rather than what we call nations: loosely

4. Robert A. Nisbet, The Quest for Community (New York: Oxford University
Press, 1953), p. 99.

5. Ibid,, p. 154.

6. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy (Fairfax, Virginia: Thoburn Press, [1971] 1978), pp. 138-48. Cf.
Ethelbert Stauffer, Christ and the Caesars (Philadelphia: Westminster Press, 1955).
