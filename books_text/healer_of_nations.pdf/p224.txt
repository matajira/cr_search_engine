210 Healer of the Nations

then will his kingdom stand? . . . Or else how can one enter a
strong man’s house and plunder his goods, unless he first binds the
strong man? And then he will plunder his house” (Matthew
12:24-26, 29).

It is our job as Christians to work constantly to plunder
Satan’s house, in every area of life. This is what dominion means.
It is what serving as the leaven of God’s kingdom means (Mat-
thew 13:33). It is what it means to be an ambassador for Christ, a
disciple of Christ, disciplining the nations. This involves pitting
Satan’s less consistent followers against his more consistent fol-
lowers. In foreign relations, this is the equivalent of exorcising
demons. But it necessarily involves exercising good judgment. We
must distinguish between friend and foe. This is what the foreign
policy of the West, and especially the United States, has failed to
do for over two generations,

Strengthening Our Allies

Having made the distinction between friend and foe, our for-
eign policy establishment must do whatever it can to strengthen
the confidence of our allies and weaken the support the world
gives to our enemies.

This support must recognize degrees of reliability and wor-
thiness. In the face of aggression from a true tyranny, we can give
limited verbal support to a local dictator. A domestic tyrant is less
of a threat to a godly nation than an expansionist empire is. Any-
one who cannot understand this should play no role in the corps of
foreign policy specialists. Unfortunately, the vocal elements of the
legislature, the universities, the media, and the entire foreign pol-
icy establishment in the West has great difficulty in grasping this
elementary principle.

In such alliances, the direction of policy must always be pri-
marily in the hand of the Christian nation. We are not to rely on
dictators. We are not to become dependent on them. We are to
make calculated good use of them as buffers against invasion. Ifa
dictator stands between our borders and the expanding Commu-
nist empire, we must do what we can, cost-effectively, to strengthen
