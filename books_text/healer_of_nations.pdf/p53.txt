God Created the Nations 39

ship!) refused to see communism’s crimes, and when they no
longer could do so, they tried to justify these crimes. The problem
persists: In our Eastern countries, communism has suffered a com-
plete ideological defeat; it is zero and less than zero. And yet West-
ern intellectuals still look at it with considerable interest and em-
pathy, and this is precisely what makes it so immensely difficult for
the West to withstand the East."

A godly foreign policy must begin with repentance, as
Solzhenitsyn elsewhere has written. “Repentance is the first bit of
firm ground underfoot, the only one from which we can go for-
ward not to fresh hatreds but to concord, Repentance is the only
starting point for spiritual growth. For each and every individual.
And every trend of social thought.”* In this sense, the isolationists
have things partially correct, for they say that we must clean up
our own societies before trying to clean up everyone else's (see
Chapter Four). But they assume that our backyard will never be
perfectly clean, and we can therefore forever ignore everyone
else’s backyard.

If Christians took this perfectionist personal attitude with
respect to sharing the gospel with others, or before imperfect
churches could send out missionaries, there could be no evangel-
ism. Sanctification is a long-term, lifetime project. It is an inside-
out process, for regeneration begins with the individual soul, but
eventually sanctification does begin to affect the outside world.
Imperfect people are to minister to others.

Imperfect nations are also to minister to others, and I do zot
mean to limit this to national civ governments. Nations sometimes
must offer protection to other nations. In doing so, they can also
gain protection. This has implications for foreign policy and
defense policy. For example, the support of specific antiCommu-
nist freedom fighters is often a wise policy; it allows the United
States and other Western nations to inflict economic, political,

It. Solzhenitsyn at Harvard (Washington, D.C.: Ethics and Public Policy
Center, 1980), pp. 17-18.

12. Solzhenitsyn, “Repentance and Self-Limitation in the Life of Nations,” in
Solzhenitsyn, From Under the Rubble (Boston: Little, Brown, 1974), pp. 108-9.
