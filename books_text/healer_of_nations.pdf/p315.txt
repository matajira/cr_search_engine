What the State Gan Do 301

Iam not calling for wholesale revisions of the Constitution to-
day. These changes must be implemented only after a full-scale
revival and the clear-cut political triumph of Christians as self-
conscious Christian voters. Attempting to amend the Constitu-
tion before you have the votes is suicidal; this would play into the
hands of.the humanist left wing. Just as internationalism prior to
international revival is extremely dangerous, so is attempting any
Constitutional amendment nationally before national revival.
This would be a top-down political transformation, something
quite foreign to Christian social theory.*6 It puts the cart before
the horse, The religious transformation must precede the political
transformation; the political transformation must precede the
Constitutional transformation.

We must therefore content ourselves for the present with small
steps.

First Steps

The fundamental principle of the covenant is the transcen-
dence of God. He is in control of the nations. Thus, there is no
cause for alarm. If the foreign policy elite were all to resign tomor-
row, the nation could still carry on.

Our strategy as Christians must initially be personal, as out-
lined in Chapter Eleven. We cannot beat something with nothing.
If we maintain that international relations means personal rela-
tions, and economic relations, and missions, then we must build
these up as we do our best to tear down the statist monopoly of
modern diplomacy. This will take time, skill, ‘patience, and a lot
of money. If Christians are not faithful in this regard, then we can
expect a God-imposed crisis to speed up the process of transition.

26. On this point, I am in complete disagreement with James Jordan’s recom-
mended program of self-conscious elitism in social and political transformation,
which he calis a top-down system, despite its tendency toward “impersonal
bureaucracies” and the obvious anti-evangelism attitude fostered by such an elit-
ist outlook, which he admits has been the result historically. “Elites seldom feel
any need to evangelize.” Precisely! See Jordan, The Sociology of the Church (Tyler,
‘Texas: Geneva Ministries, 1987), pp. 17-22.
