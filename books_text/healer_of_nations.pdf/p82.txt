68 Heater of the Nations

part of a philosophical tradition: the assertion that man’s institu-
tions must reflect a unity of being with the true heavenly reality —
or, what amounts to the same thing—~to deny any heavenly reality
over mankind, leaving man to create the true reality on earth.

Such a view is totally opposed to the Biblical view, which
asserts that mankind as a species is a creature—totally distinct
from God the Creator—and that men are divided ethically and
covenantally, a division that extends beyond the grave into eter-
nity. It is covenantal unity — unity of confession and belief—that is pri-
mary, not political unity or “the oneness of man” as a species.

What was God’s response to this new theology? “But the Lorp
came down to see the city and the tower which the sons of men
had built. And the Lorn said, ‘Indeed the people are one and they
all have one language [lip, confession, ideology], and this is what
they begin to do; now nothing that they propose to do will be
withheld from them, Come, let Us go down there and confuse
their language [lip, confession, ideology], that they may not
understand one another's speech [lip, confession, ideology)”
(Genesis 11:5-7).

It is surprising to hear God say that because the people are
unified, “nothing that they propose to do will be withheld from
them.” In one sense, we know that God can always stop men from
doing anything; but the language used here points to the fact that
in terms of the economy God has established in the world, there is
strength in unity. God does not want the wicked to rule the world,
so He moves to destroy their unity.

It is important to see that it was not a simple unity of language
that gave these men power. Rather, they all thought the same
way. They had a common ideology, a common religious faith. Without
this anti-God unity, they could not have cooperated. In order to
shatter this unity, God did not simply divide their languages. First
and foremost, He shattered their ideologies.

What the story of the Tower of Babel tells us is that Biblical re-
ligion faced a single rival, anti-God religion. (We are not told that
there were faithful covenant people in that rebellious society, but
we must presume that there were, for God never allows His
