222 Healer of the Nations

dient to the principles of Biblical law. To sustain this outward obe-
dience, however, they will require a public increase of Gad’s spe-
cial grace: conversions to Christianity. Common grace requires
the sustaining pressure of special grace. This is why these nations
are ripe candidates for the gospel.

Any discussion of “Third World poverty’ should begin with
the recognition, as Bauer says: “The one common characteristic of
the Third World is not poverty, stagnation, exploitation, brother-
hood or skin colour. It is the receipt of foreign aid. The concept of
the Third World and the policy of official aid are inseparable. The
one would not exist without the other. The Third World is merely
a name for the collection of countries whose governments, with
occasional and odd exceptions, demand and receive official aid
from the West. . . . Thus, the Third World is a political and not
an economic concept.”6

What has been the result of this official foreign aid in the field
of international relations? Almost universal hostility to the West.”

Another great irony is that poorer people are taxed in the West
in order to finance bureaucratic projects that employ as managers
and technicians the upper classes of the poverty-stricken nations. -
This is Robin Hood in reverse: robbing from the poor to give to
the rich.

An even greater irony is that several major studies of the
growth process indicate that direct material aid has contributed
relatively little to the economic growth of the recipient nations.
The key elements are personal and ethical: outlook toward the
future, self-discipline, management techniques, and better use of
existing resources.8

The overall percentage of international aid is extremely small.
Government aid to large Third World nations (China, India, etc.)

5, Gary North, Dominion and Common Grace: The Biblicat Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987), ch. 6.

6, P. T. Bauer, Reality and Rhetoric: Studies in the Economics of Developraent (Cam-
bridge, Massachusetts: Harvard University Press, 1984), p. 40.

7. Hid, p. A.

8. Ibid., p. 44.
