86 Healer of the Nations

until the final judgment. Third, because we all know that some
sort of law is necessary for social order, the appropriate law-order
has been given by God to fit the needs of all men, saved and lost.
This law-order is logically discoverable by all men, saved and
lost.

There is a fourth argument, seldom stated openly. Since this
natural law-order is actually a myth, there can never be agree-
ment concerning true law. Then, they conclude, all laws are morally
relative. This leads to the fifth unstated argument: might makes right.
He who has the power establishes the law. This is the reigning
faith of the power religion.

In 1938, Mao Tse-tung made a statement which has become
his most familiar slogan: “Political power grows out of the barrel
of a gun.” He did not say it like this. What Mao actually said was
far more consistent: “Every Communist must grasp the truth,
‘Political power grows out of the barrel of a gun.’ Our principle is
that the Party commands the gun, and the gun must never be
allowed to command the Party. Yet, having guns, we can create
Party organizations, as witness the powerful Party organizations
which the Eighth Route Army has created in northern China. We
can also create cadres, create schools, create culture, create mass
movements. Everything in Yenan has been created by having
guns. All things grow out of the barrel of a gun.”3

All things grow out of the barrel of a gun: here in one sentence is the
confession of faith of the power religion. It is no accident that a
basic strategy of the Chinese Communists when they captured a
village was to arrest wealthy residents. They would demand a ran-
som in the form of rifles. These were extremely scarce in China,
and families would have to sell almost everything to buy thern.
Upon handing over the required number of rifles, the man would
be released. If his family had any sign of wealth remaining, he
would be arrested again and again, until the family was destitute.*

3. Mao Tse-tung, ‘Problems of War and Strategy,” (Nov. 6, 1938), in Selected Works
of Mao Tse-tung, 5 vols. (Peking: Foreign Languages Press, 1965), II, pp. 224-25.

4, Raymond de Jaeger and Irene Corbally Kuhn, The Enemy Within (Garden
City, New York: Doubleday, 1952), pp. 42-43.
