Missionaries Make the Best Ambassadors 173

missionaries of Christ’s international kingdom.

We are all colonialists. Christians promote the colonization of
the world under God. Humanists promote the colonization of the
world under Satan. Christians do not promote the colonial ambi-
tions of any single earthly nation, but instead promote the crea-
tion of a world Christian civilization. In our era, humanists have
promoted either colonialism by open nationalism—which ended
in the 1960’s— and, in recent years, colonialism by hidden nation-
alism: either Chinese or Russian Communism. The answer to
false colonialism is God’s colonialism: discipling the nations under
God’s heavenly commonwealth.

Any nation that refuses to accept missionaries from a Chris-
tian nation should be regarded as having broken all treaties with
that nation. No binding covenants with such a nation should be
continued after the closing of its borders to the Christian nation’s
missionaries. A nation that has shut the door to the gospel has es-
tablished a public covenant with Satan. At that point, the prohibi-
tion against making covenants becomes operational.

In summary:

1. The nations will eventually come under the rule of God’s
law.

2. There is no escape from bondage: to Christ or to Satan.

3. The church, as the mountain of God, is the earthly source
of the law of God.

4. Pentecost was the first public sign of this process of interna-
tionalizing the kingdom of God.

5. Those foreigners who were converted to Christ at Pentecost
returned as ambassadors of heaven.

6. The missionary is the key figure in the establishment of a
bottom-up program of building an international kingdom of God
on earth.

7. The Word of God applies covenantally to all nations and
races.

8. This means that individual nations are formally and pub-
licly to covenant with God in history.

9. Christians must place their first loyalty to Christ—over
family, church, and nation.
