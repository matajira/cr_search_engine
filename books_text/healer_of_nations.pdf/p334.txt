 

320° Healer of the Nations

Blackwell, Morton, 134-35, 192
Blackstone, William, 54
Boland Amendment, x
borders, 169, 234-35
boundaries
kingdom &, 167
national, 45, 52
nationhood (ancient), 29
nationhood (humanist), 31-32
pollution, 51
sword &, 60
Thirty Years’ War, 164-65
trust in, 234-35
whole earth, 53
Bozo the Clown, 294
Brazil, 223
Bricker, John, 208-9
Britain, 187; see also Round Table,
Rhodes
broadcasting, 307-8
brotherhood, 45
brotherhood of man, 64, 204, 230
Bryant Chucking & Grinder Co., 113
Brzezinski, Zbigniew, 65-66
Buckley, James, 293
Bugs Bunny, 294
bureaucracy, 72, 77, 133, 181, 191, 220
Burgoyue, John, i1
Bush, Prescott, 130
business, 175, 282-83
businessmen, 175-96, 282
Butterfield, Herbert, ix

Cain, 45, 64, 66, 204

Calvary, 51, 101

Calvin, John, 59

capital, 229

captivity, 249

Casey, William, 226

Castro, 69

Castro, Fidel, 268

cause and effect, 218

cease-fire, 40, 75, 117, 118, 121, 215

Centurion, 161-62
Chambers, Whittaker, xiti, 101-2,
247-48
chariot, 107
charity, 229
charities, 281
Chase Manhattan Bank, 224
Chedorlaomer, 209
chess, 239
Chesterton, G-K., 18-19
China, 166
Christianity
civil citizenship, 148-49
civilization &, 77
free market, 190
humanist alliance, 6
internationalism, 46-47, 73, 94, 159,
230, 259-74
nationhood, 169
one-world order, 46-47, 73, 94
pluralism vs., 96
politics &, 41
revival, 8, 20, 33-34, 41, 238
utopian?, 19, 73
Christians
assume power, 289
defensive mentality, 104, 234
deny law (antinomian), 86
humanist alliance, 53, 177
loss of confidence, 235
love, 260
making godly judgments, 207
maturity, 144
natural law, 87, 89, 92, 233
new humanity, 46
plundering Satan, 210
politics &, 93
responsibility, 177, 217, 276
retreat, 19
retreat of, xii
Roman Empire, 206
serpents, 275
silent, 248
