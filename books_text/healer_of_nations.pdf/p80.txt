66 Healer of the Nations

to his village, to his tribe, to his region, to his nation; more
recently it spread to his continent. . . .”2° The creed of unified
humanity is to serve as the religious foundation of a humanist
one-world order. We must do what we can, he says, to bring Com-
munist nations into this new order.?” It is the United States’ re-
sponsibility to work toward this new global community, he says,
or else we could get chaos. The key is economics, not faith: “a
gradual shaping of a community of the developed nations would be
a realistic expression of our emerging global consciousness. . . .”%

So there is an emerging world consciousness. Why, then, did
the Soviet Union invade Afghanistan in late 1979?

The Tower of Babel

We see in the story of the Tower of Babel another attempt by
rebellious mankind to build himself a kingdom without the Crea-
tor God of the Bible. This same impulse had been going on since
the days of Cain. I am indebted to James Jordan for many of the
following insights.

“And it came to pass, as they journeyed from the east [literally,
eastward], that they found a plain in the land of Shinar, and they
dwelt there” (Genesis 11:2). Just as Cain moved away from God
by moving east (Genesis 4:16), so this “eastward” movement indi-
cates movement away from God. This group of people was under
the leadership of Nimrod, son of Cush, son of Ham (Genesis
10:6-12). According to these verses, Nimrod founded both
Babylon and Nineveh (Assyria), the two great empires that op-
pressed Israel later in history and that sum up anti-God statism in
the Prophets and in the Book of Revelation.

These men knew that their Tower—probably some kind of
stepped pyramid, a symbolic “holy mountain” and “holy ladder” to
God—would not physically reach into heaven. They were not

26. Zbigniew Brzezinski, Between Tivo Ages: America’s Role in the Technatronic Era
(New York: Viking, 1970), p. 58.

27. Ibid., p. 302.

28, Ibid., pp. 307-8.

29. Ibid., p. 308.
