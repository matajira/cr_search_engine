Editor's Introduction xi

Chief of the armed forces. The Democrats, of course, have also
reversed their position since 1946. Confusion reigns supreme.

Christianity and Foreign Policy

The whole field is wrapped in mystery for most Christians. 1
knew that the author who wound up with this topic would have to
start from scratch.

I assigned the book to the first author in the fall of 1985, His
manuscript did not come close to meeting the needs of the series.
The second author came a bit closer, but not close enough. The
authors’ advances were mounting up, and time was running out.
So, in mid-May of 1987, I decided that I would have to write the
book. I sat down at my computer, and in between my normal re-
quired output of three monthly newsletters and my normal 10
hours per week devoted to writing my economic commentary on
the Bible, I hammered out this book. It took five weeks. Had I not
had Ray Sutton’s model of the Biblical covenant in my mind, I
doubt that I could have done it.

Foreign policy in the United States is controlled by a tightly
knit “old boy” network of dedicated humanists, who quite prop-
erly regard their control over foreign policy as the linchpin in their
overall control of the United States government.? They do not
want outsiders criticizing their little monopoly. They have
devoted millions of dollars since World War II to finance books,
journals, and studies on American foreign policy, all of which con-
clude that we must be firm with the Communists until we join with
them in a one-world humanistic order. We must be visibly tough
negotiators while we are capitulating to the vast bulk of their de-
mands. Most important, the West must not try to roll back Com-
munism. As “realists,” we must accept the Soviets’ operating princi-
ple: “What's ours is ours, and what’s yours is negotiable.” Then we
negotiate, And negotiate. And if the Soviets press us too hard, and
demand too much, we then criticize South Africa’s apartheid.

2. Gary North, Conspiracy: A Biblical View (Ft. Worth, Texas: Dominion Press,
1986).
