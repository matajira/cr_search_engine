166 Who Owns the Family?

Biblical death penalties re-instituted “overnight.” But this ap-
proach is designed to change the emphasis of law from reform to
restitution. The Biblical position is that people are best reformed
by having to pay restitution, instead of being incarcerated.

Resolution On The Sovereignty Of The Church

BE IT RESOLVED, that the party in Precinct #.
acknowledges that the Church is a God-ordained institution with a
sphere of authority separate from that of civil government and thus
the Church is not to be regulated, controlled or taxed by any level
of civil government.

Remember, historically the Church has been the best
“buffer-zone” between the family and the State. Nothing can pro-
tect the family better. So, what happens to the Church indirectly
affects the family. This is why the State is simultaneously attack-
ing the family and the Church.

The resolution above is designed to remove all regulations off
the Church. Again, on this issue it’s an all or nothing proposition. If
the State is allowed to regulate the Church at all, then it has the
power to regulate everything!

Resolution On The Legitimate Function Of Civil Government

WHEREAS, God is Sovereign over all the world and has
divinely instituted civil government among men, for His own
glory and for the public good, and for the administration of this in-
stitution He has ordained civil rulers to exercise their authority
under Him in obedience to His laws in order to promote justice,
restrain wickedness, punish evildoers, and protect the life, liberty,
and private property of the citizens, and provide for domestic and
national defense; and

WHEREAS, when civil government assumes responsibilities
and authority beyond this well delineated scope it occurs at the ex-
pense of the other God-ordained institutions, the Family and the
Church; now therefore,

BE IT RESOLVED, that the party in Precinct #_____ sup-
ports this historic concept, established by our nation’s Founding
Fathers, of limited civil government jurisdiction under the laws of
