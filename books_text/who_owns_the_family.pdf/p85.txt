Who Owns Your Inheritance? 59

tangible. He grew rich—rich enough to afford all those wives.
The intangible initially meant more to him. He knew if he had
wisdom, then he would have everything. His problem appeared
when the things he could afford turned out to be forbidden.

Conditional

The second point I made to the father who asked me about his
children concerned the “conditional” character of inheritance. In
the same passage above, God continued, telling Solomon, “So if
you walk in My ways, to keep My statutes and My command-
ments, as your father David walked, then I will lengthen your
days” (I Kings 14).

God granted Solomon wisdom and wealth on the basis of his
Saithfulness. But if Solomon turned from the Lord, then he would
lose his inheritance. In fact, Solomon did fall away and his
kingdom was divided. After Solomon, the Book of Kings takes a
sad turn of events.

Of course, the message is that Solomon is like the first Adam
who allowed women to mislead him. His fall eventually
culminated in the coming of Christ, the faithful Son who never
disobeyed His Father.

Yet, in the I Kings 3 statement that the Lord made to
Solomon, we should see that inheritance should not be given in-
discriminately. All children should not necessarily receive the
same amounts. Nor should all the children receive anything at all.

Only the faithful should receive an inheritance. If all the chil-
dren are faithful, then all should receive equal proportion, But the
point is that faithfulness determines who receives the inheritance.

Living Trust

Finally, I told the father that inheritance should be a living
trust. Even in our day, this is called an “inter vivos” trust.

What is it?

A “living trust” is where the inheritance passes to the heirs be-
fore the death of the testator. The basis for such a concept goes all
the way back to the Scripture. The patriarchs, for example, be-
