14 Who Owns the Family?

not leaving large sums of money to the children. The general rule
was, “don’t leave so much money that the children have to devote
full time to spending it.” Many times this rule was implemented
by some sort of discretionary inheritance to make the heirs earn
their own way.

Godfrey Cabot, for example, did not leave his heirs a large
amount of money. Why? He believed that real prosperity should
come to them through their own diligence, and not through his
death. Therefore, he helped them while he was alive to get started
in businesses. If the effort failed, the heir didn’t receive any in-
heritance. In other words, the faithful received the blessing. By
doing this, he made them become disciplined.

Dominion

Third, the ability to dominate is the final characteristic. Many
have and can make a sudden fortune, either through an in-
heritance, lottery, hot tip on the stock market, or other ways. But
it is another matter to stay on top. Moreover, staying on top is
difficult, but even harder is the use of wealth to dominate other
people and ideas. In other words, have dominion.

The Avoidance of Corruption

In the super-dynasties, such as the Sassoons and the Roths-
childs, discipline and dominion come together in an interesting
way: the ability to use vice without becoming addicted to vice. This illus-
trates the general Biblical principle that even if men’s hearts are
not submissive to God, God will bless their efforts externally if
they follow certain fundamental principles. The self-discipline of
avoiding vice, year after year, no matter how close at hand, is one
such principle.

Take the Sassoons. Stanley Jackson has written a biography
on the Sassons where he calls them the Rothschilds of the East.
(The Sassoons [New York: E. P. Dutton & Co., 1968], p. 3.) Their
dynasty goes back to Sheik Sason (his descendants would use the
modern variation of Sassoon) in 1778. He had risen from coin col-
lector to the most influential banker of Baghdad. They were Or-
