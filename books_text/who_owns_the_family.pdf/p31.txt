A Covenant, Not a Contract 5

of the said Alice Nicholls shall be reserved as a stock for her son,
John Nicholls, for him to enjoy and possess at the age of twenty
and one years, in the meantime to be improved by the parents
towards his education. Also, that what estate of hers the said Alice
shall be found and committed to the said Mr. Clarke, if it should so
please the Lord to take the said Alice out of this life before the said
Thomas Clarke, she shall then have the power to dispose of all the
said estate sometimes hers so as it seems good to her. Moreover, if
it shall please the Lord to take away the said Thomas Clarke by
death before the said Alice, she surviving, then the said Alice shall
have and enjoy her own estate she brought with her, or the value
thereof, and two hundred pounds added thereto of the estate left
by the said Thomas Clarke. All which the said Thomas Clarke
does hereby promise and bind himself to allow of and perform as a
covenant upon their marriage. In witness of all which, the said
Thomas Clarke has signed and sealed this present writing to Peter
Oliver and William Bartholmew of Boston, for the use and behoof
of the said Alice Nicholls. Dated this twentieth day of January
1664,
per me, Thomas Clarke

Marriage covenants, like this one, used to be standard.
Whenever someone got married, the two worked out a covenant.
Today, however, it’s quite different. Marriage licenses, drawn up
by the State, are used. What are they for? Some states have de-
bated their meaning, but generally there is precedent that a mar-
riage license is a health notification. It is not a statement of permis-
ston. (There was a time in American history when in certain
southern states it did involve permission, when people of two
races were involved.)

I don’t think there is anything wrong with this, as long as it
doesn’t become more than a health notification. In a day when AIDS
is a growing concern, as a matter of fact, I would think that some-
one would want to make sure he is not marrying a person with
this, or some other awful sexual disease. This would then place
the civil marriage license under the general civil quarantine provi-
sions of Leviticus 13 and 14.

The problem is not so much with the marriage license, but
