Who Owns Sexual Privacy? 81

with authority? Sex involves submission to one’s partner. The
Apostle Paul says the following to the Church at Corinth:

Nevertheless, because of sexual immorality, let each man have
his own wife, and let each woman have her own husband. Let the
husband fulfill his duty to his wife, and likewise also the wife to her
husband. The wife does not have authority over her own body, but
the husband does; and likewise also the husband does not have au-
thority over his own body, but the wife does. Stop depriving one
another, except in agreement for a time that you may devote your-
selves to fasting and prayer, and come together again lest Satan
tempt you because of your lack of self-control (I Corinthians
7:2-5).

God is not against sex. When confined to the covenant of mar-
riage, sex is spiritual. Paul provides us with several controlling
ideas, demonstrating the relationship between sex and authority,

One, there is an implicit mutually assured dependence created by
the covenant. The woman is dependent on the man, and he is de-
pendent on her. The man is dependent on the woman by allowing
her to fill the special void in his life which was created, when God
removed a rib from Adam's side to make the woman. If anything
else fills that void, he is acting autonomously.

The woman is dependent on man by allowing the man to give
her a functional definition (not ethical). This is the essence of re-
ceiving the man’s name. Thus, both are dependent on each other.

If she allows another to give her definition, then, like the man,
her autonomous rebellion appears. Eve allowed Satan to give her
a new definition: to be as God (Genesis 3:5). She submitted to
Satan ethically by eating the forbidden fruit, and she then took
dominion over her husband by tempting him to defy God. They
both abandoned God's ethics, and this disrupted (inverted) their
God-assigned functional authority relationship.

Two, sexual submission is an authority issue. In Genesis we
see that God says of the woman, “In pain you shall bring forth
children; Yet your desire shall be for your husband, and he shall
rule over you” (Genesis 3:16). The practical, physical result is
summarized by one writer, “in place of the joy at the irreducible
