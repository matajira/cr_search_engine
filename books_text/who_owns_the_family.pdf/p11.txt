Editor's Introduction xi

state power. But family authority is also under siege. Like the loss
in authority that the church has suffered, the loss in family au-
thority has come as a result of the very members of the family hav-
ing abandoned their fear of God, under whom the marriage oath
was taken. Divorce rates have soared —a public announcement of
people’s lack of covenantal fear in God. Family members no
longer believe that a vow taken before God has any permanent
meaning, let alone any threat of judgment. The oath taken before
God has lost its power outside of the civil government's court-
room, where perjury is still a punishable offense —punishable by
the state, though not by God, men assume. Men now fear only
those who can kill the body.

Thus, the loss of faith in the God who enforces all three cove-
nant oaths in history has led to the monopoly of state power in his-
tory. The state can kill the body, or at least sentence the body to
prison. Since men no longer believe in the earthly or eternal valid-
ity of any institutional manifestation of God’s power to destroy
both body and soul in hell, the oaths of both church and family
are regarded either as irrelevant or relevant only when backed up
by state power. The state covenant is the only covenant that any-
one pays much attention to today.

The church has slid down the slope into cultural impotence.
The family is close behind. Only the state remains as a force to be
taken seriously in history—and history increasingly is the only
thing anyone takes seriously.

A Shift in Opinion

In the last two decades, there has been the beginning of a
change in opinion, Christians and non-Christians alike have
begun to recognize the historical threat of a collapsing family
structure. They have begun to see what Stalin saw in 1936, when
he pragmatically imposed rigorous civil laws that protected the
Russian family against the acids of original Marxist theories that
favored free love and the abolition of the family. He saw that
Marxism’s theories of the family would destroy the Soviet Union,
so the new laws made divorce difficult, abortion illegal, and
