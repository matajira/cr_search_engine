24 Who Owns the Family?

the State to limit the number of children is actually a direct effort
to stop the dominion of Christ.

2. Parents represent the Lord to their children. Paul com-
mands the children in the Church at Ephesus, “Obey your parents
in the Lord, for this is right” (Ephesians 6:1).

3. Because of this high calling given to parents, they are to
“discipline” their children. We normally think of discipline only in
a negative light. But in the Book of Proverbs, discipline is “in-
structional” (positive) and “correctional” (negative). Solomon, for
example, tells fathers to take their children out to study animals,
like the ants, to learn about diligence and self-discipline (Proverbs
66-11). But Solomon also tells parents to use the rod, as we have
seen, to correct the child. So, parents have the authority to educate
and punish the children God has entrusted to them.

Parents are simply trustees of what belongs to God. Again, I
should stress that Christians have to be careful how they argue be-
fore a watching world. Their children do not belong to them, nor
do they belong to the State. They belong to God! God has dele-
gated certain authority for parents to fulfill the trusteeship dele-
gated to them. If the State interferes, God will deal with the State!

Summary

By whose authority? By God's authoritative Word. All author-
ity ultimately resides here, and any other authority is derived from
God. It is precisely because God’s authority is absolute that the
State has no right to violate the space and territory of other
spheres, either the family or the Church.

1. In this chapter, I began with the court case of New Jersey a.
TL.O, The point I made was that the State is trying to shift au-
thority away from the parents, even if it has to shift to the children
first. I have made three basic points about Biblical authority.

2, Second, I established that there are only two authorities:
God and man. There are consequently two systems of authority:
Theism and Humanism, Theism recognizes that authority is out-
side of man in God. Humanism tries to place authority outside of
God in man and nature.
