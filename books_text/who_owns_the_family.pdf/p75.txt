Who Owns Discipline? 49

of the Biblical covenant. For, without it, there is no law.

Notice what parents are not able to do: no torturing, nothing
which is life-threatening. The Bible protects the child, as well as
stipulates the methods of discipline,

What if parents abuse their rights? They can be prosecuted.
But they should not be taken to court for obeying God’s law, like the
Snyder’s. If we had a Biblical society, the State would punish
those who break the law of God. I am sorry to say that we have
fallen from grace.

Summary

There is hope for rebellious men because there is a clear mes-
sage from God. He disciplines those whom He loves. He loves this
nation. So, He is disciplining it and will discipline it.

The principle in this chapter has been that discipline is the
Lord’s, entrusted to parents in the home. (“Entrusted” implies
trusteeship.)

1. I introduced the discipline issue with the Snyder's case.
Paul Snyder was not able to discipline his daughter effectively
because she found protection by the State. This is nothing short of
an open attack on the family, Mr. Snyder was in no way abusing
his daughter. He was simply doing what he was supposed to
under Biblical law.

2. God entrusts the responsibility of discipline to the parents.
Deuteronomy 21:18-21 was used.

3. The specific kinds of discipline that parents can use are:

A. Verbal discipline
B. Denial discipline
CG. Withdrawal

D. Corporal

E. Disinheritance

Other forms of discipline are given to the State and the
Church. I have already mentioned this. But, what the Lord gives
to the family should be upheld by the State and the Church.

In the next chapter, I want to turn our attention to the in-
