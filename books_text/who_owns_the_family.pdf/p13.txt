Edditor’s Introduction xiii

fully since 1965. Thus, what may initially appear to be merely a
defense of the integrity of the family necessarily leads to an offense
against unlawful state power. It is the war between two irrecon-
cilable religions, the religion of the Bible and the religion of state-
deifying secular humanism. There can be no permanent peace
treaty between the two camps. There will be winners and losers,
on earth and in eternity.

The humanists are going to be the losers, not simply in eternity
but also in history. Fearful retreatist Christians refuse to believe
this, so they stay on the sidelines of life, as they have stayed for
over a century. They will continue to cry out to the forces of the
dominion religion to pull back, to cut another unworkable deal
with the humanists, and to content ourselves with a cease-fire.
Cease-fires with Satan don’t last, either with the public schools or
the Soviet Union. There is a war to the death going on.

This book is a call to Christians to join the winning side.
