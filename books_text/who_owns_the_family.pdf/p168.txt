12
WHAT CAN THE CHURCH DO?

We often forget the Church can do something. That’s not
meant entirely as a criticism. We've seen so many churches go lib-
eral and become irrelevant. Or, maybe we haven't seen any that
do much of anything. Think for a moment. How many churches
in your community take a stand as a church? Not many, [ll bet. I
know that in my town the largest churches are usually the most
compromised. They won't take a position on abortion. They won't
get involved in the Christian education battle.

But maybe we don’t consider the Church because we're so
used to thinking in terms of individuals. Do you know of any books
or tapes that talk about “how the institutional Church can change
society”? How ironic. Americans are an extremely religious peo-
ple. I've already pointed out that the Church is the single largest
organization (however fragmented) in America. At a number of
places, I've even suggested that the Church can do more than an
individual.

Why is this? Hear the words of Christ.

Talso say to you that you are Peter, and on this rock I will build
My church, and the gates of Hades shall not prevail against it.
And I will give you the keys of the kingdom of heaven, and what-
ever you bind on earth will be bound in heaven, and whatever you
loose on earth will be loosed in heaven (Matthew 16:18-19).

He didn’t say He would build His political party, His family,
His Christian school system, His tract society, His satellite radio
network, or His corporation. He said, “I will build my church.”
Hades will not be taken by the Society of Christian Joggers,

142
