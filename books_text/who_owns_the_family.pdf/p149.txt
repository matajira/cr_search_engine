Conclusion 123

Principle 10: the principle of generational growth. The final princi-
ple is called “Who Owns the Future Generation?” It’s about the
generational expansion of Christianity. In a famous case involving a
Mr. Armstrong (1842), a minister tried to re-baptize his daughter,
after she had already been baptized. The court ruled the minister
was “out of line,” interfering with the covenantal obligations of
Mr. Armstrong as father, The Biblical basis for this ruling was
even included in the judge’s remarks. Why? Scripture teaches that
children are part of the covenant, and to be claimed by God.

In this chapter, I included three basic guidelines for rearing a
Christian dynasty: Destiny, Discipline, Dominion.

This is a future orientation of the family. God entrusts it to the
parents.

Summary

So, I’ve presented the ten principles of family ownership. This
chapter has attempted to provide a brief summary.

But, the task is not complete. We still need to apply what we've
learned about the family. By way of application, I want to discuss
what the family, church, and state can do to put the trusteeship of
the family back into the parents’ hands. In the following chapter, I
want to begin with the family.

What can the family do? Is it helpless? Are there specific
things your family can be doing? Yes. Let’s turn to the next
chapter to find out!
