162 Who Oums the Family?

treasuries usually create. Christians will then use this marvelous
opportunity to move society closer to a Biblical solution—a solu-
tion that protects the family. Meanwhile, we position ourselves as
the one group that sounded the alarm in advance.

Some Christians may say that this is an unloving, unchari-
table attitude. These Christians are embarrassed by God, and embar-
rassed by the specific requirements that God has clearly set forth
with respect to society’s legitimate and proper control of homosex-
ual behavior (Leviticus 20:13). This embarrassment about God’s
laws will be gone before this century ends. When AIDS begins to
hit the American heterosexual community, as it now has in
Africa, and when the terror comes close to Christian homes, and
when there is no room in hospitals for sick Christians, then today’s
hesitant, soft-hearted Christians will change their minds and stop
apologizing for God's supposed harshness.

A worldwide soctal and religious transformation is coming, in the form
of a virus. Christians and non-Christian heterosexuals are about to
learn what the penalty is for being embarrassed by God’s civil law
and refusing to impose it. The penalty is visible judgment. There
is nothing like God’s visible judgment to firm up practical Chris-
tian theology. There is also nothing like it for weakening (or even
eliminating) God’s opponents.

This may sound radical today. In 1995, it will sound too soft.

Resolution On Taxes
BE IT RESOLVED, that the party in Precinct #_____ op-

poses a state income tax and any new kinds of taxes or additional
tax increases on the State or local levels.

T have spent an entire chapter of this book discussing how a
graduated tax system has hurt the family. If your state has an in-
come tax, you've got to get rid of it. If nothing else, freeze taxation.
Many reliable sources say that if we can just /reeze taxation, we'll
be able to win eventually. Remember, as long as the State has a
graduated tax structure, you're going to have a difficult time ac-
cumulating a decent inheritance for your family.
