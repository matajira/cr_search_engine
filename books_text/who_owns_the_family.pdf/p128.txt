102 Wha Owns the Family?

reproachfully. For some have already turned aside after Satan. If
any believing man or woman has widows, let them relieve them,
and do not let the church be burdened, that it may relieve those
who are really widows (I Timothy 5:3-16).

The Church, not the State, is given the responsibility of taking
care of widows. Set instructions are given. Disbursements are not
to be given non-discriminately. Widows have to be a certain age,
and should have demonstrated their faithfulness.-The Biblical sys-
tem is not a blind, “hand-out” approach. “If someone will not
work, then he doesn’t eat!” But why is the Church given the role of
protecting and providing for the family?

Redemptive History

The Biblical history of the family sheds light on the Church’ role.

Let’s start with Genesis. The first family was told to “subdue
the earth” (Genesis 1:26). Did they do what they were told? No.
Adam and Eve failed and were judged, but God redeemed them.
He sacrificed animals that provided atonement, or covering. “As for
Adam and his wife, the Lord God made tunics of skin, and clothed
them” (Genesis 3:21). The family would have been lost if God had
not provided redemption, and pulled it back up into the Kingdom
of God.

Throughout the entire Old Testament, this story repeats itself.
The family falls, is judged, and redeemed. Each time,the message
is: the human family cannot save the world. A “new” family is needed.

When you come to the time of Christ, the Gospels speak as
though there is a conflict between the family and Christ. On one
occasion, Jesus says,

Do not think that I came to bring peace on earth. I did not
come to bring peace but the sword. For I have come to set a man
against his father, a daughter against her mother, and a daughter-
in-law against her mother-in-law. And a man’s foes will be those of
his own household. He who loves father or mother more than Me
is not worthy of Me. And he who does not take his cross and follow
after Me is not worthy of Me. He who finds his life will lose it, and
he wha loses his life for My sake will find it” (Matthew 10:34-39).
