38 Who Owns the Family?

A. 1st Commandment: No other gods means God owns the
family,

B. 2nd Commandment: No other worship means the family
that worships together stays together.

C. 3rd Commandment: No manipulation of God’s name
Means man is to live by obedience to God’s law and trust Him for
the results.

D. 4th Commandment: The family is to allow God to sanction
it by submitting to God’s structure of time: worship and rest one
day and work the other six.

E. 5th Commandment: Inheritance comes through faithful-
ness.

F. 6th Commandment: An attack on man is destruction of the
image of God. Since the “image” is “male and female” (family),
murder is an assault on the family,

G. 7th Commandment; Adultery directly affects the marriage
covenant.

H. 8th Commandment: Theft is an attempt to manipulate man,
As we saw in the 3rd commandment (paralleling the 8th), man is to
live by ethics not magic.

I. 9th Commandment: Children are to learn how to sanction
properly. Bearing false witness is an unlawful sanction.

J. 10th Commandment: Coveting what belongs to another is
an attempt to take someone else’s estate.

What could be added to this ethical standard? Jesus certainly
didn’t try to add anything. He said, “Do not think that I came to
destroy the Law or the Prophets, I did not come to destroy but to
fulfill. For assuredly, I say to you, till heaven and earth pass away,
one jot or one tittle will by no means pass from the law till all is
fulfilled” (Matthew 5:17-18). So, when a person accepts Jesus
Christ as his Lord and Savior, he accepts God's righteous standard
for his life. This is the standard for all families. If they lived by it
and taught their children to follow it, imagine what kind of society
we would have!

Our forefathers fled to this country to be able to Aeep these com-
mandments. Freedom to them was the liberty to obey God, not
disobey Him. How things have changed! The State should not set
