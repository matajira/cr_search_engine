What Can the Church Do? 155

against, and at the spiritual weakness of those who pray them.
They are like hand grenades. If you don’t intend to throw them,
don’t pull the pins.

Summary

I have presented what the Church can do to change society.
Three points were made.

1. I called for true prophetic preaching, the kind that chal-
lenges society. It’s the kind of preaching that proclaims and calls
down the judgment of God.

2. I pointed out that faithful “weekly communion” would keep
the Church from being condemned by the world. And, if the
world gets off the Church’s back, freedom comes to the family,
especially the families in the Church.

3. I said that Church discipline distances the State from. the
Church. Its only when the Church fails to discipline its own
members that the State can gain access. God says the Church will
“judge the world,” that is, if it disciplines its members.

Also, I explained a weapon of discipline, the imprecatory psalms,
that the Church can use against the State. James says, “You do
not have because you do not ask” ( James 4:2). Isn't it time we ask
God to put down the enemies of the Church by destruction or con-

- version?

The time Aas come. If the Church doesn’t respond with at least
these three plans of action, then our society will lose. We will lose.
Our children will lose. And, the Church certainly will lose be-
cause “judgment begins at the house of the Lord” (1 Peter 4:17)!!!
