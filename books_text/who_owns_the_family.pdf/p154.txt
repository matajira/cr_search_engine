128 Who Owns the Family?

Christians were beginning to wonder what would happen to this
once great Christian nation.

God’s people rolled up their sleeves and got busy reclaiming a
nation that belonged to God. They called on Him to honor His
Word, as He had promised in His Word:

When I shut up heaven and there is no rain, or command the
locusts to devour the land, or send pestilence among My people, if
My people who are called by My name will humble themselves,
and pray and seek My face, and turn from their wicked ways, then
I will hear from heaven, and will forgive their sin and heal their
land (II. Chronicles 7;13-14).

England recaptured its Christian heritage through the great
preaching of George Whitfield and John Wesley, Wesley's preach-
ing literally sobered up hundreds of thousands of the English
working class. It made them thrifty, future-oriented people. His
message of eternal salvation and earthly responsibility laid the
groundwork of the Industrial Revolution, which began in the
1780s.

Contrast England to France. The French were also a people
who had once been faithful to God, but had turned away from
their covenant. Did they come back to the Lord? No. Instead
their leaders listened to the philosophes, a group of pagan, God-
hating philosophers who thought the future hope of France was in
an evolutionary and revolutionary view of man, and not God.

The histories of England and France are quite different from
that point on. From 1789 to 1795, France went through a bloody
revolution. By the end of the 1790s, Napoleon Bonaparte, a ruth-
less military dictator, had come to power. He marched France into
military victories, and then defeat. French political life was dis-
rupted by the Revolution, and it has never fully recovered. Polit-
ical instability coupled with stagnant bureaucracy have been the
marks of French life for almost two centuries.

England, on the other hand, sailed into one of its finest hours
from the 1780s onward. England’s share of world trade soared; she
became the master of the seas. Industrial production increased
rapidly with the Industrial Revolution. By 1850, England was the
