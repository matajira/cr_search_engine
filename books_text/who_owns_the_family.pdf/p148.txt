122 Who Owns the Family?

Principle 6; the principle of life. “Who Owns Life?” was the
name of this chapter. Keeping in mind the covenantal grid, I re-
stated the five points of a sacred covenant. This parallels with the 6th
of the Ten Commandments, “You shall not murder.” The impor-
tant legislation here is Roe x Wade. For the first time, the State set
itself up as the final determinant of life and death. The Bible says
that this privilege belongs only to God. Since God considers un-
born babies human beings, so should the State. The most serious
problem with Roe a. Wade is that it destroyed the whole concept of
“due process,” the right of trial by jury.

Principle 7: the principle of sexual privacy. One case that I con-
sidered was People x Onofre. The other was Griswold v. Connecticut.
The issue was the “boundary of sexual privacy.” The first case
placed sexual privacy outside of marriage. All kinds of sexual perver-
sity were allowed: homosexuality, lesbianism, etc.

Does this mean the Bible is against sex? No. God created man
and woman, so He is not against sex. He’s against sex outside of
marriage. In the remainder of the chapter, I presented some basic
principles for sex in marriage.

Principle 8: the principle of worldview. The title of the chapter is
“Whose Worldview?” I wanted to discuss the whole question of
education. But, the real issue at risk is worldview. In Ohio v. Whisner
it was established that education determines a child’s entire
worldview, which makes education a religious question. Evidently
the State saw this because it said, “The real question is not which
Teligion is the best religion, but how shall the best be secured.”

What is the correct worldview? Using the “covenantal grid” of
the first chapter, I briefly developed a covenantal worldview.

Principle 9: the principle of protection. This chapter was called
“Who Protects the Family, and How?" I used the famous Quinlan
Case to demonstrate that the family needs other institutional protec-
tion. The State protects the family by implementing the capital
offense laws of the Bible, seeing all of these crimes are directly
against the family. The Church, however, protects the family by
being a true guardian of the needs of the family. The Church and
not the State is given this responsibility.
