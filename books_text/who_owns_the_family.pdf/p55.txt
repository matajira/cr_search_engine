By What Standard? 29

(2) You shall not make for yourself an idol, or any likeness of
what is in heaven above or on the earth beneath or in the water
under the earth. You shall not worship them or serve them; for I,
the Lord your God, am a jealous God, visiting the miquity of the
fathers on the children, on the third and the fourth generation of
those who hate Me, but showing lovingkindness to thousands, to
those who love Me and Keep My commandments.

(3) You shall not take the name of the Lord your Ged in vain,
for the Lord will not leave him unpunished who takes His name in
vain,

(4) Remember the Sabbath Day to keep it holy. Six days you
shall labor and do all your work, but the seventh day is a sabbath
of the Lord your God; in it you shall not do any work, you nor
your son or your daughter, your male or your female servant or
your cattle or your sojourner who stays with you. For in six days
the Lord made the heavens and the earth, the sea and all that is in
them, and rested the on the seventh day; therefore the Lord
blessed the Sabbath Day and made it holy.

{5) Honor your father and your mother, that your days may be
prolonged in the land which the Lord your God gives you.

B. Second Five Commandments

(6) You shall not murder.

(7) You shall not commit adultery.

(8) You shall not steal.

(9) You shall not bear false witness against your neighbor.

(10) You shall not covet your neighbor’s house; you shall not

covet your neighbor's wife or his male servant or his female servant
or his ox or his donkey or anything that belongs to your neighbor
(Ex. 20:1-17).

Commandment 1

The first part of the Biblical covenant establishes God as the
ultimate authority. He owns the world. This identifies God as the
source of the covenant.

The first commandment identifies God as the liberator. It
teaches that God’s redemption demands total allegiance. The
commandment begins, “I am the Lord your God, who brought
