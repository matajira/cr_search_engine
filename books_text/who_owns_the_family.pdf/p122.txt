96 Who Owns the Family?

bond between people and everything. Scripture says the bond
that holds life together is the covenant. Break it, and death
results, destroying the bond and incurring the judgment of God.

Let’s take death as an example. God says, “Whoever sheds
man’s blood, by man his blood shall be shed; for in the image of
God He made man” (Genesis 9:6). If a man breaks another’s con-
tinuity with life by murdering him, then he has to die, losing his
bond with the living.

Humanism tries to put continuity some place other than faith-
fulness to God’s Word. Using the same example of death, the Hu-
manist Manifesto IT, principle 2 says, “Promises of immortal salva-
tion or fear of eternal damnation are both illusory and
harmful. . . . There is no credible evidence that life survives the
death of the body. We continue to live in our progeny and in the
way our lives have influenced others in our culture.”

Then, in a story out of a public school, grade 5 social studies
text, humanism views continuity with life and death much differ-
ently from Holy Scripture. In a tale about an Eskimo we read,
“He was to save his own life by eating his wife. At first he only cut
small pieces from her clothing and ate them. . . . She ran for her
life, and then it was as if Tuneq saw her only as a quarry that was
about to escape him; he ran after her and stabbed her to death.
After that he lived on her, and he collected her bones in a heap”
(Man: A Course of Study, 1970).

See the difference? Continuity with life is based on the Word
of God in Christianity. But in pagan religions that communicate
the message of humanism, like the horrible story above, contin-
uity with life is according to disobedience. The one who breaks
God’s law and murders another is the one who lives.

So, the world and life view of the Bible is covenantal. The final
point of God’s system is continuity. Christianity teaches that the
true heirs of life will be Christians, The ones who get disinherited
are the unbelievers,

Summary
In this chapter, I’ve ventured into the heart of Christian edu-
cation: the proper world and life view. Whose worldview? God's
or man’s?
