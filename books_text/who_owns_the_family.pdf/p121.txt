Whose Worldvian? 95

or one where it is being upheld? In a day when anything but a
Christian morality is present in our society, the ethical aspect of a
Christian worldview is a major priority.

Sanctions

A Christian world and life view says there are consequences to
any act. Obedience is rewarded with blessing and disobedience is
punished. There is an “ethical” relationship between cause and
effect. Moses says, “So keep the words of this covenant to do them
that you may prosper in ail that-you do” (Deuteronomy 29:9).
Again, this is one of the five most important things a person must
learn in life.

Humanism, however, teaches that there are no lasting conse-
quences to immorality, not even a hell where people are eternally
punished. The Humanist Manifesto II, principle 3, says, “We affirm
that moral values derive their source from human experience.
Ethics is autonomous and situational, needing no theological or
ideological sanction.” Sanction. There’s that word again.

One psychology text says, “If a situation pressures a person to
act in a certain way, then the person is not likely to be judged as the
cause of the act” (Experiencing Psychology, Science Research Associ-
ates, 1978).

No judgment? Boy, are these humanistic authors in for a big
surprise when they die! The Bible emphatically teaches that there
are eternal consequences. Hebrews says, “And as it is appointed
for men to die once, but after this the judgment, so Christ was
offered once to bear the sins of many” (Hebrews 9:27-28).

See the connection the Bible makes? If there is no judgment,
then there is no real payment for sin, no Christ. That's at stake
with the humanistic worldview. Christians believe that because
their worldview includes real sanctions, there is a need for real de-
liverance, salvation. “No eternal sanctions—no salvation. No
eternal sanctions—no need for the cross.”

Continuity

The final point in the covenantal worldview speaks to every-
thing from who lives and dies, to inheritance. Continuity is the
