68 Who Owns the Family?

In Cincinnati, OH, abortuary allowed dense smoke to pour from
its chimney. When firemen arrived on scene, they were told,
“We're burning babies.”. . .

Massachusetts Supreme Court ruled goldfish could not be awarded
as prizes because that would violate the state’s anti-cruelty laws.
The same court upheld mandatory state funding of abortions. . . .

In California, babies aborted at 6 months were submerged in
jars of liquid to see if they could breathe through their skin (they
couldn’t). .. .

An Ohio medical research company tested brains and hearts of
100 fetuses as part of a $300,000 pesticide contract. . . .

Dr. Jeromino Dominguez writes, “On any Monday you can
see about 30 garbage bags with fetal material in them along the
sidewalks of abortion clinics in New York.”

Oddly, there are people—yes, even Christian people —who
will be repulsed more by my listing of these horrors than by the
horrors themselves.

The effects of abortion have been devastating. Words can
hardly describe the horrors created in our society by this one piece
of legislation.

In this chapter, the principle has to do with fe. Keeping in
mind the covenantal structure of the family, the principles start
over, following the outline of the Ten Commandments. Remem-
ber the sixth commandment, “You shall not murder.” Why does
God forbid murder? Because He images His “transcendence” in
man, So, the sixth commandment parallels the first, also teaching
the principle of “transcendence.” Since man images God, he
should not destroy another human who does the same. To do so is
tantamount to attempting to strike out at God. So, God speaks
very clearly about how life and death are determined. Here is
perhaps the most damaging effect of Roe » Wade.

Due Process
The infamous decision affected our entire judicial system.
What do I mean?
The common law principle of innocent until proven guilty has
been reversed. It’s not just that you're guilty until proven inno-
