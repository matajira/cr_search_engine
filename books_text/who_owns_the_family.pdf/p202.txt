SUBJECT INDEX

Abortion

Due Process, 68

Eugenic, 64
Activism

Christian, 66, 138
Adoption

difficulty of, xx

National Adoption Exchange, xxii

price ceilings; xx

world statistics, xx
Adultery, 34-35, 79
AIDS, 72, 145

Cost to tax payer, 161

Heterosexual community, 162

Jails, 165

Public view, 161
Amendments

9th, 65

14th, 65

16th, 52, 66, 151

16th, Never Legally Ratified, 56
American Medical Association, 63-64
Authority

Biblical, 19-23

church,

family, xi, 22

Layered, 22-23

Parental, 48

Plural, 20

Representative, 19
Atonement, 182

Baalism, 117
Baby-buying, xxi
Baptism, 109, 132

Barnett, Walter 75
Beckman, M.J. 56n
Benson, Bill 56n
Bestiality, 80

Bible Reading, 152
British Israelitism, 22

Cabot, Godfrey J. 112, 114
Carter, Jimmy xvii, xviii
Children’s Rights Humanist’s view,
16-17
Church
Courts, 152
Keys of, 143-144
Membership, 66, 130
Sovereignty, 166
weakness, x
Christian Education, 66, 133
Christian School Movement, 133
Cicero, 18
Civil Government
failure of programs, xviii
social & economic programs, xviii
welfare, xviii
Civil War, 156
Communism, 34, 132
Compulsory Education, 87, 99
Continuity
in Deuteronomy, 8
in Marriage, 11-12
Contract, 120
Corporation Excise Tax, 52
Covenant
Corxftinuity, 8
Definition, 6-7

176
