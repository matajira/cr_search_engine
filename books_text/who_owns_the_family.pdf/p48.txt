22 Who Owns the Family?

trines loosely called “British Israelitism.” (A frightening example
of such thinking, mixed in with open racialism, is the literature
written by the late Wesley A. Swift, whose writings were used to
create a heavily armed paramilitary band in California in the late
1950s and early 1960s, and whose pamphlets still circulate.)

No one sphere is allowed to establish primary authority into
the other sphere. Each one has certain responsibilities and
privileges that the others do not. Here is the “check and balance”
of “plurality” of leadership. There are three kinds of leaders:
elders in the Church, fathers in home, and civil magistrates in so-
ciety. Spheres limit other spheres. For example, if the State
becomes corrupt, the institutional Church must be more vocal
and attempt to correct the State in a lawful and prophetic way.

Layered Governments

‘Third, Biblical authority is dayered. Notice in the passage from
Deuteronomy that there is an “appeals” system. Again, a “check
and balance” feature appears. But the “layered” effect means no
earthly authority is absolute,

Sometimes authorities become corrupt and they have to be
disobeyed by appealing to a higher authority, someone who can be
“appealed to.” Such was the case when Daniel's friends, Shadrach,
Meshach, and Abednego would not bow to Nebuchadnezzar’s
statue of gold (Daniel 3:1-18). What happened? They were thrown
into a fiery furnace (Daniel 3:19ff.). This furnace symbolized a
“trial.” Whose trial? God’s! Although the king thought this was his
judgment, God’s court had been “appealed” to. The three right-
eous men were found innocent, and were protected.

Even the family’s authority is not absolute. Early in the history
of the Church, a woman should have disobeyed her husband! Here
is the story.

But a certain man named Ananias, with Sapphira his wife, sold
a possession. And he kept back part of the proceeds, his wife also
being aware of it, and brought a certain part and laid it at the apos-
tle’s feet. But Peter said, “Ananias, why has Satan filled your heart
to lie to the Holy Spirit and keep back part of the price of the land
