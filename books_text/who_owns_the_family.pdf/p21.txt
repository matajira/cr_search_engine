Introduction xxi

maternity care expenses that are so low that they don’t actually
cover expenses. Supposedly, the government wants to prevent
“baby-buying” and sets ceilings, for example, in the District of
Columbia, at $2,500. This means that the adoption agency can-
not pay for the maternity home care. This closes off a potential
solution for the pregnant girl, She often finds it easier, and less ex-
pensive, to have her own child destroyed in an abortion chamber
{Abortion Clinic),

Here is a classic example where a “free-market” approach to
unwanted pregnancies would “pay” mothers to carry their chil~
dren full-term and allow them to be adopted.

Let’s get our terminology clear. The Bible is clear: kidnapping
carries the death penalty (Exodus 21:16). The Bible teaches that a
society that honors God must inflict the highest possible penalty
on kidnapping in order to discourage the practice, Because of this
ever-present threat, it is legitimate for civil government to require
adoption agencies to keep careful records about the source of sup-
ply for any child, and to limit legal adoptions to transactions with
these conforming agencies,

Second, the Bible is clear that promiscuity is evil. It should be
restricted by government, but not ¢ivid government. It should be
restricted by church and family governments. The Church has a le-
gitimate concern for morality among its members. If the parents
fail to discipline their children, then the Church should get in-
volved in some sort of discipline (Matthew 18:15-18).

But family government is supposed to be the main parameter
for promiscuity. The head of the household of the daughter who
becomes pregnant is to decide whether to compel the marriage be-
tween the sinning daughter and her sinning male consort. If the
parent decides against the marriage, he or she can demand that
the male (or his family) pay a sufficient amount of money to pay
for the birth of the baby—not its execution, but its birth. The civil
government does not have any independent authority in this re-
gard; it simply supports the decision of the legally sovereign par-
ent (Deuteronomy 22:28-29).

Finally, if the unmarried daughter decides to have an adopting
