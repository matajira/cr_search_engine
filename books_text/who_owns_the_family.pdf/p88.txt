62 Who Owns the Family?

humanist-dominated television is its ally in this effort.

A very important person in the future is the chi/d. He repre-
sents the future. But in our day and time, the future is threatened
because of the brutal murder of children before they are born,
abortion. In the next chapter we want to consider “Who owns life?”
The Supreme Court? The mother? Who? It’s certain that every-
thing [’ve said about inheritance doesn't matter if a person kills all
his children before they become heirs. In a way, this is what's hap-
pening in our society. No inheritance. No heirs.

Let’s turn to the next chapter to learn how to save our heirs.
