78 Who Owns the Family?

monopoly over inter-personal sex.

This is what the sexual freedom crowd is desperate to deny. If
it gets the other two governments to accept this doctrine, then the
covenantal, legal integrity of the family is overthrown, and its
claim to protection from the other sovereign governments is also
overthrown.

The strategy of the humanists is clear: to create an unchallenged
State monopoly. The family’s monopoly is easiest to deny judi-
cially today in the name of “the separation of sex and State,”
meaning the separation of anti-family sex from civil prosecution.
This is being done through a coordinated campaign. It is interest-
ing that this self-conscious campaign attacks all five parts of the
Biblical covenant.

. Defining God as autonomous man (sovereignty)
Defining away the family (hierarchy)

Defining away sexual deviation (standards)

. Overturning laws against sexual deviants (sanctions)
5. Denying civil protection for infants (inheritance)

woe

The humanists’ tactics are clear: the family is the political
weak link of today’s relativistic social order, and therefore the hu-
manists’ primary political target. If they succeed in destroying its
integrity as a lawful covenant monopoly, then it’s “one down, one
to go.” The Church will be next. At the end of the process there
will be only one covenantal monopoly, the State. The sword will
rule all,

Does the Bible have anything to say about the proper principled
defense of this humanist strategy? Yes. Scripture not only con-
demns sex in any form outside of marriage, it speaks very posi-
tively about sex within the confines of holy matrimony. It offers
both a positive and a defensive campaign.

Sex Outside of Marriage

The Bible condemns every form of sex that is not performed in
Marriage.
