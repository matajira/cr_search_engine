56 Whe Owns the Family?

from the Lord and has begun to lose its blessing. Income taxation
was a curse sent by God to chasten a rebellious nation.

(As an aside, there is solid historical evidence that technically
speaking, the 16th amendment was never legally ratified. The
government simply declared that it had been ratified, despite ir-
regular voting procedures at the state level that nullified its
passage. Thirty-six states were required for its adoption. Ken-
tucky’s legislature, for example, did not adopt the 16th, but its
mistakenly illegal certification was counted. When the votes are
accurately counted, it turns out that only 32 states ratified the 16th
amendment. But voters were satisfied with the announced ratifi-
cation, then as now, and nobody bothered to check its legality un-
til the mid-1980s.}t

Voters have turned from the God who grants true freedom.
The heart of the Gospel is freedom. Christ said, “You shall know
the truth (Christ) and the truth shall set you free” (John 8:32).
Without the Gospel, people become slaves. What do you think
people were before missionaries brought the message of Christ to
the West? Just a horde of wandering barbarians. God delivered
the West from this oppression, and America was born out of the
legacy of the freedom of the Gospel. Today, however, Americans
want to be slaves because they have turned from Christ. Until
they turn back to Him and repent, they will remain slaves.

Slaves like taxes, just so long as they believe (mistakenly) that
the rich are paying a higher share than they are. If they can “get
even” with the rich by enslaving everyone, they will vote for
slavery every time. This is the sin of envy: destruction for the sake
of leveling the rich.

So, how do we repent? How do we recapture our inheritance?
We come to the fifth principle of the covenant of the family: Conti-
nuily, or inheritance, We must consider the Word of God to see what
God requires, Only by understanding and doing exactly what He

1. Bill Benson and M. J. Beckman, The Law That Never Was (Box 550, South
Holland, Illinois: Constitutional Research Assoc., 1985); XVI: The Constitution’s
Income Tax Was Not Ratified (1377 K St., NW, Suite #336, Washington, D.C.:
American Liberty Information Society, 1985).
