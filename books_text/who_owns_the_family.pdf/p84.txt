58

Who Owns the Family?

Now the king went to Gibeon to sacrifice there, for that was the
great high place; Solomon offered a thousand burnt offerings on
that altar. At Gibeon the Lord appeared to Solomon in a dream by
night; and God said, “Ask! What shall I give you?” And Solomon
said: “You have shown great mercy to your servant David my
father, because he walked before You in truth, in righteousness,
and in uprightness of heart with You; You have continued this
great kindness for him, and You have given him a son to sit on his
throne, as it is this day. Now, O Lord my God, You have made
your servant king instead of my father David, but I am a little
child; I do not know how to go out or come in. And your servant is
in the midst of your people whom You have chosen, a great people,
too numerous to be numbered or counted. Therefore give to Your
servant an understanding heart to judge Your people, that I may
discern between good and evil. For who is able to judge this great
people of Yours?” And the speech pleased the Lord, that Solomon
had asked this thing. Then God said to him: “Because you have
asked this thing, and have not asked long life for yourself, nor have
asked riches for yourself, nor have asked life of your enemies, but
have asked for yourself understanding to discern justice, behold, I
have done according to your words; see, I have given you a wise
and understanding heart, so that there has not been anyone like
you before you, nor shall any like you arise after you. And I have
also given you what you have not asked: both riches and honor, so
that there shall not be anyone like you among the kings all your
days (I Kings 3:5-13).

If God gave you one wish, what would you ask for? Solomon

was initially a faithful man. His priorities were right, at least until

he

started marrying foreign wives by the hundreds. He wished for

wisdom, Perhaps this is the reason that God could grant him such
a wish. God usually does not: give people what they want until
their priorities are right.

What Solomon’s life demonstrates is that it is not possible to

remain wise if you violate a major commandment of God, year
after year. Solomon remained smart; he just lost his wisdom for a
lengthy period. Wisdom is a product of obedience to God's laws;
disobeying the laws is the same as becoming unwise.

Nevertheless, Solomon's inheritance was both tangible and in-
