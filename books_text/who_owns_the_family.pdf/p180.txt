154 Who Owns the Family?

pray. But keep in mind that discipline is judicial. In other words,
the destruction of the wicked comes one of two ways: Actual
destruction and conversion. That's right. God could destroy the
wickedness of the State by converting it. He certainly did this to
the Roman Empire under Constantine.

So, I encourage you to try to get your church to pray the im-
precatory psalms, even in the regular worship service as a congregation,
The psalms, remember, are supposed to be prayed by the
Church. We're not to use these against our “personal” enemies.
These are enemies of the Church!

Church discipline in the “maledictory” and inner-church forms
is powerful. If the Church would use these, our world would
change. Our society would leave the family alone and let the
Church be its guardian.

Murray Norris of the Valley Christian University in Clovis,
California illustrates the power of imprecatory psalms. He has
been opposing pornography for years. Although he’s an educator,
he’s an expert at fighting immorality. Cities and church groups
regularly call him in to lead their local campaigns against porno-
graphy.

His success rate is phenomenal. He claims that if his eight steps
are followed, pornography can be stopped in any situation,
What’s his secret? I don’t know what he would say, but I think his
success rate is due to one of those eight points that advocates the
use of imprecatory psalms.

Usually the war against pornography boils down to a handful
of decadent individuals who stand in the way of morality. Murray
argues that above all else, pray that God would remove whoever
stands in the way. Pray that they would either convert or be
directly removed by God.

The results are powerful. Almost without exception, when an
imprecatory prayer has been prayed, the antagonists against
decency have retired, gotten sick (sometimes terminally ill), been
beaten at the next election, or died!

Like God's covenantal signs of baptism and communion, how-
ever, the imprecatory psalms cut both ways: at those prayed
