CONCLUSION

Let’s summarize what we've done so far. Do you remember
the ten principles? Do you recall the ten cases and pieces of legis-
lation? How about the title of the book? Can you call to mind
what this book is all about?

The title of this book is Whe Owns the Family? I've tried to an-
swer this question by means of ten principles, all developed
around ten pieces of legislation and/or cases that have changed
family life in America.

Principle I: the principle of sacred covenant. I started with “Cove-
nant, not Contract?” Why? The place to begin in answering the
question, “Who owns the family?” is here. In a famous case in
1888, Maynard v Hill, it was established that the family is not a
“mere contract,” but a sacred covenant, an “institution.”

I defined a sacred covenant as a “five-fold bond.” Using the
Book of Deuteronomy as a model, a marriage covenant consists of
the following: transcendence, hierarchy, ethics, sanctions, and continuity,
a “bond.”

Using this basic grid, the outline of the principles themselves
followed the five points of the covenant twice. So, the first chapter
establishes that the family is created by God; it is transcendent be-
cause it is a sacred covenant; it, not the State, is the trustee of children.

Principle 2: the principle of authority. Chapter 2 is called “By
Whose Authority?” Recent legislation, particularly New Jersey v
TL.O., encroaches on the family. In this particular case it was
ruled that teachers are not surrogate parents. The case was used
to limit the power of teachers to search, but in the process, the
whole zone of education was taken from the parents.

120
