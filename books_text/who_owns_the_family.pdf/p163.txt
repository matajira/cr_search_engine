What Can the Family Do? 137

moral foundation? “To protect them from today’s immoral envi-
ronment!” Question: Just what immoral environment are they in?
Silence. Angry silence. You have just blown their cover. You have
just confronted them with their own hypocrisy.

The children have been sent by their parents into the most im-
moral environment they are likely ever to face as Americans
(unless we suffer a military defeat), the most consistently anti-
Christian environment in America: the public school system.

They may blurt out, “We want the best education possible for
our kids.” Horsefeathers. For a generation, national test scores of
graduating seniors have been declining. Test scores of students in
Christian schools are consistently above grade level. Everyone
knows this, especially the parents who refuse to act in terms of this
knowledge.

But even if the public schools did prepare children better to
take tests prepared by public school educators, it would be irrele-
vant. Christian philosopher Cornelius Van Til has said it best: “It
doesn’t matter how well you sharpen a saw that’s set off angle; it
will never cut straight. Sharpen it, and it will still cut crooked,
only faster.” No matter how technically competent the local public
high school is —and declining test scores for a generation indicate
that it isn’t competent at all—the question isn’t technical compe-
tence; the questions are religious presuppositions and moral environment.

We shouldn't be misled. Christian parents know what they're
doing when they send their children into the public schools. They
know that the public schools are Baal-worshipping moral
cesspools. The fundamental issue is that they're too cheap to pay the
Christian school’s tuition. All the baloney about religiously neutral
textbooks and a neutral moral environment is nonsense, and they
know it. There is no neutrality in the war between good and evil,
between Christ and Satan, and every Christian knows it. Some
just won’t admit it when there’s money involved.

Maybe, just maybe, it isn’t a question of money. Maybe it's
that Dad wants Junior to be captain of the football team someday,
or Mom wants Sis to be homecoming queen. Maybe the local
Christian school doesn’t have a football team or a homecoming
