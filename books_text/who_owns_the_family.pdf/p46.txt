20 Who Owns the Family?

established authority and the people means the leaders were “rep-
resentatives.” Ad! government is representative. America has been built
on such a view of authority.

The authority of these ancient leaders was not in themselves.
It was “external,” or “objective,” to them. They represented God’s
authority, Their authority was completely in terms of what God
had given them, They were not to assume to themselves final au-
thority, nor were they to forget that their position came from God.

Plural Governments

Second, the Bible demands a plurality of governments. Notice
that there was not just “one” leader chosen by the Israelites. There
was plurality of leadership. Why? Biblical religion creates a sys-
tem of “checks and balances.” Again, we can see the influence of
the Biblical model on our own society. America, more than any
other nation, emphasizes the need for constitutional “checks and
balances.”

The underlying premise of Biblical pluralism in government is
that man is “totally depraved” in principle (though never in
history, for God restrains man’s sin). No one and no institution is
allowed to seek absolute authority, because the very quest for ab-
solute power corrupts men—not absolutely, for nothing man does
or can try to do is absolute, but such a quest does corrupt. It is the
quest to be as God, the original corruption (Genesis 3:5). Ab-
solute power belongs only to God. So there must be a system of
checks and balances.

Historically, the Christian faith has adopted a concept of sphere
sovereignty, Sphere sovereignty says that there are three spheres of
covenantal government in society: Family, Church, and State.
Although they overlap —a member of the family is also a member
of the State and may also be a member of the Church— the govern-
ments of these spheres are “self-contained.” By this I mean, each
sphere has certain governmental responsibilities that belong “ex-
clusively” to it.

Positively, the family has a monopoly of procreation, and
possesses legitimate sovereignty over children. (Children born
