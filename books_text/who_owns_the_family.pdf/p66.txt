4
WHO OWNS DISCIPLINE?

First, do you dare to discipline?

Second, if you do dare, will the State continue to allow you to
punish your children?

Don’t be too quick to answer these questions until you've
heard the horrifying experience of Paul Snyder, told by John
Whitehead in his excellent book, Parent's Rights. Mr. Whitehead is
a constitutional lawyer; I have thought it prudent to quote him,
word for word. Understand, I am not making up any of this. My
concerns are based on a frightening legal precedent. Writes law-
yer Whitehead:

“On June 18, 1973 Paul Snyder took his fifteen-year-old
daughter Cynthia to the Youth Services Center of the Juvenile
Department of King County Superior Court in Washington. For
some time Cynthia had rebelled against her parents. As one court
explained the situation:

Cynthia's parents, being strict disciplinarians, placed numer-
ous limitations on their daughter's activities, such as restricting her
choice of friends, and refusing to let her smoke, date, or partici-
pate in certain extracurricular activities within the school, all of
which caused Cynthia to rebel against their authority.

“Mr. and Mrs. Snyder hoped that the Juvenile Court Com-
missioner would ‘resolve the family dispute by admonishing
Cyndy regarding her responsibilities to her parents.’ Cynthia was
placed in a receiving home.

“A month later, however, Cynthia, with the help of casework-

40
