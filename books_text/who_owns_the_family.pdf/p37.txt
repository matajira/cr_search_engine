A Covenant, Nota Contract i

“naming” have to do with God’s law? God “commanded” domin-
ion over the animals, and then brought some to Adam to “name.”
This “classification” of them was the dominion mandate, the law
given to Adam. So, when Adam “names” the woman, he carries
out God's law. God’s law governs the first family. .

Freedom is the God-given liberty to obey, the liberty to avoid
disobeying God. The family’s greatest argument before an antag-
onistic civil government is that it asserts its God-given authority
to obey God’s law. When the ctoit government will not let parents raise their
children according to God’s law, then it is making itself out to be “God.”
This is the argument God blesses; this was the purpose for our
forefathers’ coming to this continent. They believed that the ethics
of the family covenant is the Word of God.

4. Sanctions (Genesis 2:24). The Biblical covenant is created
by an “oath,” the ceremonial reception of sanctions. So is mar-
riage. As the traditional marriage vows indicate, the marriage
oath is “self-maledictory”: “Till death us do part.” This is a death-
pledge. It is binding until the death of a partner. Also, however, the
pledge implies death to the one who does not persevere in the
marriage until the death of the partner.

How is a marital vow implied in “leave, cleave, one flesh”?
Genesis 2:24 speaks of a “legal” process of separation from the par~
ents, and the formation of a new household. Before someone
could “leave his father and mother,” he would have had to work
out the details on inheritance. He couldn't just walk away from his
parents. A legal “transition” from the “old house” was needed, re-
quiring an official ceremony.

Here is where the oath comes into play. Before the parents
would release their children, a legal pledge would have to be
made. Perhaps some symbolic gesture would also have been made.
In our day, the wedding ring is a token of the vow. In Christian
cultures, we don’t put rings in people’s noses, the way we do with
pigs or cows, but the meaning is the same. Each has legal powers
over the other’s behavior. Thus, Moses’ description points to sanc-
tions, received through some legal and official act of oath-taking.

5. Continuity (Genesis 2:25). Continuity means extension
