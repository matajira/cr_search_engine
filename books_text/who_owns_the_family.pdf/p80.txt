54 Who Owns the Family?

of taxation, we are incidentally able to possess the Government
and the stockholders and the public of the knowledge of the real
business transactions and the gains and the profits of every cor-
poration in the country, we have made a long step toward that
supervisory control of corporations which may prevent a further
abuse of power.” (emphasis added)

Tn case you got lost, let me summarize the significant elements
in Taft’s comments, One, up to that time, taxation was assessed
according to the population and had to go through the States. In
other words, the Federal Government could not tax apart from
them. .

Two, Taft is really providing the rationale for a graduated income
tax, one that taxes on “success.” The more you make, the more you
pay as a percentage of your income. Catch the point? Before 1913,
everyone would have paid the same amount (for example, an excise
tax or sales tax on a particular product). But the income tax is
specifically directed at the successful.

Some have said this was the beginning of the “transferrable so-
ciety,” that is, a society where money is transferred from one seg-
ment to another. It’s the Robin Hood game: take from the rich
and give to the poor (minus 20% for handling). Except, there
essentially were no government welfare programs in 1913 because
the poor were aided through private agencies and the Church.
The Robin Hood of government took from the rich in the name of
the poor and gave to itself.

Three, Taft clearly understood that such taxation would re-
quire more government involvement in the lives of the populace.
What he calls, “federal supervision.” Indeed, the Internal
Revenue Service (IRS) is an autonomous government agency
that has more power than any other government organization.
Before the IRS the citizen is legally guilty until proven innocent,
unlike the protection he receives under common law: innocent
until proven guilty.

Tafi’s rationale won the day. Within a few years, the 16th
amendment was passed, and life changed for the family, creating
a chain-reaction of events. The Federal Government went from a
