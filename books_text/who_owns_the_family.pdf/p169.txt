What Can the Church Do? 143

Christian publishers, or Christian movie producers.

We should keep in mind that Christ is talking about the
Church as an institution, not a bunch of individuals floating
around. Hell will not prevail against the institutional Church.
The fact is that many individuals will fall, but not the Church as
the Church.

Having said this, let’s put the words about “binding and loos-
ing” in “the institutional Church context.” Individuals don’t bind
and loose. The Church does, An individual may act on behalf of
the Church, but that’s the point. He’s not doing it, or should not
be doing it, as an individual. It’s the Church that will overpower
the kingdom of darkness.

How?

Christ destroys hell with “keys.” Immediately after Christ pro-
mises that hell can’t stand against the Church, He tells Peter that
he has been given “keys” to “bind and loose.” What do “keys” do?
They open and shut doors. So, by the “mere” power of opening
and shutting the door of the kingdom of heaven, hell is destroyed.

What are these keys? There are three things that open and
shut the door of the kingdom of heaven.

One, preaching of the Word of God opens and shuts the door.
Paul says,

How then shall they cal! on Him in whom they have not be-
lieved? And how shall they believe in Him of whom they have not
heard? And how shall they hear without a preacher? And how shall
they preach unless they are sent? As it is written: How beautiful
are the feet of those who preach the gospel of peace who bring glad
tidings of good things (Romans 10:14-15).

Two, the sacraments of baptism and communion open and shut
the door. Communion, for example, can serve the function of
shutting the door. Paul says,

Therefore whoever eats this bread or drinks this cup of the
Lord in an unworthy manner will be guilty of the body and blood
of the Lord. But let a man examine himself, and so let him eat of
that bread and drink of that cup. For he who eats and drinks in an
