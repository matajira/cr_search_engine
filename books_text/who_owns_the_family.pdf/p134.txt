10
WHO OWNS THE FUTURE GENERATION?

He who controls the youth of this generation will dominate the
next. Just about all great movements and leaders have recognized
this principle. Thats why Fascism invested so much in its youth
during the 1930s and early 1940s, even down to the end of WWIL.

In our time, there is a raging battle over who owns the future
generation, today’s children. The State would love nothing more
than to capture them. But our courts answered this question a
long time ago.

In 1842, the legal system of Pennsylvania had to judge an in-
teresting case. A Mr. Armstrong had a 17-year-old daughter thata
local minister wanted to baptize. Mr. Armstrong (a Presbyterian)
argued that his daughter had already been baptized as an infant,
and the minister had no right to take his daughter without his per-
mission. In violation of the father’s specific instructions, the min-
ister baptized the girl anyway. Armstrong was so angry that he
threatened the minister. The court made Armstrong put up $500
earnest money to guarantee that he would act peaceably for six
months until the matter could be resolved. When it came to deter-
mining who would have to pay the court costs, the court ruled in
favor of the father. The judge’s comments reflected a very interest-
ing understanding of the Bible,

Judge Lewis said, “It was justly remarked by Horry, Professor
of Moral Philosophy, in his treatise upon that subject, that the
words ‘train up a child in the way he should go,’ imply both the
right and the duty of the parent to train it up in the right way. That
is, the way which the parent believes to be right.

108
