Who Owns the Future Generation? 109

“The right of the father to command, and the duty of the child
to obey, is shown upon the authority of the Old Testament, to
have been established by God Himself. And the teachings of the
New Testament abundantly prove that, instead of being abro-
gated in any respect, the duty of filial obedience was inculcated
with all the solemn sanctions which could be derived from the
New Dispensation. The fifth commandment, ‘Honor thy father
and thy mother,’ was repeated and enjoined by St. Paul, in his
Epistle to the Ephesians. Children, obey your parents in the
Lord, for this is right. Ephesians 6:1.”

The judge was saying that the Old Testament taught that chil-
dren should be placed “in the Lord.” And, the New Testament did
not change this concept, because Paul says children are to be raised
“in the Lord” (Ephesians 6:1), not outside of Him.

So, what's the idea?

The Generational Principle

It’s the generational principle. The Bible teaches that Christi-
anity is to grow generation by generation. This is the fifth principle of
the covenant: continuity, The Church is to expand through evan-
gelism, but it is also to grow by raising up a “holy seed.”

All through the Bible, children of believers are claimed by
God. Abraham circumcised his household. Moses records, “Abra-
ham was circumcised, and his son Ishmael; and all the men of his
house, born in the house or bought with money from a stranger,
were circumcised with him” (Genesis 17:26-27).

The New Testament writers built on the same generational
view of the faith. Luke says about the conversion of Lydia, “And
when she and her household were baptized, she begged us, saying,
‘If you have judged me to be faithful to the Lord, come to my
house and stay’” (Acts 16:15). (emphasis added)

Notice that Lydia wanted them to “judge her.” Why not judge
everyone else in her house? They didn’t need to, since they judged
her. She was the adult believer, representing the others. She stood
for her children, and they were included in the covenant because
of her faith. She knew it, and for that reason placed her children
“in the Lord.”
