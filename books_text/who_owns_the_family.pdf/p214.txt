188 Who Owns the Family?

The Whole Bible

When, in a court of law, the witness puts his hand on the Bible
and swears to tell the truth, the whole truth, and nothing but the
truth, so help him God, he thereby swears on the Word of God—
the whole Word of God, and nothing but the Word of God. The
Bible is a unit. It's a “package deal.” The New Testament doesn’t
overturn the Old Testament; it's a commentary on the Old Testa-
ment. It tells us how to use the Old Testament properly in the per-
iod after the death and resurrection of Israel’s messiah, God’s Son.

Jesus said: “Do not think that I came to destroy the Law or the
Prophets. I did not come to destroy but to fulfill. For assuredly, I
say to you, till heaven and earth pass away, one jot or one tittle
will by no means pass from the law till all is fulfilled. Whoever
therefore breaks one of the least of these commandments, and
teaches men to do so, shall be called least in the kingdom of
heaven; but whoever does and teaches them, he shall be called
great in the kingdom of heaven” (Matthew 5:17-19). The Old Tes-
tament isn’t a discarded first draft of God’s Word. It isn’t “God’s
Word emeritus.”

Dominion Christianity teaches that there are four covenants
under God, meaning four kinds of vows under God: personal (in-
dividual), and the three institutional covenants: ecclesiastical (the
church), civil (governments), and family. All other human institu-
tions (business, educational, charitable, etc.) are to one degree or
other under the jurisdiction of these four covenants. No single
covenant is absolute; therefore, no single institution is all-power-
ful. Thus, Christian liberty is liberty under God and God's law.

Christianity therefore teaches pluralism, but a very special
kind of pluralism: plural institutions under God’s comprehensive
law. It does not teach a pluralism of law structures, or a pluralism
of moralities, for as we will see shortly, this sort of ultimate plural-
ism (as distinguished from institutional pluralism) is always either
polytheistic or humanistic. Christian people are required to take
dominion over the earth by means of all these God-ordained insti-
tutions, not just the church, or just the state, or just the family.
