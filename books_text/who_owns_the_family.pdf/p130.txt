104 Who Owns the Family?

The State

The State is to protect the family by implementing the death
penalty for the crimes specified in the Bible. How would this help
the family?

Have you considered how capital offense crimes of the Bible
are all virtually a direct attack on the family? Consider this as you
read the Apostle Paul’s list of death penalty crimes.

Therefore God also gave them up to uncleanness, in the lusts of
their hearts, to dishonor their bodies among themselves, who ex-
changed the truth of God for the lie, and worshipped and served
the creature rather than the Creator, who is blessed forever. Amen.
For this reason God gave them up to vile passions. For even their
women exchanged the natural use for what is against nature. Like-
wise also the men, leaving the natural use of the woman, burned in
their lust for one another, men with men committing what is
shameful, and receiving in themselves the penalty of their error
which was due. And even as they did not like to retain God in their
knowledge, God gave them over to a debased mind, to do those
things which are not fitting; being filled with all unrighteousness,
sexual immorality, wickedness, covetousness, maliciousness; full
of envy, murder, strife, deceit, evil-mindedness; they are whisper-
ers, backbiters, haters of God, violent, proud, boasters, inventors
of evil things, disobedient to parents, undiscerning, untrustwor-
thy, unloving, unforgiving, unmerciful; who, knowing the right-
cous judgment of God, that those who practice such things are wor-
thy of death, not only do the same but also approve those who prac-
tice them (Romans 1:24-27).

The little phrase “worthy of death” says it all. We know this
means the death penalty, because Paul used the same phrase
when he was accused of blasphemy. He said he had done nothing
“worthy of death,” meaning the death penalty (Acts 25:11).

Consider the effect these death penalties could have on family
life in America. Murderers would be condemned to die. Child
molesters would be put to death, which would mean the porno in-
dustry would die because there would not be any market. Abor-
tion would be stopped.
