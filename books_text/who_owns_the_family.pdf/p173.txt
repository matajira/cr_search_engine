What Can the Church Do? 147

Christ has died for sin and has overcome the judgment of God
through His Resurrection.” And, the Resurrection can bring
physical healing; It can even bring it to.AIDS victims. There's a
way out.

Institutionally, the medical experts say there is only one way
out: monogamy. One man, one woman: permanently, till death
(not from AIDS) do they part. The Church refuses to speak with
as clear a voice as the medical experts.

What will it take to get the Church to speak with a clear voice?
An outbreak of AIDS?

What will it take to get Christian parents to pull their children
out of the public schools? An outbreak of AIDS? (I can hear the
excuses: “Look, there hasn’t been a reported case of AIDS on
campus in over a month, And besides, our Charlie is making
straight-A’s.” Sorry, Charlie.

‘The Church will have to start by telling the world that sin is
the issue. America is in great sin. The State is in great sin. The
Church is in great sin. Family life is in great sin. And, it will all be
destroyed through disease, war, and any number of appropriate methods if
repentance does not come. .

If the Church proclaims this message, society will change. As
far as ['m concerned, it’s as though God is sending these judg-
ments and waiting for His Church to speak. If it does, people will
once again begin to listen. The State will listen. The family will
get its trusteeship back.

Sacraments

The second thing the Church needs to do is take the sacra-
ments seriously. Why do I imply that it doesn’t?

Most churches only take communion four times a year. Scrip-
ture indicates that it should be taken often. When Paul went to
Troas, Luke tells us, “Now on the first day of the week, when the dis-
ciples came together to break bread, Paul ready to depart the next
day, spoke to them and continued the message until midnight”
(Acts 20:7). The New Testament Church had communion every
week.
