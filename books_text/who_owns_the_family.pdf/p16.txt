xvi Who Owns the Family?

agreed upon about research on our society.

{1) Social disorder in our culture results from complete family
disorganization, and

(2) “Much of the same research also shows that the forces of
disorganization arise primarily not from within the family but
from the circumstances in which the family finds itself and from the way of
life that is imposed on it by those circumstances.”

Exactly!

I know of so. many parents who are trying to raise a family, but
they feel like they’re swimming upstream.

It seems like everything and almost everyone in our society is
against the family —the very thing most dear to them.

There may be those who “pretend” to defend the family, but
time after time, we find that their solutions are just as much a part
of the problem as the solutions recommended by someone who
comes right out and attacks the family.

Its like an interview in the March 15, 1979 issue of the Dallas
Morning News with Betty Friedan, founder of the National Organ-
ization for Women, and with the “mother of Women’s Lib,”
Muriel Fox, .

They seemed to care about the family, admitting that it is in “a
hopeless state of collapse.” Their proposal: “Innovative and prac-
tical solutions.”

What do they mean?

Their definition of the family says it all: “Family is people who
are living together with deep commitment and with mutual needs
and sharing.”

Get the point?

A family is enybody, even lesbians and homosexuals, “living
together with deep commitment.”

It doesn’t make any difference whether a family is built on a
heterosexual relationship. What really counts is the commitment,
even if it is perverted!

So, just because people say they are “for the family” doesn’t
necessarily mean anything. They may be, and probably are, part
of the unloyal opposition against the traditional household.
