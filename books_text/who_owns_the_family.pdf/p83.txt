Who Owns Your Inheritance? 57

tells us can we regain our inheritance, and pass it down to our
children. Let’s examine several elements of the principle of inher-
itance.

The Principle of Inheritance

An extremely wealthy man once came to me, and wanted to
know which of his children should receive his inheritance. He had
one son who was very wealthy, successful, but decadent. His
other son was young, energetic, poor, but-committed to Christ.
One of his daughters had rebelled early, but come back to the
family. Another daughter had been faithful, but recently turned
away from the Lord.

Which ones do you think should have received the inherit-
ance? Do you believe they should all have received “equal”
amounts? Should some have been disinherited? Should some have
received more than others?

I told this concerned father to keep three Biblical points about
inheritance in mind,

Tangible and Intangible

Inheritance is tangible and intangible. Scripture places the
greater emphasis on the intangible, while not excluding concrete
wealth.

Intangible wealth has to do with “character” and “ethics.” You
know the old saying, “Give a man a fish, and he'll eat for a day.
Teach a man to fish, and he won't need any more gifts.” It’s this
idea. If parents just give their children material things, but fail to
teach them the basic ethical principles of life, the children will squan-
der their wealth. That's exactly what's happening in our society.

Did you know the Bible has an entire book on the ethical prin-
ciples that should be handed down to the next generation? It’s
called the Book of Proverbs. Most of these are written by Sol-
omon, himself the richest man in the world in his day.

The thrust of the Proverbs is summed up by an event in Solo-
mon’s life, just after he became king. It demonstrates both tangible
and intangible inheritance, and where the priority should be placed,
