16 Who Owns the Family?

Let’s look at the perverted version first, so that we can accent the
Biblical principle. “

Through Elias’ travels in China, he perceived (vision) that
great fortunes could be made through opium. He was a rebel, and
he fell out of favor, unofficially, with the family for hiring outsiders
(non-Jews) in the family business. But through all of this, Elias
Sassoon displayed the rare ability éo use vice without becoming addicted
to vice.

Even if Jackson happens to be wrong in his findings about the
origin of the Sassoons’ fortune—though this seems unlikely —we
see the same principle in operation elsewhere. The Italians and
Jews who immigrated to this country and became involved in
organized crime illustrate the same kind of thing. All their lives,
they had been served wine with meals, and learned how to drink
without becoming drunkards. How did this help them to become
powerful in organized crime? Thomas Sowell explains in the fol-
lowing description of the Italian rise to power among organized
crime in the 1920s:

“Organized crime had existed in the United States before Ital-
ians became part of it. The leading gangsters were Irish or Jew-
ish, on into the 1920s. The introduction of Prohibition greatly in-
creased the scope of organized crime in the United States, at
about the same time that Italians were entering it in force. The
bootlegging of liquor and the operation of illicit drinking places
(often in conjunction with gambling or prostitution) became big
business—and a highly competitive business. The Italian gang-
sters had two decisive advantages in this violent and deadly com-
petition: (1) they could traffic in liquor without themselves becoming alco-
holics, and (2) family loyalties were as central to Italian crime as to Htalian
life in general. Sobriety and loyalty were particularly important in a
life-and-death business” (Ethnic America, New York: Basic Books
Inc., 1981, p. 125). (emphasis added)

The principle of using vice without becoming addicted to vice
has proven invaluable to all of the dynastic families, even the
families of the Mafia. The principle works because it is a corrup-
tion of an extremely important Biblical truth which wise Christian
