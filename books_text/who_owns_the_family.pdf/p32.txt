6 Who Owns the Family?

with the fact that that’s usually all there is. There is no covenant —
no sense that marriage is more than a mere contract.

The reason is simple. When a marriage covenant is drawn up
and filed at the courthouse, it’s hard to secure a divorce. Very
hard! So, in a day when our nation has turned away from God,
we should expect that the whole “covenantal” force of marriage is
ignored.

But what is a covenant? If the State has ruled that this is what
made marriage more than just a contract, an “institution,” and if it
proves that God owns the family, we should begin by defining a
covenant.

The Biblical Covenant

“Covenant” is a Biblical word, found many times all through
the Scripture. God’s Word is even divided into an Old Covenant,
often called Old “Testament,” and New Covenant, or New “Testa-
ment” (Hebrews 8:7, 13).

A “covenant” is a Divinelp created bond. The Old Testament’s
Book of Deuteronomy gives us a model. How do we know that it’s
a covenant? Because it’s the second (“deutero”) giving of the Ten
Commandments (“nomy”). Of the first copy, Moses says, “So He
declared to you His covenant which He commanded you to per-
form, that is, the ten commandments; and He wrote them on two
tablets of stone” (Deuteronomy 4:13). Since Deuteronomy is the
second giving of the law, it’s the second “covenant” with Israel.

Also, at the end of Deuteronomy, Moses calls the whole book
a covenant when he says, “So keep all of the words of this covenant
to do them, that you may prosper in all that you do” (Deuteron-
omy 29:9),

A Five-Part Program

Since Deuteronomy is a covenant, it becomes a guide. A quick
overview will help us to understand its fize parts.

1, Transcendence (Deuteronomy 1;1-3). A Biblical covenant is
established by God, not man. The covenant, therefore, always
begins by pointing to God’s transcendence. This word means “to rise
