150 Who Owns the Family?

But how does healing affect the rest of society? Show me a
Church that heals the sick, and you'll find an influential Church
in society. Let's face it. If the Church healed people, then society
would come to its side. So would the State.

In the Roman Empire, there was a bishop sent to a certain
town. He was bitterly opposed by the town’s officials. One day a
plague hit the community. A city official's daughter got the dis-
ease. He had heard that this bishop could heal, so he brought his
daughter to the Church. By the grace of God, the girl was healed.
Guess what happened? Virtually the whole town converted!

So, the second thing the Church can do to change society and
put trusteeship of the family back into the hands of parents is to
implement weekly communion.

Discipline
Third, the church can change society through discipline. What

does church discipline have to do with changing the State? Paul
gives the answer.

Dare any of you, having a matter against another, go to law be-
fore the unrighteous, and not defore the saints? Do you not know that
the saints will judge the world? And if the world will be judged by
you, are you unworthy to judge the smallest matters? Do you not
‘now that we shall judge angels? How much more, things that per-
tain to this life? If then you have judgments concerning things per-
taining to this life, do you appoint those who are least esteemed by
the church to judge? I say this to your shame. Is it so, that there is
not a wise man among you, not even one, who will be able to judge
between his brethren? But brother goes to law against brother, and
that before unbelievers (I Corinthians 6:1-6). (emphasis added)

Paul basically says that the Church is supposed to judge the world.
The Church is the final institutional judge—not the State, but the
Church. More important, Paul rebukes the Corinthians because
they are not disciplining their own members. They aren't settling
their own problems. So, trouble in the church is spilling out into
society and allowing the State to intervene. The bottom line is
that the State gets control and judges the Church, tantamount to
judging Christ!
