Who Owns Life? 69

cent. It’s that you’re guilty if you can be proven innocent. The acts
of unborn babies are neutral before God (Romans 9)— man’s only
period of neutral acts. Yet it is only after they are born, and begin
to be accountable for their external actions before God, that
American civil law protects their lives.

Tn our land, a person is supposed to be guaranteed a #rial by
Jury before he is executed. This points to the principle I’m trying to
underscore in this chapter, God owns iife, not man. The whole no-
tion of a trial by jury originated from the Biblical concept of due
process. In other words, God owns life and a trial by jury is a
“check and balance” to make sure God’s judgments are carried
out. Instead, modern law has shifted away from any acknowl-
edgement of an Absolute Standard. The Supreme Court has placed
the decision of life in the hands of the mother, and taken it from
God.

Now the precedent has been set. Since one group of human
beings has been classified “less than human,” why can’t it happen
to another group?

It has been discovered that in the Soviet Union, insane
asylums are full of Christians, because believers in God are con-
sidered “insane.” After all, they have all been educated in Marxist
schools according to Marxist logic, so what else could faith in God
point to, if not insanity? Logic proves atheism, so these people are
dangerously illogical. They are 2 menace to society.

Why couldn’t the Supreme Court some day decide that Chris-
tians are a “menace to the society”?

It happened before, you know: in the Roman Empire.

Biblical Law

God owns life. Only His Word should determine who can and

should live.
Consider a very important passage on the subject.

Tf men fight, and hurt a woman with child, so that she gives
birth prematurely, yet no lasting harm follows, he shall surely be
punished accordingly as the woman's husband imposes on him;
and he shall pay as the judges determine. But if any lasting harm
