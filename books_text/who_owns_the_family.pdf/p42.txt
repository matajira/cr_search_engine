16 Who Owns the Family?

school officials do not merely exercise authority voluntarily con-
ferred on them by individual parents; rather, they act in further-
ance of publicly mandated educational and disciplinary policies
. . . [S]chool officials act as representatives of the State, not merely
as surrogates of the parents .. .”

Do you see what is being ruled in this case?

Authority has shifted away from the parent to the State. Of
course, anyone who puts his child in a public school has already
granted such authority to the civil government implicitly. But this
case sets precedent for everyone else in New Jersey. And we can
be sure that other states will appeal to this decision.

By whose authority? The State has recently ruled that the au-
thority is the State’s. The authority shift challenges the parents.

New Rights For Children
(As summarized by John Whitehead in Parent’s Rights, pp. 24-25)

Richard Farson’s Birthnghts lists ten rights for children that are
phrased to be a direct attack on parental authority.

1. The Right to Self-Determination: Children should have the
Tight to decide matters that affect them most directly.

2. The Right to Alternate Home Environment: Self-determining
children should be able to choose from among a variety of arrange-
ments; residences operated by children, child-exchange programs,
twenty-four hour child-care centers, and various kinds of schools
and employment opportunities.

3. The Right to Responsive Design: Society must accommodate it-
self to children’s size and to their need for safe space.

4, The Right to Information: A child must have the right to all in-
formation ordinarily available to adults— including, and perhaps
especially, information that makes adults uncomfortable,

5. The Right to Educate Oneself: Children should be free to design
their own education, choosing from among many options the kinds
of learning experiences they want, including the option not to at-
tend any kind of school.

6. The Right to Freedom from Physical Punishment: Children
should live free of physical threat from those who are larger and
more powerful than they.

7. The Right to Sexual Freedom: Children should have the right to
conduct their sexual lives with no more restriction than adults.
