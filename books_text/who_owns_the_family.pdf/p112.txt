86 Who Owns the Family?

The “Failure” of the Family

The logic works this way: the State requires children to be
educated; the State uses tax money to create financial institutions
for these children who are compelled to be educated; the State
pays, so the State can legitimately determine what gets taught;
finally, all children are supposed to attend State institutions.

If something sounds wrong with this syllogism, good! To help
everyone to see what’s wrong with its logic, let’s add the word “re-
ligious” to “education.”

‘The State requires children to be religiously educated; the State
uses tax money to create financial institutions for these children who
are compelled to he religiously educated; the State pays, so the
State can legitimately determine what religion gets taught; finally,
all children are supposed to attend State religious institutions.

What's wrong with this? Isn’t it true that children need relig-
ious education to be good citizens? Of course it’s true. We also
know that some parents just won't give their children the proper
religious instruction. (This inevitably assumes the existence of a
God-determined definition of “proper.” But it also requires an in-
stitution to enforce it. The question is: Which institution?) Fur-
thermore, we know that other parents are thoughtless, and won't
spend the money to buy good religious education for their children,
Must we therefore conclude: “The State therefore needs to inter-
vene and both compel and pay for such religious instruction”?

But, the perceptive person will conclude, “This makes the
State sovereign over the family.” That’s exactly what it does. The
integrity of the family is undermined. And the family is the insti-
tution that God has assigned for the religious instruction of chil-
dren (Deuteronomy 6:6-7). It is the family which is to determine
what proper religious instruction is for its children, for better or
worse.

A perceptive person will therefore conclude: “It’s not worth the
risk. Don’t ever allow the State to compromise the family to this
extent, no matter how many parents aren’t giving proper religious
instruction to their children.” In short, hands off!
