164 Who Owns the Family?

taining the present status of private, parochial, and home schools
free from state government control.

You should know by now that the entrenched, tax-supported,
humanist State bureaucrats want to control what your children
learn. It’s probably the biggest fight today between the family and
the State. You’ve got to push for positive legislation that will pro-
tect your right—even if you don’t want to use it—to educate your
own children without governmental control. If you don’t, then the
government will control everything. It’s an all or nothing battle!

Resolution On Right-To-Work

WHEREAS, any individual should have the freedom to bid
competitively for any job he desires without being forced to join or
pay dues to any organization as a condition of employment; there-
fore

BE IT RESOLVED, by the party in Precinct #____ that
the State of _____ institute, or maintain a Right-To-
Work Law.

What happens to a family if its members, particularly the par-
ents, are not able to work without oppression? They're severely in-
hibited. Unfortunately, the government seems to be in the busi-
ness of all kinds of oppression. That's why it protects labor unions
in many states. We don’t advocate government anti-union activ-
ity. We just advocate a man’s legal immunity from persecution or
violence if he bids on a job. If you want freedom for the family,
you've got to push for the right-to-work, by which we mean “right
to bid.”

Resolution On Opposing Gun Controt

BE IT RESOLVED, that the party of Precinct #______
reaffirms the right of American citizens to keep and bear arms, as
guaranteed by the second amendment of the U.S. Constitution,
and opposes any and all legislation which would restrict that right.

You can’t expect to preserve the traditional family if you can’t
protect it. Keep in mind that Communism is rapidly coming close
