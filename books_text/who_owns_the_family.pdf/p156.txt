130 Who Owns the Family?

She is obligated to come back to the Lord, just like Israel of old.
Let's begin with the family. Returning ownership of the family
to the proper trustees will have to be part of America’s return to
Christ. In this chapter, I want to present three things you can do
with your family to put its ownership back into the right hands.

Church Membership

The first thing you need to do with your family is join a good
Church, and submit your family to the discipline of worship. Re-
member the passage I mentioned earlier about King Hezekiah?
Listen to what the people did after hearing they needed to return
to God’s House.

Now many people, a very great congregation, assembled at
Jerusalem to keep the Feast of Unleavened Bread in the second
month. They arose and took away the altars that were in Jeru-
salem, and they took away all the incense altars and cast them into
the Brook Kidron. Then they slaughtered the Passover lambs on
the fourteenth day of the second month, The priests and the Lev-
ites were ashamed, and sanctified themselves, and brought the
burnt offerings to the house of the Lord. They stood in their place
according to their custom, according to the Law of Moses the man
of God; the priests sprinkled blood which they received from the
hand of the Levites. For there were many in the congregation who
had not sanctified themselves; therefore the Levites had charge of
the slaughter of the Passover lambs for everyone who was not
clean, to sanctify them to the Lord. For a multitude of the people,
many from Ephraim, Manasseh, Issachar, and Zebulun, had not
cleansed themselves, yet they ate the Passover contrary to what
was written. But Hezekiah prayed for them saying, “May the good
Lord provide atonement for everyone who prepares his heart to
seek God, the Lord God of his fathers, though he is not cleansed
according to the purification of the sanctuary.” And the Lord lis-
tened to Hezekiah and healed the people. . . . The whole congre-
gation of Judah rejoiced, also the priest and the Levites, all the
congregation that came from Israel, the sojourners whe came from
the land of Israel, and those who dwelt in Judah. So there was
great joy in Jerusalem. . . . Then the priests, the Levites, arose
