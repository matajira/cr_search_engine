x Who Owns the Family?

kingdom that is governed by God’s revealed law. Its supporters
act in the confidence that all other things of value will be added
unto those who seek to establish God’s kingdom (Matthew 6:33).
This third religion is at last experiencing a revival, It has slept for
over 300 years. It is a threat—no, it is the threat—to the power
religionists.

Weakened Covenants

Sadly, the institutional church is simply not taken seriously
these days, either by the humanists or the average church mem-
ber. The church’s covenant-based threat of excommunication
doesn’t scare many members, especially Protestants. Churches
refuse to honor each other’s excommunications, and as a result,
they have stripped their own authority to just about zero, Now
that they find themselves increasingly under siege by the local tax-
man, the educational bureaucrat, and even the prosecuting at-
torney (for example, the Oklahoma case of a church successfully
sued by a self-acknowledged adulteress because the church ex-
communicated her), they have begun to discover the price of his-
toric impotence.

Church officers no longer believe that they, as ordained
Church officers, possess the God-given power and responsibility
before God provisionally to cast men into hell, unless these con-
demned people publicly repent. They no longer believe Christ’s
words to church officers: “Assuredly, I say to you, whatever you
bind on earth will be bound in heaven, and whatever you loose on
earth will be loosed in heaven” (Matthew 18:18). Having lost faith
in their power provisionally to condemn men’s eternal souls, they
have also lost faith in the institutional authority of the church, But
if church officers have lost faith in this crucial power of the
church, what protection can they expect today from the self-
imposed fear of pagans?

This leaves the family as the covenant institution that people
think is able to defend itself from the unlawful encroachments of
