A Pastor’s Response 307

those who neglect the Law of God as their standard for living
and discerning is wearisome and oppressive. From those within
the midst of the church who had been the most critical of theonomy came
the most shrill and unkind criticisms. I have several lengthy letters
on file from the most ardent critics of theonomy who have
“graciously” tried to point out all our problems. The anti-Law
people have in a number of cases been the most Jacking in the
fruit of the Spirit (Galatians 5:22,23).

“Theonomisis Are Troublemakers!”

Second, the label “theonomy” is becoming a catch-all term
for “troublemaker.” If one desires to smear another's reputa-
tion, then all he has to do is label him a “theonomist.”

Before I came to this pulpit, the pulpit committee was
warned about not hiring a “theonomist.” Comments such as
“watch out for him; he is a theonomist” are common in church-
es today. If this isn’t raw and blind prejudice, 1 don't know
what is. Not only are good men’s reputations being hurt, but
this form of bigotry makes it difficult to intelligently and ratio-
nally discuss the modern day application of the Mosaic Law.

There are three classes of people that Christians today have
created in response to the heated issue of theonomy. The first
class is the average Christian whom Jesus says we are to love
(John 13:34,35). The second class is our enemy whom we are to
love as well (Matthew 5:44). Then there is the “theonomist.”
With this class it is acceptable and even commendable to im-
pute ungodly motives (for example, “hidden agenda”), smear
his reputation, or plant seeds of doubts in others’ minds con-
cerning his orthodoxy. (I actually had a family question wheth-
er I was a true believer when the issue of theonomy came up!)
No doubt there are “brash,” “partisan” and “hard-nosed” theo-
nomists, but this doesn’t follow for everyone who adheres to
that perspective. The critical net has been catching fish that
don’t belong.
