Editor’s Conclusion 327

A Pair of Self-Imposed Burdens

Why? Why did the Calvinists buckle — worse, bow — to New-
ton’s Arianism in philosophy and John Locke’s Unitarianism in
political theory? Because they have carried a unique burden in
the modern history of the Church International: the burden of
scholarship. They have long regarded it as their unique, God-
assigned task to meet the humanists on a supposedly level
playing field — the pluralists’ myth of neutrality ~ and present
the case for Christianity. Only the Jesuits have carried a greater
academic burden. Calvin and Loyola studied at the University
of Paris in the 1520’s at the College de Montaigu. Calvin left in
1528, the year Loyola entered. The two men went on to build
rival movements that were strongly committed to the intellect as
a tool of evangelism. This strategy was certainly true of the
Jesuits, and true of Calvin’s followers, if not of Calvin himself.

This strategy has proven to be suicidal for the Jesuits and
nearly so for the Calvinists. Both groups decided that the best
way to do battle was by sending their brightest young men into
the fleshpots ~ or inkpots ~ of academic humanism. What can
be called the “German marathon” became a gauntlet for Calvin-
ists, and very few survived this ordeal. Almost none survived it
intellectually unscathed, as Van Til did his best to prove
throughout his career. “Get that Ph.D., my boy, and then you
will be entitled to join the Order,” said the leaders of both
orders, Jesuit and Calvinist. The Jesuits often required two
Ph.D.’s from their “boys.” But this strategy consistently failed.
The Jesuits kept “going native” on the foreign mission field,
while the Calvinists kept going pluralist on the home mission
field. (The biographies of famous Calvinist foreign missionaries
in the twentieth century could be published in a book as short
as one of my favorite short books, Famous Gentile Violinists.)

The Jesuit Order went radical almost overnight, 1965-66,""

15, Malachi Martin, The Jesuits: The Society of Jesus and the Betrayal of the Roman
Catholic Church (New York: Touchstone, 1987); cf. Garry Wills, Bare Ruined Choirs:
