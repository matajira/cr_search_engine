Theonomy and Calvinism’s Judicial Theology 29

Calvinistic methodology, a procedure for doing theology that I
first learned while a member of Coral Ridge Presbyterian
Church in Ft. Lauderdale, Florida, and later as a student at
Reformed Theological Seminary in Jackson, Mississippi. The
sovereignty of God over all of life became the operating princi-
ple for “doing” theology.

This principle of divine sovereignty, when applied to the
Bible, demands an absolute subservience to all of its prescripts,
not only in the sphere of the church, but in all walks of life. God
is the absolute Sovereign of all of life; therefore His Word should
be the controlling factor in every sphere of life’s activity.’*

The Calvinism I was introduced to was more than the “five
points.” Calvinism is not simply a synonym for predestination.
Calvinism, as I was taught, was a world-and-life view. In its
broader aspect, said Monsma, Calvinism “has a strictly scientific
meaning. It is a well-defined system of ideas, ~ of ideas con-
cerning God and man, concerning the moral, social, and poilliti-
cal life of the world. It is an organic structure, complete in
itself.”"* In becoming a Calvinist, I was assured that I would
find this “well-defined system of ideas” in the Bible.

As seminary students, we were heirs of Calvin’s Geneva, the
Puritans, and the Hodges of Old Princeton. The Bible is the
standard. All things are to be evaluated in terms of Scripture.
There is to be no compromising. This was the legacy I had

13, Ibid., p. 4.

14. Ibid., pp. 2, 3. Beattie wrote: “Hence, the Calvinistic system is seen to com-
mend itself to thoughtful minds as the sound philosophy of nature and providence,
and as the true interpretation of the Scriptures and of religious experience. This
system has a philosophic completeness, a scriptural soundness, and an experimental
accuracy which afford it strong logical confirmation, and give it secure rational
stability. It may be safely said that no other system can justify so fully this high claim,
for even those who profess no sympathy with the Calvinistic system have never yet
been able to present a better one for our acceptance.” Francis R, Beattie, The Presbyte-
rian. Standards: An Exposition of the Westminster Confession of Faith and Catechisms (Rich-
mond, Virginia: The Presbyterian Committee of Publication, 1896), p. 5.
