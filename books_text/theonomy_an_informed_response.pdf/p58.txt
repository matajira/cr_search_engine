38 THEONOMY: AN INFORMED RESPONSE

stealing from the Christian fuel depot) and retrieved our stolen
wings, they knew that they could no longer get their planes in
the air?

Maybe the critics didn’t like the theonomic plane’s ultimate
destination. Fair enough. But those of us who were interested
in the debate were waiting for an alternative plane to take off
with a better (biblical) flight plan. None was ever forthcoming.
Debate was silenced, and the pilot was dismissed.**

What really sold me on studying the issue of theonomy was
how weak the critics’ arguments were in their attempts to
ground the theonomic plane. In our classes related to covenant
theology, classic Reformed (continuity) arguments were used
against dispensationalism. When theonomy became an issue,
students found that dispensational (discontinuity) arguments, the
same arguments that were refuted in the classes related to
covenant theology, were being used in an attempt to answer
and discredit theonomy. Schizophrenia reigned in the mind of
any thinking student.

82. The humanist, in order to keep his plane aloft, must borrow from the
worldview presupposed in the Bible. The humanist plane loses altitude and eventu-
ally crashes when he assumes he can dump the fuel he stole from the pump marked
“Biblical Presuppositions.”

38. Others have observed the lack of a systematic working out of social theory by
those who are best described as “critics of contemporary culture.” James Skillen
points out that while Chuck Colson “offers keen insights into contemporary public
life,” he stops “short of proposing anything systematic.” While “he lauds the states-
manship of William Wilberforce, the carly nineteenth-century English evangelical
who led the movement to abolish the slave trade,” Golson “draws too few conclusions
from the study to suggest what a just political order and noble statesmanship should
look like.” James W. Skillen, The Scattered Voice: Christians at Odds in the Public Square
(Grand Rapids: Zondervan, 1990), p. 65.
