Editor's Conclusion 319

Bible-professing Christians.” In short, the vast majority of Chris-
tians believe that frying or dicing 50 million or so unborn ba-
bies a year, worldwide, is judicially tolerable, although of course
in bad taste, biblically speaking, but to recommend stoning
convicted murderers is literally perverse. Even the anti-abortion
groups that are willing to state publicly their biblically valid
assertion that abortion is murder are unwilling to say that an
anti-abortion law should be enforced (but not ex past facto) by
the only civil sanction that the Bible mandates for murderers:
public execution. I contend that the anti-abortion movement is
impotent politically in part because it is impotent theologically.
Its members do not take God seriously, so their opponents do
not take them seriously. They mock God’s law and then get mocked.

Sweet Jesus in Heaven (and Only in Heaven)

What has all this got to do with Theonomy: A Reformed Cri-
tique? A great deal. That volume, published by the academic
arm of a fundamentalist publishing house, lends support to the
most widely shared Christian myth of our era, namely, that
Jesus Christ abolished the civil sanctions that the Trinitarian
God had established in Israel. God required these capital civil
sanctions in order to serve as warnings to those who would
break His covenant by disobeying His laws. The capital sanc-
tions established by God in Old Covenant Israel were to serve
as public warnings — pledges — of what would take place when
the criminal’s soul was transferred by an act of the civil govern-
ment ~ execution — into God’s court of primary jurisdiction.

Most of the authors of Theonomy: A Reformed Critique believe
that the civil sanctions specified by the Mosaic law have been
abolished. This is the common view of every Christian in the

2, I note as an aside that a good number of Christians also take offense at those
few people who picket local abortion clinics. I speak here from many years of person-
al experience, Announcing themselves as Bible-believing Christians, they shout: “Do
you think God is proud of you people? You're an embarrassment to God!”
