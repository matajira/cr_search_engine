248 THEONOMY: AN INFORMED RESPONSE

on the local church’s assets. Any view of unconditionality that
destroys a modicum of discrimination or conditionality is not, as
we shall see, actually unconditional. Such a view ends up taking
from the widows and orphans to “unconditionally” feed those
outside the church. The poor widows of the church, who are
under its judicial conditions, necessarily lose whenever those
outside the church but inside its benevolence are not said to be
under its judicial conditions.

If God expects those on the inside of grace to live by condi-
tions, as Keller freely admits, then why not also those on the
outside of the church? This is saying that those without grace
have fewer sanctions than those with grace, which doesn’t
sound like real grace at all! We shall discuss this further in a
moment when we get to the real issue with Dr. Keller.

What About the Good Samaritan?

Ina sense, Dr. Keller recognizes the need to be wise and not
“give aid naively,” as I have already noted. But in another
sense, he attempts to deny this by pointing to the passages in
the New Testament where apparently no discriminatory process
was used. Here is another serious misrepresentation.

For example, Dr. Keller cites the Good Samaritan story as a
case in point. But this passage is not talking about the able poor
in the situation with the beaten man on the road to Jericho. He
is unable, and as I have made it abundantly clear in this essay
and all others on the subject of poverty, no judicial conditions
should be placed on the unable poor. (This does not mean that
in a world of scarcity there are no economic conditions.)

Dr. Keller also refers to those passages in the New Testa-
ment that command the believer to give to the needy. But the
New Testament does not explain hew and what should be given.
When the text says, “Give to anyone who asks,” it does not
specify the gift. Could it not be a job or an opportunity to work
in the case of the able poor?
