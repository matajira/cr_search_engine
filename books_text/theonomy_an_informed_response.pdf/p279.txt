Hermeneutics and Leviticus 19:19 259
A Question of Judicial Discontinuity

I agree with Poythress’ slatement regarding the exegetical
imperative. It is therefore mandatory on me or on another
defender of theonomy’s hermeneutic to do what Poythress says
must be done: (1) identify the primary function of an Old Cov-
enant law; (2) discover whether it is universal in a redemptive
(healing) sense or whether (3) it is conditioned by its redemp-
tive-historical context (i.e., annulled by the New Covenant). In
short: What did the law mean, how was it applied in ancient
Israel, and how should it be applied today? This task is not
always easy, but it is mandatory. If a person understands the
basic principles of biblical interpretation - continuity and dis-
continuity — the task gets much easier.

One hermencutical rule can help us make sense out of the
continuities and discontinuilies between the covenants: a change
in the sacraments accompanies a change in the priesthood. Whenever
there is a question of a change in judicial administration — a
suspected covenantal discontinuity - we need to ask this ques-
tion: Was the Old Testament law under discussion in some way
connected to the sacraments? If so, we should expect to see its
annulment or radical alteration - one might even say altaration
— in the New Testament.

Let us ask another question: Is a change in the priesthood
also accompanied by a change in the laws governing the family
covenant? Jesus tightened the laws of divorce (Matt. 19:3-9).
The church has denied the legality of polygamy. Did other
changes in the family accompany this change in the priesthood?
Specifically, have changes in inheritance taken place? Have
these changes resulted in the annulment of the jubilee land
laws of the Mosaic economy? Finally, has an annulment of the
jubilee land laws annulled the laws of tribal separation?

At this point, I am suggesting a weakness in the Westminster
Confession’s tripartite division of biblical law: moral, ceremonial,
and judicial. The moral law is said to be permanently binding
(XIX:2). The ceremonial law is said to have been abrogated by
