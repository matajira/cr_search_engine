358 THEONOMY: AN INFORMED RESPONSE

There were some ministers, however, who refrained from
appealing to the Bible for examples and prescriptions for re-
form. Rev. Nathaniel Ward (¢. 1578-1652), pastor at Ipswich,
Massachusetts, in his election sermon of June 1641, grounded
“his propositions much upon the Old Roman and Grecian
governments.” John Winthrop (1588-1649), first governor of
Massachusetts, described this as “an error.” There was good
reason for Winthrop’s objection: Why should the church appeal
to “heathen commonwealths” when it is the heathen principles
that have made it necessary for the church to be involved in
reform efforts? Winthrop believed that “religion and the word
of God make men wiser than their neighbors,” (hus, “we may
better form rules of government for ourselves” than to adopt the
failed principles of the past, what he called, “the bare authority
of the wisdom, justice, etc, of those heathen common-
wealths.”** It was the heathen past that had to be swept clean
if the people of God were to become the model of Christian
charity that Winthrop spoke about aboard the flagship Arbella
in 1630. Such a task is no less true in our day. Centuries of
specifically Christian activism must be swept under the histori-
cal rug if Christian scholars of Gcisler’s persuasion are correct.

Do these comments apply to the faculty of Westminster
Seminary? That remains to be seen. We shall see if they res-
pond in print or in classroom Iecturcs to our three volumes of
detailed answers to their published criticisms. We shall see if
theonomists are invited to debate on campus. We shall see if
their misrepresentations continue. We shall see if they continue
to attack theonomy by adopting implicitly some version of
natural law theory, the alternative explicitly adopted by Dr.
Geisler.

Time will tell. So will a fair share of the seminary’s brighter
students. Jesus is the truc vine (John 15:1). Rest assured, His
earthly grapevine is alive and well. Cover-ups eventually fail.

24. Thid., p. xxv.
