Whose Conditions for Charity? 235

He was not attempting to write a position paper on poverty. If
anything, he was presenting a primer on Christian economics,
not poverty. It is highly unfair to draw such strong conclusions
from an author when he is speaking in another context. Never-
theless, Chilton was attacking, among other issues, the humanis-
tic state welfare approach advocated at that time by Ron Sider.

North and Grant

What Keller does with North’s comments is extremely mis-
representative. He quotes North from an introduction to a
book, in The Shadow of Plenty, by George Grant, an author
highly endorsed by Keller himself. Grant was a publicly avowed
Christian Reconstructionist at the time that he wrote the books
that Keller likes so much. He was the Reconstructionist writer
of the 80’s who best represented the Christian Reconstruction
position on poverty. Yes, North makes very pointed comments
about aspects of poverty. But Keller tries to make it sound as
though North is against helping poor people. Then why did he
publish Grant’s book, as well as Grant’s other book, The Dispos-
sessed: Homelessness in America (1986)?

Dr. Keller does not tell you what North goes on to say in the
same introduction. North writes about the need for Christians to
help the poor. Consider the following statement from the same
section written by North, which Keller conveniently neglects to
cite:

We must put our hand to the plow and do the real labor of
charity. We can not effect reconstruction by proxy. We must, as
Grant so aptly points out “transform poverty into productivity.”
... We prove to the world that we don’t intend to let everyone
starve. We thereby build up institutional alternatives to state
welfare programs. .. . Therefore, it is our job as Christians to
preach a Word-and-deed Gospel. We must preach both with our
mouths and our actions. We must regain dominion through
more effective service, both to God and the lost [emphasis mine].
