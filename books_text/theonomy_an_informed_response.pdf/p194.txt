174 THEONOMY: AN INFORMED RESPONSE

from Christianity via Qumranian doctrine” is not certain. But
in either case, the structure of Hebrews suggests the contrast of
two approaches to God for salvation: through the eternal, living
Christ; or through the temporal ceremonial system of the Mosa-
ic administration (via a “purified” Judaism).

Hebrews 7:11-12

A major reason Johnson misconstrues the flow of Hebrews,
deeming it contra-theonomic, is due to his interpretation of
Hebrews 7:11-12. That passage reads:

Therefore, if perfection were through the Levitical priesthood
(for under it the people received the law), what further need was
there that another priest should rise according to the order of
Melchizedek, and not be called according to the order of Aaron?
For the priesthood being changed, of necessity there is also a
change of the law.

Johnson comments on this passage: “Now, the introduction of
this new principle of priestly appointment when the new priest
(Jesus) undertakes his office demands a change in the law. This is
so because on the basis of the Levitical priesthood the people
were given the law (Heb 7:11)... . [T]he priesthood of Aaron
and his sons is the very foundation of the law given through
Moses to Israel” (p. 184).

First, we should note that his analysis of Hebrews 7:11 (“on
the basis of the Levitical priesthood”) is open to a counter inter-
pretation. Johnson argues that the Law as such is founded
upon the Levitical priesthood by translating epi as “on the basis
of.” He suggests that “the priesthood of Aaron and his sons is

20. Hughes, Hebrews, p. 77; William Sanford LaSor, The Dead Sea Scrolis and the
New Testament (Grand Rapids: Eerdmans, 1972), ch. 14. J. W. Bowman, Hebrews,
fames, I @ H Peter (London: Tyndale, 1962), pp. 18-16. E E. Bruce, Epistle o the
Hebrews (NIGNT) (Grand Rapids: Eerdmans, 1964), p. xxix. Bruce points out the
emphasis on various washings as evidence (Heb. 6:2ff; 9:19f).
