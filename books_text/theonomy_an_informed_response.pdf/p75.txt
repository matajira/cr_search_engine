Some Wings for Modern Calvinism’s Plane 55

tuted, and obedience to its ordinances, if they be in accord with
these eternal principles, is the duty of the Christian.”

It is one thing to talk about the ethical requirements easily
distilled from general revelation, but theonomists are still wait-
ing for someone to demonstrate that this can actually be done.
Theonomists often catch general revelation advocates borrow-
ing from the theonomist’s garden, similar to the way humanists
borrow from the Christian’s garden. But as our nation moves
steadily from an ethic that most Americans recognize as being
Bible-based, any ethic based on general revelation will dissipate
as quickly as a morning fog vanishes at the appearing of a
blazing sun,

Robert Bork, in the Preface to Herbert Schlossberg’s Idols for
Destruction, recognizes the “borrowed capital” principle.

Some few years ago friends whose judgment I greatly respect
argued that religion constitutes the only reliable basis for morali-
ty and that when religion loses its hold on a society, standards of
morality will gradually crumble. I objected that there were many
moral people who are not at all religious; my friends replied that
such people are living on the moral capital left by generations
that believed there is a God and that He makes demands on us.
The prospect, they said, was that the remaining moral capital
would dwindle and our society become less moral. The course of
society and culture has been as they predicted, which certainly
does not prove their point but does provide evidence for it.*

General revelation operates only when it is constantly checked
and balanced by special revelation.

Does the Mosaic law, beyond the general requirements of the
Ten Commandments, play a role in the Calvinist’s ethical

37. Heyns, “Calvinism and Social Problems,” Gad-Centered Living, p. 236.

$8, Robert H. Bork, “Preface” in Herbert Schlossberg, Idols for Destruction:
Ghristian Faith and Hs Confrontation with American Society (Washington, D.C.: Regnery
Gateway, [1983] 1990), p. xvi.
