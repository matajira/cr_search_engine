Editor’s Introduction to Part HIT 203

amillennialism and premillennialism share a common pietism.
Their view of church history is the same, so their view of salvation
- the healing power of the gospel — is the same.

Transformation

With respect to transformation, there are two models that
are presented in the name of Christianity. The first is postmil-
lennialism’s model: the transformation of the world through the
widespread transformation of individuals. In short, a compre-
hensive gospel will bring comprehensive salvation in history to a
world in comprehensive sin, This will be accomplished through
the church’s empowering by the Holy Spirit. Its motto: “Sin is
no more comprehensive than the gospel, and much less power-
ful as time goes on.”

The other model is fundamentalism’s: the transforming
power of the gospel is limited to individual hearts. At most, the
gospel can transform families and churches. [t cannot transform
society, Its social reform model is. the skid-row mission: sober
up a few bums and send them to McDonald’s to work. Perhaps
a few of them will even become assistant managers before they
die or Jesus returns to rapture the church, whichever comes
first (presumably the latter).

Amillennialism is very close to fundamentalism’s model,
except that the amillennialist thinks that the return of Christ is
indeterminate, so the ex-bum may even have time to make it to
manager, unless the persecution of Christians begins (which,
Van Til always said, is inevitable).

Getting bums off skid row is a worthy goal. The question is:
How many bums can the church hope to get off and keep off
skid row? Another question is: Can we expect the bums to
escape their present economic and social condition if the
church and society place no legal conditions on the aid that
they receive? Is biblical charity conditional? Ray Sutton examines
Timothy Keller’s essay on charity and shows why Keller’s model
