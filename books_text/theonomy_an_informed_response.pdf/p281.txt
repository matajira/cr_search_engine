Hermeneutics and Leviticus 19:19 261

three areas transformed the traditional economy of Europe.
The modern capitalist system, with its emphasis on private
ownership, the specialization of production, and the division of
labor, steadily replaced the older medieval agricultural world of
the common fields. Vet this comprehensive economic transfor-
mation was seemingly accompanied by the violation of at least
the first two, and possibly all three, of the “separation” statutes
of Leviticus 19:19.

This raises an important covenantal issue: the predictability
of the external corporate blessings of God in history. A civiliza-
tion-wide violation of these Levitical laws has produced (or at
least has been accompanied by) an historically unprecedented
increase in wealth. We must therefore conclude one of three
things: (1) the laws of Leviticus 19:19 are no longer binding
because of a change in covenantal administration; (2) the coven-
antal link between corporate obedience and corporate blessings
no longer holds in New Testament times, although the laws of
Leviticus 19:19 do hold; or (3) these laws and God's corporate
sanctions are still in force; therefore, the modern world is head-
ed for a horrendous covenantal judgment of God because of
systematic violations of this particular Old Testament law. The
first conclusion is the proper one. This chapter explains why.

The Industrial Revolution

It was the industrial revolution of the eighteenth century
that visibly transformed the traditional economy. This is not to
say that industrialism somchow appcared overnight. It did
not.” But to characterize England as the first industrial society
would not have been accurate much before 1760. After 1800, it
was an appropriate designation. There is no question that the
economy of England in 1800 was a radically different kind of
system than anything the Israelites would have understood.

12. John U. Nef, The Conquest of the Material World: Essays on the Coming of Indus-
trialism (New York: Meridian, 1964).
