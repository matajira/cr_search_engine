Westminster Seminary on Penology 129

based on Hebrews 7:11, that the Jews were given the law “on the
basis of the Levitical priesthood”; the priesthood of Aaron “is
the very foundation of the law given through Moses to Israel”
(pp. 184, 185).

An equivocation trips up Johnson’s thinking here. “The law”
has been taken in one sense (the one intended by the author of
Hebrews) as the Mosaic administration of the Old Covenant or-
der, but in Johnson’s argument against theonomy “the law” is
taken in a different sense (viz. the moral stipulations revealed by
Moses).

The exegetical problem is that Johnson has chosen to take
the Greek word epi (Heb. 7:11) in the sense of “upon the legal
basis of.” While epi may take the sense of “upon the basis,” as in
Hebrews 10:28, it is there used in the dative case; any importa-
tion of the specific sense of legal basis comes from the context
(not the semantics of construction). In no instance of which I
am aware in Hebrews does epi with the genitive (as we find in
the Heb. 7:11) take the sense of “upon the basis.” For example,
God has not spoken to us “on the [legal] basis of these last
days” (Heb. 1:2), and God does not write His law “upon the
[legal] basis of their hearts” (8:10)! The author of Hebrews did
not believe that the moral stipulations of Moses were legally
predicated upon the Aaronic priesthood - or even that the
Mosaic administration of the covenant was legally grounded
upon that priesthood (whatever legal grounding could mean in
that statement). In Hebrews 7:11 he says, rather, that the law
was given “in association with” (or even “at the time of”) the
giving of the Aaronic priesthood; they coincided.

Once we correct the erroneous interpretation given to the
preposition in Hebrews 7:11 by Johnson's argument, it is evi-
dent that the question he poses in terms of it - “how sweeping
is this change of law?” (v. 12) - is not the open door to the
possibility of widescale change that he anticipates. The change
of law is a change regarding precisely that priesthood which was
instituted in association with it. The author of Hebrews himself
