26 THEONOMY: AN INFORMED RESPONSE

Calvinism and the Reformed Tradition

There is a very direct thinking process that leads someone
who views Calvinism to be the most consistent expression of
Christianity to adopt the distinctives of theonomy. Theonomy is
Calvinism’s judicial theology applied. The reader should keep in
mind that theonomy is a methodology, a way of understanding
God's law. Theonomy is not simply a body of texts woodenly
applied to a modern context. Theonomy is the application of
Reformed theology to the sphere of ethics. Greg Bahnsen made
this crystal clear in the preface to the first edition of Theonomy
in Christian Ethics, He repeats it for us in the second edition:

[T]he present study leaves a great deal to be explored and
discussed in Christian ethics as well as extensive room for dis-
agreement in the area of exegeting, understanding, and applying
God’s law in specific situations. Two people can submit to the
exhaustive theonomic principle in Christian ethics while dis-
agreeing on a particular moral question (e.g., whether a certain
biblical command is ceremonial or moral, whether lying is ever
condoned by God, etc.) Thus agreement with the thesis of this
book is not contingent upon agreement in every particular moral
issue or specific interpretation of a scriptural text.®

In principle, theonomy states that all of God’s Word is “pro-
fitable” and applicable, equipping the man of God “for every
good work” (2 Tim. 3:16). Disagreements over how a passage
applies is not an indictment against theonomy. Theonomists
want to know, contra its critics, what exegetical reasons are used
for rejecting contemporary application of God’s law. A person
who dips into the Mosaic legislation and makes a contemporary
application is in some sense a theonomist, even though his
application might differ from what other theonomists have
written. It’s the fact of application and not so much the how of

6. Second edition, Phillipsburg, New Jerscy: Presbyterian and Reformed, (1977)
1984, p. xxx. Reprinted in 1991.
