Copyright, Institute for Christian Economics, 1991

 

Library of Congress Cataloging-in-Publication Data

Theonomy : an informed response / Gary North, editor

p. cm.

Includes bibliographical references and index.

ISBN 0-930464-59-1 (alk. paper) $16.95.

1. Theonomy. 2. Calvinism. 3. Dominion theology. 4. Law
(theology). 5. Westminster Theological Seminary (Philadel-
phia, Pa., and Escondido, Ca.) 6. Reformed Church - Doc-
trines. I. North, Gary

BT82.25.T442 1991
230°.046-dc20 91-33321
cIP

Institute for Christian Economics
P. O. Box 8000
Tyler, Texas 75711
