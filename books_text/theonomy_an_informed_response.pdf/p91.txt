Fear of Flying: Clipping Theonomy’s Wings 71

Scripture? Are we to turn to “prudent analysis” to prove that
Christianity is true over against all other religions? Whatever
happened to the “self-authentication” of Scripture, both in its
general and particular pronouncements? Is this all that’s left of
Cornelius Van Til’s legacy? With Muether’s approach we are
left with only a “rational probability.”®°

So, why send your children to a Christian school if all we
need is “careful and prudent economic analysis”? Why read the
Bible for anything more than “spiritual” guidance? Muether
claims that the Bible is not needed for economics. In fact, he
takes a swipe at “contemporary evangelicalism” in general for
its “biblicist hermeneutic that depreciates the role of general
revelation and insists on using the Bible as though it were a
textbook for all of life.”*' Does he mean general revelation as
a scientific investigation of God's created order so that man
learns to be a better scientist, agronomist, and medical practitio-
ner by study and experimentation?

General Revelation

Henry Van Til wrote that “Man does not need special reve-
lation for acquiring the arts of agriculture or of war, the tech-
niques of science and art; these things are learned from nature
through the inspiration of the Spirit."** No one is disputing
the use of general revelation in this way. But even this type of
investigation has numerous ethical implications. For example,
knowledge of what works in the field of medicine still leaves
doctors and legislators with, for example, decisions relating to
abortion and euthanasia. An abortionist can be an expert in the
way he performs an abortion. He has honed this “skill” through
scientific study of the created order (general revelation). But is

30, Gornelius Van Til, The Defense of the Faith, p. 335.
31. Muether, “The Reformed Attraction,” p. 254.

32. Henry R. Van Til, The Calvinistic Concept of Cultere (Grand Rapids: Baker
Book House, 1959), p. 162.
