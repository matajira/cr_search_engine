Editor’s Conclusion 331

comes to someone who believes in both theonomy and six-day
creationism, the seminary’s attitude can accurately be summar-
ized: “Better professors with no terminal degrees than with two
of them, if theonomy and creationism are part of the deal!”
‘What about the seminary’s response to abortion? Paul Wool-
ley, who taught church history at Westminster for almost half a
century, was a vocal pro-abortionist. He kept his job. The
Board sees no inconsistency between abortion and biblical eth-
ics. The seminary has taken no stand on the abortion question.
In short, regarding the life-and-death issues of our day,
Westminster Seminary, Reformed Seminary, and Covenant
Seminary have remained officially silent. No use stirring up
controversial, donor-alienating trouble! Better a systematic
silence than a donation-threatening “Thus sayeth the Lord!” If
the three schools (five campuses) ever decide to unite, the new
institution should be called Laodicea Thcological Seminary.

Predestination Plus Amillennialism

The Calvinists have also labored under a second burden, one
that the Jesuits always avoided: the doctrine of predestination.
This doctrine teaches that man is fully responsible, yet God is
completely sovereign. Add the eschatology of inevitable histori-
cal defeat for the church (amillennialism) to the doctrine of
predestination, and you have a cultural blueprint ideal for
ghetto-building. A few amillennial Calvinists do proclaim the
legitimacy of a Christian worldview (undefined), but they also
preach against the possibility of its triumph in history.

Twentieth-century Calvinism has been overwhelmingly amil-
lennial. The postmillennialism of the Scottish tradition disap-
peared; the Dutch amillennial tradition has triumphed in Cal-
vinism’s academic circles. Amillennial Calvinists have correctly
concluded that if amillennialism is true, then their victories will
be few and far between. They have therefore tended to engage
only in those battles that they have believed they had an out-
side possibility of winning. Academically, this means battles of
