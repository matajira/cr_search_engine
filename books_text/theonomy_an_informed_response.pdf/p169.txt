 

Civil Sanctions in the New Testament 149

The written Law is the standard; that which is in question is
the rabbinic interpretation ("You have heard it said”) versus
Christ’s correct interpretation (“But I say,” cf. Matt. 7:28-29)."°
The later Mishna refers to rabbis Hillel and Shammai as the
“fathers of antiquity,”° much in the way Jesus here refers to
rabbinic predecessors.

Evil Aititudes and Civil Sanctions

Third, in an attempted reductio ad absurdum, Johnson com-
ments: “Learning from Jesus that the insult is a far more seri-
ous offense than any of us would have imagined, should Chris-
tians urge their government to make it a capital crime?” (p.
178). This misses the whole point of Jesus’ instruction. The
Pharisees tolerated vindictive anger by focusing only on the
express penal sanction of the Law against murder, But the
prohibition against murder also involves the heart attitude that
leads to it (cf. Mark 7:21; Jms. 4:1), says Jesus. Yet nowhere in

to its purity, by ridding it of their degraded comments.” “Christ in fact had not the
least intention of making any change or innovation in the precepts of the Law. God
there appointed once and for all a rule of life, which He will never repent of. But
with the Law overlaid with extraneous commentaries, and distorted out of its proper
intention, Christ champions it from out of the hold of all these excrescences, and
demonstrates its true purpose, from which the Jews had departed.” “Christ is not to
be made into a new Law-giver, adding anything to the everlasting righteousness of
His Father, but is 1o be given the attention of a faithful Interpreter, teaching us the
nature of the Law, its object, and its scope.” ({Ihid., pp. 182, 183, 184),

19, “Also, what was proclaimed at Sinai is not set aside but is given its deeply
spiritual interpretation by Jesus Christ (cf, Matt, 5:17).” Hendriksen, Matthew, p. 261.
“Thus far [to v, 20] the Saviour had been speaking of the law and of its precepts, as
they were in themselves, without any reference to the form under which his hearers
were familiar with them, and on which their views of the divine law must be found-
ed. This peculiar form had been imparted to the law by the traditional accretions and.
the superstitious practice of the Pharisees... . They were ostensibly the strictest
moralists, and much of the intolerable burden under which the people groaned,
arose from their unauthorized additions to the law which their followers confounded
with the law itself.” J. A. Alexander, The Gospel According to Matthew Explained (Lynch-
burg, Virginia: James Family, rep. (1861]), p. 134.

20. Mishna, Eduyoth 1:4, Sec also B. Pick, The Tatmud, What it Is (New York:
Schocken, [1887]), p. 23. Cited in Hendriksen, Matthew, p. 296n.
