Editor’s Introduction 7

more than Lutheranism has. Lutheranism has been less insistent
than Calvinism on the necessity of special revelation for every
sphere of human knowledge, and for that reason too, Lutheran-
ism has been less insistent on the concept of an absolute self-
consciousness of God than Calvinism has.”

To accept the idea that civil law in New Testament times
must be conformed to Old Testament laws and sanctions is to
accept the ideal of Trinitarian theocracy: the rule of God in
every realm of life, not excluding the civil realm.’ This was the
Puritans’ position on law, or at least the pre-1660 Puritans,
before King Charles IJ was restored to the throne. This was the
theology of the dominant Reformed factions during the English
Revolution of 1640-60. This was the position of at least some of
those Reformed theologians who attended the Westminster
Assembly (1643-47), which had been called by Cromwell’s Par-
liament to advise the national civil government — a fact admit-
ted by Sinclair Ferguson’s essay in the Westminster symposium.
But this Puritan view of the civil covenant is not the position of
Westminster Seminary.

Westminster’s faculty chose to reject this position publicly.
They reject theonomy’s hermeneutic, namely, that each of
God's revealed Old Covenant case laws is still in force, having
been resurrected with Christ unless a New Testament revelation or
principle has annulled it. The faculty has adopted the Napole-
onic Code’s standard of justice when dealing with any Old
Covenant case law: “Guilty until proven innocent.” The theono-
mists, in contrast, take English common law as their guide to
the Old Covenants case laws: “Innocent until proven guilty.”
This is the heart of the dispute over hermeneutics.®

7. Cornelius Van Til, A Survey of Christian Epistemology, vol. 2 of In Defense of
Biblical Christianity (Den Dulk Foundation, [1932] 1969), p. 71.

8. Note: theocracy is also an inescapable concept. It is never a question of
theocracy vs. no theocracy, It is a question of whase theocracy: God's, Satan's, or self
proclaimed autonomous man’s. A god must rule. Some god’s law must be sovereign.

9. In the 1970's, Westminster's theological problem was Herman Dooyeweerd;
