Books for Further Reading 365

Theological study of the implications of postmillennialism for
economics, law, and reconstruction.

Rushdoony, Rousas John. Thy Kingdom Come: Studies in Daniel
and Revelation. Phillipsburg, New Jersey: Presbyterian and Re-
formed, 1970. Exegetical studies in Daniel and Revelation, full
of insightful comments on history and society.

Shedd, W. G. T. Dogmatic Theology. 3 volumes. Nashville,
Tennessee: Thomas Nelson, (1888) 1980. Nineteenth-century
Reformed systematics text.

Strong, A. H. Systematic Theology. Baptist postmillennialist of
late nineteenth and early twentieth centuries.

Sutton, Ray R. “Covenantal Postmillennialism,” Covenant
Renewal (February 1989). Discusses the difference between
traditional Presbyterian postmillennialism and covenantal post-
millennialism. :

Terry, Milton S. Biblical Apocalyptics: A Study of the Most Nota-
ble Revelations of God and of Christ. Grand Rapids, Michigan:
Baker, (1898) 1988. Nineteenth-century exegetical studies of
prophetic passages in Old and New Testaments; includes a
complete commentary on Revelation.

Postmillennialism and the Jews

De Jong, J. A. As the Waters Cover the Sea: Millennial Expecta-
tions in the Rise of Anglo-American Missions 1640-1810. Kampen:
J. H. Kok, 1970. General history of millennial views; through-
out mentions the importance of prophecies concerning the
Jews.
