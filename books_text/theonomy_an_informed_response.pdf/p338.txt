318 THEONOMY: AN INFORMED RESPONSE

“The hands of the witnesses shall be first upon him to put him
to death, and afterward the hands of all the people. So thou
shalt put the evil away from among you” (Deut. 17:7).

Notice what the critics are saying. God used to require the
Israelites to stone certain convicted criminals. “Again, thou shalt
say to the children of Israel, Whosoever he be of the children
of Israel, or of the strangers that sojourn in Israel, that giveth
any of his seed unto Molech; he shall surely be put to death:
the people of the land shall stone him with stones” (Lev. 20:2).
Obviously, God used to be sick. He was the Sick Old Being in
the sky. But then Jesus came and replaced the Sick Old Being.

This is an ancient heresy. It is called Marcionism. Marcion in
the second century A.D. proposed a two-gods theory of history:
the malevolent Creator of the Old Testament and the merciful
redeemer of the New Testament. While Christians do not go
this far, they still have a similar view of God's civil sanctions. He
used to be a hard-nosed S.O.B. in His Jewish phase, but now
He pays no attention to what civil governments legislate or
don’t legislate, enforce or don’t enforce: God, the civil creampuff.

Then the theonomists came along and started calling atten-
tion to what these modern judicial Marcionites are really saying.
This has upset the Marcionites terribly.

‘Abortion Is Murder, Sort Of”

The theonomists keep upping the ante. They have even
recommended the passage of new laws requiring the execution
of abortionists and the former mothers who paid the physicians’
to do these specialized acts of murder on their behalf. But
squeamish Christians think that such a punishment is uncalled
for. These poor women need compassion, we are told. The
theonomist answers: But what about justice in the name of the
lifeless victims? What about compassion for the babies yet to be
conceived? Who will protect them? Irrelevant, we are told by
