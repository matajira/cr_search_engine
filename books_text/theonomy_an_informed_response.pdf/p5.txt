This book is dedicated to
MOISES SILVA.

the lone author in the
Westminster Seminary
symposium who was wise
enough not to write on the
assigned topic.
