106 THEONOMY: AN INFORMED RESPONSE

striving to persuade men and societics to submit to His rule
{and rules)? Scripture teaches us that our laboring is not in vain
and that tribulation is not incompatible with greater manifesta-
tion of Christ's saving dominion. Scripture teaches us that per-
secution and hardship are no obstacles to the Great Commis-
sion that we teach the nations to obey all that Christ has com-
manded. I do not see how any legitimate charge of “trium-
phalism” can be laid at our feet for believing these biblical
truths,

Actually, the threat of triumphalism in many forms seems to
be the unifying concern of the recent book by the Westminster
faculty. The editors offer this commentary: “Even to some
sympathetic observers of theonomy the most troubling aspect of
the movement, beside its application of the penal sanctions of
the Old Testament judicial law, is the triumphalist tone of
much of its rhetoric” (p. 193). The title of section 4 in the book
is “Fheonomy and Triumphalist Dangers,” but the whole book
is permeated with this theme. Dennis Johnson counsels the
acceptance of “political powerlessness” (p. 191). The spirit of it
all is captured by Bruce Waltke’s plea: “May the church boast
in its weakness, not in its might!” (p. 85). Of course, one must
be careful not to run to the opposite extreme from triump-
halism and seek a kind of spiritual and social masochism for the
church! Where personal “boasting” is the issue, let us stick to
our weaknesses, Where honoring the Lordship of our Savior is
at stake, let us pray for the exercise of His might.

We should not deny that triumphalist dangers are very real.
The church has sometimes experienced the threat of a trium-
phalist attitude in eschatology which leads some to ignore or
downplay the suffering which is part of the Christian’s experi-
ence and the church’s history on earth. There is the threat of a
triumphalist attitude in hermeneutics that leads one into think-
ing the interpretation and application of the Old Testament law
is a simple and obvious affair, or that presumes to have all the
