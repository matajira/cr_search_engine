292 TIEONOMY: AN INFORMED RESPONSE

The Lermeneutical Principles

Because this law is in Leviticus, the third book of the Penta-
teuch, I initially assumed that it is in some way related to the
third point of the biblical covenant model: ethics.** Leviticus
is the book of holiness, meaning legal or ritual “set-apartness.”
This led me to consider the question of boundaries, an aspect
of the third point. I call my commentary on Leviticus, Bound-
aries and Dominion.

The two boundary laws of the field were obvious starting
points for any discussion of Leviticus 19:19, They led me to
consider the location of these field boundaries: on one family’s
property. This answer raised the question of authority: Who
owned the field? Answer: the head of a household. This point-
ed me to the related issues of seed and inheritance. The princi-
ple of separation here was tribal, not covenantal,

The third law of Leviticus 19:19 was not a law governing the
field; it was a prohibition on certain kinds of clothing. I asked
myself: What boundary did this relate to? I had to determine
what the distinguishing features of wool and linen were in
Mosaic Israel,

Leviticus concentrates on priestly laws, so this line of investi-
gation became a possibility, once I was sure the third law had
little or nothing to do with seed and inheritance. I looked up
references to linen. Linen was the exclusive fabric of priestly
sacrifice after the exile. I was led to this conclusion: the prohibi-

5B. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 3.

86. To cross-check the references, I used NavPress’ Wordsearch computerized
Bible search program. To activate it, I hit two keys. (I use the Wordsearch Bridge
accessory program.) In five seconds I was inside Wordsearch. I then pulled up all
verses in which inen was mentioned. ‘This took about 30 seconds. Then I removed
from this list all verses that did not also mention wool. This took another 30 seconds.
Then I read the remaining verses. This took a few minutes. Anyone wha works with
biblical texts needs a Bible search program like this one. It enables me to do in
minutes what normally takes hours. That means, 1 am willing to do what I might
have neglected. The program also searches by Strong’s numbers.
