304 THEONOMY: AN INFORMED RESPONSE

am beginning to see why certain members of my congregation
thought I would stone them if I disagreed with them and why
others thought I was just a little bit “strange.” If Dr. Johnson
would read pages 78 and 79 of House Divided,’* he would see
that he has given a gross caricature and false presentation of
“reconstructionism.”

In addition, Dr. Johnson implies in an unwarranted fashion
that according to Theonomic principles, “The state should
execute church members who apostatize but not Hindus who
persist in their ancestor’s idolatry.”"” Show us where any the-
onomist teaches this or how it logically follows. No wonder
there are those who thought I believe in salvation by law.

Finally, Richard Gaffin claims that postmillenialism obscures
the dimension of suffering for the Gospel and as such distorts
“the church’s understanding of its mission in the world.”¥
However, Gary DeMar directly addresses this in his The Debate
Over Christian Reconstruction, seeing no contradiction between
the theme of victory and suffering.’® Statements such as
Gaffin’s only make it more difficult to traverse the theological
minefield before us,

Pastoral Reflections and Concerns

In observing the controversy that has for the last: fifteen
years whirled around the label theonomy and has to this point
culminated with the writing of T:ARC, there are several con-
cerns that have arisen that have a direct bearing on the church.

tions,” ARG, p. 171.

16. Bahnsen and Gentry, House Divided: “Theonomy most assuredly does not
endorse capital punishment for rebellious ‘adolescents’.”

17, Johnson, “The Epistle to the Hebrews and the Mosaic Penal Sanctions,”
TARC, p. 189.

18. Richard B. Gaffin, Jr., “Theonomy and Eschatology: Reflections on Postmil-
lennialism,” Z:ARG, pp. 217f.

19, Demar, The Debate Over Christian Reconstruction, pp. 24-26, 153-160.
