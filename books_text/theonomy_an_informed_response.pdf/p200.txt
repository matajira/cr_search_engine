180 ‘THEONOMY: AN INFORMED RESPONSE

which the Israelitish people have been subjected. . . . Jesus
Christ’s being a Priest, is a clear proof that the Mosaic law
about the priesthood is abrogated.”

Sanctions and Israel

Johnson agrees that the “change in law” has its focus in the
Levitical priesthood. But he attempts to extend the context of
the “change in law” to include “the relation of the Mosaic penal
sanations to the priesthood and sanctuary, the heart of the cove-
nant” (pp. 185-86). This argument, in his view, ultimately dis-
mantles the theonomic position, for “the author of Hebrews
presents the penalties of the Mosaic Law as covenant sanctions,
visited justly on those who violate covenant with the Lord” (p.
190). Elsewhere he writes: “. . . the justice of the Mosaic sanc-
tions [not just apostasy-type sanctions -KLG] presupposed the
offender’s privileged status and prior commitment as a member
of the Lord’s covenant” (p. 189). “The specific statements of
Hebrews 2:2 and 10:28 sum up the impression gleaned from
other, less explicit New Testament passages: the Mosaic penal
sanctions belonged in the context of the discipline and purity of
the covenant community” (p. 191).

Besides committing such informal logical fallacies as fallacy of
accident (Johnson confuses expanded application with original
jurisdiction) and hasty generalization (he judges all sanctions on
the basis of apostasy sanctions), there are some additional,
imposing problems confronting his presentation.

42. Ibid., p. 339.

48. This seems to involve him in argumentative self-contradiction. In one place
he argues “as part of this system, certain laws can be grouped together into categories,
since together they reinforce particular aspects of God's lordship over Israel” and
“certain penal sanctions belong to categories of laws that set Israel apart from all the
noncovenantal nations as a holy people” (p. 176, emphasis mine). But his fundamen-
tal argument regarding the removal of the Mosaic penal sanctions is based on the
alleged repeal of the apostasy law (Heb. 10:28), which seems to overlook its potentially
distinct categorization.
