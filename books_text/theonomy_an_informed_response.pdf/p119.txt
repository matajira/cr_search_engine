Westminster Seminary on Pluralism 99

No Enforcement of the First Table of the Law?

The general point of Barker’s argument is that the civil
magistrate should be prejudiced toward Christian values only
with respect to matters pertaining to the second table of the law,
all the while protecting the religious liberty of non-Christians;
the state should approach these matters only through natural
revelation. Barker says that according to the will of the Lord
Jesus Christ, the civil magistrate today is not expected nor
permitted to enforce the first great commandment (viz., loving
God), that is the first table of the law (viz., our duty toward
God). Barker thinks that Jesus taught this view in His answer
regarding the coin and taxation (Matt. 2:15-22).

As interesting as the discussion of Christ’s answer to His crit-
ics is, Barker’s line of reasoning really does not demonstrate
what he set out to show. In this passage Jesus taught that it is
indeed lawful for political subjects to give tribute-money to
Caesar (cf. v. 17). To infer from that premise that it is, then,
unlawful for Caesar himself to give tribute to God (enforcing
the civil aspects of the first table of God’s law) is an enormous
non-sequitur.

Barker attempts to squeeze that conclusion out of Jesus’
answer by pointing to the distinction which Jesus draws be-
tween the things belonging to Caesar and the things belonging
to God. However that distinction in itself was nothing new —
certainly not a new divine revelation of a truth which was un-
known or inoperative in the Old Testament (e.g., Jehoshaphat’s
distinction between “Jehovah’s matters” and “the king’s mat-
ters,” 2 Chron. 19:11) - and everyone is aware that in the Old
Testament, where that distinction was taken into account, the
hing was indeed obligated to show tribute to God by enforcing the
civil provisions of the first table of God’s law, Consequently,
Christ’s reminder of that distinction cannot én iiself have the
logical force of revoking such an obligation. Barker’s reasoning
does not deduce anything from the text, but rather reads it into
the text from outside.
