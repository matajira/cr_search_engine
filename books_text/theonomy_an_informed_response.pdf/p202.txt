182 THEONOMY: AN INFORMED RESPONSE

The Law was not specifically revealed to the Gentiles as it was to
the Jews at Sinai. Nevertheless, according to Paul, the Gentiles
do by natural instinct those things which are prescribed by the
Law . .. and this fact shows that the work of the Law is written
on their own hearts. In transgressing those things prescribed in
the Law, however, it may be said that the Gentiles were actually
transgressing the Law itself. Here, the plural is used to show that
the Gentiles had transgressed divine commands and ordinances,
and also that their sins were many and varied. We may say that
the Gentiles transgressed specific items of the Law, a thought
which the plural form of the noun would also support.**

This portion of Johnson’s argument based on the theme of
Hebrews is woefully lacking. The theonomic ethic is not under-
mined by the evidence he presents,

The A Fortiori Argument in Hebrews

In Johnson’s view, the sustained a fortiori argument in Heb-
rews is telling against the theonomic ethic. “Throughout the
epistle the superiority of the new order to the old is reinforced
by the repeated use of the word ‘better’ (thirteen times in Heb-
rews) and by a series of @ fortiori (how much more’) arguments
that reason from the value of the Mosaic order to the greater
value of the order established by Jesus (2:2-3; 9:13-14; 10:28-29;
12:25)” (p. 183).

As we have already seen, though, this docs not have a bear-
ing on the question of moral and civil issues. The specific con-
cern in Hebrews is with the “so great salvation” that these Heb-
rew Christians are in danger of apostatizing from. The Levitical
system in particular is given the most consideration in his argu-
ment (Heb. 5-10) because Christ is the telos of the sacrificial
system. The mediation by a human priesthood and through
ceremonial actions has passed away, being fulfilled in Christ.

46. E. J. Young, The Book of Isaiah (Grand Rapids: Eerdmans, 1965), 2:156-57.
