Hermeneutics and Leviticus 19:19 275

the New Testament is by public profession of faith, public bap-
tism, and public obedience; it is not by genetics. Inheritance is
by adoption, not by biological reproduction. This is a testimony
to the fact that covenantal faithfulness is more fundamental in history
than biology. It always has been, as God’s adoption of Israel as a
nation testified (Ezek. 16). But because of the historic impor-
tance of the prophesied seed of Israel, the seed laws predomi-
nated over the adoption laws in the Mosaic economy.

The advent of Jesus Christ restored adoption to visible pri-
macy. “But as many as received him, to them gave-he power to
become sons of God, even to them that believe on his name”
(John 1:12). With the death of Jesus Christ and the annulment of the
Old Covenant, the seed laws ceased. They were not resurrected
with Christ, There was no further need to separate seeds within
Israel; the prophecy of the seed of blessing had been covenan-
tally and historically fulfilled. So had the Levitical land laws
(Lev, 25). The Mosaic law’s mandatory link between physical seed and
land ceased for all time. The family and tribal boundaries within
the land, like the boundaries establishing the judicial holiness
(separateness) of national Israe] from the world, were covenan-
tally annulled by the New Covenant. The new wine of the
gospel broke the old wineskins of Israel’s seed laws.

Nowhere is this clearer than in the Iettcr to the Hebrews. It
begins with an affirmation of Christ’s inheritance: God the
Father “Hath in these last days spoken unto us by his Son,
whom he hath appointed heir of all things, by whom also he
made the worlds” (Heb. 1:2). His inheritance is expressly tied
to His name: “Being made so much better than the angels, as
he hath by inheritance obtained a more excellent name than
they” (Heb. 1:4). Jesus is the high priest of an unchangeable
priesthood (Heb. 7:24). His priesthood, because it is after the
order of Melchizedek, is superior to the Levitical priesthood
(Heb, 7:9-11). This has changed the Levitical laws: “For the
priesthood being changed, there is made of necessity a change
also of the law” (Heb. 7:12). This inchides the laws of tribal
