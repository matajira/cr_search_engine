60 THEONOMY: AN INFORMED RESPONSE

whole law to contemporary society, including, but not limited
to, the civil magistrate.’ The appeal to Rutherford came early
in Schaeffer's writing.

Schaeffer rightly decried a de facto sociological law — “law
based only on what the majority of society thinks is in its best
interests at a given moment” — but offered no worked-out
worldview to counter and replace it. He wrote about a “Chris-
tian consensus” and how that consensus is found in the Bible,
but he did not inform us of its biblical content as it relates to a
comprehensive biblical worldview in the particulars.

There are times, however, when Schaeffer closely resembled
a Reconstructionist. ‘This is best demonstrated in his repcated
references to Paul Robert's painting Justice Instructing the Judges.

Down in the foreground of the large mural the artist depicts
many sorts of litigation — the wife against the husband, the archi-
tect against the builder, and so on. How are the judges going to
judge between them? This is the way we judge in a Reformation
country, says Paul Robert. He has portrayed Justice pointing
with her sword to a book upon which are the words, “The Law
of God.” For Reformation man there was a basis for law. Modern
man has not only thrown away Christian theology; he has

7. Richard Flinn, “Samuel Rutherford and Puritan Political Theory,” journal of
Christian Reconstruction, Symposium on Puritanism and Law, ed. Gary North (Winter
1978-79), pp. 49-74.

8, Schaeffer was more comfortable with historical and logical argumentation than
with biblical exposition. Gonsider how he argues against abortion:

Schaeffer claims to base his arguments against abortion on both logical
and moral grounds, but it is interesting that he accentuates the logical side.
In fact, he never appeals specifically to Scripture to buttress his position. The major
logical argument employed involves the impossibility of saying when a devel-
oping fetus becomes viable (able to live outside the womb), for smaller and
smaller premature infanis are being saved. Since the eventual possibilities for
viability are staggering, “The logical approach is to go back to the sperm and
the egg.” Dennis B Hollinger, “Schaeffer on Ethics,” Reflections on Francis
Schaeffer, edited by Ronald W. Ruegsegger (Grand Rapids: Zondervan/Acad-
emie, 1986), p. 250. Emphasis added.

  
