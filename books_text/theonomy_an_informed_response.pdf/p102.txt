82 THEONOMY: AN INFORMED RESPONSE

Clowney in the Pluralism of Westminster. On the key question of
the keys and the sword — ecclesiastical sanctions and civil sanc-
tions — which is the central issue dividing theonomy’s confession
from Westminster’s confession, Clowney’s assertion underlies
most of the faculty’s contributions. His authority on the ques-
tion of authority is authoritative for Westminster.”

The Coherence of Covenant Theology

There are five points in the biblical covenant model: the
absolute sovereignty of God (transcendence), the representative
nature of God’s authority (hierarchy), the revealed law of God
(ethics), the sanctions of God {oath), and the progress of God’s
kingdom in history (succession).* Westminster Seminary’s facul-
ty defends the sovereignty of God; Westminster is a Calvinist
institution. The theonomists have no debate with them on this
point. (To the degree that the doctrine of the sovereignty of
God is revealed in God’s Genesis 1 account of a literal six-day
creation, the theonomists do have a debate with the faculty of
Westminster, for the theonomists are six-day creationists. West-
minster has never taken an official position on six-day creation-
ism, but Kline’s “framework hypothesis” is opposed to it, and
Muether’s essay openly ridicules it. The editors let this pass.)*

In the four other areas of covenant theology, we oppose the
world-and-life view of those members of the faculty who deny:
(1) the ministerial authority of the civil magistrate (Rom. 13:1-
7); (2) the authority of biblical law over any concept of natural
law; (3) the legitimacy of biblically specified civil sanctions in
defense of God’s law; and (4) postmillennialism. This means a
majority of the contributors to Theonomy: A Reformed Critique.

2. Gary North, Westminster's Confession: The Abandonment of Van Til’s Legacy (Tyler,
‘Texas: Institute for Christian Economics, 1991), ch. 1; pp. 138-46.

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987),

4, John R. Muether, “The Theonomic Attraction,” Theonomy: A Reformed Critique,
p. 284.
