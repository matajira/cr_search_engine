Westminster Seminary on Pluralism 107

answers and a completely unproblematic scheme of thought.”
There is the threat of a triumphalist attitude in personal rela-
tions which makes one unteachable and harsh and dogmatic.
There is the threat of a triumphalist attitude in ethics which
elevates concerns about social justice and politics to a higher
place in our priorities than it properly has. There is the threat
of a triumphalist attitude which sceks power and dominion over
our fellow men, whether as teachers of Scripture or rulers of
the state. Triumphalism in all these manifestations is a misera-
ble failure in the life of any believer, whether a thconomist or
a theonomic critic. It is failure to recognize the sovereignty of
God, the mystery and grace of His kingdom, and the biblical
exhortations to humility in following the Messiah. For that
reason, the various concerns of the Westminster faculty over
triumphalist dangers are well-taken.

Nevertheless, there is an anomaly about these cautions issued
by the Westminster faculty. Ever since its inception during the
modernist controversy, Westminster Seminary has communicat-
ed a sense of theological confidence in the teaching of God’s
inspired Word. There was no hesitation about triumphalism or
being dogmatic when the faculty wrote in defense of Scriptural
credentials against liberal detractors (God’s Infallible Word, 1946),
There was no hesitation about triumphalism or being dogmatic
when the faculty wrote in defense of theological integrity
against neo-orthodox detractors from the confession of faith
(Scripture and Confession, 1973). When I was a student at the
seminary we were taught caution and humility before the Word
of God, but also to be bold in believing in the accessibility of its
message, the necessity of applying it to all of life, and confi-
dence in its declarations. I had professors who did not hesitate
to teach and defend positions on the most mysterious matters for

26. The essays by John Frame (“The One, The Many, and Theonomy") and
Vern Poythress (“Effects of Interpretive Frameworks on the Application of Old
‘Testament Law”) particularly and wisely warn against this.
