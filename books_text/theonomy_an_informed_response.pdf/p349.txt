Editor’s Conclusion

Dick Wilson, ©. T. Allis,!° Ned B. Stonehouse, and Edward J.
Young were textual defenders of high repute internationally.
No one has replaced them in this generation. Today, traditional
Calvinism’s stream of technically precise biblical scholarship has
run, if not dry, then at least dangerously shallow.

Post-1788 Calvinists have not been major academic figures in
any field outside of theology, except possibly when defending
an existing humanist academic paradigm by means of accepted
humanist academic techniques (e.g., George Marsden’s techni-
cally excellent but stylistically “neutral” history, Fundamentalism
and American Culture). “Look, look: we can do it, too!” is not
a battle cry of cultural conquest.

Consider the major intellectual force in modern life, Darwin-
rwin’s Origin of Species appeared in 1859. It took until
1 Charles Hodge to respond with What Is Darwinism?, a
ative analysis that concluded that Darwinism is atheism. But
Hodge was virtually alone among conservative seminary profes-
sors in his rejection of evolution’s timetable. The defenders of
long eons of time and a doctrine of progressive creation were
A. A. Hodge, Charles Hodge’s son; Francis Patton, who became
the president of Westminster Seminary in 1902; B. B. Warfield
of Princeton Seminary; W. G. T. Shedd, a conservative teaching
at Union Theological Seminary in New York; and Baptist A. H.
Strong of Colgatc-Rochester Seminary.'* The men who sup-
ported Charles Hodge’s forthright rejection of Darwinism and
its timetable were not seminary professors."® This situation has
not changed in more recent times.

 
  
 

16. All of Allis’ major books were written after he had retired from Westminster
Seminary. His extrordinary output of books began in 1945, when he was in his
sixties. He died in 1972, writing almost until the day he died.

17. George M. Marsden, Findamentalism and American Culture: The Shaping of
Twentieth-Century Evangelicalism, 1870-1925 (New York: Oxford University Press,
1980).

18. Smith, Seeds of Secularism, p. 98.

19. Idem,
