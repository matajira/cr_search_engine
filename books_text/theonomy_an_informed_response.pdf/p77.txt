3

FEAR OF FLYING:
CLIPPING THEONOMY’S WINGS

Gary DeMar

There is no doubt that Francis A. Schaeffer broadened the
appeal of the reformed faith with his popular writing style and
activist worldview. Schaeffer’s popularity was extensive enough
that he was recognized by the secular media as the “Guru of
Fundamentalism.”! Schaeffer filled the intellectual gap that
resided in much of fundamentalism. In a sense, he carried on
the tradition of his early mentor, J. Gresham Machen.

Prior to 1968, little was known of Francis Schaeffer. He had
isolated himself from American evangelicalism by ministering to
the roaming discards of society who were trekking through
Europe hoping to find answers to life’s most perplexing prob-
lems. The publication of The God Who Is There and Escape from
Reason introduced him to an American evangelicalism in crisis.
Schaeffer had an impact where many Christian scholars had
made only a few inroads to the hearts and minds of a disen-
chanted and impotent Christendom. What did Schaeffer do
that was different? Certainly Carl F H. Henry’s The Uneasy

1. Kenneth L. Woodward, “The Guru of Fundamentalism,” Newsweek (Nov. 1,
1982), p. 88.
