Whose Victory in History? 217

foundations of either postmillennialism or amillennialism! His
objections are primarily theological, as opposed to exegctical.”

Gaffin argues that the New Testament’s eschatological lan-
guage and events should be understood primarily (approaching
exclusively) as referring to discontinuous completions of the Old
Covenant’s legal order, rather than as future applications of the
New Covenant’s legal order. He understands that postmillen-
nialism emphasizes the results of the New Covenant in history.
As an amillennialist, he prefers to emphasize the discontinuous
jedicial break from Old Covenant history rather than the contin-
uous cultural effects in church history of the comprehensive
New Covenant judicial order, an order which is today uniquely
empowered by the Holy Spirit in a way that the Old Covenant
order was not. He writes that

the fall of Jerusalem is decisively misunderstood unless we recog-
nize that — even for the apostolic church, when it was still future
— its primary affinities are not toward the future but the past,
toward the first coming, as it marks the end of the brief transi-
tional period from the old to the new covenant. It is a funda-
mental misreading to see the eschatological discourses of Jesus
(Mt 24, Mk 13, Lk 21) and the. Book of Revelation as fulfilled
almost exclusively or even largely in the events of A.D. 70, as if
those cvents were of major eschatological importance.'* The

17. Notice Gary North’s frequent complaint about just such a lack by amillennial-
ists. North, Millennialism, pp. 114, 171, 173, 178, 183, 214. The silence in Gaffin’s
chapter in this area is deafening.

18. Is it really a “fundamental misreading” of the Olivet Discourse to interpret
Jesus’ material in the light of His own statement that “This generation shall not pass,
till all these things be fulfilled” (Matt. 24:34)? 1s it really a “fundamental misreading”
of Revelation to interpret it in the light of the following statements? “Ihe Revelation
of Jesus Christ, which God gave unto him, to show unto his servants things which
must shortly come to pass” (Rev. 1:1), “Blessed is he that readeth, and they that hear
the words of this prophecy, and keep those things which are written therein: for the
time is at hand” (Rev. 1:3). “And he said unto me, These sayings are faithful and true:
and the Lord God of the holy prophets sent his angel to show unto his servants the
things which must shortly be done” (Rev. 22:6). “And he saith unto me, Seal not the
sayings of the prophecy of this book: for the time is at hand” (Rev, 22:10). Somehow
