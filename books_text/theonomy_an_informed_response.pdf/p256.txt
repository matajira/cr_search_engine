236 THEONOMY: AN INFORMED RESPONSE

‘We must offer the poor both the bread of life and the bread of
grain. We must offer them shelter from heil and shelter from the
weather.’

It is truly amazing what context does! Does the man who
said, “We prove to the world that we don’t intend to let every-
one starve,” sound like the same person Keller quoted? Hardly,
but I think that this is the straw man that Keller prefers.

Moreover, we need to ask the obvious question: “Why was
North going to so much trouble to write the introduction if he
was in so much disagreement with Grant?” Why doesn’t Keller
point this out? This is sheer deception, bringing us to perhaps
the most misrepresentalive aspect of Keller’s article.

George Grant .

Dr. Keller denies George Grant’s affiliation with the Recon-
structionist movement in his “Addendum” at the end of his
essay, I think it is obvious why Keller does this. He wants to be
able to accuse Christian Reconstructionism of something that is
simply not true. ‘To accomplish this, he has to take George
Grant away from Christian Reconstructionism.

Why is Keller wrong when he says that Grant is not a Recon-
structionist in his writings? First, not only did Gary North write
introductions to George’s books, but he edited, published, and
financed much of George’s work in the 80’s. Why was he publishing
George Grant if he didn’t agree with Grant’s prescriptions for
transforming poverty? I can tell you from experience, North
never publishes somcone with whom he does not agrec, at least
on the specific topic of the book or article.

Second, what George Grant believes at present is not the
issue. Dr. Keller remarks that Grant is presently not a Recon-
structionist.

3. North, “Editor's Introduction,” to George Grant, In the Shadow of Plenty: The
Biblical Blueprint for Welfare (Ft. Worth, Texas: Dorninion Press, 1986), pp. xv-xvi.

 
