40 THEONOMY: AN INFORMED RESPONSE

here that we were told we would find a fully developed, com-
prehensive, biblical world-and-life view. Kuyper’s brand of Cal-
vinism has been described as the “only modern exception” to
the tendency of Christians cither to abandon social action in
favor of piety or to abandon piety in favor of social action.

The “Kuyperian” tradition “was at once pious and socially
influential.”? “As Abraham Kuyper said, there is not one inch
of creation of which Christ doesn’t say ‘Mine.’ ? In his Lectures
on Calvinism, Kuyper discussed politics, science, and art — a
rather odd mix, but it was more than the familiar five points of
Calvinism, (Economics and law were strangely absent.)

Reading Kuyper was like reading a repair manual that was
all diagnosis and little if any instruction on how to fix the prob-
lem. Here’s a sample:

That in spite of all worldly opposition, God’s holy ordinances
shall be established again in the home, in the school and in the
State for the good of the people; to carve as il were into the
conscience of the nation the ordinances of the Lord, to which the
Bible and Creation bear witness, until the nation pays homage
again to Godt

Everything that has been created was, in its creation, furn-
ished by God with an unchangeable law of its existence. And
because God has fully ordained such laws and ordinances for all
life, therefore the Calvinist demands that all life be consecrated
to His service in strict obedience. A religion confined to the
closet, the cell, or the church, therefore, Calvin abhors.*

1. Irving Hexham and Karla Poewe, Understanding Cults and New Religions (Grand
Rapids: Eerdmans, 1986), p. 126,

2, Ibid.

3. Douglas Groothnis, “Revolutionizing our Worldview,” Reformed Jowmal (Nov-
ember 1982), p. 23.
ham Kuyper, Lectures on Calvinism (Grand Rapids: Eerdmans, [1931]

 

4,
1970). p
5, Ibid., p. 58.
