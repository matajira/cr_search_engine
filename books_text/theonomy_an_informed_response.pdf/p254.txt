234 THEONOMY: AN INFORMED RESPONSE

Chilton and North

Consider how he caricatures Reconstructionism by summar-
izing on the basis of two writers quoted out of context.

Chilton and North see the roots of modern poverty as cultur-
al and personal disobedience to God’s law. They remind us so
often that wealth is God's benediction on faithfulness to his law
that they appear to be saying poverty is simply the result of sloth
and sin.'

He is careful to say, “appear” in his quotation, for which I am
grateful. He then quotes North and Chilton in some places
where they actually do speak of sinful causes of poverty. He
does not, however, refer to comments by these individuals to
indicate that they see other causes of poverty besides cultural
and personal poverty.? The effect he is trying to create, howev-
er, is that Chilton and North are theologically callous toward
the poor. This is completely false, as we shall see.

Another problem with Keller’s assessment is that he has not
selected Reconstructionist writings that are truly representative
of Christian Reconstructionism’s basic paradigm on poverty.
Chilton’s and North’s comments are not designed to present
the system as such. They speak mostly in the context of crit-
iquing liberals who fail to address personal responsibility at all.
For example, David Chilton wrote his famous Productive Chris-
tians in an Age of Guilt-Manipulators (1981) in response to Ronald
Sider’s Rich Christians in an Age of Hunger (1977). The nature of
Chilton’s effort was polemical, designed for a specific purpose.

1. “Theonomy and the Poor: Some Reflections,” Timothy J. Keller, Theonomy: A
Reformed Critique, edited by Robert W. Godfrey and William S. Barker (Grand Rapids:
Zondervan Academie Books, 1990), p. 263.

2. David Chikon, Productive Christians in an Age of Guilt Manipulators (2nd edi;
‘Tyler, Texas: Institute for Christian Economics, 1981), pp. 65-66. In this section,
Chilton advocates the Old Testament laws of gleaning. He is in no sense failing to
recognize legitimate cases of poverty that are due to the inability to work.
