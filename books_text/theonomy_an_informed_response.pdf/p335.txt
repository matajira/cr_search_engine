EDITOR’S CONCLUSION

Several years ago, my wife and I were having dinner with a
group of financial advisors. One of them was (and is) an atheist,
an anarchist, and a man who lives openly with a woman who is
not his wife. (So far, you cannot identify him; there are a lot of
men in the business who fit his description.) When we meet, he
almost always gets into a discussion with me about God and
death. After a few minutes, my wife asked him this: “What are
you going to say to God on the day of judgment when He
condemns you to eternal torment?” His answer was immediate:
“T will tell Him, ‘You have no jurisdiction over me.’ ”

This is the implicit answer to God by every covenant-breaker
in history, from Satan to the present. The covenant-breaker
denies God’s jurisdiction, meaning God’s authority to speak the
law to him. It was to refute this deadly confession of faith that
the Second Person of the Trinity came into history as a man.

Let us get something straight from the start: Jesus Christ
brought the most ferocious message in history. He announced to His
enemies, meaning God’s enemies, that if they refuse to submit
to His jurisdiction in history, they will be judged by God and
sent to hell first, and after that comparatively brief and mild
experience, they will be dumped into the lake of fire (Rev,
20:14-15). At least in hell they will have the ability to communi-
cate, though not to their advantage (Luke 16:22-31), In prepar-
ation for the lake of fire, God will give them perfect bodies at
the resurrection, so that they will be better able to experience
