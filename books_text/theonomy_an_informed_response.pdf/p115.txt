Westminster Seminary on Pluralism 95

What do we find in this present-day America, in which the
achievements of centuries are so rapidly being lost and in which
that liberty which our fathers won at such cost is being thrown
away recklessly by one mad generation? I think the really signi-
ficant thing that we find is that America has turned away from
God. In the political and social discussions of the day, God’s law
hhas ceased to be regarded as a factor that deserves to be rec-
koned with at all... .

A nation that tramples thus upon the law of God, that tram-
ples upon the basic principles of integrity, is headed for desiruc-
tion unless it repents in time. ... The real reason why young
men fall into crime is chat the law of God is so generally dis-
obeyed. . .. The real evil is the ruthless disregard of the law of
God [by both individuals and states],'*

As we can see, Machen was not vague and tentative in his
political ethic based upon the word and law of God. He thun-
dered against “soul-killing collectivism,” against “the modern
paternalistic State,” against the ideal of being “under govern-
ment tutelage from the cradle to the grave,”"* against the state
disregarding enforcement of the law of God, and against the
“evil” of government schools, So confident was he that he took
his testimony right into the highest reaches of civil government,
speaking before House and Senate committees of the United
States Congress.

If we were to judge from the recently published book by the
faculty of Westminster Seminary, Theonomy: A Reformed Critique,
we would say that the spirit of Machen with respect to socio-
political ethics has largely departed. Confident application of
the law of God to the state is no longer enthusiastically en-
dorsed or pursued. No one speaks with the stature of Machen
(understandably enough), but no one seems to speak for the

13. Zbid., pp. 139, 140, 141.

14. Ibid., pp. 128, 133-85. Machen wrote: “From this dreary goose-step there will
be no escape.”

15. “Proposed Department of Education” (1926), ibid., pp. 99-123.

 
