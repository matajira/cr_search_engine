168 THEONOMY: AN INFORMED RESPONSE

should be: “The author of Hebrews presents the Mosaic penalty
for apostasy as a covenant sanction, visited justly on those who
violate covenant with the Lord.” But I will later show that He-
brews will not even allow that limited point.

The Stages of Hebrews

And what of the four-staged structure of the author of Heb-
rews? A careful analysis of the four stages cited by Johnson will
show that they are (1) contrasting mediatorial agency (involving
covenantal form), and (2) even then focus only on a contrast of
the. mediatorial agency in regard to one’s approach to or rela-
tionship with God (involving covenantal membership). This four-
phased argument in Hebrews in no way undermines the con-
tinuance of God’s moral law or His civil sanctions for criminal
deeds.

The theme of Hebrews is the superiority of Christ over all media-
torial agencies associated with the Old Covenant administration. The
author of Hebrews argues that to spurn Christ in deference to
angels, Moses, Aaron, and the Old Covenant sacrificial system
is to risk eternal damnation. To exalt any Old Covenant media-
torial authority over Christ is not only eternally dangerous but
theologically absurd, for only Christ has been “appointed heir
of all things.” Only through Christ has “God made the world.”
Only Christ is the “brightness of Ilis glory and the express
image of His person.” Only Christ “upholds all things by the
word of His power.” Only Christ “purged our sins.” Only
Christ “sat down at the right hand of the Majesty on high”
(Heb. 1:2b-3).3 Neither angels, nor Moses, nor Aaron did such.
The angels are but “ministering spirits” (Heb. 1:14). Moses is in
the house of God, Christ is over it (Heb. 3:2-6). Priests were
ministers in the “carthly sanctuary”; Christ is “in the true taber-
nacle, which the Lord pitched, not man” (Heb. 8:1-4; 9:1, 11).

3, The anarthrous appellation in verse 2 points out the charakter (Gk.) of Christ
as the Son, as in 3:6; 5:8; 7:8; 7:28.
