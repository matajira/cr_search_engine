Westminster Seminary on Pluralism 111

teach you; His Anointing teaches you concerning all things, and
is true. . .” (1 John 2:27).

‘We do not want to portray the error of a dogmatic spirit in
such an extreme light that people jump back into the arms of
agnosticism, do we? We do not want to suggest that the task of
teaching the nations everything that Christ has commanded
needs to be placed on long-time hold until sufficient scholar-
ship has been applied extensively and persuasively. We do not
wish to suggest that the great commission is really too great to
carry out. We do not want to intimidate God’s people in their
willingness to go out and apply those commandments (responsi-
bly, of course) to their personal lives and cultures. So let’s not
overstate our case about its difficulty.

Finally, let me observe that it can hardly be a well-reasoned
criticism of theonomic ethics that some “potentially dangerous
ideas” could arise from encouraging the state to follow the holy
laws of God in Scripture. We live in a fallen world where ad-
herents of any and every political philosophy (including attempt-
ed biblical ones) will err in carrying out their ideals. That being
the case, it only makes sense to err on the side of the angels,
starting with the best (indeed, infallible) ideals available to men
— the revealed laws of God! Just imagine what “potentially [nay,
actually] dangerous ideas” have stemmed from not following
God’s law, but rather the human speculations found in Rous-
seau, Marx, Mill, Buckley, Galbraith, and many others! The
world is a dangerous place — too dangerous for human authori-
ties (or their theoreticians) not to be restrained and regulated by
the justice of God’s laws.

I believe that Dr. Machen would have concurred.
