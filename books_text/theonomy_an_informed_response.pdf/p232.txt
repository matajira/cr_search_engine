212 THEONOMY: AN INFORMED RESPONSE

tive, v. 24) this has been accomplished. In other words, Paul
interprets the “until” from the Hebrew of Psalm 110:1 by an
effective aorist,’° allowing its progressive import up to its suc-
cessful completion in verses 24 and 25,

This fact is confirmed for us in 1 Corinthians 15:28, where
we read of the still future expectation of the total subjugation of
all His enemies: “And after [hotan with the aorist passive sub-
junctive means “after”"'] all things shall be subdued unto him,
then [fete] shall the Son also himself be subject [future indica-
tive] unto him that put all things under him, that God may be
all in all.” Of the aorist verb used here, A. T. Robertson notes:
“It is prophecy, of course.””? That is, the subduing that Paul
had in mind was still in the future when he wrote. Gaffin’s
theological objection to postmillennialism, then, would appear
equally applicable to Paul! Thus, on Gaffin’s analysis, Paul would
be “denying” the reality of Christ’s present rule by expecting
some future unfolding of victory in addition to what He had in
the first century!

Furthermore, we should note that even death itself has already
been conquered definitively and in principle by Christ in the first
century (see 2 Tim. 1:10, cp. Acts 2:24; Rom. 6:9, 12-13; Heb.
2:14; 1 John 3:14). Gaffin recognizes this, for he writes: “believ-
ers are ‘alive from the dead,’ already resurrected” (p. 211). This
is despite the fact that, according to Paul in 1 Corinthians
15:26, the conquering of death awaits the historical end of
history for its conclusive manifestation to the world. This un-
derstanding of Christ’s victory over death - definitive in the

10. A. TL Robertson, Word Pictures in the New Testament (Nashville: Broadman,
1960), 4:191. “he idea is that emphasis is laid on the end of the action as opposed
to the beginning (ingressive).” A. ‘I: Robertson, A Grammar of the Greek New Testament
in the Light of Historical Research (4th ed.; Nashville: Broadman, 1934), p. 835.

11. The subjugation in view’ is not yet as Paul writes. Hence, it is not the ascen-
sion-session’s definitive subjugation in principle. Rather, it looks to the progressive
subjugation in history.

19. Robertson, Word Pictures, 4:192.

 
