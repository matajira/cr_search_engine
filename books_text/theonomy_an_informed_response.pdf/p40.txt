20 THEONOMY: AN INFORMED RESPONSE

that triggered this shift. A year later, I read Meredith G. Kline’s
essays in the Westminster Theological Journal on covenant oath
signs, and they made me a paedobaptist.

In the second semester, I also audited John Murray's weekly
one-hour course in sanctification. These lectures transformed
my thinking about law and ethics, His discussion of the three-
fold aspect of sanctification — definitive, progressive, and final -
revolutionized my thinking as much as Van Til had. This out-
line is the biblical basis of the idea of progress in history,
though Murray never made this application, and I did not yet
recognize it. Without this doctrine of imputed sanctification, all
applied economics is irrational, as I argue in my discussion of
imputation and value theory in my economic commentary on
Genesis, The Dominion Covenant: Genesis (1982).

There is no doubt in my mind how the idea of theonomy
came to Rushdoony: his study of the book of Job and Van Til's
writings. That is what he says did it, and I believe him. Theon-
omy as a social theory came to me as a result of my Arminian
understanding in 1960 that economics needs to be restructured
by the Bible. I believe today that I would have been unable to
begin this specific work of reconstruction had I not been intro-
duced to Van Til’s apologetics and John Murray's concept of
imputed sanctification. They were both Calvinists.

Judicial Theology

Why is Calvinism so significant? Because it is so intensely
judicial. Calvinism brings sinners to the foot of the cross by its
defense of the doctrine of the holiness of God, meaning the God
of permanent law and permanent sanctions. 1 will say it here, and I
will keep on saying it: strip Calvinism of its judicial character and
it ceases to be Calvinism. Yhis is why theonomy came out of Cal-
vinism by way of Van Til’s reconstruction of apologetics.

Van Til was a revolutionary. He abandoned every trace of
Greek philosophy in the intelicctual heritage of Christianity.
But in doing this, he also stripped away the fig leaf of Roman
