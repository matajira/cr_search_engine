78 THEONOMY: AN INFORMED RESPONSE

omy’s been answered.” A similar scenario is operating with the
dispensational critics of Christian Reconstruction: Dominion
Theology: Blessing or Curse?* This tome has become the deus ex
machina for dispensational non-readers.

Ifyou think I am exaggerating, then consider this. An article
appeared in a well-known dispensational magazine purporting
to be the first in a series of articles that would evaluate Chris-
tian Reconstruction. The article was heavily footnoted, but you
had to write to the publisher if you wanted a copy of the notes.
Always the inquisitive one, I of course dutifully requested a
copy. All the footnotes were from Deminion Theology. The entire
article was based on the research of one book. No original
research had been done. Then I learned, in correspondence
with the author, that he had been assigned the task of writing
on Christian Reconstruction with reference only to Dominion
Theology.

Until a person works through the published works of the
major Reconstructionist authors, he should not speak out on
the subject. I fully expect that all of our critics will do this in
the future. They will back up their criticisms with citations from
the primary sources of Christian Reconstruction. Furthermore,
they will not exaggerate their claims. They will address their
criticisms (o what Reconstructionists have said or written. I am
quite confident about this development.

You understand, of course, that I am a postmillennialist.

45, H, Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse?
(Portland, Oregon: Multnomah Press, 1988).
