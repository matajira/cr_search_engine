224 THEONOMY: AN INFORMED RESPONSE

with Christ’; nothing, the New Testament teaches, is more basic to
its identity than that” (p. 211, emphases mine). Is suffering
{persecution?)* throughout the “entirety” of the interadvental
period a “fundamental” aspect of the church’s existence? Is
there absolutely “nothing . . . more basic” in the New Testa-
ment? If we are not suffering (persecution?) are we a true
church? Is Gaffin suffering? Gaffin’s statements are inordinately
applied in an attempt to win points for his pessimistic eschatolo-
gical view. Surely they are overstatements.

Third, what of the specifically positive prophetic statements of
the New Testament, which actually set before us our divinely
ordained victory-oriented expectation for the future, rather
than describing our hope amidst present trial when it arises (as
it did so universally among our first century forefathers)? Does
not 1 Corinthians 15:20-28 hold before us the prospect of the
universal triumph of the gospel of Jesus Christ as He sovereign-
ly reigns from the right hand of God?*! Do not the statements
of cosmic redemption set forth the confident expectation of a
redeemed world (John 1:29; 3:17; 12:30-31; 47; 1 John 2:2; 2
Cor. 5:19)?*? Do we not have the right to hope that the king-
dom of God will dominate and permeate the entirety of human
life and culture (Matt. 13:31-33)?*? Are we not commanded to
“make disciples of all the nations” under the absolute authority

40. If persecutional suffering is not in Gaffin’s mind here, then all other forms
of suffering are irrelevant to the argument contra postmillennialism, as I will demon-
strate below.

31. Sce: Bahnscn and Gentry, House Divided, pp. 214-17. Charles Hodge, Com-
mentary on the First Epistle to the Corinthians (Grand Rapi ’erdmans, [1864] 1969),
pp. 326-36.

32. Babnsen and Gentry, House Divided, pp. 203-10. B. B. Warfield, “Christ the
Propitiation for the Sins of the World,” in John E. Meeter, ed., Selected Shorter
Writings, 2 vols, (Nutley, New Jersey: Presbyterian and Reformed, [1915} 1970), 1:23.

33. Richard C. Trench, Notes on the Miracles and the Parables of Our Lord (Old
‘Tappan, New Jersey: Revell, rep. 1943), 2:109-123). Alfred Edersheim, The Life and
Times of Jesus the Messiah (Grand Rapids: Eerdmans, [1883] 1971), 1:598 (in Bk. 3; ch.
28), J. A. Alexander, The Gospel According to Matthew (Grand Rapids: Baker, [1860]
1980), pp. 367-70.

 
