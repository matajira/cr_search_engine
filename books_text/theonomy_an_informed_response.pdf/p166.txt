146 THEONOMY: AN INFORMED RESPONSE

Introduction te the New Testament Evidence

Let us now turn to consider Johnson's “brief survey of the
other New Testament references to the penalties of the Law of
Moses” (p. 178). Despite the initial appeal of Johnson’s argu-
ment, it is flawed at every stage of its development.

1. The Sermon on the Mount

Johnson’s treatment of Matthew 5:21ff, while recognizing its
reference to capital sanctions (contrary to McCartney's over-
sight: p. 145), is seriously defective:

In the Sermon on the Mount Jesus cites the law’s requirement that
murderers be brought to judgment; then he asserts that not only
murder but also unjustified anger brings liability to judgment, and
anger expressed verbally brings eternal condemnation (Mt 5:21; Lev
24:17). Jesus cites the law’s ‘eye for eye, tooth for tooth’ principle,
only to tighten further its restriction on vindictive retaliation (Mt
5:38; cf. Ex 21:24; Lev 24:20; Dt 19:21). Thus, on the one hand,
Jesus announces a more severe penalty than the Mosaic law: under
Moses, murder was grounds for the judgment of execution; but for
Jesus’ disciples a cross word or taunt becomes grounds for eéernal
destruction. Yet, on the other hand, Jesus restricts his disciples’ right
to inflict on others the penalties provided in the Torah. Are Jesus’
teachings in the Sermon on the Mount intended to function as a guide
to new covenant jurisprudence in the political sphere? Learning
from Jesus that the insult is a far more serious offense than any of
us would have imagined, should Christians urge their government
to make it a capital crime? If so, are we then forbidden by Jesus’
word in Matthew 5:38 [sic] from pressing charges against the perpe-
trators of this and other misdeeds? The implied argument from
lesser sin/punishment to greater confirms that the law reveals God’s
justice; but Jesus’ treatment of the penal sanctions poses difficulties
for us if we assume that biblical statements about God's justice in
interpersonal relations are always intended to define the role of civil
government as ‘God’s agent of wrath’ (Ro 13:4) (p. 178).
