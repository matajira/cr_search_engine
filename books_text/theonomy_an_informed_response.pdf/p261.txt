 

Whose Conditions for Charity? 241

thing which Keller and most of the critics failed to do in prepa-
ration for publishing Theonomy: A Reformed Critique.

Critics of Christian Reconstructionism often resist the proof
that many theonomists have been willing to refine and develop
their position, as evidenced by the fact that the very critics
refuse to read the flow of the corpus of material over a ten to
twenty year period. If Dr. Keller and others at Westminster are
going to lump us all together, then they are going to have to
read all the matcrial. If not, then they should only deal with
the people whose writings they have read in total. Unless they
do this, they will continue to caricature and misrepresent, and
then be shown to have produccd sloppy and even bigoted
scholarship.

Thus, simply put, Keller makes his article read as though
Reconstructionists don’t care about the poor! Nothing could be
further from the truth. All of us have been involved in practical
kinds of poverty relief in a variety of ways in our own churches:
everything ranging from homes for unwed mothers to strect
feedings. Dr. Keller presents the complete opposite by failing to
grasp the context of Chilton’s and North’s writings, and by
removing Grant’s work from the Reconstructionist movement.
If I were to take your money and then accuse you of not giving
to the poor, who would really be uncharitable? Mr. Keller does
virtually the same, only with theological concepts and commit-
ments. He takes away, and then accuses them of lacking the
very thing he has taken. This is most uncharitable.

Sutton

When it comes to my own writings, Dr. Keller does the
same. He neglects quoting from the full context of my paper on
The Theology of Poverty, a paper published in 1985. He criticizes
me for being overly reductionistic, charging that I fail to con-
sider ail of the causes of poverty. (What are ail of the causes of
poverty, Dr. Keller?) I know that Dr. Keller read this paper
