6 THEONOMY: AN INFORMED RESPONSE

presupposition: sanctions in any institution should be consistent with
the law; therefore, “the biblically specified punishment must fit the
biblically specified crime.” It is not simply that the Westminster
faculty has rejected the idea that God’s revealed law identifies
the crime. Rather, it is that they — or at least most of them
(especially the Gordon-Conwell faction) — self-consciously reject
the notion that the Bible also identifies the appropriate civil sanc-
tions in the New Covenant era. This is the heart of the contro-
versy between Westminster Seminary and the theonomists. It is
reflected most clearly in Dennis Johnson’s essay in the West-
minster symposium and the responses in this book by Gentry
and Bahnsen. (It will be interesting to see how Dr. Johnson
responds. The ICE publication fund awaits this challenge.)

Consider the statement that was inserted into the 1647 revi-
sion of the legal code of the Massachusetts Bay Colony: “The
execution of the law is the life of the law.”* In short, “no sanctions
~ no law.” Westminster's faculty agrees. They have chosen to
Kill the ideal of a resurrected Old Covenant legal order by
denying the legitimacy of executing that law-order's specified
civil sanctions. Another civil law-order should replace it; what,
they refuse to say. They have become ethical dualists: one law-
order for God’s covenant people (church and family) and an-
other law-order for civil government. Operationally, with respect to
its view of civil law, Westminster Seminary has become Lutheran®
The faculty has not heeded Van Til’s warning:

It is for these reasons that those who have sought to contrast
the genius of Lutheranism with the genius of Calvinism have
stated that Calvinism has emphasized the authority of Scripture

5. This statement appears in “Book of the General Laws and Liberties Governing
the Inhabitants of the Massachusetts [sic], 1647,” in The Koundations of Calonial Ameri-
ca: A Documentary History, edited by W. Keith Kavenaugh, 3 vols. (New York: Chelsea
House, 1973), I, p. 297. The emphasis was in the original document.

6. On Luther's dualism, see Charles Trinkaus, “The Religious Foundations of
Luther's Social Views,” Essays in Medieval Life, edited by John 1. Mundy (New York:
Biblo & Tannen, 1955).
