Editor’s Conclusion 341

to finance such a luxury in a few decades. For now, let us get
on with the task at hand: the evangelization of the world.

If Calvinists refuse to listen, as they did after 1790 in the
United States, the results will be similar: the expansion of non-
Calvinist churches that worship God somewhat loosely rather
than worshipping the academic degree very tightly.

The Theonomic Vision

Being postmillennialists, theonomists see far greater possibili-
ties for Calvinism. We see that new technologies have made it
possible for outsiders of all kinds to make an impact in areas
that had previously been closed to them. Beginning in 1981,
the ICE has proven to its satisfaction, and its critics’ dissatisfac-
tion, that new publishing technologies allow small, struggling
organizations to crack through even the most rigorously en-
forced blackouts. This is why the Westminster faculty was finally
driven to write Theonomy: A Reformed Critique.

Our vision is far more than merely academic. It is pastoral
and ecclesiastical. It is familistic. It is political. It is economic.
Theonomists recognize that during a major cultural crisis,
which we are surely well into today, small groups with a com-
prehensive vision can gain influence way out of proportion to
their numbers. This is not possible in quiet, stable times. Cal-
vinists need to prepare to take advantage of this opportunity.
Calvinism possesses a uniquely comprehensive worldview.

The breakdown of the reigning paradigms of humanist
civilization is already upon us. This presents an opportunity for
social transformation which Christians have not seen since
1860, when Darwin and Huxley began their work of recon-
struction, and which Calvinists have not seen since the restora-
tion of Charles II to the British throne in 1660. This time,
however, there is a worldwide civilization: Western humanism.
This has not been the case since the tower of Babel. This makes
the present opportunity historically unprecedented.
