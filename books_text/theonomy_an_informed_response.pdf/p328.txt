308 THEONOMY: AN INFORMED RESPONSE

Theonomists don’t have a monopoly on sin. There are diffi-
cult people in every theological camp. Related to this tendency
for portraying theonomists in the worst light is the penchant
for judging them in light of the worst representatives or what
appears to be objectionable. This is the fallacy of “guilt by asso-
ciation,” where “a position is objectionable because of other
positions (or people) which are associated — or may be made to
appear to be associated — with the position.”

Many things have come to me about what other “theonom-
ists” have been doing (much of which I have not given cred-
ence to) and since I am a theonomist, 1 must be just like them.
This simply isn’t true. Not all dispensationalists should be
judged unfairly because of R. B. Thieme; neither should ail
theonomists be judged according to poor representatives or
what people perceive to be a poor representative of the posi-
tion. I Corinthians 13:7 has application to theological debate as
well.

What Is the Alternative?

Third, if the theonomists are not on the right track, then
what are the answers? It is easy to talk about “natural law,” but
which version are we to espouse? There have been a number
through history.

The man in the pew needs specific answers and details. For
the everyday Christian to be faithful in his walk and calling, he
needs relevant answers from the Bible, not the imaginations of
man (II Corinthians 10:4,5).

It is disconcerting to read statements such as, “Christians
who find themselves with governing responsibilities in such a
situation may indeed search all of God’s Word for reflections of
his justice that will aid them in their task, made so difficult by
the mixed situation of ‘the present evil age’ (Galatians 1:4)”

Qt. Bahnsen and Gentry, House Divided, p. 58, Emphasis his.
