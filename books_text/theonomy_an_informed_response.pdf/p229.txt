Whose Victory in History? 209

earthly prosperity and dominion such that the wilderness with
its persecutions and temptations will be eliminated or even
marginalized” (p. 228). This helps explain the endorsement of
pluralism in Theonomy: A Reformed Critique.* In essence, plural-
ism is the best for which history's losers may hope.®

This leads me now to consider the only purely eschatological
chapter in the book before us, Richard B. Gaffin’s “Theonomy
and Eschatology.” After struggling for a definition of postmil-
lennialism (pp. 197-202), Gaffin provides the reader with four
theological reservations he has regarding the “triumphalism”
inherent in postmillennialism. Due to space limitations, I will
consider only his two major reservations.’ One he calls his
“primary reservation” (p. 202); the other his “most substantial
reservation” (p. 210). Despite Gaffin’s well-deserved reputation
as a careful reformed theologian and his argument’s prima facie
plausibility, I believe a studied analysis of his presentation will
expose debilitative argumentative flaws within it.

De-eschatologization

Gaffin’s “primary reservation” to Reconstructionist postmil-
lennialism is that it “ ‘de-eschatologizes’ the present (and past)
existence of the church,” so that postmillennialists inadvertently
“devalue Christian life and experience in the present (and the
past)... .” (p. 202). This, he argues, is because postmillen-
nialism “effectively compromises and, in part, even denies” the
“kingship of the ascended Christ” (p. 202).

 

5, See: Will S. Barker's chapicr 10, entitled: “Thconomy, Pluralism, and the
Bible” in TRC.

6. See: Gary North, Political Polytheism: The Myth of Pluratism (Tylex, Texas:
Institute for Christian Economics, 1989) and North, Millennialism.

71. Lwill deal with the alleged problem of watchfulness-and-imminence in my He
Shall Have Dominion: A Postmillennial Eschatology (Tyler, Texas: Institute for Christian
Economics, forthcoming). Also in that work, Gaffin's fourth issue (the wilderness
theme) will be shown to be improperly employed, by a positive setting forth of the
prophetic expectation of Scripture.

 
