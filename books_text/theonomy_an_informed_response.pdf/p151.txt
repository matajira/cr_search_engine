Westminster Seminary on Penology 131

fication, narrows the premise of the @ fortiori argument used by
the author of Hebrews: “At issue is not divine justice in abstract
as a model for political jurisprudence,” but the Lord’s expecta-
tion of a people with covenantal obligations (p. 187).’® On the
contrary, that certainly is at stake, if the author's argument is
intended to be theologically and logically sound!

Tf in any of the ways God metes out punishment for trans-
gression, He is arbitrary, harsh, lenient or of changing attitude,
then one could indeed entertain the possibility that the threat of
eternal condemnation for spurning so great a salvation offered
in the gospel might be “escaped” (Heb. 2:3). But if universally
valid and unchanging justice does not characterize even the capi-
tal crimes of the Old Covenant order, then the New Covenant
(especially with its greater power or emphasis upon grace)
could indeed enunciate threats which do not apply to everyone
or apply for all time. Hell may be threatened, but God could
change His mind (again!) about the absolute justice of His
sanctions — just as He has done with the civil code (on John-
son’s hypothesis).

After all, if God has not insisted upon the universal, un-
changing justice of the lesser (civil penalties), how much more
could we expect that He would relent upon the justice of the
greater (eternal penalties)! This would be a perverse reversal of
the very point made by the author of Hebrews. So Johnson is
in error, The divine justice of all. the penalties of the Mosaic
order — civil, ecclesiastical, eschatological, or what have you — is
precisely the premise upon which the author builds his argu-

Institute for Christian Economics, 1985): “The New Covenant brings greater respon-
sibility for obedience. With the giving of new light and new power in the New Coven-
ant, the responsibility of men to obey the voice of God is increased. ‘fo whom much
is given, much is required (Luke 12:48). . . . The revelation of the New Covenant is
even more inescapable than that of the Old Covenant (Heb. 12:25), and to it we
should give ‘the more earnest heed’ (Heb. 2:1-4)” (pp. 167-68).

16. Nobody is interested, of course, in any “justice in abstract” (pejorative
wording). Itis, however, vital (both ethically and existentially} that justice be “univer-
sal.”
