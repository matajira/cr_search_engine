 

EDITOR’S INTRODUCTION TO PART II

For there is no power but of Cod: the powers that be are ordained of
God. Whosoever therefore resisteth the power, resisteth the ordinance of
i God: and they that resist shall receive to themselves damnation. For
rulers ave not @ terror to good works, but to the evil. Wilt thou then not
be afraid of the power? do that which is good, and thou shalt have praise
of the same: For he is the minister of Ged to thee for good. But if thow
do that which is evil, be afraid; for he beareth not the sword in vain: for
he is the minister of God, a revenger to execute wrath upon him that
doeth evil. Wherefore ye must needs be subject, not only for wrath, but
i also for conscience sake. For this cause pay ye tribute also: for they are
God's ministers, aitending continually upon this very thing. Render
therefore to all their dues: tribute to whom tribute is due; custom to whom
custom; fear to whom fear; honour to whom honour (Rom. 13:1-7;
emphasis added).

 

Christ has not given the sword but the keys to those who are charged with
authority in his name.

Edmund P. Clowney (1961)!

Vern Poythress has titled his book on biblical law, The Shad-
ow of Christ in the Law of Moses. 1 think Theonomy: A Reformed
i Critique would have been more accurately titled, The Shadow of

1. Edawad P Clowney, Preaching and Biblical Theology (Grand Rapids: Eerdmans,
1961), p. 81.

 
