A Pastor's Response 299

was fueled by ARC. Though T:ARC cannot take all the blame,
these are nonetheless the perceptions that arose.

False Perceptions

1. “If one is a theonomist, then he is a legalist and denies the
work of the cross.” This is a common misconception, regardless
of how effectively one argues against it. I am not a Jegalist in
terms of thinking that the Law could save or that our standard
of conduct is determined by man made rules. The Scripture
makes it clear that “by the deeds of the Law no flesh will be
justified in His sight” (Romans 3:20), and I joyfully and heartily
maintain that a man is saved by God’s sovereign grace alone
{Ephesians 2:8,9; Titus 2:11-14). I could not count the number
of times I was told, “John, you need to read the book of Gala-
tians.” I have read it many times and rejoice in its message that,
because of what Christ did on the cross, I am free from the
curse of the Law (Galatians 3:13) in that Christ as my Head and
Substitute has fulfilled all its demands and suffered all its penal-
ties in my place! But this book of Galatians which so magnifi-
cently affirms salvation by grace through faith alone also teach-
es that the Law is not contrary to the promises of God (Gala-
tians 3:21). As Paul aftirms in another place, the Law is estab-
lished by grace (Romans 3:31 in the context of 3:21-31). As a
standard for Christian ethics, the Law is still binding on us
today (Matthew 5:17-19; Romans 13:8-10; Galatians 5:13,14; 1
Timothy 1:5).

2. “If you are a theonomist, then you believe that you force
people to change and put to death those who disagree.” Only
the Holy Spirit can change a person’s heart as he is irresistibly
drawn (John 6:37,44) to Jesus Who is the Desire of All Nations
(Haggai 2:7). The method the Holy Spirit uses to draw people
to the Redeemer is not worldiy (John 18:36) and thus coercive,
but He uses the faithful testimony and lives of Christians. Chris-
tians do not conquer through the sword but through the Gos-
pel (Matthew 18:28-30) and the spiritual weapons God gives us
