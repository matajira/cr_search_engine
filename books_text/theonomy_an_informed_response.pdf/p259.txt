Whose Conditions for Charity? 239

destitute workers (Leviticus 19:9-10; 23:22; Deuteronomy 23:24-
25; 24:19-22). These “gleaner laws” stipulated that farmers and
landowners leave the edges of their fields unharvested and that
overlooked sheaves remain uncollected. Any among the poor or
the alien who were willing to follow behind the harvesters and
gather that grain were welcome to it, thereby “earning” their
own keep. Ruth tock advantage of this just provision and was
thus able to uphold her responsibility to Naomi. . . . According
to RJ. Rushdoony, “This was indeed charity, but charity in
which the recipient had to work, in that gleaning the fields was
hard, backbreaking work.”

Grant applied Old Testament Law to the poverty question.
This made him unique as an evangelical. Why didn’t other
evangelicals and Reformed people outside the theonomic camp
come up with the gleaning approach? How could they, when they
reject the continuing application of Old Testament civil law,
which Keller also does? Interesting, isn’t it, how we find that
those who appear to be so much in favor of helping the poor
abandon the most obvious Scriptural advice when they reject
the application of the endive Bible?

In all fairness to George Grant, however, he did not end
with the Old Testament. He was no legalist. He skillfully ap-
plied the Gospel and New Testament by calling for what he
expressed as the need for compassion. He said:

The Samaritan in the story is a paragon of virtue. .. . But
perhaps even more significant than his strict adherence to the
Law was the compassion that the Samaritan demonstrated. He
wasn’t simply “going by the rules.” His was not a dry, passionless
obedience. He had “put on tender mercies, kindness, humble-
ness of mind, meekness, longsuffcring” (Colossians 3:12). He
“became a father to the poor, and searched out the case” of the

6. Ibid., pp. 79-80.
