8

WHOSE VICTORY IN HISTORY?

Kenneth L. Gentry, Jr.

It is quite evident that Westminster Seminary’s published
critique of Christian Reconstructionism is systematically operat-
ing from a two-fold agenda. The aspect of the agenda receiving
the most focused concentration in Theonomy: A Reformed Critique
is the denial of God’s objective divine sanctions in history. This
resistance to the contemporary, historical application of God’s
Law (including its criminal sanctions) to all of life and culture,
is evident throughout the work.

But there is a second feature of the book, which is vitally
related to this concept and which flows as a major undercurrent
through many of its chapters. That strong and deadly undertow
is the book’s antipathy to postmillennialism’s confident expecta-
tion of the culture-wide victory of Christianity in history.

The tragedy of such a dual agenda should be evident. When
historical pessimism is coupled with a studied resistance to historical
divine sanctions, the effective result is a denial of Christian culture.
Christian Reconstructionists have vigorously argued this as their
distinctive contribution to Christian social theory.’

1, See especially the recent works in this area: Gary North, Millennialism and
Social Theory (Tyler, Texas: Institute for Christian Economics, 1990) and Kenneth L.
