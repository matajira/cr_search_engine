The Sorry State of Christian Scholarship 353

delivered eight lectures during his four-day stay at Liberty.
This extended period of time and the open-forum atmosphere
of his lectures gave any and ail students and professors ample
opportunity to question Dr. Bahnsen. In addition, there was
plenty of time for a debate with thosc professors who took issue
with Reconstructionist distinctives. The “heretic” was in their
midst. Why not demonstrate to the entire student body that
Christian Reconstruction is “heresy” by making a public specta-
cle of the position?

Why didn’t those who opposed Christian Reconstruction
intellectually “flog” Dr. Bahnsen openly? The critics of Chris-
tian Reconstruction have the same problem as the evolutionists:
the Christian Reconstruction position is very strong and, so far,
has not been successfully refuted by an appeal to the Bible (at
least from what I’ve seen and read from the critics), and the
critics’ position is weak and nearly indefensible. How do you
keep the public from finding this out? Answer: Avoid public
debate and publish inaccurate critiques that would rate a grade
of C- at any second-rate humanist college.

‘Three of the professors demonstrated their “scholarly” wares
by hosting a forum on Christian Reconstruction after Dr. Bahn-
sen had left the campus. How convenient. Wait until the most
articulate spokesman for the position leaves, create a theological
straw man, burn him in front of an interested audience, and
then claim that you refuted the position.

Dr. Norman L. Geisler, director of the Center for Research
and Scholarship (it says here), was the chief antagonist. Liberty
University’s student newspaper called it “a debate.”'® Those
professors and students who were sympathetic to Christian
Reconstruction did not learn about the “debate” until they saw
it advertised on a poster the day before Dr. Bahnsen was to
leave the campus. The poster described the event as follows: “A

15. Curt W. Olson, “Biblical Government Debated,” The Liberty Champion (March
26, 1991), p. 6.
