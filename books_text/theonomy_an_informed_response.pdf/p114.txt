94 THEONOMY: AN INFORMED RESPONSE

to Machen? “The technique of tyranny has been enormously
improved in our day. . . . A monopolistic system of education
controlled by the State is far more efficient in crushing our
liberty than the cruder weapons of fire and sword. Against this
monopoly of education by the State the Christian school brings
a salutary protest.”*

Machen also found the wretched intrusion of the state into
areas not authorized by God displayed in the “child-labor
amendment,” “over-regulated cities,” the system of National
Parks, federal police surveillance and fingerprinting, managed
currency (unbacked dollars), as well as the advocacy of a Feder-
al Department of Education (calling this a “vicious proposal”).
Machen declared that “if liberty is not maintained with regard
to education, there is no use trying to maintain it in any other
sphere. If you give the bureaucrats the children, you might as
well give them everything else.”® Machen insisted upon certain
basic rights of individuals and families which must never be
trampled under foot for any supposed advantage or in any
emergency (e.g., property, privacy, speech), He insisted upon
judicial restraint, states’ rights (versus federal intrusion), and a
free market. :

Machen openly opposed socialist conceptions of the state and
explicitly taught that “the Christian idea, which is also the truly
American idea, [is] that the State exists for the repression of
evil-doers and the protection of individual liberty.” Accord-
ing to him, the civil government is “not intended to produce
blessedness or happiness but intended to prevent blessedness or
happiness from being interfered with by wicked men.”” And
Machen would have the statc find its standards for dealing with
wicked men in the law of God.

8. Ibid., pp. 66-67, 68.

9. “Shall We Have a Federal Department of Education?” (1926), itid., p. 98.
10. “The Christian School the Hope of America” (1984), ibid., pp. 137-38.
11, bid., p. 131.

12, bid., p. 138.
