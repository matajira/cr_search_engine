10

HERMENEUTICS AND LEVITICUS 19:19
—- PASSING DR. POYTHRESS’ TEST

Gary North

¥% shall heep my statutes. Thou shalt not let thy cattle gender with a
diverse kind: thou shalt not sow thy field with mingled seed; neither shall
@ garment mingled of linen and woolen come upon thee (Lev. 19:19).

When I wrote Westminster's Confession, | hammered on several
themes. One of them was this: “You can’t beat something with
nothing.” If a Christian refuses to accept the covenantal ideal of
Christendom, then what does he offer in its place? In whose
name? According to which biblical texts? The faculty at West-
minster Theological Seminary clearly rejects the ideal of Chris-
tendom. They have attacked theonomy’s assertion of the legiti-
macy of this ideal, but they have offered no alternative. Several
members have accepted the legitimacy of natural law theory
(whether Aquinas’ version or Newton’s is unclear). This breaks
with Van Til’s rejection of natural law theory (both Aquinas’
version and Newton’s).' The question then arises: Is theonomy

1. Gary North, Westminster's Confession: The Abandonment of Van Til’s Legacy (Tyler,
Texas: Institute for Christian Economics, 1991).
