204 THEONOMY: AN INFORMED RESPONSE

is antinomian. If put into practice, it would lead to the bank-
ruptcy of the church. It is based on guilt rather than law.

The provision of charity is a positive sanction by the church.
This is what distinguishes it covenantally from the state, biblical-
ly: the church, unlike the state, is an agency of positive sanc-
tions. So is the family. The state, with its monopoly of violence,
is not. By making the state into an agency for administering
positive sanctions, the statist transforms the state into a pseudo-
family,’ and in some cases, into a pseudo-church. The state
then becomes messianic.

Preaching

The church, not the family or the state, is the central
institution in history. It alone carries into the world beyond the
final judgment (Rev. 21, 22). Neither the family nor the state
can claim this degree of centrality. Any discussion of Christian
reconstruction must begin with the church. Any attempt to
defiect the primary responsibility for Christian reconstruction to
any other institution is doomed to failure, for no other insti-
tution is empowered by God to withstand such pressure and
responsibility. Clanism-patriarchalism results from making the
family (or the private school) the central covenantal institution.
Statism is the result of the attempt to make the state the central
institution: Marxism, socialism, fascism, or Keynesianism. Nei-
ther familism nor statism is biblical.

The institutional church has three aspects: the preaching of
the Word of God, the administration of the sacraments (positive
sanctions), and the defense of the sacraments against unauthor-
ized participants (negative sanctions). Calvinism has always
stressed preaching, although it has formally admitted the other
marks of the church. (Weekly preaching is universal in Calvinist
churches; weekly communion isn’t.) So, I focus on preaching.

5, Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
‘Texas: Institute for Christian Economics, 1986), ch. 5.
