 

 

 

A Pastor's Response $11

fairly and accurately. One would not expect the dispensation-
alist to do so, but one would expect those who in many ways
appreciate the underlying principles of theonomy to do so.
Maybe the contributors to ARC assumed the average reader
would understand the basic tenets of theonomy and its
strengths. If this is the case, then the assumption was wrong.
The man in the pew is getting a much different picture, and it
is not a pleasant one to behold. As the Holy Spirit continues to
drive each of us to the infallible Word of God, the misconcep-
tions will be remedied and agreement will be reached. In the
meantime, may the critics of theonomy be more careful in their
scholarship and assessment of this perspective and of these men

who are endeavoring to grapple with the “whole counsel of
God.”
