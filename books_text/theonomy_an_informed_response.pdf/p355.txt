Editor’s Conclusion 335

for ministerial training had always been corrupting, both epis-
temologically and institutionally. Greek intellectual presupposi-
tions and Roman Catholic teaching methods were imported
into the Protestant church by way of the college’s classical cur-
riculum. The Puritans had long recognized this threat, but not
even Oliver Cromwell and his army could eradicate the power
of Oxford and Cambridge.” Harvard and Yale became equal-
ly immune in New England. These bastions of heresy, not
orthodoxy, were honored. Yet today, Bible-believing seminaries
prefer faculty members who are certified by Harvard or Yale.

Formal certification is the today model for all education, but
the original model was the European university of the twelfth
century. This worship of academic certification has always been
the weak link in Calvinism as an institutional phenomenon.”
First came the Protestant scholastic gauntlet. Then came the
Unitarian gauntlet. Finally came the seminary. Until Van Til
came on the scene, this also served as a Scholastic-type gauntlet:
Scottish common sense realism.”

The seminary’s graduates, few in number, at graduation are
thrown into a glutted market that can pay them practically
nothing. Ignoring the task of imparting a marketable skill -
tentmaking — the seminary teaches students a few rudimentary
skills of biblical exegesis, mainly in the form of writing one or
two sermons per year, if that many. It also requires students to
develop their undergraduate skills in taking examinations.
These examinations are the same sort of academic exercises
that were once imposed on the seminary’s faculty members by
the secular universities that granted them their degrees.

30. John Morgan, Godly Learning: Puritan Attitudes towards Reason, Learning, and
Education, 1560-1640 (Cambridge: Cambridge University Press, 1986), Conclusion.

$1. The lust for certification has been a lust of middle aged men that is then
imposed on youth. Youth normally has more interesting lusts to contend with, a fact
that has never sat well with middle aged men.

32. Mark A, Noll, ed., The Princeton Theology, 1812-1921 (Grand Rapids: Baker
Book House, 1983), pp. 30-33.
