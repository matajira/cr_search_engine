352 THEONOMY: AN INFORMED RESFONSE

minster Theological Journal, “the reviewer demanded that nobody
be allowed to respond to him in print - and the editor yield-
ed!” It’s unfortunate that the “Christian intellectual world is
retreating to a new dark age - one which shuns open investiga-
tion of the truth, blackballs those who disagree, and works
according to prejudice instead of analysis.”"* Such antics have
been shunned by the best Protestant scholars. The victim says:

Our Christian forefathers through the ages staunchly main-
tained that the truth has nothing to fear from public exposure.
‘They always figured that the easiest (and most honest) way to
silence a contrary point of view was to refute it. The desperation
to keep the Christian public from contact with hearing or consid-
ering the theonomic point of view makes one think we are deal-
ing with pornography, rather than stodgy, age-old Puritan theol-
ogy!”?

Much of Protestant “scholarship” has adopted some of the
tenets of the “Politically Correct” speech crowd. Just as there’s
a “politically correct” way to talk about race, sex, and class on
college campuses,” there’s a “theologically correct” way to talk
about law, eschatology, and social action at many churches,
Christian colleges, and seminaries.

Is There “Liberty” at Liberty?

Consider a 1991 incident of this sort of “Christian scholar-
ship.” Dr. Bahnsen, who is a formidable debater and known as
such, was invited by a professor at Liberty University in Lynch-
burg, Virginia, to speak on numerous topics. Dr. Bahnsen

11. Greg L. Bahnsen, No Other Standard: Theonomy and Its Critics (Tyler, Texas:
Institute for Christian Economics, 1991), p, 1.

12. Idem.
13, Idem.

14. See New York Magazine (January 21, 1991), Newsweek (December 24, 1990),
Campus (Winter 1991) for examples of the new intellectnal fascism.
