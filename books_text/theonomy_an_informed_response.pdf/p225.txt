Editor’s Introduction to Part LT 205

Hermeneutics

If theonomy had no principle of biblical interpretation — no
hermeneutic — then it could offer no guide for preaching. The
essay by Vern Poythress in the Westminster symposium accuses
theonomists of not having offered a hermeneutic that is both
theonomic and at the same time uniquely capable of being
applied to real-world situations. He argues that other hermen-
eutics are equally valid, especially Meredith G. Kline’s.

If true, this would be a serious accusation. More than this: it
would be fatal to theonomy’s claim of universal transformation.
So, the issue of hermeneutics is vital for the theonomic move-
ment. Theonomy’s principles of biblical interpretation must be
made clear enough to enable pastors to use them. If this cannot
be done, then the preaching of the churches cannot be trans-
formed by theonomy or lead to transformed lives and institu-
tions. This is why the lack of a uniquely theonomic hermeneutic
would be fatal for theonomy. Unless the church preaches by
means of such a hermeneutic, theonomy will remain a curiosity
of scholars. It will not lead to social redemption.

Poythress is a victim of an affliction called multi-perspectiv-
alism. It leaves the expositor with too many potential paths to
the Bible’s truth. This truth remains ever elusive, for with too
many paths leading into it, there are too many paths leading
out of it. There can be no uniquely biblical guide to specific
personal or social actions if there are many paths to truth.

Poythress wants a blueprint so cluttered with options that no
building could ever be constructed by using it, and certainly no
building taller than a single story. To assert the existence of
many covenantal blueprints is to assert the non-existence of any
uniquely biblical covenantal blueprint. This is his way of deny-
ing the concept of biblical covenantal blueprints. By opening
the hermeneutical door to everyone, he closes the door to the
ideal of Christendom. This is a more polite, seemingly less
humanism-compromised way of denying the legitimacy of
Christendom than the approach taken by the Calvinistic “prin-
