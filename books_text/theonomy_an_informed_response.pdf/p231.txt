Whose Victory in Ilistory? 2i1

“until” all of his enemies become his footstool. This is such a
significant truth for biblical theology that Psalm 110:1 is the
Old Testament reference that is quoted most often in the New
Testament. Gaffin rightly points out that Christ is currently the
absolute sovereign and has been since His ascension (p. 203).
But he sees the “until” of Psalm 110:1 as pointing to Christ’s
Second Advent, when He defeats death when He closes history:
“,.. that decisive, quantum transition is plainly associated with
events concommitant with his personal bodily return . . . and
not with some prior, intermediate point or set of developments
leading to his return” (p. 203). Is he correct in this view?

The word “until” in Psalm 110:1 is the Hebrew word: @.
According to Brown, Driver, and Briggs this word marks “not
an absolute close, but an epoch, or turning-point, in the
fut(ure].”® This lexical observation may be seen at work in
Paul’s important employment of Psalm 110:1 in chapter fifteen
of 1 Corinthians. Paul’s majer point in 1 Corinthians 15, to be
sure, has to do with “the end” of history (v. 24), when the
resurrection occurs at Christ’s Second Coming (v. 23). The resur-
rection isthe ultimate and conclusive demonstration of Christ’s
victory over death. And as a matter of fact, this is the focus of
Paul’s attention (sce vv. 12-23, 35-38).

Yet in the very context in 1 Corinthians 15 where Paul al-
ludes to both Psalm 110:1 (see v. 25) and Psalm 8:6 (sce v. 27),
he also clearly mentions Christ’s present continuing reign as a
progressive, unfolding reality: “For He must reign [present infini-
tive] until He has put all His enemies under His feet” (v. 25). His
present reign, which is from heaven where He has all authority,
seeks its historical manifestation through the present progressive
abolishing of “all rule and all authority and power” (v. 24).
Consequently, He will not come (v. 23) until “the end” (v. 24a),
which will not occur until “after” (totan with the aorist subjunc-

9, Francis Brown, S. R. Driver, and Charles A. Briggs, A Hebrew and English
Lexicon of the Old Testament (Oxford: Clarendon Press, 1907), p. 725.
