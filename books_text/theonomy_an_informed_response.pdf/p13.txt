Editor's Preface xiii

dark cloud over the Protestant world. For three centuries,
Protestants have relied on the Presbyterians to defend the
integrity of the Bible from the higher critics. They have also
expected the Presbyterians (with a little help from the Luther-
ans) to defend the (non-predestinarian) “fundamentals of the
faith.” The Presbyterians, in turn, long relied on Princeton
Seminary; and then, after 1929, on Westminster. But there is
no one at Westminster today with the skills ot reputation of
Young, Stonehouse, Murray, or Van Til, nor is there likely to
be if things do not change drastically. Yet Westminster’s aca-
demic leadership in the orthodox Protestant world has not been
challenged by another seminary. Westminster Seminary is still
the most academically competent Protestant seminary in the
English-speaking world. This fact is a grim testimony to the
historically incomparable deterioration of orthodox Protestant
seminary scholarship since Van Til’s retirement in 1972.

A Universal Deterioration

Americans complain that the Scholastic Aptitude Test (SAT)
scores of high school seniors have fallen, year by year, since
1963. This slow but steady erosion of academic performance
has been matched, step by step, by the decline of America’s
Bible-affirming colleges and seminaries. There is simply no
academic leadership remaining in any single Bible-believing
institution of higher learning. Bible-believing Protestants have
no university, meaning a Ph.D.-granting institution (excluding
Bob Jones University’s self-anointed effort). We also cannot
point to any Christian college or seminary and say: “Here is the
last bastion. Here we can find the whole counsel of God taught
with confidence, rigor, and full documentation, in every depart-
ment without exception.”

There is no longer a last bastion. The mainline institutions of
Christian higher learning have all gone soft theologically; most
have gone liberal. Nothing saved them: not tax-exemption, not
accreditation by humanists, not computerized mailing lists, not
