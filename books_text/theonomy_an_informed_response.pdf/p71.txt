Some Wings for Modern Calvinism’s Plane 51

Similar to the appeals by Kuyper, Henry R. Van Til, and
Meeter, the authors of the symposium believed that the com-
prehensive nature of the applicability of the Bible was unique
to Calvinism. This included the applicability of God’s law. “In
Reformed church worship the law is an integral part of the
sacred program. Many Fundamentalist fellow-Christians seem
to know the law in only one relation, viz., that of sin and re-
demption. . . . The Heidelberg Catechism™ recognizes the
significance of the law both as a teacher of sin and as a norm
for the Christian’s life of gratitude, and it gives an exposition of
that law precisely in the latter context.”

The Presbyterian dispensationalist, Donald Grey Barnhouse,
had no such high view of the law. He considered it to be a
“tragic hour when the Reformation churches wrote the Ten
Commandments into their creeds and catechisms and sought to
bring Gentile believers into bondage to Jewish law, which was
never intended either for the Gentile nations or for the
church,” Jn following the debate over Christian Reconstruc-
tion, a number of Reformed brethren seem to be more com-
fortable with the dispensationalism of Barnhouse than the high
view of the law of the Reformed confessions and catechisms. It
is time for Calvinists to abandon dispensationalism.

28. On the Forty-First Sabbath, question 108 of the Heidelberg is read:
Ques. 108. What doth the seventh command teach us?

Ans. ‘That ail uncleanness is accursed of God, and that, therefore, we
must, with all our hearts, detest the same, and live chastely and temperately,
whether in holy wedlock, or in single life.

Under the “Explanation and Proof” various texts are added, including Leviticus
20:10: “And the man that committeth adultery with another man’s wife, even he that
committeth adultery with his neighbor's wife, the adulterer and the adulteress shall
surely be put to death.”
29. Bouma, “The Relevance of Calvinism for Today," God-Gentered Living, p. 20.
30. Quoted in 8, Lewis Johnson, “The Paralysis of Legalism,” Bibliotheca Sacra,
Vol. 120 (ApriJune 1963), p. 109.
