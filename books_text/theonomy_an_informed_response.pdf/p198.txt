178 THEONOMY: AN INFORMED RESPONSE

priestly ministry of the OT to the priestly ministry of Jesus.””"
Or as Windisch puts it: Hebrews views the law “not as a pre-
scription for the behavior of the individual, but as the sum of
sacrificial regulations for the ancient cultic community.””

The “fault” (Heb. 8:7) in the Old Covenant was not in the
civil and moral standards (Heb. 2:2; 8:10; 10:16; cf. Rom. 7:12;
1 Tim. 1:8), but in the temporary nature of the administrative
priesthood (along with its concomitant ceremonial features),
which could not finally settle the sin question. “A new order of
priesthood presupposes a new disposition of law. The introduc-
tion of a new and different order of priesthood necessitates the
setting aside of the law insofar as its prescriptions for the regulation
of the old priesthood and its ministry are concerned, and the provi-
sion of a new law by which the new system is governed.””
“The ‘change in the law’ is seen in this, that, with the establish-
ment of the order of Melchizedek, the numerous precepts of
the law respecting the function of the levitical priesthood have
been abrogated and have fallen into desuetude, and. . . have
been replaced by the new principle or ‘law’ of faith.”

Calvin comments: “The Law contains both the rule for good
living and the free covenant of life, and there run through it
many outstanding passages which instruct us in the faith and in
the fear of God. None of this has been abolished in Christ, but
only that part which was involved in the old priesthood.”">

Purdy comments on Hebrews 7:11: “What could this mean?
That the whole law was set aside? Then why is the argument

31, TDNT, 4:1078.

$2. H. Windisch, Der Hebraerbrief (Tubingen: Mohr, 1931), p. 66.

33. Hughes, Hebrews, p. 256 (emphasis mine).

34, Ibid, p. 257. See the antipathy of the Jews to this in Acts 6:14; 8:3; 21:28;
Phil. 3:57, “He turns to explain the priesthood of Christ, the true, pure understand-
ing of which abolishes all the ceremonies of the Law.” John Calvin, The Epistle of Paul
the Apostle to the Hebrews and the First and Second Epistles of St, Peter, trans. by William
B, Johnston, in David W. Torrance and Thomas F. Torrance, eds., Calvin's Commen-
taries (Grand Rapids: Eerdmans, 1963), p. 3.

38. Calvin, Hebrews, p. 96.
