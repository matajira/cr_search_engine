264 THEONOMY: AN INFORMED RESPONSE

growth of population was also taking place in othcr European
nations and in North America — nations that had not yet experi-
enced an industrial revolution.” This points to the possibility
that the slow but steady increase in agricultural productivity
outside of England had been more important in increasing
population than England’s industrial revolution was. Agricultur-
al productivity did not rise in England after 1750. England
became an importer of food, selling its industrial products
abroad to pay for these imports.” This meant that other areas
were producing agricultural surpluses.” Successful agricultur-
al techniques discovered in one region were imitated through-
out Western Europe. This leads us back to the problem of
Leviticus 19:19 and the corporate blessings of God. If progress
is a blessing, and innovation is at the heart of progress, is the
Bible opposed to progress? Is it opposed to innovation?

Innovation

The fundamental change in the West’s traditional economy
was the appearance of widespread innovation. As never before
in man’s history, innovation began to reshape economic pro-
duction. Entrepreneurs gained access to capital, and this capital
allowed them to test their visions of the future in the competi-
tive marketplace. Either they met consumer demand more
efficiently than their competitors, thereby gaining short-term
profits until other producers imitated their techniques, or else
they failed. The winners were the consumers, whose economic
decisions steadily became sovereign in the economy. Rosenberg

25. Ibid., p. 6. Cf Shepard B. Clough, The Economic Development of Western
Civilization (New York: McGraw-Hill, 1959), pp. 241-42.

26. Brinley Thomas, “Food Supply in the United Kingdom During the Industrial
Revolution,” in Joel Mokyr, ed., The Economics of the Industrial Revolution. (London:
Gcorge Allen & Unwin, 1985), p. 142.

27. A surplus does not necessarily mean abundance, and surely did not mean
this in the eighteenth century. A surplus is merely an asset that its producer regards
as less valuable to him than the item he receives in exchange.
