124 THEONOMY: AN INFORMED RESPONSE

proach against a neighbor, slanders, breaks promises, or lends
money upon interest (vv. 3-5). In Psalm 24 David again asks
who may ascend into the hill of Jehovah and stand in His holy
place (v. 3) —- however his concern here (which indirectly saps
Johnson’s argumentation of strength) is for the holiness of God
which is appropriate to the entire world due to God’s tabernacle
on earth (vv. 3, 7-10): “The carth is Jchovah’s and the fulness
thereof, the world and they that dwell therein” (v. 1), We learn
that anybody on earth who has sworn deceitfully (v. 4) is ren-
dered unfit to enter God’s presence (typified at the tabernacle).

If we ask what the Bible includes in “all” that would render
someone unfit for God’s presence, and we think more broadly
and theologically now, the answer would be any sin of any sort at
any time or place at all. Dr. Johnson knows this. “God is light; in
Him there is no darkness at all” (1 John 1:5). “The evil man
shall not sojourn with” Jehovah (Ps. 5:4). His “eyes are too pure
to look on evil” (Hab, 1:23). Therefore all who are under the
curse of sin will one day be ordered “Depart from Me into the
eternal fire” (Matt. 25:41) — “shut out from the presence of the
Lord” (2 Thess. 1:8-9). “Nothing impure will ever enter” the
eternal city of God — “nor will anyone who does what is deceit-
ful” (Rev. 21:27; cf. Psalms 15 and 24!),

Johnson has said that “certain” of Israel’s penal sanctions
expressed a heightened responsibility to separate from all that
rendered a person unfit for God’s presence. From what we
have seen in Scripture, this would mean that the penal sanc-
tions in Isracl should have been enforced against every single
kind of sin in whatever degree — including promise-breaking,
slander, ungracious lending, etc. But that simply is contrary to
historical and revealed fact, in which case Johnson’s explanation
for the Mosaic penal code is misleading and refuted (modus
tollens). It has proven to be arbitrary or inconsistent. It should
have been the case (on Johnson’s hypothesis) that ail sins called
for the sanction of “cutting off” in Old Covenant Israel.
