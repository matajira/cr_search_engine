188 THEONOMY: AN INFORMED RESPONSE

ment? This is not a radical question. After all, most evangelicals
affirm future eternal sanctions as well as capital punishment for
murder, Still further, may we on the basis of Johnson’s argu-
ment throw out the requirement of witnesses (Heb. 10:28) for
excommunicatory sanctions?

It is true, of course, that the author of Hebrews draws these
penal sanctions into a discussion of the ultimate repercussions
of spiritual apostasy from the Christian faith. But his a fortiori
argument does not dismantle the civil utility of the penal sanc-
tions. We have examples of this fact in other New Testament
passages. In Matthew 5, Jesus applied the prohibition against
murder to the root cause of murder, which is hatred. And
though He references the fact of the capital sanction against
murder, His urging the deepest spiritual meaning of the Law
does not render its capital punishment inoperative. In Hebrews
10, the writer applies the prohibition against idolatrous aposta-
sy to its root effect, unbelief. And though he references the fact
of the capital sanction against idolatrous apostasy, His urging
the deepest spiritual meaning of the Law does not render its
capital punishment inoperative (see discussion below for the
nature of the apostasy in view). That is, there may be temporal
capital sanctions against physical acts administered by the civil
magistrate, while at the same time there exists the threat of eter-
nal judgmental sanctions against related spiritual acts, adminis-
tered by the Lord of glory.

Just because the Church is only given the “keys of the king-
dom,” whereby she punishes by excommunication, this does not
mean that the State may not have an obligation to wield its
sword against the same excommunicable act. Afler all, if a
church today has a member who murders someone, may not
the church excommunicate him and the State capitally punish
him? The two types of sanction are distinct, to be sure, but they
are not mutually exclusive.
