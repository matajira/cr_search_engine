lex talionis, 146
liberalism, xi, xv, xiii-xiv
Liberty University, 352-54
linen, 284, 290, 292
Longman, Tremper
death penalty, 114
Israel vs. America, 113
penology, 137
prisons, 115
sanctions, 117-18
simplicity theory, 303
subjectivity, 115
Lord's Supper, 195, 204
Loyola, Ignatius, 327
lunch (free), 252
Luther, Martin, 22
Lutheranism, 6, 86

Machen, J. Gresham
America’s apostasy, 95
bureaucracy, 93
democracy, 93
education, 94
free market, 94
God's law, 90-91, 93, 95
Jamison &, 18
Mencken on, 355
neutrality, 93
pietism, 92
political liberty, 93, 94
Schaeffer &, 57
scholarship, xii, 346
social reform, 91-92
state, 89, 94, 95
worldview, 89

media, 349
Madison, James, 327, 332

377

magistrate
mediatorial justice, 156
minister, 138, 139
Murray’s view, 250-51
Marcion, 140, 199, 318
Martin, Walter, 347
Marx, Karl, 65
Maselink, William, 72-73
masochists, xvii
Massachusetts, 6
McCartney, 142n, 143, 146,
147n
McIntire, Carl, 19
Meeter, H. H., 47-49
Mencken, H. L., 355.
Methodists, 333
mind-reading, 25
Mises, Ludwig, 19
Molech, 276
Monsma, John, 27-28
Moral Majority, 68-69
Morris, Henry, 330
mortgages, 268-69
Mouw, Richard, 65-66
Muether, John R.
creationism, 82
DeMar on, 23
doomed task, 15
gold standard, 70
intellectual standard, 330
little reading, 24
Kline &, xx
rhetoric, 104n
unserious critic, 295
mules, 282
Munster, 84
murder, 114, 144, 188
