282 THEONOMY: AN INFORMED RESPONSE

What about genetic experimentation? The same prohibition
applied. There could be no lawful, systematic mixing of seeds.
Man was not to apply his ingenuity to the creation of new spe-
cies of plant. Hybrid animals and seeds were illegal to develop.
They could be purchased from abroad, but since most hybrids
are either sterile (e.g., mules) or else they produce weak off-
spring, there was little economic incentive to import hybrids
except as a one-generation consumer or producer good. Such
imports were legal: with no “inheritance” possible, there was no
symbolic threat from hybrids. A hybrid was.not prohibited because
of its status as a hybrid. It was illegal to produce hybrids delib-
erately because of the prohibition against mixing seeds, which
was fundamental. The practice of seed-mixing was illegal, but
not because this practice produced biological hybrids.

As evidence of this statement, I offer the mule. Here is the
classic animal hybrid: the sterile product of horse and donkey.
It is a very strong work animal. It was used as a military animal
in ancient Israel (II Chron. 12:40). The presence of mules in
the household of the kings (II Sam. 13:29; I Kings 10:25) and
the presence of 240 mules among those who returned to Jeru-
salem from Medo-Persia (Ezra 2:66) indicate that there was
never any “creation ordinance” against hybrids. If there had
been, Ezra and Nehemiah would have kept such beasts out of
the land when they returned to rebuild the walls of Jerusalem
and the temple. Mules could be imported, even though it was
not legal to breed horses and donkeys to produce them. The
deliberate mixing of seeds was illegal, not the offspring as such.

It needs to be pointed out that this law did not apply to the
familiar practice of grafting the branches of one species of fruit
tree into the trunk of another. Leviticus 19:19 was specific:

47, The mascot of the United States Military Academy (Army) is a mule. Mules
were still being used in wartime as late as. World War IT.

48. Rabbinic opinion on this verse forbade grafting. See Nachmanides (Rabbi
Moshe ben Nachman, the Ramban), Commentary on the Torah: Leviticus (New York:
Shiloh, {12672} 1974), p. 295. He cites the Talmud: Kiddushin 39a.
