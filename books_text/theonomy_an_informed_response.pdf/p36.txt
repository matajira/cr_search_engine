16 THEONOMY: AN INFORMED RESPONSE

road to belief in theonomy. If there were, I would be in the
middle of it, directing traffic and collecting tolls. Critics such as
Muether should recognize by now that I know something about
direct-mail advertising. If there were a formula for becoming a
theonomist, I would be in the mails with it. I have detected no
pattern in the many conversions to theonomy. There are many
individual roads to the law of God, although most of them tend
to be dirt or gravel roads today. The two main paved highways
lead Calvinists and charismatics into Christian Reconstruction.
The Calvinists come as a result of Calvinism’s unique theologi-
cal heritage: judicial theology. Charismatics are likely to come
by way of postmillennial eschatology (David Chilton’s Paradise
Restored) and social activism. If we ever get a highway into (and
therefore out of} Baptist country, theonomy will become a
serious organizational threat to the pietist-humanist alliance.”

In investigating the so-called theonomic attraction, let us
begin with the co-founders of the Christian Reconstruction
movement: R. J. Rushdoony and Gary North.

R, J. Rushdoony

Theonomy as a theory began with Rushdoony’s conversion
to Calvinism. His father had been a Presbyterian pastor to
various Armenian churches. He was an Arminian. An Arminian
Armenian. His son switched allegiance from Jacobus Arminius
to John Calvin. From Calvin’s doctrine of the absolute sover-
eignty of God over time and eternity, Rushdoony moved to the
philosophy of Cornelius Van Til: the absolute sovereignty of God in
all thought. This means the absolute sovereignty of the Bible in
all thought. But if this is true for the intellect, Van Til taught,
it must be equally true of ethics. Rebellion is not merely intel-
lectual; it is covenantal, involving the whole man. Therefore,
rebellion must involve all of man’s institutions.

2. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion Clyler,
‘Texas: Institute for Christian Economics, 1985), pp. 2-5.
