Westminster Seminary on Pluralism 109

These exhortations should be taken to heart by theonomists.
We must not assume the unteachable arrogance of the Jews
(Rom. 2:17-20). Nothing can be taken for granted. We must
check and recheck our answers, needing more experience in
the Word of righteousness (Heb. 5:13-14). Unbending dogma-
tism is inappropriate. Yes, that all needs to be said and put into
practice.

Yet there is also reason to worry that Christians (especially in
our day) can be so averse to any biblical confidence and theo-
logical “dogmatism” that they will unduly hesitate to proclaim
God’s Word with power and conviction ~ with specificity and
application (not just broad generalitics). There is the danger
that the Evil One will so intimidate us before the task of read-
ing God's law and putting it into practice (or exploit our linger-
ing rebellion against the commandments of God) that we will
hide behind the “complexities” and present “political uncertain-
ties” so that the task is never seriously engaged or begun — even
more that it is never boldly brought to bear as the result of
responsible, sanctified study.

The Evil One would love to silence the guardians of God’s
good news (including its moral standards), If we have a morally
sick society, it would be Satan’s desire to hamper the doctor
from ever arriving with the needed medicine! Or if we cannot
be stopped altogether, at least he might hamper us from apply-
ing the full dosage, keeping the effects of the healing Word of
God confined to our private hearts and delimited church zones.

Conclusion

My earnest admonition back to my brothers on the Westmin-
ster faculty (and everywhere) is that we not portray the task
God has given us as overly difficult and virtually impossible to
do. My admonition is against a kind of functional agnosticism that
easily becomes the theologian’s (and seminarian’s) self-inflicted
paralysis. Nobody who knows me and the nature of my work
will mistake me for endorsing gullibility, naivete, and lack of
