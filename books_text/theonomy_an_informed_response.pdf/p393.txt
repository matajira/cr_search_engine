Grant, George
Chilton &, 240
conditional charity, 244
HELPS, 240
Old Testament law, 238-
40
Reconstructionist, 236-37
Samaritan, 240, 244
State welfare, 237-38
Great Commission, 106
Groothuis, Douglas, 32

Hardy, Oliver, xviii
hatred, 188
healing, 197, 203, 324
Heidelberg Catechism, 51
hell, 181, 199, 315
HELPS, 240
Hendrikson, W., 226-27
Henry, Carl, 57
hermeneutic
Bahnsen’s, 119-20
continuity, 140
Kline's, 343
Poythress, 256-58
preaching, 205
theonomic, 26-27, 292-
93
‘Westminster vs. theon-
omy, 7
biblical, ix
Herod, 100
hierarchy, 83-86
history, 202-3
hit & run, 2,11
Hitler, Adolf, 36, 73, 103
Hodge, A. A., 30, 31, 329
Hodge, Charles, 162, 329

373

holiness, 260-61, 284n, 292
holiness (sanctions &&), 122-26
homosexuality, 33, 36-37, 53
Hughes, P. E., 220, 222-23
humanism

borrowed capital, 38n

formula, 198

New Deal, xi

Westminster Seminary

&, xv

hybrids, 282, 291

idolatry, 132, 156-57, 189,
190-91

imports, 282, 288

incest, 161-62

incoherence, 198

“index,” 348-51

industrial revolution
agriculture &, 262, 264
England, 261-64
origins, 262, 268
population boom, 263-64
property rights, 262-63
spread of, 263

inflation, 65

inheritance
adoption, 275
authority &, 289
boundaries, 289
changes in, 259
eunuchs, 274-75
grace, 273
land, 273-74
Lev. 19:19, 269
priesthood &, 276
promise, 270, 277, 286

innovation, 264-65
