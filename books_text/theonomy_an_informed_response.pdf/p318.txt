298 THEONOMY: AN INFORMED RESPONSE

collect an arsenal of weapons to prove that only theonomists
did these harsh things (discipline). T:ARC became one of the
more formidable and widely used weapons, along with some of
the dispensational diatribes. What followed afterwards was a
church that was marked by dissension, struggle and suspicion,
and most of the conflict was blamed on theonomy.

In assessing and observing the conflict and misunderstand-
ings within the particular body (and others as well across the
country), the reaction to theonomy ranged from the ridiculous
to the slanderous. The one thing that stands out in the ap-
proach many have taken in confronting the issue of theonomy
is this: instead of going to primary sources, such as Theonomy in
Christian Ethics* or By This Standard® or Rushdoony’s The JInsti-
tutes of Biblical Law® or Gary DeMar’s fine work, The Debate over
Christian Reconstruction,’ the critics have relied solely on second-
ary sources, such as T:ARC. This seems to be endemic with the
modern-day opponents of theonomy. With this approach the
theonomist will never get a fair hearing. From my own experi-
ence, it is noteworthy that those who do read the primary
sources are surprised at what all the fuss is about and are excit-
ed to find that the Old Testament is not the Word of God
“emeritus.” From a moral perspective, there are some issues of
integrity, scholarship, and honesty at stake.

Needless to say, because I was the pastor, I have received the
brunt of much of the criticism from vocal critics within the
congregation due to my sympathies for the theonomic position.
The following is a sample of the things that were said to me
directly, or about me through the grapevine, and much of it

4. Greg L. Bahnsen, Theonomy in Christian Ethics (Nutley, New Jersey: Presbyteri-
an and Reformed, [1977] 1984).

5. Bahnsen, By This Standard.

6, Rousas John Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey:
Presbyterian and Reformed, 1973).

7. Gary Deman, The Debate Over Christian Reconstruction (Ft. Worth, Texas: Do-
minion Press, 1988).
