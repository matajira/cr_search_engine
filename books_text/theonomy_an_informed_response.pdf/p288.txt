268 THEONOMY: AN INFORMED RESPONSE

and waste which had previously contributed little to the output
of the village.”** What was required, in short, was the estab-
lishment of new boundaries.

These legal boundaries established the private ownership of,
and therefore personal responsibility over, the crucial means of
production in an agricultural society: specific units of land. The
fruits of one’s capital and labor inputs could be more easily
identified and claimed. This created economic incentives to
improve the land and to introduce new crops, including the
bleating crop known as sheep. Specialization of agricultural
production and the resulting increase in output per unit of
resource input increased both wealth and population in carly
modern England. This in turn led to the industrial revolution.
My point is that the increasing precision of the legal claims of
private owners of land, enforceable in civil courts, was the
crucial change that made possible the agricultural revolution.
The development of new crops and new breeds was the result,
not the cause, of that crucial revolution. In short, the new
boundaries — geographical but especially legal ~ led to greater
dominion. It was the transformation of legal relationships that
produced the transformation of the rural economy of England.

There was a perverse side of English inheritance: the cldest
son inherited all of the family’s land (primogeniture). Further-
more, except in cases of bankruptcy, this land could not be sold
(entail). The result was that younger sons were pushed into the
clergy, the military, or government service. (Not business: there
was a stigma attached to business.) In an economic sense, the
younger sons were “sacrificed” for the sake of the family’s
name: the survival of the property as a social force.

The only way for a landowner to get immediate access to
additional money out of this system, such as the money for a
daughter’s dowry, was to mortgage the property. Very long-
term mortgages became universal by the early years of the

36. Ashton, Industrial Revolution, p. 18.
