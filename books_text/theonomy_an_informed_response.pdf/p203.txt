Church Sanctions in the Epistle to the Hebrews 183

‘We may now personally and directly “come boldly to the
throne of grace, that we may obtain mercy and find grace to
help in time of need” (Heb. 4:16).

Hebrews 2:2

Let us consider some specifics of Johnson's presentation in
this regard:

Hebrews 2:2 affirms the justice of the Mosaic penal sanctions, as
a basis for the a fortiori argument that neglect of the great eschat-
ological Word of God, the word of salvation spoken through the
Lord himself, will justly bring even more severe punishment
than that meted out to Old Testament violators of the law. ‘Ev-
ery trespass of unwillingness to hear received a just reward’
(iteral trans.). For God, then, to prescribe the penalties of the
Mosaic Law for those members of Israel who disregarded his
covenant word, given through angels, was unquestionably a
display of his justice (p. 186),

We must keep in mind that an a fortiori argument from. the
lesser (capital punishment) to the greater (eternal condemna-
tion) in itself does not remove the lesser. Johnson knows this:
“The implied argument from lesser sin/punishment to greater
confirms that the law reveals God’s justice” (p. 178). Here is the
key question: Does the greater divine judgment for rejecting
the works of Jesus remove the lesser divine judgment for reject-
ing the words of the prophets (Matt. 11:24)? The theonomist
answers no. In another place, Jesus makes an a fortiori reference
to the fact that civil magistrates are called “gods” in the Old
Testament, so how much more the Messiah in the New Testa-
ment (John 10:34-36). Does the appearance of the Messiah
invalidate the authority of civil magistrates? Certainly not. In
fact, a fortiori arguments depend upon the validity of the lesser
argument in order to establish the greater. If the lesser is inval-
id, how can the greater be urged?
