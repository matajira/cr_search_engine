176 THEONOMY: AN INFORMED RESPONSE

In addition, we should note that in Hebrews 7:12, the writer
of Hebrews is dealing with the ceremonial Levitical system. It is
the temporally-conditioned ceremonial form of the Law that is
under scrutiny, not the moral or civil ethics of the Law (cf.
below; cf. Rom. 3:31; 7:12). This is evident from the immediate
context which contrasts Christ’s Melchizedekan priesthood with
the Aaronic priesthood and then notes: “. . . if perfection were
through the Levitical priesthood. . .” (Heb. 7:11). It also is
seen to be so on the basis of the explanatory gar (“for”) in the
following verse: “For He of whom these things are spoken
belongs to another tribe, from which no man has officiated at
the altar” (Heb. 7:13). Lenski comments: “What law is referred
to is evident; it is not the whole Mosaic law, but the laws per-
taining to this priesthood, gar being added in the confirmatory
sense of ‘indeed.’ "*° That is, according to the argument of the
writer of Hebrews, perfection was never attainable through the
Levitical system (cf. Heb. 10:1).” Furthermore, Jesus was not
from the tribe of Levi.

After all, it is only one aspect of the Law, the “preceding
commandment” (freagouses entoles [sg.], Heb. 7:18) regarding
“the Levitical priesthood” (Heb. 7:11), that is in view.”* It is in

  

28. This seems clearly directed to the Qumran doctrine that expected a purifica-
tion and re-establishment of the Aaronic system. See the Qumranian Manual of
Discipline for the Future Congregation of Israel.

26. R.C. H. Lenski, The Interpretation of the Epistle to the Hebrews and of the Epistle
of James (Columbus, OH: Lutheran Book Concern, 1938), p. 224. Westminster divine
William Gouge comments: “By law, is here in special meant the ceremonial law,
which was most proper to that priesthood, and which was most especially ahrogated
by Ghrist’s priesthood.” William Gouge, Commentary on. Hebrens: Exegetical and Exposito-
ry (Grand Rapids: Kregal, 1980 [1655)), p. 501.

27. The writer of Hebrews makes a strong case for the weakness of the priests
themselves in Heb. 5:3; 7:18, 24, 27-28; 8:1,

28. “The former commandment refers in particular to the legislation whereby the
levitical priesthood and its succession were regulated (vv. LL above.) “Our author's
primary concern, however, is with that part of the law (‘a former commandment’)
which prescribed and controlled the sacrificial system.” Hughes, Hebrews, pp. 264,
265. “It must be borne in mind through out that by the ‘commandment’ is meant the
ordinance which created the Levitical priesthood, not the Law in general.” W. F. Moulton,
