Editor's Introduction to Part IT 87

Conclusion

In this section, Bahnsen replies, first, to the entire Westmins-
ter faculty with respect to the issue of political pluralism — the
broader theological issue - and second, to Dennis Johnson's
essay on the question of civil sanctions in the New Covenant
era, Gentry also presents a pair of point-by-point responses to
Johnson. His first essay is more than a response to Johnson,
however; it is a refutation of the idea of the state as a non-cov-
enantal agency in the New Testament era. Gentry makes it very
clear: covenant-breakers cannot escape God’s judgment in
history. The question is: Who should administer God’s judgments in
history against those who violate His covenant law? Should it be a
covenant-keeping civil magistrate, or a covenant-breaking civil
magistrate, or some covenant-breaking military invader? Or will
God bring His sanctions directly, as He did against Ananias and
Saphira? The eschatological issue then becomes relevant: Will
there be a restoration of biblical civil justice after God’s judg-
ments in history are imposed? That is, are Isaiah 2:1-4; 4:4-6;
32; and 65:17-25 still covenantally operative today? If not, why
not? (Gentry deals with eschatology in Part III.)

In 1973, Bahnsen’s Th.M. thesis asserted continuity between
the Old Covenant civil magistrate’s authority and the New
Covenant civil magistrate’s authority. His thesis commitice
accepted it and awarded him the degree. This has been one
bone of contention between Bahnsen and certain members of
the faculty (as well as certain members of the Board) ever since.
The faculty waited 17 years to respond to Bahnsen in print, but
respond they did. In this, they became Clowney’s representative
mouthpieces, whether they admit this or not — and several do.

By asserting a discontinuity between the authority of the civil
magistrate in the Old Covenant and the authority of the civil
magistrate in the New Covenant, Dennis Johnson (who was a
theonomist until just before Westminster hired him)’ and his

9. Gf. Dennis Johnson, “Evolution and Modern Literature,” journal of Christian
