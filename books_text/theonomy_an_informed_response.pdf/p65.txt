Some Wings for Modern Calvinism’s Plane 45

Augustine believed that peace with God precedes peace in the
home, in society, and in the state. The earthly state too must be
converted, transformed into a Christian state by the permeation
of the kingdom of God within her, since true righteousness can
only be under the rule of Christ.

Not only in the realm of ethics and politics must conversion
take place . . . [but also] for knowledge and science. Apart from
Christ, man’s wisdom is but folly, because it begins with faith in
itselfand proclaims man’s autonomy. The redeemed man, on the
other hand, begins with faith and reason in subjection to the laws
placed in this universe by God: he learns to think God's thoughts
after him. All of science, fine art and technology, conventions of
dress and rank, coinage, measures and the like, all of these are
at the service of the redeemed man to transform them for the
service of his God.”

Van Til believed, along with Augustine, Calvin, Kuyper,”*
and Klaas Schilder - Christian scholars whose predestinarian
views are expounded in The Calvinistic Concept of Culture — that
the building of a Christian culture is a Christian imperative.
The Reconstructionists agree. Van Til castigated the Barthians
for their repudiation of a Christian culture. “For them,” he
wrote, “there is no single form of social, political, economic
order that is more in the spirit of the Gospel than another.”"*

Reconstructionists today are hearing a similar refrain from
both Reformed and dispensational theonomic critics. If there is
no specifically biblical blueprint, we are left with a pluralistic
blueprint (William S. Barker), no blueprint (John Muether), or
a postponed blueprint (dispensationalism). When we read that
“religious pluralism within a society is our Lord’s intention for

12. Henry R. Van Til, The Galvinistic Concept of Culture (Grand Rapids: Baker
Book House, 1959), p. 87.

13, Kuyper’s emphasis on common grace as “the foundation of culture” leads
one of his critics to write “that Kuyper can never really get special grace into the
picture.” Van Til, Calvinistic Concept of Culture, pp. 118, 119.

14, Ihid., p. 44.
