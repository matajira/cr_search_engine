Civil Sanctions in the New Testament 147
The Justness of Capital Sanctions

In the first place, we should notice that Jesus’ appeal to the
Mosaic Law regarding capital punishment for murder in Mat-
thew 5 is such that it assumes both ihe death penalty’s jusiness and its
validity. The Matthaen reference to capital punishment is im-
portant in the debate over penal sanctions for at least two rea-
sons:

(1) Contrary to the expectations of such writers as McCart-
ney," Christ does not appeal back to Genesis 9:6 for a justifi-
cation of capital punishment for murder. Neither does Jesus
here urge the repeal of this capital sanction (which would bring
Him into contradiction with Himself in Matt. 5:17-19), He no
more repeals the capital sanction for literal murder with His
statement in Matthew 5:21-22, than He repeals the divine pro-
hibition against physical adultery in verses 27 and 28. Both
remain binding as standing law from God.

(2) We must note that Christ appeals to this function of
God’s Law in the very context in which He makes reference to
its integrity for the era of “the kingdom of heaven” (Matt. 5:20).
This is significant because of the whole question of continuity/
discontinuity between the major redemptive eras (cf. Matt.
11:12-13; John 1:17), Yet in no way do His words express a
repeal of the sanction for murder, This is despite His speaking
in terms of the very fulfillment era He ushered in by His incar-
nation and suffering.

15. “The only sanction applicable is death, which is administered by God himself.
Perhaps the Noahaic covenant (Ge 9:1-17; ef. esp. vv. 9-10) applies to the world at
large. . . . This means that the only sanction required of all civil government by God’s
covenant with all mankind is the death penalty for murder.” McCartney, TRG, p.
147, By one stroke of the pen McCartney has bid good riddance to the moral obliga
tion of government to punish theft, rape, kidnapping, etc. In fact, he has removed
the civil government from all moral obligation to public sanctions, except in the onc
case of murder!
