Some Wings for Modern Calvinism’s Plane 49

own domain as State, it is bound to the Word of God as the
Church or the individual.” For Meeter, a “State is Christian”
when it uses “God’s Word as its guide.”*!

Meeter left the inquiring theonomist with additional ques-
tions: “If the Bible, then, is the ultimate criterion by which the
State must be guided in determining which laws it must admin-
ister, the question arises, with how much of the Bible must the
State concern itself?” He told us that “Civil law relates to
outward conduct.”* The inquiring theonomist is looking for
specifics, a methodology to determine which laws do apply to
the civil spherc. What “outward conduct” should the State
regulate? Sodomy and adultery are certainly “outward con-
duct.” (This is the legal issue of “victimless crimes.”)

Like Kuyper and Henry Van Til before him, Meeter, who
asserts that the Bible “is the ultimate criterion by which the
State must be guided in determining which laws it must admin-
ister” never set forth a biblical methodology. In fact, he never
quoted one passage of Scripture to defend his position, al-
though there are vague references to biblical ideals! Reading
Meeter was like reading an unfinished novel. The plane still
had no wings.

The Calvinistic Action Committee’s God-Centered Living

I next moved to a symposium produced by the Calvinistic
Action Committee: God-Centered Living. God-Centered Living
began with this noble goal: “This book seeks to be of help to
those who desire to know what the will of God is for the practi-
cal guidance of their lives in the complex relations and situa-
tions of our modern day.” The Committee encouraged the
reader with its intent not. simply to “theorize,” describing its
method as “a call to action” based on the “clarification and

21. Hid, p. 112.
22. Ibid., p. 128.
28. Mbid., p. 127.
