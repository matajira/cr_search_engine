A Pastor’s Response 301

ing were giving their counsel and advice to members of this
congregation. Never mind that the PCA has a system of church
courts to address issues of doctrinal error and church division.
What was it that so scared the critics?

5, One of the more amusing misconceptions was that I am “a
follower of a white-haired Indian guru in California.” [ assume
the proponent of this statement was referring to Mr. Rush-
doony. He is white-haired, but he is neither an Indian nor a
guru.’ The penchant for not finding out all the facts seems to
be another trait of those who are endeavoring to interact with
or critique theonomy. T-ARC contributes to this by not present-
ing the Theonomic system in a systematic fashion and thus
endeavoring to critique it piecemeal. Furthermore, in light of
the “tens of thousands of pages of written material in the past
two decades,”’° the quotations extracted from from the The-
onomic camp were paltry and selective. Many of the difficulties
raised by the authors of ARC have been addressed, as we
shall see in one example.

Residual Effects

The preceding statements were real live flesh and blood
responses to the issue of theonomy as it boiled over in the
church and as people tried to understand it by reading books
such as T-ARC, Thankfully, many of the misunderstandings
were remedied, but the leftover effects were still harmful.

One missionary supported by the church was almost denied
further support because of his Theonomic leanings, in spite of
the fact that he has a tremendous impact on the mission field
where he serves, he is faithful in evangelism (using Evangelism

9. Ironically, Rushdoony served as a missionary to the Western Shoshone tribe
in northern Nevada and southern Idaho in the 1940's and the early 1950's. He has
an adopted son who is an Indian.

10. Foomote in Tremper Longman II, “God’s Law and Mosaic Punishments
Today,” EARC, p. 41.
