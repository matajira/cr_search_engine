Davis, D. Clair, 310
day care, 338
deacons, 232
death, 227, 229
death penalty
anger?, 148-49
Jesus on, 147, 153
Johnson on, 127
law’s rejection, 185-86
Longman on, 114
death rate, 263
debate, 3, 86, 347, 349-54
decentralization, 273-74
defilement, 127-28
DeMar, Gary, 22-25
democracy, 36, 93
demolition, 17
demons, ix-xi
Dickens, Charles, xiv
dikes, 17
dinosaurs, 334
discipline, 297-98
discontinuity, 121, 143
discrimination, 252
disinheritance, 290
Disney World, xi
dispensationalism, 51, 140, 296
dispensationalism
Calvinism &, 38
establishment, xvii
hyper, 19
Kline’s, xx
theonomy &, 78
division of labor, 337n
dogma, 108
dominion, 268
Dooyeweerd, Herman, 7n, 41
doubletalk, 42

371

Drucker, Peter, 336

drug addict, 231-32
dualism, 84, 102, 132n, 260
dynastic marriages, 274

earnest, 199
earthquakes, 287n
Ebonites, 140
economics, 19, 20, 21
education, 94
elect, 253
election sermons, 357
enclosure movement, 267-69
England, 261-64
Enlightenment, 325-26
entrepreneurship, 265
epistemology, viii, xi
eschatology
Abraham’s promise, 279
ascension, 210-13
Christ’s comings, 201-2
conquest, 343
gradualism, 215-14
Jesus, 217n
motivation, 324-25
predestination &, 331-33
responsibility &, 200-1
eternal life, 269-70
ethics, 5, 17, 26
eunuch, 274-75
evangelism, 301-2
evolution, 72
executions, 317-18
excommunication, 187-88, 195,
280
export, 285
exports, 283, 288
eye for eye, 146
