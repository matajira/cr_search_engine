310 THEONOMY: AN INFORMED RESPONSE

But to Dr. Davis, I would express special gratitude for both
the content and tone of his article.* I found his to be the
most “pastoral” - if I may use an overworked term. He ex-
pressed sincere appreciation for the work of theonomists and
the positive challenge they can be to the church today, recog-
nized that “Doubtless Theonomists are not totally to blame for
the way they are perceived;””” and challenged the adherents
of theonomy to work within the bounds of the church,”* some-
thing which many are striving to do.

However, the misconceptions still abound. In the halls of
academia, maybe therc is a balanced understanding and appre-
ciation of the theological debate over the modern application of
God’s Law, But what is filtering down to the average Christian,
as I have observed it from a pastor’s perspective in not only the
congregation 1 served but in many others, is not balanced. Fel-
lowship is being broken, suspicions are being raised about godly
Christians, qualified and gifted men are being denied pulpits
because of their Theonomic leanings, reputations are being
unjustly hurt, serious, unsubstantiated, slanderous accusations
are being made, and fear is becoming the trait which character-
izes those who are grappling with Theonomic implications for
the first time,

Has T:ARC contributed to this state of affairs? For the well-
read Christian the answer would have to be no. For the believer
who has never even heard of theonomy, yes. Those who are
not familiar with the primary sources and who have not taken
the time to seriously read them are not getting either the whole
or proper picture of theonomy as it is portrayed by authors
such as those in T:ARC.

For the sake of the health of the church, I hope those within
Reformed circles will be more careful to represent theonomy

26. D. Clair Davis, “A Challenge to Theonomy,” LARC, pp. 380-402.
27. Ibid., p. 395.
28. Ibid, p. 394.
