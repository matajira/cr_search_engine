252 THEONOMY: AN INFORMED RESPONSE

based on a policy of unconditional welfare. Joseph rose to pow-
er by means of his solution to the famine problem of his day.
What was his solution? He gave to his own family (a covenant-
keeping condition), but he developed a program through Di-
vine inspiration whereby he sold the Egyptians food (a cove-
nant-breaking condition). He did not give away food to the
people of Egypt, even though he had persuaded the Pharaoh
to take it from them by force. He used their condition of near-
starvation to get these pagan Gentiles to surrender the owner-
ship of their livestock and their land. His program was not
conditionless. He in essence enslaved the Egyptians to the pow-
ers that were, of which he was the chief. The Gentiles were
legally enslaved to the Pharaoh, while Joseph’s family was given
the best of Egypt's land. This partially explains the later hatred
of the Jews by the Egyptians. Yet Dr. Keller calls this a welfare
system. Indeed it was: a state welfare system. Predictably, it led
to enslavement.

Free Lunches vs. Freedom

‘There is no such thing as a free lunch. Somebody pays be-
cause someone always absorbs the burden of the conditions.”

Let us assume for the sake of argument that Dr. Keller is
correct: an unconditional, compulsory, benevolent program by
the state is really sanctioned by the Bible. Does Dr. Keller be-
lieve that the state does not require any conditions for wealth
distribution? There is always discrimination in one form or
another, even though it may not be racial. There is no neutrality.
The state has to impose conditions, because anyone who gives
away anything, even if it means driving down a street and
throwing food out of the back of a truck, has to decide upon
what basis a particular street is chosen. He or his superiors
must make several other decisions, the main one being the

19, The obvious example is Jesus on the cross.
