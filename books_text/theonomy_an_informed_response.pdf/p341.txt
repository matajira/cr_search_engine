 

Editor’s Conclusion 321

Round Three

This book is the third in a three-stage response to Theonomy:
A Reformed Critique. First came my personal response: Westmins-
ter’s Confession. Then came Dr. Greg Bahnsen’s reply to 17 years
of criticisms, No Other Standard. Finally comes this collection of
theological responses.

Why did I write one, edit one, and publish all three? There
are several reasons, First and foremost, the ICE is a publishing
organization. Its job is to produce books. Westminster's faculty
gave theonomists one more opportunity to clarify our views; so,
we took three. We know that Christian Reconstruction is some-
limes rejected because of people’s confusion about our views, so
we are always happy to be given a legitimate opportunity to
explain the details of our position one more time (thrice).

Second, there is the question of tactics. In any movement,
there are followers who want to be sure that its leaders can
defend the system. This is especially true of an intellectual and.
ideologica] movement, which Christian Reconstruction is. There
are also potential recruits on the sidelines who are waiting to
see who gives the best account of himself. We would like to
recruit seminary students, and the Westminster faculty has
given us a tremendous opportunity to recruit Westminster
students. I am not one to look a gift horse in the mouth.

Then there is the tactical question of heading off additional
attacks. If potential critics see that the faculty of Westminster
Seminary did not succeed in damaging the theonomic position,
and in fact opened themsclves up to some serious questioning,
then it may not pay others to take us on in print. By taking on
“the best and the brightest,” and giving a credible account of
ourselves, we may be able to head off trouble from other critics
down the road. This seemed worth the extra effort: responding
with three books to Westminster’s one.

So much for tactics. For me, there is a third aspect of this
confrontation. I really do believe in what Cornelius Van Til
accomplished intellectually. I really do believe that Westminster
