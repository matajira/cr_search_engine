Hermeneutics and Leviticus 19:19 257

presumes that the separation of seeds and fabrics in Leviticus
19:19 has something to do with separating good and evil.)
“How do we decide how Leviticus 19:19 applies to us?”> This
is indeed the question. It is not just the theonomists’ God-given
assignment to answer it. It would have been helpful if Dr. Poy-
thress had suggested his solution to it. He avoided this task,

Greg Bahnsen says that this law no longer applies.’ He is
correct on this point, but Poythress is not persuaded by Bahn-
sen’s general explanation. Poythress cites Bahnsen: “We should
presume that Old Testament standing laws continue to be
morally binding in the New Testament, unless they are rescind-
ed or modified by further revelation.”* Poythress adds: “Strict,
wooden application of this principle would appear to imply the
continuation of Leviticus 19:19 in force.”* He notes in a foot-
note that Rushdoony argues that Leviticus 19:19 still applies,
thus making all hybrids immoral.’ Therefore, Poythress implies
(correctly), those theonomists who reject Rushdoony’s interpre-
tation of Leviticus 19:19 need to produce specific evidence of a
judicial discontinuity between the tcstaments that has annulled
the literal application of this law.

Poythress says that this law might be regarded as part of the
Old Covenant’s food laws and hence abolished. The Old Coven-
ant’s laws of separation no longer apply, Bahnsen says.* Poy-
thress asks: “But how do we tell in practice what counts as a
‘separation’ principle? How do we tell what elements in Mosaic
statutes are shadows and in what way are they shadows? How

3. Ibid., p. 104.

4. Poythress does not cite a source for this assertion.

5. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
‘Texas: Institute for Christian Economics, 1985), pp. 345-46.

6. Poythress, p. 106.

7. Ibid., p. 106n. He cites R. J. Rushdoony, The Institutes of Biblical Law (Nutley,
New Jersey: Craig Press, 1973), p. 285. For a detailed critique of Rushdoony's
argument, see Gary North, Boundaries and Dominion: The Political Economy of Leviticus,
forthcoming, Appendix E.

8. Bahnsen, By This Standard, p. 346.
