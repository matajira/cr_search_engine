384

Westminster Confession,

27n, 259-60, 351

‘Westminster Seminary

abortion, 33]
ambiguity, 90, 108
Bahnsen, 11
cease-fire, 10
Christendom, 9-10
confidence, 107-8
covenant, 82
covenant theology, 8
creationism, 82, 330
defensive mentality, 110
dogma, 108

God’s law, 96
hermeneutics, 7
history, 8

hit & run, 2, 11
humanism &, xv

in 1963, xi-xii
inept, 2, 3

itching, 3

Lutheran, 6, 86

THEONOMY;: AN INFORMED RESPONSE

Machen vs., 90, 96
natural law, 6, 333
Pandora, 3
political agnosticism, 96
sanctions, 8
ten commandments, 14
term papers, 2
theonomy, vs., 10-11
triumphalism, 106
wheat & tares, 104
widow, 248, 255
Williams, Roger, 84, 196
Winthrop, John, 358
witchcraft, 77
witnesses, 152n
Woolley, Paul, 331
worst case, 144-45

Yoder, John, 351n
Young, Davis, 330
Young, E. J., xii, xx, 329, 330

Zaccheus, 165n
