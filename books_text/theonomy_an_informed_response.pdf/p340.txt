320 THEONOMY: AN INFORMED RESPONSE

world. They have all adopted the opinion of my atheist libertar-
ian friend: God has no civil jurisdiction in New Covenant history.
When it comes to the civil sanctions of the Mosaic law, the
faculty members of Westminster Theological Seminary stand
shoulder to shoulder with every covenant-breaker on earth, and
with virtually all Christians, too. They announce publicly to
God: “You have no civil jurisdiction over us!”

Suggest for a moment that God still requires capital punish-
ment for anything except murder (and maybe even not except-
ing murder), and a wail goes up from the Christians. “Not
Jesus! Not our sweet Jesus! We're under grace, not law!” On
the contrary, the theonomists point out, we're visibly under
pagan politicians, bureaucrats, and lawyers.

Fundamentalists are accused by their critics as promoting a
religion of “pie in the sky, bye and bye.” On the whole, this
criticism is accurate. Except for the so-called positive confession
charismatics, fundamentalists do reject the notion of God’s
positive sanctions for covenant-keepers in history. So do non-
fundamentalist amillennialists, But in saying this, they also are
forced to conclude that there will be no predictable, corporate
negative sanctions imposed by God until judgment day. This is
why both groups get upset when theonomists say that civil gov-
ernments are still required by God to impose His specified
sanctions in enforcing [is revealed laws. They deny that God
brings corporate negative sanctions against societies that break
His revealed laws. Under the Old Covenant, critics admit, God
threatened to impose His sanctions directly if Israel failed to
enforce His laws. If pressed, they will even admit that this same
threat hung over Ninevch (Jonah 1-3). But every Christian
group except the theonomists insists that this system of corpor-
ate sanctions was abolished by Jesus. Therefore, they conclude,
there is no reason for Christians to seek to legislate God’s Old
Covenant laws, let alone those embarrassing civil sanctions.

They are wrong. God’s judgment in history is coming. Will
it be civil or more directly imposed?
