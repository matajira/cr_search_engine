Civil Sanctions in the New Testament 163

formula” of expulsion is applied in the New Testament to a
social sin that is defined as such in the Old Testament, there is
the clear expression of the continuity of God’s Law in the New
Covenant era. The New Testament Christians were under obliga-
tion to this Mosaic regulation under the threat of the church’s
greatest censure.

Conclusion

The rationale for Johnson’s treatment as investigated above
is explained in his statement that: “Ultimately the New Testa-
ment must be our guide in determining how the various cate-
gories of commandments in the Law of Moses function as God’s
authoritative Word to the postresurrection church” (p. 190).
We have weighed in the balance Johnson’s first stage of argu-
ment against the theonomic view of the Mosaic penal sanctions,
and have found it wanting. The New Testament does not disen-
gage the Mosaic penal sanctions. In fact, it assumes and applies
them.

There are serious objections to his presentation that under-
mine his conclusions, But this is only the first, preparatory
stage of his case. As the title of his chapter indicates, his prima-
ry argument will be made in his treatment of the Mosaic penal
sanctions as referenced in the Epistle to the Hebrews. We will
turn our attention to that matter in the next chapter.
