200 THEONOMY: AN INFORMED RESFONSE

formed Critique? Is it a threat to the peace of the church, either
because of the issucs it raises or the tactics of its adherents? (If
so, can a similar legitimate criticism be made against Martin
Luther, John Calvin, and John Knox?)

This raises a fundamental theological question: What is the
future of the church? If the church has a limited role in history,
then its responsibilities are minimal. If its responsibilities are
minimal, then it needs to reduce the level of confrontation with
the world. This is the belief of most critics of theonomy.

If the level of confrontation between the church and the
world is reduced, what happens to the level of confrontation
within the church? Some might argue that it will also be re-
duced. I disagree. The level of conflict within the church will
increase, If the world is inevitably going to hell in a handbasket,
then church members will spend their lives tearing up the
peace of the church over such matters as the proper color of
the carpet. Another fertile ground for church splits is the choir.
The debates that went on inside the Russian Orthodox Church
from 1900 to 1917 had little to do with the looming threat of
revolution or the rise of Bolshevism.” After 1917, the Russian
Orthodox Church was under continual siege. Its leaders ceased
debating over trivialities. They were too busy avoiding martyr-
dom — Stalin killed 80,000 priests - to worry about trivialitics.

If we want God to leave us in peace to do His work, we had
better not confine our ecclesiastical debates to trivialitics. This
means that Christians need to know what the church’s God-
assigned task is in history.

Eschatology

This leads us to the question of eschatology: What is the
church expected by God to accomplish before the return of
Jesus Christ, either to rapture His saints to heaven for a thou-

2. Ellen Myers, “Uncertain Srumpet: The Russian Orthodox Church and
Russian Religious ‘Thought, 1900-1917,” fowrmeal of Christian Reconstruction, XI (1985).
