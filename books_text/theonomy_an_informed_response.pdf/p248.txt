228 THEONOMY: AN INFORMED RESPONSE

ways remains a sinner saved by grace, who must struggle to
grow in grace and die to sin. Throughout the millennial cra,
God’s people on earth will remain in mortal, unresurrected
bodics and will need the sustaining grace of Christ. This is the
sort of thing Paul is discussing here in Philippians 3:10. Such
death to sin and self is eminently helpful in enduring overt
persecution, but it is not limited in its usefulness to the persecu-
tion environment.

Romans 8:17

Romans 8:17 reads: “. . . if so be that we suffer with him,
that we may be also glorified together.” Of this verse Gaffin
comments: “This correlation of future glory and present suffer-
ing is a prominent concern in the section that follows. At least
two points are worth noting about ‘our sufferings’ (v. 18): (1)
their nature/breadth and (2) their terminus” (i.c., the resurrec-
tion) (p. 213). (Due to pressures of space limitations, I will only
briefly respond to Gaffin's use of this passage.)

It is important to note that this passage is something of a
conclusion to Romans 6-7, And Romans chapters 6 and 7 are
dealing with the internal struggle of the Christian against in-
dwelling sin! The postmillennialist does not teach that there is
coming a day in which Christians will no longer have a sin
nature. As Murray notes on this verse: “Christian suffering
ought not to be conceived of too narrowly. In the passages so
far considered, and elsewhere in the New Testament (e.g., 2 Co
1:5-10; 1 Pe 4:12-19), suffering includes but is more than perse-
cution and martyrdom.”” Gaffin seems to be aware of this,
although not of its disutility to his anti-postmillennial argument,
where he writes: “suffering is everything that pertains to crea-
turely expericnce of this death-principle” (p. 214), and “suffer-
ing with Christ is the totality of existence ‘in the mortal body’

42. Johu Murray, The Epistle to the Romans (The New Intemational Commentary on
the New Testament) (Grand Rapids: Eerdmans, 1959), 1:213.
