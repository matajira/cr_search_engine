Civil Sanctions in the New Testament 143

$:16-17). But instead of repealing it, the New Testament con-
firms it (Matt. 5:17; Luke 16:17; Rom. 3:31; 1 Cor. 7:19; 9:21;
1 John 2:3-4; 5:3). This is because the Law is not contrary to
the promise of God (Gal. 3:21a). In other words, the argument
that urges discontinuity in Scripture requires proof; not that which
urges continuity (cf. John 10:35; 2 Tim. 3:16-17). This is what the
entire book of Hebrews is about: a presentation of the case for
the discontinuance of the ceremonial levitical ministry, i-e., “the
old covenant atonement structures” (p, 184).

In fact, even in the New Covenant era, it is the Law of God
that is written on the heart (Jer. 31:33; Heb. 8:8-10; 10:16).
Thus, in the New Testament itself, Law obedience expresses the
Golden Rule of social conduct (Matt. 7:12), defines the conduct
of love (Matt. 22:40; Rom. 13:10; Gal. 5:14; Jms. 2:8), promotes
spirituality (Rom. 7:14; 8:3-4; Heb. 8:10), and evidences holi-
ness, justice, and goodness (Rom. 2:13; 7:12, 16; 1 Tim. 1:8;
Heb, 2:2). The Law convicts of sin (Matt. 19:16-24; John 7:19;
Acts 7:53; Rom. 7:7; Jms. 2:9-11; 1 John 3:4) and restrains the
sinner (1 Tim. 1:8-10), because it is the standard of God’s Judg-
ment (Rom. 2:13-15; cf. Matt. 7:23; 13:41; Jms. 2:10-12), Con-
sequently, he who is not subject to the Law of God in the New
Covenant era is at enmity with God (Rom. 8:7). Thus, the Law
may randomly be cited by the apostles as confirmation of their
message, as it so often is, as Dan McCartney even admits (pp.
129-59).

And what shall we say to the following statement? “The
variety of ways in which the New Testament applies Old ‘Testa-
ment laws (e.g., 1Co 5:6-8; 9:9-10) should caution us against
presuming a particular understanding of continuity or disconti-
nuity as our starting point” (p. 175). Why? Why should such
caution be urged, when a specific God-ordained law is applied in
a deeper and fuller way? How can expanded application under-
mine original jurisdiction? How can greater depth of meaning
rule out the obvious surface meaning? Does the ocean have no
surface because it has great depth? As we shall see below, Jesus
