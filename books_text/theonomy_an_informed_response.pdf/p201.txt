Church Sanctions in the Epistle to the Hebrews 181

Johnson sees all Mosaic penal sanctions as designed only for
“the discipline and purity of the covenant community,” i.e. they
are intended directly for Israel in the Old Testament and indi-
rectly for the Church in the New Testament. Yet the Old Testa-
ment itself sets forth the Law as a model to the nations beyond
Israel (Deut. 4:5ff) that must be spoken before kings (Psa.
119:46; cf. 2:9ff). It is a “light” to the whole world (Isa. 51:4),
despite the fact the entire earth has transgressed it (Isa. 24:5).
Were not the Canaanites judged for its breach (Lev. 18:24-27;
Deut. 12:29-31)?" By it are not all the wicked condemned
(Psa. 119:118-119; Rom. 3:19)?

On Johnson’s view one is left to wonder how God could
judge the nations around Israel on their moral and civil fail-
ures, but never ceremonial failures, in the Old Testament.” Or
how the Ten Commandments could be obliged upon pagans,
since the Commandments begin with a distinct reference to
Israel’s redemption from pagan bondage (Exo. 20:1-3; Deut.
5:6-7). Are the Ten Commandments, then, expressly for the
covenant community?

Isaiah 24:5, referred to above, reads: “The earth is also
defiled under its inhabitants, because they have transgressed
the laws, changed the ordinance, broken the everlasting cove-
nant.” Westminster Seminary’s Old Testament scholar, the late
E, J. Young, explained this passage in such a way as to illustrate
the obligation of non-covenant pagans to God’s Law:

44. Craigie writes that the words of Deuteronomy 12:29-81 “not only function as
a warning to the Israclites, but they also present the religious justification for the
expulsion and extermination of the Canaanites. They were not to be dealt with
harshly simply at the Lord’s whim, nor out of sheer political necessity, but because
their life style, as reflected in their religion, had hecome repugnant to God, the
creator of all men. . . .” Peter C. Graigie, The Hook of Deuteronomy (Grand Rapids:
Eerdmans, 1976), pp. 219-20.

45. The prophets judged pagan nations for slave trade (Amos 1:6; cf. Exo. 21:16;
Deut. 24:7), witchcraft (Nah. 3:4; cf. Exo. 29:18; Lev. 19:21), loan pledge abuse (Hab.
2:6; cf. Exo. 22:25-27; Deut. 24:6ff), and so forth.

  
