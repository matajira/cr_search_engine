7

CHURCH SANCTIONS IN THE
EPISTLE TO THE HEBREWS

Kenneth L. Gentry, Jr.

The editors of Theonomy: A Reformed Critique write that John-
son shows “that in the Epistle to the Hebrews the sanctions are
cited to maintain the purity of the covenant community (rather
than the state)” (p. 125). We must examine in detail his major
supporting evidence for this argument.

As he continues his analysis of the New Testament usage of
the Mosaic penal sanctions, he contends that methodologically
“among the most significant of these are the passages in the
Epistle to the Hebrews that compare and contrast the Mosaic
penalties to the judgment awaiting those who repudiate the
new covenant inaugurated by Christ” (p. 178). He presents
what he considers the telling argument against the theonomic
ethic, which is that the theonomic employment of

the Mosaic penal sanctions overlooks the redemptive-historical
place assigned to them by the Epistle to the Hebrews. The pun-
ishments of the Mosaic Law belong clearly to the old order, and
thus they point ahead to the higher privilege and the resultant
higher accountability of the new covenant order established in
