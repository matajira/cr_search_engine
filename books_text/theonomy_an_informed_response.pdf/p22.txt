2 THEONOMY: AN INFORMED RESPONSE

the Westminster Theological Journal, W. Robert Godfrey, who was
co-editor of Theonemy: A Reformed Critique, had cut a sweetheart
deal with Kline: Bahnsen would not be allowed to reply to
Kline in the pages of the Westminster Theological Journal. But that
classic example of institutionally incestuous, hit-and-run book
reviewing did not sit well with the more academically inclined
and morally rigorous members of the Westminster faculty.
Besides, Kline is conspicuously absent from the pages of Theon-
omy: A Reformed Critique. (Now that I think of it, Kline has been
conspicuously absent from just about everything since about
1981.) So, the question remains: Why did they do it? Why did
they decide to take the time and trouble — insufficient trouble,
as it turned out — to produce their collection of embarrassingly
inept essays?

How inept? The level of academic performance that is exhib-
ited in the essays in Theonemy: A Reformed Critique ranges from
shoddy to mediocre. Compared to what most of these men have
written elsewhere, the essays in Theonomy: A Reformed Critique
have the appearance of exercises produced over three or four
Saturday afternoons, or perhaps during a spring break, not a
multi-year, supposedly co-ordinated effort to refute a serious
theological position. Their essays exude the easily identifiable
odor of late-night graduate school term papers: way too much
coffee, not enough research, and no prayer. Any reader who
wants to evaluate the truth of my accusation need only read
their original book, then read this critique or my book, West-
minster’s Confession,’ and finally read anything else written by
the same faculty members (except for John Muether). He will
then ask himself; What happened? And this question: Why?

These are the two questions I have been asking myself ever
since I first read their book. I still have no clear answers, only
suspicions. Given the fact that the Westminster faculty had

3. Gary North, Westminster's Confession: The Abandonment of Van Til’s Legacy (Tyler,
‘Texas: Institute for Christian Economics, 1991).
