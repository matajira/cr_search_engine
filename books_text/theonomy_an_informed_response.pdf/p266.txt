246 THEONOMY: AN INFORMED RESPONSE

Conditionality and Scripture

Dr, Keller objects to the way that I applied the concept of
conditionality to the area of benevolences. He agrees that con-
ditions should be applied after a person comes into the church,
such as is the case with the “widow indeed” and the Pauline
requirement of working before eating. What Dr. Keller objects
to is the application of conditionality before a person is saved,
because he thinks that this does not present a true picture of
the Gospel. To elaborate, he says that the Reformed doctrine of
salvation is unconditional first, proceeding to conditions after
entering the faith. In other words, justification is monergistic and
Sanctification is synergistic.

I generally agree with his statement regarding the Reformed
doctrine of salvation, but Dr. Keller is somewhat misleading
about the “monergism of justification.” To clarify my position,
I originally spoke to the area of the application of benevolence.
I made it clear that in application there should be conditions
met. Although I used the analogy of the unconditional and
conditional in reference to salvation, I was not saying that a
person has to be saved to be helped, nor was I saying that a
person has to be completely morally straight before the church
administers benevolence to him. I suggested some practical
qualifiers before helping people. But does the doctrine of salva-
tion mean that no conditions af all should be met by the able
person in need? In the final analysis, even Dr. Keller would
place “some” conditions on people before helping them. He
says: “Of course we should be on the lookout for fraud, and we
must not give aid naively in such a way that it is immediately
abused.”

On what basis does Dr. Keller make such a condition — one,
by the way, with which I would not disagree? Given his state-
ments, I am not sure, because he sounds as though he is so

18. Keller, Dheonomy, p. 277.
