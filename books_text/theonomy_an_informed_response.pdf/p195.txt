Church Sanctions in the Epistle to the Hebrews 175

the very foundation of the law given through Moses to Israel.”
And in a footnote he states that nomotheteo in Hebrews 7:12
means “to give law, establish legally” (p. 185). This argument
attempts to establish the entirety of the Mosaic Law upon the
temporal priestly system, which, when removed, would elimi-
nate the foundation of the entire Law.

But his interpretive argument is at best only a possibility. In
light of all the other pro-theonomic arguments, it is highly
unlikely. Interestingly, the Greek verb nomotheteo occurs only
twice in the New Testament, in Hebrews 7:11 and 8:6. Al-
though it does carry the meaning of “ordain, establish, enact,”
it also has another meaning: “to order a matter by law, to settle
legally.”*" In fact, two major New Testament lexicons even
distinguish these two uses in Hebrews.” For Hebrews 7:11,
the Theological Dictionary of the New Testament opts for the idea of
“to order a matter by law, to settle legally.”**

The meaning of nomotheieo in Hebrews 7:11 is that it was on
the basis of the Levitical priesthood that the people had the
Law authoritatively explained to them, i.e., when matters of dispute
arose. Jamieson, Faussett, and Brown’s Commentary suggests
Malachi 2:7 is behind this statement. Malachi 2:7 reads: “For
the lips of a priest should keep knowledge, and people should
seek the law from his mouth; for he is the messenger of the
Lorp of hosts” (Mal. 2:7). This idea is supported elsewhere in
the Old Testament, as well (Lev. 10:8-11; Num. 27:21; Deut.
17:8-11; Neh. 8:7).

21. W. Gutbrod, “nomos” in G. Kittel, ed., Theological Dictionary of the New Testa-
ment, trans. by Geoffrey W. Bromiley (Grand Rapids: Eerdmans, 1967), 4:1090.

22. Ibid, See also: W. F Arndt and EW. Gingrich, A Greek-English Lexicon of the
New Testament (Chicago: University of Chicaga Press, 1957), p. 844. Joseph Thayer,
A Greck-English Lexicon of the New Testament (New York: American Book, 1889), p.
427.

28. TDNT, 4:1090.

24, Robert Jamieson, A. R. Fausset, and David Brown, A Commentary, Critical and
Explanatory on the Old and New Testaments (Hartford: S. 8, Scranton, n.d.) 2:457.
