XX THEONOMY: AN INFORMED RESPONSE

The pluralists have justified this implicit break with Van Til
by substituting Kline’s theory of intrusionist ethics for Calvin’s
view of the covenant. Kline has justified his dispensational ap-
proach to the Mosaic covenant by arguing that Van Til’s view
of common grace leads to a uniquely New Testament concept
of @ common ethical and legal order which somehow unites coven-
ant-breakers and covenant-keepers without violating the pre-
suppositions of cither group. In other words, Kline has used
the worst aspect of Van Til’s thought, his view of common
grace,' to undermine the most important aspect of Van Til’s
legacy: his rejection of all common-ground philosophy and
ethics. Accompanying this sleight-of-hand operation has been
Kline’s view of Genesis 1, which substitutes the so-called frame-
work hypothesis” for a six-day succession of creation events. It
was this thesis that Edward J. Young challenged in his book,
Studies in Genesis 1 (1964), politely using Nic. H. Ridderbos as a
substitute for his colleague Kline. John R. Muether and the
other pluralist followers of Kline have applied Kline’s hermen-
cutic to political philosophy. They are being completely faithful
to their mentor’s rejection of the ideal of Christendom.

The theonomists refuse to go along with these sleight-of-
hand substitutions. We continue to defend Van Til’s basic doc-
trines: the Creator/creature distinction, the absolute sovereignty
of God, the Trinity as the ground of all secondary being and
reasoning, the self-attesting authority of Scripture, the impossi-
bility of neutrality, the illegitimacy of all natural law theory, and
the sole legitimacy of analogical reasoning (“thinking God’s
thoughts after Him”).

It is time for Westminster Seminary’s faculty to reaffirm
publicly a commitment to these doctrines. Then, member by
member, department by department, they need to apply them.

1. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987).

2. A literary framework supposedly governs the text of Genesis I: day one
parallels day four; day wo parallels day five; day three parallels day six.
