254 THEONOMY: AN INFORMED RESPONSE

Unconditionality is mythology — as mythological as judicial neu-
trality. It is the grand illusion of Pelagian and Arminian Chris-
tianity. It is the huge hoax of neo-evangelical political liberal-
ism: the politics of guilt and pity. The real issue for Dr. Keller
is the imposition of both economic and judicial conditions by
politicians and bureaucrats. I would rather be theologically
honest about conditions and live under Christ's.

Conclusion

On the afternoon of my completion of this article, the show-
case of socialism’s welfare state, Sweden, democratically decided
to scrap its legendary system. Why? Said one Swedish repre-
sentative: “Because the people are tired of being taxed fifty per
cent of their income.” Socialism has become economically obso-
lete. Sweden has begun to learn: charity has conditions.

Let the church turn again to the Law, the Prophets, and the
Gospels for a saner, more biblical, and much more long-lasting
theology of poverty — a view of Scripture where judicial condi-
tions are real, but so is Grace! This is why biblical charity must
discriminate between the deserving poor and the undeserving
poor. To do anything less is to deplete the resources of the
righteous and transfer them to the unrighteous. But we know
that the wealth of the sinner is laid up for the just, not the
other way around (Prov. 13:22b). Sweden has at long last begun
to grasp this principle of biblical justice. If we support those
who have become poor through their own immoral behavior,
then we are subsidizing evil. God does not call us to support
evil. Satan does, however; such charity expands his kingdom at
the expense of God's. We should resist his temptation.

There can never be unconditional charity in a world of
scarcity. To give charity to one person is to deny it to another.
There can be no neutrality. The myth of neutrality undergirds
the reality of the modern welfare state: compulsory wealth-
redistribution. As followers of Christ, we must reject the myth
of neutrality wherever we encounter it. This is Van Til’s legacy.
