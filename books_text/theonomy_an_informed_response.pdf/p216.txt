196 THEONOMY: AN INFORMED RESPONSE

theological traditions have steadfastly refused to answer, decade
after decade. Like Roger Williams, they wind up defending
some version of natural law theory and the civil authority of
general revelation over the Bible. To put it in the quaint lan-
guage of the Calvinist seminary, they relegate biblical law to an
aspect of the “historical-redemptive economy,” meaning the
Masaic economy, which was supposedly buried with, but not
resurrected with, Jesus Christ. That is, they treat biblical civil
law as if it were God’s law, emeritus.

This view of the authority of God’s civil law automatically
transfers to covenant-breakers the authority to name the stan-
dards of civil government and then enforce them by means of
whatever sanctions they can get covenant-breaking voters to
accept. And then, lo and behold, the Christians say, “We, too!”
(Yes, even when the pagans start murdering pre-born infants.)

The issue is very simple: “If the state refuses to protect God’s
church from those who would attack it, then no other agency is
empowered to.” Next: “If the state is not Christian, it is non-
Christian.” But as Van Til kept saying, to be non-Christian is
necessarily to be anti-Christian. There are only two categories,
he insisted: covenant-keeping and covenant-breaking, But his
spiritual heirs at Westminster Seminary no longer accept such
a harsh judicial distinction. Van Til was wrong, they tell us
today. There has to be a third category: covenant-neutral. If
there isn’t, then the theonomists are correct. Let it never be!

With the publication of Theonomy: A Reformed Critique, West-
minster Seminary has publicly abandoned Van Til’s legacy. The
amazing thing is, they refuse to say so clearly. This self-cons-
cious lack of clarity may have confused Westminster's donors, at
least temprarily; it should not confuse anyone else.

What Westminster’s Challenge to Theonomy Is All About

As we have seen in Gentry’s essays on Dennis Johnson, the
heart, mind, and soul of Westminster Seminary’s critique of
theonomy is the faculty's concentration on Jesus Christ's office
