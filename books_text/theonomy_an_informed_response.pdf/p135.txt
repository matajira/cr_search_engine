 

Westminster Seminary on Penology 115

Overlap of Opinion

What Longman has written in his article is not much of a
critique of the theonomic position, but on the whole a virtual
agreement with it, at least in basics. He, too, wants civic life
guided by divine revelation, not autonomy and arbitrariness.
He, too, thinks that “the time is ripe” for a change from the
prison system to the Old Testament requirement of restitution.
He writes that “the most significant contribution of theonomists,
however, is simply their pointing to the Bible as crucial to the
whole issue of just punishments... . There is deep wisdom and
necessary guidance to be found in the principles of law and
punishment contained in the Old Testament. . . . We can be
grateful to theonomy for forcing the church to take these issues
seriously” (pp. 41, 54).

Longman might believe that his differences with theonomic
penology are bigger than they really are because he entertains
certain misperceptions of the theonomic ouuook. For instance,
he repeatedly says that theonomists are loathe to admit any
kind of “subjectivity” whatsoever in the process of using God’s
law in the punishment of criminals (pp. 42, 49, 50, 51).* He
thinks theonomists are unwilling to consider “the mentally
deficient” or “minors” as ineligible for having the death penalty
applied to them (p. 44).5 But these things are just not so.

Longman seems to think that theonomists feel the applica-
tion of the Old Testament penal sanctions today is an easy and
simple matter, not difficult at all - just a matter of looking up
answers in a book, as it were (pp. 45, 48, 49, 50, 52). Those
who know me and my teaching know better; I have never

4, Sometimes Longman over-describes what theonomists allegedly fear as “sinful
subjectivity.” But of course nobody — Longman included — would welcome “sinful”
subjectivity into anything the Christian does! Moreover, I certainly do not believe, as
Longman portrays my view, that any “human subjectivity” amounts to “autonomy”!

 

5. For years I have taught such exceptions in carefully defined cases: eg.,
incompetence due to organic brain disorders; holding parents liable for behavior
which should have been governed in their young children.
