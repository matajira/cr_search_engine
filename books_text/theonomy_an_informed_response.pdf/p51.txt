Theonomy and Calvinism’s Judicial Theology 31

where theonomy is scorned, read to the critics the following
quotation from Hodge. Of course, don’t tell them the source of
the quotation until you get their response:

It is our duty, as far as lics in our power, immediately to
organize human society and all its institutions and organs upon
a distinctively Christian basis. Indifference or impartiality here
between the law of the kingdom and the law of the world, or of
its prince, the devil, is utter treason to the King of Righteous-
ness. The Bible, the great statute-book of the kingdom, explicitly
lays down principles which, when candidly applied, will regulate
the action of every human being in all relations. There can be no
compromise. The King said, with regard to all descriptions of
moral agents in all spheres of activity, “He that is not with me is
against me.” If the national life in general is organized upon
non-Christian principles, the churches which are embraced
within the universal assimilating power of that nation will not
long be able to preserve their integrity.'*

Hodge called the Bible the “great statute-book of the king-
dom.” In effect, he was a “biblicist” who believed the Bible
should be used as a textbook on social theory. But Muether
tells us that using the Bible as a textbook is the essence of fun-
damentalism, not of Reformed theology.”* Muether’s battle is
now with A. A. Hodge. It is a mismatched fight.

The Lure of Pluralism

What replacements for the firm foundation of a biblical
worldview are being offered by today's Calvinist theologians as
the essence of Reformed theology? Appeals are being made to
natural law, general revelation, and common grace as seemingly
full, independent, and reliable standards of ethical inquiry. The
Bible appears to have become only one ethical standard among

18, Ibid. pp. 283-84,
19. Muether, “The Theonomic Attraction,” p. 283.
