Westminster Seminary on Pluralism 105

as much as possible the darkness of our rebellious world, preach-
ing the good news of salvation and persuading men to submit
to the commandments of the Lord. Indeed, it is precisely a
“lawful use” of God’s law to let its standard restrain and punish
the criminal activities of men in socicty (1 Tim. 1:8-10), while
we all await the return of the Lord from heaven in flaming fire
upon all who practice lawlessness (2 hess. 1:7-9; Jude 14-15).

In the mean time, Christ does not expect Tis followers to
prefer more darkness to less, but to reform their societies as
much as possible in anticipation of the world which is coming.
It seems that this theonomic attitude should be endorsed by
adherents of cach of the millennial views, not simply postmil-
lennialists. The moral requirement to- seek a civil government
which honors the just requirements of God is not diminished in
the slightest even if the postmillennial confidence that this kind
of government will one day prevail in the world proves unwar-
ranted by Scripture.

In particular I feel that the article by Dr. Richard Gaffin in
the. Westminster volume unfairly portrays the theonomic
position as forgetting the theology of the cross and — in “trium-
phalist” notes of progress or victory — obscures or removes the
constitutive dimension of suffering from the present triumph of
the church. Is there anything at all in the tenets of theonomic
ethics which would require it to obscure the suffering of the
individual believer or the church in this age? The answer is
quite simply no. The question is not whether the people of God
shall suffer in this age (even when the ruling powers are more
favorable to a biblical perspective). The questions are rather
these: (1) according to the Bible, do our inevitable sufferings
issue in greater or lesser manifestation of Christ's saving rule on
earth, breaking the power of sin, and (2) should our inevitable
sufferings as obedient followers of the Messiah deter us from

25. “Theonomy and Eschatology: Reflections on Postmillennialism,” ibid., ch. 9,
pp. 210-18 (esp. 216-17).
