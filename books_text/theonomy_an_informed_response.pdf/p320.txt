300 THEONOMY: AN INFORMED RESPONSE

(II Corinthians 10:3-6; Ephesians 6:10-18). The Kingdom does
not expand through the use of raw brutal force as typified by
the Muslims. Neither is a society that has been Christianized
and has as the foundation of its law God’s Law an oppressive
Khomeni-type regime. The Law is a law of diberty (James 1:25).

3. A rather severe criticism that came out of the woodwork
was that “yes, John preaches salvation by grace, but because he
is a theonomist, he must be a liar.” Has the reaction (or overre-
action) to theonomy become so heated and fearful that things
like this have to be said to refute the position? Serious damage
is done to reputations with this line of reasoning.

4, “Theonomists are the ones who cause the trouble when
the issue comes to the forefront in a church.” This statement
was relayed to me several times in the controversy here and I
have heard it said in other circles as well. In T:ARC, this is
implied when the statement is made that former students who
are now pastors come to the authors “because of the disruption
of their churches over a sincere layperson’s zeal for theonomy
as the one true understanding of the Scripture’s teaching.”
The impression is given that the theonomists are the ones
behind all the trouble, No doubt there have been theonomists
who have upset churches by not presenting their position in a
gracious or patient manner, but there have been many situa-
tions where the non-theonomists have stirred the pot to boiling,
and my situation was one. There are churches and pastors that
take a strong Theonomic stance and are well balanced in their
overall teaching. Theonomy per se is not the cause behind the
disruption in churches. How people respond to it and portray
it is. Morcover, when the controversy became rather intense,
certain members of the congregation who were “deeply con-
cerned” at how “divisive” cheonomy was sought out the advice
of outside “authorities.” Theological celebrities and personalities
as diverse as S. Lewis Johnson, John Frame, and David Hock-

8. Preface, T:ARC, p. 10.
