332 THEONOMY: AN INFORMED RESPONSE

their own making, in which there is no visible opponent.
Thus, the amillennial Calvinist scholar’s idea of a battle for the
minds of men is a debate over the Alexandrian vs. the Antio-
chan basis of Barth’s theology.” No one cares who wins it.
This is why Rushdoony’s books have always scared academic
Calvinists, even Van Til. After the publication of Intellectual
Schizophrenia (1961), the editors of the Westminster Theological
Journal decided not to review his books. He was taking Van
Til's comprehensive critique of all non-Christian thought and —
horror of horrors — attacking the public school system, even
including higher education. Westminster Seminary’s professors
are required to attend graduate school, which means either a
secular humanist university or the Free University of Amster-
dam, which in 1961 was rapidly becoming a kind of baptized
humanist university. Rushdoony had become far too hot a
potato for Westminster Seminary to handle. He actually be-
lieved that we need to apply Van Til’s philosophical critique of
humanism to the institutions and practices of humanism! *
Well, this sort of thing sounded like theocracy to them. And
we all know what James Madison thought about theocracy! So
the blackout on Rushdoony’s books began, even though Rush-
doony publicly defended Madison’s political theory.** The
faculty suspected that an unspoken evil lurked within his com-
prehensive, Vantillian critique of humanism: a denial of politi-
cal pluralism. His /ustitutes of Biblical Law (1973) intensified this
suspicion, and Bahnsen’s Th.M. thesis (1973) persuaded them
of its reality. It took 17 years of simmering for the faculty’s

24, This may be why Westminster waited 17 years to criticize theonomy in print.

25. In the Fall, 1990, issue of the Westminster Theological Journal, the quarter in
which Theonomy: A Reformed Critique appeared, there were essays on the following
topics: (1) Was Barth’s theology Alexandrian or Antiochan? (2) What was Barth’s
interpretation of Schleiermacher? (8) What was the echo narrative technique in
Judges 19? (4) How were the Isaianic servant songs used in the missiology of Acts?
(5) how do we solve the unidentifiable interlocutor problem of James 2:18?

26. R. J. Rushdoony, The Nature of the American System (Fairfax, Virginia: Tho-
burn Press, [1965] 1978), pp. 72-75.
