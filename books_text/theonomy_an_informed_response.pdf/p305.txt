Hermeneutics and Leviticus 19:19 285

sheep.™ Wool tends to retain human sweat on the wearer’s
body. So, wool and linen in the same garment were at cross-
purposes biologically — and, as we shall see, ritually.

Clothing covers a person. This is symbolic of God’s judicial
covering of Adam and Eve. They wanted a covering of the field
(fig leaves); God required a covering from a slain animal. This
means that to mix wool and linen was to mix ritual opposites.
The wearing of such a mixture was symbolic of the mixing of priests
and non-priests. It was all right for a nation of non-priests to
wear such a mixture; it was prohibited to a nation of priests.
This is why the export of this cloth was not prohibited. The
recipient nations had no priestly status in God’s covenant, and
hence the mixture would have no ritual meaning.” God did
not threaten non-priestly nations with negative sanctions if they
violated some ritual requirement for priests in Israel. Their
sacraments had no power to invoke God's sanctions, positive or
negative. Had some group or nation been circumcised under
God, then these clothing requirements would have applied.

Inside a priestly nation, such a mixture was a threat to the
holiness of the priests when they brought sacrifices before God.
As between a priestly nation and a non-priestly nation, this
section of Leviticus 19:19 symbolized the national separation of
believers from unbelievers. Deuteronomy 22:11 is the parallel
passage: “Thou shalt not wear a garment of divers sorts: [as] of
wool and linen together.” Its immediate context is another case
law, one which we know from Paul’s epistle to the Corinthians
refers to people, not just animals: “Thou shalt not plow an ox
and an ass together” (Deut. 22:10). Paul wrote: “Be ye not
unequally yoked with unbelievers: for what fellowship hath
righteousness with unrighteousness? and what communion hath
light with darkness?” (IT Cor. 6:14). It is legitimate to apply the

51. “Wool,” Software Toolworks illustrated Encyclopedia (Grolier Encyclopedia) (1990).
52, That is to say, the sacramental sanctions were absent.
