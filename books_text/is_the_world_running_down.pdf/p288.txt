252 IS THE WORLD RUNNING DOWN?

The Self-Conscious Defection of Pietists

Today’s pietistic Protestants, with their emphasis on internal self-
transformation to the exclusion of social responsibility, resent such
teaching. There is enormous hostility to the idea that adhering to
God’s social laws brings external prosperity. Both the pietists and the
Christian socialists refuse to consider such a proposition. If the
proposition is true, then it places great responsibility on Christians
to begin to rebuild Western civilization by reconstructing every in-
stitution in terms of biblical law. But Christians are embarrassed by
biblical law. They are embarrassed by the God of the Bible who has
imposed biblical law on His people, and who holds everyone respon-
sible for obeying it.2? Outraged by such a view of God, they stick
their fists figuratively in the face of God, deliberately misinterpret
both God and His law, and shout their defiance: “Is God really noth-
ing more than the abstract, impersonal dispenser of equally abstract
and impersonal laws?”49 For this reason, Christianity today ts culturally
impotent and trrelevant. Christians are not in positions of leadership in
any major social or political institution. They are fed by the scraps of
power that fall from the humanists’ tables.

Where are the Christian orphanages? I am not referring to Chris-
tian orphanages operated by Christians in Korea for some other for-
eign land. Where are the orphanages run by Christians in their own
nations? Where are the Christian homes for the retarded? Where are
the Christian schools for the deaf or blind? There are almost none.
Why not? Because there is no tithing. Because there is no vision of a
Christian social order. Because there is a futile faith in neutrality,
Christians assume that the State has the right to educate the deaf and
blind. They assume that education is essentially technical, and that as
long as a competent instructor is located and financed by taxes, the
handicapped children will receive all the education they need, irre-
spective of the theology of the technically competent instructor. In
some perverse way, Christians assume that all that the deaf and blind
kids need is the ability to read and write — the same preposterous error
that enables the humanists to gain continuing support from Christian
taxpayers for the humanist-controlled government school system. Their
physical handicaps become an excuse for their theological neglect.

39, Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
Texas: Institote for Christian Economics, 1985).
40. Rodney Clapp, ‘Democracy as Heresy? Christianity Teday (Feb. 20, 1987), p. 23.
