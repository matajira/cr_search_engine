Comprehensive Redemption; A Theology for Social Action 243

repenting from particular sins. We do not sin in general without sinning in
particular. If men are to stop sinning, then they need to know which
actions constitute sin before God. They need standards of moral be-
havior. Without a comprehensive law structure, men cannot know what
God expects them to do. They also cannot know what God expects
them to refrain from doing.

Overcoming Corruption

When the institutions of society have been corrupted — corrupted
in specific ways by specific individuals~ then they need to be reformed
by godly men who are reconstructing social institutions in terms of
God's revelation of His standards in His law. It is not enough to see
men regenerated. When they are regenerated, they must ask them-
selves: What things did I do before that were wrong, and what must
I do differently to have my work acceptable to God? Unless these
questions are asked and subsequently answered, the fruit of men’s
regeneration will be minimal. In some cases, it may continue to be
evil. For example, what if some persecutor in the Soviet Union were
converted to Christ? Would he be fulfilling God’s law by becoming
an even more efficient persecutor of God’s people? No; he would
have to get out of that calling. There is a book written about just
such a convert, Sergei Kourdakov’s The Persecutor (1973). He defected
to the West, wrote the book and was murdered by Soviet agents (or
so the evidence indicates). It is not enough, then, to call for men to
turn to Christ. They must also turn away from Satan and all of Satan’s
works.

When a society is so at ease in Zion, when men and women no
longer concern themselves about the specific nature of their sins,
when social institutions are ignored as being beyond the scope of
God’s law, when preaching is no longer geared to helping specific
Christians reform every area of life for which they are morally re-
sponsible, when leaders no longer read the Bible as a source of
guidance in concrete decisions based on concrete laws in the Bible,
when Christians no longer have faith in the long-term success of the
gospel, in every area of life, in time and on earth, then the judgment
of God is at hand. Then they must be awakened from their slum-
bers. When the steady preaching of God’s law, week by week, insti-
tution by institution, is no longer present in a society that was once

35. dbid., ch. 3.
