The Unnaturalness of Scientific Natural Law 23

‘I Observe; Therefore You Are”

The most widely shared of the eight views is the second: reality is
created by observation. Herbert says that this is the second aspect of
the vision offered by the Copenhagen physicists, led by Niels Bohr
and Werner Heisenberg. “Although the numerous physicists of the
Copenhagen school do not believe in deep reality, they do assert the
existence of phenomenal reality. What we see is undoubtedly real, they
say, but these phenomena are not really there in the absence of an
observation. The Copenhagen interpretation properly consists of
two distinct parts: 1, There is no reality in the absence of observa-
tion; 2. Observation creates reality. “You create your own reality,’ is
the theme of Fred Wolfe’s Taking the Quantum Leap.”** Herbert goes
on to explain what all this means. (Note that he uses “she” as the per-
sonal pronoun when he refers to physicists~a sign of the cultural
times, since not a single female physicist is referred to in his book.)

What's at stake in the quantum reality question is not the actual exist-
ence of electrons but the manner in which electrons possess their major attributes.
Classical physicists imagined that every particle possessed at each moment
a definite position and momentum; each field likewise possessed a particu-
Jar field strength at every location. If we agree to call any entity — particle,
field, apple, or galaxy — which possesses its attributes innately an “ordinary
object” then the fundamental message of classical physics was this: the en-
tire physical world consists of nothing but ordinary objects.

Quantum theory suggests, on the other hand, that the world is nof made
of ordinary objects. An electron, and every other quantum entity, does not
possess all its attributes innately, An electron does possess certain innate
attributes— mass, change, and spin, for instance—which serve to distin-
guish it from other kinds of quantum entities. The value of these attributes
is the same for every electron under all measurement conditions. With
respect to these particular attributes, even the electron behaves like an or-
dinary object.

However, all other attributes, most notably position and momentum,
which, it was thought, classical particles possessed innately, can no longer
be attached to the electron without qualification, These attributes — called
“dynamic” to distinguish from the “static” attributes mass, change, and
spin— do not belong to the electron itself, but seem to be created in part by
the electron’s measurement context. The fact of the matter is that nobody
really knows these days how an electron, or any other quantum entity, ac-
tually possesses its dynamic attributes.

24, Herbert, Quantum Reality, p. 17.
