7
MYSTICISM VS. CHRISTIANITY

On the one hand, the days, times present, are evil; on the other hand,
these days must be eagerly redeemed as a season of great value and mean-
ing. The contrast is a dramatic one. Instead of flight from evil days, there
1s an eager purchase or redemption of them as a time or season of great
profit and advantage under God. . . . For this reason, because the godly
man’s concern is to redeem the time, Christians, especially Purttans, have
been highly conscious of time and the clock. As a valuable commodity,
time cannot be wasted. This horror of wasting time is alien to those out-
side the world of Biblical faith. Consciousness of time is for the ungodly a
consciousness of decay and death... .
R. J. Rushdoony!

Christianity is a religion of ethical restoration. It preaches Christ
and Him crucified, The perfect humanity of Christ is declared by
God to be the possession of the redeemed person. Instead of looking
at the sins of each Christian, God looks at Christ’s perfection.
Christ’s perfect humanity is imputed to redeemed men, just as Adam's
sin is imputed to all men until they become regenerated through
God’s grace (Rom, 5:19).? It is this alone which enables man to
escape from the curse of God’s final judgment.

Understand that it is Christ’s perfect humanity which is imputed
(judicially transferred) to man by God at the point of the person’s
regeneration, The divinity of Christ remains His possession alone.
God does not share His divinity with man. Man does not become God,
either by science, or techniques of meditation, or magic, or self-
discipline. Christ was both God and man from the beginning. He

1. R. J. Rushdoony, Revolt Against Maturity: A Biblical Psychology of Man (Fairfax,
Virginia: Thoburn Press, 1977}, p. 230.

2, John Murray, The /mputation of Adam’s Sin (Nutley, New Jersey: Presbyterian &
Reformed, 1977).

134
