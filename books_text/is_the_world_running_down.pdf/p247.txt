The End of Ilusions: “Creationism” in the Public Schools 211

capturing the United States. Those who would subsequently seek to
control taxpayer-financed schools would be compelled by state and
federal law to operate in terms of humanism’s worldview, the myth
of religious neutrality. By law, humanists would automatically gain
control of American public education. They fully understood that if
they could take over the public schools, they could then use their
control over the schools to capture every human institution, for they
would shape the thinking of succeeding generations of students.

The humanists’ goal has always been conquest. They seek the
establishment of the kingdom of man. The early progressive educat-
ors were self-conscious in their strategic planning. This is why R. J.
Rushdoony called his 1963 study of the founders of modern progres-
sive education, The Messianic Character of American Education.” Stephen
Jay Gould, Harvard’s paleontologist and Scientific American colum-
nist, has summarized the history of Creation Science’s attempt to get
Creatorless creationism into the public schools:

The legal battle over teaching evolution in public schools has passed
through three broad phases. The laws of the Scopes era simply barred the
teaching of evolution outright. The Supreme Court finally struck down this
strategy in 1968, but only after these laws had enjoyed 40 years of effective-
ness in muzzling the presentation of evolution in public schools.

Since they could no longer ban evolution, fundamentalists then adopted
a new strategy of legislating equal time for teaching the sectarian, literal in-
terpretation of the Book of Genesis that they call “creationism.” The initial
“equal time” laws were, at least, honest in the sense that they properly iden-
tified creationism as a religious alternative to evolution, These statutes were
soon struck down.

The phase-two defeats restricted the legal options of fundamentalists to
a third strategy. They invented a bogus subject called “scientific creation-
ism”—simply the old wolf of Genesis literalism, lightly clothed in a woolly
patina of supposed empirical verification.

But a new name couldn't hide the unaltered content and context, Only
Arkansas and Louisiana passed laws mandating equal time for “evolution
science” and “creation science.” The Arkansas law was struck down after a
full trial that included my own testimony as a witness. Federal District
Court Judge Adrian Duplantier then struck down the Louisiana law with-
out trial. The state appealed to the United States Supreme Court.

On June 19 [1987], the Supreme Court declared the Louisiana act un-
constitutional. Justice Byron White, in his one-page concurrence with the

7. Philadelphia: Presbyterian and Reformed.
