Dominion and Sanctification 161

Dominion requires the mastery of every area of life in terms of
God’s revealed laws. This in turn requires faithful preaching of the
comprehensive effects of God’s redemption. Christ bought back
everything when He sacrificed Himself. What dominion produces is
order and growth, as well as orderly growth.

When God brings judgment on rebellious societies, He brings
disorder and stagnation. The modern no-growth humanists are
proclaiming a gospel of stagnation. They want order—a top-down,
centrally planned order—but they do not want growth. The very
complexity of a modern growing economy threatens their ability to
promote State-directed order. Thus, their ideology is hostile to
growth of most kinds.

God says that such a view of His kingdom is evil, although it is
appropriate to view Satan’s kingdom in this way. To promote a zero-
growth philosophy is to promote historical stalemate —a stalemate
between God’s kingdom and Satan’s, between growth and decay, be-
tween good and evil. Satan wants a stalemate if he cannot get a vic-
tory. God will not allow Satan a stalemate.

Long-term economic growth is a product of God’s grace in
response to covenantal faithfulness, itself a gift from God. Long-
term economic growth is therefore a denial of stalemated kingdoms.
It is a demonstration of God’s victory over Satan, creativity over
destruction, ethics over power.

In summary:

1. The state of grace is a state of growth.
2, Humanism offers a graveyard society in place of Christianity.
3. The Hebrews in Egypt experienced population growth.
4. God drove the Canaanites out of Canaan slowly, so the
animals would not take over Canaan in the meantime,
5, Better rule by God’s enemies than dominion by animals.
6. God’s people must be patient.
7, They are to establish their rule by their faithfulness to biblical
law: dominion through ethics.
8. God promises external blessings to covenantally faithful
societies.
9, Ethics, not entropy, is normative.
10. Nature is not normative; the Bible is,
11. History is linear.
12. The Hebrews failed ethically; therefore, they did not exper-
ience world-transforming growth.
