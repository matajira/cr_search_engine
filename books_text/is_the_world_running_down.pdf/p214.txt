178 IS THE WORLD RUNNING DOWN?

superstructure that is built on top of the true structure of society, the
mode of production. Marx wrote in the Communist Manifesto (1848):
“What else does the history of ideas prove, than that intellectual pro-
duction changes its character in proportion as material production is
changed? The ruling ideas of each age have ever been the ideas of its
ruling class.” Again, “The mode of production in material life
determines the general character of the social, political and spiritual
processes of life.” Lo and behold, we find that The Emerging Order is
divided into two sections, He titles Section I: “The Great Economic
Transformation.” He titles Section II, “The Religious Response.”

Rifkin scores initial points with conservative Christians by at-
tacking liberalism. Liberalism is a dying philosophy in our day, he
says. But his reason for saying this is based on his philosophy of eco-
nomic determinism (one of twentieth-century liberalism’s most precious
creeds, taken from Marx): “The twilight of liberalism is upon us
because the basis for liberal society no longer exists. Liberalism is
founded on one overriding precondition — the possibility of untimited
economic growth.” 8

Here it is again, precondition. It is specifically an economic precon-
dition. You know, as in “economic substructure” or “mode of produc-
tion.” But historically, the argument is wrong. Classical liberalism
preceded modern economic growth. It began to be formulated in the
seventeenth century, especially by John Locke toward the end of the
century. The Scottish Enlightenment—Adam Fergusson, David
Hume, Adam Smith, etc. —- developed the premises of classical liber-
alism during the first three-quarters of the eighteenth century. Then,
around 1780—ajfter the fundamental tenets of classical liberalism
were in place—the Industrial Revolution in Britain began. There
was feedback between the ideology and the growing economy, just as
there will always be feedback between covenantal law, covenantal
faithfulness, and covenantal blessings (Deut. 8:18). This hardly
means that economic growth was dependent on the economic pre-
condition of economic growth. If anything, it was the growing accep-
tance of the economic recommendations of classical liberalism that made possible
the West’s economic growth.

11. Karl Marx and Frederick Engels, “The Manifesto of the Communist Party”
(1848), in Selected Works, 3 vols. (Moscow: Progress Publishers, [1969] 1977), I, p. 125.

12. Marx, “Preface,” 4 Contribution to the Critique of Political Economy (New York: In-
ternational Library, [1859] 1904), p. 11.

13, Rifkin, Emerging Order, p. 7.
