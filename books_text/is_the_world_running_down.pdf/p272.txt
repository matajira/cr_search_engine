236 IS THE WORLD RUNNING DOWN?

have a right to be critical of errors made by those who lack relevant knowl-
edge. Disagreements of this sort can also be corrected. Inequality in know!-
edge is always curable by instruction.

In other words, I am saying that all human disagreements can be
resolved by the removal of misunderstanding or of ignorance. Both cures
are always possible, though sometimes difficult. Hence the man who, at
any stage of a conversation, disagrees, should at east hope to reach agree-
ment in the end. He should be as much prepared to have his own mind
changed as seek to change the mind of another, He should always keep
before him the possibility that he misunderstands or that he is ignorant on
some point. No one who looks upon disagreement as an occasion for teach-
ing another should forget that it is also an occasion for being taught.

But the trouble is that many people regard disagreement as unrelated to
either teaching or being taught. They think that everything is just a matter
of opinion. I have mine. You have yours. Our right to our opinions is as in-
violable as our right to private property. On such a view, communication
cannot be profitable if the profit to be gained is an increase in knowledge.
Conversation is hardly better than a ping-pong game of opposed opinions,
a game in which no one keeps score, no one wins, and everyone is satisfied
because he ends up holding the same opinions he started with.

I cannot take this view, I think that knowledge can be communicated
and that discussion can result in learning. If knowledge, not opinion, is at
stake, then either disagreements are apparent only—to be removed by
coming to terms and a meeting of minds; or, if they arc real, then the gen-
uine issues can always be resolved —in the long run, of course — by appeals
to fact and reason. The maxim of rationality concerning disagreements is to
be patient for the Jong run. I am saying, in short, that disagreements are
arguable matters. And argument is both empty and vicious unless it is
undertaken on the supposition that there is attainable truth which, when at-
tained by reason in the light of all the relevant evidence, resolves the
original issues.”

Very few serious scholars really believe this any longer. They
may do their best to make their arguments coherent, but when pressed,
they really do wind up arguing that everything is simply a matter of
individual opinion, individual prejudice, or individual class posi-
tion. Marx believed that all philosophy is a class weapon used by the
ruling social class to subjugate another.”* Everything for Marx was a

27. Mortimer Adler, How to Read a Book: The Art of Getting a Liberal Education (New
York: Simon & Schuster, [1940] 1967), pp. 246-48.

28. “Just as philosophy finds its material weapons in the proletariat, so the prole-
tarial finds its intellectual weapons in philosophy.” Marx, “Contribution to the Critique
