The Unnaturalness of Scientific Natural Law 27

world.*! Nothing happened. Nevertheless, Isaac Asimov wrote the
forward to the book, so Gribbin was at least within the “pop science”
fraternity in 1974, and was not cast out after 1982.

Gribbin is a defender of Copenhagen’s version of quantum
theory: nothing is real at bottom, and so it is the observation that im-
parts reality to the subatomic world. He discusses the fact that
Einstein found the conclusions of quantum mechanics unacceptable;
so did physicist Erwin Schrédinger. The two men wanted desper-
ately to believe in the inherent orderliness of autonomous nature.
But such a view is scientifically old fashioned, Gribbin’s book dem-
onstrates. Modern quantum mechanics rests on the presupposition
that the subatomic world is governed by equations, and there is no
underlying physical reality corresponding to the equations. The
equations, not physical reality, are primary in modern quantum
mechanics. As he forthrightly says, nothing is real. “For what quan-
tum mechanics says is that nothing is real and that we cannot say
anything about what things are doing when we are not looking at
them.”32

Schrédinger resisted this implication of modern physics, which,
after all, is supposed to deal with what the Greeks called phusis, the
independent, eternal laws of nature. The subatomic world, modern
physics tells us, is a world in which light is both wave and particle.
It is a world governed by cquations, not physical reality. So
Schrédinger designed a hypothetical experiment which he believed
would refute the worldview of quantum mechanics. Place a cat in a
box, he said. In the box is a hammer suspended over a glass con-
tainer of poisonous gas. The hammer is connected to a triggering
device that in turn is connected to another device that registers the
decay of a radioactive substance. If a particular atom decays, the
recording device signals the triggering device that holds the hammer.
This second device releases the hammer, which falls on top of the
glass container, releasing the gas. The cat dies.

Say that there is a 50-50 chance that in any period of time, the
particular controlling atom will decay. The cat therefore has a 50-50
chance of surviving. Thus, the survival of the cat is based on a statistical
probability, the decay or non-decay of an atom.

31. John Gribbin and Stephen Plagemann, The Jupiter Effect (New York: Walker,
1974),

32, John Gribbin, In Search of Schrédinger’s Cat: Quantum Physics and Reality (New
York: Bantam, 1984), p. 2
