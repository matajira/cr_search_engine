136 IS THE WORLD RUNNING DOWN?

he wants to transform traditional humanism as well as traditional
fundamentalism into a new, anti-biblical religious ideology.” (What
he really wants, as do almost all revolutionaries throughout history —
religious and secular —is sexual license freed from the restraint of bibh-
cal morality!)

There is nothing even remotely Christian about Rifkin’s social
theory, for there is nothing remotely Christian about his cosmology.
Social theorists who take seriously the social entropy theory, but who
also regard themselves as Christians, are either deluded about the
humanistic origins of the theory, or else they do not understand (or
accept) Christianity’s implications for social theory.

New Left, New Age

The conservative American sociologist, Robert Nisbet, has
described the political and intellectual changes that preceded the
higher consciousness or “New Age” politics. When he wrote this
analysis, the “New Age” political movement was barely discernible.
Its fundamental principles, however, were already present in the
New Left political movement which appeared during the Vietnam
War era after 1965, The conflict between the rationalist Old Left and
the subjectivist New Left continues, though not with the same visible
confrontations that shook the university campuses in the late 1960's.
Nisbet wrote in 1972;

I think it can be said accurately that the Old Left’s hatred of the New
Left was, and is, based on two fundamental points. The first is obvious
enough. It is the New Left's seeming disdain for the nice, bureaucratic-
humanitarian society the Old Left helped to build, that had, so to speak,
liberalism-and-six-per-cent as its motto, and that now seemed to be going
down the drain as a result of the antics of the New Left. The universities in
America, from Harvard across the country to Berkeley, had become
cherished, increasingly luxurious homes for Old Radicals, exhilarating set-
tings for the permanent politics to which the Old Left was consecrated.
When the New Left turned with such ferocity upon these monasteries of

20. Tbid., Section II: “The Religious Response.”

21. “The sexual revolution of the past twenty years has done a great deal to liber-
ate men and women from the umnatural inhibitions imposed by the Puritanism of
orthodox Christian dogma.” Zéid., p. 246. On the perpetual call for unregulated sex-
val activity in revolutionary circles, especially the idea of the communality of wives,
see Igor Shafarevich, The Socialist Phenomenon (New York: Harper & Row, 1982), pp.
12, 23-24, 36-38, 47-48, 63-64, 214, 236.
