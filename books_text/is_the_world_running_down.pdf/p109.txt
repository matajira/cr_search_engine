Entropy and Social Theory 73

Yet Rifkin chides premillennialists for giving up on the world,
“The premillennialists view history in much the same way as the sec-
ond law of thermodynamics. However, their overriding preoccupa-
tion with arriving as quickly as possible at the end of history —God’s
return — precludes any serious service as stewards over God's created
order. Their attention has become so riveted on anticipation of the
coming of the kingdom and saving as many souls as possible in the
remaining time, that they have left God’s created order unguarded
and unprotected. In not honoring their covenant to God to serve as
stewards, the premillennialists are acting in direct rebellion.”? He
challenges them to become stewards. How can they do this? By
adopting his philosophy of anti-growth and anti-science. How do we
slow down this universal evil, meaning the entropy process? By
abandoning most Western technologies. “Technologies, after all, are
designed to speed up the entropy process by more progressively us-
ing up the stock of available matter and energy in the world.””

Rifkin is attempting to reduce Christians’ resistance to his theor-
ies by making them feel guilty. He chides them for becoming consist-
ent with their eschatological pessimism. He calls them to a life of
sacrifice, despite guaranteed failure, This strategy of guilt-manipu-
lation is an effective means of producing a program for your enemies’
cultural paralysis.

Simple to Complex?

The creationist quite properly challenges the evolutionist to ex-
plain how it is that an increasingly complex and increasingly orderly
world could have evolved from a random, “noisy,” Creator-less, lifeless
world. Evolutionists feel the heat of this question. (Someday, they
will be subjected to a lot more heat.) Isaac Asimov has gone so far as
to say in a footnote response to an essay written by Scientific Crea-
tionist Duane Gish: “Astronomers do not believe the Universe began
in a disordered, chaotic state. It began, in fact, in a condition of high
order. A departure from this order, an inhomogeneity, led to the for-
mation of stars and galaxies in accordance with the second law.”

This is an astounding statement. He is saying that the “cosmic
stuff” of the legendary Big Bang was highly ordered. No molecules
yet, but more highly ordered than molecules! No organic life yet,

9. Ibid., p. 236.
10. Dbid., p. 233.
