The Entropy Debate within Humanistic Science 95

in the Darwinian dualism between “man, the product of purposeless
evolution,” and “Man, the purposeful master of evolution.” Confu-
sion only increased when proponents of another religious perspective
entered the debate on the side of ecology and environmentalism,
namely, mystics who see no hope in the humanist rhetoric of pro-
gress, science, and economic growth. Accompanying these Eastern
mystics were representatives of Christian pietism, who also see no
long-term hope in the rhetoric of earthly progress."° Having no hope
in the future, they reject science and technology as valid means of
bringing long-term improvement to society or nature. It is this per-
spective which baffles Maddox. Yet as a Darwinian, he is caught in
entropy’s logical trap. His universe is running down, just as Rifkin’s
is. The only question is the time frame in which the entropy process
is socially significant.

Ignoring the Long Run

As an evolutionary scientist, Maddox is aware of entropy, but he
also knows that it is an exceedingly long-range problem. “In the long
run, as Lord Keynes put it, we shall all be dead. The catalogue of
natural disasters includes equally unpredictable natural phenomena
which will eventually be more damaging. . . . The ultimate dis-
aster, remote though it may be, will come within the transformation
due eventually to take place in the sun.”"! The sun will become ex-
hausted and expand to up to a hundred million miles in diameter.
The earth is 93 million miles away from today’s sun. But this event is
“certain” to be a thousand million years in the future. “These are hor-
rendous prospects, but they provide a kind of yardstick with which
to assess the durability of spaceship earth. On this scale, the self-
destructive potential of terrestrial technology will be puny for a long
time to come, The analogy of the spaceship is false simply because
the scale of the earth is so different from that of any spaceship that
could be constructed artificially.”

The response of the believer in conventional science and technol-
ogy is straightforward. The human race has plenty of time remaining.
Even though humans are increasing the dissipation of the universe’s

10, Rifkin cites some of these authors, and certain anabaptist radicals, in turn,
cite him

LU, Ibid., p. 27.

12. Dbid., p. 28.
