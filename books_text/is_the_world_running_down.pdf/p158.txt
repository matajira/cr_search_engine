122 48 THE WORLD RUNNING DOWN?

The Christian Alternative

The Christian, on the other hand, does have hope in life after
death: for the cosmos and for himself. He also has faith in the law-
order revealed to him in the Bible. He knows that the process of sacial
decay can be reversed through repentance and humility before God. The physi-
cal universe is an open cosmos—open to God and His grace. God
sustains it. The social world is also an open cosmos. God sustains it,
too. We live in an ethically open box, not a closed box marked by long-
term disintegration with only moments of short-term growth. There-
fore, God tells ethically rebellious men to agonize over their sins, not
over the cosmos. “And in that day did the Lorp God of hosts call to
weeping, and to mourning, and to baldness, and to girding with
sackcloth” (Isa. 22:12). But hedonists prefer not to listen to God's
call: “And behold joy and gladness, slaying oxen, and killing sheep,
eating flesh, and drinking wine: let us eat and drink, for to morrow
we shall die” (Isa. 22:13).

Rifkin wants us to put on perpetual sackcloth and ashes, to
mourn the fate of ourselves and our social cosmos, which has no
more hope than the entropy-bound universe. But his sackcloth offers
no future. It is metaphysical sackcloth, not ethical sackcloth. It is not
to be worn as a sign of repentance and distress over sin. It is to be
worn as a permanent cloak, a symbol of man’s submission to inevitable cos-
mic destruction,

Rifkin’s world is a closed universe. This universe cannot save it-
self or save us, either. In fact, we must do our best to conserve the
environment—why, he does not say, for there is no one over us to
condemn us or reward us. We cannot save it from destruction; we
can at best delay the demise of our little corner of the cosmos, but
only by dissipating energy out of the rest of it. Man cannot save man
or his environment, nor can the environment save man. Entropy
swallows everything. Ours is supposedly a world devoid of long-term
positive feedback, and what little growth there is, Rifkin regards as a
threat to long-term survival (meaning an extra few generations).

Restoration

What is the biblical alternative to this philosophy of ultimate de-
struction? God’s promise of ultimate restoration. The problem is, Chris-
tians have too often preached that this restoration can come only
after the final judgment (amillennialism) or after Christ’s return to
