316 IS THE WORLD RUNNING DOWN?

effort, workers could increase their income. When instructed by
profit-seeking managers, the workers did just that.

Rifkin quite properly traces this alien, clock-oriented worldview
right back to the Puritans. He cites Richard Baxter’s late-seven-
teenth century work, A Christian Directory: “A wise and skilled Chris-
tian should bring his matters into such order, that every ordinary
duty should know his place, and all should be . . . as the parts of a
clock or other engine, which must all be conjunct, and each right
placed.”25 Rifkin exaggerates for effect: “The clock culture called
forth a new faith: the future could be secured if everyone would only
learn to be on time.”?6 He says that modern society makes punctuality
the key to man’s self-salvation.

Progress; Productivity, Choices, and Freedom

What Rifkin fails to understand is that there is a process of devel-
opment going on. Techniques of mass production make possible the
spread of wealth to lower classes by means of price competition. Pro-
duction for a mass market became more profitable than production
for the court and the nobility. This was the process Max Weber
called the democratization of demand.” Initially, a few types of
products of fairly low quality are made available to the masses. By
lowering prices, producers can create a mass market that will absorb
the increased output of factories.

This is only the first stage of the process. Then comes greater
product differentiation and increased consumer choice. At first,
price competition expands the market. New groups gain access to
goods not previously available to them, either because prices were
too high before, or because the products did not even exist, As parti-
cipants in the production process, workers add to other people’s
wealth. Producers are buyers; step by step, as output per unit of in-
put increases, as a result of the specialization of production, the
wealth of all the participants increases. The initial expansion of buy-
ing alternatives itself expands as productivity increases. Some pro-
ducers may specialize in producing for this newly improved buying

25. Time Wars, p. 96.

26. Ibid., p. 97.

27. Max Weber, General Economic History (New York: Collier, [1927] 1961), p. 230.

28. Gary North, ‘Price Competition and Expanding Alternatives,” The Freeman
(August 1974).
