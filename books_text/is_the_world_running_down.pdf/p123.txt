Entropy and Social Theory 87

Max Weber, the great German sociologist, did understand, but
his warnings have generally gone unheeded by the professional
scholars, The democratization of society in its totality, Weber wrote,
“is an especially favorable basis of bureaucratization, but by no
means the only possible one.” We cannot say that this increase in the
power of bureaucracies is automatic, he said, but there are reasons
to believe that modern political conditions are especially favorable
for bureaucratic expansion, “The power position of a fully developed
bureaucracy is always great, under normal conditions overtowering.
The political ‘master’ always finds himself, vis-a-vis the trained offi-
cial, in the position of a dilettante facing the expert.”# Politicians
lose control.

The profit management system of capitalism at least keeps the
“experts” in check by means of price competition, innovation, and
the legal ability of consumers to say “no."# But this aspect of the
market has not been understood by the vast majority of intellectuals
in the twentieth century. Rifkin is no exception.

Waste Is Expensive

As we have already seen, he contrasts “productivity” with “con-
servation.” This is the language of the ecology movement, as well as
most versions of Eastern religion that are popular in the West. I else-
where discuss at some length the relationship between private own-
ership and the incentive to conserve assets, and the relationship
between socialist ownership and the incentive to waste or consume
“free” resources.#? Productivity in a free market must take into con-
sideration all known costs of operation, and the free market encour-
ages owners to discover formerly hidden costs, in order to reduce
them through greater efficiency. One of these costs is the cost of the
reduced future value of capital assets that are being used up in any given produc-
tion process. Waste is a cost to owners of capital assets. If owners can
find a way to reduce waste or extend the productive life of their capi-

40. Max Weber [pronounced Mawx VAYber], Ecenomy and Society: An Outline of
Interpretive Sociology, edited by Guenther Roth and Claus Wittich (New York: Bed-
minster Press, 1968), III, p. 991. This chapter on bureaucracy was published post-
humously in the early 1920's in Weber's Wirtschaft und Gesellschaft.

41. Ludwig von Mises, Bureaucracy (Cedar Falls, Iowa: Center for Futures Edu-
cation, [1944] 1983). This book is now distributed by the Libertarian Press, Spring
Mills, Pennsylvania.

42. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1988), ch. 14: “Pollution, Ownership, and Responsibility.”
