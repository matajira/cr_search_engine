Dominion and Sanctification 157

lish external peace? Because the church cannot do it. “How could the
church be expected to establish the kingdom by taking over the
world when even God cannot accomplish that without violating
man’s freedom of choice?”> Well, God can accomplish this the same
way He presumably converted Mr. Hunt: by regenerating people. Cer-
tainly, if Mr. Hunt can be converted to Christ, others can, too. It is
up to God how many He will bring to Himself through Christ.

The king’s heart is in the hand of the Lorp, as the rivers of water: he
turneth it whithersoever he will (Prov. 21:1)

For whom he did foreknow, he also did predestinate to be conformed to
the image of his Son, that he might be the firstborn among many brethren
(Rom. 8:29).

According as he hath chosen us in him before the foundation of the
world, that we should be holy and without blame before him in love: Hav-
ing predestinated us unto the adoption of children by Jesus Christ to
himself, according to the good pleasure of his will (Eph. 1:4-5).

It does not take a top-down bureaucratic State run by Jesus in
person in order to establish peace on earth. God can change people's
hearts, send them His Spirit, and give them His law as a tool of gov-
ernment—self-government, family government, church govern-
ment, and civil government. Mr. Hunt worries about the free will of
men, yet he also admits (as every dispensationalist must admit) that
Jesus will allow no freedom to sin without punishment in the millen-
nial reign. Jesus will rule sinners with a rod of iron, they say. Yet dis-
pensationalists tell us categorically that He will never rule the world
in love through the hearts of a majority of people in the future.

Why not? Where is the power of the gospel?

Stagnation as Judgment
God’s covenant governs the family, as well as the church. The
family is a covenantal institution."6 The same system of blessings
and cursings that governs the church also governs the family. We
read of such blessings in the fifth commandment: long life for honoring
parents (Ex, 20:12), health (Ex. 23:25), and large families (Ps. 127:5).
Long-term stagnation— economically, demographically, intellec-
tually —is.a sign of God’s displeasure. Growth must not be seen as
inherently destructive. More than this: @ static culture cannot survive. It

15. Idem.
16. Sutton, That You May Prosper, ch. 8.
