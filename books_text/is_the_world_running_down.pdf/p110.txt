74 1S THE WORLD RUNNING DOWN?

but more highly ordered than organic life! This boggles the mind of
the average person who, unlike Asimov, is unable to write two books
a month. Asimov does not tell us which astronomers have said this.
He also does not mention how this highly ordered system started, or
what it was. He does not because he cannot. But this is supposedly
irrelevant. In the standard false humility side of modern two-faced
science, he adds: “Scientists do not know how the condition of high
order began, but scientists are accustomed to lack of knowledge.”
They are always ready to feign humility when they get caught on the
horns of an obvious intellectual dilemma.

Eschatology and the Second Law

But creationists erroneously believe that they have achieved
more of a victory than the idea of entropy entitles them to. The pre-
viously cited flyer asserts: “Evolution demands that things ‘wind up’
even as we see them run down. Therefore the evolutionist looks for
things to improve.” Again, what do we mean, “improve”? What
“things”? For that matter, what does the evolutionist mean by
“improve”?

Pastor Tommy Reid has written an intelligent essay from a pre-
millennial perspective, one which relies in part on the postmillennial
optimism of David Chilton’s book, Paradise Restored. Reid has also
read Jeremy Rifkin, His observations are very significant for the
thesis of this book, namely, that the use of the entropy concept in
social theory leads to anti-Christian, socially paralyzing conclusions
(as Rifkin understands so well):

Recent concepts have contributed to cither a falalistic or monastic atti-
tude among other evangelicals, not the least of which is our modern em-
phasis on science. The rather recent discovery of the law of thermodynam-
ics has shaped the philosophical view of the world held by some evangeli-
cals, The second law of thermodynamics states that all matter and energy
were created in this original state with an order and value to them. [This is

11. “The Genesis War,” Science Digest (Oct. 1981), p. 82. Six pages later, when he
concludes his own essay against Gish, he becomes absolutely confident, in contrast
to his earlier “scientific” humility: “In fact, the strongest of all indications as to the
fact of evolution and the truth of the theory of natural selection is that all the inde-
pendent findings of scientists in every branch of science, when they have anything to
do with biological evolution at all, always strengthen the case and never weaken it,”
(Emphasis in the original.) Always strengthen? Never weaken? This is humble
science? No, this is just sloppy philosophy masquerading as science.
