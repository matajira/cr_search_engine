222 IS THE WORLD RUNNING DOWN?

God. The world is positioned in the evil one and does not have its source in
God; the world is that realm which is dominated by Satan and his stan-
dards. It is correspondingly appropriate that Satan is designated by John as
“he that is in the world” (I John 4:4). The world is in Satan, and Satan is in
the world. This confirms the ethical understanding of the term “world”
which has been discussed above, for the created realm certainly does have its
origin in God and hes God immanent to it. Thus, “the world” (which is not
of God, but is characteristically in and occupied by Satan) cannot be iden-
tified with created reality or the whole of humanity. “The world” must be in-
terpreted (in the above passages) as an unethical spiritual realm, the king-
dom of darkness, the city of reprobate man.2

Bahnsen goes on for several more pages in order to correlate “this
age” with “the world” in the same sense of ethics, but there is not
space here to reproduce Bahnsen’s brilliant insights. The point
should be clear: “the world” can be taken in many passages to mean
Satan’s ethical realm, not the whole kosmos as such.

The riches of this “world”: The word can also mean the riches and
advantages of earthly life. The Bible asks, “For what is a man advan-
taged, if he gain the whole world, and lose himself, or be cast away”
(Luke 9:25)? It can also mean Israel. The Pharisees stated of Jesus
that “the world’— meaning a large number of Israelites—“is gone
after him” (John 12:19b). Thus, in some instances, kosmos was used
to refer to persons in a group, but the word usually referred to a
much broader concept: the world order.

The love of this “world”: When the Bible speaks of God’s love for the
world, it obviously does not include the prince of this world, Satan,
for an everlasting fire has been prepared for him and his angels
(Matt. 25:41b), God loves the world, meaning that which He
created, but He nevertheless intends to visit the world with a cleans-
ing fire (II Peter 3:10). The world today will, in part, survive that
fire, yet elements of it will not. In other words, the world loved by
God is now, but it also wilf be. There is both a present and future
aspect, just as there was a separate world order prior to the Noachic
Flood, yet God preserved elements of that world by His grace in the
ark (II Peter 2:5), There is both continuity and discontinuity in the
biblical concept of Kosmas. It was, is now, and shall be, despite major
changes.

The redemption of the “world”: The theological question that has to

2, Ibid., p. 24.
