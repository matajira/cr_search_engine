The Division within Dispensationatism 293

I could not have said it any better. This is the Christian Recon-
struction position. His recommended five-point program outlined in
Chapter Four instinctively mirrors Ray Sutton’s five-point covenant
model.?” Yet somehow I wonder where in Scofield’s Reference Bible
this view of biblical law (code name today: “biblical standards”) is
taught. I do not recall its being in there. Neither does the ever-safely
silent faculty at Dallas Theological Seminary. What I do remember
was the 1963 attack on the Ten Commandments by (then) Dallas
Seminary professor S. Lewis Johnson, in Bibliotheca Sacra, Dallas
Seminary’s scholarly journal:

At the heart of the problem of legalism is pride, a pride that refuses to
admit spiritual bankruptcy, That is why the doctrines of grace stir up so
much animosity. Donald Grey Bamhouse, a giant of a man in free grace,
wrote: “It was a tragic hour when the Reformation churches wrote the Ten
Commandments into their creeds and catechisms and sought to bring Gen-
tile believers into bondage to Jewish law, which was never intended either
for the Gentile nations or for the church.”* He was right, too.

The Ten Commandments are Jewish law, not ethically binding
general principles, said Barnhouse and Johnson. And if the Ten
Commandments are Jewish law and no longer binding on New Tes-
tament Christians, there can be little doubt that any attempt to justify
the view that any aspect of Old Testament law ts binding today is necessarily
anti-dispensational in intent. This is the essence of the ethical view held
by traditional dispensationalists, as articulated by two of the move-
ment’s most important leaders, Barnhouse and Johnson. {I doubt
that former professor, former pastor Johnson would still affirm such
an idea; anyway, I hope not.)

There is a very good reason why the younger socially active dis-
pensationalists such as Mr. Schnittger receive no teaching offers or
lecture invitations at Dallas Seminary and Grace Seminary, at least
not in the field of New Testament ethics: they have “departed from
the true faith,” which was delivered in 1830 by John Nelson Darby
(or possibly Margaret Macdonald in her trances),® spread in the

27. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: In-
stitute for Christian Economics, 1987).

28. He cites Barnhouse, God's Freedom, p. 134.

29, S, Lewis Johnson, “The Paralysis of Legalism,” Bibliotheca Sacra, Vol. 120
(April/June, 1963), p. 109.

30. This is Dave MacPherson's thesis: see footnote #2, above.
