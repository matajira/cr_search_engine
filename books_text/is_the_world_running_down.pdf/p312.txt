276 JS THE WORLD RUNNING DOWN?

worry about that; tet’s unite in our common concern for our fellow man,”
and so forth. That opens the door to a very deceptive lie which literally
turns us from heaven as our hope to this earth, which is at the heart of the
kingdom-dominion teaching, that we—man—was given dominion over
this earth, and the problem is that he lost the dominion to Satan, and the
big thing is that we need to regain the dominion. . . . But it opens the door
to a marriage with New Age beliefs, as you know, with humanistic beliefs,
so that we will all be joining together in working for ecological wholeness,
working for peace, working for prosperity, because we are not concerned.
about heaven, or the return of Christ, or the Rapture, but we have got to be
concerned about earth, the threat of ecological collapse, the threat of a
nuclear holocaust.¥

Here we have the continuing historical theme in ali traditional
dispensationalism: the radical separation of heaven and earth, which
necessarily implies the increasing connection between hell and
earth. The dispensationalists are promoting the spread of Satan’s
imitation New World Order when they protest the validity of
Christ's New World Order, which He established definitively with
His death, resurrection, and the sending of the Holy Spirit at
Pentecost. Dispensationalism delivers the world to Satan and his fol-
lowers dy default, and all in the name of biblical orthodoxy regarding
the Rapture—orthodoxy which began no earlier, they are forced to
argue, than 1830.

Whose New World Order?

Now, let me say right here, as I have said earlier in this book: I
believe in the New World Order of Jesus Christ, inaugurated at Cal-
vary and visibly sanctioned in history by the resurrection and ascen-
sion of Christ to the right hand of God, where He now reigns in
power and glory, What I reject is the imitation New World Order of
humanism. But there is a biblical New World Order, There is a new
ereation in Christ. It was established definitively at Calvary. It is being
established progressively in history. And it will be established finally at
the day of judgment.

We cannot expect to beat something with nothing. We cannot ex-
pect to defeat the humanists’ New World Order with a traditional
(1830) theology of guaranteed historical defeat— the theology of tra-
ditional dispensational pessimillennialism, We must fight theological

30. Dominion: A Dangerous New Theology, Tape #1 of ibid.
