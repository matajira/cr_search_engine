212 IS THE WORLD RUNNING DOWN?

majority, expressed the heart of the matter with beautiful succinctness.
“This is not a difficult case. . . .” he wrote. “The teaching of evolution was
conditioned on the teaching of a religious belief... . The statute was
therefore unconstitutional under the Establishment Clause.”

We who have fought this battle for so many years were jubilant. The
Court, by ruling so broadly and decisively, has ended the legal battle over
creationism as a mandated subject in science classrooms. I do not chink the
fundamentalists can invent a fourth strategy.

The Fourth Strategy: “Shut Them Down!”

He is wrong. There is a fourth strategy: to shut down all taxpayer-
financed schools. That step has not been taken by fundamentalists,
who still believe in taxpayer-financed education with all their hearts.
They send their children to be educated by their enemies, in order to
save a few dollars in tuition money, This is the fundamentalists’
equivalent of passing their children through the fire. “Moreover thou
has taken thy sons and thy daughters, whom thou hast borne unto
me, and these hast thou sacrificed unto them to be devoured. Is this
of thy whoredoms a small matter, that thou hast slain my children,
and delivered them to cause them to pass through the fire for them”
(Ezek. 16:20-21)?

Cornelius Van Til argued throughout his career that problems of
knowledge are always problems of ethics. When we find an error in
logic, we should begin our search for the ethical origin of the logical
error. The logical error of the Scientific Creation movement is its
continual appeal to neutral principles of interpretation, an appeal to
a common-ground epistemological system. But a common-ground
epistemology necessarily implies the existence of a common-ground
ethical system. There is no epistemological neutrality because there
is no ethical neutrality. Creationists who appeal to a common-
ground neutral scientific worldview do so in part because of ignor-
ance. They are seldom even vaguely familiar with the history of
theology, philosophy, or even science. But the underlying basis of
this self-conscious appeal to a common-ground scientific epistemol-
ogy, despite a hundred years of ridicule and defeat by Darwinians, is
ethical. The fundamentalists will not give up the idea that the state is
economically, legally, and morally responsible for the education of
children, The Creation Science movement has been seeking to get in

8. Stephen Jay Gould, ‘The Verdict on Creationism,” Naw York Times Magazine
(July 19, 1987}, p. 32.
