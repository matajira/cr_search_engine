Comprehensive Redemption: A Theology for Social Action 249

Hardly anyone preaches judgment for restoration’s sake. Hardly
anyone speaks of judgment as the prophets did, namely, as a painful
means of moral and institutional restoration. The judgment that the
liberals expect is that of historical defeat and impotence for liberal,
humanistic values. The judgment that fundamentalists expect is one
which Christians will escape, and which is not related directly to the
post-resurrection rule of death-proof saints during the millennium.
The judgment that amillennialists look forward to is the end of time,
the last earthly event before the final judgment. None of these per-
spectives offers the biblical view of judgment, namely, that God
chastises His people —covenantally, not just individually —as a way
to restore them to faith in Him and to enable His people to engage
once again in the task of Christian reconstruction: building the king-
dom of God on earth, by means of His law. In short, no one preaches
prophetic judgment any more.

Infiltration and Replacement

The French Revolution, like the Russian Revolution, relied
heavily upon the existing bureaucratic structure for the implementa-
tion of social change. The revolutionaries recognized that the incum-
bent bureaucrats were vital, at least initially, for the consolidation of
power by the new rulers, The stability of bureaucracy is perhaps its
greatest strength. Loyalty of bureaucrats is directed toward the pre-
vailing offices, not to individuals. When the revolutionaries replaced
the king or czar, it made little difference to those holding bureau-
cratic positions, Lenin was the son of a minor Russian bureaucrat.
Many of the French revolutionaries were lawyers and others who
had worked with the various levels of the bureaucracy, either as
employees or as hired representatives of business or the nobility.

The revolutionaries understood how bureaucracies operate. If
Christians are to be equally successful in reshaping the civil govern-
ment, they also must learn how the bureaucratic system works.
Christians need to understand what motivates members of bureau-
cracies. They need to gain experience in working with bureaucra-
cies. They need to have their own people inside bureaucracies,
either as employees or as representatives of the civil government or
business. Such an education must not be undertaken in order to
make the present order function more smoothly, but the opposite: éo
gum up the existing humanistic soctal order through its own red tape. We need
to infiltrate the bureaucracies in order to secure a foothold in the ex-
