84 IS THE WORLD RUNNING DOWN?

It is obvious that a literal application of Rifkin’s version of Gen-
esis would prohibit the extermination of one of Gad’s biological crea-
tions. Rifkin is straightforward: “Every species must be preserved
simply because it has an inherent and inalienable right to life by vir-
tue of its existence.” It is equally obvious that a literal application of
the Protestant version of the dominion covenant would encourage
this particular “exploitation of nature.” Kill the creatures! Smallpox
would be eradicated from the earth.

There is, sadly, still a third view, neither Eastern nor Christian:
the scientific view. “Keep the organisms alive, so that scientists can
conduct more research, even if there is a risk that the disease will get
out of the laboratory and back into society at large.” (This may
already have happened once, In 1978, a medical photographer for
Birmingham University died of smallpox. She had been working one
floor above the virology research department which had been in-
volved in smallpox research.)*? The autonomy of scientific research
is placed above the welfare of society.

What the Bible never teaches is the preservation of all life or all
species, irrespective of the legitimate — though we can argue about
what constitutes “legitimate” ~ needs of mankind. Care with nature,
yes. Caution in introducing new species (through genetic manipula-
tion), yes.%3 Care in moving one species to a new environment where
it has few biological enemies, of course. But it is the needs of man—
specifically, the needs of a growing number of regenerate and mor-
ally sanctified people who act as God’s lawfully delegated agents — as
revealed in the Bible and through Bible-governed human wisdom that must
govern the dominion covenant. In other words, because there is a
fixed moral order there can be a progressive development of the creation under
the auspices of man. As men begin to conform themselves to the
terms of biblical law, they will exercise greater power, meaning long-
term, God-honoring, man-satisfying power, over the creation. This
is what Genesis 1:26-28 is all about.

Dubos, a non-Christian, has an inkling of this necessity of pro-
gress, although men will debate over the proper definition of progress.

31, ibid, p. 210,

32, Associated Press story, Durham Morning Herald (Sept. 17, 1978).

33. Rifkin, Whe Shall Play God? (New York: Dell, 1977). For a more optimistic
and less apocalyptic view of genetic engineering, see William Tucker, Progress and
Privilege: America in the Age of Environmentalism (Garden City, New York: Anchor
Press/Doubleday, 1982), ch. IL.
