4

THE ENTROPY DEBATE
WITHIN HUMANISTIC SCIENCE

From the biblical perspective, the triumph of man under God involves the
conquest of time and history, its redemption in terms of covenantal pur-
pose, by timetable, schedule and clock among many other things... .

This concept undergirded the exuberant conquest of time and nature by
Western man as scientist. The world of the clock, timetable and schedule
was seen as the liberation of man in terms of his purposive mastery of time
and nature. But, as scientific man moved steadily from his Christian
origin and perspective into a philosophy of process [evolutionism —G.N.],

he perversely saw the timetable, clock and schedule as, first, a means of
de-humanizing man as against God's insistence that man ts primantly
covenant man, and, second, as a tyranny to be rebelled against in the
name of freedom. Thus, at the moment of science’s triumph, science began
to be viewed as demonic by tts very sons, who sought vain refuge from the
clock of history in “time lived.”

R. J. Rushdoony!

Jeremy Rifkin has taken the long-term pessimistic implications
of the second law of thermodynamics and has used them to attack
the Western concept of short-term progress, especially scientific pro-
gress. He is using the ultimate implications of Western science to call
into question the benefits of Western technology. He is using the
linear time concept involved in entropy —the modern scientific basis
of linear time (“time’s arrow,” Sir Arthur Eddington called it)—to de-
stroy Western humanist man’s admittedly naive faith in history, He
is using Western science to justify the acceptance of Eastern
mysticism. He has seen a vulnerable spot in the soft underbelly of

1. R. J. Rushdoony, The Mythology of Science (Nutley, New Jersey: Craig Press,
1967), pp. 76-77.

91
