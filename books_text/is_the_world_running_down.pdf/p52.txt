16 IS THE WORLD RUNNING DOWN?

The modern world began on 29 May 1919 when photographs of a solar
eclipse, taken on the island of Principe off West Africa and at Sobral in
Brazil, confirmed the truth of a new theory of the universe. It had been
apparent for half a century that the Newtonian cosmology, based upon the
straight lines of Euclidian geometry and Galileo’s notion of absolute time,
was in need of serious modification. It had stood for more than two hun-
dred years, It was the framework within which the European Enlighten-
ment, the Industrial Revolution, and the vast expansion of human knowl-
edge, freedom and prosperity which characterized the nineteenth century,
had taken place. But increasingly powerful telescopes were revealing
anomalies. In particular, the motions of the planet Mercury deviated by
forty-three seconds of an arc a century from its predictable behavior under
Newtonian laws of physics. Why?

In 1905, a twenty-six-year-old German Jew, Albert Einstein, then work-
ing in the Swiss patent office in Berne, had published a paper, “On the elec-
trodynamics of moving bodies,” which became known as the Special
Theory of Relativity. Einstein's observations on the way in which, in cer-
tain circumstances, lengths appeared to contract and clocks to slow down,
are analogous to the effects of perspective in painting. In fact the discovery
that space and time are relative rather than absolute terms of measurement is
comparable, in its effects on our perception of the world, to the first use of per-
spective in art, which occurred in Greece in the two decades ¢. 500-480 B.c.8

This astronomical experiment confirmed in the minds of contem-
porary scientists Einstein’s theory of special relativity. Johnson
argues that this confirmation led to a fundamental restructuring of
modern man’s view of the universe. What he does not mention is the
staggering fact that in that same year, 1905, Einstein published two
other papers that had equally disrupting effects on modern science.
The first, on the photoelectric effect, won him the Nobel Prize in
physics in 1921; the second dealt with what scientists call Brownian
motion; the peculiar wiggling of extremely tiny particles of matter
when suspended in liquids. The first paper confirmed the reality of
quantum mechanics, and this has led most theoretical physicists to
deny the reality of a subatomic universe. The second paper showed
how the reality of atoms could be decided experimentally. The ex-
periments by Jean Perrin that were based on Einstein’s suggestion
later confirmed the existence of atoms.? Einstein’s three papers

8. Paul Johnson, Modern Times: The World from the Twenties to the Eighties (New
York: Harper & Row, 1983), p. 1.
9. Herbert, Quantum Reality, pp. 35-36.
