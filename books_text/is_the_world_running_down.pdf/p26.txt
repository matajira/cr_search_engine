XxVi IS THE WORLD RUNNING DOWN?

denied that economic growth is morally valid or even sustainable,
long-term. Fifth, he has called Christians to adopt Eastern mysti-
cism in the name of a new Christianity. Sixth, his recommended eco-
nomic system would lead to the creation of a huge bureaucratic
tyranny, yet he argues as if he were calling for a decentralized,
minimal-State® form of government.

In short, Jeremy Rifkin is a specialist in deception. He is very
clever and therefore a very dangerous man. He has self-consciously
selected his target: Christians, especially the charismatics and the
neo-cevangelicals. He writes that “we are in the morning hours of a
second Protestant Reformation. . . . [I]t is the evangelical commu-
nity, with its resurgent spiritual vitality, that has the momentum,
drive and energy that is required to achieve this radical theological
transformation in American society.”® He is determined to take ad-
vantage of this second Reformation, to redirect it along very differ-
ent paths from those outlined in the Bible.

Overturning Capitalism

What kind of transformation is he talking about? Radical! At the
very least, it will require a sharp reduction of America’s economic
wealth. “As long as we continue to devour the lion’s share of the
world’s resources, squandering the great bulk of them on trivialities
while the rest of the world struggles to find its next meal, we have no
right to lecture other peoples on how to conduct their economic de-
velopment.”* You know the argument: 1) we eat; 2) they are
hungry; 3) therefore... .

Then, just to make every successful person feel doubly guilty,
“Today, the top one-fifth of the American population consumes over
40 percent of the nation’s income.” Do you know why one-fifth of the
population consumes over 40 percent of the nation’s income? Because
one-fifth produces over 40 percent of the nation’s income. That fact, I
assure you, Jeremy Rifkin and the professional guilt-manipulators
never, ever tell you. I believe that people should not be made to feel

22. I capitalize State when I refer to civil government in general. I do not capi-
talize it when | refer to the administrative unit in the United States known as the
state,

23. Jeremy Rifkin (with Ted Howard), The Emerging Order: God in the Age of Scarcity
(New York: Ballantine, [1979] 1983), pp. x, xii.

24. Jeremy Rifkin (with Ted Howard), Entropy: A Naw World View (New York:
Bantam New Age Books, [1980] 1981), p. 190.

25. Ibid., p. 194.
