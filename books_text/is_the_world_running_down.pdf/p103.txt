3
ENTROPY AND SOCIAL THEORY

Now, however, a new world view is about to emerge, one that will eventu-
ally replace the Newtonian world machine as the organizing frame of his-
tory: the Entropy Law will preside as the ruling paradigm over the next
period of history. Albert Einstein said that it is the premier law of all
science; Sir Arthur Eddington referred to it as the supreme metaphysical
law of the entire universe. The Entropy Law is the second law of therma-
dynamics. The first law states that all matter and energy in the universe is
constant, that it cannot be created or destroyed. Only its forms can change
but never its essence. The second law, the Entropy Law, states that maiter
and energy can only be changed in one direction, that is, from usable to
unusable, or from available to unavailable, or from ordered to disordered.

Jeremy Rifkini

Is there any relationship between men’s worldviews and the kind
of science they adopt? Probably there is. Henry M. Morris suggests
that one reason why today’s younger defenders of evolution have
begun to abandon the neo-Darwinian synthesis of slow, organic
changes is that they were educated during the “radical ’sixties.” Rev-
olutionary Marxism had been a quiet but effective intellectual force
throughout the 1960's on many campuses.? His chapter title is
“Evolution and Revolution.”

It therefore seems fair to ask this question of the Creation Scien-
tists: Is there any relationship between the kind of defense offered by
one generation of six-day creationists and the overall worldview that
they hold? I suggest that there are such relationships. A more impor-
tant question is this one: Is there any relationship between the

1. Jeremy Rifkin (with Ted Howard), Entropy: A New World View (New York:
Bantam, [1980] 1981), p. 6.

2. Henry Morris, Evolution in Turmoil (San Diego, California: Creation-Life Pub-
lishers, 1982}, pp. 91-93.

67
