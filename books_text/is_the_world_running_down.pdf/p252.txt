216 IS THE WORLD RUNNING DOWN?

question of fairness cannot be solved apart from an appeal to legttimate author-
iy: first, the authority of God; second, the authority of those to
whom God has delegated the responsibility of speaking in His name
and enforcing His law in any specific area. This is the issue that the
Supreme Court cannot avoid, yet one which it refuses to face
squarely. In the name of neutrality, the Court has defined God as the
will of the people, and it has identified the agency responsible for
voicing the will of the people as the civil government. The tenured,
tax-financed, humanistic teachers insist that they alone lawfully
represent the civil government in the classroom, and the Supreme
Court has now upheld their claim against the legislators of the State
of Louisiana and the taxpayers who elected them to office. The
bureaucrats have become the voice of humanism’s god. This is the
lesson of modern democracy.

In short, the day the Constitution became religiously neutral was
the day that the foundation of this Supreme Court decision was
established. It was only a matter of time and effective lobbying by
humanists who believe in salvation by politics and lobbying. The
mythological doctrine of “equal time for Satan” eventually becomes
“no time for God.” Neutrality is a myth. The Scientific Creationists
must learn this lesson. The Supreme Court has now given them a
lesson that has overturned a quarter century of their misguided efforts
to “save the public schools” and “make them fair” by trying to legislate
“equal time” for an officially Creatorless doctrine of creation.

Who was W. T. Couch, whose statement of academic freedom so
impressed Russell Kirk? I had the unpleasant experience in the sum-
mer of 1963 of working for three months as an associate of the late
Mr. Couch. Couch was a dedicated anti-Christian humanist, a man
who sneered at the very idea of the Bible as the infallible word of
God. I was never impressed with his reputation as a “distinguished
editor,” but I fully understood his search for suitable academic em-
ployment, He never produced much of anything in the months that I
worked with him, and he died leaving no visible mark on the con-
servative intellectual movement. He spent the three months I worked
with him trying to get the multi-million dollar conservative founda-
tion that employed us to spend a literal fortune to produce a modern
version of the French philosophes’ encyclopedia. He vehemently op-
posed any suggestion that the project should be straightforwardly
conservative. It was the same old story: use money supplied by con-
servatives to produce academic materials that are officially neutral
