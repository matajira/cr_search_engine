120 IS THE WORLD RUNNING DOWN?

legitimately believe in long-term static stability; the entropy of the
universe must eventually overcome all of man’s efforts to preserve
himself or nature. Furthermore, life increases entropy, so a static
society will require technological advances, if only to “stay even,”
since civilization will have to run faster and faster “just to stay in the
same place,” as the Queen said to Alice in Alice in Wonderland. A
theory of a static society which relies on a reduction of technology to
maintain its changeless order is an exercise in mythology. Stand still
in a universe governed by entropy, and you inevitably fall behind.

All Rifkin can offer mankind is a kind of grim “holding action”
which does not even hold out the hope of temporary victory through
technological innovation and capitalization, Thus, he says, “In an
entropic sense, the only way to ‘save’ time is to keep a society’s
energy flow as close as possible to that which naturally takes place in
our environment. In this way, the end of time and life will approach
as slowly as possible. But the pragmatist will try to ‘save’ time by
attempting to streamline the existing energy flow. This will only
escalate the entropy process and, along with it, decrease the amount
of time available to sustain life for generations yet to come.”? In
short, we cannot legitimately speak of “growth.” Why not? Because growth
is really contraction. More is really less: “. . .‘growth’ is really a
decrease in the world’s wealth, nothing more than a process to take
usable energy and transform it into an unusable state.”® The ques-
tion arises: “Usable by whom?” If men are not supposed to use it,
who should be allowed to use it? For whom are we to save the world’s
wealth, as it inevitably erodes? Impersonal nature?

All living things are parasites that feed off of the available energy
around them: “. . . even the tiniest plant maintains its own order at
the expense of creating greater disorder in the overall environment.”?
He quotes from Harold Blum’s book, Time’s Arrow and Evolution:
“The small local decrease in entropy represented in the building of
the organism is coupled with a much larger increase in the entropy
of the universe.”!° Life speeds up the entropy process. The best way
to reduce the overall “rate of entropy” of the universe is clear enough,
but Rifkin never admits it: put an end to all life.

. Ritkin, Entropy, p. 246.
Idem.

Bid., p. 53.

. Ibid., p. 52.

Seen
