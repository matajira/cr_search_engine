190 Is THE WORLD RUNNING DOWN?

lines: “The Darwinian evolutionists claim to be scientists, but they
ignore the operations of what is probably the most fundamental law
of science, the second law of thermodynamics. They refuse to appeal
to God to explain the origin of life~ an order-producing, increasingly
complex phenomenon — which must be regarded by consistent scien-
tists as an unexplainable, impossible event in a truly autonomous
universe that is governed as a whole by an invariable law of decay
and increasing randomness. Thus, they are not truly scientific, for
they refuse to apply this invariable scientific law to the study of
origins. This shows that they apply scientific laws selectively. This il-
legitimate selectivity therefore shows that they are not really
disciplined by a rigorously scientific methodology.”

The Darwinians respond with arguments along these lines: “The
creationists claim to be scientists, but they constantly appeal to
God’s miracles in order to explain historic events. They cannot
prove the existence of God, and they cannot offer evidence that
would enable us to explain how God interacts with the world. They
even deny that His miracles are subject to the restraints of predic-
table scientific law. Nevertheless, they claim to be scientists. This is
nonsense; they are only pseudo-scientists. They do not govern their
scientific inquiries by the second law of thermodynamics or by any
other scientific law. They apply scientific laws selectively. This il-
legitimate selectivity shows that they are not really disciplined by a
rigorously scientific methodology.”

I think these arguments are very similar. They both focus on the
same issue: the opposition’s failure to adhere to a methodology that
applies absolutely fixed scientific laws to all the available empirical
evidence. I intend to show that the arguments on both sides are so
similar—and initially so compelling intellectually—that it should
lead Christians to reconsider the whole approach of trying to apply
absolutely fixed, impersonal laws to all available evidence. Both
sides cannot be correct. Something must be wrong somewhere.
Since neither side is willing to “stick to the rules,” yet both claim to be
rigorous (“scientific”) in their approach to questions, perhaps there is
a more acceptable (and more honest) methodology which allows us
to be selective in the application of the familiar regularities of
nature, yet still be truly scientific. Maybe absolutely fixed, imper-
sonal laws do not really exist. In short, we need to ask ourselves:
“What is the proper scientific methodology?” We also need to ask
ourselves: “What does the Bible tell us concerning law?”
