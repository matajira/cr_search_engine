Dominion and Sanctification 159

Negative feedback is a sign of God’s curse, even for individuals at
the final judgment. Positive feedback is a sign of God’s grace. It
depends on one’s ethical standing before God: “For whosoever hath,
to him shall be given, and he shall have more abundance: but who-
soever hath not, from him shall be taken away even that [which] he
hath” (Matt. 13:12), There is growth for the godly and contraction
for the ungodly. In neither case is there the status quo.

Humanism, Paganism, and the Status Quo

A zero-growth philosophy is the product of humanism, both
secular and occult, It is a philosophy of the status quo—the preser-
vation of the society of Satan. The universe is cursed; its resources
are limited; but this reality is not evidence that favors a no-growth
philosophy. The biblical doctrine of fallen man does not teach men
to believe in a world that is cursed forever. Judgment and final
restoration are coming. Time is bounded. Redeemed mankind must
accomplish most of the dominion assignment in history. (Because of
sin, mankind’s historical fulfillment of the dominion assignment will
never be perfectly complete.)

Humanists and satanists wish to deny the sovereignty of God,
and therefore they affirm the sovereignty of the entropy process.
They wish to escape the eternal judgment of God, so they affirm an
impersonal finality for all biological life. Men have sometimes
turned to a philosophy of historical cycles to help them avoid the tes-
timony of God concerning linear history. Others have turned to the
entropy process when they have adopted a Western version of linear
history. They settle for slow decay rather than cycles, The goal in
both cases is to escape the judgment of God. All of them prefer to
avoid the truth: for covenant-breakers, the growth process will be
cut short, A new downward cycle will triumph. Entropy will triumph.
Anyway, something will triumph, but not the God of the Bible.

Rushdoony’s comments on pagan antiquity’s hostility to change
is applicable to the zero-growth movement of the modern humanist
world:

The pagan hatred of change was also a form of ascetism [asceticism —
G.N.], and it is present in virtually all anti-Christianity. The hatred of
change leads to attempts to stop change, to stop history, and to create an
end-of-history civilization, a final order which will end mutability and give
man an unchanging world, Part of this order involves also the scientific
efforts to abolish death. This hatred of change is a hatred of creation, and of
