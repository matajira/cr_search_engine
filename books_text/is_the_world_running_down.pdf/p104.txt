68 Is THE WORLD RUNNING DOWN?

eschatological views of one group of creationists and the type of in-
tellectual defense they adopt? I think there is. Finally, is there a rela-
tionship between one’s eschatology and one’s overall social outlook? I
think there is. That is the topic of this chapter.

Natural Science and Social Theory

The question inevitably arises: Can the so-called “law of entropy”
be applied in social theory? Such attempts are questionable. Those
who rely heavily on the entropy theory in their cosmological expla-
nations also tend to produce pessimistic social theories. It is my con-
tention that: 1) their pessimism is their presupposition; and 2) pessi-
mists sometimes adopt certain “entropic” cosmologies in order to
support their pessimistic theories. Thus, what appears to be a con-
clusion—social pessimism — is in fact an original presupposition.

For over six years, the evangelical world has remained silent in
the face of Rifkin’s books, except when the response has been some-
what favorable. I believe that this silence has prevailed in large part
because evangelical scholars share Rifkin’s interpretation of entropy,
his use of natural science in developing social theory, and his under-
lying pessimism regarding history and man’s part in it. Evangelicals
may not be comfortable with his economic and political conclusions,
but they are even more uncomfortable in challenging them, given
their own shared frame of reference: his pessimistic worldview.

»

The Resurrection

What the evangelical world has failed to emphasize is that the
doctrine of Christ’s resurrection is the unique New Testament doc-
trine that should be the starting point in Christian social theory.
Creation, rebellion, resurrection, and restoration: here is the Bible’s
message of hope and transformation im history ~ not just for individu-
als, but also for societies that must become the basis of Christian
social theory.

3. One attempt to apply certain aspects of the entropy theory to economics is a
difficult book by a brilliant economist, Nicholas Georgescu-Raegen, The Entropy Law
and Economie Process (Cambridge, Massachusetts: Harvard University Press, 1971).
The problem is that it is not clear that the entropy process of physical science is really
similar to the processes of ignorance and waste in economic planning. The author
uses the same word to describe both realms, but this does not prove the case for
social and economic entropy. In any case, the book has not exercised visible influ-
ence within the economics profession.
