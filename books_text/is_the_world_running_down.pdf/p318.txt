282 Is THE WORLD RUNNING DOWN?

Many Christians are coming to Christian Reconstructionism by
way of eschatology; some are announcing their postmillennialism,
while others adopt the language of postmillennial victory yet main-
tain that they have not abandoned their premillennialism. There is
no question that since 1979, a growing minority of fundamentalists
have abandoned their anti-historical outlook. They have begun to
call for Christian dominion, Some have recognized that Christianity
is the religion of historical optimism. They understand that if you
deny this optimism toward the church’s earthly future, you seriously
impair the spread of the gospel. Others have recognized that Chris-
tianity’s tool of dominion is biblical law. Dispensationalism is losing
its younger leaders to Reconstructionism as they choose either of two
forks in the road: postmillennial eschatology or biblical law. We
Reconstructionists are happy to get them for either reason, but our
preference is for the second road: ethics.

Defection Over the Question of Time

History is linear. It moves in a straight line, from creation to final
judgment. But it is not only linear; it is progressive. There is positive
feedback in history. Covenantal righteousness brings forth God’s exter-
nal, historical blessings (Deut. 28:1-14), while covenantal rebellion
brings forth God’s external historic curses (Deut. 28:15-68). This is
why Christians can have confidence concerning the earthly success
of the church in Aistory, What this means is that the righteous efforts
of each Christian have positive effects in the future.

This has been a popular idea. Humanists in the West have stolen
it in every generation. The problem is that Christians from time to
time abandon the very idea that the humanists have stolen: earthly
optimism. The Christians sometimes become pessimistic about the
earthly effects of their own hard work, and this leads to a “fortress
church” mentality: “Form a circle with the wagons, boys: the satan-
ists are getting closer!” They wait for God to intervene in history and
pull them out of trouble, and even more important, ou of their per-
sonal responsibility for the future of the gospel. In Appendix D, I cited
Lehman Strauss in Dallas Seminary’s journal Bibliotheca Sacra:

We are witnessing in this twentieth century the collapse of civilization.
It is obvious that we are advancing toward the end of the age. Science can
offer no hope for the future blessing and security of humanity, but instead it
has produced devastating and deadly results which threaten to lead us
