154 IS THE WORLD RUNNING DOWN?

This ethical process is two-fold: definitive (“declared by God
from outside history into history”) and progressive (“maturing by the
grace of God inside history”). The definitive declaration of God—
“Not guilty!”—is based on Christ’s perfect work in history; God
transfers Christ’s moral perfection to us at conversion. Without this,
we could not enter into God’s presence in the heavenly sanctuary,
any more than men (except for the high priest once a year) could
enter into the holy of holies and live. Yet we are not to “rest on His
laurels.” We are to work on His laurels:

For by grace are ye saved through faith; and that not of yourselves: it is
the gift of God: Not of works, lest any man should boast. For we are his
workmanship, created in Christ Jesus unto good works, which God hath
before ordained that we should walk in them (Eph, 2:8-10).

Visible Cleansing in History

We now return to the original theme concerning the progressive
sanctification of the church in history: “That he might sanctify and
cleanse it with the washing of water by the word, that he might pre-
sent it to himself a glorious church, not having spot, or wrinkle, or
any such thing; but that it should be holy and without blemish”
(Eph. 5:26-27). What can this possibly mean?

What it cannot possibly mean is that the church of Jesus Christ
will not mature ethically in history. There is progress for the church.
In fact, there can be no long-term progress in the world (“common
grace”) without progress for the church (“special grace”). The world
eats the crumbs that fall from the table of the saints,”

While God in all ages has imputed to the church the perfection of
Christ—the only basis of its standing before God—it nevertheless
matures; ethically, creedally, intellectually, and in every other way.
It begins zn principle without wrinkles, yet it also develops toward a con-
dition of no wrinkles. Meanwhile, it has wrinkles,

Will the church as a collective, covenantal organization mature unto
perfection? Paul says that it will. Will this be sin-free perfection
before the return of Christ? John’s first epistle says not: there will
always be sin and the need for confession. But Paul insists: there will
be ethical maturation in history.

What this means is that Christ’s ethical perfection is imputed to a col-

10. Gary North, Dominion and Common Grace.
