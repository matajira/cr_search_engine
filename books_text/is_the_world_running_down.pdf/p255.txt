Appendix C

COMPREHENSIVE REDEMPTION:
A THEOLOGY FOR SOCIAL ACTION*

For God so loved the world, that he gave his only begotten Son, that
whosoever believeth in him should not perish, but have everlasting life

(John 3:16).

There is no more familiar verse in the Bible in today’s evangelical
world. This is the “verse of verses” in presenting the gospel of salva-
tion to those outside the faith. [t is this verse that is supposed to con-
vey to the unregenerated the idea of the love of God. It is also the
verse which most clearly offers to man the chief incentive to believe:
eternal life. But it means much more: Christ placated God's wrath
against the world, so that God would sustain the world in history until the final
judgment.

It is common for men to point to the introductory phrase, “For
God so loved the world,” and to conclude that this verse teaches that
God sent Christ to die to save all men. The term, “the world,” sup-
posedly refers to all the souls of all men on earth. In other words,
when we speak of “the world,” we mean the aggregate of mankind.
The focus of concern is the conversion of souls, Evangelicals see
their area of personal responsibility as essentially fulfilled when they
deliver the gospel of personal salvation to the lost— salvation out of
this world. The comprehensive gospel is, in their eyes, comprehensive
with respect to souds (Christ died to save all men), but /imited with re-
spect to the effects of redemption, namely, human actions and institutions.

But men are never saved out of this world. Christ’s prayer in
John 17 is clear: “I pray not that thou shouldest take them out of the
world, but that thou shouldest keep them from the evil. They are not

*A preliminary version of this essay was published in The Journal of Christian
Reconstruction, VIU (Summer 1981); “Symposium on Social Action.” P.O. Box 158,
Vallecito, CA 95251, $7.50.

219
