2 IS THE WORLD RUNNING DOWN?

ground, or wherever it was headed. But He had to get this energy
from outside the “local system,” namely, from outside the local envir-
onment of the clothing and the bodies of the Israelites. Problem: the
Bible says nothing like this, and the kinds of miracles that take place
throughout the Bible cannot be accounted for by any such rechan-
neling of physical energy. Thus, the second law of thermodynamics
does not allow for events such as the miracles described in these
Bible passages. In short, #f the second law of thermodynamics is universally
true, then the Bible is wrong.

This is why Christians must state categorically that the rule of
the second “law” of thermodynamics is not universal. Above all, we
must defend the resurrection of Christ against the humanists who do
not want to believe it, and even if they feel compelled to acknowl-
edge its historicity, refuse to accept the Bible’s interpretation of its
meaning.? For example, did Jesus’ resurrection take place because
God rechanneled some of the sun’s energy into the tomb and then
into Christ’s dead body? How much energy (measured in “ergs”) did
it take to raise Jesus from the dead? If we could just figure out how to
tap into this same energy source, would we also be able to raise peo-
ple from the dead? We can see where this sort of reasoning leads to:
the detfication of scientific man.

Thus, the Bible testifies to the non-universal nature of the second
Jaw of thermodynamics, It is a common feature of man’s environment,
but if is not universal. Overcoming entropy from time to time is one
way that God points to His own sovereign control over history. The
second law is at most a common aspect of God’s providential order-
ing of the cosmos. It is a secondary source of physical continuity.
Continuity and discontinuity are always interpreted in reference to
each other, and the incarnation and bodily resurrection of Jesus Christ are the
greatest discontinuities in mankind’s history. In short, the historic reality of
God's miracles is a refutation of the asserted universality of the sec-
ond law of thermodynamics.

It’s a Miracle!

“It’s a miracle!” People say this when they really mean, “It’s
terrific, but completely unexpected.” But there really are miracles in

2, In the fall of 1964, T heard UCLA's Prof. Lynn White, Jr., say in the classroom
regarding the resurrection: “Maybe it did happen, We live in a world in which any-
thing can happen.” By explaining the resurrection in terms of randomness rather
than in terms of the decree of God, the humanist has denied the resurrection, even if
he accepts the historical reality of the event. The “Lact” of the resurrection of Christ's
body from the grave proves little if anything. All facts are interpreted facts.
