xxiv IS THE WORLD RUNNING DOWN?

get is Jeremy Rifkin. To strengthen the case against Rifkin, I call
into question some misguided and simplistic conclusions that many
popularizers of modern humanistic science have proclaimed in the
name of science—conclusions that Rifkin has adopted in order to at-
tack Christian orthodoxy and Western civilization, In attacking the
fundamental idea in Rifkin’s thesis —that the second law of thermo-
dynamics threatens to engulf and overcome Western civilization—I
necessarily must challenge certain aspects of the apologetic method-
ology of the Creation Science movement. This methodology rests on
an overemphasis on the second law of thermodynamics.

To repeat: because the Scientific Creationists have adopted the
second law of thermodynamics as their biggest gun in the war
against evolutionists, they have fallen into a trap: using the Fall of
man and God’s subsequent curses as the ordering principle of their
scientific theory, rather than using the resurrection of Christ and
God’s subsequent blessings. This strategy has now backfired outside
the realm of physical science, as we shall see. (I believe that a case
can also be made that it has backfired within the realm of physical
science, and I present suggestions along these lines in Appendix A.
The key problem is the proper Christian use of humanistic science’s
hypothetically uniformitarian standards.) This strategy has created
a Christian mind-set that plays into the hands of Rifkin, who also
proclaims that he, too, has adopted the second law of ther-
modynamics as the ordering principle of his radical social theory. It
should also be noted that this strategy has gained very few converts
within the ranks of the physical science profession.

What I argue throughout this book is that Rifkin’s growing popu-
larity among otherwise conservative evangelicals has been aided by
the reliance that conservative Christians have placed on “entropy” as
the number-one intellectual weapon in their war against the evolu-
tionists. Entropy, modern science informs us, is a measure of disor-
der, a disorder that does not decrease for the universe as a whole
(maybe). When entropy is not actually increasing, the total disorder
of the universe remains constant, “in equilibrium” (maybe). The
concept of entropy has an almost hypnotic effect on many Christians
who possess only a smattering of scientific knowledge.

Rifkin is an accomplished intellectual hypnotist. He has placed
“entropy” at the end of a 12-inch chain, and he swings it back and
forth in front of his Christian audience. His words are soothing:
“You are growing sleepy. You are winding down. Your mind is wear-
