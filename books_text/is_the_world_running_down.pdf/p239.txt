The Disastrous Quest for Scientific Common Ground 203

formitarian regional evolutionary processes— admittedly processes of lim-
ited duration—to modify the idea of uniformitarian, universal en-
tropy. They want this form of uniformitarianism to refute the sup-
posed “discontinuities” (anti-uniformitarianism) of the Christians:
creation, miracles, and the Noachic Flood, and above all, the dis-
continuity of final judgment.

The Christians ultimately argue for a world of final discontin-
uity. The Christians proclaim cosmic restoration at the final judg-
ment, a day that will inaugurate a new era of uniformitarianism
peace without curses. The Darwinists ultimately argue for a world of
continuity. They prefer the uniformitarian heat death of the
universe, the result of triumphant entropy, to God’s discontinuous
day of triumphant final judgment.*

Stability vs. Change

It appears that while we all need fixed reference points in order to
discuss historical change, none of us can tolerate the rule of such an
inflexible monarch. We cannot seem to discover processes in nature
that change at absolutely fixed rates over time. The Scientific Crea-
tionists want occasional miracles, the Darwinian evolutionists want
occasional catastrophes, and the theonomists want Old Testament-
New Testament changes in administration or application of God’s
fixed ethical law.

Every philosophy has to come to grips with the problem of
change within a framework of stability. The Bible teaches that God is

34, Ido not want to go into a detailed discussion of the heat death of the universe,
but there is a peculiarity of the heat death thesis. Entropy expands toward total ran-
domness as time passes, although it is difficult to explain what it is that is passing
when you define time in terms of entropy. If heat really dies, then the end result is a
temperature of absolute zero. Now we must invoke the seldom-discussed third law
of thermodynamics: at absolute zero, a system will have an entropy of zero. See
Charles W. Keenan and Jesse Wood, General College Chemistry (New York: Harper &
Row, 1966), p. 419. Here is a major antinomy (self-contradiction) of modern
science: the entropy of the universe increases as it approaches perfect randomness
and absolute zero as a limit. Then, discontinuously, it reaches its cosmic limit, and
instantaneously (time’s dying gasp) the closed system of the universe shifts from a
state of maximum entropy to a state of zero entropy, Thus, even the universe of the
evolutionists faces a great and final cosmic discontinuity. The world of perfect equi-
librium — pure randomness without direction or hope of direction —is also the world
of perfect death, absolute zero, and zero entropy. “The universe is dead. Long live
the universe.”

35. Greg L. Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg, New
Jersey: Presbyterian & Reformed, 1984), Part IV,
