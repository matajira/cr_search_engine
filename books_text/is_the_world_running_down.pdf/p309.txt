Are Postmillennialists Accomplices of the New Age Movement? — 273

Eschatology has consequences. While there are hundreds of
thousands of fundamentalists, Pat Robertson among them, who
have not yet abandoned the official doctrines of premillennialism,
they have abandoned premillennialism’s inherently pessimistic and
retreatist social conclusions. They have become activists. Thus, it
will be only for a few more years—less than a decade, I would guess
—that those fundamentalists who remain as public activists will still
cling officially to their premillennial theology. The humanists under-
stand this. Dave Hunt understands this. Millions of premillennial-
ists in the middle have not thought about this. When they do start
thinking about it, there will be an ecclesiastical rupture over the tim-
ing of the Rapture (premillennial or postmillennial), and the Chris-
tian Reconstruction movement will inherit the best and the brightest
of the younger (presently) premillennial activists.

We Reconstructionists need only to publish and wait patiently.
Our eschatological adversaries will do our work for us. The premil-
lennial activists will eventually be tossed out of their churches by
outraged premillennial retreatists, who deeply resent being called re-
treatists, pessimists, and allies of the humanists—which they most
certainly have been and continue to be. And once the activists see
what their fellow premillennialists have done to them, and why,
many of them will adopt postmillennialism. We Reconstructionists
will at last recruit the shock troops we need. At that point, the hu-
manists can kiss goodbye their monopoly over American political
life,

Dominion Theology and the New Age Movement?

Christianity is the source of the idea of progress in the history of
mankind. Other groups have stolen this vision and have reworked it
along anti-Christian lines, from the Enlightenment” to the Social
Gospel movement to the New Age movement, but this does not
mean that postmillennial optimism is the cause of the thefts. It only
means that Satan recognizes the motivating power of orthodox Chris-
tian theology. It surely does not mean that eschatological pessimism
is in any way an effective shield against humanism, New Age phi-
losophy, or socialism. Jeremy Rifkin is proof enough. He is a
pessimist who appeals for support to pessimists within the Christian
community.

26. Robert A. Nisbet, “The Year 2000 and All That,” Commentazy ( June 1968).
