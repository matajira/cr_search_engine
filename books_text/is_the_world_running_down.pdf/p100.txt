64 IS THE WORLD RUNNING DOWN?

tainly would be better than hell. But it is not better than resurrection
and eternal life for those people whom God chose before the founda-
tion of the world to regenerate (Eph. 1:4-7).

Columbia University’s astronomer Lloyd Motz gives us science’s
two options: heat death or cosmic crushing. He favors the latter, by
the way. “While it appears that the earth is safe from galactic catas-
trophes, it is not safe from the various overall cosmological events
that can, and ultimately will, bring things to an end. An end here
does not mean that all matter will disappear but rather that a situa-
tion will occur where the orderly evolution and change that a man
sees going on all around him will cease. This will happen either
because the universe has run down, like the spring of a watch, or
because it has contracted down to a tiny, but highly concentrated, bit
of matter.”

He favors the oscillating universe, as did all the pagans of the an-
cient world. Somehow, being crushed to death gives man hope, for
“man’s existence implies that life will occur over and over again, but
not precisely as it evolved in the present universe, for the normal
fluctuations that occur in all physical systems will change the initial
conditions of each new expansion phase of the universe, so that no
two such phases will be identical. Thus, men have (in their own ex-
istence) not only the promise of life renewed but also the promise of
almost infinite variety in such life.”® This is humanistic science’s
version of hope in the resurrection. This is how he hopes to escape
the curse of God, “ashes to ashes, dust to dust.” Cosmic dust will
revive itself, and it will again bring forth life.

Who knows, maybe you will someday become a dinosaur with a
high IQ! Such is the logic of the humanist who combines Darwin
and ancient man’s cyclical cosmology in order to escape the logic of
Rudolph Clausius. This is reincarnation without a belief in the
human soul. This is madness.

Conclusion
The only thermodynamics textbook I have seen that at least
points to the underlying cosmological issues is Gordon J. Van
Wylen’s. He is at least willing to ask the inevitable questions that are
raised by the equations for the second law of thermodynamics and its

5B. Motz, The Universe: Its Beginning and End, p. 305
59. Ibid., p. 317.
