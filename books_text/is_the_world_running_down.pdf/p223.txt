Appendix A

THE DISASTROUS QUEST FOR
SCIENTIFIC COMMON GROUND

‘Together with thinking of the results of science as they are offered to us
in various fields, we must think of the methodology of science. Perhaps
there is greater agreement among scientists on the question of methodology
than on the question of results. At any tate, it ts quite commonly held that
we cannot accept anything that ts not consonant with the result of a sound
scientific methodology. With this we can as Christians heartily agree. It is
our contention, however, that it is only upon Christian presuppositions
that we can have a sound scientific methodology. And when we recall that
our main argument for Christianity will be that it is only upon Christian
theistic presuppositions that a true notion of facts can be formed, we see at
once that it ts in the field of methodology that our major battle with
modern science will have to be fought. Our contention will be that a true
Scientific procedure is impossible unless we hold to the presupposition of
the triune God of Scripture, . . . The chief major battle between Chris-
Hantly and modern science is not about a large number of individual facts,
but about the principles that control science in its work. The battle today
is largely that of the philosophy of science.

Cornelius Van Til?

What I argue in this appendix will alienate a lot of my readers. I
argue that the methodologies of the Scientific Creationists and the
modern Darwinian evolutionists are remarkably similar, and that
this has compromised the Scientific Creation movement.

On the surface, this may seem preposterous, both to the Scien-
tific Creationists and the Darwinians. Nevertheless, the similarities
are there, and they should not be ignored, let alone deliberately con-
cealed from the public, The fundamental agreement concerns the doc-

1. Cornelius Van Til, Christian-Theistic Evidences (Philli
byterian and Reformed Pub. Co., [1961] 1978), pp. v

187

  

burg, New Jersey: Pres-
