146 18 THE WORLD RUNNING DOWN?

presence with His people—a slave population growing so rapidly
that it made the Pharaoh of the enslavement tremble (Ex. 1:7-10).

Even after the exodus, God told them that their numbers were
insufficient to enable them to subdue the land of Canaan all at once.
Speaking of the pagan cultures still in the land, God said: “I will not
drive them out from before thee in one year; lest the land become
desolate, and the beast of the field multiply against thee. By little
and litde I will drive them out from before thee, until thou be in-
creased, and inherit the land” (Ex. 23:29-30).

This is an extremely important passage. First, it affirms man’s
authority over land and animals. Even the morally perverse
Canaanite tribes possessed God-given authority over the works of
nature. Men, not the beasts, are supposed to subdue the earth. Sec-
ond, this passage warns God’s covenant people against attempting to
achieve instant dominion. They must first build up their numbers,
their skills, and their capital before they can expect to reign over the
creation.

Pagans possess skills and capital that are important to the contin-
uity of human dominion. Pagans can be competent administrators.
Their labor can be used by God and society until an era comes when.
God’s people are ready to exercise primary leadership in terms of
God’s law. At that point, ethical rebels will either be regenerated
through God’s grace, or else steadily replaced by the new rulers of
the land.? Until then, God’s people must be content to wait patiently,
improving their own administrative abilities and increasing their
numbers, Dominion is an ethical process, a process of self-government under
God's law.

God promised His people a specific reward for covenantal faith-
fulness: “And ye shall serve the Lorp your God, and he shall bless
thy bread, and thy water; and I will take sickness away from the
midst of thee. There shall nothing cast their young, nor be barren, in
thy land: the number of thy days I will fulfil” (Ex. 23:25-26). He pro-
mised them fealth, including an absence of miscarriages among both
humans and domesticated animals. This means that He promised
them an escape from miscarriage-producing genetic defects. In
short, He promised them an escape from an important aspect of the
curse. (Perhaps we can regard genetic “misinformation” part of the

3. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
“Yexas; Institute for Christian Economics, 1987).
