Entropy and Social Theory 89

being potentially a second Protestant Reformation.*
In summary:

1, Men’s religious views of the world influence the kinds of science they
do.
2, The theology of the Creation Science movement influences their
selection of arguments.
3. Their pessimistic eschatology has shaped their use of the second law
of thermodynamics.
4, Jeremy Rifkin has attempted to apply natural science to social
theory.
5. He has also selected the second law as the key idea in his system.
6. The evangelical world has failed to place Christ’s resurrection at the
center of its view of time, judgrnent, and society.
7. Creation Scientists emphasize Adam’s Fall, God’s curse, and the
decay of the universe.
8. Their reliance on the entropy concept has placed them at the mercy
of Rifkin,
9. The pessimism of Creation Science is based on its eschatology and
its reliance on the second law.
10. They see no long-term improvement for the world.
11. They cannot explain the improvement we have seen without calling
into question their apologetic methodology: the second law.
12. Rifkin exploits this weakness.
13. He appeals to the pessimistic eschatologies of his targeted audience,
evangelical Christians,
14, A kind of fatalism concerning the world has overwhelmed Christian
social thinkers.
15. The world is “wound back up” by miracles.
16. Obviously, entropy is not universally binding.
17. Christians have not paid sufficient attention to the healing effects of
righteousness.
18. Ethics is fundamental; entropy isn’t.
19. How can we explain Western history without a concept of progress?
20. What threatens the West is its abandonment of the gospel, not
entropy.
21. The cursed effects of entropy can and have been overcome.
22. Rifkin blames Christianity for the crises of the West.
23. He rejects the biblical idea of dominion by covenant.
24. He ignores the evidence presented by other scientists that Christian-
ity isn’t to blame.

44. Ibid., Introduction.
