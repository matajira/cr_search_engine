242 IS THE WORLD RUNNING DOWN?

ration was promised to Israel, whereas no restoration was promised
to the surrounding pagan cultures. The key differentiating factor is
restoration.

What God’s prophets prayed for and announced was judgment
unto restoration. When the culture had departed so far that men had
forgotten God, God struck them down. “Woe unto them that are at
ease in Zion,” the prophet Amos announced (Amos 6:1), and it is this
warning that is supposed to awaken the sleepwalking members of
His congregations. Judgment is one effective way to awaken them,
to relieve them of their ease.

What are the basic forms of judgment? There are many, Deuter-
onomy 28 lists several: geographical (v. 16), financial (v. 17), agricul-
tural (v. 18), pestilential (v. 21), drought (vv, 23-24), military (v, 25),
dermatological (v. 27), psychological (vv. 28, 66-67), medical (vv.
60-62), demographic (v. 62). The general curse: “And it shall come
to pass, that as the Lorp rejoiced over you to do you good, and to
multiply you; so the Lorn will rejoice over you to destroy you, and
to bring you to naught; and ye shall be plucked from off the land
whither thou goest to possess it. And the Lorn shall scatter thee
among all people, from the one end of the earth even unto the other;
and there thou shalt serve other gods, which neither thou nor thy
fathers have known, even wood and stone. And among these nations
shalt thou find no ease . . .” (vv, 63-65a.). The scattering process
was designed to provide them with a most practical education in
comparative religion. They would learn what it means to be a ser-
vant of a foreign god.

Judgement Is Comprehensive

This is the lesson of Deuteronomy 28. Judgment is comprehen-
sive because sin and rebellion are comprehensive. Sin and rebellion
infect every area of life. Satan is at work everywhere. He offers a
challenge to God wherever he can. Because God requires His ser-
vants to exercise dominion in every area of life, across the face of the
earth (Gen. 1:26-28; 9:1-7), His law is comprehensive. Rebellion
against His law is also comprehensive.

If judgment is comprehensive, then in order for men to avoid
comprehensive judgment, they must repent. This repentance must
be as comprehensive as the sins had been during the period of rebel-
lion. This also means that the standards of reconstruction must be
comprehensive. If men are repenting in general, then they must be
