The Unnaturalness of Scientific Natural Law 17

almost singlehandedly shattered the worldview of Newtonian science
~shattered it so completely that even Einstein, who later tried to
pick up the pieces, could not put them together again.”

Newton’s view of the universe was the product of Christian pre-
suppositions. As those presuppositions have been borrowed and
then abandoned by modern, officially atheistic or religiously skep-
tical scientists, this view of natural law has become increasingly un-
acceptable to scientists, as I hope to indicate in this chapter. Thus,
any appeal to scientific natural laws as a defense of the Christian
faith will prove increasingly useless, and has proved nearly useless
for three decades. Modern scientists have steadily abandoned any
reliance upon the traditional concept of natural law— itself an un-
stable mixture of Greek philosophical speculation and Christianity"
~precisely because they have self-consciously abandoned Christianity.

This is why any attempt to refute evolutionists by an appeal to
the second law of thermodynamics will inevitably backfire. The pop-
ularity in Christian circles of the writings of Jeremy Rifkin indicates
that this appeal has already backfired. In short, it is a waste of effort
to attempt a scientifically acceptable refutation of twentieth-century
physical science by means of an argument based on nineteenth-cen-
tury concepts of physical cause and effect.

I realize that at this point, most of my readers will not fully
understand what I am saying. I have therefore decided to prove my
case in Chapter One. Unfortunately, much of the material I use to
prove my case is somewhat technical. So to help everyone get
through it, I recommend that each reader put these words in the
back of his mind: “These scientists must be crazy.” You do not need
to remember every argument. All you need to do is remember my

10. Though I am not sufficiently competent academically to judge the accuracy of
the following book, let me at least suggest that Christian physicists would find in-
triguing the little-known published critiques of Einstein by Herbert Eugene [ves
(1882-1953), collected and published in one volume, The EINSTEIN Myth and the Ives
Papers: A Counter-Revolution in Physics, edited by Richard Hazlitt and Dean Turner
(Old Greenwich, Connecticut: Devin-Adair, 1979). This could be a dead end, but at
Jeast it is a place to begin rethinking the shattered universe that Einstein bequeathed
to mankind.

11. Archie P, Jones, “Natural Law and Christian Resistance to Tyranny,” Christi-
anity and Civilization, 3 (1983); Jones, “Apologists of Classical Tyranny: An Introduc-
tory Critique of Straussianism,” Journal of Christian Reconstruction, V (Summer 1978);
Rex Downie, “Natural Law and God’s Law: An Antithesis,” Journal of Christian
Reconstruction, ibid.
