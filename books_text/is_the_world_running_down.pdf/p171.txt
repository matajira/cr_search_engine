Mysticism vs. Christianity 135

with nature. We are to integrate ourselves into the web of nature. “In
the new scheme of consciousness, security is achieved by becoming
an integral participant in the larger communities of life that make-
up [sic] the single organism we call earth. Security is no longer to be
found in self-contained isolation but rather in shared partnership
with the creation. Security is not to be found in dominating and
manipulating, but rather in reweaving ourselves back into the web
of relationships that make up the earthly and cosmic ecosystems.”

We see here the truth of the statement that men either become
subordinate to the God of the Bible and dominant over nature, or
else they rebel against God and become subordinate to nature.
Rifkin goes so far as to revive the idea that the universe is itself pur-
poseful, “a mind pulsating with purpose and intention. . . . In this
way one eventually ends up with the idea of the universe as a mind
that oversees, orchestrates, and gives order and structure to all
things.”'6 Only Christianity, which preaches God as the purposeful
source of history and meaning, is more hated by Darwinian scien-
tists than this idea of purposeful nature. It represents a revival of
pantheism. It also represents a revival of the outlook of ancient
paganism: nature, not man, as an autonomous source of decisions.

To end our long, self-imposed exile; to rejoin the community of life.
This is the task before us. It will require that we renounce our drive for
sovereignty over everything that lives; that we restore the rest of creation to
a place of dignity and respect. The resacralization of nature stands before
us as the great mission of the coming age.”

He calls for the re-sacradization of nature. But nature is not sacred,
nor was it ever intended by God to be sacred. Only those who wor-
ship the creature rather than the Creator would argue for re-
sacralization. Rifkin wants us to adopt the cosmology of Eastern
mysticism, but in the name of a “new” Christianity. He sees that a
revival of Christianity in the latter days of the twentieth century
seems likely, and he knows that the widespread adoption of Eastern
religion is unlikely. As a non-Christian who sees that the statist, ex-
pansionist, and above all, rationalist world is losing its adherents,®

18. Bid., p. 90.

16. Rifkin and Perlas, Algeny, p. 195.

17. Bid., p. 252.

18. Rifkin, Emerging Order, p. 89.

19. Ibid., ch. 1: “An Establishment in Crisis.”

 
