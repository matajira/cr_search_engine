322 IS THE WORLD RUNNING DOWN?

ferring our private experience of empathy into public policy, we
begin a new time journey, one in which temporal awareness is used
to empathize with the future. In this new temporal world, time poli-
tics becomes empathetic politics.”*1

He is unfortunately silent about how we might transfer his vision
of a new time politics into a specific political program. I suppose his
first step will be to mount a grass-roots campaign against daylight
savings time, which is clearly an assault of commercial time against
natural time, and furthermore it was an invention of that capitalist
time master, Ben Franklin. Never forget, “In an empathetic time
world, our reality conforms to nature’s.”#? Then we will no doubt
need a ban against imported digital wristwatches. After that, it gets
vague. How about local laws against beepers in public places?

To conduct a political transformation, you need constituents.
You also need organizational allies. So, who are the representatives
of this new empathetic politics? They are all those crazies who began
to grab for political power in the late 1960’s, meaning Rifkin’s old
constituencies. “Many new movements have emerged in recent
years, each embracing aspects of the empathetic time vision. The
environmental movement, the animal-rights movement, the Judeo-
Christian stewardship movement, the eco-feminist movement, the
holistic health movement, the alternative agriculture movement, the
appropriate technology movement, the bio-regionalism movement,
the self-sufficiency movement, the economic democracy movement,
the alternative education movement, and the disarmament move-
ment come readily to mind.” They come readily to whose mind?
Have you ever seen one piece of literature, one single booklet, from
any of these screwball movements that argues that the primary (or
even secondary) reason for its existence is the world’s desperate
need for a new political order based on a totally new view of time?
No? Neither have I. And neither has Rifkin. He cites no evidence.
He just lists his hoped-for organizational allies.

One thing is sure: with this witches brew of a list, he has unoffi-
cially abandoned his expressed hope in The Emerging Order (1979) of
capturing the evangelical movement and the charismatics. His
future does not lie with the “Pat Robertson for President” crowd. So,
he has returned to his old haunts, seeking to enlist at least part-time

41, Idem.
42. Ibid., p. 210,
43. Ihid., p. 206.
