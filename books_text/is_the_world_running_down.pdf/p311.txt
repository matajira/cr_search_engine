Are Postmillennialists Accomplices of the New Age Movement? 275

Now you would say, boy, that’s a pretty hopeless thing, well, but Peter
didn’t say that. He said, “Seeing that these things will all be dissolved, what
manner of persons ought you to be in all holy conversations and godliness?”
He said, “The day of the Lord is coming in which the heavens will be rolled
up like a scroll. The elements will melt with a fervent heat,” and so forth.
And éhat in fact, Peter says, ought to motivate us to holy living, to turn
totally from this world, from the materialization and all of the ambitions,
and so forth, to a hope in the heavenlies, in a new creation, and it ought to
motivate us to godiiness. But these people are saying “no, the motivation we
need is the desire to build, to reconstruct planet earth, to realize that
ecologically we got problems.” I mean we should be concerned about all
that. 'm not denying that, but that’s not our hope; that’s not the primary
goal of the church: social transformation. But the primary goal is to save
souls, and to bring men to the cross of Jesus Christ, and I feel—I don't fecl,
T’'m convinced—that the kingdom-dominion teaching is playing into the
hands of the very lie that turns us from the cross and from the gospel and
the true solution to a humanistic idea, but all done in the name of Jesus
Ghrist, and for good cause.

Hunt tries to protect himself. He makes an off-hand remark re-
garding ecology: “I mean we should be concerned about all that. I’m
not denying that. . . .” Baloney,. He is not concerned in the slightest.
He shares this lack of concern (and lack of specifics, Bible-revealed
answers) with dispensationalists in general. We are still waiting for
the first study of “ecology and the Bible” written by any dispensation-
alist, with Bible-based answers to ecological questions. I devote
more space to this topic than to any other in my third volume on the
Book of Exodus, Tools of Dominion. Dispensationalists have written
nothing about ecology because: 1) they would have to use Old Testa-
ment law to deal with the question, which their theology catego-
rically rejects; and 2) they see no possibility of cleaning up the earth
before the Rapture. The very idea of cleaning up the earth is a so-
cialisttc New Age deception, in Dave Hunt’s view, as we have seen.
He is quite specific about the link between the New Age movement
and ecology:

But forgetting that for the moment, people will say, “Well I mean, you
know, whether we are going to be taken to heaven, or whether the kingdom
is on this earth, or, you know, whether we are going to be raptured, or
whether we are not going to be raptured, those are future events. Let’s not

29. Dominion and the Cross, Tape #2, of Dominion: The Word and New World Order.
