The Pessimism of the Scientists 43

The Christian version of the escapist religion is sometimes called
“pietism,” but its theological roots can be traced back to the ancient
heresy of mysticism. Rather than proclaiming the requirement of ethi-
cal union with Jesus Christ, the perfect man, the mystic calls for meta-
physical union with a monistic, unified god. In the early church, there
were many types of mysticism, but the most feared rival religion
which continually infiltrated the church was gnosticism. It proclaimed
many doctrines, but the essence of gnostic faith was radical individ-
ualism. It involved a self-conscious retreat from the material realm
and escape to a higher, purer, spiritual realm through techniques of
self-manipulation: asceticism, higher consciousness, and initiation
into secret mysteries. Gnosticism survives as a way of thinking and
acting (or failing to act) even today, as R. J. Rushdoony has pointed
out. The essence of this faith is its antinomianism—anti (against)
nomos (law). Gnostics despise the law of God. But their hatred for
the law of God leads them to accept the laws of the State.

Rushdoony has commented on the persistance of gnosticism
throughout Western history right up to the present. A major feature
of gnosticism is the gnostics’ contempt for time, their unwillingness
to try to change external events. Their exclusive concern was salva-
tion of the individual and escape from the external world. In some
cases, they even had contempt for the material world, as well as for
morality, as Rushdoony notes:

Gnosticism survives today in theosophy, Jewish Kabbalism, occultism,
existentialism, masonry, and like faiths. Because Gnosticism made the indi-
vidual, rather than a dualism of mind and matter, ultimate, it was essentially
hostile to morality and law, requiring often that believers live beyond good
and evil by denying the validity of all moral law. Gnostic groups which did
not openly avow such doctrines affirmed an ethic of love as against law,
negating law and morality in terms of the “higher” law and morality of love.
Their contempt of law and of time manifested itself also by a willingness to
comply with the state... . The usual attitude was one of contempt for the
material world, which included the state, and an outward compliance and
indifference. A philosophy calling for an escape from time is not likely to in-
volve itself in the battles of time.?

The basic idea lying behind escapist religion is the denial of the
dominion covenant. The escape religionist believes that the tech-

3. Rousas John Rushdoony, The One and the Many: Studies in the Philosophy of Order
and Ultimacy Fairfax, Virginia: Thoburn Press, [1971] 1978), p. 129.
