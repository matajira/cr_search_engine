Dominion and Sanctification 153

has meaning. We must be content with this apparent contradiction
between original perfection and historical development.

We must respect the limitations of our own minds when we en-
counter any variation of the “full bucket” problem. We know that
God is perfectly glorious. We also know that history has meaning.
We dare not say that the creation, and especially man, adds some-
thing to God (Western mysticism, e.g., Meister Eckhart), yet we
also dare not say that history is irrelevant to God and man (Eastern
mysticism, e.g., the concept of maya, or the illusion of the material).
God is glorious, and history has meaning and pleases God. God does
everything perfectly, yet man has legitimate work to do. We must
affirm both,

Sanctification: Both Definitive and Progressive

So it is with Christ's ethical perfection. He was perfect man and
fully God at birth, yet His life, death, resurrection, and ascension
were not meaningless. Christ possessed an original perfection, yet He
also experienced a historically maturing perfection. He began with per-
fection appropriate to an infant; he matured to perfection ap-
propriate to an adult.

We all are born in sin, and we all die in sin. Yet some people are
saved, while others are lost. How can we make sense of this?

All regenerate people are made perfect in the sight of God
through the perfection of Christ which is imputed (“declared”) to
them. Yet all regenerate people continue to sin.

If we say that we have no sin, we deceive ourselves, and the truth is not
in us. If we confess our sins, he is faithful and just to forgive us our sins,
and to cleanse us from all unrighteousness (I John 1:8-9).

Here it is again: cleansing. We never escape the taint of sin, yet
God provides a way for us to cleanse ourselves ethically before Him.
Christ’s perfection is imputed to us. God declares us “Not guilty!” at
the time of our regeneration. Yet He also offers us a way to receive
confirmation of that same declaration, day by day, as we sin, confess
our sin, and go back to work,

Our limited goal, obviously, is the steady reduction of stn in our
lives. The ultimate goal is moral perfection. Yet we never achieve this
in history. All we can do is mature in righteousness. We do not evolve
into perfection. We certainly do not evolve into God, But we are sup-
posed to mature ethically in history.
