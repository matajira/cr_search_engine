The Question of Ethics 123

rule bodily during the millennium (traditional premillennialism).
The possibility of progressive restoration as God’s historical down
payment on full restoration beyond the grave and the resurrection
has not been taken seriously in the twentieth century. Thus, with
respect to the period of history prior to Christ’s bodily return, Chris-
tians have tended to agree with a vision of the future that parallels
Rifkin’s: a future without serious potential for peace, prosperity,
growth, and the beating of swords into ploughshares. In fact, some
Christians have argued that anyone who comes in the name of God
with such a message of earthly optimism has adopted the social and
economic outlook of the Antichrist.

What we need to argue as Christians is that the gospel possesses
power to transform men, and if it can transform men, then it can
also transform men’s social and economic relationships. The eco-
nomic and technological conditions that Rifkin describes —a social
world governed by entropy —is a clear denial of the power of the gos-
pel. The resurrection of Christ points to the overcoming of entropy’s
curses.

But what about history? Scientific Creationists have argued from
the beginning that physical entropy is a universal phenomenon — the
law of physical science. If they are correct, then how can Rifkin be
incorrect, for he bases his worldview on the second law of ther-
modynamics?

Now, the main argument Rifkin uses — that entropy points to the
heat death of the universe — has nothing to do with mankind’s social
arrangements. By the time the heat death of the universe arrives,
man will not be around to shiver. Social arrangements are concerned
with life, not death.

What about the theology of entropy? The point made by the
Creation Scientists is that the origin of life itself is a contradiction of
entropy. Life must have come before the second law of ther-
modynamics appeared. In fact, the whole universe must have been
in place. What we have experienced since then is a reduction in
order, not an increase. Thus, there could never have been evolution.

The problem for Christians who adopt this argument is that the
language of decline is so powerful. What about social institutions?
What about the extension of longevity through science? What about

15. Dave Hunt, Peace Prosperity and the Coming Holocaust (Eugene, Oregon: Harvest
House, 1983). Mr. Hunt apparently dislikes commas in his book titles.
