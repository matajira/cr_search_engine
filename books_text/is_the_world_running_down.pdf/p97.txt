The Pessimism of the Scientists 61

please. The universe is a closed system. By humanist definition, it
must be a closed system.

Jaki has identified the source of Engels’ animosity to Clausius.
“Clausius, entropy, and the heat-death of the universe meant one
and the same thing for Engels. They represented the most palpable
threat to the materialistic pantheism of the Hegelian left for which
the material universe was and still is the ultimate, ever active reality,
Engels made no secret about the fact that the idea of a universe
returning cyclically to the same configuration was a pivotal proposi-
tion within the conceptual framework of Marxist dialectic. He saw
the whole course of science reaching in Darwin’s theory of evolution
the final vindication of the perennial recurrence of all, as first advo-
cated by the founders of Greek philosophy.”

So we find that poor Professor Bazarov must reject Clausius’
theory of heat death, and worse, that he must cite Engels as his justi-
fication. He notes that “the reactionary views of Clausius have been
the subject of Engels’ crushing criticism.”#9 He then cites “material-
ist” Boltzmann’s theory of fluctuations as a possible alternative to
Clausius, reproducing a section from Lectures on Gas Theory.° But he
then rejects the heart of Boltzmann’s theory, namely, the existing
equilibrium of the universe.» He offers no resolution to the problem.
He uses two arguments that have gone nowhere in this century: 1)
that the thermodynamic principles that apply to a laboratory experi-
ment do not apply to the universe as a whole (an approach taken by
the physicist Ernst Mach in the late nineteenth century, in contradic-
tion to his own theory of the gravitational influence of the whole uni-
verse on all parts)5? and 2) the appeal to some sort of statistical for-
mula escape hatch, without a description of the physical processes
that would make the statistical solution possible (Boltzmann’s ap-
proach). [t is an oddity of history that Boltzmann killed himself in
1906 because other physicists kept clinging to Mach’s soon to be out-
moded anti-atomism theory,®* yet they both unsuccessfully opposed
Clausius.

48. Jaki, Science and Creation, p. 312.

49, 1, P. Bazarov, Thermodynamics (New York: Macmillan, 1964), p. 76, The type-
face of this book is the familiar style used only by the English-language division of
Moscow’s publishing operation. It is obvious that Macmillan simply photocopied
the book and published it in the United States.

50. Ibid., p. 77.

51. Ibid., p. 78.

52. Jaki, pp. 297-98.

53. John T. Blackmore, Ernst Mack: His Work, Life, and Influence (Berkeley: Uni-
versity of California Press, 1972), ch. 13.
