Dominion and Sanctification 151

need here to defend this theology in detail. God is God; man is not.

But if Christ was born a perfect human being, why did He have
to suffer and die? Because He was our office-bearer. He suffered the
punishment that sinful men deserve, so that they can escape it.

He lived a perfect life. While He began perfect, unstained by
Adam’s original sin, He nevertheless had to work out His perfection
in fear and trembling: praying, shedding tears, and doing His
Father’s will in history. His perfection was a demonstrated perfection in
history. It was not a perfection beyond history, it was perfection within
the confines of history. It was perfection that left evidence behind.

And many other signs truly did Jesus in the presence of his disciples,
which are not written in this book: But these are written, that ye might
believe that Jesus is the Christ, the Son of God; and that believing ye might
have life through his name (John 20:30-31).

So, Jesus began perfect, and He matured this perfection. He was
not more perfect ethically at His death than at His birth, yet He was
required by God to walk the highways of Palestine, performing
miracles, confronting His opponents, training His disciples, and
then dying on the cross. We dare not say that He was more perfect
ethically at His death than at His birth, for perfection is perfection;
it cannot be added to. Yet we also dare not deny that His perfection
matured in history, giving evidence of what a righteous walk before
God should be. Thus, Paul wrote: “Be ye followers of me, even as I
also am of Christ” (I Cor. 11:1), Christ, as revealed in the Bible, is the
only appropriate model for men to imitate.

For whom he did foreknow, he also did predestinate to be conformed to
the image of his Son, that he might be the firstborn among many brethren
(Rom, 8:29).

And be not conformed to this world: but be ye transformed by the

renewing of your mind, that ye may prove what is the good, and accept-
able, and perfect, will of God (Rom. 12:2).

So, Jesus went from perfection to perfection. This is a variation
of what Cornelius Van Til calls the “full-bucket problem.” God was
perfect before He created the world. He did not need the world, as
heretical mystics have proclaimed for millennia. A perfect Being has
no needs that men can satisfy. Such a Being has no needs other than
selfcommunion. Nevertheless, He created the world for His glory.
