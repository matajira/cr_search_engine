Christianity vs. Social Entropy 105

that the universe is running down to the timeless oblivion of heat
death. Nevertheless, only Bertrand Russell and a few hard-core evo-
lutionists have fearlessly discussed the grim implications of this
pessimistic claim. Thus, Rifkin adopts a very clever tactic: to pro-
mote optimism by means of a system which guarantees mankind the
ultimate defeat of meaninglessness, impotence, and death. To this
extent, Rifkin is epistemologically schizophrenic: he cannot decide
whether to adopt Western entropy-based pessimism or the escapist
bliss of anti-rational, anti-scientific Eastern mysticism.

  

False Optimism

Riftkin’s language is unquestionably religious in tone. “This book
is about hope: the hope that comes from shattering false illusions and
replacing them with new truths.”4 But where is this hope? In what
does it consist? The sober analysis of Bertrand Russell was more
honest: “The same laws which produce growth also produce decay.
Some day, the sun will grow cold, and life on the earth will cease.
The whole epoch of animals and plants is only an interlude between
ages that were too hot and ages that will be too cold. There is no law
of cosmic progress, but only an oscillation upward and downward,
with a slow trend downward on the balance owing to the diffusion of
energy. This, at least, is what science at present regards as most
probable, and in our disillusioned generation it is easy to believe.
From evolution, so far as our present knowledge shows, no ulti-
mately optimistic philosophy can be validly inferred.”5

Russell’s viewpoint is consistent. Its innate pessimism is straight-
forward, Whenever we encounter a philosophy that emphasizes the
entropy process, we will find a dead end at the end of history —ulti-
mately, the destruction of history. The universe grows cold. All
directional movement ceases. There is no way to measure change
over time, because there is no longer any non-random change to
measure, nor any living being to measure it. The concept of time it-
self becomes meaningless. Time ends.

Intellectual Schizophrenia

Rifkin’s book evades the obvious implications of his entropy
process, He cannot decide which philosophy he wishes his readers to

4, Rifkin, “Author's Note,” Entropy.
5, Bertrand Russell, “Evolution,” in Religion and Science (New York: Oxford Uni-
versity [1935] 1972), p. 81.
