Time for a Change: Rifkin’s “New, Improved” Worldview 313

major change in how businesses could be operated. It advanced the
precision of work schedules, making possible new production proc-
esses. This was comparable to the magnitude of social change
wrought by the advent of the train in comparison to the stagecoach.
Then came the wristwatch. This was the private automobile of time
measurement—decentralized, inexpensive, mass produced, life-
changing. The impact that the wristwatch had on our lives in com-
parison to the town clock’s chimes or factory whistle was very impor-
tant so important that it seems safe to conclude that it was far more
important than any future technique in measuring Auman time will
ever be. Improvements in watches have been made steadily, but
their effects have been marginal in terms of the changes in our lives
that they have wrought. A wristwatch that does not require winding
is an improvement over one that does; one that has a little beeper
alarm is also an improvement (except when it goes off at 12 noon on
Sunday when you are sitting in church), but that beeper is not very
significant in changing the way we work or live our lives. We even
have a phrase for describing merely marginally useful improvements
in technology: “bells and whistles.” It applies very well to today’s im-
provements in measuring time.

To argue that time-measurement technology has significantly
changed our lives in this century is simply ridiculous. Rifkin grabs
for any fact that might prove his case, and his desperation shows. He
says that in 1927, Emily Post said that a widow should wait for three
years before remarrying. By 1950, this had shrunk to six months. To-
day, Amy Vanderbilt says that the widow should resume the usual
social course within a week. !* He implies, but never is so foolish as to
say, that this speeding up is in some way the product of improved
time measurement. If he ever said this outright, the howls of derisive
laughter would overwhelm him, Did the self-winding wristwatch
shorten the widow's mourning period to six months? Has the quartz
watch or the light-emitting diode watch now reduced it to a week?

In Puritan days, when life was shorter, harder, and more costly
than today, widows were approached by suitors within a few weeks
of the funeral of dear departed husbands. The widow inherited at
least a third of her husband's land, plus whatever was being held in
trust for his children. She could not work it well by herself; there
were always unmarried men who wanted land, for it was income-

18. Time Wars, p. 5.
