Introduction 5

these topics. This is the century-old clash between some variant of
Darwinism and creationism. There is no biblically acceptable way to soften
this confrontation. There can be no “smoothing over of differences.” The
two systems are incompatible. This is not a case of semantic confu-
sion; this is a case of all-out intellectual war. It is at root a war between
rival religious worldviews, rival religious presuppositions concerning
God, man, law, and time. We must heed Van Til’s warning:

In the first place, Christian theism must be defended against non-
theistic philosophy. We have sought to do this in the course in apologetics.
In the second place, Christian theism must be defended against non-theistic
science, . . . Christianity is an historical religion. It is based upon such
facts as the death and resurrection of Christ, The question of miracle is at
the heart of it. Kill miracle and you kill Christianity. But one cannot even
define miracle except in relation to natural law. Thus, we face the question
of God's providence. And providence, in turn, presupposes creation. We
may say, then, that we seek to defend the fact of miracle, the fact of provi-
dence, the fact of creation, and therefore, the fact of God, in relation to
modern non-Christian science.®

The Intolerance of the Darwinists

The Darwinists have been much more consistent about the “no
exceptions” nature of their position than the Christians have been.
Those scientists who go into print in order to set the terms of accept-
able scientific discourse on this crucial question of origins have defined
creationism out of the debate. The comment by Harvard
University’s Stephen Jay Gould is typical: “As in 1909, no scientist or
thinking person doubts the basic fact that life evolves. Intense
debates about how evolution occurs display science at its most excit-
ing, but provide no solace (only phony ammunition by willful distor-
tion) to strict fandamentalists.”” In addition to teaching paleontol-
ogy, Gould also writes a monthly column in Scientific American. He is
representative of mainstream public science.

Even less temperate are the remarks of Ganadian Michael Ruse:

. .. [believe Creationism is wrong: totally, utterly, and absolutely wrong.
I would go further. There are degrees of being wrong. The Creationists are

6. Cornelius Van Til, Christian-Theistic Evidences (Phillipsburg, New Jersey: Pres-
byterian and Reformed Pub. Oo., [1961] 1978), p. vii. I am quoting from the
original edition, because a line has been dropped from the 1978 edition,

7. Stephen Jay Gould, Hen’s Teeth and Horse's Toes: Further Reflections in Natural His-
tary (New York: Norton, 1983), p. 14.
