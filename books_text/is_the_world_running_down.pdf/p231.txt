The Disastrous Quest for Scientific Common Ground 195

Morris could easily argue that this assumption is not warranted
by the inherent presuppositions of Darwinian science regarding
cosmic change. How does the flux of nature’s processes produce
fixed laws? How does anything remain fixed in a world of change?
Therefore, Darwinian science really cannot assume what it needs in
order to be scientific: fixed Jaws. But then Young could respond that
Scientific Creationists are only part-time believers in fixed laws, for
they believe in miracles ~ God's intrusions in history that temporar-
ily overcome fixed laws, indicating that such laws are not actually
fixed. They are fixed, “except when God interferes.”

We are now dealing with that age-old philosophical problem of
structure and change, of law and flux. Christian philosopher Cor-
nelius Van Til argued throughout his career that the mind of self-
proclaimed autonomous man cannot find an answer to this problem.
Either stability is destroyed by change, or else change is really an il-
lusion, and there is only the unity of stability, This is why Creation
Scientists and Darwinian evolutionists go from one position to the
other, from historical change to eternal law, to defend their respec-
tive systems, and ultimately each position is held by means of a
logic-destroying dialectical tension. This is why the debate over
evolution and creation cannot be solved by an appeal to the “brute
facts” of “autonomous science.”

This is also why the apologetic method (empiricism, the appeal
to supposed common facts) of the Scientific Creationist movement is
flawed. It appeals to a hypothetical common logic of man as if such a
common ground existed. But men interpret all facts in terms of their
religious presuppositions, The inability of either side to convince its
opponents testifies to the futility of the search for common facts,
common theories, or common anything else. The futility of the at-
tempt to get Creation Science into the public schools has not been
recognized by these dedicated men, despite twenty years of failure in
the courts and the state textbook committees, precisely because they
still really believe in the myth of scientific neutrality: the appeal to a
commonly shared body of scientific opinion. There is no shared body
of opinion. There is only warfare over whose all-or-nothing system
will be used in the classroom. The Darwinists, being more consistent,
have triumphed. They will not tolerate equal time for God.

16. On this common ground aspect of the Creation Research Society, see Walter
Lammerts’ introduction to the first Annual (1964) of the CRS.
