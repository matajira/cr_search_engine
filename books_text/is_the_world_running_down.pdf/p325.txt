The Division within Dispensationalism 289

Dispensationalism’s Fig Tree Problem

The dating of “the terminal generation” by pretribulational dis-
pensationalists for three and a half decades, 1948-1981, had been tied
to the creation of the State of Israel in 1948. Hal Lindsey wrote in
1970: “The general time of this seven-year period [the future tribula-
tion period] couldn’t begin until the Jewish people re-established
their nation in their ancient homeland of Palestine.”"” In his list of
proofs that prophecy is being fulfilled in our day, the first one he lists
is the return of the dispersed Jews to Israel.® But there is a major
theological problem with such so-called prophetic fulfillments: they
cannot possibly be going on today, according to original dispensa-
tional theory, The “clock of prophecy” stopped with the coming of the
church, pretribulational dispensationalism always insisted. Only
with the “69th week of Daniel,” after the rapture, will it begin ticking
again. The Church Age was never predicted in the Old Testament;
it is “the great mystery”; the Church Age is the “great parenthesis” in
Old Testament prophecy. The dispensational author Harry Ironside
wrote a whole book on the topic, The Great Parenthesis. The entire dis-
pensational system, with its self-conscious rejection of the continuing
validity of Old Testament law, from the beginning rested on the
crucial presupposition that the New Covenant church was a radical
prophetic break from Old Covenant Israel.° As Charles Ryrie
wrote, “If the Church is not a subject of Old Testament prophecy,
then the Church is not fulfilling Israel’s promises, but instead Israel
herself must fulfill them and that in the future.” “The Church is a

17. Late, Great, p. 42.

18. Hal Lindsey, There's a New World Coming: A Prophetic Odyssey (Santa Ana, Cali-
fornia: Vision House, 1973), p. 81.

19. The “ultradispensationalists” point out that since Peter cited Joel 2 as being
fulfilled at Pentecost (Acts 2), Pentecost was obviously a fulfillment of prophecy.
They quite properly conclude that Peter could not have been starting today’s
“Church Age” New Testament church, but rather an interim Jewish church. Paul
alone started the New Testament gentile church, they argue. They then deny water
baptism along with everything in Acts 1-8 (or al of Acts, say some ultradispensational-
ists who alsa reject the Lord’s Supper}. There is ne consistent dispensational answer to
this problem, and Charles Ryrie’s desperate attempts to overcome it reveal just how
exegetically bankrupt the dispensationalist system has always been. Ryrie appeals to
the covenant theologians’ arguments for continuity in order to refute the ultradispen-
sationalists, and to the ultradispensationalists’ arguments for discontinuity in order to
refute the covenant theologians. It is an embarrassing performance. Charles Caldwell
Ryrie, Dispensationalism Today (Chicago: Moody Press, 1965).

20. Charles Caldwell Ryrie, The Basts of the Premillennial Faith (Neptune, New
Jersey: Loizeaux Brothers, [1953] 1972), p. 126.
