The Question of Ethics 129

 

theory of “life increases entropy” thesis: “To a local ‘open’ system,
directly applicable in most situations and always applicable as a nor-
mal tendency in the system, with exceptions possible only under cer-
tain special conditions as described elsewhere, and then only at the
cost of offsetting external conditions which maintain the integrity of
the two laws in the universe as a whole,”# Thus, it is easy for un-
suspecting Christians who have been influenced by the Scientific
Creationists’ apologetic to be almost hypnotized by Rifkin’s rhetoric.
They may take seriously his zero-growth economy in the name of a
moral imperative to reduce the “entropic” impact of God’s curse.

The biblical response is to call mankind to a self-conscious over-
coming of the effects of entropy through technology, science, free-
dom, and self-government under God and God’s law. Paraphrasing
Patrick Henry, “If this be premeditated assault on the dying cosmos,
make the best of it!”

In summary:

1. Humanists are in rebellion against time.

2. Man’s problem is ethical.

3. Christian social theory is based on the idea of a fixed ethical
order above yet within history.

4. The entropists argue that the only fixed aspect of the historical
process is the second law of thermodynamics,

5. Humanists say that ethical principles evolve with nature itself.

6, Christians say that the universe is open upward to God, since
we can appeal to God for justice within history.

7. Darwin's universe is an ethically closed box: no appeal to any-
thing outside nature and history.

8. The Christian sees the curses of entropy as temporary, capable of
being overcome partially in history and totally after the fina] judgment.

9. The Darwinists see no escape from entropy’s destructive
effects.

10. Thus, Darwinism’s vision is one without long-run hope.

11. Life for Rifkin becomes a grim holding action in the face of
cosmic disintegration.

12. Rifkin hates all signs of growth, for he believes that mankind's
growth and order come at the expense of increased entropy elsewhere
in the environment.

13, The microcosm (human civilization) reflects the macrocosm
(entropy’s destruction).

21. Ubid., p. 122.
