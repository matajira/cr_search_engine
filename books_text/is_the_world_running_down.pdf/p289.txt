Comprehensive Redemption. A Theology for Social Action 253

The Christians have transferred power to the humanists because
the humanists long ago recognized that power flows in the direction
of those who exercise responsibility, And when you can get the ma-
jority to subsidize the program, while turning its administration over
to you and your accomplices, you have pulled off a major coup. That
is precisely what the humanists did, and are still doing. Just con-
vince the Christians that the State, rather than the church or other
Christian voluntary institutions, is responsible for the care of the
poor, the education of the young, the care of the aged, the womb-to-
tomb protection of the least productive members of society, etc., and
you can get them to finance the religion of secular humanism with
their own tax money.

This has many important benefits for humanists. First, the
humanists control the institutions that certify competence (universi-
ties, colleges, accreditation boards). This means that only those peo-
ple screened and certified by them will get the jobs. Second, the
humanists believe in salvation by politics, so more of their efforts will
be devoted to the capture and control of the State and all State-
subsidized institutions. Third, the humanists are long-term build-
ers, since they have no faith in the after-life. In their theology, “what
you see is what you get,” and all they see is life on earth. Fourth, by
taxing Christians, they reduce the amount of money left to Chris-
tians for the financing of Christian social institutions — the alterna-
tives to the State’s institutions. Incredibly, the vast majority of Chris-
tian voters believe that this system is not only justified, but that it is
the very best system possible. They rarely protest. They limit their
protests to feeble, misguided, and ineffective efforts to “win back the
public schools,” as if public schools had ever been consistently Chris-
tian to begin with. Such efforts must fail, precisely because the in-
itial premise— that the State has the primary responsibility to care
for the weaker members of society — is itself fallacious. Zt is not the civil
government, but the individual Christian, who ts responsible. He joins with
other Christians to improve the delivery of services, since a group
can make use of the division of labor principle, but he must always
see himself as the responsible agent. He can withdraw financial sup-
port when he is convinced that the agency has sufficient funding or
even too much. This keeps the salaried people in line, which is far

41, Robert L. Thoburn, The Children Trap: The Biblical Blueprint for Education (Ft.
Worth, Texas: Dominion Press, 1986).
