Comprehensive Redemption: A Theology for Social Action 245

issue. We have little time remaining to reverse the political process.
Every year that we are delayed by the murderers in high places, a
million babies die in the United States, and perhaps 55 million per
year worldwide.% God’s judgment is preferable to this.

What godly men must do is this: prepare for a coming cataclysm.
They must offer valid alternatives to today’s social degeneration, in
every sphere of life. Each man need not attempt to provide guide-
lines for total reconstruction, but each man must find at least one
area, preferably the one in which he possesses valid authority. Men
must write, teach, and work to rebuild. They must prepare their
families and churches for a coming cataclysm. They must do what-
ever they can to be in positions of leadership during and following a
cataclysm. In fact, a series of cataclysms is likely, as sketched in
Deuteronomy 28. We must be ready to survive, so that we will be ready
to lead. We must confront the world with prophetic preaching, chal-
lenging those in authority to repent, to turn back from their specific-
aily evil ways.

No Pity

One thing should be borne in mind: God will not pity the objects
of His wrath. The prophets repeated this warning: God would not
pity them. “And I will dash them one against another, even the
fathers and the sons together, saith the Lorn: I will not pity, nor
spare, nor have mercy, but destroy them” ( Jer. 13:14). Ezekiel was
even more specific concerning God’s lack of pity:

Moreover the word of the Lorp came unto me, saying, Also, thou son
of man, thus saith the Lorp God unto the land of Israel; An end, the end is
come upon the four corners of the land.

Now is the end come upon thee, and I will send mine anger upon thee,
and will judge thee according to thy ways, and will recompense upon thee
all thine abominations.

And mine eye shall not spare thee, neither will I have pity: but I will
recompense thy ways upon thee, and thine abominations shall be in the
midst of thee: and ye shall know that I am the Lorp.

Thus saith the Lorp God; An evil, and only evil, behold, is come.

An end is come, the end is come: it watcheth for thee; behold, it is
come.

36. World Population and Fertility Planning Technologies: The Next 20 Years (Washing-
ton, D.C.: Office of Technology Assessment, 1982), p. 63.
