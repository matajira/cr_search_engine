70 Is THE WORLD RUNNING DOWN?

‘The debate goes on and on, like two endless loop cassette tapes play-
ing at each other.

At best, to appeal to the second law as anything more than a con-
venient way to expose the possible scientific inconsistency of the evo-
lutionists, and thereby to help strengthen Christians’ confidence in
creationism, is to grant too much authority to modern science, The
problem is that this seemingly convenient argument (which some-
how never convinces our evolutionist opponents) has in some cases
been taken so seriously by Christians that they have followed
Rifkin’s example, and have begun to construct Christian social
theory in terms of the second law of thermodynamics. When we see
where Rifkin’s approach is taking us, we should reconsider this ap-
plication of classical physics to society. It is far better to rely on the
Bible to provide us with our foundations of social theory.

Whenever anyone uses “neutral” physical science to support any
social theory, he faces the problem that a radically unneutral human-
ist worldview undergirds modern physical science. An alien world-
view can too easily be quietly imported into Christian social theory
when Christians use modern science as a supposedly common-
ground basis for the construction of social theories. Christians give
away too much to the enemy when they begin with humanism’s
science to construct their worldview.

I. Creation Science’s Social Pessimism

Tam arguing in this book that there is a serious danger to Chris-
tianity when Christians try to use the second law of thermodynamics
to justify a particular social theory. Let me cite an example from the
world of Scientific Creationism. In a flyer produced by the Bible-
Science Association and the Genesis Institute (same address), we
read the following: “The creationist realizes that the world is grow-
ing old around him, He understands that things tend to run down,
to age, to die. The creationist does not look for the world to improve,

 

energy from the sun, the organic world is an open system. Hence evolution is possi-
ble. Entropy may be increasing through the universe, taken as a whole, but it does
not mean that, in small localized areas, entropy cannot decrease. The world of
organic evolution is one such area. The sun shines down on the Earth, This makes
the plants grow. Animals live and feed on the plants, And thus life goes forward,”
Michael Ruse, Darwinism Defended: A Guide to the Evolution Controversies (Reading,
Massachusetts: Addison-Wesley, 1982), p. 296. Cf. pp. 306-7. This is a theory of
“sunshine evolution”; see pp. 192ff. below.
