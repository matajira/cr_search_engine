174 IS THE WORLD RUNNING DOWN?

20. Darwinists deny any link between ethics and the operations of
nature.

21. Creation Scientists have not publicly challenged the Dar-
winists on this point.

22. They seem to have adopted Rifkin’s views concerning entropy.

23. They are also eschatological pessimists, as Rifkin is.

24. They agree with Rifkin that obeying biblical Jaw cannot
restore the fallen world.

25. Science is never neutral,

26. Scientists are heavily influenced by the prevailing world view
of scientific culture.

27. If Creation Scientists were self-consciously covenanta] and op-
timistic, this would be reflected in their attitudes toward the thesis of
the universal reign of entropy.

28. The universe is not autonomous.

29. We must not transfer to social theory humanist theories of
autonomous nature.

30. Christians must begin to think seriously about social theory.

31. They must abandon Jeremy Rifkin’s pagan social theory.

32. Christians must begin to have faith in God and in history.

33. Pessimism toward the success of the gospel in history paralyzes
Christians in their efforts to rethink social theory.

34. Pessimism regarding the triumph of the gospel in history is the
worst sort of pessimism possible.

35. It is just this pessimism that has crippled the worldview of
Creation Scientists.
