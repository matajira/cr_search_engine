188 IS THE WORLD RUNNING DOWN?

trine of unéformitartanism: “Natural laws never change.” Natural laws
are seen as universal—the same in this region of the universe as in
any other. The trouble is, neither side agrees with the other with
respect to the scientifically legitimate applications of these natural
laws.

 

The Creation Science movement uses certain arguments with
respect to the second law of thermodynamics in its attempt to prove
its case against evolutionists that the evolutionists also use against
the creationists. Both groups use two basic approaches—closed
systems vs. open systems—each mutually contradictory, with
respect to that fundamental debating point, the meaning and proper
use of the second law of thermodynamics. Both sides affirm that:

1. The second law of thermodynamics has only been shown scien-
tifically to operate in closed equilibrium systems.

2. Ail local systems are ultimately open systems.

3. Insofar as science is concerned, the universe is a closed system.

4. The universe is therefore running down,

Whenever one side attacks the other by using a variation of the
“closed system” argument, the other side responds with, “You can’t
say that because this particular system is an open system.” So the
arguments get nowhere. Neither side will allow itself to be pinned.
down in any given instance by the logic of the second law of thermo-
dynamics.

I have argued in this book that Christian cosmologists’ reliance
on the entropy concept has placed them perilously close to the social
theory of Jeremy Rifkin. In fact, this dependence on the concept of
entropy has made the Creation Scientists virtually defenseless
against the bulk of Rifkin’s pessimistic conclusions,

The Terms of Discourse

As used in popular books (e.g., Jeremy Rifkin, Henry Morris),
entropy is spoken of as a law that proves that ail of nature is decay-
ing, wearing out, and becoming increasingly random. The “noise” of
the universe is overcoming its original coherence, The forces of life,
growth, and heat are steadily losing to a purported counter-force,
the second law of thermodynamics: death, decay, and randomness.

Such a heavy reliance on entropy has led to a paralyzing social
pessimism within the creationist movement. In fact, it is this unstated
