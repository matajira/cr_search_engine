310 IS THE WORLD RUNNING DOWN?

What Rifkin never mentions is that it is mainly our tomorrow
evenings that are scheduled in advance: the latest television shows and
sports spectaculars. The United States is a nation devoted to the
pursuit of leisure—leisure in such quantities that no other society
has ever experienced anything like it. Common people have color
television sets, videocassette players, stereo equipment, and access
to more varieties of entertainment than the richest king on earth ever
had, back when there were still kings on earth. We can bring the
finest performances of the finest orchestras into our living rooms
every evening. We can also bring the worst dramas ever written, the
least humorous comedy skits ever produced, and pour out our lives,
hour by hour, in the fruitless quest for meaningful amusement. We
spin the dials of our television sets, or flip from channel to channel
with our remote control units, vainly searching for something really
worth watching. We seek ways to carve minutes off of our work days
in order gain the extra time we need to bore ourselves into the wee
hours of the morning.

Tired of scheduling your evening’s entertainment? Then sit
down with a videocassette recorder any time you want, and watch
the program it recorded automatically a day or week ago. No matter
how mediocre the televised programs may be most of the time, you
can watch all you want, any time you want, for the price of a blank
videocassette tape. '* Yet Rifkin never mentions television. The fact
that one or more television sets are on inside most American homes
for over seven hours a day testifies to the enormous build-up of spare
time, meaning easily wasted time. The lure of “free” entertainment
is almost irresistible to most people.'®

Rifkin ignores all this. This desperate quest for electronic enter-
tainment reflects a crisis of man’s soul, a crisis made cost-effective for

14. T once saw a cartoon of a delightfully pathetic character, Mr. Tweedy. A friend
was looking in his claset, which was stacked floor to ceiling with videotapes of “Bow!-
ing for Dollars.”

15. In 1974, my wife and I agreed to buy a television only on this basis: each of us
promised to pay 25 cents per half hour of viewing when either of us selected a pro-
gram to watch. We gave this money away to charities at the end of the month. The
person who selected a program had to pay. We exempted only news broadcasts and
documentaries, We cut our viewing time to a few hours a week, excepting (of
course) news shows and documentaries. Just the measurable acknowledgment that
our time was worth a few cents per hour was enough to get us to allocate our time far
more rationally. We refused to watch junk, which made up the bulk of the scheduled
programs. In our spare time, we started our publishing business, which eventually
made us rich. This is redeeming the time.
