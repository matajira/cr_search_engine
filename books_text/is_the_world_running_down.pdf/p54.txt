18 IS THE WORLD RUNNING DOWN?

conclusion: modern science has lost tts mind. Science has lost its collec-
tive mind because scientists have abandoned faith in the God of the
Bible.

But this raises a key question: Should Christians appeal to mod-
ern science, let alone nineteenth-century science, in order to defend
their position? Should they build their case for creationism in terms
of scientific concepts that have been abandoned by modern science,
and which also lead them into the clutches of anti-Christian social
theorists?

Before I answer these questions, I need to introduce you to a
world of arrogant intellectuals who are in the process of going crazy.

An Autonomous (Self-law) Universe?

Do the Jaws of nature exist independently of man, man’s obser-
vations, man’s tools of observation, man’s mathematics, and man’s
mind? The Bible says yes, The animals reproduced according to
their kind before man arrived on the scene (Gen. 1:24-25}, These
laws do not exist independent of a Person, meaning God Himself,
but they exist independent of man. Today, however, the idea of a
law-governed universe independent from man has begun to bother
many scientists. As they have become more self-consistent concern-
ing epistemology —“What can man know, and how can he know it?”
— they have placed all of nature's orderliness in the mind of man.

This sounds crazy to most people. Obviously, everyone agrees
that the universe existed before man came on the scene. Nature
must have operated in terms of fixed laws. Man only discovers these
laws; he does not create them. But this common sense view has come
under increasing attack ever since Immanuel Kant wrote his Critique
of Pure Reason in 1787. Kant wrote: “Thus the order and regularity in
the appearances, which we entitle nature, we ourselves introduce. We
could never find them in appearances, had not we ourselves, or the
nature of our mind, originally set them there.”? We can know noth-
ing of the universe as such, or as he called it, the thing-in-itself. We
cannot say that nature is aufonomously an orderly system, Only
through the ordering processes inherent in the rational human mind
can any orderliness of nature be described, Kant maintained.
Modern science has become more and more consistent with Kant in

12. Immanuel Kant, The Critique of Pure Reason (1787), translated by Norman
Kemp Smith (New York: St. Martin’s, [1929] 1965), Sect. A 125, p. 147.
