The Division within Dispensationalism 291

apostasy. In short, dispensationalists have once again taken a strong
stand in favor of a theologically perverse predestination to reprobation,
but without also affirming predestination to salvation: “No signifi-
cant number of citizens of the State of Israel will be converted to
Jesus Christ prior to the Rapture and the great tribulation, no matter
what Christians do.” This is one more example of the dispensational-
ists’ soterialagy of inescapable impotence, a doctrine of the self-imposed
and perpetual weakness of the Holy Spirit in the so-called “Church
Age.” The Christian gospel is doomed to failure, they insist, just as
surely as the State of Israel is doomed to continued apostasy.

The historical worldview of dispensationalism is about to be
transformed as the “generation of the fig tree” gets older and older.
Dispensationalists are about to learn to lengthen their time perspec-
tive. This, in and of itself, will change the character of American
fundamentalism. (God willing, it will also cure Christians of their
willingness to go into long-term debt, with the Rapture as a way
escape from their creditors.)

This shift in perspective has already begun. For one thing, dis-
pensational leaders are now talking about a coming revival, possibly
before the twentieth century is over, a revival greater than any other
in man’s history. This substitution of the prophecy of worldwide revival for
the prophecy of the imminent Rapture constitutes the best evidence of “creeping
postmillennialism” that I can point to, Yet it is all being taught so subtly
that people in the pews or in front of their TV sets are unaware of
the major transformation that is taking place in their own thinking.
And the wonderful thing is that any old-line dispensationalist leader
who sees exactly where such talk about worldwide revival is headed,
and who calls attention to it, warning people to come back to the “old
time religion” of the imminent secret Rapture, is in danger of being
dismissed as anti-evangelical, someone opposed to the spread of the
gospel. Thus, a major shift in time perspective is coming, from the
short run to a much longer run.

The postmillennial Christian Reconstruction movement will be
the primary beneficiary of this shift.

Defection Over the Question of Ethics

Traditional dispensationalists are also. concerned about the
ethical side of the theology of Christian Reconstructionism, because
Reconstructionists place so much emphasis on biblical law as a tool
