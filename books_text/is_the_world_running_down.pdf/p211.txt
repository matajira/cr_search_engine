CONCLUSION

The fact is, the assumptions upon which modern man and woman base
their sense of meaning, purpose and direction in the world is false. Not
just partially false, but 100 percent false. Progress, science, and technal-
ogy have not resulted in greater order and value in the world, but their op-
posite, This is not a purely philosophical or soctological observation. . . .

The modern world completely and utterly contradicts the second major
flaw of thermodynamics—a law which has guided the entire age of
physics. Like a giant blind spot, we have refused to understand the pro-
found implications of this law even as we have selectively applied it in
order to create the modern technological society.

Jeremy Rifkin’

The “Entropy Law’ is simply the latest and most scientific sound-
ing of Satan's blueprints for Christ’s defeat in history. It is indicative
of the crisis in the Christian worldview today that so many Bible-
believing, well-educated Christians have taken seriously a whole
series of misuses of the legitimate concept of entropy. What is
desperately needed today to restore Christians’ confidence in the
future of the church and also to reconstruct science in terms of a con-
sistent Christian worldview is a new Reformation. This Reforma-
tion must be based on five principles: the doctrine of the absolute
sovereignty of God,? God’s hierarchical covenants,? biblical law,*

1, Jeremy Rifkin, The Emerging Order, pp. 61-62.

2. Gary North, Unconditional Surrender: God’s Program for Victory (3rd ed.; Ft,
Worth, Texas: Dominion Press, 1987), ch. 1.

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, ‘Texas: Insti-
tute for Christian Economics, 1987).

4, R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973); Greg L. Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg,
New Jersey: Presbyterian & Reformed, 1984).

175
