xviii IS THE WORLD RUNNING DOWN?

culturally from tearing down Darwinism, since everything is under
the “curse of entropy” anyway. The battle against evolutionism
therefore appears to be an intellectual exercise indulged in by a tiny
handful of obscure fundamentalist scientists. If Christians in general
are unwilling to risk everything they possess in a broad-based chal-
lenge to Darwinian humanism in every area of life, how will Scien-
tific Creationists ever prove their case to the Darwinists— not prove
it technically in some narrow academic field, which has never been
the main issue, but prove it ideologically and psychologically?

We are dealing with covenant-breakers who worship at the
shrine of pragmatic politics. If we tell them that our worldview offers
them no hope for the earthly future, how shall we lead them to
Christ? Only by asking them to surrender in history for the sake of a
promised victory outside of history. We are asking them to join the
losing side in history. A few low-level humanist leaders may switch,
by the grace of God, but most of them will not. Probably none of the
top leaders will. Why should they? They believe in history but not in
God. Scientific Creationists are asking them to believe in God but
not in history. This has been Rushdoony’s assertion for thirty years:

The humanists believe in history, but not in God. The funda-
mentalists believe in God, but not in history.

Furthermore, if Christian leaders, scholars, and writers become
persuaded that the second law of thermodynamics has doomed this
world to historical decline, why will they want to take the lead in
proclaiming this inevitable defeat of the church to their present fol-
lowers? Do Creation Scientists believe that politicians win votes by
announcing in advance to their followers that a political victory will
change nothing significant? That defeat is inevitable, no matter
what the outcome of the election? If Creation Scientists believe that
defeat is inevitable—and I believe that most of them do—they will
be unsuccessful leaders in any movement for comprehensive social
reform along biblical lines.

If a Christian believes that in the long run his earthly efforts are
doomed, and if he also believes that the Darwinists will control the
seats of power until Jesus cornes again, then the smart tactic is to lie
low, keep his mouth shut, and do the best he can to preserve the
status quo until Jesus comes again and visibly smashes His enemies.
Why would Christians risk sacrificing their life’s work in order to
launch an attack that calls public attention to their own weakness in

 
