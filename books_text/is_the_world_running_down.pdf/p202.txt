166 1S THE WORLD RUNNING DOWN?

He then went on to observe that premillennialists run most of the
rescue missions. “Premillennialists have a pretty good record in
meeting the physical needs of people.” This is quite true, but there is
no doubt from his words that he does not believe it is possible for
Christians to influence the creation of a world in which there will be
freedom, righteousness, and productivity—a world in which fewer
rescue missions will be necessary. His vision of social action is to get
people out of the gutter. This is because his view of the gospel is to
take people out of this world—first mentally and then physically, at
the Rapture.

Now, let me say that I believe in the Rapture of the saints into the
sky at the return of Christ in judgment. As far as I know, every
Bible-believing group believes this, What most Christian groups
have denied throughout church history is that this gathering of the
saints takes place before the final judgment. The Rapture of the
saints inaugurates the final judgment. There is a tendency for dis-
pensationalists to argue that their opponents have denied the Rapture
just because they date the Rapture at the end of history rather than at
the beginning of the seven-year tribulation period or at the begin-
ning of the millennium. The debate within Christianity is not over
the reality of the so-called Rapture, but only over its dating in history.
The non-premillennialists expect the Rapture at the close of history.

Before the Rapture, we will see the public triumph of God’s peo-
ple in history, in every area of life. Jesus said in His Great Commis-
sion: “All power is given unto me in heaven and in earth” (Matt.
28:18). The positive effects of this covenantal progress of Christ’s
people in history will be manifested in man’s physical environment.

Overcoming Decay

Physical decay will be completely overcome at the final judg-
ment. All Christians believe this. Henry Morris has commented on
this aspect of the final judgment in his exposition of Revelation
22:3: “The agelong curse is gone. There is no more death and no
more sin. The earth and its inhabitants, indeed the entire creation,
are henceforth to thrive in fullest vigor forever. None will ever age,
nothing will ever be lost, all work will be productive and enduring.
The entropy law, the so-called second law of thermodynamics, will
be repealed. Information will nevermore become confused, ordered
systems will not deteriorate into disorder, and no longer will energy
