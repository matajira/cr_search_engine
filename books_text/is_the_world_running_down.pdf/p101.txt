The Pessimism of the Scientists 65

physical state, entropy. He is willing to do what the other textbook
writers judiciously avoid: come to grips with God.

A final point to be made is that the second law of thermodynamics and
the principle of increase in entropy have great philosophical implications.
The question that arises is how did the universe get into the state of reduced
entropy in the first place, since all natural processes known to us tend to in-
crease entropy? Are there processes unknown to us, such as “continual crea-
ton,” which tend to decrease entropy, and thus offset the increase in en-
tropy associated with the natural processes known to us? On the other end
of the scale the question that arises is what is the future of the universe? Will
ic come to a uniform temperature and maximum entropy, at which time life
will be impossible? Quite obviously we cannot give conclusive answers to
these questions on the basis of the second law only, but they are certainly
topics that illustrate its philosophical implications. The author has found
that the second law tends to increase his conviction that there is a Creator
who has the answer for the future destiny of man and the universe.

Next, consider his comments in the 1973 edition. He and his co-
author ask some new questions: “Does the second law of thermo-
dynamics apply to the universe as a whole? . . . If the second law is
valid for the universe (we of course do not know if the universe can
be considered as an isolated system) how did it get in the state of low
entropy?” Then they repeat his original affirmation of a Creator,
although they do not capitalize the word in the later edition. They
raise the relevant question: /s the universe really a closed system? As be-
lievers in God, obviously they know that it isn’t, but they do raise the
question. It is he question that must be raised.

Modern physics and modern astronomy leave mankind without
hope. Bertrand Russell saw its implications clearly. He wrote in
1935: “Some day, the sun will grow cold, and life on the earth will
cease. The whole epoch of animals and plants is only an interlude
between ages that were too hot and ages that will be too cold. There
is no law of cosmic progress, but only an oscillation upward and
downward, with a slow trend downward on a balance owing to the
diffusion of energy. This, at least, is what science at present regards

60. Gordon J. Van Wylen, Thermodynamics (New York: Wiley, [1959] 1961), p.
169. ‘Three comments are in order. First, Van Wylen was Chairman of the Depart-
ment of mechanical engineering at the University of Michigan, Second, Wiley is a
conventional publisher of acientific books. Third, the book was in its third printing.

61. Gordon J. Van Wylen and Richard Sontag, Fundamentals of Classical Thermo-
dynamics (2nd ed.; New York: Wiley, 1973), p. 248.
