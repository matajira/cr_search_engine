194 18 THE WORLD RUNNING DOWN?

cosmos to have had enough time to evolve, given presently observed
rates of change. This does strike a major blow against the Darwin-
ists. But to focus on the second law of thermodynamics, rather than
on the Darwinists’ overdrawn bank account of available time, has
not persuaded many conventional scientists of the case for creation-
ism. The two camps shout at each other endlessly over the proper
use of the second law; it would be cheaper and waste less time for
each group simply to buy an endless-loop cassette and play the re-
cordings at each other. No one would listen, but it would give every-
one more time to develop more effective arguments.
It would also get Jeremy Rifkin out of the picture.

Uniformitarianism: The Shared Faith

Why all this concern about the extent of entropy? Because the
Darwinians base their case against the creationists on the logic of
uniformitarianism, and the Scientific Creationists reply in kind
(almost), Uniformitarianism is the scientific-religious doctrine that
the laws of nature are fixed. Uniformitarianism asserts that the laws
that govern all historical change have been stable over time. Without such
fixed laws, both sides claim, there could be no science.

The Debate Begins

Scientific Creationist Henry Morris states that to the best of our
knowledge, all the laws of nature have been in effect since the begin-
ning of time. At least he admits that this is to the best of our knowl-
edge. He argues, however, that the evolutionist’s presupposition of a
world of constant change, of evolutionary flux, is a denial of these
fixed laws of nature.

Evolutionist Willard Young counters this argument: “Evolution
is a process which occurs under appropriate conditions within the laws
of nature as they stand, Rather than suggesting that the laws of nature
evolve, it assumes, on the contrary, that they have remained a con-
stant. Except for this implicit assumption, the theory of evolution
has nothing whatever to say about any of the fundamental laws of
nature.” At least he admits that the fixed laws of nature are
assumptions.

14, Henry M. Morris, Scientific Creationism (San Diego, California: Orcation-Life
Publishers, 1974), p. 18.
15. Young, Fallacies of Creationism, p. 168
