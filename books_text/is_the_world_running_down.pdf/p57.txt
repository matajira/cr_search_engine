The Unnaturatness of Scientific Natural Law 2

Each of these theories is defended by brilliant scientists. My
favorite, because of its sheer arrogance, is number 4, plural worlds.
This can be regarded as the ultimate in magical reasoning, a scienti-
fic legacy of the Renaissance’s theory of multiple worlds, or the
“principle of plenitude,” as Arthur Lovejoy called it.” Herbert sum-
marizes: “Of all claims of the New Physics none is more outrageous
than the contention that myriads of universes are created upon the
occasion of each measurement act. For any situation in which sev-
eral different outcomes are possible (flipping a coin, for instance),
some physicists believe that al/ outcomes actually occur. In order to ac-
commodate different outcomes without contradiction, entire new
universes spring into being, identical in every detail except for the
single outcome that gave them birth. In the case of a flipped coin, one
universe contains a coin that came up heads, a coin showing tails.”"#

Our apologetic approach must recognize the reality of what has
happened since 1905: “Just as Newton shattered the medieval crystal
spheres, modern quantum theory has irreparably smashed Newton’s
clockwork. We are now certain that the world is nof a deterministic
mechanism. But what the world is we cannot say.”2?

Will Heathkit Eventually Offer One of These?

On the Science page of the New York Times (April 14, 1987) are
two stories. These two stories summarize the Darwinian revolution
better than anything I have ever seen or expect to see. The first dis-
cusses “the black skull.” With its discovery in 1986, “the small conten-
tious fraternity of paleoanthropology was stunned into a rare state of
unanimity. Everyone agreed that the skull was the most significant
early humanlike fossil to be found in more than a decade. Everyone
agreed it would necessitate a major rethinking of the human family
tree.” Today, however, everyone is arguing again about how to fit the
2.5 million-year-old skull into the human family tree. Anyone who
wants to learn more about this latest piece of Darwinian mythology
is welcome to pursue it. The skull points once again to “man, the
product of cosmic purposelessness.” Man has only “recently” come
out of a tree. Poor man, the grandson of apes.

20. See Arthur O. Lovejoy, The Great Chain of Being: A Study of the History of an Idea
(New York: Harper Torchbook, [1933] 1960), chaps. 4, 5.

21, Herbert, Quantum Reality, p. 19.

22. Ibid., p. xii.
