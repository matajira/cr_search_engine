36 IS THE WORLD RUNNING DOWN?

Laws of nature are only the beginning of the intellectual-theological
problem. What about laws of human behavior? What about laws of
social development? Are these laws really laws? Are they fixed? Can
we make accurate predictions if we know them? Do they even exist?

This raises another very important and difficult intellectual prob-
lem: the relationship between physical regularities and soctal regularities.
Can we legitimately apply physical laws (regularities) to social rela-
tionships? I argue in this book against such a connection, but others
have long argued the opposite.

Then there is an additional question: How will defenders of vari-
ous world-and-life viewpoints interpret the regularities that they dis-
cover? It is never simply a question of “raw data,” “brute facts,” and
“inescapable law.” It is always a question of how these perceived facts are
interpreted. People are not neutral, not even scientists. So, we must
also bear in mind that the various discoveries of science will be used
by scholars to prove different things. This is why we need to under-
stand the major competing worldviews, even before we begin to dis-
cuss the primary topic of the book, the second law of thermodynam-
ics and its legitimate uses (if any) in social theory.

Conclusion

The search for regularities is legitimate. It is an inescapable
aspect of all rational and scientific investigation. Nevertheless, we
must beware of any proposed system of regularities that relies on any
supposed autonomous “law” of nature. Nature is orderly, just as God
is orderly, but nature is not autonomous, and nature’s laws are not
autonomous. Nature is not simple, which is why we have to devise
mental “shorthand” models of reality to assist us in our search for
order, and to help us to predict future events. We are simple-
minded; nature is not simple.*! The more that physicists study
nature, the more complex and baffling it becomes for them. The nice
stable mathematical universe described by Newton is accurate, up to
a point, but not a subatomic point. Our knowledge of Newtonian
regularities enables us to achieve tremendous progress.? But the

51, Physicist Eugene Wigner writes: “The world around us is of baffling complex-
ity and the most obvious fact about it is that we cannot predict the future. . . . It is,
as Schrddinger has remarked, a miracle that in spite of the baffling complexity of the
world, certain regularities in the events could be discovered.” Wigner, “The Unrea-
sonable Effectiveness af Mathematics,” op. cit., p. 4.

52, Ibid., pp. 8-9.
