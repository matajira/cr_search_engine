246 IS THE WORLD RUNNING DOWN?

The morning is come unto thee, O thou that dwellest in the land: the
time is come, the day of trouble is near, and not the sounding again of the
mountains.

Now will I shortly pour out my fury upon thee, and accomplish mine
anger upon thee: and I will judge thee according to thy ways, and will
recampense thee for all thine abominations,

And mine eye shall not spare, neither will I have pity: I will recompense
thee according to thy ways and thine abominations that are in the midst of
thee; and ye shall know that I am the Logo that smiteth.

Behold the day, behold, it is come: the moming is gone forth; the rod
hath blossomed, pride hath budded.

Violence is risen up into a rod of wickedness: none of them shall re-
main, nor of their multitude, nor of any of theirs: neither shall there be
wailing for them.

The time is come, the day draweth near: let not the buyer rejoice, nor
the seller mourn: for wrath is upon all the multitude thereof,

For the seller shall not return to that which is sold, although they were
yet alive: for the vision is touching the whole multitude thereof, which shalt
not return; neither shall any strengthen himself in the iniquity of his life.

They have blown the trumpet, even to make all ready; but none goeth
to the battle: for my wrath is upon all che multitude thereof.

The sword is without, and the pestilence and the famine within: he that
is in the field shall die with the sword; and he that is in the city, famine and
pestilence shall devour him (Ezk. 7:1-15).

Most people on earth have been refugees, captives, and tribute-
payers in this century. Certainly, they have been tribute-payers to
the messianic State. They have tasted the fruits of the religion of
humanity. A few nations have avoided outright military invasion:
the United States, Ganada, New Zealand, Australia, and Latin
America. Now Latin America is being threatened, and Central
America has actually experienced Communist take-overs. No one is
immune. The Chinese Communists went on the Long March in the
early 1930’s to escape from the military forces of the anti-
Communists. They thought it no great sacrifice to retreat, in order
to fight another day. A decade and a half later, they were victorious.

Responsible Christianity

What conquering ideological humanist armies have been willing
to suffer for the sake of “the cause,” few comfortable Christians are
courageous enough even to contemplate as an outside possibility.
They would rather die, they say. Better yet, they would rather be
