Introduction a

world of historical optimism. They also tend to prefer cosmic
pessimism to cosmic optimism, for cosmic pessimism fits so much
better into a worldview based on historical pessimism.

Negative Feedback

Scientists also speak of the process of negative feedback. “What
goes up must come down,” we are told from youth. Nothing multi-
plies forever. Things grow for a while, and then they stop growing.
There are limits to growth in a finite world.

Christians acknowledge that negative feedback is a limiting fac-
tor in a cursed world. The animals are not allowed to multiply and
overcome the land (Ex. 23:29). They are restrained by man or by
“the forces of nature,” meaning the environment’s built-in limitations
on the compound growth process. A multiplying species runs out of
food or living space; some other rival species competes for the lim-
ited number of resources; still another species begins to prey on the
expanding one, either externally (“beasts of prey”) or internally
(parasites). Similar restraints limit the development of human insti-
tutions in ethically rebellious civilizations.“ Fallen man is never
wholly free from sin, His institutions and his environment will never
be wholly devoid of the process of negative feedback, in time and on
earth.?

But is this system of cosmic negative feedback inescapable? Are
there no exceptions? What about the miracles in the Bible? Aren’t these
classic examples of the non-universal nature of the entropy process
{assuming what needs first to be proven, namely, that entropy is the
physical basis of negative feedback)? This question should remain in
the back of the reader’s mind throughout this book. The question of
how miracles fit into a world supposedly governed by unbreakable
natural law should be the central question in all modern science. If
natural law is breakable, then we must ask: How? Under what cir-
cumstances? Why? More to the point, what is the nature of “natural
law”? Is it an autonomous, impersonal force that operates whether or
not God intends otherwise?

22, Garrett Hardin, “The Cybernetics of Competition: A Biologist’s View of
Society,” in Helmut Schoeck and James W. Wiggins (eds.), Central Planning and Neo-
mercantilism (Princeton, New Jersey: Van Nostrand, 1964). Hardin, a dedicated evo-
lutionist, does not discuss the possibility of the process of negative feedback being
limited by the ethical character of a culture.

93, Garrett Hardin, Nature and Man's Fate (New York: Rinehart & Co., 1959), pp.
48-55.
