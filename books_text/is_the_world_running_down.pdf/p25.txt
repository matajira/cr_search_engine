Preface XxXV

ing out. You are steadily losing IQ points. Everything is becoming
confused. Now listen carefully as I tell you the scientific secret of the
ages. Repeat after me: ‘Entropy, entropy, entropy.” He uses the
word as a kind of talisman, a magical device to manipulate his
chosen victims.

There is an old line: “You can’t beat something with nothing.”
The concept of entropy has been misused to create a philosophy of
long-run nothingness. Until Christians stop talking about the world’s
future in terms of “sovereign entropy,” they will not be able to
develop a believable and consistent positive alternative to modern
humanism, meaning a positive program which will give people hope
for solving this world’s problems. As I argue in this book, to appeal to
an exclusively other-worldly hope is to abandon hope for this world. It
leads to social pessimism.

Apostle of a New Age

Tam attempting to refute a new heresy, which is in fact a very an-
cient heresy: the New Age movement.” The New Age movement
has made important inroads into Christian circles since 1979, and
also inroads into the thinking of Christian leaders, One man, more
than any other, is responsible for this successful infiltration: Jeremy
Rifkin. What I argue in this book is that some of the key presupposi-
tions of the Scientific Creation movement serve as the foundations
for Rifkin’s cosmological, economic, and social analyses. More than
this: their shared presuppositions have made it very difficult for six-day
creationists and other evangelicals to respond to Rifkin.

Yet Christians must respond to Rifkin. First, his general presup-
positions are wrong. They are closer to Eastern mysticism than to
Western rationalism. Second, his anti-rational arguments are ini-
tially disguised, for he appeals to what he (and the Scientific Crea-
tionists) say is the key fact of Western scientific rationalism, the en-
tropy law, in order to make his case against Western rationalism.
Third, he has distorted the history of the West. Fourth, he has

20. Gary North, Unholy Spirits: Occultism and New Age Humanism (Ft, Worth,
Texas: Dominion Press, 1986).

21. The Entropy Law was actually the praposed title of the book by Rifkin which
was eventually published under the title, Entropy. Sometime in late 1979 or early
1980, the Peoples Business Commission, Rifkin’s organization, sent out a brochure
describing this forthcoming book, which also listed Noreen Banks as co-author,
along with Ted Howard.
