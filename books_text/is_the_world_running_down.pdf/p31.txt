Preface xxxi

Caribbean islands, new Black liberation movements are popping up
all over, In Canada, the Quebecois Liberation Front has brought the
fires of revolutionary nationalism right up to the U.S. frontier. On
the European continent, similar fires are scorching the hides of im-
perialists. . . . In Asia the entire continent seethes with the move-
ment. The victory of the first stage of the Chinese Revolution can be
said to have struck the sparks that set off the whole world-wide phe-
nomenon of revolutionary nationalism.”#

What about the United States? On page 157, Rossen informs us
that “the American version of the concept of revolutionary national-
ism will be anti-capitalist and socialist in content, and national in
form and rhetoric.”42

Mr. Rifkin’s partner specified in the March/April 1971 issue of
The New Patriot (his own publication) what his understanding of
Marx was:

Marx laid the sturdy foundations for the scientific revolutionary-social-
ist methodology, and for any modern revolutionary to ignore those founda-
tions would be as stupid as for a physicist to ignore the findings of Isaac
Newton. But neither can a modern revolutionary limit himself to the find-
ings of Marx, That is why I use the expression “scientific revolutionary
methodology” rather than the expression “Marxism.” The problem with
most of those who cail themselves Marxists today is that they accept Marx-
ism as a dogma and not as a scientific tool, a revolutionary methodology
which is constantly being refined, added to, improved on the basis of the
revolutionary experience of the last century and a quarter.*®

Co-authors Ted Howard, Randy Barber, and Rifkin were all as-
sociated with the Peoples Bicentennial Commission, which was later
renamed the Peoples Business Commission. As he said to a Washing-
ton Star reporter in 1979, “Our job is to develop a politics for the ’80s
and ’90s,"4*

A New Targeted Audience: Evangelicals

This was the original goal of the Peoples Bicentennial Commis-
sion: political change. Rifkin began to target evangelical Christians
in the late 1970's with his book, The Emerging Order, This was simply a

41. Tbid., p. 12.
42. Tbid., p. 13.
43. Ibid., p. 91
44. Washington Star (Jan, 24, 1979).
