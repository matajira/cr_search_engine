98 IS THE WORLD RUNNING DOWN?

noonday brightness of human genius, are destined to extinction in
the vast death of the solar system, and that the whole temple of
Man’s achievement must inevitably be buried beneath the debris of
a universe in ruins— all these things, if not quite beyond dispute, are
yet so nearly certain, that no philosophy which rejects them can hope
to stand, Only within the scaffolding of these truths, only on the firm
foundation of unyielding despair, can the soul’s habitation hence-
forth be safely built.”

The proponents of zero economic growth are simply trying to
adapt today’s way of thought and life to what science assumes will
probably be incomparably long-term implications of the entropy
process. They are doing this as a religious commitment. They refuse to
allow themselves to be deluded by the ¢lluston of meaning provided by
an inevitably “short-run” period of economic growth — say, a few mil-
lion years, Growth is a product of future-orientation. The debate over
the legitimacy or desirability of growth—any kind of growth—is
necessarily a debate about mankind’s legitimate orientation toward
the future.

If mankind has no ultimate earthly future, which is what scientist-
philosophers-speculators have argued, whether Darwinists or East-
ern mystics, then what is our proper response as a species? Both
sides of this humanist debate call on mankind to keep a stiff upper
lip, and persevere as best we can. The Darwinian entropists add that
while we are at it, let us continue to eat, drink, and be merry—at a
compound growth rate of 6% per annum, if we can —for eventually
the human species dies.

The social entropists reply that this would produce a fool’s para-
dise, that man must find inner meaning, or union with the (dying)
cosmos, or metaphysical union with an impersonal god who will
somehow survive entropy. They want to restructure man’s immedi-
ate institutions, thought patterns, and reproduction rate in order to
acknowledge ritually the trillion-year meaning of entropy. Theirs is a
religious commitment. They believe that the works of man’s hands—of
all men’s hands — should testify zmmedtately to the cosmic reality of en-
tropy, even if mankind might conceivably eke out another century or
two of growth through improved technology and capital accumula-
tion. The fact that the earth could easily have 10 million centuries
ahead does not impress the social entropists.

14, Bertrand Russell, “A Free Man's Religion” (1903), in Mysticism and Logic
(Garden City, New York: Anchor, n.d.), pp. 45-46.
