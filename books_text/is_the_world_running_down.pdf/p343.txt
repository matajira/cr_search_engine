Time for a Change: Rifkin's “New, Improved” Worldview 307

ism; it is the leaders of socialist governments, not free market soci-
eties, that are so arrogant as to imagine that anyone or anything
other than God can successfully “organize the future.”

Nevertheless, Rifkin claims that “big is better” is the reigning or-
thodoxy of our era—which, on the whole, it is and that this ortho-
doxy has now begun to be challenged. “A spatial heresy began to
take hold, winning over legions of converts to a new vision. ‘Small is
beautiful’ began to challenge the once-powerful myth that bigger is
better.”6 Rifkin is still living in the light of the dying embers of the so-
cial fires of 1965-71, the world of youthful dreams about bug-free
organic farms, safe political revolution, and chemical euphoria.
Small looked good in those days, just so long as a tuition check from
Daddy arrived on time each semester. But graduation day finally ar-
rived, or Sonny dropped out of college, and Daddy is not sending
checks any more. In fact, Daddy and his friends are fast ap-
proaching retirement, and the kids are now expected to ante up the
retirement money, through endless increases in the Social Security
tax. The checks are now supposed to flow in the other direction, and
the West is facing a monumental and inescapable debt crisis.”
Unfortunately, this is not a crisis that committed socialists like to dis-
cuss in detail, for it was their ideology of compulsory wealth redistri-
bution that created this debt crisis. So they invent other crises that
can be substituted for the real one.

Today, Rifkin announces, there is a far deeper conflict than the
conflict over “big is better” vs. “small is beautiful.” It is the concept of
efficiency. Yes, folks, efficiency, that familiar bugaboo of anti-capitalist
critics (who recognize belatedly that socialist economies are woefully
inefficient), that ascetic religion of the Industrial Revolution, that
hottest of hot topics (circa 1825), is with us still. We now know what is
wrong with the West: *. .
time values of the modern age,”* The god of modern time is the goal
of faster. And faster than you can say, “New, Improved Worldview,”
Jeremy Rifkin has written volume six (or is it seven?) of The Prophet
Speaks, which exposes the evils of Western, technological time. Not
linear time, this time, but computer time, which is simulated time.
(Don’t hold your breath for Rifkin to define this precisely). He an-

. efficiency and speed characterize the

6. Bbid., p. 3

7. Lawrence Malkin, The National Debt (New York: Henry Holt, 1987); Peter G.
Peterson, "The Morning After,” The Adantic Monthly (October 1987).

8. Time Wars, p. 3.
