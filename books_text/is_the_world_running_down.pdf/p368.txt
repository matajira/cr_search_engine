332

weak case, xiii
worldview, xv
Davis, John J., 264-65
DDT, 14t-42
deaf, 252
death, 8, 42, 100-1, 109, 111, 121, 198
(see also heat-death)
debates, xii, xii
debt, 291, 307
decay, 159-60, 166-68, 170, 188
decentralization, 233
deductive logic, xiii
default, 253
defeat, 156, 171
democracy, 86-87, 216
despair
alternative to, xiv-xv, xxiii
creationism, 70-71, 128
entropy &, 70-71, 128
Darwinism, xiit
heat death, 54-55, 109
preferred to God, 63-64
premillennialism &, 113
pessimillennialism, 76, 113-14
Rifkin, 107, 177
Russell, Bertrand, 54-55
zero growth, 112
dispensationalism
activists in, 294
allies of humanism, 272
antinomian, 264, 289, 292-93, 300
crisis of, 261, 269, 294
division within, 261, 281, 294-99
ethics &, 292
“gap” theory, 270
halfway house, 297
Holy Spirit debate, 298-300
hybrid system, 296
pessimism of, 265-67
rescue mission theology, 264
responsibility, 292, 294
Rifkin &, 264, 269-71
social ethics, 304
white flag, 264
see also Rapture
determinism, 227
dicts, 302

 

IS THE WORLD RUNNING DOWN?

directionality, 51-52, 282
disagreement, 235-36
disasters, 109
discontinuity, 2, 4, 176-77, 201-2
disintegration, 32
doctrines of the faith, x
dominion
biological? 147
creation &, 13-14
ethical, 110, 146, 148, 155, 160
God's laws &, 161
institutions &, 156
long-term growth, 160
narrow?, 303
religion, 44-46
Rifkin vs,, 80-81, 134, 138
steady, 146
stewardship, 85
dominion theology
agenda, 295
Dave Hunt &, 257, 275-76
need for, 254
utopian?, 267
Don Quixote, 306
Drucker, Peter, 315
dualism, 228-29
Dubos, René, 81-82, 94

earnest, 111
Eckhart, Meister, 133, 153
ecology, 141, 275
Eddington, Arthur, 19, 62, 91
Eden, 124-27
education, 506, 200, 209, 212-13,
Appendix B (see also public schools)
Edwords, Frank, 271
Edwards » Aguillard, 206
Einstein, Albert, 16-17, 19, 25, 27, 28,
32
electrons, 23-24, 29, 320
energy
atomic, 126
consumption of, 99
evolution &, 201-2
final dissipation of, 109
garden of Eden, 126
God-substitute, 199, 204
