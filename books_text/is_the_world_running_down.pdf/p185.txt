Dominion and Sanctification 149

rection of Christ was thought to be the only major interruption in the
course of linear time from the creation of the world until it ends
altogether.>

The judgments of God are revealed in history, and they are two-
fold: cursings and blessings.® Because Christians have been heavily
influenced by humanism’s pessimism and by pessimistic doctrines of
the future, they have overemphasized the cursings of God in history
and underemphasized the blessings of God in history.

Sanctification in History

Is it possible for the church of Jesus Christ to purify itself ethically?
The Bible says yes. But if it does steadily cleanse itself ethically,
won't this manifest itself in history? Won't God honor His covenantal
promises in Deuteronomy 28:1-14? In short, can the effects of moral
purification be bottled up inside the institutional church? Satan
wishes this were the case, but it cannot be true. Unfortunately for
the church, Satan’s wish has become the dominant Christian theol-
ogy in our day—a theology of eschatological pessimism.

Sanctification involves moral cleansing. The Bible says that this
cleansing of the church will be achieved before the end of time.
Paul’s words are clear on this point:

Husbands, love your wives, even as Christ also loved the church, and
gave himself for it; that he might sanctify and cleanse it with the washing of
water by the word, that he might present it to himself a glorious church, not
having spot, or wrinkle, or any such thing; but that it should be holy and
without blemish (Eph. 5:25-27).

Paul's language leaves no doubt concerning Christ’s intention to
cleanse His church, It also leaves no doubt that Paul was writing for
the so-called “Church Age.” No dispensational group, even including
the groups classified as “ultradispensationalists” (anti-baptism fol-
lowers of C. R. Stam,’ and zero-Lord’s Supper followers of E. W.
Bullinger), has classified Ephesians as anything except a “Church
Age” document.

8. Harold J. Berman, Law and Revolution: The Formation of the Western Legal Tiadi-
tion (Cambridge, Massachusetts: Harvard University Press, 1983), pp. 26-27.

6, Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: Insti-
tute for Christian Economics, 1987), ch. 4

7. Cornelius R. Stam, Things That Differ: The Fundamentals of Dispensationalism
(Chicago: Berean Bible Society, 1959).
