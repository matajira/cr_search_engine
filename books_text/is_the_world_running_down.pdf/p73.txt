The Unnaturalness of Scientific Natural Law 37

more deeply that scientists look into the workings of nature, the
more complex nature seems to be, and the more overwhelmed the
scientists become. Their own equations become the only true reality
for them.

In this sense, nature is like the Bible. It is simple enough for re-
tarded people to understand and be converted; it is also difficult
enough to baffle teams of scholars. Both nature and the Bible reflect
God. He is both simple (one) and complex (three). We should be-
ware of any exclusive reliance on such shorthand theories as Occam’s
famous fourteenth-century razor: that the simplest explanation is
always best. The simplest explanation, from Occam to Darwin to
modern physics, always excludes God as an unnecessary and overly
complex hypothesis.*3 [t may not be harmful to use a modified ver-
sion of Occam's explanatory razor, as Henry Morris does,** but tak-
ing it too seriously is dangerous. If we adopt it as 2 universal rule,
the humanists will use it to erase God from the universe.

Regularities are covenantal. They are created by God. God uses
them for His purposes, and we can use them for ours. But the farther
away we get from God ethically, the less effective our knowledge of
“universal laws” will be. Furthermore, the world around us will
begin to display the annoying aspects of wear and tear that Jeremy
Rifkin and Henry Morris call entropy. God's curses, day by day, will
manifest themselves more plainly. These manifestations of God’s
judgment will not be random, however. In this sense, they will be far
more threatening than mere impersonal entropy.

This covenantal cause-and-effect relationship between ethical re-
bellion and external curses from God also points to its opposite: the
closer we get to God ethically, the /ess our social world and even the
natural world will be subject to the cursed aspects of entropy. Our
wives and animals will not suffer miscarriages (Ex. 23:26). Our

53, This statement may appear extreme, since there are no doubt Christian
physicists. But in their textbooks and professional papers, physicists do not refer to
God as their operating presupposition, or at least not the God of the Bible, nor do
they cite the Bible as the foundation of their own understanding of the way the world
works. This is implicitly epistemological atheism, as Cornelius Van Til argued
throughout his career, and in the profession as @ whole, this has not been simply atheism

by default.
54. “It would be helpful to keep in mind Occam's Razor (the simplest hypothesis
which explains all the data is the most likely to be correct). . . .” The Genesis Record,

p. 195, We can safely say “most likely to be correct,” which means that we use it when
we like the conclusions, but it is not an absolute law of nature or interpretation. It is
simply a handy mental tool
