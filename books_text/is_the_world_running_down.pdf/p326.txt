290 IS THE WORLD RUNNING DOWN?

mystery in the sense that it was completely unrevealed in the Old
Testament and now revealed in the New Testament.”2!

Thus, original dispensationalism appears to teach that no event
which has taken place since the establishment of the church until to-
day could possibly have been predicted in the Old Testament. Obvi-
ously, the church is still around, and it is being affected by present
historical events, such as the creation of the State of Israel. At best,
what is happening today is a kind of “shadow” of future, post-Rapture,
tribulation eva fulfillments of Bible prophecy.?? No Bible prophecy is
actually being fulfilled in our day, according to scholarly, technically
precise dispensationalism. John Walvoord admitted this in his 1963
address at the Congress on Prophecy, Pretribulational dispensation-
alists can legitimately speak only of “preparation for the world events
predicted to follow the rapture. . . .”?3 Preparation, yes; actual ful-
fillment of prophecy, no. But if Hal Lindsey had emphasized this
highly important qualification to dispensational theology in his Late,
Great Planet Earth, it would never have made it into a second printing.

Pretribulationalists for many years predicted that the Rapture
would take place in 1981: 1948 + 40 (one generation) — 7 (great tribu-
lation period) = 1981. This was the 40-year “generation of the fig tree”
(Matt. 24:32-35), When the Rapture did not take place as dated, the
“generation of the fig tree” seemed to get a stay of execution.”* When
the Rapture also does not take place in 1988, as it will not, the present restruc-
turing of pretribulational theology will accelerate. How many years can the
generation of the fig tree be extended? Time has just about run out
on dispensationalism’s reliance on the creation of the State of Israel
as the motivational cornerstone of their system, If Israel is ever de-
feated militarily, or if it should be converted to Christianity, the dis-
pensational system will collapse. Dispensationalists have “bet the
farm” on the State of Israel’s continuing existence and its continuing

21, fbid., p. 136.

22. David Schnittger is careful to use this shadow terminology in his booklet. “I
do think the shadows of tribulational conditions are lengthening, and we could very
well live to experience the rapture.” He goes on to warn his readers: “Such a hope
should not cause us to falsely conclude the forces of evil are winning, and the church
age must end in defeat. The evidence of Matthew 13 and church history indicates
just the opposite. The church is moving forward in an unprecedented fashion.”
Chrisuan Reconstruction, p. 15.

23. John Walvoord, “Is the End of the Age at Hand?” in Charles Feinberg (ed.},
Focus on Prophecy (Westwood, New Jersey: Revell, 1964), p. 167. .

24. Frank Goines, Generation of the Fig Tree (Tulsa, Oklahoma: By the Author,
1979).
