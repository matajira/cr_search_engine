The Question of Ethics 119

no source of law beyond the state, to which man can appeal against the

state. Humanism therefore imprisons man within the closed world of the
state and the closed universe of the evolutionary scheme.®

Defenders of both Darwinian and New Age social entropy be-
lieve that man dwells in an ethically “closed box.” Man has no appeal
beyond the cosmos.

Third, both sides have a theory of entropy. The creationist sees
the universe as under a curse and headed for destruction, except for
one mitigating factor: the grace of God. There certainly appear to be
degradative factors in our cursed immediate environment, but these
factors can be mitigated and partially overcome through the creation
of God-honoring institutions. Furthermore, all Christians believe
that degradative processes, assuming that they are aspects of the
curse, will be overcome after the final resurrection, for the curse will
be overcome.

Both the scientific Darwinians and the social entropists believe in
the ultimate material effects of entropy. There is no escape. Where
there is directional movement, there is an increase in entropy. But
the conventional Darwinians still think that it pays man to seek to
overcome the effects of entropy temporarily through scientific exper-
imentation, technological progress, and planning. They proclaim a
temporary optimism based on the autonomous ethics of scientific do-
minion. The social entropists reject this view. The new ethics of
man, they argue, must be based on a static society in a universe
which is steadily moving downhill. But both interpretations elevate
the constancy of entropy over the creationists’ concept of fixed ethical
order, The only ultimate order for the humanist is the long-run movement
toward disorder and death. The ethical debate among humanists centers
around the question of what mankind needs to do to withstand tem-
porarily the effects of “inescapable entropy,” and how long those
effects can be withstood.

False Hope
The social entropist may appear to be offering mankind a more
consistent ethical vision of “the life of entropy,” but this recommended
lifestyle is really not very consistent. “Entropic man” really cannot

6. Rushdoony, Introduction to E. L. Hebden Taylor, The New Legality (Nutley,
New Jersey: Craig Press, 1967), pp. vi-vii; the text of this citation was incorrectly
printed in Taylor’s book and was later corrected by Mr. Rushdoony.
