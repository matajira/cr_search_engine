The Unnaturalness of Scientific Natural Law 29

What about electrons? What about radioactive decay? Does a
physical process of cause and effect govern them? Is there a physical
explanation for the decay of a particular atom at a particular time?
No, says quantum mechanics. Gribbin writes that “no ‘underlying
reason’ for radioactive decay or atomic-energy transitions to occur
when they do has ever been found. It really does seem that these
changes occur entirely by chance, on a statistical basis, and that
already begins to raise fundamental philosophical questions. In the
classical world, everything has its cause. But in the world of the
quantum, such direct causality begins to disappear as soon as we
look at radioactive decay and atomic transitions. An electron doesn’t
move down from one energy level to another at a particular time for
any particular reason. . . . No outside agency pushes the electron,
and no internal clockwork times the jump. It just happens, for no
particular reason, now rather than later.”

Things happen for no particular reason. This sounds crazy to the
average person. It sounded crazy to Schrédinger and Einstein, too. In
later years, Schrédinger said of his hypothetical experiment, “I don’t
like it, and I'm sorry I ever had anything to do with it.” Neverthe-
less, this nonsense is today the foundation of modern physical
science, from chemistry to genetic engineering. The idea of physical
cause and effect has disappeared in the discipline of subatomic physics. The
idea of statistical randomness has triumphed to such an extent that
scientists really believe that all there really is in the unobserved
world is statistical randomness, until they or one of their instruments
measures an effect. In short, “no observer—no reality.”

Lest Gribbin’s speculations regarding the lack of causality be
dismissed as the ramblings of an unrepresentative physicist, con-
sider the words of John von Neumann, one of the most respected
mathematicians of this century. He wrote what Herbert has called
the quantum bible.?” Neumann said that “there is at present no occa-
sion and no reason to speak of causality in nature — because no ex-
periment indicates its presence, since macroscopic experiments are
unsuitable in principle, and the only known theory which is com-
patible with our experiences relative to elementary processes, quan-
tum mechanics, contradicts it.” Historian of science Stanley Jaki

 

 

35. Ibid., p. 66.

36. Ibid., introductory page.

37. Herbert, Quanium Reality, p. 47.

38. J. von Neumann, Mathematical Foundations of Quantum Mechanics, tans. R. T.
Beyer (Princeton, New Jersey: Princeton University Press, 1955), p. 327; cited by
Stanley Jaki, The Relevance of Physics (University of Chicago Press, 1966), p. 362.
