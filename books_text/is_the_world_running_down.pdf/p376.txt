340

no sanctification, 170, 260
Ritkin’s strategy &, 71-72
see also dispensationalism
Presbyterianism, 229, 231
presuppositions, xxiii, 77, 208
prices, 88, 97, 168, 309, 316
priests, 311
production, 87, 100, 168-69, 316
progress, 77-78, 84-85, 91, 95, 164-66,
178, 177, 273
prophecy, 289, 290
providence, 33, 168-70
Prussia, 213
public schools, 195, 200, 209-10, 213,
253, 285
pumps, 51, 62
punctuated evolution, 77
Puritanism, 164, 313-14, 316

quantum physics
causation, 29
Einstein vs., 19
equations, 27
irrational, 31
theories, 20

questions (three key), 12

railroads, 312
randomness, in, 50-51, 53, 57, 124-26
(see also equilibrium)
Rankine, W. 8. M., 56
Rapture
amillennialists vs., 248
antichrist &, 287
biblical view of, 166
dating of, 290-91, 298
debt &, 291
ethics & 292
fever, 288
footnotes &, xix
Hunt's concern, 276, 298
“lifeboat,” 292
politics &, 272
reason, 236 (see also methadology)
rebellion, 99-100, 108, 132-33, 148,
152, 267
reconstruction, 132, 245

IS THE WORLD RUNNING DOWN?

red tape, 249
redemption, 161, Appendix C (see also
gospel, restoration}
reform, 227, 229, 233, 238, 283, 298
Reformation, 175-76, 180
regeneration, 63, 157
regularities in nature, 35, 37
Reid, Tommy, 74-75, 76-77
reincarnation, 64
relativism, 214, 235
religion, 233-34, 238
religious right, 272
Renaissance, 182
rescue missions, 166, 264
resources, 96
responsibility
Christians’, 251, 292
comprehensive, Appendix C
escape religion, 44
ethical cleansing, 156
hopelessness vs. 294
locus of, 254
optimism &, 10-1
personal and institutional, 225-27
power &, 251-54
retreat from, 255, 303
theology of, 255
time & 283, 292
Pharisees, 278
pietism vs., 255
restoration, 122-23, £31, 242
resurrection (of Jesus)
creationism &, xi, xix
discontinuity, 2
effects of, ix, xx-xxiii
entropy &, 10, 123, 168, 324
history &, 168
humanism’s alternative, 64
liberation of nature, 176-77
life expectancy &, 8, 110, 124, 168
methodology, xvi
new world order, 10
random?, 2n
Rifkin denies, 177
Satan's defeat, 114
social theory, 68-69
theology of, xx-xxii
