330

Berman, Harold, 148-49

Bible-Science Association, 70, 77

Biblical Blueprint Series, 295

biblical law, 228, 300 (see also
antinomianism, covenant, ethics)

Big Bang, 53, 63, 73-74, 109

“bigger is better,” 306-7

biology, 185

black skull, 21

Blum, Ed, 298

Blum, Harold, 120

Bohm, David, 24-25

Boltzmann, Ludwig, $5, 56-58, 61

box (see “open box”)

Brennan, Justice, 206

Bullinger, E. W., 149

bureaucracy, 86-87, 92, 157, 216, 233,
249, 254, 259

Burke, Edmund, 232

bush, 3

calendars, 305

Calvinism, 180, 268, 298

capitalism, 168-69

captivity, 247

carburetor, 128

cards, 125-26

Carson, Rachel, 141

cartel, xxviii

cataclysm, 245

cat experiment, 27-28

causality, 29, 37

CEN University, 271

Chafer, Lewis Sperry, 294

change, 159-60, 195, 196, 203-4

chaos, 31, 72

charismatics, 298-300

Chilton, David, 231n

Christ (see Jesus)

Christian Reconstruction
covenantal, 47
Dave Hunt &, 287
ethics &, 117, 291-94, 303
progress, 47

Christianity, x, 196, 228-32, 246-7 (see
also church, covenant, creeds)

IS THE WORLD RUNNING DOWN?

chureh
cleansing, 154-57
defeated?, 263
no maturation?, 260
polities &, 238
progress, 154-55
sanctification, 149-50, 154-57
membership standards, 42
state &, 232-40
Wrinkles, 154

Church Age, 114, 149, 264, 289, 297,
299

church history, 263

classical liberalism, 178-79

Clausius, Rudolph, 52-54, 56

cleansing, 153, 154-57, 275

clock of prophecy, 289

clocks, 75, 91, 104, 311-13

closed box
Darwinism, 191, 198
debate, 204-5
how big?, 96-97
no observer, 30
universe, 52, 61-62, 65, 75, 92, 109
see also “open box”

closed systems, 52

circular reasoning, 237

citizen, 226

citizenship, 156, 258

Clauser, John, 26

Colson, Charles, 258

Columbus, 182

common grace, 223-24

common ground, xxiii, 70, Appendix
A, 212

complexity, 73-79, 191-92 (see also
simplicity)

communism, 59-6

computopia, 318-20

condors, 140

consciousness, 137 (see also higher
consciousness)

conservation, 85, 87-88, 122, 180, 309

constants, 115, 117, 184, 185 {sce also
uniformitarianism)

Constitution, 216

continuity, 2, 11, 176, 202-3
