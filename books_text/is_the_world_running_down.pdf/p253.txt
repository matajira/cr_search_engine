The End of Illusions: “Creationism” tn the Public Schools 217

and inherently anti-Christian. I can understand why he was a firm
supporter of the doctrine of academic freedom.

But why should conservative Russell Kirk affirm his faith in such
a doctrine? Because Kirk is a defender of the medieval Roman Cath-
olic philosophical compromise called scholastic natural law theology.
He therefore plays into the hands of the God-haters whose expressed
goal is to remove all traces of God from the classroom. It was a Prot-
estant fundamentalist version of this same common-ground philoso-
phy that led the Creation Scientists to invent a compromised hybrid
doctrine of creation that self-consciously made no reference to the
Creator. The Supreme Court threw it out anyway —in fact, because
it made no sense. But like Dracula, it may rise again from the dead.

Gould Gloats

Thus, we should not be surprised to find Prof. Gould appealing
once again to academic freedom as the justification for the Supreme
Court’s decision:

But creation science is also a sham because the professed reason for im-
posing it upon teachers~to preserve the academic freedom of students to
learn alternative viewpoints —is demonstrably false. Creationists are right
in identifying academic freedom as the key issue, but they have the argu-
ment perversely backward.

It was their law that abridged the most precious meaning of academic
freedom, the freedom of teachers to follow the dictates of their consciences,
their training and their professional commitments. . . .

“Creation science” has not entered the curriculum for a reason so simple
and so basic that we often forget to mention it: because it is false, and
because good teachers understand exactly why it is false. What could be
more destructive of that mest fragile yet most precious commodity in our
entire academic heritage—good teaching—than a bill forcing honorable
teachers to sully their sacred trust by granting equal treatment to a doctrine
not only known to be false, but calculated to undermine any general
understanding of science as an enterprise? ?

The answer to this familiar trade union appeal is simple: he who
pays the piper calls the tune. The classroom evolutionists should be
forced by economic necessity to persuade people to pay voluntarily
for their time spent in the laboratory, classroom, and library. If there

12. Gould, “Verdict,” p. 34.
