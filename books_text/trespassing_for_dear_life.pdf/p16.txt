3

OBEDIENCE TO GOD
AND NOT TO MEN

Each person is responsible before God for everything he says
and does in his lifetime. Jesus warned us; “But I say unto you,
That every idle word that men shall speak, they shall give account
thereof in the day of judgment” (Matthew 12:36). Thus, a person’s
conscience is a lawful authority. The fundamental rule of govern-
ment is sedf-government under God's law. The primary enforcing agent
is the conscience. No other human government possesses the
God-given authority or the God-given resources to police every
aspect of each person’s daily walk before God. Any government
that attempts this is inherently tyrannical.

When a person faces God on judgment day, there will be no
committee beside him to “take the rap.” Only Jesus Christ can do
this for a person, as God’s lawful authorized authority who died in
place of a God-redeemed individual. There will be no one else ex-
cept Jesus Christ at the throne of judgment who can lawfully in-
tervene and tell God the Judge, “This person was following my or-
ders, and therefore should not be prosecuted.”

Therefore, the fundamental representative voice of God’s author-
ity in each person’s life is his own conscience. Because the individ-
ual. will face God on judgment day, the fundamental form of
human government is self-government. This is basic to Christian
ethical, social, and legal theory. Any society that attempts to deny
this principle of justice is in revolt against God.

This is not to say that a person’s conscience is absolutely sover-
eign. There has been no single, God-authorized human voice of

12
