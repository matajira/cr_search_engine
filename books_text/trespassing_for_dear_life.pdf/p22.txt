18 — Trespassing for Dear Life: What About Operation Rescue?

Is Lying Always Immoral?

Isn't tying always immoral? The Bible certainly does not teach
that it is. The Bible says that Christians should not lie to each
other, “Wherefore putting away lying, speak every man truth with
his neighbour: for we are members one of another” (Ephesians
4:25). But this rule does not always prevail in dealings between
civil governments or between governments and their citizens. For
example, civil governments certainly believe in the legitimacy of
mnilitary lying, so they train and send out spies, and they camouflage
troops and weapons. Moses sent spies into Canaan before the in-
vasion (Numbers 13). Joshua, who had been one of the spies
under Moses, did the same a generation later ( Joshua 2).

Are we to say such decisions by civil governments are morally
wrong? If so, then why did God allow Moses and Joshua to sénd
out spies to spy out the land of Canaan? In times such as today —
days filled with life-and-death crises — Christians had better not be
naive about such matters. If Christians are morally required by
God to avoid lying to the civil government in all cases, then on
what moral basis did Christians in Europe hide Jews in their
homes during the terror of the Nazis?

If you have qualms about accepting the idea of self-conscious
lying as a legitimate part of civil disobedience, please consider the
following passages in the Bible to see how God deliberately lies to
unjust civil rulers and false prophets in order to bring them low:

‘And the Lorn said, Who shall persuade Ahab, that he may
go up and fall at Ramoth-gilead? And one said on this manner,
and another said on that manner. And there came forth a spirit,
and stood before the Lorp, and said, I will persuade him, And
the Lorp said unto him, Wherewith? And he said, I will go forth,
and | will be a lying spirit in the mouth of all his prophets. And
he said, Thou shalt persuade him, and prevail also: go forth, and
do so. Now therefore, behold, the Loro hath put a lying spirit in
the mouth of all these thy prophets, and the Lorp hath spoken
evil concerning thee (I Kings 22:20-23);

For every one of the house of Isael, or of the stranger that so-
journeth in Isracl, which separateth himself from me, and setteth
