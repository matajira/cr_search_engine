Obedience to God and Not to Men . 15

Renouncing State Jurisdiction

The Bible therefore teaches that men are under the lawful
authority of one or more civil governments. As in church gov-
ernment, this judicial authority is supposed to be enforced hi-
erarchically, on an appeals-court basis. The civil law is given
to men by God through the state in order to establish boundaries
of lawful individual and corporate behavior. The biblical le-
gal principle is this: “Whatever is not forbidden is allowed.” Like
Adam in the garden, who could lawfully eat from any of the trees
in the garden except one, so is man allowed by civil law to do any-
thing he wants that is not explicitly prohibited in the Bible or
implicitly prohibited by the application of a biblical principle.
Civil government, like church government, imposes restraints on
evil behavior, its role is to keep men from doing evil acts, not to
make men good. It is supposed to impose negative sanctions
against evil behavior, The state is not an agency of personal salvation. It
is not supposed to save men; it is to protect them from the evil acts
of other men.

The individual is supposed to possess the God-given legal
right to remove himself from the jurisdiction of any civil govern-
ment that he believes to be immoral. Because civil governments
rule over geographical areas, the act of renouncing jurisdiction is
normally accomplished through personal emigration. Until World
War I, the right of legal emigration out of a nation and almost
universal immigration into a nation were honored in Europe and
North America. Very few nations required passports.

Because of the difficulty of moving, especially prior to the in-
vention of the steam engine (ships and trains), God has estab-
lished other means of renouncing jurisdiction. One of these is the
right of revolution. This right is lawful only when conducted by
lesser magistrates who have been raised up by God to challenge
immoral rulers, The Book of Judges deals with this right of revo-
lution by lesser magistrates and national leaders who revolt
against foreign invaders who have established long-term rule.
