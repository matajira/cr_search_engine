Obedience to God and Not to Men 19

up his idols in his heart, and'putteth the stumblingblock of his
iniquity before his face, and cometh to a prophet to inquire of
him concerning me; I the Lorp will answer him by myself: And I
will set my face against that man, and will make him a sign and a
proverb, and I will cut him off from the midst of my people; and
ye shall know that I am the Lorn. And if the prophet be deceived
when ‘he hath spoken a thing, I the Lorp have deceived that
prophet, and I will stretch out my hand upon him, and will de-
stroy him from the midst of my people Israel. And they shall bear
the punishment of their iniquity: the punishment of the prophet
shall be even as the punishment of him that seeketh unto him
(Ezekiel 14:7-10).

The relevant New Testament passage is Il Thessalonians 2:11-12:
“And for this cause God shall send them strong delusion, that they
should believe a lie: That they all might be damned who believed
not the truth, but had pleasure in unrighteousness.” Are we to say
that we cannot do likewise under any circumstances? Are we sup-
posed to be holier than God? People who try to be holier than God
wind up like Satan: initially tyrannical and. then impotent.

Does Might Make Right?

God brought negative sanctions in history against Egypt and
Jericho. God also brought positive sanctions in history to the mid-
wives and Rahab. This proves that God's civil government (the
civil aspect of God’s universal kingdom) is alone absolutely sover-
eign, and earthly civil governments are hierarchically subordinate
to God’s kingdom rule. The civil government that imposes final
sarictions in history and eternity is the absolutely sovereign civil
government in history and eternity.

This does not mean that “might makes-right.” It means that
God is right, God is mighty, and the kings of the earth will bow
down to him. It was not the task of the midwives or Rahab to at-
tempt to force the kings of their.day to bow down to God. They
were not required or authorized by God to bring visible negative
sanctions against these rebellious rulers. These women were not
civil rulers themselves, they had no legal authority to bring nega-
