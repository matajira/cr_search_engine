Criticisms of Operation Rescue 37

May Ged protect each of us from the morally fatal lure of hoped-
for respectability in the eyes of murderers and their moral accom-
plices in the pews. May God also protect us from the false dilemma
of “either/or,” where we are asked to choose between providing
homes for unwed mothers and refusing to challenge legalized
murder in the doorways and streets of our local towns and cities.

“Where Will It All End?”

Where does civil disobedience stop?

Where does moral cowardice stop? Where does fuil-time
Christian blindness to humanism’s long-term program of legalized
murder stop? In the Gulag Archipelago? In the gas chambers? Or
in Atlanta? I prefer to see Christian moral cowardice and judicial
blindness stopped in Atlanta. I can see where we are headed if
they persist.

Conclusion

The criticisms of Operation Rescue that I have covered in this
chapter are without theological foundation. This does not mean
that there are no valid criticisms possible. The valid criticisms are
of two kinds: strategic and tactical. Under strategic, I include the
fundamental question of armed resistance, which I discuss in the
Conclusion. Under tactics, f include such matters as counting the
cost and timing. Are we sure that there might not be a better way
to fight abortion, such as by organizing lawsuits against abortion-
ists that have hurt women? For example, what would a series of
successful multi-million dollar judgments against abortionists do
to the malpractice insurance premiums of abortionists? But these
tactical questions are not fundamental. They are not based on
theology.

Christians should not dismiss the program of non-violent re-
sistance that Operation Rescue recommends by appealing to silly
arguments. If the critics cannot do better than what they have
done so far, then Christians had better reconsider any initial hos-
tility they may have shown to Operation Rescue.
