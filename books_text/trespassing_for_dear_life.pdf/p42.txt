CONCLUSION

The intellectual bankruptcy of some of the published criti-
cisms of Operation Rescue does not automatically legitimize
Operation Rescue. We should not be lured into the ‘mistake of get-
ting on a controversial bandwagon just because those who say we
should stay bome are not intellectually or theologically capable of
defending their negative position.

T have discussed Operation Rescue as a real-world example of
non-violent Christian resistance, I see nothing wrong with what
they have done, as of late 1988. I have serious reservations about
where the group may be-in a few years, or where its radical spin-
offs may be. But in a time of social, moral, political, and medical
turmoil, as the 1990’s will almost certainly be, it is impossible to be
sure where any group will be.

What we need from Operation Rescue is an official statement
of tactical and strategic faith. We need a statement that under no
circumstances will Operation Rescue or any of its official repre-
sentatives call for armed resistance to civil authority-without public
support from a lesser magistrate. We need a statement that violence
will not be initiated by Operation Rescue groups against the bod-
ies of private citizens, except for unarmed physical interposition:
separating murderous physicians from their clients and targeted
unborn victims. We need also a statement that the deliberate de-
struction of the actual tools used by licensed murderers in their
crimes will endanger only the property and not any person. When
we get these official statements, we can then offer our support.

As a matter of tactics, it would be nice to hear that Operation
Rescue has advised all participants of the need to fight this battle

38
