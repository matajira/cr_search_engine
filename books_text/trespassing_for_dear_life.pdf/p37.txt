Criticisms of Operation Rescue 33

Or how about child pornography? “Before the sale of child
pornography will stop, hearts must be changed from rebellion
against God to love for God through faith in Christ. . . . Our ul-
timate goal is not a constitutional amendment, which will change
nothing.”

A constitutional amendment changes nothing? The civil law
changes nothing? Well, the enforcement of Federal laws surely
changed segregation in the South, and changed it within a single
decade, 1960-70. What kind of theology teaches that civil law
changes nothing?

I will tell you what righteous civil law changes: evil public acts.
This is all that civil law is supposed to change. It does not save
men’s souls; it is intended to change men’s public behavior.

Those who tell us that laws change nothing are taking up the
old liberal line: “You can’t pass laws against pornography. They
won't change anything.” How about this one? “Don’t bother to
pass a law against prostitution; it won’t change anything.” Or how
about this one: “It does no good to pass laws against selling cocaine
to children in exchange for homosexual favors. That won't change
anything.”

Men do not need to be converted to Christ in order for them to
change their outward ways. Nineveh was not converted to the
God of the Bible by Jonah’s preaching. Nineveh, the capital city
of Assyria, later invaded Israel and carried the Israelites away
into pagan captivity. Nineveh remained the capital of a covenant-
breaking empire. But almost overnight, in response to Jonah’s
message, Nineveh changed its outward behavior, and in so doing,
avoided the promised external judgment of God that Jonah had
predicted, (And when the judgment did not come, Jonah was
depressed.)

This is what the anti-abortionist protesters are trying to do:
avoid the external, national judgment of God. But the pre-whale Jonahs
of our day are telling them to go to Tarshish instead. Tarshish is so
much less controversial. Tarshish is so much safer.

Until you move out to sea, and the storm starts.
