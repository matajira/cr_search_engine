4
CRITICISMS OF OPERATION RESCUE

Operation Rescue’s tactic of “trespassing for dear life” has now
begun to divide the Christian community. It has already. divided
Christian leaders. This division appears to cut across denomina-
tional and even ideological lines. Christian leaders are being forced
to take a position, pro or con, with regard to the legitimacy of this
physical interposition. Like Congress, they prefer to avoid taking
sides, but the pressures can no longer be avoided easily.

There are two signs ini front of abortion clinics:

“No Trespassing”
“Thou Shalt Not Kill”

The “No Trespassing” sign is symbolically stuck into the grass.
The “Thou Shalt Not Kill” sign is literally being carried (or ought
to be literally carried) by an anti-abortion picketer.

The picketers have now begun to realize that they face a major
moral decision: either ignore the implicit “No Trespassing” sign or
ignore the covenantal implications of the “Thou Shalt Not Kill”
sign. The fact of the matter is that if Christians continue to obey
the abortionists’ “No Trespassing” signs, God may no longer
honor this humanistic nation’s “No Trespassing” sign to Him. He
will eventually come in national judgment with a vengeance. This
is a basic teaching of biblical covenant theology. (It is conven-
iently ignored in the pseudo-covenant theology of the critics.)

A small, hard core of dedicated Christians has now decided
that they cannot obey both signs at the same time. One of these

23
