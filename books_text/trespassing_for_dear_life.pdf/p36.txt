32 Frespassing for Dear Life: What About Operation Rescue?

in Christians’ eyes before their society becomes formally cove-
nanted to God? This is exactly what the critic is saying.

Let us recognize this argument for what it is: the standard liberal
theological line. Baptized, of course. It is the Bible-thumping funda-
mentalist’s version of the old liberal pitch: “The laws of the primitive
Hebrews were applicable only in the context of an agricultural
community, etc., etc.” Christians have been hit with this moral rel-
ativism for over a hundred years. This is what such an interpretation
of the Bible is: moral relativism, pure and simple. This is humanistic
antinomianism wrapped in covenantal swaddling clothes, This is the lan-
guage of a person who has, in the words of Proverbs 24:10, fainted
in the day of adversity, and whose strength is small.

“Enforcing Righteous Law Is Irrelevant”
Befare abortion will stop, hearts must be changed from rebellion against

God to love for God through faith in Christ... . Our ultimate goal is not a
constitutional amendment, which will change nothing.

Really? Then why did no nation legalize abortion until after
World War IT? Were they all Christian nations before World War II?

If we have to wait until almost all people in the U.S. are con-
verted to saving faith in Jesus Christ before we can stop abortion
in America, then only the postmillennialist can have any confi-
dence that legalized abortion will ever be stopped, and only then
during the millennium. Everyone else should give up the fight,
this theologian is telling us. There is no earthly hope. Abortion
will not be stopped this side of the millennium.

This is just one more excuse for sitting safely inside the walls
of your local church, or handing out tracts on the Bill of Rights-
protected sidewalk. It is an excuse supported by one of the flimsiest
arguments imaginable, namely, that passing a law changes nothing.

Let us substitute the words “selling cocaine to minors” for the
word. “abortion.” Here is what we get: “Before the sale of cocaine
to minors will stop, hearts must be changed from rebellion against
God to love for God through faith in Christ. . . . Our ultimate goal
is not a constitutional amendment, which will change nothing.”
