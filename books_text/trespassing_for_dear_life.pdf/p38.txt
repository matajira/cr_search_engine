34 Trespassing for Dear Life: What About Operation Rescue?

“Living the Gospel” in Temporary Safety
Onty the preaching and teaching and living of the gospel of Christ in. the

power of the Holy Spirit is able to awaken an apostate church to repentance
and faith,

This is exactly what the anti-abortion activists say. Living the
gospel of Christ means doing what you can do effectively that may save judi-
cially innocent lives, But for our “deeply concerned” antinomian
critic, living for the gospel apparently means sitting safely in the
sanctuary and praying prayers in private. And running for politi-
cal office, of course.

Where are the imprecatory psalms in all this? Where are preach-
ers who are willing to stand before their congregations on Sunday
morning, praying down the visible curses of God on named abor-
tionists, named civil magistrates, and all U.S. Supreme Court
justices who voted for Roe x Wade? Where’is Psalm 83 in their
churches’ liturgies? When I at last locate some “safety first” critic
of non-violent confrontation who is at least involved in weekly
picketing and praying public imprecatory psalms as part of his
church’s weekly worship service, I will be more inclined to take his
arguments seriously. Until then, I prefer to reject these arguments
as self-justifying pious gush.

The Invisible Gospel

Preaching the gospel is sufficient to change all things. It does no good to
look for physical solutions, such things as demonstrations or planned civil
rebellion. Preaching is sufficient,

To which I answer, with James: “What doth it profit, my breth-
ren, though a man say he hath faith, and have not works? Can faith
save him? If a brother or sister be naked, and destitute of daily
food, And one of you say unto them, Depart in peace, be ye warmed
and filled; notwithstanding ye give them not those things which
are needful to the body; what doth it profit? Even so faith, if it
hath not works, is dead, being alone. Yea, a man may say, Thou
hast faith, and I have works: shew me thy faith without thy works,
and I will shew thee my faith by my works” (James 2:14-18).
