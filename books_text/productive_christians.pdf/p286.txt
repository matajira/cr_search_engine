272 Productive Christians in an Age of Guilt-Manipulators

The point is not simply that these declines took place during
the same period. The crucial point is that Murray establishes,
beyond any reasonable doubt, that these changes were caused by
the welfare system. As he argues: “When large numbers of people
begin to behave differently from ways they behaved before, my
first assumption is that they do so for good reason”—the “good
reason” being the changes in the incentive structure of American social
policy.5¢ All the changes in the behavior of the poor “could have
been predicted (indeed, in some instances were predicted) from
the changes that social policy made in the rewards and penalties,
carrots and sticks, that govern human behavior. All were rational
responses to changes in the rules of the game of surviving and getting
ahead.”>? In welfare, in the risks attached to crime, and in educa-
tion, the “rules of the game” were radically altered during the
1960s, and these changes, which reinforced each other, had an
enormous impact on the incentive structure facing the poor.

The changes in the welfare system informed the poor that
there was no longer any social stigma in becoming dependent
upon aid (indeed, it was a “right”); personal responsibility was de-
nied; and all welfare recipients were equally deserving of lifelong
support (in fact, the very concept of “deserving poor” was dis-
carded). The poor became pauperized. Low-income males found that
the financial rewards of dropping out of the labor force were su-
perior to those of holding down a job; low-income women were
faced with the plain economic fact that living with a man out of
wedlock and bearing illegitimate children—in effect, marrying
the state—would guarantee a stable income. Many men and
women chose these avenues; they were the paths of least
resistance.

In the area of crime, the risks of arrest, trial, and punishment
especially for the poor, and even more especially for poor youths
~were considerably lowered. It began to be perceived, increas-

56. The best all-around study of the nature of incentive structures and their
effects on decision-making is Thomas Sowell’s Knowledge and Decisions (New York:
Basic Books, 1980).

57. Murray, pp. 154f. Italics added.
