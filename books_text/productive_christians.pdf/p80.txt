66 Productive Christians in an Age of Guilt-Manipulators

those who desired “gradual socialism.” It cannot be done, he said;
it cannot come about without violent expropriation, The basic
tule is this: “If you desire to confiscate, you must confiscate.”

Ronald Sider claims to be working for liberation and equality
for the oppressed. He declares that slavery is wrong, and. appears
to damn it with every breath. Yet his concrete proposals for
reform look quite the opposite under the searchlight of biblical
law. I am willing to grant him some measure of sincerity — some;
but whether or not he is aware of what he is really doing, the effect
of his proposals would be a totalitarian, oppressive regime the
likes of which Hitler was never able to achieve. It would make the
state nothing less than God. And when man plays god, the result
is always bondage. During the Second World War, F. A. Hayek
published a stirring warning to the people of England, who were
blindly pursuing the policies which had brought the Nazis to
power in Germany, He wrote: “The ‘substitution of political for
economic power’ now so often demanded means necessarily the
substitution of power from which there is no escape for a power
which is always limited. What is called economic power, while it
can be an instrument of coercion, is, in the hands of private indi-
viduals, never exclusive or complete power, never power over the
whole life of a person. But centralized as an instrument of political
power it creates a degree of dependence scarcely distinguishable
from slavery.”21

The Jubilee
With everything Ronald Sider has said about the “Jubilee Prin-
ciple,” it might be expected that an extended discussion of the
subject would naturally fall under the heading of the biblical Poor
Laws. However, it will not be examined in this chapter, for two
Teasons.
First, Sider refers to the Jubilee so often, and draws so many

20. Hilaire Belloc, The Savile Stats (Indianapolis: Liberty Classics, 1977), p. 168,
2. FA. Hayek, The Road to Sefdom (The University of Chicago Press, 1944),
pp. 145f, Italics added.
