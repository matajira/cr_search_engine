Biblical Law and Christian Economics 31

the arts, and everything else, He is always Lord. His command-
ments for righteous living—by the individual, the community,
social institutions and departments of government—have not
been altered. Their relevance and authority will remain until
heaven and earth pass away.

God’s Law and the State

In some ways, this section is the most important part of the book:
a great deal of what I will say in later chapters will simply expand
on the doctrines presented here. This is because Ronald Sider’s
assumptions about the role of the state underlie much of his argu-
ment. If Mr. Sider had agreed with the biblical view of the respon-
sibilities and limits of civil government, his book would be the size
of a small pamphlet: he would have had very little to say.

The basic outline of the duties of civil government is found in
Romans 13:4. Paul tells us that the.civil authority “is a minister of
God to you for good. But if you do what is evil, be afraid; for it
does not bear the sword for nothing; for it is a minister of God, an
avenger who brings wrath upon the one who practices evil.”

Every civil ruler, Paul says, has an obligation to be “God's min-
ister.” In other words, he must administer the word of God in his
sphere of authority. To the extent that he fails:to do this, he is an
unfaithful minister—just as any pastor would be an unfaithful
minister of the gospel if he failed to apply God’s word to his con-
gregation, Jesus Christ is Lord of ail rulers, in heaven and in
earth (Ephesians 1:20-22), and all rulers are commanded to sub-
mit to His Lordship or be destroyed:

Now therefore, O Kings, show discernment;

Take warning, O judges of the earth.

Worship the Lorp-with reverence,

And rejoice with trembling.

Do homage to the Son, lest He become angry,
and you perish in the way,

For His wrath may soon be kindled.

How blessed are all who take refuge in Him!

(Psalm 2:10-12)
