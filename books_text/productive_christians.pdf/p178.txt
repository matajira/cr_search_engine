“Does the need for ‘privacy’ and ‘space’ make it right for one fam-

ily to occupy a house that . . . could easily meet the needs of ten
or fifteen people?”

(Ronald Sider, Living More Simply

for Evangelism and Justice, p. 18)

“My son, if sinners entice you, do not consent. If they say, ‘Come
with us... . Throw in your lot with us, we shall all have one
purse’, My son, do not walk in the way with them, keep your feet
from their path. For their feet run to evil, and they hasten to shed
blood.”

(Proverbs 1:10-16)
