Bibliography 403

Friedman, Milton and Rose. Free to Choose: A Personal Statement.
New York: Avon Books, 1981.

Gilder, George. The Spirit of Enterprise, New York: Simon and
Schuster, 1984.

. Wealth and Poverty. New York: Basic Books, 1981.

Greaves, Bettina Bien. Free Market Economics: A Basic Reader. Irv-
ington, NY: Foundation for Economic Education, 1975.

. Free Market Economics: A Syllabus. Irvington, NY:
Foundation for Economic Education, 1975.

Greaves, Percy L. Jr. Mises Made Easier: A Glossary for Ludwig von
Mises’ HUMAN ACTION. Dobbs Ferry, NY: Free Market
Books, 1974.

—_______. Understanding the Dollar Crisis. Boston: Western
Islands, 1973.

Griffin, Bryan F. Panic Among the Philistines. Chicago: Regnery
Gateway, 1984.

Groseclose, Elgin. America’s Money Machine: The Story of the Federal
Reserve. Westport, CT: Arlington House, 1980.

———___—_—.. Money and Man: A Survey of Monetary Experience.
Norman, OK: University of Oklahoma Press, 4th ed., 1976.

Harper, F. A. Why Wages Rise. Irvington, NY: Foundation for
Economic Education, 1957.

Hayek, Friedrich A. The Constitution of Liberty. Chicago: The Uni-
versity of Chicago Press, 1960.

——_--.. New Studies in Philosophy, Politics, Economics, and the
History of Ideas. Chicago: The University of Chicago Press,
1978.

—_________. The Road to Serfdom. Chicago: The University of
Chicago Press, 1944.

. Studies in Philosophy, Politics, and Economics.
Chicago: The University of Chicago Press, 1967.
~~» ed. Capitalism and the Historians. Chicago: The
University of Chicago Press, 1954.
