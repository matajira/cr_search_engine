424

Borgin, Karl, xitin, 299n

Brethren of the Free Spirit, 325-26
Brookes, Warren, 256n.

Brown, John, 4, 73, 75

Bududira, Monseigneur Bernard, 307
Bukharin, Nikolai, 340n

Bukovsky, Vladimir, 283n
Butterfield, Fox, 292n

Calvin, John, 333n, 353n

Cannibalism, 278

Capital, 378, 394-95

Capitalism. Se Free enterprise

Capitalists vs. free market, 280-82

Carson, Clarence B., 9n, 87, 88n.

Carter, Jimmy, 253-54, 260

Carter, Ruth Stapleton, 260-61

Cathars, 324-25, 335

Certification, 348-49

Chamberlain, John, 9, 218

Chamberlain, W. H., 291

Channing, William Ellery, 74

Chaplain, U. S., 360

Charity, 8, 45-67, 110, 169-72, 174,
318-19

Chavez, Cesar, 46

Chavez, Manuel, 46

Cheka, 291

Chernenko, Konstantin, 261, 285

Chicago Declaration, 335

Chin Shih Huang, 338

China, 291, 292n, 297, 304, 338

Christian colleges, 348-49, 350

Christian Reformed Church, 350-351

Christian Socialism, 3-7, 9, 244,
304-09, 310-12, 316-17, 322-35,
341-42, 396

Civil rights policies, 273, 275

Clark, Colin, 117

Class conflict, 43, 83, 150, 357

Clouse, Robert G., 140n, 219n, 250n,
310, 312n, 314n, 364

Club of Rome, 52

Productive Christians in an Age of Guilt-Manipulators

Cobo, Father Bernard, 338n

Cochrane, Charles Norris, $6n

Cohen, Stephen F., 340n

Cobn, Norman, 323, 325n, 331n,
354n

Colet, John, 142

Colonialism, 276-77, 294-95, 302-03

Columbus, Christopher, 277

Commodities, 41, 378

Commodity agreements, 103-05, 154,
379

Communion, 54, 166-67

Communism, 3-4, 8, 208, 217, 258,
285-86, 324, 325n, 331, 333, 335,
379
of Jesus and early church, alleged,

168-69

Compassion, 223, 273, 282

Competition, 125-30, 183, 293-94,
379, 391, 393-94

Comte, August, 325n

Conservatism, 33, 218

Corbett, Kathleen, xiiin, 299n

Courville, Donovan, 160

Creation, 348-49, 360

Credit expansion, 197-98, 379-80, 381,
386-87

Crime, 271, 272-73

Currency, 380

Cumbey, Constance, 250

Curse, 21, 46, 48, 49, 92-95, 115-16,
120-21, 157, 183-85, 191, 195, 201,
221, 243-44, 302, 380, 382

Darwinism, 348-49, 350, 390
Davis, John Jefferson, 250
Death penalty, 290-91
Death-wish, 324, 340-42
Debasement of currency, 41-42,
197-98, 200, 380
“Decentralization,” 260, 261, 262
Defense, 285n
Deism, 179, 180-181, 200, 381
