408 Productive Christians in an Age of Guilt-Manipulators

Carson, Clarence. The World in the Grip of an Idea. New Rochelle,
NY: Arlington House, 1979.

Cohn, Norman. The Pursuit of the Millennium: Revolutionary
Millenarians and Mystical Anarchists of the Middle Ages. New York:
Oxford University Press, revised edition, 1970.

Djilas, Milovan. The New Class: An Analysis of the Soviet System.
New York: Frederick A. Praeger Publishers, 1957,

—__.____.. The Unperfect Society: Beyond the New Class. New
York: Harcourt, Brace & World, 1969.

Golitsyn, Anatoliy. New Lies for Old: The Communist Strategy of
Deception and Disinformation. New York: Dodd, Mead & Com-
pany, 1984,

Gouldner, Alvin W. The Tivo Marxisms: Contradictions and Anomalies
tn the Development of Theory. New York: Oxford University Press,
[1980] 1982.

Harris, Joan M. The Sojourners File, Washington, D.C.: New Cen-
tary Foundation, 1983.

Isaac, Rael Jean, and Erich Isaac. The Coercive Utopians: Social
Deception by America’s Power Players. Chicago: Regnery Gateway,
1983.

Kirkpatrick, Jeanne J. Dictatorships and Double Standards: Ra-
tionalism and Reason in Politics. New York: Simon and Schuster,
1982,

Kuehnelt-Leddihn, Erik von. Leftism: From de Sade and Marx to
Hitler and Marcuse. New Rochelle, NY: Arlington House, 1974.

Laquer, Walter, ed. Fascism: A Reader’s Guide. Berkeley: University
of California Press, 1976.

Lecky, William Edward Hartpole. Democracy and Liberty. Two vols.
Indianapolis: Liberty Classics, [1896] 1981.

Mackay, Thomas, ed. A Plea for Liberty: An Argument Against Social-
ism and Socialistic Legislation. Indianapolis: Liberty Classics, 1981.
Markov, Georgi. The Truth that Killed. New York: Ticknor &

Fields, 1984,
