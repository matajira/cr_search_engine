162 Productive Christians in an Age of Guilt-Manipulators

world, through such activities as money-lending (Deut. 23:12).
The spoils of war in Joshua's day could not be relied upon for
more than one or two generations, if the sons of the covenant re-
mained faithful. There would be too many sons of the covenant for the tiny
nation to support.

“This is not the sort of exegesis we find in the socialistic inter-
pretations of the radical anabaptists, yet the logic of the demo-
graphics of dominion is obvious. The Jubilee Year could become
economically significant to a family only if the land was in sin,
and the curse of zero growth was upon them. If the Jubilee Year
worked at all in the land of covenantally faithful Israel, or in any
land which came under the rule of God's law, it worked as a disin-
centive to remain in the land of one’s fathers. It was God’s way to
tell faithful societies to have no hape in geography. Godly men cannot
plan on inheriting a portion of an original inheritance, the spoils
of war. They must make plans to move outward, bringing the
whole world under God's law.”!?

Again, Sider has blown his cover. He has no intention of sub-
mitting to biblical standards of justice. He uses the Bible to mask
his real intentions with a superficial Christian flavor, but what he
wants is socialistic redistribution of capital. The Jubilee, for a limited
time and in a limited area, called for restoration —not “redistribu-
tion” or “equalization”—of specified, non-income-producing,
ancestral lands to deserving heirs. It cannot be applied outside
Israel. It cannot be applied after the resurrection of Christ. And it
cannot legitimately be used as a smokescreen for socialism.

If Ronald Sider weré really concerned for the poor, he would
seek to implement the biblical Poor Laws we have studied. If he
were concerned for the poor, he would seek to encourage the
godly investment of private capital so that real wealth for all of so-
ciety would rise. If he were concerned for the poor, he would en-
courage them to build, work, and save for the future, resisting the
attempts of an ungodly state to enslave them. If he were con-

17. Gary North, “The Fulfillment of the Jubilee Year,” Biblical Economics Today,
April/May 1983 (revised).
