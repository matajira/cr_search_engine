278 Productive Christians in an Age of Guilt-Manipulators

Do we reaily need to refute this nonsense? Is it necessary to
point out that people who ran around naked with bones in their
noses, practiced cannibalism, incest, and human sacrifice, used
“stone age” tools, practiced hunting-and-gathering and slash-and-
burn agriculture, worshiped clay gods and real demons, and dis-
figured their women and children, were in almost every sense less
developed than the Christian civilization of Western Europe?
Earth to Captain Sider: Check the oxygen level in your tanks.

The real issue in the wealth-vs.-poverty question—the thing
which requires explanation —is not the existence of poverty. It is
the existence of wealth. Sider seems to think that wealth is “natu-
ral,” and that poverty has been imposed on the Third World by
others. But that is exactly the reverse of the truth. It is poverty
which exists in abundance. Poverty has been the condition of
most people in the world, for most of human history. It is, in fact,
amazingly easy to be poor. Any one of us could do it in a minute,
eyes closed and hands behind his back. What is difficult is creat-
ing wealth, increasing the productivity of the earth and raising the
standard of living for the peoples of the world, As Adam Smith
put it: “When you have got a little, it is often easy to get more.
The great difficulty is to get that little.”® In fact, economic pro-
ductivity has very litde te do with resources as such: There are
millions of very poor people in the Third World who have access
to cultivable land. Their low productivity primarily reflects “want
of ambition, energy anid skill, and not want of land and capital.”66
Peter Drucker reminds us that “no country is ‘underdeveloped’
because it lacks resources. ‘Underdevelopment’ is inability to ob-
tain full performance from resources; indeed, we should really be
talking of countries of higher and lower productivity rather than
of ‘developed’ or ‘underdeveloped’ countries. In particular, very
few countries— Tibet and New Guinea may be exceptions —lack
capital. Developing countries have, almost by definition, more

65. Adam Smith, An Inguity into the Nature and Causes of the Wealth of Nations
(New York: The Modern Library, 1937), p. 93.

66. P.T. Bauer, Reality and Rhetoric: Studies in the Economics of Development (Cam-
bridge, MA: Harvard University Press, 1984), p. 8.
