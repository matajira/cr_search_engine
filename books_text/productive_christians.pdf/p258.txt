244 Productive Christians in an Age of Guilt-Manipulators

once. bragged about God’s blessings; we now feel ashamed of
them. Our freedom has become an unstable anarchy; our stability
has resulted in stagnation. God has judged.

And in the midst of all this, Ronald Sider and the Christian so-
cialists have appeared as God’s scourges of further chastisement,
and a disobedient, antinomian church has blindly followed them
into the ditch. Christian organizations, abandoning God’s law,
have endorsed statism as the cure for our diseases. The moral fiber
of our country—made strong by obedience—has rotted away in
envy and guilt. Culturally and psychologically, we have committed
suicide, and our sins will be visited upon our great-grandchildren,
The wrath of God is evident in our seduction by shoddy, morally-
bankrupt misanthropes. Our future—if the spokesmen for salvation-
by-suicide are any indication of it—will be a downward spiral into
self-absorption, slavery, despair, and damnation.

And yet we still have our Bibles, we still have our homes, we still
have a considerable remainder of the rule of law; capital of all kinds
with which to build. Will we repent? I am just enough of an op-
timist to hope that we will yet turn to God and begin again the con-
struction of a Christian culture based on biblical law. “What can be
learned from the experience of the revolutionary era? That man,
without God, even with the circumstances in his favour, can do
nothing but work his own destruction. Man must break out of the
vicious revolutionary circle; he must turn to God whose truth alone
can resist the power of the lie, Should anyone consider this momen-
tous lesson of history to be more sentimental lament than advice for
politics, he is forgetting that the power of the Gospel to effect order
and freedom and prosperity has been substantiated by world
history. Let him bear in mind that whatever is useful and beneficial
to man is promoted by the fear of God and thwarted by the denial
of God. He should bear in mind especially that the revolutionary
theory was an unfolding of the germ of unbelief and that the
poisonous plant which was cultivated by apostasy from the faith
will wilt and choke in the atmosphere of a revival of the faith.”

6. G. Groen van Prinsterer, Unbelief and Revolution: Lecture XI (Amsterdam:
‘The Groen van Prinsterer Fund, 1973), p. 22.
