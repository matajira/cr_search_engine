“Increased foreign aid is imperative. The developing countries
simply do not have the resources . . . outside help is essential.”
(Ronald Sider, Rich Christians in

an Age of Hunger, pp. 218f. [p. 205])

“And the Lorp will make you abound in prosperity, in the off-
spring of your body and in the offspring of your beast and in the
produce of your ground, in the land which the Lorp swore to your
fathers to give you. The Lorn will open for you his good store-
house, the heavens, to give rain to your land in its season and to
bless all the work of your hand; and you shall dend to many na-
tions, but you shall not borrow.”

(Deuteronomy 28:11-12)
