8
THE LAW. AND THE PROFITS

The notion that there can be any such thing as “unfair profit” is
one of the oldest socialist ideas, produced by two problems which
are central to the very nature of socialism: envy and ignorance.
Envy, because socialists assume that anyone who is successful
taust be wicked; and ignorance, because socialists do not under-
stand economics.

In his brief section on profits,1 Ronald Sider has fallen prey to
both of these errors, The section is titled “Making a Fair Profit?”
—the implication being, of course, that the high profits he goes on
to cite are unfair, He lists examples of certain profit-making en-
terprises, and then states that ‘‘the returns on investments in poor
countries are unjustly high.”? His logic seems to be that “high”
profits—or, at least, profits above a certain undefined percent-
age—are necessarily unjust. No proof is given for this assertion.
He does not tell us just how high profits should be; ‘he does not
reveal his infallible source which determines what a “fair” return
on investment is; he does not show that these profits were obtain-
ed in an unjust manner (in which case it would be not the profits
but the entire enterprise itself that is unjust). To repeat: Sider
says that certain profits are very high, and concludes that they are
unjust. As long as you don’t think demonstration is essential to an
argument, you could say that Sider has put his case very nicely.

He seems to feel uneasy about this, however. Some uneducated

1, Sider, Rick Christians, pp. 160. [cf. pp. 1511.1.
2. Did., p. 162.

123
