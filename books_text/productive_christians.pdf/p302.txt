288 Productive Christians in an Age of Guilt-Manipulators

and are hostile to or sabotage socialist construction are all enemies of the
people.96

It is not difficult at all for Communists to accomodate their pro-
gram to the will of the people, because they decide who “the people”
are, Those who oppose socialism just aren’t people.

The same writer for theOtherSide went on to warn his readers
never to use the word terrorism when describing the actions of the
revolutionaries, because they are “resistance forces”; so whatever
they do, it isn’t terror.°” A Presbyterian minister argued similarly
in Sojourners:

Violence in the Scriptures is not what someone does to try to-defend the
oppressed-poor from the injustices that threaten their lives. Rather,
violence in the Bible refers to what the oppressed-poor suffer at the
hands of their wealthy oppressors.%®

This may provide a clue about what Sider means when he says
he’s against “violence.” Violence is only what the upper classes do
to the lower classes. What the lower classes do may be aiolent, but
it’s not violence,9?

‘The essential point to be grasped on this issue is the central
fact that Dr. Sider’s appeals for “charity” and “equality” call for ac-
tion by the state. That means coercion, the use of force. And that
means guns, handcuffs, prisons, firing squads, and all the rest. As
Paul Johnson has well observed, “the destructive capacity of the
individual, however vicious, is small; of the state, however well-
intentioned, almost limitless.”100

96. Quotations from Chairman Mao Tse-Tung (Peking: Foreign Languages Press,
1966), pp. 45f. Italics added.

97. Blase Bonpane, p. 40.

98. Tom Hanks, “Why People Are Poor,” Sojourners (January 1981), p. 21.

99, If this sounds confusing to you, you probably haven't been to seminary.
Seminaries often teach Greck and Hebrew, but the common tongue is
Doublespeak. Example: “The Bible is infallible, but not inerant,” The achieving of
a full professorship is usually a mark of exceptional fluency in this language. Its
use keeps the money coming in from the enemies of the psople, in order to fund the
activities of the seminary on behalf of the people.

100. Paul Johnson, Modem Times: The World from the Twenties to the Eighties
(New York: Harper and Row, 1983), p. 14.
