BIBLIOGRAPHY

Theological Foundations

Balke, Willem. Calvin and the Anabaptist Radicals. Grand Rapids:
William B. Eerdmans, 1981.

Campbell, Roderick. Israel and the New Covenant. Tyler, TX:
Geneva Ministries, [1954] 1982.

Chilton, David. Paradise Restored: A Biblical Theology of Dominion.
‘Tyler, TX: Reconstruction Press, 1985.

Clouse, Robert G., ed. Wealth and Poverty: Four Christian Views of
Economics, Downers Grove, IL: InterVarsity Press, 1984.

Davis, John Jefferson. Your Wealth in God’s World: Does the Bible Sup-
port. the Free Market? Phillipsburg, NJ: Presbyterian and Re-
formed Publishing Co., 1984,

DeMar, Gary. God and Government. Three vols. Atlanta: American
Vision Press, 1982, 1984, 1985.

Eidsmoe, John. God and Caesar: Christian Faith and Political Action.
Westchester, IL: Crossway Books, 1984.

Grant, George. Bringing in the Sheaves: Transforming Poverty into Pro-
ductivity, Atlanta: American Vision Press, 1985.

Griffiths, Brian. Morality and the Market Place: Christian Alternatives to
Capitalism and Socialism. London: Hodder and Stoughton, 1982,

Jordan, James B. The Law of the Covenant: An Exposition of Exodus
21-23. Tyler, TX: Institute for Christian Economics, 1984.

Kaiser, Walter C. Jr. Toward Old Testament Ethics. Grand Rapids:
Zondervan, 1983.

399
