196 Productive Christians in an Age of Guilt-Manipulators

law, His judgment upon the disobedient, and the urgent need for
repentance.

After getting their attention by thundering against the sins of
the surrounding nations (1:3-2:5)—a fact which shows the law’s
validity outside the theocratic kingdoms of Israel and Judah—
Amos then turned his guns toward Israel, and dealt immediately
with its sins in the area of economics. Righteous men were being
condemned because their wealthier enemies had bribed judges,
and poor men were sold into slavery over trifles (2:6). Instead of
obeying the laws commanding mercy on poor brethren (Leviticus
25:25-28), the rich men of Israel were trampling them into the
dust. Gross immorality, perhaps even ceremonial prostitution,
was taking place (2:7). In violation of the laws on debt (Deuter-
onomy 24:10-13), the poor man’s collateral was withheld from
him—and all the while, the people were claiming to be devout
and pious men, daring even to display the “profits” of their sins in
the house of worship (2:8). Their response to those who brought
God’s word was to attempt to corrupt them, and where that
failed, to silence them (2:12).

Amos included in his denunciation the wealthy women of Israel
(4:1-3), calling them “cows of Bashan.” Bashan was a well-
watered, fertile region, and the cattle of Bashan were, under-
standably, quite fat—which is just Amos’ point about these
women. He accused them of oppressing the poor and crushing the
needy. This charge was not because they had actually done
anything personally against the poor, for they probably rarely
ever came in contact with the poor people at all. The basis for the
charge was their expensive demands on their husbands. Their
self-indulgence crushed those who were below them. How is this
possible? The socialist myth—that wealth per se is a cause of
poverty —is not supported here. There are several possible ways
that their affluent lifestyle could have caused oppression. Since
the poor were ignored in Israel, the luxuries of these women were
probably bought at the expense of the Tithe, zero-interest loans,
gleaning, and the like, Their weak husbands were unable to pro-
vide for the needy because their wives’ greed made it impossible to
