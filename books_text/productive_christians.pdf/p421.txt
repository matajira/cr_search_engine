Bibliography 407
—________... Race. and. Economics. New York: David McKay
Co., 1975,
Taylor, E. L, Hebden. Economics, Money and Banking. Nutley, NJ:
The Craig Press, 1978.

Tucker, Robert W. The Inequality of Nations. New York: Basic
Books, 1977,

Tucker, William. Progress and Privilege: America in the Age of En-
vironmentalism, Garden City, NY: Anchor Press/Doubleday,
1982.

Weber, James A. Grow or Die! New Rochelle: Arlington House,
1977.

Williams, Walter E. America: A Minority Viewpoint. Stanford, CA:
Hoover Institution Press, 1982.

—_—_____. The State Against Blacks. New York: McGraw-Hill
Book Co., 1982.

Socialism and Revolution

Anderson, Digby, ed. The Kindness that Kills: The Churches?
Simplistic Response to Complex Social Issues. London: SPCK, 1984.

Belli, Humberto. Nicaragua: Christians Under Fire, Garden City,
MI:The Puebla Institute, 1984.

Billington, James H. Fire in the Minds of Men: Origins of the Revolu-
tionary Faith. New York: Basic Books, 1980.

Bloodworth, Dennis. The Messiak and the Mandarins: Mao Tsetung
and the Ironies of Power. New York: Atheneum, 1982.

Braley, Russ, Bad News: The Foreign Policy of the New York Times.
Chicago: Regnery Gateway, 1984.

Brown, Anthony Cave, and MacDonald, Charles B. On a Field of
Red: The Communist International and the Coming of World War IT.
New York: G. P. Putnam’s Sons, 1981.

Butterfield, Fox. China: Alive in the Bitter Sea. New York: Times
Books, 1982.
