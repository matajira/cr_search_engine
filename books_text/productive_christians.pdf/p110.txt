96 Productive Christians in an Age of Guilt-Manipulators

changed the truth of God for a lie” (Romans 1:23-25). The central
fact about the heathen is that they are living in willful rebellion
against the one true God, and are therefore under God’s curse,
The economic issue is a symptom of their condition; but the prob-
lem with pagans is primarily religious and ethical. To neglect this
central point in order to focus only on their poverty is radically
unbiblical and immoral.

If pagans are truly to be helped, they and their culture must be
converted to the Christian faith. If we seek merely to neutralize
the effects of God's righteous judgment upon them, we are
manifesting contempt for Him, and our efforts will nat be blessed.
Our major concern must be to reconcile the heathen to the God
whom they have offended. The problem is religious; the solution
is religious as well,

To take only one example of how the religious issue affects
culture—how the “wisdom” of pagans is actually foolishness
(Romans 1:22)—consider the pagan view of time. Western civiliza-
tion has been transformed by the biblical concept of time, involv-
ing dinear development through history, with a beginning, middle,
and end.? This is one of the most basic and crucial differences be-
tween Christianity and paganism, which tends to view time as
oyelical.28 In India, this non-Christian attitude alone is enough to
keep the culture from becoming productive. Hinduism sees
human existence (it can hardly be called history) in terms of never-
ending four-billion-year cycles, Hindi, the nation’s most widely
spoken language, uses the same word (kal) for both yesterday and
tomorrow. With his notions of past and future blurred and mixed
together, with virtually no comprehension of chronology, the Hin-
du’s sense of time is as undeveloped as that of an infant. Those of

22. See Rousas John Rushdoony, The Biblical Philosophy of History (Nutley, NJ:
Presbyterian and Reformed, 1969); idem, The One and the Many: Studies in the Phi-
losophy of Order and Ultimacy (Tyler, TX: Thoburn Press, [1971] 1978), pp. 142-48,

23, See Charles Norris Cochrane, Christianity and Classical Culture: A Study of
Thought and Action from Augustus to Augustine (New York: Oxford University Press,
[1940] 1957), esp. pp. 456-516, On the practical, technological effects of the
Christian view of time, see David S. Landes, Revolution in Time: Clocks and the
Making of the Modem World (Cambridge, MA: Harvard University Press, 1983).
