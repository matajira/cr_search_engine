li4 Productive Christians in an Age of Guilt-Manipulators

it is simply incorrect to think that the population explosion in the poor coun-
tries is the sole or perhaps even the primary cause of widespread hunger in the
world. Our ever increasing affluence is also at the heart of the problem.*

Postponing for the present an examination of that last remark,
notice the slight change of tune. First, the population explosion
will lead to “global disaster.” Second, the population increase
isn’t really the problem at all. This does not mean, however, that
Sider is unconcerned about overpopulation. Even though he ad-
mits again that overpopulation is not “the main reason for contin-
uing poverty,” he makes this demand two paragraphs later:

The right kind of aid—- focused especially on promoting labor-intensive,
agricultural development using intermediate technology—will help
check population growth . . . the right kind of action could probably
avoid disaster?

This is known in the language of Propaganda Chic as getting
‘em coming and going: Population growth is not the problem—
but if we don’t stop it with foreign aid, we will have a global disas-
ter on our hands, and it will be all our fault. If you’re looking for
logic here, don’t bother. But there is a method to this madness.
Remember, Sider’s fundamental ethic is plundering the rich, At all
costs, no matter how convoluted the argument, he wants to be
Robin Hood. The fact that foreign aid is wrong, or that it doesn’t
work, or that reducing the size of population will not decrease
hunger, will have no effect on his premise. His @ priori principle is
that of statism and egalitarianism (another example of his serious
confusion, since you can’t really have both). P. T. Bauer explains
how the thinking runs: “If the basis of the advocacy of aid is sim-
ply the need to reduce the allegedly wide and widening gap in in-
comes then such advocacy would not be affected even if it were
recognized that aid need not promote the material progress of the recipients
as long as it impoverished the donors.”®

4. Ibid., p. 153.

5. Ibid., p. 54 [p. 47].

6. P. T. Bauer, Dissent on Development (Cambridge, MA: Harvard University
Press, 1971, 1976), p. 119. Italics added.
