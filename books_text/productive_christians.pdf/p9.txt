FOREWORD
by Gary North

What you are about to read is one of the most devastating
polemical books I have ever read. Each edition gets better, mean-
ing tougher for Ronald J. Sider to-deal with. I will put it even
more boidly: the first edition was impossible for Sider to deal
with, given his views about God, man, law, and the authority of
the Bible. The second edition meant that Chilton was not going
away. The third edition means that the Sider phenomenon, as an
intellectual phenomenon, is dead.

Dr. Sider has recently treated us to a revised edition of Rich
Christians in an Age of Hunger, In revising his book, he violated one
of the fundamental principles of life: “When you're in a hole, stop
digging.” The first and second editions of Chilton’s Productive
Christians in an Age of Guilt-Manipulators exposed to anyone who
cared to consider it just how deep Dr. Sider had already dug him-
self in, With his revised edition, Dr. Sider has now gone at least
thirty feet deeper, leaving an even more massive pile of error all
around the rim of his ideological chasm. Chilton just stood at the
top and shoveled it back down on the head of our hapless pro-
fessor. I think it is safe to. say that there. will be no third edition of
Rich Christians, But I hope there will be.

When Ron Sider revised his book, and deliberately refused to
reply to Chilton’s book, or even give it a footnote, he paid Chilton
the highest of all academic compliments. He admitted implicitly
that he was theologically and intellectually incapable of respond-
ing to any of Chilton’s arguments, let alone the whole book. It
meant that he chose to pretend that Chilton’s book had not gone
through two editions, and was not being read by thousands of

ix
