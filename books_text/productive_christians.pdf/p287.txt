Da Vu-—A Romp Through Ronald Sider’s Second Edition 2.73

ingly, that crime does pay (as it was simultaneously perceived that
honest labor is not as rewarding, in its effects on either status or
pocketbook). As the prospective benefits of criminal . activity
steadily outweighed the prospective costs, more and more people
opted for the short cuts. The traditional connections between be-
havior and results were obscured.

In education, the same things happened. Sanctions against
low academic performance and violence were dropped; thus “a
student who did not want to learn was much freer not to learn in
1970 than in 1960, and freer to disrupt the learning process for
others. Facing no credible sanctions for not learning and possess~
ing no tangible incentives to learn, large numbers of students did
things they considered more fun and did not learn. What could
have been more natural?”5*

What we have witnessed in America over the past twenty
years is the systematic destruction of an entire class—the in-
escapable outcome of our social welfare policies. That is not to say
that these results have been intentional on the part of the plan-
ners, What is important is this: The results are just as certain as if
they had been intentional. As far as its efects on poor blacks are
concerned, civil rights and welfare policy in this country might as
well have been determined by the Ku Klux Klan.

Dr. Sider’s second edition seeks to make much of the argument
that the Bible often blames poverty on oppression (pp. 53f.). I
could not agree more. But what the professor has resolutely refused
to acknowledge is that the degradation, impoverishment, and out-
right enslavement of millions in this country has primarily been
caused by the oppressive policies of elite planners like himself.
Truly, “the compassion of the wicked is cruel’ (Proverbs 12:10); and.
that includes the sincere but misguided compassion of those who
seek to assist the poor by unbiblical (and therefore wicked) policies.
Compassion, if not informed and ruled by biblical standards of
justice and mercy, can become the cruelest form of oppression.59

58. Ibid., pp. 173f.
59. See Digby Anderson, ed., The Kindness that Kills: The Churches’ Simplistic
Response to Comptex Social Issues (London: SPCK, 1984).
