236 Productive Christians in an Age of Guilt-Manipulators

Rejoice greatly, O daughter of Zion!

Shout in triumph, O daugher of Jerusalem!
Behold your King is coming to you;

He is just and endowed with salvation,
Humble, and mounted on a donkey.

And I will cut off the chariot from Ephraim,
And the horse from Jerusalem;

And the bow of war will be cut off.

And He will speak peace to the nations;
And His dominion will be from sea to sea,
And from the River to the ends of the-earth. (Zechariah 9:9-10)

Constantly, the prophets told of Christ’s kingdom beginning
with His first coming. If this is really the case, we would expect it
to be the message of the Apostles as well. As a glance at your con-
cordance will reveal, Christ’s kingdom is a primary topic of the
Gospels. A study of these references alone should convince you
that He had no intention of “postponing” it at all, contrary to the
claims of Scofield and others. The authoritative interpretation of
Christ’s kingdom was given by Peter on the Day of Pentecost. He
reminded the Jews of their father David’s prophecy. “Because he
was a prophet, and knew that God had sworn to him with an oath
to seat one of his descendants on his throne, he looked ahead and
spoke of” — the Second Coming? No!—“the resurrection of Christ”
(Acts 2:30-31), Christ became the King at His resurrection, after
which he declared: “All authority has been given to Me in heaven
and in earth” (Matthew 28:18). Jesus is King now, not in some
future earthly reign. He is on “David's throne” now, for that
merely symbolized His heavenly throne. If Christ now has “‘all
authority in heaven and in earth,” what could possibly be added
to that authority in the future? Paul tells us that when God raised
His Son from the dead,

He. . . seated Him at His right hand in the heavenly places, far above
all rule and authority and power and dominion, and every name that is
named, not only in this age, but also in the one to come. And He put all
things under His feet . . . (Ephesians 1:20-22).
