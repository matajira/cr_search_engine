186 Productive Christians in an Age of Guilt-Manipulators

are as technically proficient as American barbers are, he may be
right; but that has little to do with the variations in price between
the two countries, As we noted in Chapter One, exchange takes
place because each party values the other’s good or service more
highly than he values his own, This means that value is not intrin-
sic, Value is imputed. Each of us forms consumer preferences on a
scale in his own mind, With changing conditions, these preferen-
ces also change. Men’s preferences cannot be measured in money
or anything else. They can only be compared. Whenever you
make an exchange, you do it in terms of your scale of values,
deciding if a certain good is worth what you must give up for it.
Meanwhile, the other person is’ doing the same; and if the ex-
change is completed, that means that each of you valued the
other’s good higher than you did your own. If the goods were truly
valued equally, there would be no exchange: neither one of you
would be anxious to buy or sell. You exchange because you would
rather have the other good than that which you trade for it. This
means that there can never be a “just price’”—but prices can be
just. Prices are just when there is no fraud or coercion in’ the
market— when God’s laws are obeyed. It follows that a price set by
the government must always be unjust. Instead of allowing free men to
buy and sell in terms of their own scales of value, the state applies
coercion— necessarily backed by potential violence—in order to
force people to comply with its arbitrary justice. Always, this is
done in favor of one class or pressure group against another, in
direct violation of Gad’s command:

You shall do no injustice in judgment; you shall not be partial to the
poor nor defer to the great, but you are to judge your neighbor fairly.
{Leviticus 19:15)

How does Sider get around this verse, seeing that it condemns
his class-warfare ambitions? He answers: “The God of the Bible is
on the side of the poor just because he is not biased.” That is
slick, It must be a gift.

10. Dbid., p. 84 [p. 76].
