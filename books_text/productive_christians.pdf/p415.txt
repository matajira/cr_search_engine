Bibliography 401
—_______.. Salvation and Godly Rule, Vallecito, CA: Ross
House Books, 1984.
——_--____._ Thy Kingdom Come: Studies in Daniel and Revelation,
Tyler, TX: Thoburn Press, [1970] 1978.
Schaeffer, Francis A. A Christian Manifesto. Westchester, IL: Cross-
way Books, revised edition, 1982.
. The Great Evangelical Disaster. Westchester, IL:
Crossway Books, 1984.
Schaeffer, Franky. Bad News for Madern Man: An Agenda for Chris-
tian Activism, Westchester, IL: Crossway Books, 1984.
Schlossberg, Herbert. Idols for Destruction: Christian Faith and Its
Confrontation with American Society (Nashville: Thomas Nelson
Publishers, 1983.
Thoburn, Robert L, The Christian and Politics. Tyler, TX: Thoburn
Press, 1984.
Van Til, Cornelius. The Defense of the Faith. Philadelphia, PA; Presby-
terian and Reformed Publishing Co., third revised edition, 1967.

Economics and Society

Alchian, Armen A. Economic Forces at Work. Indianapolis: Liberty
Press, 1977.

Anderson, Martin. Welfare: The Political Economy of Welfare Reform
in the United States. Stanford, CA: Hoover Institution Press,
1978.

Banfield, Edward C. The Democratic Muse: Visual Arts and the Public
Interest. New York: Basic Books, 1984.

—______. The Moral Basis of a Backward Society. New York:
The Free Press, 1958.

—_____. The Unheavenly City Revisited. New York: Little,
Brown & Go., 1974.

Bastiat, Frederic. Economic Sophisms. Irvington, NY: Foundation
for Economic Education, 1968.

. The Law. Irvington, NY: Foundation for Eco-
nomic Education, 1950,
