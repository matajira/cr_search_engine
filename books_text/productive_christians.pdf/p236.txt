222 Productive Christians th an Age of Guilt-Manipulators

authority (for example, that of policemen, social workers,
teachers, landlords, employers), and is apt to think that he has
been “railroaded” and to want to “get even.” He is a non-
participant: he belongs to ho voluntary organization, has no
political interests, and does not vote unless paid to do so.”8

Banfield concludes: “So long as the city contains a sizable lower
class, nothing basic can be done about its most serious problems.
Good jobs may be offered to all, but some will remain chronically
unemployed. Slums may be demolished, but if the housing that
replaces them is occupied by the lower class it will shortly be turned
into new slums.

“Welfare payments may be doubled or tripled and a negative
income tax instituted, but some persons will continue to live in
squalor and misery. New schools may be built, new curricula
devised, and the teacher-pupil ratio cut in half, but if the children
who attend these schools come from lower-class homes, the
schools will be turned into blackboard jungles, and those who
graduate or drop out from them will, in most cases, be function-
ally illiterate. The streets may be filled with armies of policemen,
but violent crime and civil disorder will decrease very little. If,
however, the lower class were to disappear —if, say, its members
were overnight to acquire the attitudes, motivations, and habits of
the working class—the most serious and intractable problems of
the city would disappear with it.”?

In a word: regeneration, The issue is not poverty or hunger, but
faith and ethics. The present-oriented slave cannot be helped by
mere capitalist moralizing about pulling himself up by his own
bootstraps—he does not want to. Nor will he be helped by
handouts—they will only reinforce his moral defects. This is not
to say that we shouldn’t give him “charity” when it is néeded. But
it is to say that, if we are genuinely charitable, we must give much
more than money and food, and that our charity mist not be
focused ori mere money and food, And, particularly, we must not

8, Edward C. Banfield, The Unheavenly City Revisited (Boston: Little, Brown
and Company, 1974), pp. 61f.
9, Ibid., pp. 234f.
