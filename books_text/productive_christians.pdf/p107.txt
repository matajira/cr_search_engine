The Third World 93

terms of biblical law, a culture that engages in long-term rebellion
against God’s law will sink to the level of abject poverty and
deprivation, The law promises that. Conversely, if a culture has
suffered long-term misery, we can make a judgment about its
history—which is not to say that we may automatically assume
anything about its present inhabitants. They may indeed be very
godly. If so—if a cultural transformation has taken place spiritu-
ally~they are already on their way toward godly dominion,
although the process may take a generation or more to become
materially evident. But if God is on His throne, His people will be
blessed. He controls the environmental conditions, and He can
cause the desert to blossom (Isaiah 35; 43:19-21), But He will not
do it without the Spirit being poured forth in regeneration and
sanctification; physical, material, economic blessings flow from
cultural obedience (Isaiah 32:15-16).18

As the gospel progresses throughout a society, food becomes
easier to obtain, and attention turns again to the original tasks of
godly dominion which were mandated in the Garden. No Protes-
tant culture has—yet—been plagued by famine (but we should
expect famine and more if our national apostasy remains un-
checked). Godly cultures have the “Puritan work ethic” deeply in-
grained into their natures, and this has notable effects in econom-
ics: rising productivity, rising real wage rates, and accelerating
dominion over every area of life.

But ungodly men, as we have seen, are slaves by nature, Their
sin drives them to lord it over others in ways that are forbidden
and economically unproductive, and they are driven to relinquish
their-proper responsibilities (which are productive), to seek pres-
ent benefits rather than to sacrifice in the present for future
rewards, and to be enslaved by others. The unbelieving culture
thus gravitates toward statism and socialism. We can see this in
the story of how the disobedient Israelites became enslaved to the
state (I Samuel 8). The king became, in effect, a substitute god,
and they desired that he should save them; they therefore forfeited

18. For extensive biblical material on this point, see my recent book Paradise Re-
stored: A Biblical Theology of Dominion (Tyler, TX: Reconstruction Press, 1985).
