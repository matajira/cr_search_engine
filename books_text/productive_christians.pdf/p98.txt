84 Productive Christians in.an Age of Guilt-Manipulators

God is on the side of the poor;.as a matter of principle, He con-
stantly, “over and over again”® overthrows the rich and exalts the
poor; and we must side with Him in the revolution, As Karl Marx
phrased it, the battle cry of the revolutionaries must be: “perma-
nent revolution!”’? Anarchist Leo Tolstoy agreed: “The only
revolution is the one that never stops.”’8

(Actually, this notion of permanent revolution brings up an in-
triguing point: the “see-saw philosophy of history” is apparently
required here. When God overthrows the rich, they become poor,
and the oppressed become rich. Since God always sides with the
poor, and regularly overthrows the rich, He must side with the
formerly wealthy against the nouveaux riches. In Sider’s social
theory, everyone is miserable: if you’re poor, the rich oppress
you; and if you’re rich, God overthrows you. Sort of like Cosmic
Hot-Potato—up, down, up, down, up, down; the last one with
the money goes to hell.)

But this is not an accurate statement of biblical social justice.
Siding with the poor is not automatic with God, nor should it be
with His people. As we have seen already, the law demands justice:

You shall not follow a multitude in doing evil, nor shall you testify ina
dispute so as to turn aside after a multitude in order to pervert justice;
nor shall you be partial to a poor man in his dispute. . . . You shall not
pervert the justice due to your needy brother in his dispute (Exodus
23:3,6).

You shail do no injustice in judgment; you shall not be partial to the
poor nor defer to the great, but you shall judge your neighbor fairly
(Leviticus 19:15),

We may not mechanically assume that the poor man’s cause is
right. “The first to plead his case seems just, until another comes

6. “Door Interview: Ron Sider.” The Wittenburg Door (Oct. /Nov. 1979, No.
51), p. 27, Italics added.

7. Karl Marx: Essential Writings, Frederic L. Bender, ed. (New York: Harper
and Row, 1972), p. 272.

8. James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith
(New York: Basic Books, 1980), p. 417.
