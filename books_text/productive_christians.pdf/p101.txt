5
THE THIRD WORLD

About 25 years ago the term “Third World” came into promi-
nence. It is a concept of primary importance for socialists, a fraud
which can be used for many purposes. Clarence B, Carson ob-
serves: “For a brief period it looked as if the Third World might
become a definite entity, but it did not. It has remained largely a
concept with whatever content one wished to ascribe to it . . .”4
Where the world had been “divided” into capitalist and com-
munist factions, the Third World concept is supposed to refer to
nonaligned, less-developed countries—although exactly which
countries are included depends eritirely on who is currently using
the term, and what ax he is grinding.

“Nonalignment” in Third World nations serves the purposes of
revolutionary socialism as well. Now the world is divided between
the industrialized, Western exploiters and the non-industrialized,
exploited nations of the Third World. Western businessmen who
invest in less-developed countries are, it is said, neo-colonial
powers, obscenely profiting from their economic control over the
poor nations. Of course, now that some of the Third World coun-
tries are making money, a new division is required, and a “Fourth
World” has emerged.? The culprits absconding with “the best of
both Worlds” are, naturally, the industrial concerns of the West.

There is a great political advantage brought about by making

1, Clarence B. Carson, The World in the Grip of an Idea (New Rochelle:
Aalington House, 1979), p. 439.
2. Sider, Rick Christians, pp. 33M. cf. pp. 178).

87
