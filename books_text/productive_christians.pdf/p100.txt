“,. . The right to nationalize foreign holdings.”
(Ronald Sider, Rich Christians in
an Age of Hunger, p. 145 [p. 134])

“You shall not steal.”
(Exodus 20:15)
