330 Productive Christians in an Age of Guilt-Manipulators

reason come alive in man, and pagans could therefore also have
faith. Through this faith, through reason come to life, man be-
came godlike and blessed. Heaven is, therefore, nothing of
another world and is tobe sought in this life. It is the mission of
believers to establish this Heaven, the kingdom of God, here on
earth. Just as there is no Heaven in the beyond, there is also no
hell and no damnation. Similarly, there is no devil but man’s evil
lusts and greed. Christ was a man, as we are, a prophet and a
teacher, and his supper is a plain meal of commemoration where-
in bread and wine are consumed without any mystic garnish,”25

Engels explained that “by the kingdom of God Minzer meant a
society without class differences, private property and a state au-
thority independent of, and foreign to, the members of society. All
the existing authorities, insofar as they refused to submit and join
the revolution, were to be overthrown, all work and all property
shared in common, and complete equality introduced.”26 And he
makes this highly significant observation: “Miinzer preached these
doctrines mostly cloaked in the same Christian phraseology, behind which the
new philosophy had to hide for some time.”?” By using superficially
biblical language, Miintzer was able to gain a following among
many who might have repudiated his damnable doctrine if it had
been presented in the clear light of day as a call to envy and mass
murder,

Mintzer created an army of citizens, which enforced his doc-
trine of equality upon the countryside by what Engels praised as
its “robust vandalism”®®: robbing, burning, and destroying the
property of the rich. “Let your swords be ever warm with blood!”
Miintzer exhorted the faithful.” In 1525 he was successful in rous-
ing up all of central Germany in the bloody, so-called “Peasant
Rebellion” (although, it must be carefully noted, he attracted

25. Friedrich Engels, The Peasant War in Germany (Moscow: Progress
Publishers, 1956; second revised ed., 1965), p. 55.

26, Ibid., p. 56.

27. Ibid., p. 55.

28. Ibid, p. 27.

29. Shafarevich, The Socialist Phenomenon, p. 57.
