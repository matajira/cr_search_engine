268 Productive Christians in an Age of Guilt-Manipulators

And that is how his whole book goes. The professor is indeed
clever. He covers himself well. He almost never states any specific
standard or goal. He never really provides a blueprint—“just a
general theme in terms of the 1848 Supplement.”*® The same sort of
ambiguity is evident in Cry Justice, *° Sider’s annotated anthology
of Bible quotations on poverty. I certainly have no quarrel with
the Scripture in the book (although the translations are occa-
sionally sloppy). But Sider’s notes and the “tough, weighty” study
questions at the back of the book are masterpieces of innuendo
and insinuation. Little is actually said—but by sneaking in
through the basement, Sider manages to say quite a lot. I will
repeat my statement from Chapter 15: “Sider states himself some-
what vaguely with respect to the specific political programs he
prefers, the means employed to enforce them, and the limits of
state power, He is vague about just how much personal wealth
constitutes immoral wealth. But he is clear enough: we need more
compulsory wealth redistribution. We have too much wealth.
Vague standards of righteousness, coupled with emotional
generalities, can produce a lot of guilt. That, of course, is the
whole point.”

Making War on the Poor

Where Dr, Sider has been explicit enough for us to know what
he is talking about, however—the urgent need for more social
welfare legislation and funding, the importance of the further pro-
liferation of rights and “empowerment,” and so on~the policy
prescriptions he favors have proven to be utterly disastrous in
practice, A number of books have dealt with this subject, from
various perspectives: Martin Anderson’s Wélfare,t! Roger

39. The Communist Manifesto, written by Karl Marx and Friedrich Engels, was
published in 1848, See Appendix 3, below.

40. Ronald J. Sider, ed., Cry Justice: The Bible Speaks on Hunger and Poverty
(Downers Grove, IL: InterVarsity Press, 1980).

41. Martin Anderson, Welfare: The Political Economy of Welfare Reform in the
United States (Stanford: The Hoover Institution, 1978).
