God's Law and the Poor 55

elders of the gate, since the gate was the place where the elders sat in
judgment (cf. Deuteronomy 22:15; 25:7). The local elders of the
covenant community supervised the administration of the third-
year tithe, dispensing it among the local Levites and the needy
aliens, orphans and widows residing in the town. In the New
Covenant there is no longer a central sanctuary; thus the third-year
tithe is to be the regular pattern of our tithing today. Normally, the tithe
should be paid to the elders of our local church (I Corinthians
16:2), and the elders are responsible for administering it in the
direction of Levitical activities and for charitable purposes. If,
however, a church is not fulfilling its mandate to proclaim God’s
word as law for society, the tithe should be withheld from it and
given instead to institutions which more fully conform to Levitical
standards. We must not rob God by tithing to apostates. Thus,
for example, a man brought his offerings to the prophet Elisha
during a time of national apostasy, when the prophets formed a
remnant church (II Kings 4:42-44)..As faithful Christians, we are
responsible for the godly disposition of our tithes.

The Jocal administration of charity is crucial. It ensures that
funds go to those who are truly needy, rather than to professional
paupers, The charitable aspects of the tithe did not mean simply a
handout to everyone who lined up. Charity is to be dispensed by
responsible leaders of the covenant community who are in daily
contact with the needs of the people. The general principle still
holds: those who won't work don’t eat. Those who attempt to live
by a welfare ethic are quickly exposed in a locally-administered
program, and will be unable to get away with “mooching.” Even
in charity, God’s law teaches responsibility. This is in stark con-
trast to the governmentally-financed “charity” promoted by
Ronald Sider. Murray Rothbard observes: “State poor relief is
clearly a subsidization of poverty, for men are now automatically en-
titled to money from the state because of their poverty. Hence, the
marginal disutility of income foregone from leisure diminishes,
and idleness and poverty tend to increase further, which in turn
increases the amount of subsidy that must be extracted from the
taxpayers. Thus, a system of legally subsidized poverty tends to
