80 Productive Christians in an Age of Guili-Manipulators

Exodus, the destruction of Israel and Judah, and the Incarnation),
God intervened to liberate the poor. Second, he says, God is always
at work in history, casting down the rich and exalting the poor.
Third, he states that God’s people are on the side of the poor.?

A basic objection to all this is that Sider has committed the
same fallacy of equivocation that we examined in the previous
chapter. Who are “the poor”? Who are “the rich”? Is God always
for the one and against the other? If we desire to be biblical, we
can no more make “‘the poor” an abstraction than we can make
“slaves” an abstraction, God’s law is not abstract, but specific.

The Bible declares that God is actually against certain poor
people. The sluggard, who is lazy and thoughtless about the
future, has no claim on God’s mercy (Proverbs 6:6-11; 13:4, 18;
19:15; 20:13; 21:25-26; 24:30-34; 28:19). God certainly is not
“on the side” of any lawbreakers who happen to be poor. Just as
the rich often are tempted to be proud, denying God’s goodness, so
the poor are tempted to covet the possessions of others and to take
God's name in vain (Proverbs 30:7-9). In fact, this is a prominent
theme in the biblical definition of God’s relationship to the poor
man: God promises, “When he cries out to Me, I will hear him,
for I am gracious (Exodus 22:27). But immediately, God offers
this warning: “You shall not curse God, nor curse a ruler of your
people” (Exodus 22:28). If we are unjust to the poor, and they cry
out to God, He will hear and avenge them, and provide for their
needs. But a poor man must not curse Gad, as if He has been un-
fair in His providential dealings with him; also, he must not revile
those in authority over him. These are special temptations to
which the poor can easily fall prey, and the poor are sternly cau-
tioned against succumbing to them. Whenever we feel oppressed,
we want to lash out at God for dealing us a bad hand. ‘The un-
godly poor will blame God for their misfortune, and they are
promised nothing but judgment. Any man who blasphemes God,
be he rich or poor, is to be put to death (Leviticus 24:13-16).
Moreover, the ungodly poor, with their slave mentality, are apt to

2. See, e.g., Sider, Rich Christians, pp. 59-85 [pp. 53-78].
