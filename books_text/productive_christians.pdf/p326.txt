312 Productive Christians in an Age of Guilt-Manipulators

come to specifics it is mainly state action that they recommend”;1®5
moreover, “they never ask the most important question: just how
does a society move from poverty to affluence?”166

Berger asks a pointed question of his own: “There is good rea-
son to believe that the strategy to which the bishops are commit-
ting the prestige of the Church may end by harming rather than
helping the poor. Human lives are at stake: by what right, then,
do these men appear before us, wrapped in the mantle of author-
ity of prophets and popes stretching back to ancient Israel, and
dare to tell us that one set of highly precarious policy choices rep-
resents the will of God for our time?”!67

Exactly, By what right? More fundamentally, By what stand-
ard?!6® Gary North writes: “Always be suspicious of someone
coming in the name of Christ who tells you that the Bible does not
provide blueprints. You can be reasonably certain that you are
about to be told that he has a ‘new, improved interpretation’ of the
topic under discussion which is in accord with the ‘ultimate con-
cern’ of the Bible, or the ‘overall sentiment’ of the Bible, or what-
ever the latest buzz-words are that people prefer to use when they
are trying to avoid a discussion of the explicit teaching of the Bible.”16?

It should be noted, of course, that the members of the self-
appointed “Lay Commission on Catholic Social Teaching and the
U.S. Economy” (which produced a pro-capitalist response to the
bishops) are no more interested in biblical blueprints than are
their fathers in the faith. Right at the beginning of their docu-
ment, they tell us that “Christian Scripture does not offer pro-
grammatic guidance for the concrete institutions of political econ-
omy,” and they go on approvingly to quote a scholar who declared
that the economist should not “theologize or moralize in his treat-
ment of his subject matter or, what is worse, try to derive an eco-

365. Ibid, p. 32.

166. Zbid., p. 34,

167, Ibid, p. 35.

168. See Rousas John Rushdoony, By What Standard? An Analysis of the Philoso-
phy of Cornelius Van Til (Tyler, TX: Thoburn Press, [1958] 1983).

169. Gary North, in Clouse, ed., Wealth and Poverty, p. 200.
