The Conguest of Poverty 235

‘Thou shalt break them with a rod of iron,
Thou shalt shatter them like earthenware. (Psalm 2:8-9)

Kings and rulers are advised by the Psalmist to submit to the
tule of Christ. If they do not, they will be destroyed (Psalm
2:10-12). The reign of the Messiah is not pictured by the Old
Testament writers as confined to Jerusalem; instead, it will be
universal, in which all nations will serve Him (Psalm 72). This
necessarily means the acceptance of His law as recorded in Scrip-
ture. The:notion that Christ’s kingdom has nothing to do with
politics and economics is altogether false. Isaiah announced that
“the government will rest on His shoulders . . . There will be no
end to the increase of His government or of peace” (Isaiah 9:6-7).

When will Christ’s kingdom begin? The prophet Daniel was
given the answer. Interpreting Nebuchadnezzar’s dream, Daniel
foretold the future of four great world empires, symbolically
represented by a statue. First there was the Babylonian empire; it
would be followed by the Medo-Persian, the Greek, and the
Roman empires. But during the last empire, a stone would strike
it, bringing it to destruction, and becoming a mountain which
would fill the earth, The stone represented the kingdom of Christ,
which would endure forever (Daniel 2:31-45). Because of the ob-
vious connection of the beginning of God’s kingdom with the
Roman Empire, those who wish to deny it have invented the
myth that we would see a “‘reoived Roman Empire” in the last
days. The Bible says nothing of this; but as someone has remarked:
Dispensationalists believe in the revival of the Roman Empire; we
believe in the revival of Christianity.

Daniel goes on to show Christ ascending in the clouds to His
Father and receiving everlasting dominion, in order that “all the
peoples, nations, and men of every language might serve Him”
{Daniel 7:13-14). This theme is picked up by Zechariah, who
connects Christ’s triumphal entry into Jerusalem, just before His
crucifixion, with His universal rule. Premillennialists arbitrarily
and quite high-handedly insert a gap of 2000 years between these
verses, but, again, without a word of biblical support:
