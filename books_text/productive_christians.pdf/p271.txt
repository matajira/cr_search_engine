Déji Vi—A Romp Through Ronald Sider’s Second Edition 257

The Plot Thickens

Some statements made in the first edition were so embarrass-
ing, of course, that they had to be cut out entirely. Terms such as
“fair price” and “fair profits” are nowhere to be found in the rewrit-
ten work, It is important to keep in mind that Sider never specifically
repudiates his former pronouncements; but at least he does us the favor
of not repeating them.

Sider has created knotty problems for his loyal followers, how-
ever. As far as I can tell, only once in the new book does he admit
that he has changed his mind, and that is on an issue of only
minimal debate.?3 Has the professor backed down from his other
positions? If so, why doesn’t he tell us? If not, why does he delete
his previous statements? What about all those people who be-
lieved (and tried to act on) the first edition?

One place where this is crucial is where Sider talks about the
Jubilee Year. In the first edition, as we have seen, Sider asked if
we should implement the Jubilee, and answered:

Actually, it might not be a bad idea to try the Jubilee itself at least
once, . . . In 1980 all Christians worldwide would pool all their
stocks, bonds, and income producing property and redistribute
them equally. (p. 93)

In the new edition, Sider asks the same question: Should people
today seek to apply the Jubilee Law? This time, he answers:

Certainly not. The specific provisions of the year of jubilee are not.
binding today. (pp. 84f.)

In line with this new position, Sider deletes his former statement
about pooling our possessions in fulfillment of the Jubilee
(although he doesn’t tell his readers he has changed anything).

23. This is the issue of whether personal evil does as much harm as structural
injustice. Sider asserts that he has changed his position on this matter, In at least
North America and Western Europe, he says, both problems are probably equal
in their damaging effects. (As far as I know, this has never been a matter of major
disagreement, but Sider seems to regard his new position as a notable concession
to his critics.) See Sider, Rich Christians, second ed., p. 122.
