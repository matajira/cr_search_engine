The Basis for Economic Growth 225

blessed with more dominion, while those who disobey will be
cursed. With increased responsibility, productivity will increase.
The culture will grow, in numbers and in wealth.

The issue is not poverty. The issue is not even capital supply.
“Abundant food is in the fallow ground of the poor, but it is swept
away by injustice’ (Proverbs 13:23). Resources are not infinite.
But they are vastly more than we can imagine. The earth was
made by'God, and He is capable of bringing forth tremendous
prosperity, Even the undeveloped land of the poor man can sup-
port a great deal of production. But it is presently swept away by
injustice —by ungodly structures and practices which God curses
by withholding abundance. And the only real issue is faithful obe-
dience to the law of God, Capital can grow. Productivity can in-
crease until the Last Judgment. But the basis for such economic
growth is biblical law.

The religious and economic history of England provides a good
illustration of this. Early in the eighteenth century, a high-society
lady once joked that Parliament was “preparing a bill to have ‘not’
taken out of the Commandments and inserted in the Creed.” 12 It was
not far from the truth, By all descriptions of the period, it was
characterized by rampant ungodiiness and almost complete dis-
regard for biblical standards in every area of life. J. C. Ryle
wrote:“Christianity seemed to lie as one dead. . . There was
darkness in high places and darkness in low places—darkness in
the court, the camp, the Parliament, and the bar—darkness in
country, and darkness in town--darkness among rich and
darkness among poor~a gross, thick, religious and moral
darkness—a darkness that might be felt.”5

The government and the courts were corrupt: open bribery was
a continual practice, and the poor were flagrantly oppressed ~
which is not to say that the poor were any better. Crime was
abundant, and the attempt of the authorities to suppress it (by

12. Arnold Dallimore, George Whitefield: The Life and Times of the Great Evangelist
Of the 18th-Century Revival (Edinburgh: The Banner of Truth Trust, 1970), I: 28.

13. J. C. Ryle, Christian Leaders of the 18th Century (Edinburgh: The Banner of
Truth Trust, 1978), p. 14.
