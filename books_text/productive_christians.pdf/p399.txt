Glossary 385

alchemy, in which the government magicians-try to turn base ma-
terials into gold. The government, knowing that its magic money
is really worthless, backs it with “legal tender” laws, forcing people
to accept it. Fiat money is theft. See Inflation.

Fractional reserve banking is the commercial banks’ practice of
lending out more money than is actually held in their vaults. This
is the most powerful tool of inflation. The present reserve require-
ment (set by the Federal Reserve) is eight percent. This means that,
theoretically, banks could lend out up to $12.5 million for every $1
million they really possess. It is a sophisticated, legal means of
theft. It is also quite profitable for banks. Why do civil govern-
ments allow it? Because they enjoy access to less expensive loans.
See Credit expansion.

Free enterprise is the freedom to buy and sell at will, without
government intervention. The civil government's biblical function
is to protect the market from private coercion or fraud. State in-
tervention is legitimate only where the “enterprise” is forbidden by
Scripture. Government control of trade is a practice of Antichrist
{Revelation 13:17).

Free trade is free enterprise on an international basis—the ex-
change of goods and services among citizens of many nations, un-
hampered by national commodity agreements, tarriffs, or any
other barrier to trade, Assuming nations are not at war, trade
should be unhampered, If any trade is lawful, then it should be
completely free trade.

Future orientation is an attitude of optimism about the possibil-
ity and desirability of progress. It is a necessary requirement. for
expanding the real wealth of society. Only a Christian culture,
holding the firm promise of God’s blessings for the future, can sus-
tain a long-term future orientation. Such a future orientation pro-
duces lower interest rates. God has commanded us to develop the
earth and to disciple the nations, and He has guaranteed the suc-
cess of our mission. See Dominion.
