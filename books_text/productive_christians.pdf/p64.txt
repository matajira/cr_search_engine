50 Productive Christians in an Age of Guilt-Manipulators

ency to abandon our responsibilities if some agency is there to
assume them for us, The basic social institution is the family.
Family members are best equipped to deal with needy relatives,
in terms of personal care and attention. They are more aware of
the real wants of the person, and, because they are close to the sit-
uation, are most able to detect abuses of charity. God wants to
build responsible relationships within families, and the church’s
responsibility in caring for needy members grows out of the fact
that it is our larger family, “the household of God.” But any ap-
peal to the larger family must be only as a last resort.

Even then, charity is restricted. A widow is to be placed on the list
for aid only if she herself is engaged in charitable service, “having
a reputation for good works; and if she has brought up children, if
she has shown hospitality to strangers, if she has washed the
saints’ feet, if she has assisted those in distress, and if she has de-
voted herself to every good work” (I Timothy 5:10). Biblical char-
ity, as we shall see again and again, never subsidizes irresponsibility.
A crucial principle of biblical law is that “if anyone will not work,
neither let him eat” (II Thessalonians 3:10). The further away we
get from familial charity, the more likely it is that this principle
will be abused. State welfare fraud is so universal as to be prac-
tically axiomatic; but it is virtually impossible to engage in long-
term deception of one’s family.

Biblical law is geared toward responsible action on the part of
individuals and families. National greatness does not come about
through legislation or governmental coercion, Ronald Sider’s call
for a “guaranteed national income”? is geared only toward na-
tional irresponsibility. It is based on the ethic that I have a right to
as much money as I can vote out of my neighbor’s pocket. In
short, “thou shalt not steal, except by majority vote.”. Moreover,
it will only enslave us to the state, as Auberon Herbert poirited
out a century ago: “So long as great government departments . .
supply our wants, so long shall we remain in our present condi-
tion, the difficulties of life unconquered, and ourselves unfitted to
conquer them. No amount of state education will make a really
intelligent nation; no amount of Poor Laws will place a nation

3. Sider, Rich Christians, p. 212 [ef. p. 202].
