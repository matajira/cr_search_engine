15
PREPARING THE CHURCH FOR SLAVERY

The policies of state intervention do not attain their professed
goals.. They cannot do so, for they are in violation of God’s com-
mands, and this is His world. Foreign aid does not help the poor;
price controls create an imbalanced, chaotic market; minimum
wage laws result in unemployment; profit restrictions increase
consumer costs; enforced economic equality means a radical
political inequality; in short, all attempts by the state to abolish
poverty serve only to intensify it in one way or another. We have
also seen, however, that statism has another, often unprofessed,
goal: domination over men. This urge for power is the only thing
that statism can satisfy —and that lasts only so long as God with-
holds His judgment. The demand for statism is a demand for
control—nothing more or less. The real goal of increasing govern-
ment intervention is totalitarianism.

What part does Ronald Sider play in the establishment of a
totalitarian state? While he does call repeatedly for statism, that is
not the main thrust of the books published under his name. There
are many in our day who would try to convince us that we need a
bigger Big Brother. But Sider’s function in the Revolution is
much more specialized. Whether he is a calculating propagandist
or merely an ignorant tool in the hands of others (the evidence
points strongly toward the former), he nevertheless is serving the
cause of totalitarianism in a way in which, for example, Marx and
Keynes were unable to do so. This is because of two factors: his
Sphere of activity and his message.

Ronald Sider’s semi-official standing as a theologian gives him

205
