10 Productive Christians in an Age of Guilt-Manipulators

The other aspects of my argument have to do with economics.
Christians have generally left economics to the secularists, and that is
why so many have fallen for Sider’s socialistic fallacies. I have tried to
explain the principles of biblical economics clearly and simply, with-
out a lot of verbiage. But, just in case, I have included a glossary and
an extensive bibliography of reliable books, most of which are rela-
tively easy to read. Of course, all books must be tested in the light of
Scripture—which means you must acquire a familiarity with the
Bible, particularly with regard to what it says on economics. The
Book of Proverbs is important for this, as well as the books of Moses.
Economics is not boring (in fact, these days it’s a pretty scary subject).
Most of what you do all day is an economic activity, and you need to
know what Gad’s word says about that big chunk of your life.

A few words about how to read this book would be in order,
First, some may be offended at certain rather playful observations
I make about Sider’s position, On this point I stand firmly with
the prophet Elijah: that which is ridiculous deserves ridicule.
Besides, it helps you keep reading. But despite the occasional
humor, I have taken Sider seriously as well; he is deadly earnest,
and his policies are just plain deadly.

Second, the first two chapters form the basic argument for the
rest of the book. They are longer chapters, but the reader is ad-
vised to start at the beginning. Most of what I say elsewhere will
assume that you have absorbed the first two chapters.

The Sider Thesis

It may be helpful at this point to introduce some of Sider’s ideas
for those who are unfamiliar with them. A major theme is that of
the “simple life,” as I noted above. We shall see later that this
official position of Sider’s is actually a hoax (I won’t tell you which
chapter that’s in; read the book!). But the fundamental principle
behind this is the view that sin is built into the structure of reality;
that sin is in thengs, and that things, in fact, actually cause us to
sin. Consider the following lines:

. +. possessions are highly dangerous . . . Jesus was so un-American
that he considered riches dangerous . . . Riches are dangerous because
