Preparing the Church for Slavery 209

will go to hell.? He encourages groups to examine and evaluate
each other’s expenditures in terms of his envious standards. The
practical results of such tactics can be observed in any issue of
theOtherSide, as writers regularly flog themselves for their failure to
be “totally committed” to the ideal of economic and population
stagnation, confessing their occasional cravings for steaks and
other wicked goodies.

As guilt produces impotence, it also leads people to call for
more and more controls from the state. The passive population is
not only malleable, yielding, submissive; it positively welcomes
State intervention. Sider’s cleverness in this is diabolical. First, he
tells us that a billion people are starving because of our eating
habits. Next, he urges his guilt-absorbed readers to reduce their
quotas of beef. But this is not enough, because “unless one also
changes public policy, the primary effect of reducing one’s meat
consumption may simply be to enable the Russians to buy more
grain at a cheaper price next year or to persuade farmers to plant
less wheat’’*—and just think how much more guilty we would be
then! And so the pliant masses who read Sider—having been
primed by a dozen years of statist indoctrination in the public
schools—are manipulated by this alternation of envy, guilt, and
hopelessness into asking for stricter controls, broader legislation,
increasing intrusions, more bondage. The guilty, unable to solve
life’s problems, will be saved by the state.

And Sider’s goal of establishing an oppressive regime is work-
ing. Christians have become obsessed with their own imagined
wrongdoings (while entirely ignoring their real violations of God’s
law), agonizing over their criminal inability to feed the hungry,
glutted with remorse and shame because of their secret love for ice
cream, suffering nagging worries about just how much their vaca-
tions contribute to world poverty, wondering how many children
die on their account—longing for the day when the government
will be empowered to decide all these questions, taking the grow-

3, Sider, Rick Christians, p. 172 [p. 164]. Obviously, this means he is a legalist
in the first sense of our discussion in Chapter One (see p. 22, above).
4, Ibid., p. 205 [p. 192].
