Dé&ja Vu A Romp Through Ronald Sider’s Second Edition 255

Associated Press report, the USDA study found that “low-income
families consume more meat per capita than high-income ones,
and that blacks consume more than non-blacks.”?° Oops. {I can
see the headline now: “White Theology Prof Accuses Poor Blacks
of Overconsumption.”) Faced with embarrassing data like that,
Dr, Sider had every reason to write in a slightly lighter shade of
purple.

There are some revisions that must have been especially galling
to make. In the original edition, Professor Sider was practically
foaming at the mouth over our iraminent doom at the hands of the
nefarious Population Explosion. Remember those scary project-
ions?

The population explosion is another fundamental problem... . .
By the year 2000 the world’s population wil! have climbed to about
seven billion persons. (p. 18)

That was in 1977, But the 1984 Sider, although still perspiring
heavily, said this:

The population explosion is another fundamental problem. . . .
By about the year 2000 the world’s population will have climbed to
approximately six billion people. (pp. 24f.)

Let’s see. That’s a loss of one billion people in just seven years, At
this astonishing rate, by the time the professor. publishes the
Jubilee Edition of his book, there won't be anybody left to buy it.
Things are looking up, eh, Ron?

As a matter of fact, to take the word of Dr. Sider himself, things
are improving. His first edition reported that the percentage of dis-
posable income spent on food in the U.S. was 17 percent, while in
India it came to 67 percent (p. 44). Seven years have passed, and
Sider now gives us heartening news: “In the United States it is a
mere 12.7 per cent. In India it is 55.5 per cent. . .'.”(p. 35). The cost
af food is dropping, for both countries, You would think that Sider would

20. Associated Press story, Tylr Moming Telegraph, 18 December 1982.
