136 Productive Christians in an Age of Guilt-Manipulators

Advertising broadens the choices available to consumers, and
enables us to “shop” ahead of time. Brand names make shopping
more efficient, by enabling us to purchase goods of easily recog-
nizable quality. When I buy jeans, I don’t have to take the time to
sort through a jumble of different types, analyzing each pair for
durability. I just head for the Levi's. More than this, advertising
enables us to discriminate differences we hadn’t been aware of,
The nationally-advertised “Pepsi Challenge” made consumers
aware that Pepsi really does taste better to some consumers than its
competitor. A product that fails to live up to its claims cannot suc-
ceed, We are responsible for testing everything (I Thessalonians
5:21). Christians aren’t supposed to be gullible.

What then is the problem with advertising? Sider finally stum-
bles onto the point:

Given our inherent bent for idolatry, advertising is so demonically
powerful and convincing that most people persist in their fruitless effort
to quench their thirst for meaning and fulfillment with an ever-rising
river of possessions.>

He’s muddled it, but he’s reached the basic issue: idolatry. Peo-
ple who are enslaved to the present, thinking only of immediate
gratification, are seduced by advertising into buying the latest
doodads and baubles. Their ethic is not one of saving, invest-
ment, and generous giving, but of consumption. And the central
fact here is net the advertising. It is the slave mentality of the people,
a condition that can be corrected only by regeneration and the
deep cultural penetration of Christian values.

For example, let’s consider the most blatant instance of false,
unscrupulous advertising in all history: the temptation of Adam
and Eve. God had placed the forbidden tree right in the middle of
the Garden—not to tempt His creatures, but to give them
strength as they daily grew in the ability to obey Him. But when
the serpent handed them a line, they swallowed it, Why? Was the
temptation too strong for them? To say that would be to charge
God with deceiving them. No temptation is too strong (I Corin-

5. Bid., p. 49 [ef. p. 41].
