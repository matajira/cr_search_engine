198 Productive Christians in an Age of Guilt-Manipulators

Israel is quite applicable to us today. God judged them for their
oppressive covetousness, and He will judge us too.

The fractional reserve banking system can work against any
group, depending on which group succeeds in gaining access to
the newly created money first. For example, if the Federal gov-
ernment runs a massive deficit—if!!! (a little humor)—and the
Federal Reserve Board steps in and buys a portion of the govern-
ment’s bonds by creating fiat money, the government will spend
the newly created money in subsidizing its patrons. If this should
be welfare recipients or Social Security pensioners, then these
groups can benefit temporarily (at least until inflation destroys the
value of the currency, not to mention the social order). Those
hard-pressed middle-class people who are on tight budgets may
not choose (or be able) to indebt themselves. Thus, they see their
savings go down the inflationary drain, while the poor, who have
no savings, maintain their living standards by means of their
teady access to “indexed” welfare payments. They get the fiat
money first, spend it before it depreciates, and leave the middle
class businessmen and families holding the bag—the “bag” being
a savings account, or pension program, or annuity, or cash-value
life insurance savings full of worthless paper money.

Under fractional reserve banking, which is universal today, it
pays to be the favored group which gets access to the fiat money
first, before its purchasing power deteriorates. We can expect
class war eventually, such as Germany experienced in the years of
mass inflation, 1921-23, Everyone wants to be the favored person
in the race against price inflation, which is in turn caused by
monetary inflation. Thus, by looking at the short run, and voting
for a debt-based monetary system, today’s middle-class voter is
sealing the doom of the social order which has permitted the eco-
nomic benefits to flow to the middle class—a social order based on
voluntary exchange, contract, social peace, and honest money.
Ignorance of economics, when coupled with envy, and motivated
by a false sense of guilt, can produce a devastating social crisis.

Amos also attacked these people for their empty religion (4:4-5;
5:4-7). While it is commonly held that religion is man’s attempt to
