Déja Vu-—A Romp Through Ronald Sider’s Second Edition 283

places like” Russia itself, and “in places like” wherever the Soviets
and their protégés set foot, but I don’t want to be picky. I'm just
glad the professor has noticed that the Soviets are, in some places
anyway, oppressive,’?

But then J wonder: Why is Dr. Sider actively pursuing policies
which will, according to his own admission, bring the United
States of America under ‘the totalitarian domination of the
Soviets?

A little background will put this point in perspective. Productive
Christians was originally published in 1981. In Chapter 15 (“Prepar-
ing the Church for Slavery”), I declared my opinion that Professor
Sider’s doctrines of guilt-manipulation are preparing the way for
the establishment of a totalitarian state.”® Of course, making
prophecies like that can be a nerve-wracking experience. Once
the cat was out of the bag, there. was nothing I could do but sit on.
pins and needles waiting for Sider to get moving on the project.
Til say one-thing for the guy: give him an idea and he runs with it.
He raced into print the following year with Nuclear Holocaust and
Christian Hope: A Book for Christian Peacemakers (published by Inter-
Varsity Press, naturally), in which he and co-author Richard
‘Taylor give their plan for achieving world peace.

The strategy is known, in military terms, as Defeat.’? (Strangely,
Sider does not use this precise terminology; perhaps he considered.
it too technical.) It’s pretty simple, really. First, you disarm — totally
disarm — and render the nation utterly defenseless. Then comes
the fun part: “We. believe that such an action could very likely

77. For a horrifying -account of current Soviet oppression see Avraham
Shifrin, The First Guidebook to Prisons and Concentration Camps of the Soviet Union
(New York: Bantam Books, 1982).

78. “A civilization that feels guilty for everything it is and does and thinks wil!
lack the energy and conviction to defend itself when its existence is threatened.”
Jean-Frangois Revel, How Democracies Perish (Garden City, NY: Doubleday &
Go., 1984), p. 10.

79. For documentation of Soviet control and funding of the peace and nuclear
freeze movements in the West, see John Barron, Tike KGB Today: The Hidden
Hand (New York: Reader's Digest Press, 1983), pp. 249-293; cf. Vladimir Bukov-
sky, “The Peace Movement and the Soviet Union,” Commentary (May 1982), pp.
25-41; and Jean-Francois Revel, How Democracies Perish, pp. 144-59.
