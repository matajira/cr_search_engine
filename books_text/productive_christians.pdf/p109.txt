The Third World 95

the dominion of Christians—not by military aggression, but by
godly labor, saving, investment, and orientation toward the
future. For a time, ungodly men may have possessions; but they
are disobedient, and become dispossessed:

Though he piles up silver like dust,

And prepares garments as plentiful as the clay;

He may prepare it, but the just shall wear it,

And the innocent will divide the silver. (Job 27:16-17)

This is where history is going. The future belongs to the people
of God, who obey His laws. “The wealth of the sinner is stored up
for the righteous” (Proverbs 13:22), and “‘to the sinner He has
given the task of gathering and collecting so that He may give to
one who is good in God’s sight” (Ecclesiastes 2:26). This is what
God did with Israel. They inherited already settled lands, while
God smashed the heathen, having allowed them to build up capital
while incurring increasing judgment. because of their sins (Gen-
esis 15:13-16; Joshua 11:19-20). The seventeenth-century Puri-
tan Thomas Watson understood this well: “The meek Christian is
said to inherit the earth, because he inherits the blessing of the
earth, The wicked man has the earth, but not as a fruit of God’s
favour. He has it as a dog has poisoned bread. It does him more
hurt than good. A wicked man lives in the earth as one that lives
in an infectious air. He is infected by his mercies. The fat of the
earth will but make him fry and blaze the more in hell,”?*

The most important fact about poor pagans is not that they
ate poor, but that they are pagans. They are, as Romans 1 reminds
us, apostates. Where did pagans come from? They came from the
same place we all came from: Noah’s Ark. The ancestors of
today’s pagans knew God (Romans 1:21). They knew God. They
were God's people, receiving the blessings of the covenant from
His gracious hand. But they deliberately exchanged the worship
of the true God for the adoration of “corruptible man and.of birds
and four-footed animals and crawling creatures... . They ex-

21, Thomas Watson, The Beatitudes (Edinburgh: The Banner of Truth Trust,
1971), p. 117,
