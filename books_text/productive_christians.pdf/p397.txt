Glossary 383

Egalitarianism is the ungodly desire, provoked by envy, to
reduce all men to an arbitrarily conceived “equality,” at least in
those matters that the state can supposedly control. Egalitarians
are statists: they want the government to enforce equality upon
society. This results in an inequality before law, since the relatively
wealthy are legally discriminated against in favor of the relatively
poor, This kind of inequality is condemned by biblical law. It
violates the principle of the étke: fixed proportion taxes.

Empiricism is the unbiblical notion that experience is the only
source of knowledge. John Locke and David Hume were notable
exponents of this view. The Bible tells us, on the other hand, that
God's word is the source of knowledge. Our experience can test
truth only insofar as our experience is subject to God’s word,
Christians begin their search for knowledge with the assumption
that the Bible is true. See Presuppositions.

Entrepreneurs are those who act in the present to produce a
more desirable situation in the future. They act because they
believe that doing nothing new, would not be as beneficial. In this
sense, everyone is an entrepreneur, engaging in speculation
about, and action toward, the unknown future. In business ter-
minology, an entrepreneur is one who directs the factors of pro-
duction in order to achieve a desired result in the most efficient
and profitable way.’ But we should remember that the “profes-
sional” entrepreneur's function is not qualitatively different from
that of anyone who allocates his resources in anticipation of the
future. Entrepreneurs are specialists in risk-taking and economic
forecasting.

Envy is the belief that one person’s wealth is the cause of the
poverty of others. The envious man blames others for his own
want. His primary desire is not so much to obtain their property
as to see them deprived of it. This is one reason why socialists can-
not be convinced of the undesirability of socialism’s inefficiencies,
even when they admit that their policies will not result in the
benefit of themselves or the poor; for the main goal of socialism is
