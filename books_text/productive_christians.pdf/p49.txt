Biblical Law and Christian Economics 35

absolute private ownership.”? In terms of this, Sider calls for a na-
tional (state) food policy,® (state to state) foreign aid,® a guaran-
teed national income,!° international taxation,"! “land reform”
{i.e., expropriation of lands from the rich),!? bureaucratically
determined “just prices,”!3 national health care,!* population
control,!5 and the right of developing nations to nationalize
foreign holdings'®—all of which involve theft of one sort or
another. In. Sider’s Robin Hood Theology, loving my poor
neighbor means robbing my rich neighbor.

This is not to suggest that the rich have no responsibility to help
the poor. But it does-mean that the poor have a responsibility not
to steal from the rich. “You shall do no injustice in judgment, you
Shall not be partial to the poor nor defer to the great, but you are to judge
your neighbor fairly” (Leviticus 19:15)—which does not mean
Karl Marx’s definition of a “fair” distribution of wealth, but
rather according to the standard of God’s law. Any judgment of a
man on the basis of his property (or his lack of it) is theft. In at-
tempting to justify coveteousness and theft in the name of Gad’s
ownership, Ronald Sider has committed blasphemy. “Woe to
those who call evil good, and good evil!” (Isaiah 5:20).

Work and Dominion

The biblical method of attaining dominion is through diligent
labor. When Adam rebelled, he chose instead to have dominion
by playing god, rejecting God's leadership over him. He wanted

7. Ibid., p. 209.

8. Ibid., p. 214 [pp. 2108].

9. Ibid., p. 145, 207, 2188 [pp. 134, 196f., 2138].
10. Zid., p. 212 [cf. p. 202].

11. Ibid., p. 220 [p. 218].

12. Hbid., pp. 160, 218 [pp. 150, 215].

13. Dbid., pp. 165, 21if.

44, Did., pp. 212, 218.

15. Zbid., pp. 214, 218 [ef. pp. 24-26, 214].

16, Jid., p. 145 [p. 134].
