250 Productive Christians in an Age of Guilt-Manipulators

3. Constance Cumbey?
4, John Jefferson Davis*
5. Brian Griffiths’
6. Ronald Nash¢
7, Gary North?

8. John Robbins*

. Franky Schaeffer?

10. Herbert Schlossberg!®

Tt may have been just an oversight, but somehow or other the
professor neglected to answer any of these critics. True, he did
mention two of them in his endnotes: Gary North and P, T,
Bauer, He cites North as an “example” of an “Enlightenment”
economist reflecting a “modern, secularized outlook,” a state-
ment which never fails to elicit peals of laughter from those who
have actually read North's work. The only explanation I can think
of for such an absurd remark (outside of pure, mindless slander) is

o

3. The Hidden Dangers of the Rainbow: The New Age Movement and Our Coming Age
of Bosbarism (Shreveport, LA: Huntington House, 1983), pp. 1564.

4. Your Wealth in God's World: Does the Bible Support the Free. Market?
@hillipsburg, NJ: Presbyterian and Reformed Publishing Co., 1984), pp. 388.

5. Morality and the Market Place: Christian Alternatives to Capitalism and Socialism
(London: Hodder and Stoughton, 1982), pp. 125-55.

6. Social Justice and the Christian Church (Milford, MI: Mott Media, 1983), pp.
161-68.

7. “Free Market Capitalism,” in Robert G. Clouse, ed., Wealth and Poverty:
Four Ghristian Views of Economics (Downers Grove, IL: InterVarsity Press, 1984),
pp. 44ff.; also “Phe North-Sider Debate” (April 1981), two cassettes (Dominion
‘Tapes, P.O. Box 7999, Tyler, TX 757it), $10.

8. “Ronald Sider Contra Deum,” The Trinity. Review (March/April 1981);
reprinted in Biblical Economics Today (April/May 1982).

9. Bad News for Modern Man: An Agenda for Christian Activism (Westchester, IL:
Crossway Books, 1984), pp. 60ff.; cf. the criticism of the Evangelicals far Social
Action by his father, the late Francis A. Schaeffer, in the last book he wrote: The
Great Evangelical Disaster (Westchester, IL: Crossway Books, 1984) pp. i1l-15. See
also Franky’s satirical work (co-authored with Harold Fickett), A Madest Proposal
for Peace, Prosperity, and Happiness (Nashville: Thomas Nelson Publishers, 1984),
dealing with population control; Sider is specifically named as a target of the
book in the “postscript,” pp. 132ff. Their conclusion about Sider is similar to
mine: “Either he did not know what he was saying, or he was espousing views
that are well-nigh totalitarian” (p. 134).

10. Idols for Destruction: Christian Faith and Its Confrontation with American Society
(Nashville: Thomas Nelson Publishers, 1983), pp. 243f.
11, Sider, Rick Christians, second ed., pp. 102, 242.
