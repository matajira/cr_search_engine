30 Productive Christians in an Age of Guilt-Manipulators

ceremonial “days and months and seasons and years” (4:10), and
for being circumcised in the attempt “to be justified by law”
(5:2-4). It is most significant that when the Apostles speak of our
freedom from certain Old Testament regulations, they cite only cere-
monial laws—such as sacrifices, the priesthood, circumcision and
feasts—which pictorially represented the mediatorial work of
Christ unti! He came. For example, no New Testament text con-
demns the practice of restitution (and, in fact, Jesus pronounced
Zaccheus to be saved after he had demonstrated his willingness to
obey this detail of the law); no New Testament text can be used to
support unlawful practices of homosexuality, usury, or debase-
ment of currency. These are all aspects of God’s abiding moral
law, and not one word of Scripture alters their force.

Both Testaments distinguish between laws that were ceremon-
ial (and thus merely temporary) and those laws which were
moral. The moral law is the abiding definition of sin and right-
eousness (Romans 3:20; 7:7; 1 John 3:4), The ceremonial regula-
tions, on the other hand, symbolically represented the means of
restoration to God's favor through the Mediator who was to come
(Hebrews 7:10).The moral law answers the question, “How
should I live?”; the ceremonial law symbolically answers the ques-
tion, “How can I be restored to God’s favor after breaking His
moral law?” Thus the Old Testament writers were aware of the
crucial difference between obedience on the one hand (observance
of God's requirement of justice, mercy and faithfulness in every
area of life), and sacrifice on the other hand (observance of the
ceremonies which symbolized restoration). This distinction was
much more clearly revealed in the New Testament, of course,
since it was written after the symbols had met their fulfillment in
Christ (see e.g., Romans 2:28-29; Philippians 3:2-3; Colossians
2:11-14). But the distinction was also understood in the Old
Testament (Isaiah 1:11-23; Hosea 6:6; Amos 5:21-26; Micah
6:6-8). The ceremonial law was never intended to be a permanent
feature of the duty of believers. But the moral law has lasting
validity. God has not surrendered His authority in any area of
life. In the family, education, government, economies, science,
