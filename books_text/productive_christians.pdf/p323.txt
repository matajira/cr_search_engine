Déja Vu-A Romp Through Ronald Sider’s Second Edition 309

spirit of those benumbing articles which so delight subscribers to
theological journals. But we are talking to murdérers. Liberation theo-
Jogians may look cute and harmless, with their preoccupied looks
and professorial elbow patches, their footnotes and qualifications;
but they are advocating a reign of terror. It is not as if we are not
sure of their meaning and intentions. The problem is not knowl-
edge. The problem is the common unwillingness to perceive, to
discern, to judge righteous judgment.

The Bottom Line: Whose Blueprints?

I began the first chapter of this book with a brief consideration
of Sider’s statements on the Bible’s authority. That issue, ulti-
mately, is the point of contention between us. Let us note again
Sider’s commendable remarks on this question:

According to biblical faith, Yahweh is Lord of all things. He is the sover-
eign Lord of history. Economics is not a neutral, secular sphere inde-
pendent of his lordship. Economic activity, like every other area of life,
should be subject to his will and revelation.”

Following biblical principles on justice in society is the only way to
lasting peace and social harmony for all societies.25#

Notice what the essence of theological liberalism is~it is allowing our
thinking and living to be shaped by the surrounding society’s views and
values rather than by biblical revelation.1°°

Scripture, as always, is the norm.16¢

Unfortunately, these excellent remarks are accompanied by
the assertion that, when we ask specific questions about God's will
for the economic sphere, “the Bible does not directly answer these
questions. We do not find a comprehensive blueprint for a new economic
order in Scripture,”161

157. Sider, Rick Christians, p. 115 [pp. 1021].
158; Zbid., p. 206 [p. 193].

159. Zid, [p. 77].

160. Zid., p. 210 [p. 194].

161. Zbid., p. 205 [p. 193]. Iealics added.
