382 Productive Christians in an Age of Guili-Manipulators

Division of labor is an indispensable requirement for exchange,
in which each member of the economy works in the area in which
he can be most productive. God has distributed various gifts to in-
dividuals, and the demands of the market draw men into those ac-
tivities that they expect will be the most productive for the society
as a whole, and therefore most profitable personally, Each man
then trades his product or service for those of others. As men spe-
cialize in what they do best, the overall productivity of the society
increases.

Dominion is man’s responsibility to subdue the earth for God's
glory. For Adam, this involved three general tasks: production of
goods, scientific investigation, and aesthetic endeavors. By the
division of labor, the work of dominion can be greatly furthered as
each person learns to specialize in a particular field within these
areas. By rebelling against God’s Jaw, man abandoned this ‘re-
sponsibility; and the more externally rebellious a culture is, the
Jess dominion it will have. This is the biblical explanation for so-
called “primitive” cultures: they are not chronologically primitive
but decadent and degenerate. Their forbears turned away from
God’s law as it was known to Noah and his sons; and the more
they apostatized, the less able they were to cope with their sur-
roundings (see Curse). Dominion is restored to the people of God
as they are re-created in God's image, and the subduing of earth
again becomes possible. See Blessings.

Economics is the study of people’s actions as they purposefully
use means to attain desired ends. The starting-point of Christian
economics is not what “works,”—pragmatism—but the com-
mands and prohibitions of God’s law. All human action stands
under the judgment of God. Lawful activity is blessed by God, in
this life and the next; unlawful activity is cursed by God, in this
life andthe next. Unbiblical economics does not work because it is
morally wrong and therefore is cursed. Biblical economics
acknowledges scarcity (Genesis 3:15-17), the dominion covenant
(Genesis 1:28), and personal responsibility (Philippians 2:12).
