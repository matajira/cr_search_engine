Religious Repression in Ethiopia 371

secret police and imprisoned for three years. His grace was ac-
cused of subversion for having made attempts to speak out against
the regime’s Red Terror campaign directed at its internal opposi-
tions.

5. The Communist regime has bombed several churches and
religious shrines. It has converted many churches into military
barracks. It removed incense and candles from the market places
and shops in order to deprive the Church and believers of religi-
ous services. Priests and monks were left to languish in prison for
speaking out against the oppressive rule of the regime.

The Communist regime has arbitrarily dismissed from their
post the following 14 Archbishops: Their Holinesses

. Archbishop Yohanes of the province of Tigre
. Archbishop Mikael of Eritrea

. Archbishop Gebriale of Wello

. Archbishop Markos of Jogam

. Archbishop Joseph of Kaffa

. Archbishop Kirloss of Elubabor

. Archbishop Matheias of Jerusalem

. Archbishop Philips, member of the Senoid
. Archbishop Jackob of Wellega

10. Archbishop Endrias of Gonder

41. Archbishop Selama of Gemugofa

12. Archbishop Saweros of Shoa

13. Archbishop Thimeteous of Sidamo

14. Archbishop Atnatious of New York

CONDONE

In 1984 the atheistic, Communist regime had the audacity to
appoint an avowed Marxist-Leninist from its own ranks, a Mr.
Ababaw Yegzaw, to head the administration of the Ethiopian Or-
thodox Church. The appointment of Mr. Yegzaw to head the ad-
ministration of the Ethiopian Orthodox Church finalized the con-
trol of the Church by the Communist regime.

In direct contravention to canon law, demotions and transfers
