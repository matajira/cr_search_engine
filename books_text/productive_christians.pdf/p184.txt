170 Productive Christians in an Age of Guilt-Manipulators

example of Jesus and his disciples, Sider’s “Jerusalem Model” is
no model at all. It was a special tactic designed to meet unique cir-
cumstances. And, obviously, if all Christians were to simply liq-
uidate all their capital and redistribute it for immediate consump-
tion, there would soon be nothing left. Selling property can con-
tinue only so long as there remains property to sell and buyers
with assets to exchange. Certainly, the example of these early
Christians rebukes our indifference and unwillingness to help
needy brothers and sisters around us. But it does not demand that
we do what they did, unless we get a load of 8,000 jobless Chris-
tian immigrants dumped on our community too~and even then,
capital consumption should be a last resort. We will also need rick
nonbelievers to sell to—economically productive people who can put
our capital to good use.

Sider's reasons for wanting Christians to practice economic
equality are not really related to the church. His goal is to use
Christians to appeal for governmentally enforced economic equal-
ity, and he feels that the demand for this would be more credible if
the church were to observe it as an example. This is the basis for
everything he is demanding from the church —it is all a means to his very
political ends. This can be seen in the title of his article, “Sharing
The Wealth: The Church As Biblical Model For Public Policy.”
He sees the church as “the most universal body in the world to-
day”;!! thus if he can control the church, he will be well on his
way to controlling the world. Here is his reasoning:

To ask government to legislate what the church cannot persuade its
members to live is a tragic absurdity. . . . Only as groups of believers in
North America and Europe dare to incarnate in their life together what
the Bible teaches about economic relationships among the people of God
do they have any right to demand that leaders in Washington or West-
minster shape a new world economic order. . . only af the body of Christ is
already beginning to live a radically new model of economic sharing will our demand
for political change have integrity and impact.1?

10. The Christian Century (June 8-15, 1977), pp. 560ff.
11. Bid., pp. 564f,
12. Tbid., pp. 560, 563. Ialics added. See also Sider, Rich Christians, p. 205 [p. 193].
