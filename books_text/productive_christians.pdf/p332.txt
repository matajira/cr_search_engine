318 Productive Christians in an Age of Guilt-Manipulators

While he has spruced up his terminology to some slight extent,
trying not to sound frightening, he has made no fundamental
changes in his basic message of guilt-manipulation and the ad-
vocacy of totalitarian state controls. He has presented no evidence
that his policies will work to alleviate poverty, or even that they
will not actually create more poverty and misery themselves. And
he has not abandoned his commitment to corifiscation and de-
struction; nor has he said a single word about productivity and the
development of resources. Most basic of all; he has not changed his
position on the “blueprints” issue: God’s holy Word, according to
Sider, still has no answers, no concrete ethical guidance for social
policy. The anarchist/socialist Pierre-Joseph Proudhon was right:
At the bottom of politics one always finds theology, '#

We can dispose of Ronald Sider at this point; by this time I
have refuted him several times over. Let me spend these last few
lines concentrating on you, the reader. If you are concerned about
the plight of the poor, what should yeu do about it? E am more con-
vinced than ever before that mere poverty programs, no matter
how well funded and organized, will never solve the problem,
What is needed is productivity. We are to be, as my title states, Pro-
ductive Christians. This means faithfulness and diligence in our call-
ings. It means training up our children to be responsible, honest,
and hardworking. It means placing a high value on the spirit of
enterprise, on the ability to imagine and bring about new oppor-
tunities, to create increasing wealth. And, above all, it means
godliness — following the Blueprints. Wealth and productivity for
all of society will come through God’s blessing alone. He has
promised an overwhelming abundance of blessings in every area
of life for His obedient people, and we must seek His face the
“lifting up of His countenance,” as the biblical liturgy reminds us
(Numbers 6:24-26)— for the satisfaction of our needs.

What about charity programs? These are certainly valid and
necessary, assuming they are truly charitable, and not coercive
(e.g., statist) in nature. But, again, we must be careful to follow

183, Cited in Erik von Kuehnelt-Leddihn, Leflism, p. 54.
