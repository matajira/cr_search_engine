D&a Vu-—A Romp Through Ronald Sider’s Second Edition 271

mid-1950s, but the difference in rates was extremely smail—as
late as 1965, barely more than a single percentage point,

“Beginning in 1966, black male LFP started to fail substantially
faster than white LFP. By 1972, a gap of 5.9 percentage points had
opened up between black males and white males. By 1976, the
year the slide finally halted, the gap was 7.7 percentage points. To
put it another way: from 1954 to 1965, the black reduction in LFP
was 17 ‘percent larger than for whites. From 1965 to 1976, it was
271 percent larger.”**

This change was absolutely unprecedented, Murray says; un-
til this happened “we had never witnessed large-scale voluntary
withdrawal from (or failure to enlist in) the labor market by able-
bodied males.”55 The tragedy of this decline in black LFP is com-
pounded by the fact that, during the same period, it was mirrored
in almost every other aspect of black activity in American culture.
For example, after steady improvement from the 1950s to the early
60s, black schools and educational performance suddenly took a
nosedive during the years of radical growth and proliferation of
welfare programs (i.e, after 1964), resulting in what is now an enor-
mous gap between blacks and whites. During the same period, the
number of criminals and victims soared rapidly: whereas the
homicide rate among blacks had steadily dropped between 1950
and 1960, it suddenly climbed sharply after 1964. At the same time,
there was a rapid rise in both the number of illegitimate births
among blacks, and the number of black teenagers giving birth. In
fact, it was precisely among the youth that the changes in attitudes
and performance were most pronounced: Across the board, in
every area, the young behaved differently from everyone else. The
black family had remained fairly stable for decades; but as the new
generation grew up, the number of “one-parent” families among
low-income blacks rose dramatically, Again, we must remember
that sudden changes like these are utterly unprecedented in
American history; and, beginning in the mid-1960s, the changes all
happened to the poor, and they happened all at once.

54, Ibid., p. 76.
85, Ibid, p. 77.
