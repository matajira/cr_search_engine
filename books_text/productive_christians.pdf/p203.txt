Statism 189

force oral hygiene on the population. Dental Justice For All.
Smile!

Sider does not appeal to Scripture for this plan, Instead, he
says, “Norway has proven that a national food policy can make a
difference.” No argument on that point: Norway, one of the
most heavily socialistic nations in the world, is in deep financial
trouble. Norway suffers the highest production costs in the world.
Its social welfare system eats up about 55% of its gross national
product. Every industrial worker receives high government sub-
sidies (shipbuilders are subsidized to the tune of $10,000 per
year), and it multiplies its problems by strictly inhibiting all
private investments.’* Give Ronald Sider the controls, and you
too can have a Worker’s Paradise. Bauer pinpoints the problem:
“The state cannot create new additional productive resources.
The politicians and civil servants who direct its policy dispose
only of resources diverted from the rest of the economy. It is cer-
tainly not clear why overriding the decisions of private persons
should increase the flow of income, since the resources used by the
planners must have been diverted from other productive public or
private uses.”!5

Thus, “the controls imposed under comprehensive planning
generally have nothing to do with raising popular living stand-
ards. Indeed, they usually depress them.”'6 Mises summed it up:
“There is no other alternative to totalitarian slavery than liberty.
There is no other planning for freedom and general welfare than
to let the market system work. There is no other means to full
employment, rising real wage rates and a high standard of living
for the common man than private initiative and free enterprise.”!7

If this is true—and it has been demonstrated again and again

13. Sider, Rich Christians, p. 214.

14, US News and World Report (February 20, 1978), p. 55; and Time
{November 20, 1978), p. 84.

15. P. T. Bauer, Dissent on Development (Cambridge: Harvard University Press,
1971, 1976), p. 73.

16. Ibid., p. 86.

17, Ludwig von Mises, Planning for Freedom (South Holland, IL: Libertarian
Press, fourth ed., 1980), p. 17.
