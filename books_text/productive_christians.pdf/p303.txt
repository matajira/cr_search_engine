Déja Vu—A Romp Through Ronald Sider’s Second Edition 289

Socialism is inseparable from violence. Regardless of all their pro-
fessions of peaceful intent, socialists everywhere have had to
resort to the use of violence to bring about their goals. They can-
not do otherwise. The very nature of socialism—that the state
shall be empowered to regulate people's activities and confiscate
people's property ~ demands violent activity, The success of any
socialistic “land reform” program depends on only one thing:
which side has more firepower.

If Dr. Sider truly wished for a “non-violent revolution,” he
would limit himself to exhorting wealthy citizens to give away
their possessions —leaving it up to their own discretion as to how
fully they will comply with his requests. But the professor has not
so limited himself. He has called repeatedly for legistation of his
demands. That brings in the state. And the only reason for bring-
ing in the state is that the state has a legal monopoly on coercion
and violence. The state has more firepower. Sider’s program of the
Gentle Nudge is merely a temporary expedient. His goal, in-
escapably, is armed force. It is thus no accident, no oversight, that
the professor's associates in the Jubilee Fund are using charity
money to finance bloodthirsty terrorists.

In his new edition, Dr Sider objects to this accusation:

‘To argue that Christians should work politically to change those aspects
of our economic structures that are unjust is not to call for a violent
revolution that would forcibly impose a centralized, statist society. I
believe that the way of Jesus is the way of nonviolent love, even for
enemies. 1 therefore reject the use of lethal violence. The exercise of
political influence in a democratic society, of course, involves the use of
nonlethal pressure (or force}. When we legislate penalties for drunken
driving or speeding, we use an appropriate kind of nonlethal “force.”
The same is true when we pass legislation that changes foreign policies
toward poor nations, makes trade patterns more just, restricts the op-
pressive policies of multinational corporations't or increases foreign

101. In his new edition, Sider appears to be quite lathered up about the
dangers of multinational corporations. Ironically, concern about the menace of
the multinationals hit the international leftist fashionable-idea network just as
their actual power and influence began to decline. The only real danger posed by
