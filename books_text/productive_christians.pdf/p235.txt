The Basis for Economic Growth 221

Salvation, in this world and the next, is not found in econom-
ies. Regeneration is the only foundation for social stability and
growth. Slaves need liberty, but they cannot be legislated into it.
If men are not bound to Christ, they will be slaves to Satan, and
there will be no escape from the whirlpool of cultural decomposi-
tion. At least totalitarianism, for ail its faults, acts as a temporary
buffer of semi-law before the final end in complete dissolution.
Edward Banfield’s study of a backward society has already been
cited; but he wrote another book which was much more upsetting
to sociologists, for it was based on his research among social
classes in America, and shows the fundamental causes of institu-
tional poverty in this country: “At the present-oriented end of the
scale, the lower-class individual lives from moment to moment, If
he has any awareness of a future, it is of something fixed, fated,
beyond his control: things happen # him, he does not make them
happen. Impulse governs his behavior, either because he cannot
discipline himself to sacrifice a present for a future satisfaction or
because he has no sense of the future. He is therefore radically im-
provident: whatever he cannot use immediately he considers
valueless. His bodily needs (especially for sex) and his taste for
“action” take precedence over everything else—and certainly over
any work routine.. He works only as he must to stay alive, and
drifts from one unskilled job to another, taking no interest in his
work. . . . Although his income is usually much lower than that
of the working-class individual, the market value of his car, televi-
sion, and household appliances and playthings is likely to be con-
siderably more. He is careless with his things, however, and, even
when nearly new, they are likely to be permanently out of order
for lack of minor repairs. . .. .

“The lower-class individual has a feeble, attenuated sense of
self, he suffers from feelings of self-contempt and inadequacy, and
is often apathetic or dejected. . . . In his relations with others he
is suspicious and hostile, aggressive yet dependent. He is unable
to maintain a stable relationship with a mate; commonly he does
not marry....He feels no attachment to community,
neighbors, or friends (he has companions, not friends), resents all
