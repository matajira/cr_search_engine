206 Productive Christians in an Age of Guilt-Manipulators

unique access to positions of influence denied to other statists. He
claims to be a Christian, and his writings are dotted with Bible
quotations. He teaches at a school which trains men for the
ministry, He is invited to speak at Christian churches, colleges,
seminaries, and conferences, His books are distributed by an im-
portant Christian publishing house, and I have not yet seen one
Christian bookstore that does not carry his books. (One store near
me refuses to stock any books that uphold biblical law, while
Sider’s books advocating theft are prominently displayed. ) His ar-
ticles are printed in the leading Christian magazines; clearly, he
has a powerful platform from which to speak. His message is be-
ing heard, and is increasingly accepted. (In speaking to a Chris-
tian schoolteacher whom I had just met, I happened to use the
word biblical. She immediately assumed, for no other reason, that
I was talking about Sider and the Evangelicals for Social Action),

Sider’s specific message is one of guilt. In a previous chapter,
we noted some of the many ways he attempts to cause us to be
ashamed, embarrassed and humiliated because of the blessings
we enjoy under the hand of God. Envy is directed at us from all
sides by his skillful manipulation of fallacies, distortion of facts,
and misinterpretation of Scripture. Envy is then inverted, and
becomes guilt—not in the biblical sense of actual, moral trans-
gression of God’s law, but psychological, sociological guilt, the
feeling of being responsible for the envy of others. And perhaps no
tool of totalitarianism is as significant as the ability to induce guilt
feelings.

How is this so? It is because of the relationship of guilt and the
loss of freedom. Uf a man is enslaved to guilt, he is rendered
powerless. The child who feels guilty for not completing his
homework is unable to face his classmates and teacher with
confidence. The businessman who feels guilty for having made
some error is unable to fully direct his concentration to the tasks at
hand. When we are ashamed — “guilty” — for failing to remember
someone’s birthday or anniversary, we are less able to deal with
him personally. I have known several college students whose
sense of guilt because of one or two instances of tardiness was so
