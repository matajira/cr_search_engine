“Slavery is an example of an institutionalized evil.”
(Ronald Sider, Journal of Theology for
Southern Africa, December 1979, p. 38.)

“You may acquire male and female slaves from the pagan nations

that are around you.”
(Leviticus 25:44)
