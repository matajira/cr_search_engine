“I want to argue that one of the central biblical doctrines is that
God is on the side of the poor... .”

(Ronald Sider, The Christian

Century, March 19, 1980, p. 314)

“God is not one to show partiality, but in every nation the man
who fears him and does what is right, is welcome to him.”
(Acts 10:34-35)
