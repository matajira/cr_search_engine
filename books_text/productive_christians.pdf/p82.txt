“God displayed His power at the Exodus . . . to free a poor op-
pressed people.”

(Ronald Sider, The Christian

Century, March 19, 1980, p. 315)

“He brought forth His people with joy, His chosen ones with a joy-
ful shout.”
(Psabn 105:43)
