Foreign Aid 109

poverty level, in fact more than $30 billion are spent on programs
to get people out of poverty, and there are still more than 5 mil-
lion families below the poverty level. Clearly most of the money
spent on the poor does not directly reach the poor, but is absorbed
by the salaries of officials, staffs, consultants, and by other ex-
penses of antipoverty organizations.”23

Third, foreign aid actually widens the gap between rich and poor na-
tions. As we have seen, it inhibits those factors which would pro-
duce growth (e.g., by creating dependence rather than responsi-
bility}. More than this, it encourages explicit envy toward the
rich, who are held to be responsible for the plight of those below
them, Unfortunately, envy cannot be contained. Once directed
against the rich in other countries, it soon focuses on people who
are better off in one’s own country. The idea that it is evil to make
personal economic progress takes hold of the people, and fear of
being envied prohibits growth and encourages poverty. Sociolo-
gist Helmut Schoeck writes: “One of the decisive factors in under-
development or non-development is the “envy-barrier,” or insti-
tutionalized envy among the population .. . Now when, as is
quite patently the case, many of the politicians in developing
countries make use of all their powers of rhetoric or persuasion for
the crudest exacerbation of their people’s envy of the rich indus-
trial nations (even to the point of branding the latter as the cause
of their own countries’ poverty), these people’s sense of envy —to
which their cultures already make them overprone —is intensified.
Thus the feeling and states of mind which inhibit development are
not lessened but confined and given political sanction by the
countries’ leaders.”2+

23. Thomas Sowell, Race and Economics (New York: David McKay Company,
Inc., 1975), pp. 1958.

24, Helmut Schoeck, Enay: A Theory of Social Behaviour (New York: Harcourt,
Brace & World, Inc. 1970), pp. 197£.; ef. 46ff.; see also P. T. Bauer, Dissent on
Development pp. 118, 120; and Reality and Rhetoric: Studies in the Economics of Deotlop-
ment (Cambridge, MA: Harvard University Press, 1984), p. 84.
