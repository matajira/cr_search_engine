The Conquest of Poverty 237

The kingdom of Jesus Christ is now being extended over all the
earth. God has “delivered us from the domain of darkness, and
transferred us to the kingdom of His beloved Son” (Colossians
1:13). As members of His kingdom, Christians are ruling with
Him now; “He has made us to be a kingdom, priests to His God
and Father” (Revelation 1:6). In His messages to the churches of
Asia, Jesus exhorted each one to overcome the powers of evil in
terms of their high calling as kings and priests; and He made a
promise to those who obeyed, using the language of Psalm 2:

And he who overcomes, and he who keeps My deeds until the end, to
him I will give authority over the nations: and he shall rule them with a
rod of iron, as the vessels of the potter are broken to pieces, as I received
authority from My Father (Revelation 2:26-27).

Jesus is King now, in this age; and His obedient people have
every reason to expect increasing victories in this age, as they confront
the nations with the omnipotent authority of their Lord. This will
not come without a struggle, as the ungodly seek to retain their il-
legitimate hold on the world. But victory is ours, in principle, as
we are to march forth into all the world and into every field of life,
conquering in Jesus’ name.

Matthew 16:18—Christ’s promise that “the gates of hell shall
not prevail against the church” —is often watered down to mean
only that the church will be divinely protected against attacks by
the forces of evil. Come now. When have you ever heard of gates
attacking anything? Gates do not attack. Gates defend. The picture
here is not that of the church besieged by the forces of evil. It’s the
other way around. The church is the one on the offensive. God’s people
are attacking the forces of evil, and Jesus promises that the
ungodly will be defenseless under our attack. We will win! We
share Christ’s dominion now, and we are to extend that dominion
throughout creation, confident of victory.

Marcellus Kik said: “It is true that we must not underestimate
the influence and power of the Evil One; but it is also true that he
can be easily overcome by those who believe in the power of the
blood of Christ and are not ashamed to testify of it. They are the
