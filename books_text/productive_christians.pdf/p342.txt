328 Productive Christians in an Age of Guilt-Manipulators

A century later, what was essentially the same Christian her-
esy reappeared, earnestly striving for peace and justice through
socialism; this time, it was incarnated in the Taborites, who were
more self-consciously bloodthirsty than even the Apostolic Breth-
ren. The end of the world was coming in 1420, they decided, so
they had to work fast: “It is necessary for each of the faithful to
wash his hands in the blood of the enemies of Christ,” declared
their prophets. “All peasants who refuse to join the Taborites shail
be destroyed along with their property.”}* Everything was social-
ized (again, wives included), towns were razed to the ground, and
men, women, and children were indiscriminately and gleefully
slaughtered. These Christian socialists were regarded as almost
completely inhuman in their taste for cruelty and torture (special
atrocities were reserved for pregnant women).?° As with the pre-
vious experiments in socialistic Christianity, the Taborites were
remarkably successful: they shook central Europe to its founda-
tions, and their impact was felt as far away as England and Spain.
While the earth has endured long after 1420 (in stubborn defiance
of the inspired prophecies), the Taborites’ world ended in a bloody
battle in 1434. For almost a century thereafter, Christian Social-
ism left the Church in relative peace; then, in a Satanic, frantic
attempt to destroy Christian civilization and the Protestant Refor-
mation, it raised its head again, in even more murderous and de-
vastating forms.

Christian Socialism During the Reformation

The term for revolutionary socialism during the Reformation
was Anabaptism. The Anabaptists, while they claimed to be “true”
Christians, denied virtually ali the content of the faith.?! They re-
jected biblical law, rebelled against the Church’s government,

19, Shafarevich, The Socialist Phenomenon, p, 30,

20. Erik von Kuehnelt-Leddihn, Leftism: From de Sade and Marx to Hitler and
Marcuse (New Rochelle, NY: Arlington House, 1974), pp. 154f.

21. See Ray Sutton, “The Baptist Failure,” in James B, Jordan, ed, The Fail-
ure of the American Baptist Culture (Tyler, TX: Geneva Ministries, 1982), pp.
152-84, for a Reformed analysis of Anabaptist theology.
