3
THE EXODUS AS A LIBERATION MOVEMENT

As we have seen, Ronald Sider regards slavery as always a sin.
In terms of this, he naturally feels that all slaves should always be
liberated. A recurring theme in his writings is that the biblical ac-
count of the Exodus of Israel provides a divinely inspired prece-
dent for liberating the oppressed peoples of the world through
political action. In other words, at “pivotal points of revelation
history,” God worked to free slaves. Since this indicates God’s
concern for the oppressed of the world, we too should work for
their liberation from unjust economic and social structures, etc.,
etc.

It is crucial to examine carefully Sider’s principle of interpreta-
tion here. Summing up the Exodus account, he says that God
“acted to free a poor, oppressed people . . . he acted to end eco-

nomic oppression and bring freedom to slaves. . . . The liberation
of a poor, oppressed people was right at the heart of God’s
design.”!

But this is a serious misreading of the biblical record. Consider
how God described what He was doing:

And the Lorn said, “I have surely seen the affliction of My people who
are in Egypt, and have given heed to their cry because of their
taskmasters, for I am aware of their sufferings. So I have come down to
deliver them from the power of the Egyptians, and to bring them up
from that land to a good and spacious land, to a land flowing with milk
and honey . . .” (Exodus 3:7-8),

1. Sider, Rich Christians, pp. 60f. [pp, 54f.].
69
