120 Productive Christians in an Age of Gutlt-Manipulators

Am I wrong in seeing a pattern here? All five causes of “over-
population” are entirely man-made. All five stem from man’s sin-
ful attempt to lord it over other men. All five, in other words, are
reducible to one: statism. Runaway population explosions are caused by
runaway state controls over the population.

Now let us return to Ronald Sider’s “cure” for overpopulation:
foreign aid. As P. T. Bauer states, “foreign aid promotes cen-
tralized and closely controlled economies.”"7 And “closely con-
trolled economies” produce overpopulation! There is a saying
that some cures are as bad as the disease, but this is not the case
here. Ronald Sider’s cure is the disease.

E. CG. Pasour, Jr., remarks that much of the cause of world
hunger ‘can be attributed to the destruction or reduction of
private property rights of food producers in countries where the
hunger problem is most acute.” He continues: “Numerous ex-
amples can be cited where governments have weakened or
destroyed economic incentives by confiscating private land, forc-
ing farmers to work on collectivized farms, instituting price con-
trols on food, and other such measures. India provides a good ex-
ample . . . much of the food crisis in India can be attributed to
actions taken by the Indian government affecting incentives of
food producers. After her big electoral victory in 1972, Mrs. Gan-
dhi’s party reduced the amount of land that could be held by an
adult male from 30 irrigated acres to 18 acres, . . . In addition to
the direct effect of land confiscation on incentives, the policy also
affected the profitability of tractors and implements. The reduced
acreage was not enough to support the machinery.

“The government also nationalized the wholesale grain trade,
forcing farmers to sell their crops at fixed prices below the market
level, whereas previously farmers were permitted to sell half of
their grain to wholesalers at the higher market price: The impact
of such actions on the quantity of food produced and marketed is
predictable.”"8

Overpopulation is virtually a necessary result of socialism. The

47, Bauer, p. 128.
18. Cited in Weber, p. 213.
