410 Productive Christians in an Age of Guilt-Manipulators

Schall, James V. Liberation Theology in Latin America, San Fran-
cisco: Ignatius Press, 1982.

Scott, Otto. Rabespierre: The Voice of Virtue. Vallecito, CA: Ross
House Books, 1974.

. The Secret Six: John Brown and the Abolitionist Move-
ment. New York: Times Books, 1979.

Shafarevich, Igor. The Socialist Phenomenon. New York: Harper and
Row, 1980.

Shifrin, Avraham, The First Guidebook to Prisons and Concentration
Camps of the Soviet Union. New York: Bantam Books, 1982.
Sowell, Thomas. Marxism: Philosophy and Economics. New York:

William Morrow and Co., 1985.

Spencer, Herbert. The Man Versus the State: With Six Essays on Gov-
ernment, Society, and Freedom. Indianapolis: Liberty Classics,
1981.

Tolstoy, Nikolai. Stalin’s Secret War. New York: Holt, Rinehart and
Winston, 1981.

Tyrmand, Leopold. The Rosa Luxemburg Contraceptives Cooperative:
A Primer on Communist Civilization. New York: The Macmillan
Company, 1972.

Voslensky, Michael. Nomenklatura: The Soviet Ruling Class. Garden
City, NY: Doubleday and Co., 1984.

Wittfogtl, Karl A. Oriental Despotism: A Comparative Study of Total
Power New York: Random House, [1957] 1981.
Wolfe, Bertram D. Three Who Made a Revolution: A Biographical

History, New York: Stein and Day, [1948] 1984.

The Failure of Interventionism
Borgin, Karl, and Corbett, Kathleen. The Destruction of a Conti-
nent: Africa and International Aid. New York: Harcourt Brace
Jovanovich, 1982.

Carson, Clarence. Organized Against Whom? The Labor Union in
America, Alexandria, VA: Western Goals, 1983.
