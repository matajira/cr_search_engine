Preface io the Third Edition xiii

ception: in fact, the severity of the famine has been deliberately in-
creased by.the state and used as a weapon to force the people into
submission. It is, to a large degree, a planned famine—a calculated,
intentional matter of government policy. Provident farmers who at-
tempted to store food from good harvests for future seasons of
drought have been charged with “capitalist accumulation” and ex-
ecuted for treason against socialist ideals. There is probably no na-
tion on earth whose government is so slavishly devoted to the Soviet
Union and its collectivist agricultural policies. Mengistu’s socialist
tyranny has triggered patriotic insurrections against his regime in
at least twelve of Ethiopia’s fourteen provinces (see Appendix 5).?

In striking contrast to the “Let-them-eat-ideology” approach of
Sider and Mengistu has been the work of a man who is, without a
doubt, the most eminent economist working today on the issues of
the Third World, foreign aid, and the economics of development:
the Christian scholar Lord Peter T. Bauer, of the London School
of Economics. Bauer is the author of several books, all of which
are absolutely indispensable for any in-depth study of develop-
ment: Dissent on Development (Harvard, 1971, 1976), Equality, the
Third World, and Economic Delusion (Harvard, 1981), and Reality and
Rhetoric (Harvard, 1984). In a field clouded with muddled think-
ing, vague rhetoric, and often downright crackpottery, his
straightforward style and refreshing clear-headedness are not only
welcome but positively astounding. He is perhaps best known,
and.most resented, for his stubborn insistence that the canons of
economics (and a good deal of common sense) should be applied
to “development economics”; that the study of development is simply
an extension of the study of economics, and operates by the same
rules.

2. For more information on the politics of famine in Ethiopia, write to Philip
Nicolaides, Accuracy in Media, 1275 K Street, N.W., Suite 1150, Washington,
D.G. 20005; see also the essay on Ethiopia in Biblical Economics Today, Vol. VIL,
No. 3 (April/May 1985), On the effects of socialist policies in other African na~
tions, see Karl Borgin and Kathleen Corbett, The Destruction of a Continent: Africa
and International Aid (New York: Harcourt Brace Jovanovich, 1982); and Geoffrey
Wheatcroft, “The Anguish of Africa,” The New Republic (January 9 & 16, 1983),
pp. 18-23.
