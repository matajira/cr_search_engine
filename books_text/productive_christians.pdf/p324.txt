310 Productive Christians in an Age of Guilt-Manipulators

Thus—even though God is the sovereign Lord of economics;
even though economics should be subject to His revelation; even
though biblical economic principles will provide lasting peace and
social harmony; even though Scripture is the norm — there are no
norms! God, who has promised all these wonderful blessings to
those who obey Him, has abandoned us without leaving so much
as a blueprint! Where shall we turn?

Indeed, that is precisely the question. As Eve discovered, if we
declare that God’s blueprints are insufficient, or unavailable, or
just “missing,” we will find others who will be happy to accomo-
date us with forged blueprints of their own. Denying that any divine
blueprints exist, Sider has allowed his own “thinking and living to
be shaped by the surrounding culture’s views and values,” as he
himself phrases it so well.

The professor is not alone. In 1984, InterVarsity Press pub-
lished Wealth and Poverty: Four Christian Views of Economics, edited
by Robert Clouse. The four views are: Syee-market capitalism” (in
terms of biblical law), defended by Gary North; ‘the guided-market
system” (a somewhat fascistic capitalism in which the government
controls the “free” market), defended by William E. Diehl; “decen-
tralist economies” (a retreatist, communalist socialism), defended by
Art Gish; atid Yentralist economics” (full-throttle statism), defended
by John Gladwin. What is most noteworthy about this collection
—which could have have been subtitled Three Commie Wimps Meet
Godzilla—is that Gary North’s opponents, while disagreeing
among themselves about which brand of socialism should be in-
stalled, are in complete agreement on one central issue: The Bible
has no blueprints! Let them speak for themselves:

Diehi: The fact that our Scriptures can be used to support or condemn
any economic philosophy suggests that the Bible is not intended to lay
out an economic plan which will apply for all times and places. (p. 87)
There is no system which is inherently Christian in nature. (pp. 10if.)

Gish: The Bible does not advocate one particular economic system for
the world. (p. 118)
