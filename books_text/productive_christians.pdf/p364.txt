350 Productive Christians in an Age of Guilt-Manipulators

Catholic Church only when he decided to get married.5 The
Church’s hierarchy tolerated his economics and political theory,
but marriage was just a bit too much. His appearances were
always marked by wildly enthusiastic crowds at Protestant and
State-supported colleges.

Why did this Roman Catholic verbal revolutionary receive
warm welcomes by Protestant faculty members and humanist
faculty members? Because Groppi was preaching a popular brand
of humanism in 1969 and 1970—~the “pre-Kent State” humanism
of college radicalism without personal risks—which ‘his listeners
shared.® (The confrontation between the National Guard and the
students at Kent State University in Ohio ended the college vio-
lence of the 1964-70 era, as an old-time New Deal liberal college
classmate of mine predicted it would the week of the shootings.
“Too risky for them now,” he said. “They might get their +++***
shot off.” The following semester, all over the world, campus vio-
lence ceased, and a decade and a half later, it is still quiet.)

The market for soft-core radicalism revived on the Christian col-
lege circuit in the late 1970s. There was a real “need” for a quiet,
self-effacing, kindly preacher of class conflict and socialist wealth-
redistribution. The schools are still staffed by Darwinists, and
their faith is still intact. They trust the predestinating State, not
the free market economy. But they needed a spokesman who was
uniquely gifted to “sell the ideological product” on the post-Kent
State campus. Where there is demand, the free market will even-
tually respond. The market produced the supply: Sider.

Rich Christians in an Age of Hunger is only one of a series.of books
designed to call into question the ability of free market institu-
tions, coupled with Christian charity, to achieve the economic
goals of the kingdom of God. The Christian Reformed Church’s

5. I personally heard him tell an audience of enthusiastic university students
that he had given the following spiritual counsel to a black parishoner as the man.
was about to go on a theft spree during a late-1960’s race riot in Milwaukee:
“Don't get caught, Joe; don’t get caught!” The crowd cheered.

6. For an account of one such visit, see “Father Groppi At Calvin College,”
The Standard Bearer, XLVI (June 1, 1970).
