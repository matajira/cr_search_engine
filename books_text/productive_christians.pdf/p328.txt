314 Productive Christians in an Age of Guilt-Manipulators

have reverence for some “transcendent” something-or-other (i.¢., a
false god) “which others approach in other ways”? “Thou shalt
have no other gods before Me,” the Lord thundered from Sinai
(Exodus 20:2). Lets face facts, comrades: God just doesn’t like
pluralism. He certainly did not encourage reverence for the
“transcendent” golden calf (Exodus 32), or the “transcendent” gods
of the Canaanites (Deuteronomy 7:1-5; 12:1-3; 13:1-18; 17:2-7); in
fact, what the Canaanites called transcendent, God called abomin-
ation (Deuteronomy 18:9-13).

‘What Novak would rather not admit is that the moment he
abandoned the Bible as the ultimate standard of truth, of right
and wrong, of “appropriateness,” of “human liberty,” of “transcen-
dence,” he chose something else as the standard. It is impossible to
make any kind of moral judgments except in terms of a moral
standard, Now, why is it that men who call themselves Christians,
who were baptized under oath into the holy Name of the Trinity,
are so fearful of following God’s Word in God’s world? The Bible
is clear: If we love God, we must love His law, we must obey His
commandments (Deuteronomy 6:4-6; John 14:15, 21; 1 John 5:3).
“It is a question of ethics, not net financial worth, which is the over-
riding social and economic concern of the Bible. This is why we
are inevitably driven back to consider specifics of biblical law.
This is why we must raise the question of ‘blueprints.’ "175

Get this down: The question is not, and never has been, one
of “blueprint or no blueprint.” The question is always: “Whose
blueprint?” As the “parable” in Appendix 3 points out, there is
always an Architect; and He (or he) always has a blueprint. Pro-
fessor Sider and his associates are merely applying for what they
claim is a vacant job.

All law is religious. Al law is religious. Every law-order, every
society, every culture is founded on a “blueprint,” on some ulti-
mate standard and set of values. And whatever is the source for
that standard is the god of that system. This is why every society is a
theocracy. A theocracy is inescapable. The choice facing man is not,

475. Gary North, in Robert G. Clouse, ed,, Wealth and Poverty, p. 164.
