52 Productive Christians in an Age of Gutlt-Manipulators

Tithes

We must begin our analysis of the biblical Poor Laws with a
consideration of tithing, since this is a basic duty of the Covenant.
It is commonly held that we are no longer under any obligation to
tithe in this “dispensation.” There is not a shred of evidence to
support such a position: the law of the tithe has never been revoked.
And, it should be noted, while the modern abandonment of
tithing has a superficial appearance of freedom, it has actually
been replaced with a tyrannical legalism. Listen to any radio or
television preacher—or perhaps your own pastor— appealing for
funds. If he rejects the tithe, what is the basis for his plea? LOVE,
He does not, of course, define love as the Bible defines it—keep-
ing God’s commandments (Romans 13:10; I John 5:3)—but
rather according to the perceived “needs” of his own ministry.
God's simple requirement is that we give ten percent of our in-
come; once we have paid His tax, we know that no more is
demanded. The modern preacher, on the other hand, defines
your love for God in terms of how much you give. (“How much
do you love God? Only ten percent? Only twenty? Only thirty?
Shame on you! You should love God lots more than that! If you
really, completely love Him, you'll sign over your next paycheck
to me and drop it in the plate. And don’t worry about taking care
of your family. How selfish of you. God will take care of them,
After all, He’s taking care of me, isn’t He?”)

Ronald Sider’s approach is not really much different. His
scheme for a “graduated tithe” (which he claims is only a “modest
beginning” for whatever it is he really has in mind) is based, not
on Scripture, but on the Club of Rome's fallacy-ridden publica-
tion entitled Limits io Growth. Where God requires ten percent of
our income, Sider demands that all income above $14,850 (fig-
ured for a family of five) be given away.® Now, Sider may object
to this charge, claiming that he isn’t demanding anything. But it’s
difficult to read it any other way. Perhaps he is, after all, only
hinting. But it’s a pretty strong hint, spiked heavily with guilt-

6. Sider, Rich Christians, pp. 175ff. [pp. 1668].
