100 Productive Christians in an Age of Guilt-Manipulators

just aid. The opening pages of his book contain some chilling
futuristic speculations in which the Indian Prime Minister
threatens to blow up Boston and New York with nuclear explo-
sives, in retaliation for not receiving U.S. aid.5 “Responsible peo-
ple consider even this horrifying prospect a genuine possibility

. in times of severe famine, countries like India will be sorely
tempted to try nuclear blackmail.”6 Perhaps. But Sider is stacking
the deck considerably. He implies that the United States is evil
and heartless, preoccupied with other concerns, unwilling to com-
ply with “just trade patterns.” Meanwhile, the Prime Minister is
stricken with compassion for the hungry, pleads to deaf ears, and
becomes deperate. She really doesn’t like the idea, but “the
momentum of events could force her to explode one of the bombs.
Terrible retaliation might follow. But she is desperate.”? In sum,
we are the guilty ones for not dispensing government aid, while
this potential mass murderer deserves our pity. She was “forced”
to give the order, because our program was insufficient: “A little
was given, but it was never enough.” Nor will any amount of aid
ever be enough for those whose envy of the rich is so vicious that
they can contemplate the slaughter of millions. God commands
the poor to call upon Him for deliverance. When, instead, they
are ready to destroy those who fail to “‘share” sufficiently, they are
not to be pitied. Envy cannot be appeased.?

Modest Proposals?

In addition to the usual methods of foreign aid, Ronald Sider
proposes two measures which should receive particular attention
in this discussion: tariffs and commodity agreements. 1 don’t wish to
sound repetitious, but it must be stated again: these policies are
immoral. The state has no biblical right to intervene in the market

5. Ibid., pp. 14-16. [This scenario is dropped in the second edition, but its like-
lihood is again threatened on pp. 27£.}

6. Ibid., p. 16.

7. Ibid, Italics added.

8. Ibid. p. 15.

9. See Henry Hazlitt, The Conquest of Poverty (New Rochelle, NY: Arlington
House, 1973), pp. 125ff.; ef. Schoeck, pp, 1938.
