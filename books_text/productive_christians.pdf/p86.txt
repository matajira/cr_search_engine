72 Productive Christians in an Age of Guilt-Manipulators

that needed product.”? But, to their everlasting shame, business-
men don’t seem to listen to his suggestions. They foolishly are in-
tent on producing what people say they wart rather than what ex-
perts say they need. What is required, therefore, is Liberation.
Liberation will apply Sider’s good intentions by force, at the point
of a policeman’s gun. Just as in the Vietnam War it became “nec-
essary to destroy the village in order to save it,” society will
achieve liberation through totalitarianism.

Committed as he is to liberation from slavery, Ronald Sider is
not ignorant of past. attempts. One movement particularly
revered by him is the Abolitionist activity of the nineteenth cen-

_ tury. He especially applauds the radicalism of the preacher
Charles Finney, who founded Oberlin College as a haven for
abolitionism and feminism, Jonathan Blanchard, an early student
at Oberlin, went on to become Wheaton College’s first president,
and Sider mourns that Wheaton eventually declined from its orig-
inal position as a hotbed of social activism.

However, it is Sider’s statements on Finney and Abolitionism
which are of special interest. Writing in the Christian Century, he
claims that “Charles Finney’s evangelical abolitionists stood
solidly in the biblical tradition in their search for justice for the
poor and oppressed of their time.”* Expanding on this theme, he
writes elsewhere:

Finney was the Billy Graham of the nineteenth century. He led
evangelistic crusades throughout the country. The filling of the Holy
Spirit was central in his life and preaching. He was also one of the
leading abolitionists working to end the unjust system of slavery.
Church discipline was used at his church at Oberlin College which he
founded against anyone holding slaves. Finney and his students prac-

2. “Door Interview: Ron Sider,” The Wittenburg Door (Oct./Nov. 1979, No.
51), p. 27.

3. Sider, “Resurrection and Liberation: An Evangelical Approach to Social
Justice,” in Robert Rankin, ed., The Recovery of Spirit in Higher Education: Christian
and Jewish Ministries in Campus Life (New York: The Seabury Press, 1980), pp.
1548,

4. Sider, “An Evangelical Theology of Liberation,” The Christian Century
(March 19, 1980), p. 318,
