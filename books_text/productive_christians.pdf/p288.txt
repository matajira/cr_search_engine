274 Productive Christians in an Age of Guili-Manipulators

Toward the end of his chilling study, Murray poses a question
each of us should consider thoughtfully: “Let us suppose that you,
a parent, could know that tomorrow your own child would be
made an orphan. You have a choice. You may put your child with
an extremely poor family, so poor that. your child will be badly
clothed and will indeed sometimes be hungry. But you also know
that the parents have worked hard all their lives, will make sure
your child goes to school and studies, and will teach your child
that independence is a primary value. Or you may put your child
with a family with parents who have never worked, whe will be
incapable of overseeing your child’s education—but who have
plenty of food and good clothes, provided by others. If the choice
about where one would put one’s own child is as clear to you as it
is to me, on what grounds does one justify support of a system
that, indirectly but without doubt, makes the other choice for
other children? The answer that ‘What we really want is a world
where that choice is not forced upon us’ is no answer, We have
tried to have it that way. We failed. Everything we know about
why we failed tells us that more of the same will not make the di-
lemma go away.”6

Murray’s powerful arguments and solid documentation can-
not simply be shrugged off. His work is causing “paradigm shifts”
everywhere: Even The New Republic, which certainly possesses
respectable leftist credentials, was forced to acknowledge that
Murray has dealt a deathblow to the standard liberal assumptions
about welfare. Late in 1984 the magazine published a review of
both Murray’s book and socialist Michael Harrington’s latest plea
for massive increases in social welfare spending.*! While the
reviewer could not quite bring himself to admit that Murray had
won the argument hands down, he did conclude that Murray's
findings had rendered Harrington’s arguments completely
untenable (where I come from, we call that “winning an argu-

60. Murray, p. 233.
61. Michael Harrington, The New American Poorly (New York: Holt, Rinehart
& Winston, 1984).
