The Prophetic Message . 195

not one whit of actual support from the prophet.?

Amos began his ministry around the middle of the eighth cen-
tury .c. Sent by God from the southern kingdom of Judah,
where he had been a shepherd and farmer, he went to the nor-
thern kingdom of Israel to warn them of approaching judgmient.
Israel at this time was in a period of great wealth and prosperity,
but it was also in the stage of “covenantal forgetfulness” spoken of
in Deuteronomy 8:11-20:

Beware lest you. forget the Lorp your God by not keeping His com-
mandments and His ordinances and His statutes which I am comman-
ding you today; lest, when you have eaten and are satisfied, and have
built good houses and lived in them, and when your herds and your
flocks multiply, and your silver and gold multiply, and all that you have
multiplies, then your heart becomes proud, and you forget the Lorp
your God who brought you out from the land of Egypt, out of the house
of slavery . . . otherwise, you may say in your heart, “My power and
the strength of my hand made me this wealth.” But you shall remember
the Lorp your God, for it is He who is giving you the power to make
wealth, that He may confirm His covenant which He swore to your
fathers, as it is this day. And it shall come about that if you ever forget
the Lorn your God, and go after other gods and serve them and worship
them, I testify against you that you shall surely perish. Like the nations
that the Lorp makes to perish before you, so you shall perish; because
you would not listen to the voice of the Lorp your God.

This is exactly what happened in the days of Amos. The Israel
of the eighth century had forgotten God asthe source of wealth,
As a result, both church and state were corrupt, the rich were op-
pressing the poor, the courts were controlled by bribes, and
political leaders were emphasizing military might over godliness
as the means of security. Society had become so perverse that the
people could no longer distinguish between good and evil. Amos
came to them with a message of the abiding authority of God’s

3. Some of the following material has been adapted from my “Studies in.
Amos” column, published in the Chalcedon Report (P. O. Box 158, Vallecito, CA
95251), from January through August 1980. Sce my forthcoming commentary
on Amos.
