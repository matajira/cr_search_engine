Déa Va—A Romp Through Ronald Sider’s Second Edition 281

simply a defender of the interests of the rich over against the other
classes? Usually, the very opposite is the case. The truth is, free-
market economists have more often than not found themselves at
odds with the interests of the wealthy and powerful. This is
because the wealthy often wish to maintain their position by using
the coercive power of the state to hamper the innovative activity of
the creative entrepreneurs below them. Adam Smith, the original
consumers’ advocate, observed: “People of the same trade seldom
meet together, even for merriment and diversion, but the conver-
sation ends in a conspiracy against the public, or in some contriv-
ance to raise prices.””1 As F. A. Hayek put it: “The main task of
those who believe in the basic principles of the capitalist system
tust frequently be to defend this system against the capitalists.”?2

The eighteenth-century version of many of Ronald Sider’s
statist theories and policies was known as mercantilism, a system
that worked (and still works, in its modern forms) to favor the in-
terests of powerful elites. Adam Smith tells us who invented it
(hint: it wasn’t the free-market economists): “It cannot be very
difficult to determine who have been the contrivers of this whole
mercantile system; not the consumers, we may believe, whose in-
terest has been entirely neglected; but the producers, whose in-
terest has been so carefully attended to; and among this latter
class our merchants and manufacturers have been by far the prin-
cipal architects.”74

The great twentieth-century economist Ludwig von Mises
emphatically agreed: “The rich, the owners of the already operat-
ing plants, have no particular class interest in the maintenance of
free competition. They are opposed to confiscation and expropria-
tion of their fortunes, but their vested interests are rather in favor
of measures preventing newcomers from challenging their posi-
tion,””#

71. Adam Smith, The Wealth of Nations, p. 128.

72, Friedrich A. Hayek, Studies in Philosophy, Politics, and Economics (Chicago:
University of Chicago Press, 1967), p. 192.

73. Smith, p. 626.

74. Mises, Human Action, p. 82; cf, pp. 268f., 808.
