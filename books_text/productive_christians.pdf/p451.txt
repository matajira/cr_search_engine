What Is the ICE? 437

Biblical Chronology is devoted to studies in ancient history,
with a view to helping lay foundations for Christian social theory
and historiography. Biblical Economics Today is a four-page
report that covers economic theory from a specifically Christian
point of view. It also deals with questions of economic policy.
Christian Reconstruction is more action-oriented, but it also
covers various aspects of Christian social theory. Covenant
Renewal explains the Biblical covenant and works out its implica-
tions for the three social institutions of culture: family, church and
state. Dispensationalism in Transition has its emphasis on
eschatology (doctrine of the endtimes). It challenges traditional
Dispensationalism’s “Code of Silence.”

The purpose of the ICE is to relate biblical ethics to Christian
activities in the field of economics. To cite the title of Francis
Schaeffer’s book, “Hrow should we then live?” How should we apply
biblical wisdom in the field of economics to our lives, our culture,
our civil government, and our businesses and callings?

If God calls men to responsible decision-making, then He
must have standards of righteousness that guide men in their decision-
making. It is the work of the ICE to discover, illuminate, explain,
and suggest applications of these guidelines in the field of econom-
ics. We publish the results of our findings in the newsletters.

The ICE sends out the newsletters free of charge. Anyone can sign up
for six months to receive them. This gives the reader the oppor-
tunity of seeing “what we're up to.” At the end of six months, he or
she can renew for another six months.

Donors receive a one-year subscription, This reduces the extra
trouble associated with sending out renewal notices, and it also
means less trouble for the subscriber.

There are also donors who pledge to pay $15 a month. They
are members of the ICE’s “Reconstruction Committee.” They help to
provide a predictable stream of income which finances the day-to-
day operations of the ICE. Then the donations from others can
finance special projects, such as the publication of a new book.

The basic service that ICE offers is education, We are present-
ing ideas and approaches to Christian ethical behavior that few
