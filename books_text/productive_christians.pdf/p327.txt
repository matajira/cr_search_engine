Déa Vu—A Romp Through Ronald Sider’s Second Edition 313

nomic system from Holy Scripture.”!7" Nobody seems willing to
say exactly what is so terrible about deriving an economic system
from Scripture, but all are agreed: It’s dangerous.

The Vice-Chairman of the Lay Commission, Michael Novak,
is an influential scholar whose writings (especially his more recent
ones) have done much to educate Roman Catholics and Protes-
tants alike about the workings of the market economy and the fail-
ures of socialism. Yet he, too, seems anxious to free himself from
the Bible’s oppressive authority. In his most well-known work on
economics Novak discusses, with marked disapproval, what he
calls “the attempt to Christianize the system.”!7! Ours is a “plural-
istic” society, Novak reminds us; thus any Bible-based “unity of
moral vision,” any attempt to deal with social issues in terms of
Christian values, is “inappropriate.”!72 What he says next is noth-
ing less than amazing: “Daily life is (as Christians believe) a con-
test with the world, the flesh, and the devil. An attempt to impose
the Kingdom of God upon this contest is dangerous not only to
human liberty but to Christianity itself (and to any other religion
similarly tempted).”17 I would think that the Kingdom of God
could be “dangerous” only to the world, the flesh, and the devil.
What concept of “human liberty” is it that demands freedom from
the rule of God? What shail we impose upon this contest, if not the
Kingdom of God? Is there a more “appropriate” kingdom?

Novak goes on to teli us that “Christian symbols [i-e.,
standards} ought not to be placed in the center of a pluralist society.
They must not be, out of reverence for the transcendent which
others approach in other ways.”!7* Yes, but what about the fact
that Christianity happens to be true? Where did Novak's “ought
not” and “must not” come from? Says who? How can any Christian

170, Toward the Future: Catholic Social Thought and the U.S, Economy (New York:
Lay Commission on Catholic Social Teaching and the U.S, Economy, 1984), pp.
inf.

171, Michael Novak, The Spirit of Democratic Capitalism (New York: Simon and
Schuster, 1982), pp. 674.

172. Ibid., p. 67,

173. p. 68,

174, Ibid., p. 70.

 
