What Is the ICE? 439

influences, are usually the slaves of some defunct economist. Madmen in
authority, who hear voices in the air, are distilling their frenzy from
some academic scribbler of.a few years back. I am sure that the power of
vested interests is vastly exaggerated compared with the gradual en-
croachment of ideas. Not, indeed, immediately, but after a certain inter-
val; for in the field of economic and political philosophy there are not
many who are influenced by new theories after they are twenty-five or
thirty years of age, so that the ideas which civil servants and politicians
and even agitators apply to current events are not likely to be the newest.
But, soon or late, it is ideas, not vested interests, which are dangerous
for good or evil.

Do you believe this? If so, then the program of long-term
education which the ICE has created should be of considerable in-
terest to you. What we need are people with a vested interest in ideas,
a commitment to principle rather than class position.

There will be few short-term, visible successes for the ICE’s
program. There will be new and interesting books. There will be
a constant stream of newsletters. There will be educational audio
and video tapes. But the world is not likely to beat a path to ICE's
door, as long as today’s policies of high taxes and statism have not
yet produced a catastrophe. We are investing in the future, for the
far side of humanism’s economic failure. This és a long-term invest-
ment in intellectual capital. Contact us at: ICE, Box 8000, Tyler,
TX 75711.
