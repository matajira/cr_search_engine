208 Productive Christians in an Age of Guilt-Manipulators

scarcely fought for victory but simply awaited the expression of
God’s judgment. His behavior at Stamford Bridge and Hastings
was utterly different. Both battles were equally long and equally
hard fought, between armies almost equally matched; but in the
first he was always in attack, and in the second never. In the first,
and in the whole episode of York, he undoubtedly inspired
everyone; but in the second, he left no evidence of leadership at
all. He acted like a different man: something had changed him in
the eighteen days between. . . . He was behind the line, and most
of the men in front, one can only suppose, stood facing death all
day without a word of encouragement or command from the King
they were fighting for.”*

Guilt produces passivity, and makes a man programmed for defeat.
The importance of this for totalitarianism cannot be overem-
phasized. If a whole society can be made to feel guilty, it will be
unable to withstand an enslaving state: it is ripe for conquest.
This has.long been recognized as the most successful method of
rendering men passive and pliable, incapable of resistance to
statist domination and control. A major aspect of the Communist
takeover of China was the manipulation of envy and guilt by the
organization of community discussions “around leading ques-
tions, such as, ‘Who has supported whom?’ and ‘Who has made
whom rich?’, and the encouragement of the aggrieved to ‘spit out
their bitterness’. . . . the Communist exploitation of grievances
was probably more systematic than anything in the past.””?

This is precisely what Sider is doing. He piles on the guilt fast
and thick: we are guilty for eating meat, sugar, fish, bananas; for
drinking wine and coffee; for making profits; for having extra
clothing in the closet; for having green lawns; even for living in
North America. He approves of the heretical, legalistic position
that anyone who lives on more than the bare necessities of life

1. David Howarth, 1066: The Year of the Conquest (New York: The Viking
Press, 1977), pp. 176ff.

2. John Meskill, An introduction to Chinese Civilization (Lexington: D. C. Heath
and Company, 1973), pp. 3244.
