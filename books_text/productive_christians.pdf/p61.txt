God's Law and the Poor 47

So much for solidarity.

God is firmly opposed to this activity. Not that our borders
shouldn't be protected against military invaders and criminals ~
but mere immigration is not a crime. Virtually all the activity of
the Immigration and Naturalization Service is thus in flagrant
violation of the law of God. God ‘tells us that He loves the
stranger, and commands us to love him also:

He . . . shows His love for the alien by giving him food and clothing. So
show your love for the alien . . . (Deuteronomy-10:18-19).

Note: in the Bible, ove is always action. It is defined here as pro-
viding strangers with what they need in order to live. Obviously,
then, it angers God if we abuse them, trouble them, or make life
hard for them. They are to receive the same justice in court as
native citizens: “There shall be one standard for you; it shall be
for the strangers as well as the native, for I am the Lorn your
God” (Leviticus 24:22); ‘You shall have one statute, both for the
alien and for the native of the land” (Numbers 9:14). Specifically,
any oppression of strangers is strictly. forbidden, and brings on.
divine judgment (Exodus 22:21-24).

This does not mean the abolition of all distinctions, however. It
does not constitute a legal mandate for integration. Indeed,
Israelites were permitted to sell diseased meat to strangers, since
pagan cultures generally have no objection to eating it (Deuteron-
omy 14:21). In addition, full citizenship in Israel was denied to
certain ethnic groups for three and sometimes ten generations
(Deuteronomy 23:3, 7-8), But while the Bible maintains a real-
istic appraisal of the often heathen backgrounds of immigrants, it
nevertheless commands justice, fair treatment, and positive con-
cern for their welfare. (For the advantages of population growth,
including immigration, see Chapter 7.)

What then should we do about illegal aliens? Gary North
makes the following suggestions: “First, require proof of immuni-
zation, or require those without proof to be immunized. Second,
abolish the minimum wage law. Third, abolish all public welfare
programs. Fourth, abolish the requirement that the children of il-
