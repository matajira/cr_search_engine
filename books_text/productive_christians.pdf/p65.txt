God's Law.and the Poor : 5t

above want; no amount of Factory Acts will make us better par-
ents, These great. wants which we are now vainly trying to deal
with by acts of Parliament, by prohibitions and penalties, are in
truth the great occasions of progress, if only we surmount them by
developing in ourselves more active. desires, by putting forth
greater efforts, by calling new moral forces into’existence, and by
perfecting our natural ability for acting together in voluntary
associations. To have our wants supplied from without by a huge
state machinery, to be regulated and inspected by great armies of
officials, who are themselves slaves of the system which they ad-
minister, will in the long run teach us nothing, will profit us noth-
ing.”*

“It is a mistake to suppose that government effort and individ-
ual effort can live side by side. The habits of mind which belong to
each one are so different that one must destroy the other... .
Men will not do things for themselves or for others if they once be-
lieve that such things can come without exertion on their own
part. There is not sufficient motive. As long as the hope endures
that the shoulders of some second person are available, who will
offer his own shoulders for the burden? It must also be remem-
bered that unless men are left to their own resources they-do not
know what is or is not possible for them. If government half a cen-
tury ago had provided us all with dinners and breakfasts, it would
be the practice of our orators today to assume the impossibility of
our providing for ourselves.”$

Thus, .as ‘we examine the biblical Poor Laws, we must con-
stantly remind ourselves of this central fact: The Bible commands re-
sponsibility. Apart from individual and familial responsibility,
these laws indeed will not work. Basic to social change and recon-
struction must be regeneration by the Holy Spirit. through the
propagation of the gospel, in.order that men and women newly
created in God’s image will begin to assume their responsibilities
under God. With this in mind, we may turn our attention to the
Poor Laws of the Bible.

4, Auberon Herbert, The Right and Wrong of Compulsion by the State (Indian-
apolis: Liberty Classics, 1978), pp. 179f.
5, ibid, p. 77.
