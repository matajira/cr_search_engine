22 Productive Christians in an Age of Guilt-Manipulators

some external standard of justice; for God, there és no external
standard — He is the standard. He “knows” the difference between
good and evil by simply determining it. His law alone is the yard-
stick of right and wrong. And that was the privilege coveted by
Adam and Eve. They wanted to know good and evil, not by sub-
mitting to the external standard of justice provided by God’s com-
mands, but by usurping the prerogatives of Deity, determining
for themselves the difference between right and wrong. As the
Apostle John succinctly stated it: “Sin is dawlessness” (1 John 3:4).

“Wait a minute,” you say. “Isn’t that legalism? Didn’t Jesus
and the apostles declare that we are free from all those Old Testa-
ment regulations?” It wouldn't really be fair to reply that the
strictest adherence to Old Testament law allows for much more
freedom than do the more “enlightened” stipulations of our bene-
volent despots in the federal bureaucracy. That issue will be tem-
porarily shelved, and I'll answer your questions directly: NO.

Let’s begin with a working definition of legalism, Legalism can-
not be defined simply as rigorous obedience to the law: after all,
Jesus Christ obeyed the law fully, in its most exacting details —
and He, certainly, was no legalist. The true legalist is the person
who subscribes to one or more of the following heresies—ideas
which are roundly condemned in Scripture:

1) Justification by works. This is the most critical aspect of the
legalistic faith. It was abhorred and refuted by.the writers of both
Old and New Testaments. We must note here that no one—not
even in the days of Moses—was ever justified by his works. The
only basis of salvation is the finished work of Jesus Christ, in fully
satisfying the demands of God’s law, and suffering its penalties, in
the place of all His people. The view that God accepts us as His
children because of our works is completely at odds with the
teachings of Scripture. One who is a legalist in this sense is cer-
tainly not an orthodox Christian.

2) The requirement of obedience to Old Testament ceremonial laws. Be-
fore Christ came, God’s people were required to observe certain
ceremonies — sacrifices, feasts, and so’ forth—which symbolically
portrayed the way of restoration to God’s favor. These received
