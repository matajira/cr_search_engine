Subject Index

Demand. See Supply and demand

Denton, Sen. Jeremiah, 363

Depression, 381

Destructionism, 36, 76, 120, 130,
139-50, 174, 191, 264-65, 280, 302,
341, 345-46, 383-84

Diehl, William. E., 310, 364

Disarmament, 283-84

“Distribution,” 279-80. See also
Redistribution

Division of labor, 116-17, 382

Dolcino, Fra, 326-27

Dallinger, Johann von, 334-35

Dominion, 35-38, 61, 92-96, 161-62,
230-45, 266n, 382

Donatists, 333

Doormat psychology, 351

Doublespeak, 288n

Drucker, Peter F., 278-79

Dubos, René, 119n, 303n

Eco, Umberto, 321, 327
Economic calculation, 94, 293-94
Economics, 10, 19-21, 33-43, 89-92,
101-109, 116-21, 124-30, 181-89,
217-22, 226-28, 309-17, 382
Education, 271, 272, 273
Egalitarianism, 383. See also Equality
Elites, 281-82
El Salvador, 287
Emerson, Ralph Waldo, 74-75
Engels, Friedrich, 268n, 308, 329-30,
331, 339
Enlightenment, 180, 251
Entrepreneur 125-29, 157-58, 280,
293, 383
Envy, 9, 12, 43, 83, 123, 126, 130,
139-50, 172-74, 194, 201, 206, 208,
223, 243, 285-86, 326, 329, 337,
383-84, 386
and manipulation, 144, 206-07, 208,
305-06, 329
and socialism, 123, 130, 140, 142

425

cure for, 149 .

Equality, 15, 154, 157-59, 162, 165-75,
326, 329, 330, 331, 334-35, 336, 337

Erasmus, Desiderius, 142

Ethics and Public Policy Center, 367

Ethiopia, xii-xiii, 367-75

Evangelicals for Social Action (ESA),
6-7, 70-71, 178, 206

Exchange, 38-40, 182-83, 186, 382,
384

Exclamation points, 253-54

"Facial justice,” 177, 337

Faith, loss of, 347-49

Famine, xii-xiii, 92-95, 118-21, 202-03,
373-75

Fascism, 263, 310

Federal Reserve System, 32, 134,
198, 384, 385

Fellowship see Communion

Ferguson, Adam, 293

Fiat money, 384-85

Fickett, Harold, 250n

Finney, Charles G., 72-75

Flynt, Larry, 261

Foolishness, 15-16, 190

Ford, Henry, 126

Foreign aid, 35, 90-92, 99-110, 194,
201, 297-301
implications, 105-07
results, 107-09
multinational (multilateral), 300-01

Fossedal, Gregory A., 285n

Fourier, Charles, 339

Fractional reserve banking, 41-42,
197-98, 379-80, 385, 386-87

Freedom, 8, 71, 295-96

Free enterprise, 7-8, 38-40, 94, 101-05,
178-79, 182-83, 186, 218-19, 220,
223, 226-27, 264-65, 312-13, 350,
352, 368, 374, 375, 379, 387
vs. “capitalism,” 280-82

Freeman, Roger, 268-69
