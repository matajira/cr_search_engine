WHAT IS THE ICE?

by Gary North, President, ICE

The. Institute for Christian Economics is a non-profit, tax-
exempt educational organization which is devoted to research and
publishing in the field of Christian ethics. The perspective of those
associated with the ICE is straightforwardly conservative and pro-
free market. The ICE is dedicated to the proposition that biblical
ethics requires full personal responsibility, and this responsible
human action flourishes most productively within a framework of
limited government, political decentralization, and minimum in-
terference with the economy by the civil government.

For well over half a century, the loudest voices favoring Chris-
tian social action have been outspokenly pro-government inter-
vention. Anyone needing proof of this statement needs to read Dr.
Gregg Singer’s comprehensive study, The Unholy Alliance (Arling-
ton House Books, 1975), the definitive history of the National
Council of Churches. An important policy statement from the
National Council’s General Board in 1967 called for comprehensive
economic planning, The ICE was established in order to challenge
statements like the following:

Accompanying this growing diversity in the structures of national life
has been a growing recognition of the importance of competerit planning
within and among all resource sectors of the society: education, economic
development, land use, social health services, the family system and con-
gregational life. It is not generally recognized that an effective approach
to problem solving requires 2 comprehensive planning process and co-
ordination in the development of all these resource areas.

433
