118 Productive Christians in an Age of Guili-Manipulators

for it—since “population control is people control,” and statist
rebels, abandoning their rightful dominion over the earth, seek in-
stead domination over men. Weber convincingly argues that—
provided the spiritual conditions for liberty exist in the first place
—high population density acts to retard the centralization of po-
litical power, and promotes freedom from state control.!? Would-
be totalitarians thus have a very definite incentive to control pop-
ulation growth, and it is to this fact that we must now turn.

Overpopulation and Statism

The foregoing discussion is not meant to imply that there is no
such thing as overpopulation (although much of the rhetoric on
the subject is long on discourse and short on evidence). A defini-
tion would help. Overpopulation in any meaningful sense must
refer to a situation in which sufficient food is not available for the
total number of people. Thus, “overpopulation” cannot refer to
any absolute number of people, but only to the number of people
relative to the food supply. In a word, what we're talking about is
famine. If you have ten people stranded on an island with only
enough food to sustain one life, you’ve got overpopulation. Before
the arrival of the Europeans, North America was “overpopulated,”
since the Indians were not able to produce sufficient food for them-
selves—and, at most, there were only a few hundred thousand of
them.1! Yet the same continent, with the same natural resources,
now supports hundreds of millions. What made the difference?
Christianity. Pagan cultures, with a slave mentality, routinely see
themselves as at the mercy of their environment, and are thus

10. Weber, pp. 135ff,; cf. P, T. Bauer, Equality, the Third World, and Economic
Development (Cambridge, MA: Harvard University Press, 1981), pp, 42-65. The
most informative work to date on the advantages of population growth is Julian
L. Simon, The Ultimate Resource (Princeton, NJ: Princeton University Press,
1981). See also Gary North’s essay “Population Growth: Tool of Dominion’ in his
recent book Moses and Pharaoh: Dominion Religion Versus Power Religion (Tyler, TX:
Institute for Christian Economics, 1985), pp. 11-27; cf. the presentations by
Bauer, Simon, and others in “Are World Population Trends a Problem?” (pub-
lished by the American Enterprise Institute, 1984).

11. R. J. Rushdoony, The Myth of Overpopulation (Fairfax, Virginia: Thoburn
Press, [1969] 1973), pp. 1ff.
