Socialism, the Anabaptist Heresy 335

Cathars and Albigenses, who provoked the severe and implacable
medieval laws against heresies by their activities, and with whom
a bloody struggle was carried on, were socialist and. communist.
They attacked marriage, the family and property, Had they been
victorious, the result would have been a traumatic social disloca-
tion and.a relapse into barbarism.”**

But they were not victorious. They failed. Socialism parading
as radical Christianity was shown to bea pious-sounding fraud.
Orthodoxy had demonstrated that there can never be any such thing as
“Christian Socialism,” because socialism is antichrist. And so the tactics
changed. Socialism went secular, and it went underground as
well, dropping the theological approach and turning to an
avowedly autonomous, philosophical rationale instead.

It is striking that the two great opponents of that era— Reformed.
orthodoxy and the Anabaptist heresy —resurfaced in our age at the
same time. In 1973 (the year of the socialistic, blueprint-denying
Humanist. Manifesto ID, Ronald Sider and his Anabaptist/socialist
colleagues (at least some of whom, at this writing, are still
pacifists) issued the Chicago Declaration of Evangelical Social Concer,
which brought a forthright demand for Christian socialism to the
attention of Christians across the country. In the very same year,
two Reformed works were published which will mean the even-
tual defeat of Christian socialism in our day as well: R. J. Rush-
doony’s Institutes of Biblical Law*’ and Gary North’s Introduction to
Christian Economics.4° Just as, according to tradition, Pelagius and
Augustine were born in the same year (354), so God again has
brought the poison and its antidote into the world simultaneously,

Philosophical Socialism
After the Reformation, the socialists abandoned all pretense to
biblical justification whatever. “The preacher and the wandering

44. Quoted in ibid., p. 77.

45, Rousas John Rushdoony, The Institutes of Biblical Law (Nutley, NJ: The
Craig Press, 1973).

46. Gary North, An Introduction to Christian Economics (Nutley, NJ: The Craig
Press, 1973).
