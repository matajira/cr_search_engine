Biblical Law and Christian Economics 25

basic motive of legalism: antinomianism. The antinomian is op~
posed to the authority of God in human affairs. While he may
cloak his humanism in a garb of extreme religiosity (as did the
Pharisees) or “radical Christianity,” his primary goal is to abolish
God’s law and replace it with his own laws. He wants to be “like
God, knowing good and evil.” On the surface, antinomianism
and legalism appear to be diametrically opposed; in reality, they
are both rooted in the sinful atternpt to dethrone God,

Law and the New Testament

What does the New Testament say about the validity of the Old
Testament law? It is often assumed that Christ’s death and resur-
rection have freed us from any obligation to keep the law and, in
the sense of justification, that is certainly true. Because of Christ’s
finished work, no believer is under the law’s condemnation: our
obligations have been fully met by our Lord. But that was true of
Old Testament believers as well. Abraham and. David, for in-
stance, were justified by God’s grace through faith, just as we are
(Romans 4:1-8), Therefore, justification by faith cannot be claimed
as a basis for rejecting the law’s demands, since the Old Testa-
ment believers, who were told to keep the law, were justified by
faith as much as we are.

The Teaching of Christ

Any examination of the New Testament teaching on law must
begin with Jesus Christ’s declaration of His relationship to the
law, and the law’s continuing requirements of His disciples:

Do not think that I came to abolish the Law or the Prophets; I did not
come to abolish, but to fulfill. For truly I say to you, until heaven and
earth pass away, not the smallest letter or stroke shall pass away from the
Law, until all is accomplished. Whoever then annuls one of the least of
these commandments, and so teaches others, shall be called least in the
kingdom of heaven; but whoever keeps and teaches them, he shall be
called great in the kingdom of heaven. For I say to you, that unless your
righteousness surpasses that of the scribes and pharisees, you shall not
