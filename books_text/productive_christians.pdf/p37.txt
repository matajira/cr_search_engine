Biblical Law and Christian Economics 23

their completion in Jesus Christ, and are no longer literally bind-
ing upon us, There is a very real sense, of course, in which we still
keep these laws: Jesus Christ is our priest, He is our sacrificial
atonement, and we cannot approach God apart from Him. Thus,
in their real meaning, all these laws are observed by all Christians.
But consider what a literal observance of these laws would mean,
now that Christ has fulfilled these shadows: if you were to sacrifice
a lamb today, you would be saying, in effect, that Christ’s atone-
ment on the cross was insufficient—that you need an additional
sacrifice to be accepted with God. That is heresy. Before the com-
ing of Christ, observance of the ceremonial law was obedience; after
His death and resurrection, it is disobedience. The false teachers op-
posed by Paul in Galatians held to both of these two aspects of le-
galism—salvation by works and the requirement of Old Testament
ceremonies.

3) A third form of legalism is addressed in Romans 14 and
Colossians 2: The requirement of obedience to man-made regulations. The
Galatian legalists at least may be commended for their insistence
upon biblical regulations. They were very wrong, but their stand-
ards were derived from Scripture. But Paul also had to contend
with a host of regulations which originated from mere human pre-
jedice, and which some Christians attempted to impose upon
others. “Touch not; taste not; handle not,” they demanded~
when God had said nothing of the kind. There are many matters
of individual conscience, taste, and idiosyncrasy which should re-
main so. But we are all dictators at heart, and we often like
nothing better than to force others to submit to our eccentricities.
It is in this sense that Ronald Sider is a legalist. He comes very
close— without going over the brink in many of his actual state-
ments~to making requirements out of all sorts of non-biblical
standards. According to him, Christians should “live simply,” eat
less meat and no bananas, oppose production of liquor, and give
away all income above what is required for bare necessities. Does
the Bible say one word about any of this? No—which is not to
suggest that we must be heavy meat and banana eaters (since that
isn’t commanded either). The point is that we must never uphold
