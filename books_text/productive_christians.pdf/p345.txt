Socialism, the Anabaptist Heresy 331

several nobles to his side). The rebellion was eventually put down
and Miintzer was executed; Luther said, “Whoever has seen
Mintzer can say that he has seen the devil in the flesh, at his most
ferocious.”5° That was before Luther saw Jan Bokelson*!—better
known to history as Johann (or John) of Leyden.

Bokelson began his career as the disciple of the Anabaptist
leader Jan Matthijs (or Matthys), who took over the town of
Minster in 1534. Shafarevich describes'the scene: “Armed Ana-
baptists broke into houses and drove out everyone who was un-
willing to accept second baptism. Winter was drawing to a close;
it was a stormy day and wet snow was falling. An eyewitness ac-
count describes crowds of expelled citizens walking through the
knee-deep snow. They had not been allowed even to take warm
clothing with them, women carrying children in their arms, old
men leaning on staffs. At the city gate they were robbed once
miore.”32

But those were the lucky ones, They, at least, escaped the
reign of terror which followed, as Matthijs and Bokelson ordered
the socialization of all property and ordained apostles of revolu-
tion to preach throughout Europe. The communist paradise of
Minster attracted thousands of armed Anabaptists from Ger-
many and Holland, and eventually a war broke out between the
Minster rebels and the surrounding cities. Matthijs was killed in
one of the early battles, and Bokelson took command. He estab-
lished a dictatorship (in the name of equality), and issued an
order for what was by now a standard Anabaptist/socialist tradi-
tion: Polygamy (or, more technically, wife-sharing; as Frederick
Engels observed, “It is a curious fact that in every large revolu-
tionary movement the question of ‘free love’ comes to the fore-
ground”33), No woman was allowed to be exempt, either —there
was a law against being unmarried, which meant that every girl of

30. Ibid., p. 59.

31. His last name is spelled “Bockelson” in Cohn’s account, which deals with
him and his movement at length (pp. 261-80).

82. Shafarevich, The Socialist Phenomenon, p. 61.

33, Ibid., p. 33.
