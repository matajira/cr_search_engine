Is God on the Side of the Poor? 85

and examines him” (Proverbs 18:17). The issue in justice is not
Which one is the underdog? but Which one is right? And the standard of
justice is not relative wealth or poverty, but the abiding law of Gad.
When God does “reverse the good fortune of the rich,” in judg-
ment, it is because of sin. And since the rich can be tempted to
forget God, and to trust in their own might, there is always the
danger of sneering at and oppressing those below them. God hates
this. It is He who gives the power to get wealth; oppression of
others is a denial of the divine origin of riches. But oppression of the
poor is wrong, not because God “sides” with them any more than
with others. It is wrong to oppress anyone. You are commanded to
return even your enemy’s lost possessions to him (Exodus 23:4-5).
Let's suppose, for example, that you have an enemy who has truly
wronged you. Still, you may not pervert the justice due him. You
may not like him; but you must observe the rights God has established.
For a more extreme example, let us suppose that a really worth-
less, lazy bum has committed a rape. Biblical law commands that he
be executed; but it also commands us to give him a fair trial, with
proper evidence, regardless of how much we may (rightfully) despise
him. And if the evidence is insufficient to convict him biblically, he
moust go free. In a word: justice, We won't have perfect justice in this
fallen world, not ever. Some criminals will undoubtedly go unpun-
ished for lack of evidence. But we can have swift, substantial justice,
and we should work diligently to that end. The biblical standards for
treatment of the poor acknowledge the fact that we men are sinners,
and that we tend to look down at those who are less well-off, or a bit
more well-off, than we are. So the Bible reminds us repeatedly to
abide by God’s strict canons of justice, Fairness to our fellow
men—even if they have done wrong—is the biblical mandate,
God is not “on the side” of the poor. He demands that we treat
them according to His law. If we oppress them, He will punish us;
and if they call upon Him, He will hear and deliver them.
Whose side is Ged on? Not the rich; not the poor; not any social
or economic class; not any race. The answer to the question can
be easily determined when we answer a much more important
question, posed by Moses in Exodus 32:26 (KJV):
“Who is on the Lorn’s side?”
