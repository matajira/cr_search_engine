Dé&ja Vu—A Romp Through Ronald Sider’s Second Edition 299

policy of official aid are inseparable. The one would not exist
without the other. The Third World is merely a name for the col-
lection of countries whose governments, with occasional and odd
exceptions, demand and receive official aid from the West... .
Thus, The Third world is a political and not an economic con-
cept. . . ..The Third World or the South is simply an entity for
engaging in collective bargaining with the West... . The pur-
pose: of the Third World qua collectivity is to coax or extract
money from the West.”126

Much of Lord Bauer's research over the years has covered the
disastrous — necessarily disastrous — effects that foreign aid has had
on Third World countries. As he shows, the use of the term aid to
describe international tranfers of funds considerably prejudges
the issue.

Perhaps the most crucial thing to remember about foreign aid
is that it emphatically does zot go to those pitiful, emaciated kids
on the posters. Foreign aid goes to governments. Period. This means
that the actual function of aid is to increase the powers of govern-
ment over the populace. Professor Sider and his ilk to the contrary
notwithstanding, propping up totalitarian dictators is not by any
means the same thing as helping the poor. In fact, foreign aid en-
courages recipient governments to pursue irresponsible and
destructive policies in the certain knowledge that when things go
wrong, the West will bail them out again.?2” In terms of its alleged
purpose — the alleviation of poverty— foreign aid is counterproduc-
tive, since “to support rulers on the basis of the poverty of their sub-
jects does nothing to discourage policies of impoverishment,”

One rationale for aid has been that if we don’t give it to the
Third World, the Soviets will; thus, if we want to keep the Third
World out of the Soviet camp, we must send money to their gov-

126. Bauer, Realily and Rhetoric, pp. 408.

127. The shocking story of the catastrophic effects of foreign aid programs in
Africa—“aid? which actually produces famine and genocide—is told in Karl
Borgin and Kathleen Corbett, The Destruction of a Continent: Africa and International
Aid (New York: Harcourt Brace Jovanovich, 1982).

128, Bauer, Reality and Rhetoric, p. 50.
