Appendix 4

THE BACKGROUND OF
“PRODUCTIVE CHRISTIANS”
by Gary North

Perhaps the most striking feature of the picture [of the shift in perspec-
tive.of capitalists: from the long-run, multi-generation view of the founder of a fam-
ily business to a short-run, one-lifetime outlook of the corporation man] is the ex-
tent to which the bourgeoisie, besides educating its own enemies, allows
itself in turn to be educated by them, It absorbs the slogans of current ra-
dicalism and seems quite willing to undergo a process of conversion te a
creed hostile to its very existence. Haltingly and grudgingly it concedes
in ‘part the implications. of that creed. This would be most astonishing
and indeed very hard to explain were it not for the fact that the typical
bourgeoisie is rapidly losing faith in his own créed.. . . They talk and
plead—or hire people to do it for them; they snatch at every chance of
compromise; they are ever ready to give in; they never put up a fight
under the flag .of their own ideals. . . . The only explanation for the
meekness we observe is that the bourgeois order no longer makes any
sense to the bourgeoisie itself and that, when all is said and nothing is
done, it does not really care.

Joseph Schumpeter (1942)!

1, Joseph Schumpeter, Capitalism, Socialism and Democracy (3rd ed., New York:
Harper Torchbéok, [1950] 1962), p. 16!. Schumpeter was a distinguished pro-
fessor of economics at Harvard University until his death in 1950. He was a con-
temporary of Ludwig von Mises, and in fact was an intellectual rival. Both
studied under one of the founders of the “Austrian School of economics,” Eugen
von Béhm-Bawerk, in the early 1900's, He was nota socialist, but he believed
that socialism was winning by default—-not because of the economic “failure” of
capitalism, but, on the contrary, because of its incomparable economic success,
See his essay, completed just before he died, “The March into Socialism” (1950),
reprinted as an appendix in the third edition of Capitalism, Socialism and Democracy.

347
