God's Law and the Poor 61

government intervention in all of life.

Since slavery will always exist, the biblical answer is not to try
to abolish it, but to follow God’s laws for slavery.!* While many of
these laws may seem harsh, we must recognize that, first, these
laws are remedies for irresponsibility, and seek to drive men out
of slavery; and second, the laws of God are not nearly so harsh as
the laws of men, While God’s law produces a responsible, stable
social order, man’s slavery laws are chaotic, oppressive and
tyrannical. The biblical worldview is not a fairy-tale, or romantic
perfectionism, but a realistic appraisal of men with their sins
and shortcomings. God’s word meets us where we are in our
slavery, and shows us the way toward responsible dominion
under God.

1. Obtaining slaves. Kidnapping is forbidden as a method of ac-
quiring slaves, and deserves capital punishment (Exodus 21:16).
Basically, there are only four legal ways to get slaves. They may
be purchased (Leviticus 25:44-46), captured in war (Numbers
31:32-35; Deuteronomy 21:10-14), enslaved as punishment for
theft (Exodus 22:1-3), or enslaved to pay off debts (Leviticus
25:39; Exodus 21:7). We should especially note God’s merciful
justice here. Heathen slaves who were purchased or captured in
war were actually favored by this law, since it placed them in con-
tact with believers. They received the relatively lenient treatment
of the biblical slavery regulations, and they were also able to hear
the liberating message of the gospel. Slaves making restitution for
theft or debt were also benefitted by this law. The Bible does. not
allow imprisonment (except for a man-held for trial or execution).
The thief was not caged up at taxpayers’ expense and treated like
an animal; he labored productively, in an evangelical family con-
text, and made proper restitution to the victim for his crime. He
earned back his self-respect, and restored what he owed to his vic-
tim (If those who so fervently desire “social justice” wouldn’t

14, The best exposition of the biblical slavery laws is in James.B. Jordan, The
Law of the Covenant: An Exposition of Exadus 21-23 (Tyler, TX: Institute for Chris-
tian Economics, 1984), pp. 7-92; see also Jordan’s Slavery and Liberation in the
Bibiz (Tyler, TX: Institute for Christian Economics, forthcoming).
