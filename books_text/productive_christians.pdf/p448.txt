434 Productive Christians in an Age of Guilt-Manipulators

The silence from the conservative denominations in response to
such policy proposals has been deafening. Not that conservative
church members agree with such nonsense; they don’t. But the
conservative denominations and associations have remained
silent because they have convinced themselves that any policy
statement of any sort regarding social and economic life is always
illegitimate. In short, there is no such thing as a correct, valid
policy statement that a church or denomination can make. The
results of this opinion have been universally devastating. The popular
press assumes that the radicals who do speak out in the name of
Christ are representative of the membership (or at least the press
goes along with the illusion). The public is convinced that to
speak out.on social matters in the name of Christ is to be radical.
Christians are losing by default.

The ICE is convinced that conservative Christians must devote
resources to create alternative proposals. There is an old rule of
political life which argues that “You can’t beat something with
nothing.” We agree. It is not enough to ‘adopt a whining negativism
whenever someone or some group comes up with another nutty
economic program. We need a comprehensive alternative.

Society or State

Society is broader than politics. The State is not a substitute
for society. Society encompasses all social institutions. church, State,
family, economy, kinship groups, voluntary clubs and associ-
ations, schools, and non-profit educational organizations (such as
ICE). Can we say that there are no standards of righteous-
ness—justice —for these social institutions? Are they lawless? The
Bible says no. We do not live in a lawless universe. But this does
not mean that the State is the source of all law. On the contrary,
God, not the imitation god of the State, is the source,

Christianity is innately decentralist. From the beginning, orthedox
Christians have denied the divinity of the State. This is why the Caesars
of Rome had them persecuted and executed. They denied the
operating presupposition of the ancient world, namely, the legiti-
macy of a divine ruler or a divine State.
