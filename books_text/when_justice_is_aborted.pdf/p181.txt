Resources 163

Operation Blessing
CBN Center
Virginia Beach, VA 23463

Orthodox Christians for Life
P. O. Box 805
Melville, NY 11747

Pro-Life Action League
6160 North Cicero Avenue
Chicago, IL 60646

Pro-Life Action Ministries
611 South Snelling Avenue
St. Paul, MN 55116

Rutherford Institute
P.O. Box 510
Manassas, VA 22110

Sex Respect
P.O. Box 349
Bradley, IL 60915

Why Wait?
P.O. Box 1000
Dallas, TX 75221

Women Exploited (WE)
2100 West Ainsley
Chicago, IL 60640

Women Exploited By Abortion
(WEBA)

202 South Andrews

Three Rivers, MI 49093

A number of organizations specialize in distributing pro-life
books, tracts, films, and slide presentations. Again, the following
list is by no means comprehensive, but it should point you in the

right direction.

American Portrait Films
1695 West Crescent Avenue,
Suite 500

Anaheim, CA 92801

Catholics United for Life
(CUL)
New Hope, KY 40052

Christian WorldView (CWV)
P. O. Box 185066
Fort Worth, TX 76181-0066

Couple to Couple League
3621 Clenmore Avenue
Cincinnati, OH 45211

Crossway Books
9825 West Roosevelt Road
Westchester, IL 60153

Dominion Press
P. O.Box 8204
Fort Worth, TX 76124
