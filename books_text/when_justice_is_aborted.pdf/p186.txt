168 When Justice Is Aborted

Biblical Law
Bahnsen, Greg L. By This Standard: The Authority of God's Law
Today. Tyler, Texas: Institute for Christian Economics, 1985.

—————-— « Theonomy in Christian Ethics. 2nd ed.; Phillipsburg,
New Jersey: Presbyterian & Reformed, 1984.

Rushdoony, Rousas John. The Lnstitutes of Biblical Law. Phillipsburg,
New Jersey: Presbyterian & Reformed, 1973.

Rutherford, Samuel. Lex, Rex; Or, The Law and the Prince. Harri-
sonburg, Virginia: Sprinkle Publications, 1982. Reprint of 1827
edition. Originally published in 1644.

Christian Resistance
Calvin, John. Institutes of the Christian Religion. Book IV, Chapter
20. 1559 edition. Philadelphia: Westminster Press, 1960.

North, Gary. Backward, Christian Soldiers? Tyler, Texas: Institute
for Christian Economics, 1984

—_.___, editor. Tactics of Christian Resistance. Christianity and
Civilization, 3. Tyler, Texas: Geneva Ministries, 1983.

—_-—..... , editor. The Theology of Christian Resistance. Christian-
ity and Civilization, 2. Tyler, Texas: Geneva Ministries, 1983.

Schaeffer, Francis. A Christian Manifesto. Westchester, Ellinois: Cross-
way, 1981.

Stauffer, Ethelbert. Christ and the Caesars. Philadelphia: Westmin-
ster Press, 1955.
Christian Revolution

“Junius Brutus.” A Defence of Liberty Against Tyrants, a translation
of Vindiciae Contra Tyrannos. Gloucester, Massachusetts: Peter
Smith, 1963, Originally published in 1579.
