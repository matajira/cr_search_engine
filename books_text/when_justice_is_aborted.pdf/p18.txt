xviii When Justice Is Aborted

Christian resistance, were forgotten. Something else was needed,
something shorter, cheaper, and easier to read.

I started the manuscript of this book on October 29, a Satur-
day morning, took Sunday off, and finished the first draft on the
following Wednesday. I sent photocopies by overnight mail to
several Christian leaders. Two leaders then suggested that I write
an appendix refuting published criticisms of Operation Rescue,
which IT did the following week. That took an extra day. I spent
another day tinkering with the manuscript to prepare a final draft.
So, this book was basically a one-week operation. That I could do
this in one week is a testimony to the power of the biblical covenant
model, the Word Perfect 4.2 word processing program, and the
Godspeed computerized Bible search program.

Once you understand the Bible’s five-point covenant model,
you can solve lots of intellectual, moral, and judicial problems
fairly easily. In fact, once you memorize this model, you will
recognize it again and again as you read the Bible. Much of the
Bible is structured in terms of this model. Once it gets into your
mind, it does not get out. Without this model, biblical solutions
are far more difficult to come by. So, I strongly suggest that you
take this five-point model seriously, keeping it in mind as you read
your Bible, and turning to it whenever you are called upon to
defend what you are doing in the name of Jesus Christ.
