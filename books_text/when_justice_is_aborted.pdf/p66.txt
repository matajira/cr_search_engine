48 When Justice Is Aborted

doctrines of devils; speaking lies in hypocrisy; having their con-
science seared with a hot iron” (I Timothy 4:1-2).

Christians do not take these words literally, of course. We do
not believe that a literal hot iron can sear a person’s conscience.
Paul was using a metaphor. A bleeding wound can be sealed up
by applying a hot iron to it, but the nerve endings beneath the skin
may be permanently destroyed. The person later may lose all
feeling on the seared portion of his flesh. So it is with sin. If false
doctrines or evil acts are indulged in, they can sear the conscience.
No longer will the individual hear the warning voice of God.
Again, this is not a literal voice. The conscience is representatively
the voice of God, but it is nonetheless conscience, not literally a
voice.

Self-Government Under Church Authority

God has ordained the church as His lawful monopoly that
governs the distribution (and therefore the withholding) of the
sacraments, Like any God-ordained sovereign government, the
church is run hierarchically. There is a chain of command. This
is not a top-down bureaucratic chain of command. God has estab-
lished a system of multiple bottom-up appeals courts: church,
state, and family. This appeals court structure is seen in the civil
government’s court procedure described in Exodus 18 and also in
the church government's court procedure described in Matthew
18:15-18.

In contrast to God’s appeals court hierarchy, Satan runs a
top-down chain of command. In a bureaucracy, the leader issues
orders that must be obeyed. He runs the bureaucracy the way a
general runs an army during a war. But a general during a war is
made personally responsible for the success or failure of the life-and-
death military operations. This level of personal responsibility
does not prevail in peacetime, when civilian rule again becomes
the standard. Also, the focus of a war is narrow: military victory.
This concentration of national and personal focus is narrow, unlike
a peacetime society. Thus, a bureaucratic approach to institutional
