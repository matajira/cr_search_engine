46 When Justice Is Aborted

Self-Government Directly Under God

Each person is. responsible before God for everything he says
and does in his lifetime. Jesus warned us: “But I say unto you,
That every idle word that men shall speak, they shall give account
thereof in the day of judgment” (Matthew 12:36). Thus, a person’s
conscience is a lawful authority. The fundamental rule of govern-
ment is self-government under God’s law. The primary enforcing agent
is the conscience. No other human government possesses the God-
given authority or the God-given resources to police every aspect
of each person’s daily walk before God. Any government that
attempts this is inherently tyrannical.

When a person faces God on judgment day, there will be no
committee beside him to “take the rap.” Only Jesus Christ can
do this for a person, as God’s lawful authorized authority who died
in place of a God-redeemed individual. There will be no one else
except Jesus Christ at the throne of judgment who can lawfully
intervene and tell God the Judge, “This person was following my
orders, and therefore should not be prosecuted.”

Therefore, the fundamental representative voice of God’s author-
ity in each person’s life is his own conscience. Because the individ-
ual will face.God on judgment day, the fundamental form of
human government is self-government. This is basic to Christian
ethical, social, and legal theory. Any society that attempts to deny
this principle of justice is in revolt against God.

This is not to say that a person’s conscience is absolutely
sovereign. There has been no single, God-authorized human voice
of absolute authority on earth since the ascension of Jesus Christ
to the right hand of God. The conscience is a person’s primary
voice of authority, but a wise person will defer to other God-
ordained human authorities. The Bible is clear about this. There is
@ division of labor in every area of life, including the proper interpreting of
God's law. The church of Jesus Christ is a body with many mem-
bers (Romans 12; I Corinthians 12). Paul in Ephesians writes:

And he gave some, apostles; and some, prophets; and some,
evangelists; and some, pastors and teachers; For the perfecting of
