52 When Justice Is Aborted

supposed to impose negative sanctions against evil behavior. The
State is not an agency of personal salvation. It is not supposed to save
men; it is to protect them from the evil acts of other men.

The individual is supposed to possess the God-given legal right
to remove himself from the jurisdiction of any civil government
that he believes to be immoral. Because civil governments rule
over geographical areas, the act of renouncing jurisdiction is nor-
mally accomplished through personal emigration. Until World
War I, the right of legal emigration out of a nation and almost
universal immigration into a-nation were honored im Europe and
North America. Very few nations required passports.

Because of the difficulty of moving, especially prior to the
invention of the steam engine (ships and trains), God has estab-
lished other means of renouncing jurisdiction. One of these is the
right of revolution. This right is lawful only when conducted by
lesser magistrates who have been raised up by God to challenge
immoral rulers. The Book of Judges deals with this right of revolu-
tion by lesser magistrates and national leaders who revolt against
foreign invaders who have established long-term rule.

Legitimate Deception of Unjust Rulers

Another of these God-given alternatives to departing physi-
cally is the right of civil disobedience, Men refuse to obey unjust
laws. The obvious biblical example of this is the revolt of the
Hebrew midwives against Pharaoh. They refused to carry out his
order to kill all the male babies. They lied to him about the
extra-rapid delivery of Hebrew women (Ex. 1:19), a lie so obvi-
ously preposterous that only a man blinded by God could have
believed it. After all, if the wives were delivered so rapidly, of what
possible use could a midwife be? There could be no such thing as
a midwife. Then God blessed them in this act of rebellion (Ex.
1:20).

Notice that they did not inquire with any civil magistrate
regarding the lawfulness of their acts of defiance. There is no
indication that they checked with the elders of Israel. They simply
