Are Operation Rescue’s Critics Self-Serving? 141

them, Verily I say unto you, That the publicans and the harlots
go into the kingdom of God before you. For John came unto you
in the way of righteousness, and ye believed him not: but the
publicans and the harlots believed him: and ye, when ye had seen
it, repented not afterward, that ye might believe him (Matthew
21:28-32).

In my view, this sort of schizophrenia cannot be sustained
indefinitely. What people do is more fundamental than what peo-
ple say. “Yea, a man may say, Thou hast faith, and 1 have works:
shew me thy faith without thy works, and I will shew thee my faith
by my works” (James 2:18). As time goes on, people will reshape
their theological opinions in terms of their actions. Their theologi-
cal schizophrenia will be healed by their adoption of a theology
that is more consistent with their actions. A man may insist that
he is a covenant theologian, but watch what he does. This will tell
you where he is headed. Similarly, a man may claim to be a
dispensational premillennialist, but watch what he does. This will
tell you where he is headed.

The Quality of the Arguments

What about the content, as distinguished from the rhetoric and
theology, of these anti-direct confrontation arguments? Not many
of these anti-confrontation arguments need to be taken seriously.
Most of them are reworked versions of the old.1938 arguments
against any form of Christian social involvement. A few, however,
are clothed in more modern terminology — “deep social concern”
without one iota of personal risk to the “deeply concerned” pastor.
Fewer still are serious objections that really do raise serious ques-
tions regarding non-violent anti-abortion activism. But they all say
basically the same thing: Christians should never break the civil
law as individuals who are acting on their own or in unauthorized
small groups.

While no Christian would deny that Ehud lawfully killed
Moabite King Eglon on his own, institutionally speaking, most
Christians would deny that the office of judge still operates today.
