The Author of All Authority 23

doth this man stand here before you whole. This is the stone which
was set at nought of you builders, which is become the head of the
corner. Neither is there salvation in any other: for there is none
other name under heaven given among men, whereby we must be
saved” (Acts 4:10-12). This is deeply resented by the apostles of
other loyalties, other gods.

Comprehensive Salvation and Social Action

Without this elevating of the name of Jesus Christ, there can
be no personal salvation, nor can there be social salvation, mean-
ing social healing. (A salve is a healing ointment; the English word
“salvation” is related to the English word for healing.) God heals
all men in history; therefore He saves them. “For therefore we both
labour and suffer reproach, because we trust in the living God,
who is the Saviour of all men, specially of those that believe” (I
Timothy 4:10). Not all men will go to heaven, but all men receive
undeserved, unmerited, 4é-healing blessings in history through the
grace of God in Christ. (We call this form of grace “common
grace.”)

This is why Christians who understand the comprehensive nature
of sin maust also preach the comprehensive nature of salvation in Jesus
Christ. Because Adam’s sin has tainted every aspect of life, the
redemption which is offered by Jesus Christ, the last Adam {I
Corinthians 15:45), in principle promises to cleanse every aspect
of life. The bodily resurrection of Christ in history testifies to the possibility
of comprehensive restoration in history. While sin is never perfectly
conquered in history until the final judgment, Christians can work
in confidence that their God is the God of history as well as
eternity, and therefore their efforts in history, meaning shis side of
Christ’s Second Coming, are not in vain in history.

The Promise of Restoration in History

The Book of Isaiah is full of promises regarding the future of
God’s people when they at last succeed in persuading men to
covenant with God. Notice the language of righteousness and
