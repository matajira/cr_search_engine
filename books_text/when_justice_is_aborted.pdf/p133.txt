Covenant-Breaking and Social Discontinuity M5

So, whose definition of “law-abiding citizen” should a Chris-
tian accept? Satan’s or God’s? Whose definition of “revolutionary”
should a Christian accept? Satan’s or God’s?

Tt ought to be clear by now whose definitions are covenantally
binding: God’s. Thus, when we seek to discover which course of
action is morally binding on us, we should seek first to discover
God’s definitions and descriptions of moral, covenant-keeping be-
havior. We should not allow Satan’s civil representatives to define
our categories for us. We should look to God’s definitions for
guidance. There are no common definitions, any more than there
are common principles of. civil law. There are God’s definitions
and God’s law. We begin with these. It is the myth of humanism
that anything on earth or in heaven is neutral. Everything is
covenantal. Nothing is neutral.

Breaking With Satan

There must be a fundamental break with Satan in the life of
every Christian. This is the discontinuity described in the Bible
as the transition from wrath to grace or from death to life. If it is to
become eternally binding, this transition must be made in each
person’s days on earth. “The Father loveth the Son, and hath given
all things into his hand. He that believeth on the Son hath everlast-
ing life: and he that believeth not the Son shall not see life; but the
wrath of God abideth on him” (John 3:35-36).

This fundamental break is supposed to be visible in the life of
each Christian. Therefore, with respect to Satan’s kingdom, each Chris-
tian becomes a revolutionary at the point of his conversion. He
breaks his covenant with Satan and establishes it with God. Rahab
did this when she became treasonous to Jericho by making a
covenant with the spies (Joshua 2). To make the covenant with
God she had to become treasonous to Jericho. This was the same
covenantal act. To claim her inheritance with God’s covenant
people she necessarily had to renounce her inheritance with Jericho,
She did this symbolically by placing the scarlet thread in her
window (Joshua 2:18-19).
