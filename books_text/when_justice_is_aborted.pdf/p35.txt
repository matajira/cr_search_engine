L._Transcendence/Presence of God

1
THE AUTHOR OF ALL AUTHORITY

And he changeth the times and the seasons: he removeth kings, and
setteth up kings: he giveth wisdom unto the wise, and knowledge to
them that know understanding (Daniel 2:21).

When a Christian asks himself the question, “Why should I
obey an immoral law?” he has taken the first step in developing a
theory of Christian social ethics.

When he asks himself the question, “How far should I go in
obeying an immoral law?”, he has taken the first step on the road
to social activism.

When he asks himself the question, “Im what way should I
oppose an immoral law?”, he has taken the first step on the road
to Christian resistance.

A Christian wants to please God. He does not want to do evil.
Is obeying an evil civil law itself evil? He cannot answer this
question accurately unless he has a concept of social: ethics. Usu-
ally this concept is merely implicit; Bible-believing Protestants
have not thought very much about developing an explicitly Bible-
based social ethics since the early 1700°s. The initial question is
where the Christian should begin his search for social ethics: “Why
should I obey an immoral law?” He needs a biblically valid way
to discover an accurate, God-honoring answer to this question.

It is really the question of sin and its control over men and
institutions..Christians are in the world, but we are not of the
world (John 17:10-19). By “of the world,” Jesus meant our place

17
