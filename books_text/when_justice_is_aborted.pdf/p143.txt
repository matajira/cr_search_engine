Conclusion 125

time. It takes place over many generations, as the third command-
ment says, as God shows His mercy “unto thousands [of genera-
tions] of them that love me, and keep my commandments” (Exo-
dus 20:6). In-this sense, Christianity is anti-Regoludiapagy. Tt preaches
the doctrine of regeneration through time.

Nevertheless, Christianity also teaches the moral necessity of
confrontation and resistance against evil — evil thoughts, evil acts,
evil men, and evil institutions. Christianity’s long-term earthly
goal is to reduce the influence of evil in every area of life. Therefore,
its long-term cultural goal is to reduce the influence of public evil
in every area of life.

The more publicly evil the social environment the Christian
lives in, the more revolutionary the personal transformation of
conversion is. At the same time, the less the new convert can do
to change things. This is analogous to the doctrine of transcen-
dence: the covenantal break at the time of personal conversion to
Christ is so great that the convert is left with little that he can do
to extend his faith to the institutional world around him. His
reaction to his environment must be primarily defensive. This is
what Christians face in prison camps or in the national prisons
we call Communist nations,

The less publicly evil the social environment, the more points
of contact the new convert can have with it. The more places that
he can confront it publicly and begin to change it. It is more like
the doctrine of presence: the convert is more present in the day-to-
day operations of his institutional world. He can work to change
his social environment’s public face precisely because it has not
yet departed into more fully self-consistent iniquity, This is what
Christians face in the Industrial West.

‘Thus we face a peculiar anomaly, In those social environments
in which Christianity represents a more revolutionary public break,
Christians are forced to be very circumspect and very conservative
in order to stay out of jail. They operate under a far more restricted
legal and political environment. Thus, they must “go underground”
in many of their activities: worship, baptisms, and evangelism.
