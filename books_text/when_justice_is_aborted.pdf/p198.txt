180 When Justice Is Aborted

life, 68, 92

lives, 70

Locke, John, 94
Lord’s Supper, 105
Louisiana, 158
loyalty, 22

magic, 106

Tagicians, 152

magistrates, 52, 53, 65, 86, 100,

131, 146

martyrs, 29, 72

masters, 10

media, xi-xii,

mercy, 89

midwives, 9, 52-53, 56
ight-right, 56-57

missions, 28

mobs, 72

Montgomery, 2

Moses
God's counsellor, 105-6
parents, 61
punishment of, 106
rod, 152

Moyers, Bill, 64

murder, 155-56

mutiny, 43

 

xiii, -76-77, 155.

 

natural Law, 128-29
Nazareth, 25
Nebraska, xi-xvi
Nebuchadnezzar, 13, 85
negative sanctions, 93
newspeak, 154
neutrality, 4, 75, 115
Nile, 152

Nineveh, 150
non-violence, 99-100
Nuremberg trials, 43

obedience, 43

Operation Rescue, xvii, 159-160
optimism, 35

orders, 44

origin, 18

pagans, 79

pantheism, 122, 127
Parks, Rosa, 3

patience, 32, 58, 104
Patton, Gen., 158
peace, 104

peace on earth, 22, 24
Pentateuch, 7-8
perseverance, 73, 107
Persia, 62-63

Pharaoh, 8-10, 52-53, 61, 152
Pharaoh's court, 145
Pharisees, 57

pietism, 30, 126-29
Pilate, 56

pills, 74

plague, 12-13

pluralism, 128-29
polarization, 75, 129-131
police brutality, 67-68, 71, 78, 97
politics, 21, 74-75, 106
pornography, 149
positive sanctions, 92
postmillennialism, 36
powers, 40

Pratt, Larry, 95-96
prayer, xiv, 139, 155
predestination, 109

Princeton Theological Seminary, 36

pro-choice, 144-45
progress, 129-130
Promised Land, ix
property rights, 137
prophets, 54-55, 89, 93
prostitution, 150
protests

authorized, 65
