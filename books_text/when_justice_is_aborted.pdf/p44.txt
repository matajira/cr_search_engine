26 When Justice Is Aborted

to say that the manifestation of the glory of God in history is
limited to the hearts of men. But this is not how the Bible speaks
of salvation: “For Zion’s sake will I not hold my peace, and for
Jerusalem’s sake I will not rest, until the righteousness thereof go
forth as brightness, and the salvation thereof as a lamp that
burneth, And the Gentiles shall see thy righteousness, and all kings
thy glory: and thou shalt be called by a new name, which the
mouth of the Lorn shall name. Thou shalt also be a crown of glory
in the hand of the Lorn, and a royal diadem in the hand of thy
God” (Isaiah 62:1-3).

God is seen through His people’s actions and the institutions
that they build to His glory in terms of His covenant. The presence
of God is manifested by the visible working out of His kingdom’s
principles in history. It is a mistake to assume that God is not
present just because He is not physically present. Such a view of
the presence of God belittles the work of the Holy Spirit in history.

God Is Present With His People

Jesus promised after His resurrection: “Lo, I am with thee
always, even unto the end of the world” (Matthew 28:20). He then
ascerided into heaven (Acts 1:9), But before He departed, He
promised the disciples that “ye shall receive power, after the Holy
Ghost is come upon you: and ye shall be witnesses unto me both
in Jerusalem, and in all Judea, and in Samaria, and unto the
uttermost part of the earth” (Acts 1:8).

This is one of the strangest facts in the Bible: in order for God
to be present with His people in power, His Son had to depart from
the earth. Because fesus Christ has departed physically, His people can be
closer to God spiritually than if He had remained on earth. Jesus was very
clear in His teaching about this:

But now I go my way to him that sent me; and none of you
asketh me, Whither goest thou? But because I have said these
things unto you, sorrow hath filled your heart. Nevertheless J tell
you the truth; It is expedient for you that I go away: for if I go not
away, the Comforter will not come unto you; but if I depart, I will
