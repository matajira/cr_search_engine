Covenant-Breaking and Social Discontinuity 119

19. Satan’s revolution was a discontinuity against righteous-
ness.

20. Adam was disinherited by execution.

21. .God the Father restores righteous continuity with the ulti-
mate discontinuity: the crucifixion of Jesus Christ.

22. This re-established continuity is unbreakable.

23. Satan seeks a continuity of evil.

24, The gospel’s ethical discontinuity (adoption) threatens Sa-
tan’s continuity.

25. Satan is progressively disinherited as God’s regenerating
grace spreads,

26. God’s continuity is ethical: biblical law.

27. Covenant-keepers affirm a continuity of obedience,

28. The: true revolutionary is the one who seeks to maintain
the continuity of Satan’s evil reign.

29. The true counter-revolutionary is the one who brings
Christ’s discontinuous gospel to the lost.

30. The discontinuity of the gospel is from wrath to grace,
from death to life.

31, Rahab was a righteous revolutionary.

32. Conversion is a revolutionary act against the continuity of
evil,

33. Both Satan and God offer an inheritance.

$4. Satan lies about his offer.

35. Satan hates man and hates life.

36. Satan is the great promoter of abortion: the death of man.
