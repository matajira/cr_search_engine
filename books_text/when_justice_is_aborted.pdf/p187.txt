Bibliography 169

Douglas, J. D. Light in the North: The Story of the Scottish Covenanters.
Grand Rapids, Michigan: William B. Eerdmans, 1964.

Fraser, Antonia. Cromwell: The Lord Protector. New York: Knopf,
1974.

Hall, Verna, editor. The Christian History of the American Revolution:
Consider and Ponder. San Francisco, California: Foundation for
American Christian Education, 1976.

Hill, Christopher. Puritanism and Revolution: Studies in Interpretation
of the English Revolution of the 17th Century. New York: Schocken
Books, (1958) 1964.

North, Gary, editor. Symposium on Christianity and the Ameri-
can Revolution, The Journal of Christian Reconstruction, Vol. III,
Summer, 1976. Published by the Chalcedon Foundation, P. O.
Box 158, Vallecito, California 95251.

Paul, Robert 8. The Lord Protector: Religion and Politics in the Life of
Oliver Cromwell. Grand Rapids, Michigan: William B. Eerd-
mans, 1955.

Ridley, Jasper. John Knox. New York: Oxford University Press,
1968.

Stevenson, David. The Scottish Revolution, 1637-44: The Triumph of the
Covenanters. New York: St. Martins, 1973.
