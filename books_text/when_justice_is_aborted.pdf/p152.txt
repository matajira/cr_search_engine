134 When Justice Is Aborted

Not so the Reubenites. They preferred the company of sheep.
They were deeply thoughtful — so thoughtful that they never could
quite find the time to move away from their bleating sheep. They
thought, and thought, and thought. But they did not act. They
chose instead to be conformed to the image of their sheep.

These days, unfortunately, the Reubens of this world are not
content to sit and think deep thoughts. They all seem to have
computers, word processors, and laser printers. Or maybe they
have only dot-matrix printers. The point is, they are not content
to sit among their sheep, immobilized in thought. They want
others to join them in the peaceful sheepfolds. Ifno one joins them,
then they might develop self-doubts about their immobilized con-
dition. They might even wind up in the lyrics of some future song
by some contemporary Deborah. And so they issue computer-
printed manifestos against the legitimacy of becoming involved i in
the hard realities of the war.

Sheepfold Manifestos

It is not sufficient for Christians to sit quietly. They see them-
selves as principled people in an unprincipled social environment.
They see themselves as morally different from those around them,
And so they feel morally compelled to defend their own inaction
by means of selective biblical citations. Thus it has always been.
Grand theological schemes justifying not-so-grand personal disen-
gagement can be built from selected Bible texts, once the battle
has begun. ‘After all, sheep need looking after. That is what shep-
herds are for. They need to look after the sheep (and occasionally
shear them). I can almost imagine. what one of the Reubenite
manifestos would have said, had the Reubenites owned word
processors and laser printers in the days of Deborah. (Actually,
the manifesto would probably have been a sermon that the church
secretary typed up for distribution.)

Who is this Deborah? Who does she think she is? Who put her
in charge of the armies of Israel? The Bible says that women are
not to become leaders and elders. When a society has sunk so low
