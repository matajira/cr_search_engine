xiv When justice Is Aborted

The sheriff was media perfect, It was as if he had been sent in
from central casting. A big, gruff, arrogant man, he looked every
inch a bully. It was clear on camera that he was not about to pay
any attention to the civil rights of Christians. All he cared about
was that he had been authorized by Cass County to shut down
this little church school, and if necessary, the church itself, and if
this meant dragging a bunch of praying pastors out of the church,
well so much for the power of prayer.

Rev. Ed Rowe had written a paperback book about the events
of 1982: The Day They Padlocked the Church (Huntington House,
1983). The wonders of modern printing technology were put to
use in a righteous cause. So were the technological wonders of
videotape. The edited videotape of the police dragging pastors out
of the church was used to mobilize other churches around the
nation. The church was immediately re-opened. The war esca-
lated. Hundreds of pastors streamed into Louisville.

Visiting pastors now began to pray Psalm 83 against the
sheriff and the county government. Christians are not familiar with
Psalm 83. They need to be. It includes this section:

Do unto them as unto the Midianites; as to Sisera, as to Jabin,
at the brook of Kison: Which perished at En-dor: they became as
dung for the earth. Make their nobles like Oreb, and like Zeeb:
yea, all their princes as Zebah, and as Zalmunna: Who said, Let
vs take to ourselves the houses of God in possession. O my God,
make them like a wheel; as the stubble before the wind. As the fire
burneth a wood, and as the flame setteth the mountains on fire;
So persecute them with thy tempest, and make them afraid with
thy storm. Fill their faces with shame; that they may seek thy
name, O Lorn. Let them be confounded and troubled for ever;
yea, let them be put to shame, and perish: That men may know
that thon, whose name alone is JEHOVAH, art the most high
over all the earth (Psalm 83:9-18).

The pastors prayed other similar psalms (called “imprecatory
psalms) and prayers. They took turns as teachers in the school.
They took turns as “co-headmasters” of the school. Some of them
