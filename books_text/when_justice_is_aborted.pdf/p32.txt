14 When Justice Is Aborted

of official protests. This is what Jeroboam did when Rehoboam,
Solomon’s son, imposed harsh new taxes (or possibly a system of
forced labor). Jeroboam created a new nation, the northern king-
dom of Israel. “So Israel rebelled against the house of David unto
this day” (I Kings 12:19).

We should also consider the question of lawful resistance
against a military invader. Ehud the judge slew King Eglon of
Moab through the use of deception (Judges 3:15-26). He then
called the nation to a military revolt (Judges 3:27-30). Similarly,
Jael deceived the fleeing Canaanitic general Sisera, even though
her husband {a higher covenantal authority) had made some sort
of peace treaty with Sisera (Judges 4:17). She rammed a peg
through his temple until it nailed him to the ground (Judges
4:21) ~a graphic symbolic fulfillment of God’s promise to crush
the head of the serpent (Genesis 3:15). For this act of successful
military aggression and household covenantal rebellion, Deborah
praised Jael in her song of victory (Judges 5:24-27}.

There is no indication in the Bible that any of these acts was
morally or judicially improper, and in most cases, God granted
visible positive sanctions as rewards for such action, Anyone who
says that resistance and even revolution (rebellion) are not morally
and judicially justified in the Bible has to ignore or deny a great
deal of Scripture, and also renounce the legitimacy of the English
Revolution of 1688 and American Revolution of 1776, as well as
renounce the various anti-Nazi national underground resistance
efforts during World War II.

Reader, are you ready to do this?

Conclusion

The many questions surrounding the big question of lawful
resistance by Christians against immoral civil laws can be an-
swered by a careful examination of the biblical covenant model. I
have divided this book into five chapters, with each chapter struc-
tured in terms of one of the five points. I hope Christians will better
understand what they are being called to do in this age of seem-
