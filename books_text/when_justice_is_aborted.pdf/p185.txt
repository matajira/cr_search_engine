Bibliography 167
Biblical Civil Government

DeMar, Gary. God and Government. 3 volumes. Atlanta, Georgia:
American Vision, 1982-86.

—_—_-—— « Ruler of the Nations: Biblical Blueprints for Govern-
ment. Ft. Worth, Texas: Dominion Press, 1987.

Grant, George. The Changing of the Guard: Biblical Blueprints for
Political Action. Ft. Worth, Texas: Dominion Press, 1987.

North, Gary. Healer of the Nations: Biblical Blueprints for International
Relations. Ft. Worth, Texas: Dominion Press, 1987.

. Political Polytheism: The Myth of Pluralism. Tyler,
Texas: Institute for Christian Economics, 1989.

 

Secular Humanism

Humanist Manifestos I and If. Buffalo, New York: Prometheus Books,
1973.

Hitchoock, James. What Is Secular Humanism? Ann Arbor, Michi-
gan: Servant Books, 1982,

North, Gary. Conspiracy: A Biblical View. Ft. Worth, Texas: Domin-
ion Press, 1986. (Co-published by Crossway Books.)

—_—.-___.. “From Cosmic Purposelessness to Humanistic
Sovereignty,” Appendix A, in Gary North, The Dominion Cove-
nant: Genesis, 2nd ed.; Tyler, Texas: Institute for Christian Econom-
ics, 1987.

Rushdoony, Rousas John. The Messianic Character of American Educa-
tion. Phillipsburg, New Jersey: Presbyterian & Reformed, 1963.

Whitehead, John. The Second American Revolution. Elgin, Illinois:
David C. Cook Pub. Co., 1982.

. The Stealing of America. Westchester, Ilinois: Cross-
way, 1982.
