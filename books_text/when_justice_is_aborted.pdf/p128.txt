110 When justice Is Aborted

an ethical discontinuity. This means revolution through covenant-
breaking.

God’s Discontinuity

How does God restore the discontinuity between Him and His
children? How does he heal the breach —a covenantal breach?
He does it by means of an even greater discontinuity: the covenan-
tal break at Calvary between Him and His Son, Jesus Christ. “And
about the ninth hour Jesus cried with a loud voice, saying, Eli,
Eli, lama sabachthani? that is to say, My God, my God, why hast
thou forsaken me?” (Matthew 27:46). God disinherited His Son, Jesus
Christ, so that He might adapt His lost children, the children of Adam. God
the Father had to execute His Son Jesus Christ. Why? Because
there can be no covenantal disinheritance without the death of the
disinherited heir. Only the death of God’s Son could meet this
demand, for Jesus Christ became the One through whom God’s
adopted children might inherit.

Thus, Jesus Christ served as both the sacrificial lamb and the
sacrificing high priest, as the Son who died and also as the Testator
who died. This dual role of Jesus Christ is taught specifically by
the Epistle to the Hebrews:

For if the blood of bulls and of goats, and the ashes of an heifer
sprinkling the unclean, sanctificth to the purifying of the flesh: How
much more shall the blood of Christ, who through the eternal
Spirit offered himself without spot to God, purge your conscience
from dead works to serve the living Ged? And for this cause he is
the mediator of the new testament, that by means of death, for the
redemption of the transgressions that were under the first testa-
ment, they which are called might receive the promise of eternal
inheritance. For where a testament is, there must also of necessity
be the death of the testator. For a testament is of force after men
are dead: otherwise it is of no strength at all while the testator
liveth. Whereupon neither the first testament was dedicated with-
out blood. For when Moses had spoken every precept to all the
people according to the law, he took the blood of calves and of
goats, with water, and scarlet wool, and hyssop, and sprinkled both
