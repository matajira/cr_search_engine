IV._Oath/Sanctions (blessing and cursing)

4
WHOSE SANCTIONS WILL PREVAIL?

Then the Lorp said unto me, Proclaim all these words in the cities
of Judah, and in the streets of Jerusalem, saying, Hear ye the words
of this covenant, and do them. For I earnestly protested unto your fathers
in the day that I brought them up out of the land of Egypt, even unto
this day, rising early and protesting, saying, Obey my voice. Yet they
obeyed not, nor inclined their ear, but walked every one in the imagina-
tion of their evil heart: therefore I will bring upon them all the words
of this covenant, which I commanded them to do; but they did them
not. And the Lorn said unto me, A conspiracy is found among the men
of Judah, and among the inhabitants of Jerusalem. They are tured
back to the iniquities of their forefathers, which refused to hear my
words; and they went after other gods to serve them: the house of Israel
and the house of Judah have broken my covenant which I made with
their fathers. Therefore thus saith the Lorn, Behold, I will bring evil
upon them, which they shall not be able to escape; and though they
shall ery unto me, I will not hearken unto them (Jeremiah 11:6-11).

There is no doubt that the prophet Jeremiah functioned as a
covenantal agent between God and the innocent people of Judah,
The king and his court had become corrupt. Jeremiah proclaimed
the terms of the covenant before kings. He was the prosecutor of
God's covenant lawsuit, But the king chose not to listen. He cut the
law of the covenant into pieces and threw the pieces into the fire.

84
