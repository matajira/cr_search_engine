Bible, 12-14

Darwinian, 64

Jeremiah, 89

standards, 4

changing laws, 5
psalms, xiv, 91-92
punishment (corporal), 51

Queen of Sheba, 147

Rahab, 53, 56, 115-16
Rapture, 30-31, 35, 135-36
reaction, 67, 68-73
Reagan, Ronald, xiii
reconciliation, 112, 123-24
reconstruction, 32
relativism, 148
religion

political, 21
Renaissance, 80
fepresentation

evil, 58

voice of God, 41-43

(see also magistrates)
rescue mission, 36, 136
resistance, 145-46,

sanctions, 53
resisting evil, 99
responsibility, 72
Tesurrection, 23, 27, 28

Jews, 57
retreat, 140
Reuben, 133-34, 137, 160
revival

comprehensive, 32-34, 38

lost souls, 33-34

technology, 32
revolution, 65, 87

Bible on, 103

right of, 52

righteous, 116
revolutionaries

Index 181

inward, 21

rhetoric, 140

righteousness, 26, 66

rights, 5

riots, 66

risks, 53

Roe v. Wade, 37, 91, 107, 139,
143, 144-45, 160

Rome, 1, 15, 21

Rowe, Ed, xiv

rulers
hate God, 19-20
rebellion of, 19-20

Rushdoony, RJ., 15

Rutherford, Samuel, 130-31

Satan
breaking with, 115-16
bureaucracy, 48-49
continuity, 112-13
creative, 49
defensive, 28
discontinuity, 108-10
inheritance of, 113
judged, 27
kamikaze, 30
kingdom of, 117
tyrant, 55

sacraments, 48, 50, 105

Sadducees, 57

saint, 92, 105, 107, 122

salt, 120-21

salvation
social, 25-26, 66
state?, 52

salve, 23

sanctification, 33, 122-23, 124

sanctions, 90-94
interposition, 97
negative, 56
positive, 36
watchman, 92-93
