Introduction 3

It is Saturday, December 4. You are a black person living in
Montgomery. You learn that a lady named Rosa Parks was ar-
rested the day before yesterday for refusing to give up her seat and
stand in the back of the bus. You hear that blacks are organizing
a boycott of the local bus company until the seating rule is abol-
ished. They are saying, “If we can’t sit wherever we want to, on a
first-come, first-seat basis, we won’t spend our money to ride the
bus. We should be treated just like any other passengers.” The
boycott will begin on Monday morning. Should you join the
boycott and refuse to ride the bus?

It is Monday, December 12. The leaders of the boycott are
mainly ministers. The boycott is working. The buses are 75%
empty. But the local authorities have discovered an obscure state
law that makes it illegal to run a boycott against any state or
municipal service. You are a black person who owns an automo-
bile. Many blacks have joined the boycott and are seeking alterna-
tive ways to get to work in the morning and back home at the end
of the day. You are asked by a representative of the boycotting
group to drive people to work and back home in the evening. The
city has said this is illegal, since there is a city ordinance requiting
a minimum fee for all “taxi* service, and you will be regarded as
a taxi service. Should you agree to drive people anyway?

Rosa Parks and the blacks of Montgomery defied the law. On
December 17, 1956, the U.S. Supreme Court refused to hear a
protest by the City of Montgomery against a Federal appellate
court's ruling that the segregated seating was illegal Bus segrega-
tion ended in Montgomery on December 21, 1956, a little over a
year after Mrs. Parks sat tight and broke the law.

The year is this year. You know that a local abortion clinic is
killing unborn babies. You know that the civil government has
authorized such murder if it is performed by a monopolistic,
state-licensed physician. Picketing has been tried; it has not stopped
the murdering from going on. Christians have decided that if a
large number of them block the doorway to the clinic, it will make
it more difficult for mothers to murder their infants. It will lead to
