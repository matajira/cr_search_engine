abolitionism, 35, 135
abortion
compulsory?, 143
eschatology &, 37
Jegal battle, 70
polarizing, 129-30
salt, 121
action-reaction, 68-73
Adam
freedom of, 51
representative, 42
spared, 45
adoption, 109, 117
agitators, xii, xv-xvi, xvii
AIDS, 92
Alabama, 2
Alinsky, Saul, 76
American Revolution, 14, 94, 131
antinomianism, 148
apostles, 40, 71
army, x-xi, 43-44, 50, 69

arrows, 160
Assyria, 108, 150
Athalia, 55
Atlanta, 155-87
authorities
arrogance, 85
Bible on, 40

INDEX

conflicting, 49-44
plural, 44
reactions of, 67
representation, 90
source, 18
(see also magistrates)
autonomy, 47, 50

Babylonians, 85, 108
baptism, 41

Battle of the Bulge, xi
battlefield, 20, 44
Burger, Warren, 64
Bible, 6, 15-16, 28, 41, 85
Birmingham, 154
Bishop X, 155-56
blood, 110-11

boot camp, 33
boycott, 3

“Brutus, Junius,” 93
Bryan, William J., 136
Bull Connor, 155
bureaucracy, 27, 66
bus line, 3

Caesar, 15

Caleb, ix
Calvary, 10

175
