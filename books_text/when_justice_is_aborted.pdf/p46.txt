28 When Justice Is Aborted

a completed Bible to teach us and the Holy Spirit to illuminate
our minds. We have centuries of experience behind us through the
development of church creeds and courts. We have nineteen centu-
ries of missions experience to draw upon. We also possess vast
economic wealth — capital undreamed of as recently as two centa-
ries ago, or even half a century ago. We have incomparable tools
of communication. What more do we lack? Confidence!

In spite of all these God-given tools of dominion, Christians
feel as if they were a tiny besieged army surrounded by powerful
hostile forces. When they read that the gates of hell shall not
prevail against the church (Matthew 16:18) — the church prevails,
not the angelic host of heaven — they somehow hear in their minds
something quite different: that the gates of heaven will prevail
against Satan’s demonic onslaught. They think of Satan as being
on the offensive today. They forget what the resurrection of Christ
did to Satan. They do not acknowledge that the resurrection of Jesus and
the sending of the Holy Spirit changed anything fundamentally in history: It
is not clear in their minds that ever since Christ’s resurrection and
ascension to heaven, the church has been on the offensive, and
Satan has been on the defensive.

Legitimate Confidence

With God above us and church history behind us, why should
we Christians have doubts about the earthly success of our cause?
We may have doubts regarding our own courage and capacities,
but we should have none regarding God’s strength. Psalm 110:1
makes God's plan plain: “The Lorn said unto my Lord, Sit thou
at my right hand, until I make thine enemies thy footstool.” The
fact that Jesus is seated at the right hand of God in heaven is not
a guarantee of the church’s progressive defeat until He returns
again physically in absolute power; on the contrary, He sits at
God’s right hand until! His enemies are subdued. The verse could
not be any plainer. Jesus will not return again until the church,
as His authorized representative in this New Testament era, has
made His enemies His footstool. “Then cometh the end, when he
