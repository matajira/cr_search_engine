64 When Justice Is Aborted

Justice Warren Burger was interviewed by Bill Moyers just after
the former had announced: his resignation from the Court. The
exchange is quite revealing:

CHIEF JUSTICE BURGER: Constitutional cases — constitutional jur-
isprudence is open to the-Court to change its position, in view of— of
changing conditions. And it has done so.

MOYERS: And what does it take for the Court to reverse itself?
CHIEF JUSTICE BURGER: Five votes.

The U.S. Supreme Court has reversed itself over 150 times in
its history. This is an extremely important fact for all those who
are considering the legitimacy of non-violent public protests against
some law. ‘The so-called law of the land keeps changing. The Court
responds to public pressures. It is subject to new appointments
by the President. In short, the modern concept of law is wholly
statist, divorced from any concept of permanent moral truth or

_even logical truth, The law is little more than a fluctuating major-
ity of Supreme Court justices.

It is well known that law school professors train their students
to get existing laws overturned by the courts. It is a prestigious
thing to have been the lawyer who persuaded the Supreme Court
to reverse itself, But these same professors speak to the general
public in terms of the sacrosanct law of the land.

For a Darwinist, there is no such thing as “sacrosanct.” There
are only changing environmental conditions and social responses
to those changing conditions. Thus, when a protester decides to
take a stand for a different interpretation of the civil law, the
consistent Darwinist has to say that this may be the dawn of a new
era. Like the random genetic change that makes a particular
member of a species “more fit” to survive in his changing environ-
ment, so is the citizen who protests an existing law, The Darwinist
who remains true to his faith can have nothing critical to say about
the morality of such protests and changes. He rejects the whole
idea of a permanent moral code independent from changing exter-
