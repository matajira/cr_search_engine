Couenant-Breaking and Social Discontinuity 113

again” or “born from above.” It is a legal act of adoption. A person
moves from the family of the first Adam to the family of the last
Adam, He moves from inheritance with Satan to inheritance with
Christ.

What is Satan’s inheritance? “Then shall he say also unto
them on the left hand, Depart from me, ye cursed, into everlasting
fire, prepared for the devil and his angels” (Matthew 25:41). “And
death and hell were cast into the lake of fire. This is the second
death. And whosoever was not found written in the book of life
was cast into the lake of fire” (Revelation 20:14-15). This is the
future discontinuity: separation from God’s blessing. This begins
a permanent continuity: the eternal wrath of God in the lake of fire:
cursing.

God’s Continuity

God’s continuity is ethical. His Word of law to man establishes
His continuity. “Think not that I am come to destroy the law, or
the prophets: I am not come to destroy, but to fulfil. For verily I
say unto you, Till heaven and earth pass, one jot or one tittle shall
in no wise pass from the law, till all be fulfilled. Whosoever
therefore shall break one of these least commandments, and shall
teach men.so, he shall be called the least in the kingdom of heaven:
but whosoever shall do and teach them, the same shall be called
great in the kingdom of heaven. For I say unto you, That except
your righteousness shall exceed the righteousness of the scribes
and Pharisees, ye shall in no case enter into the kingdom of
heaven” (Matthew 5:17-20).

When Christians take seriously God’s law, they place them-
selves. visibly inside His covenant. This is a visible testimony to
other men regarding the covenantal faithfulness of God.

Ye are the salt of the earth: but if the salt have Jost his savour,
wherewith shall it be salted? It is thenceforth good for nothing, but
to be cast out, and to be trodden under foot of men. Ye are the light
of the world. A city that is set on an hill cannot be hid. Neither do
men light a candle, and put it under a bushel, but on a candlestick;
