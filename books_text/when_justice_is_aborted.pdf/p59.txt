The Voice of Lauyful Authority 41

cially Christians. This means that Christians must acknowledge,
affirm, and obey the principles found in both passages. This is the
Bible talking!

Why should Christians be more bound by the Bible than
anyone else? Doesn’t God hold all people responsible for their
actions? He does, but Christians are even more responsible. The
Bible teaches that from him who has been given much, much is
required (Luke 12:48-49). Furthermore, Christians have pledged
eternal obedience to Jesus Christ, their King, the incarnate Second
Person of the Trinity: They have been baptized in His name, and
therefore they are legally under His jurisdiction. This, in fact, is the
primary meaning of baptism: to place oneself under the judicial authority of
God. When someone has been baptized in the name of the Father,
the Son, and the Holy Ghost, he or she has become God’s man or
God’s woman. Once this mark of God’s authority and man’s
subordination has been placed on a person, there can be no legal
escape.

There are five things that we can say confidently about God’s
relationship to Christians over us: 1) God’s Word is binding, 2)
God’s covenant authority is binding, 3) the covenant’s laws are
binding, 4) the sanctions (blessings and cursings) attached to this
covenant are binding, 5) the sanctions are forever. We dare not
forget or neglect even one of these five covenant principles. All five
are binding on us, and God will hold us eternally accountable for
believing and obeying all five.

This book does not attempt to set forth the case for the Bible
as God’s inspired Word. The book assumes this about the Bible,
however. We begin with the second question: the lawful voice of
authority. This is the question of representation. Which voice in
history speaks authoritatively in the name of the God of the Bible?
The Bible teaches that several authorities do: civil rulers, church
rulers, family rulers, and the conscience. All four are God’s lawful
covenantal agents. All four have taken binding oaths, either explic-
itly or implicitly. Rulers swear to uphold the law: state, church,
or family law. So does each individual. Men either accept Jesus
