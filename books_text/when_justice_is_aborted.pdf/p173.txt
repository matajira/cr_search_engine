Are Operation Rescue’s Critics Self-Serving? 155

energies to be drained away by exciting and dramatic methods to
stop abortion. . . .”

And again: “Beware of being pressured by the emphasis that
civil rebellion generates ‘media coverage’ and increases social ten-
sion and upheaval so as to bring the abortion question into the
public awareness.” Furthermore, “that is Marxist and Hegelian
tactics,”

To which I answer: Click.

The name of the sheriff who ran the Birmingham operation
will live in infamy, for his name was “media perfect”: Bull Connor.

Now, let us restructure our critic’s assertion. It is 1963 in
Alabama. We are assured by the pastor of Laodicea Covenant
Church that “Public protests will make Sheriff Connor the enemy
of our civil rights efforts. He is not our enemy, unless he refuses
to enforce the law of God.”

I get tired of hearing such nonsense, offered in the name of
covenant theology. So, let us turn to a decidedly non-covenant
theologian, from the very same city as our supposed experts on the
covenant: Atlanta,

Murder Is Wrong, Except When Convenient

We believe that abortion is wrong in cases other than where the physical
life or mental well-being of the mother is at stake.

Wow! What a moral wall of resistance against evil!

As always, we need to alter this pastor’s words only slightly.
The child is now five seconds out of the womb. Change “abortion”
to “infanticide.” We discover this “breakthrough principle” of
biblical ethics: “We believe that infanticide is wrong in cases other
than where the physical life or mental well-being of the mother is
at stake.”

Now the child is five years old. “We believe that murdering
young children is wrong in cases other than where the physical life
or mental well-being of the mother is at stake,”

Now the former child is 80 years old and infirm. You know
what is coming; “We believe that euthanasia for the terminally ill
