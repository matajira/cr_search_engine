Covenant-Breaking and Social Discontinuity il7

aspect of this world. God, on the other hand, tells the truth about
the nature of His inheritance. He tells us that this is His world,
and that we have become fellow heirs with Christ. Heirship in
God’s family is covenantal: by adoption, It is therefore also ethical.

For as many as are led by the Spirit of God, they are the sons
of God. For ye have not received the spirit of bondage again to fear;
but ye have received the Spirit of adoption, whereby we cry, Abba,
Father. The Spirit itself beareth witness with our spirit, that we are
the children of God: And if children, then heirs; heirs of God, and
joint heirs with Christ; if so be that we suffer with him, that we
may be also glorified together (Romans 8:14-17).

Not by works of righteousness which we have done, but ac-
cording to his mercy he saved us, by the washing of regeneration,
and renewing of the Holy Ghost; Which he shed on us abundantly |
through Jesus Christ our Saviour; That being justified by his grace,
we should be made heirs according to the hope of eternal life. This
is a faithful saying, and these things I will that thou affirm con-
stantly, that they which have believed-in God might be careful to
maintain good works. These things are good and profitable unto
men (Titus 3:5-8}.

Satan hates this view of the kingdom. He hates man. He wants
to inherit through the death of man. When Adam died, Satan
thought that he would inherit. Instead, Christ through faithfulness
even unto death inherited, and spoiled — collected the spoils of
war from ~Satan’s kingdom: “And having spoiled principalities
and powers, he made a shew of them openly, triumphing over
them in it” (Colossians 2:15).

Satan is the great promoter of abortion. He delights in destroy-
ing man’s inheritance. He is at war with life. God says, “All those
who. hate me love death” (Proverbs 8:36b). Satan hates God, and
Satan loves death, especially the death of man. Thus, we should
not be surprised to see the battle for the soul of the modern world
being fought over the abortion question. It will never be settled
until Satan’s kingdom is obliterated.
