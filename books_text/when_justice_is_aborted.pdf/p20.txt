2 When Justice Is Aborted

The year is 1941. You are a Christian living in German-
occupied Holland. You have been approached by a Jewish family
seeking refuge from the Nazis. It is illegal to hide Jews, but they
ask you to hide them. Should you tell them to look for refuge
elsewhere, since you do not want to break the law?

The year is 1944, The Nazis have been informed that all
Christians are required by God to tell the truth no matter what
the circumstances. They have believed this story. So, they are
going from door to door, asking every known church member if
he knows where any Jews are being hid by others. You, a faithful
Christian, know that your non-Christian neighbor is illegally hid-
ing a Jew in the attic. German soldiers come to your door and ask
you point blank: “Do you know if anyone in this neighborhood is
hiding Jews?” If you answer no, the soldiers will probably leave,
knowing that you are unlikely to lie. If you tell them yes, you will
be asked where the Jews are. If you say nothing, they will know
you know. They will arrest you for withholding evidence, and they
will also conduct a detailed search of the neighborhood. Should
you lie, tell them the truth, or remain silent?

Christians in Holland disobeyed the Nazis throughout World
War IE. On April 30, 1945, Adolph Hider committed suicide in
Berlin.

It is Thursday, December 1, 1955. You live in the city of
Montgomery, Alabama. You are a black woman coming home
from a hard day’s work. You are sitting on a bus in the front
section, which is legal as long as no white person is required by
crowding to sit next to you. By city law and local bus line rules,
blacks are not allowed to sit parallel to a white. The bus fills up.
A white man is standing at the front of the bus because there are
no-more seats available. The bus driver tells you to get up and
move to the back of the bus; a white person needs the seat. You
are required to get up and let him sit there. You will have to stand
at the back of the bus. But you have paid your fare, and your local
taxes support the municipal bus line. Should you stand up and
move to the back of the bus?
