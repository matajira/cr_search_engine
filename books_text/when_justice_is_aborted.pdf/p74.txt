56 When Justice Is Aborted

in the house of the Lorn six years. And Athaliah did reign over
the land” (II Kings 11:3). There was no lower civil magistrate.
involved here. The senior officer of the church took full responsibil-
ity for this revolt against civil authority.

Does Mightg Make Right?

God brought negative sanctions in history against Egypt and
Jericho. God also brought positive sanctions in history to the
midwives and Rahab. This proves that God’s civil government
{the civil aspect of God’s universal kingdom) is alone absolutely
sovereign, and earthly civil governments are hierarchically subor-
dinate to God’s kingdom rule. The civil government that imposes
final sanctions in history and eternity is the absolutely sovereign
civil government in history and eternity.

This does not mean that “might makes right.” It means that
God is right, God is mighty, and the kings of the earth will bow
down to him. It was not the task of the midwives or Rahab to
attempt to force the kings of their day to bow down to God. They
were not required or authorized by God to bring visible negative
sanctions against these rebellious rulers. These women were not
civil rulers themselves; they had no legal authority to bring nega-
tive physical sanctions against those in office over them. Venge-
ance was God’s, as it is today. But they were required by God to
act as law-abiding righteous people by lying to the rulers, confus-
ing them, and thwarting their proclamations. Then God brought
the rulers low.

When Paul was brought before the Roman council in Jerusa-
lem, the room was filled with Jewish religious leaders, who were
in fact subordinate rulers to Roman civil authority, They had
already admitted this in public at the most judicially critical point
in Israel’s history, the crucifixion of Christ the Messiah: “But-they
cried out, Away with him, away with him, crucify him. Pilate saith
unto them, Shall I crucify your King? The chief priests answered,
We have no king but Caesar” (John 19:15).
