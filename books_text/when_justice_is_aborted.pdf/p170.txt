152 When Justice Is Aborted

faith, if it hath not works, is dead, being alone. Yea, a man may
say, Thou hast faith, and I have works: shew me thy faith without
thy works, and I will shew thee my faith by my works” (James
2:14-18).

So, “It does no good to look for physical solutions, such things
as demonstrations or planned civil rebellion, Preaching is suffi-
cient.” This is what I call spiritualizing away the Scriptures. This
is a form of fundamentalist mysticism (what philosophers used to
call neo-Platonism). It is a withdrawal from the hard choices and
dangerous commitments of life. But most of all, it is a denial of the
Old Testament and the Epistle of James. It is a denial of God’s
real-world covenant, yet all in the name of faithful service to God.

Speak Softly and Carry No Stick

Then, when Moses entered Egypt again, forty years later, he was armed
only with the powerful word of Jehouah. And that was all he needed to liberate
his people from bondage.

I remember something about a rod that turned into a serpent
and ate the serpents of Pharaoh’s magicians. I also recall some-
thing about Moses touching the Nile River with this rod and
turning the Nile to blood. There was something about dust into
lice, too, and day into darkness, and several other unpleasant
events.

Either the critic wants us to remain content by speaking words
of visible impotence ~no lice, no frogs, no fiery hail from
heaven — or else he wants us to wait for God to turn us into “heap
big medicine men.” It does not matter which, just so long as we
avoid trouble with the civil magistrate.

What the non-violent interposers warit us to do is to pray,
preach, hand out tracts, and block doorways. The critic forgets
that we can pray with our eyes open. He forgets that we can pray
while our heads are being clubbed, and while we are being hauled
off to the local jail. We can also pray when we insist on a jury trial.
We can pray while we are writing checks—yes, even non-tax-
