Conclusion 123

world, and I come to thee. Holy Father, keep through thine own
name those whom thou hast given me, that they may be one, as
we are. While I was with them in the world, I kept them in’ thy
name: those that thou gavest me I have kept, and none of them is
lost, but-the son of perdition; that the scripture might be fulfilled.
And now come I to thee; and these things I speak in the world,
that they might have my joy fulfilled in themselves. I have given
them thy word; and the world hath hated them, because they are
not of the world, even as I am not of the world. I pray not that
thou shouldest take them out of the world, but that thou shouldest
keep them from the evil. They are not of the world, even as I am
not of the world. Sanctify them through thy truth: thy word is
truth. As thou hast sent me into the world, even so have J also sent
them into the world. And for their sakes I sanctify myself, that they
also might be sanctified through the truth (John 17:10-19).

This process of personal progressive sanctification ~ setting
oneself aside more and more for God’s service throughout one’s
lifetime ~ is each Christian’s God-assigned task, We are required
by God to set ourselves apart from the sins of this world, step by
step, year by year. This is not a process of withdrawal from this
world. Jesus made this plain: “I pray not that thou shouldest take
them out of the world, but that thou shouldest keep them from the
evil.” But if we are not to be removed from this world, yet we are
also to be set apart from Satan, how can we do this? There is only
one way: we must progressively extend the kingdom of God in history
through the preaching of the gospel and the subduing of all things
to Gad’s glory in terms of His revealed ethical standards.

Reconciliation

We are not to be reconciled to this sin-filled world. The gospel
nevertheless is a message of reconciliation, It obviously has to be
a message of reconciling this world to God through the ethical
transformation of men and institutions.

And all things are of God, who hath reconciled us to himself
by Jesus Christ, and hath given to us the ministry of reconciliation;
To wit, that God was in Christ, reconciling the world unto. himself,
