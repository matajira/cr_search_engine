54 When Justice Is Aborted

they faced this danger without visible flinching. In fact, their
courage must have been part of the success of their plan of civil
disobedience. Had they shown fear, their lies might have been
detected. Only because they did not show fear did the rulers accept
their lies as true.

The Biblical Justification for Lying

The Bible says that Christians should not lie to each other.
“Wherefore putting away lying, speak every man truth with his
neighbour: for we are members one of another” (Ephesians 4:25).
But this rule does not always prevail in dealings between civil
governments or between governments and their citizens. For ex-
ample, civil governments certainly believe in the legitimacy of
military lying, so they train and send out spies, and they camou-
flage troops and weapons. Moses sent spies into Canaan before the
invasion (Numbers 14). Joshua, who had. been one of the spies
under Moses, did the same a generation later (Joshua 2). Are we
to say such decisions by civil governments are morally wrong? If
so, then why did God allow Moses and Joshua to send out spies
to spy out the land of Ganaan? In times such as today — days filled
with life-and-death crises - Christians had better not be naive
about such matters. If Christians are morally required by God to
avoid lying to the civil government in all cases, then on what moral
basis did Christians in Europe hide Jews in their homes during the
terror of the Nazis?

If you have qualms about accepting the idea of self-conscious
lying as a legitimate part of civil disobedience, please consider the
following passages in the Bible to see how God deliberately lies to
unjust civil rulers and false prophets in order to bring them low:

And the Lorn said, Who shall persuade Ahab, that he may
go up and fall at Ramoth-gilead? And one said on this manner,
and another said on that manner. And there came forth a spirit,
and stood before the Lorn, and said, 1 will persuade him. And the
Lorn said unto him, Wherewith? And he said, I will go forth, and
I will be a lying spirit in the mouth of all his prophets. And he said,
