Conclusion 121

the second. Almost nobody thinks of the third. That is the problem
today: nobody wants to think of God’s permanent sanctions, not
even Christians. \

The Old Testament required salt in the sacrifices. “And every
oblation of thy meat offering shalt thou season with salt; neither
shalt thou suffer the salt of the covenant of thy God to be lacking
from thy meat offering: with all thine offerings thou shalt offer salt
(Leviticus 2:13), The sacrifices symbolized the burning flesh of the
lake of fire. God allowed the sacrifices of animals to serve as
symbolic substitutes for man. God’s covenant requires salt. “All
the heave offerings of the holy things, which the children of Israel
offer unto the Lorn, have I given thee, and thy sons and thy
daughters with thee, by a statute for ever: it is a covenant of salt
for ever before the Lorp unto thee and to thy seed with thee”
(Numbers 18:19).

Today, abortionists use a saline (salt) solution that is injected
into the mother’s uterus. The solution literally burns the baby to
death. The abortionist has selected a means of destroying the
innocent that testifies to the abortionists’ own eternal future, as
well as the mother’s. This is Satan’s imitation covenant, a cove-
nant of death.

Christians are called to be the salt of the earth: flavoring,
preserving, and destroying. Salt is covenantal, Christians are re-
quired by God to honor the terms of His covenant. They are to
act in his name (point two), in terms of his law (point three),
bringing His sanctions in history — blessing and cursing (point
four), extending His kingdom in history, while simultaneously
salting over Satan’s kingdom (point five).

The trouble is, Christians are unfamiliar with these tasks.
They have not been taught the essentials of covenant theology.
They do not understand the comprehensive nature of the gospel.
They do not know what it means to be Gad’s salt in history. They
barely understand point one of the covenant: the doctrine of God.
