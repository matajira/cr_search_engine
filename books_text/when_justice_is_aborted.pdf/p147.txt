Conclusion 129

these protests may be headed, even if their followers cannot: to a
total confrontation with the civilization of secular humanism, Such
a total confrontation requires a consistent, thoroughly developed
theology to defend it and implement it. After all, you can’t beat
something with nothing. There is only one possible choice today:
covenant theolagy. After the protesters have read this book, and have
accepted its conclusions, they may decide to read my other book,
which I am writing at the same time: Political Polytheism: The Myth
of Pluralism (Institute for Christian Economics, 1989). This, too,
makes Christian leaders very nervous.

These theological and political implications ofnon-violent Chris-
tian interposition may be why so few Christian leaders have be-
come vocal supporters of Operation Rescue, and why those who
have may back down when the theological and political implica~
tions of Operation Rescue become clearer (such as with the publi-
cation of this book). It will be interesting to see which is their
greater enemy: murdering babies or covenant theology.

The Polarizing Issue of Abortion

When the anti-abortion movement escalated, it produced a
major breach in traditional American fundamentalism and evangeli-
calism. The world-retreating, confrontation-avoiding pietists re-
sented the appearance of Christian concern regarding a social and
legal issue that is inescapably political. It is one thing to send
money to a local rescue mission to help sober up drunks. It is
something clse again to get involved in political action to make
illegal a now-socially acceptable form of murder, as abortion had
been throughout American history prior to 1973. This is why
Francis Schaeffer wrote The Great Evangelical Disaster. to warn
evangelicals of the moral evil of deliberately remaining on the
sidelines of life when society faces a literal life-and-death issue.

Abortion has been regarded as a sin throughout the history of
the church, but this does not faze the pietists, since they do not
know much about church history, and they regard church history
as mostly a series of mistakes. Not seeing the progress of history,
