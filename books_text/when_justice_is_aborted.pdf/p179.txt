Appendix B

RESOURCES

There are a large number of excellent pro-life and charitable
organizations that you can look to for help in your struggle for life
and truth. Each has a unique area of specialty. Each has literature,
presentations, services, resources, and opportunities that you can
take advantage of, and each is deserving of your prayerful and

financial support.

The following list is by no means comprehensive, but it should

give you a good start,

Americans Against Abortion
P.O. Box 40
Lindale, TX 75771

American Life League (ALL)
P.O. Box 490
Stafford, VA 22554.

American Rights Coalition
P.O. Box 487
Chattanooga, TN 37401

Americans United for Life
343 South Dearborn, Suite 1804
Chicago, IL 60604

Birthright
11235 South Western Avenue
Chicago, TL 60643

Black Americans for Life
419 7th Street, NW, Suite 402
Washington, DC 20004

Christian Action Gouncil (GAC)
422 C Street, NE
Washington, DC 20002

Couple to Couple League
P.O. Box 11084
Cincinnati, OH 45211

161
