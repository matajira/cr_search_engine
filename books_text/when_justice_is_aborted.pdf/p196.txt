178

gates of hell, 28

gentiles, 47, 79

German, xvi

ghetto in sky, 31

Gideon, 69-70, 75

God
civil government, 56
commander (senior), 44
continuity, 111, 113-14
counselling, 105-6
Creator, 122
discontinuity, 110
enemies of, 28
hierarchy of values, 50, 57
holier than, 55
Judge, 122
kingdom, 29, 56, 123
Jaw of, 63
minister of, 51
name, 29
name of, 22-23
Outside Agitator, xvi
patience, 104
power of, 26
presence of, 26-28
providence of, 18
sanetions, 84-94
sanctuary, 92
society’s, 20
sovereignty, 20-21
speaks, 19
Spirit of 26-27
strength of, 28
throne, 92
transcendent, 19
unchanging, 62
vassals of, 62
vengeance, 51, 56, 68
voice of, 41
voice of authority, 20
wrath of, 5
year of 25

When Justice Is Aborted

Godspeed, xviii
gospel, 13
comprehensive, 31-82
confrontational, 31-31
defeat of?, 37
invisible, 151-52
revolution, 116
social, 35
government
church, 48-51
covenantal, 7
hierarchy, 57
self government, 46
grace, 23, 115
grasshoppers, 69
Great Awakening, 34

Haman, 96
healing, 25-26, 66, 75
hell, 28
hidden agendas, 142
hierarchy
church, 48
covenant, 57
God’s values, 50, 57
Satan’s, 49
history
discontinuity, 32
God of, 23
restoration in, 23-25
resurrection &, 28
Hodge, Charles, 36
holiness, 106
Holland, 2
Holmes, Oliver Wendell, 63
Holy Spirit, xi, 26-27, 32
humanism
abolitionism &, 35
covenant, 9, 10
politics, 106!
war against, x
