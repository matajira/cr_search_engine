BIBLIOGRAPHY

When a Christian takes the first step in non-violent protesting,
he needs to ask himself these questions:

“What am I being asked to do?”
“Why am I being asked to do this?”
“Am I biblically justified in doing this?”
“What is the likely cost of my doing this?”
“What is the likely outcome of what we are doing?”

There have been many protest groups in history that have
claimed to be Christian, some good and some evil. The apocalyptic
communist revolutionaries of the thirteenth and fourteenth centu-
ries were evil, The communist and polygamous radical Anabaptist
revolutionaries of Luther’s day were evil. So, the Christian must
ask himself this question: By what standard? By what standard is the
particular protest group evaluating its legal and moral right, as
well as the tactical and strategic wisdom, of organizing this pro-
test? By what standard is it imposing its discipline? By what
standard does it pick its targets?

To answer these questions, you need a theology. You need
covenant theology. No other theology offers equally consistent, Bible-
based answers. This theology provides a comprehensive world-and-
life view, one which incorporates a theory of obedience (covenant-
keeping) and a theory of disobedience (covenantbreaking). It is
the Bible which must provide us with our standards and our
definitions, not humanism, whether secular or Christian (natural
law theory).

165
