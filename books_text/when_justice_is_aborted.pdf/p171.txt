Are Operation Rescue’s Critics Self-Serving? 153

deductible checks — to the hard-pressed families of those men who
have been put in jail or prison for their public testimony.

But not the critic. What he wants is prayer in the solitude of
his prayer closet. There are no lawsuits in prayer closets. It is nice
and safe there. For now,

Let the Other Congregations Repent First

The church must repent first, if America is to be saved from divine
judgment, and if America is going ta stop killing babies.

Of course “the church” must repent. (When a pastor says “the
church,” he really means the church across the street that just
persuaded three families to leave his church and transfer their
membership. A “dangerously radical church” is the church across
town whose stand against abortion recently persuaded his church’s
only tithing millionaire to transfer.) Judgment surely begins at the
house of the Lord. “For the time is come that judgment must begin
at the house of God: and if it first begin at us, what shall the end
be of them that obey not the gospel of God?” (I Peter 4:17).

Question: What is a tactic of non-violent, anti-abortion, bodily
interposition, if not the first stage of the church’s repentance?
Must repentance forever be confined strictly to the heart? Must it
be forever trapped inside of the local church’s four walls? Is it
limited to favorable write-ups in the society column of the local
newspaper rather than critical editorials and front page headlines?
Isn’t repentance supposed to be a public turning away from sin?

And isn’t tactically unnecessary cowardice in the face of legal-
ized murder a sin to be repented of publicly?

God's national covenant works differently from the way this
critic thinks. If America is going to gain enough time to repent,
Christians had better persuade the Supreme Court to reverse Roe
x. Wade, or Congress to remove the Court’s jurisdiction over abor-
tion (for which there is Constitutional precedent: Ex parte Mc
Cardle, 1868).
