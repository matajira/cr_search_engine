The Voice of Lawful Authority 49

rule is suitable temporarily during a war, but it is not the organiza-
tional standard in a free society when each person is made person-
ally responsible for fulfilling God’s command to subdue the earth
(Genesis 1:26-28). Bureaucracy cannot make full use of the divi-
sion of labor principle except when the assigned task of the organi-
zation is very narrow and universally accepted by a large majority
of citizens.

Satan is not all-seeing or all-powerful, so. he has to issue
commands to a massive bureaucracy of both human and demonic
followers, Yet even he cannot issue perfect commands; even he has
to allow for some latitude in his subordinates’ literal obedience.
He is a creature. He seeks to compensate for his lack of omnis-
cience by strengthening the power of his bureaucracy.

Church government, like family government and civil govern-
ment, is always hierarchical. There are officers in a church: dea-
cons and elders. They have to meet exacting moral and family
discipline requirements in order to serve as officers (I Timothy 3).
Church officers are required to settle disputes that arise between
church members (I Corinthians 6), But the appeals usually arise .
from below. The leaders of the church, being voluntary (in con-
trast to minor children in a family) must lead by example. They
do not issue commands, except when formally deciding a case
brought to them. They announce God's law from the pulpit. The
church can lawfully initiate a covenant lawsuit against rebellious
members, but a church that does this continually will not hold its
members. Cults are marked by continual top-down monitoring;
churches are marked by self-government under God’s law.

Churches are sometimes organized as independent congrega-
tions. Rare is the local church, however, that is not connected in
some way to an association of other churches with similar beliefs.
Sometimes churches are organized hierarchically as denomina-
tions, that is, hierarchically. The chain of command is formal.
Higher governmental bodies are allowed to impose discipline on
individuals through the subordinate authority of their local churches.
But there can be no church apart from some sort of hierarchical
