Preface xv

even brought their children from out of state to enroll, just to give
the kids an opportunity to take part in an historic event.

In response to the preliminary phases of this Nebraska school
war, I decided in 1982 as co-editor of Christianity and Civilization, a
scholarly journal being published by Geneva Ministries of Tyler,
Texas, to produce a journal dealing with Christian resistance. I
sent out letters to prospective authors, asking them if they had
anything to contribute. 1 was flooded with responses. Eventually,
we published two volumes: The Theology of Christian Resistance,
which was over 350 pages, and Tactics of Christian Resistance, which
was almost 500 pages. I carried copies of the second volume to
Nebraska when I visited in early December of 1983. I wrote up
the story in the December 16, 1983 issue of my newsletter Remnant
Review.

T immediately received cancellations from “Christian conser-
vatives” in Nebraska. They were outraged at my report. I was
uninformed, they said. Sileven was a troublemaker and an outlaw,
they said. We should ail Ilcave Nebraska alone, they said.

For weeks, revolving teams of twenty pastors each had gone
to the governor’s office for a meeting. “I will not meet with those
Jawbreakers,” he vowed. These groups still came to his office, week
after week. At last, he met with them. Then, in desperation over
the national media coverage and also about the state’s inability to
shut down the schools, he created a blue ribbon commission of
experts fromm outside the state to study the matter. Much to the
legislature’s consternation, the panel said the state was way, way
out of line, ‘That was the beginning of the end for the State of
Nebraska’s war on Christian schools.

Next, a lawyer provided to the church by a national Christian
ministry sued in Federal District Court and won. The county had
indeed violated the First Amendment rights of the pastors when
the sheriff dragged them out of that prayer meeting. Next, the
sheriff had a heart attack and resigned.

Finally, the state capitulated. It passed a law that virtually
freed Christian schools from all state control. The “outside agita-
