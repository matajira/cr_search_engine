Are Operation Rescue's Critics Self-Serving? 159

Conclusion

As a person committed to covenant theology, I am appalled
at the intellectually lightweight and Scripturally bankrupt sheep-
fold: manifestos that I have seen so far. The ones offered in the
name of God’s covenant are the greatest embarrassment to me.
Their arguments do not differ significantly from the manifestos
that have poured out of “Fundamentalists for Pro-Life, Sort Of
pastors.

The intellectual bankruptcy of some of the published criticisms
of Operation Rescue does not automatically legitimize Operation
Rescue. We should not be lured into the mistake of getting on a
controversial bandwagon just because those who say we should
stay home are not intellectually or theologically capable of defend-
ing their negative position.

I have discussed Operation Rescue as a real-world example
of non-violent Christian resistance. I see nothing wrong with what
they have done, as of late 1988. I have serious reservations about
where the group may be in a few years, or where its radical
spin-offs may be. But in a time of social, moral, political, and
medical turmoil, as the 1990’s will almost certainly be, it is impos-
sible to be sure where any group will be.

What we need from Operation Rescue is an official statement
of tactical and strategic faith. We need a statement that under no
circumstances will Operation Rescue or any of its official represen-
tatives call for armed resistance to civil authority without public
support from a lesser magistrate. We need a statement that vio-~
lence will not be initiated by Operation Rescue groups against the
bodies of private citizens, except for unarmed physical interposi-
tion: separating murderous physicians from their clients and tar-
geted unborn victims. We need also a statement that the deliberate
destruction of the actual tools used by licensed murderers in their
crimes will endanger only the property and not any person.

As a matter of tactics, it would be nice to hear that Operation
Rescue has advised all participants of the need to fight this battle
through the courts, and has recommended to everyone who gets
