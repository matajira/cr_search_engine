Are Operation Rescue’s Critics Self-Serving? 143

tion Rescue, you need to ask yourself these two questions: 1) If the
arguments are truly preposterous, does the manifesto writer have
a hidden agenda? 2) What is this hidden agenda?

Abortion Is Not Compulsory

Roe v. Wade is unlike commands by civil rulers requiring citizens to
perform evil acts, It does not require that anyone abort her baby.

This is the most imbecilic argument of them all. To see just
how ridiculous this argument really is, substitute the word “mur-
der” for “abort.” We get the following piece of moral and judicial
nonsense: “A law legalizing murder does not require a citizen to
murder anyone.” Does this make the legalization of murder legiti-
mate? Is a law that legalizes murder anything but perverse? So,
what should we call such an argument? Thoughtful?

A civil law does not have to command people to do something
evil in order for the law to be evil. Neither the Sanhedrin nor
Caesar’s representatives commanded the apostles to preach any-
thing evil. They just forbade them from preaching what is true and
what is required by God that all Christians preach. So the apostles
disobeyed the civil and religious authorities. They knew it was. an
evil law. They knew that God did not want them to obey it.

Civil Jaws are almost always framed negatively. They forbid
evil acts. They establish punishments for people who commit evil
acts. This is the biblical standard for civil law. A mark of the
coming of satanic law is when the state starts passing laws that
force people to do “good” things. The state has then become
messianic, a savior state. Seldom in our day does an evil law bear
this mark of Satan: that it commands pcople to do evil things.
Almost always an evil civil Law legalizes something which is evil in itself.
Sometimes an evil law will forbid what is righteous. Rarely will it
actually command people to do something immoral.

The abortion laws authorize something evil: murder. Local
trespassing laws are now being used to prohibit something right-
eous: saving judicially innocent lives. The fact that there is no
Federal law compelling mothers to abort their children is utterly
