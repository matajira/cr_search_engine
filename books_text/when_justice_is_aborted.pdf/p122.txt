104 When Justice Is Aborted

How Many Generations of Peace?

The third commandment says, “I the Lorp thy God am a
jealous God, visiting the iniquity of the fathers upon the children
unto the third and fourth generation of them that hate me; And
shewing mercy unto thousands of them that love me, and keep
my commandments.”

When. God says that He visits the iniquity of the fathers, He
means that He visits rebellious society and sees it, generation after
generation. God does not break into history with His comprehen-
sive covenant sanctions at the first sign of national iniquity. He is
patient. He is merciful. He extends time to that society for public
repentance. But He visits iniquity, generation after generation.
He sees and does not forget. Then, in the third or fourth generation
after the initial public iniquity began, He brings His negative
covenantal sanctions. This is the essence of social discontinuity.
This is the essence of revolution. It is God’s revolution against
covenantal unrighteousness.

In contrast, God shows mercy unto thousands of those who
love Him and keep His commandments. What does “thousands”
refer to? It has to mean thousands of generations. The words “third
and fourth generation” are contrasted to “thousands.”

Does this mean that history will go on for at least 40,000 years
(i.e. 40 years per generation)? Not necessarily. The term “thou-
sands” is symbolic. It means “till the end of time.” God’s covenant
blessings are continuous and endless if men remain faithful to the
terms of God’s covenant. Generation after generation, people in-
herit the blessings of God. This is the covenantal doctrine of
inheritance, It is the basis of anti-revolutionary social continuity.

The curses come at the end of much shorter intervals, While
good is allowed to compound and grow over time, evil is cut short
in the midst of time. Social continuity is available only to those
societies that remain covenantally faithful to God. This is the
positive sanction of God: blessing. Social discontinuity is the inevi-
table result of corporate covenantal rebellion that persists for three
or four generations. This is the negative sanction of God: cursing.
