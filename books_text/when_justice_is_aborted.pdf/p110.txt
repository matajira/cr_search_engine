92 When Justice Is Aborted

those who murder judicially innocent unborn infants do not under-
stand the looming problem facing modern society. The sanctions
are coming. (If AIDS is any indication, as I believe that it is, the
sanction is already here.) If praying an imprecatory psalm is a
worse offense in the eyes of a Christian than the crime of abortion,
then that Christian is fleeing from God, one way or another.

The task of being an intercessor in prayer is not denied by
Christians. A man intercedes at the throne of God in the name of
others. This is the biblical meaning of the word saint. It is someone
who has lawful access to God’s sanctuary. He is set apart because he
is morally sanctified by God’s imputation of Christ’s righteousness
to him, But a saint brings sanctions. He calls for God to impose
physical sanctions on His enemies. And God may then call him
to move from being an intercessor to an interposer. He calls him
to interpose positive sanctions — the preservation of a judicially inno-
cent life—by bringing: upon himself the negative sanction of the
state.

The next stage of protest is civil disobedience. Christians jam
the doorways of the abortion mills. While the timing of each stage
of escalating prophetic protest is difficult to judge, there is no
doubt that doing nothing is in fact domg something. It is allowing
society to come under the sanctions of God in history. It is interest-
ing that the following passage’ is used by trainers in evangelism
(soul-winning) in the personal salvation sense, but not in its corpo-
rate covenantal transgression and judgment sense, which is what
the passage deals with:

Son of man, speak to the children of thy people, and say unto
them, When I bring the sword upon a land, if the people of the
land take a man of their-coasts, and set him for their watchman:
If when he seeth the sword come upon the land, he blow the
trumpet, and warn the people; Then whosoever heareth the sound
of the trumpet, and taketh not warning; if the sword come, and
take him away, his blood shall be upon his own head. He heard
the sound of the trumpet, and took not warning; his blood shall
be upon him. But he that taketh warning shall deliver his soul. But
if the watchman see the sword come, and blow not the trumpet,
