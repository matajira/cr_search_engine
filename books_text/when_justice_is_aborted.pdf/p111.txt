Whose Sanctions Will Préoail? 93

and the people be not warned; if the sword come, and take any
person from among them, he is taken away in his iniquity; but his
blood will I require at the watchman’s hand. So thou, O son of
man, I have set thee a watchman unto the house of Israel; therefore
thou shalt hear the word at my mouth, and warn them from me.
When I say unto the wicked, O wicked man, thou shalt surely die;
if thou dost not speak to warn the wicked from his way, that wicked
man shall die in his iniquity; but his blood will I require at thine
hand. Nevertheless, if thou warn the wicked of his way to turn from
it; if he do not turn from his way, he shall die in his iniquity; but
thou hast delivered thy soul (Ezekiel 33:2-9).

See what it says? If the watchman refuses to warn the people
of their ethical transgression, then the negative sanctions will be applied
to the watchman. This is not figurative language. This is not to be
allegorized away. This is God speaking, and His Word is sure.

This is the language of the sword. The text shows us that it is
not the prophet who wields the sword; the prophet wields the
covenant. God does not call the prophet or watchman to execute
physical judgment on sinners; he calls on him to warn sinners of
impending physical judgment, God uses other agents. than proph-
ets to wield His sword or rod of wrath. But He does bring it
eventually,

Tf the church remains silent in our day, then we can expect the
sword, the famine, and the plague. Most Christians deny this fact,
either openly or in their hearts. If so, they have become false
prophets, if only to themsclves: “Then said 1, Ah, Lord GOD!
behold, the prophets say unto them, Ye shali not see the sword,
neither shall ye have famine; but I will give you assured peace in
this place” (Jeremiah 14:13).

There is one final stage of protest: armed revolution. This is
lawfully launched only by lower magistrates. This is the Protestant
doctrine called the doctrine of interposition. John Calvin discussed
it in Chapter 20 of Book 4 of his Institutes of the Christian Religion. It
was also taught by the anonymous “Junius Brutus” in the Vindiciae
Contra Tyrannos, which was published in 1581, also known as A
Defence of Liberty Against Tyrants. This book was translated into
