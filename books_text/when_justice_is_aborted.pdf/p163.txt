Are Operation Rescue’s Critics Self Serving? 145

ment, The deacons have imported the idea of “pro-choice” abor-
tionists into the church by changing the phrase to “free moral
agent.” This is one more example of how Christians baptize the
language and ideas of secular humanism.

Is a murderer an equally “free moral agent”? This church’s
deacons implicitly say so. Is “free moral agency” under God a
license from God to escape the God-ordained civil sanction of
public execution for murder (Gen. 9:5)? The U.S. Supreme Court
has eliminated this sanction, or any sanction, and this. diaconate
has now baptized the Court’s decision. They are saying, in princi-
ple, that the U.S. Supreme Court is the highest court in America;
God’s Supreme Court gains jurisdiction only after we die.

Was Pharaoh’s court the highest court in Egypt?

T would also ask this: Is it lawful for Christians in Communist
China to resist their civil magistrates today, since abortion is
compulsory there after the first child? Would these deacons say
that it is immoral for Western Christians to smuggle Bibles into
Red China, as well as tracts showing the Chinese ways to resist
this evil compulsory abortion law?

Are Christians so downright blind today that they cannot see
what will come next if Roe u Wade isn’t overturned? Will the civil
magistrates have to drag our wives and daughters to the compul-
sory abortion mills before these shepherds figure out that Roe a
Wade is in fact only stage one in the humanists’ program of
legalized euthanasia? In Holland, mercy killings have now been
legalized; first abortion was legalized, then the murder of the aged.
But these shepherds still have not caught on.

In 1925, the humanists said that all they wanted to do was to
get Darwinian evolution taught in the public schools alongside the
creation story. “That’s all we’re asking. We promise. Trust us!”
Christians did, too. Surprise!

Bait and Switch
Armed resistance by Christians is illegitimate except when a lesser
