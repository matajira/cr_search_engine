vis] When Justice Is Aborted

A far better way to speak out is to sing. Singing drives the
authorities nuts. It is also effective on television news clips, The
early Christians sang as they were herded into the Roman Colos-
seum; it impressed many people and outraged others. But it
demonstrated that Christians did things differently from other
victims of injustice. Singing can be done as loud as you want,
although quiet singing is impressive. Song sheets should be handed
out to protesters in advance of every local protest. But leaders
should recognize that people probably cannot sing enthusiastically
hours on end, Singing is appropriate as the police’s attacks esca-
late. It is a way to protest physical injustice under the threat of
violence, thereby rechanneling the initial emotional response to
scream or fight back physically.

Audible crying by participants must also be avoided. The
emotional setting of an abortion clinic is conducive to crying by
Christians. As soon as any protester begins crying audibly, except
because physical pain inflicted by the police, the leader must send
in a representative to ask the crying protester to move away from
the group while it is involved in the actual demonstration.

There is also the danger of protesters who carry weapons.
Some may do this because they are not committed to non-violent
protesting. Others may be agents provocateurs who have been sent
into the group in order to force a violent demonstration or to
embarrass the group publicly. Before anyone is allowed into the
main line of protesters, he or she must be asked by a group
representative to pull out all pockets or open a purse for inspection,
Male representatives can deal with the men, females with women.
Every protester must be screened in advance. There must be no
exceptions. These are organized protests. The organizers must do
their work thoroughly. They must make it plain to the public and
to the protesters that the group is self-policed.

But Will the Public Respond Favorably?

Tf the nation is so deeply immersed in sin that the voters do
not throw out any politician who allows abortion to continue, how
