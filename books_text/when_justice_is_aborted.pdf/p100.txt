82

When Justice Is Aborted

13. The modern state operates in terms of sentiment and
power, not permanent moral principles.

14. Non-violent protests can get out of control.

15. Christians must begin a protest with this presupposition:
Protests cannot save mankind.

16. The state cannot save mankind, either.

17. To trust the state to save is to guarantee frustration.

18. Christians must take the moral high ground.

19. The immoral and violent reactions by_ civil authorities
show the public who is on the moral low ground.

20. The political goal of the protest is to arouse the ire of the
public against civil injustice.

21. This tactic requires visible victims.

22, The level of victimization identifies those on the high
moral ground.

23, The public cares more about visible victims than about the
hidden victims (unborn infants).

24. There are four moral goals of the protest: upholding God,
saving the greatest number of innocent lives, changing the minds
of the public, and bringing evil-doers to justice.

25. Life is a positive goal.

26. The basic tactical principle of protest is Gideon’s: The
action is the reaction.

27. Lives saved nationally should be the national strategic
goal, not lives saved locally.

28. The strategy is to change the minds of voters and Supreme
Court judges.

29. Protesters should assume that the civil authorities will
escalate their violence.

30. Christians must adopt institutional rules that will reduce
the likelihood of violence and bad manners within the ranks of the
protesters.

31. The protesting group should be committed to the five
covenant rules of protest.

32. We need a fanaticism of relentless perseverance.

33. Protesters should first count the cost,

34, The goal is long-term political victory.

  
