4 When Justice Is Aborted

financial losses for the clinic. It also could become a tremendous
media event in which the absolute brutality of abortion is reflected
in the brutality of the local police against protesters. But to block
the doorway is an invasion of the clinic’s private property. The
protests have begun, and the police have started arresting those
who block the doorway. Should you approve of the protest or not?
Tf you approve, should you join the protest or not? If you suspect
that the police will escalate their physical violence against pro-
testors, should you join the protest? If you get arrested, should you
later insist on a jury trial or meekly forfeit the bail you posted in
order to be released?

Ifno Christians protest, will the abortion laws ever be changed?

What If a Civil Law Is Biblically hmmoral?

The civil government could declare a particular act illegal
which in God’s eyes is legal or moral, The civil government could
also declare something legal which in God’s eyes is illegal or
immoral. How can those under the authority of the specific civil
government in question persuade the civil authorities to bring the
law into harmony with God’s law?

The first step is for Christians to accept the fact that there really
is such a thing as God’s law. If Christians deny this, then their
protests are in vain. They must first seck explicitly biblical answers
to the question: “By what judicial and moral standard?”

Second, Christians must decide which doctrines and practices
are most important in God’s hierarchy of values and requirements.
The color of the drapes is less important than the purity of doc-
trine. Most Christians say that they believe this, But what about
applied doctrine? What about a question like abortion? What if a
church preaches sound doctrine but attempts to stay neutral about
abortion? There is no neutrality in God’s world, of course, but
there is lots of attempted neutrality. (There surely also is a great
deal of indifference.} Christians must decide which unjust laws to
obey and which to disobey, since no one can fight every aspect of
civil injustice at one time. We are creatures. No one has sufficient
