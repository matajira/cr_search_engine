7” When Justice Is Aborted

overall strategy of national victory.

Applying This Strategic Principle

Consider the anti-abortion cause. The protesters should be
clear in their minds what the overall strategy of the protests should
be: the ending of abortion. But this goal will not be achieved
through the initial confrontation. Gideon’s trumpet was not ex-
pected to destroy the enemy at that moment; it was rather a means
of gaining a response that would weaken: the enemy’s potential
counter-attack.

It is not the lives of the local unborn babies that the local
anti-abortion protest should focus on. There will be few lives saved
initially, There is always another abortion mill down the street or
outside of town; murderous women always have another opportu-
nity to abort their offspring. What really counts is the total number
of lives saved after the voters change their minds or the Supreme
Court at last reverses itself on the abortion-on-demand question.
Thus, tactics of local civil disobedience must be designed and
enforced that produce the sought-after national judicial goal, not
the short-term goal of saving lives locally,

What the organized protests should be designed to accomplish
is the national reduction of the opportunities to commit legalized
murder. This reduction may come because other physicians and
hospitals become frightened of the bad publicity, and they then
decide to stop making abortions so easy for mothers to buy. The
reduction may take place because voters at last change their
minds. What must be understood well in advance is this: a protest
that temporarily hampers a local clinic but whose tactics turn off
the television-viewing or newspaper-reading audience has not been
an effective protest. Few lives will be saved locally, and none
nationally.

Remember the first rule of civil disobedience: The action is
the reaction. Before anyone performs acts of civil disobedience,
he must have a reasonably clear picture. of the reaction he is
seeking to produce. He must do all he can to think through the
