Introduction 7

That triggered something in Sutton’s mind. Kline discusses
the outline of these treaties in several places, In some places, he
says they have five sections; in other places, he indicates that they
may have had six or even seven. It was all somewhat vague. So
Sutton sat down with Deuteronomy to see what the structure is.
He found five parts.

Then he looked at another book of the Bible that is known to
be divided into five parts: Matthew. He believes that he has found
the same structure. Then he went to other books, including some
Paulirie epistles. He found it there, too. When he discussed his
findings in a Wednesday evening Bible study, author David Chil-
ton instantly recognized the same structure in the Book of Revela-
tion. He had been working on this manuscript for well over a year,
and he had it divided into four parts. Immediately he went back
to ‘his computer and shifted around the manuscript’s sections
electronically. The results of his restructuring can be read in his
marvelous commentary on the Book of Revelation, The Days of
Vengeance (Ft. Worth, Texas: Dominion Press, 1987),

Here, then, is the five-point structure of the biblical covenant,
as developed by Sutton in his path-breaking book, That You May
Prosper: Dominion by Covenant (Tyler, Texas: Institute for Christian
Economics, 1987).

1. The transcendence yet presence of God
2. Hierarchy/representation (government)
3. Ethics/law (dominion)

4. Oath/sanctions (blessings and cursings)
5. Succession/inheritance (continuity)

Simple, isn’t it? Its acronym is THEOS. Simple though it is,
it has many important implications. Here is the God-revealed key that
unlocks the structure of every human government. Here is the biblically
mandated model of government that Christians can use to analyze
church, state, family, and numerous other non-covenantal but
contractual institutions,

The first five books of the Bible, the Pentateuch, conform to
this five-point outline, Genesis tells us who God is: the sovereign
