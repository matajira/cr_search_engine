Making Hay: The U.N. Resolution 49

submitting to the rule of this egalitarian law, the General
Assembly has consistently and insistently attacked Western
industrialized nations as “evil,”8> and even “heretical.”8 This
quickly became a primary test of “U.N. Orthodoxy,” says Paul
Johnson. “High Western living standards, far from being the
consequence of a more efficient economic system, were consid-
ered the immoral wages of the deliberate and systematic impov-
erishment of the rest of the world. Thus in 1974, the U.N.
adopted a Charter of Economic Rights and Duties of States that
condemned the workings of Western economies. The 1974 U.N.
World Population Conference was a prolonged attack on U.S.
selfishness. The 1974 U.N. World Food Conference denounced
America and other states, the only ones actually to produce food
surpluses. The Indian Food Minister thought it ‘obvious’ they
were ‘responsible for the present plight of the poor nations,’ and
had a ‘duty’ to help them. Such help was not ‘charity, but ‘defer-
red compensation for what has been done to them in the past by
the developed nations.’ The next February, the ‘non-aligned’
countries castigated ‘the obstinacy’ of the ‘imperialist powers’ in
preserving the structures of ‘colonial and nco-colonial exploita-
tion’ which nurture their Juxurious and superfluous consumer
societies,’ while they keep a large part of humanity in misery and
hunger.”*” And this, despite the fact that “during the previous
fourteen years alone (1960-1973), official development aid from
the advanced nations direct to the poorer countries, or through
agencies, amounted to $91.8 billion, the largest voluntary trans-
fer of resources in history.”8 The messianic U.N., wielding the
double-bladed axe of egalitarian law, would not be satisfied until
an absolute leveling of resources had been accomplished — until
the West was reduced to the same ruinous chaos as the rest of the
world,

This is why, when the U.N. stages International Years, the
specious causes to which they are dedicated are nothing more
than a pretext, a propaganda platform, for the advancement of
the globalist, egalitarian, and messianic crusadc.

The International Year of the Woman (1979), for instance,
was simply an excuse to bludgeon the already guilt-racked West
even more, One prominent feminist asserted: “This entire ‘Year’
business has proven to be a farce. Sure, there are still inequities
and gross displays of unabashed chauvinism here in the West.
