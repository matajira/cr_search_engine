Aubade: A Biblical Hope 231

up a weak and unesteermed people against all odds, and win?
Isn’t it?

For though we walk in the flesh, we do not war ac-
cording to the flesh, for the weapons of our warfare are
not of the flesh, but divinely powerful for the destruction
of fortresses. We are destroying speculations and every
lofty thing raised up against the knowledge of God, and
we are taking every thought captive to the obedience of
Christ (2 Corinthians 10:3-5).

We are invincible (Ephesians 6:10-18; Romans 8:37-39).
Even the gates of hell shall not prevail against us (Matthew
16:8). If, that is, we would only do our job. If we would only take
the Gospel hope beyond, to “the uttermost parts of the earth”
(Acts 1:8), if we would only “make disciples of all nations” (Mat-
thew 28:19), if we would only “rebuild the ancient ruins . . . raise
up the age old foundations . . . and repair the breach” (Isaiah
58:12) by caring for the poor, the afflicted, and the dispossessed
(Isaiah 58:10).

It is time to go to work.

We may have to go it alone for a time. That didn’t stop David
(i Samuel 17:40), so it shouldn’t stop us.

We may have to work with few, or even no resources. That
didn’t stop Jonathan (1 Samuel 14:6), so it shouldn't stop us.

We may have to improvise, utilizing less than perfect condi-
tions and less than qualified workers and less than adequate fa-
cilities, That didn’t stop Peter, James, and John (Acts 4:20), so it
shouldn't stop us.

We may have to go with what we've got, with no support, no
notoriety, and no cooperation. That didn’t stop Jeremiah ( Jere-
miah 1:4-10), so it shouldn’t stop us.

We may have to start ‘in weakness and in fear and in much
trembling” (1 Corinthians 2:3), without “persuasive words of wis-
dom” (1 Corinthians 2:4), That didn’t stop the Apostle Paul
(t Corinthians 2:1) so it shouldn’t stop us.

It is time to go to work.

Dominion doesn’t happen overnight. Victory doesn’t come in
a day. So the sooner we get started, the better off welll be. The
sooner we get started, the quicker the victory will come. In order
