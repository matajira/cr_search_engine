Duende Dancehall: The Crists 27

down in the subways, and the angry young street toughs up in
Harlem. You can’t get away from them. They are everywhere.”

In Washington, D.C., the fact that the President's nearest
neighbor on Pennsylvania Avenue is a shuffling, twisted, filthy
shell of a man sleeping fitfully on a sidewalk heating grate has
become epigrammatic of the problem.'® He, and the other
10,000 to 13,000 displaced and dispossessed like him in the na-
tion’s capitol, symbolize the magnitude of the problem.1”

It is a national problem.

“Homelessness is a massive epidemic,” a congressional com-
mittee report declared recently, “so overwhelming that the prob-
lem must be treated as a national emergency.”!®

According to George Getschow of the Wall Street Journal,
“Across the United States, tens of thousands of families and indi-
viduals . . . have joined the ranks of the homeless, jobless, and
dispossessed.”!9 He goes on to say, “A recent report by the U.S.
Conference of Mayors says thousands of families have been
evicted from their homes and are living in cars, campgrounds,
and rescue missions,”2°

The motley ranks of America’s homeless have swollen to out-
landish proportions. In Cincinnati, where according to the City
Housing Assistance Program 29% of the citizens were found to
be in need of sheltering aid,?! there are approximately 2,000
homeless men, women, and children.” In Tulsa, a city that has
boasted one of the lowest unemployment rates in the nation,
homelessness has continued to climb to an estimated 1000 per-
sons today.”? Denver has gone from boom to bust and back again
so many times over the last decade that the fallout and shakeout
has resulted in a homeless population of about 2,500.% Cleve-
land, where the Salvation Army has been forced to open five new
soup kitchens just since 1982, all in predominately blue collar
neighborhoods, is facing a homeless population of nearly 1,500."

“Sunbelt” cities, especially hard hit by the exodus of workers
from the “rustbelt,” face catastrophic conditions. Houston has
between 15,000 and 20,000 horeless.2° Santa Monica has 3,000.7
Orlando has 3,000.% San Antonio has 23,000.” Atlanta’s first
shelter opened its doors to the homeless in 1979,3° Now the city
has twenty-seven different operations.3! Salt Lake City’s mayor
complains that his city has become a “blinking light” for dispos-
sessed transients, ?? In Tucson and Phoenix, officials are worried
