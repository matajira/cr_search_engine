46 Tue DispossesseD

Secondly, the program argued that “eviction is contrary to
this right of shelter.”

Thirdly, the program allowed for intervention in the private
sector so that the first two provisions could be enforced. “In situ-
ations of housing shortages, the member states must be pre-
pared,” the program says, “to intervene in the free market to reg-
ulate rents and provide financial assistance to individuals unable
or unwilling to pay for private sector accommodation.”®

Finally, the program called for a comprehensive legal overhaul
of member-states: the repeal of vagrancy laws, the decriminaliza-
tion of trespassing laws, universal legal, medical, psychiatric and
educational services, and the establishment of a “minimum social
guarantee,”65

In short, the commission called for the abolition of private
property, and the complete socialization of the European Eco-
nomic Community.

Landlords would no longer be able to set rent values on their
properties commensurate with the market. They would no
longer be able to protect the integrity of those properties through
eviction or selective leasing. And, they would more than likely
no longer be able adequately to maintain those properties due to
decreased income and increased socially directed taxes.

That is the crux of the Third World models.

And this is what the U.N. wishes for us. This is, for all in-
tents and purposes, the whole reason the U.N, declared 1987 the
International Year of the Homeless: to centralize control over
property worldwide in general, and to centralize control over
property in the U.S. in particular.

Thanks. But, no thanks.

 

The Origin of the Specious

Clearly homelessness is a serious problem, even a crisis. To
advocate reform is certainly commendable. To struggle on be-
half of the dispossessed is a worthy cause. But the U.N. has long
shown a unique ability to warp worthy causes, twisting them to
malevolent ends.

Consider the cause of peace.

The U.N. was founded in 1945 as “man’s dast hope for
peace”® a purpose somehow more blasphemous than noble.
Certainly the maintenance of world peace is a worthy cause. But
