48 Tue DispossesseD

has been relegated to the sidelines while the power, influcnce,
and bureaucratic control necessary for the realization of those
messianic aspirations are focused and consolidated. World Peace
has become, over the years, nothing more than a specious pre-
text for the establishment of the U.N.’s read goal: World Control.

The U.N. Legacy

The U.N.’s messianism, though more often than not veiled,
has been evident from the start.

The U.N. charter in its Preamble announces its salvific pur-
pose, declaring that “We the people of the United Nations deter-
mined to save . . . have resolved to combine our efforts to accom-
plish these aims. . .® As R. J. Rushdoony has commented,
“Man needs a source of certainty and an agency of control: if he
denies this function to God, he will ascribe it to man and toa
man-made order, This order will, like God, be man’s source of
salvation: it will be a saving order.”®!

How would the U.N. usher in its “salvation”? How would it
bring about its grand “New Age” or “World Order” and “Interna-
tional Unity”? Very simply, it would institute a new canon of
law. According to Rushdoony, “The U.N. holds as its basic
premise a thesis which has a long history in both religion and in
politics, the doctrine of salvation fy law, It belicves that world
peace can be attained by world law.”8?

And what is the nature of this salvific law?

It is a law rooted in absolute egalitarianism, first and fore-
most. As Rushdoony has asserted, “The goal of all humanists,
all advocates of the religion of humanity, is the unity and one-
ness of all men.”* Thus, anything that divides or stratifies man-
kind, be it national boundary, economic condition, cultural
diversity, or religious exclusivity, is wrong and must be criminal-
ized. The salvific law of the U.N. would level all men every-
where, equally. The rich would be forced to distribute their
wealth to the poor, The advanced would be forced to share their
technology with the primitive. The blessed would be forced to
contribute their advantage to the cursed. The powerful would be
forced to dole out their might to the weak. Thus, egalitarian law
is, according to Herbert Schlossberg, “The dual effort to raise
the lower classes and debase the higher. . . .”8

Bowing its knee to the U.N.’s messianism, and obediently
