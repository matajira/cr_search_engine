146 Tue Dispossessep

Parliament went from one extreme to another. When at
times English social welfare policies seemed a bit schizophrenic
—unabashed enclosure during the Great Depression of 1854:
settlement; and unremitting triage during the Irish Potato Fam-
ine of 1846: resetilement—it was due to these two dramatically
divergent traditions.

Without the clear direction of Scripture, it seems, men left to
their own devices are tempest tost to and fro on the waves of
doubt, dashed from dire to drastic with no in between.

Carrying on the Traditions

Interestingly, both extremes — settlement or containment and
resettlement or consignment — are still well represented in mod-
ern social welfare policies, Even in the U.N.’s recommendations
for the International Year of the Homeless, the two traditions are
readily apparent.

“In order to stabilize migratory movements and transitional
panics,” says one U.N. publication, “residency requirements,
density level enforcements, and decentralization effectualization
will be essential. Homelessness cannot hope to be addressed un-
til sample populations are fixed.” This containment ideal is
best exemplified by the efforts of the Communist governments in
Guyana, Angola, and Nicaragua. The UNCHS looks admiringly
at Nicaragua’s settlement program, calling it “an ambitious plan
to the year 2000” which aims at “the decentralized distribution of
population from one national center— Managua — through nine
regional centers, with 20,000 to 100,000 inhabitants and a catch-
ment area of 50,000 to 500,000, to nineteen secondary
centers... and fifty-two service centers.”3°

The idea of all this is, very simply, to keep the poor in their place.
Stop migration. Inhibit transtency. Hold homelessness at bay by
ghettoizing the poor.

Though certainly in the U.S. such repressive measures are
not allowed, containment sentiments can still be discerned in
rent control,3! industrial regulation,*? and farm subsidy meas-
ures,33 In addition, 46 of the 50 states have residency require-
ments for relief applicants that also effectively enforce the settle-
ment mindset. Each of those policies serve in one way or
another to stifle opportunity and stymie change.

Simultaneously and quite schizophrenically, modern social
