264 Tue DispossesseD

41, Ibid.

42. Ibid., p. 2

43. Lucy Komisar, Down and Out: A History of Social Welfare (New York: Frank-
lin Waits, 1973), p. 3

44. Lens, p.

45. Latimer Allwin-Jones, The Industrial Revolution (London: Crier Press,
1961), p. 129,

46. Ibid,

47. Ibid.

48. William Widowford, The Keepers of the Poor (New York: Carouthers Bind-
ery, 1922), p. 86.

49. Karl de Schweinitz, England’s Road to Social Security (Philadelphia: Univer-
sity of Pennsylvania Press, 1943).

50. Sec Jacob Riis, How the Other Half Lives (New Vork: Hill and Wang, 1957);
and Alfred T. White, £mproved Dwellings for the Laboring Classes (New York:
Daybreak Books and Reprints, 1961).

 

Chapter 8— Epitaph in Rust: Unemployment

- Johnson Oatman and Edwin O. Excell, “Count Your Blessings,” 1919.

Ibid.

Ibid.

Ibid,

. Ibid.

. The New York Times, November 28, 1979

. Harvey Olander, The Rust Relt (Cleveland: Treaty Township Press, 1985),

p. 61

8. Ibid., p. 62.

9. Ibid., p. 65.

10, Thid

11, Randy Barber and Jeremy Rifkin, The North Will Rise Again: Pensions,
Politics, and Power in the 1980s (Boston: Beacon Press, 1978), p. 59.

12, John Naisbitt, Megatrends (New York: Warner Books, 1982).

13, Barry Bluestone and Bennett Harrison, The Deindustrialization of America
(New York: Basic Books, £982).

14. Barber and Rifkin, pp. 59-77,

45, Bluestone and Harrison, p, 4.

16. Ibid.

17. Ibid.

18. Ibid

19. Ibid

20. George Grant, Bringing in the Sheaves (Atlanta: American Vision Press,
1985), pp. 168-171,

21. Tom Peters and Nancy Austin, A Passion for Excellence: The Leadership
Difference (New York: Random House, 1985); and John Naisbitt and
Patricia Aburdenc, Reinventing the Corporation (New York: Warner Books,
1985).

22, Lola Peterson, Resistance to Change: Business, Family and Faith (Los

Angeles: Peterson Publications, 1981).

NOWRONS
