H2 Tue Dispossessep

dered the situation for the thousandth time. The haunting
refrain of an old hymn rang in the hallows of his mind.

When upon life’s billows you are tempest tossed,
When you are discouraged thinking all is lost,
Count your many blessings, name them one by one,
And it will surprise you what the Lord hath done.!

He ran a hand through his short-cropped sandy hair, its pep-
pered flecks of grey growing in number—daily now. He examined
his reflection between the fly specks on the sink’s mirror. He
barely recognized the face looking back at him. He had become
a stranger to himself.

Are you ever burdened with a load of care?

Does the cross seem heavy you are called to bear?
Count your many blessings, every doubt will fly,
And you will be singing as the days go by.?

A businessman walked into the room. Or a lawyer. His suit,
charcoal grey flannel with a nipped-in waist, looked as if it had
been tailor fit. His oxford cloth shirt was pink, with a button
down collar, His vest was a tattersall check, subtle and sophisti-
cated, red and black on a cream background. His silk tie showed
chic horizontal stripes to match, and his European cut shoes
glistened smartly on the tile.

Mick Gallin caught himself on the sink as a gall of bitterness
rose in his throat.

When you look at others with their lands and gold,
Think that Christ has promised you His wealth untold,
Count your many blessings, money cannot buy
Your reward in heaven, nor your home on high.?

He took a deep breath. And then another. Vertigo swept over
him anyway. It came in wave after wave of jumbled feelings, im-
pressions, and memories. He reached into the waistband of his
baggy work pants and withdrew a small revolver. A Saturday
night special. He fondled it in his grip.
