ACKNOWLEDGEMENTS

 

“Composition is the easiest thing in the world,” Mozart is
reported to have boasted. “All you have to do is write down the
music you hear in your head.”

Easy for him to say!

The melody for this book has been playing in my head for
years. But for some reason when I sat down to write, it did not
flow forth in a torrent —lyrical, lustrous, and lilting. The melody
was still there, but it had betangled itself betwixt and between
harmony, rhythm, dynamics, tempo, and timbre, The writing
process became quite difficult, mind bogglingly laborious. With-
out the help of a number of philanthropic virtuosos, this per-
formance would never have seen the stage.

Mel and Dorra Hunter, the dedicated researchers who turned
a backroom enterprise into the most informative newsletter on
the United Nations available anywhere, were most cooperative.
They let me pour through their old files of The U. N. Insider, from
which I gleaned my best sources on the International Year of the
Homeless, and without which this book would have been impos-
sible. The cessation of their newsletters’ publication schedule
due to Mel’s recent bout with cancer is a great, great loss.

The Hunters’ New York neighbor, Bill Holiday, also assisted
me immeasurably by giving me access to his extraordinary
library of antique books on theology and the social sciences. I
could never have found the valuable sources necessary for a
book of this sort without his help.

Robbie Breckenridge also aided my research, giving me a
home away from home while I was in New York. He steered me
in the right direction when time, money, and encouragement
were all of the essence.
