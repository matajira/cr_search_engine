Epitaph in Rust: Unemployment ng

ers have been attached to bills as wide-ranging as gasoline tax in-
creases to road repair legistation.5” House Speaker Tip O'Neill
argued in 1983 that “one way or another, we are going to have to
step in and get our people back to work.” Unfortunately, sixty-
nine of the seventy-two riders called for federal tax dollars to
provide the necessary job openings.** Instead of stimulating the
economy from the bottom up by removing disincentives, regula-
tory restrictions, developmental controls, and windfall profits
taxes—the things that forced U.S. Steel to close Mick Gallin’s
plant— the legislation simply inflicted more hardships on business
by imposing top down controls through subsidies, limitations,
interventions, and restrictions—the very kinds of tactics that
caused the economic transitions of the 70s and 80s to sour in the
first place.

The legislative initiative to generate jobs was well intended
—jobs obviously must be generated —but fatally flawed, for two
basic reasons.

First, real job creation can only occur through economic
growth. Prosperity or wealth is not a stable commodity that can
simply be managed and redistributed in order to achieve equality
or justice. It is rather the result of productivity. It is the fruit of
work.© Thus in the long run, spreading existing wealth around
doesn’t help anyone, in fact it only hurts everyone. Only by
work, by the sweat of our brows, can our fields of thorns and this-
tles yield a bounteous harvest.© The flaw of the legislative initia~
tive has been that it focuses almost exclusively on the distribu-
tion of wealth (more salaries dispensed), ignoring the need to
stimulate production (economic growth).®

Secand, the job creation initiative is based upon the idea that
“society owes people a living.” America was founded as “the land
of opportunity.” The rights guaranteed by our society have
always been “life, liberty, and the pursuit of happiness.” But enti-
tlement to a particular standard of living, or to a particular level
of economically defined social guarantees, has never been a part
of our system.* Always generous and always charitable, Ameri-
cans have nevertheless maintained a bootstrap ethic: Hard
work, diligence, and productivity are the only means to improve
the lot of the poor. Thus, until the “war on poverty” was initiated
in the mid-60s, virtually all charitable efforts in this country
were aimed at expanding the opportunities afforded the poor by
