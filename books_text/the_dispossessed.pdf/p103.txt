Time in a Bottle: Alcoholism 87

He was quiet now, Broken. He looked tired. Not just tired
from lack of sleep, tired from too much life and too much death.

“Dreams. Couldn’ shake ’em. Nightmares. Sol. . . started
to drink.”

“So you wouldn’t have to remember?”

“Yeah. So I wouldn’t remember.”

“Did it ever work?”

“No”
“Well then, why... ?”
“Look . . .” he cut me off. “Leave me be. Will y’ just leave

me be? I never asked for no shrink. For no Good Samaritan
either. Leave me be, okay?”

He turned away sullenly. A tearful glint caught the light in
the corners of his weathered eyes.

“Henry...” I tried.

“Go! Now!” he spat savagely. “I told y’ before. If y’ can
remember, y’re not drinkin’ enough. I already remembered too
much tnight. Thanks t’ you. So go! I got some serious drinkin’ t’
do tnight.”

I left him there. On his grate. And walked on into the cold
and the dark. Remembering. In a stupor of self-pity.

Alcoholism

Alcoholism has always been closely identified with the prob-
lem of chronic homelessness. When the dispossessed strike a
pose in our mind’s eye, our imagination naturally constructs a
stereotype: a dirty, ragged, unshaven shell of a man slumped
over an empty bottle of cheap liquor. Our image of homelessness
is simply not complete without the craving for alcohol.

It has always been so.

In 1890 Jacob Riis published his classic work, How the Other
Half Lives. There he described the “reign of ram” among the poor
and dispossessed, saying, “Where God builds a Church the devil
builds next door —a saloon, is an old saying that has lost its point
in New York, Either the devil was on the ground first, or he has
been doing a good deal more in the way of building... turn
and twist it as we may, over against every bulwark for decency
and morality which society erects, the saloon projects its colossal
shadow, omen of evil wherever it falls mto the lines of the poor.
Nowhere is its mark so broad or so black. To their misery, it
