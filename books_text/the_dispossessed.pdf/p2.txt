Other books by George Grant

Bringing in the Sheaves:
Transforming Poverty into Productivity, 1985

In the Shadow of Plenty:
The Biblical Blueprint for Welfare, 1986

To the Work: Ideas
‘for Biblical Charity, 1987
