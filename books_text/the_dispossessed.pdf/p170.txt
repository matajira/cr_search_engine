154 Tue Disrossessep

heart wrenching moments of daily life . . .”— casually dressed,
strong features highlighted by the slowly extinguishing sunset
behind—*. . . cut deep troughs of remembrance in the hearts and
minds of all who have ears to hear, to all who have eyes to see,”

Close-in, tight on the reporter’s stern, yet compassionate
expression. Crusty, but benign. Pathos. After an appropriately
reflective pause — twelve seconds: “This is Ben Thompson, on
the banks of the San Jacinto River. Channel Four Action Alert
News.”

“Cut .. . that’s a wrap.”

“Okay. Let’s hit it. We gotta get this in and edited for the
broadcast at ten.”

“Pulitzer material, Ben. Great stuff.”

“Well thanks, Maggie. Let’s hope so.”

“Hustle, hustle guys. Get a move on.”

The mom, object of so much attention just moments ago,
looks past the tent flap as the crew loads mini-cams, lighting
towers, monitors, audio mixers, and other assorted video
gadgetries into two vans, The hustle and bustle sweep right by
her and her small family. Like life.

The reporter, noticing her at last, walks toward the tent, awk-
ward and sheepish now. Not at all the image of sophistication he
casts on camera. “Mrs. Tolbert, I, uh... I want to thank you
for your cooperation on this piece.”

“No problem,” she replies as she steps from the tent.

“Td like to help . . . if there’s. . . .”

“No, We'll be fine.”

He pulls a twenty out of his wallet and thrusts it toward her.
“Td like to at least pay you for your trouble . . . for your time.”

“Thanks.” She takes the money and turns back toward the tent.

He stands there for a moment, uncertain about what to say
or do, and then he too turns to go,

“It’s too bad it won't make a difference,” she says as she slips
past the flaps.

He stops in his tracks. A furrow of determination gouges his
Adonisian good looks. A challenge. “I'll make it make a differ-
ence,” he promises.

The stunning and innovative piece that night, Thanksgiving
Eve, received such widespread acclaim that Ben Thompson was
