194 ‘THe DispossessED

Covenant, and to the sprinkled blood, which speaks bet-
ter than the blood of Abel (Hebrews 12:18-24).

We have a Home. We have been made heirs to a Kingdom
(Romans 8:17) and citizens of a Holy Nation (Philippians 3:20).
Christ has prepared mansions for us (John 14:1-3) in a magnifi-
cent city (Revelation 21:1-27), We have been enthroned (Ephe-
sians 1:3, 2:6).

But again, this New Home is not simply pie in the sky for the
sweet by and by. Our New Home is real here and now as well,
for God has given us a taste of Heaven on earth in the Church.

The Church gives us contact with our Heavenly Home in
worship.* During worship we actually climb up the Mountain of
God and gather around His Throne (Hebrews 4:16). We actually
ascend into the Heavenlies (Ephesians 2:6). We actually join in
the Heavenly Throng singing praises unto our King (Hebrews
12:1-2). We actually sample the wonder of Heavenly Fellowship in
our fellowship with one another (1 John 1:3). We actually eat
Heavenly Food when we take the Lord’s Supper ( John 6:32-58).
Ged actually inhabits our praise (Psalm 85:14), comforts our woes
(John 14:16-18; 2 Corinthians 1:3-5), fills up our lack (Matthew
6:25-34), and empowers our work (1 Thessalonians 1:5; 2 Tim-
othy 1:7). In the Church we are afforded Sanctuary and Refuge.
We are given our Home.

‘When we were overwhelmed by sins, you atoned for
our transgressions. Blessed is the man you choose and
bring near to live in your Courts! We are filled with the
good things of your House, of your Holy Temple. You
answer us with awesome deeds of righteousness, O God
our Savior, the hope of all the ends of the earth and of the
farthest seas, who formed the mountains by your power,
having armed yourself with strength, who stilled the
roaring of the seas, the roaring of their waves, and the
turmoil of the nations (Psalm 65:3-7).

As in the days of the Old Covenant, life under the Shelter of
the Church is a gracious provision of Almighty God. There is
nothing we can do ourselves to earn it, deserve it, or merit it. It
is the gift of God.
