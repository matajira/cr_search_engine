244 ‘THE DispossessED

ness, given in Numbers 19, is contact with death (v. i1). The sprink-
ling unto resurrection is made on the third day, and again on the
seventh (v, 12), pictures of the first resurrection (that of Jesus, on
the third day) and of the second resurrection (at the end of
history).

Where does all this come from? Genesis 3:17 says that the soil
of the earth is cursed for man’s sake. Thus, people in the Old
Testament always wore shoes, to keep themselves away from the
cursed ground. That is why the Bible calls attention to the wash-
ing of feet in the Old Testament and right up to the Last Supper.
That is why men only went barefoot on “holy” ground (Exodus
3:5; Joshua 5:15). In the New Covenant, however, the death of
Jesus Christ removes the curse from the ground,

Since the ground was cursed, to be dirty or unclean was to
have the curse on you. You needed to wash it off, to be cleansed.
Now, precisely what was this curse? The curse was nothing other
than death. God had told Adam and Eve that they would die
(spiritually) on the very day they sinned. In their death they
would return to the cursed dust.

Now we-have the picture. Dust = death. To be dirty or un-
clean was to have death on you. To be cleansed was to be
granted symbolic resurrection.

All the various kinds of uncleanness in the Bible are pictures
of death and the curse. These are given in Leviticus 11-15. We
cannot survey all of these, but let us note the one passage that is
relevant— Leviticus 15:19ff. This passage tells us that the “issue
of blood” is not an unhealed cut or wound. Rather, the issue is
from the woman’s private parts, the place babies come from.
The fountain of life has become a fountain of death (Leviticus
20:18), for life is in the blood (Leviticus 17:11), and the continual
flow of blood is a continual loss of life.

When Jesus healed the woman with an issue of blood, then,
He was granting her symbolic resurrection. He was showing
Himself as the True Red Heifer, whose death would cleanse the
world.

Now with this context in mind, we are in a better position to
look at the details of the story of the Gerasene Demoniac. From
there we will move to a discussion of its relevance as a model for
psychological dysfunction.
