The Drawing of the Dark: A Personal Look 1B

“Uh, . . . just getting warm,” I replied as I took his hand.

“You're green, J can always tell. New to the streets. That’s
why I made the offer.”

“The offer?”

“Tour guide, Show you the ropes. A few tips. Trade secrets.
All that.”

“Oh, I see. Well . . . thanks.”

Red was a ten-year veteran of the streets. So he really knew
the ropes. In the next few hours I would learn more about home-
lessness than I had in five years of serious research —a gift of in-
sight given me in the house of God.

He showed me the best places to eat, to sleep, and to pass the
time of day. “They say beggars can’t be choosers, They’s wrong,”

He guided me through the labyrinthine steam tunnels run-
ning under Park Avenue, long known on the streets as a hobo’s
haven. “This is the one place jackrollers is at a disadvantage. Us
skels has got ’em over a barrel here.”

He taught me how to bypass the subway turnstiles and
pointed out all the most lucrative scams, hustles, and cons. “Just
don’t mess with the books or the montes.”

And he warned me away from the public shelters and the
horrid welfare hotels as well. “You got a better chance at makin’
it on the streets. [t's safer.”

Red had worked for the city in the parks and recreation de-
partment for six years. “But then the city went bust and they laid
me off. City’s okay now I hear. Now’s just me that’s bust. Me an’
all the other skels.”

After a hearty lunch scrounged from a restaurant wholesaler’s
surplus, Red bade me farewell. “Got me business to attend to.
Stay away from the shelters. See ya at Mass, huh?” And he was
off, checking the pay phones for forgotten quarters, checking the
trash barrels for abandoned treasure.

T never saw him again.

Third Street Men’s Shelter

Despite Red’s insistence, I knew that I needed to visit the
public shelters. I wanted to see for myself the vineyards where
today’s grapes of wrath are stored. So, I made my way toward
the Bowery on the lower east side.

After only a few blocks, I decided that I just couldn’t walk
