Sall Crazy After All These Years: Mental Hiness 63

hypoglycemia, convulsive seizures, respiratory depression, and
jaundice.”#!

Dartal and Mellaril are two other commonly prescribed anti-
psychotic tranquilizers despite similar dangers, as are Stelazine,
Lithium, Haldol, and Prolixin.*? In every case, after prolonged
use the “cures” afforded by such psychotropic agents are worse
than the “diseases” they were prescribed to control and contain.

With psycholagy practically and philosophically bankrupt,
dependent upon the broken cruich of anti-psychotic drug use
and abuse, it is no wonder that deinstitutionalization has been
no more successful than previous policies of institutionalization.

The U.N.’s Answer

Mental illness, recognized as “a primary cause of homeless-
ness, especially urban homelessness,”# has taken a high profile
in the U.N.’s International Year of the Homeless. Naturally, the
programs and the literature of the various agencies involved in
the “Year” have focused much of their attention on appropriate
responses, reactions, and solutions to “the scandal of unleashed
and untreated insanity.”"*

So, what do they recommend?

More of the same, of course. Beat the dead horse. Only beat
it harder, faster, and more often.

The U.N. recommends re-institutionalization. More psy-
chology. More psychiatry. More anti-psychotic drugs. More of
everything that has failed so miserably in the past. And of
course, more money to subsidize the whole vicious mess.

This comprehensive re-institutionalization plan was ulti-
mately adopted by the European Economic Community’s Com-
mission on Poverty and Homelessness. The plan includes “a
basic right” for all citizens “to appropriate medical and psychiat-
ric services, including the right to hospitalization” and compre-
hensive care that is both “planned and long term, maximizing
all-round health and not reliant on short term crisis solutions.”

The commission concluded by declaring its support for the
U.N.’s mandate that the homeless be treated by a “holistic ap-
proach” to mental care, “involving innovative multi-disciplinary,
psycho/socio/medico/logical services.”*

More of the same. Throw good money after bad. If at first
you don’t succeed, try, try again. Try the same old proven fail-
ures again and again and again and again.
