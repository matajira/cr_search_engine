40 Tur DispossesseD

can’t pay their mortgage,” notes Bob Widdowson, director of
The Shelter National Housing Aid Trust in London. “And
young people leave home and have no place to go. It’s certainly
gotten worse in the last eighteen months.”!

As in America, homelessness in Europe has reached crisis
proportions,

The Third World’s Trauma

If the state of affairs in the U.S. and Western Europe seems
dismal, the plight of many Third World nations is downright
depressing.

It is now conservatively estimated that one billion of the
Third World’s two-and-a-half billion people do not have perma-
nent housing.!! Of these, approximately 100 million have no
housing whatsoever, ?

In Latin America, nearly 20 million dispossessed children
and youths live and sleep in the streets./3 In the cities as many as
50% of the inhabitants live in cardboard and tin squatter settle-
ments.!# Mexico City has almost 550,000 homeless squatters. !5
Rio de Janeiro has 700,000.'° Lima has 400,000.!7 And Bogota
has 140,000.18

The future offers little hope that the squalor of these home-
less settlements will improve. In fact, the rapid urbanization of
the Third World makes “hope” seem an absurd notion altogether.
It is estimated that by the year 2000, approximately one in two
Third World inhabitants will be living in cities.19 What this
means is that in less than fifteen years, the population of those
cities will more than double—from 900 million to 2.1 billion.”°
Of these more than 2 billion urban dwellers, about 500 million
will be living in sixty cities of more than 5 million inhabitants.?!
And the population of a few of those already humongous cities
will bloat beyond the bounds of even the wildest imagination.
Consider for instance that, if present trends continue, by the
year 2000, Mexico City will have 31 million inhabitants,”* Sao
Paulo, 25.8 million,” and Bombay, 16.8 million.

Where will they all then live?

Many will have to settle for a cardboard box or a tin hovel
amidst the mud, waste, and squalor of a shanty town slum.
Many will have to settle for even less than that.
