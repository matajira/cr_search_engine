PART THREE

 

BREAKING THE
CASTE

Behold, how fitly are the stages set

For their relief that pilgrims are become,
And how they us receive without one let
That make the other life our mark and home.

What novel ties they have, to us they give,
That we, though pilgrims, joyful lives may live,
They do upon us too such things bestow,

That show we pilgrims are, where ’ere we go.

John Bunyan
