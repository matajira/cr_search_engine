 

No refuge could save
the bondsman, the hireling, the slave
from terrors of the flight,
from gloom of the grave,
When forced
by ’ere wits to take
the leave by the Dogtown Gate.

Lawrence Dwight Appleby

 
