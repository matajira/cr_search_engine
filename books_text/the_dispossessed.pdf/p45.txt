Duende Dancehall: The Crisis 29

source of the figures, the time of the year, and . . . the purpose
for which the numbers are put forth. The kinds of living
arrangements defined as ‘homeless’ may also vary considerably,
adding a further element of uncertainty, and making historical
and cross-regional comparisons hazardous.”*# Is it any wonder
then that social scientists are more prone to play percentage
point ping pong than they are to attempt to arrive at workable
solutions to the problem?

But, as Heritage Foundation policy analyst Anna Kondratas
has argued, “Sound public policy requires that we have some
reasonable idea of the scope of a problem before we attempt to
rectify it."4° That is why, despite al] difficulties and uncertainties,
studies are continually conducted. And that is why, despite all our
reservations and hesitations, we must take each of those studies
into account as we try to bring focus to the issue of homelessness.

Probably the two most important surveys of homelessness
conducted of late are those of the U.S. Department of Health
and Human Services (HHS), and the U.S. Department of
Housing and Urban Development (HUD). Of the more than
one hundred and some-odd additional studies of homelessness
over the past five years,*° these two have proven to be the most
representative, the most respected, and the most often cited.*

The HHS report concluded that advocacy groups were prob-
ably not far off the mark with their two to three million “guessti-
mates,”#8 while the HUD report, released just six months later,
concluded that the low range figure of 250,000 to 350,000 “was
more likely.”

So, which was right?

In all probability, off studies accurately measured their sam-
ples. It is just that they measured two different samples, thus high-
lighting two different aspects af the homelessness problem.*°

The HHS study was conducted in the dead of winter when
most of the homeless are driven indoors by the elements. Crowded
into soup kitchens, storefront missions, and public shelters, the
normally dispersed and mobile population of the dispossessed
were then accessible and countable.

The HUD study, on the other hand, was conducted during
the late spring. All but the most infirm and disabled of the home-
Jess had by that time vacated the shelters for the freer domains of
the parks, the streets, and the highways. And many, unable to
