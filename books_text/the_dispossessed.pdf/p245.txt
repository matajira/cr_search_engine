Aubade: A Biblical Hope 229

tions on their behalf. They had seen His very great and precious
promises to them confirmed at every turn, There had been the
ten plagues in Egypt. There had been the glorious exodus and
the parting of the Red Sea. There had been the provision of
manna from heaven. There had been the cloud by day, the fire
by night, and the continual Presence at the Tabernacle, But now
that they were on the threshold of the Promised Land; now that
victory was imminent; now that they could consummate their
covenant task they were nervous. They were full of doubt. They
were hamstrung with faithlessness.

The land was flowing with milk and honey all right, just as
God had promised (Numbers 13:27). It was rich with grapes,
pomegranates, and figs (Numbers 13:23). It was all that they had
hoped for, and more. But . . . it was filled with strong men and
fortified cities (Numbers 13:28).

And... . there were giants. Those indomitable giants.

Never mind that the land was already given into their hand
(Numbers 13:30). Never mind that God was with them and had
removed the protection of the enemy (Numbers 14:9).

Never mind all that! Look at those giants!

But notice, those giants never defeated Israel. They never
had the chance, because Israel beat herself. With the bludgeon
of pessimism. With the club of faithlessness.

And so the people retreated from their promises to wander in
the wilderness for forty years (Numbers 14:32-35).

All because of the giants.

Unlike his forbears, David knew that he could “stand and not
be shaken” (Hebrews 12:28). He knew that he could depend on
God’s Word. He knew that he did not have to yield before the ut-
terly impossible circumstances that faced him. He knew that he
could act decisively on the truth and reliability of God’s testimony
and that he would thus prevail. He even knew that he could make
due without the entangling encumberments that the king had
thrust upon him as a help, as.added security (1 Samuel 17: 38-39).

Giants in the land?

So what! God raises up giant killers. In fact, He raises up
whole armies of giant killers (2 Samuel 21:18-22).

As it was in the beginning, is now, and ever shall be.

There is a giant in the land.
Clearly homelessness is a complex problem of gargantuan
proportions.
