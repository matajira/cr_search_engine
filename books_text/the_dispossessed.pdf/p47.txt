Duende Dancehall: The Crisis 31

Thus, when considered together, these two seemingly contra-
dictory surveys of homelessness actually bring clarity and defi-
nition to the crisis at hand in a way that neither alone could ever
bring. Together they provide a profile of the dispossessed hereto-
fore entirely unavailable.

First, taken together the two studies demonstrate irrefutably
that the problem of homelessness has reached crisis proportions.
There are only 91,000 shelter beds available nationwide.** So,
even if the most conservative estimate of total homelessness is
accepted as normative, at least two thirds of the dispossessed
could not come in out of the cold even if they wanted to, “Even
with all our shelters open and every bed filled, we're meeting less
than half the need,” says Betty Knott, who operates a church
shelter in Atlanta.* The numbers then are incidental. The stud-
ies agree: Homelessness is an encroaching crisis.

Secondly, taken together the two studies demonstrate that
there are two very different categories of homeless. There are the
chronic, hardcore, permanently dispossessed and there are those
who have hit the skids only recently, only temporarily. Kim
Hopper and Ellen Baxter vividly describe this clear contradis-
tinction in their disquieting book, Private Lives/Public Spaces:
Homeless Adults on the Streets of New York City. “A tattered appear-
ance, bizarre behavior, belongings carried in plastic bags or
cardboard boxes tied with string, swollen ulcerated legs or
apparent aimlessness: these are the obvious features which dis-
tinguish the homeless from other pedestrians and travelers. But
there are also those who have been able to maintain a reasonable
good personal appearance and whose behavior betrays no appar-
ent sign of disorder, and they are often overlooked by casual
observers. Their presence during late night hours when com-
muters have gone home and stores have closed, and especially
their repeated presence in the same sites days or weeks later, is
the only telling sign.”

Common sense indicates it. Observation supports it. And
the HUD and HHS studies taken together confirm it. There are
two very different categories of homelessness.

Chronic Homelessness

The ranks of the chronically homeless are populated almost
exclusively by men, usually older white men, most of whom
suffer from serious mental or physical disorders.** They live ina
