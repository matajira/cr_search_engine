72 Tue DispossesseD

no job. No job history. No job skills. No job leads. No job refer-
ences. Nothing.

Her share from the sale of the brownstone came to just under
$45,000. But after paying her half of the back debts, she was left
with a mere $39,000. And with that, she was to start a new life.

Kathi immediately moved into a small, one bedroom apart-
ment and went to work as a waitress in a Brooklyn Kosher deli.
She made about $900 a month, including tips. Jacob mean-
while, had quit drinking, gone back to the electrical supply busi-
ness, and had remarried. His annual income returned to his pre-
divorce level—nearly $65,000 a year—and he and his young
new wife purchased a home in the Long Island suburbs.

“Tl admit it right off. I became very bitter at that point. Very
bitter, Why he should have been able to just pick up and carry
on as if nothing had happened just escaped me. Yes indeed,” she
said, “I was bitter.”

Then, she began to drink for consolation. “At first, it was just
a bit of sherry at night. But before long, I was hitting the bottle
pretty hard.”

When her work began to suffer, Kathi sought psychiatric
help. “The doctor gave me some tranquilizers and listened to me
ramble, but he never really gave me anything tangible. He
never really gave me any help. I just decided, to hell with it. To
hell with it all.”

I met Kathi in the Riverside Clinic, a rehabilitation center in
New York’s upper west side that specializes in indigent women.
“I just woke up one day in a welfare hotel and realized that I was
on the road to becoming one of those shopping bag ladies. I was
out of money. I’d lost my apartment and my job, I was a total
mess. I thought, What's a nice Jewish girl like me doing in a
place like this?’ I decided then and there I was going to get the
help I needed. Somehow. I was going to rebuild my life.”

She checked herself into the Riverside program and began
the long and arduous task of returning to the mainstream. “I'm
still bitter. And that’s something I'll have to continue to deal
with. I know that it was my own irresponsibility that got me into
trouble. The alcohol and all. But even so... it seems to me
that women have been led down the primrose path. We've fought
so hard to be ‘liberated.’ To be ‘equal.’ And here to find out, all
that ‘liberation’ has only earned us more pain and more heart-
