102 Tue DispossesseD

Housing Stock Conversion

In order to escape the clutches of rent control and to insure a
return on their investments, many landlords simply remove
their properties from the rental market and convert to condo-
miniums. In New York more than 57,000 rental units were con-
verted to cooperative or condominium ownership from 1978 to
1984.7 Nationwide, between 1970 and 1982, 1,116,000 single
room rental units were converted, nearly half the stock.® Dur-
ing the same period, New York lost 87% of its total “SRO,” or
“poverty hotel” capacity.2§ Other cities have lost as much as 65%
of their rooming units this way.” The poor are simply displaced
because their landlords can no longer afford to subsidize their
rents.

Social scientists have created the word “gentrification” to
describe this displacement and conversion process. In a working
paper prepared by the Community Service Society of New York
for the Institute for Social Welfare Research, Kim Hopper and
Jill Hamberg describe the phenomenon: “Unlike the Depres-
sion, when households doubled up and owners boarded up va-
cant units awaiting the return of prosperity, in the 1970’s land-
lords and financial institutions gave up on the ‘bottom’ of the
market —both the buildings and the people. At the same time, a
growing number of young professional and managerial people,
increasingly locked out of the first-time buyers market, began to
outbid moderate-income tenants in a pinched rental market.
The result is ‘reverse filtering’ where people — rather than build-
ings — filter down through the market.”

In short, in times of housing shortages, moncy talks, If a land-
lord knows that he can convert a marginally profitable, rent con-
trolled tenement into a high profile condo development for young,
upwardly mobile professionals, wild horses won't hold him back.

Rent controls then, simply cut off the bottom of the market.
They eliminate low budget housing altogether. Rather than pro-
tecting the poor, the controls assail the poor, leaving many with
no other recourse than the streets.

Poor Tenants and Poor Landlords

Not only do rent controls contribute to the displacement and
impoverishment of tenants, in many cases they produce displaced
and impoverished landlords as well.
