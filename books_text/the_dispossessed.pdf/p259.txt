APPENDIX TWO

UNDERSTANDING
THE STORY OF THE
GERASENE DEMONIAC
by James B. Jordan

 

The story of the Gerasene Demoniac is of great value in un-
derstanding the plight of the chronically homeless in the world
today, and I count it a privilege to be asked by George Grant to
prepare this Appendix on the subject.

The Story’s Context

The Gerasene or Gadarene incident is the first of three
events recounted in Mark 5, all having to do with the subject of
the resurrection. In the second half of the chapter we have the
two stories of the woman with an issue of blood and of Jairus’s
daughter. The two stories are sandwiched together (Mark
5:21-43). The woman had been “ceremonially unclean” and thus
untouchable for twelve years, and the child was twelve years old
when she died (vv. 25, 42.) Both are called “daughter” (vv. 34, 35).

It is obvious that Jesus resurrects (or resuscitates) the child.
It is less obvious to us today that the healing of the woman is also
a resurrection, because we are not familiar with the laws of
Leviticus. According to Leviticus 15:19ff., a woman with an
“issue of blood” was “unclean.” To be “unclean” is to be symbolic-
ally dead, and to be “cleansed” is to be symbolically resurrected,

‘The basic law of cleansing (or resurrection) is found in Num-
bers 19. The death of the “red heifer” substituted for the unclean
person, and the ashes of the heifer, sprinkled on the unclean per-
son, granted symbolic resurrection. The main cause of unclean-

243
