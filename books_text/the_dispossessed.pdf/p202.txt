 

He liked being a rich, wild young man on
Earth ever so much better than being a respecta-
ble spinster under the grey skies of Old North
Australia. When he dreamed, he was sometimes
Eleanor again, and he sometimes had long mor-
bid periods in which he was neither Eleanor nor
Rod, but a nameless being cast out from some
world or time of irrecoverable enchantments. In
these gloomy periods, which were few but very in-
tense, and usuaily cured by getting drunk and
staying drunk for a few days, he found himself
wondering who he was. What could he be?

Cordwainer Smith

 
