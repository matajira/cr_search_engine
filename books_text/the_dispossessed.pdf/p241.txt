Aubade: A Biblical Hope 225

sued their tasks with righteous zeal and passionate fervor, they
often fall short of the ultimate goal of Scriptural poverty relief:
getting the poor back on their own two feet, back to self
sufficiency. Though no one can doubt the tremendous impact
that General William Booth and his Salvation Army or Mitch
Snyder and his Community for Creative Non- Violence have had
on the issue of homelessness, that lack is nonetheless glaringly
evident.

Charity must be more than a reaction to governmental pro-
grams: a cry for tougher standards and fewer benefits from the
conservative traditionalists; a cry for easier standards and more
benefits from the liberal progressivists, Charity must recon-
struct. It dare not simply be a social or political or economic tac-
tic. Charity must offer a reaf hope in the face of a real Curse. As
David Chilton has asserted, the “true Christian reconstruction of
culture is far from being simply a matter of passing Law X and
electing Congressman Y. Christianity is not a political cult. It is
the divinely ordained worship of the Most High God.” He goes
on to argue that if our primary response to cultural problems is
social or political action, “we are, in principle, atheists; we are
confessing our faith in human action as the ultirnate determiner
of history.” Where traditionalist and progressivist ministries
break down is the same place that Luther’s secularization plan
broke down: They fail to take into account the full and trans-
forming spiritual dimension of relief.

Reconstructionist ministries represented by the Spurgeonic
Charities,!? Voice of Calvary Ministries,!! and H.E.L.P. Ser-
vices'!? have commonly tried to avoid the trap of either a conser-
vative humanism on the one hand or a liberal humanism on the
other. They have been Church oriented, sacramental, and inte-
grationist. They have gone beyond soup kitchens, shelters, and
self help enterprises, training the poor and homeless through
godly discipline and discipleship to make it in the wilderness of
this world.!® They have included emergency relief in their reper-
toire of care,"* but have incorporated job training," spiritual
counseling,'® liturgical reorientation,!”? congregational fellow-
ship," and diaconal service as well.!9 They have sought to trans-
form poverty into productivity. They have given the homeless
True Sanctuary.

That is real charity!
