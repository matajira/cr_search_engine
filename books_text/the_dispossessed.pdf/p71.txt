FOUR

STILL CRAZY AFTER
ALL THESE YEARS:
MENTAL ILLNESS

 

His hand shook with emotion as he scrawled the words across
dingy yellow pages in a well-worn spiral notebook.

The wind was a torrent of darkness among the gusty
trees, the moon was a ghostly galleon tossed upon cloudy
seas, the road was a ribbon of moonlight over the purple
moor, and the highwayman came riding— riding — riding —
the highwayman came riding, up to the old inn-door.*

He cast about, looking, muttering, and gesturing to whom or
what I couldn’t tell. Then a vacant grin slowly spread over his
dull countenance and he returned to his labor.

He'd a French cocked-hat on his forehead, a bunch of
lace at his chin, a coat of the claret velvet, and breeches of
brown doe-skin; they fitted with never a wrinkle: his boots
were up to the thigh! And he rode with a jeweled twinkle, his
pistol butts a-twinkle, his rapier hilt a-twinkle, under the
jeweled sky.?

The stark contrast was obvious at once. At least, it was ob-
vious to most, But not to Raul. He continued to copy. Stanza
after stanza, the small blue volume before him yielded up its
passions and dramas to the small spiral. His spiral.

Over the cobbles he clattered and clashed in the dark
inn-yard, and he tapped with his whip on the shutters, but

53
