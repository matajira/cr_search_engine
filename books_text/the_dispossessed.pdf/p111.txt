Time in a Bottle: Alcoholism 95

because of the debilitative effects of alcohol on their families,
their health, and their mental faculties. But either way, alcohol
takes a high profile in the dilemma of dispossession.

It stands to reason then that alcohol rehabilitation and re-
pentance should be a major plank in any platform for the recov-
ery of the homeless. Unfortunately, most social service agencies
that work with the dispossessed sluff over the issue. The U.N. in
its International Year of the Homeless literature ignores it. And
most church outreaches, shelter ministries, soup kitchens, and
rescue missions are oblivious to it, or have given up trying to
stop it.

Is that any way to solve a problem?
