44 Tue Drspossessep

The whole idea was his “baby” and he has superintended the
project from its inception.

Premadasa was also the architect of one of the most compre-
hensive non-Marxist experiments in national socialization in
modern history. Serving under the previous regime of Sirimaro
Bandaranaike, he moved the smal! island nation south of India
from a parliamentary democracy to a total welfare state with an
economy socialized to 90% .*

Every citizen, rich and poor, received two “free” kilos of rice
monthly from the state. All staples — bread, flour, and sugar—as
well as textiles, household goods, and shelter were provided
through a massive government subsidy program. Education
from elementary school to university, as well as medical care and
legal counsel, were provided as citizen entitlements.

By 1977 Premadasa and Bandaranaike’s expensive experi-
ment collapsed. The treasury was drained and no more foreign
capital could be obtained. And ultimately, the government fell.

Re-aligned with a new party and distanced from Bandaran-
aike, Premadasa rose from the ashes of his failure and regained
power.*' Immediately he set forth to re-sucialize the housing in-
dustry. In Colombo, where nearly one-third of the 600,000 resi-
dents are either homeless or subsisting in squatter settlements,
he initiated three massive urban renewal projects in cooperation
with the United Nations Center for Human Settlements.*? The
focus of the projects was not only to provide “improvements to
poorly distributed and derelict amenities” but also to insure
“regularization of ownership” and “community organization.”53
Translated that means state control of land ownership, the aboli-
tion of private property, and the collectivization of urban popu-
lations.

Thus far, the projects are still in the “pre-implementation
phase” of “deed centralization and clarification.” So, despite the
rhetoric and the political power plays, the homeless are no better
off than before, except that now they cannot claim exclusive
rights to the label “dispossessed.” They have been joined in that
category by former property owners.

Quite a “success.”

This is the model the U.N. wishes for us to emulate.
