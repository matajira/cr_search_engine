Epitaph in Rust: Unemployment Jat

the basis of job creation, God made new production the stimulus.
His laws are explicit.

Now when you reap the harvest of your land, you shall
not reap to the very corners of your field, neither shall you
gather the gleanings of your harvest. Nor shall you glean
your vineyard, nor shall you gather the fallen fruit of your
vineyard; you shall leave them for the needy and for the
stranger. I am the Lord your God (Leviticus 19:9-10).

When you reap your harvest in your field and have
forgotten a sheaf in the field, you shall not go back to get
it; it shall be for the alien, for the orphan, and for the
widow, in order that the Lord your God may bless you in
all the work of your hands, When you beat your olive
tree, you shall not go over the boughs again; it shall be for
the alien, for the orphan, and for the widow. When you
gather the grapes of your vineyard, you shall not go over
it again; it shall be for the alien, for the orphan, and for
the widow (Deuteronomy 24:19-21).

Landowners in Israel were not to be saddled with the added
burden of subsidizing the living of the poor, but they were to provide
them with the means, the opportunity to make their living them-
selves. The poor were not to bleed off the profits of landowners,
stealing from them the fruit of their labors. Instead, the poor were
to engage in production themselves, living off the fruit of their
labors.

Thus, ancient Israel was a fue “land of opportunity.” God’s
laws expanded opportunity by expanding the economy in general.

Instead of straining the economy with top down controls and
mandates for redistribution, God’s law stimulated the economy
with bottom up incentives and opportunities.

The poor were enabled, not just appeased. And the wealthy
were not penalized in the process. It was a system in which the
rich could get richer (if they contrived to work hard and obey
God’s laws) but then so could the poor (if they worked hard and
obeyed God’s laws). It was a system that broke the connection be-
tween joblessness and homelessness by giving the dispossessed op-
portunities within the growing economy.
