34 Tur Drspossrssep

When Jesus began His ministry, His attentions were espe-
cially devoted to the oppressed. He dwelt among them (Luke
5:1-11); He ate with them (Luke 5:27-32),; He comforted them
(Luke 12:22-34); He fed them (Luke 9:10-17); He restored them
to health (Luke 5:12-16); and He ministered to them (Luke
718-23). When He summarized His life’s work, He quoted
Isaiah, saying,

The Spirit of the Lord is upon Me, because He an-
ointed Me to preach the gospel to the poor, He has sent
Me to proclaim release to the captives, and recovery of
sight to the blind, to set free those who are downtrodden,
to proclaim the favorable year of the Lord (Luke 4:18-19).

But while the oppressed are the objects of God's special care,
the sluggardly are the objects of His special condemnation.

Sluggards waste opportunities (Proverbs 6:9-10), bring pov-
erty upon themselves (Proverbs 10:4), are victims of self-inflicted
bondage (Proverbs 12:24), and are unable to accomplish any-
thing in life (Proverbs 15:19). A sluggard is prideful (Proverbs
13:4), boastful (Proverbs 12:26), lustful (Proverbs 13:4), wasteful
(Proverbs 12:27), improvident (Proverbs 20:4), and lazy (Prov-
erbs 24:30-34). He is self-deceived (Proverbs 26:16), neglectful
(Ecclesiastes 10:18), unproductive (Matthew 25:26), and devoid
of patience (Hebrews 6:12). A sluggard will die for the lack of
discipline, led astray by his own great folly (Proverbs 5:22-23),
Though he continually makes excuses for himself (Proverbs
22:13), his laziness wil] consume him (Proverbs 24:30-34), para-
lyze him (Proverbs 26:14), and leave him hungry (Proverbs
19:15). A sluggard’s wasteful and irresponsible behavior will ulti-
mately land him in the gutter. His moral catatonia will drive him
over the edge of responsibility, prosperity, and sanity.

The Christian’s Duty

As Christians, we are commanded to show charity and to ex-
ercise compassion to both the oppressed poor and the sluggardly
poor—to both the temporarily dispossessed and the chronically
dispossessed. It is not enough simply to acknowledge their exist-
ence. It is not enough to be able to make distinctions between
them. It is not enough to compare government studies and Bible
