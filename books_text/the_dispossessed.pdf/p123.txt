The Broken Citadel: Housing Regulation 107

Igor Shafarevich,** Sven Rydenfelt,3’ Miron Dolot,* and many
others have shown, whenever the free market is substantially
tampered with, the poor are dispossessed altogether.

That is not to say that the free market is always entirely
benevolent to the poor. In times of social and economic up-
heaval, quite the opposite has proven to be the case. If we look
back at Nehemiah 5, we see that not only were the wealthy abus-
ing their taxing power in order to oppress the poor, they were
also taking advantage of a famine in order to dispossess them
(verse 3). Nehemiah’s rebuke covered not only their excessive
taxation, but also the usurious interest they were charging peo-
ple during the famine (verses 6-13). There is nothing wrong ordi-
narily with loaning money out at interest—a good free market
principle — but in a time of famine or disaster, such a practice can
be oppressive. Indeed, the Law of God states that the poor are
not to be charged interest on charity loans (Ex, 22:25).

Moreover, a semi-free or mixed economy allows for even
more abuses. In the sixteenth century for instance, the people of
England lived for the most part, in villages and small rural set-
tlements. They supported a quasi-feudal system under which
they would work the fields for a “land-lord,” paying him rent
from their produce.?? Obviously these peasants were pour, but
what they lacked in affluence was more than made up for in
security. A peasant who had a bad crop, or who had an accident,
could go to his lord for help. Or, he could go to the Church,
known for centuries as a refuge for the poor.”

But due to the great boom in mercantilist enterprises, the na-
tion was undergoing a dramatic transformation. With wool
manufacturing expanding rapidly all across Europe and demand
for the raw commodity driving the price ever upward, the lords
found that they could make much more money grazing sheep on
their fields than from the rents they charged peasants.*!

The free market went to work. By a number of devices like
“enclosure” and “rack-renting,” the peasants were evicted.*2 And
for the first time since the panics of the fourteenth century
plague era, the land was flooded with homeless wanderers.#

In time, however, the presence of these vagrant peasants
sparked two important developments in English society that
would form the backbone of its moral and economic supremacy
for the next 350 years. First, the peasants became the natural
