The Instrumentality of Mankind: A Biblical Pattern 293

their reach to include her. Because the deeds of her mouth and
works of her hands proved that she would depend on the Word of
God to live, she was granted the privileges of the community of
faith. She was brought into the circle of the covenant. She was
given a Home.

This gracious provision of God is illustrated time after time
throughout Scripture

The Ethiopian eunuch obtained an entrance into the cove-
nant (Acts 8:38) because he submitted himself to the terms of the
covenant (Acts 8:36-37).

Cornelius the centurion obtained the promises of the cove-
nant (Acts 10:44-48) because he trusted the Gospel of hope (Acts
10:22, 31, 44).

Similarly, when Jesus was in the district of Tyre and Sidon, a
Canaanite woman received privileges of the covenant because of
her great faith.

And behold, a Canaanite woman came out from that
region, and began to cry out, saying, “Have mercy on
me, O Lord, Son of David; my daughter is cruelly
demon-possessed.” But He did not answer her a word.
And His disciples came to Him and kept asking Him,
saying, “Send her away, for she is shouting out after us.”
But He answered and said, “I was sent only to the lost
sheep of the house of Israel.” But she came and began to
bow down before Him, saying, “Lord, help me!” And
He answered and said, “It is not good to take the chil-
dren’s bread and throw it to the dogs.” But she said, “Yes,
Lord; but even the dogs feed on the crumbs which fall
from their masters’ table.” Then Jesus answered and said
to her, “O woman, your faith is great, be it done for you
as you wish.” And her daughter was healed at once
(Matthew 15:22-28).

Just as the Gospel “is the power of God for salvation to every-
one who believes, to the Jews first and also to the Greek” (Rom-
ans 1:16), so the privileges of the covenant are available to everyone who
submits to the terms of the covenant, to the covenant member first and
also to the sojourner.

But, privilege brings responsibility.

Jesus warned His disciples about sidestepping the bounda-
ries of the covenant, saying,
