212 Tue DispossessEp

the Lord Himself was his protection, judging all his oppressors
(Psalm 94:6, 146:9; Jeremiah 7:6, 22:3; Ezekiel 22:7; Zechariah
7:10; Malachi 3:5).

But there were safeguards. The safeguards were partially de-
signed to protect Israel from pagan pollution. But even more im-
portant than that, they protected the sojourners themselves from
the ill effects of promiscuous entitlement, So for instance, there
were ceremonial restrictions (Deuteronomy 7:1-6), and restrict-
ions on cohabitation ( Joshua 6:23). With special privilege came
special responsibility. If the sojourner was to reap the rewards of
Israel’s Theocratic Republic, then he would have to function as a
responsible, obedient citizen. Like any other member of the cov-
enant he would have to honor the Sabbath (Exodus 20:10), the
Day of Atonement (Leviticus 16:29), and the Feast of Unleav-
ened Bread (Exodus 12:19), He shared the prohibitions on eating
blood (Leviticus 17:10-13), immorality (Leviticus 18:26), idolatry
(Leviticus 20:2), and blasphemy (Leviticus 24:16). He came
under the Shelter of God’s promises because he obeyed God’s
commands,

Nothing could stay God’s hand from blessing those who hon-
ored Him, just as nothing could stay His hand from judging those
who dishonored him. Thus, if the sojourner wished to share in the
privileges of God’s chosen people, he would have to honor God by keeping
His Word. There was, and is, no other path to blessing.

Rahab the harlot, though she was not a member of God’s
covenant, came into the midst of it and submitted to God’s rule,
depending on His Word to live (Joshua 2:8-21). Though not of
God’s Household, she entered in, abiding by its standards, and
thus obtained its securities. She turned her back on pagan Jer-
icho and cast her lot with Almighty God and His people (Joshua
6:22-25). Her life and liberty could not have been had another
way. Israel was an opportunity society, but only for those who
observed the “rules.”

Likewise, Ruth was not a member of God's covenant. She
was a Moabitess (Ruth 1:4), A sojourner. But the charity of
God’s land of bounty and table of bounty was not closed to her
(Ruth 2:2-23), She was given the opportunity to labor, to work,
to glean, because she had committed herself to the terms of the
covenant, the God of the covenant, and the people of the cove-
nant (Ruth 1:16-17), The structures of charity in Israel expanded
