Epitaph in Rust: Unemployment i7

Many of the displaced unemployed “double up” with others:
moving in with parents, sharing an apartment with siblings, or
pooling resources with friends. The New York Housing Author-
ity estimates that 17,000 families are “doubling up” in that city’s
public housing projects alone.2? That amounts to one out of
every ten households officially living in the projects. Sociologist
Henry Schechter estimates that nationwide “the number of fami-
lies living with others as ‘subfamilies’ doubled, from a low of 1,3
million in 1978 to 2.6 million in 1983. Similarly, the number of
unrelated individuals living with others went from 23.4 million
in 1978 to 28.1 million in 1983,"#1

Many others of the displaced unemployed are able to avoid
homelessness by securing public housing, In 1983, nearly
160,000 families applied for such assistance in New York alone.#?
Unfortunately, with a low 3.8% turn over rate nationwide, and
with waiting lists as long as five to six years, the relief public
housing can give is minimal.

So where do the rest go? Where do the displaced unem-
ployed go when they can’t move in with family or friends, when
they are unable to get public assistance?

Hundreds of articles and feature stories in chronicles as var-
ied as The Wall Street Journal** and People Magazine, The New York
Times*6 and Rolling Stone,*7 Newsweek Magazine*® and The Humble
Echo,® The Christian Herald® and The U.N. Habitat News,®' have
documented their flight to the streets, their flight into homeless-
ness. Invariably the chronicles sample the heart-wrenching
riches to rags stories that abound in times like these.

Stories of people like Mick Gallin.

For fourteen years, Mick worked hard. He was one of 3,500
steel workers in the U.S. Steel mill in Youngstown, Ohio. He’d
always been thankful for his job —all the more so when in 1977
he saw 4,100 of his friends and neighbors lose theirs when the
Campbell Works of the Youngstown Sheet and Tube Company
closed its gates for good. He had read in the papers how as many
as 35% of those former workers were forced into early retirement
~—at less than half of their previous salary.5? Another 15% were
still looking for work a year later.*3 About 40% had been able to
find other work, but most had taken huge wage cuts.*t The re-
maining 10% were forced to take to the streets.55 So, Mick was
thankful and “counted his blessings.” He redoubled his efforts to
make himself indispensible to his employers.
