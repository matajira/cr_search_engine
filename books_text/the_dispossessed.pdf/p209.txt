Recoverable Enchantments: A Biblical Perspective 193

Of course, with great privilege comes great responsibility.
“To whom much is given, much is required” (Luke 12:48).

At the exodus, Israel was called out of homeless bondage,
and promised a New Home. But at Kadesh-Barnea they refused
to receive it (Numbers 14). Thus, they remained in the wilder-
ness, wandering and dispossessed.

When they finally returned to Kadesh-Barnea after a genera-
tion, Moses warned them that if they rebelled, they would suffer
expulsion just like their parents, just like Adam and Eve (Deu-
teronomy 28:2-68). He warned them that their New Home was
received by grace, but kept by faithfulness.

This message was reiterated by all the prophets from Isaiah
to Amos, from Jeremiah to Zephaniah, from John the Forerun-
ner to Paul the Apostle. With a single voice they exhorted the
people to remain faithful (“faith-full” or “faith that is full” of
righteous good deeds; see Philippians 2:12-16; James 2:14-26).
Otherwise, they would lose their promised dominion, their
Home,

Our Heavenly Home

In the New Covenant our promises are better, our hope is
better, and our Home is better than that of the sons of Sinai and
Moriah, For by Christ, “the Apostle and High Priest of our Con-
fession” (Hebrews 3:1), we have become partakers of a Heavenly
calling and entered into Zion.

For you have not come to a mountain that may be
touched and to a blazing fire, and to darkness and gloom
and whirlwind, and to the blast of a trumpet and the
sound of Words which sound was such that those who
heard begged that no further Word should be spoken to
them. For they could not bear the command, “If even a
beast touches the mountain, it will be stoned.” And so
terrible was the sight, that Moses said, “I am full of fear
and trembling.” But you have come to Mount Zion and
to the city of the living God, the heavenly Jerusalem,
and to myriads of angels, to the general assembly and
Church of the first-born who are enrolled in heaven, and
to God, the Judge of all, and to the spirits of righteous
men made perfect, and to Jesus, the mediator of a New
