 

Give me your tired, your poor,
Your huddled masses yearning to breathe free,
The wretched refuse of your teeming shore.

Send these, the homeless, tempest tost to me,
I lift my lamp beside the golden door.

Inscription on the Statue of Liberty

 
