66 ‘Tne Dispossessep

And he needed to comfort others as he himself had been
comforted, by the Word of truth.

To the modern ear this plan for the treatment of chronic
menial illness seems naively simplistic. (For a more detailed
study of the Gerasene story and its relevance to schizophrenia,
see Appendix 2). And yet the most technologically advanced
testing has borne out time and time again that this “nouthetic
therapy” based upon the Gerasene paradigm not only works, but
it works phenomenally well.*

In a study conducted by the Lafler Institute for Psychophar-
macology, nouthetic therapy was compared to “traditional” psy-
chotherapeutic treatment for the severely psychotic. The pa-
tients who received no psychoanalysis, no psychiatric care, no
drugs or psychotropic agents, but only “pastoral counseling” and
“rehabilitative discipleship” had a 62% higher recovery rate than
those treated “in accord with standard psychiatric operating pro-
cedures.” The study defined rehabilitative discipleship as . . .
“confrontational and ethical discipline. The evangelical pattern
of admonishment, encouragement, and community participation

. as drawn from Scriptural analysis. . . .*5 In other words,
the “nouthetic” control group was turned over to Inca] churches
where their psychoses and delusions were confronted with the real-
ity of God's transforming Word and God's reinforcing community.

And it worked. Better than the best that the professionals
had to offer.

The various and sundry U.N. agencies charged with direct-
ing the International Year of the Homeless want nothing to do
with the Gerasene Paradigm. Most professionals have never
heard of it. And too many pastors have never thought to make
use of it, either. The same study conducted by the Laffer Insti-
tute revealed that only 24% of all the pastors contacted in an
eight state region “had even the slightest acquaintance with reha-
bilitative discipleship . . . or utilized even the most rudimentary
fashion . . . of Biblical counseling in the course of their regular
parish ministry,”54

“All Scripture is inspired by God and profitable for teaching,
for reproof, for correction, for training in righteousness, that the
man of God may be adequate, equipped for every good work”
(2 Timothy 3:16-17). Only the Bible can tell us of things as they
really are (Psalm 19:7-11) because only the Bible faces reality
