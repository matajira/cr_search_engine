252

21.

22.
23.
24,
25,

‘THE DispossEsszD

Subcommittee on Housing and Community Development of the Com-
mittee on Banking, Finance, and Urban Affairs, U.S. House of Repre-
sentatives, Homelessness in America (Washington: U.S. Government Print-
ing Office, 1984).

The Los Angeles Times, October 27, 1983.

US Magazine, May 19, 1986.

People Magazine, February 3, 1986.

The full appellation given to the designation is, “The International Year of
Shelter for the Homeless” (LYSH), but the popular press and even a ma-
jority of the monographs written in support of the “Year” use the abbrevi-
ated name, so that form has been adopted here throughout. See HAB-
ITAT “Press Kit,” United Nations Centre for Human Settlements,
Nairobi, Kenya, 1985.

H. TYSH Project Guidelines, UNCHS/HABITAT, Nairobi, Kenya, 1985.
. Technica] Co-operation Monograph Series, UNCHS/HABITAT, Nairobi,

Kenya, 1986.

. Housing America: Freeing the Spirit of Enterprise, UN IYSH Program,

U.S. Department of Housing and Urban Development Office of Public
Affairs, 1985.

. John E. Cox, “Objectives of the UN International Year of Shelter for the

Homeless (TYSH) 1987,” Ekistics 307, July/August 1984.

. Ken Auletta, The Underclass (New York: Vintage Books, 1983)
. Lawrence M, Mead, Beyond Entitlement: The Social Obligations of Citizenship

(New York: The Free Press, 1986).

. Newsweek, December 16, 1988.

US Magazine, May 19, 1986

. USA Today, March 13, 1986.
. The Houston Chronicle, May 26, 1986,

. Periodical Guide 1985, for The New York Times.
. Ibid., for The Los Angeles Times.

. Thid., for The Washington Post.

. Newsweek, June 2, 1986,

George Grant, In the Shadow of Plenty (Fort Worth, Texas: Dominion
Press, 1986).

. George Grant, Bringing in the Sheaves (Atlanta: American Vision Press,

1985).

. See the discussion in chapters 12, 13, and 14.
. Herbert Schlossberg, Idols for Destruction (Nashville: Thomas Nelson Pub-

lishers, 1983).

Chapter 1— The Drawing of the Dark: A Personal Look

1.

2.
3.

Hampton Faucher and David Peoples, Bladerunner: The Complete Screenplay
(San Diego: Blue Dolphin Enterprises, 1982), p. 51.

Ibid., p. 93.

Ibid., p. 58.
