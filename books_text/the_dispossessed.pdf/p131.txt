Epitaph in Rust: Unemployment HS

tenaciously to a rapidly sinking status quo. Some went down
with the ship. Instead of accepting and even advancing the
“gradual” transformation of the economy from a heavy industrial
base to a high tech information or service base, most Americans
fought the changes off with all their might.22 We were basking in
the victories of the 60s instead of bracing for the battles of the
80s. We failed to invest in more efficient manufacturing methods
for fear of trimming jobs.%3 We continued to subsidize declining
industries well beyond their legitimate life span in the economy.
We ignored the trends in the world marketplace — and covered it
over obstinately with protectionist tariffs and trade barriers.”
We stifled entrepreneurial activity and rewarded ploddingly con-
servative and predictably uncreative endeavors with special tax
structuring, occupational licensing, and industrial regulation.”
We caved in to union pressure to maintain archaic management,
pension, and investment policies as well as seriously inflated
wages, and minimum wage laws.” In the midst of the changes in
the world economy that were occurring, and with the full weight
of our industrial and government policy resisting those changes,
everything slowed. Other nations like Japan, West Germany, and
South Korea jumped into the place usually reserved for the U.S.
The economy was thrown into an odd and awkward stasis.”

Then in 1973, the bottom fell out with the energy crisis.°° De-
clining industries that had been putting off the inevitable for far
too long began to collapse.3! Instead of going through gradual,
natural transitions, American businesses were backed against
the wall. It was do or die. A lot of them died. Instead of taking
the opportunities afforded by the prosperity of the sixties to
launch into the future, we maintained our safe and comfortable
position in the past, We held on, We ignored the warning signs.
We resisted the changes. Then, when push came to shove, and
changes were forced upon us, we no longer had the option of mak-
ing a smooth, easy, and voluntary economic evolution. We were
faced with utter chaos and calamity. Massive layoffs ensued.
What could have been, what should have been a “gradual transi-
tion,” became a catastrophe.

During the last few years of the 70s and the first few years of
the 80s, the nation’s steel production capacity was slashed by
11%, automaking capacity was cut 8%, rubber manufacturing
capacity dropped 14%, and consumer electronics manufactur-
