Aubade: A Biblical Hope 227

David was confident.

But it was an impossible situation. To say that the odds were
against the brazen young Israelite is an understatement at the
very best. It was suicidal.

Or, at least it looked that way. But then, looks can be deceiv-
ing. What seems to be, is ail too often entirely out of line with the
facts.

And what were the facts? What did David know that Goliath
didn’t?

David knew that the land belonged to God, not to the Philis-
tines (Psalm 24:1). He knew that God had sovereignly entrusted
the stewardship of the land to the chosen people ( Joshua 1:2-6).
He knew that if they would simply obey God's Ward, heed His
ordinances, keep His precepts, honor His statutes, adhere to His
standards, and yield to his commands, then blessings would
come upon them and overtake them (Deuteronomy 28:2; Psalm
19:7-11; Psalm 119:1-2). They would be blessed in the city and
blessed in the country (Deuteronomy 28:3), They would be
blessed with fruitfulness and blessed with bountifulness (Deuter-
onomy 28:4-5). They would be blessed coming in and blessed
going out (Deuteronomy 28:6). Their enemies would be de-
feated and flee before them, in seven directions (Deuteronomy
28:7). They would abound in prosperity (Deuteronomy 28:11).
For the Lord Himself would establish them as “a holy people to
Himself” (Deuteronomy 28:9), and all the peoples of the earth
would be sore afraid of them (Deuteronomy 28:10).

It looked like David didn’t have a chance against Goliath.

But David knew better. He knew the facts.

He knew he wouldn’t lose. He couldn't tose. As a faithful
member of God’s legion, he was more than a conqueror (Romans
8:37). He was an overcomer (1 John 5:4). He was victorious (1
Corinthians 15:57). Already. The battle had been won.

So he said as much.

“This day the Lord will deliver you up into my
hands, and I will strike you down and remove your head
from you. And I will give the dead bodies of the army of
the Philistines this day to the birds of the sky and the
wild beasts of the earth, that all the earth may know that
there is a God in Israel, and that all this assembly may
