190 Tue DispossessED

the sole of your foot; but there the Lord will give you a
trembling heart, failing of eyes, and despair of soul. So
your life shall hang in doubt before you; and you shall be
in dread night and day, and shall have no assurance of
your life (Deuteronomy 28:58-66).

Since all men are sinners (Romans 3:23), we all fall under
the Curse, We are all naturally inclined to nomadism and dispos-
session.

That’s the bad news.

The New Home

But thanks be to God, He has not left us in that helpless
estate. He has offered us a glorious Hope by grace. He has
offered us Salvation from the Curse (Romans 8:1-2), Redemption
from the wilderness (Ephesians 2:4-7), and Restoration to a New
Home in a New Eden (Romans 8:18-22).

That's the good news.

Thus says the Lord God, “On the day that I cleanse
you from all your iniquities, I will cause the cities to be
inhabited, and the waste places will be rebuilt. And the
desolate land will be cultivated instead of being a desola-
tion in the sight of everyone who passed by. And they
will say, This desolate land has become like the Garden
of Eden; and the waste, desolate, and ruined cities are
fortified and inhabited.’ Then the nations that are left
round about you will know that I, the Lord, have rebuilt
the ruined places and planted that which was desolate; I,
the Lord, have spoken and will do it” (Ezekiel 36:33-36).

Thus, while godless men continue to reap the bitter harvest
of homelessness, God gives His faithful people Rest by grace. He
restores us to our original purpose. He brings us into His House-
hold (Ephesians 2:12-22).

And the Home He gives us, the Restoration and Rest He offers
us, is not merely spiritual, to be realized only when we attain to the
by and by. He gives us a Home here and now. He plants us in the
land (Exodus 15:17). He blesses us in the land (Deuteronomy
28:2-6). He establishes our future in the land (Luke 19:13).
