122 THE DispossesseD

Progtessivism

Biblical faith is progressive. Humanism is conservative,
whether left or right, whether liberal or traditional.?? The Bibli-
cal faith constantly presses for advance. It breaks old wineskins
(Luke 5:37-38). Humanism constantly presses for stabilization.
It relies on old broken cisterns ( Jercmiah 2:13).

The reason is simple, Biblical faith is innately optimistic.
Humanism is innately pessimistic.”

‘The Bible shows the righteous man starting with a corrupted
earth: thorns and thistles (Genesis 3:18). Through diligent labor,
obedience, thrift, and righteousness, man shapes and tills and
rules over the earth. Under the guidance of the Holy Spirit, he
takes it from chaos to order. By the power of the Holy Spirit, he
takes it from a wilderness into a garden (Isaiah 51:3; Isaiah
58:10-12; Ezekiel 36:33-36). The Bible is the story of Paradise
Restored .®4

Humanism on the other hand, looks at history quite oppositely.
The story of man for the humanist begins east of Eden in pristine
beauty.®! But with civilization comes pollution, ecological imbal-
ance, shortages, and chaos, The best man can hope for is to stall
the inevitable: utter desolation. The sun is burning out. The at-
mosphere is disintegrating. The ecosystem is collapsing. Man
holds a very delicate balance. According to the humanist, life on
earth is the story of Paradise Lost.2?

Thus in times of tension, in times of change, Christians look
forward with anticipation, with hope, and with faith. While hu-
manists tremble, fret, and fear, Christians move ahead, chal-
lenging the obstacles, utilizing opportunities, and posing solu-
tions. Humanists arc forced to fight to maintain, to conserve the
status quo, and to resist the future.

Conclusion

Long term sustained unemployment leads inexorably to
homelessness. Much of the reason for the sudden epidemic pro-
portions of dispossession in the U.S. is the transitional nature of
our economy and the incumbent rise in joblessness.

It only stands to reason then, that a major factor in combat-
ting homelessness should be job creation.

Many leading figures in both the U.S. Congress and the
United Nations argue that Government must create those jobs
