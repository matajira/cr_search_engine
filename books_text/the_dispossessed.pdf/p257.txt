Appendix 1: Do's and Dan'’s 24

world in worship, through the Lord’s Supper. To take the Lord’s
Supper is not to indulge in an abstract theological ritual. It is a
tangible offering to God, a consecration before God, a communion
with God, and a transformation in God. It is thus a conscious
drive at the heart of reality. It is the Word made manifest. In this
simple yet profound act of worship, the meaning and value of all
life is revealed and fulfilled. The poor, like all men, need a dou-
ble dose of reality. And only the Church can serve up that reality
as she gathers around the sacramental altar.

The Word reforms the lifestyles of the poor, The discipling
and disciplining process of life in the local Church repatterns a
man’s ways according to the ways of the Lord, confronting him
with the Word incarnate.

Like all the rest of us, the homeless desperately need the life-
style adjustments that only life in the Body can effect. The ritual
of worship and consistent discipleship trains them in humility,
joy, perseverance, diligence, responsibility, and gives them a
“new song.” It instills in them Godly habits. It repatterns them ac-
cording to the ways of God. Through constant fellowship within
the community of faith, the homeless have these new habits rein-
forced. Their expectations and desires are slowly brought into
conformity with the expectations and desires of the righteous.
They are reformed. And the “boundary of fear” restrains the
homeless from old patterns of sloth and _ self-destruction.
Through work requirements, moral expectations, and commun-
ity obligations, all enforced by Church discipline (Matthew 18; 1
Corinthians 5), they are encouraged to grow in grace and matur-
ity. They learn that their attitudes, actions, and inactions have
very real consequences (Galatians 6:7). They who are “weary and
heavy laden” are liberated from the slave market shackles of the
world and are yoked with the “gentle” and “easy” discipline of
Christ instead (Matthew 11:28-30). They are able to come Home.

Thus, only a distinctly ecclesiastical outreach to the homeless
—one that emphasizes the Word revealed, the Word made mani-
fest, and the Word incarnate~can genuinely and effectively re-
habilitate the homeless. Only the Church can offer Refuge from
the howling Wilderness.®

6. Dont violate your own rules. If you've established a Scrip-
tural standard for your ministry, don’t abandon that standard
whenever you run into an exceptional case. We can be flexible
