206 Tue Disposszssep

and assure us with His Life. So now we are similarly to care for
others (Luke 10:30-37). With great privilege comes great respon-
sibility. We are to do unto others as He has done unto us.

Whenever God commanded Israel to imitate Him in ensur-
ing justice for the wandering homeless, the alien, and the sojour-
ner, He reminded them that they were once despised, rejected,
and homeless themselves (Exodus 22:21-27; 23:9; Leviticus
19:33-34). It was only by the grace and mercy of God that they
had been redeemed from that low estate (Deuteronomy
24:17-22). Thus they were to exercise compassion to the dispos-
sessed, They were to give them Sanctuary.

Privilege brings responsibility. If Israel refused to take up
that responsibility then God would revoke their privilege (Isaiah
1;11-17). If they refused to exercise reciprocal charity then God
would rise up in His anger to visit the land with His wrath and
displeasure, expelling them into the howling wilderness once
again (Exodus 22:24).

The principle still holds true. This is the lesson Jesus was
driving at in the parable of the unmerciful slave.

For this reason the Kingdom of Heaven may be com-
pared to a certain king who wished to settle accounts
with his slaves. And when he had begun to settle them,
there was brought to him one who owed him ten thou-
sand talents. But since he did not have the means to
repay, his lord commanded him to be sold, along with
his wife and children and all that he had, and repayment
to be made. The slave therefore falling down, prostrated
himself before him, saying, “Have patience with me, and
I will repay you everything.” And the lord of that slave
felt compassion and released him and forgave him the
debt. But that slave went out and found one of his fellow
slaves who owed him a hundred denarii; and he seized
him and began to choke him, saying, “Pay back what
you owe.” So his fellow slave fell down and began to en-
treat him, saying, “Have patience with me and I will
repay you.” He was unwilling however, but went and
threw him in prison until he should pay back what was
owed. So when his fellow slaves saw what had happened,
they were deeply grieved and came and reported to their
