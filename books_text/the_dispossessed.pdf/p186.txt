170 Tue DispossesseD

 

What were these facilities?

There was, first of all, the widespread, unorganized relief that
the poor obtained for themselves through begging. This was an
accepted and acceptable method of help (Luke 16:19-22; Mat-
thew 15:22-28). The asking and giving of charity was, in fact, an
integral part of medieval life and the wandering beggar was one
of the characteristic figures of the age. He had received a dra-
matic place and honorable endorsement from Francis of Assissi
and many other religious leaders. The mendicant friar, the pil-
grim to the Holy Land, the scholar at the university, the wander-
ing minstrel or juggler or jester—they all had social approval
when they sought for alms, They gave to homeless begging a
kind of honor, a status, an ethical currency that made govern-
mental involvement not only unnecessary, but undesired as well.

In addition to mendicancy, before Luther there were three
avenues of organized help available to the needy that served to
keep the magistrates from considering social welfare initiatives.
One of these was the ancient and long-established institution of
the guilds. Whether they were social, craft, or merchant guilds,
all of them emphasized cooperative self-help, brotherhood, and
mutual benefit. Thus in times of privation or calamity, they took
care of their own. In addition, most of the guilds maintained
“works of charity” for the indigent in their communities. This in-
volved such things as feeding the needy on feast days, stocking a
common pantry with corn and barley for emergency relief, the
maintenance of hostels for destitute travellers, and other kinds of
intermittent and incidental help.

Besides the guilds, there were numerous private foundations
established throughout Christendom designed specifically to
meet the needs of the homeless and poor. It is quite evident from
records of the day that bequests and large gifts by individual
benefactors were as much a part of the medieval culture as they
are of our own.” At the time of the Reformation there were in
England no fewer than 460 charitable foundations.?” Money was
not only willed for the establishment of almshouses, hospitals,
orphanages, hostels, schools, asylums, poor farms, and granar-
ies, but also was designated to cover funeral costs, widows’ pen-
sions, tenement improvement, and other philanthropic enter-
prises on behalf of the poor.

Finally, and most importantly, there was the Church. Before

  
