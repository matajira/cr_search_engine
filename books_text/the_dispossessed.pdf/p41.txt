TWo

DUENDE DANCEHALL:
THE CRISIS

 

They have always been with us. They are with us still.

Shriveled and weatherbeaten, dispossessed vagrants begged
for alms from passing pilgrims outside ancient Ephesus. Today
those same timeworn faces and pleading hands can be found
along Fifth Avenue in New York, still thirsty for wine.

Disoriented and feebleminded, dispossessed ragmen pillaged
the grimy alleyways of eighteenth century Vienna. Today, those
same desperate and delirious souls collect tattered bits of rubbish
in cherished shopping bags on Peachtree Street in Atlanta.

Bruised and abused, dispossessed women slept in the dreary
haunts and sewers of Victorian London. Today, those same
broken lives bed down along Colfax Avenue in Denver, still
alone in the cold and the dark.

Wild-eyed and atrophied, dispossessed waifs scoured the
nightside markets of 19th century Paris ever alert for a hustle, a
con, or a debauched jaunt, Today those same youngsters, riven
with rootlessness, cruise Castro Street in San Francisco, like flies
on parade.

Homeless and hopeless, dispossessed farmers fled depres-
sion-racked Oklahoma in droves, only to huddle together in
ramshackle “Hoovervilles” and labor camps down the San Fer-
nando Valley. Today, those same discouraged and disillusioned
families crowd into tin-and-tent towns along the bayous and
under the bridges in Houston.

They have always been with us. They are with us still.

And ironically, they seem to be with us in greater numbers
than at any time since the Depression.

Anecdotal suspicion has given way to irrefutable evidence.
Social service agencies and institutional charities around the

25
