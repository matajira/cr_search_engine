And the sound of the ocean
still rings in my ear

And the beckoning question:
Why am I here?

Someday I know
Til find my way home.

Until then I'll walk these
Crowded streets alone.

Til sleep with the sidewalk
Under my head.

While somewhere my children
are fast in their bed.

But someday I know
Yll find my way home.

Dennis Welch
