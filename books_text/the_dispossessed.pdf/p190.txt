I74 ‘Tue DispossesseD

Luther’s plan called for the establishment of a “common
chest.” He recommended that “there shall be ordered for the bur-
ghers and kept in place for all times, two casks or council chests
in which bread, cheese, eggs, meat, and other foods and provi-
sions shall be placed; and there shall also be a box or two where-
in money may be put for the upkeep of the common chest.”

In addition to voluntary contributions from various sources
there was to be a “tax requirement” committing cach inheritor,
merchant, craftsman, and peasant to contribute to the chest each
year.*5 Servants and young laborers who did not own property but
had “burgher and parish rights” would have their portion deducted
by their employers. All this was ta be above and beyond the tithe
and would finance all relief as well as cover the costs of public edu-
cation and pay the salaries of the clergy. The fund was to be ex-
pended by ten supervisors or overseers, independent of the Church
but “chosen in an open burgher’s meeting in the parish hall.”

Almost immediately, the citizens of Leisnig in Saxony and
Ypres in France made Luther’s plan public policy.” In less than a
year, Germany's Emperor Charles V, England’s Queen Anne
Boleyn (and thus King Henry VIII), France’s Francis I, and Scot-
tand’s James V accepted and implemented the plan in some way,
shape, or form, or another.® A social welfare revolution occurred
almost overnight. Like the Reformation itself, it swept across the
continent, leaving it forever altered, The effect was threefold.

First, Luther's plan shifted responsibility for the poor from the
Church to the state. This jurisdictional turnaround was based upon
his conviction that the Church was “an institution of grace,”
while the state was an “institution of works.”*° He wanted a wall of
separation between the two.© He wanted to limit the Church to
“spiritual authority” and the state to “cultural authority,” having
suffered long under the corrupted medieval Church bureaucracy.
Every change in the social welfare system in the West since 1600
has simply been a development of this “Luther-esque” presupposi-
tion: Relief is the state’s responsibility, not the Church's,

Second, though Luther's plan maintained distinctions be-
tween the “deserving” and the “undeserving” poor, the seculariza-
tion of relief necessitated a redefinition of those terms. No longer
were the poor categorized by their relationship to the church.
Instead, they were identified by their trade, their property,®*
their heritage,® their education,* or their proclivity to work.5
