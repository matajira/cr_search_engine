SEVEN

THE BROKEN CITADEL:
HOUSING REGULATION

 

They stared at each other from across the cavernous room in
apparent recognition, in apparent disbelief. One of them, a huge,
hulking man with a shock of blonde curls matted and skewed
beneath a well worn Dodgers cap, was in the process of trading
his dignity for a five pound block of cheese. The other, a dimin-
utive, bespectacled man clad in immaculate work khakis, dignity
already long since gone, was waiting rather impatiently in the
long food stamps line.

When their eyes met, there in the New York City Social Ser-
vices Center, they were both surprised. The big man was so sur-
prised, he nearly dropped his teeth. And his cheese. The small!
man nearly dropped his file folder, filled to overflowing with the
artifacts of his demise.

“Hey... Hey, ain’t you Mr. Rodecker?” The big man
caught him before shame sent him in flight.

Rodecker’s stomach tightened into a knot. He wondered to
himself how much more humiliation he would have to endure.
“Yeah, Tam. And you're . . . Walker. Kevin Walker, if I'm not
mistaken.”

“Well, I'll be! Hah! Pll be! What the hell you doin’ here, Mr.
Rodecker?”

With his cheese in tow, and a wad of USDA paperwork bil-
lowing from the right rear pocket of his Levis, the big man
crossed the room toward a fidgeting, wary Rodecker.

“Look, Kevin, just leave me alone. Go away.”

“Hey, that’s no way to be neighborly, I just . . .

“We're not neighbors.”

“Yeah. Right. You made sure of that, didn’t you? Took care
of that real good.”

”

97
