60 Tue Dispossessep

May there be no sadness or farewell, when I embark;
for tho’ from out bourne of Time and Place the flood
may bear me far, I hope to see my Pilot face to face when
I have crost the bar.??

I suppose my message struck home somehow. Something in-
expressible passed between us. Then, obvious glee spread across
his face. He chortled happily and ducked back inside. Home.
His home.

Deinstitutionalization

Raul is the product of a social service revolution. It is a revo-
lution called deinstitutionalization. It is a revolution now mired
in controversy, threatened with catastrophic failure.

In 1955, nearly 50% of our nation’s hospital beds were occu-
pied by mental patients.2 At that time, there were approxi-
mately 550,000 people committed in our lock up wards,
asylums, and institutions.

Reformers within the psychiatric community felt that the sit-
uation was abominable. The quality of care was declining. Re-
covery rates were abysmal. Criticism was universal. The situa-
tion had to be remedied somehow. “Instead of locking them up
and throwing away the key,” said one early prominent reformer,
“we must find and then implement an apparatus of rehabilitation.
so that they can return to their communities and live out normal
non-institutionalized lives.”

The reformers, afier a long and protracted lobbying effort,
finally won the day when in 1963, President John F. Kennedy
signed into legislation a comprehensive deinstitutionalization
program. The goal of the legislation was naively hopeful, though
certainly praiseworthy; release all but the most chronically ill
mental patients from state-run asylums, and return them gradu-
ally to the community via halfway houses and the like.

Almost immediately the population of mental hospitals was
slashed by 75% .?8 As many as 1,000 patients were discharged
each day.” And new admissions were reduced to rates not seen
in neatly seventy-five ycars.28 Social scientists cbulliently heralded
the program as a monumental success.

Tt was anything but.

As it turned out, once discharged, most of these mentally ill

 
