Mama Don't Take My Kodachrome Away: The Media 461

Hebron. But Absalom sent spies throughout all the
tribes of Israel, saying, “As soon as you hear the sound of
the trumpet, then you shall say, ‘Absalom is king in
Hebron,’” Then a messenger came to David, saying,
“The hearts of the men of Israel are with Absalom” (2
Samuel 15:7-10, 13).

Absalom covered his conspiracy with a cloak of righteous-
ness. His conniving, malignant intentions were obscured by a
thoroughly benevolent, pious exterior.

And the king, taken as he was by that exterior, didn’t know
what was happening until it was too late. By then he was too
compromised to arrest the crisis, He was forced to flee (2 Samuel
15:14). He had to learn the hard way, as Eve had before him, that
just because someone or something looks “good,” “desirable,” or
even “delightful,” is assurance of precious little (Genesis 3:6). He
had to learn the hard way, as Paul would after him, that just
because someone or something comes disguised as an “angel of
light” or a “servant of righteousness,” is no guarantee of anything
(2 Corinthians 1:14-15).

What Absalom did was to take very real concerns and issues
and blow them out of proportion, twisting the situation to serve
his own ends: the overthrow of the reigning administration. He
took facts, figures, and anecdotes and molded them and shaped
them to fit his own predisposition. He called on all his skill, all
his charisma, all his personal attractiveness and all his inside
contacts. He played on the emotions of the people. He showed
an impeccable sense of timing. In short, he manipulated the sit-
uation masterfully. He exploited an aged king, a complacent ad-
ministration, and latent discontent, making “news” and making
“truth” by the sheer force of his proficient willfulness—not at all
unlike the modern news media and their masterful manipulation
of social issues like homelessness to give credence to their particu-
lar socio-political cause.

Absalom wreaked a lot of havoc. So has the news media. But
there is one thing that neither of them counted on: The good
guys always win in the end. There may be defeats along the way.
There may be major set backs from time to time. Tranquility
may be dashed. The faithful may be sent into flight. But only for
a time, In the end, the cause of the righteous will be upheld (Job
