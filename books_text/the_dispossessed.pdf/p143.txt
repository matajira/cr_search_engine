The Skies Discrowned: The Farm Crisis 127

All across the vast farm belt there is mounting alarm that the
grim harvest of failures and foreclosures that has marked the
agricultural economy for the last five years will continue on inde-
finitely. The fact is, the American farmer is facing the biggest
farm depression in fifty years—since the Great Depression.’
More than 400,000 farms are currently threatened with bank-
ruptcy and foreclosure?

With farm debt approaching the $200 billion mark, banks
are beginning to get nervous.? “When a farmer's debt-to-asset
ratio climbs above 26% or so,” says banker Mal McCabe, “finan-
cial institutions are forced—by market necessities — to call their
Joans due.” That hard economic reality has been especially bit-
ter in states like Iowa where in 1986 the average debt-to-asset ratio
is 36.9%, and North Dakota where the average is 34.7%, and
Nebraska: 34.3%, and Kansas: 31.8%, and Illinois: 30.8%, and
Michigan: 28.6%, and Wisconsin: 26.2% .'! It is really no won-
der then that in 1984 the Farmers Home Administration (FHA)
reported a delinquency rate of more than 25% ,'2 and most ex-
perts were estimating that another 30-40% escaped insolvency
only by temporarily refinancing debt." “Most farmers are just
buying time now. Waiting for the inevitable,” said McCabe.

Over the last decade, farmers have had good harvests.5
Their efficiency has improved dramatically due to the “green
revolution” and other technological advances.16 The weather has
not been terribly adverse.!” Rainfall has been generally good.*
Demand for food products is up.!% Marketing, processing, and
distribution are no problem whatsoever.2? So why arc we in the
midst of a terrible farm crisis?

“The fact is that the ‘farm crisis’ is not a ‘farm crisis’ at all,”
says Vern Oglethorpe, an Iowa farmer. “It is a political crisis.”

Farm Debt

In many ways, farmers have no one but themselves to blame
for the massive debt load they suffer under. They, after all were
the ones to sign the contracts, buy the machinery, and expand
the operations. This is the position Budget Director David
Stockman took when he opposed farm bailouts in 1984.2! But in
many other ways, Vern Oglethorpe is right and David Stockman
is wrong: The farm crisis has been precipitated by a number of
ill-advised political moves.
