The Golden Ship: The Secular Shift 473

Even so, before Luther, virtually no magistrate sought to
contravene the complete separation of government and charity.
Even monarchs like Scotland’s Queen Margaret, who had great
concerns for the poor, channeled their energies on their behalf
through the Church, not through the court.”

After Luther

But ail that changed with the Reformation and the influence
of Martin Luther.

The Reformation affected virtually every aspect of life in
Christendom.“ Mostly, for the good. The Bible was reestab-
lished as the sole rule for life and godliness. Orthodoxy was re-
affirmed. Apostasy was condemned, Corruption was exposed.
Revival was sparked. Commerce was invigorated. Culture was
spawned. Constitutional liberty was institutionalized. Souls
were saved, captives were set free, and life abundant was
granted to millions.

The Reformation caused a great deal of social upheaval,
needless to say. Again, mostly for the good. Still, there were
some adverse effects, especially for the poorest of the poor.

Local parishes all too often were thrown into an uproar, a
political brouhaha, that disrupted whatever was left of their re-
lief efforts. Instead of discipling the needy, ministers and deac-
ons wrestled over doctrinal concerns and struggled over the reins
of power. The poor were left out.

Monasteries, already waning in number and influence due
to corruption, were often forcibly closed down, thus eliminating
that outlet of service to the destitute and homeless. In 1536 and
1539 Henry VIII expropriated all the monasteries within his do-
main and divided their properties among his loyal followers.“!
Similar measures in Germany and Switzerland substantially di-
minished the availability of aid to the needy. And though Bucer,
Knox, Zwingli, and other reformers valiantly worked to restore
charity on a city by city and parish by parish basis, the numbers
of homeless and destitute only increased all the more.* In fact,
of all the reformers, only Calvin was able to make significant in-
roads in and around his city of Geneva.#

Luther, knowing that the abominable situation of homeless-
ness and dispossession had to be tended to if Christendom were
to further advance and if the Reformation were to survive, pub-
lished Liber Vagatorum and promoted its innovative notions.
