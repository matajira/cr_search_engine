The Labyrinth and the Garden 377

darkness for the Egyptians and a source of light for the Israelites (v.
20). Like the flaming sword of the garden, the flaming glory cloud
served as an unpassable barrier to the enemies of God. God created
a protective enclosed space for His people. The tabernacle and the
temple later symbolized this same sort of enclosed space. But at the
heart of this enclosed space is ethics. In the case of the tabernacle and
temple, the center was the holy of holies, in which the tablets of the
law resided.

The last instance we have in the Bible concerning Egypt as a place
of refuge prior to a “resurrection unto dominion” is the case of the
parents of Jesus. They obeyed the angel which told them to go down
into Egypt (Matt, 2:13-14). This was done to fulfill a messianic proph-
ecy (Hosea 11:1), “Out of Egypt have I called my son” (Matt. 2:15b).
After Herod’s death, they returned to Israel (Matt. 2:20-21).32 Herod,
the “guardian” at the “gate” of Israel, was no longer a threat.

The Guardian

Men who would have sought to enter Eden’s garden needed to
bypass the angelic guardians. Similarly, in the Melanesian legend of
the labyrinth, the supernatural female guardian serves as the
destroyer of anyone who does not know the secret of the labyrinth.
But what the Bible teaches is that access through the protected gate
is not based on secret knowledge; it is based rather on one’s covenan-
tal relationship with the God who assigned the angelic guardians to
their place.

Jacob, upon returning from the wilderness experience of labor
under Laban, faced a barrier, the Jordan River (Gen. 32:10), and a
guardian, his brother Esau. He wrestled all night with a theophany
of God who renamed him and gave him a blessing (32:24-29).
Moses, upon returning to Egypt with an uncircumcised son named
Gershom (from a Hebrew root meaning “driven out”),33 faced the
guardian of the border, an angel, and he was not permitted to pass
through until Zipporah circumcised Gershom (Ex. 4:24-26).3* In

32. That they should have traveled to Egypt is not surprising. A thriving colony
of Jews had lived in Alexandria since the days of Alexander the Great, who had at-
tracted them by promising them the same legal rights that Greeks possessed. They
prospered and helped the city to become a major commercial center, Henri Daniel-
Rops, Israel and the Ancient World (New York: image, 1964), p. 349.

33. Strong’s Exhaustive Concordance, Hebrew and Chaldee Dictionary, p. 28, #1644.

34. For a detailed treatment of this deeply symbolic event, see my essay, “Phe
Marriage Supper of the Lamb,” Christianity and Civilization, 4 (1985).
