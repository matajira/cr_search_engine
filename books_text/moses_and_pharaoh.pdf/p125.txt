Covenantal Judgment 107

his servants and his cattle flee into the houses: And he that regarded
not the word of the Lorp left his servants and his cattle in the field”
(Ex. 9:20-21). The problem: Where had they purchased these new
herds of cattle?

They could either have bought or confiscated cattle from the
Hebrews, since the Hebrews had not lost their cattle in the plague
(Ex. 8:6). The Hebrews had cattle after the hail, since the hail did
not strike Goshen (Ex. 9:26). It is also possible that cattle were im-
ported from Canaan. We are not told. If the Egyptians bought cattle
from the Hebrews, it meant that they were unwilling to confiscate
the Hebrews’ cattle. It would have been possible for them to have
marched in and taken cattle out of Goshen. They did not confiscate
all the.cattle, nor did they buy all the cattle, since the Hebrews still
had cattle when the hail began. Perhaps they never sought the
Hebrews’ cattle. We know that the Hebrews took large herds of cattle
with them when they left Egypt (Ex. 12:38). This indicates that the
Egyptians did not confiscate their cattle after the plague, and proba-
bly not after the hail. Conceivably, they did confiscate the cattle after
the hail, and gave back the cattle at the time of the Exodus, but the
Bible does not say that this happened.

When the hail stopped, the Israelites had all the cattle still alive
in Egypt. Did the Egyptians buy from them? It is not easy to say.
The Hebrews requested precious metals and jewels from the Egypt-
jans at the time of the Exodus, which might be interpreted as a
unique event, indicating that they had not owned any jewels before.
Did the Egyptians buy their cattle from the Hebrews without paying
in jewelry? We do not know. What we do know is that the Israelites
had crops and cattle, the key resources, immediately following the
hail. The slaves were now rich.

Of course, if the Israelites had been facing starvation, then their
position would not have risen absolutely. They would have been bet-
ter off than the Egyptians, but they would have had to consume their
tesources, making it impossible for them to have profited from their
position of relatively greater wealth. But thcy were being protected
by God; they were not going to starve. They might have traded their
cattle and crops for the surplus gold and jewels of the Egyptians, had
God decided to let them remain in Egypt. They would have collected
the valuables of the Egyptians in exchange for their surplus food.
However, God had even better plans; they would keep the food and
also collect the jewels of the Egyptians. God’s protection had already
