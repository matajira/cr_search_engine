60 MOSES AND PHARAOH

ity and tyranny, for they were extracting these labor services from
the consumer-oriented markets and redirecting them into statist
projects, These projects were State monopolies; there was no way to
calculate the benefits they conveyed to Egypt, except insofar as
Egypt was defined as the State, and the State was equated with the
Pharaoh. This enormous transfer of productive wealth—human
capital~from the market to the bureaucratic State benefited the
pharaohs in the short run, but it made the Egyptian economy less
productive and less rational economically. The value of the labor ser-
vices of an individual Hebrew could easily be calculated on a free
market; the value of the labor services of all Hebrews could not be
calculated in the State’s public works programs.

The Hebrews were forced to work rigorously. This was
significant as a means of oppression, it was not necessarily significant as
a testimony to the rationality of the Egyptian economy. By transfer-
ring their labor services to statist building projects, the Egyptian
taskmasters reaffirmed the commitment of the State to its own
deification at the expense of national per capita wealth. The State
would collect its huge “tithe” on a permanent basis. Yet it could not
guarantee that this “tithe” would be used efficiently. As the Egyptians
learned in the year of the Exodus, there had been far better uses for
Israel’s labor than the construction of treasure citics and coerced
work in the fields. The State could, for a time, extract labor from the
Hebrews; it was unable to escape the inevitable costs. It was also
unable to escape the necessity of making accurate cost-benefit
analyses, despite the fact that the pharaohs believed that they had
donc so. The Hebrews worked rigorously, but at the time of the Ex-
odus, Egypt learned how expensive this labor had been, and how
wasteful the expenditure had been, The Pharaoh of the Exodus was
no longer able to enjoy his treasure cities; he was at the bottom of the
Red Sea, and the treasures were gone.

A drowned Pharaoh, it should be noted, renders questionable
the simultaneous belief in two possibilities: 1) the conventional
dating of the powerful Eighteenth Dynasty in the fifteenth century
(whose pharaohs’ mummies still exist); and 2) the dating of the Ex-
odus in the fifteenth century. If you assume the former, you cannot
easily hold to the latter. Yet virtually all Christian historians accept
the fifteenth-century dating of the Eighteenth Dynasty. Thus, with
the exception of amateur historian Courville, they have wound up
arguing for both positions simultaneously, or worse, arguing for a
