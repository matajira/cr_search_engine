296 MOSES AND PHARAOH

omnicompetent State isn’t omnicompetent; it is progressively in-
competent, as Egypt’s experience reveals. Such a State is simply the
chief institutional manifestation of covenant-breaking man’s attempt
to imitate, and then usurp, the omnipotence of God, But omnipo-
tence is an incommunicable attribute of God. Therefore, the only
possible source of man’s long-term but limited power is biblical
ethics, Adherence to biblical law, by grace through faith, is the only
means of fulfilling the terms of the dominion covenant.

Thus, there is no need for Christians to become adherents of the
escapist religion in order to avoid becoming adherents of the power
religion. The dominion religion is God’s alternative. “We do not pre-
tend that the fate of the world is in our hands, That way lies mad-
ness, being a burden that no human being can bear. Yet, we are not
condemned to resignation and quietism, still less to despair. We are
not the lords of history and do not control its outcome, but we have
assurance that there is a lord of history and he controls its outcome.”$

I cite Schlossberg repeatedly becausc it is his book, more than
any other in my lifetime, which has best stated the theological case
against the power religion. (Solzhenitsyn’s Gulag Archipelago has best
stated the historical case.) The idols of covenant-breaking man—
history, humanity, mammon, nature, power, and religion—again
and again reappear in the civilizations built by self-professed auton-
omous man. All idols, he asserts, are either idols of history or idols of
nature.® Covenant-breaking man asserts his authority over both na-
ture and history, but because man is a creature, mankind is thereby
captured, for mankind is, in the humanist view, both a product of his-
tory and a product of nature. By seeking power over both, covenant-
breaking people place themselves in bondage to the self-professed
masters of the mysteries. By asserting that “man must take control of
man,” the humanist thereby implicitly asserts that “some men should
take control over all the others.” By seeking to exercise dominion
apart from God, ethical rebels thereby deny their own creaturehood
and therefore their status as humans.’

Egypt is the archetype of covenant-breaking society. It proclaimed
divinity for its leader, the sole link between-the gods and mankind. It
sought escape from the rigors of nature (drought and famine) and

5. Ibid, p. 304.
6. Hbid., p. Ul.
7. CG. S, Lewis, The Abolition of Man (New York: Macmillan, 1947),
