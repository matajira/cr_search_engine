Cumulative Transgression and Restitution 83

come due. They were collecting capital which lawfully belonged to
them. The Egyptians were simply paying restitution. God had prospered
the pharaohs by giving one of them a monopoly over the grain sup-
plies in an era of famine. God’s representative, Joseph, had provided
the necessary agricultural forecast and the efficient administration of
the program. Then the heirs of the Pharaoh enslaved the Hebrews,
who had been promised the land of Goshen as a permanent reward
(Gen. 45:8-10; 47:6), The State, in the person of Pharaoh; had
broken the covenant. That covenant was a civil covenant with God,
since it had been established with His people. God, in the day of the
Exodus, collected His lawful tribute. They had broken their treaty
with His people, and as the Hebrews’ lawful sovereign, He intervened
to bring judgment upon the Egyptians.

There is no escape from the laws of God, either individually or
socially, God held the Egyptians fully responsible for upholding their
covenant with the Hebrews. The Egyptians had profited from
Joseph’s warning. They had also profited from the labor provided by
generations of Hebrews. They were held fully responsible for deci-
sions made by the pharaohs. They paid for their sins, and because of
the additional capital possessed by the Egyptians of the Exodus
period —as a direct result of the reduced taxes paid by their ancestors
—they also paid for the sins of their fathers. After all, the Egyptians
of the Exodus period were the beneficiaries of the sins of their
fathers. They had been bound by the terms of their fathers’ promise
to Israel that the land of Goshen would belong to Isracl, and they
had broken this covenant.

The Egyptians paid more than jewels to the Hebrews. ‘They paid
their firstborn. God had told Moses that this would be the price ex-
tracted from them if they did not repent through their representa-
tive, the Pharaoh: “And thou shalt say unto Pharaoh, Thus saith the
Lord, Israel is my son, even my firstborn, And I say unto thee, Let
my son go, that he may serve me; and if thou refuse to let him go,
behold, I will slay thy son, even thy firstborn” (Ex. 4:22-23). The
Egyptians had enslaved God’s firstborn. They had, in effect, kidnap-
ped them, The penalty for kidnapping is death (Ex. 21:16). By refus-
ing to allow the Israelites to go and sacrifice to their God, the
Pharaoh was admitting that he was guilty of kidnapping, for he was

5. Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1982), ch. 23: “The Entrepreneurial Function,”
