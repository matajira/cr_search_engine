6 MOSES AND PHARAOH

Moses vs. Pharaoh

Pick up any commentary on the Book of Exodus. Read its ac-
count of the first fifteen chapters. You will find a lot of discussion of
Hebrew vocabulary, Moses’ theology, and the sovereignty of God’s
power. But what you will not find is a detailed discussion of Egypt.
You will not find an analysis of the theology and culture of the soci-
ety which placed the Hebrews under bondage. You will not find a
discussion of the relationship between Egypt’s theology and Egypt’s
economic and political institutions.

These are remarkable omissions, It is not that commentators
have no knowledge about Egypt. Rather, it is that they have failed to
understand the theological and political issues that were inherent in
this confrontation. Sufficient information is available to construct at
least an outline of Egyptian society. While Egyptology is a highly
specialized and linguistically rigorous field of study, there are num-
erous scholarly summaries of the religion and social institutions of
Egypt. I am no specialist in this field, and I have no immediate ac-
cess to a large university library of books and manuscripts relating to
Egypt, but interlibrary loans and normal intelligence are sufficient
to “open the closed book’ of at least the bare essentials of Egyptian
thought and culture. The bare essentials are sufficient to enable
anyone to draw some simple conclusions concerning the differences
between the gods of Egypt and the God of the Israelites, Further-
more, it is not that difficult to make other comparisons: socialism vs.
market freedom, bureaucracy vs. decentralized decision-making,
the omniscient State vs. limited civil government, static society vs.
future-oriented society, stagnation vs. growth. Yet the commen-
tators, as far as I have been able to determine, have systematically
refused to discuss such issues. They have been blind to the all-
encompassing nature of the confrontation. To a great extent, this is
because they have been blind to the implications of biblical religion
for both social theory and institutions.

Chronology

There are other topics that need to be discussed. One of the most
important is the problem of chronology. Commentaries can be found
that do attempt to deal with this issue, but I have yet to find one
which openly faces the overwhelming difficulties posed by the almost
universal acceptance of the conventional chronology of Egypt. What
