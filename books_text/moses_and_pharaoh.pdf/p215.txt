The Rule of Law 197

the patricians had calculated that they should possess a great power
in the exclusive knowledge of the law, but because the law, by its
origin and nature, long appeared to be a mystery, to which one could
be initiated only after having first been initiated into the national
worship and the domestic worship.”*

This meant that residence in a city was not the same as citizen-
ship. ‘his is a universal distinction: residency vs, citizenship. But it
is possible for residents to receive the protection of civil law. This
was the case in Israel, the only example in the ancient world of a
political order which granted comprehensive iegal protection for
religious aliens. Not so in classical civilization: “To live in a city did
not make one subject to its laws and place him under their protec-
tion; one had to be a citizen. The law did not exist for the slave; no
more did it exist for the stranger... . These provisions of ancient
law were perfectly logical. Law was not born of the idea of justice,
but of religion, and was not conceived as going beyond it. In order
that there should be a legal relation between two men, it was neces-
sary that there should already exist a religious relation; that is to say,
that they should worship at the same hearth and have the same
sacrifices. When this religious community did not exist, it did not
seem that there could be any legal relation. Now, neither the
stranger nor the slave had any part in the religion of the city. A
foreigner and a citizen might live side by side during long years,
without one’s thinking of the possibility of a legal relation being
established between them. Law was nothing more than one phase of
religion, Where there was no common religion, there was no com-
mon law.”9 In short, many gods, many law-orders.

Fustel had a tendency to exaggerate the impact of family religion
in later Greek culture, although its influence never died out. There
were important modifications in Greek political religion, especially
in Athens, from the sixth century before Christ until the conquest of
Greece by Macedonia, late in the fourth century, 8.c. Early in the
sixth century, Solon revised the laws of Athens and began to en-
courage’ immigration.!° Foreign craftsmen were promised citizen-

 

8. Numa Denis Fustel de Coulanges, The Ancient City: A Study on the Religion,
Laws, ond Institutions of Greece and Rome (Garden City, New York: Doubleday Anchor,
[1864] 1955), Bk. EE, ch. XI, p. 192.

9. Tbid., pp. 192-93.

10. H. D. E. Kitto, The Greeks (Baltimore, Maryland: Penguin, [1951] 1962), p.
100.
