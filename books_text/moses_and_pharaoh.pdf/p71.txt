Rigorous Waste 53

free market, and he overestimated the benefits of coercion, When
they left Egypt triumphantly, after God had reduced Egypt's econ-
omy by means of plagues and spoils, the Egyptians learned just what kind
of economic losses a nation can sustain as a result of kings’ errors in cost-benefit
analysis.

The Hebrews worked very hard. Did this ensure their produc-
tivity? Can we conclude that hard work is efficient work? How do we
measure or calculate efficient labor? How could the Egyptians have
made such estimations? How did they know when they were getting
“their money’s worth” out of these slaves?

If we accepted the labor theory of value, we would have to con-
clude that no matter what they were assigned to achieve, their
rigorous efforts must have produced profitable results. (This is a very
good argument against the labor theory of value.) But how can
anyone measure efficiency if there are no profits? A socialist econ-
omy has no profits and losses to compare. A divine monarch does
not permit a free market in labor services once he enslaves a people.
Slavery in Egypt in Moses’ day meant hard labor in constructing
treasure cities. Hard work led to waste on a massive scale. The
slaves’ efforts benefited the king, and the Egyptians patd for their king's
public works projects in many ways: lost labor that the Hebrews might
have provided the general population, lost raw materials that went
into the projects, and the greatest cost of all, the growing wrath of
God which would culminate in the destruction of the economy, the
Pharaoh, and the army. The enslaving kings no doubt were satisfied
with the transaction; the people, governed by a false theology, tem-
porarily may have approved; but the end result was unmitigated
destruction, The mere expenditure of human effort on State public
works projects does not guarantee a return on the investment which
is positive. Without a free market, in which the competing bids of buyers and
sellers of resources determine the allocation of scarce resources, there is no way for
the State’s officials to calculate economic value accurately, They can only
take estimates, but there is no self-correcting information system
available to inform them of the accuracy or inaccuracy of their
judgments.

Egypt had a theology which asserted the ability of the Pharaoh to
make such judgments, which is precisely the theology a consistent
socialist commonwealth must have if it is to be a valid substitute for a
market economy, The integration of all economic plans can be fur-
thered by the market, or it can theoretically be accomplished by an
