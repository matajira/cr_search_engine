220 MOSES AND PHARAOH

rules of the game.” The formal rules of political democracy overcame the for-
mal rules of legal predictability and equality before the law.

Legal Predictability and Judicial Sovereignty

What are some of the basic judicial aspects of a legal order which
respects the rule of law? Joseph Raz lists eight convenient guide-
lines:

1, All laws should be prospective, open, and clear. One cannot be guided
by a retroactive law that does not exist at the time of action.

2, Laws should be relatively stable,

3. The making of particular laws should be guided by open, stable,
clear, and general rules.

4. The independence of the judiciary should be guaranteed.

5. The principles of justice must be observed —open and fair hearings,
absence of bias.

6. The courts should have review powers over the implementation of
the other principles.

7. The courts should be easily accessible.

8, The discretion of crime-preventing agencies should not be allowed to
pervert the law.5!

The emphasis is on legal predictability. However, Raz is overly
confident in the courts as protectors of human freedom through the
rule of law. What is to prevent the courts from exercising the same
sorts of arbitrary rule that are characteristic of legislatures and ex-
ecutives? By establishing the civil court system as finally sovereign, a
defender of the rule of law violates the biblical principle of multiple
sovereignties. He lodges absolute final sovereignty in a human in-
stitution. Freedom can never survive long under such an absolutist
system. We have already seen in the United States the creation of
what lawyer Carrol Kilgore has called judicial tyranny,®? and what
Harvard law professor Raoul Berger has called government by judi-
ciary.5 As Berger concludes; “Let it not be said of us as Gibbon said
of Rome: ‘The image of a free constitution was preserved with decent

51. Joseph Raz, “Fhe Rule of Law and Its Virtue,” in Robert L. Cunningham
{ed,), Liberty and the Rule of Law (College Station, Texas: Texas A&M University
Press, 1979), pp. 7-ll.

52. Carrol D, Kilgore, Judicial Tyranny (Nashville, Tennessee: Nelson, 1977).

53, Raoul Berger, Government By Judiciary; The Transformation of the Fourtemth
Amendment (Cambridge, Massachusetts: Harvard University Press, 1977).
