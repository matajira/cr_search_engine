Unconditional Surrender 183

God’s moral universe is orderly (common grace), so there are
cause-and-effect relationships between righteousness (covenant-
keeping) and prosperity, This orderliness is no less a matter of grace
than God’s imputation of Christ's perfect righteousness to His peo-
ple. It is a world-ordering grace.

Terms of Surrender

God instructed Israel to destroy utterly the Canaanites (Deut.
7:16-24). Tt was failure on Israel’s part that resulted in the failure of
their mission (Jud. 1:21-36). They did not utterly destroy the cities of
Canaan. Why was this total destruction required by God? Because
God was establishing a new base of operations for Israel, one which
was to have been unpolluted by foreign gods, Once established in
the land, however, the Israelites were not to demand the uncondi-
tional and immediate surrender of every pagan nation, They were to
offer ferms of peace, which might involve perpetual servitude, to na-
tions far away from Canaan (Deut. 20:10-15). Furthermore, Israel
was not supposed to have a standing army, meaning a king who
multiplies horses, which are too easily used in offensive military
operations (Deut. 17:16). Conquest was by means of God’s word, as
Jonah the prophet was instructed to deliver to Nineveh. The “clean
sweep” in Canaan was unique in Israel’s history, and even here, the
terms of God’s unconditional surrender were not successfully im-
posed, for these terms required totally faithful servants, The imposi-
tion of the terms of unconditional surrender by God’s people
demanded unconditional faithfulness on their part. They failed.

Christ, however, was unconditionally faithful to God, and there-
fore He was able to impose these terms on Satan at the cross. The ex-
tension of His terms of surrender is what the New Testament era is all about.
The steady encroaching on Satan’s fallen kingdom is what the
preaching of the gospel and the establishment of Christian institutions, gov-
emed by God's law, are intended to accomplish. God’s terms are still
unconditional; men must surrender totally to God, either before
they die (or, in the case of the final generation, before Christ comes
in judgment), or else after they die (or after Christ comes in judg-
ment). Eventually, all mankind will surrender unconditionally.
“Wherefore God also hath highly exalted him, and given him a name
which is above every name, that at the name of Jesus every knee
should bow, of things in heaven, and things in earth, and things
under the earth; and that every tongue should confess that Jesus
