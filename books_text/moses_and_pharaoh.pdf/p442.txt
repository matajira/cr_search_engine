424 MOSES AND PHARAOH

the ICE devotes so much time, money, and effort to studying what
the Bible teaches about economic affairs.

There will always be some disagreements, since men are not
perfect, and their minds are imperfect. But when men agree about
the basic issue of the starting point of the debate, they have a far bet-
ter opportunity to discuss and learn than if they offer only “reason,
rightly understood” as their standard.

Services

The ICE exists in order to serve Christians and other people who
are vitally interested in finding moral solutions to the economic crisis
of our day, The organization is a support ministry to other Christian
ministries. It is non-sectarian, non-denominational, and dedicated
to the proposition that a moral economy is a truly practical, produc-
tive economy.

The ICE produces several newsletters. These are aimed at in-
telligent laymen, church officers, and pastors. The reports are non-
technical in nature. Included in our publication schedule are these
monthly and bi-monthly publications:

Biblical Economics Today (6 times a year)
Christian Reconstruction (6 times a year)
Tentmakers (6 times a year)

Biblical Economics Today is a four-page report that covers eco-
nomic theory from a specifically Christian point of view. It also deals
with questions of economic policy. Christian Reconstruction is
more action-oriented, but it also covers various aspects of Christian
social theory. Tentmakers is aimed specifically at church officers,
seminarians, and other people who are responsible for handling
church funds.

The purpose of the ICE is to relate biblical ethics to Christian ac-
tivities in the field of economics. To cite the title of Francis
Schaeffer’s book, “How should we then live?” How should we apply
biblical wisdom in the field of economics to our lives, our culture,
our civil government, and our businesses and callings?

If God calls men to responsible decision-making, then He must
have standards of righteousness that guide men in their decision-
making. It is the work of the ICE to discover, illuminate, explain,
and suggest applications of these guidelines in the field of economics.
