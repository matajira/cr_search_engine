272 MOSES AND PHARAOH

for a godly culture. The possibility of economic, social, military, and
agricultural collapse always faces a covenantally faithful people—a
warning of what will inevitably happen should they rebel against the
terms of God’s covenant (Deut. 28:15-68). Nevertheless, the cyclical
pattern of prosperity and depression, which denies the possibility of
perpetual expansion, is normative only for covenantally rebellious
cultures. God nowhere says that all societies will conform historically
to the cyclical pattern. Covenantal dominion is expansionist.

Economic growth is the inescapable product of the extension of
God’s kingdom. That the Protestant Reformation, and especially
Puritan Calvinism, produced historically unprecedented economic
growth, is not a random correlation.” Because nature is under the
curse, and therefore limited, such compound growth cannot be an
eternal process. Such expansion therefore points to an end of time, the
end of the limits of our curse-induced scarcity. Such growth therefore
points to a coming judgment, a cessation of the processes of the cursed
earth. Paganism’s attempt to substitute endless cycles of growth and
decay is an ancient ploy to blind men to the reality of final judgment.
The modern version of this ancient philosophy is the “zero growth”
philosophy.?! By abandoning the pagan use of the metaphor of
growth —a metaphor which of necessity includes decay — and replac-
ing it with a biblical version which relates ethical conformity to God’s
law with external dominion and expansion, godly men remind
ungodly men of the coming judgment. Growth will end when time
does; growth clearly cannot go on forever.

20. On this point, see Max Weber's classic study, The Protestant Ethic and the Spirit
of Capitalism (New York: Scribner's, [1904-05} 1958). For a defense of Weber against
those who have criticized his thesis, see Gary North, “Che Protestant Ethic’
Hypothesis,” The Journal of Christian Reconstruction, 111 (Summer 1976). For documen-
tation concerning the applicability of Weber's hypothesis in American colonial
Puritanism, see my three essays on Puritan economic thought and practice in New
England, 1630-1720, in The Journal of Christian Reconstruction, vol. V, no. 2; vol. V1,
nos. 1, 2.

21. The mast widely read example of this zero-growth philosophy is Meadows, et
al,, The Limits to Growth (New York; Universe Books, 1972). The book is subtitled: A
Report for the Club of Rome on the Predicament of Mankind. It was a kind of
“media event’ in the early 1970's. It is seldom quoted in the 1980's. Another example
of this sort of analysis is Entropy: A New World View, by Jeremy Rifkin and Ted
Howard (New York: Bantam New Age Book, 1980). The most forceful defense of
zero economic growth is E. J. Mishan’s book, The Economic Growth Debate: An Assess-
ment (London: George Allen & Unwin, 1977).
