304 MOSES AND PHARAOH

Velikovsky’s Controversial Reconstruction”?

In 1952, the brilliant and controversial Jewish scholar, Immanuel
Velikovsky, published Ages in Chaos, the first volume of a projected
series.23 The later volumes in the series were delayed for a quarter of
a century.” Ages in Chaos offered startling evidence that the accepted
chronology of the ancient world is deeply flawed. Specifically, there is
a 500-700 year “gap” in conventional chronologies which actually
should not be there. Because of the centrality of Egyptian chronology,
he argues, this gap therefore exists in the other chronologies of the an-
cient Near East and classical civilization. He labored long and hard
to prove his case, and his researches are awesome. His reconstructed
chronology has been verified (though not in the eyes of conventional
historians and archacologists) by several of his followers.2°

The Velikousky Affair

It is not appropriate to deal with the whole of Velikovsky’s works
in this appendix. His Worlds in Collision (1950) created universal out-
Yage among astronomers, So outraged were certain astronomers at
Harvard University that they put great pressure on Macmillan, the
publisher, to drop the book, despite its best-selling status. This cam-
paign began before the book had been published, and before any of
the critics had read it.26 Refuse to suppress it, they threatened, and
Harvard University’s astronomy department will not offer manu-
scripts to Macmillan’s textbook publishing division. Macmillan
eventually capitulated and gave the publishing rights of this best-
selling book to Doubleday, a company which had no textbook pub-
lishing division.

The book eventually went out of print in the United States and

22. “My work is first a reconstruction, not a theory. . . .” Immanuel Velikovsky,
“My Challenge to Conventional Views in Science,” Pensée, LV (Spring 1974), p. 10.

23. The whole series was to be called Ages in Chavs, with the first volume titled,
From the Exodus to King Akhnaton, The book became so well known as Ages in Chaos
that the real title never caught on.

24. Velikovsky, Peoples of the Sea; Ramses H and His Time (Garden Gity, New York:
Doubleday, 1978). These books officially are part of the Ages in Chaas scrics. But as L
said in the previous footnote, the general series’ title, Ages in Chaos, became too closely
associated with the title of the first volume, From the Lxodus ta King Akhnaton, a title
which nobody except Velikovsky has ever bothered to use.

25, Cf. Israel M. Isaacson, “Applying the Revised Chronology,” Pensée, IV (Fall
1974), Lewis M. Greenberg, “The Lion’s Gate at Mycenae,” sbid., HHI (Winter 1973),

26. David Stove, “The Scientific Mafia,” Pensée, TT (May 1972), p. 6.
