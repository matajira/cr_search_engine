204 MOSES AND PHARAOH

and other “strangers within the gates” of the advantages and moral
necessity of accepting biblical laws.*® They long ago adopted the lan-
guage of “right reason” and “human rights” to defend the common
law, which was heavily influenced by biblical law. In adopting in-
compatible judicial doctrines, Christians relinquished their claim to
the only law-order which is universally valid and universally binding:
biblical law.

God’s law-order cannot be successfully defended intellectually in
terms of natural law, because no system can be defended successfully
in terms of natural law. The strangers within the gates have the work
of the law written on their hearts, but they actively and wilfully sup-
press this testimony (see below: “Natural Law Theory vs. Biblical
Law”), They need the protection of biblical law, which has been re-
vealed to us in God’s word. God's people also need the protection of
biblical law, to protect them from the evil deeds of others. Every man
needs biblical law; and every man had better acknowledge his need
for a substitutionary sacrifice becausc of his own transgression of at
least some of the requirements of biblical law.

Self-Government

When biblical law is enforced without respect to persons, society
is given the legal structure which favors economic development and
external blessings. Men are told of their moral and legal respon-
sibilities before God. Seif-government under God's law is the primary form
of government in every sphere of life: civil government, family govern-
ment, church government, economic government. There is to be a
means of settling disputes: an appeals court which enforces biblical law
without respect of persons. There is an appeals court in the church
(Matt. 18:15-20; I Gor. 6:1-10) and the civil government (Ex.
18:13-26). No earthly government can possibly afford to police every aspect of
human action. No human court possesses sufficient economic
resources to do so. Any court which would attempt this seeks to im-
pose a top-down bureaucracy which is antithetical to personal initia-
tive, personal responsibility, and economic development. Such a
concept of government is the pyramid view, where the State is god,
an omniscient directing agent staffed by automatons who simply
carry out orders to the letter. The pyramid society is self-defeating; it is

20. Gary North, “Ihe Intellectual Schizophrenia of the New Christian Right,”
Christianity and Civilization, No. 1 (1982). This is a publication of the Geneva Divinity
School, in Tyler, ‘Texas,
