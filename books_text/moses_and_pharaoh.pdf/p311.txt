Imperfect Justice 293

development, but I believe that the establishment of /aisser-faire laid the
groundwork for a reform in the civil service in the latter part of the
century~the establishment of a civil service chosen on the basis of ex-
aminations and.merit of professional competence. You could get that kind
of development because the incentives to seek such places for purposes of
exerting “improper” influence were greatly reduced when government had
few favors to confer,

In these ways, the development of daisser-faire laid the groundwork for a
widespread respect for the law, on the one hand, and a relatively incorrupt,
honest, and efficient civil service on the other, both of which are essential
preconditions for the operation of a collectivist society. In order for a collec-
tivist society to operate, the people must obey the laws and there must be a
civil service that can and will carry out the laws. The success of capitalism
established these preconditions for a movement in the direction of much
greater state intervention,

The process I have described obviously runs both ways. A movement in
the direction of a collectivist society involves increased governmental in-
tervention into the daily lives of people and the conversion into crimes of
actions that are regarded by the ordinary person as entirely proper. These
tend in turn to undermine respect for the law and to give incentives to cor-
rupt state officials. There can, I think, be little doubt that this process has
begun in Britain and has gone a substantial distance. Although respect for
the law may still be greater than it is here [the United States], most
observers would agree that respect for the law in Britain has gone down
decidedly in the course of the last twenty or thirty years, certainly since the
war [World War IE], as a result of the kind of laws people have been asked to
obey... .

The erosion of the capital stock of willingness to obey the law reduces
the capacity of a society to run a centralized state, to move away from free-
dom, This effect on law obedience is. thus one that is reversible and runs in
both directions. . . .

This seems to me an important point. Once the government embarks
on intervention into and regulation of private activities, this establishes an
incentive for large numbers of individuals to use their ingenuity to find
ways to get around the government regulations. One result is that there ap-
pears to be a lot more regulation than there really is. Another is that the
time and energy of government officials is increasingly taken up with the
need to plug the holes in the regulations that the citizens are finding, creat-
ing, and exploiting. From this point of view, Parkinson's law about the
growth of bureaucracy without a corresponding growth of output may be a
favorable feature for the maintenance of a free society. An efficient govern-
mental organization and not an inefficient one is almost surely the greater
threat to'a free society, One of the virtues of a free society is precisely that
