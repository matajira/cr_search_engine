Separation and Dominion 141

implies their victory with God, in eternity but also in history. Further-
more, with respect to the enemies of God, their separation from God
leads directly to their defeat by God, not only throughout éternity, but
in time and on earth. Their eventual defeat by the people of God—
those who honor the terms of God’s separating covenant —in time
and on earth is an earnest (down payment) of their coming eternal
defeat. Without biblical law, men become progressively impotent culturally.‘”

When God separates His people from the world by means of His
separating covenant, He provides them with the means of external
victory, not simply their individualistic internal victory over per-
sonal sin, As God progressively separates His people in terms of
their conformity to His law, He thereby gives them their tool of do-
minion. The dogs of Egypt had more understanding of this fact than
did the fleeing slaves of Moses’ day. The dogs of Egypt had a better
understanding of the implications of God’s covenantal partition than
the twentieth century’s hordes of self-proclaimed experts in biblical
prophecy. The dogs of Egypt may have whetted their tongues, but if
they ate anyone, it was dead Egyptians, not dead Hebrews.

17. Gary North, “Common Grace, Eschatology, and Biblical Law,” The journal of
Christian Reconstruction, IIL (Winter 1976-77).
