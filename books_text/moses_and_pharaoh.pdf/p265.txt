15
THE FIRSTBORN OFFERING

The Lorp spake unto Moses, saying, Sanctify unto me all the firstborn,
whatsoever openeth the womb among the children of Israel, both of man
and of beast; it is mine (Ex. 13:1-2).

God owns all things (Ps. 24:1). Nevertheless, He set apart ritually
the firstborn males of Israel. It was a symbol to the people of Israel of
the sovereignty of God. Each family had to regard the precious first-
born as Ged’s property. “For all of the firstborn of the children of Israel
are mine, both man and beast: on the day that I smote every firstborn
in the land of Egypt I sanctified them for myself” (Num. 8:17). God set
them apart— sanctified them—as His special possession.

The symbolism of the firstborn is important in the Bible. Adam
was God's created firstborn son (Luke 3:38), and Adam was created
to serve God as God's assistant in charge of the earth. Adam’s rebel-
lion called forth God’s grace in sending Jesus Christ, the second
Adam (I Cor. 15:45). Jesus Christ was more than a created firstborn
Son; He was the only begotten Son of God, the second Person of the
Trinity (John 1:14, 18; Acts 13:33). This only begotten Son was set
aside by God as a living, literal sacrifice to God—the second sacrifice
that forever removed the first sacrifices (Heb. 10:9)—so that Gad’s
people might not taste the second death (Rev. 20:14-15).

The sacrifices of the Old Testament looked forward to Christ’s
final sacrifice. The owner of the pregnant beast could not profit from
the potential productivity of the first male offspring. “All the firstling
males that come of thy herd and of thy flock thou shalt sanctify unto
the Lorp thy God: thou shalt do no work with the firstling of thy
bullock, nor shear the firstling of thy sheep” (Deut. 15:19). The animal
belonged to God. To use a modern phrase, “God took His cut (percen-
tage) off the top.” He received an immediate payment; the owner had
to wait until the birth of the second male to reap his profit.

247
