Imperial Bureaucracy 41

Thoth. In fact it was believed in very early times in Egypt that Ra, the Sun-
god, owed his continued existence to the possession of a secret name with
which Thoth provided him: And each morning the rising sun was menaced
by a fearful monster called Aapep which lay hidden under the place of
sunrise waiting to swallow up the solar disc. It was imposstble, even for the
Sun-god, to destroy this “Great Devil,” but by reciting each morning the
powerful spell with which Thoth had provided him he was able to paralyze
all Aapep’s limbs and rise upon this world.**

Theologically, it was the Egyptians who were in bondage. It was
they who needed deliverance: ethical, political, and social. Instead,
they enslaved those people whose God could alone grant Egypt the
deliverance which all men need, the God who had granted them
preliminary manifestations of His power and mercy under Joseph.

Death and Resurrection: The Contrast

The significant point here is the difference between the biblical
and pagan views of death and resurrection. The places of the dead
did not become centers of religion or culture for the Hebrews, nor
were these locations considered the dwelling places of spirits, human.
or otherwise. They were just the caves or burial places of those who
would one day be resurrected, either to life or death (Dan. 12:1-3).
Death and resurrection were central concerns of both pagan and
biblical religion, but the heart of biblical religion is ethics, not ricual.
As James Peters has remarked, the center of the tabernacle and the
temple was the ark of the covenant, and inside this ark were the two
copies of God’s covenant with Israel, a covenant of ten “words” or
commandments. It is this summary of God’s laws of life, not the
physical remains of death, which is primary in biblical religion.

The periodic celebrations of social renewal by the ancients — the
chaos festivals—were their attempt to achieve metaphysical renewal.
The very cosmos itself was to be reborn periodically through men’s
acts of ritual chaos. They believed in a religion of revolution, By ritually
recreating the “time before time” —the time of the creation, meaning
the advent of order out of disorder — pagans celebrated their concept
of death and resurrection. In these festivals, of which the
Caribbean’s carnival and New Orleans’ Mardi Gras are pale imita-
tions, regeneration comes from below during a temporary cultural and rit-

34. E. A. Wallis Budge, “Preface,” The Book of the Dead (New Hyde Park, New
York: University Books, [1920] 1960), pp. xi-xii.
