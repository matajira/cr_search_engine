334 MOSES AND PHARAOH

cleric, amateur demographer, and economist,!? wrote his enor-
mously influential book, An Essay on the Principle of Population, in 1798.
In it, he made a series of dire analyses and prophecies concerning
overpopulation and looming food shortages—prophecies that he
revised downward in later editions of his book,!8 Unfortunately, his
nineteenth-century followers ignored his later revisions.!?

The most famous — and erroncous—of Malthus’ observations is
this: the means of subsistence increases arithmetically (“1, 2, 3, 4,
5... .”), while all species have a tendency to increase geometrically
(2, 4, 8, 16, 32. . . .”).2° There is no evidence for the existence of
these numerical relationships.?: Most important, we. cannot
measure a fixed “tendency”—and tendencies were all that he ever
claimed for his theory??— to geometrical expansion of population in
that crucial species, humanity. Malthus’ theory was refuted in the in-
dustrial West by three developments: 1) contraceptive technologies;
2) even earlier, by the very means of restraint he recommended, late
marriages; and 3) the rise of scientific agriculture, by which
mankind multiplied food even faster than man multiplied himself.
Malthus began to recognize this in later editions of his book. He
wrote in the final chapter of the last edition: “From a review of the
state of society in former periods compared with the present, I
should certainly say that the evils resulting from the principle of
population have rather diminished than increased, even under the
disadvantage of an almost total ignorance of the real cause, And if
we can indulge the hope that this ignorance will be gradually
dissipated, it does not seem unreasonable to expect that they will be
still further diminished.”?3 It should also be understood that he was
utterly opposed to abortion, contraceptive technologies, and other
“mechanical’ means of reducing the birth rate. In this sense,
twentieth-century “neo-Malthusians” have recommended policies

17, In 1804, he became the very first person to hold a chair in political economy,
at the newly founded East India College. He filled this post until his death in 1834.
William Petersen, Population (2nd ed.; New York: Macmillan, 1969), p. 142.

18. Gertrude Himmelfarb, The idea of Poverty: England in the Early Industrial Age
(New York: Knopf, 1984), pp. 113-22.

19, Ibid., pp. 122-32.

20. See Petersen, Population, p. 149.

21. Himmelfarb, p. 127.

22. Antony Flew, “Introduction,” ‘Thomas Malthus, An Essay on the Principle of
Population (New York: Penguin, [1970] 1982), pp. 19-21.

23. Cited in Warren S. Thompson, Population Problems (3rd ed.; New York:
McGraw-Hill, 1942), p. 29.
