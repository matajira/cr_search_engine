280 MOSES AND PHARAOH

rely on it, which meant that they could rely on Him. They did not
need to trust in some bureaucracy for their existence. They could
rest in God. And because they could rest in God, visibly, one day
each week, they learned that they could rest in God on the battlefield
and in the work place. Each family gathered its own manna daily
(Ex. 16:16). There was precisely enough for every individual, since
God, not man, was the central planner (Ex. 16:18). Once they began
to rely on God’s mercy as the source of their prosperity, and on bibli-
cal law, which was their tool of dominion, they could abandon the
religion of their fathers, the religion of bureaucratic welfarism. Each man
could then attend to his own affairs in confidence, even as each man.
had gathered up his own manna. Each man knew that he was under
the direct sovereignty of God, and that his own economic prosperity
was not metaphysically dependent on the State and the State’s bu-
reaucratic planning. Their fathers had never fully grasped this prin-
ciple, and their fathers had perished in the wilderness.

They would no longer long for Egypt. The sons would not be
burdened by memories of a welfare State existencc—unreliable
memories — where they supposedly had eaten “bread to the full” (Ex.
16:3). The children would no longer pine away for the food of bond-
age, “the cucumbers, and the melons, and the leeks, and the onions,
and the garlick” (Num, 11:5). Instead, they chose the bread of dili-
gence, the fruits of victory.

Conclusion

If there has been any universal religion in the second half of the
twentieth century, it has been the religion of the bureaucratic welfare
State. Not since the days of Egypt, said Max Weber in the first dec-
ade of the twentieth century, have we seen a bureaucracy like the
ones we are constructing, nation by nation.* And, he might have
pointed out half a century later, not since the days of Egypt have we
seen tax burdens like those which are universal today, In fact,
neither the system of universal bureaucracy nor the system of confis-
catory taxation in Egypt came anywhere near the systems the
modern statist priests have imposed on their populations — popula-
tions that groan under the burdens, but who cry for more benefits,
higher tax burdens on the rich, more coercive wealth redistribution,

4. “Max Weber on Bureaucratization in 1909,” Appendix I in J. P. Meyer, Max
Weber and German Politics: A Study in Political Sociology (London: Faber & Faber, [1943]
1956).
