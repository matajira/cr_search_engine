Index

West's, 329

population explosion, 343

population studies, 335

Potiphar, 32, 179, 375

pottery, 324, 369, 372

power, 5

power religion, xv, 1-3, 39, 199, 261,
295

power State, 244

Pravda, 59

predictability (legal), 212, 215, 219-23,

228, 243
present, 217
present-orientation, 76
President, 223
price (zero), 331
prices, 58-59, 96
pricing system, 102
priesthood, 250
priests, 160
princes, 19n
prison, 288, 375
privilege, 193, 230
procrastination, 286
procedure, 285, 289
production factors, 92-93
productivity, 327
profits, 53, 98, 102
progress
classical theory, 267-69
economics, 270-72
Edelstein, Ludwig, 267n
Egypt vs., 47
ethics &, 142, 266
inequality &, 75-76
loss of faith in, xv
Renaissance, 269
social theory, 142
promisc, 150
property, 117, 118, 210
protection, 192-93, 197, 204
Protestantism, 213
public law, 189
public works, 51, 53, 60, 90
Puritans, 225
Puritanism, 272n
Putnam, James, 306

413

pytamids, 54, 369, 370
Pyramid Age, 28, 29, 34, 46
pyramid society, 204-5, 211-14

Quakers, 213
quality of life, 354-55
Quay, Eugene, 343n
quietism, 296

Rachel, 85
Rahab, 65
Ramses II, 307, 313
rats, 342
rationalism, 201, 215, 234
Rawlinson, George, 7, 17n, 29n, 302-3
Raz, Joseph, 220
Reagan, Ronald, 344n
reason, 235
Rebekah, 66
rebellion, 237, 239
rebirth, 375
recoil, 259
reconstruction, 4, 275-76
redemption, 151, 165, 250n
Red Guards, 367
Red Sea, 185, 256, 295, 298, 307,
376
Redford, Donald, 385, 386
Reformation, 272
regeneration, 176, 236
regularities, 277-78, 279, 293
«Rehoboam, 386
relativism, 9, 126, 235, 240, 241
release, 254
Renaissance, 268
renewal, 42
representative government
Adam, 115
covenantalism, fi1
liberalism, 113-14
Pharaoh, 110, 180
reproduction rate, 22
residency, 197
resources, 96, 100-1, 331, 332, 333
responsibility
biblical law, 279
dominion religion, 5

 
