Covenantal Judgment 105

affect Goshen: blood, frogs,.and lice. Only with the fourth plague,
flies (or insects), did God declare that Goshen would be spared (Ex.
8:22). As slaves, the Israelites were under the covenantal rule of the
Egyptians, both for good (the economic benefits of living in an exten-
sive empire) and evil. Like the animals who were cursed because of
man’s sin in the garden, or draftees in an army ruled by an incompe-
tent Commander-in-Chief, so are the slaves of a rebellious: culture.
And in the case of the ethical condition of the Israelite slaves, their sub-
sequent behavior indicated that they were not wholly innocent victims
of a society whose first principles they should have utterly rejected. The
leeks and onions of Egypt had their appeal in Goshen, too,

That God should bring frogs to curse Egypt as the second plague
was fitting. The frog was an important fertility deity in Egypt. The
frog-goddess Heqet at Abydos was pictured as sitting at the bier of
Osirus, a god of death and rebirth. Frog amulets were popular as
symbols of new life and new birth. “The ‘matlametlo, a great frog
over five inches long, hides in the root of a bush as long as there is a
drought, and when rain falls, it rushes out. It comes with the rain as
the beetle with the rising of the Nile; both are symbolic of new life
and growth.”! Just as the Nile, Egypt's life-bringer, became the
death-bringer, so did the frogs become a plague.

After the frogs came the third plague, when the dust of the
ground became lice. Again, we can almost imagine Pharaoh’s
thoughts, as he stood scratching himself: “Look, I don’t need any
more proof of your mastery of magic. If you can’t get rid of the lice,
then just sit quietly. Who needs magical powers like yours at a time
like this?? The magicians tried to match this event and failed (Ex.
8:18). At this point, they capitulated. If they could not make the na-
tion even more miserable than it already was, if they were unable to
louse up Egypt even more, then their opponent must be God. “This
is the finger of God,” they said, using’ the same term which Jesus
used to describe the Spirit of God in casting out demons (Luke 11:20:
“finger of God”; Matt. 12:28; “Spirit of God”}. They recognized that
they were dealing with supernatural power which was greater than
their own, meaning a God who was more powerful than the gods of
Egypt. By telling Pharaoh that the finger of God was the source of

1. Jane E. Harrison, “Introduction” to E. A. Wallis Budge, Osiris (New Hyde
Park, New York: University Books, [1911] 1961), p. vii. The introduction actually
first appeared as an essay in The Spectator (April 13, 1912), and was included in the
1961 edition.
