54 MOSES AND PHARAOH

omniscient agency; in Egypt's case, this agency was supposedly the
Pharaoh. “He was a lonely being, this god-king of Egypt,” writes
Wilson, “All by himself he stood between humans and gods. Texts
and scenes emphasize his solitary responsibility. The temple scenes
show him as the only priest in ceremonies with the gods. A hymn to
a god states: ‘There is no one else that knows thee except thy son,
(the king), whom thou causest to understand thy plans and power. It
was the king who built temples and cities, who won battles, who
made laws, who collected taxes, or who provided the bounty for the
tombs of his nobles.” Egypt possessed the necessary theology for a
consistent socialist commonwealth, but this theology was wrong, as
the Egyptians learned in the year of the Exodus. The king did not
possess omniscience; he did not know what the true costs of enslav-
ing the Hebrews really were.

The pharaohs who constructed the mighty pyramids of the Old
Kingdom had weakened the Egyptian economy drastically. Wilson
has described these structures quite accurately: huge, noneconomic
construction projects that were supposed to last for eternity, but
which had to be followed by more of them in each generation.* The
brick pyramids of the later pharaohs were not equally majestic, but
their construction involved comparable problems. Were they
cost-effective? Only the Pharach could decide, since there was no
free market available for men to use as a means of evaluating the
true costs involved. The Hebrews were forced to work rigorously, but
this could not guarantee that they were working efficiently. The wit’s
definition of modern commercialism applies to the Pharaoh's
pyramids and cities: something done magnificently which should not
have been done at all. The Pharaoh, as a divinity, was supposed to
know what ultimate value really is, but he was not divine, so he
faced the inescapable economic problem that has baffled all central
planners, namely, the impossibility of making rational economic caleulation
in an economy without competitive free markets. This is the problem
described by Ludwig von Mises as the problem of economic calcula-

5. John A. Wilson, “Egypt,” in Henri Frankfort, et al., The Intellectual Adventure of
Ancient Man: An Essay on Speculative Thought in the Ancient Near East (University of
Chicago Press, [1946] 1977), p. 77.

6, John A. Wilson, The Burden of Egypt: An Interpretation of Ancient Egyptian Culture
(University of Chicago Press, [1951] 1967), p. 98.
