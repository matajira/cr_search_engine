Imperial Bureaucracy 31

inorganic, or abstract.”!! There was no absolute distinction between
creator and creature; instead, there was a continuity of being.

The Divine Monarch

The doctrine of continuity of being has a tendency to become the
doctrine of the divinization of man..Furthermore, the divinization of
man has an equally distinct tendency to become a doctrine of the
divine State, or the divine Church, or the divine Church-State. The
State, as the most concentrated power in human affairs, becomes the
mediating institution between the gods and evolving mankind. We
can see this in the history of Egyptian kingship, Wilson’s summary is
to the point: “The king of Egypt was himself one of the gods and was
the land’s representative among the gods. Furthermore, he was the
one official intermediary between the people and the gods, the one
recognized priest of all the gods. Endowed with divinity, the pharaoh
had the protean character of divinity; he could merge with his
fellow-gods and could became any one of them. In part this was
symbolic, the acting of a part in religious drama or the simile of
praise. But the Egyptian did not distinguish between symbolism and
participation; if he said that the king was Horus, he did not mean
that the king was playing the part of Horus, he meant that the king
was Horus, that the god was effectively present in the king’s body
during the particular activity in question.”!? The Pharaoh deputized
priests to perform religious duties, just as he deputized bureaucratic
functionaries to perform administrative duties, but State theory
maintained that these deputies acted for him as the supreme incar-
nation of the gods. Egyptian theology was polytheistic, but it. was
also monophysite: “. . , many men and many gods; but all ultimately
of one nature.”!3

To understand the enormous significance of the Hebrews’ stay in
Egypt, we have to understand the central position of the Pharaoh.
Joseph’s ability te interpret the king’s dream, and then to administer
the collection and distribution of grain, elevated the Pharaoh's posi-
tion, reinforcing the traditional Egyptian State theology. Then, two
centuries later, Moses smashed the very foundations of Egypt by
smashing men’s faith in their king’s position as a divine figure.
Again, citing Wilson: “The gods had sent him forth ‘to tend

1. Bid, p. 62.
12. Ibid., pp. 64-65.
13, iid, p. 66.
