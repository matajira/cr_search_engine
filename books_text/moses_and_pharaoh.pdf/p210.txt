192 MOSES AND PHARAOH

by His sovereign act of grace, and had placed all the Israelites under
the rule of His covenantal law-order. Covenants are always restrictive,
they exclude even as they include. The civil covenant is also restric-
tive. Rushdoony comments on Israel's civil covenant: “God as the
source of law established the covenant as the principle of citizenship.
Only those within the covenant are citizens. The covenant is restric-
tive in terms of God’s law; it is also restrictive in terms of a bar
against membership, which appears specifically, naming certain
kinds of groups of persons. This aspect of the law is usually over-
looked, because it is embarrassing to modern man, It needs there-
fore especial attention. In Deuteronomy 23:1-2, eunuchs are barred
from citizenship; bastards are banned through the tenth generation.
Ammonites and Moabites were banned through the tenth genera-
tion, or they are totally excluded, depending on the reading of the

 

text. Edomites and Egyptians were eligible for citizenship ‘in their
third generation’; the implication is that they are eligible after three
generations of faith, after demonstrating for three generations that
they believed in the covenant God and abided by His law. The
throne being the ark in the tabernacle, and the tabernacle being also
the central place of atonement, membership in the nation-civil and
in the nation-ecclesiastical were one and the same.”

There is therefore a covenantal relationship between the kind of
god a society believes in and the kind of citizenship which the society
creates. The foundation of both the theological covenant and citizen-
ship is farth. “Citizenship rested on faith. Apostasy was treason. The
believing alien had some kind of access to the sanctuary {II Chron.
6:32-33), at least for prayer, but this act did not give him citizenship.
The alien[s]— Egyptian, Babylonian, Ethiopian, Philistine, Phoeni-
cian, and any others —could be citizens of the true or heavenly Zion,
the city of God (Ps. 87), but the local Zion, Israel, was not to admit
the banned groups except on God’s terms. . . . Thus, it would ap-
pear from the evidence of the law that, first, a restrictive membership
or citizenship was a part of the practice of Israel by law. . . . Second,
the predominant fact in Israel was one law for all, irrespective of
faith or national origin, that is, the absolute requirement of justice
for all without respect of persons.” Non-citizens were protected by
God’s law. This meant protection from citizens who might use their
possession of citizenship as a means of exploiting strangers. It there-

4, R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), pp. 99, 100.
