384 MOSES AND PHARAOH

king.?* The Sphinx in Greek legend is representative of pagan un-
derground demons in general. Jane Harrison comments: “Two spe-
cial features characterize the Sphinx; she was a Harpy carrying off
men to destruction, an incarnate plague; she was the soothsayer with
the evil habit of asking riddles as well as answering them. Both func-
tions, though seemingly alien, were characteristic of underworld
bogeys; the myth-making mind put them together and wove out of
the two the tale of the unanswered riddle and the consequent deathly
pest.”39 The Sphinx was also a tomb-haunter.*?

Immanuel Velikovsky has identified the historical source of this
Greek legend: the Pharaoh Akhnaton. It was not Greece’s Thebes
that was the original location of these legends; ‘it was Egypt's
Thebes. There is strong evidence that Akhnaton committed incest
with his mother, Queen Tiy. Furthermore, this queen was associated
with the first appearance of a female sphinx. Akhnaton had two
sons, Velikovsky speculates, just as Oedipus had. One son, the fa-
mous King Tut (Tutankhamen), was buried with great splendor, just
at Eteocles was in the Greek legend. The other son, Smenkhkare,
was buried ignominiously, just as Polynices was (by his sister An-
tigone). The uncle of these two sons, Ay, the brother of Queen Tiy,
then made himself Pharaoh, just as Creon, the brother of Jocasta,
made himself king of Thebes.*!

This is not some crackpot theory. There is no doubt that Akhnaton
was incestuous, C. D, Darlington, the geneticist, has accepted Veli-
kovsky’s basic outline.*# So has the distinguished historian, Cyrus
Gordon, who calls the book a tour de force.*? Three anatomists, using
a microseriological method, demonstrated that both Smenkhkare and
Tutankhamen belonged to the same rare blood group.4* This in-
creases the likelihood that these two were brothers, as Velikovsky sug-
gested. The two mummies also had very similar skuli structures, two
of the anatomists, Harrison and Abdalla, have reported.*

38. Robert Graves, The Creek Myths, 2 vols. (Baltimore, Maryland: Penguin,
1955), II, p. 10, Sect. 105.2.

39. Jane Harrison, Prolegomena to the Study of Greek Religion (New York: Meridian,
{1963] 1960), p. 207.

40. Tbid., p. 2.

41. Velikovsky, Oedipus and Akhnaton: Myth and History (Garden City, New York:
Doubleday, 1960).

42. C.D. Darlington, The Evolution of Man and Society (New York: Simon &
Schuster, 1969), pp. 118-20.

43. Cyrus Gordon, “Oedipus and Akhnaton,” Pensé, il (Fall 1972), p. 30.

44. Nature, Vol. 224 (Oct. 25, 1969), p. 325.

45. Antiquity, Vol. 46 (February 1972), p. 10.
