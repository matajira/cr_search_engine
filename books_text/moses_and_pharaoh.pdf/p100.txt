82 MOSES AND PHARAOH

judgment, and their sons continued in the same sin. But there came
a time when the restitution came due. All the sabbatical years of
release that had been ignored, and all the capital goods that had
been required for them to give to the released slaves (Deut. 15:14),
had to be paid, plus a penalty for theft (Lev. 6:5), by that final
generation. They themselves went into bondage to the invading
Amalekites (Hyksos, called the shepherd kings by conventional
historians) for at least a century.t The glory of Egypt was removed.

If the sons are not to be punished for the sins of their fathers, why
should the generation in the year of the Exodus have been required
to bear such a heavy economic burden? The explanation which is
most consistent with biblical law is the argument from the concept of
familistic capital. The heirs of earlier generations of enslaving Egyp-
tians had become the beneficiaries of the labor of earlier generations
of Hebrews. The fathers and grandfathers had extracted labor from
the Hebrews at below-market prices. Had below-market pricing not
been in effect, they could have hired the Hebrews to construct the
cities, However, they wanted something for nothing: rigorous labor with-
out competitive wage rates. They had sunk their capital into
monuments for the Egyptian State. They had escaped the taxation
levels that would have been necessary to hire the services of the
Hebrews, had there been a free market for labor services. Their
heirs had become the beneficiaries of all the capital which had been
retained within the families—capital that would have gone to the
Pharaoh in the form of additional taxes to finance his self-glorifying
public works projects.

Furthermore, we can conclude that such capital could have been
invested in growth-producing activities. We have no idea what the
compound rate of economic growth was in that era, but some growth
in capital was possible, Therefore, the sons who saw their riches
“borrowed” by the Hebrews were simply returning the compounded
capital that they and their ancestors had coercively extracted from the Hebrews.
The Hebrews had been forced to pay homage, in the form of taxes in
kind (labor services, forfeited freedom), to the Egyptian State. Now
the heirs of Joseph were collecting on past accounts which had finally

4, Siegiricd J. Schwantes, 4 Short History of the Ancient Near East (Grand Rapids,
Michigan: Baker Book House, 1965), p. 76. Other historians believe that the period
of the Hyksos may have been two centuries. Courville believes that the Hyksos
period was over four centuries: from the Exodus to Solomon. The Exodus Problem, I,
pp- 124-25.
