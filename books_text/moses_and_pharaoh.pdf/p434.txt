416

population, 100
sin, 145, 160, 174-73
sincerity, 145, 172
Singapore, 327
slave
circumcised, 188
classical, 197
foreign god, 193
permanent, 193
release, 178-79
slave-masters, 182
slave wife, 85-6, 178
slaves, 25, 255
slavery, 32-34
debt, 193
dual, 255
era of, 50
escapism, 261
external, 260
laws of, 81
lifetime, 259
psychological, 260
reproach, 279
responsibility vs., 145
static, 275
training, 194, 260
triple, 256-59
U.S. South, SL
slick solution, 356
Smenkhare, 384, 385, 386
Smith, Adam, xii, 25, 209
social change, 142, 262
Social Darwinism, xii
social justice, 231
social order, 37, 199, 201, 205, 229-31
social regeneration, 368
social transformation, 187
socialism, 366
demonic, 119
population, 27
calculation, 53
poverty &, 328
secular, xvii
Third World, 339
socialists, 216-17
society, Hl
Sodom, 116-17, 149

MOSES AND PHARAOH

Solomon, 7, 21, 299-300, 324
Solon, 197-98
Solzhenitsyn, A. 119n, 296
soul houses, 369
sovereignty (see also “government”
absolute (God's), 49, 67
autonomy, 129, 130
bureaucracy, 103
courts, 220
delegated, 122
derivative, 131
Egypt's, 84
elite, 227
final, 118
institutional, 206
institutions, 212
law &, 196, 244
military victory, 84
multiple, 205, 207, 220, 223
normative, 267
ownership &, 118-19
partial, 120
Pharaoh's, 103, 108-10, 179, 184
plural, 240
token, 127
Soviet Union, 288-92
abortion, 345-47
Constitution, 288, 291
“defense,” 340
depopulation, 365
famine, 365-66
grain, 339
nuclear war, 360
peasants, 365
population, 347
“pro-marriage,” 364
Sowell, Thomas, 212
special-interests, 234
specialization, 90, 97
Sphinx, 383-84
spiral, 374
spoils, 260
spontaneous order, 229, 231, 233
stagnation, 333
Stalin, Josef, 365-66
Stamp Act, 225n
“starter,” 160, 161
