358 MOSES AND PHARAOH

cipled Christians and controversy in the media brought the change.
As U.S. Senator Everett Dirksen put it, “When I feel the heat, I see
the light.” The destroyers much prefer darkness because their deeds
are evil (John 3:19), (Sadly, Eerdmans republished it in 1985.)

Unholy Crusades

A standard cliché used against the idea of Christian civilization is
this: “What would you do, inaugurate another series of holy cru-
sades?” This sort of comment implicitly assumes that religious war-
fare is uniquely the product of Christianity, and it also assumes that
the crusades characterized Christian medieval civilization. Both
assumptions are false.

First, the term “crusade” was never used in the era in which the
four major ones took place, 1096-1204. It is a modern term. “People
at that time spoke of the road to Jerusalem, the voyage, the journey,
the pilgrimage.”!26 Second, a crusade was perceived as a defensive
war against an expansionist Islamic empire. Third, the wars were
supposed to be battles between professional armies. Except for the
outrageous sack of Constantinople, in the fourth crusade of 1204,178
common people were not deliberately chosen as victims. Remember,
the feudal order in Europe was essentially military, and a code of
military honor governed warfare. It was considered dishonorable for
a soldier to battle against peasants or commoners —a violation of the
separation of status groups. Finally, the armies were tiny, and so
were the ships that carried them, Green’s summary brings things
into perspective: “It would be impossible to talk of a nation in arms
in the Middle Ages. Most wars were fought by small armies, costly
for those who equipped them, but lacking in the total effort which
typifies modern warfare. Kings who started wars had no wish to
exterminate or unduly spoil their adversary; they wanted to bring
the issue to a successful negotiated conclusion. Wars were rarely na-
tional in any modern sense of the word, but conflicts over rights and
honour. . . . Moreover the numbers involved were small. The Vik-
ing raiders (each of whose ships can hardly have carried more than

126. Regine Pernoud, The Crusades (New York: Capricorn, 1964), p. 13.

127, Ibid., pp. 15-17.

128. The crusaders had only raised half of their expected troups, so they could
pay only two-thirds of their transportation bills to the Venetians, who demanded
payment. Finally, they sacked Constantinople to get the needed funds. R. W.
Southern, The Making of the Middle Ages (New Haven, Connnecticut: Yale University
Press, 1953), p. 58.
