162 MOSES AND PHARAOH

Cassuto believes that the law was given to the Israelites seven
weeks after the Exodus. Exodus 19:1 reads: “In the third month [new
moon], when the children of Israel were gone forth out of the land of
Egypt, the same day came they into the wilderness of Sinai.” Cassuto
comments: “The mention of the third new moon is not uninten-
tional. Since the Exodus from Egypt, the last two weeks of Nissan
and four wecks of Iyyar had passed, and we are now in the seventh
week. Since seven was considered the number of perfection, seven
days constituted, according to the customary conception of the an-
cient East, a given unit of time, while seven weeks formed a still
higher unit, and just as after six days of labour the seventh day
brought rest and the enjoyment of the results of that labour, so after
six weeks of the travails of journeying, the seventh week brought a
sense of exaltation and of drawing nearer to the word Divine.
Although the Torah does not state the exact day on which the Reve-
lation on Mount Sinai occurred, and only the later tradition con-
nects the Festival of Weeks with the commemorative of the giving of
the ‘Lorah, yet it is obvious that this tradition corresponds to what, if
not expressly stated in Scripture, is at least alluded to therein by in-
ference.”2°

The firstlruits offering the day following the sabbath of Passover
week was marked by the wave offering of the sheaf of grain ~ the un-
baked offering (Lev. 23:10-11). At Pentecost, or the Feast of Weeks,
forty-nine days later, the wave offering was a pair of leavened loaves
(Lev. 23:17). In the interim, the grain had been harvested, ground
into flour, allowed to rise by means of yeast, and baked as a com-
pleted offering to God. This symbolism of discontinuity, followed by
continuity, should be clear enough.

New Testament Symbolism

The same parallelism is present in the New Testament events:
the Passover meal of Christ and the disciples, followed by His death
and resurrection.3! Then, forty-nine days after Christ's resurrection,

30. U, Cassuto, A Comineniary on the Book of Exodus, translated by Israel Abrahams
(Jerusalem: The Magnes Press, The Hebrew University, {1951} 1974), p. 224,
"31. There are some difficult problems associated with the dating of Christ's
Passover meal with the disciples. The most convincing presentation is Hoebner’s:
they met on, Thursday night, Nissan M4, which was the Pharisees’ practice. The
Passover lamb was slain betwen 3-5 P.M. that afternoon by the Pharisees and
Galilcans. The Judean dating, used by the Sadducees, was different. They slew the
lamb that year on Friday afternoon, since they dated Nissan 14 from Thursday
