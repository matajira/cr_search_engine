Preface xiii

ity, but they do so by means of the same arguments that the founders
of the dominant intellectual stream, Darwinian central planning,
used against the Social Darwinists.4 They peddle the conclusions of
the really dangerous brand of Darwinism—the Darwinism of the
planning elite5—in the name of Christianity.

The hue and cry against my explicitly revelational Christian eco-
nomics has now been raised in the unread little journals of the Chris-
tian academic community.* What has offended them most is the
heavy reliance I place on Old Testament law. On this point, they are
in agreement with the antinomian pietists: all such laws are no
longer binding.”

Why this hostility to Old Testament law, or even New Testament
“instructions”? Because Old Testament law categorically rejects the
use of taxes to promote statist social welfare programs. It categoric-
ally rejects the idea of State power in coercive wealth-redistribution
programs. Samuel warned the people against raising up a king, for
the king would take ten percent of their income (I Sam. 8:15, 17). He
promised that the State would, in short, extract the equivalent of
God’s tithe from the hapless citizenry. And in the twentieth century,
most modern industrial civil governments extract four to five times
God’s tithe. The tax policies of the modern welfare State are there-
fore immoral. More than this: they are demonic.

“Proof texting, proof texting!” cry the church-attending Dar-

4. The best introduction to the history of this subject is Sidney Fine, Laissez Faire
and the General- Welfare State: A Study of Conflict in American Thought, 1865-1901 (Ann Ar-
bor: University of Michigan Press, 1956).

5. Gary North, The Dominion Covenant: Genesis, Appendix A: “From Cosmic Pur-
poselessness to. Humanistic Sovercignty.”

6. Sce, for example, the essay by Thomas E. Van Dahm, professor of economics
at Carthage College (which I had never before heard of), “The Christian Far Right
and the Economic Role of the State,” Christian Scholars Review, XII (1983), pp. 17-36.
He peddled another diatribe, this time against the biblical case for the gold stand-
ard, to The Journal of the American Scientific Affitiation, XXXVI (March 1984): “The
Christian Far Right and Economic Policy Issues.” This journal originally devoted its
space to essays critical of the six-day creation position, but in recent years, i¢ has
branched out, publishing articles that deny the legitimacy of applying Old Testa-
ment biblical standards in many other academic areas besides gealogy and biology.

7. Van Dahm writes; “This article did not deal with the basic issue of whether Old
Testament laws and even New Testament ‘instructions’ are binding on Chris-
tians—and others—in contemporary society. A recent treatment of this issue,
offering a definite ‘no’ answer I found persuasive is Walter J. Chantry’s God’s Right-
anus Kingdom... .” JASA, p. 35, footnote 44. Here we have it: the defenders of
power religion (statist planning) join hands with the defenders of escapist religion
(antinomian pietism) in their opposition to dominion religion (biblical law).

 
