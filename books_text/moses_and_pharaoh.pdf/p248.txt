230 MOSES AND PHARAOH

to which the coercive functions of government are strictly limited to
the enforcement of uniform rules of law, meaning uniform rules of
just conduct towards one’s fellows.”8+ But what if these “uniform
rules” discriminate against a particular economic group? The
graduated income tax is one example. There are thousands of
others, since virtually all of the modern welfare State’s legislation is
economically discriminatory,

The Preservation of Social Order: Market or State?

Hayek is caught in a dilemma. He wants a social order which ac-
tually preserves order. He wants a society which is rational, both for-
mally and substantively. On the one hand, he wants legal predic-
tability (formal rationalism). He wants eguality before the law. The pro-
blem is, market competition produces economic winners and losers.
“From the fact that people are very different it follows that, if we
treat them equally, the result must be inequality in their actual posi-
tion, and that the only way to place them in an equal position would
be to treat them differently. Equality before the law and material
equality are therefore not only different but are in conflict with each
other; and we can achieve either the one or the other, but not both at
the same time. The equality before the law which freedom requires
leads to material inequality.”®5 The losers can and do use democratic
politics to redistribute the winnings in the name of social justice. He then ob-
serves, with considerable historical justification, “More than by
anything else the market order has been distorted by. efforts to pro-
tect groups from a decline from their former position; and when gov-
ernment interference is demanded in the name of ‘social justice’ this
now means, more often than not, the demand for the protection of
the existing relative position of some group. ‘Social justice’ has thus
become little more than a demand for the protection of vested in-
terests and the creation of new privilege. . . .”86

On the other hand, he also wants the civil government to provide
a safety net, so that the social order of capitalism can be insulated
against revolutionary shocks. He thinks civil government can reduce
social disorder by violating his earlier principle of “unequal results
from equality before the law.” He wants a minimum welfare State: “The
reasonable solution of these problems in a free society would seem to

84. Studies, p. 165.
85, Hayek, Constitution of Liberty, p. 87.
86. Hayek, Studies, p. 173.
