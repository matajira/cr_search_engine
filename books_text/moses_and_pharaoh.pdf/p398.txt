380 MOSES AND PHARAOH

be refreshed — what Adam was supposed to have done in the garden.
But it is still a dominion training ground, which it does not seem to
be in the era after the resurrection. It'is a place which points to the
resurrection of many in the future, for it points backward to the res-
urrection of Jesus Christ.

Wilderness

Moses’ career was a three-stage encounter with the labyrinth and
the wilderness. For 40 years, he lived in Egypt, which had become a
labyrinth for the Israelites. He fled, for he knew his life was doomed in
Egypt (Ex. 2:15). For 40 years, he lived as a shepherd in the wil-
derness. There, in the “backside of the desert,” God confronted him
at Mount Horeb (Ex. 3:1). He was drawn out of that wilderness back
into Egypt. Then he spent 40 years in a new wilderness, having been
driven out of Egypt.

In each case, the number 40 was associated with a wilderness ex-
pericnce. In cach case, this experience involved training. Noah’s
world had become a labyrinth, a spiritual wilderness, a place fit for
judgment. The death of Noah’s world was accomplished by 40 days
of rain (Gen. 7:17), but he escaped in a massive enclosed space—a
garden experience, where. he cared for the animals. The army of
Israel was stymied for 40 days on the battlefield by the Philistines
under Goliath (I Sam. 17:16). Elijah fled into the wilderness (I Ki.
19:4), and journeyed 40 days until he reached Mount Horeb (v. 8),
where he lodged in a cave (v. 16), an enclosed space. Jesus also spent
40 days in the wilderncss, in preparation for His ministry (Matt.
4:1-2),

Moses’ forty years in the wilderness of the Exodus points to the
wilderness as both a labyrinth and a garden. For the older genera-
tion, it was a wilderness. They would ail die there, except Joshua
and Caleb. For the young, it was more of a garden, with the manna
and springs refreshing them daily. It was a place of wandering for
the older Israelites—a labyrinth from which there was to be no
escape —and a place of training for the young. The way out was
through the barrier, the Jordan River (Josh. 3:14-17). At Gilgal, they
were circumcised (Josh. 5:2-4), they celebrated the passover (5:10),
and on that day the manna ceased (5:12). The wilderness was no
longer their garden; the land of Canaan was, if they would remain
faithful and conquer it militarily in the name of the Lord. The way
through the garden was direct confrontation, city by city, culture by
