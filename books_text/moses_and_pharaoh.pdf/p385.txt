The Demographics of Decline 367

Despite the repeated connection between socialism and genocide,
this urge to mass destruction is more than mere economics. Stalin’s
purge of up to a million Communist Party members in the late 1930's
indicates that some other motive is involved. 162 The Soviet dissident,
Sakharov, says that between 1936 and 1939, over 1.2 million Party
members went into the camps, and only 50,000 regained their free-
dom.!63 Total arrests of all citizens in 1938 were probably over 7
million, possibly in the 9 million range.'6* This was in addition to
the 5 million already in the camps, and this did not include actual
criminals.!6> For twenty years of Stalin’s reign, 1930-50, at least 20
million people died in the camps or were executed, and this figure is
probably too low; it may have been 30 million dead.15 The chapter
title of Isaac Deutcher’s account of the period is hyperbolic, but more
accurate than he really believed: “The Gods are Athirst.”167

Asian Communism

This readiness to execute millions for the sake of Communist doc-
trine has been repeated: in Communist China under Mao and in
Cambodia in the 1970's. The Chinese death rate is a mystery. In the
first phase (1949-51), as many as 15 million may have died, or as few as
a million.'® In the second phase, the period of the “Great Leap For-
ward” (late 1950’s), we simply cannot know for certain. Mosher cites
evidence that in 1960 alone, the number of famine-related deaths may
have been as high as 30 million, or as low as 11 million.'® In the third
phase, the “Cultural Revolution” of the 1966-68, the Red Guards
murdered at least 400,000 people.'”° As for Cambodia, a fifth of the
Cambodian population, about 1.2 million people, died during the ini-
tial Communist take-over, from April of 1975 until early 1977.17

162. Robert Conquest provides various estimates that 500,000 to 1,000,000 peo-
ple were executed: The Great Terror, pp. 702, 713, For a profound literary account of
the purges, see the novel by Arthur Koestler, Darkness at Noon.

163. Conquest, p. 713.

164, Ibid., p. 702.

185. Ibid., p. 708.

166. Ibid., p. 710.

167. Deutcher, Stalin, ch. 9.

168. Paul Johnson, Modern Times, p. 548.

169, Mosher, Broken Earth, p. 264.

170, Johnson, Modem Times, p. 558.

171, Ibid., p. 657. This is the estimate of John Barron and Anthony Paul, Murder
of a Gentle Land (New York: Reader's Digest Press, 1977), p. 206. This book provides
many anecdotal accounts of the ruthless murders and torturings that went on in 1975
and 1976 in Cambodia.
