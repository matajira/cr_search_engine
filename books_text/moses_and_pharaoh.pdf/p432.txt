4l4

Egypt's, 83
escapism vs., 4-5
freedom &, 281
linear time, 175
Moses’ generation fled, 73, 145, 151
Pauls views, 259
self-government, 204-5
universal, 196,
resurrection, 40-41, 42, 251, 378
restitution, 81-84
Egypt’s, 116, 178
Pharaoh's, 116, 180
spoils, 254
restoration, 265
revelation, 100
revolution
final, 173-74
French, 142
past (Calvary), 157
religion of, 41, 186
social theory, 142-43, 148
revival, xvi, 176, 246
revivalism, 158n.
Ricardo, David, 335
riddle, 383
right (divine), 206
right (left), 42
right reason, 224
rights, 118, 128-29, 193, 202, 204
risks, 275
ritual, 37, 40, 43
Road to Serfdom, 228
Robbins, Lionel, 218n
Robespierre, Maximilien, 362
Rockefeller Foundation, 343n
Roe » Wade, 344
Rome, 137, 187, 211, 220-21, 381
Roman Catholic Church, 138
Roman Catholicism, 351-52
Roman Empire, xv
Romania, 348, 349
rope, 129
Répke, Wilhelm
creeds, 208-9
market ethics, 208
population, 26n
property, 210

MOSES AND PHARAOH

statism, 210
taxes, 210
Rothbard, Murray
diminishing returns, 92n
Leoni, 122n
Marxism, 121
monopoly, 56-57
Randians, 113n
rights, 111
society, 111
State, 121
utilitarianism, 114n
Royal Astronomical Society, 306
rule of law, 220, 228, 243
rumor, 69, 7t
Rushdoony
Armenia, 364n
citizenship, 192
covenant, 192
deification, 48
dowry, 85
family, 207
gnosticism, 3-4
guilt, 287
Jaw as warfare, 155
leaven, 16In
redeemer State, 287
sovereignty, 206
Tower of Babel, 47
unitary State, 206
Russell, Bertrand, 27
Russians, 347
Ruth, 190-91
Rutherford, Samuel, 225

sabbath
food, 277
polluted, 251
rest, 253
sabbitical year, 82
sacred cows, 341
sacred space, 379
sacrifice
comprehensive, 130
cost of, 248-50
living (ethics), 168
personal, 167-69, 181
