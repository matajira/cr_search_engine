386 MOSES AND PHARAOH

more familiar to most people than he is, although her prominence ap-
pears to have been great only during the first five years of his reign,
when he had not yet declared his monotheism or moved his capital
from Thebes to what is now called Tell-el-Amarna (Akhetaten).52 (It
turns out that as an infant, she had been wet-nursed by the wife of
Ay,53 who was the brother [“Creon,” argues Velikovsky] of Queen
Tiy.5* Nefertiti was not the wife of King Tut, contrary to popular
opinion.) Redford also acknowledges that Smenkhkare and Tutan-
khamen were brothers, but not that they were sons of Akhnaton.55

Akhnaton was narcissistic. Despite the destruction of his
monuments by his successors, we still have more clay and stone bas-
reliefs of Akhnaton and his family than we possess of the kings and
queens of England, from William the Conqueror (1066) to Queen
Elizabeth II. Many of the carvings are so detailed anatomically that
they can be described as exhibitionistic—unique in Egyptian
history .5°

Akhnaton, Tutankhamen, and Smenkhkare were late eightenth-
dynasty monarchs. They followed Thutmose III by at least a cen-
tury. What Velikovsky’s reconstruction indicates is that Thutmose
III was the Pharaoh Shishak, who invaded Israel in the reign of
Rehoboam, the son of Solomon.5? This was in the fifth year of
Rehoboam’s reign (II Chron, 12:2). Rehoboam came to the throne
around 930 z.c.58 Thus, Akhnaton, the great-great-grandson of
Thutmose [II,*° ruled sometime in the late ninth or early eighth cen-
tury, B.c., not in the mid-fourteenth century B.c., as the conven-
tional histories insist.6° In short, this Egyptian “inventor of
monotheism” may even have been a contemporary of the prophet
Isaiah, at least in the prophet’s youth. So much for Breasted’s “first
prophet” theory.

Breasted’s laudatory account of Akhnaton also includes the
sculpture of the era. The king’s artists were instructed to make the

52. Redford, pp. 78-79.

33. Ibid, p. 151.

54. Ibid., p. 207.

55. Ibid., p. 192.

56. Velikovsky, Oedipus and Akhnaton, p. 78,

57. Velikovsky, Ages in Chaos (Garden City, New York: Doubleday, 1952), ch. 4.

58. Edwin R. Thiele, A Chronology of the Hebrew Kings (Grand Rapids, Michigan:
Zondervan, 1977), pp. 2, 75.

59. Redford, Aktenaten, gencological chart, p. 13.

60. Christiane Desroches-Noblecourt, Tutanthamen (Boston: New York Graphic
Society, [1963] 1978), p. 105.
