The Psychology of Slavery 255

better to have served as earthly slaves in Egypt than to bear the visi-
ble risks and burdens of freedom under God. Neither the Hebrews
nor Pharaoh wanted to admit that God does not call His people to be
perpetual slaves in body, and that they are never to become slaves of
men in spirit. Neither Pharaoh nor the Hebrews could admit that
“where the Spirit of the Lord is, there is liberty” (II Cor. 3:17b).

The Bible contrasts two sorts of servitude: slavery to the earthly
life and its sinful perspectives, on the one hand, and slavery to God,
whose “yoke is easy” and whose “burden is light” (Matt. 11:30), on the
other, Slaves unto life or slaves unto death: men cannot escape from sub-
ordination, The lure of Satan is to convince men that they can achieve
freedom from all subordination by asserting their own autonomy
and sovereignty—in short, to be as God. The result is temporal
ethical subordination to Satan, and eternal judgmental subordina-
tion to God. When God's hand is lowered in judgment on a civiliza-
tion, ethical subordination to Satan also means destruction, in time
and on earth.

God recognized the weakness of these newly freed slaves. When
it came time for them to depart from Egypt, He led them east to the
Red Sea. Had they gone in a northeast direction, they would have
had to pass through the land of the Philistines, although geographic-
ally speaking, it would have been a shorter route into Canaan. But
God recognized their weakness; they were not ready for a direct
military confrontation (Ex. 13:17-18). They might have returned to
Egypt had they been challenged from the front. They were being
diiven from the rear, at first by the Egyptians who implored them to
depart, and then by the attacking army of Pharaoh.

This pressure from the rear was accompanied by God’s pillar of
fire and cloud in front of them (Ex. 13:21-22). They looked forward
toward the manifestation of God which preceded them, despite the
fact that their major motivation was rearward. If they were to flee,
they had to march forward, and they became used to following God's
theophany, the fire and the cloud. Psychologically, they were still
slaves of Egypt, always tempted to look backward at their former
home and former masters, but because they had been ejected, they
were forced to pay attention to the leadership of God. Psychologic-
ally speaking, they were marching out of Egypt more than they were
marching toward Canaan. But the discipline of marching was basic to their
training, a means of transforming their vision and motivation.
(Marching is a universally recognized means of training military
