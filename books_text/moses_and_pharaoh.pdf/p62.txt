44 MOSES AND PHARAOH

the other did not. One was a fertility cult; the other was not. As
Wunderlich remarks: “The idea of a link between veneration of the
dead and a fertility cult runs counter to our modern ways of think-
ing. But there is a close connection, so close that we might almost
speak of the cult of the dead as a form of fertility magic. . . . It is
based on an ancient belief that the dead know the future. Ancestors
are also responsible for providing for the continuation of the race.”*®

Such a view of the legitimacy of consulting the dead is utterly
foreign to biblical religion. The one example in the Bible of a
Hebrew leader consulting the dead was Saul’s use of the witch of
Endor—a “medium?” with a familiar spirit (I Sam. 28:7)—who called
up Samuel from the dead. This was in direct violation of Leviticus
19:31. God cut Saul off the very next day, as Samuel told him (I Sam.
28:19), thereby fulfilling the law’s warning: “And the soul that
turneth after such as have familiar spirits, and after wizards, to go a
whoring after them, I will even set my face against that soul, and will
cut him off from among his people” (Lev. 20:6). The use of the word
“whoring” points to the fertility cult aspects of the cult of the dead.

Neither system could be reconciled with the other. There could
be a temporary truce between them, but ultimately onc or the other
had to triumph. The confrontation between Moses and Pharaoh was
to determine which system would surrender to, or be defeated by the
other. God made it clear in advance to Pharaoh just which system
would lose: “For now I will stretch out my hand, that 1 may smite
thee and thy people with pestilence; and thou shalt be cut off from
the earth. And in very deed for this cause have I raised thee up, for
to show in thee my power; and that my name may be declared
throughout all the earth” (Ex. 9:15-16).

A Digression on Greece

It is revealing that Wunderlich, whose intellectual reconstruction
of the Egypt-influenced “palace” of Knossos—the Bronze Age,
labyrinth-based mausoleum venerating the cult of the dead on the
island of Crete— recognizes that only sterility and stagnation could
result from the cult of the dead. But instead of looking to Christian-
ity for an answer as to how the ancient world eventually cscaped
from this cult, he looks to the classical Greeks, He sees the classical
Greeks as the inheritors of Knossos. It was they, he argues, who

40. Hans George Wunderlich, The Seoret of Crete (New York: Macmillan, 1974),
pp. 294-95, 295-96.
