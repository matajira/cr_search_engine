78 MOSES AND PHARAOH

“Who made thee a prince and a judge over us?” This time, they
knew. This time, most important of all, they went free.

Conclusion

Moses was the victim of envy when the Hebrews of his youth
refused to subordinate themselves to his rule, and when they spread
the rumor of his execution of the Egyptian. He fled into the wilder-
ness for 40 years, leaving his brethren in slavery for an additional 40
years. Gossip placed a whole generation in needless bondage. It was
God’s judgment on them.

The sin of envy strikes the sinner. It restricts his ability to co-
operate with his fellow man. It rankles in his heart and can lead to
slower or even zero economic growth. In the case of the Israelites, it
led to an additional 40 years of bondage. The Hebrews preferred to
live in bondage to a socially distant, cruel, self-proclaimed divine
monarch than to subordinate themselves under a man of their own
covenant, They prefcrred to be slaves than to be under God’s repre-
sentative, Moses. They preferred the delights of rumor-spreading to
the delights and responsibilities of freedom. They preferred to tear
down Moses from his pedestal rather than elevate themselves, under
Moses’ leadership, to freedom. They received what they wanted,
another generation of servitude.
