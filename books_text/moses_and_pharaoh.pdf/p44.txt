26 MOSES AND PHARAOH

Conclusion

The historically unprecedented growth of the Hebrew popula-
tion in Egypt startled the Egyptians. It took 215 years for the 70
lineal heirs of Jacob and their circumcised servants, plus circumsised con-
verts attracted during the first 135 years in Egypt, to grow to 600,000 men,
plus women and children. This sign of God’s grace was visible to all.

Rapid, long-term population growth in response to covenantal
faithfulness is one of the promiscd blessings of biblical law. A poten-
tially greater blessing waited for them in the land of Canaan: no
miscarriages, long lives, reduced sickness (Ex. 20:12; 23:25-26).
These blessings did not occur; the continuing ethical rebellion of the
Hebrews led instead to population stagnation, a curse.

A growing population is a tool of dominion, as are all the bless-
ings of God. The humanists’ hostility to population growth in the
final decades of the twentieth century is part of a growing suspicion
of all forms of economic growth. Growth points to an eventual using
up of finite resources, including living spacc. This, in turn, points
either to the end of growth or the end of time. The thought of an end of
time within a few centuries is not acceptable to humanists. There-
fore, they have instead attacked the concept of linear growth, since
growth—especially population growth—cannot be linear indefi-
nitely in a finite universe,!® (See Appendix B: “The Demographics of
Decline.”)

Until these attitudes are seen by large numbers of Christians for
what they are— aspecis of paganism— Christians will continue to labor
under a modern version of Egyptian slavery. This slavery is both
religious and intellectual. It cannot be limited to the spirit and the
intellect, however; ideas do have social consequences. Christians can-
not legitimately expect to conquer the world for Jesus Christ in terms of the
ideology of zero-growth humanism. Such a philosophy should be handed
over to the humanists as their very own “tool of subservience,” the
opposite of dominion. Even better would be population decline for the
God-haters. They would simply fade away as an influence on earth.

18. This hostility to population growth compromised even Wilhelm Répke’s eco-
nomic analysis. His fear of “mass society” overwhelmed his otherwise good sense.
He. never understood that it is not sheer numbers of people that create “mass
society,” but rather the rebellious ethical and religious assumptions of the population
that create “mass society.” Répke’s anti-population growth theme appears in several
of his books, but especially in International Order and Economic Integration (Dortrecht,
Holland: Reidel, 1959), Pt. II, ch. IV: “The International Population Problem.”
