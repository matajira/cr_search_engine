Introduction 3

to biblical law. They have sought the blessings of God’s covenant
while denying the validity and cternally binding ethical standards of
that covenant. In short, they have confused the fruits of Christianity
with the roots. They have attempted to chop away the roots but
preserve the fruits.

 

2. Escapist Religion

his is the second great tradition of anti-Christian religion. Seeing
that the exercise of autonomous power is a snare and a delusion, the
proponents of escapist religion have sought. to insulate thernsclves
from the general culture—a culture maintained by power. They
have fled the responsibilities of worldwide dorninion, or even re-
gional dominion, in the hope that God will release them from the re-

 

quirements of the general dominion covenant.

The Christian version of the escapist religion is sometimes called
“pietism,” but its theological roots can be traced back to the ancient
heresy of mysticism, Rather than proclaiming the requirement of ethe-
caf union with Jesus Christ, the perfect man, the mystic calls for meta-
physical union with a monistic, unified god. In the carly church, there
were many ty, of mysticism, but the most feared rival religion
which continually infiltrated the church was gnosticism. It proctaimed
many doctrines, but the essence of gnostic faith was radical personal
individualism — personal escape from matter —lcading to radical imper-
sonal collectivism: the abolition of human personality through absorp-
tion into the Godhead, Tt proclaimed retreat from the material realm
and escape to a higher, purer, spiritual realm through various
“Eastern” techniques of self-manipulation: asceticism, higher con-

  

   

sciousness, and initiation into secret mysteries.

Gnosticism survives as.a way of thinking and acting (or failing to
act) even today, as Rushdoony has pointed out. ‘I'he essence of this
faith is its antinomanism. Gnostics despise biblical law. But their hat-
red for the law of God leads them to accept the laws of the State.
“Gnosticism survives today in theosophy, Jewish Kabbalism, occult-
ism, existentialism, masonry, and like faiths. Because Gnosticism
made the individual, rather than a dualism of mind and matter, ulti-
mate, it was essentially hostile to morality and law, requiring often
that believers live beyond good and evil by denying the validity of all
moral law. Gnostic groups which did not openly avow such doctrines
affirmed an ethic of love as against law, negating law and morality in
terms of the ‘higher’ law and morality of love, Their contempt of law

  
