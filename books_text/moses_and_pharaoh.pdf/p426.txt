408 MOSES AND PHARAOH

Jordan, James, 194
Jordan River, 157, 279, 376, 377
Joseph, 17, 31, 34, 83, 84, 109, 145, 375
Josephus, 15, 379
Joshua, 22, 140
jubilee year, 188, 194, 278
Judah, 190
Judas, 378
judge, 191n, 203
judges, 70, 153-54, 155, 195
judgment
Adam's, 96
discontinuity, 174-75
collective, 110, 202-4
Egypes gods, 144
ethical standards, 43
final, 137, 332
growth &, 272
righteous, 96
social change &, 150, 174-75 .
judicial supremacy, 225
Jukes, Andrew, 160-61
Jung, C. G., 374
Jupiter, 306
jury, 221-22, 243, 285
justice
arbitrary, 286
protection from citizens, 192
final, 173, 196
imperfect, 282-94
perfect, 205, 212
predictable, 283
procedure, 285
regular, 283
speedy, 283, 286
universal, 196
justification, 179, 181

Kabbalism, 3
Kallen, Horace, 306
Kennedy, John F., 359
Kenyon, Kathleen, 319
Keynes, J. M., xviii, xiv
Keynesianisrn, xi
Khrushchev, N., 345-46
kidnapping, 70, 83, 87
Kilgore, Carrol, 220

king lists, 17
King ‘Lut, 384
kingdom
civilization of, 163
economic growth, 272
extension of, 158
law &, 187
leaven, 159-60, 165, 169-72
maturing, 165
New Testament, 154
surrender to, 184
kinsman-redeemer, 378
Kino, H. D. F, 137
Knossos, 44, 322-24, 369, 371-72
knowledge, 95-6, 374, 379, 383
Koncharis, 312
Koop, C. Everett, 344n
Kronos, 306
Kulaks, 365

Laban, 32, 34, 85

labyrinth, 1800, 369-84
hell, 378, 383
resurrection, 378

Lachmann, Ludwig, 99n, 270-71

laissez-faire, 292

lamb, 249

Lamennais, 211

land, 264

land of the dead, 374

Lange, Oskar, 57

Laodicea, 357m

Last Supper, 153n

 

Latin, 138

law
blessings &, 263
capitalism, 214, 243

 

common, 225
confidence in, 201, 243
content of, 215
continuity, 137, 158
Darwin vs., 225
discovered, 223-4
dominion, 141

elitest, 226

evolution, 232
expediency of, 245
