16

MOSES AND PHARAOH

Correlation of Scriptural Incidents with Egyptian History
by the Traditional and Reconstructed Chronologies

 

Incident
or Era

Traditional
Background or Date

Reconstruction
Background or Date

 

Noachian Flood Not recognized as factual. The

Dispersion
from Babel

Abraham
enters Canaan

Famine of

Joseph

Enslavement
of Israel

The Exodus

Period of
the Judges

United
Monarchy
of Israel

Sacking of
Solomon's
Temple

Fall of
Israel to
Assyria

Fall of Judah
to Babylon

proper background for the im-
mediate post-diluvian period is
the Mesolithic period, dated ¢.
10,000 z.c. or earlier.

If recognized at all, the inci-
dent is set far back in the pre-
dynastic.

Commonly set in early Dynasty
XU dated ¢, 1900 B.c. Earlier
dates are entertained.

No famine inscription datable
to the era of Joseph as placed in
the Hyksos period.

Eighteenth dynasty theory of
Exodus must recognize an early
king of this dynasty as the
pharaoh initiating the enslave-
ment. This would be Amen-
hotep I or Thutmose 1.

Eighteenth dynasty theory
must recognize the position
either at the end of the reign of
Thutmose Il or early in the
reign of Amenhotep II, Date ¢.
1445 B.c.

Encompasses the period of Dy-
nasty XVIL from Amenbotep
WI, all of XIX as currenily
composed, and the first half of
XX. Dates: 1375-1050 3.c,

Background is in Dynasties
XX and XXI. Dates, 1050-930

BAC.

Shishak identified as Sheshonk
I of Dynasty XXII. Date is 926
B.c. in fifth year of Rehoboam.

Must be placed in the back-
ground of Dynasty XXIII to
retain the established date 722-
721 B.c.

In Dynasty XXVI. Date ¢. 606
B.C.

‘The Mesolithic background for
the immediate _post-diluvian
period is accepted. Daie ¢. 2300
B.C.

Dated 27 years before the unifi-
cation of Egypt under Mena.
Date, ¢. 2125 6.c.

Dated very soon after the be-
ginning of Dynasty IV; 1875

B.C.

Equated with the famine inscrip-
tion in the reign of Sesotris I of
twelfth dynasty. Dated 1662 B.c.

Enslavement initiated by Sesos-
tis IET of Dynasty XII. Date, «.
1560 B.c.

The reconstruction places the
Exodus at the end of the five
year reign of Koncharis, second
primary ruler of Dynasty XIII,
but 26th in the Turin list. Date
is 1446-1445 B.c.

Falls in the Hyksos period, ¢
1375-1050 #.c

Background is in early Dynasty
XVIII ending near the begin-
ning of the sole reign of Thut-
mose II. Dates, 1050-930 .c.

Shishak identified as Thutmose
IW of Dynasty XVIII. Date
926 B.c.

Falls in the fifth year of Mer-
neptah dated 721 k.c. S
nism indicated by inscription of
this year telling of catastrophe
to Israel.

In Dynasty XXV. Date c. 606
B.C.

  
