158 MOSES AND PHARAOH

enemies under his feet. The last enemy that shall be destroyed is
death” (I Cor, 15:24-26). Yet Christ’s victory is in principle behind
s: “For he hath put all things under his feet” (I Cor. 15:27a), Christ
has all power right now (Matt. 28:18). The great discontinuities of God's
covenantal history are past: His crucifixion, resurrection, and ascension.
The arrival of His Spirit at Pentecost gave us our official papers as
His ambassadors, our commissions as His judges. There are only
two great discontinuities remaining: Satan’s final rebellion (Rev.
20:7-8) and defeat (Rev. 20:9-10), and the final judgment (Rev.
20:12-15). (Some commentators might call these two discontinuities
the last continuity, since they take place close together.) In between,
there are the daily struggles between the two armies, the ebb and
flow of the mopping-up operations, and the progressive extension of
God's kingdom, in time and on earth.
Itis a mistake, then, to expect what Israel was told to expect. It is
a mistake to expect the delivery of our marching orders. It is a
mistake to expect visible, direct, cataclysmic interventions of God on
earth. Miracles still occur, but not the pillar of the cloud and the
pillar of fre. God's law is still in force, but we no longer need to have
its terms delivered to us on tablets of stone actually written by God.
There are still spiritually Canaanitic cities to be conquered, but not
by the blast of trumpets on our seventh day of marching around
them. We si at the Lord’s victorious table. We no longer stand, staffs
in hand. The lamb has been consumed already. We need not offer it
again. The blood is on our doorposts. We need not sprinkle it on
them again (Heb. 9}. The continuity of God's law, not the discon-
tinuities of God's military victories or miracles, is our standard.”

 

Unleavened and Leavened Bread

The unleavened bread which the Hebrews were commanded to
use during the Passover feast (Ex. 12:15) was a symbol of the impen-
ding discontinuity, their deliverance from Egypt. They had to cook and
eat in haste. It was not to symbolize affliction as such, for as Eder-
sheim wrote in the late nineteenth century, “the bread of the Paschal
night was not that of affliction because it was unleavened; it was
unleavened because it had been that of affliction. Fer it had been

23. It is one of the major weaknesses of revivalism in general, and the twentieth-
century Pentecostal movement in particular, that Christians have relied so heavily
on miraculous manifestations of the power of God, rather than relying on the con-
tinuous power of the law of God as a tool of dominion.
