322 MOSES AND PHARAOH

Egypt and Crete

We must understand how modern archaeologists operate, and
the extent to which they are tied to Egypt’s chronology by way of
Darwin. I have discussed the origin of the labyrinth design in Chap-
ter 2 and in Appendix C. The link between Egypt’s labyrinth and
Crete’s is recognized by informed archaeologists. Sir Arthur Evans
excavated the “palace” of Minos from about 1902 to 1930. He was an
evolutionist. He used the evolutionary speculations of anthropolo-
gists Edward Tylor and Lewis Morgan (as did Frederich Engels) to
provide a stage theory of historic development. Both of these schol-
ars became prominent in the 1870’s. This stage theory —savagery,
barbarism, and civilization ~ was first developed in the early nine-
teenth century by Swedish scholar Sven Nilsson, who wrote in the
1830’s.66 But where did Nilsson get the idea? From Danish scholar
Christian Thomson.*? Thomson came up with the idea of the divi-
sion of ages by construction materials— stone, bronze, and iron. In
1816, he had been given the difficult task of sorting out huge quan-
tities of artifacts possessed by the Royal Commission for the Preser-
vation and Collection of Antiquities, This collection was jumbled
together. What came first? He thought about it for three years, and
then came up with the Stone Age, Bronzc Age, Iron Age classifica-
tion. There were few references to iron implements, he knew, prior
to 800 8.c. Copper and bronze were mentioned much earlier. So the
bronze age must have come later. Common sense told him that the
stone age was earliest of all. The first scholars outside Denmark who
accepted this classification scheme were the Swedes and Germans.
At mid-century, the British scholars refused to accept it. A decade
later, after Darwin’s Origin of Species, the idea spread rapidly.®8

Evans used this assumption of cultural evolution— from primi-
tive to complex, from Bronze Age to Iron Age, from savagery to civi-
lization to explain the “palace.” McNeal is forthright: *. . . I have
said, in effect, that he went out to the hill of Knossos with certain
ideas in his head, and that he excavated the site in the light of his
previous intellectual commitments. In other words, the objects as
they came out of the ground were compelled (by force if necessary!)
to fit Evans’ prior ideas.”6?

66. R. A. McNeal, “The Legacy of Arthur Evans,” California Studies in Classical
Antiquity, VE(973), p. 207,

67. Ibid., p. 208.

68. Barry Fell, Saga America (New York: Times Books, 1974), pp, 29-30, 43-44.

69. McNeal, p. 209.
