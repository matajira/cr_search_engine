314 MOSES AND PHARAOH

Exodus have overlooked one important factor—a factor which, standing
alone, is adequate to negate this theory as far as meriting serious considera-
tion. This is the well-recognized fact that it would have required far less
than the situation described in Scripture to have resulted in a rapid and
easy rebellion on the part of the tribute-paying peoples, There would cer-
tainly have resulted a complete loss of any empire that Egypt may have con-
trolled at the time.

The empire of Thutmose [JI extended to the widest limits in all of
Egyptian history, All the cvidence points to the total absence of any such
crisis at the death of Thutmose ITT. . ...52

The opponents of an infallible Bible have recognized these prob-
lems, and they have forced baffled conservative commentators to re-
duce the significance of the Exodus to an event “of more manageable
proportions.”53 In short, conscrvative Christian historians have been
forced by their own chronological presuppositions to retreat from the
Exodus as an event of God’s massive judgment—an event which
God Himself said would be a warning and a testimony to the whole
world (Ex. 9:16). Courville cites E, Eric Peet: *, . . if the numbers of
the [Hebrew] emigrants were nearly 2,000,000, which is a legitimate
deduction from Ex. 12:37, the movement was one which would have
shaken Egypt to its very foundations, and which, even if it had failed
to be recorded in one of the numerous monuments which have sur-
vived in Egypt, would at any rate have left some unmistakable im-
pression in Egyptian history.”5+

Though Courville does not use the following analogy, it is clear
to me that the conservative defenders of the Bible are as trapped in
the chains of Egyptian chronology as the Israelites were trapped by
Pharaoh’s taskmasters. They, too, are afraid to depart from Egypt,
with its leeks, onions, and tenured teaching positions, for the wilder-
ness of an unknown chronology seems too great for them.

Courville is the closest thing to a Moses of biblical chronology
that this generation has seen. Another Seventh Day Adventist, Ed-
win Thiele, did superb work in reconstructing the chronology of the
Jater Hebrew kings, but the more difficult deliverance is the
deliverance from the. chronological empire of Egypt. Rather than
making Israel's history the reference point for the chronologies of the
ancient world, humanist scholars have clung to the unquestionably

92, Zbid., pp. 133-34.
53, Ibid. p. 135.
54. Idem. Peet, Egypi and the Old Testament (1924), pp. 105-6.

 
