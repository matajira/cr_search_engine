The Labyrinth and the Garden 381

culture, cutting each off, one by one. It was not a labyrinth for them,
not a place of wandering and indirect excursions. The pathway was
direct.

Broken Walls and Death

When the Israelites fled Egypt, Egypt died. When they fled the
wilderness, the manna ceased, and the wilderness returned to its
condition as a place of death. When the Israclites attacked the walled
cities of Jericho and Ai, these cities died. The whole culture of Ca-
naan died to the extent that the Israelites remained faithful and con-
quered the people of the land. But when Israel grew rebellious, var-
ious civilizations penetrated their walls (the Book of Judges), and
finally Assyria and Babylon completed the process. There was no
escape, God through His prophets warned them, no safe way out of
the land except through Babylon—to the east, The east was the
place of judgment. There would be no escape back into Egypt, Jere-
miah warned (Jer. 42:19). Those who dishelieved him perished in
Egypt when God delivered the Egyptians into the hands of Nebuch-
adnezzar (Jer. 44:30; 46).

God closed the Old Covenant era with the fall of Jerusalem.
Israel had become spiritual Babylon.®6 Israel had brought Babylon
back into the land through the Babylonian Talmud and other eastern
practices, The destruction of Israel by Rome was the fina] judgment
on geographic Israel. The Israelites were scattered in all directions.
Israel would never again serve as a garden place. God’s garden place
today is the church, and only the church. What seems to be a garden
place in our era— modern Israel ~ is a technological imitation of the
garden, not the spiritual place prophesied of old.

The End.of Old Israel

When men rebel against God, they are driven out of the garden.
This happened to Adam. It happened to the Israelites when they fied
from Egypt. It happened to them again with the invasions of Assyria
and Babylon. Finally, it took place under Rome’s dominion in 70
.b, Biblically, we see that when the dragon invades the garden and
men subordinate themseives to him ethically and covenantally, he
captures them. The only hope of deliverance is ethical deliverance,

36. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Tyler, Texas:
Reconstruction Press, 1985), ch. 21.
