Original Ownership 17

regard. The restitution payment was being extracted from the Egypt-
jans by a series of external judgments. Abram refused Sodom’s pay-
ment, while God demanded Egypt's. Egypt’s payment was no gift.

Moses’ message to Pharaoh was clear: God owns the world. He also
controls its operations. Moses reminded Pharaoh of the source of his
miseries. The land was in ruins. Egypt had been overtaken by a
series of disasters, But Moses’ point was that these had not been
“natural” disasters. They had been supernatural disasters. To prove
his point, he promised to pray to God, and God would then halt the
hail and thunderstorms. The proof of God’s ownership-is God’s word.
He made the world, He made man, and He is sovereign over both
man and the world. But to demonstrate His ownership before
Pharaoh, God stopped the hail.

His word was sufficient proof. He did not need to verify His
word before Pharaoh. Nevertheless, God provided the additional
evidence. Yet Moses told Pharaoh that he would not fear God,
despite the evidence. Pharaoh’s heart was hardened. The evidence
did not matter. If he would not listen to Ged’s prophet, he would not
assent to the evidence of his eyes. If God’s word was insufficient,
then the absence of hail would not be sufficient. He would still not
fear God. In this sense, he resembled Satan, whom he represented.

Pharaoh never did believe the testimony of his eyes. Right up until
the moment when the waters of the Red Sea closed over him, he refus-
ed to assent to the obvious. He raced into the arms of death, shouting
his defiance against God, breaking his word, and taking the Egyptian
State with him. He refused to believe God’s word, so the testimony of
his eyes meant nothing. His operating presupposition was that he, the
Pharaoh, was god. God is not the being He claims to be, nor could
such a being exist, Pharaoh presupposed. No sovereign, absolute be-
ing can lay claim to total control, and therefore original ownership of
everything, he believed. He died for his beliefs.

God’s claim ts comprehensive. He possesses absolute property
rights to every atom of the universe. He created it, and He owns it.
He, unlike man, does not operate in terms of a definition of owner-
ship which requires the right and ability of an owner to disown the
property at his discretion.! His ownership is original; no other being

1, FA. Harper writes: “The corollary of the right of ownership is the right of
disownership. So if I cannot sell a thing, it is evident chat [ do not really own it.” Lib-
erty: A Path to Its Recovery (Irvington, New York: Foundation far Economic Educa-
tion, 1949), p. 106.
