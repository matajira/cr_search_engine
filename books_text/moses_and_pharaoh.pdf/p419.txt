city &, 200
contracts &, 214
discontinuities, 158;
“dominion covenant”
Egypt, 115
ethical, 135
household, 199
judicial, 42
restrictive, 192
Ruth's, 191
covenant-keeping, 182
property, 118
representative, 115
separation, 135-36
social, 110
subordinates, 105
sword, 134
coyenantalism, Lil
covetousness, 72
cows, 341
craftsmen, 198
Crawford, O. G. S., 303
creation
discontinuity, 149
Egyptian view, 30
limited, 332
“new,” 251
sovereignty &, 265
creativity, 37, 329, 330
creed, 208-9
Creon, 384, 386
Crete, 44, 322-24, 369
crumbs, 10
crusades, 358-59
cult of the dead
Crete, 324
Egypt, 38-39, 49, 369-72
power religion, 297
resurrection, 38-39, 49
Cumbey, Constance, 330n
curse, 168
Custance, Arthur, 18n
cycles, 266, 267-69

Daedalus, 370, 35n
dancing, 370, 374
Darius, 146

Index 401

see also

Darlington, C. D., 384
Darwin, xiii, 225, 322, 335
Daube, David, 85n
David, 190
Davis, John J., 68n
DDT, 328
dead issues, xi
deadlines, 289
death
abortion, 349
animal sacrifices, 167
cult of (see “cult of the dead”)
firstborn, 83
Jabyrinth, 373
spiritual (Adam), 176
twentieth century, 360-61
decay, 266
decentralization, 208
defeat
Calvary, 155
Egypt over God?, 33, 177
pagan warfare, 132-33; 135,
power religion, 1, 295
separation & (Satan), 140
total, 1
training for, 173
deification, 48; see also “contimuity of
being”
degree, 227
deism, 113, xii
delegation, 122
deliverance, 144, 151, 159, 261, 382
Delta, 313
demands, 125-26
democracy
bureaucracy vs., 226-28
envy, 74
free market & 219-20
liberalism, 219
protection of, 229
demography, 335; see also
“population”
depersonalization, 36
depopulation, 365; see also “death,”
“zero population growth”
depravity, 205
desert, 380
