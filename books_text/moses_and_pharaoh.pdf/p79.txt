Rigorous Waste 61

later date for the Exodus. Such is the power of humanist scholarship
in our day that well-meaning Christian scholars have surrendered
themselves to the humanists.

Anyone who argues for a thirteenth-century date of the Exodus
has sold out the case for biblical inerrency by denying the truth of
I Kings 6:1. This is far more serious than making yourself look ridic-
ulous by arguing for the doubtful proposition that the Exodus really
did take place in the fifteenth century, but somehow it left no trace—
not even a hint of a minor regional dislocation — in the records of the
Eighteenth Dynasty, and furthermore, that the Pharaoh’s body was
somehow retrieved from the sea, mummified, and buried honorably.

It is the initial assumption which must be rejected — the fifteenth-
century dating of the early Eighteenth Dynasty —because I Kings 6:1
makes it impossible to date the Exodus in any other century except
the fifteenth. It was not some powerful early Eighteenth Dynasty
Pharaoh, whose mummies have all survived, who died in the Red
Sea. Some other Pharaoh, whose mummy did not survive, and who
was a member of some other dynasty, was the Pharaoh of the Ex-
odus. The Eighteenth Dynasty is therefore improperly dated by con-
ventional historians, Christian and non-Christian. This is why we
should take seriously Courville’s reconstructed chronology, at least
as a preliminary step for a thorough reconsideration of the chron-
ology of the ancient Near East.

Conclusion

The pharaohs, claiming omniscience, abandoned the free market
for labor—a market which offers men at least some means of
evaluating economic value. They claimed omnipotence, yet the
Pharach of the Exodus was totally vanquished. They extracted
rigorous service from the Israelites, yet they had no way of knowing
whether or not such service from the Israelites was a national
benefit. They believed that slavery was a national benefit, yet one of
them finally learned that it was a national disaster. The arrogance of
a sovereign central planning system was shattered in the year of the
Exodus. What several pharaohs had believed was rigorous service to
the Egyptian State turned out to be rigorous waste on a scale un-
dreamed of by the Pharaoh who first enslaved Israel. It was Egypt,
finally, which paid the price for this waste,

The modern version of the pharaohs’ economy, socialist eco-
nomic planning, also rests on an implicit assumption of near-
