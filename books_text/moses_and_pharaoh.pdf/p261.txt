The Rule of Law 243

also puts all men under the terms of biblical law. The God who made
Adam also made all men responsible to Him through Adam. Thus,
when the Hebrews placed foreign residents under both the restraints
of biblical law and the benefits of biblical law, they made a fun-
damental break with paganism, Natural law theory, which was later
paganism’s attempt to appropriate the universalism of biblical law,
was adopted by the West through the influence of church canon law
and other remnants of Hebrew law.
Some fundamental aspects of the rule of law are these:

1. A common set of moral requirements

2. Public proclamation of the law

3, Universal application of these standards

4. Equality before the law (no respecting of persons)

In short, the law is to be predictible and universal. Civil law as it
applies to citizens in their daily activities is to be sufficiently simple
so that the vast majority of men can understand its general principles
and its specific applications (case laws). This is not to deny that
specialized applications of biblical law principles are never going to
be complex. But those who work in specialized areas of society are to
be aware of these specialized applications. It may be that a “jury of
one’s peers,” or at least a portion of the jury’s membership, should be
composed of people selected randomly from a group of specialists in
the particular sphere or calling of the litigants. The crucial issue is
legal predictability, not the technicalities of the law. In short, men are
to have reasonable expectations concerning the decisions of judges
and juries in specific legal conflicts: Most important, men must have
confidence in the integrity of the law uself, and not just the institutional
agencies of law enforcement, for without confidence in the law itself,
men will not believe that their day-by-day adherence-to the law is
related directly to external benefits, both individual and social. They
will lose the major incentive of self-government under law.

Without the rule of law, capitalism could never have developed.
It developed in the West precisely because Christianity was the
religion of the West. As Christianity’s influence has waned, especially
in the last hundred years, capitalism has been challenged by increas-
ingly hostile socialist critics. Formal legal predictability, the bedrock of a
free market social order, has been abandoned by the intellectuals
and the voters. The predictable “rules of the game” that govern
capitalism are no longer respected by expansionist civil governments
