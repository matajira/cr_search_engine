364 MOSES AND PHARAOH

employment, not even at this moment enough bread to go around,
no one would be satisfied with his share, and instead of universal
contentment, universal dissatisfaction would result. What was to be
done? The population was too large for the scheme of the leaders to
be carried out successfully, therefore either the scheme must be
abandoned or the population must be diminished. "146

An odd aspect of the French Revolution was that it was officially
“pro-marriage” (and therefore presumably pro-natalist) at the same
time that the guillotine was in full force. This was also true for
Stalin’s Russia in the late 1930’s, when a pro-marriage reformation of
the Soviet law code was passed while he was executing millions,
either deliberately or indirectly by putting them into slave labor
camps. The French Revolutionary Assembly passed a head tax on
unmarried persons over thirty years old. There was even a national
celebration to honor husbands and wives.!*7 The Nazi policies of the
1930's imitated this same schizophrenia: pro-family tax policies and
subsidies for births, yct death-producing slave labor camps for
millions.

The Armenian Genocide

The next major example of a deliberate policy of population ex-
termination is the Turkish persecution of the Armenians. It came in
two waves, in 1895-96 and two decades later in 1915-16. Mass
murders were conducted in the first period, and again in 1909 under
the so-called Young Turks. In the final wave, the number of victims
was at least 800,000 and possibly as high as two million.'48 In this
case, the victims were long-term residents of the Ottoman Empire,
but were considered religious foreigners. Even the familiar “ian” and
“yan” endings of the surnames of Armenians are emblems of their
servitude; the Turks gave these name endings to Armenians in order
to identify this “foreign population.”}49

146. Ibid., pp. 423-24,

147, William Petersen, Population, p. 148.

148. Dickran H. Boyajian, Armenia: The Case for a Forgotten Genocide (Westwood,
New Jersey: Educational Books, 1972), p. 1.

149. ‘This was told to me by my father-in-law, R, J. Rushdoony, whose own fam-
ily had resided in the far north of Armenia, close to the Russian border, and had
escaped the required name change. His family escaped to Russia in the 1915
persecution, and since his father had British pounds, he was able to buy his way to
Archangel and from there to New York City. Rushdoony was born in New York
shortly after their arrival.

 
