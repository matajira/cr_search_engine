Population Growth: Tool of Dominion 15

only with the rise of the king “who knew not Joseph.” That the “sojourn”
also began back in the time of Abraham is clear from the statement in
Hebrews 11:9 which reads:
By faith he [Abraham] sojourned in the land of promise, as in a
strange country, dwelling in tabernacles with Isaac and Jacob, the
heirs with him of the same promise.
Others of the ancients than Paul thus understood the 430-year sojourn. The
translators of the Hebrew Scriptures into Greek have added a phrase to.
make clear the meaning of Exodus 12:40 as they understood it. The Sep-
tuagint reading of the verse is:

The sojourning of the children and of their fathers, which they so-
journed in the land of Canaan and in the Jand of Egypt. . . .

Josephus, as a Hebrew scholar of antiquity, thus understood the verse:

They left Egypt in the month Xanthicus, on the fifteenth day of the
lunar month; four hundred and thirty years after our forefather
Abraham came into Canaan, but two hundred and fifteen years only
after Jacob removed inte Egypt.*

This citation from Courville’s important study indicates that it
was long ago understood that the 430 years of Exodus 12:40 must be
interpreted in terms of the entire pilgrimage experience, Abraham to Moses.
The reference to “the children of Israel!” must be understood as
Hebrews in general, not simply to those born of Jacob. It includes
Abraham and Isaac, This means that Palestine was an Egyptian
vassal region throughout the Patriarchal cra of Exodus 12:40. It also
helps to explain why Abraham journeyed to Egypt during the
famine (Gen. 12:10). Egypt was the capital.

On the next page is Courville’s chart of his proposed recon-
structed chronology of Egypt and Israel? Understand that
Courville’s book is almost unknown in Christian circles, and even
less known in academic circles. His reconstructed chronology is not
taken seriously by archeologists and historians, any more than
Velikovsky’s chronology iri Ages in Chaos was (or is) taken seriously.

What Courville has accomplished is a brilliant reconstruction of
Egypt's chronology in terms of the 215-215 division. He has pin-
pointed the famine as having begun 217 years before the Exodus,

8. Ibid., I, p. 140. For Joscphus’ statement, sce Antiquities of the Jews, Book II, ch.
XV, sec. 2, in Josephus: Complete Works, William Whiston, translator (Grand Rapids,
Michigan: Kregel, 1960), p. 62.

9, Taken from The Journal of Christian Reconstruction, 11 (Surmmer 1975), p. 145.
