154 MOSES AND PHARAOB

that victory by imposing the terms of the treaty of the great King.1®
Armies stand, for they must be ready to march. Judges sit, ready to
dispense judgment. Christians are not warriors whose primary
assignment is to physically destroy nations that are not yet ethically
subdued; they are instead ambassadors —an army of ambassadors —
who come before a defeated population to announce the terms of
peace. But every peace treaty involves surrender; every peace treaty is
imposed by a victor The enemy's commander was defeated at
Calvary; it is our task to convince his subordinates to lay down their
weapons and sign the peace treaty.'% The decisive battle was won at
Calvary; the mopping-up operation is still going on (Rom. 16:2
1 John 3:8).

 

 

New Testament Revisions

There has been a shift in assignments since the days of the
Passover. The Hebrews were commanded to annihilate their Ca-
naanitic enemies. God planned to make a clean sweep of the larid of
Canaan. He intended the total devastation of those nations. The
Hebrews were to spare no one (Deut. 7:16-24). Dominion was to be
hy means of military might initially, and then by settling the land.
While there was eventually to be evangelism, as the Book of Jonah
indicates, the Hebrews were first to establish the kingdom in Israel
on a firm basis, and then God's promised blessings were to bring a
particular response on the part of Israel: expansion.

Since the resurrection, Christ has been planting His kingdom by
means of the sword of the gospel. The word of God, we are told, is
sharper than a two-edged sword (Heb. 4:12). We possess “the sword
of the Spirit, which is the word of God” (Eph. 6:17). The prophecy of
Isaiah is progressively coming true: “He shall smite the carth with
the rod of his mouth, and with the breath of his lips shalt he slay the
wicked” (Isa. 11:4). Those who bring the message of Christ are ethical
soldiers; their main task is to judge the world, subduing it by mcans
of the Holy Spirit, but in terms of His law. The kingdom has been
removed from genetic Israel and given to those who bring forth its
fruits (Mart. 21:43). The fruits produced by righteous living
manifest the kingdom. It is lawful living, in time and on earth,

 

18. The phrase is Meredith Kline's: Healy of the Great King: The Cournant Structure of
Deuteronomy (Grand Rapids, Michigan: Eerdmans, 1963).

19. Gary North, Unconditional Surrender: God's Program for Victory (2ud ed.; Tyler,
Texas: Geneva Divinity School Press, 1983), pp. 64-65, 69-70, 10-13.
