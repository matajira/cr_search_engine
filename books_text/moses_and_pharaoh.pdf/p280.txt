17
THE METAPHOR OF GROWTH: ETHICS

Thou shalt bring them in, and plant them in the mountain of thine in-
heritance, in the place, O Lorp, which thou hast made for thee to dwell
in, in the Sanctuary, O Loxp, which thy hands have established (Ex,
15:17).

The sociologist-historian Robert Nisbet has written: “There has
never been a time in Western thought when the image of social
change has not been predominantly biological in nature.”! Even his
use of the English-language phrase, “in nature,” is revealing of the
influence of the metaphor; he means, as we all do, “in form” or “in
essence,” or “fundamentally,” but we all frequently substitute the
phrase “in nature.” The power of the metaphor of nature is very
great, intellectually and linguistically.

There is no doubt that the Bible uses the language of biological
process in order to communicate certain truths concerning the affairs
of mankind. Such terms as “planting,” “grass,” and “seed” are fairly
common. The Bible unquestionably describes the processes of social
change in the language of biological growth and decay. Never-
theless, the Bible does not teach that mankind is a reflection of
nature, or that the affairs of man are reflections of nature’s processes.
The reverse is the case. Man is the archetype of nature, not the other
way around, If is nature’s processes which are analogous to man’s.

Man’s mortality does appear to be analogous to the processes of a
cursed earth, of growth and decay, and his certain aspects of man’s in-
stitutions also resemble living and dying biological organisms. But

1. Robert A. Nisbet, Prejudices: A Philosophical Dictionary (Cambridge, Massachus-
sets: Harvard University Press, 1982), p. 274.

262
