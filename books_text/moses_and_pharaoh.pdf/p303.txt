Imperfect Justice 285

criminal prosecutions we find as long as five months spent in the
selection of a jury; the same murder charge tried five different times;
the same issues of search and seizure reviewed over and over again,
conceivably as many as twenty-six. different times; prosecutions
pending a decade or more; accusations routinely sidestepped by an
accused who makes the legal machinery the target instead of his own
conduct.”!

Where have modern secular humanistic courts failed? Fleming
cites Lord Macaulay’s rule: the government that attempts more than
it ought ends up doing less that it should. Human law haa its limits.
Human courts have their limits. “The law cannot be both infinitely
just and infinitely merciful; nor can it achieve both perfect form and
perfect substance. These limitations were well understood in the
past. But today’s dominant legal theorists, impatient with selective
goals, with limited objectives, and with human fallibility, have em-
barked on a quest for perfection in all aspects of the social order,
and, in particular, perfection in legal procedure.”?

The requirements of legal perfection, Fleming says, involve the
following hypothetical conditions: totally impartial and competent
tribunals, unlimited time for the defense, total factuality, total
familiarity with the law, the abolition of procedural error, and the
denial of the use of disreputable informants, despite the fact, as he
notes, that “the strongest protection against organized thievery lies
in the fact that thieves sell each other out.”? Costless justice theory
has adopted the slogan, “Better to free a hundred guilty men than to
convict a single innocent man.” But what about the costs to future
victims of the hundred guilty men? The legal perfectionists refuse to
count the costs of their hypothetical universe.*

The goal of correct procedure as the only goal worth attaining is
steadily eroding both the concept of moral justice.and the crucially
important deterrent, a speedy punishment. Everything is to be
sacrificed on the altar of technical precision. “In this way the ideal of
justice is transformed into an ideal of correct procedure.”§ But the
ideal is impossible to achieve perfectly, and by sacrificing all other
goals, the cost has become astronomical. The incredible complexity

. Macklin Fleming, The Price of Perfect Justice (New York: Basic Books, 1974), p. 3.
. Dbid,, po 4.
. Wbid., p. 5.
. Ibid., p. 6.
. Bid, p. 9.

onoene
