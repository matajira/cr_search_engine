96 MOSES AND PHARAOH

cient. Omniscience is an incommunicable attribute of God. There is
a special knowledge which belongs only to God: “The sccret things
belong unto the Lorp our God: but those things which are revealed
belong unto us and to our children forever, that we may do all the
words of this law” (Deut. 29:29). Adam therefore faced limits on his
knowledge. He had to learn. He faced a world which required an
educational process based on trial and error. Eventually, there would have
been children and descendants. He could not perfectly have known
the mind of any other human being. Thus, he could not perfectly
have known other people’s wants, needs, and ability to pay in order
to attain their goals. He would have needed a source of publicly
available information (a price system) in order to learn about the
kinds of economic demand other people placed on the available
resources, including the demand for knowledge in its broades .
and the demand for time. He would have needed knowledge con-

 

cerning just exactly what resources were available. His incentive to
discover this would have been dependent on the economic demand
for resources in the markerplac

 

Thus, the very concept of the in-
communicable attribute of God’s omniscience points to the in-
escapable concept of scarcity. Man has to give up something in order
to gain certain kinds of knowledge.

The Fall of man came, in part, because man was unwilling to
pay for a specific resource: righteous judgment. He refused to render
righteous judgment on Satan when he heard the tempter’s offer.
Adam did not immediately judge Satan provisionally.and then wait
for God to return to render final judgment on the serpent. Adam did
not wait for God to render this final judgment and invest him and his
wife with the judge’s robes of authority before he ate from the tree of
the knowledge of good and evil. Adam therefore faced a scarce
universe, even in the garden. He faced scarcity of good judgment
and scarcity of time. He was unwilling to “pay the price,” meaning
the judgment price and the ime price. He wanted instant gratification.®
But he could not escape paying a price. ‘The price he actually paid
was a lot higher than he had estimated.!® He therefore faced the
restraint of scarcity.

9. Gary North, “Witnesses and Judges,” Biblical Economics Today, V1 (Aug./Sept.
1983), pp. 2-3.
10, North, Genesis, ch. 9: “Gosts, Choices, and Tests.”
