12 MOSES AND PHARAOH

Japheth, 30 from Ham, and 26 from Shem.? At the feast of taber-
nacles in the seventh month, beginning on the fifteenth day, the
priests were to begin a week of sacrifices. For seven days, a descend-
ing number of bullocks were to be sacrificed: 13, 12, 11, 10, 9, 8, and 7,
for a total of 70 bullocks. Then, on the eighth day (the beginning of
the next week), one final bullock was to be sacrificed (Num. 29:12-36).
Presumably, these were sacrifices for all the nations of the world, plus
Israel. There were 70 elders in Israel at the time of God’s confirmation
of the covenant at Sinai (Ex. 24:1). God at one point took His Spirit
from Moses and gave it to the 70 elders (Num, 11:16), Also, when the
Israelites defeated Adoni-Bezek after the death of Joshua, he confessed
that he had slain 70 kings (Judges 1:7), presumably a number refer-
ring symbolically to the whole world. Seventy men were sent out by
Jesus to evangelize southern Israel (Luke 10:1, 17). In Christ’s day,
there were 70 members of the Sanhedrin, plus the President.* So the
number “70” meant for the Hebrews something like “a whole popula-
tion,” although this does not deny the validity of 70 as the number of
lineal heirs who came down into Egypt.

The growth of the Hebrew population has te be considered a re-
markable expansion. How long did it take? This question has also
baffled Bible-believing commentators. When did the Exodus occur?
When did Jacob’s family enter Egypt? Were the Israelites in Egypt a
full 430 years? Donovan Courville, the Seventh Day Adventist
scholar, has called this chronology question “the Exodus problem.”5

 

2. Frederick Louis Godet, Commentary on the Gospel of Luke, 2 vols. (Grand Rapids,
Michigan: Zondervan, {1887]), II, p. 17. Godet discusses the problem of 70 vs. 72,
which occurs in this estimation, and also in the diffcring New Testament references
to the 70 or 72 sent out by Jesus (Luke 10:1).

3. Some manuscripts read 72. Godet argués that 70 is the correct reading: idem.

4. Alfred Edersheim, The Life and Times of jesus the Messiah, 2 vols. (Grand Rap-
ids, Michigan: Eerdmans, [1886]), II, p. 54. Cf. “Sanhedrim,” in McClintock and
Strong, Cyclopaedia of Biblical, Theological, and Ecclestastical Literature (New York: Har-
per & Bros., 1894), IX, p. 342.

5. Donovan A. Courville, The Exodus Problem and Its Ramifications (Loma Linda,
California: Challenge Books, 1971), 2 volumes. Courville’s original insight concern-
ing the need for a reconstruction of Egypt's chronology came from Immanuel
Velikovsky’s study, Ages in Chaos (Garden City, New York: Doubleday, 1952), which
presents the case against the traditional chronologies of the ancient world. Velikov-
sky identified the Hyksos rulers (“shepherd kings”) of Egypt as the invading Amelek-
ites, He argued that modern scholars have inserted a 500-700 year period into all the
histories of the ancient world (since all ure based on Egypt's supposed chronology), a
period which must be eliminated. Velikovsky wrote that “we still do not know which
of the two histories, Egyptian or Israelite, must be readjusted” (p. 338). Courville’s
book shows that it is modern scholarship’s version of Egypts chronology which-is
defective, not the chronology of the Old Testament. Sec Appendix A: “Fhe Recon-
struction of Egypt’s Chronology.”

 
