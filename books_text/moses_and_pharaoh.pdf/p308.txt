290 MOSES AND PHARAOH

office or the local department of the Interior Ministry. These offices can’t
keep up with the flood either and also break their deadlines, for which they
too are reprimanded and lose their bonuses. The bureaucratic machine is
thus obliged to work at full stretch, and you transfer the paper avalanche
from one office to another, sowing panic in the ranks of the enemy. Bureau-
crats are bureaucrats, always at loggerheads with one another, and often
enough your complaints become weapons in internecine wars between bu-
reaucrat and bureaucrat, department and department. This goes on for
months and months, until, at last, the most powerful factor of all in Soviet
life enters the fray — statistics. '*

As the 75,000 complaints became part of the statistical record,
the statistical record of the prison camp and the regional camps was
spoiled. All bureaucrats suffered. There went the prizes, pennants,
and other benefits. “The workers start seething with discontent,
there is panic in the regional Party headquarters, and a senior com-
mission of inquiry is dispatched to the prison.”!5 The commission
then discovered a mass of shortcomings with the work of the prison’s
administration, although the commission would seldom aid specific
prisoners. ‘The prisoners knew this in advance. But the flood of pro-
tests continued for two years. “The entire bureaucratic system of the
Soviet Union found itself drawn into this war. There was virtually
no government department or institution, no region or republic,
from which we weren't getting answers. Eventually we had even
drawn the criminal cons into our game, and the complaints disease
began to spread throughout the prison—in which there were twelve
hundred men altogether. | think that if the business had continued a
little longer and involved everyone in the prison, the Soviet bureau-
cratic machine would have simply ground to a halt: all Soviet in-
stitutions would have had to stop work and busy themselves with
writing replies to us.”16

Finally, in 1977, they capitulated to several specific demands of the
prisoners to improve the conditions of the camps. The governor of the
prison was removed and pensioned off.!7 Their ability to inflict death-
producing punishments did them little good, once the prisoners learned
of the Achilles’ heel of the bureaucracy: paperwork. The leaders of the
Soviet Union could bear it no longer: they deported Bukovsky.

14. Ibid, pp. 37-38.
15. Ibid, pp. 38-39.
16. Fid., p. 40.

17. Idem.
