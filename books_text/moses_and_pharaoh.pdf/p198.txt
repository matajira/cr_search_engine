180 MOSES AND PHARAOH

again, he was bargaining with God. This time, he used the law of
God to try to justify his actions. Instead of flatly denying the right of
Israel to sacrifice to God, or denying Israel. the right to take along
wives, or children, or cattle, he was now denying the legitimacy of
any judicial case that God might bring against him as a rebellious,
law-denying sovereign, He was asking God to sanction all of his past
transgressions, including his unwillingness to grant the Hebrews the
right to worship their God. But if these earlier transgressions were
not really iNegitimate, then God would have to sanction Pharaoh's
original argument, namely, that he had been a lawful sovereign dur-
ing the period of the subjugation. In fact, he was asking for God’s
sanction on the whole era of enslavement. He wanted his blessing;
he was paying for it “fair and square.” God owed him this blessing.

God did not grant him a blessing. What Pharaoh was paying was
restitution, and he was paying it under extreme duress. He would
gain no blessing from God; he would have no stamp of approval on
his actions as a self-proclaimed divine monarch, He was not going to
be able to buy his way out of judgment. He still had not recognized
the nature of the God he was dealing with.

God warned Moses as they were leaving Egypt: “For Pharaoh
will say of the children of Israel, They are entangled in the land, the
wilderness hath shut them in. And I will harden Pharaoh's heart,
that he shall follow after them; and I will be honoured upon
Pharaoh, and upon all his host; that the Egyptians may know that I
am the Lorn. And they did so. And it was told the king of Egypt that
the people fled: and the heart of Pharach and of his servants was
turned against the people, and they said, Why have we done this,
that we have let Israel go from serving us?” (Ex. 14:3-5).1 It is clear
that Pharaoh covenantally represented his subordinates well. The
Egyptians were not innocent victims of a misguided leader who did
not represent them ethically and spiritually. They advised him to
pursue the fleeing Hebrews. They had not learned, and they had not
humbled themselves before God for their generations of sinful deal-
ing with the Israelites. They had not yet received God's final verdict
on the assertion of Egypt’s continuity-of-being theology. They had

1. The phrase, “entangled in the land,” is expressive of the labyrinth concept
which dominated Egyptian and ancient pagan thought. The Hebrews’ wandering in
the wilderness did become an entanglement —an ethical entanglement, rather than
a physical entanglement. On the labyrinth, see Appendix C: “The Labyrinth and
the Garden.”
