8 MOSES AND PHARAOH

are tempted to ignore these difficulties, or even to ignore the clear
teaching of the Bible. Many of them date the Exodus much later.
They allow a hypothetical chronology of Egypt to dictate their inter-
pretation of Scripture. This is not the way that Christian scholarship
is supposed to be conducted.

In the early 1950’s, Immanuel Velikovsky, a genius (or fraud, his
critics say) began to publish a series of studies that reconstructed
{among other things) the chronologies of the ancient world. Velikoy-
sky began his reconstruction with a discussion of an ancient Egypt-
ian document, long overlooked by historians, which contains refer-
ences to a series of catastrophes that look remarkably similar to those
described in early chapters of the Book of Exodus.

Then, in 1971, an amateur historian named Donovan Courville
published a book which was based in part on Velikovsky’s work, but
which went far beyond it. Courville’s book has been systematically
ignored by Egyptologists and Christian scholars alike. 1 know of one
case where a seminary professor absolutely refused to discuss the
book with his students, cither publicly or privately, when asked
about it. Why the hostility? Because Courville’s book, like Velikov-
sky’s books, offers a frontal assault on the reigning presuppositions of
historians regarding the reliability of Egyptian records and the
reliability of the conclusions based on them, In Courville’s case, the
affront is worse: he is saying that Christian specialists in the field of
ancient history have accepted the testimony of humanist (Darwin-
ian) scholars and humanist (Egyptian) records in preference to the
clear testimony of the Bible. Conservative scholars resent the impli-
cation that they have compromised their scholarship in order to seek
recognition from (or avoid confrontation with) the conventional,
dominant humanist academic community. Thus, I have seen no
commentary on the Book of Exodus which refers to (let alone pro-
motes) either Velikovsky or Courville, nor do the standard Christian
encyclopedias.

This commentary is the exception. For this reason, it represents
a break with prevailing scholarship concerning the circumstances of
the Exodus. It may be incorrect, but it is incorrect in new
ways— ways that do not begin with the presupposition that conven-
tional humanist historical scholarship is binding, or the presupposi-
tion that the biblical account of history is inferior to the Egyptian
record, My position is clear: it is better to make mistakes within an
intellectual framework which is governed by the presupposition of
