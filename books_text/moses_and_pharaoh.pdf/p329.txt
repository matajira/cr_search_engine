The Reconstruction of Egypt's Chronology Sil

When was the Ipuwer document written? The Egyptologists dis-
agree. Some historians believe that it was written in the period be-
tween the Old and the Middle Kingdoms, while Gardiner believed, as
Velikovsky also believed, that it was a document from the Hyksos era,
at the end of the Middle Kingdom, meaning at the end of the Thir-
teenth Dynasty.*! Non-“Velikovskyite” John Van Secters agrees.*?
Both eras were transitional eras marked by great disruptions.

Is it proper here to use the word “both”? The conventional chron-
ologies of Egypt assume the existence of two great periods of political
and economic chaos in Egypt's early history, one immediately fol-
lowing the Sixth Dynasty, supposedly beginning about 2150 s.c.
(late Early Bronze Age) and lasting for perhaps a century,‘ and the
second period, called the Hyksos period, beginning at the end of the
Thirteenth Dynasty (or possibly the Fourteenth), also lasting for at
least a century, 1670-1570 3.c.* Courville believes that these two
chaotic periods were actually the same period, the era of Amalekite
domination which immediately followed the Exodus, i.e., after 1445
3.c, Problem: he estimates the Hyksos rule as lasting 430 years,
from the Exodus almost to the reign of Sclomon.* This is a very
long estimate.

Could there have been an earlier period of political catastrophe?
Could Courville’s telescoping of two sets of records into one era be
incorrect? We know that the pyramid-building age ended before
Moses’ day, and probably before Joseph’s day. There had been a per-
iod of feudalism prior to Sesostris III, who Courville believes was the
Pharaoh of the oppression. It is easy to imagine some sort of national
political disruption which had broken the power of the pyramid
pharaohs. Why not two catastrophic periods? The main reason why
not: the Early Bronze Age identification of the Ipuwer Papyrus. This
seems to be the period of the Exodus.

Another major problem for Courville’s thesis is that Ipuwer, who
lived in the Sixth Dynasty, addressed his lament to Pepi II. Courville

4i. Velikovsky, Ages in Chaos, pp. 49-50.

42, John Van Seeters, “A Date for the ‘Admonitions?” The Journal of Egyptian
Archaeology, L (1964), pp. 13-23. Predictably, he omits any reference to Velikovsky.
Cf, Pensée, LIL (Winter 1973), pp. 36-37.

43, Siegfried J. Schwantes, A Shart History of the Ancient Near East (Grand Rapids,
Michigan: Baker Book House, 1965), p. 67.

44, Ibid., p. 76. Some Egyptologists believe this era lasted two centuries or more.

45. Donovan Courville, The Exodus Problem and Its Ramifications, 2 vols, (Loma
Linda, California: Challenge Books, 1971), I, p. 124.
