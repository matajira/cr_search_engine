The Labyrinth and the Garden 371

attached to every great temple in Egypt, and there is good reason to
think that such temples of Osiris were better and miore regularly
served by the priests than the larger temples,’ ”!

Both Herodotus and Strabo described a huge Egyptian labyrinth
at Hawara, It was a two-storied structure, with 1,500 rooms above
and 1,500 below, It was excavated in 1888 by. Sir Flinders Petrie. !?
Wunderlich writes: “The purpose of the costly pyramids and mortu-
ary temples involved in the worship of the dead pharaoh was to pro-
pitiate Osiris, to win favor of the god of vegetation. The fifteen
hundred burial vaults and dwellings for the dead in the labyrinth [on
the bottom or underground floor—G.N..] were meant for provincial
sovereigns, princes of the blood and similar highly placed person-
ages of the Twelfth Dynasty of the Middle Kingdom. [This would
have been the era of the pharaohs from Joseph to Moses’ youth, ac-
cording to Courville.—G.N.] The labyrinth, therefore, was not a
mortuary femple in the strict scuse of the word. By that is meant, in
Egypt, a structure for the cult of the dead but not a burial place.”18

The “Palate” of Knossos

Wunderlich traces the interrelationships between Egypt and
Knossos, the so-called “palace” of the legendary King Minos, on the
island of Crete. This relationship was recognized by the Roman his-
toridn Pliny, whose Natural History (XXXVI, 13) says that Daedalus,
the designer of the labyrinth at Grete, took the design from Egypt.
The palace of Minos, which was so lavishly described—a better
word might be “invented”—and partially reconstructed by the Brit-
ish archaeologist Sir Arthur Evans during the first three decades of
the twentieth century, was in fact a giant tomb, not a palace. It was
never intended to be inhabited by the living. Its soft. gypsum floors
that can be scratched by a fingernail, its huge and unmovable pithot
(urns), its dark labyrinthine hallways, its lack of any protecting wall,
its distance from agriculturally productive land, its “bathtubs” with-
out drain pipes (sarcophagi), and “indoor plumbing” without drain
pipes (circulating vents for mummies) all point to a vast cult of the
dead, not the residence of the king. The supposedly happy and free-

Ii. Tbid., pp. 4-15.

12. Joseph Campbell, The Masks of God: Primitive Mythology (rev. ed.; New York:
Penguin, [1969] 1978),.p. 70.

13. Wunderlich, Secret of Crete, p. 248.

14, Hooke, p. 17.
