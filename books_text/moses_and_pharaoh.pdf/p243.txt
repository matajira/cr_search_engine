The Rule of Law 225

He might also have added that it was the American colonists’
hostility to the assertion of unlimited Parliamentary sovereignty in
making laws which was a major factor in the coming of the
American Revolution.® They believed, especially after 1770, that
the common law of England could make void certain acts of Parlia-
ment, a doctrine taken directly from the writings of the early
seventeenth-century English jurist Sir Edward Coke [GOOK], who
had greater influence on this point in the thinking of colonial lawyers
before the Revolution than the Commentaries of Blackstone, who was
a defender of Parliamentary sovereignty.”? The American ideal of
the doctrine of judicial supremacy and constitutionalism was not in-
vented by Chief Justice John Marshall.7!

Hayek traces the origin of Western legal liberty to the struggles
between Parliament and Crown in the Puritan revolution or British
Civil War, and subsequently in the Glorious Revolution of 1688.72
There was a concerted effort to secure the independence of judges.
The debates from 1641 to 1660 focused on the prevention of arbitrary
actions by the civil government. Hayek even notes the influence of
Puritan Samuel Rutherford’s defense of the rule of biblical law:
“Throughout, the governing idea was that the law should be king or,
as one of the polemical tracts of the period expressed it, Lex, Rex.”73

This faith in biblical law, and subsequently the faith in independ-
ent natural law and right reason, began to wane as a result of ra-
tionalism and secularism, especially after Darwin, for Darwin
destroyed men’s faith in nature, including morality “naturally” in
harmony with the forces of nature.’* Because there is no longer a
doctrine of fixed and infallible revealed law to govern the courts, and
no longer any faith in a universal “higher law,” the courts have

69. Bernard Bailyn, The Ideological Origins of the American Revolution (Cambridge,
Massachusetts: Belknap Press of Harvard University Press, 1967), ch. 5; Edmund
S. Morgan and Helen S. Morgan, The Stamp Act Crisis: Prologue to Revolution (rev.
ed,; New York: Collier, 1963); R. J. Rushdoony, This Independent Republic: Studies in
the Nature and Meaning of American History (Fairfax, Virginia: Thoburn Press, [1964]
1978), ch. 4: “Sovereignty.”

70. Randolph G. Adams, Political Ideas of the American Revolution: Britannic-
American Contributions to the Problem of Imperial Organization, 1765-1775 (2nd ed.; New
York: Barnes & Noble, [1939] 1958), p. 141.

71. Ibid, p. 142.

72. Constitution, p. 169.

73. Idem.

74. R. J. Rushdoony, The Biblical Philosophy of History (Nudey, New Jersey:
Presbyterian & Reformed, 1969), p. 7.
