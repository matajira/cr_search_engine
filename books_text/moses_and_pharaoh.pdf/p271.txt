‘The Firstborn Offering 253

of God’s adopted sons, allowing them to participate in redeemed
man’s week, which begins with man’s rest because Christ’s sacrifice is
now behind us chronologically. Thus the Lord’s day has been moved
to the eighth day, so that the first day of man’s week is, as it was in the
garden, a day of rest. Redeemed men announce their dependence on
God by beginning their work week with rest, a statement of their
complete dependence on God’s grace and blessings —a ritual decla-
ration of their having abandoned autonomous man’s week, which
allows no cost-frce rest for man. Christians announce their rehance
on the work of the second Adam, Jesus Christ, thereby renouncing
their reliance on their own efforts, as sons of the first Adam, God
restores their capital to them by reducing their former obligation to
pay in blood on the eighth day. We rest now, and we have increased
wealth to show for it. Let the fathers rejoice, and let all mothers re-
joice, too. The blood no longer flows in the new covenant Isracl,
