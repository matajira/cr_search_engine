Unconditional Surrender 187

it is; and I wil] give it to him” (Ezk. 21:27). Give what? The diadem
of power (Ezk. 21:26). God still overturns Satan’s kingdoms, but by
the steady expansion of the gospel age, the progressive dominion of
God’s law and God’s people, in time and on earth. It is our task to
lay the groundwork for a new kingdom, built on the foundation of
God’s law — which is to govern every human institution—by God’s
grace, which is applied to the heart of every Christian (Heb. 8:7-13).
Christianity does not preach a religion of social revolution, but a
religion of ethical regeneration, restitution, and repentance.

While Christianity preaches the tactics of social continuity dur-
ing its “minority religion” phase — go the extra mile with your enemy,
turn the other cheek — it nevertheless is a religion of social transfor-
mation. The Christian revolution takes place in the hearts and
minds of men—the place where all revolutions begin, The op-
ponents of Christianity recognize that Christianity is indeed a
religion of total transformation. To them, the ethical discontinuity
between the Old Adam and the New Adam represents a revolu-
tionary doctrine. It threatens them with the destruction of their anti-
Christian civilization.

The Roman emperors launched a series of bloody, though inter-
mittent, persecutions against the early church because they recog-
nized the all-or-nothing nature of the confrontation. It was either
Christ or Caesar. The Roman State was quite willing to tolerate any
religion which acknowledged the divinity (or genius) of the emperor.
Christians refused, They paid a heavy price. But Rome paid a
heavier price in the long run, So did Egypt.

 
    
