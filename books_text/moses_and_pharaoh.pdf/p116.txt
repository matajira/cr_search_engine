98 MOSES AND PHARAOH

punishment at all. The Hebrews would have been able to manufac-
ture just as many bricks as before with no additional inputs, in-
cluding labor. But the punishment was a punishment, as the
Hebrews complained to Moses. The Bible therefore draws our atten-
tion to economic theory at this point. There is an optimum produc-
tion mix. There is a law of diminishing returns. We achieve greater
output per unit of resource input by means of the division of labor
and the consequent specialization of production. For any given proc-
ess of production, at any point in time, the economist tells us that there
is some theoretically optimum mix of production factors.

There is a major epistemological problem here: a “point in time”
is a theoretically discrete unit of time which is in fact zmmeasurable
and is therefore outszde of time. A point in time is therefore timeless. It
is analogous to a theoretically discrete point in a line which is also
autonomous and immeasurable. A point in time is a theoretical (and
indescribable) period in which human action is not possible —“no
time for action”—and if human action is not possible, it becomes
problematical (self-contradictory) to speak of such phenomena as
market-clearing prices, knowledge, and responsibility. We are deal-
ing here with innate contradictions or antinomies in all human
thought, so at best we can only approach the idea of the optimum
production mix, just as we can only approach the idea of economic
equilibrium. If we lean too heavily on the weak reed of autonomous
human logic, the reed collapses.

While men can never have perfect knowledge of this economi-
cally optimum production mix, either in theory or in practice,
especially in a world of constant change (including technological
change), they still must try to approach this optimum mix if they wish
to minimize waste and maximize income. Biblical economics in-
forms us that there is an objective plan in the mind of God which serves as
a theoretical foundation for the assertion of the existence of this eco-
nomically optimum mix. Maximum economic output (and therefore
maximum income) requires specific co-operation in terms of a plan.

It must be understood, however, that approaching this optimum
is not easy, even as a theoretical matter. What are the success in-
dicators that enable planning agents to determine whether or not
they are approaching the optimum production mix? The existence of
profit and loss indicators in a competitive free market is the preliminary
answer. Nevertheless, these indicators have built-in limits, both in
practice and in theory. Modern economic theory is officially in-
