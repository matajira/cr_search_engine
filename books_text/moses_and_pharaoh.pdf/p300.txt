19
IMPERFECT JUSTICE

Moreover thou shalt provide wut of all the people able men, such as fear
God, men of truth, hating covetousness; and place such over them, to be
rulers of thousands, and rulers of hundreds, rulers of fifties, and rulers of
tens: And let them judge the people at all seasons: and it shall be, that
every great matter they shall bring unto thee, but every small matter they
shall judge: so shall it be easier for thyself, and they shall bear the burden
with thee (Ex, 18:21-22).

The Bible is not a perfectionist document. While it lays down a
standard of human perfection (Gen. 17:1; I Ki. 8:61; Matt. 5:48)—a
standard met only by Jesus Christ (Matt. 3:17; Rom 3:23; II Cor.
5:21)—it nonetheless acknowledges in its very law code that the ad-
ministration of even a perfect law system designed by God must be
understood as fallible, limited, and tainted with sin. As this passage
amply demonstrates, the Bible is hostile to the humanists’ quest for
perfect justice on earth.

Under Moses’ direct rule, God’s revelation was instantly
available in any given case. Yet there was insufficient time available
for Moses to hear every case of legal dispute in the land. Perfect
justice was limited by time and space. Men had to come to Moses’
tent and stand in (presumably) long lines (Ex. 18:14). The quest for
perfect earthly justice from God through His servant Moses was
eating up countless man-hours. Not only was Moses’ time limited,
but so was the time of those who stood in lines.

Jethro warned Moses that the people, as well as Moses himself,
were wearing away (v. 18). He recommended the creation of a
judicial hierarchy, thereby taking advantage of the principle of the
division of labor. Moses could reserve time to hear the cases that
were too difficult for his subordinates—“every great matter” (v.
22)—and in doing so, would redeem his allotted time more wisely by

282
