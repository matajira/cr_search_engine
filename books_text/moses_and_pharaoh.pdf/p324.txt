306 MOSES AND PHARAOH

attained number-one standing on the best-seller list nationally.

I should state at this point that I do not “believe in Velikovsky.” I
think we need to consider his chronological reconstruction, but I do
not take seriously his astronomical explanations — or Whiston’s, for
that matter—of such Bible events as the parting of the Red Sea
(near-collision with Venus), the manna (hydrocarbons floating down
from Venus), or the halting of both sun and moon in Joshua’s day
(another near-collision with Venus). While there may have been as-
tronomical events of unusual magnitude at the time —though the
Bible is silent concerning them —they do not explain the events. The
quest for naturalistic explanations here seems futile, although not
necessarily illegitimate. Velikovsky has not shown how Venus could
have raised the sea, kept the waters high and the walkway dry for
hours (Ex, 14:21), and then allowed the split “mountains” of water to
crash down just in time to drown the Egyptians. (The evidence from
mythology and literature that Velikovsky presents to buttress his
case that Venus is a recent addition to the solar system seems plausi-
ble to me, but my competence to judge the scientific astronomical
matters involved in such a hypothesis is non-existent.) Nevertheless,
I regard Velikovsky as one of the most powerful scholars of this or
any century, a man whose thorough command of diverse sources in
half a dozen arcane scientific and linguistic disciplines bordered on
genius or the occult, He is not to be ignored or dismissed lightly.

The Need for Reconstruction

Ages in Chaos never received the attention that Worlds in Collision
did. It is a less comprehensive theory, limited primarily to
chronology and the documentary records relating to chronology. He
begins with a summary of the obvious: the Exodus cannot easily be
placed in the dynasty of any Pharach whose accepted chronology
matches the chronology of I Kings 6:1. The Eighteenth Dynasty,
which by conventional dating occurred in the fifteenth century a.c.,
included pharaohs who were very powerful. The documentary
record of their reigns provides no evidence of any successful rebel-
lion of slaves. Scholars have long recognized this problem.

If this date cannot be accepted, then what about the period in be-
tween the Eighteenth and Nineteenth Dynasties? No good, says
Velikovsky. “Stress has also been laid on the fact that Palestine was
under Egyptian rule as late as the disturbances of -1358 [1358
B.c.—G.N.]}, which put an end to the reign of Akhnaton. [Quoting
