16
THE PSYCHOLOGY OF SLAVERY

Is not this the word that we did tell thee in Egypt, saying, Let us alone,
that we may serve the Egyptians? For it had been better for us to serve the
Egyptians, than that we should die in the wilderness (Ex, 14:12).

The day of the Lord had come to Israel, the long-awaited day of
release from bondage. God had manifested His power in a series of
mighty acts, culminating in the death of the firstborn of Egypt. If
ever there was going to be a day of release, this was it. Remaining in
bondage for “a better day to flee, a more opportune moment to
escape” would have been preposterous. Pharaoh’s will to resist their
departure had been temporarily overcome. The Egyptians had en-
couraged their rapid departure. God had opened the way of escape
to them, and the Egyptians had literally pushed them out of the
land, paying them to depart.

All the same, neither Pharaoh nor the Israelites had escaped
from their basic outlooks. Pharach had temporarily capitulated to
the demands of Moses. The Israelites had temporarily taken heart at
God’s mighty acts, and they had even dared to demand restitution
(spoils) from their Egyptian captors. But Pharaoh was a self-
proclaimed divine monarch, theoretically sovereign over all Egypt,
and the Hebrews had been his slaves. The Hebrews were convinced
that their position as servants in the land was permanent. They had
been slaves in body, and their responses to Moses, time after time,
were the responses of people who were also slaves in their souls.
Pharaoh soon became convinced that he had made a mistake in
allowing them to leave (Ex. 14:5), and the Hebrews soon became
convinced that they had made a mistake in listening to Moses and
leaving. The Hebrews could not forget the earthly power of their
former master; they seemed unable to acknowledge the awesome
power of their new master, Jehovah. Their cry went up to Moses:

254
