270 MOSES AND PHARAOH

The Concept of Economic Growth

Redeemed man’s hope is in continual internal and external prog-
ress, including economic growth (Deut. 8:6-9). The promise is ulti-
mate external victory (Isa. 66:22), Even the normal processes of
nature will be overcome by the healing power of God’s grace, as
hostile animals will find peace with each other (Isa. 65:25). The kind
of progress which the Bible denies is the progress hoped for by the
drunkard, a concept of progress which is autonomous: “Come ye, say
they, I will fetch wine, and we will fill ourselves with strong drink; and
tomorrow shall be as this day, and much more abundant” (Isa. 56:12).
This kind of progress cannot be sustained. God judges it.

Economic growth is always under severe restraints. Compound
growth rates of 5% per annum cannot continue for centuries.'§ But
it is valid to expect continual blessings in response to continued
faithfulness to God’s laws. Economic growth is not an autonomous
process that can be engineered into a socicty. It is the product of a
world-and-life view that can be adopted by a society,

But there is an additional problem for humanist economics. The
Christian economist believes that the mind of God is comprehensive
and normative. God can make plans and evaluate economic growth,
and man, who is made in God’s image, can also make plans and
evaluate aggregate growth, though not perfectly.!7 The blessings of
God that appear in the form of increased numbers of cattle or other
assets are meaningful because of the plan of God. He imputes value
to certain material objects, and therefore when individuals increase
their holdings of them, they are indeed better off than before.

The subjectivist economist has no consistent way to be sure that
economic growth is a blessing. Some economists, such as E. J.
Mishan, positively reject the idea that economic growth is beneficial.
But the problem is deeper than simply evaluating the costs and
benefits of growth. Is the metaphor of growth even applicable to the
economy, given the presuppositions of subjectivism?

Ludwig Lachmann recommends that economists abandon the
whole concept. He does speak of “economic progress,” which he

16. Gary North, “The ‘Theology of the Exponential Curve,” The Freeman (May
1970); reprinted in An Introduction to Christian Economics (Nutley, New Jersey: Craig
Press, 1973), ch, 8.

17. Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1982), pp. 59-65.
