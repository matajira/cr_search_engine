Imperfect Justice 291

Subsequently, according to one Soviet defector, the Soviets have
had to scrap the Constitution once again (unofficially, of course) in
the area of protection of free speech.'® Dissidents are again subjected
to long-term incarceration in psychiatric hospitals, as they were be-
fore Bukovsky and other dissidents organized their protesting techni-
ques, both insisde and outside the Gulag.1® What this probably
means is that international public opinion no longer is focused on
the dissidents. As Bukovsky notes, “they wrote a Constitution with a
plethora of rights and freedoms that they simply couldn’t afford to
grant, rightly supposing that nobody would be reckless enough to in-
sist on them being observed.” Nevertheless, when internal
dissidents and external observers have put the pressure of formal
paperwork on the ruling elite, they have sometimes capitulated.

Centralization and the Declining Respect for Law

Economist Milton Friedman has suggested that the respect for
law that prevails in capitalist societies is closely related to the
absence of comprehensive, detailed administrative regulations. The
British experience of the nineteenth century provides Friedman with
a case study of the public’s response to civil law.

In the early nineteenth century, British citizens were notorious
law-breakers. They were a nation of smugglers, ready to offer
bribes, ready to take advantage of every inefficiency in the law-
enforcement system. Very little could be accomplished through legal
channels, Friedman says that one of the reasons why Jeremy Ben-
tham and the Utilitarians were hostile to civil government and
adopted laissez-faire doctrines in the early nineteenth century was
because they recognized that civil government was completely
inefficient. “The government was corrupt and inefficient. It was
clearly oppressive. It was something that had to be gotten out of the
way as a first step to reform. The fundamental philosophy of the
Utilitarians, or any philosophy that puts its emphasis on some kind
of a sum of utilities, however loose may be the expression, does not
lead to laisser-faire in principle. It leads to whatever kind of organiza-

18. He appeared on the Cable News Network in February, 1984.

19. On the use of mental hospitals as substitute prisons, see Zhores Medvedev
and Roy Medvedev, A Question of Madness: Repression by Psychiatry in the Soviet Union
(New York: Random House, 1971); Sidney Bloch and Peter Reddaway, Psychiatric
Terror: How Soviet Psychiatry Is Used to Suppress Dissent (New York: Basic Books, 1977).

20. Bukovsky, p. 239.
