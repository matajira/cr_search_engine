The Reconstruction of Egypt's Chronology 301

the most reasonable approach to them is to examine in more detail
the historical context in which they [the data] date the Exodus. This
biblical date for the Exodus has a reciprocal relationship with the
events described in Exodus as related to Egyptian history. A
pragmatic approach to this date suggests a period of Egyptian
history that should be examined for a possible relationship to the
biblical Exodus, and considerable agreement of the evidence from
Egyptian and biblical sources pointing to that period supports the
accuracy of the chronological datum (480 years) from which that
search started.”!° He does his best to show why a mid-fifteenth cen-
tury date is viable, but he does not begin with the premise that this
must be the case, irrespective of modern interpretations of the Egypt-
ian evidence. He appeals to pragmatism instead.

Roland Harrison, one of the ISBE’s associate editors, elsewhere
argues for a thirteenth-century dating. Harrison’s study is based on
a survey of the conclusions of the secular archaeologists, who debate
endlessly about the proper dating of the various Bronze Ages (Early,
Middle, and Late), a humanistic classification system which is based
entirely on nineteenth-century evolutionary social theory.!! He men-
tions the fact that early in the twentieth century, Bible scholars ac-
cepted a late-thirteenth-century date for the Exodus. In the 1920's
and 1930's, excavations in Palestine, especially Jericho, convinced
several archaeologists that the traditional mid-fifteenth century
dating is correct. But he is not convinced: “The question cannot be
settled simply by an appeal to the book of Kings in the light of an ar-
bitrary dating for the fall of Jericho.”!? Notice his subtle shift in
argumentation: he tries to overcome the explicit teaching of I Kings
6:1 by means of a brief reference to doubts concerning the reliability
of certain archeological excavations conducted early in the twentieth
century. But I Kings 6:1 does not mention the fall of Jericho; it does
specifically mention the Exodus, Harrison’s argument is muddled.
His recommended chronology specifically rejects the téstimony of
I Kings 6:1. Yet this is all done in the name of Jesus. Such is the fate
of ostensibly Christian scholarship which arbitrarily abandons a so-
called “simple appeal” to the explicit testimony of the Bible. It is

 

10. Ibid., p. 237.

11. On this point, see R. A. McNeal, “The Legacy of Arthur Evans,” Cakiforma
Studies in Classical Antiquity, VI (1973), pp. 206-20.

42. R. K. Harrison, introduction to the Old Testament (Grand Rapids, Michigan:
Eerdmans, 1969), p. 175.
