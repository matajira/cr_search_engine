278 MOSES AND PHARAOH

predictability of His providence. Manna provided a daily and
weekly testimony to God’s sovereign power: providential regularity
within a framework of ecological abnormality. The curse of the ground
was overcome daily, which pointed to the Author of that curse, and
the power He possesses to deliver His people ethically and economi-
cally from the effects of that curse. A God who can be relied upon to
overcome the laws of biology and ecology on a daily basis can also be
relied upon to honor His covenantal promises to provide external
blessings for those who honor His law-order.

A remnant of the wilderness experience of manna was provided
by God in the land of Israe] by guaranteeing a triple harvest in the
year preceding that sabbatical year which preceded the jubilee year
(Lev. 25:21), There is no subsequent reference to the occurrance of
this miraculous triple crop, probably because the Hebrews in the
pre-exilic period did not honor the sabbath year principle, which is
why God sent them into exile (II Chron. 36:20-21). After their
return to Israel they did honor the sabbatical year, according to a
non-canonical source (I Maccabees 6:49), but there is no mention of
the jubilee year or to its triple harvest. It seems likely that the
manna experience was the background for Christ’s multiplying of
the loaves in “a desert place” (Matt. 14:15-20), which was part of a
ministry which He announced in terms of the jubilee year (Luke
4:18-19).

Biblical Law and Conquest

This younger generation was being prepared to conquer Ca-
naan. They had to learn two things about biblical law. First, biblical
law is personal. Second, it is regular enough to be predictable. They
also learned that self-discipline and faithfulness are necessary for
survival, since violations of the sabbath requirements had brought
criticism from God (Ex. 16:28). They learned of the continuity of
biblical law over time (four decades) and over geography (wandering).
God’s provision of the manna created a psychology of godly dominion.
Humble before God, and totally dependent upon His care, they
could become totally confident in the face of human enemies. They
expected to find a tight relationship between God’s commands and
God’s blessings. They learned humility before God and confidence
before the creation. “And he humbled thee, and suffered thee to
hunger, and fed thee with manna, which thou knewest not, neither
did thy fathers know; that he might make thee know that man doth
