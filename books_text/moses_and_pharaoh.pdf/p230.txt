212 MOSES AND PHARAOH

ble action placed on people occupying the lower rungs of authority.
Third, no single institution has absolute and final authority in any in-
stance; appeal can be made to other sovereign agents of godly judg-
ment. Since no society can attain perfection, there will be instances
of injustice, but the social goal is harmony under biblical law, in
terms of an orthodox creed. God will judge all men perfectly. The
State need not seek perfect justice, nor should citizens be taxed at the
astronomical rates necessary to sustain the quest for perfect justice.3!

Hayek has made a point which must be taken seriously by those
who seek to explain the relationship between Christianity and the
advent of free enterprise capitalism in the West. “There is probably
no single factor which has contributed more to the prosperity of the
West than the relative certainty of the law which has prevailed
here.”32 Soweil’s comments are especially graphic: “Someone who is
going to work for many years to have his own home wants some fairly
rigid assurance that the house will in fact belong to him— that he
cannot be dispossessed by someone who is physically stronger, better
armed, or more ruthless, or who is deemed more ‘worthy’ by political
authorities. Rigid assurances are needed that changing fashions,
mores, and power relationships will not suddenly deprive him of his
property, his children, or his life.”** Hayek quite properly denies the
validity of the quest for perfect certainty, since “complete certainty of
the law is an ideal which we must try to approach but which we can
never perfectly attain.”*+ His anti-perfectionism regarding the rule of
law is also in accord with the anti-perfectionism of Christian social
thought in the West.35 Christianity brought with it a conception of
social order which made possible the economic development of the
West.

Biblical Law and Capitalism

There is no doubt that formal legal predictability was a major factor
in the rise of capitalism, By “capitalism,” I mean a system of private

31. Macklin Fleming, The Price of Perfect Justice (New York: Basic Books, 1974).
For an analysis of Fleming’s critique of the modern criminal justice system, see my
review of the book in The Joumat of Christian Reconstruction, U1 (Winter 1975-76).

32. Hayek, The Constitution of Liberty (University of Chicago Press, 1960), p. 208.

33. Thomas Sowell, Knowledge and Decisions (New York: Basic Books, 1980), p. 32.

34, Hayek, Constitution of Liberty, p. 208.

35. Benjamin B. Warfield, Perfectioniom (Philadelphia: Presbyterian & Reformed,
1958). This is an abridged version of Warfield’s two-volume study, published by Ox-
ford University Press in 1931, and reprinted by Baker Book House in 1981,
