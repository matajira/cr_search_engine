The Reconstruction of Egypt's Chronology 323

By now Evans had made two very important assumptions, first that the
civilization of the Cretan Bronze Age was a discreet [typo: he means
discrete—G.N.] entity, and second that it could be considered in terms of
youth, maturity, and old-age. Thus far'litéle has been said about the ar-
tifacts, and one may wonder whether they have not become lost in the
metaphorical shuffle. The point is, that Evans fitted the artifacts to his partic-
ular organic model of reality, and the way in which he did so was perfectly in-
genious. Faced with the necessity of forging a link between the guiding
abstraction and the artifacts which’could be apprehended empirically, he
wove into his synthesis another set of ideas, this time concerned with the
nature of Minoan art. Evans looked at the artifacts and divided them into
three classes corresponding to the tripartite scheme which he already had in
mind. . . ..Tn this way Evans connected the biological metaphor with the
archaeological artifacts which he pulled from the ground, The result. was a
sequence, a relative chronology,7°

He then divided the types of pottery into a scheme: Stone Age;
Minoan: Early Minoan, Middle.Minoan, Late Minoan; and Iron
Age. He did the same with art. As he excavated, the stratigraphic
evidence was lost.7! McNeal refuses to say that this was deliberate, or
that Evans falsified the record. Others have cast doubt on his handling
of the evidence and his creativity in reconstructing the, “palace,”
especially the paintings.?? As McNeal says of the early archaeologists,
“In their rush to construct elaborate evolutionary sequences, they
tended to forget the strata. Or, to put the matter another way, there
was a regrettable habit of interpreting the strata in terms of sequences
previously constructed on solely evolutionary criteria.”

This practice probably arose from the mistaken idea, already noted,
that pottery types could be stacked end-to-end like railroad cars. We know
now that pottery does not go in and out of existence in just this way. A new
style does not necessarily begin where another leaves off. Evans thought
that only one style marked a given period, But quite apart from the exist-
ence of gradual transitions between different styles, we find totally different
types in simultaneous use. Since potters are both conservative and pro-
gressive, old styles can be retained long after new ones are in vogue. There
is thus a definite danger of refining the relative sequence too much and of
marking off stages where no stages ever existed.”*

70. Itid., pp. 216-17.

71, Ibid, p. 218,

72. Hans Georg Wunderlich, The Secret of Crete (New York: Macmillan, 1974),
pp. 79-82.

73, McNeal, p. 219.

74. dem,
