Introduction 7

readers are not told is that Egyptians did not believe in chronology. The
historical records which modern (and even classical Greek) histor-
jans have used to reconstruct Egypt's chronology are woefully defi-
cient, The Egyptians simply did not take seriously their own history.
They did not believe in the importance of linear time. The records
they left reflect their lack of concern. A century ago, historian
George Rawlinson began his chapter on Egyptian chronology with
this statement: “It is a patent fact, and one that is beginning to ob-
tain general recognition, that the chronological element in early
Egyptian history is in a state of almost hopeless obscurity.”? He was
incorrect, however, concerning the coming “gencral recognition” of
the problem. Only the most scholarly and detailed monographs on
Egypt bother to warn readers about the problem.

There are several kinds of chronological documents, including
the actual monuments. “The chronological value of these various
sources of information is, however, in every case slight. The great
defect of these monuments is their incompleteness. The Egyptians
had no era, They drew out no chronological schemes. They cared for
nothing but to know how long each incarnate god, human or bovine,
had condescended to tarry on the earth. They recorded carefully the
length of the life of each Apis bull, and the length of the reign of each
king; but they neglected to take note of the intervals between one
Apis bull and another, and omitted to distinguish the sole reign of a
monarch from his joint reign with others.®

Readers are also not informed of the fact that virtually all chron-
ologies of the ancient Near East and pre-classical Greece are con-
structed on the assumption that the conventional chronology of
Egypt is the legitimate standard. What modern scholars believe is
the proper chronology of Egypt is then «posed on, the chanologtes of all
other civilizations of the ancient Near Kast, including the biblical chronol-
ogy of the Hebrews. Thus, when the Bible says explicitly that the
Exodus took place 480 years before Solomon began to construct the
temple (I Kings 6:1), historians interpret this information within the
framework of the hypothetical Egyptian chronological scheme.
When they even admit that the pharaohs of the supposed dynastic
era of the fifteenth century before Christ were extremely powerful
kings—men like Thutmose [I[—whose mummies still exist,? they

7. George Rawlinson, A History of Egypt, 2 vols. (New York: Alden, 1886), II, p. 1.

8. [bid., T, p. 2.

9, Photographs of the mummies of Thutmose IIT and Amenhotep IJ appear in
Donovan Courville, The Exodus Problem and Iis Ramifications, 2 vols. (Loma Linda,
California: Challenge Books, 1971), I, p. 37.
