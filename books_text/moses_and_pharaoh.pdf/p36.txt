18 MOSES AND PHARAOH

Jacob’s Heirs

Unquestionably, the growth of the Hebrew population was
rapid. If the sons of Jacob, which included each family’s circumcised
bondservants, came down to Egypt 215 years before Moses led their
heirs out of Egypt, then the Hebrews experienced long-term popula-
tion growth unequaled in the records of man. Remember, however,
that people lived longer in Joseph’s era. Kohath, Moses’ grandfather,
lived for 133 years (Ex. 6:18). Levi, Kohath’s father, died at age 137
(Ex. 6:16). Moses’ brother Aaron died at age 123 (Num. 33:39).
Moses died at age 120 (Deut. 34:7), Nevertheless, Moses acknowl-
edged that in his day, normal Jife spans were down to about 70 years:
“The days of our years are threescore and ten; and if by reason of
strength they be fourscore years, yet is their strength labour and sor-
row; for it is soon cut off, and we fly away” (Ps. 90:10). (Again, the
number “70” appears, in this case to describe a whole lifetime, rather
than a whole population.) Caleb boasted about his strength for a
man of 85 (Josh. 14:10-11), indicating that in his generation life spans
had shrunk.

These years of long life were reduced after the Exodus. Men
seldom survived to age 130. (One exception: Jehoiada, the high
priest, lived to 130: [1 Chron. 24:15.)!2 But if, during the years in
Egypt, they begat children from an early age and continued to bear
them until well into their eighties and nineties, as Jacob had done
before them, then we can understand how such a tremendous expan-
sion of numbers was possible. As I explain below, foreigners in large
numbers covenanted themselves to Hebrew families. It is also possi-
ble that Hebrew men married Egyptian wives in the first century of
prosperity, as Joseph had done (Gen. 41:45), This would have greatly
expanded the number of children born into Hebrew families, since
the Hebrew husbands would not have been limited exclusively to
Hebrew women. A family of five boys and five girls could have
become a family of 100 Hebrew grandchildren within a generation.
Of course, not every family could have seen this happen, since some

12. Dr. Arthur CG. Custance, a creationist scholar and medical physiologist,
argues in The Seed of the Woman (Brockville, Ontario, Canada: Doorway, 1980) that
there are fairly reliable records concerning several dozen long-lived individuals
(over 110 years of age), including 32 age 150 or more, and one, Li Chang Yun, who
died in China in 1933 at the startling age of 256. He had survived 23 wives (p. 481).
“Those who saw him at age 200 testified that he did not appear much older than a
man in his fifties.”
