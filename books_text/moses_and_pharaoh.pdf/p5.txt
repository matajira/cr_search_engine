This book is dedicated to
Robert A. Nisbet

who taught me to ask two crucial
questions: “What is the nature
of social change?” and “What
is so natural about it?”
