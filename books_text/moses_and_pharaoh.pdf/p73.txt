Rigorous Waste 55

tion in a socialist commonwealth.? An economy without competitive
markets is an economy without rational economic guidelines. It is an econ-
omy which is “flying blind.”

The central planner does not know what slave labor is really
worth, for such labor commands no free market price. He does not
know what the true cost of his capital equipment is, since there is no
competitive market for capital goods. The Pharaoh, like any other
socialist planner, could only guess, All he could do was to make in-
tuitive judgments about what his cities were worth to him and what the
actual costs of production really were. The larger the scope of the
projects, and the larger the slave labor supply, the more difficult it
was to make such intuitive estimates apart from fully competitive
prices. But competitive pricing is precisely what socialist economic
planning denies.

Socialism’s Economic Miscalculation

Mises commented at some length on this problem of economic
calculation in his book, Soczalism (1922). “Let us try to imagine the
position of a socialist community. There will be hundreds and
thousands of establishments in which work is going on. A minority
of these will produce goods ready for use, The majority will produce
capital goods and semi-manufactures. All these establishments will
be closely connected. Each commodity produced will pass through a
whole series of such establishments before it is ready for consump-
tion. Yet in the incessant press of all these processes the economic
administration will have no real sense of direction. It will have no
means of ascertaining whether a given piece of work is really neces-
sary, whether labour and material are not being wasted in com-
pleting it, How would it discover which of two processes was the
most satisfactory? At best, it could compare the quantity of ultimate
products. But only rarely could it compare the expenditure incurred
in.their production. It would know exactly —or it would imagine it
knew — what it wanted to produce. It ought therefore to set about ob-

7. Ludwig von Mises, “Economic Calculation in the Socialist Commonwealth,”
(1920), in F. A. Hayek (ed.), Collectivist Econemic Planning (London: Routledge &
Kegan Paul, [1935] 1963), ch. 3. Cf. Mises, Socialism: An Economic and Socialogical
Analysis (2nd ed.; New Haven, Connecticut: Yale University Press, [1922] 1951), Pt.
II. This has been reprinted in bath hardback and paperback editions in 1981 by Lib-
erty Classics, Indianapolis, Indiana, Sec also Hayek, Individualism and Economic Order
(University of Chicago Press, 1948), chaps. 7-9; T. J. B. Hoff, Economic Calculation in
the Socialist Society (London: Hodge & Co. 1948); reprinted by Liberty Classics, 1981.
