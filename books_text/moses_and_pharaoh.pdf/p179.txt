Continuity and Revolution 161

God, priest, and offerer.2? Christ was our offering, yet He was the
offerer; He was also the High Priest and God.28 He was the firstfruits
(I Cor, 15:23), from which the peace-offering had to be made.
Leavened dough could not be burned on the altar (Lev, 2:11). It
had to be brought already baked, ready for eating. Leavened bread
was Offered as a finished work, the fully-risen product of the “starter.”
It was not to be burned on God’s altar, not because it was “corrupted”
or “sin-laden,” but because it was a finished loaf.?° Burning it would
have ruined it. It was not the “corrupted” nature of leaven that kept
it off God’s fiery altar, for honey was also prohibited (Lev. 2:11).
There was nothing corrupt about honey. Honey, like leavened
bread, is a finished product, the product of labor, capital, and time.

Pentecost

It is extremely important to note that this compulsory offering of
the firstfruits, which included the leavened bread offering, came on
the day of Pentecost. The Greek word, “pentekostos,” means fifty.
The firstfruits offering was made on the fiftieth day after the sabbath
day of the Passover week, the feast of unleavened bread (Lev.
23:15-16), or forty-nine days after the wave offering. On the day after
the sabbath which fell during the Passover week, the priests brought
a sheaf of grain offering and waved it before God, Then, forty-nine
days of maturation later, came the baked bread of the day of
Pentecost. At Passover, the people were required to use unleavened
bread, the symbol of religious, cultural, and historical discontinuity.
At Pentecost, they were required to offer leavened bread, the symbol
of continuity and completion, At the Passover, Israel found its
release from bondage. At Pentecost, they experienced full blessings.

27. Andrew Jukes, The Law of the Offerings (Grand Rapids, Michigan: Kregel,
1968), pp. 115-21. The book was first published in the late nineteenth century.

28. Ibid, pp. 118-19.

29. I rarely disagree with the published conclusions of R. J. Rushdoony, but I
think his assessment of the meaning of leaven is incorrect. He writes: “Leaven is
taken by some as a symbol or type of sin; it is rather a symbol of corruptibility. . . .
Man’s obedience to the law is a leavened offering, clearly corruptible, yet when
faithful and obedient to God’s authority and order, a ‘sacrifice’ well-pleasing in His
sight and assured of His reward,” Institutes of Biblical Law, p. 83, I am arguing that
Jeaven symbolizes neither sin nor corruptibility; leaven is a symbol of the continuity of
development, meaning maturation over time, All of men’s offerings are corruptible; focus-
ing on leaven as a uniquely corruptible offering misses the point. Leaven as a sym-
bol of continuity fits Rushdoony’s postmiliennial eschatology far better than leaven
as a symbol of corruptibility.
