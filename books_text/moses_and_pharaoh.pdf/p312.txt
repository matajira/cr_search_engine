294 MOSES AND PHARAOH

the market tends to be a more efficient organizing principle than centralized
direction. Centralized direction in this way is always having to fight some-
thing of a losing baitle.2

Conclusion

What we can and should strive for is to conform our human law
codes to the explicit requirements of the Ten Commandments and
the case-law applications of biblical law. The answer to our legal
crisis is not to be found in the hypothetical perfection of formal law,
ner can it be found in the hypothetical perfection of substantive
(ethical) justice. Judges will make errors, but these errors can be
minimized by placing them within the framework of biblical law. Be-
fore God gave the nation of Israel a comprehensive system of law,
Jethro gave Israel a comprehensive system of decentralized courts.
By admitting the impossibility of the goal for earthly perfect justice,
Moses made possible the reign of imperfectly applied revealed
law —perfect in principle, but inevitably flawed in application. The
messianic goal of a perfect law-order, in time and on earth, was
denied to Moses and his successors.

One of the most obvious failures of the modern administrative
civil government system is its quest for perfect justice and perfect
control over the details of economic life. The implicit assertion of
omniscience on the part of the central planners is economically fatal.
The result of such an assertion is an increase of regulations, in-
creased confusion among both rulers and ruled, and a growing
disrespect for civil law. The productivity of the West cannot be main-
tained in the face of such an exponential build-up of central power. It
is only because the laws are not consistent, nor universally enforced
or obeyed, that the modern messianic State has survived. The price
of perfect human justice is too high to be achieved by the efforts of
men.

23. Friedman, pp. 245-47.
