70 MOSES AND PHARAOH

placed there by God or society.. Murder is an act of self-proclaimed
autonomous man against another man, created in God’s image, who
is entitled to protection by the law of God.

Moses was not sanctioned by Egyptian law to execute the Egypt-
jan taskmaster. But this man had no biblically legitimate authority
over the defenseless Hebrews. Their land had been stolen, and they
had all been kidnapped — a capital offense (Ex. 21:16), He deserved
death, as did all the taskmasters in Egypt. The New Testament
affirms that Moses was a faithful man in the decision to stand with
his fellow Hebrews and then in his flight from Egypt (Heb.
11:24-27).

Why did he do it? In part, because he made a miscalculation
concerning the hearts of his brethren. “And seeing one of them suffer
wrong, he defended him, and avenged him that was oppressed, and
smote the Egyptian. For he supposed his brethren would have un-
derstood now that God by his hand would deliver them: but they un-
derstood not” (Acts 7:24-25). The Pharaoh sought to kill Moses,
despite Moses’ position in the Pharaoh’s family, when he learned of
Moses’ act (Ex. 2:15).!

Moses was clearly the most highly placed Hebrew of his day. No
other Hebrew resided in the king’s household. No other Hebrew had
access to the highest authorities in the land. No other Hebrew had
grown up under the instruction of Egyptian tutors, possibly even to
serve as a ruler in the State. If Moses’ act was not murder, then we
have to view him as a judge of Israel comparable to Samson,
Deborah, Ehud, and other judges who defended Israel from con-
quering enemies.

Israel was a captive people. The Hebrews had been unlawfully
thrown into slavery. The covenant between the Egyptian State of
Joseph’s day and the Hebrews had been broken by Egypt. They had
become captives in a forcign land. Moses, who was used to the trap-
pings of authority, witnessed a criminal act by an Egyptian against a
Hebrew brother. Moses took action, thinking there were no

1, Donovan Courville speculates that the daughter of Pharaoh was Sebek-nefru-
re, the daughter of Amenembhet III, and the last of the kings of Dynasty XII. He
also speculates that she married Kha-nefer-re, the twenty-fourth king on the Turin
papyrus. There is a legend that Chenephres (Greek transliteration) was the foster-
father of Moses, but prior to Courville’s chronological reconstruction, it was not be-
lieved possible, given this king’s placement on the Turin papyrus. Courville, The Ex-
odus Problem and its Ramifcations (Loma Linda, California: Challenge Books, 1971), 1,
pp. 155-57,
