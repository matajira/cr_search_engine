iu
SEPARATION AND DOMINION

But against any of the children of Israel shall not a dog move his tongue,
against man or beast: that ye may know how the Lorp doth put a
difference between the Egyptians and Israel (Ex. 11:7).

God announced that there was a radical covenantal difference
between the Hebrews and the Egyptians, This difference was about
to be manifested in a sharp break historically: the Exodus. So great
was this difference, that no dog would lick its lips (literal translation:
sharpen its tongue) at the Hebrews upon their departure from
Egypt. No dog would eat any Hebrew (as dogs later ate Jezebel, the
result of God’s special curse: I] Kings 9:36). The Egyptians now
respected them and their leader, Moses. “And the Lorp gave the
people favour in the land of Egypt, in the sight of the Egyptians.
Moreover the man Moses was very great in the land of Egypt, in the
sight of Pharaoh’s servants, and in the sight of the people” (Ex. 11:3).
The meaning was clear: the Hebrews would leave victorious, having
seen their enemies so thoroughly defeated, that not one of them
would raise a cry against them. No jeering crowds would force them
to “run the gauntlet,” throwing rocks or garbage at them as they
departed. The Egyptians believed in the continuity of being, and
Moses had vanquished the representative of Egypt’s gods, who him-
self was believed to be divine. Were not the Hebrews linked to that
victorious God, through Moses?

Consider the Exodus from the point of view of a citizen of Egypt
or one of the Canaanitic nations. A slave population had successfully
challenged the dominant political order of its day. Egypt’s wealth
and power, even in decline (if Courville’s chronology of the dynasties
is correct), were recognized throughout the ancient world. Yet Egypt
could not bring these Hebrews into submission. The ancient world
viewed a military defeat as a defeat for the gods of the vanquished

132
