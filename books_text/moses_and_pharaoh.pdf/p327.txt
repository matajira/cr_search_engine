The Reconstruction of Egypt’s Chronology 309

strongly inclined to agree with Courville and Velikovsky: this document
was the product of the Exodus. But even if it wasn’t, it presents a pic-
ture of the shock to the Egyptian mind that such an event must have
produced.?® The gods of Egypt had been laid low; the order of the uni-
verse, which had been guaranteed by Pharaoh, had been overturned.

This document is one of the most important pieces of evidence
used by Velikovsky in his reconsiruction of Egyptian chronology. It
offers evidence of a major discontinuity in the static order of Egypt, a
break which deeply affected the writer.3? Here is a side-by-side com-
parison produced by Velikovsky in 1973:4

SOME PARALLEL TEXTS

Exodus 7:21... there was blood Papyrus 2:5-6 Plague is through-
throughout all the land of Egypt. _out the land. Blood is everywhere.

Exodus 7:20 . . . all the waters Papyrus 2:10 The river is blood.
that were in the river were turned

to blood.
Exodus 7:24 And all the Egyp- Papyrus 2:10 Men shrank from
tians digged round about the river __ tasting—human beings, and thirst
for water to drink; for they could after water,

not drink of the water of the river.

Exodus 7:21... and the river Papyrus 3:10-13 That is our

stank. water! ‘hat is our happiness!
What shall we do in respect
thereof? All is ruin!

Exodus 9:25... and the hail Papyrus 4:14 Trees are destroyed.
smote every herb of the field, and Papyrus 6:1 No fruit nor herbs are
brake every tree of the field. found...

Exodus 9:23-24 ... the fire ran Papyrus 2:10 Forsooth, gates, col-
along the ground... there was. —_umns and walls are consumed by
hail, and fire mingled with the fire.

hail, very grievous.

38. The focus of the poem is “the world tumed upside down,” in effect. The sage
complains that slaves and poor people who formerly had nothing are now rich, while
the formerly rich are now poor. The slave girls do not appear to have left the ation
in a massive exodus. ‘They remain in the land, with the jewels. If this poem is a
product of the Hyksos invasion, it indicates that some wealth was still left to the
upper-class Egyptians, since they must have had items of value thal were later
confiscated by poorer people.

39. Ages in Chaos, pp. 22-39.

40., Velikovsky, ‘A Reply to Sticbing,” Pensée, IV (Winter 1973-74), p. 39.
