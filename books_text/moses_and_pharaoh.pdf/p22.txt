4 MOSES AND PHARAOH

and time manifested itself also by a willingness to comply with the
state... . The usual attitude was one of contempt for the material
world, which inchided the state, and an outward compliance and in-
difference. A philosophy calling for an escape from time is not likely
to involve itself in the battles of time.”?

Their denial of the continuing validity of biblical law has led
them to deny the relevance of earthly time. By denying biblical law,
they thereby foresake the chief tool of dominion — our means of using
time to subdue the earth to the glory of God. The basic idea which
undergirds escapist religion is the dental of the dominion covenant. The
escapist religionists believe that the techniques of self-discipline,
whether under God or apart from God (Buddhism), offer power over
only limited areas of life. They attempt to conserve their power by
focusing their ethical concern on progressively (regressively) nar-
rower areas of personal responsibility. The “true believer” thinks that
he will gain more control over himself and his narrow environment
by restricting his self-imposed zones of responsibility, His concern is
self, from start to finish; his attempt to escape from responsibilities
beyond the narrow confines of self is a program for gaining power
over self. It is a religion of works, of self-salvation. A man “humbles”
himself—admits that there are limits to his power, and therefore
limits to ihe range of his responsibilities —only to elevate self to a
position of hypothetically God-like spirituality,

  

Escapist religion proclaims institutional peace—“peace at any
price.” Ezckiel responded to such an assertion in the name of God:
*,., they have seduced my people, saying, Peace; and there was no
peace” (Ezk. 13:10a). Patrick Henry’s inflammatory words in March
of 1775—“Peace, peace —but there is no peace”*— were taken from
Ezekiel and also Jeremiah: “They have healed also the hurt of the
daughter of my people slightly, saying, Peace, peace; when there is
no peace” (Jer, 6:14). This rival religion proclaims peace because it
has little interest in the systematic efforts that are always required to
purify institutions as a prelude to social reconstruction.

3. Rousas John Rushdoony, The One and the Many: Studies in the Philosophy of Order
and Ultimacy (Fairfax, Virginia: Thoburn Press, [1971] 1978), p. 129.

4. Norine Dickson Gampbell, Patrick Henry; Patriot and Statesman (Old Greenwich,
Connecticut: Devin-Adair, 1969), p. 130. The substance of Henry's famous St.
John’s Church speech, which mobilized the Virginia Assembly, was reconstructed
by a later historian, William Wirt, but is generally considered representative. This
was Henry's famous “Give me liberty or give me death” speech, one of the most fa-
mous speeches in U.S. history.

 

  
