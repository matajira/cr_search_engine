412 MOSES AND PHARAOH

Patriarchs, 14
Paul, 259
Pax Romana, 269
peace
classical liberalism’s, 214
covenant, 181
democracy, 219
Patrick Henry, 4
Sider’s, 340
static (Egypt), 47
treaty, 153-55, 181
peace treaty, 173, 181, 183
peasants, 366
Pensée, 306
Pentecost
ambassadors, 158
calendar, 161-62,
discontinuity, 158
fall of Jerusalem, 382
leaven church, 163
symbolism, 161-65
Pentecostalism, i58n
“people,” 137
Pepi I, 318
Pepi H, 311-12
perfection, 168, 181, 182
perfectionism, 181n, 212, 282, 285
Pericles, 198
persecutions, 187
personhood, 352-53
persuasion, 102
pessimism, xv
Peters, James, 41, 373
Petrie, W. M. F., 307, 371
Pharaoh
bricks, 29
calculating, 52
capitulation, 177
compromise, 184
cost analysis, 87
covenant-breaker, 109
daughter of, 80
distribution, 52
divine, 31, 84, 109
economist, 97
Exodus, 32
Exodus (name of), 312

flexible, 124
guessing, 55
hardened, 127, 130
herdsman, 32, 108
humiliation, 177
kidnapper, 83-84
medicine-man, 32
Nile &, 108
obstinate, 126
power of, 54
rope, 129
seeks approval, 178
sovereignty, 64, 119, 126-28,
179, 185
value &, 52
Pharez, 190
Pharisees, 164, 379
Philippines, 342
pietism, 3
Pietists, 213
pilgrimage, 358
pillars, 158, 255, 276, 376-77
piracy, 213
plagues, 104-5, 106, 120, 124, 144
plan, 97-98 98, 99, 101
planning, 48, 55, 58
planting, 264-66
plumbing, 371
pluralism, 206, 207, 244
politics, 73, 196
political economy, ix
pollution, 331
“polylegalism,” 196, 206
Polynices, 384
polytheism, 9, 63, 196, 206
poor people, 143, 193
population
annual growth, 20
bureaucracy vs., 63-64
conversions, 24
decline, 326-68
Exodus, 22
resources &, 100
scattered, 89, 90-1
stagnation, 25, 26
steady-state, 22, 23
threat, 48
