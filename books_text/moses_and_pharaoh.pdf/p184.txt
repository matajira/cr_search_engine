166 MOSES AND PHARAOH

was that which had not had time to mature fully. The animals were
yearling lambs (Num. 28:3, 9), young bulls (Num. 28:11), a young
goat, or kid (Num. 28:15), and young pigeons (Lev. 1:14; 5:7). The
day of atonement required young animals (Num. 29:1-5). These
animals had not yet begun their work. The red heifer, which was
used to make the ashes for the water of purification, had to be
umblemished, three years old, and never yoked (Num. 19:2). The
sacrifices required an animal cut down in its prime, with its productive
life ahead of it. This animal forfeited both the joys and labors of the
bulk of its adult life. Unquestionably, this symbolism pointed to Jesus
Christ, the lamb of God slain in the midst of His prime. In time and on
earth, He forfeited a life of dominion. He forfeited the joy of eating
thé fruits of His labor. He forfeited the leavened loaves and the
honey. He forfeited the blessings of long life, despite His perfect
keeping of the law of God. He forfeited all this, so that His people
might receive these blessings. They are to exercise dominion, in time
and on earth. They are to labor. They are to eat the firstfruits, sym-
bolized by Pentecost. They are also to eat the honey and the baked
leaven loaves.33 They are to serve, in short, as God’s leaven, “incor-
rupting” Satan’s former kingdoms, causing the kingdom of God to
rise up. They are given what Christ forfeited: vistble dominion, in time
and on earth. God does not burn up the leaven before its time, be-
fore it has matured, before it is fit for communion’s joyful eating. A
leavened offering, like honey, is not burned on Gad’s altar.

But what about Satan? Isn’t Satan eventually burned? Aren’t
Satan’s followers burned, as salted offerings? Haven't they been
given time? This points to the removal of the devil’s ability to con-
tinue to develop. He will be cut down in the midst of his rebellion

33. I know of no church which celebrates the communion meal with honey, yet
many of them use leavened bread. This is inconsistent. The use of leaven points to
the use of honey. The completed work of Christ’s sacrifice is behind us historically.
The completed offering of Christ at Calvary points both to the use of leaven (the
formerly prohibited completed baked bread) and honey (the formerly prohibited
completed sweetener). Honey ought co be substituted for the bitter herbs. The
church has not been consistent with its symbolism; bitter herbs were never incor-
porated into the Christian Passaver, yet honcy has not replaced the Old Covenant’s
required herbs — an obvious lack of consistency. Passover was to be fasted. What was
bitter is now sweet. The contrast has not been made visible symbolically.
Deliverance has not been consistently symbolized. The taste of victory implied in
honey’s sweetness has not been a feature of the church's sacraments. When the
church’s eschatology changes to a more optimistic view of the role of the church in
history, and its victory over creation is made progressively clearer, churches will
then adopt the use of honey in the communion service.
