Unconditional Surrender 185

triumph, without maintaining a shred of original sovereignty for
himself or the Egyptian State, he raced after the Hebrews in a rage.
Why had he let them go? Didn’t they know who he was? Didn't they
know what Egypt was? Didn’t they know that he represented man’s
divinity in this world? He would show them, He would bring them
low. Every Hebrew knee would bow to him, for they had not allowed
him even a trace of sovereignty. They had wanted him to surrender
unconditionally to their God (and therefore to them, ‘given the
Egyptian theology of the continuity of being), although God allowed
him to keep his kingdom. But it was the principle of the thing that
concerned him. If they were going to demand unconditional sur-
render by him to their God in principle, then he was going to de-
mand visible unconditional surrender from them once again. He
was going to drag them back. They would not go free. They would
not sacrifice to any God who would not acknowledge at least a
degree of independent sovereignty to Pharaoh and his State. It
would be a fight to the finish. It turned out to be just exactly that.

Pagan kingdoms implicitly want unconditional surrender from
their enemies. Lawless men want the same. As men grow more arro-
gant, as they attempt to divorce themselves from the concept of
lawful dominion under the restraints of God’s law, they adopt
policies of unconditional surrender, in time and on earth. They
launch sneak attacks, in viclation of Deuteronomy 20:10-15, as the
Japanese did against the Russians in 1904 and as they did against the
United States in December of 1941. Pagan governments demand un-
conditional surrender, as the Allies demanded from Germany, Italy,
and Japan in the Second World War.® In short, they want “a fight to
the finish,” just as Pharaoh wanted when he pursued the Israelites
into the Red Sea. They seek to be free of the restraints of God's law
in order to impose a law-order which violates God's law.

, Conclusion

Unconditional surrender is an inescapable concept: the question is, “Sur-
render to whom?” Will it be surrender to God, progressively through
time, until the final day of judgment, when surrender will be ab-
solute and unconditional? Or will it be surrender to Satan’s
kingdom, in time and on earth, through Satan’s radical breaks in the

6, Ann Armstrong, Unconditional Surrender (Brunswick, New Jersey: Rutgers Uni-
versity Press, 1961),
