The Rule of Law 231

be that, while the state provides only a uniform minimum for all who
are unable to maintain themselves and endeavors to reduce cyclical
unemployment as much as possible by an appropriate monetary
policy, any further provision required for the maintenance of the ac-
customed standard should be left to competitive and voluntary
efforts.”®? Which of Hayek’s two irreconcilable arguments are we to
believe?

Hayek is stuck. Volume two of Law, Legislation and Liberty is titled,
The Mirage of Social Justice. He proclaims the idea of one law for all
men. He proclaims the benefits of general rules that are written
without any attention to the specific individual results of such
rules.88 As a defender of methodological individualism, he attacks
the very concept of social justice. Such a concept presupposes a
hierarchy of collective ends. But we cannot as “scientific economists”
speak of the “value to society” of any economic good or service.89 We
can only speak of a service’s value to individually acting men or to an
organization. Society is not an organization; it is a spontaneous
order. Civil government is an organization; society is not. “And,
though the order of society will be affected by actions of government,
so long as it remains a spontaneous order, the particular results of
the social process cannot be just or unjust.”°° Thus, it is illegitimate
to speak of social justice. Such a concept is anthropomorphic and im-
mature.®! Society cannot act for a single purpose.?? To whom can we
appeal if we believe that the outcome of spontaneous and therefore
unplanned market forces is somehow unjust? There is no answer.*?
The concept of social justice has meaning only in a command soci-
ety.°* The term itself has become an implement of demagoguery.*®

Having said all this, he nevertheless calls for a State-imposed
redistribution of wealth in the name of preserving social order. Yet
he opposes socialism and democratic economic interventionism
because such coercion is destructive of morality.* And then, as if to

87. Hayek, Constitution, p. 302.

88. The Mirage of Social Justice, ch. 7: “General Welfare and Particular Purposes.”
89. Ibid. p. 75.

90.. Hbid., p. 32.

94. Ibid., pp. 62-63.

92. Ibid., p. 64.

93. Ibid., p. 69.

94. Idem.

95. Ibid, p. 97.

96. The Political Order of a Free People, pp. 170-71.
