14 MOSES AND PHARAOH

The Patriarchal Eva: 215 Years

The best place to begin to unravel this problem is with the chron-
ology of Abraham’s family. We are told that he was called out of
Haran when he was 75 years old (Gen. 12:4). Isaac was born 25 years
later, when Abraham was a hundred (Gen. 21:5). Jacob and Esau
were born 60 years later, when Isaac was 60 years old (Gen. 25:26).
Finally, Jacob died at age 130 in Egypt (Gen. 47:9). Therefore, from
Abraham's entrance into a forcign land until the Israelites’ descent
into Egypt, about 215 years elapsed (25 + 60+ 130). If we assume that
the establishment of the covenant took place in the first year or so of
Abrahams sojourn in Canaan, with 25 years in between the covenant
(Gen. 15) and the birth of Isaac (Gen, 21), then we can begin to make
sense of the data. God said that Abraham’s heirs would be in bondage
for 400 years, while Paul said it was 430 years from the covenant to the
Exodus. Wwe subtract 25 from 430—from the covenant to the birth of
Isaac, the promised son of the covenant line — we get 405 years. This
is very close to the 400 years of the “affiction” promised in Genesis
15:13-14 and mentioned by Stephen in Acts 7:6.

We are now arguing about only five years, from the birth of Isaac
to the period in which the captivity “in” Egypt—under Egypt's
domination began. We are told in Genesis 21 that it was only after
Isaac was weaned that Ishmael mocked him—“laughing” in the
Hebrew (vv. 8-9). This can be understood as the beginning of the
period of Egyptian persecution, for Ishmael was half Egyptian.” It
was the time of Isaac’s youth, perhaps about age five. Abraham then
expelled the Egyptian woman and her son, who travelled into the
wilderness (21:14). Thus, it was not the bondage period in geograph-
ical Egypt that God had in mind, but the entire period of pilgrimage,
during which they were afflicted by strangers.

Residence in Egypt: 215 Years

The culmination of this period of rootlessness, or life in foreign
lands, was the final era of outright bondage in Egypt (Gen. 15:14).
Courville’s comments are appropriate, that

the periad of affliction began back in the time of Abraham and not with the
Descent, Actually, the affliction in Egypt did not begin with the Descent but

7. Laan indebted to James Jordan for this insight. If it is incorrect, then we would
have to adopt Courvilie’s approach, namely, to argue that it seems legitimate to un-
derstand the 400 years of Gen. 15:13 as a rounding off of 405.
