The Reconstruction of Egypt's Chronology 307

Sir W. M. Flinders Petrie:] ‘Joshua did not find any such Egyptian
hold during his conquest.’. . . No reference has been found that
could be interpreted as even hinting at an exodus during the inter-
regnum between the Eighteenth and Nineteenth Dynasties, and
only the fact that the situation was such as to make an exodus possi-
ble favors this hypothesis.”32

The next theory reduces the age of the Exodus further: it has for its cor-
nerstone a stele of Mernepiah, in which this king of the Nineteenth Dynasty
says that Palestine “is a widow” and that “the seed of Israel is destroyed.”
This is regarded as the earliest mention of Israel in an Egyptian document.
Merneptah did not perish in the sea, nor did he suffer a debacle; he obvi-
ously inflicted a defeat on Israel and ravaged Palestine. The circumstances
do not correspond with the pronounced tradition of Israel, but since it is the
first mention of Israel, Merneptah is regarded by many as the Pharaoh of
the Exodus (about -1220), and Ramses II, his predecessor, as the Pharaoh
of the Oppression. Other scholars, however, consider the mention of Israel
in Palestine in the days of Merneptah not as a corroboration, but as a
refutation of the theory that Merneptah was the Pharaoh of the Exodus.
They argue that if he found Israel already in Palestine, he could not have
been the Pharaoh of the Exodus.

A further obstacle.to placing the Exodus in the reign of Merneptah has
also been emphasized. If he really was the Pharaoh of the Exodus, then the
Israelites must have entered Palestine at least a generation later, about -1190
to -1180; on this theory there remains only a century for the events of
Judges.*

Velikovsky then quotes from W. F. Albright, who in turn had
been cited by Petrie: “Under any chronological system which can
reasonably be advanced, the date of Israel’s invasion and settlement
falls within the period (1500-1100 before the present era) when the
country was ruled by Egypt as an essential portion of its Syrian Em-
pire.” Then Velikovsky asks some: key questions. “But if this is so,
how could the Israelites have left Egypt, and, having left Egypt, how
could they Have entered Palestine? Moreover, why do the books of
Joshua and Judges, which cover four hundred years, ignore the rule
of Egypt and, indeed, fail to mention Egypt at all?”3+

These are obvious questions, but few conservative Bible com-

32, Immanuel Velikovsky, Ages in Chaos (Garden City, New York: Doubleday,
1952), p. 8.

33. Did. p. 9.

34. Ibid., p. IL.
