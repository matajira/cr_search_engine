State
caretaker, 211
continuity, 9
deification, 60
dependence on, 274
efficiency, 293
night watchman, 211
obedience to, 291-93
omnicompetent, 296
power, 241, 244
pseudo-father, 217
redeemer, 287
rights &, 128-29
static, 47
worship of, 244
static, 275, 279
statism, 210
Stent, Gunther, 148n
stewardship, 116, 19
strabo, 371
straw, 89, 91
structure, 147
stubble, 91
subjective utility, 99, 201, 218-19
subjective value, 100, 270
subordination, 188, 255
success indicators, 102, 218
sun god, 36
Supreme Court, 221, 222, 226
surrender, 154, 181-85, 241; see
also “defeat”
swastika, 373
sword, 154, 377

tabernacle, 192, 377
tale, 89, 89n

talents (parable), 5
talismans, 33

‘Talmud, 379

tamar, 190

tasks, 274

taxation, 210, 21¢

Ten Commandments, 294
Thebes, 383, 386

theft, 214

theology as life, 9
theology, x

Index 47

theophany; 377
Thiele, Edwin, 299-300, 314, 386n
Third World, 338-39
thrift, 338
Thomson, Christian, 322
throne, 192
thrones, 153, 157, 174
‘Thutmose IIT, 313, 386
time
battles of, &
before time, 41
bondage, 266
classical, 267
“dough,” 170
end of, 26, 272
judgment, 332
linear, 7
maturation, 165
original, 265
overcoming, 266
personalism, 175
point in (outside), 98
scarcity, 95, 283
test period, 191
tithe, 214
Abraham's, 116
God's, 116
States’, xiii, 60
Tiy, 384, 386
‘Tories, 2160
‘Tower of Babel, 47
training, 376
treaty, 153-55, 155
tree of life, 373, 375, 383
trial and error, 96
tribute, 79, 80, 84-85
Trinity, 97
truth, 237, 241
Tsountas, C., 303
tug of war, 129
Tullock, Gordon, 223, 226,
228
Turks, 364
Tutankhamen, 384-86
‘Tylor, Edward, 322
tyranny, 220, 261
‘Tyrrell, R. E., 287
