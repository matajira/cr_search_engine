Separation and Dominion 139

The Essence of Christianity (1841), created a sensation, and converted a
whole generation of European intellectuals to atheism. Frederick
Engels, Marxism’s co-founder, remarked once that “One must him-
self have experienced the liberating effect of this book to get an idea
of it. Enthusiasm was general; we all became at once Feuerbach-
ians.”!° In this book, Feuerbach attacked Christianity’s concept of
saved and lost. Such a view of man separates men from other men.
Yet man is a unified whole, a species being. In fact, Feuerbach said,
God is really nothing more than man’s own thoughts, projected into
the religious consciousness of men. “God is the human being; but he
presents himself to the religious consciousness as a distinct being.”!!
The Christian denies. that man is God, and this is unforgivable.
Even worse, Christians say that some men will be saved by God,
and others will not be saved. “To believe, is synonymous with good-
ness; not to believe, with wickedness. Faith, narrow and prejudiced,
refers all unbelief to the moral disposition. In its view the unbeliever
is an enemy to Christ out of obduracy, out of wickedness. Hence
faith has fellowship with believers only; unbelievers it rejects. It is
well-disposed towards believers, but ill-disposed towards unbeliev-
ers. In faith there lies a malignant principle.”'?

Marx and Engels, his most famous converts, rejected
Feuerbach’s brand of non-divisive humanism. ‘They saw the “illus-
ion” of God as a product of a deliberate lie: a weapon used by
capitalists to suppress the proletariat. The problem is class divisions;
the solution is class warfare, with the proletariat fmally emerging
victorious over the bourgeoisie.!? They called for unconditional sur-
render by the bourgeoisie; they called for all-out warfare. They
predicted absolute victory. They saw that true victory over evil in-
volves triumph, in time and on earth,’* They saw that there must be
a self-awareness on the part of the “vanguard” of history, the pro-
letariat, concerning the irreconcilable differences between them and

10, Frederick Engels, “Ludwig Feuerbach and the End of Classical German Phi-
Josophy” (1888), in Karl Marx and Frederick Engels, Selected Works, 3 vols. (Moscow:
Progress Publishers, [1969] 1977), III, p. 344.

11. Ludwig Feuerbach, The Essence of Christianity, trans. George Eliot (New York:
Harper Torchbooks, [1841] 1957), p. 247.

12. Ibid., p. 252. Emphasis in original.

13, Marx and Engels, “Manifesto of the Communist Party” (1848), in Selected
Works, 1, pp. 125-26.

14. Gary North, Unconditional Surender: God's Program for Victory (2nd ed.; Tyler,
Texas: Geneva Divinity School Press, 1983).
