The Demographics of Decline 327

The assumption of Western intellectuals concerning demo-
graphics is that population growth threatens per capita income.
“More mouths to feed” means more starvation. But this assumption
is not correct. It is not the number of mouths to feed which is
significant economically; rather, it is the productivity available to feed
those mouths. This has been a continuing theme in Bauer's books.
He writes: “Rapid population growth has not been an obstacle to
sustained economic advance either in the Third World or in the
West. Between the 1890s and 1930s the sparsely populated area of
Malaysia, with hamlets and fishing villages, was transformed into a
country with large cities, extensive agricultural and mining opera-
tions and extensive commerce. The population rose from about one
and a half to about six million; the number of Malays increased from
about one to about two and a half million. The much larger popula-
tion had much higher material standards and lived longer than the
small population of the 1890s. Since the 1950s rapid population in-
crease in densely-populated Hong Kong and Singapore has been ac-
companied by large increases in real income and wages. The
population of the Western world has more than quadrupled since the
middle of the eighteenth century. Real income per head is estimated
to have increased by a factor of five or more. Most of the increase in
incomes took place when population increased as fast as, or faster
than, in the contemporary less developed world.”3

Bauer's focus is on character, attitudes, and institutional ar-
rangements, not natural (physical) resources. How else can we ex-
plain the spectacular increase in per capita income which has been
experienced in Hong Kong (at least prior to the fear that the Com-
munist Chinese would not renew their lease with the British crown
colony in 1999)? “The number of people who can live in any area at
the specified standard of living is not determined by the extent of
land or of other physical resources available there. It depends very
largely on the personal qualities, social institutions and mores and
political arrangements of the population, on the state of technology
and on external market conditions for imports and exports.”*

Thus, the guilt felt by the West’s intellectuals concerning popula-
tion growth in the Third World is valid, but not because “we” taught
the Third World about modern medicine and other life-saving

3. P. T. Bauer, £quality, p. 43.
4. Ibid., p. 50.
