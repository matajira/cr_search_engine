The Metaphor of Growth: Ethics 265

related to the metaphor of growth which was understood by ancient
pagan cultures. While the Hebrew metaphor was similar in lan-
guage to those pagan metaphors of agricultural prosperity, there
were important differences in usage. These differences stemmed
from the. differing conceptions of the creation.

The Hebrews believed that God created the universe out of
nothing. God’s sovereignty in the fiat.act of creation therefore
guarantees His total sovereignty over the affairs of history. The doc-
trine of creation out of nothing implies the doctrine of God’s provi-
dence. History is linear—creation, Fall, redemption (incarnation,
crucifixion, resurrection), partial restoration, final judgment, total
restoration —and it is totally personal. The Fall of man was seen as
being ethical, an act of man’s will in defiance of God. This Fall was
not understood as a product of some flaw in nature, for nature had
been created good, The Fall of man rcsulted in the judgment by God
of both man and his environment, but the cause was man’s ethical
rebellion, Ethical restoration, which involves personal repentance
and adherence to God's covenant, was promised by God. The origi-
nal peace of nature will return (Isa. 11:6-9). Restoration therefore
meant to the Hebrews the eventual victory of God in time and on
earth. The New Testament’s vision is the same (Rom. 8:18-23).

Pagan cosmologies did not share this view of creation. God was
not seen as the Creator who brought the creation out of nothing.
Matter, whether in the form of raw chaos, or water, or a cosmic egg,
was seen as co-eternal with “god.” This god’s victory over chaos at
the time of “creation” (the emergence of “ordered being”) — the “origi-
nal time”+— was viewed as a continuous process. God cannot guar-
antee the outcome of the struggle between order and chaos. He can-
not guarantee the outcome because he himself is finite. Historical
change is neither linear nor absolutely personal; it is inherently

 

4, The subject of pagan man’s concept of “original time,” the time of the creation,
is treated in most of the books by Mircea Eliade, but especially in Cosmos and History:
The Myth of the Eternal Return (New York: Harper Torchbooks, 1959), and in The
Sacred and the Profane: The Nature of Religion (New York: Harper Torchbooks, [1957]
1961), ch, 2: “Sacred Time and Myths.” Pagan men in primitive cultures have long
believed that they can participate in this original time through special rituals,
especially chaos festivals. It is significant that neither Judaism nor Christianity cele-
brates the creation ritually, nor docs it appear in their religious calendars. The crea-
tion is understood as exclusively the work of God.

  
