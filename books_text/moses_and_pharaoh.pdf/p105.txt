Cumulative Transgression and Restitution 87

was willing to make restitution by the shedding of a lamh’s blood,
“for there was not a house where there was not one dead” (Ex.
12:30b). God required restitution for the years of servitude beyond
the maximum permitted, six. In fact, Egypt even owed Israel for the
first six years, since the six-year slave contract was a debt-slavery
contract, and Israel had not been in debt to Egypt. Egypt had acted
as though Israel had been a lawful captive in wartime, or a debtor to
Egypt. Israel was neither. Furthermore, Egypt owed restitution for
having kidnapped Israel. Egypt owed Israel for having treated Israel
as less than a full wife, trying to expel her without returning her
dowry, as though she had been a slave-wife. Egypt paid dearly for
these acts of long-term lawlessness.

The cost of the pharaohs’ brick pyramids and brick treasure cities
turned out to be far higher than any Egyptian, especially the various
pharaohs, had dared to calculate. Any intuitive cost-benefit analysis
in the mind of a pharaoh so many benefits, in time and eternity,
from a new pyramid or city versus so many expenditures in feeding
and controlling the Hebrew slaves—turned out to be catastrophically
erroneous: The pharaohs drastically overestimated the benefits of
their construction projects, and they underestimated the real costs of
enslaving the Hebrews. The pharaohs had abandoned the most im-
portant pair of guidelines for making accurate cost-benefit analyses,
namely, the free market, which establishes prices, and the law of God,
which establishes God's justice. All the pharaohs, from the enslaving
Pharaoh tothe Pharaoh of the Exodus, ignored the principle of resti-
tution in their dealings with the Israelites. When the final bills came
due, ancient Egypt collapsed.
