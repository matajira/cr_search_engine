372 MOSES AND PHARAOH

spirited people who were called the Minoans by Evans were figments
of his vivid imagination, The Minoans were an integral part of the
Bronze Age culture —a demonic, fearful, death-obsessed culture.

These “Minoans” mummified their dead, as the Egyptians did,
for similar theological reasons, at least with respect to the dead per-
son’s passage through the underworld.'° As Wunderlich shows, they
had trading relations with the Egyptians, who called them the Keftiu.6
The link with Egypt is testified to by the Egyptian-style clothing of
men pictured on the walls of the “palace,” as well as by drawings of
these “Minoan” people in a tomb in Egypt. Evans dug up an Egypt-
ian statue in the “palace” which was made in the era of Egypt’s
Twelfth Dynasty.!? Conversely, Egyptian ruins contain examples of
pottery. that look like the “Minoan” pottery.!® (On Evans’ misuse of
the evidence to make it fit an evolutionary model, see Appendix A:
“The Reconstruction of Egypt’s Chronology,” under the subhead,
“Egypt and Crete.”)

Beyond Crete

The labyrinth is an extremely important symbol. It appears in
most of the ancient cultures in one form or another. Sir Arthur
Evans discovered numerous coins on Crete that had the Minotaur
and labyrinth designs on them.!9 These coins were not contem-
poraneous with the construction of Knossos, of course; coins began
to appear about the sixth century, B.c. Hooke reports that examples
of labyrinth designs appear constantly on seals from Asia Minor,
Palestine, and Mesopotamia.”° Archaeologists have discovered laby-
rinths drawn in rocks in Britain and Scandinavia. Hooke recognized
this as the probable product of trade between the Mediterranean and
the Baltic region.2! But what Hooke did not know is that the
Knossos labyrinth and Minotaur designs have been discovered in
pre-Columbus North America and South America.

In rock formations in Oraibi, New Mexico and Cuenca, Ecua-
dor, the labyrinth pattern of Knossos appears. The Minotaur —half

15. Wunderlich, ch, 21: “A Visit in the Underworld”

16. Zbid., pp. 148, 174-80.

17. Leonard Cottrell, The Bull of Minos (New York: Rinehart & Co., 1958), pp.
136-37.

18. Ibid., p. 138. ,

19, Hooke, pp. 9-10.

20. Bid, p. 10,

21. Hid, p. 41.
