418

ultimate resources, 333
uncertainty, 118, 260
union, 3
United States, 341
unleavened; see “leaven”
unleavened bread
discontinuity, 158
haste, 150, 158-59, 159
unity, 199
urn, 371
USSR, 59, 119n, 288, 291; see
also “Soviet Union”
usury, 193, 213
utilitarianism, 219-92
utility, see “subjective utility”
utopia, 234

value, 53, 270
Van Dahm, T,, xiiin
Vaninger, Stan, 319
variable proportions, 92
“‘Velikovsky Affair,” 304-6
Velikovsky, Immanuel, 8, 304-12, 384
vengeance, 131, 143
Venus, 306-7
Vickers, Douglas, xiv
victory
Communism’s faith, 139
comprehensive, 269
covenantal, 269
cultural, 174
earthly, 269
Exodus, 156
freedom &, 259
inevitable, 135
Jesus’, 153, 157-58, 171
leaven, 165
Passover, 151
past, 157-58, 171
redemption, 165
slaves, 135
spoils, 260
step by step, 174
Vikings, 358
Vincent, M. O., 349
vineyard, 118
Von Mises; see “Mises”

MOSES AND PHARAOH

wages, 335
walls, 375, 379, 381, 383
Wallace, Alfred Russe}, 335
Walzer, Michael, xvi
wandering, 278, 383
warfare, 155-56
warlord, 122
Washington National Gallery, 366
waste, 50-32
wave offering, 162
wealth, 2
Weber, Max
bureaucracy, xvi, 35, 226-27
bureaucratic cage, 211
capitalism, 213
degree, 227
democracy, 227
medieval city, 200
Protestantism, 272n
rationalism, 215, 234
Webster, Nesta, 362-63
week, 248, 251
welfare State, 45, 230-31, 245, 279
West, 212
whirl, 269
wife (slave), 85-6
wilderness, 140, 152, 279, 376, 380-82
Willesen, Folker, 19n
William the Conqueror, 386
Wilson, Woodrow, 226
Wilson, John A.
continuity, 30
divine Pharaoh, 31, 109
future-past, 47
herdsman, 32, 108
medicine-man, 32
Pharaoh’s power, 54
pyramids, 54
“the people,” 137
wine skins, 171
Wines, E. C., 19n
Wirt, William, 4n
Wittfogel, Karl, xn, 275
woman, 382
World Bank, 330
Worlds in Collision, 304-5
World War IE, 65, 338-39, 359
