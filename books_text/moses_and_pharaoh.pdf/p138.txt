120 MOSES AND PHARAOH

have some claim on the Hebrews as a form of His personal property,
could be accepted in theory by the Egyptians. What was repugnant
to them was the idea that He owned everything, “lock, stock, and
barrel,” in the traditional English terminology. That left them no
bargaining room.

The Hebrews belonged to God, the Egyptians belonged to God,
and Egypt belonged to God. God's assault against Egypt was not a
form of competition for temporary advantage, God against Pharaoh.
His victory was not an instance of a temporarily sovereign invader
who might be overcome later, when conditions changed. The same
event —the withdrawal of visible judgment—was interpreted differently
by Moses and Pharaoh. Pharaoh acted as though he believed that
God’s withdrawal of the plagues was a sign of His weakening, as if
cosmic forces or the gods of Egypt had finally begun to repel this in-
vader. He grew arrogant each time a plague ended. Moses had told
him that the removal of the hail and thunder was proof of God's con-
tinuing sovereignty, proof that the forces of nature arc not autono-
mous, but under the direct administration of God.

 

The Marxists’ Dilemma

This incident points to a fundamental problem for economic
theory, namely, the establishment of a point of originating (and
therefore final} ownership. The modern socialist movement,
especially Marxism, asserts that all ownership should be collective,
and the tools of production should be lodged in the State. Marx’s
words in Part II of the Communist Manifesto (1848) do not explicitly
establish State ownership, but they deny the rights of private owner-
ship: “In this sense, the theory of the Communists may be summed
up in the single sentence: Abolition of private property.” Again,
“Capital is a collective product, and only by the united action of
many members, nay, in the last resort, only by the united action of
all members of society, can it be set in motion. Capital is, therefore,
not a personal, it is a social power.”* At the end of Part II, he argues
that under pure communism, class antagonisms will disappear, and
therefore the State, as an organ of repression used by the ruling class
to suppress the lower classes, will finally disappear, “When, in the

 

3. Karl Marx and Frederick Engels, “Manifesto of the Communist Party” (1848),
in Marx and Engels, Selected Works, 3 vols. (Moscow: Progress Publishers, [1969}
1977), E, p. 120.

4, Ibid, 1, p. 121.
