308 MOSES AND PHARAOH

mentators, with only a few major exceptions, have even hinted to
their readers that such questions exist, let alone have solutions. They
have remained silent because they have no answers. In fact, Velikov-
sky himself never did come up with a final position. When did the
Exodus take place? Velikovsky was never sure. What he was sure of
was that either the chronology of Egypt was incorrect, or that the
biblical account is flawed. He concluded his book with this sum-
mary: “. . . we still do not know which of the two histories, Egyptian
or Israelite, must be readjusted. At the same time we observed how
the histories of other ancient countries and peoples accord with
either the Israelite or the Egyptian chronology; and how the histories
of Cyprus, Mycenae, and Crete, in correlating with one side or the
other, create confusion in archaeology and chronology.”35

The Ipuwer Papyrus

If an event as discontinuous and comprehensive as the Exodus
took place, then we might expect to find references to it in Egyptian
history. The absence of such a document need not automatically be
assumed to be evidence against the Exodus, for documents that old
rarely survive, and we can also imagine that any document testifying
to such a defeat of Egypt's gods would be destroyed by subsequent
Egyptians. But such a document does exist. It is called the Ipuwer
papyrus, also known as The Admonitions of an Egyptian Stave, the title
selected by Alan Gardiner for his 1909 translation. It had been ac-
quired by the Leiden Museum of the Netherlands in 1828, and it was
translated and studied in the late nineteenth century. This ancient
Egyptian document records a series of catastrophes that befell Egypt.
Velikovsky offers fourteen pages of parallel references between this
document and the account of the judgments in the Book of Exodus.
There are some remarkable parallels, including the most startling, a
reference to the Nile: “The river is blood” (Papyrus 2:10).36

The Ipuwer document goes on to say: “Nay, but gold and lapiz
lazuli, silver and turquoise . . . are hung around the necks of slave-
girls. But noble ladies walk through the land, and mistresses of
houses say: ‘Would that we had something we might eat.’ "37 [ am

35. Ibid., p. 338.

36. Ibid., p. 26.

37. Cited by Henri Frankfort, Ancient Egyptian Religion (New York: Harper
Torchbook, [1948] 1961), p. 85. Ipuwer’s poem is reproduced by Adolph Erman, The
Literature of the Ancient Egyptians, translated by A. M. Blackman (New York: Dutton,
1927), pp. 94ff.
