Preface xv

The End of an Era

The fires of the Enlightenment are beginning to burn low. The
civilization of the Enlightenment is losing confidence in its owa prin-
ciples. Perhaps even more important, if is beginning to lose faith in the
future. The American historian-sociologist Robert Nisbet has put it
well:

It was belief in the sacred and the mythological that in the beginning of
Western history made possible belief in and assimilation of ideas of time,
history, development, and either progress or regress. Only on the basis of
confidence in the existence of divine power was confidence possible with
respect to design or pattern in the world and in the history of the
world... .

But it is absent now, whether ever to be recovered, we cannot know.
And with the absence of the sense of sacredness of knowledge there is now
to be seen in more and more areas absence of real respect for or confidence
in knowledge —that is, the kind of knowledge that proceeds from reason
and its intrinsic disciplines. From the Enlightenment on, an increasing
nutnber of people came to believe that reason and its works could maintain

 

a momentum and could preserve their status in society with no influence
save which they themselves generated. But the present age of the revolt
against reason, of crusading irrationalism, of the almost exponential
devclopment and diffusion of the occult, and the constant spread of nar-
cicissm and solipsism make evident enough how fallible were and are the
secular foundations of modern thought, It is inconceivable that faith in
either progress as a historical reality or in progress as a possibility can exist
for long, to the degree that either concept docs exist at the present moment,

amid such alicn and hostile intellectual forces. '*

 

The leaders of this staggering humanist civilization have now
adopted the strategy of every dying civilization which has ever lost
the confidence of its citizens: they resort to the exercise of raw power.
This was the strategy of the Roman Empire, and it failed.!! This
substitution of power for ethics is the essence of the satanic delusion, It is
the essence of the power religion. It also is the essence of failure.

What will replace this phase of humanist civilization? Some ver-
sion of the society which Solzhenitsyn has called the Gulag Ar-

10. Robert Nisbet, History of the Idea of Progress (New York: Basic Books, 1980), p.
353,

11. Charles Norris Cochrane, Christianity and Classical Culture: A Study of Thought
and Action fiom Augustus to Augustine (New York: Oxford University Press, [1944]
1957).
