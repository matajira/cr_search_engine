174 MOSES AND PHARAOH

should expect to see is the Satanists’ equivalent of the Exodus: a
desperate rebellion by a people who had experienced generations of
rule by their enemies,%®

Let the Satanists celebrate their communion standing up, staffs
in hand, Those stafis were broken at Calvary. We are seated on the
thrones of judgment in history, and we shall dispense continual
justice, making their final revolt all the less justified, all the more
culpable, and all the more unsuccessful. The continuity of the word of
God will bring external cultural victory, step by step. “But the word of the
Lorb was unto them precept upon precept, precept upon precept;
line upon line, line upon line; here a little, and there a little; that
they might go, and fall backward, and be broken, and snared, and
taken” (Isa. 28:13). The enemies of God cannot survive the steady
onslaught of God’s people, as the latter progressively fulfill the terms
of the dominion covenant.39

Conclusion

The Passover points to a radical break with evil. The leaven of the
world—sin, death, and corruption—is not to be the ethical founda-
tion of God’s kingdom. Unieavened bread symbolized this radical
ethical discontinuity with Egypt and Egypt’s gods and culture. Bitter
herbs symbolized the grim reality of life under the dominion of Satan
and his representatives. God called the Israelites to obey His law.
Obedience to God’s law was to become the foundation of a new civi-
lization. At the feast of Pentecost, they were to celebrate the founding
of this new civilization, and they were to use leavened bread in this
ritual.

To accomplish the liberation of Israel from the bondage of sin,
represented by Egyptian civilization, God destroyed Egypt. The
avenger of blood gained vengeance for the blood-stained land on
Passover night. A radical historical discontinuity was the event which
drove the Israelites out of bondage and toward the land of Canaan.
This, in turn, was designed to bring the continuity of the maturation
process. Ethical conformity to God over time produces this continuity of
growth, both personally and culturally.

The ethical discontinuity of sin brings the historical discontinuity of
God's judgment. Adam learned this lesson when God expelled him

38. Gary North, Unconditional Surrender, pp. 200-1.
39. fbid., ch. 8.
