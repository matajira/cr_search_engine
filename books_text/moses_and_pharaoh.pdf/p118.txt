100 MOSES AND PILARAOH

must be compared over time. But pure subjective value theory, if
pressed to its logical conclusions, cannot deal with interpersonal
value aggregates, nor is it possible for “scientific” economics to com-
pare intrapersonal subjective utility over time. Thus, the more con-
sistent economists have admitted that no one can scientifically deter-
mine the optimum quantity of anything.

This is why God’s revelation of His permanent law structure is an
imperative concept for the very existence of truly logical economics.
Without such a permanent standard, simultaneously personal and ob-
jective, cconomics is logically impossible. In a subjective universe
without an authoritative God who reveals Himself to man, who in
turn is made in God’s image and can therefore understand God’s reve-
lation to man, there can be no permanent objective standards that are
relevant or meaningful for human action, including economics.

Other Things Never Remain Equal

One critic of the concept of the law of diminishing returns is Julian
Simon. He argues that economists have erred in attempting to apply
the concept in the case of mineral extraction, “The concept of
diminishing returns applies to situations where one element is fixed in
quantity — say, a given copper mine—and where the type of technol-
ogy is also fixed, But neither of these factors apply to mineral extrac-
tion in the long run, New lodes are found, and new cost-cutting ex-
traction technologies are developed. ‘lherefore, whether the cost rises
or falls in the long run depends on the extent to which advances in
technology and new lode discoveries counteract the tendency toward
increasing cost in the absence of the new developments. Historically,
as we have seen, costs have consistently fallen rather than risen, and
there is no empirical warrant for believing that this historical trend
will reverse itself in the foreseeable future. Hence there is no ‘law’ of
diminishing returns appropriate here.”

Simon also applies this interpretation of the law of diminishing
returns to the field of population theory. He argues that classical eco-
nomic theory followed Malthus and held that additional people
would reduce the per capita share of fixed economic resources. “The
more people using a stock of resources, the lower the income per per-
son, if all else remains equal.”!5 But new people coming into the

14, Julian Simon, The Uitimate Resource (Princeton, New Jersey: Princeton Uni-
versity Press, 1980), p. 53.
15, Tid. p. 257.
