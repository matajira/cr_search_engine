faith in, 245
formal, 216-219
God's, 227
higher, 224, 225
imposed, 241
legislation vs., 224
liberalism, 215
limitations, 285
obedience, 291
O. T., xiii, 4, 141, 175-76, 199
Parkinson’s, 293
personal, 278
pluralism, 244
predictable, 212, 219-23, 278
prosperity & 212
public, 188-89
reconstruction &, 275
regularity, 156
religion &, 196-97
respect for, 293
retroactive, 223, 226
rites, 196-97
rule of, 188, 220, 228, 243
salvation by, 287
self-government, 204
sovereignty, 215
strangers &, 203
universal, 196
warfare, 155, 156
‘Western, 240
law-order, 234
lawlessness, 206, 286
lawyers, 228
legal tradition, 240
leaven
church, 163, 163, 166
competing, 167, 169
“corrupt gifts,” 161
doctrine, 164
Egypts, 159, 167, 172
finished work, 161
holy, 160, 164
kingdom, 159-60, 169-72
maturation, 160, 161n; 165
purging, 167
rebellion, 164
Satan's, 166-67

Index 409

sin, 160, 175
“starter,” 160
victory, 165

Lee, F. N., 140n

leeks, 280

left (right), 42

legal briefs, 286

legal order, 229

legal rules, 219

legalism, 67

legislation, 224, 226

legitimacy, 109, 179

Leiden Museum, 308

Leoni, Bruno, 122n

Letwin, William, xviin

Levites, 22

Lewis, C. S., 239, 296n

Lex, Rex, 225

liberalism
evolutionism’s effects, 219
formal law, 229
Hayek, 229
Mises, 112-14
Protestantism &, 214
victimless crimes, 203

libertarianism, 110, 121-22, 203

liberation theology, xvii, 143, 195n,
342

lice, 105

lies, 64-66

life, 259

life span, 180

limitations, 285

litigation, 284

loans, 193

Lord’s Supper, 158, 164, 170, 171-72,
200; see also “communion”

love, 195

lower class, 76

Lucas, J. R., 235, 244

luck, 269

Macaulay, 285
Macedon, 45
Macmillan, 304-5
MAD, 359-60

magic, 39, 40, 297, 376
