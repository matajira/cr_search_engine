170 MOSES AND PHARAOH

perish in the flames. God waits to apply the final heat (II Pet.
3:9-10). First, His yeast— His church — must do its work, ih time and
on earth. The kingdom of God (which includes the institutional
church, but is broader than the institutional church) must rise, hav-
ing “incorrupted” the satanic.dough of the kingdom of Satan with the
gospel of life, including the life-giving reconstruction of all the in-
stitutions of culture.

What a marvelous description of God’s kingdom! Christians
work inside the cultural material available in any given culture,
seeking to refine it, permeate it, and make it into something fine.
They know they will be successful, just as yeast is eventually suc-
cessful in the dough, if it is given sufficient time to do its work. This
is what God implicitly promiscs us in the analogy of the leaven:
enough time to accomplish our individual and collective assignments. He tells
us that His kingdom will produce the desirable bread of life. It will
take time. It may take several poundings, as God, through the
hostility of the world, kneads the yeast-filled dough of men’s cul-
tures.3* But the end result is guaranteed. God does not intend to
burn His bread to a useless crisp by prematurely placing it in the
oven. He is a better baker than that.

The Symbolism of Communion

Christians should not eat unleavened bread exclusively at their
celebrations of the Lord’s Supper. They should eat large chunks of
leavened bread, delighting in the flavor and its ability to fill them.
This is what God says His kingdom is like. The leavened bread is a syin-
bol of God’s patience with us, a symbol of His restraint. As Peter wrote,
concerning the fiery judgment to come at the last judgment, God is
not slack concerning his promise, “but is longsuffering to us-ward,
not willing that any should perish, but that all should come to repen-
tance” (II Peter 3:9b). He delays the application of fire to the earth
(II Peter 3:10). As Christians celebrating the Lord’s Supper, we look
toward the future, toward the effects of our labors, in time and on
earth. We are God’s yeast, inevitably permeating the whole loaf, un-
til the risen dough is ready for the final fire. God dées not intend to throw
the dough into the fire prematurely, He does not intend to burn up the
work of His hands. He allows us to make our peace offering. Christ

34. Tam using the analogy of pounding the dough to apply to, historical cir-
cumstances. It is a suggestive analogy, not necessarily an inescapable implication of
the biblical text.
