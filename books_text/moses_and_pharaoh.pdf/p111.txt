The Optimum Production Mix 93

cither have to dismiss workers (the now excessive complementary pro-
duction factor) or add more land (the now “sub-optimum” production
factor) to the existing number of workers.

There is, in this agricultural example, an optimum mix (optimum
proportions) of the complementary factors of production: more land
or fewer workers. To produce a product at this optimum point
minimizes waste and maximizes income. The existence of such an
optimum point therefore pressures the resource owner to release-one
factor of production—in this case, labor—so that this scarce factor
can be used in some ether industry or on some other farm, thereby
increasing total output and maximizing consumer satisfaction with a
given quantity of resource inputs. The former resource renter is a
beneficiary of this decision: he no longer needs to pay for the freed-
up resource input. Other resource users also become beneficiaries,
for a pew source of the scarce resource has now been made available.
The consumer is also a beneficiary: competition among producers
will tend to reduce the cost of final output and will also eventually
reduce the consumer price of the. product.

We can examine this topic from another angle. If the law. of
diminishing returns were not true, then by taking a fixed supply of
one factor of production and adding to it a complementary factor,
eventually the proportion of the first (fixed) factor in the production
mix would approach (though never quite reach) zero percent. After
you reach the point of diminishing returns, the original (fixed) factor
of production is increasingly “swamped” by the second (variable) fac-
tor, (Both factors are actuaily variable in practice; this is only an il-
lustration.) To use an analogy from mathematics, the “swamped”
factor of production is like a fraction with an increasingly large
denominator, and therefore a decreasing value. One-to-two becomes
one-to-three, and then downward to, say, one-to-five billion, One
five billionth.is obviously a lot smaller than one half. The value of
the fraction approaches zero as a limit. By fixing the numerator and
increasing the denominator, the “percentage contribution” of the
numerator to the “value” of the fractional number is reduced.’

To return to the example of the 800-acre farm, as we add. more
and more men, we will eventually overwhelm. the productivity of the
resource. factor which is in fixed supply, namely, land. Jf by adding

7. The analogy is imperfect because there are no “optimal” fractions. You never
get increasing returns by adding to the denominator. The analogy applies only to the
case where the optimum production mix has been passed.
