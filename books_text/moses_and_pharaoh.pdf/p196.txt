178 MOSES AND PHARAOH

for at least a century, and possibly four—about twice the duration of
Israel’s stay in Egypt.

Pharaoh knew precisely what his surrender implied. The gods of
Egypt had been decisively defeated. Pharaoh had been driven to
capitulate completely to the demands of Moses and Moses’ God.
This God had demanded that Pharaoh allow the whole nation of
Israel to journey three days in order to sacrifice to Him. Now God
had been able to extract His demands from Pharaoh, Pharaoh was
implicitly admitting that the Egyptian theology was a myth, that
there is no continuity of being between man and God, that there is a
God so great and so powerful that He can extract His every demand
from mighty Egypt, the center of the earth. Here was a God unlike
any ever encountered by Pharaoh or his predecessors.

Pharaoh also understood what Egypt’s sin against the Hebrews
had been. They had enslaved Israel, breaking their treaty with Israel
and Israel’s God. They had treated Israel as a concubine, a slave
wife. They had stolen Israel’s dowry, the land of Goshen. There was
restitution to pay, Pharaoh, however, did not want to pay all that he
owed. He wanted one last admission on the part of the Hebrews that
he was not really guilty. He wanted Moses to bless him.

How could this man have hoped for one moment that the God of
Israel might bless him? How could he have imagined that God
would regard him as anything but a lawless rebel? Was Pharaoh at
last asking for mercy? Was he at last humbling himself before the
God of the Israelites? Was his request for a blessing a sign of his
repentance? The answer is unconditionally ne to all these questions.
What was Pharaoh really asking for? He was asking for God's seal of
approval on his actions as a monarch, the master of Egypt. He was
asking for God’s sanction as a /awful former master of Israel. He was
‘trying to justify his tyranny and his continual lying, He was trying to
cover himself with the protecting law of God, but without humbling
himself before that law. He was trying to get God to acknowledge
publicly that Pharaoh’s acts of charity—which were in fact tribute
payments extracted by God’s awesome power — entitled him to Ged’s
protection.

The Year of Release

The law of God respecting Hebrew slaves placed specific re-
quirements on the Hebrew masters. Pharaoh must have understood
the basic principle of lawful slave ownership. “And if thy brother, an
