284 WESTMINSTER’S CONFESSION

He lies. Let us not try to put a good face on it. He lies. He lies
for Jesus. He lies for the kingdom of God. He lies for the sake
of revenge. He deliberately misleads the reader. I become his
visible target, but his reader is the real victim.

The Gold Standard

He identifies my Biblical Blueprints Series as an example of
biblicism. He specifically cites my book, Honest Money, and asks:
“Why, for example, should the United States return to the gold
standard? Because careful and prudent economic analysis
suggests it will produce a healthier economy? No, because
Deuteronomy 25:15 says that you shall have just weights and
measures.”**

Two comments are in order. First, why should I trust mod-
em economists more than I trust Deuteronomy 25:15? (Why
should anyone in his right mind trust modern economists, with
or without Deuteronomy 25:15?) Second, Honest Money specifi-
cally teaches that the United States should not return to the
gold standard. I wrote the book in order to deny the legitima-
cy, biblically, of the traditional gold standard. Yet in order to
ridicule me and my “biblicism,” Muether deliberately twists
what I wrote. (I am assuming here that he read the book. If he
did not read it, then he is not a liar; he is merely a phony.) In
the chapter called “A Biblical Monctary System,” under the
subhead called, “The Gold Standard,” I wrote this:

For the State to say that only gold should circulate is a re-
striction on individual liberty. For the State to say that only gold
is legal tender (a legally mandatory form of money) is also a
violation of individual liberty. Let people decide how and what
they use as money, provided that no fractional reserves are
involved.

44, Ibid., p. 255.
