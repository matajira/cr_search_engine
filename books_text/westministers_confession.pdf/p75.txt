Calvin’s Divided Judicial Legacy 51

worn on, the debates over eschatology have intensified. The
latest is the debate over the New World Order.

The Sovereignty of God

Calvin Icft no doubt regarding his view of the sovereignty of
God. He was an Augustinian. (So was Martin Luther, but his
successors, beginning almost immediately with Philip Melanch-
thon, returned to a far more Pelagian outlook.) Calvin left few
doubts about hierarchy, either. The Church is Presbyterian in
structure, and the State is to protect the Church. “Yet civil
government has as its appointed end, so long as we live among
men, to cherish and protect the outward worship of God, to
defend sound doctrine of piety and the position of the church,
to adjust our life to the society of men, to form our social be-
havior to civil righteousness, to reconcile us with one another,
and to promote general peace and tranquility.”® He affirmed
the ideal of the Christian State.'® (This outlook has been a
major embarrassment for his post-1788 American followers.)
For Calvin, as for Aquinas, Christendom included the State and
all other institutions. In the post-1788 era, the very concept of
Christendom has become anathema to almost all Protestants,
indicating that the Deists, the Unitarians, and the post-Miinster
Anabaptists have triumphed over original Calvinism specifically
and pre-Newtonian European thought and culture generally.
But in the era of the Reformation, Calvin’s viewpoint was not
revolutionary. Indeed, any departure from such a view would
have been regarded as revolutionary, Not until the Civil War

8. Martin Luther, The Bondage of the Will (1524). This has been reprinted in the
Library of Christian Classics, volume XVII: Luther and Erasmus: Free Will and Salva-
tion (Philadelphia: Westminster Press, 1969).

9. John Calvin, Institutes of the Christian Religion (1559), translated by Ford Lewis
Battles (Philadelphia: Westminster Press, 1960}, Book IV, Chapter XX, Section 2.

10. Ibid., IV:XX:14,

11. Excepting only the Covenanters: The Reformed Presbyterian Church of
North America,
