The Question of Law 127

a bright and shining future.”” Klaas Schilder was to learn
better a generation later. This subsidy destroyed the theological
moorings of the Free University and then Kuyper’s denomina-
tion. Kuyper never sensed the inescapable danger.

This transformation was guaranteed from 1905. The money
and the monopoly grant of power (degree-granting) inevitably
corrupted the Free University. Here is the great and predict-
able irony: “Kuyper’s ultimate goal was none other than the
liberation of higher education from the state and its return to
the people.”“ The result, of course, was exactly the opposite.
The source of the funding — in this case, coercively confiscated tax
revenues — always determines the standards and character of the
recipient institutions. The non-public universities had to meet
the standards of the State. These standards were supposedly
religiously neutral; in fact, nothing is religiously neutral. What
Kuyper should have demanded was the removal of all State
sanctions from education: money, examinations, and supervi-
sion of academic degrees. But such a view is radical even today,
despite a century of public school tyranny and declining stan-
dards, let alone in 1905. Instead, he sought State subsidies.

This infusion of money and monopoly degree-granting
authority led, decade by decade, to the destruction of Christian
education in the Netherlands. The Dutch Catholics by the
1960's had become the Roman Church's most flaming national
pocket of radicals,’ while the confession-affirming Calvinists
have continued to shrink in influence, going along with Dutch
socialism with only mild and half-hearted protests. Kuyper had
supported social welfare legislation from the beginning.”

13, Frank Vanden Berg, Abraham Kuyper: A Biography (St. Catherines, Ontario:
Paideia Press, [1960] 1978), p. 219.

14, Ibid., p. 214.
15, Ibid., p. 216.

16. See, for example, A New Catechism: Catholic Faith for Adults (New York:
Herder & Herder, 1967), put out by the Bishops of the Netherlands.

17. Vanden Berg, Kuyper, p. 191.
