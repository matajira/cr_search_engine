2

CALVIN’S DIVIDED JUDICIAL LEGACY

If we distinguish between a theoretical and strict “Theonomic” view-
point on the one hand, and more practical and loose “theonomic” view-
point on the other, we might say that Calvin was not a Theonomist but
a theonomist. That is, an examination of Calvin’s theoretical writings on
the judicial aspects of the Mosaic law will reveat that he believed that
they were given to Israel in a rather unique fashion, and are not bind-
ing on modern civil governments. Yet, an examination of Calvin’s
practical writings and sermons (such as the sermons on Deuteronomy)
will reveal that he used the Mosaic law, including its judicial aspects, as
the foundation for social, political, and legal wisdom, and generally
favored imitating the Mosaic laws in the modern world.

James B. Jordan (1990)'

John Calvin was a transitional figure. (Adam and Jesus
Christ were the ultimate transitional figures; everyone else is
either a mini-transitional figure or a micro-transitional figure.)
He inherited a great deal of philosophical baggage from the

past. He scrapped only part of it. Whenever he relied on the

Bible or Augustine, he was usually secure from misinterpreta-

1. James B. Jordan, “Editor's Introduction,” John Calvin, The Covenant Enforced:
Sermons on Deuteronomy 27 and 28 (Tyler, Texas: Institute for Christian Economics,

1990), pp. xxibcuxiii.
