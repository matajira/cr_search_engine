108 WESTMINSTER’S CONFESSION

ation. They are too immersed in the common-ground theolo-
gies of the American civil religion and Abraham Kuyper’s com-
mon grace theory.’

Constantine called together the Council of Nicaea. It is this
appeal back to Constantine’s precedent that alienates the mod-
ern Christian defenders of religious and political pluralism.
They see clearly that the intellectual conflict within the Church
over the legitimacy and possibility of Christendom has always
been between the Constantinians and the pietists, and they
have self-consciously sided with the pietists. The idea of a
Christian ruler in an explicitly and legally Christian society is
morally repugnant to them. They prefer to believe in religious
neutrality, natural law, and a civil government that imposes
sanctions other than those specified in the Bible. Those colonial
Presbyterians who shared this outlook revised the Westminster
Confession of Faith to remove this one minuscule trace in the
creed of the subordination of the civil magistrate to God, for if the
civil magistrate is not entitled to call a Church assembly for
counsel, then he surely is not required by God to listen to any
pronouncements by such an assembly. If there is absolutely no
legal connection between Church and State, then there is no
judicial obligation for a civil magistrate to listen to a Church
council. What appears to be an intrusion by the State into
Church affairs — calling a Church assembly for counsel - is in
fact a legal acknowledgment that the State must consider the
judicial pronouncements of the Church: not automatic subordi-
nation, but at least co-authority. The political piuralists recog-
nize this, and have therefore denied the right of the Church to
tell the State what the Bible requires of its magistrates.

Then who should tell magistrates what is required of them?
“The sovereign people, the creators and sole enforcers of the civil
covenant!” Who, then, is the god of such a national covenant?

16, Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987).
