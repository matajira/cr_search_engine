324 WESTMINSTER'S CONFESSION

The point is that there are hundreds of such details to be
sorted out and applied to the contemporary situation. Recon-
structionism does not actually provide the clear, simple, uncon-
testably “biblical” solutions to ethical questions that it pretends
to, and that are so attractive to many conservative Christians.
Reconstructed society would appear to require a second encyclo-
pedic Talmud, and to foster hordes of “scribes” with competing
judgments, in a society of people who are locked on the law’s
fine points rather than living by its spirit (p. 23).

Ah, yes: “living by the spirit.” A noble goal, indeed. Precisely
the goal of the Anabaptist revolutionaries who tore Europe
apart in Luther’s day.*

To see more clearly where Mr. Clapp is headed, try this
experiment. Rather than thinking “Reconstructed society” to
yourself, substitute “Constitutional law and republican guaran-
tees of liberty.” There is no doubt about it, such a system of
civil government involves complexity. Do you see a place for
legislatures filled with people who debate details carefully
before they agree to any policy? Do you see a court system in
which judges often disagree, and which takes time, debate,
thought, and contending lawyers to sort out the truth? Do you
see voters who disagree? Do you see, in short, a system of
political and judicial liberty? Isn’t this the essence of constitu-
Uionalism? But would Mr. Clapp impress his readers by coming
out forthrightly against constitutional law?

The only practical alternative to judicial complexity in histo-
ry that comes to my mind is the tyranny of arbitrary law, which
in our day was best incarnated by Josef Stalin, who, when he
was awakened by the barking of a blind man’s dog one eve-
ning, ordered the dog shot. Also its owner.’ No muss, no fuss,
no lawyers (“scribes”), No “Talmudic” debates over details,

4. Igor Shafarevich, The Socialist Phenomenon (New York: Harper & Row, 1980},
ch, 2,

5, Nikolai Tolstoy, Stalin's Secret War (Holt, Rinehart & Winston, 1981), p. 38.
