348 WESTMINSTER’S CONFESSION

and the name of the Church’s senior pastor. If he is not a
member of a local Church, I would cut off all contributions.
(This is not the same thing as refusing to buy services or goods
from a ministry.) Also, these ministries should make it clear
that they do not seek people’s tithe money (the first ten per-
cent); they should be supported exclusively by individual offer-
ings above the tithe and from contributions from churches. These are
measures to be taken by donors. But due diligence by donors
is not sufficient to change the system. Until the churches start
preaching covenant theology and enforcing it, there will be no
fundamental change.

Time is now running out on independent parachurch minis-
tries. This is why the 1990’s will be the window of opportunity
for the churches, meaning the window of increased responsibility.
The churches are now faced with two major responsibilities: 1)
replacing the parachurch ministries as the latter decline, which
the churches are presently unwilling to do; or 2) bringing both
positive and negative sanctions against the parachurch minis-
tries, which they are equally unwilling to do. Without covenant
theology, pastors flee responsibility like the plague. Preaching
Christian impotence as a way of life, they produce it.

2. Mine is the Good Shepherd Reformed Episcopal Church, Tyler, Texas,
pastored by Ray Sutton.
