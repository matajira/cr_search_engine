Calvin's Millennial Confession 353

speaks here only of the disciples of Christ. He shows the fruit
of his doctrine, that wherever it strikes a living root, it brings
forth fruit: but the doctrine of the Gospel strikes roots hardly
in one out of a hundred.” So, in order to have a universal
fulfilling of this prophecy, there must be a great multiplication
of the disciples of Christ. This wll take place in the future. “It
seems that the Prophet does not describe here the state of the
church for a time, but shows what would be the kingdom of
Christ to the end.”?

A Day of Small Beginnings

These and many other passages reveal Calvin’s postmillen-
nialism. So far, so good. But in his comments on the parallel
prophecy in Isaiah 2:4, his pessimism intruded. This condition
of ploughshares will come only when “the kingly power of
Christ is acknowledged. . . . But since we are still widely distant
from the perfection of that peaceful reign, we must always
think of making progress; and it is excessive folly not to consid-
er that the kingdom of Christ here is only beginning.” This still
is compatible with postmillennialism: a long time-frame. But
then he added: “The fulfilment of this prophecy, therefore, in
its full extent, must not be looked for on earth.” To its full
extent, yes; but what about in between? He did not say. He
ended his comments with this: “It is enough, if we experience
the beginning, and if, being reconciled to God through Christ,
we cultivate mutual friendship, and abstain from doing harm
to anyone.”

If the “we” here meant those living in his day, then there is
nothing necessarily amillennial implied by the passage. But by
focusing people's attention to the necessarily incomplete fulfil-

12, Calvin, Commentaries on the Twelve Minor Prophets (Graid Rapids, Michigan:
Baker Book House, [1557] 1979), III, p. 265: Micah 4:3.

13. Calvin, Commentary on the Book of the Prophet Isaiah (Grand Rapids, Michigan:
Baker Book House, [1550] 1979), J, p. 102.
