The Question of Millennialism 177

is saying, the power of Christ’s resurrection is realized in the
sufferings of the believer; sharing in Christ’s sufferings is the
way the church manifests his resurrection-power.”" Again, “to
‘know’/experience Christ is to experience the power of his
resurrection and that, in turn, is to experience the fellowship
of his sufferings — a total reality that can then be summed up as
conformity to Christ’s death.”*

Question: isn’t to “‘know’/experience Christ” to experience
also the victory of His bodily resurrection and His bodily ascen-
sion to the right hand of God? Not in Gaffin’s theology. He
never even mentions this possibility. The Christian Reconstruc-
tionist and the traditional postmillennialist answer: the total
reality of Christian living is a great deal more than “conformity
to Christ’s death.” The total reality of Christian living is our
comprehensive, progressive conformity in history to the total
historical reality of Christ’s death, resurrection, and ascension. Amil-
lennial theologians publicly ignore the existence of such an
interpretation. We can hardly blame them, given the limits of
their eschatology and the even greater limits of its appeal.

Prior to World War I, the great amillennial Dutch theolo-
gian Klaas Schilder wrote a trilogy: Christ in His Suffering, Christ
on Trial, and Christ Crucified. He needed three more volumes:
Christ in the Grave, Christ Resurrected, and Christ Ascended. But
there is not much to say about Christ in the grave, and amil-
lennialists get very nervous discussing Christ resurrected, let
alone Christ ascended. They interpret the history of the
Church in terms of Schilder’s three volumes. They do not
think culturally and socially except in these terms. The Dutch
in Kuyper’s day and Schilder’s day tried to design a Christian
culture, but without Old Testament law. World War II and its
aftermath ended all such attempts. Schilder’s trilogy was resur-

15. Hid., p. 213.
16. Idem.
