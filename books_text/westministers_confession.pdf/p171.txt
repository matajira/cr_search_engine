The Question of Law 147

Bryan, and Wilson came to the American electorate as both
Presbyterians and Democrats. How could this be? Each man
supposedly tied his ethics explicitly to the Bible; each promoted
his views in the name of Christianity. They did not agree on
much of anything with respect to social ethics. Why such confu-
sion? Because they did not say exactly where they had derived
their social ethics. One thing is clear: none of them appealed
directly to biblical texts, both Old and New Testaments.
‘The theonomists do. This is their offense.
