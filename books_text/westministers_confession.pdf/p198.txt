174 WESTMINSTER’S CONFESSION

church’s existence is (to be) “suffering with Christ”; nothing, the
New Testament teaches, is more basic to its identity than that.”

He cites IT Corinthians 4:7: “But we have this treasure in
earthen vessels, that the excellency of the power may be of
God, and not of us.” This imagery of man as a vessel is familiar
in Scripture. Paul uses it in Romans 9:

Nay but, O man, who art thou that repliest against God?
Shall the thing formed say to him that formed it, Why hast thou
made me thus? Hath not the potter power over the clay, of the
same lump to make one vessel unto honour, and another unto
dishonour? What if God, willing to shew his wrath, and to make
his power known, endured with much longsuffering the vessels
of wrath fitted to destruction: And that he might make known
the riches of his glory on the vessels of mercy, which he had
afore prepared unto glory, Even us, whom he hath called, not of
the Jews only, but also of the Gentiles? (Rom. 9:20-24).

The question is not whether we are vessels. The question is:
Which vessels get progressively smashed by God in history, the vessels of
wrath or the vessels of glory? The answer to this question is bibli-
cally clear, and nowhere is it clearer than in Psalm 2, one of
the most disconcerting Bible passages for the amillennialist:

Why do the heathen rage, and the people imagine a vain
thing? The kings of the earth set themselves, and the rulers take
counsel together, against the Lorp, and against his anointed,
saying, Let us break their bands asunder, and cast away their
cords from us. He that sitreth in the heavens shall laugh: the
Lord shall have them in derision. Then shall he speak unto
them in his wrath, and vex them in his sore displeasure. Yet

LL. Ibid., pp. 210-11, He cites his essay, “The Usefulness of the Cross,” Westmins-
ter Theological Journal, XLI (1978-79), pp. 228-46, He could also have cited Edmund
Clowney’s essay, which also defended suffering as an idea for Christians: “The
Politics of the Kingdom,” ibid., XLI (Spring 1979), p. 303.
