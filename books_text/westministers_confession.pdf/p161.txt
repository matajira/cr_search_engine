The Question of Law 137

there was nothing any postmillennialist could do about it.
Other than writing from off-campus, there still isn’t.

What will the Church face in the future, according to Kui-
per? Cultural defeat. He said emphatically that “when the
present dual process of the evangelization of non-Christian
peoples and the development of the forces of evil shall have
run its course, the victory to all appearances will be on the side
of the price of darkness.” He could not have made it any
plainer. Only a radical discontinuity from beyond history that
ends history will at last — at the last — bring victory. “However,
with catastrophic suddenness Christ will appear in person,
destroy Satan and his domain, and upon its ruins perfect his
own -everlasting kingdom.”” Then he admitted the obvious,
which he specifically identified as obvious: “Those who take this
view are obviously much less optimistic about the immediate
results of the presentation of the social teaching of the gospel
than are their postmillenarian brethren.” Notice his use of
the word immediate. By this word, he really means in all of
histery, but he did not have the courage to say this plainly. He
then went on to defend the not-so-obvious regarding amillen-
nial preachers of social salvation: “But let no one think that
they are for that reason less zealous for their task.”**

Kuiper said that the preacher must “deal with social prob-
lems in the pulpit because it is his duty to preach the whole
Word.”** Fine; this brings us to the question: What does the
Bible teach about society? Here, he grew vague. He was not
ready to affirm in Christ’s name the late nincteenth-century
political liberalism that Machen had espoused. After all, Kuiper
was Kuyperian. But he did insist that Christian preachers must

42.. Kuiper, p. 30.
43. Idem,

44, Idem.

45. Idem.

46. Ibid, p. $1.
