This book is dedicated to the most accomplished
instructor I had at Westminster Seminary,

Norman Shepherd

who combined Machen’s eschatological optimism,
Van Til's presuppositional apologetic, and Murray’s
precise theological language. He was a loyal de-
fender of Westminster’s original confession.
