Abusing the Past 243

Sinclair B. Ferguson

Dr. Ferguson’s title asks: “An Assembly of Theonomists? The
Teaching of the Westminster Divines on the Law of God.” His
answer gives away half the store to the theonomists, This is
why I have few complaints. He is faithful to the diversity of
judicial opinion at this remarkable committee of the saints. It is
this diversity which the opponents of theonomy at the presbyte-
rial level have steadfastly refused to acknowledge. It is not
Ferguson who abuses history; it is theonomy’s Presbyterian
opponents who will now have to answer to Ferguson. His essay,
like Moises Silva’s, is not what I would call critical. Crucial,
possibly, but not critical.

No, he says, they were not theonomists. Of course, some of
them did believe in executing people for the following crimes:
adultery, witchcraft, and blasphemy.* George Gillespie, in
Aaron’s Rod Blossoming, a book dedicated to the Assembly, did
write:

I know some divines hold, that the Judiciall Law of Moses, so
far as concerneth the punishments of sins against the moral
Law, Idolatry, blasphemy, Sabbath-breaking, adultery, theft, &c,
ought to be a rule to the Christian magistrate; and, for my part,
I wish more respect were had to it, and that it were more con-
sulted with.

Samuel Bolton saw the moral law of Christ as the extension
of Mosaic law: “Acknowledge the moral law as a rule of obedi-
ence and Christian walking, and there will be no falling out,
whether you take it as promulgated by Moses, or as handed to
you and renewed by Christ.” Here is a concept that is al-
most exactly what we in the “Tyler group” regard as ours: the

25. Ferguson, p. 340.
26. Ibid., p. 342.
27. Ibid, p. 343.
