150 WESTMINSTER’S CONFESSION

the public situation as we can cffect, whether in society or in
state, presupposes this undifferentiated stage of development.’

This was Van Til’s amillennialism at work.*

Mystery and Irresponsibility

There is no better way for a Christian to proclaim his own
personal and cultural irresponsibility in history than to pro-
claim the mystery of God’s specific revelation. Mystery is defin-
ed as man’s permanent ignorance. Mystery cannot be over-
come. It does exist, of course: “The secret things belong unto
the Lorp God: but those things which are revealed belong unto
us and to our children for ever, that we may do all the words
of this law” (Deut. 29:29). Notice that mystery and biblical law
are contrasted. The impenetrable mysteries of God are not to
discourage us, because we have His revealed law. But in deny-
ing the legitimacy of biblical law in New Testament times,
modern antinomians are implicitly (and sometimes explicitly)
substituting mystery for biblical law. This can lead to mysticism:
personal withdrawal into the interior recesses of one’s incom-
municable consciousness (escape religion). It can also lead to
antinomian Pentecostalism: direct authoritative messages from
God lo a few uniquely gifted leaders (spokesmen in history:
point two of the biblical covenant) — messages that replace
God’s law, since God’s law is no longer binding. That this
(power religion) leads again and again to ecclesiastical tyranny
should surprise no one. In either case, there is an increase of
personal irresponsibility.

To classify as one of “the secret things of God” the idea of
God’s predictable sanctions in history requires a leap of faith.

3. Van Ti, Common Grace (1947), in Common Grace and the Gospel (Nutley, New
Jersey: Presbyterian & Reformed, 1972), p. 85.

4. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 3.
