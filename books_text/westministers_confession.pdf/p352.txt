328 WESTMINSTER’S CONFESSION

summary of the basic theology of Reconstructionism. I know
this, because he sent a copy to his former student, Pastor Ray
Sutton. Pastor Sutton showed it to me.

I wrote a letter to Dr. Hannah telling him that CT would
reject the piece because it did not discuss the splits and in-
fighting of the Reconstructionist movement. A few weeks later,
CT rejected it. He told Pastor Sutton that the editor had told
him that they had hoped for an essay that went into the details
about the Reconstructionists’ in-fighting. Dr. Hannah refused
to rewrite it.

Christianity Today’s editors waited three years for someone
with Dr, Hannah’s reputation and academic credentials to
submit a hatchet piece. They waited in vain. So they finally
assigned the job to Rodney Clapp. (Just for the record, the
executive editor’s name is Muck. You probably won’t believe
me, but that really is his name: Terry GC. Muck. Muck and
Clapp — a Reconstructionist’s dream come true.)

Who Is Rodney Clapp?

In 1983, he co-authored the infamous article for Christianity
Today, “If not Abortion, What Then?” (May 20, 1983), and
subtitled, “Why pro-life rhetoric is not enough.” It was this
article that earned Franky Schaeffer’s well-deserved wrath:
“Christianity Today, by discussing abortion in terms of its impact
on ‘poor people,’ once again was up to the same old evangeli-
cal trick of trying to appear fashionable while half-heartedly
stating the Christian position. Having their cake and eating it
too. Fashionable, because poverty is in while abortion is out.
Fashionable, because trying to hector the prolife movement for
supposedly too much rhetoric and not enough compassionate
action, they became a shill for the secular media version of the
prolife movement and its activities.”*

8, Franky Schaeffer, Bad News for Modern Man (Westchester, Hlinois: Grossway
Books, 1984), p. 54.
