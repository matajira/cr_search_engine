210 WESTMINSTER’S CONFESSION

are the intrusionists’ exegetical goods? All I have seen so far
from intrusionists is a systematic rejection of God’s law.
Panglossianism is not the solution, The solution is to see
what Westminster has done in the case of Bahnsen and all
other theonomists, Panglossianism has not governed the hiring
practices of the Clowney and post-Clowney era of Westminster
Seminary. Don’t take my word for it; ask Norman Shepherd.

Dan G. McCartney

Dr. McCartney, one of the Gordon-Conwell crew, fails to cite
a single work by any theonomist in his article. In fact, he cites
only two books, both showing that the term prophet applies to
the whole of the Old Testament. Wow!

He sees the issue: Old Testament law and its specified penal
sanctions. He replies in an entire section that “Law Is
Christological and Covenanul.” Very good. Unfortunately, he
fails to define “covenantal” - a traditional game of covenant
theologians that stretches back about 400 years. Instead, he
shifts ground and says that the law is Christocentric. The Old
Covenant’s law was not made for the nations around Israel, he
says.*® This means that he needs to refute Bahnsen. (It also
means that he needs to refute Jonah.) He does not even men-
tion Bahnsen. This is neutral scholarship, apparently, which
means never having to refer to the specific arguments of your targeted
victims. To refer to them by name would, no doubt, be in poor
taste. Worse; one’s students might actually discover where the
victims’ arguments are developed in depth, and thereby reject
one's own unsubstantiated asscrtions.

He takes a unique — and necessary - approach, He systemat-
ically ignores the theonomists’ case for the civil use of the Old
Testament laws. He includes a section, “Historical or Covenan-

48. McCartney, “The New Testament Use of the Pentateuch: Implications for
the Theonomic Movement,” ibid., p. 129.

49. Ttid., p. 131.
