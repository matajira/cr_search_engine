Conclusion 305

faculty: How to break from Kline’s near-dispensationalism
without embracing theonomy? How to survive in the middle
and still be called Reformed? There was a time when such a
declaration was intellectually acceptable in Reformed circles,
but not after Van Til destroyed the common sense rationalism
of the old Princeton apologetic system. Only those Reformed
theologians who reject Van Til can safely reject both Kline and
theonomy, but only by appealing to natural law theory. Escap-
ing Kline and Bahnsen, they are trapped either by Aquinas or
Newton. But how can you embrace Aquinas without also em-
bracing Rome? And how can you embrace Newton without also
embracing Darwin, Heisenberg, and Mandelbrot?”

There is but one remaining alternative: mysticism. This is not
a path open to Reformed theologians, since Reformed theology
is explicitly judicial. In short, how can you embrace mysticism
and not embrace either the individualism of John Wimber’s
signs and wonders charismatic movement or else Eastern Or-
thodoxy’s communalism? And how can you build an explicitly
Christian society on these non-judicial theologies?

Intrusionism Intruding

When push has come to shove exegetically, the Westminster
faculty has always deferred to Kline. Kline, after all, is on the
Westminster Seminary payroll; Bahnsen never has been. The
seminary has already made its public decision. The overwhelm-
ing majority of its members prefer the cthical and judicial
discontinuity of Kline’s intrusion thesis to the continuity of
theonomy. His intrusion thesis has the hallmarks of dispensa-
tionalism, and it no doubt makes them feel uncomfortable, but
it does not make them academic outcasts: spiritual wanderers
crying in the fully accredited wilderness. If I am wrong, then
all sixteen contributors to Theonomy: A Reformed Critique will no

11. On Benoit Mandelbrot’s theory of chaos, see James Gleik, Chaos: Making a
New Science (New York: Viking, 1987).
