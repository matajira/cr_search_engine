302 WESTMINSTER’S CONFESSION

People” will allow: a majority of the Supreme Court. Supreme
Court Chief Justice Warren Burger drove home this point in a
televised interview with Bill Moyers:

CHIEF JUSTICE BURGER: Constitutional cases — constitutional
jurisprudence is open to the Court to change its position, m
view of - of changing conditions. And it has donc so.

MOYERS: And what does it take for the Court to reverse itself?
CHIEF JUSTICE BURGER: Five votes.®

So, we are still waiting. Our non-Vantilian critics have offer-
ed natural law theory as their substitute for theonomy, but
“natural law theory” is just a phrase. The question is: What is
the actual content of this theory? What, precisely, do we learn
from this theory about how we are supposed to live? With
respect to actual content, our non-Vantilian critics are still
attempting the impossible: beating something with nothing.

A Challenge to Theonomy in Van Til’s Name

Theonomy: A Reformed Critique is supposedly written from the
point of view of Van Til, At least, no one in the symposium
explicitly broke with Van Til. The conventional reader who
knows anything about Westminster will suppose that if any
apologetic position is represented by this symposium, it must
be Van Til’s. The editors stated in their Preface that the contri-
butors were all committed to “a defense of the faith opposed to
the autonomy of human reason. . . .”” If this is not a reference
to Van Til’s Defense of the Faith and his multi-volume In Defense
of Biblical Christianity, then the symposium’s readers are being
misled.

6. “The Burger Years,” CBS News Special (July 9, 1986), p. 6.
7. Theonomy: A Reformed Critique, p. 10.
