Sic et Non: Judicial Agnosticism 199

During much of the Clowney era, Knudsen was either the
editor or the managing editor of the Westminster Theological
Journal, Like Clowney, he did his work quietly to restructure
Westminster’s confession.”

Tremper Longman ITI*

Chapter 2 of the book begins with the presupposition that
Chapter 1 implicitly rejects: the connection between the case
laws and the Ten Commandments. Writes Dr. Longman:
“However, what is the Mosaic case law but the application of
the Ten Commandments to the nation of Israel? None of the
civil or moral laws is independent of the Ten Commandments;
they are all summarized in them. The case laws are specifica-
tions of the general principles of the Ten Commandments.”
One would be hard-pressed to find a better statement of the
theonomic position on the Decalogue.

What is the catch? Why is this essay in a book critical of
theonomy? Because Dr. Longman finds that the application of
biblical law to specific cases is “very difficult.”** That's it?
That, basically, is it. “In reading the standard works of theon-
omy, one can easily get the impression that Old Testament laws
are simple and clear-cut. We have already scen evidence to
dispute this, at least from the perspective of the modern inter-
preter.”* (Note the word “simple.” We shall see it again in
this connection.)

There is little indication in his foomotes that he has read the
standard works of theonomy. (Neither have most of his col-

21. Years under Knudsen as editor or managing editor: 1968-70, 1971, 1975-
78, 1980. Godfrey's era: 1978-81. Silva took over in 1982, the year Clowney retired.
The journal became far less hostile to our books after that.

22. What a great name!

23, Tremper Longman III, “God’s Law and Mosaic Punishments Today,” ibid.,
p- 46.

24. Ibid., p. 45.

25, Ibid., p. 52.
