152 WESTMINSTER’S CONFESSION

when thy herds and thy flocks multiply, and thy silver and thy
gold is multiplied, and ail that thou hast is multiplied; Then
thine heart be lifted.up, and thou forget the Lorp thy God,
which brought thee forth out of the land of Egypt, from the
house of bondage; Who led thee through that great and terrible
wilderness, wherein were fiery serpents, and scorpions, and
drought, where there was no water; who brought thee forth
water out of the rock of flint; Who fed thee in the wilderness
with manna, which thy fathers knew not, chat he might humble
thee, and that he might prove thee, to do thee good at thy latter
end; And thou say in thine heart, My power and the might of
mine hand hath gotten me this wealth. But thou shalt remember
the Lorp thy God: for if is he that giveth thee power to get wealth,
that he may establish his covenant which he sware unto thy fathers, as it
is this day. And it shall be, if thou do at all forget the Lorp thy
God, and walk after other gods, and serve them, and worship
ther, I testify against you this day that ye shall surely perish. As
the nations which the Lorp destroyeth before your face, so shall
ye perish; because ye would not be obedient unto the voice of
the Lorp your God (Deut. 8:11-20). (emphasis added)

The vast majority of Bible-aflirming theologians today as-
sume that there has been a radical New Covenant break from
Old Covenant citizenship.* They assume (though seldom, if
ever, attempt to prove exegetically) that the Old Covenant’s
close links between the social rewards of covenant-keeping and
the social cursings of covenant-breaking are no longer opera-
tive in the New Covenant order. More than this: there are sup-
posedly no predictable covenantal sanctions in New Covenant history,
meaning no sanctions applied by God in terms of biblical law. Kline
and his disciples argue that God does not bring predictable
covenantal sanctions against a social order at all, i-e., that the
historical sanctions in the New Covenant era are random from

5. On Old Covenant citizenship, see North, Political Polytheism, ch. 2. The
fundamental idea of citizenship is the legal authority to bring negative sanctions in
the civil realm. The stranger-foreigner did not possess this right in ancient Israel.
