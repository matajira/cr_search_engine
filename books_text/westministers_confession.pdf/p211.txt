The Question of Millennialism 187

progressive kingdom development in history toward the present
triumphant condition of the Church in heaven. While our
citizenship is in heaven, this heavenly “passport” progressively
entitles us only to the kinds of rights and benefits given to
someone in Iraq who holds an Israeli passport. (This defeatist
outlook on Church history is equally true of premillennialism.)
The result is predictable: the Church Militant has become in
our day the Church Wimpetant.

Some critics of Reconstructionism resent our calling amillen-
nialism pessimistic. Yet the system is intensely pessimistic.
There is no developed system called “optimistic amillennial-
ism.” There can be none, Occasionally, an amillennialist admits
this fact in print. In a review of Kenneth Gentry’s book, Before
Jerusalem Fell, Rev. Stuart Jones, a Westminster graduate, forth-
rightly admitted the correctness of our accusation when he
challenged the book’s argument for a pre-A.D. 70 date. While
he did not actually summarize the author’s thesis or provide a
coherent alternative, Rev. Jones did make his position clear:
“This weakens the argument for preterism (present rather then
future fulfillment), and leaves room for pessimism.”* He
learned his millennialism well at Westminster. Pessimism as a
way of Christian thinking must be defended. He is a staunch
defender of pessimism. So are his eschatological peers.

Conclusion

If this is “realized” eschatology, I’d prefer another option.
So would a lot of other Christians, which is why Calvinistic
amillennialism cannot recruit and keep the brighter, more
activist students. Gaffin tclls his disciples that they, like the
Church, have a lifetime of frustration ahead of them. This
comforts the pietists among them, but it drives the activists in

28. New Horizons (Feb, 1991), p. 28. This magazine is published by the Orthodox
Presbyterian Church, The essence of his review can be seen in his statement, “But
I digress.” The whole review is a digression.
