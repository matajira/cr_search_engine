340 WESTMINSTER’S CONFESSION

but not that nasty Old Testament God, who fortunately is in
the far reaches of the universe, living on permanent disability
payments. They think of the Old Testament as God’s Word
(emeritus).

God's law is just too harsh, and even worse, too complicated:
“Reconstructed society would appear to require a second ency-
clopedic Talmud, and to foster hordes of ‘scribes’ with compet-
ing judgments, in a society of people who are locked on the
law’s fine points rather than living by its spirit” (p. 23). Spirit,
man, Spirit: feel that Spirit! Especially in the abortionist’s office.
The scalpel or the hatchet: one is as good as the other, as long
as the job gets done.

Christianity Today long ago sold its birthright for a mess of
trendiness, It’s motto is simple: “trendier than thou.” Rodney
Clapp’s hatchet job on the Christian Reconstructionists is sim-
ply the latest in a long line of frivolous attacks on those Chris-
tians who believe that it is the Bible, rather than the latest essay
on the Op Ed page of the New York Times, that should be the
authoritative guideline for Christian activism.

Abstract law of an abstract God? Has Rodney Clapp ever
read the 119th Psalm? Has his editor ever read it? How long
will such mockery of God and His law go on?

To answer my rhetorical question: it will not go on much
longer. AIDS, if nothing else, wil! bring such humanistic fash-
ionableness to its well-deserved end. The judgment of God has
sneaked up on this civilization from behind.

Are Reconstructionists concerned about what Christianity
Today and Rodney Clapp have done to us in the eyes of today’s
neo-evangelical culture? Hardly. We're far more concerned
about the cultural impact of AIDS than the cultural impact of
Clapp.

aR
