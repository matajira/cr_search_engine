Conclusion 301

Greek philosophy? Protestants who adopt the medieval synthe-
sis must also adopt the medieval concept of Christendom. Medi-
eval theorists did not regard Christendom as baptized Stoicism.
They regarded Christian civilization as a separate entity from
pagan civilization, a unique civilization required by God and
governed by Ilim. Neither the dispensationalists nor the Re-
formed amillennialists have ever presented a comprehensive
statement of what their respective social theories are.*

Third, where are your actual applications of natural law
theory to the whole of culture? They do not exist. Why should
they? The defenders of natural law theory today have no faith
in Christendom. They regard the concept as pre-modern, i.e.,
pre-Newton. They have no faith in the earthly future of the
Church. The costs of working out the outline and the details of
their alternative to biblical law are too high. No one who thinks
that such a task has no institutional payoff in the future is
going to expend a lifetime of effort and money that it necessar-
ily requires to complete it.

Politics

One aspect of society is politics. Here the American critics of
theonomy like to appeal to the U.S, Constitution. This is a
wholly illegitimate appeal. James Madison and his associates
removed any reference to natural law or natural rights from
the Constitution. The appeal to natural rights was Jefferson’s
strategy in the Declaration of Independence. It was abandoned
by the Framers of the Constitution. There is no appeal to high-
er law in the Constitution, only an appeal to the sovereign
agent, “We, the People.”® Over the last two centuries it has
become clear that a mere five people determine what “We, the

4. Gary North, Millennialism and Social Theory (Tyler, Texas: Institute for Chris-
tian Economics, 1990).

5. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 10.
