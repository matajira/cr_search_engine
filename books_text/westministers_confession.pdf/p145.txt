The Question of Law 121

political hierarchy: they were democrats who rejected the idea
of religious or economic restrictions on the right to vote. The
Diggers did not agree with the first three on economic hierar-
chy: they were communists. These issues were not settled dur-
ing Cromwell’s era. Then Charles II returned to the throne,
and the Calvinists were driven out — out of Oxford and Cam-
bridge, out of political office, and out of the pulpits of the land.
Only a few pietistic Calvinists were willing to sign the Act of
Uniformity (1662) in order to retain their positions, men like
William Gurnall, whose Christian in Complete Armour is a gigantic
exercise in pietistic introspection: a manual of personal reform
to the exclusion of social reform. This culturally retreatist out-
look is inherent in all pietism: the denial of any ethically neces-
sary connection between the individual's regeneration and
society’s transformation.

The Pietist-Unitarian Alliance

Pietism is the worldview of both Christian individualism and
the closed small society (e.g., the Amish). Anglo-American
pietism has for over three centuries been in an alliance with
political unitarianism: both proclaim the legitimate autonomy
of politics from the judicial claims of the Bible. The triumph of
Locke's Whig political vision in 1690 (developed during his
stay in the Netherlands in the early 1680's) was an extension of
the unitarian theology and social views of Isaac Newton, not
the Calvinism of Oliver Cromwell.

American Presbyterianism is a product of the Confessional
revision of 1788, That revision was grounded in the worldview
of Newtonianism. A year after the 1788 Synod, in May of 1789,
the General Assembly had Rev. John Witherspoon chair a
committee to prepare an address to the newly elected President
of the United States. The committee drafted a lengthy report
in which it expressed those sentiments that have been passed
down from textbook to textbook. Echoing Washington’s famil-
iar Masonic rhetoric regarding the social utility of religion in
