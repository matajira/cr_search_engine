Sic et Non: Judicial Agnosticism 193

all modern Presbyterian seminaries require. They normally
require a B.A. from an accredited college for all entering stu-
dents. The Presbyterian denominations still require seminary
attendance for ordination. The seminaries, in turn, usually
require their faculty members to have an M.A. or Ph.D. from
a humanist university, or a Th.D. from another seminary, very
often liberal. In short, officially they say “no” to humanism
when trying to raise money from Christian donors, but they
require everyone seeking any position of academic authority on
campus to run the humanist academic gauntlet. This has been
going on in Christian higher education for about eight centu-
ries. When will this tradition end?

In our day, the drift into political and theological liberalism
has gone on, decade after decade, in the Christian colleges.*
The result is visible for all to sec: the triumph of humanism in
the vast majority of the Protestant churches, and the total
isolation of tiny pockets of “Christian scholarship” — scholarship
that self-consciously refuses to challenge the humanist social
order head-on in the name of the Bible. Well did Journey mag-
azine summarize “What’s Wrong With Our Reformed Seminar-
jes”: secularization, the body count problem, the wimp syn-
drome, evangelicalization, the de-emphasis of systematic theol-
ogy, the loss of the Vantilian apologetic, the “practicalization”
of theology, and the loss of the covenant theology perspec-
tive."* The seminaries have steadily drifted toward liberalism.

Mumbling for Jesus

Ivis the offense of the theonomists that we have understood
Van Til’s comprehensive challenge to modern humanism, and
have therefore launched sustained academic attacks on many
fronts. By doing this, we are implicitly asking (and 1 am explic-

13, James Davison Hunter, Evangelicaliom: The Coming Generation (Chicago:
University of Chicago Press, 1987).

14, Journey (Jan-Feb. 1987), pp. 5-6.
