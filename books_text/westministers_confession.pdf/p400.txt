376 WESTMINSTER’S CONFESSION

strengthening, 241
vows, 172
Federalist, 321
Ferguson, Sinclair, 243-45
Fields, W. C., 88
First Baptist Church, xii
final judgment, 185
Finley, M. I., 234
flatline eschatology, 159-60
Fort Contraction, 26
Fowler, Paul, 331
Frame, John
anti-neutrality, 206
Kline, 206
Rushdoony review, 30
sic et non, 202
Van Til &, 35, 242n
framework hypothesis, 39
frameworks, 94-97
franchise, 227
Free University of Amster-
dam, 34, 126-28
freedom, 197

Gaffin, Richard

ascension of Christ, 170,
177-80

Church already victori-
ous, 170

ercedal progress, 171-
72

Eastern Orthodoxy, 183

final judgment, 185-86

interpreting the Bible,
167-68

marriage vows, 172

polemics, 185-86

Shepherd &, 224-25

suffering motif, 173-79

verbiage, 180-82

vessels of wrath, 174-6
Gamaliel, 313, 315
Geisler, Norman, 300

Geneva College, xii (note)
ghetto, xi, 87, 139, 140,
146, 181
Gillespie, George, 243
gnosticism, 114
Codirey, W. Robert
adultery theme, 240-41
Calvin or Van Til?, 240
faking it, 242
ignoring evidence, 238-
39
job audition, 237-38
Westminster's Confes-
sion, 240
gold standard, 283-86
Good Samaritan, 271
goons, 259
Gordon-Conwell, 26, 39,
100, 304
Grant, George, 278-80
Great Awakening, 80n
Great Commission, 114
Gregory VIL, 50, 107
Gurnall, William, 121

Hannah, John, 327-28
Harvard College, xiii, 191-
92
Haskins, Charles Lee
Body of Liberties (1641),
248-49
