164 WESTMINSTER’S CONFESSION

then was given another opportunity to prosecute God’s lawsuit,
which turned out to be successful — unique in the Old Cove-
nant era.

Conclusion

What if I were to come to you and try to recruit you to a
difficult missionary field, namely, the city of Sodom. No, I
don’t mean San Francisco; I mean the original city. I would
then tell you that in fact the whole world is Sodom, or will
progressively become so in the future. You are being asked to
spend your life there, just as Lot spent his days there: vexed. I
assure you that no angels will come to lead you out. There will
be no widespread conversion of the city, cither — not in your
lifetime or in anyone else’s lifetime. There will be no fiery
judgment until the last day, and I refuse to tell you when that
will be. The best news I can tell you about your assignment —
indeed, the only good news — is that your wife will not be
under any risk whatsoever of being turned to salt. I then as-
sure you that this program is called a victory assignment, part of
amissionary program known as realized eschatology. What would
you think of my recruiting strategy? You would probably re-
gard me as either a madman or a Calvinistic seminary profes-
sor.

This is why the present-day theology of our sanctions-deny-
ing Calvinistic seminaries tends to undercut evangelism. If
graduates believe in God’s predestination, they had better not
be taught that God will inevitably bring either negative sanc-
tions against covenant-keepers or at best random sanctions,
compared to how he will deal in history with covenant-break-
ers. The Dutch version of common grace is a negative legacy.
Yet it is this aspect of Van Til’s theology rather than his attack
on natural law theory that is emphasized by his disciples at
Westminster Seminary, not to mention Mr. Muether, who is
now the librarian of Reformed Seminary in Orlando, Florida.
