272 WESTMINSTER’S CONFESSION

that anyone in need is my neighbor, if that also means that I am
in any way legally or morally obligated to help my neighbor. (If
it does not mean this, why bring it up?) He means something
else. He never says what he means.

David Chilton calls this guilt-manipulation.” Rushdoony
calls it the politics of guilt and pity.*® I call ic the politics of
zero conditions: unconditional bankruptcy.

Dr. Keller presents his theology of welfare in the section,
“The Issue of Conditions.” In it, he attacks Ray R. Sutton’s
paper, “The Theology of the Poor.” Sutton argues there that
churches are net required by God to give money to drug ad-
dicts and drunks, A chronic repeater of some offense is also not
entitled to aid. “To give to him unconditionally, sight unseen,
is a waste of God’s money.” The underlying covenant theology
- a covenant thcology with sanctions (point four) - leads Sutton
to this conclusion. This conclusion is precisely what repels
Keller. Sutton replies to Keller in detail in Theonomy: An In-
formed Response. Here I need only to summarize Keller’s posi-
tion.

Keller insists that “When God’s grace first comes to us, it
comes unconditionally, regardless of our merits.”** This is
true, although here is the proper place for Keller’s discussion
of the perseverance of the saints. Keller then makes this leap of
faith: “Ai first, we should show mercy to anyone in need, as we
have opportunity and resources. We should not turn them
away by analyzing them as ‘undeserving’ even if sin is part of
the complex of their poverty.”*> Where is a single Bible refer-
ence? Are we omniscient, the way God is? No. Then why dis-

22. David Chilton, Productive Christians in an Age of Guilt-Manipulators: A Biblical
to Ronald J. Sider (Sth ed.; Tyler, Texas: Institute for Christian Economics,
[1981] 1990).
28. R. J. Rushdoony, Politics of Guilt and Pity (Fairfax, Virginia: Thoburn Press,
[1970] 1978).
24. Keller, p. 276.
25. Did., pp. 276-77.
