262 WESTMINSTER’S CONFESSION

and Bahnsen; he merely notes the existence of the difference.*
So, for that matter, has the entire faculty at Westminster Seminary
since 1929, including Van Til.

Like the 20 million land mines that the Soviets planted in
Afghanistan before they retreated in 1989, Van Til planted
bombs al! over the traditional Reformed landscape; they are
still exploding. They will continue to explode, with or without
Dr, Waltke's tramping around the countryside without a theo-
logical road map. Waltke declares, “Westminster folk applaud
them [theonomists] for basing themselves squarely on Cornel-
ius Van Til’s apologetics.”* Excellent; now all we need the
“Westminster folk” to do is to follow the logic of Van Til’s
apologetics and (1) make a public denial of political pluralism
and (2) make a formal clarification of the general equity clause
of the Westminster Confession. It is to Waltke’s credit that he
does recognize this two-fold challenge to Westminster's faculty,
as well as our challenge to their amillennial eschatology.’

The trouble is, in his section on “Weaknesses,” Waltke at-
tempts to dismiss the whole of Bahnsen’s thesis . . . in six pag-
es! (Shades of the late Robert Strong of Reformed Seminary,
who took only three.) Bahnsen’s arguments are “exegetically
flawed”; they are “logically defective.”* He then appeals to
“Ockham’s razor.”” To use this sort of offhand language when
dealing with a work as profound and rigorous as Theonomy in
Christian Ethics, written by a man with a Ph.D. in philosophy,
seems a bit presumptuous. (Bahnsen replies to his critics in his
book, Ne Other Standard; I do not need to defend him here.)
More than this; Waltke ceases to argue in favor of his essays
thesis: the non-Reformed character of theonomy. There is not
one reference to a Reformed confession, systematic theology, or

. Ibid, p. 75.
. Ibid., p. 79.
Idem.
. Tbid., p. 83.
Idem.

a a)
