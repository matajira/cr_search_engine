A Positive Biblical Confession Is Mandatory 89

mitment to the authority of the Bible (point two of the biblical
covenant model). It must be put into operation covenantally in
points three through five: law, sanctions, and eschatology.

The Need for Answers

Those who claim allegiance to the Bible must use it to ans-
wer fundamental questions. Without this willingness to apply
the Scriptures to real-world issues, the assertion of the editors
regarding the Bible and the confessions could be interpreted in
a Barthian fashion, as indeed such statements were interpreted
in the Presbyterian Church, USA, from 1936 (Machen’s expul-
sion from the PCUSA) until the Confessional revision in 1967.
Therefore, let us ask the faculty some basic questions of theol-
ogy and applied theology. Let us ask them to write about these
issues if they have not yet published any formal position pa-
pers. Let us seek clarification. These are the questions that
theonomists have been grappling with ever since Rushdoony’s
By What Standard? appeared in 1959.

Was the world created in six 24-hour days?
Was the earth created on day one?

Were the stars created on day four?

Is the earth older than the stars?

Are stars billions of light years away?

Was the speed of light always a constant?

Is modern cosmology (“Big Bang”) fraudulent?
Is modern historical geology fraudulent?
Was Noah's Flood universal?

What is the geological evidence for it?

Do we need geological evidence to prove it?
When did the Flood occur?

When did dinosaurs disappear?

How old are the pyramids?

‘What year was the temple built?

When did the Exodus occur (I Ki. 6:1)?
Was Rahab wrong to lie?
