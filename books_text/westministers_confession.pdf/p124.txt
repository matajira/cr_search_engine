100 WESTMINSTER’S CONFESSION

Rushdoony’s Institutes of Biblical Law and my Introduction to
Christian Economics, and one year after the publication of Greg
Bahnsen’s Theonomy and Christian Ethics. 1t came from Meredith
G. Kline, a professor of Old Testament at both Westminster
Seminary and Gordon-Conwell Theological Seminary. While he
was at Gordon-Conwell, Dr. Kline wrote an essay for the West-
minster Theological Journal, “Comment on an Old New Error.”
It was a review article of Bahnsen’s Theonomy in Christian Ethics.
This Gordon-Conwell connection is, in my view, extremely
important in the whole Westminster Seminary vs. theonomy
debate. The kinds of criticisms emanating from the Klinite
graduates of Gordon-Conwell are of a very different style and
content from those coming from within the traditional Presby-
terian and Reformed camp at Westminster.

The publication of Kline’s essay involved a very peculiar
review procedure. First, the main section of Bahnsen’s ‘book
had been accepted by Westminster Seminary in 1973 as his
Th.M. thesis. This fact should never be forgotten by the read-
ers of Westminster's Confession and Theonomy: A Reformed Critique,
Second, Rev. Bahnsen was not allowed to reply to Kline in the
WT]. Here is how I described the problem in the 1979-80 issue
of The Journal of Christian Reconstruction, when I published
Bahnsen’s reply, “M. G. Kline on Theonomic Politics.” Dr.
Kline and the editor of the WT] in 1978 (W. Robert Godfrey)
have had cleven years to lodge a complaint against the accura-
cy of my statement; they never have. As editor, I wrote:

We want to be fair. We offer Dr. Kline the right to reply to
Dr. Bahnsen’s piece. We did not make a verbal deal with Dr.
Bahnsen, as the editor of the Westminster Theological Journal
made with Dr. Kline, that no onc will be allowed to publish a
rebuttal to his essay.”

2. “Editor’s Introduction,” Journal of Christian Reconstruction, VI (Winter 1979-
80), p. 9
