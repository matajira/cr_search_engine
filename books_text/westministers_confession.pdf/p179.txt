God’s Predictable Historical Sanctions 159

in the covenantal relevance of Christianity for other institu-
tions, They cannot do this logically or theologically, but they
attempt it anyway. It makes for good editorial copy. It also
makes for incoherent book-length studies. Hence, they refuse
to write book-length studies. They confine themselves to essays.

Muether’s Verbal Legerdemain

Muether’s language of God’s historical inscrutability, of this
world’s historical open-endedness, is a carefully contrived illusion,
an example of verbal legerdemain. On the one hand, he says
that the Church is in exile in history. This is its permanent
historical condition. This condition is guaranteed by a Calvinis-
tic, predestinating, totally sovereign God. On the other hand,
he asserts that God’s ethical randomness is manifested in histo-
ry. “Things may improve, things may get worse. Common
grace ebbs and flows throughout history.” He defines “exile”
as an indeterminate condition in which things may get better or
may get worse, yet on average stay pretty much the same
throughout New Covenant history. (Would you like to con-
struct an ethical system or social philosophy in terms of this
view of history? How about a theory of business? Or technolo-
gy? No? Neither would anyone else.)

This assertion of indeterminacy, as I have already argued, is
a contrived illusion. If God applied His sanctions randomly,
then the institutional, covenantal outcome would hardly be
random; it would be perverse. Covenant-breakers would retain
control over culture throughout Church history, despite the
death, resurrection, and ascension of Christ to the right hand
of God the Father. But this is precisely what Calvinist amillen-
nialists say must happen. It is predestined by-God this way.

Kline, Muether, and the Random Sanctions amillennialists
are all bearers of Bad News. A flailine eschatology in a world

18, Muether, p. 18.
