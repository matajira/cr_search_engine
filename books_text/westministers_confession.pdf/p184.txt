156 WESTMINSTER’S CONFESSION

Christians in general and the Church specifically. There is nothing
random about exile.

Muether’s theology of cultural defeat is self-conscious, for he
thoroughly understands exactly what his pessimillennialism
implies: “First, we cannot get caught up in the things of this
world. This world is penultimate; it will pass away, and so we
must eagerly await the new world to come.” He goes on:
“The church in this world, in other words, is a people in exile.
We are far short of the kingdom of God. . . . The church is
called to suffer in this world.”* From this we can legitimately
infer what is never stated publicly by these defenders of Christ-
ianity’s cultural impotence in history: covenant-breakers are not in
comparable exile and are not called to suffer nearly so much as the
Church is. Why did God change the rules after the ascension?

Muether’s Total Discontinuity: Final Judgment

What is most significant about Muether’s essay in terms of
social theory is that he clearly asserts a radical discontinuity
between what he calls the coming kingdom and this world of
Church history. “The kingdom of God will come from above,
not made with human hands, and no cultural activity, re-
deemed or unredeemed, will carry over into the new or-
der.”* This is a consistent and inescapable assertion of the
common grace amillennialism’s worldview: the self-conscious
denial of the eternal cultural relevance of anything men do in history.
All of mankind’s cultural efforts are completely doomed,
whether produced by covenant-keepers or covenant-breakers.

If this were the case, the works of covenant-keepers and the
works of covenant-breakers would be equal in historical. impact.
There would be no cultural “earnest” — no cultural down pay-
ment by God — in history. God would pull victory out of the

13, Muether, p. 15.
14, Idem.
15, Idem.
