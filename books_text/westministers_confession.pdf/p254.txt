230 WESTMINSTER’S CONFESSION

natural law code? Have they prospered? Which Bible verses
allow us to transfer such judicial sovereignty to a common-
ground system of jurisprudence?

We get no answers. Year after year, decade after decade,
century after century, we get no answers from the Christian
defenders of natural law theory. We just get accusations that
those who object are a bunch of theocrats. (And we all know
what Franklin and Eleanor Roosevelt thought of theocrats!)

Barker tells us that he relies on the writings of Paul Woolley
and Edmund P. Clowney — an honest admission.™ This came
as no surprise to me.

John R. Mucther

I reserve my comments for Chapter 10. I will only note here
that he ends his essay with an appeal to the political views of
Edmund P. Clowney.” This was predictable, since Muether
has a degree from Gordon College (“Gordon-Conwell Lite”).

Timothy J. Keller

See Chapter 10. He, too, relics on the insights of Edmund
Clowney. Keller has a degree from Gordon-Conwell.

D. Clair Davis

At least Dr. Davis is not from Gordon-Conwell. He is from
Wheaton College (as both student and professor), Dr. Davis
writes “A Challenge to Theonomy.” He praises theonomists for
providing specific details about how the Bible can be applied to
modern society. He alone in the book mentions Roe v. Wade,
and he identifies it as paganism in action.” He says that

94. Tbid., p. 239n.
95. Ibid., pp. 258-59.
96. Ibid., p. 283n.
97. Ibid, p. 389.
