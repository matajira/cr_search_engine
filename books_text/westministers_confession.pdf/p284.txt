260 WESTMINSTER’S CONFESSION

Bruce K. Waltke

Waltke is not a Westminster product, nor is he a Calvin Col-
lege-Free University of Amsterdam product. He is also not a
Gordon-Conwell product. He is a product of Harvard Uni-
versity and the Scofield Reference Bible. By examining his essay,
as well as onc of his previous contributions, we can get a better
idea of what the underlying problem is with Theonemy: A Re-
formed Critique. This problem is easy to state: the authors are
united only in what they do not like. They do not like theon-
omy in its present form. But they have no alternative to offer.
Therefore, most of them grow testy when asked by me to sug-
gest something. They prefer to proclaim a resolute judicial
agnosticism.

Bruce Waltke is not a follower of Meredith G. Kline. He is
not a follower of anybody, as far as his footnotes indicate. This
is his theological problem. He wings it theologically every time
he writes.

In Theonomy: A Reformed Critique, Waltke, Th.D, (Dallas Sem-
inary), Ph.D. (Harvard), offers a critique of the: theological
work of Greg Bahnsen. Rest assured, Professor Waltke is a very
clever fellow. He understands the impact of rhetoric. He di-
vides his essay into three main parts: Dispensationalism, Re-
formed Theology, and Theonomy. You get the picture: theon-
omy is clearly not dispensational, but it is not Reformed, either.
We are talking about three separate theological systems. If the
theonomists were to concede this, we would lose the argument.
Waltke merely assumes it, but if he can get the reader to con-
clude it, he wins the argument.

He is careful to offer us theonomists this left-handed compli-
ment: “We commend theonomists for their conviction, with
Reformed theologians, that the law is a compatible servant of
the gospel. . . .”? Yes, he is so very, very happy to have us

2. Theonamy: A Reformed Critique, p. 79.
