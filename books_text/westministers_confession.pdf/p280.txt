256 WESTMINSTER’S CONFESSION

punishable by hanging, and theft of a lesser amount by whip-
ping. Under a number of English statutes, restitution — single,
double, or treble - was a common penalty imposed by justices of
the peace for a variety of specified property crimes. The Bible,
however, prescribed multiple restitution as the penalty of the
thief in most cascs, or “if he have nothing, then he shall be sold
for his theft.”

From the beginning, the colonial magistrates regularly fol-
lowed the biblical patterns, imposing double restitution when
the offender was capable thereof, and requiring thieves unable
to make restitution otherwise to satisfy the court’s sentence by a
term of service. The exaction of these penalties was without
specific statutory authority until 1646. Prior thereto, the colonial
treatment of theft furnishes an example of the shaping of law by
magisterial discretion in the way favored by Winthrop, When
restitution was feasible, it was usually the only punishment im-
posed, but the courts did not hesitate to combine it with one. or
more of a variety of other penalties, ranging through whipping,
the stocks, a fine to the court, and degradation from the rank of
gentleman,

Logan ends his essay with a familiar, though implied, accu-
sation against the “simplistic theonomists.” He writes: “Whatev-
er else they were, the New England Puritans as a group were
not simplistic. They did not see themselves as some kind of
reincarnation of the nation of Israel, and they did not want to
see Israel’s judicial code reincarnated in their common-
wealth.”® Here it is again: theonomy as simplistic and. theono-
mists as judicial simpletons. When the faculty of Westminster
Seminary hears the words, “biblical law,” they immediately
think, “simplistic!” hey, of course, are much too sophisticated
for such simplistic laws as those that God specified to the peo-

58, Haskins, Law and Authority, pp. 158-54.
59. Logan, p. 384.
