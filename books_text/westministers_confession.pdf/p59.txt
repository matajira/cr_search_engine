The Question of Inheritance 35

abandoned. The hiring of Clowney’s son David marked the
tombstone over the legacy of Van Til on campus at Wesumin-
ster East. (At Westminster West, John Frame still keeps the
flame alive, although buried under several layers of outlines.)

What had gone wrong? Basically, it was this: Van Til had
not followed through with the revolution he had launched. His
intellectual revolution had been framed as a negative critique,
Not as a positive alternative. His was a system that was immedi-
ately useful only for blowing up bad things, not rebuilding.
The foundation stones for developing a positive alternative
were in place: the doctrine of creation (the Creator-creature
distinction), the doctrine of the Trinity (the one and the many),
the self-attesting nature of Scripture, the non-neutrality of
man, the identification of the doctrine of the ontological subor-
dination of Jesus as the foundation of all heresies, and so forth.
But he had no way to get from his systematic dynamiting of
natural law theory to comprehensive social reconstruction,
except by way of biblical law. Rushdoony and I took that path
in the late 1960’s."* Van Til would not follow.

2. The New Life Church Alternative

Rev. C. John Miller had worked with Rushdoony and me at
the William Volker Fund in Burlingame, California, in 1963, I
was just starting out; he was at a turning point in his career.
He seemed to be a dedicated follower of Van Til. While em-
ployed by the Volker Fund, he wrote a three-volume manu-
script on the failure of public cducation. It was never pub-
lished. In 1966, he joined the faculty at Westminster.

The counter-culture deeply affected Miller. He adopted new
approaches to evangelism. They were people-oriented, but not

18, Readers may forget: Rushdoony also offered only negative published
critiques of humanism prior to the publication of The Instines of Biblical Law in 1973.
This is why he was willing in 1960 to write the Introduction to Dooyeweerd’s in the
Twilight of Westen Thought (Preshyterian & Reformed, 1960). He did not yet sec the
threat that Dooyeweerd’s common-ground philosophy represented.
