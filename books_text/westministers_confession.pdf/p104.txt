80 WESTMINSTER’S CONFESSION

A Question of Sustained Vision

It has been a recurring theme in almost all of the published
criticisms of theonomy that the founders of the movement —
Rushdoony, North, and Bahnsen — do not agree on all points.
Added to this is the fact that we do not get along with each
other. (I actually like Bahnsen; it is the recurring essay dead-
line problem that separates us.) Bahnsen and I are indeed
united against Rushdoony’s view of the Church, which is at
best theologically imprecise and is unquestionably colored by
his personal rebellion: Rushdoony does not belong to a local
church, nor has he taken communion in two decades, except
when he is on the road, speaking at a church that has a policy
of open communion or is unaware of his non-member status.’

71. His shift in opinion from Presbyterianism to independency can be seen in his
essay, “The Puritan Doctrine of the Priesthood of All Believers,” Journal of Christian
Reconstruction, VI (Summer 1979), which is really a defense of the anti-Puritan Great
Awakening and revivalism. In 1964, Rushdoony was still in the OPC, and he was
hostile to the Great Awakening: “The decline and defeat of the Great Awakening is
sometimes scen as the end of the commonwealth idea. In actuality, the defeat of the
Great Awakening . . . was a tiumph of the holy commonwealth idea.” Rushdoony
then attacks Jonathan Edwards as an experientialist, a Platonist, and a follower of
John Locke. R. J. Rushdoony, This Independent Republic: Studies in the Nature ond
“Meaning of American History (Fairfax, Virginia: Thoburn Press, [1964] 1978), p. 105.
He attacks revivalism as undenominational and anti-denominational. “It was not
greatly concerned with saving the church, in many instances, but rather with reviv-
ing America, Its cry was ‘Save America’ ” (p. 107). He reverses this whole argument
in the 1979 essay. “With the Great Awakening, there was a growing break with civil
religion” (p. 25). He attacks Nathan Hatch’s Sacred Cause of Liberty for arguing as he
had in This Independent Republic that the Great Awakening was the first major move
in America toward a civil millenuarianism and civil religion (pp. 21-24). The Great
Awakening was positive, he says. Why? Because lay people challenged Church
hierarchy in the name of the priesthood of all believers. “No longer was it the duty
of the laity merely to listen silently and obey: they were now an aggressive priest-
hood” (p. 28). Compare this analysis with Charles Hodge's heavily documented
account of the anti-ecclesiastical, disrupting impulse of the Great Awakening: Consti-
tutional History of the Presbyterian Church, 2 vols. (Philadelphia: Presbyterian Board of
Publication, 1851), II, chaps. 3-5. Rushdoony then praises the rapid growth of
Baptists and Methodists alier 1800, “both of whom in those days placed great stress
on the priestly role of the laity” (p. 25). He praises Baptist founder Isaac Backus as
against the Puritan clergy, who “were still fearful of the people's priesthood” (p. 19).
By 1979, Rushdoony had become an noncommuning independent.
