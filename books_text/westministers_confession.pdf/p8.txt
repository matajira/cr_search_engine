It was Dr. Van Til who shocked the new students into doc-
trinal awareness. No fact is unrelated to the God of the Bible,
he declared. All truth, to be known aright, must be seen in the
light of the revelation of the Creator and Redeemer. By God's
grace we, his redeemed creatures, think God’s thoughts after
him. Christianity is not probably true; it is truth. All merely
human philosophy and science is challenged and found want-
ing. God upholds all things, including unbelievers. The believ-
er and the unbeliever have everything in common metaphysi-
cally, but epistemologically they have nothing in common. In
our proclamation of God and his grace, we present the triune
God as the sole ground for all our salvation from sin, for all of
life, and for ali our thinking.’

If it is indeed not our King’s intention for the civil authority
to enforce the first great commandment, then among the five
alternatives Bahnsen offers as possible standards for civil law,
natural revelation as indeed “a sin-obscured edition of the
same law of God” “suppressed in unrighteousness by the sin-
ner” is that to which we must appeal. .. .

William S. Barker*

1. The Orthodox Prestyterian Church, 1936-1886, edited by Charles G. Dennison
(Philadelphia: Orthodox Presbyterian Church, 1986), p. 324.

2. Barker, “Theonomy, Pluralism, and the Bible,” in William S. Barker and
Robert W. Godfrey (eds.), Theonomy: A Reformed Critique (Grand Rapids, Michigan:
Zondervan Academie, 1990), p. 240.
