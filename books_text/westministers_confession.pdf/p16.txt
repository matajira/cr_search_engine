xvi WESTMINSTER’S CONFESSION

would have lost his job as Director of the British Mint had they
become known. His hand-picked successor at Oxford, William
Whiston, did go public with his own Arian views, and he was
dismissed.” In private, Newton was also a practicing alche-
mist. His magical experiments were conducted in secret, and
his successors in physics successfully suppressed this informa-
tion. It did not become known until the British economist John
Maynard Keynes bought the Newton papers at auction. He
wrote an essay on these experiments, published posthumously
in 1947.'° Even today, only a handful of historical specialists
are aware of this occult side of Newton’s thought.” Keynes
called him the last of the Babylonians and Sumerians, for New-
ton looked at the universe as if it were a gigantic riddle. For
quantum physics, it is a much more puzzling riddle than it was
for Newton and his followers, 1660-1927.

The first social science, economics, was developed in the
seventeenth century as a conscious reaction against the English
Civil War and the subsequent cultural disruptions (1640-60).
The Christians could not agree on anything; thus, concluded
the fledgling economists, a truly scientific approach to social
theory would have to renounce any appeal to the supernatural.
It would have to renounce morality, too. Science would have to
be morally and religiously neutral, Writes historian William
Letwin: “Nevertheless there can be no doubt that economic
theory owes its present development to the fact that some men,
in thinking of economic phenomena, forcefully suspended all
judgments of theology, morality, and justice, were willing to
consider the economy as nothing more than an intricate mech-
anism, refraining for the while from asking whether the mecha-

15, Bid., p. 471.

16. John Maynard Keynes, “Newton the Man,” in Newton Tercentenary Colebra-
tions (Cambridge: Cambridge University Press, 1947).

17. Betty J.T Dobbs, The Foundation of Newton's Alchemy; Ox, “The Hunting of the
Green Lyon” (Cambridge: Cambridge University Press, 1975).
