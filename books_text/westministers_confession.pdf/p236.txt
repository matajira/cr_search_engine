212 WESTMINSTER’S CONFESSION

legal issue, he said. Then Jordan wimped out, as he later ad-
mitted. “I should have asked him, ‘How about marrying your
widowed mother?’ ”)

Second, he has relegated politics to the realm outside the
bounds of biblical ethics. If the civil covenant is nof in fact a
covenant, and if biblical ethics és part of the New Testament
(his argument), then civil government is beyond biblical law
and its sanctions. This is in fact the position of Westminster's
faculty. This is Westminster's confession. It internalizes God's law:

In summary, the most basic use of the Pentatcuch in the New
‘Testament is to establish the covenantal nature of the gospel.
Since the law is covenantal, it is the inward obedience of the
heart stemming from the relationship to God that determines
the New Testament’s positive use of the law. The ecclesiastical
and ethical applications of the law to the church all flow from
this covenantal basis, inasmuch as the only contact between the
Gentile church and the law of Moses is through Christ, the
covenant mediator.

The law of the covenant is said to be internal. There is a
huge problem lurking around in these shadows: his argument
undermines the idea of the family as a covenantal institution. Ne does
not say this explicitly, but his silence testifies to it. Only the
Church is a covenantal institution, he implies. Yet this radical
departure from the concept of the biblical covenant needs to be
proven, not just asserted (civil covenant) and ignored (family
covenant). He writes, “Not once in the New Testament is the
civil aspect of the Old Testament law applied to the civil au-
thority as an ideal.”** If this were not the case, then political
liberalism would be anti-Christian. Let it not be! Better to
remove all of God’s civil sanctions from civil law, even though
in doing so, we bring God’s sanctions down on us. (If anyone

53. McCartney, p. 144.
54. Ibid, p. 145.
