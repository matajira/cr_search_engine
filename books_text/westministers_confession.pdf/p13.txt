Foreword xiii

Presbyterian Church of North America, the Protestant Re-
formed Church, and the Reformed Episcopal Church.

Calvinism is not only in an institutional crisis; it is in a philo-
sophical crisis, Its advocates no longer agree on what Calvinism
is or what it means. In this sense, it has a great deal in com-
mon with every other movement on earth. Calvinism’s leaders,
generation after generation, have signed up almost all of their
followers to sail on a ship run by humanists. Now that ship is
visibly sinking.

The West’s Philosophical Crisis: Disintegration

The cultural moorings have been ripped up: in Communist
Europe and in the Western democracies. The universities of
the West in principle became multiversitics.a century ago with
the creation of the elective system at Harvard. Since then,
knowledge has exploded into more and more tiny fragments.
But with this fragmentation, the coherence, meaning, and
wisdom of humanist education have disappeared. This is not
an epistemological crisis limited to the ideologically disruptive
social sciences; it is basic to the physical sciences, too.

Quantum physics since 1927 has taught us that there is
nothing holding the universe together at the subatomic level
except mathematical equations, except when there is a human
observer. No observer means no “down there.” Only when mea-
sured by a human being does material reality in the form of
wave functions return to subatomic nature, we have been
told.” This has been accepted in theory by most theoretical
physicists; only recently have a series of experiments suggested
that there really is something “down there” besides equations
in between scientific observations.®

7. John Gribbin, In Search of Schrédinger’s Cat: Quantum Physics and Reality (New
York: Bantam, 1984).

8 I refer here to the experiments conducted by a team of physicists led by
