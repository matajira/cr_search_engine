46 WESTMINSTER’S CONFESSION

Rhetorically and structurally, this book resembles an exercise
in Hegelianism. Time and again, the authors use the tradition-
al thesis-antithesis approach to attack theonomy. They adopt the
sic et non (yes and no) strategy that Abelard used in the clev-
enth century to undermine men’s faith in the Church fathers.
But Hegel implied that there would be a temporary resolution
of each pair of synthesis-antithesis dichotomies. Each synthesis
would become the next thesis, Where, then, is Westminster's
synthesis? Theonomy: A Reformed Critique offers a collection of
antinomies without resolution, dichotomies without healing. It
is a confession of fifteen theologians and a librarian in search of
a synthesis, It offers a new confession. The question is: What is
the nature of this new confession? It is this: “A positive confes-
sion regarding the legitimacy of Christendom is itself not bibli-
cal. Therefore, Westminster could not hire Bahnsen and had to
fire Shepherd.”

Conclusion

The thesis of the present book is that Westminster Seminary
faced a crisis in 1965. Would it remain true to Van Til’s apolo-
getic? Would the members of the faculty at last do what they
had never done before? Would they take Van Til’s insights on
the failure of natural law theory and apply them systematically
and fearlessly to their own disciplines? Would they break with
their academic peers and begin to pioneer a whole new world-
view based on Van Til’s decisive break with humanism? Would
they offer reconstructions of their own academic disciplines
based on Van Til’s insistence that only the Bible is a valid
foundation of truth in every field of thought? Would they, in
short, become theological revolutionaries?

We know the answer today. Like a dog returning to its
vomit, the anti-theonomists have gone back to natural law
theory. They have abandoned the legacy of Van Til. The quasi-
theonomists on the faculty watch silently from the sidelines.
They have not made it clear either to their students or their
