94 WESTMINSTER’S CONFESSION

the Philadelphia campus is radon. Underneath the Escondido
campus is the San Andreas fault. Decisions, decisions.

Westminster Seminary as an institution does not want to
choose. Theonomy: A Reformed Critique makes this clear. The
faculty has never devoted much time or effort to answering the
kinds of questions I listed - not Machen’s faculty, not Clow-
ney’s, and not today’s. To answer them, you have to have a
paradigm: a set of intellectual tools and standards that enable
you to frame questions and also the valid approaches to possi-
ble answers. In short, you have to have a framework.”

Confessions and Frameworks

The Westminster Confession was such a framework in its
day. But its focus was circumscribed to the primary concerns of
the institutional Church. Christianity involves far more than
the institutional Church. So does God’s kingdom. The Church
must speak authoritatively to the whole of life, since its mem-
bers participate in the whole of life. If the Church remains
silent, then its members will hesitate to exercise authority in
their callings. This is exactly what has happened.

The self-imposed limitations of the Westminster Assembly
became the Anglo-American Reformed theological standard.
Thirteen years after the Assembly ended its work, Charles II
was restored to the throne, and a generation of persecution
against Calvinists began, Only with the Glorious Revolution of
1688-89 did persecution lessen, but the new society was in-
creasingly rationalist, unitarian, and contractual, not covenan-
tal. The vision of a national covenant faded, even in North
America. Casuistry — the application of Christian ethics to speci-
fie cases — died as a discipline by 1700.** Newton triumphed

23. Yes, I have already thought of it: the title for a John Frame newsletter. Four
pages of outlines, every month!

24. Thomas Wood, English Casuistical Divinity During the Seventeenth Century
(London: S.RC.K., 1952), pp. 32-36.
