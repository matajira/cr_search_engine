Abusing the Past 237

onomy and the Reformed Heritage: Historical Connections.”
W. Robert Godfrey tells us what Gordon-Conwell Seminary
would like us to believe about Calvin. Sinclair B. Ferguson
takes us through the Westminster Assembly. Finally, Samuel T.
Logan turns the New England Puritans into such flexible fel-
lows that one has difficulty understanding why Roger Williams
tramped through the snow in the middle of a Massachusetts
winter just to escape from them.

W. Robert Godfrey

J first heard of Dr. Godfrey the day I brought my new wife
in 1972 to Westminster Seminary for a three-hour visit. I had
not expected that it would take so long. That day, there was a
special lecture on campus by (then) Mr. Godfrey (Ph.D., 1974,
Stanford University, plus several years at Gordon-Conwell). It
was a very special kind of lecture. It was a job audition, He was
being considered for a post on the faculty. As I say, I had
never heard of Dr. Godfrey. Nevertheless, I told my wife the
following (which she still remembers clearly): “You are about to
hear the most boring lecture you have ever heard,” She an-
swered: “How do you know that?” I replied: “The guy is after
a job, He has to ‘show his stuff,’ which means he has to prove
that he is a scholar. This means, above all, that he must avoid
getting caught making a mistake. So, what he will do is summa-
rize his doctoral dissertation, since doctoral dissertations are so
narrow that if they are selected properly, nobody previously
has bothered to write directly on the topic. This is why he will
summarize his dissertation. That way, it is unlikely that any
professor will catch him in a mistake.” I knew what I was talk-
ing about; I was then completing my doctoral dissertation.

We went to the lecture. The room was warm. The lecture
was incredibly dry. It was, of course, a summary of what later
became his doctoral dissertation. Afterwards, my wife said, “I
was so embarrassed, I kept dozing off. That was the most bor-
ing lecture I’ve ever heard.”
