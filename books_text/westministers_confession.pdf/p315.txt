An Editor’s Task: Just Say Not 291

God’s sanctions in history that John R. Muether emphatically
rejects (see Chapter 6). Here is my position:

The presence of a self-maledictory oath is the mark of coven-
antal sovereignty. Only three institutions lawfully can require
such an oath: church, state, and family. Such an oath implicidly
or explicitly calls down God’s negative sanctions on the person
who breaks the conditions of the oath. These sanctions are
historical, although few Christians believe this, despite Paul's
warning: “Wherefore whosoever shall eat this bread, and drink
this cup of the Lord, unworthily, shall be guilty of the body and
blood of the Lord. But let a man examine himself, and so let
him eat of that bread, and drink of that cup. For he that eateth
and drinkcth unworthily, cateth and drinketh damnation to
himself, not discerning the Lord’s body. For this cause many are
weak and sickly among you, and many sleep. For if we would
judge ourselves, we should not be judged. But when we are
judged, we are chastened of the Lord, that we should not be
condemned with the world” (I Cor. 11:27-32).

Self-judgment, institutional judgment, and then God's judg-
ment: all take place in history. But the modern church has grave
doubts about this idea of God’s negative sanctions in history, It
therefore does not expect to experience God’s promised positive
sanctions in history. The next step is obvious: to lose faith in
meaningful historical progress. Here is the origin of pessimillen-
nialism’s lack of confidence in the work of the church, the effects
of the gospel, and the future of Christianity.

Without the oath and its associated sanctions, the church is
not legally distinguishable from any other oathless institution.
Furthermore, the oath that creates a new family is taken no
more seriously than an oath of church membership. So, only
one oath-bound institution remains that is still taken seriously,
because of the sanctions attached to the oath: the state. The rise
of statism is always accompanied by the decline of the church
and the decline of the family.

Which oath is supposed to be central in society? The church’s
oath. Why? Because only the church survives the final judg-
ment. It alone extends into eternity. It is the church that alone
