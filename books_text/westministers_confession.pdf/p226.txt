202 WESTMINSTER’S CONFESSION

formed Critique does not supply arguments on this, Bahnsen’s
bedrock exegesis. Why not?

I offer this suggestion: because none of them is confident
that Bahnsen can be answered by means of Westminster’s new
confession. Silence is the better part of valor in this instance.
They just pretend that someone else has answered him, the
same way that the defenders of Christian pluralism pretend
that someone has presented a biblical, exegetical case for plur-
alism. It is convenient. It is not intellectually honest, of course,
but it is convenient.

Bruce K. Waltke

I cover Dr. Waltke in greater detail in Chapter 10. Besides,
he is long gone. His wanderlust returned, and he left.* I will
say this: he knows where the real conflict lies. He knows who
the proper source is. In rejecting Bahnsen, he cites the political
views of Edmund Clowney.” This has been the dividing issue
at Westminster for a quarter of a century: Clowney vs. Van Til,
Clowney vs. Bahnsen, and (finally) Clowney vs, the Orthodox
Presbyterian Church. Edmund Clowney’s career deserves a
book, or at least a complete issue of Journey.

John M. Frame

Frame likes some aspects of theonomy, but he doesn’t like
others. Did anyone expect anything else? Sie ef non John strikes
again! In the words of one professor at Covenant Seminary:

There have been three approaches to apologetics at Westmin-
ster Seminary. Van Til said that everyone else was wrong.
Frame thinks that there are some correct things in everyone’s

32. Perhaps he knew something about the financial condition of Westminster
East that I don’t know.

38. Ibid., p. 84.
