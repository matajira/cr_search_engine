A Negative Confession Is Insufficient 109

The Offending Clause”

Having been called into session by Parliament, the Assembly
had no qualms about adding this justification of Parliament's
act:

As magistrates may lawfully call a synod of ministers, and
other fit persons, to consult and advise with, about matters of
religion; so, if magistrates be open enemies to the Church, the
ministers of Christ, of themselves, by virtue of their office, or
they, with other fit persons upon delegation from their Church-
es, may meet together in such assemblies (XXXI:IT).

This was a two-fold justification: (1) why they came to West-
minster and (2) why they were taking over control of the
Church from the King and his bishops. The “open enemy to
the Church” was clearly Charles I. The Puritans of New Eng-
land were in New England because of him and his father,
James I. The Puritans did not need to be persuaded about the
theological legitimacy of a synod to deal with Charles I or the
call by Parliament to assemble, They responded with enthusi-
asm, and they sat for four long years to complete their work.
It is this clause that the American revision of 1788 removed.
It is this clause that still retroactively bothers the consciences of
those who profess allegiance to the revised Westminster Con-
fession of Faith. Why? The clause no more authorizes the mag-
istrate to tell the Church what to conclude than the Arian
emperors could lawfully tell the early Church what to believe.
The magistrate calls the synod only for consultation. “Aye,
there’s the rub.” The modern humanist asks rhetorically: Why
should a magistrate call a synod for consultation? The modern
pictist asks the same. Consultation about what? The modern
humanist asks: Isn’t this a violation of the fundamental princi-

17, The 1788 revision also removed the clause identifying the Papacy as the
Antichrist: XXV:VI. This was clearly an improvement.
