An Editor’s Task: Just Say No! 269

And it came to pass, that, when Elisabeth heard the salutation
of Mary, the babe leaped in her womb; and Elisabeth was filled
with the Holy Ghost: And she spake out with a loud voice, and
said, Blessed art thou among women, and blessed is the fruit of
thy womb. And whence is this to me, that the mother of my
Lord should come to me? For, lo, as soon as the voice of thy
salutation sounded in mine ears, the babe leaped in my womb
for joy (Luke 1:41-44),

Funny thing, how one impersonal fetus leaped when another
impersonal fetus entered the room. Just another random event
which is all too easily misused by anti-abortion bigots!

What did this apologist for convenient murder think was in
Mary’s womb? The “impersonal fetus” of God? And had one of
these professional shedders of blood performed an abortion on
Mary, what would his colleagues imagine that God’s response
would have been? We can be pretly sure about the answer of
the attendees of that symposium. “It ail depends on whether
the abortionist in question was state-licensed or not.”

Go With the Flow!

That Waltke later reversed himself and became an anti-
abortionist is to his credit. It was just a little late. He had al-
ready given his blessing to those Christian professionals who,
four years later, began to ply their bloody trade legally in the
United States. He will someday meet face to face in heaven the
unborn victims of his academic presentation. Each person has
his own horrors of final judgment to think about; if I were
Waltke, this one would be mine.

Bruce Waltke has had a checkered career. First dispensa-
tional; then Reformed. First a pro-abortionist; then an anti-
abortionist. First a Dallas Seminary professor, then a Regent
College professor, then a Westminster Seminary professor, and
once again a Regent College professor. Of Bruce Waltke, it can
be truly said: “A double minded man is unstable in all his
