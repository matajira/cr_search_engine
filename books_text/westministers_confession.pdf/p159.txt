The Question of Law 135

tion of souls is utterly false.” He then linked the salvation of
society directly to the salvation of souls, a perfectly biblical
perspective: “Of all men no one is more firmly convinced than
the Calvinist that there can be no such thing as the salvation of
society apart from the salvation of the individuals constituting
society; . ..”*

This raises a crucial millennial question: If there will never
be widespread conversion of souls, can there ever be, in Kui-
per’s words, “the salvation of society”? Kuiper was well aware
that postmillennialists would ask that question; there were still
a few of them around (though not on the faculty) in 1939. As
an amillennialist, he steadfastly refused to answer this question
directly, (His spiritual heirs would answer it five decades later.)
Instead, he went from the question of the possibility of society’s
salvation to the motivation of Christians to work toward it, In
short, he individualized his social message.

The question how effective his message will prove does not
trouble the Reformed preacher out of measure. What concerns
him is that he has marching orders. Most assuredly, he prays
with all the fervor at his command that God the Holy Spirit may
cause the seed of the Word to bring forth fruit a hundredfold.
He is also confident that his labors will not be in vain in the
Lord. But he does not need the postmillennial view of the fu-
ture to sustain him in his work. Likely a minority of Reformed
preachers today take the position that through the preaching of
the gospel the kingdom will be brought to perfection.

Notice the final phrase, “through the preaching of the gos-
pel the kingdom will be brought to perfection.” Here Kuiper
resorted to that familiar amillennial rhetorical trick - read: lie
- of attributing to one’s postmillennial opponents a prediction

37, Idem.
38. Idem.
39. Ibid., p. 29.
