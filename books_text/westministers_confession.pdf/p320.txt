296 WESTMINSTER’S CONFESSION

Consider the Christian who hears two messages. In the first
church, he is told that the world cannot be healed in history,
but God does intend to rescue His people from the evil-doers
of this world. In the other church, he is told that there is no
earthly hope, but God is not going to Rapture His people. The
first church has several thousand members, lots of programs,
youth groups, a large building, and a gymnasium. The other
church is tiny, has few programs, and has not grown for ten
years. In this church, they preach Calvinist doctrine, which is
unknown to most visitors, and alienates most of the others. But
this church also preaches that there are no specifically Chris-
tian solutions to the problems of this world. Now, which form
of pietistic retreat from this world do you think will sell?

Man is saved by grace (justification), not by doctrinal purity
{i.e., theological sanctification). So, the person who selects the
large church is no worse off, if doctrine is closely related to
action (i.e., inaction). With greater knowledge there always comes
greater responsibility (Luke 12:47-48). Thus, hearing rigorously
doctrinal sermons places the listener under greater responsibil-
ity. But if his church preaches that Christians are not given any
unique solutions to real-world problems, then what difference
does all the doctrine make? It merely places the listener under
greater condemnation. He would therefore be foolish to re-
main in a pietistic Reformed church. He should attend the
equally pietistic Baptist church. And he will.

Christian Reconstruction preaches the triumph of Christen-
dom. it issues marching orders to an army that cannot lose in history.
Tt has been the goal of the Christian Reconstructionists to move
forward in every sense. We have tried to move forward exeget-
ically, which is why we have published Bible commentaries. We
have tried to move forward philosophically, too. We have tried
to move forward culturally. We have done this at considerable
expense. And I assure you, we have had neither encourage-
ment nor much constructive criticism from those Christian
leaders who are committed to sitting on the sidelines, let alone
