268 WESTMINSTER’S CONFESSION

critical of their own presuppositions. And so he began his dis-
cussion with a consideration of abortion — in a book on human
reproduction!

“The first argument in favor of permitting induced abortion
is the absence of any biblical text forbidding such an act.”
Already, the assembled physicians must have begun to breathe
easier. They could almost hear the cash registers ringing (pre-
computers}. But isn’t the unborn child a soul? God forbid! Let
us not refer to “unborn child.” It is properly called a fetus. This
sounds safely impersonal.

A second argument in favor of permitting induced abortion
is that God does not regard the fetus as a soul [Hebrew nephesh],
no matter how far gestation has progressed. Therefore, the fetus
does not come under the protection of the fifth commandment.
... We should note this contrast between the Assyrian Law and
the Mosaic Law: the Old Testament, in contrast to the Assyrian
Code, never reckons the fetus as equivalent to a life."*

Well, now: all those in favor of identifying their views on abor-
tion with the pagan Assyrian Code, which we all know was
clearly opposed to the Bible, please stand up!

As I said, Professor Waltke is a clever man. He structures his
arguments with great rhetorical skill. The problem is, he has
made an academic career out of switching arguments.

In order to cnd his argument with authority, he added:
“The Talmud appears to reflect the biblical balance by allowing
abortion when the life of the mother was in danger (Mishna,
Oholot, 7:6).”"" Surely, we should ali conclude, the Talmud is
a more reliable commentary on the Old Testament’s view of
unborn children than the New Testament is, for the New Tes-
lament says:

15, Bid. p. 9.
16. Bbid., pp. 10, 11.
17. Mid., p. 18.
