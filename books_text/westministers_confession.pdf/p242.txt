218 WESTMINSTER’S CONFESSION

hell, yet it is obvious that hell and the lake of fire are instru-
ments of God’s cosmic torture. Christians cannot stomach a
God who imposes serious sanctions, and they reject the very
suggestion that in a holy commonwealth, they would be res-
ponsible personally for imposing God’s earthly sanctions. Thus,
they have rejected theonomy. They would rather live under
any version of humanism and demonism than be personally
responsible for stoning a convicted criminal. God has given
them their desire.”

Johnson’s essay is typical of the worldview of modern piect-
ism, both Reformed and Arminian. It is a theological defense
of Christianity without legitimate sanctions outside the local
church cloister. The only ultimate biblical negative sanction in
New Covenant history is excommunication, This lets Christians
off the cultural hook. They know that covenant-breakers care
nothing about excommunication. Covenant-breakers do not
perceive excommunication as a personal threat, assuming they
know what it is, which they don’t. (How many Christians are
aware that excommunicate is related grammatically to the word
communion, as in holy communion? Not many.) Therefore,
covenant-keepers are seemingly let off the hook for the cvils of
covenant-breaking society. Christianity’s triumphs are confined
to the cloister for the sake of reduced cultural responsibility.

Kingdom Without Sanctions

Theologically, this is a concept of God’s kingdom without
sanctions. God is seen as imposing His predictable sanctions
only after death. For pictists, Jesus is King of dead Kings and
Lord of dead lords. God’s earthly sanctions are random, Kline
and Muether tell us. The sanctions might as well be Satan’s.
(With such a view of historic sanctions, they are Satan’s: coven-
antally perverse, a reversal of Deuteronomy 28.)’? God still

69. And has sent leanness into their seminaries.
70. See Chapter 6, above.
