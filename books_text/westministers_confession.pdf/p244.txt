220 WESTMINSTER’S CONFESSION

this. No, you’re right; you should never trust a summary of
anything this bizarre. I need to prove it.

Jesus is superior to the angels, the heavenly mediators of the
law (Heb. 1:1-2:18). The central passage is Psalm 8:5-7, which
indicates that humanity's subordination to the angels (through
the angelically mediated law of Moses) was only temporary, now
that Jesus has been crowned with glory and honor.”?

Questions: If the heavenly angels mediated Old Covenant
Jaw to national Israel, then which angels mediated the “law of
nations” to all the other nations? And more to the point, now
that Jesus has removed the penal sanctions of Old Covenant
law because He has removed His heavenly angels, which angels
now remain as civil mediators in history?” Using which civil
laws? We Christians are now under these non-biblical laws in
civil affairs. Why did Jesus, as Lord of lords, transfer such civil
authority to fallen angels in New Covenant history? And if He
did not transfer such sovereignty to them, then why did He
adopt their standards of civil law when He replaced them? But
Johnson prudently avoids this line of reasoning, for which we
can hardly blame him, given the magnitude of his thesis and
the paucity of its argumentation.

He also argues that the civil penal sanctions were closely —
ie., indissolubly — related to the Old Covenant priesthood and
sanctuary.” “, . . the Mosaic penal sanctions belonged in the
context of the discipline and purity of the covenant communi-
ty. They pointed toward the exclusion of apostates, whose lives
showed contempt for the Lord of the covenant, from the com-

72, Tid, p. 182.

73. Or is Dr. Johnson implicitly denying the existence of demonic influences in
post-A.D. 70 history? If so, he has a really tough thesis to defend. See Gary North,
Unholy Spirits: Occultism and New Age Humanism (Ft. Worth, Texas: Dominion Press,
1986).

74, Johnson, pp. 185-86.
