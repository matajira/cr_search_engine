3

A POSITIVE BIBLICAL CONFESSION IS
MANDATORY

But what saith i? The word is nigh thee, even in thy mouth, and in
thy heart: that is, the word of faith, which we preach; That if thou shalt
confess with thy mouth the Lord Jesus, and shalt believe in thine heart
that God hath raised him from the dead, thou shalt be saved. For with
the heart man believeth unio righteousness; and with the mouth confes-
sion is made unto salvation (Rom. 10:8-10).

The evangelical world proclaims the necessity of each indi-
vidual’s making a positive confession in public regarding his
confidence in the saving work of Christ on Calvary as his only
hope of eternal life. The individual covenant between God and
man is grounded on a positive public confession. This confes-
sion is judicial. It proclaims that Jesus Christ’s atoning work on
Calvary has satisfied God’s legal claims against the confessor, now
that he has made this public declaration.

But what about the other three covenants: ecclesiastical,
familial, and civil? What about confession? Each involves taking
a self-maledictory oath before God: invoking God’s lawfully
applied negative sanctions should the oath-taker violate the
