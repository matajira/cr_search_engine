The Question of Inheritance 21

his initial success in Theonomy: A Reformed Critique. The question
that my book attempts to answer is this: Was this also a success-
ful counter-revolution intellectually and theologically? You
know my answer already: no. Now I must prove my case.

Tn 1964, Robert D. Knudsen assigned That Hideous Strength
to his entire class (ie., me} on “The Fate of Freedom in West-
ern Philosophy.” Lewis’ book was a revelation to me. In this
1946 novel, some of the major issues of the next half century
were spelled out. It covered the revival of occultism, the
growth of State-funded humanist science, the control of the
press by narrow elites, the bureaucratic war against Christiani-
ty, and much more. But it was the above-quoted statement,
made by the character in the novel who was most like Lewis,
that grabbed me. Here, in one brilliant paragraph, was Van
Til’s view of history: the ever increasing self-consciousness of
both covenant-keepers and covenant-breakers. Tistory does not
move backward. It cannot remain ethically neutral. It moves
forward in a series of conflicts - not class conflicts but covenan-
tal conflicts. There is no escape from choosing. The lines are
drawn ever more sharply and tightly over time.

Little did I know as I sat in my basement room in Machen
Hall, reading Lewis’ novel, that upstairs in the Westminster
Seminary administration offices, the reality of Lewis’ vision was
being played out. Fundamental choices was being made, day by
day. These choices were being forced upon the seminary be-
cause of three institutionally inescapable events. ‘hese deci-
sions were to determine the direction the seminary was to take
over the next quarter century.

Institutional Turning Points

The two crucial events in the life of any organization are its
founding and its first transition at the death or retirement of
the founder. Westminster Seminary, however, had three crises.
The third came when the seminary’s constitution was restruc-
tured in 1965 in order to lodge greater power in one office,
