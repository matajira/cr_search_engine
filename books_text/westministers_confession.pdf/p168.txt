144 WESTMINSTER’S CONFESSION

idea of the Christian’s task of “thinking God’s thoughts after
Him” as a result of his exposure to Vos’ concept of God's king-
dom as implying “the subjection of the entire range of human
life in all its forms and spheres to the ends of religion.” Vos
made it clear that the kingdom of God in history encompasses
everything. This is the kingdom of God on earth as civilization,
as Christendom. It is the very antithesis of Edmund Clowney’s
narrow definition of God’s earthly kingdom as the institutional
Church alone. Clowney had a different confession. He system-
atically reshaped Westminster Seminary to make it conform to
his confession.

Edmund Clowney rejected the Scottish-Puritan view of the
kingdom of God. What he taught was pietism, He taught what
London’s Baptist Metropolitan Tabernacle’s pastor Peter Mas-
ters has preached. Masters proclaims the new Westminster's
confession, and for the same reasons. He, too, rejects thean-
omy: “Reconstructionist writers all scorn the attitude of tradi-
tional evangelicals” - read: traditional pietists - “who see the
church as something so completely distinct and separate from
the world that they seek no ‘authority’ over the affairs of the
world.” No authority over the affairs of this world: these are the
key words. This is what Clowney seeks. This is what Masters
seeks, This is what all pietists seek. This is what Westminster's
confession proclaims, In terms of its view of God’s revealed
jaw, Christian corporate responsibility, and the kingdom of
God in history, Theonomy: A Reformed Critique is merely the
Clowney-Masters pictism writ large.

By identifying the kingdom of God in history solely as the
institutional Church, Clowney and his disciples remove all

1979), ch. 6.

64. Peter Masters, “World Dominion: The High Ambition of Reconstruction,”
Sword & Trowel (May 24, 1990), p. 18.

65. With a few mild-mannered dissenters, who as yet have offered nothing con-
crete to substitute for it.
