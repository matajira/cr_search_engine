The Question of Millennialism 175

have I set my king upon my holy hill of Zion. 1 will declare the
decree: the Lorp hath said unto me, Thou art my Son; this day
have I begotten thee. Ask of me, and I shall give thee the hea-
then for thine inheritance, and the uttermost parts of the earth
for thy possession. Thou shalt break them with a rod of iron; thou
shalt dash them in pieces like a potter’s vessel. Be wise now therefore,
O ye kings: be instructed, ye judges of the earth. Serve the Lorp
with fear, and rejoice with trembling. Kiss the Son, lest he be
angry, and ye perish from the way, when his wrath is kindled
but a little. Blessed are all they that put their trust in him (Psa.
2). (emphasis added)

Lest we imagine that this is merely another Old Testament
proof text,’ consider Revelation 2:26-29: “And he that over-
cometh, and keepeth my works unto the end, to him will I give
power over the nations: And he shall rule them with a rod of
iron; as the vessels of a potter shall they be broken to shivers:
even as I received of my Father. And I will give him the morn-
ing star. He that hath an ear, let him hear what the Spirit saith
unto the churches.” There is a two-fold process of overcoming:
personal and cultural. They are linked ethically and judicially.
They are also linked eschatologically. This means historically.

Clay jars, Gaffin writes, are believers “in all their mortality
and fragility.” Well, so what? What docs this professor of
systematic theology think covenant-breakers are made of, stain-
less steel? But, as with every amillennialist, he gets his biblical
imagery backwards. He secs the Christians as clay pots and the
covenant-breakers as rods of iron, from now until doomsday. It is
true that the covenant-breaker is sometimes employed by God
as a rod against us (negative sanctions in history), but never
apart from the promise of a future reversal of the sanctioning
relationship:

12, “Proof text” is the phrase used by critics to dismiss a biblical text that proves
something they don’t like one little bit.

13. J6id., p. 211.

 
