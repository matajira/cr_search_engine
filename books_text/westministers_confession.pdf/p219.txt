Sic et Non: Judicial Agnosticism 195

that links together the contributors to the symposium. What
they are agreed on is that the specific defense of theonomy that
was set forth by Greg Bahnsen in 1973 is either wrong (the
majority view) or exegetically inadequate. On such a meager
foundation as this negative presentation, cultural skyscrapers
are not built.

The Westminster faculty is not unified regarding the judicial
substance of its critique, but only its form: “Not Bahnsen, not
here, not ever!” This is why the careful reader cannot discover
what, exactly, the contributors suggest as an allernative to
biblical law. They have no idea. They do not say. All they know
is that they do not want biblical law as the basis of civil law. If
the U.S. Supreme Court authorizes the murder of unborn
babies, that is good enough for Westminster. If it should re-
verse itself, that is also good enough for Westminster. What is
not good enough is Bahnsen’s formulation of theonomy.

So, in this chapter, we will scan the highlights of eight essays
that tell us why theonomy is just not good enough, plus one.

Robert D. Knudsen

Having taken a class from Dr. Knudsen, I can say that he is
a decent lecturer, even when there is only one person enrolled.
That was the case with me. He would come into class, put his
lecture notes on the podium, give the lecture, and walk out. (I
could not cut that class!) The class was “The Fate of Freedom in
Western Philosophy.” The only problem I ever had with it was
that I could never understand what exactly he was offering as
an alternative. One thing seemed certain, and most of his
students knew it: Knudsen had little use for the philosophy of
Van Til. In 1963, we knew the department of apologetics was
divided. He was a Dooyeweerdian to the extent that he was
anything, and his unwillingness to write only made things more
difficult for us. We never could figure out what he wanted
Christians to do, other than preach a soul-saving gospel. (That
was our problem with Van Til, too.) He would never raise or
