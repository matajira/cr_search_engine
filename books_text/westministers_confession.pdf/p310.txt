286 WESTMINSTER’S CONFESSION

measures. Because he was neither academically or exegetically
equipped to refute my thesis, he imputed to me the very idea
I wrote the book to refute.

Or else he never bothered to read the book.

North the Charismatic

Muether identifies me as “a professed convert to charismatic
thought.”** A professed convert is someone who publicly
adopts a particular position or belief. When, then, was my
profession? What evidence does he offer for this accusation?
He refers to a newsletter I wrote that reported that my wife
was healed of a long-standing physical affliction the very day
the elders of our church anointed her with oil and prayed over
her.” But he does not mention that it was my wife who was
healed. That would make my case look too strong — a personal
witness to the truth. He begins with a partial citation of my
account:

“[The healing]” - note, he uses brackets so as to avoid mention-
ing my wife — “did not lead to tongues-speaking, but it did lead
to a new willingness to accept the fact that no one ecclesiastical
organization has all the answers.” This ecclesiastical relativism is
astonishing from an allegedly Reformed author, but it is consis-
tent with contemporary evangelicalism.*

Notice the pejorative phrases. Ecclesiastical relativism! Con-
temporary evangelicalism! Yet Westminster Seminary has been
developing its academic program for a quarter of a century by
adopting the ideal of broad-based evangelicalism. My view is
this: Calvinism is true, but the Calvinist Scholastic tradition has
ignored biblically legitimate practices in other traditions.

48. Muether, p. 251.

47. North, “Reconstructionist Renewal and Charismatic Renewal,” Christian
Reconstruction, XM (May/June 1988).

48, Muether, p. 253.
