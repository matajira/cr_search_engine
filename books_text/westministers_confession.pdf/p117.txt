A Positive Biblical Confession Is Mandatory 93

more brick in a gigantic pile of bricks strewn randomly across
the academic landscape” - a pile that has been expanding
exponentially for a century.”

To answer the controversial kinds of questions that I listed
above, a Christian must take risks, He also needs a systematic
worldview. He needs a handbook of biblical law. Because the
modern Church believes that “the Bible is not a textbook on
[fill in the blank],” it has nothing authoritative to say to the
world except to warn people to flee the world. Yet even this is
not possible, since history is a package deal: you do not just
flee it, except by dying. We must live in the world. But to live
in it, we must either make institutional peace with it as the
historically defeated servants of God, or else change it in order
to manifest God’s kingdom standards on earth, including God’s
civil-judicial standards. Theonomists recommend the latter. All
other major Christian groups recommend the former.

There is no permanent cease-fire with Satan in history.
There is also no zone of neutrality between Christ and Satan.
This compels us to choose. If we refuse to choose, we are
brought under God’s negative sanctions anyway. History is a
realm of decision-making, “No decision” is still a decision.

Both campuses of Westminster Seminary are under siege
from all sides. From the “east,” each campus is threatened by
the liberal accrediting agency: “Get women on your board or
we'll revoke your accreditation. Choose!” On the “west” side,
each campus is threatened by traditional donors: “Make up
your mind whether you are Calvinists or mush-mouthed neo-
evangelicals. Choose!” On the “north” side, each campus is
threatened by the challenge of the theonomists: “Be true to
Van Til. Choose!” On the “south” side, each is threatened by
students: “This place is too academic. Lighten up!” Underneath

21. Bernard K. Forscher, “Chaos in the Brickyard,” Science (Oct. 18, 1963), p.
339.

22. M. King Hubbert, “Are We Retrogressing in Science?” Geological Society of
America Bulletin, vol. 74 (April 1963).
