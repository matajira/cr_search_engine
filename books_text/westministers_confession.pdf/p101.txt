A Positive Biblical Confession Is Mandatory 77

covenant is. We can see this by searching the Westminster
Confession and its catechisms for a definition of “covenant.”
There is none. This absence of any definition did not hinder
the acceptance of Calvinism, any more than Marx's refusal to
define class hindered the acceptance of Marxism. The word
covenant became a kind of mantra that was repeated over and
over whenever the Calvinist was asked to distinguish his theo-
logical position from the others. “We believe in God’s cove-
nant.” What is this covenant? “A personal-legal bond.” How
does it apply to the real world? “Covenantally.” What are its
unique features? “Covenantal.” How are they different from
any other legal bond? “God imposes it.” But how? “Sovereign-
ly.” What do you mean, sovereignly? “TULIPly.” And from
there, the Calvinist could deflect the discussion away from the
covenant and back to the familiar five points of predestination,
of which he was the textual master. If the questioner pursued
the matter of the covenant, he would be given long shrift — if
necessary, three years long: seminary.

The Calvinist’s seminary education does not clarify the cove-
nant. It systematically does not clarify the covenant. The student
may be assigned Hodge’s three-volume Systematic Theology
(1873), which devotes more space to a refutation of the philoso-
phy of Sir William Hamilton (you remember him, of course)
than it does to the doctrine of the covenant. Or perhaps he is
assigned L. Berkhof’s Systematic Theology, which has a skimpy
cight-page index for 738 pages of text, and which lists covenant
under “covenant of grace,” “covenant of works,” and “covenant
of redemption,” a traditional tripartite classification device that
few if any Calvinist theologians are willing to defend any more.
This division is surely not emphasized in the classroom. The
student looks up “covenant of grace,” and learns that: (1) it is
a gracious covenant; (2) it is a Trinitarian covenant; (3) it is an
eternal and therefore unbreakable covenant; (4) it is a particu-
lar and universal covenant; (5) it is essentially the same in all
dispensations, though its form of administration changes. This
