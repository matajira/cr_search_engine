A Positive Biblical Confession Is Mandatory 97

and are inhospitable to new truths. On the contrary, we wel-
come new discoveries with all our heart; and we are looking, in
the Church, not merely for a continuation of conditions that
now exist but for a burst of new power. My hope of that new
power is greatly quickened by contact with the students of West-
minster Scminary. There, it sccms to me, we have an atmos-
phere that is truly electric. It would not be surprising if some of
these men might become the instruments, by God’s grace, of
lifting preaching out of the sad rut into which it has fallen, and
of making it powerful again for the salvation of men.™

Conclusion

That postmillennial vision — that positive historical confession —
is long gone from Westminster. A new confession has replaced
it, a confession of what Christianity isn’t in history, of what the
kingdom cannot accomplish on earth, and of what the Bible
doesn't provide: blucprints, What Westminster’s confession
proclaims is the impossibility and undesirability of establishing
Christendom. The kingdom of God in history has been inter-
nalized and ghettoized by this new confession.

It is not enough to proclaim one’s hostility to a particular
position. What must also be proclaimed is an agreed-upon,
comprehensive alternative to whatever is formally rejected. But
Westminster Seminary has offered only a negative confession,
though disguised in the swaddling clothes of Christian cultural
relevance — a cultural relevance without the biblical authority,
law, sanctions, or millennial victory. This confession, culturally
speaking, calls Christians to content themselves with tiptoeing
through TULIP. It therefore rejects Christian Reconstruction.
The incompatible positive confessions of the book’s individual
authors reveal an institution and a tradition in the throes of a
monumental crisis.

28, J. Gresham Machen, “Christianity in Conflict," in Vergilius Ferm (ed.),
Contemporary American Theology (New York: Round Table Press, 1932), I, pp. 269-70.
