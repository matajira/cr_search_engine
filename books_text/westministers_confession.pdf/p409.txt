Bible’s authority, 88
Clowney’s &, 26, 31, 144
deteriorating, 283
ghetto theology, 114
internalizing, 212
muddled, 96, 221
natural law, 229, 244
negative, 15-16, 97-98
neutral State, 75
oath (civil) denied, 76

Wheaton College, 23-26,
101, 236

Whigs, xvii, 122

Whigs Ecclesiastical, 236

Whiston, William, xvi

385

wilderness, 258

Williams, Roger, xvi, 17,
111, 237, 281

Wimber, John, 305

Wines, E. C., 320

Winthrop, John, 249, 251,
254, 255

Witherspoon, John, 121

women, 345

Woolley, Paul, 14, 27, 29,
58, 113, 132-33, 232

‘Wycliffe Bible Translators,
343

Young, Edward, 14, 39
