Sic et Non: Judicial Agnosticism 225

ster East goes bankrupt), perhaps he will write a book about
the whole ugly affair. If I am still around, I will publish it.

William S. Barker

Now we get to the practical part of Theonomy: A Reformed
Critique, where the exegetical rubber hits the political road. We
come at last to pluralism. Barker asks the question: “Is plural-
ism biblical?"** Since this issue has not been dealt with exeget-
ically since Roger Williams first propounded it - Williams ap-
pealed exclusively to natural law — I had hoped for something
more to the point than three pages devoted to the coin in Jesus’
“render unto Caesar” confrontation,®®

But before Barker gets to the coin incident, he goes right to
the appropriate historical source for his theology, the 1789
statement of the Presbyterian Church of America, “Preliminary
Principles.” This was written the year after that Church had
rewritten the original Westminster Confession of Faith. That
1788 ecclesiastical assembly did not justify its actions exeget-
ically. The next year, it passed the “Preliminary Principles.” It
also sent a letter of congratulations to President Washington.
Echoing Washington’s Masonic rhetoric on the close tie be-
tween religion in general (but not Christianity specifically) and
public virtue, the address announced: “Public virtue is the most
certain means of public felicity, and religion is the surest basis
of virtue. We therefore esteem it a peculiar happiness to be-
hold in our Chief Magistrate a steady, uniform, avowed friend
of the Christian religion, and who on the most public and
solemn occasions devoutly acknowledges the government of
Divine Providence.” The address then identified the role of the
Presbyterian Church in the American political religion: “We
shall consider ourselves as doing an acceptable service to God
in our profession when we contribute to render men sober,

85. Barker, “Thconomy, Pluralism, and the Bible,” itid., p. 228.
86. Ibid., pp. 284-36.
