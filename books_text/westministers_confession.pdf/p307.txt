An Editor’s Task: Just Say No! 283

So far, Muether is merely ill-informed. He is an amateur
who is in too far over his head academically. He has not done
his homework. This is typical of our critics. As such, his efforts
would be worth a few zingers, a little public roasting. But this
is not the heart of his criticism of the theonomists. The essay
then degenerates into a personal attack on my integrity, and
not just an attack: a tissue of lies.

“Sola Scriptura”: Beneath Contempt

Mr. Muether attacks what he calls “The Biblicism of Theon-
omy.” In his view, biblicism ~ also known in Church history as
sola Scriptura — is a liability. He specifically identifies an exam-
ple of a comparable biblicism: belief in the six-day creation. He
identifies belief in such a view of creation as fundamentalism.
(And you know what they think about fundamentalism at Gor-
don College!) “Fundamentalists use the Bible as a textbook on
geology, finding evidence of a literal six-day creation and a ten-
thousand-year-old earth.” What? The Bible as a textbook?
Outrageous! (Substitute the word “marriage” for “geology,”
and see if you like the result.) This view of creation is standard
fare at the neo-evangelical institutions that have been waging
war against Calvinism since the end of World War II.

Sadly, there is no course at Westminster Seminary on the
necessity of the six-day creation, any more than there is a sec-
tion in the required ethics course on abortion as murder. This
is why the editors were willing to allow this passage to get into
the book. It was not enough to be “neutral” on the six-day
creation; Westminster Seminary is now publicly represented by
someone who is verbally contemptuous of it. Westminster's
confession gets worse and worse over time.

Then Mr. Muether overplays his hand. In doing so, the tone
and character of his essay changes. He goes beyond rhetoric.

43, Muether, p. 254.
