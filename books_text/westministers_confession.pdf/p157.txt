The Question of Law 133

no discussion of the relevance of Christianity for Western civili-
zation, few lessons from the past — and his endless nervous
coughing. Ie was among the very worst lecturers on history I
ever heard, and I heard a career full. He would have made a
superb librarian or bibliographer, not to mention a railway
clerk.?* He missed his calling.) I ended the review with this
statement, which I think is still appropriate today:

. . it is indicative of the state of the seminary today that Mr.
Woolley never hesitated to state his opinions frankly on campus,
while the politically conservative faculty members at Westminster
Seminary, such as Dr. Van Til and John Murray, chose to keep
their political views hidden, sticking to exegesis and their aca-
demic disciplines. The liberals use the classroom for their pur-
poses, and the conservatives use the classroom for theirs. Unfor-
tunately, the classroom goals of the conservatives have been far
too limited to promote an effective, long-range program of
Christian reconstruction. The liberals win by default. That is the
significance, not of J. Gresham Machen, but of his orthodox
followers who share his outspoken political beliefs but who do
not speak out, as he did,™

There are other related aspects of Westminster's history that
have been blacked out. It is time to let our light shine on them.

Abandoning Machen’s Legacy: Stage One

R. B. Kuiper was professor of systematic theology at West-
minster the first year, 1929-30. He left for three years to be-
come president of Calvin College, 1930-33. He returned to
become professor of practical theology at Westminster in 1933,
a position he held for two decades. He ended his career as

33. He used to memorize U.S. and Canadian train schedules as an exercise. In
his dotage, he directed trains from his living room. When you promote abortion in
the name of Christian ethics, God deals with you accordingly.

$4, Journal of Christian Reconstruction, IV (Winter 1977-78), p. 180.
