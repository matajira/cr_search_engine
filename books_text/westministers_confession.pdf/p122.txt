98 WESTMINSTER’S CONFESSION

Negative confessions will not persuade men who are caught
in a cultural crisis to die in order to defend them, The bulk of
the contributors to Theonomy: A Reformed Critique have forgotten
a fundamental rule of life: whatever is not worth dying for is
not worth living for, either.
