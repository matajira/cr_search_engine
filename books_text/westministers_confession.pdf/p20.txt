xx WESTMINSTER’S CONFESSION

again in August of 1973, with this warning: “Moral reform does
not mean the ability to recognize evil but the power to do good
and to rebuild in terms of righteousness and justice.”? But
seminaries in 1973 had not yet advanced even to the prelimi-
nary stage of recognizing evil. In the year of our Lord, 1991,
they still haven’t,

In 1973, Greg L. Bahnsen submitted his Th.M. thesis to the
faculty of Westminster Theological Seminary, “The Theonomic
Responsibility of the Civil Magistrate.” It was accepted. For
seventeen years, some members of the faculty remained unhap-
py with the decision to award him his degree. For over seven-
teen years, they have successfully blocked his appointment as
professor of apologetics, despite the fact that he played by the
seminary’s rules and earned a Ph.D. in philosophy at a secular
university. But they never publicly offered a reason.

Then, in late October, 1990, they finally offered an indirect
excuse for this exclusion: Theonomy: A Reformed Critique.* This
symposium can be analyzed from many angles, but one angle
surely is this: the book is a dressed-up theological defense of
two decisions taken by the seminary a decade earlier: (1) not to
hire Greg Bahnsen; (2) to fire Norman Shepherd.> The semi-
nary has long needed a cover for these two decisions. It has
needed a specifically theological justification. Now it has one.
The theological justification that the faculty has now adopted is
this: a denial that the establishment of Christendom is a valid biblical
goal. Bahnsen and Shepherd came far too close to this ancient
Christian ideal. Thus, they had to be excluded from Westmin-
ster Seminary. They had rejected Westminster’s confession.

8. Chalcedon Report (Sugust 1973).

4. Theonomy: A Reformed Critique, edited by William S$. Barker and W. Robert
Godfrey (Grand Rapids, Michigan: Zondervan Academie, 1990).

5, See Appendix E: Julius Shepherd.”
