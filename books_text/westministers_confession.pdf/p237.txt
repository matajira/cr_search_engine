Sie et Non: Judicial Agnosticism 213

asks me, “Are you suggesting that AIDS is God’s sanction
against society for refusing to enforce God’s civil laws against
homosexuality?”, I reply, “If it isn’t, it is the best imitation since
syphilis!”) The Christian antinomians who take this position —
that the State is not a covenantal institution under God’s coven-
antal laws and sanctions — must also argue, as Kline does, that
God no longer brings His predictable corporate sanctions in
New Covenant history.”

McCartney is a pietist, pure and simple. He insists on the
internalization of kingdom law and kingdom sanctions in New
Covenant history. The case laws are out!

Therefore, the New Testament’s approach to the Old Testa-
ment is noi an attempt to readapt or contemporize case law, in
the way the Rabbis did. The law, or rather the Old Testament as
an entirety, is focused on Christ, and through him it becomes
applicable to believers. Thus case law is not directly applicable,
even to believers; it is applicable only as a working out of God’s
moral principles, an expression of God’s character revealed in
Christ."

Sanctions Removed

The Church is the only institution is which God’s sanctions
still apply, he says, and there is only one final sanction: excom-
munication. “As we have noted, the New Testament gives no
indication of the law's sanctions as applicable to any except
Christ and, through him, his people. .. . There may indeed be
punishment for people within the church (2Co 10:6), but this
does not involve civil authority or those outside the church
(1Co 5:12), and its only form is various degrees of removal
from fellowship (being ‘cut off” from the people).”*”

55. See above, Chapter 6.
86, McCartney, p. 146.
87. Iid., p. 147.
