God’s Predictable Listorical Sanctions 163

In response, I argue that it is never a question of God’s
predictable historical sanctions vs. no sanctions. The question
rather is this: Against whom will God’s negative sanctions be
predictably imposed, covenant-keepers or covenant-breakers?
There can be no neutrality. The amillennialist and the premil-
lennialist both insist that prior to the next prophesied eschat-
ological discontinuity, which they insist is Christ's Second Com-
ing or the Rapture, God’s negative sanctions will be imposed
either equally against covenant-keepers and covenant-breakers
(Kline’s Random News) or progressively against covenant-keep-
ers, with covenant-breakers acting as God’s appointed agents
(Van Til’s Bad News). The familiar denial of God’s predictable
negative sanctions in history is in fact an affirmation of the
inevitability of His negative sanctions against the Church, from.
Pentecost to the bodily return of Christ in power and glory.

The postmillennialist, in sharp contrast, denies that coven-
ant-keepers will be the primary targets of God’s negative sanc-
tions throughout history. He argues that the message of the
Bible is covenantal: faithfulness brings God’s blessings, while
rebellion brings God’s curses (Deut. 28). This is the message of
the Old Testament prophets. They brought covenant lawsuits
against Israel and Judah, judicially calling all covenant-breakers
back to covenantal faithfulness, and threatening them with
direct, culture-wide, negative sanctions if they refused. Further-
more, in a shocking disregard of the non-theonomists’ princi-
ple that only ancient Israel was under the judicial requirements
of God’s covenant, Jonah was sent to Nineveh to announce the
same message: in 40 days, God would bring His sanctions
against them. Jonah, initially acting in a non-theonomic fash-
ion, remained faithful to his principle that God was not really
interested in bringing Nineveh under the terms of His cove-
nant. He steadfastly refused to bring this covenant lawsuit
against Ninevch, and he suffered an unpleasant three-day
experience as a result of this refusal. He was given time to
rethink his position, which he did, becoming theonomic. He
