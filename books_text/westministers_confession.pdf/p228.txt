204 WESTMINSTER'S CONFESSION

Here is the place to point out one very clever strategy that
our critics have adopted. They point to Bahnsen’s apologetic
defense of theonomy and conclude that it is simplistic. Theonomy
in Christian Ethics did not attempt to apply all of the case laws
to contemporary issues. It was a rigorous defense of a particu-
lar hermeneutic. But we have published over a hundred books
and scholarly journals that apply our hermeneutic position,
both positively and negatively, to the affairs of this world. The
critics fail to mention this somewhat annoying fact. They do
not offer us even the courtesy that House and Ice, as dispensa-
tional critics, extended: providing a lengthy annotated bibliog-
raphy of our works. The reader is left with the impression that
we are simplistic.

They are not reticent to cite the journalistic hatchet job
written by Rodney Clapp and published in Christianity Today?”
References to Clapp’s essay appear seven times in the Westmin-
ster book. Yet Clapp recognized clearly that what we theonom-
ists are suggesting is the very opposite of simplicity. We have
never pretended otherwise, contrary to Clapp.

The point is that there are hundreds of such details to be
sorted out and applied to the contemporary situation. Recon-
structionism does not actually provide the clear, simple, incon-
testably “biblical” solutions to ethical questions that it pretends
to, and that are so attractive to many conservative Christians.
Reconstructed society would appear to require a second encyclo-
pedic Talmud, and to foster hordes of “scribes” with competing
judgments, in a society of people who are locked on the law’s
fine points rather than living by its spirit (p. 23).

To which I replied:

37. Rodney Clapp, “Democracy as Heresy,” Christianity Today (Feb. 20, 1987). I
replied to his article, accusation by accusation, in my report, “Honest Reporting as
Heresy,” a newsletter sent out by the Instilute for Christian Economics in 1987,
which none of the Westminster authors notes. Reprinted as Appendix B, below.
