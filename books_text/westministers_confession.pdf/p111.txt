A Positive Biblical Confession Is Mandatory 87

ties in the war against Satan's earthly kingdom, not to sit by the
fire and reminisce about the bad old days.

Van Til’s declaration of war against philosophical Athens
was total. But in making this declaration, he necessarily called
the Church of Jesus Christ back into the war room. Van Til
insisted that the Church had been epistemologically AWOL —
absent without leave — for almost 2,000 years. The Church, not
covenant-breaking man, must declare the terms of war and
peace. The kingdom as a corporate entity is to rest judicially on
God’s terms for man’s surrender in history."” The kingdom of
God is God’s civilization, both in heaven and on earth, in time
and eternity."*

Van Til’s declaration of covenantal war, if taken seriously,
must be accompanied by several events: letting down the draw-
bridge, sounding the trumpets, and arming the troops. The
troops must be given their marching orders, The idea of per-
manent comfort inside the castle is an illusion; the castle is
useful only during temporary sieges. As surely as the city of
Jerusalem was a death trap for those who occupied it before
David captured it, and again when Titus captured it over a
thousand years later, so is the familiar comfort of the ecclesias-
tical castle when serious enemies are determined to take it and
raze it. The ghetto is a place to avoid during a pogrom. Van
Til warned us that the pogroms are coming, for as covenant-
breakers progressively recognize the threat that covenant-keep-
ers pose to their way of life, the covenant-breakers will tyran-
nize the Church.

Rushdoony’s postmillennialism also sounded a warning: to
the covenant-breakers. The coming “pogroms” will be spiritual,
the products of the irresistible saving grace of God. But at this

17. Gary North, Unconditional Surrender; God's Program for Victory (8rd ed.; Tyler,
Texas: Institute for Christian Economics, 1988).

18. Gary North, “God’s Govenantal Kingdom,” in Gary North and Gary DeMar,
Christian Reconstruction: What It Is, What it isn’t (Tyler, Texas: Institute for Christian
Economics, 1991), Part I.
