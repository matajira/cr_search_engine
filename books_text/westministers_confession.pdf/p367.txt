The Paralysis of the Parachurch Ministries 343

in the world” (I John 4:4). Instead, Christians perceive “meek”
as meaning “meek before men and institutions.”

If this perspective were true, why did the Psalmist say, “I
will speak of thy testimonies also before kings, and will not be
ashamed” (Ps, 119:46)? Why did Solomon the king say, “Seest
thou a man diligent in his business? he shall stand before
kings; he shall not stand before mean men” (Prov. 22:29)?

A Denial of Covenant Theology

Why have Christians achieved so little culturally in the last
two centuries? Why, with the unique exception of Wycliffe
Bible translators, have Christians not built institutions whose
accomplishments dwarf those of their rivals? I think it has
something to do with their progressive abandonment of coven-
ant theology, with its five points: the absolute sovereignty of
God, the doctrine of hierarchical representation, the doctrine
of biblical law, the doctrine of God’s sanctions in history, and
the doctrine of inheritance. The Church does not preach it,
and so it shivers in the shadows of humanist society.

The doctrine of God's absolute sovereignty is proclaimed
today only by a tiny handful of Calvinists. Similarly, the doc-
trine of the continuing authority of biblical law has been denied
by almost every Christian group, including the Calvinists, “No
creed but Christ, no law but love” is the antinomians’ battle cry
of cultural surrender. The doctrine of long-term inheritance —
postmillennialism — is having a revival today, but for well over
a century, Christians have affirmed pessimillennialism: that until
Jesus comes again bodily to reign on earth, the Church will
experience a series of inevitable defeats.

Why such pessimism? Because in a world in which autono-
mous man rather than God is believed to have the final say
regarding personal salvation (“decisions for Christ”), law (“nat-
ural”), and inheritance (“pie in the sky, by and by”), what else
should we expect? So, what can we do to persuade ourselves
and others that such a view of history is wrong? I suggest that
