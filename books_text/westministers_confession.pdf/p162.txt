138 WESTMINSTER’S CONFESSION

go to the Bible, including the law, to discover these great social
principles:

... the Reformed preacher brings a social message because he
finds such a message in God's Word. He finds it in the preach-
ing of the prophets, the Baptizer, Jesus, and his apostles, but
also in many portions of Scripture which are not themselves
sermons. He finds it here and there and everywhere in Scrip-
ture. Determined as he is to declare the whole counsel of God,
he cannot keep silent.”

Would he take the leap? Would he say it? Would he say those
crucial words, “Mosaic law”? Not quite. He drew up to the
edge of the chasm, but he would not leap. He did say this,
however: “The Calvinist sees in the Bible both law and gospel,
The two are interwoven. To distinguish between them is not
only valid but highly necessary. Yet to separate them is to do
violence to Holy Writ. The Old Testament contains both law
and gospel. The New Testament contains both gospel and law.
. .. Both gospel and law are intended for all men.”*

Whoa, there, R. B. For all men? Is that what you said? You
are beginning to sound like Norman Shepherd, who, as we all
suspect, is only a few steps behind Greg Bahnsen. This is why
neither of them teaches at Westminster. Like Hans Brinker,
you are skating on thin ice. Watch out for Edmund Clowney
and his blowtorch!

Abandoning Machen’s Legacy: Stage Two

Four decades after Kuiper’s piece appeared, the Westminster
Theological Journal published Edmund Clowney’s article, “The
Politics of the Kingdom.” Kuiper had written: “Of all men no
one is more firmly convinced than the Calvinist that there can

47. Bid., p. 27.
48. Idem.
