8

SIC ET NON:
THE DILEMMA OF
JUDICIAL AGNOSTICISM

Ef any of you lack wisdom, let him ask of God, that giveth to all men
liberally, and upbraideth not; and it shall be given him. But let him ask
in faith, nothing wavering. For he that wavereth is like a wave of the
sea driven with the wind and tossed. For let not that man think that he
shall receive any thing of the Lord. A double minded man is unstable in
all his ways (fas. 1:5-8).

Westminster’s Theonomy symposium suffers from the paraly-
sis of indecision, an indecision rooted in theological and philo-
sophical double-mindedness. Praising Van Til, they have aban-
doned Van Til. Criticizing theonomy, they offer no alternative
to theonomy. Left to themselves as a faculty, they do not agree
on the answer to that crucial practical question: What is to be
done?? They have attempted to do what no one should ever
attempt: beat something positive with nothing specific.

Indecision is a common academic affliction, but is especially
noticeable on seminary faculties. The faculty members hesitate

1. Gary North, Millennialism and Social Theory (Tyler, Texas: Institute for Chris-
tian Economics, 1990), ch, 13: “What Is To Be Done?”
