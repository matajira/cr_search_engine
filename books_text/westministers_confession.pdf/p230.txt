206 WESTMINSTER’S CONFESSION

civil laws under the Ten Commandments, as illuminated by the
case laws. Simple, no; radical, yes.

Kline and Pluralism

Frame goes after Meredith Kline far more than he goes
after Bahnsen, making this one of the better essays in the book.
He sees clearly that Kline’s system relies on the idea of reli-
giously neutral law (the Noahic covenant): the foundation of
pluralism." This is why the pluralists in the book rely on
Kline. Frame states forthrightly: “Religious neutrality is not
only a wrong goal but also impossible in the nature of the case.
All crime comes from false religion. .. .”* In this sense, Frame
is being faithful to Van Til’s legacy. This is what marks his essay as
exceptional in this collection.

He calls both the theonomists and the intrusionists to get to
work exegetically. I have no objection; I have spent a million
dollars and seventeen years doing this. But I must remind
Frame and Poythress (who calls for the same thing), doing de-
tailed exegesis requires motivation. Which system of theology pro-
vides this motivation? A system that declares that God’s laws
and sanctions are still operational in New Testament times, or
a system that declares all of the Mosaic legislation as an intru-
sion in history that is not in any way judicially binding on civil
governments today?

The answer is obvious. Where is Kline’s exegesis of the law?
{For that matter, where is Kline’s post-1980 exegesis of any-
thing?) Where are his followers’ exegeses of the law of God?
There are none. They are pluralists, one and all, as later essays
in Theonomy: A Reformed Critique indicate. They are unhappy
with Frame’s work. They wish, many years ago, he had been
hired somewhere else. Then they would be able to say today:
“Therefore, Westminster could not hire Frame,” as they say

41, Frame, p. 94.
42. Ibid., p. 98.
