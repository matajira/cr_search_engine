Calvin’s Millennial Confession 351

Will the covenant-breakers overwhelm the covenant-keepers in
history? Will the kingdom of Satan’s leaven replace the king-
dom of God’s leaven in history? No. This is why we are told by
Christ to pray, “Thy kingdom come.” Calvin wrote in the Insti-
tudes: “From this it appears that zeal for daily progress is not
enjoined upon us in vain, for it never goes so well with human
affairs that the filthiness of vices is shaken and washed away,
and full integrity flowers and grows.”* But what about the
ungodly? On this point, Calvin had no doubts.

Meanwhile, he protects his own, guides them by the help of his
Spirit into uprightness, and strengthens them to perseverance.
But he overthrows the wicked conspiracies of enemies, unravels
their stratagems and deccits, opposes their malice, represses
their obstinacy, until at last he slays Antichrist with the Spirit of
his mouth, and destroys all ungodliness by the brightness of his
coming.”

The Kingdom of God

Calvin saw the kingdom of God as advancing throughout
history. “Again, as the kingdom of God is continually growing
and advancing to the end of the world, we must pray every day
that it may come: for to whatever extent iniquity abounds in the
world, to such an extent the kingdom of God, which brings along
with it perfect righteousness, is not yet come.”* This is an im-
portant passage, for it shows that Calvin saw the two kingdoms
as mutually exclusive: as one advances, the other retreats. They
cannot both advance at once. Thus, any discussion of the ad-
vance of God’s exclusively ecclesiastical kingdom paralleling Sat-
an’s advancing external, culiural kingdom — a basic theme of

6. Ibid., UE:XX:42.
7. Idem.

8. Calvin, Commentary on a Harmony of the Evangelists: Matthew, Mark and Luke
(Grand Rapids, Michigan: Baker Book House, [1555] 1979), I, p. 320: Matt. 6:10.
