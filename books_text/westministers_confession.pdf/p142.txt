118 WESTMINSTER’S CONFESSION

cross and of our salvation by the strength of his hand, and
finally hold all power in heaven and on earth. All this he does
until he shall lay low all his enemies [I Cor. 15:25; cf. Ps. 110:1)
(who are our enemies too) and complete the building of his
church. This is the true state of his Kingdom; this is the power
that the Father has conferred upon him, until, in coming to
judge the living and the dead, he accomplishes his final act.*

Finally, what is the relationship between the biblical doctrine
of sanctification — definitive, progressive, and final - and the
biblical doctrine of the ascension? What is the role of the doc-
trine of progressive sanctification in Westminster’s confession?
Is progressive sanctification in history restricted to the regener-
ate heart, the institutional Church, and Christian families? If
so, on what biblical basis is it so limited? Why can’t there be
progressive sanctification in civil government? Why not in the
economy? There was under the Old Covenant: “But thou shalt
remember the Lorn thy God: for it is he that giveth thee pow-
er to get wealth, that he may establish his covenant which he
sware unto thy fathers, as it is this day” (Deut. 8:18). Why not
progress in society generally? What about in education? Sci-
ence? Technology? Does Westminster’s faculty have a doctrine
of progress for New Covenant history? Can any amillennialist
or premillennialist have a doctrine of progress for New Testa-
ment history?*

Conclusion

These are a few of the questions that we hope Westminster’s
faculty will answer some day. But will we have to wait another
seventeen years?

35, Ibid, WEXVI:16.
36. North, Millennialism and Social Theory, ch. 4.
