The Question of Inheritance 45

Seminary. (In a peculiar sense it still is. Faithful OPC mem-
bers kcep sending in checks to keep CRC and PCA faculty
members employcd.) For ecclesiastical old times’ sake, perhaps,
they pay greater attention to Bahnsen.* Fourth, Bahnsen’s
book is an apologetic. It has a narrower focus and a more
limited goal than Institutes of Biblical Law or my Tools of Domin-
ion. The scholar who attempts to refute it is not risking getting
entrapped in some academic mine ficld he is not familiar with.
Fifth, Bahnsen has not written very much over the years. The
critic is less likely to get sandbagged by the classic retort: “If
Professor Dork had just read my book on. . . .” The critics did
not have to read very much in order to form a theologically
defensible opinion of theonomy, or so they seem to have be-
lieved. If they believed this, they were wrong. They needed to
pay far more attention to his arguments. Judgment cometh!

On the One Hand; On the Other

The essays in the Theonomy critique were written by men
holding advanced theological degrees and, at least at some
point during the five-year effort to produce it, who were in
some way connected with Westminster Seminary. Because
Westminster Seminary has long enjoyed the reputation as the
most academic of Presbyterian-related conservative seminaries,
and perhaps even the most academically rigorous of all Bible-
affirming seminaries, every essay in this book should have been
a cut above the fundamentalist diatribes that have greeted the
work of theonomists since about 1985. Such is not the case.
Compared to Timothy Keller, Dave Hunt is a pillar of scholar-
ship and sclf-restraint. Compared to John R. Muether, Wayne
House is Augustine.

23. Today, there are few fulltime WTS professors who belong to and atlend an.
Orthodox Presbyterian Church.

24. One of the authors, Dennis Johnson, was his classmate at Westmont College
and at Westminster,
