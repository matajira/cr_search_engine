Sic et Non: Judicial Agnosticism 217

omists: How? From that point on, it is all downhill. The re-
mainder of the essay is an extended no to biblical law.

Let us review the Old Testament’s capital sanctions. First,
Old Covenant law required the witnesses to take the lead in
executing the convicted criminal (Deut. 17:7). Second, death by
stoning was mandatory in most capital crimes. All the men of
the local community were to participate (Deut. 21:21). Ged has
not changed these laws. Of all the applications of biblical law
that I have proposed, none has received the ridicule and out-
rage that this one has, yet the case law texts are quite clear.
Why such resistance? The critics cannot bring themselves to be-
lieve that a Christian would take these specified requirements
seriously. Even those Christians who still favor capital punish-
ment want it done behind sealed walls by a paid executioner.
They do not want to participate personally in such an act of
lawful public vengeance. In short, they do not want to become fully
responsible biblical witnesses. This was Adam’s sin, too.”

Basically, Christians really do believe that the God of the
Old Testament was - and I stress was - a barbarian. They
would deny this verbally, if questioned; nevertheless, they
accept it psychologically, Marcion was a second-century heretic
who said that the fierce God of the Old Testament was differ-
ent from the benign God of the New Testament. And when it
comes to a choice between Marcion’s theory of the Bible’s two
gods — Old Testament vs. New Testament ~ and theonomy’s
assertion of a continuity between Old Covenant civil sanctions
and New Covenant civil sanctions, they choose operational Mar-
cionism every time.® They see stoning as a mark of this barba-
rism. They really will not use the word torture when describing

66. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
‘Texas: Institute for Christian Economics, 1986), pp. 122-25.

67, Gary North, The Dominion Covenant: Genesis (2nd ed; Tylev, Texas: Institute
for Christian Economics, 1987), Appendix E: “Witnesses and Judges.”

68. See my section on “The Continuing Heresy of Dualism” in the Epilogue,
“What Are Biblical Blueprints?", in all ten volumes of the Biblical Blueprint Series.
