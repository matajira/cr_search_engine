186 WESTMINSTER’S CONFESSION

areness of the realized eschatology taught in Scripture — that
eventually God must somehow “get in his licks” and “settle
things” in history, as distinct from eternity. But what is the
eternal order other than the consummation of history, the histori-
cal process come to és final fruition? The new heavens and
earth, inaugurated at Christ’s return, will be the climactic vindi-
cation of God’s covenant and, so, his final historical triumph, the
ultimate realization of his purposes for the original creation,
forfeited by the first Adam and secured by the last. Inherent in
both a postmil and a premil outlook, it seems, is the tendency,
at least, toward an unbiblical, certainly un-Reformed separation
or even polarizartion of creation and redemption/eschatology.”*

The New Heavens and New Earth are exclusively future, he in-
sists, contrary to Isaiah 65:17-23. Professor Gaffin preaches a
“realized eschatology,” except when it actually comes to real
realized eschatology. Then he preaches deferred eschatology:
victory beyond history.

He tells us that Jesus secured what Adam forfeited. Indeed,
Christ regained title to the whole world.”” Adam had the legal
authorization from God to leave an inheritance to his heirs. So
does Jesus. But amillennialists insist that Jesus merely secured
title; title will not be transferred to His people progressively in history.
Again, this is “definitivism” apart from progressivism; it is the
fundamental theological error of all amillennialism. It has no
vision of the progressive realization of Christ’s definitive con-
quest in history. Christ’s conquest in history is assumed to be
based exclusively on power, not on covenantal faithfulness, and
it will be achieved only ultimately, i.e., outside of history: in
heaven (Church Triumphant) and at the end of history
(Church Resurrected). It supposedly has nothing to do with
the Church Militant (history). In amillennialism, there is no

26. Gaffin, p. 222n.

27. Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth,
‘Texas: Dominion Press, 1987}, pp. 61-62.
