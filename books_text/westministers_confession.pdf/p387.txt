Books for Further Reading 363

Sutton, Ray R. That You May Prosper: Dominion By Covenant.
Tyler, TX: Institute for Christian Economics, 1987. A detailed
study of the five points of the biblical covenant model, applying
them to church, State, and family.

General Works on Eschatology

Clouse, Robert G., ed. The Meaning of the Millennium: Four
Views. Downers Grove, IL: InterVarsity Press, 1977. Advocates
of the four major views of the millennium present each case.

Erickson, Millard J. Contemporary Options in Eschatology: A
Study of the Millennium. Grand Rapids, M1: Baker, 1977. Exam-
ines modern views of eschatology: millennium, and tribulation.

Works Defending Postmillennialism or Preterism

Adams, Jay. The Time Is At Hand. Phillipsburg, NJ: Presbyte-
rian and Reformed, 1966. Amillennial, preterist interpretation
of Revelation.

Alexander, J. A. The Prophecies of Isaiah, A Commentary on
Matihew (complete through chapter 16), A Commentary on Mark,
and A Commentary on Acts. Various Publishers. Nineteenth-cen-
tury Princeton Old Testament scholar.

Bahnsen, Greg L. and Kenneth L. Gentry. House Divided:
The Break-Up of Dispensational Theology. Ft. Worth, TX: Domin-
ion Press, 1989. Response to H. Wayne House and Thomas
Ice, Dominion Theology: Blessing or Curse? Includes a comprehen-
sive discussion of eschatological issues.

Boettner, Loraine, The Millennium. Revised edition. Phillips-
burg, NJ: Presbyterian and Reformed, (1957) 1984. Classic
study of millennial views, and defense of postmillennialism.
