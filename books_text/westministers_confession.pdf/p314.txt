290 WESTMINSTER’S CONFESSION

newsletter as Appendix C, just to make everything as clear as
possible.) Understand that I am reprinting this letter only
because of Muether’s outrageous accusation against theonomy.

Lest anyone imagine that I wrote this because I had an
advance copy of Muether’s essay (I didn’t), I should point out
that it has been my policy to send back large donations to
donors to make sure that they are not sending money that is
owed to their local church. In November of 1988, I sent back
a $25,000 check with a warning to the donor that if he was a
church member, he owed 10% to his church before he owed
any other organization.

Your donation of $25,000 is much appreciated. It is a very
large donation. While it is not normal for recipient organizations
to refuse donations, or in any way discourage them, | want to
get things clear in both our minds. It is my policy to recom-
mend that donors tithe to their churches before making dona-
tions to ICE. I do this because parachurch ministries have invad-
ed many of the traditional areas of church service, just as the
State has, and this has weakened the churches. I also believe
that the tithe is owed to God through the church.

Naturally, I have good uses for the donation. But I don’t
want to take the money on terms that will put either of us on
God’s hotseat. If you are not a member of a church because you
are not a Christian, then I guess ICE is better than most places
to spend the money. But if you’re a church member, I want us
both to be sure that ICE isn’t draining off moncy that belongs
elsewhere.

T will wait for you to let me know that for sure you want the
money to go to ICE before I cash the check.

He was not a church member, so he sent it back. Only then did
ICE cash it.

In Biblical Economics Today, Vol. XIII, No. 1 (Dec/Jan. 1991),
I wrote the following on the centrality of the church. The essay
is titled, “Tithing and Submission.” It proclaims the doctrine of
