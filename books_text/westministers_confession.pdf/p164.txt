140 WESTMINSTER’S CONFESSION

fled from Egypt. He raised up Aholiab and Bezaleel and en-
dowed them with great technological skills, so that they could
carry out the building of the tabernacle. He also enabled them
to teach others. But if one’s vision for Church history is a
ceaseless wandering in the wilderness — a Christian ecclesiastical
ghetto called the kingdom of God in history — then any sugges-
tion of God’s endowing His people with cudéural talents implies
His burdening them with cultural responsibility. This, above all,
is what pietists resent and reject. So, they deny Christendom.
Clowney, like all of his amillennial colleagues at Westmins-
ter, preached the hope of our future resurrection, What he
and they never, ever preach is a theology of Jesus’ past ascen-
sion. IT can hardly overemphasize this. It is this implicit denial
of the historic cultural impact of Jesus’ ascension that is at the
very heart of their worldview. Listen to Clowney’s exhortation:
“The politics of the kingdom of heaven is the politics of faith,
hope and love: faith that confesses the risen Savior, hope that
looks for his appearing, love that is enflamed by his sacrifice on
the cross. Only the realism of resurrection hope can sustain the
Christian as a pilgrim travelling home.”*’ Here it is: pietism
with a vengeance, It is the pilgrim motif - a pilgrimage out of
cultural responsibility in this world, not into it. It is a pilgrimage
of suffering, not a pilgrimage of conquest. We supposedly never
enter the Promised Land on earth and in history; our march-
ing orders are lo march in circles until Jesus comes again. “The
heavenly community of Christ is called to an earthly pilgrim-
age. The people of God may not abandon the program of his
kingdom — ‘if so be that we suffer with him, that we may be
also glorified with him’ (Rom. 8:18).”** Nowhere in Clowney’s
theology is the intensely judicial doctrine of the Lord’s Supper:

52. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Insti-
tute for Christian Economics, 1990), ch. 34: “The Ability to Teach.”

58. Clowney, p. 308.

54. Thid., p. 308,
