The Paralysis of the Parachurch Ministries 345

— the person who wilfully refuses to join a local church or take
communion — announces that he prefers historical impotence
to influence, irresponsibility to responsibility. God then gives
this to him. We can see this process institutionally in the recent
demise of the parachurch ministries.

The Crisis of the Parachurch Ministries

Again and again, the leaders of these ministries have refused
to submit themselves and their organizations to the formal
judgment of a local Church or a national ecclesiastical body.
Even when they do formally submit, as soon as they are threat-
ened with discipline, they remove themselves from any ecclesi-
astical jurisdiction. One by one, they have faded in influence.
The 1980’s have brought most of them down. The others are
struggling mightily for mere survival. Only the women’s minis-
tries persist: Phyllis Schafly’s, Beverly LaHaye’s, and James
Dobson’s ministry to women. (Even Dobson is now complaining
publicly of financial cuts.) The Christian women of America
have pulled the financial plug on these ministries, and in
American Christianity, this means bankruptcy. Women write most
of the donation checks. Paying the pipers, they call the tunes.

The case of Jim Bakker is the most glaring. He thought that
he was above God’s judgment. He refused to submit to God’s
laws governing marriage and debt. He refused to honor the
Assemblies of God’s threatened covenant lawsuit against him.
The result was not just the decline of his ministry. He is in jail
for a long, long time. First, the humanist media brought judg-
ment against him. Then the civil government did. Spurning the
Church’s covenant lawsuit, he came under the state’s.

Unleashing a media feeding frenzy, Bakker made his peers
victims. Jerry Falwell tells the story of the time he got into a
cab in the Northeast. The cab driver was staring into the rear
view mirror at him. One of Falwell’s assistants asked: “Do you
know who this is?” The cabby answered: “I sure do. That
bimbo sure got you, didn’t she?” He had confused Falwell with
