Sie et Non: Judicial Agnosticism 223

This is an economic disaster for a victimized wife. If she is
guilty of leaving without a valid biblical reason, she probably
keeps the children anyway, which is covenantally disastrous for
the children. Johnson may choose to argue that the guilty
party will surely be excommunicated. As far as the children are
concerned, so what?

The point is, the State must adopt some standard of guilty
and innocent. It must enforce some system of sanctions. The
question is: Which standard? What sanctions? Johnson and his
colleagues are remarkably silent about this obvious application
of the principle of theonomy. All the nouthetic counselling”
in the world will not overcome one basic problem: if the State’s
laws of divorce are not biblical, then they are anti-biblical. This is
Van Til’s legacy: there is no neutrality. But the present faculty of
Westminster refuses to adopt this legacy as its own, Better to
adopt pluralism for civil law and pictism (and suffering) for the
Christian heart. In short, they have adopted this principle of
civil justice: covenant-breaker’s rights.

Pietism Revisited

Johnson proclaims the traditional pietist’s concept of purity:
the personal serenity of one who has attained moral purity in the midst
of a cultural sewer. This is the “sewer serenity” doctrine of pro-
gressive personal sanctification according to Ed Norton (1950's)
and the Teenage Mutant Ninja Turtles (1990's). It sure sounds
like Eastern mysticism to me, But, then again, pietism always
has been innately mystical.

The pietist docs not usually want to sound like a cultural
retreatist. So he adopts the language of a higher calling, a
higher self-discipline. He says things like this: “The punish-
ments of the Mosaic Law belong clearly to the old order, and
thus they point ahead to the higher privilege and the resultant

82. See above, p. 37.
