Calvin’s Divided Judicial Legacy 59

Testament laws as examples of natural law theory in history.
He advocated the imposition of public execution for many of
the infractions listed in the Old Testament’s Mosaic law. ‘Shus,
his defense of natural law theory was of a very different charac-
ter from anything proposed by post-Newtonian, post-Darwin-
ian, post-Heisenberg, and post-Van Til Calvinist defenders of
“Christian” political pluralism, who would strip away every
trace of the Mosaic law from contemporary civil law and return
us to Noah, whose only direct command from God to impose
a specific negative sanction involved the crime of murder.*
Having abandoned both Calvin and Van Til, they would return
us to the unitarian politics of Thomas Jefferson and James
Madison, and do so in the name of Jesus.

God’s Sanctions in History

Here we find a similar discrepancy. Calvin wrote in his
sermons on Deuteronomy that God’s positive and negative
sanctions apply directly to individuals in history. If this is true,
then it becomes possible for men to construct ethical theory in
terms of God’s law. If covenant-keepers as @ class are generally
blessed in history because of their outward and inward obedi-
ence to God's law, and covenant-breakers as a class are general-
ly cursed in history because of their rebellion against God’s law,
then the expansion of Christian civilization is assured. On the
other hand, to the extent that this positive covenantal correla-
tion does not apply in history, it becomes less possible for men
to construct ethical theory in terms of God’s law. If covenant-
keepers as a class are not prediciably blessed in history, and
covenant-breakers as a class are not predictably cursed in history,

38. John Murray, Principles of Conduct: Aspects of Biblical Ethics (Crand Rapids,
Michigan: Eerdmans, 1957), pp. 112-13. He is followed in this argument by dispen-
sationalists II. Wayne House and Thomas D. Ice, Dominion Theology: Blessing or
Curse? (Portland, Oregon: Multnomah Press, 1988), pp. 126-27. It is interesting that
the dispensationalist critics were forced to appeal to a traditional Reformed view of
the covenants in order to defend their position.
