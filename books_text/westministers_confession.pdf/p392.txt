368 WESTMINSER’S CONFESSION

conversion of the Jews; this volume reproduces his early argu-
ments for the historic postmillennial position, and his later
arguments against it.

Schlissel, Steve and David Brown. Hal Lindsey and the Restora-
tion of the Jews. Edmonton, Alberta, Canada: Still Waters Revival
Books, 1990. A Jewish-born Reconstructionist pastor responds
to Hal Lindsey's claim that Christian Reconstruction is anti-
Semitic. Schlissel’s work is combined with David Brown’s work
that demonstrates that postmillennialism is the “system of pro-
phetic interpretation that historically furnished the Biblical
basis for the most glorious future imaginable for the Jews!”

Sutton, Ray R. “A Postmillennial Jew (The Covenantal Struc-
ture of Romans 11),” Covenant Renewal (June 1989). Sutton has
a conversation with a postmillennial Messianic Jew.

Sutton, Ray R. “Does Israel Have a Future?” Covenant Re-
newal (December 1988). Examines several different views of
Israel's future, and argues for the covenantal view.

Toon, Peter, ed. Puritans, the Millennium and the Future of
Israel: Puritan Eschatology 1600-1660, Cambridge: James Clarke,
1970. Detailed historical study of millennial views with special
attention to the place of Israel in prophecy.
