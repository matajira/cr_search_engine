The Question of Inheritance 23

dominant force at Westminster Seminary, which it still is. The
first stage of the inheritance was completed during stage two:
from the Old Princeton’s postmillennial optimism to the New
Amsterdam’s amillennial pessimism.

This vision of historical despair was best articulated by Van
Til. Instead of adopting the view of God’s sanctions in history
that is presented in Leviticus 26 and Deuteronomy 28 — where
covenant-breakers get weaker over time, and covenant-keepers
get stronger — he reversed the roles played by each group. He
asserted that as each side becomes more self-conscious and
more consistent (Lewis’ vision), covenant-breakers will become
culturally dominant, while covenant-keepers will lose influence
and become increasingly tyrannized by their enemies. Van Til
wrote:

But when ail the reprobate are epistemologically sclf-conscious,
the crack of doom has come. The fully self-conscious reprobate
will do all he can in every dimension to destroy the people of
God. So while we scek with all our power to hasten the process
of differentiation in every dimension we are yet thankful, on the
other hand, for “the day of grace,” the day of undeveloped
differentiation. Such tolerance as we receive on the part of the
world is due to this fact that we live in the earlier, rather than in
the later, stage of history, And such influence on the public
situation as we can effect, whether in society or in state, presup-
poses this undifferentiated stage of development>

The third event was institutional: the accession of Edmund
Prosper Clowney, S.T.M., to the office of Dean of Academic
Affairs in 1963, a position that he held for almost 20 years, In
the 1965 academic year, he became the acting president of the
seminary, a newly created office, and then in 1966, the year he
was awarded his honorary doctorate from Wheaton College, he

5. Van Til, Common Grace (1947), in Common Grace and the Gospel (Nutley, New
Jersey: Presbyterian & Reformed, 1972), p. 85.
