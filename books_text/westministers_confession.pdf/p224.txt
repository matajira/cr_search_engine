200 WESTMINSTER’S CONFESSION

leagues.) He does not cite any of the following books: James
Jordan’s The Law of the Covenant, my economic commentary on
Genesis, The Dominion Covenant: Genesis, and my commentaries
on Exodus: Moses and Pharaoh, The Sinai Strategy, and Tools of
Dominion. Well, this is not quite true. He does refer to my view
on stoning, which appears in The Sinai Strategy, but he refers to
H. Wayne House and Thomas D. Ice’s book, Dominion Theology:
Blessing or Curse?, as his authority.° He has obviously not
looked at my books. This is understandable; I am not the issue,
since I was not rejected for a job at Westminster, never having
applied. Bahnsen is different.

Dr. Longman insists that the penalties in the Old Covenant
were flexible. This means that the maximum penalty was al-
ways only that: a maximum.” I agree entirely; this is the the-
sis of my book, Victim’s Rights, which is basically a 300-page
extract from Tools of Dominion, The key question that Dr, Long-
man fails to address is this: On what basis could the judges
have imposed a penalty less than the maximum? The answer,
biblically, is easy: the victim of the crime specified the penalty. The
standard interpretation of the lex talionis that the rabbis have
taken for over a thousand years is that the judges could substi-
tute a monetary payment for a strict “eye for eye” penalty.”
TI argue that the victim’s right to substitute penalties keeps a
godly society from becoming the victim of arbitrary civil judges.
These two crucial issues are ignored by the defenders of judi-
cial flexibility: (1) how to place judicial limits on civil judges; (2)
how to defend the rights of the victim.

26. Ibid, p. 47n.

27. Tbid., p. 52.

28. Cf. Rabbi Moses ben Nachman (Nachmanides, the Ramban), Commentary on
the Torah: Exodus (New York: Shilo, [1267?] 1973), p. 868. The same position was
defended by the early nineteenth-century founder of the movement that has become
known as Orthodox Judaism, Samson Raphael Hirsch: The Pentateuch Translated and
Explained, wanslated by Isaac Levy, 5 vols. (rd ed; Gateshead, London: Judaica
Press, [1967] 1989), II, p. 815.
