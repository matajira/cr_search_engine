Calvin's Divided Judicial Legacy 49

tion. But in several key doctrinal areas, he was confused. I do
not mean merely muddled; I mean double-minded. He pro-
claimed opposite positions on different occasions. He offers a
“yes” on one occasion and a “no” on another. This litany of sic
ef non has continued down through the centuries in Calvinism.
This dualism has led to the creation of rival wings within Cal-
vinism, wings that are still flapping against each other. As a
result, Calvinism does not soar; it scurries around on the
ground like a frightened chicken.

Calvin’s confusion parallels the confusion of the Christian
Church from the fifth century onward, This confusion is closely
related to the biblical covenant model; indeed, it is an historical
manifestation of that covenant’s five points: the transcendence
of God, the hierarchy of institutional authorities, the law of
God, the sanctions of God in history, and the millennium. The
early Church correctly formulated the Trinitarian doctrine of
God. No one in the camp of the orthodox is suggesting the
need to revise the early creeds on this point. Had the Church
failed here, we would all be Arians, or even worse — just like
the vast majority of our neighbors are today. On the question
of the absolute sovereignty of God, however, the Pelagians
steadily triumphed over the Augustinians in the West after
Augustine’s death in 430. It was only with the revival of Augus-
tine’s doctrine of predestination by Luther and Calvin that the
Reformation began to recover the abandoned Augustinian
heritage.

On the other four covenantal doctrines, there has never
been any agreement. In 1054, Eastern Orthodoxy split with
Western Catholicism over the question of proper hierarchy:
Pope vs. Patriarch, Church vs. State. Questions of Church and
State in the West came to a head in the eleventh century, cul-
minating with King Henry IV’s decision to stand barefoot in
the snow at Canossa for three days in 1077, in his successful
attempt to get Pope Gregory VII to remove his 1076 excom-
