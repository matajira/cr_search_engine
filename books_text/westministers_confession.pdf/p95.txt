Calvin’s Divided Judicial Legacy 71

they also reject the Christian foundations of that theory — the
foundations that Van Til’s apologetics destroyed.

In the case of the pluralists on the faculty of Westminster
Seminary, they publicly proclaim their commitment to Van
Til’s apolegetics, yet they steadfastly ignore the implications for
social and political theory of his rejection of natural law theory.
They cling to Van Til’s pessimistic amillennialism — itself a
departure from Calvin - and then they justify their rejection of
God’s predictable, biblical law-based sanctions in history on this
basis. Some of them even come in the name of both pluralism
and Calvin. But they have this nagging problem: Calvin's hand-
ling of Servetus. They have refused for sixty years to address this
thorny political problem, yet it is at the heart of Calvin’s view
of society. It is time for every Calvinist to ask himself and his
seminary instructors these questions:

If it was morally and judicially wrong for Calvin to have
approved of the execution of Servetus, then how much of Cal-
vinism must we scrap, and on what biblically exegetical basis?
What does this comprehensive theological replacement for
Calvin's equally comprehensive worldview look like? Finally, why
haven't post-1788 Calvinists offered us this alternative?

Students at Westminster Seminary have been unclear from
the opening of the seminary in 1929 regarding the conflicting
legacies of Calvin and Van Til. This should not be surprising.
Van Til did his best to cover up these conflicts throughout his
career, and not even Rushdoony and Bahnsen could get him to
clarify his position. His students did not perceive that there was
a problem, since Westminster Seminary rarely (if ever) assigns
a book by Calvin, let alone his sermons on Deuteronomy. What
is surprising, however, is that the faculty decided in 1990 to go
into print with Theonomy: A Reformed Critique, with these conflic-
ting legacies visible to the careful reader. They have now
opened that controversial can of worms that Van Til spent half
