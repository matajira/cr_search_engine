Sie et Non: Judicial Agnosticism 191

screening mechanisms for all literate men in a hierarchically
structured society. The centralized bureaucracy of the Roman
Church became the model for civil government. Every bureau-
cracy must screen access to its positions. The popes, kings, and
princes all recognized this fact early, and they sought to exer-
cise control over the universities. The pagan Emperor Freder-
ick IT of Sicily in the carly twelfth century established the Uni-
versity of Naples in order to secure total State control over all
civil justice.* Others who saw their opportunity were the vari-
ous heretical religious orders. The “Spiritualists” invaded the
universities and could not be driven out.’ The universities of
Paris, Oxford, and Cambridge all fought and lost the war
against heresy during their initial three centuries. Only after
the Reformation was orthodoxy restored at the English univer-
sities, and they soon moved towards Arminianism. (Emmanuel
College at Cambridge did become a center for Calvinism in the
late sixteenth and early seventeenth centuries, but then came
under Archbishop Laud’s attacks.)®

The English Puritans recognized that the humanism and
rationalism of Cambridge and Oxford constituted a major
problem, but they were unable to take control of those two
sacrosanct institutions, even after military victory in a civil
war.’ The Puritans in New England built Harvard College in
a wilderness in 1636, adopted a European rationalist curricu-
lum, and prayed that immersion in Latin, Greek, Hebrew,

universities privileges enjoyed by the religious orders. See Gerhard W. Ditz, “The
Protestant Ethic and Higher Education in America,” Oxford Review of Education, 1V
(1978), pp. 164-65.

4. Ernst Kantorowicz, Frederick the Second, 1194-1250 (New York: Ungar, [1931]
1957), pp. 132-35.

5. Friederich Heer, The Medieval World: Europe, 1100-1350 (New York: World,
1962), chaps. 9, 10,

6. John Morgan, Godly Learning: Puritan Attitudes towards Reason, Learning and
Education, 1560-1640 (Cambridge: Cambridge University Press, 1986), pp. 247ff.

71. Ibid., Conclusion.
