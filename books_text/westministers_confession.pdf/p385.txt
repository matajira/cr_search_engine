BOOKS FOR FURTHER READING

Theonomic Studies in Biblical Law

Bahnsen, Greg L. By This Standard: The Authority of God’s Law
Today. Tyler, TX: Institute for Christian Economics, 1985. An
introduction to the issues of biblical law in society. (available in
Spanish)

Bahnsen, Greg L. No Other Standard. Tyler, TX: Institute for
Christian Economics, 1991. A detailed response to the major
criticisms of theonomy, including Theonomy: A Reformed Critique.

Bahnsen, Greg L. Theonomy in Christian Ethics. Nutley, New
Jersey: Presbyterian and Reformed, (1977) 1984. A detailed
apologetic of the idea of continuity in biblical law.

DeMar, Gary. God and Government, 3 vols. Brentwood, Ten-
nessee: Wolgemuth & Hyatt, 1990. An introduction to the fun-
damentals of biblical government, emphasizing self-govern-
ment.

Jordan, James, The Law of the Covenant: An Exposition of Exo-
dus 21-23. Tyler, TX: Institute for Christian Economics, 1984.
A clear introduction to the issues of the case laws of the Old
Testament.
