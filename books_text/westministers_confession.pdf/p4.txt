Copyright, Gary North, 1991
Van Til cover photo courtesy of
Westminster Theological Seminary.

Torn picture reproduction courtesy
of Robert Langham Photography.

 

Library of Congress Cataloging-in-Publication Data

North, Gary,
Westminster’s confession : the abandonment of Van
Til’s legacy / Gary North.
p. cm.
Includes bibliographical references and index.
ISBN 0-930464-54-0 (alk. paper) : $14.95

1, Theonomy. 2. Calvinism. 3. Dominion
theology. 4. Law (Theology). 5. Reformed
Church - Doctrines. 6. Westminster Theological
Seminary (Philadelphia, Pa. and Escondido, Ca.).
7. Van Til, Cornelius, 1895-1988, 8. Sociology,
Christian — United States. 9. Religious pluralism
— Christianity - Controversial literature.

IL. Title.
BY82.25.T443N69 1991
230°.046 - dc20 91-7200

CIP

 

Institute for Christian Economics
P. O. Box 8000
‘Tyler, Texas 75711
