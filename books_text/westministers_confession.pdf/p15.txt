Foreword xv

the analogy from which philosophers had reached the very
idea of law. It has deprived political and juristic thought of the
-pattern to which they had conceived of government and law as
set up. Physics had been the rock on which they had built.”"

Problem: to the extent that Christian scholars have adopted
the “latest findings” of the secular humanist world as their pro-
fessional standard of academic discussion and inquiry, they are
trapped on board without lifeboats. But now the good ship
Rational Cause and Effect is visibly sinking.

The Newtonian Ideal

How long had this connection between physics and civil law
been true? When did physics become the primary model for
social theory?” From the seventeenth century, especially after
1660. When Cromwvell’s reign ended and Charles II returned
to the throne, social thought turned from the Bible and medi-
eval (organic) natural law theory to physics. Descartes had sct
the mathematical ideal early in the century; Sir Isaac Newton
and the Fellows of the Royal Society after 1661 established the
mathematical-experimental ideal in physical science, and the
magnitude of their achievements restructured the realm of
social theory.”

This triumph came at the expense of biblical Christianity,
especially Puritanism. Newton was a unitarian (Arian) theologi-
cally, although he kept his theological opinions quict.’* He

11, Roscoe Pound, Contemporary Juristic Theory (Claremont, California: Claremont
Golleges, 1940), p. $4. Gited by R. J. Rushdoony, “The United States Constitution,”
Journal of Christian Reconstruction, XII (1988), p. 35.

12. There is a competing humanist viewpoint, organic social theory, with biology
as the model.

13. Louis I. Bredvold, The Brave New World of the Enlightenment (Ann Arbor:
University of Michigan Press, 1962).

14, Gale E. Christiansen, In the Presence of the Greator: Isaac Newton and His Times
(New York: Free Press, 1984), pp. 470, 564.
