Sie et Non: Judicial Agnosticism 219

has kingdom agents in history, but their New Testament juris-
diction is supposedly confined inside the four walls of the
institutional Church — and only the orthodox churches at that.
In short, there can be no valid concept of Christendom. Chris-
tendom was a heresy of the Middle Ages, and, sad to say, of
Calvin and the pre-1660 Puritans. But we have been freed
from all that by the 1788 revision of the Westminster Confes-
sion. “Free at last, free at last; Lord God half-mighty, we're free
at last!” Free to serve as civil slaves of Satan’s agents in history.

No one has articulated the theology of this new freedom
better in such a short space than Dennis Johnson. The penal
sanctions of the Old Covenant were legitimate, but only be-
cause the people of Israel were formally covenanted to God as
a nation. “Certain penal sanctions belong to categories of laws
that set Israel apart from all the noncovenantal nations as a
holy people, with God’s temple in their midst. . . . Since the
coming of Christ, God’s covenant people are no longer a single
nation that uses physical force and penalties as means to main-
tain the community’s purity and integrity.””) How does he
make such a conclusion? By redefining terms and by obscuring
these new definitions. “Purity” and “integrity” are implicitly
defined as exclusively personal; the “community” is defined as
exclusively ecclesiastical, This is what his essay is designed to do:
redefine the words without explicitly admitting it.

“We've Under Grace, Not Angels!”

He offers the most bizarre argument to defend this thesis
that I have yet encountered. He offers others, but this deserves
special attention. He says that Old Covenant civil law was medi-
ated by angels; when Jesus removed the angels as mediators,
He removed Old Covenant civil law. Trust me; he really says

71. Johnson, p. 176.
