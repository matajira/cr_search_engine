182 WESTMINSTER’S CONFESSION

relentlessly. This does not change anything, but at least it al-
lows Christians, in Gaffin’s words, to get in a few licks.” Thus,
one avoids controversial specific transgressions, such as abor-
tion. I will be more impressed when they focus on this one
issue as the representative transgression of the whole society.
Until then, criticism of the humanist order in general remains
little more than a verbal smoke screen for inaction. It is the
theologians’ systematic refusal to bring a specific covenant
lawsuit against this God-rejecting society. They do not believe
that God will prosecute such a lawsuit in history by imposing
His negative sanctions, so they see no impelling reason to bring
it.

In contrast, the theonomist asks: “What level of progressive-
ly accumulating sanctions is now hanging over a nation that
executes a million and a half unborn infants each year?” There
will be no tenured sccurity anywhere in a society that comes
under such sanctions.

The Consequences of Christ’s Resurrection and Ascension

For many ycars, I have taunted non-theonomists with this
slogan: “You can’t beat something with nothing.” They have
said nothing public in response, but they have not needed to.
Their implicit answer is clear; it is based self-consciously on
their two (or three) pessimillennial eschatologies: “With respect
to social theory, we know we have nothing culturally to offer,
but since God does not really expect the Church to defeat any-
thing cultural in history anyway, nothing is all we need.”

The more intellectually sophisticated among them have
contented themselves with writing critical analyses of modern
humanist culture. By implication, they are calling Christians to
avoid the pits of Babylon. But calling Christians to “Come out
from among them!” without also providing at least an outline
of a cultural alternative to come in to (i.e., to construct) is sim-

23, Ibid, p. 222n.
