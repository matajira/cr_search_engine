Sic et Non: Judicial Agnosticism 2038

system and some incorrect things. Poythress thinks that every-
one is correct, from a certain point of view.

Judicial Simpletons

Frame suggests that “the greatest appeal of the Christian
reconstruction movement, or theonomy, lies in the simplicity
and radicalism of its proposal.”** I reply: radical, yes; simple,
no, We have never regarded our task of reviving biblical casu-
istry as a simple task. Casuistry is difficult. It is simply ridicu-
jous even to hint at this supposed simplicity of our self-imposed
vision. In 1972, in the concluding words of my Ph.D. disserta-
tion on the decline of the New England theocratic view of
economics, I wrote the following:

In a transitional era - one in which the burdens of the inherited
intellectual and cultural paradigm seem too great to bear any
longer — the innovators regard their predecessors as men en-
meshed in a tangled web of conflicting policies. The web no
longer scems to hang together. Under these circumstances, the
innovators are seldom aware of the possibilities for multiple
applications of their own philosophical Archimedean point. It
makes the task of reconstruction appear far easier than it really
is.

Could I have made it any plainer? Do my 650+ pages of exe-
gesis on three chapters of Exodus, limited to a discussion of
economic questions, indicate that I regard this task as sim-
ple?** Of course, I keep forgetting: I don’t count, since I nev-
er applied for a job at Westminster.

34, Frame, “The One, The Many, and Theonomy,” idid., p. 89.

35. Gary North, “From Covenant to Contract: Secularism in Puritan New
England, 1691-1720,” Journal of Christian. Reconstruction, VI (Winter 1979-80), pp.
198-94.

36, Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Tnsti-
tute for Christian Economics, 1990), pp. 209-878.
