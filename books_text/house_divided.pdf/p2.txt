Other books by Greg L. Bahnsen

Theonomy in Christian Ethics, 1977

Homosexuality: A Biblical Perspective, 1978

By This Standard: The Authority of God’s Law Today, 1985

Other books by Kenneth L. Gentry, Jr.

The Christian Case Against Abortion, 1982

The Christian and Alcoholic Beverages, 1986

The Charismatic Gift of Prophecy: A Reformed Analysis, 1986
Before Jerusalem Fell: Dating the Book of Revelation, 1989
The Beast of Revelation, 1989
