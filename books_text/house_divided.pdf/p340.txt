Argumentation Errors 301

4. Dominion Theology evidences a widespread problem in much
theological debate: careless mistakes and fallacious logical argument,

5, Dominion Theology is filled with contradictory statements
that cause the work to be self-destructive,

6. House and Ice are carelessly imprecise in their use of ter-
minology and argument, thus causing misunderstanding through
unnecessary distortion.

7, House and Ice frequently engage in a fallacious style of ar-
gument when they appeal to emotions, particularly when these
appeals are based on misinformation and misguided hypothetical
projections,

8, House and Ice make a number of fallacious sweeping as-
sertions in their presentation, which lead to overstatements,

9, House and Ice make certain technical failures in their re-
search and analysis of a number of matters regarding issues both
of theological understanding and historical fact.

10, House and Ice fall into non sequitur arguments that have a
surface appearance of cogency, but when carefully considered fail
of their intent,
