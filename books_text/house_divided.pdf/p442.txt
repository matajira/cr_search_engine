SUBJECT INDEX

Abomination of Desolation, 271-73
Abrahamic Hope, 152-54
Against Heresies, 261, 306
Amillennialism, 156
Ancient Church

Eschatology, 237-43

See Church Fathers:
Antichrist, 373
Antinomianism, 18-19, 313-14
Anti-Semitism, 53-54, 167, 377-79
Apologetics, 80-83
Armageddon, 333-4
Armageddon Now!, di
Arminianism, 79

Before Jevusatem Fell, 263, 280

Bibliotheca Sacra, xy, xvi

Biola College (Bible Institute of
‘Los Angeles), xiv

The Blessed Hope and the Tribulation,
361

By This Standard, 76-77, 112,113

Calvin College, xi

Calvinism, xx, 3, 14, 80, 147
Law, 46

Capital Punishment, 78, 130-31,
294-95

Ceremonial Law, 34-36, 96-101,
113-15, 126

Charismatic Movement, 318-19

Chiliasm, see Premillennialism

Christian Reconstruction, xxv, 3, 13,
29-44
Academic Blackout, soavii-xiii

  

Anti-Semitism, 53-54, 167
Charismatic Movement, 319-20
Church and Israel, 168-71
Dispensationrdism, xliv, 5-6
History, 13-15, 60-63
Intramural Disagreements, 60-63,
69-72
“Movement: 69-70
Personalities, 64-65
Postmillennialism, 146-47
Preterism, 258
Second Coming, 307-8
Synthesis with world, 81-83
Christian Reconstruction From a Pre-
Tribulational Perspecti
Christian Schools, xii
Christianity Today, xxxviti, 363
Church 293-94
New Israel, 168-69, 373-74
Church Fathers
Church and Israel, 169
Eschatology, 237-45, 249-53
Hermeneutics, 374-75
Jews and Christ, 167
Preterism, 276-80
Church and State, 78-79, 313
Civil Magistrate, 77-78, 125-34
Clemeniine Homilies, 278, 298
Common Grace, 81
Conquest, 312
Council of Ephesus (431), 242
Covenant Theology, xi-xii, xxiv,
80-81, 117, 119, 150, 170-71, 353
Culture, 19-27
Christianization, 193-210

  

407
