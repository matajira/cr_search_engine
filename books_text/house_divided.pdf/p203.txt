158 House Divided

6. Patriarchal and Mosaic era prophecies foresee a time in
earth history, issuing forth from the first Advent of Christ, in
which God’s glory and righteousness will cover the earth (Gen.
22:17; 49:10; Num. 24:17-19).

7. The prophets of the Old Testament continue the hope of
the Gospel Victory Theme when they urge kings and judges to
bow to Christ and promise that the ends of the earth will turn to
God in salvation (Psalms 2; 22; 72; Isa, 2:1-4;9:1-7).

8. The Gospel Victory era will be gained apart from Jewish
exclusiveness and Old Testament ceremonial distinctive and all
saved. people will be on an equal footing with God through Jesus
(Jer. 3:16-17; 31:31-34; 48:47; 49:6, 39).

9. The beginning of the Gospel Victory fruition is with the
ascension of Christ to the right hand of God (Dan. 7:13-14).

10, The New Testament record of Christ's birth reflects on the
Gospel Victory Theme of the Old Testament expectation, show-
ing that Christ’s first coming began the fruition of the promises
(uke i).
