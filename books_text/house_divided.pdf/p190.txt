The Conflict of Expectations 145

ent era being an intercalation in the plan of God, © but all the
while expecting an imminent, secret rapture '° and secret resur-
rection of saints only (the first resurrection), followed by a seven
year tribulation in which the Jewish temple will be rebuilt and
Jewish evangelism without the baptism, sealing, indwelling, and.
filling power of the Holy Spirit "’ will be more successful in seven
years than 2,000 years of Holy Spirit-empowered Church evan-
gelistic labor, '° followed by the visible, glorious Second Coming of
Christ “to the earth,”!? at which point saved Jews of the Old Testa-
ment era and the Tribulation saints will be resurrected (a second.
resurrection), ”° to establish a 365,000 -day”! literal, earthly, politi-
cal kingdom with Jews*? reigning at the highest levels (over their
servants, the saved Gentiles), 23 which kingdom will be over an
earthly population composed also of unresurrected mortals, while
a heavenly city will “be made visible above the earth” for 1,000
years in which dwell glorified, resurrected saints,“ and will wit-
ness the divinely approved reinstitution of memorial animal sacri-
fices,* followed eventually by the second humiliation of Christ,
when His personally administered kingdom revolts against Him

15, ibid, pp. 133-38, 201,

16, ibid., p. 203, One of the most remarkable phenomena exists in regard to
this “secret rapture .” The very verse it is based most solidly on is 1 Thessalonians
4:16, which is the noisiest verse in Scripture! “Tor the Lord himself shall descend
with a shout, with the voice of the archangel, and with the trump of God.”

17, Ibid, p. 263.

18, Ibid.,p. 269 speaks of the salvation of “a multitude that defies enumera-
tion” in the Tribulation, See also pp. 273-74.

19, Ibid., pp. 206, 280, 478, ‘There is no question but that the Lord Jesus
Christ will reign in the theocratic kingdom on earth . .” (p. 498).

20, Hid., pp. 410-11,

21, One thousand years times 365 days (it must be ‘literal,’ they say). Ihid.,
pp. 491f.

22, House and Ice, Dominion Theology, pp. 145, 169, 174, 400-5,

23, Pentecost, Things to Come, pp, 507-8,

24, Pentecost speaks of unresurrected people in the millennium Gbid., pp.
489, 503 #.), as well as glorified people, although the resurrected people will be
floating above the earth in a heavenly Jerusalem (Chapter 31) and “casting its
light, which is the shining of the effulgence of the Son, onto the earth so that ’the
nations of them which are saved shall walk in the light of it” (pp. 577f.).

25, bid, pp. 5128.
