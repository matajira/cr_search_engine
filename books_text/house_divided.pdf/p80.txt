The Reconsiructionist Option 31

between biblical times and our own is so wide; * however, these dif
ferences are not especially relevant to the question of ethical alidity.

That is, it is one thing to realize that we must translate bibli-
cal commands about a lost ox (Exodus 23:4) or withholding pay
from someone who mows fields (James 5:4) into terms relevant to
our present culture (e. g., about misplaced credit cards or remu-
neration of factory workers). It is quite another thing altogether to
say that such commands carry no ethical authority today! God ob-
viously communicated to His people in terms of their own day
and cultural setting, but what He said to them He fully expects us
to obey in our own cultural setting, lest the complete authority of
His word be shortchanged in our lives.

Moreover, it should be obvious that in teaching us our moral
duties, God as a masterful Teacher often instructs us, not only in
general precepts (e.g., “Do not kill,” Exodus 20:13; love one another,”
1 John 3:11), but also in terms of specific illustrations (e. g., rooftop
railings, Deut. 22:8; sharing worldly goods with a needy brother,
1 John 3:17) — expecting us to learn the broader, underlying prin-
ciple from them. Again, those biblical illustrations are taken from
the culture of that day. After the New Testament story of the good
Samaritan, Jesus said “Go and do likewise” (Luke 10:37). It does
not take a lot of hermeneutical common sense to know that our
concrete duty is not thereby to go travel the literal Jericho road
(rather than an American interstate highway) on a literal donkey
(rather than in a Ford) with literal denarii in our pockets (rather
than dollars), pouring wine and oil (rather than modern antiseptic
salves) on the wounds of those who have been mugged. Indeed,
one can be a modern “good Samaritan” in a circumstance that has
nothing to do with travel and muggers whatsoever. Unfortunately
though, this same hermeneutical common sense is sometimes not

4, A critic like Rodney Clapp “Democracy as Heresy,” Christianity Today (Reb,
20, 1987) is seriously misled to think that this question of culture gap is any more
uncomfortable for — or critical of — theonomists than it is for any other school af
thought committed to using the ancient literature of the Bible (whether Old or
New Testamen®) in modern society. The alternative - which any believer should
find repugnant - is simply to dismiss the Bible as anachronistic.
