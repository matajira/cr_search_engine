Hal Lindsey's “The Road to Holocaust” 379

counted their ministries. * But how is this worse than the fact
Lindsey has been married three times?

Lindsey argues that: “Law and Grace are two completely
different systems of approaching God. They are antithetical to
each other. We must approach God by one or the other, but we
can’t mix them. If we try to live by any part of the Laws ystem, we
are obligated to keep the whole system .”® But then later he writes:
“In all ages men have had to come to God by faith.” Since he
agrees with us that even in the Mosaic era, when Israel was defi-
nitely obligated to the Law of God, God was approached only on
the basis of faith, then how can he put down the Reconstructionist
ethic founded on the Law of God as if it presupposes a different ap-
proach to God? Besides, the Reconstructionist does not put the be-
liever under the Law as a means of salvation, despite his distor-
tion of our views !® Interestingly, he proves we are not under the
Law of God but under the requirement of walking in the Spirit by
quoting Remans 13:8-1066 — which cites the Law of God! Under a
heading entitled “The Holy Spirit Did Not Come to Help Us
Keep the Law” his first quoted Bible verse is Hebrews 10:16: ‘I will
put my laws in their hearts, and I will write them on their minds”!
Reference here to Remans 8:3-4 is conspicuous by its absence.

Conclusion

These few brief comments should illustrate the tremendous
problems with Lindsey’s superficial and mean-spirited analysis of
Reconstructionism. He is even less successful than House and Ice
in his assault. In all honesty it seems that the dispensational cri-
tiques of Reconstructionist theology are degenerating to ever
new lows. The y have gone from bad (House and Ice) to worse
(Hunt) to worst (Lindsey).

62, Lindsey, Road to Holocaust, p, 159,

63, Ibid., p. 161, cp. p. 173.

64, Ibid., p. 266,

65, Ibid,, p. 164: “If these men are this far off on their interpretation of some-
thing that is a main theme of the New Testament (i. e., Law vs. Grace), is it any
wonder they are 60 far off on something as complex as Biblical prophecy?” See
also p. 173.

66, Ibid., p. 162,
