278 House Divided

He then cites Matthew 24:19-21 as his lead-in reference and later
refers to Luke 21:20, 23, 24! He even states: “If any one compares
the words of our Saviour with the other accounts of the historian
[Josephus] concerning the whole war, how can one fail to wonder,
and to admit that the foreknowledge and the prophecy of our
Saviour were truly divine and marvelously strange.”*°

Another ancient document that makes reference to the de-
struction of the temple based on Matthew 24:2-34 is the Clementine
Homities.°7 There we read: “Prophesying concerning the temple,
He said: ‘See ye these buildings? Verily I say to you, There shall
not be left here one stone upon another which shall not be taken
away [Matt. 24:3]; and this generation shall not pass until the de-
struction begin [Matt. 24:34]. . . .‘ And in like manner He spoke
in plain words the things that were straightway to happen, which
we can now see with our eyes, in order that the accomplishment
might be among those to whom the word was spoken .™

In Cyprian (A. D. 200-258) we have clear reference to Matthew
24 as referring to Jerusalem’s a. D. 70 fall.°? In the entirety of
Treatise 12 he is dealing with testimonies against the Jews, includ-
ing Christ’s prophecies.

Clement of Alexandria (A. D. 150-215) discusses the Seventieth
Week of Daniel 9 as a past event: “The half of the week Nero held
sway, and in the holy city Jerusalem placed the abomination; and
in the half of the week he was taken away, and Otho, and Galba,
and Vitellius. And Vespasian rose to the supreme power, and de-
stroyed Jerusalem, and desolated the holy place .” ‘°° As a matter

9%. Tbid., 3:7:7. This shows that Eusebius deemed Luke 21 and Matthew 24
to be parallel accounts.

97. Though not written by a noted church father, it is an important late sec-
ond century work that touches on the matter before us, House and Ice boldly
state that preterism is found in “none of the early church writings” (p. 258). Yet,
here is a work that shows carly consideration of the matter, apparently picking up
on views current in that day,

9B, Clementine Homilies, 3:15. See Roberts and Donaldson, Ante-Nicene Fathers,
vol. 8, p, 241,

99, C yprian, Treatises, 12:16, 15, See especially Roberts and Donaldson,
Ante- Nicene Fathers, vol. 5, pp. 507-11.

100. Clement of Alexandria, Miscellanies 1:21.
