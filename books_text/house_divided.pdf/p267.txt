224 House Divided

Firsi, we have the very presence of the Risen Christ with us.
The Great Commission specifically promised this (Matt. 28:20).
He will never leave nor forsake us (Heb. 13:5). Second, since the
Ascension of Christ, we have the indwelling of the Holy Spirit
(John 7:39; 14:16-18), who will convict the world of sin, righteous-
ness, and judgment (John 16: 7-15). In fact, it was “expedient” for
us that Christ go away so that we might have. His presence in the
Person of the Holy Spirit (John 16:7). It is no regression in
redemptive history, no diminishing of our resources, no “mere”
mystical presence: The Holy Spirit’s coming is glorious in every
respect. His coming is “power from on high” (Luke 24:49).

Third, it is the Father’s delight to save sinners (Eze. 18:23;
Luke 15:10; 1 Tim, 2:5), Fourth, we have the Gospel, which, as
mentioned previously, is the power of God (Rem. 1:16; 1 Cor. 1:18,
24), Fifth, we have full access to God in prayer (Heb. 4:16),
through Jesus’ name (John 14:13-14; 15:16; 16:23, 24, 26; 1 John
3:22; 5:14-15), which even promises greater works than Christ
(John 14:12) and opens the full resources of heaven to us (John

 

church during the present age. As a result, Dominionists aften use ‘take over,’
bringing in,’ or ‘establishing the kingdom language” (ibid,, p. 407). It should be
noted that this is no “Reconstructionist” distinctive, for other postmillennialists do
the same: David Brown writes: “That more fidelity on the Church’s part would
have hastened the predicted consummation, is language which we are fully war-
ranted in using [toward the] long promised subjugation of the world to Christ.” And
“There is a satistaction unspeakable in anticipating the endless ways in which the
Spirit may get himself renown, by what he will yet do in and by the church. ...
[T]he heart delights to think of [these instrumentalities] as destined to effect that
universal submission to the sceptre of Christ which is to characterise the latter day”
(Brown, Christ’s Second Coming, pp. 323-24, cp. p. 156), Warfield writes: “Chris-
tians are His soldiers in this holy war, and i is through our victory that His victory ig
known” (Warfield, Biblical and Theological Studies, p. 493), And: “There is the
church struggling here below — the ‘militant church’ we may call it the riumphing
church he would rather teach us to call it, for the essence of his presentation is not
that there is continual strife here to be endured, but that there is continuous vic
tory here to be won, The picture of this conguering church is given usin” Revelation
19 (Selected Shorter Writings, ed. by John E, Meeter, 2 vols. [Nutley, NJ: Presbyter-
ian and Reformed, (1915) 1970], vol. 1, p. 348). Still again: “It is the distinction of
Christianity that it has come into the world clothed with the mission to reason its
way to its dominion, ... And itis solely by reasoning that it will put all its ene-
mies under its feet” (Warfield, Selected Shorter Writings, vol. 2, pp. 99-100), Em-
phases mine, See Chapter 12, note 26.

 
