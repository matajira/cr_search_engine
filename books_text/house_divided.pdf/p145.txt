96 House Divided

The Bible teaches us that God alone is holy (Rev. 15:4) and
that God alone is good (Mark 10:18). But Paul had no hesitation
in attributing these exclusive divine attributes to the law of God as
well: “So the law is holy, and the commandment is holy, and right-
eous, and good” (Rem. 7:12). The holiness and goodness of God
refer to His essential character, not simply to the pleasure of His
will. And it is this essential character which comes to expression in
“the law,” according to Paul. Now to which law was Paul referring?
Regardless of the claim of House and Ice that the law of Moses is
different from the law of God, the” law of God about which the
Apostle Paul was speaking was precisely the well-known law of
Moses — that law which was the special pride and advantage of the
Jews (Rem. 2:17-18; 3:1-2) and whose precepts are quoted by Paul
right out of the Mosaic revelation (e. g., 2:21-22) and called “the
law” (7: 7-8). This is the law which is holy, just and good — display-
ing God’s own unchanging and absolute righteousness.

Dispensational theology has not yet come to grips with the
essentially godly (Godly) character of the law which was revealed
by Moses, the absoluteness of God’s moral nature, and the im-
mutability of the Lord. This failing is, upon analysis, manifest in
Dominion Theology by House and Ice. Undoubtedly the authors’
hearts move one direction on those issues, while their theological
constructs would logically lead them another.

Case-Law and Ceremonial-Law Categories

It is commonplace in the history of theology to draw distinc-
tions between the moral law, judicial (case-) law, and ceremonial
Jaw as found in the Mosaic revelation. This way of schematizing
the Old Testament commands can be found in many of the creeds
and theologians of the past. House and Ice recognize that many
Christians accept these internal distinctions in the Old Testament
law. 33 It is not some novel device dreamed up by theonomists to
rescue them from a theological bind in their system.

33, House and Ice, Dominion Theology, p. +2.
