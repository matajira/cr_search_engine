The Victory of the Kingdom 215

about “whenever” *? Christ “hands over” the kingdom to the Father.
In the syntactical construction before us, the “handing over” (NIV)
or “delivering up” (KJV) of the kingdom must occur in conjunc-
tion with “the end.” Here the contingency is of the date: “when-
ever” it may be that He delivers up the kingdom, then the end will
come. Associated with the predestined end here is the prophecy
that the kingdom of Christ will be delivered up to the Father only
“after he has destroyed all dominion, authority and power.” *

Gathering this exegetical data together, the conclusion must
be that the end is contingent, but it will come whenever it may be
that He delivers up the kingdom. When Christ turns over the king-
dom to the Father, the end has come. But this will not occur until
“after He has destroyed all dominion, authority and power.” Con-
sequently, the end will not occur, Christ will not turn the kingdom
over to the Father, until after He has abolished His opposition.
Here again is the Gospel Victory Motif in the New Testament in a
way co-ordinate with Old Testament covenantal and prophetic
expectations.

And notice further: Verse 25 demands that “He must! reign
until He has put all His enemies under His feet.” Here the present
infinitive (Creign”) indicates the continuance of his reign. We have
already seen that He is presently reigning, and has been so since
His ascension. '° Here we learn that He must continue to reign, He
must continue to put His enemies under His feet — but until
when? The answer is one that is identical to that which has
already been concluded. It is expected to occur before the end of

12, A better translation of Aotaz is “whenever.” We know not “when” this will be
(Matt. 24:36).

13, The Greek for ‘hands over” here is paradidoi, which is in the present tense
and subjunctive mode. When /oiza is followed by the present subjunctive it indi-
cates a present contingency that occurs in conjunction with the main clause: here
t-be coming of the end, Arndt-Gingrich, Lexicon, p, 592,

14, In the Greek text the hotaa is here followed by the aorist subjunctive,
katargese. Such a construction indicates that the action of the subordinate clause
precedes the action of the main clause, William F, Arndt and F, Wilbur

Gingrich, A Greek-English Lexicon of the New Testament and Other Early Christian
Literature (Chicago, IL: The University of Chicago Press, 1957), p. 592.
15, Greek: dei.
16, See Chapter 12, “The Presence of the Kingdom.”
