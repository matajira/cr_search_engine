42 House Divided

lifted from the face of Scripture’s laws. A tremendous amount of
homework remains to be done, whether in textual exegesis, cul-
tural analysis, or moral reasoning— with plenty of room for error
and correction. None of it is plain and simple. It must not be car-
ried on thoughtlessly or without sanctified mental effort.
Moreover, in all of it we need each other’s best efforts and
charitable corrections. Only after our ethical senses have been
corporately exercised to discern good and evil by the constant
study and use of God’s law — only after we have gained con-
siderabl y more experience in the word of righteousness (Heb.
5:13-14) — will we achieve greater clarity, confidence, and a com-
mon mind in applying God’s law to the ethical difficulties which
beset modern men. Nevertheless, even with the mistakes that we
may make in using God’s law today, I prefer it as the asis for eth-
ics to the sinful and foolish speculations of men. It would be ab-
surd for a man to resign himself to poison just because medical
doctors occasionally make mistakes with prescription drugs!

Transformat ionalism

As Christians we have been entrusted with God’s prescriptions
for how men should live their lives so as to bring glory to the
Creator and to enjoy loving, peaceful relations with other men in
this world. God’s prescriptions counter the destructive tendencies
of sin, and those destructive tendencies are felt in all areas of life,
from private and personal matters of the heart to the public mat-
ters of socio-political affairs. God’s prescriptive guidance is
needed, therefore, in all areas of life. Moreover, the Christian
acknowledges that Jesus Christ is Lord in every aspect of human
experience and endeavor. In every walk of life a criterion of our
love for Christ or lack thereof is whether we keep the Lord’s words
(John 14:23-24) rather than founding our beliefs upon the ruinous
sands of other opinions (Matt. 7:24-27).

Thus Christians who advocate Reconstructionism reject the
social forces of secularism which too often shape our culture’s con-
ception of a good society. The Christian’s political standards and
agenda are not set by unregenerate pundits who wish to quaran-
