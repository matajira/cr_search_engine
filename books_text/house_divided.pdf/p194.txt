10
THE EXPECTATION OF THE KINGDOM

A survey of the Biblical material revelational of the Gospel Victory
theme,

The pre-dominant and distinguishing theme of Biblical escha-
tology is that of a sure expectancy of gospel victory in time and on
earth. This can be seen in various ways in the Old Testament
revelational record.

The Edenic Expectation of Victory

In order to understand a thing aright, it is always helpful to
seek to understand its purpose according to its designer and
builder. Eschatology ' is a theological discipline that is concerned
with discovering the divinely revealed, long range purpose ‘of the
world and of history. It is concerned with the consummational di-
rection of history: What will the consummation be? What are its
precursors? How will it be brought about? When will it occur? By
necessity, then, eschatology must be concerned. with creation, for
it is the divinely decreed fruition of creation as we know it — the
end is tied to the beginning (Isa. 46:10; Rev. 21:6; 22:13).

God has created the world — and for a purpose. Despite the
confusion brought into the question by certain leading dispensa-
tionalists, Reformed theology has as the ultimate goal of universal

1, All references to “eschatology” should be taken to refer to cosmic eschatol-
ogy (e, g., the coming of Christ), rather than personal eschatology (the state of
the believer at leath).

2. The very question of “purpose” immediately draws a distinction between
Christian eschatology and secular eschatology. Secularism disdains discussion of
purpose, because it wholly undermines it with its fundamental irrationalism (in
that it is rooted in Chance),

149
