32 House Divided

applied to the cultural illustrations communicated in Old Testa-
ment moral instruction. > For instance, the requirement of a roof-
top railing (Deut. 22:8), relevant to entertaining on flat roofs in
Palestine, teaches the underlying principle of safety precautions
(e.g., fences around modern backyard swimming pools) — not the
obligation of placing a literal battlement upon today’s sloped roofs. °

Dispensational Differences

There are, then, cultural discontinuities between biblical moral
instruction and our modern society. This fact does not imply that
the ethical teaching of Scripture is invalidated for us; it simply
calls for hermeneutical sensitivity. In asking whether it is theologi-
cally legitimate to make contemporary use of biblical (especially
Old Testament) precepts — even those pertaining to civil law —
then, our concern is more properl y with redemptive-hisiorical discon-
tinuities, notably between Old and New Covenants. Clearly, the
Scriptures teach us that a new day arrived with the establishment
of Christ’s kingdom, the New Covenant (Luke 22: 20; Jer.
31:31-34; Heb, 8:7-13; 10:14-18), and the age of the Spirit (Acts
2:16-36; Luke 3:16-17) — a day anticipated by all the Old Cove-
nant scriptures (Luke 24: 26-27; Acts 3:24; 1 Peter 1:10-1i).

5, Just here Christopher J. H. Wright has misconceived and thus badly mis-
represented the “theonomic” approach as calling for a “literal imitation of Israel”
which simply lifts its ancient laws and transplants them into the vastly changed
modern world (“The Use of the Bible in Social Ethics: Paradigms, Types and
Eschatology,” Transformation [ January/March, 1984], p. 17). The same kind of
simplistic misrepresentation of theonomic ethics is found in Meredith Kline’s
“Comments on an Old-New Exror,” Westminster Theological Journal (Fall, 1978),

6. “But this is an easy example,” complain House and Ice (House and Ice,
Dominion Theology, p. 39). You see, on the same page, they are trying to criticize
theonamic ethics for leaving it a “vague area” in determining “how literally case
laws should be brought into our New Covenant era.” They complain because
having such “easy” examples available (and there are many more) makes it hard
for House and Ice to prosecute their complaint or win the argument. Nobody
denies, of course, that some case laws are more difficult than others to under-
stand and apply. But the same thing could be said about all of Scripture (even the
New Testament)- indeed, Peter said it (2 Peter 3:16)!
