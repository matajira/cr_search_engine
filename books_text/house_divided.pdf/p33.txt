Publisher% Foreword xxxlit

to exercise his self-appointed task of refuting the critics’ books in
one-page reviews in Bibliotheca Sacra, where no one is likely to read
them. A similar strategy has been adopted by Grace Theological
Seminary. But then Dr. House “broke ranks” and went into the
theological battlefield by writing a book. He was dragged into this
by Rev. Ice, the Don Quixote of dispensational theology. They
now face something more threatening to their reputations than
windmills.

The Dispensational Memory Hole

Decade after decade, dispensational theologians cling to a ver-
sion of church history which even their own students know is a
series of preposterous falsehoods strung together with classroom
polemics. Take, for example, a myth repeated by House and Ice,
that the major promoter of postmillennialism was the early eigh-
teenth-century Anglican theologian, Daniel Whitby. Dr. Gentry
deals with this in his chapters on “The Exposition of the Kingdom”
and “Documentation Inadequacies.” Now, anyone with even a
brief knowledge of the history of Puritanism knows that there
were many postmillennialists in the seventeenth-century Puritan
camp, including John Owen. Whitby was born in 1638 and did
not write until the early eighteenth century. He is a minor figure
in the history of the church, which is why the dispensationalist
polemicists dwell on him as the originator: it makes postmillen-
nialism appear to be a backwater eschatology. Dispensationalists
comfort themselves with the thought that “real Bible-believers
don’t believe in postmillennialism,” in the same way that Southern
rednecks believe that “real men don’t eat quiche.” Only dispensa-
tional writers have ever proclaimed this Whitby myth, and they
have done so generation after generation, but never those who teach
church history and who also hold a Ph.D. in the field. Sadly, the church
historians on dispensational campuses are apparently unwilling or
psychologically unable to go to their less well-informed colleagues
and say, ‘Look, fellows, this whole story was a myth our founders
invented for polemical reasons, and we are making fools of our-
selves by continuing to proclaim it .” So the Whitby myth goes on,
