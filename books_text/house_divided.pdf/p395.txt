Theological Schizophrenia 359

Some, such as those in the theonomist movement, even want
to reinstate Old Testament civil codes, ignoring Christ’s teaching
in the parable of the wheat and the tares in which He warns that
we live with the good (the wheat) and evil (the tares), and cannot
root out the tares. Only God is able to do that and He will —
when the Kingdom comes in its final glory. *

Comment: Colson would be hard pressed to find a Reconstruc-
tionist who believes in utopia or that the Christian’s concern
should be for the world rather than the salvation of souls. Can’t it
be both, with the salvation of man’s soul a priority? Colon’s very
biblical and compassionate ministry, Prison Fellowship, is con-
cerned with the souls of prisoners and their life in general. The
Bible does say that God “so loved the world” (John 3:16). Recon-
structionists want to know how the saved should act in the world
before “the Kingdom comes in its final glory.” This is the message
of Paul’s epistles to the early churches. The Pauline letters were
designed to show these new Christians how to live “in this present
evil age” (Galatians 1:4). Aren’t values like prohibitions against
theft and murder imposed on society? One of Colson’s heroes is
William Wilberforce. Wilberforce was concerned with slaves as
they lived in this world. Why not just preach the gospel to them and
then tell them to remain in the condition in which the gospel found
them? The State, according to the Bible, has the power of the sword
to enforce these values (Remans 13:4). But it’s Christian values that
Colson objects to. Or is it? He just told us in his article on “The
Kingdom of God and Human Kingdoms” that Exodus 22 is a great
example for prison reform. In another place he tells us that we
should “apply God’s laws.” The application of God’s law to society
does not conflict with the theology of the parable of the wheat and
the tares. According to Colson’s logic, nothing should be done to
restrain evil based on the theology of the parable of the wheat and
the tares. The Parable of the Wheat and the Tares has to do with
Jina jadgment. Taking Colson’s view, there could be no temporal
punishment, either in family government or civil government.

24, Ibid.
