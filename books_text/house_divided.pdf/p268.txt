The Victory of the Kingdom 225

14:13; James 4:15; 1 John 5:14), Sixth, in His ministry, Christ wit-
nessed the falling of Satan’s kingdom as His followers exercised
authority over demoniacs (Luke 17:10). In fact, Satan was cast
down (John 12: 31) and bound by Christ in order that Christ might
“spoil his goods” (Matt. 12:28-29; cp. Rev. 20:2-3). Christ specifi-
cally came that He might “destroy” Satan (Heb. 2:14) and his
“works” (1 John 3:8), making a show of him and openly triumph-
ing over him (Col. 3:15), having judged him (John 16:11). Conse-
quently, his people might not only resist the devil so that he will
flee from them (James 4:7), but even expect to “bruise Satan
under” their feet (Rem. 16: 20), “because greater is he that is in
you, than he that is in the world” (1 John 4:4). Because of all this,
the Gospel has the power to “open their eyes, and to turn them
from darkness to light, and from the power of Satan unto God”
(Acts 26:18).

Now, of course, this does not prove that God intends that the
world should be “Christianized” (that has been demonstrated on
other grounds). But it should embarrass any diminishing of the
potentialities of the gifts and grace of God. Let us hear no more of
this talk of “the church age [being] a time in which Satan and his
rebellious court refuse to give up their rule .” 37 Who cares that
Satan “refuses to give up”?!

The Intrinsic Pessimism of Dispensationalism

Due to space considerations we will not deal at length with the
following matter. It should be at least briefly broached, however.
House and Ice are dismayed at the Reconstructionist characteri-
zation of dispensationalism as “pessimistic”: “Christian Recon-
structionists often misrepresent the premillennial view by saying
that our position is inherently a pessimistic one.”3* Nevertheless,

37, House and Ice, Dominion Theology, p, 235,

38, Ibid., p. 146, cf, discussion on their pages 142-49, 161, 180, 170, 182-88, It
must be recognized that it is not just Reconstructionists who so characterize dis-
pensationalism, Even liberal commentators point to the “lethargy” inherent in
dispensationalism (e.g., Ted Peters, Futures: Human and Divine [Adlanta, GA: John
Knox Press, 1978], pp. 28-36). House and Ice state that “the gospel in history is
