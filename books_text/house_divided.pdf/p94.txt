4
HOW SHOULD WE THEN DECIDE?

An assessment of the theological reasoning employed by House and Ice,

Dispensationalism produces one kind of orientation or per-
spective in ethics. Reconstructionism produces another. It is not
impossible for the two positions to agree on particulars here and.
there (and happily they do), but it is evident from the above dis-
cussion that dispensationalism and Reconstructionism represent a
conflict of visions regarding the Christian life and involvement in
the world. Their respective approaches to ethics are in tension
with each other at fundamental points — viz., the moral unity of
Scripture, the absolute authority and sufficiency of God’s law, and
the present scope of Christian responsibility in the world.

Betraying the Supreme Judge

The authors of Dominion Theology are dispensationalists who
feel that the Reconstructionist view of ethics is mistaken. Why?
Sometimes they offer the opinion that Reconstructionism is mis-
taken because there is no “scriptural basis . . . exegetical proof”
for the position ‘ — it is not supported by the Bible and is actually
contrary to it. At other times, though, they suggest a different stan-
dard for judging Reconstructionism to be in error:

Christian Reconstructionism argues for a way of thinking about
God’s law, his plan for the future, and the role of the church fun-
damentally different from that commonly accepted among main-
stream evangelical. ... We shall describe the two major areas

1, H, Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse?
@ortland, OR: Multnomah, 1988), p. 8.

45
