The Mission of the Kingdom 203

Even early fallen man was driven to cultural exploits (Gen. 4:20-22)
well beyond the expectations of humanistic anthropologists and
sociologists. Man, then, was created and given a mandate to bring
order to all of God’s creation. As is evident in their close relation in
Genesis 1:26, the dominion drive is a key aspect of the image of God
in man. Culture is not an accidental aside of the historical order.
Now it should not go without notice that the New Testament
often speaks of the redemption of the “world” — the very system of
men and things of which we have been speaking. Although we are
prone to speak of Christ as “my personal Savior” (and certainly He
is! ), we too often overlook the fact He is also declared to be “the
Savior of the world.” There are several passages which speak of the
world-wide scope of redemption. These passages are quite instruc-
tive in their eschatological data. Let us lay them before you.

Redemption of the World

Jn John 1:29, John the Baptist sees Jesus and utters these words:
‘Behold, the Lamb of God who takes away the sin of the world.” As
such these words are compatible with 1 John 4:14 which informs us
of the divine purpose of Christ’s incarnation: “The Father has sent
the Son to be the Savior of the World.” In John 3:16-17 we read.
that “God so loved the world that He gave His only begotten Son,
that whoever believes in Him should not perish, but have eternal
life. For God did not send the Son into the world to judge the
world; but that the world should be saved through Him” (cp. John
12:47). 1 John 2:2 teaches that “He Himself is the propitiation for
our sins; and not for ours only, but also for those of the whole
world.” In 2 Corinthians 5:19 Paul conceives of Christ’s active
labor thus: “God was in Christ reconciling the world to Himself.”

These passages clearly present Christ in His redemptive labors:
He is the Lamb of God; He takes away sin; His purpose in coming
was to save; He provides propitiation for the sinner; He is reconcil-
ing sinners to Himself. But a point frequently overlooked in these
passages is that these verses just as clearly speak of the divinely
assured world-wide outcome of His redemption. 33 In 1 John 4:14

33. See B, B, Warfield, “Christ the Propitiation for the Sins of the World,” ix
Selected Shorter Writings, Chapter 23,
