An Opportunity for Cross-Examination 7

the public dialogue. This would have allowed an excellent oppor-
tunity to air, examine, and evaluate the reasoning set forth in
Dominion Theology It would have permitted a correcting of the re-
cord about what Reconstructionists do and do not really believe.
The Christian public would have been given a serious look at the
contrasts and relative strengths or weaknesses of the conflicting
systems of thought: dispensationalism and Reconstructionism. It
would have provided an occasion to examine and assess the rea-
soning and biblical exegesis employed by authors House and Ice,
as well as to do the same with respect to the argumentation en-
listed in support of Reconstructionism. Bahnsen was glad for the
invitation and eager for the encounter.

But House apparently had second thoughts about the debate.
Even though he had gone into print with a hard-hitting attack
upon the alleged errors of Reconstructionism — and thus would.
have felt his work to be defensible (even important for the health
of the Church) — House now felt that the nature and structure of
the dialogue with Bahnsen should be altered. In fact, he said that
his request for the change in the character of the debate was non-
negotiable; he simply would not participate unless the change
were made. He now wanted a “debate” without any direct cross-
examination between the parties to the debate. That is, each side
could bring their prepared remarks, but there must not be any
interchange between the debaters on a one-to-one footing where
direct questions and challenges would need to be entertained and
answered. In short, a debate with its heart cut out. It was pre-
cisely cross-examination which would have made the debate valu-
able to the public and a true test of the two conflicting theological
positions. Their relative strengths would then be open for inspec-
tion. Undoubtedly, House had his own good reasons for not want-
ing the debate with cross-examination a constitutive part of it.

Some other forum has needed to be sought in which the work
of House and Ice can be publicly cross-examined. That explains
why this chapter opened with the statement that this present book
might not have been necessary, and it explains why it now is. The
extensive discussion of Reconstructionism by House and Ice should
