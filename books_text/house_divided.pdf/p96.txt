How Should We Then Decide? 47

The Protestant (and biblical) standard of theological truth,
over against appeals to tradition or group opinion (as we find in
Romanism), is sola Scriptura. Scripture alone should be the source
of our theological convictions and the final judge in all disagree
ments. Critics of Reconstructionism like House and Ice tend to
forget that standard, in practice not being loyal to it. They want
to look around to see who in the social circle with which they are
comfortable will agree or disagree with certain ideas. The way in
which they vacillate between group opinion and biblical exegesis
as the criterion for evaluating Reconstructionism betrays a failure
(in practice, not profession) to let Scripture function as their
supreme authority.

Horrid Consequences

This same failure is evident when House and Ice attempt to
sway their readers away from the Reconstructionist perspective in
ethics by asking “What Would a Christian Reconstructed America
Be Like?” (the title of Chapter 4 in their book). They offer a cou-
ple of opening ‘scenarios” purporting to represent what would
happen if the Reconstructionist view in ethics came to be widely
adopted — imaginary cases intended to make the reader recoil
from Reconstructionism in shock or dread. The implicit line of
reasoning is that, whether or not Reconstructionism is bibdical, the
consequences are so uncomfortable that readers will not want to ac-
cept it anyway.

We could, if we wished, pause here to expose the artificiality,
contrivance, and downright fraudulence of the alleged illustra-
tions offered by House and Ice for what would happen in a Recon-
structed America. “They resort to attacking straw men. However,

5. If we were fo go into detail, high on the list would be the two preposterous
suggestions made by House and Ice that (1) theonomic ethics is incompatible
with the Bill of Rights, and (2) a theonomic civil magistrate would apply criminal
sanctions against anyone holding dispensational beliefs. Such statements display
inexcusable ignorance of the First Amendment of the U.S. Constitution, as well
as of the actual requirements of God's law. About the former, John W. White-
head observes: “Thus the philosophical base af the First Amendment was that cf
