The Mission of the Kingdom 207

Though these passages do not teach an “each-and-every uni-
versalism,” as in liberal thought, they do set forth the certain
divinely assured prospect of a coming day in which the world as a
system (a kosmos) of men and things, and their relationships, will be
redeemed. A day in which the world will operate systematical y
upon a Christian ethico-redemptive basis. Christ’s redemptive la-
bors will have gradually brought in the era of universal worship,
peace, and prosperity looked for by the prophets of the Old Testa-
ment. As John put it to the first-century Christians who were un-
dergoing various tribulations: Christ is the propitiation not for
their sins only, they being few in number (a little flock, Luke
12:32), but for the sins of the world as such. There is coming a
day, in other words, in which Christ will have sought and have
found that which was lost (Luke 19:10): the world. Hence, the
Great Commission command to baptize “all nations” (Matt. 28:19).

The Drawing of All Men

Another class of passages that have an identical import is that
which speaks of Christ’s labors as having fruition among “all men.”
Particularly relevant are two passages: John 12:32 and 1 Timothy
2:6. In John 12:32 Jesus is comforting His disciples while in the
shadow of the cross: “And I, if I be lifted up from the earth, will
draw all men to Myself.” In 1 Timothy 2:6 Paul is encouraging
Christians to effectual fervent prayer for all men (1 Tim. 2:1) be-
cause: Christ “gave himself a ransom for all, the testimony borne
at the proper time .” We will only briefly deal with these two pas-
sages, in that the idea is basically the same as that already pres-
ented in the passages that make reference to the world.

The John 12:32 passage, although set in the context of “the
shadow of death,” as it were, is filled with covenantal hope for the
disciples. Though Christ will soon be taken from the disciples and
by wicked hands crucified, nevertheless, there is a bright ray of
hope as the Gospel Victory Motif penetrates the clouds of despair.
And that hope is just this: “And I, if I be lifted up from the earth,
will draw all men unto me.” Here the Lord does set up a condition
for His redemptive labors. The condition set forth in the protasis
