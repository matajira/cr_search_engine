xlviii House Divided

longer.”! I wish I could take some of the courses over: I was around
at least long enough for graduation day to come.

While studying at Grace Theological Seminary, two influ-
ences converged causing me to reject dispensationalism. The first
was my researching a paper on the Lordship Controversy. *This
led to my discovery of the significance of the Acts 2 enthronement
passage, which shook my dispensationalism to its very foundation.
The second was the discovery at about the same time of O. T.
Allis’s Prophecy and the Church. This work bulldozed the residue of
my collapsed dispensationalism. A couple of friends of mine (Rev.
Alan McCall and Mr. Barry Bostrom, Esq.) and I not only soon
departed dispensationalism but transferred from Grace Seminary
to Reformed Theological Seminary in Jackson, Mississippi. Pre-
viously we had been partial Calvinists, now we had become fully
reformed, hence non-dispensational.

At Reformed Seminary I took two courses that initially
seemed implausible and misguided extravagance. The courses
were “History and Eschatology” (in which was defended postmil-
lennialism) and “Christian Theistic Ethics” (in which was set forth
theonomic ethics). Both of these courses were taught by my co-
author, Dr. Greg L. Bahnsen.

Regarding the eschatological question, even though I was no
longer a dispensationalist I had assumed Pentecost, Lindsey, and
other dispensationalists were correct in affirming “postmillennial-
ism finds no defenders or advocates in the present chiliastic dis-
cussions within the theological world.” Unfortunately, I still had.
dispensational blinders on my eyes, for in the very era in which
Pentecost’s book was published (1958) there were at least four
notable works in defense of postmillennialism — one of them en-

Per anal The Late Great Planet Harth (Grand Rapids, MI: Zondervan,
, P. 140,

2, This paper was eventually published. See Kenneth L, Gentry, Jr., “The
Great Option: A Study of the Lordship Controversy; Baptist Reformation Review
(Spring, 1976), pp. 494.

3. J. Dwight Pentecost, Things To Come: A Study in Biblical Eschatology (Grand
Rapids, MI: Zondervan/Academie, [1958] 1964), p. 387; cp. Lindsey, Late Great,
p. 176,
