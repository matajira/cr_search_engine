xliv House Divided

explosive questions, yet pretends that it has answers to the on-
slaught of the theonomists.

I will say it once again: the theological debate is over. Christian
Reconstructionism has not yet won the debate with every known
theological critic, but it surely has won the debate with the dispen-
nationalists. And in engaging the dispensationalists directly, Dr.
Bahnsen and Dr. Gentry have brought up to date the work of
O. T. Allis. Allis inflicted mortal wounds on dispensational theol-
ogy in 1945. Bahnsen and Gentry merely act as public coroners.
Their autopsy report is now on record. Ladies and gentlemen, the
cadaver is surely dead; rigor mortis has set in. It is time to give ita
decent Christian burial.

But first, read House Divided, the equivalent of an open-casket
funeral.

The Quiet Defection of the Seminaries

What few dispensationalists in the pews realize is that even
Dallas Seminary no longer emphasizes dispensational theology to
the degree that it once did. Ever since its accreditation in the
mid-1970’s, it has emphasized such topics as Christian counseling
far more than 1950’s dispensationalism. The departure of Charles
Ryrie from the Dallas faculty was symbolic of this shift in em-
phasis. Meanwhile, in the late 1980’s, Talbot Theological Seminary
in La Mirada, California abandoned dispensationalism entirely.
For the sake of alumni donations, however, neither seminary dis-
cusses these changes openly. Only Grace Theological Seminary of
the “big three” still forthrightly preaches Scofieldism’s “received.
truths .“ (To its credit, Grace Seminary still teaches a literal six-
day creation, a doctrine which Dallas Seminary, like all the Cal-
vinist seminaries except for Reformed Episcopal Seminary and
Mid-America Reformed Seminary, has carefully avoided defend-
ing. Dallas Seminary’s self-proclaimed “literal hermeneutic”
begins only with Genesis 2.)

The problem is, the opponents of theonomy in all camps keep
such inside information bottled up. They refused to engage us in
open debate for over fifteen years. Then, in late 1988, the pub-
