The Conflict of Expectations 141

demption, this kingdom will exercise a transformational socio-
cultural influence in history as more and more people are con-
verted to Christ. Postmillennialism, thus, expects the gradual, de-
velopmental expansion of the kingdom of Christ in time and on
earth. This expansion will proceed by means of the full-orbed.
ministry of the Word, fervent and believing prayer, and the conse-
erated labors of His Spirit-filled people, all directed and blessed
by the ever-present Christ, Who is now at the right hand of God.
ruling and reigning over the earth. It confidently anticipates a
time in earth history (which is continuous with the present) in
which the very gospel already operative in the world will have
won the victory throughout the earth in fulfillment of the Great
Commission. During that time, the overwhelming majority of
men and nations will be Christianized, righteousness will
abound, wars will cease, and prosperity and safety will flourish.
After an extended period of gospel prosperity, earth history will be
drawn to a close by the personal, visible, bodily return of Jesus
Christ (accompanied by a literal resurrection and a general judg-
ment) to introduce His blood-bought people into the consum-
mative and eternal form of the kingdom, and so shall we ever be
with the Lord.

Our Concern

We will begin with a quick biblico-theological overview of the
revelatory progress of eschatology in Scripture. From there we
shall move on to consider several of the more significant questions
raised by House and Ice — questions regarding the nature, pres-
ence, mission, and victory of the Messianic kingdom; the rise of
postmillennialism in ecclesiastical history; and the legitimacy of a
preteristic approach to certain prophecies.

In doing this we will be answering their concern:

At best, the Reconstructionist system is built upon a theological in-
ference, from which they try to develop a large body of Scripture.

4, What kind af statement is this? No Reconstructionist attempts to develop a
large body of Scripture from a theological inference. Surely they mean it the
