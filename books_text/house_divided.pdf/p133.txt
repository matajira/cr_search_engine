84 House Divided

Summary

1. Carefulness and accuracy of portrayal are incumbent upon
those who would seek to critique any system of thought.

2. The large number, and especially extreme character, of
House and Ice’s reports about the alleged teachings of Recon-
structionist ethics are more than sufficient to impeach House and
Ice as critics of the position.

3. Though House and Ice say, “it is absolutely critical to elim-
inate any margin for misunderstanding, “they have turned.
around and widened that margin by miles.

4, House and Ice have created a credibility gap regarding
their role as reviewers. One can hardly avoid becoming disen-
chanted with their scholarship.

5. The authors of Dominion Theology are either unable or un-
willing to portray Reconstructionism fairly. In Christian charity, I
do not know which is the least severe conclusion to draw here.

 

Reconstruction” (pp. 18-19), but in the “Chalcedon Report.” In 1965 Rushdoony did
not establish the Chalcedon Foundation ‘in Vallecito, California” (p, 18), but in
Canoga Park (hundreds of miles away). One or two of these factual bloopers
would be embarrassing enough. One after another destroys credibility.

73, House and Ice, Dominion Theology, p. 35.
