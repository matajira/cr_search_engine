The Expectation of the Kingdom 155

The prophets greatly expand the theme of victory under the
Messiah. Isaiah 2:1-4, 17-20 promise that in the “last days?...
all nations shall flow to the house of the Lord” (v. 2), issuing forth
in international peace (v. 4). In Isaiah 9:1-7 we learn that the
Messiah’s kingdom, once established, will ever increase. It is ter-
ribly important to notice the close connection between “the son”
being born (His humiliation at the incarnation) as the one upon
whom universal government devolves (v. 6) and his kingdom (His
exaltation at the resurrection/ascension) growing and bearing
peace (v. 7). In Isaiah 11:9, the future of the earth is seen
prophetically as “full of the knowledge of the Lord as the waters
cover the sea.” Even the arch-enemies of God and His people,
Egypt and Assyria, will be healed and will on an equal footing
worship with Israel (Isa. 19: 22-24).

Jeremiah foresees the day when the ark of the covenant will no
longer be remembered, but in which “all the nations will be gathered
before” the “throne of the Lord” ( Jer. 3:16-17). The New Covenant
(initiated by Christ, Luke 22; 1 Cor. 11) will issue forth in world-
wide salvation (Jer. 31:31-34), Enemies of God’s Old Testament
people will be brought to blessing in the last days: Moab (Jer.
48: 47), Ammon ( Jer. 49:6), Elam (Jer. 49: 39), and the Philistine
(Zech. 9:7).

With Isaiah Daniel sees the expansion of the kingdom to the
point of dominion in the earth (Dan. 2:31-35, 44-45; cp. Isa.
9:6-7). The Messiah’s ascension and session will guarantee that
“all people, nations and languages should serve Him” (Dan.
7:13-14). It must be noticed that Daniel 7:13-14 speaks of the
Christ’s ascension to the Ancient of Days, not His return to the
earth. It is from this ascension to the right hand of God ' that
there will flow forth universal dominion.

Days of prosperity, peace, and righteousness lie in the future,
according to Amos 9:11-15; Micah 4:1-3;5:2-4, 16-17; 7:16-17;

9, Le., in the times initiated by Christ at His First Advent (Acts?:16,17, 24;
1 Cor, 10:11; Gal, 4:4; Heb. 1:1-2; 9:26; James 5:3; 1 Peter 1:20; 1 John 2:18; Jude 18),
10. See later discussion of His present Kingship in Chapter 12,

   
