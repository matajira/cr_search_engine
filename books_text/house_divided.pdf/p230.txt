The Presence of the Kingdom 185

ing of gifts to His subjects, in the manner of a warrior-king
returning triumphantly to his capital city upon his victory over
the enemy (Acts 2:33; Eph. 4:7-12).27 It promises His divinely
royal assistance to His people (Rem. 8:34). In fact, He is there at
the throne even now awaiting the collapse of all His enemies
under that majestic and gracious rule (1 Cor. 15:23, 24; Heb. 1:3,
18; 10:13). 28 This is why there is so much “kingdom of God” proc-
lamation in the New Testament. 9

Early Christians Considered Him King

In Acts 3:15 Peter preaches Christ as the “prince of life.” In
Acts 5:31 he asserts his obligation to disobey civil authority when
it demands that he cease preaching Christ. His rationale is impor-
tant: “Him bath God exalted with his right hand to be a Prince
and a Saviour.” The word “prince” here can literally be translated
“leader, ruler, prince.”°° He was exalted to become Prince or
Ruler.

In Acts 17:7 we learn of the civil turmoil the early Christians
were causing. The charge against them is most interesting and
must be based in reality, even if largely misunderstood by the
unbelieving populace. Just as the Jews accused Jesus of claiming
to be a king, 31 so we read of the charge against His followers:
“These all do contrary to the decrees of Caesar, saying that there is
another king, one Jesus .” Just as Jesus did in fact teach that He
was a king (though in a non-political sense, John 18:36-37), his
followers did the same.

The Apostle John clearly declared that Jesus was even then in
the first century ruling and reigning: “Jesus Christ, who is the
faithful witness, and the first begotten of the dead, and the prince

27, Cp. Genesis 14; 1 Samuel 30:26-31; Judges 5:30. See Isaiah 53:12.

28, See Chapter 14 on “The Victory of the Kingdom.”

29, See Acts 8:12; 14:22;19:8; 20:25; 28:23, 31; Remans 14:17; 1 Corinthians
4:20; 6:9-10; 15:50; Galatians 5:21; Ephesians 5:5; Colossians 1:13; 4:11; 1 Thes-
salonians 2:12; 2 Thessalonians 1:5; 2 Timothy 4:14:18; Hebrews 1:8;12:28;
James 2:5; 2 Peter 1:11. See Chapter 11 on “The Nature of the Kingdom.”

30, Arndt and Gingrich, Lexicon, p. 112.

31. See Matthew 27:29, 37; Mark 15:12, 26; Luke 23:3; John 18:33; 19:12,15,21.

 
