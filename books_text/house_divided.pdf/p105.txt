56 House Divided

transformation, knowing that it is impossible to effect any genuine
change before the violent intervention of Christ. Since House and
Ice are dispensationalists, they would both have to agree with
author, Dave Hunt, that trying to change society through Chris-
tian activism is not biblical (rather a sign of the New Age heresy).
Moreover, some pietists in the history of the Church have re-
nounced society altogether and retreated to the cloistered holiness of
a monastery. Now House and Ice do not actually say Christians
should exclusively promote personal piety, reject worldly society,
and stay in monasteries. But it is “possible” for a dispensationalist
to do so. There are some things which pietistic dispensationalists
have in common with monks. So the tendency toward a monastic
life cannot be denied. And just remember the /egalism which
Bahnsen noted as a dispensationalist tendency in Chapter 2 !
Since its the Roman Catholic Church which has fostered legal-
istic, monastic living, we have a further indication that dispensa-
tionalism shares Romanist sympathies.

What? You say that dispensationalism is notorious for its
stands against theological liberalism and Roman Catholicism?
That makes no difference whatsoever. Dispensationalists must be
“unwitting” liberals and Romanists. They are guilty, because they
might be guilty. Sure, I cannot produce any actual evidence of ac-
tual endorsement of the actual distinguishing marks of liberalism
or Romanism in the actual authors, House and Ice. The potential
danger alone is enough to condemn them. House and Ice should
be shunned as liberal Roman Catholics should be shunned!

In fact, things are far worse than we imagined. In terms of
conceptual structures and analysis, the dispensationalism of
House and Ice shows definite parallels to the reasoning of cultural
relativists, as revealed by Bahnsen in Chapter 2. House and Ice
would, of course, openly repudiate cultural relativism, despite
their being conceptual brothers under the skin. House and Ice be-
lieve that what is right and wrong changes on the basis of tempo-
ral and ethnic changes — that morality is relative to the dispensa-
tion (law/grace) and the race (Jew/Gentile) of the society about.
which one is thinking. Cultural relativists say the same thing (and
