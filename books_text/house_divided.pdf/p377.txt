Ethical Lapses 339

Dispensationalists: On Matt. 5:17: “Christ begins with a strong
denial that he had come as a rebel, a rejecter of law, though he
was often accused of being a lawbreaker (Mark 2:24; Luke 6:2).
.«« Rather than being a rejecter of law, Christ had come to fulfill
the law.?!#1

Christians Are Not Under Law but Grace:

Jehovah's Witnesses: “This fact proves that they are not under
the old law covenant with its Ten Commandments, but are under
the new covenant, by God’s undeserved kindness.” '*?

Dispensationalists: “We are not under law but under grace... . ”!*3

We Cannot Distinguish Ceremonial and Moral Laws:

Jehovah's Witnesses: “The law covenant cannot be taken apart,
so that a part of it, the ceremonial part, could be abolished, and
the other part of it, the so-called ‘moral’ part, remain,” '**

Dispensationalists: “The codes of Israel reflect the character of
God in ceremonial, moral, and civil expressions; none of these
manifestations are ever presented as superior to the others nor
severable. These three are inseparable parts of the law of God for
Israel.” 1*5

The Nations Are Not Under Moses’ Law:

Jehovah's Witnesses: “Thus the sabbath commandment was a com-
ponent part of God’s covenant with Israel, and it could not be sep-
arated from that covenant. The Gentile nations were not and never
have been under God’s Fourth Commandment of the covenant.”!#6

Dispensationalists: “The real question is whether the Mosaic law
given to Israelis to be practiced by any other nation not under the
covenant. The answer is no!” !*7

141, House and Ice, Dominion Theology, p. 105.
142, Let God Be Tree, p. 194,

143, Ryrie, Balancing the Christian Life, p. 151.
144, Let God Be Tree, p. 188,

145, House and Ice, Dorninion Theology, p, 89.
146, Let God Be True, p, 174,

147, House and Ice, Dominion Theology, p. 100.
