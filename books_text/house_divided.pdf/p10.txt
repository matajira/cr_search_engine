x House Divided

man is seen to be nothing more than a freedomless cog in a vast
impersonal machine.

In order to escape the pessimistic implications of this lower-
storey worldview, humanists have proposed an escape hatch: a
correlative upper- storey universe. The upper storey is supposedly
one of humanistic “freedom”: faith, feeling, emotion, personality,
randomness, religion, non-cognitive reality, ie. Kant’s noumenal
realm. It also supposedly provides meaning for man, but only
non-cognitive (irrational”) meaning. It is meaning which is
meaningless in rational (‘lower storey”) terms.

There is no known point of contact or doorway between these
two realms, yet modern man needs such a doorway to hold his
world and his psyche together. This is why the modern world is in
the midst of a monumental crisis, Schaeffer argued.

Schaeffer got the core of this idea from his professor of apolo-
getics at Westminster Theological Seminary, Cornelius Van Til,
although you would not suspect this by reading Schaeffer's foot-
notes. Van Til argued throughout his long career that all non-
Christian philosophy from the Greeks to the present is dualistic: a
war between the totally rational and the totally irrational.
Creating a memorable analogy, Van Til said that the irrationalist
and the rationalist are like a pair of washerwomen who support
themselves by taking in each other’s laundry. The intellectual
problems created by each school of thought are unresolvable in
terms of its own presuppositions, and so the defenders of each sys-
tem seek temporary refuge in the very different but equally
unresolvable problems of the rival school.

Why do they do this? Because non-Christian man prefers to
believe anything except the God of the Bible, who issues His cove-
nant law and holds all men responsible for obeying it, on pain of
eternal judgment. They would prefer to dwell in an incoherent
dualistic universe of their own devising rather than in God’s uni-
verse, dependent on His grace.

The Two-Storey World of Orthodox Christianity

The New Testament teaches that there are two realms of exist-
ence in this world: the eternal and the temporal. Each of these
