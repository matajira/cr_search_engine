280 House Divided

Surely it may not be stated, as do House and Ice: Why is it
that all of the early fathers, when referring to Revelation and
Matthew 24, see these as future events?”1°7

Nero and Revelation

House and Ice write: “If Chilton could show that Nero is the
ruler spoken of in Revelation, then he would have a major victory
for his view. But he cannot .”!°* As has been shown in great detail
in Before Jerusalem Fell, many lines of evidence converge upon
Nero: '°° (1) His place as the sixth among the Roman emperors
(Rev. 17:10), 1° (2) his being followed by a seventh, briefly reigning
emperor (Galba, Rev. 17:10),11 ! (3) his name’s numerical value of
666 (Rev. 13:18), (4) his living while the temple still stood (Rev.
ti:1-2), (5) the prominence of his persecution in first century
Christianity (Rev. 13), and more. There is an old adage: If the
shoe fits, wear it. Nero’s footprints are all over Revelation.

Systemic Futurism as « Modern Phenomenon

House and Ice write that the preteristic approach represents “a
new way to deal with the Olivet discourse and the book of Revela-
tion” ''? that “was not recognized . . . until recently. It was not

107, House and Ice, Dominion Theology, p. 258 (emphasis mine), In the final
analysis, however, one must wonder how their argument carries weight in light
of the 1830 J. N, Darby/Plymouth Brethren roots of dispensationalism (which
they admit, p. 422), After all, it is Charles C. Ryrie, the chief proponent oxis-
pensationalism and one of the endorsers of the book by House and Ice, who de-
fends dispensationalism from “the charge of recency” by labeling such a charge a
“straw man” and arguing from history as a “fallacy.” In addition he writes: “The
fact that something was taught in the first century does not make it right (unless
taught in the canonical Scriptures), and the fact that something was not taught
until the nineteenth century does not make it wrong . .” (Dispensationalism
‘Today, p. 66).

108, House and Ice, Dominion Theology, p. 259.

109, See Gentry, Before Jerusalem Fell, Chapter 12.

110, The first seven emperors were: Julius, Augustus, Tiberius, Gaius,
Claudius, Nero, and Galba. See Josephus, Antiquities 18:2:2; 18:6:10;19:1:11;
Suetonius, Lives of the Twelve Caesars; Dio Cassius, Julius 84,

an. Galba reigned from June, 4.0. 68 to January, 4.0. 69, Josephus, Wars
4:9:2, 9,

112, House and Ice, Dominion Theology, p. 264,
