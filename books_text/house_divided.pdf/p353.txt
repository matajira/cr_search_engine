Ethical Lapses 315

jennialism ‘seems to have been originated by the Ebionite arch-
heretic Cerinthus, a “false apostle.” This charge cannot i any way
be supported; it is a pure fabrication. There are no records from church
history that lead to such a conclusion. Chilton needs to jind a his-
torian outside of his own camp who will support his claim.” ?? Now
what is the reader of House and Ice’s work to think of Chilton?
House and Ice clearly lead the reader to stand aghast at Chilton’s
reckless charge. They state that the “charge cannot in any way be
supported,” that it is “pure fabrication” from Chilton, that “there
are no records from church history” supportive of it, and that
there is no “historian outside of his own camp” who agrees with it.
Tn all humility we must respond that this is nothing less than bla-
tant, intentional falsehood, for in the same paragraph as Chilton’s
statement quoted by House and Ice, Chilton provides source doc-
umentation from church father Eusebius to substantiate his claim!
There Chilton writes: “For an account of Cerinthus and his here-
sies, see... Eusebius, Ecclesiastical History, iii.xxvii. 1-6; iv.xiv.6;
vii.xxv. 2-3.”

Now, whether or not that which “seems” to be the case to Chilton
is true, the fact of the matter is he does have footing in history to ponder
the possibility, In Eusebius (A. D. 260-340), “the father of Church
history,” we read a statement he has gotten from his predecessor
Caius of Rome (fl. a. D. 200-220): “But Cerinthus also, by means
of revelations which he pretends were written by a great apostle,
brings before us marvelous things which he falsely claims were
shown him by angels; and he says that after the resurrection the
kingdom of Christ will be set upon earth, and that the flesh dwell-
ing in Jerusalem will again be subject to desires and pleasures.
And being an enemy of the Scriptures of God, he asserts, with the
purpose of deceiving men, that'there is to be a period of a thou-
sand years for marriage festivals .« '* Whether or not Caius and.
Eusebius were wrong, the point remains: There are at least two

17, ibid., p. 197 (emphasis mine),

18, Eusebius, Hcclesiastical History 3:28:1-2. See also: 7:25:3: “Cerinthus , ,
founded the sect which was called after him the Corinthian, . . . For the doctrine
which he taught was this: that the kingdom of Christ will be an earthly one. ...”
