The Preterist Interpretation of the Kingdom 273

fathers speak of the fulfillment of Daniel’s 70 Weeks, including the
“abomination of desolation,” in Jerusalem’s destruction. ”*

3. The Alleged Differences Between Luke and Matthew. House and
Ice make an issue of the language of “deliverance” found in Luke
21:28. They see it as reflective of Zechariah 12-14, where Jeru-
salem is surround by the nations: This passage, they say, “fits very
well into the language of Matthew 24 — the nations have sur-
rounded Jerusalem. It does not fit the A. D. 70 destruction of Jeru-
salem, since that was accomplished by one nation — Rome....
It would also be difficult to see how a single nation would fit this
passage even if hyperbole were used.”

Assuming Zechariah 12-14 to be relevant to Matthew 24, it
should be noted that historically it cannot be argued that the Jewish
War, which saw the destruction of Jerusalem, was “by one nation.”
The war was not by one nation, but an empire of nations — the
Roman Empire that consisted not only of the nation of Italy, but
the lands or nations of Syria, Asia Minor, Palestine, Gaul, Egypt,
Britain, and others. Furthermore, Josephus points to numerous
auxiliaries from a number of nations which participated. ””

4, Coming as Lighining. House and Ice note that the “coming” of
Christ in Matthew 24:27 cannot represent the invasion of Rome
under Christ's behest as a judgment coming, The problem is that
the ‘language of the coming of Christ is sudden and intervention-
ist,“7'Le. , like lightning: “It does not matter how swift an army
is, it could never come with that kind of speed .“” The reason they

74, See footnote 101 below and preceding text.

75, House and Ice, Dominion Theology, p, 291,

76, Joseph Ward Swain, The Harper History of Civilization (New York: Harper
and Bros,, 1958), vol, 1, p. 198, Webster's New 20th Century Unabridged defines an
empire thus: “a state uniting many territories and peoples under one ruler” or
“the territories, regions, or countries under the jurisdiction and dominion of an
emperor.” The Roman empire was composed of imperial provinces, senatorial
provinces, and client kingdoms,

77, For example, in one place he mentions soldiers and horsemen from
Caesarea, from Syria, from the kings Antiochus, Agrippa, and Sohemus, and
from Maichus, the king of Arabia Josephus, Wars, 3:4:2; cp, 3:1:3).

78, House and Ice, Dominion Theology, p. 295.

79, Ibid.
