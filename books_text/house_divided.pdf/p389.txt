Theological Schizophrenia 353

Wayne House

For now, let’s take a look at what I would call “dispensational
schizophrenia” in Wayne House.

w

The Constitution arose from the Puritan idea of covenant.
The first great document of the colonies had been the Mayflower
Compact, and state after state adopted similar documents. It is
interesting that the biblical book of Deuteronomy — the covenant
book — was the most quoted source in political writings and
speeches preceding the writing of the Constitution. There is am-
ple reason to believe that the Framers and ratifiers of the Consti-
tution saw themselves as entering into solemn covenant, an act
of lasting and binding importance.

The Constitution reflects a genius in its construction.’1

Comment: Christian Reconstruction also relies heavily on the
covenant concept. In fact, covenant theology is contrasted with
dispensationalism in Charles H. Ryrie’s Dispensationalism Today.
Moreover, the book of Deuteronomy is one of the most quoted
Old Testament books used to support the relationship between
obedience/disobedience and societal blessing/cursing. ’* How then
can the Constitution reflect “a genius in its construction,” but
Christian Reconstruction is a “curse” when it follows a similar
covenant model with societal application? If a Reconstructionist
proposed that our founding fathers used Deuteronomy as a model
for the our nation’s constitutional government, House and Ice
would have a fit. But when a dispensationalist discovers such a

11, Wayne House, ed., “Editor’s Introduction,” Restoring the Constitution:
1787-1987 (Dallas, TX: Probe Books, 1987), p. 8.

12. The heavy use of the Book of Deuteronomy was the objection of William
E, Diehl in his “A Guided-Market Response” to Gary North's “Free Market Cap-
italism” article in Robert G, Clouse, ed,, Wealth & Poverty: Four Christian Views of
Economics (Downers Grove, IL: InterVarsity Press, 1984), Dich! writes:

That the author [Gary North] is strong on “biblical law” is apparent,
The essay provides us with thirty-nine Old Testament citations, of
which twenty-three are from the book of Deuteronomy (p. 66).
