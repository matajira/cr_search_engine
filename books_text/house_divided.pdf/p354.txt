316 House Divided

ancient historical charges that Cerinthus (cu. a. D. 100) originated
the doctrine. Chilton did not personally fabricate the charge; it can
be supported from antiquity. The readers would never suspect
that, as they scoffed at Chilton upon reading House and Ice. In
addition, despite House and Ice’s assertions to the contrary, there
are other historians outside of Chilton’s Reconstructionist “camp”
that suggest the same. W. G. T. Shedd (1820-1894) writes of pre-
millennialism: “It appears first in the system of the Judaistic-
Gnostic Cerinthus."!9 Even dispensationalist, Dallas Seminary-
trained Patrick Boyd calls Cerinthus “the earliest chiliast”?° —
though he obviously would not grant that Cerinthus created pre-
millennialism. He cites other historians pointing to Cerinthus
thus: V. Ermoni and Hans Bietenhard. We could also cite amil-
lennialists W. J. Grier and Louis Berkhof,”! as well as Schaff’s
Religious Encyclopedia: “The ultimate root of millenarianism is the
popular notion of the Messiah current among the Jews. ... It is
found in Cerinthus (Eusebius, Eccl. Hist., 3:28; 7:25), in the Tes-
taments of the Twelve Patriarchs (Jud., ca. 25; Benjam., ca. 10), and.
amongst the Ebionites (Jerome, Jn Jes., 40:1, 66: 20).?22

Origins of Postmillennialism

Postmi.Jennialists are put down by House and Ice as those who
operate theological y in a reckless fashion, rather than seeking to
understand Scripture: ‘Whitby and his modern followers present
their arguments and explanations based upon unproved assump-
tions — assumptions resulting in a hypothesis rather than some-
thing which is the fruit of the study of Scripture or even the voice
of the church.This reference to “unproved assumptions” is

19, W. G, T. Shedd, A History of Christian Docirine, 2 vols. (9th ed.; Minneapolis:
Klock and Klock, [1889] 1978), vol. 2, pp. 390-91.

20, Alan Patrick Boyd, “Dispensational Premillennial Analysis of the Escha-
tology of the Post-Apostolic Fathers (Until the Death of Justin Martyr)/ unpub-
lished master's thesis, Dallas Theological Seminary, 1977, p. 17.

21, W, J. Grier, The Momentous Event (Edinburgh: Banner of Truth, 1970
[1945)), p. 26 and Louis Berkhot, The History of Chrisizan Doctrines (Grand Rapids,
ME Baker Book House, [1937] 1975), p. 262.

22, Philip Schaff, A Religious Encyclopedia: Or Dictionary of Biblical, Historical, Doc-
trinal, and Practical Theology (New York: Funk and Wagnalls, 1883), vol. 3, pp. 1514-15.

23, House and Ice, Dominion Theology, p. 209,
