104 House Divided

that conviction. | The abiding validity of God’s law is examined in
terms of the character of God, the life and example of the
Messiah, the sanctifying work of the Holy Spirit, the blessings
promised for conformity to the law, the underlying unity of God’s
covenants, the nature of God’s word as a norm, the ethical rele-
vance of the entire Bible, New Testament concepts of grace and
faith and love, New Testament ethical themes like kingdom right-
eousness or holiness or the fruit of the Spirit, numerous particular
passages in the New Testament which assume or state the author-
ity of the Old Testament law, repeated New Testament ethical
judgments on particular issues which apply the Old Testament
law, a host of particular texts and teachings of Jesus or the Apos-
tles, and Matthew 5:17-20.

Of all of these mutually supportive and independently chal-
lenging lines of reasoning, House and Ice examine only one in
Dominion Theology: the exposition of Matthew 5:17-19. The other
texts of Scripture elaborated upon in the many other theonomic
arguments are not given specific attention as such. Therefore,
although Matthew 5 is the literary center of attention on the ques-
tion before us, the opinion of House and Ice is premature and
strained when they comment: “If Bahnsen’s view on this passage is
wrong, then his entire thesis is in dowbt.”* The fact is that the dis-
pensational rebuttal of theonomic ethics would have considerably
more work to do before House and Ice would be home free. As it
is, however, even the theonomic support gained from Matthew
5:17-20 is not left “in doubt” by their discussion of the text.?

1, The following list touches only on the lines of argumentation found in
‘Theonomy in. Christian Ethics (ex. ed.; Phillipsburg, NJ: Presbyterian and Reformed,
[1977] 1984) and By This Standard: The Authority of God's Law Teday (Tyler, TX: In-
stitute for Christian Economics, 1985), out of the many which could be men-
tioned and which have been set forth by Reformed scholars for centuries.

2, H, Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse?
(Portland, OR: Multnomah, 1988), p. 104 (emphasis mine),

3, For an extensive analysis af the text and theology af this passage, see Bahnsen,
Theonomy, Chapter 2,
