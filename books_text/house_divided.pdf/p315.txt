274 House Divided

give is simplistic. Rome simply came too slow. But how do they
know that the “speed” of lightning is the issue here? What if it is
the specifically mentioned matter of-direction: from east to west? In
Luke 10:18 the fall of Satan from power was likened to lightning.
There the downward direction was the point, not how rapidly he
accelerated. As a matter of historical fact, the Remans (the New
Testament version of God’s rod of anger against Israel, cp. Isa.
Isa. 10: 5) entered Judea from the east .*°

4. The Search for the Second Coming. “If [Jordan] were to take the
whole of the Olivet discourse as already fulfilled, as Chilton does
the whole book of Revelation, then he is left with the problem of
where does the Bible actually teach the second coming?” We
must ask, why would one fave to find the Second Coming in the
Olivet Discourse? But, in fact, preterists find it in the Matthew
24:36-25:46, as House and Ice well know.”

5, Is Preterism Intrinsically Erroneous? We were quite surprised
to read the following comment, which is set out as a criticism:
“The Reconstructionist, preterist approach means that many per-
sonalities, events, and places referred to prophetically in the
Scriptures have already been fulfilled.”®* So? As a matter of fact,
that is true; but as a general statement, how is it harmful? What
of the dozens of Bible verses related to Christ’s first coming —
thirty-one of which they list! 84 Regarding the verses ‘Azy choose as
references to Christ’s first coming, they are preteristic!

Both the orthodox Jews today and those in antiquity have felt
that Christians are misapplying prophecies to past events: In dis-
cussing Daniel 9, Athanasius says: “So the Jews are trifling, and
the time in question, which they refer to the future, is actually come.
For when did prophet and vision cease from Israel, save when Christ
came, the Holy of Holies? For it is a sign, and an important proof,

80, Josephus, Ware 3:1. See Eduard Lohse, The New Testament Environment,
trans. by John E, Steely (Nashville, TN: Abingdon, 1976), pp. 48i.

81, House and Ice, Dominion Theology, p. 298, Their statements regarding
Chilton and Jordan are erroneous, see Chapter 18,

82, Ibid., p. 297,

83, Ibid, p. 54,

84, Ibid., p. 321,
