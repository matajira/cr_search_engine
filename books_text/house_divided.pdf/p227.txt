182 House Divided

and among them, says Christ. Hence, even in Christ’s ministry
men were pressing into it (Luke 16:16).

Paul speaks to the Colossians in a way quite agreeable to the
reformed view of the coming of the kingdom: Colossians 1:12-13:
“Giving thanks unto the Father, which bath made us meet to be
partakers of the inheritance of the saints in light: Who bath de-
livered us from the power of darkness, and bath translated us into
the kingdom of his dear Son.” Inarguably He is speaking of
Christ’s kingdom, for He calls it “the kingdom of his dear Son.”
dust as clear is the fact that the “translation” of those saints nearly
2000 years ago was considered a past fact, not a future prospect.
Paul uses aorist tense verbs when he speaks of their being “de-
livered” and “translated.” He does the same in 1 ‘Thessalonians
2:12, He even speaks of those who were his helpers in the ministry
“for the kingdom of God” (Col. 4:10).

Thus, John follows suit in Revelation 1:6 and 9: “And [Christ]
bath made us kings and priests unto God and his Father. ...1
John, who also am your brother, and companion in tribulation,
and in the kingdom and patience of Jesus Christ .“ In these verses
John speaks of the Christians of the Seven Churches of Asia (Rev.
1:4, 11; 2-8) as already “made” (aorist tense) to be “a kingdom” (Lit-
erall y). In fact, John is already a fellow with them in the “king-
dom” (Rev. 1:9). Clearly the kingdom came in its initial stages
during Christ’s ministry. Just as clearly can we assert: That it was
not postponed is evident from the biblical record.

Christ Presented Himeelf as King

The Triumphal®? Entry of Christ is interesting in this regard:
The people cried, “Hosanna: Blessed is the King of Israel that
cometh in the name of the Lord. And Jesus, when he had found a

23, We hesitate to use the term “Triumphal” of His entry for two reasons: (1)
House and Ice seem to find such terminology indicating triumph distasteful (see
their Chapter 8 and Appendix C), (2) In the dispensational view Christ was in no
way triumphal in His entry, for He was rejected in His presentation of Himself a5
an earthly-political king and had to postpone the kingdom, which He had
declared was “at hand” (House and Ice, Dominion Theology, pp. 173, 279), See
Chapter 14, note 36.
