xxxvill House Divided

to books. There is a reason for my style. Both my outraged critics
and my worried associates have failed to understood the extent to
which I am self-conscious in what I do. You can date the appear-
ance of these hostile introductions: the early 1980's.

I have tried to model my polemical writings after Martin
Luther’s tracts against his theological opponents, of whom there
were many. Had he confined his criticisms to a strictly academic
defense of his 95 theses, he would not be remembered by anyone
today except a handful of specialists in church history, who would
probably be Roman Catholics, (Have you ever read the 95 theses?
Yes, I mean even you seminary professors.) Had Luther persisted
stubbornly in a purely academic strategy, he would eventually
have been burned at the stake. But he understood the possibilities
for radical institutional change that were offered by the printing
press, and he pioneered the polemical pamphlet. You can find few
examples in subsequent history that match Luther's tracts for in-
vective, vitriol, and contempt for one’s opponents. I am only a
pale imitation of Luther in this regard. Yet the heirs of Luther’s
Reformation click their tongues and shake their heads at my style,
as if they did not owe their very freedom to criticize me to the so-
cial and political effects of Luther’s pamphlets. They act as though
they believe that the Reformation was little more than a scheduled
debate in the faculty lounge.

Academic Suppression in the Name of Jesus

There has been method in my seeming madness. By 1980, I
had waited patiently for over five years for scholars or polemicists
in any Christian camp to respond to R. J. Rushdoony’s monu-
mental book, The Institutes of Biblical Law (Craig Press, 1973). His
two dozen books, several of them classics,*° were greeted with
stony silence. This was especially obvious with respect to the Insti-
tutes, The only important exceptions were Harold O. J. Brown’s
1974 Christianity Today essay on the major books of 1973, in which

40. 1 refer to The Messianic Character of American Education, Freud, Foundations of
Social Order, and The One and the Many.
