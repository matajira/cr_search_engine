Hal Lindsey’s “The Road to Holocaust” 371

203, 218, 220, 225, 261); sometimes breathing marks are absent
(pp. 200,204,219,228,270, 227); sometimes the breathing marks
are wrong (p. 203); sometimes he accidentally substitutes accents
for breathing marks (p. 53) and breathing marks for accents (p.
218). He erroneously transliterates the Greek fara by spelling it
pasa.’ If he is going to employ the Greek characters he ought to
do so properly and carefully.

Of Luke 18:8 Lindsey writes: “In the original Greek, this ques-
tion assumes a negative answer. The original text has a definite
article before faith, which in context means ‘this kind of faith.’” 17 But
it does not “assume” a negative answer. The classic Greek gram-
mar Funk-Blass-Debrunner notes “that when an interrogative par-
ticle is used [as in Luke 18:8, KLG], oz is employed to suggest an
affirmative answer, me (meti) a negative reply... .”4® But neither
of these particular particles occur here and so the answer to the
question is “ambiguous,” 19 in that the one used here (ara) implies
only “anxiety or impatience.” Lindsey’s reference to the definite
article before “faith” has absolutely nothing to do with the ex-
pected answer, it merely defines that to which the question refers,
not whether a negative is expected.

Lindsey parenthetically notes in a translation of Matthew
28:19: “make disciples of [Greek= cut of].”2! Elsewhere he notes of
this very text that “the genitive construction means ‘a part out of a
whole” 2? Two fundamental problems present themselves here:
(1) Such evidence would be a mere interpretive bias on his part.
The genitival construction can bear ten or more different mean-

16, Ibid. p. 220.

417, Ibid., p. 48.

48, Robert W. Funk, ed., F, Blass and A, Debrunner, A Greek Grammar of the
New Testament and Other Early Christian Literature (Chicago: University of Chicago
Press, 1961), p. 226 (section 440).

19, Ibid.

20, William F, Arndt and F, Wilbur Gingrich, A Greek-English Lexicon of the
New Testament and Other Early Christian Literature (Chicago: University of Chicago
Press, 1957), p. 103.

21, Lindsey, Road to Holocaust, p. 49.

22, Ibid., p. 277.
