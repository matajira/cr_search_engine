352 House Divided

Instead of embracing the thesis of the Reconstructionists that
laws that make up the Mosaic legislation (properly interpreted
and applied through the grid of the New Covenant)‘are binding
in the New Covenant era, House and Ice claim that Christians
are to use “wisdom” in determining the application of the law. The
authors never show us how this works in actual practice or how
this might differ from Christian Reconstruction. One is left with
the impression, however, that Reconstructionists do not use wis-
dom when they attempt to apply the Mosaic legislation in the
New Covenant era.

Here’s another more fundamental question: What if the Recon-
structionist and the neo-dispensationalist 1° come to the same con-
clusion on the applicability of a Mosaic law for the church age in
the area of civil penalties, say, for sodomy? What if a group of
neo-dispensationalists are elected to public office and determine,
based on wisdom, that the death penalty for a public act of
sodomy is the wise thing to implement? Both the Reconstruction-
ist and neo-dispensationalist have come to the same conclusion.

9, The following is excerpted from a letter by me addressed to the editor of
Christianity Today in response to “The Theonomic Urge” (April 21, 1989:

“Generally, the movement's proponents hold that the civil laws of
‘Old Testament, theocratic Israel are normative for all societies in all
times.” We believe that it is more correct to insist that the whole Bible is
normative, This implies that in considering how to apply the Old Testa-
ment laws, we must also consider the implications of the death, resur-
rection, and ascension of Christ, the pouring out of the Spirit, and the
breaking down of the wall between Jew and Gentile, There are elements
of continuity and discontinuity between the covenants, and we cannot
afford to ignore either, And, if we affirm that the Bible is God’s inerrant
Word, how can we in good conscience ignore its teachings in any area
of life?

10. Neo-dispensationalist is a reference to the hybrid view of dispensational-
ism espoused by House and Ice. This is recognized by a recent reviewer of
Dominion Theology “Its appreciation of the Law and its proclamation of salvation
by grace in every age move it closer to the Reconstructionists’ home camp,”
Robert Drake, ‘What Should the Kingdom of God Look Like?,” World (February
11, 1989), p. 13,
