Theological Schizophrenia 363

Comment: For Lindsey, the rebirth of Israel in 1948 is the key to
Bible prophecy. A generation, says Lindsey, “is something like
forty years.” By adding forty years to 1948, we get 1988. But Lind-
sey is a pre-tribulationist. He believes that the rapture occurs
seven years before Jesus returns to set up His millennial kingdom.
This means that the rapture should have occurred sometime around
1981 with 1988 being the year of the Second Coming. This is date
setting with a vengeance. Of course, Lindsey tries to cover him-
self by hedging his bets with ‘something lke forty years,” “if this isa
correct deduction,” “forty years or so,” and ‘could take place.”
Lindsey’s prophetic guesses were not considered guesses by his
readers. Many took the rebirth of Israel and the forty-year gener-
ation scenario as date setting. Gary Wilburn, in his review of the
film version of The Late Great Planet Earth, seems to agree that the
“48-88 scenario makes up the general thesis of the book: “The
world must end within one generation from the birth of the State
of Israel. Any opinion of world affairs that does not dovetail with
this prophecy is dismissed.”22 Lindsey in his The 19803: Countdown
to Armageddon, while still hedging, leads his readers to a pre-1990
climax of history: “Many people will be shocked by what will happen
in the very near future. The decade of the 1980's could very well be the
last decade of history a-8 we know i123 Well, we are about to go into the
1990s. Why should we take Lindsey seriously on anything he says?
While Lindsey has not said that we will not see the 90s, his inti-
mations lead many Christians to believe that the end is quite near.

(2)

In an interview published in Christianity Today in April 1977,
Ward Gasque asked Lindsey, “But what if you're wrong?” Lind-
sey replied: “Well, there’s just a split second’s difference between
a hero and a bum. I didn’t ask to be a hero, but I guess I have be-
come one in the Christian community. So I accept it. But if ’'m
wrong about this, I guess Pll become a bum.”*

32. Gary Wilburn, “The Doomsday Chic,” CAristionity Today, 22 (January 27,
1978), p. 22,

33, Hal Lindsey, The 1980's: Countdoum to Armageddon (New York: Bantam
Books, 1980), p. 8.

34, W, Ward Gasque, “Future Fact? Future Fiction?,” Christianity Today, 21
(April 15, 1977), p. 40.
