Ethical Lapses 329

cal, and economic aspects of society during the church age in the
way Reconstructionists affirm?”’”

The Narrow Christian Mission:

Jehovah's Witnesses: “From the foregoing one thing is clearly
seen: It is not the duty of the remnant of the congregation yet on
earth ... to enter into political alliances with the nations of this
world. . . . No; the responsibility of the remnant ... is to praise
Jehovah’s name and bear witness to his supremacy and glory.
How? By ministering the spiritual ‘food at the proper time’ to
those hungering and thirsting for the truth, inviting all to partake
of the ‘water of life freely.’ ”7®

Dispensationalists: “The . . . directives given to the church [are]
to carry out its mission of individual evangelism and teaching in
order to build up believers to live in faithfulness to our Lord dur-
ing this dark age.”’® “Premillennialist . . . believe that the em-
phasis on social and cultural issues reflect the purpose God has for
this age, namely the individual duties of a Christian before a
watching world, rather than the redemption and conversion of in-
stitutions.”®°

Satan’s Dominant Rule:

Jehovah’s Witnesses: “From [Luke 4:6] it is unreasonable to think
anything else than that all world government were the Devil's
property. How else could he have offered them to Christ?”

Dispensationalists: Satan “is ‘the ruler of this world? which
means he is constantly at work in human government and its po-
litical systems.”®? “{KJosmos . .. refer[s] to the organized system
under the domination of Satan...“

77, House and Ice, Dominion Theology, pp. 341-42, 150,

78. Let God Be True, p, 132,

79, House and Ice, Dominion Theology, p, 154,

80, Ibid., p, 155.

81, Let Cod Be Tree, p. 56.

82, Hal Lindsey, Satan Is Alive and Well on Planet Earth (Grand Rapids, MI:
Zondervan, 1972), p. 77.

83, Pentecost, Things to Come, p. 131,
