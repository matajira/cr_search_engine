Publisher’s Foreword Xxix

with the history of dogmatic and exegetical theology. There has
been a continuing debate over eschatology among church theolog-
ians for at least 1,800 years, and over postmillennialism since the
sixteenth century. But Rev. Ice would have us believe that not one
of these postmillennial arguments has ever penetrated his con-
sciousness, an assertion which I am quite ready to believe.

One thing is certain: nobody debated pre-tribulational dispen-
sationalism prior to 1830. It did not exist before then. This is one
aspect of Rev. Ice’s dilemma. There are others.

My Letier Apparently Triggers His Paradigm Shift

I had pointed some of this out to him in private corre
spondence two years before our public debate, but he was not per-
suaded. In fact, in 1986 he sent me a letter of about a dozen pages
telling me in great detail how I needed to revise my published.
statements regarding the history of dispensationalism, since I had
relied too heavily on Dave MacPherson’s revisionism. He refers to
this letter in his Preface, 27 though not that it was a computer let-
ter, printed by a dot-matrix printer with a fading ribbon, and had
been sent to me on uncut computer print-out pages. It unfolded
like an accordion. I did not want to strain my eyes or my patience
by reading it. So, I marked in its margins that I would be willing
to read all this if he could get it published in book form, but not
before. I reminded him that the dispensational camp had stopped.
writing books defending the system in the mid-1960’s, and that the
movement desperately needs modern books that can at least give
the illusion of successfully defending the system. Maybe his could
be such a book. Thus, we now have Dominion Theology: Blessing or
Curse?, as he freely admits .28

My personal challenge had apparently triggered something in
Rev. Ice’s mind. Up until that time, he had steadfastly publicly
proclaimed himself to be a Christian Reeonstructionist. He had.
been the pastor of David Schnittger, the author of the excellent

27. Ice, “Preface,” Dominion Theology, p. 8.
28, Ibid., p. 9.
