PREFACE

WHY I COULD NOT
REMAIN A DISPENSATIONALIST

Rev. Kenneth L. Gentry, Th. D.

From 1966 until 1975 I was a dispensationalist, I was attracted
to the movement because it boasted of a consistent biblical out-
look, which could explain the times. I was saved by the grace of
the Lord Jesus Christ at a dispensationalist youth camp in Boca
Raton, Florida; I attended a dispensationalist church (Calvary
Bible Church, Chattanooga, Tennessee) pastored by my dispen-
sationalist uncle (Rev. John S. Lanham); I graduated from a dis-
pensationalist college (Tennessee Temple University, Chattanooga,
Tennessee) with a degree in Bible; I attended a dispensationalist
seminary for two years (Grace Theological Seminary, Winona
Lake, Indiana); and I even owned a loose-leaf New Scofield Refer-
ence Bible, filled with all the notes necessary to make and keep one
a dispensationalist.

In many ways it was great being a dispensationalist, yet also
frustrating. It was great to know that we had the reasons for the
problems of modern society. It was frustrating that as a Christian
I was not expected to have any hope of successfully promoting any
biblical solution to those problems, even though I was taught that
the earth is the Lord’s and the gospel is the power of God unto sal-
vation. At the age of twenty, I even turned down a life insurance
policy because I was convinced that I would not be around long
enough to have a family that would need it. My college days were
lived “with anticipation, with excitement” because I thought “we
should be living like persons who don’t expect to be around much

xlvii
