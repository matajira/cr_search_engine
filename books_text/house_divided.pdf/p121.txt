72 House Divided

and it is easy to find other theonomists who can present cogent
counter-arguments to North (although his position might be right).

Distortion by Tailored Expression

Another variety of misrepresentation employed by House and.
Tee is to put a slant on their account of Reconstructionist beliefs
which edits out the true intent or meaning of those beliefs. This is
conspicuous when they assert that “theonomists center God’s com-
mands for today on Old Testament law.” '’ Incredibly, the authors
attempt in their very next sentence to substantiate this charge of
Old-Testament-preoccupation by quoting a Rushdoony state-
ment which does not even mention the Old Testament as such!
(Thus, Rushdoony flatly states that‘... the law is the way of
sanctification. ‘”) The authors are so obsessed with their precon-
ceived criticism of theonomists that they read it into theonomist
statements without the slightest textual warrant, Such overbear-
ing bias is not flattering, especially when the authors elsewhere
contradict (and refute) their own charge that theonomists nar-
rowly stress the Old Testament ! Contrary to the impression they
try to communicate to their readers by comments and expressions
used elsewhere, they quote Rushdoony: “The Biblical concept of
Jaw is broader than the legal codes of the Mosaic formulation, It
applies to the divine word and instruction in its totality”? An
honest report would be that theonomists teach that we are to fol-
low the moral norms revealed by God throughout the Bible, “in-
cluding” those found in the Old Testament (cf. 2 Tim. 3:16-17) —
but House and Ice turn this into an obsessive “centering on” the Old
Testament. Such twisting of what theonomists mean borders on
deceit. Notice how they do it again. Theonomists uphold the jus-
tice of God found in the Old and New Testaments (in fact, main-
taining that the Old must be interpreted in light of the New). But
listen to how House and Ice put it. “The Reconstructionists be-
lieve that the Law of God, or Biblical Law, as codified in the Old

17, bbid., p. 29,
18, id., p. 35.
