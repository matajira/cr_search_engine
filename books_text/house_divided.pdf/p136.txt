The Theological Concept of Law 87

at any time flows from his perfect character. .. . Revealed law
contains many instructions — both principles and specific direc-
tives — which remain the same despite changes in temporal factors
such as time, place, dispensation, or immediate divine purpose.
These are continuities, '

This continuity of moral character (in God and thus in His re-
vealed moral principles) does not preclude different expressions or
different applications of God’s moral demands suited to the situa-
tion or culture which is addressed (e. g., self'giving love to my
neighbor can be expressed in terms of the specific details of an
overloaded donkey as well as someone mugged on the Jericho
road: Exodus 23: 5; Luke 10:30-37). Theonomists recognize this, as
is evident in the detailed exposition found in Chapter 3 above.
They also, as acknowledged by House and Ice,® teach that God’s
immutable character does not prevent “changes in God’s instruc-
tions,” where He so orders (e. g., the sacrificial cultus and temple:
Heb. 9-10). Nor do theonomists deny that in terms of His imme-
diate purposes, God the Lawgiver is free to make exceptions to
His ordinary requirements (e. g., sparing Cain or David).’

House and Ice say that there have been “different manifesta-
tions” of God’s moral character revealed to different groups in
different ways, mentioning as examples: general revelation, the
garden, divine visitations, communication with Abraham, and.
the Mosaic laws Theonomists could say the same. They would
also be able to endorse the statement that:

Every revelation of God’s law reflects his character, regardless of
when it was given. Moreover, there is a basic consistency in God’s
law, with variations based only on cultural factors and God’s
purposes for mankind at the time the law is given. ‘

Given such agreement between our two authors and theono-
mists, what is the nature of their objection to theonomic reasoning

5, House and Ice, Dominion. Theology, pp. 86, 87-88,
6, Ibid, p. 88.

7. Ibid., p. 89,

8. Ibid., pp. 86-87.

9. Ibid., p. 87.
