For Further Reading 383

Crossway, 1988. Theologians of various persuasions discuss
relationship of Old and New Covenants; evidence of impor-
tant modifications in dispensationalism.

Gerstner, John H. A Primer on Dispensationalism. Phillipsburg, NJ:
Presbyterian and Reformed, 1982. Brief critique of dispensa-
tionalism’s “division” of the Bible.

Halsell, Grace. Prophecy and Poltitcs: Militant Evangelists on the Road
to Nuclear War. Westport, CN: Lawrence Hill, 1986. Journalist
enters the world of dispensationalist Zionism, and warns of
political dangers of dispensationalist prophetic teachings.

Hendriksen, William. Israel and the Bible. Grand Rapids, MI:
Baker, 1968. Amillennial discussion of the place of the Jews in
the New Covenant.

Hendriksen, William. More Than Conquerors: An Interpretation of the
Book of Revelation. Grand Rapids, MI: Baker, (1940) 1982.
Amillennial commentary on Revelation.

Jones, R. Bradley. The Great Tribulation. Grand Rapids, MI:
Baker, 1980. Amillennial study of major themes in millennial
discussions.

Jones, R. Bradley. What, Where, and When Is the Millennium?
Grand Rapids, MI: Baker, 1975. Amillennial; includes critical
discussion of dispensationalism.

Jordan, James B. The Sociology of the Church, Tyler, TX: Geneva
Ministries, 1986. Chapter entitled, ‘Christian Zionism and
Messianic Judaism,” contrasts the dispensational Zionism of
Jerry Falwell, et al. with classic early dispensationalism.

Kimball, William R. What the Bible Says About the Great Tribulation.
Phillipsburg, NJ: Presbyterian and Reformed, 1983. Re-
examines the Olivet Discourse and the supposed biblical basis
for the notion of a future tribulation.

Kimball, William R. The Rapture: A Question of Timing. Grand.
Rapids, MI: Baker, 1985. Exegetical critique of the pre-trib
rapture theory.
