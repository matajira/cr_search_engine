The Theological Concept of Law 93

are not binding in all ages and upon all cultures — items which
must be distinguished and identified on the basis of Scriptural
teaching. 23 The other Mosaic regulations and precepts communi-
cate God’s unchanging moral principles — and indeed are identical
to those precepts which correspond to the divine moral principles
learned through general revelation. 24

Dispensationalists disagree with this perspective at a few
crucial points. First, they claim that none of the Mosaic regulations
or precepts are 25 such universally obligatory, but were binding
only on Old Testament Israel. Christians today are under the reg-
ulations and precepts only of the Adamic covenant, the Noahic
covenant, and the New Testament. 25 House and Ice assert that
“the Mosaic law given to Israel” is not binding upon “any other
nation not under the [Mosaic] covenant .””* This means that “the
law of God .. . and the law of Moses are two different things."2"

Second, dispensationalists claim that all of the details or regula-
tions of the Mosaic code are of the same character. They all “reflect
the character of God,” and the entire Mosaic code “stands or falls as a
whole.” Third, the precepts learned in general revelation are not
equivalent to the regulations and precepts of the Mosaic revelation.
House and Ice speak of “the written law given to Moses” being
different from “the law written on the hearts of the Gentiles.”*

Back to the Law and God’s Character

What should we make of the dispensationalist perspective
elaborated here? House and Ice maintain that the law of God is
“different” from the law of Moses. (1) Obviously the Mosaic code

23, Eg,, the laying aside of animal sacrifices (Heb. 9) and dietary restrictions
(Acts 10),

4, Eg,, the prohibition of homosexuality learned in the Mosaic law (Lev.
18:22; 20:13), written epistle (1 Cor.6:9; 1 Tim, 1:8-10), as well as general revela-
tion (Rem, 1:24, 26-27, 32),

25, House and Ice, Dominion Theology, p. 119,

26, Ibid., p. 100.

27, Ibid. (emphasis mine),

28, Ibid., p. 89 (emphasis mine),

29, Ibid., p. 129,
