FOR FURTHER READING

A Positive Ethical Alternative: Biblical Law
Bahnsen, Greg L. By This Standard: The Authority of God’s Law To-
day. Tyler, TX; Institute for Christian Economics, 1985.
Bahnsen, Greg L. Theonomy and Christian Ethics. Phillipsburg,
New Jersey: Presbyterian & Reformed, (1977) 1984.

Jordan, James B. The Law of the Covenant: An Exposition of Exodus
21-23. Tyler, TX: Institute for Christian Economics, 1984.
North, Gary. Tools of Dominion: The Case Laws of Exodus, 2 vols.

Tyler, TX: Institute for Christian Economics, 1989.
Rushdoony, Rousas John. The Institutes of Biblical Law. Phillips-
burg, New Jersey: Presbyterian & Reformed, (1973) 1988.
Rushdoony, Rousas John. Law and Liberty. Vallecito, CA: Ross

House Books, (1971) 1988.

General Works on the Millennium

Clouse, Robert G., ed. The Meaning of the Millennium: Four Views.
Downers Grove, IL: InterVarsity Press, 1977. The four major
views of the millennium presented by advocates of each view.

Erickson, Millard J. Contemporary Options in Eschatology: A Study of
the Millennium. Grand Rapids, MI: Baker, 1977. Examines mod-
ern views of eschatology, the millennium, and the tribulation.

Works on Dispensationalism

Allis, Oswald T. Prophecy and the Church. Philadelphia, PA: Presby-
terian and Reformed, 1945. Classic comprehensive critique of
dispensationalism.

381
