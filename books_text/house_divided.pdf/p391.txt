Theological Schizophrenia 355

serving of full protection of the law. '* This is the only passage in
Scripture that clearly shows that the unborn child was protected.
by law. Finally, why take the prohibitions against these crimes but
not the punishments?

Tommy Ice on Law

Tommy Ice states that he “used to be a Christian Reconstruc-
tionist.” He has remarked on a number of occasions that those
who call themselves Christian Reconstructionists are attracted to
the position because they have found a theology that supports
their cultural and societal activism. Tommy Ice believes that for
Reconstructionists, ideology supports theology. But doesn’t this
assessment cut both ways? Can’t the Reconstructionist assert that
a dispensationalist like Tommy Ice has chosen a theological sys-
tem that releases him from cultural activism? Why doesn’t Tommy
Ice come out and say that it is wrong — even heretical — for Chris-
tians to be involved in anything beyond evangelism? Why does he
still advocate a message of societal application of the law of God if
it is useless and unbiblical to do s0? '5

What standard does Tommy Ice ask us to use as a standard for
righteousness? What should the Christian activist use? The fol-
lowing quotation is the epitome of schizophrenia. It presupposes a
culturally activistic Christian and the application of the Mosaic
legislation.

14. H, Wayne House, “Miscarriage or Premature Birth: Additional Thoughts
on Exodus 21:22 -25," Westminster Theological Journal, vol. 41, no. 1 (Fall 1978), pp.
108-23,

15. Tommy Ice writes: “Nothing in dispensationalism prohibits a strong in-
volvement in social issues.” “Dispensationalism, Date-Setting and Distortion,”
Biblical Perspectives, vol 1, no. 5 (September/October 1988), p. 6. This quotation
by Ice seems to contradict other statements made by him that involvement in so-
cial issues is not the job of the church during the so-called “church age.” In a letter
written to Houston physician Dr. Steven F. Hotze, Ice writes: “Please, Steve,
show me the New Testament passages which instruct us with the obligation of
providing a ‘Christian altemative in our culture!’”“A strong involvement in 50-
sial issues” by Christians assumes a “Christian altemative.”
