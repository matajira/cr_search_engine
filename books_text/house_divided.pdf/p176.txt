The Civil and Cultural Use of the Law 129

What is defective with this position ought to have been notice-
able when House and Ice were thinking it through. Upon reflec-
tion, few people today would really want the moral standards for
civil government limited to the revelation of God’s will to Noah.
First, the revelation to Noah does not cover many (indeed, most)
of the matters which have to be dealt with by civil magistrates in
order to protect their citizens; for instance, the revelation to Noah
says nothing about stealing and fraud, nothing about rape and
kidnapping, nothing about perjury and contracts, nothing about
compensation for damages, etc. House and Ice unduly restrict the
function and authority of the civil government by tying its powers
solely to the Noahic covenant.

Second, the one major civil provision made in the Noahic cov-
enant deals with the punishment of murder. However, most peo-
ple would prefer to have the advantage of progressive revelation,
looking to the Mosaic law for the explanations and qualifications
which are necessary to apply the law about murder. For instance,
the Noahic revelation takes no account of the difference between
accidental and premeditated homicide. If the moral authority of
the civil magistrate today were limited to the Noahic covenant, as
House and Ice say, then we would be prevented from applying
such a qualification in our courts today. It simply is not wise for us
to turn away from the help which God’s revelation makes avail-
able to us after the time of Noah.

Third, it is obvious that the apostle Paul did not think of the
civil magistrate’s authority in the limited terms proposed by
House and Ice. When Paul spoke of the legitimate powers of the
state in Remans 13, he chose the example of collecting taxes (v. 7).
However, there is absolutely no hint of such a thing in the Noahic
covenant. Paul was either consciously supplementing the Noahic
standards for civil rule (taxes would have been morally illegitimate
prior to that point), or he did not adopt the artificial restriction of
civil prerogatives and respo”nsibilities to the Noahic covenant.

Fourth, we might imagine that House and Ice begin now to
work hard and creatively in order to devise some way to justify
civil taxes, manslaughter distinctions, and prohibitions about
