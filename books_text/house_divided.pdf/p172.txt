8
THE CIVIL AND CULTURAL USE OF THE LAW

A reply to the argument that God’s Law has no moral authority in
politics and society.

The opposition of dispensationalists House and Ice to the
Reconstructionist vision of ethics and the Christian life comes to a
head over the issue of applying God’s law (including the Mosaic
revelation of its precepts) to modern society and the civil magis-
trate. This seems to be the thing which dispensationalists find
especially controversial about Christian Reconstruction. Were it
not for this, it is likely the criticism would be toned down, and the
effort put into refuting Reconstructionist ethics would be less in-
tense. However, given the theonomic understanding of the uni-
versal and perpetual authority of God’s law, it would be arbitrary
special pleading to exempt society and its civil leaders from moral
obligation to the provisions of God’s law which speak to them,
provide guidance for them, and pose an absolute ethical standard
by which to evaluate them.

It remains for us, then, to defend the Reconstructionist posi-
tion against the complaints and arguments of House and Ice per-
taining to the civil magistrate and social transformation. As it
turns out, much of what needs to be said in response to the cri-
tique of Dominion Theology as it addresses these subjects has
already been written in previous chapters. The major drawbacks
faced by critics House and Ice are that they so extensively misrep-
resent the Reconstructionist position (see Chapter 5 above) and
employ false standards and numerous kinds of fallacious argu-
mentation throughout their book (see Chapter 4 above).

125
