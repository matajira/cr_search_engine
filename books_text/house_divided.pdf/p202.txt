The Expectation of the Kingdom 157

history, using Mary for His glory. The prognostication is guided
by the Victory Theme, not despair and lamentation. She recog-
nizes that in the soon coming birth of Christ God will do “mighty
deeds with His arm,” He will “scatter the proud” (v. 51). He will
“bring down rulers” and “exalt those who are humble” (v. 52). He
will fill “the hungry with good things” (v. 53). He will do it through
His people (v. 54) in keeping with the Abrahamic Covenant (v.
55). There is absolutely no intimation of defeat here, nor of a
postponed millennial reign.

Zacharias’s prophecy continues the glad tidings. He sees Christ's
birth as bringing tidings of victory for God’s people over their ene-
mies (w. 68-71). This, again, is in fulfillment of the Abrahamic
Covenant (v. 73). Christ is the sunrise that will “shine upon those
who sit in darkness and the shadow of death” (vv. 78-79). Else-
where this refers to the Gentiles (Isa. 9:1,2; Matt. 4:16). This light is
later seen as a positive force, dispelling darkness in the present
age (Rem. 13:11-13; 1 John 2:8).

Summary

1, There is a strand of victorious expectation of the spread of
righteousness, which begins in the Old Testament and continues
into the New Testament.

2, At the creation of man, God so designed and commis-
sioned man as to expect the worldwide operation of righteous cul-
tural endeavor (Gen. 1:26-27). The Gospel Victory Theme of
eschatological expectation comports well with God’s creational
purpose.

3. A vital aspect of the image of God in man has to do with his
drive to dominion, a drive that must be governed by godly princi-
ples in order to fulfill its true intent (Gen. 1:26-27).

4, The first prophecy in Scripture, Genesis 3:15, expects a
history-long struggle between Christ and Satan, with Christ ulti-
mately winning the victory in history.

5. The Abrahamic Covenant promises the spread of salvation
to “all the families of the earth” (Gen. 12: If. ), The Gospel is the
tool for the spread of the Abrahamic blessings (see Gal. 3:8, 29).
