xiii House Divided

Gentlemen of the closed campus and monopolistic classroom,
your free ride is just about over. Better get over to the library and
do some intellectual calisthenics.

No More Subsidies

Because my business and my foundation have been graciously
provided with sufficient funds, I have been given the opportunity,
and therefore the moral responsibility, to break through this sys-
tematic black-out. It has taken a lot of money, a lot of books, and
a lot of acerbic introductions. I decided years ago that I would
have to depart from the tradition of American scholarly debate
(though not the British tradition). Scholars in the U. S. are expected
to deal gently with each other in print, a tradition, like tenure,
which has become a kind of academically enforced subsidy of a
vicious, well-entrenched, intellectually corrupt, humanistic, es-
tablishment academic community, so-called — a rag-tag collection
of tenured pedants who have grown intellectually flabby over the
years as a result of institutional and financial insulation.

The Christian academic world, while not equally corrupt, is
analogously flabby. I decided in 1980 to taunt them publicly at
every appropriate opportunity. I saw no other way to expose them
and their charade of intellectual integrity, and no other way to get
them to venture into the intellectual arena to defend themselves.
Once in that public arena, I knew, we in the Reconstructionist
camp would have an opportunity to prove our case.

And prove it Bahnsen and Gentry do. One by one, they very
politely and graciously expose the arguments of House and Ice as
halfbaked, carelessly researched, insupportable, and intellec-
tually dishonest. Part III by Dr. Gentry is gentlemanly almost to a
fault (certainly not my style!); it is also devastating. It reveals to
what depths desperate men will resort in order to defend a visibly
lost intellectual cause. The desperation of dispensationalism to-
day is available for public viewing by men of honest scholarship,
point by point, in Part HI. If House and Ice are the most com-
petent defenders of dispensationalism today, then dispensational-
ism as a system clearly has no tomorrow.
