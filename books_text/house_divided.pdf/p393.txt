Theological Schizophrenia 357

“theonomy” in Kingdoms in Conjlict with statements about the law
in an article published in Transforming Our World, with the title
“The Kingdom of God and Human Kingdoms,*makes one wonder
whether Mr. Colson is not aware of Reconstruction distinctive or
simply unwilling to study them thoughtfully. Of course, there is
always a third alternative. Maybe I don’t understand Charles
Colson. Consider his schizophrenia.

w

Recently I addressed the Texas legislature. . .. I told them
that the only answer to the crime problem is to take nonviolent
criminals out of our prisons and make them pay back their vic-
tims with restitution, This is how we can solve the prison crowd-
ing problem.

The amazing thing was that afterwards they came up to me
one after another and said things like, “That’s a tremendous
idea. Why hasn't anyone thought of that?” I had the privilege of
saying to them, “Read Exodus 22, It is only what God said to
Moses on Mount Sinai thousands of years ago.” ®

Comment: Colson does not take the legislators to natural law or
Adamic or Noahic law. Rather, he refers them to the Mosaic legis-
lation, a set of laws that dispensationalists tell us were unique to
Israel. These laws are not for the Gentile nations, say Scofield and
company. According to dispensationalists, they are Israel-specific
case laws. Even Ted Koppel seems to agree with Colson and.
(maybe) Reconstructionists against (maybe) dispensationalists:
“What Moses brought down from Mount Sinai were not the Ten
suggestions.”®° House and Ice and their “wisdom” approach want to

19, Charles Colson, “The Kingdom of God and Human Kingdoms,” James
M, Boice, ed. Transforming Our World: A Call to Action (Portland, OR: Mult
nomah, 1988), pp. 154-55, Consider ai! of Exodus 22 for amoment:

You shall not allow a sorceress to live (v. 18).
Whoever lies with an animal shall surely be put to death (v. 19).
He who sacrifices to any god, other than to the Lan alone, shall be
utterly destroyed (v. 20).
Colson’s article appears in a book distributed by the publisher of Dominion Theology.
20, Delivered at Duke University’s Commencement in May of 1987.
