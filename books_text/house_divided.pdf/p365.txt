Ethical Lapses 327

with the Jehovah’s Witness cult? It would not be difficult to find,
as they did, similarities. Consider the following as purely illustra-
tive of the error of such analysis, while keeping in mind their in-
troductory statement: ‘We also are not saying that all of the cate-
gories dealt with below are wrong, just that the two traditions
have parallel beliefs.”*”

The Kingdom Was Preached Only to the Jews:

Jehovah's Witnesses: “To the Jews exclusively he preached, say-
ing: Repent, for the kingdom of the heavens has drawn near.’

After this announcement of the Kingdom Jesus went to

John, showing the primary purpose for which he came to earth,
namely, to bear witness to God’s kingdom. . .. But there is no re-
cord they continued to [preach the kingdom] after his ascension
on high. Such announcement would not be appropriate until his
return, ... "6

Dispensationalists: “The ‘gospel of the kingdom’ as announced
by John . . . proclaimed the good news that the promised king-
dom was ‘at hand.’ Although the news at the first advent was re-
stricted to Israel, prior to the second advent it will be announced.
not only to Israel but to the whole world.” “All the signs mentioned
by Christ in Matthew 24 and Luke 21, which were to precede the
setting up of the kingdom, had not been fulfilled, thus preventing
a reoffer of the kingdom in Acts .”°9

The Kingdom Is Wholly Future:

Jehovah's Witnesses: The government which will one day exer-
cise dominion over all the earth [is] God’s kingdom by Christ
Jesus.“"“In the capacity of priests and kings of God they reign a
thousand years with Christ Jesus. This ‘royal priesthood’ is spoken
of by the apostle Peter as ‘a holy nation, a people for special pos-
session, who inherit the Kingdom.”

€7. House and Ice, Dominion Theology, p. 385,
3 58, a God Be True (Brooklyn, NY: Watchtower and Tract Society, 1946), pp.
7, 140.
69, J. Dwight Pentecost, Things to Come: A Study in Biblical Eschatology (Grand.
Rapids, MI; Zondervan/Academie, [1958] 1964), pp. 472, 469,
70, Let God Be Tree, pp. 250-51.
71, Ibid., pp. 137-38.
