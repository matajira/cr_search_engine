196 House Divided

little flock”). If 5/6 of the world’s population were to become
Christian in the next century or two, would they not still be “called
out” from an ethically fallen world? (3) In addition, when the
Lord returns, He will return to a wheat field with tares in it — not
to a tare field with wheat in it (Matt. 13:24-30),

The Full-Orbed Character of Redemption

The basic theological problem with House and Ice is that which
Gary DeMar and Peter Leithart have so well captured in the title
of their recent book: “The Reduction of Christianity.” They restrict
the focus and effect of redemption: “The purpose of the church 1!
in this present age [is] that of a witness.” 2 In the context, that “wit-
ness” is rather narrowly defined: “Believers are here to witness to
the coming kingdom, not to inaugurate the kingdom rule.” " “The
biblical approach is to expose evil with the light (Ephesians 5:13)
and to call men to escape the wrath to come by trusting in Jesus
Christ as their Savior and having their names recorded among those
who will participate in the coming of the future kingdom of God.” ‘*
They aver that the words of the Great Commission “refer exclu-
sively to Christian evangelism and soteriological salvation,” * by
which they mean the salvation of individuals. Of the evangelistic
mandate in Mark 16:15 and Luke 24:46-47, it is claimed: “There is
no language or tone in either of these passages that would support
the notion of Christianizing the world.” ‘ “Our calling in the pres-
ent age is primarily evangelism and discipleship .”*”

Let us consider the biblical data.

11, Here their use of “church” must mean the church universally considered,
i.e,, the mass of believers, rather than the institutional church, This is due to the
next quotation cited.

12, House and Ice, Dominion Theology, p, 165,

13. Citing Saucy, ibid.

14, Ibid, p. 342,

15, Ibid,, p. 151.

16. Ibid., p. 152,

17. Ibid, p. 168,
