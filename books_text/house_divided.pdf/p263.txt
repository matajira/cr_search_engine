220 House Divided

In Remans 13:11-14 and 1 John 2:8 the apostles see the king-
dom light as already shining, ready to dispel the darkness. The
progress and growth of the kingdom will not be thwarted by
Satan, for the “gates of hell will not be able to prevail against it”
(Matt. 16:18). Though slow, it will advance in God’s good time
(cp. 2 Peter 3:8-9).

Tn light of the Gradualism Principle, it is useless for House and
Ice to point to alleged “failures” in the history of the church as evi-
dence that the postmillennial hope, as such, is wrong. 22 Nowhere
in the definition of the postmillennial hope is it necessary that we
already have had the requisite growth; *° theoretically it could well
be entirely future from our time! Postmillennialist Warfield could
admit to the presence of “evil men waxing worse” without forfeit-
ing his postmillennialism: “Some of these evils [of which Paul
speaks] had already broken out in his own times, others were
pushing up the ground preparatory to appearing above it them-
selves. It is historically plain to us, no doubt, that they suitably
describe the state of affairs up to at least our day. But we must re-
member that all the indications are that Paul had the first stages
of ‘the latter times’ in mind, and actually says nothing to imply
either that the evil should long predominate over the good, or that
the whole period should be marked by such disorders .““With
that word of caution in mind, we do not agree, however, that
there has been no progress in history — when the long run is viewed.
We do believe there has been progress made from the times the
Christians were thrown to the lions to the times of the successful es-
tablishment of Dallas Theological Seminary and Multnomah Press.

The postmillennial system has built into it the expectation of
long, slow, incremental growth, as House and Ice are aware. 25

22, “How do Reconatructionists account for, even by their own admiagion, so
much false doctrine and so little orthodoxy in Christendom today?” House and
Ice, Dominion Theology, p. 263, cp. pp. 336-40, 351.

23, There is no postmillennial sine gua non demanding that by the year 1989 (or
any other year) there must be the fullness of the kingdom.

24, Warfield, Biblical and Theological Studies, p. 500.

25, House and Ice, Dominion Theology, pp. 232, 240.
