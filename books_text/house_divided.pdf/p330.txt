Argumentation Evrors 291

the ethical positions of their contemporaries.”'* How does this not
contradict their own concern with compromise?

What are the prospects of Christ’s Church in history, espe-
cially in these “last days”? House and Ice write that God “is dem-
onstrating that, unlike Adam and Israel, this new people, the
church, will be fatthful to him even though it is their lot to suffer per-
secution and conflict during the darkness of this present age.” !”
This idea appears again: “God answers back in history [to Satan],
‘Yes, men made new will serve me and remain loyal even through
suffering and deprivation. Look at the church, my bride !”!* Yet at
other places the Church is said to be destined to unfaithfulness:
“Tragically, this will contribute to the further unfaithfulness of the
church in these last days before the return of Messiah.” At one
point they are led to ask: “If common grace has been increasing,
why has sin progressed so far? Why has the church become so
diluted in her faith and practice?”*° And, “Common grace is on
the decline, especially God’s restraint of evil. This accounts for the
rising apostasy and decline of Christianity."Zl Which is it? Will the
church prove to Satan that it will be faithful? Or will it apostatize?

The Olivet Discourse is found in Matthew 24 and 25, as House
and Ice point out. ?? Of this passage they state: “Reconstruction-
ists believe the Olivet discourse was fulfilled in A. D. 70.”*But
then just a few pages later they admit the truth: “Reconstruction-
ist James Jordan says, verses 36 and following [of Matthew 24]
refer to the second coming.*2+ Which is it? Are their readers to be-
lieve Reconstructionists hold the Olivet Discourse was fulfilled in
A.D. 70? Or that they hold that only the first 34 verses of one of the
two chapters containing it were fulfilled in A. D. 70, with the re-

16. Cited in Gary DeMar, The Debate over Christian Reconstruction (Ft. Worth,
TX: Dominion Press, 1988), p. 185,

17, House and Ice, Dominion Theology, p. 170 (emphasis mine).

18, Ibid., p. 180,

19, Ibid., p. 161 (emphasis mine),

30 ta

 
