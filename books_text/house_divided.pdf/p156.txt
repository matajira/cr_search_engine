108 House Divided

Christ’s teaching on the law in Matthew 5:17-19. Dispensational-
ism presumes the very opposite: namely, that the Mosaic precepts
are not binding, unless they happen to be repeated in the New
Testament. Matthew 5:17-19 settles this conflict between theo-
nomic and dispensational hermeneutics, resolving the issue on the
theonomic side.

Not surprisingly, then, we find that House and Ice !° attempt
to derive from their dispensational interpretation of Matthew 5:18
something which at least suggests the passing away of the entire
law, thus countering the appearance of a categorical endorsement
of its continuing moral validity. 't They give verse 18 this sense:
Within the general framework of all time, not the smallest detail
of the law will pass away until all things (the details of God’s law)
are realized. The exegetical defense of this rather strange way of
taking the Greek is defended in a long footnote. '° But the reasoning
employed there is strained, illogical and grammatically inaccurate.

13, House and Ice, Dominion Theology p. 112.

14, It needs to be noted that House and Ice (p. 111) criticize Bahnsen’s treat-
ment of Matthew 5:18 by saying that he incorrectly translates the (common)
Greek verb genciai as “invalid” [sic: “become invalid”?]. But Bahnsen nowhere
does that. In fact, on the very same page, House and Ice quote Bahnsen’s treat-
ment of Matthew 5:18 and it is quite evident that he translates the Greek verb a5
“ake place.”

15, House and Toe, Dominion Theology, pp. 120-21.

16. House and Ice make “one jot o7 tittle” (v. 17) the supposed antecedent io
the Greek term “all things,” but the two expressions do not agree in either num-
ber or gender. Far from being considered collectively as a plural, “one jot or one
tittle” are presented disjunctively with the emphatic repeating of the adjective
“one” (instead of using it only once to cover and group together “jot” and “tittle”)
Our authors are simply reading into the text what their interpretation will neces-
sitate. The plural conception is not true to Matthew's sense and syntax, Further-
more, if the neuter word “all” were intended to refer back to “one jot or one tittle,”
its gender would ordinarily have agreed with the last-mentioned item in the com-
plex antecedent - but it does not (cf. the feminine word “tittle”). By a violent
abuse of logic applied to the Pindaric construction in Greek, the authors now
“reason” that since a neuter plural word can sometimes take a third singular verb,
therefore the third singular verb attached to phrase “one jot or one tittle” turns
that phrase into a neuter plural! The usage of the Pindaric construction, by the
way, had been greatly weakened by the time of the New Testament, was not
invariable, and applied to words not nominal expressions — much less to complex
nominal expressions ! The fallacious and forced reasoning here is readily refut-
