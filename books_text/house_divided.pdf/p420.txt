384 House Divided

McPherson, Dave. The Incredible Cover-Up. Medford, OR: Omega
Publications, 1975. Revisionist study of the origins of the pre-
trib rapture doctrine.

Masselink, William. Why Thousand Years? or Will the Second Coming
Be Pre-Millennial? Fourth edition. Grand Rapids, MI: Eerd-
mans, 1953. Amillennial critique of chitiasm.

Mauro, Philip. The Seventy Weeks and the Great Tribulation. Swengel,
PA: Reiner Publishers, n.d. Former dispensationalist re-
examines prophecies in Daniel and the Olivet Discourse.

Miladin, George C. [s This Really the End?: A Reformed Analysis of
“The Late Great Planet Earth, ” Cherry Hill, NJ: Mack Publish-
ing, 1972. Brief response to Hal Lindsey’s prophetic works;
concludes with a defense of postmillennial optimism.

Poythress, Vern S. Understanding Dispensationalists. Grand Rapids,
MI: Zondervan/Academie, 1987. Irenic interaction with dis-
pensationalism, focusing on hermeneutical issues.

Provan, Charles D. The Church Is Israel Now: The Transfer of Condi-
tional Privilege. Vallecito, CA: Ross House Books, 1987. Collec-
tion of Scripture texts with brief comments.

Rutgers, William H. Premillennialism in America. Goes, Holland:
Oosterbaan & Le Cointre, 1930. Historical study of premillen-
nialism, from the early Church to Scofieldism, with emphasis
on America.

Vanderwaal, C. Hal Lindsey and Biblical Prophecy. Ontario,
Canada: Paideia Press, 1978. Lively critique of dispensation-
alism and Hal Lindsey by a Reformed scholar and pastor.

Weber, Timothy P. Living in the Shadow of the Second Coming: Ameri-
can Premillennialism 1875-1982. Grand Rapids, MI: Zonder-
van/Academie, 1983. Touches on American dispensationalism
in a larger historical and social context.

Wilson, Dwight. Armageddon Now/ Grand Rapids, MI: Baker,
1977. Premillennialist studies history of failed prophecy, and
warns against newspaper exegesis.
