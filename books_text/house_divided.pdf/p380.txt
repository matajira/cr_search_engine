20
WHERE DO WE GO FROM HERE?’

The Reconstruction debate demands deeper and further consideration,
and despite the failure of House and Ice it should continue,

The issues engaged in the debate to which this work is directed
represent an intramural-discussion among evangelical Christians.
Though a friendly debate between brothers in Christ, it is at the
same time one of great importance. The particular debate as
engaged with House and Ice is one between two distinct theologi-
cal schools: dispensational theology and Reformed theology.

The reader should carefully note that we have stated that it is
a debate with dispensational theology, not simply dispensational
eschatology. By that we mean to point out that dispensationalism is
a complete theological system in itself— it is not simply an escha-
tology, one aspect of theology. The Reconstructionism represented
by the present writers, Bahnsen and Gentry (and by R. J. Rush-
doony, Gary North, Gary DeMar, and many others), is a noble
species of Reformed theology. Dispensationalists House and Ice
have attempted to set forth the errors of Reconstructionism as if a
distinctive and even aberrant theology, whereas in actuality it is
confessionally rooted in the historic reformed theology and tradition.

The Call to Careful Argumentation
In that the debate is a lively and significant one today, it re-
quires the diligent and careful consideration of thinking Chris-
tians, who should be concerned about the Christian faith in the
world today. We confess that we were quite disappointed with
House and Ice’s work, not only in the scholarly and logical errors

343
