The Preterist Interpretation of the Kingdom 277

as future events?”°° And since they spend a good deal of space on
the influence of Daniel 9; 24ff. on Matthew 24, surely they would
include the handling of Daniel 9 in this statement.®! After all, they
attempt to distinguish Luke 21:20-24 from Matthew 24:15 by “com-
parison of the description in Matthew and Daniel.”®? They even
state: “One major reason Matthew 24 could not have been ful-
filled in A.D. 70 is that ‘the abomination of desolation’ (24:15) was
not accomplished in the destruction of Jerusalem .“*Thus, on
their own analysis Daniel 9 should be no more preteristically ful-
filled than Matthew 24 and should be no more heard of being in-
terpreted preteristicall y in early Christianity than it is.

It is here we begin to suspect that they have done no first-hand
reading in patristics, though they write with confidence as if they
were well read. Let us note, however, a few samples that falsify
such a claim.

Eusebius (A, D, 260-340) details the woes that befell Jerusalem
in a.D. 70, mostly by reference to Josephus (the method of
Chilton, which is disdained by House and Ice) .94 He writes that
“it is fitting to add to these accounts [i. e., Josephus’s] the true pre-
diction of our Saviour in which he foretold these very events.”g*

90. Ibid., p. 258 (emphasis mine), Also:“Tf Chilton and Russell's view is cor-
rect, then a majority of the New Testament was not recognized as afreadyful-
filled until recently. It was not until fifteen hundred, vears later that Chilton’s
preterist interpretation arose... When did the preterist interpretation first
arise in the history of the church? The promulgation of this view ‘in anything like
completeness’ was by a Spanish Jesuit of Antwerp, named Alcasar, in the begin-
ning of the seventeenth century (1614)” (p. 272). On p. 273 they cite Beckwith
who says: “[Alcasar’s] work is the first to attempt a complete exposition of the en-
fire premillennial part of the book.” The qualifying statements “in anything like
completeness” and “complete exposition” are interesting. Two pages later they
write: “The futurist interpretation is the approach used by the earliest church
fathers, We do not argue that they had a sophisticated system, but the clear
futurist elements were there” (p. 275), We would argue the same for the
“elements” of preterism.

91, House and Ice, Dominion Theology, pp. 259, 287-90,

92, Ibid., p. 290,

93. Ibid., p. 287,

94, Ibid, p. 289,

95. Eusebius, Ecclesiastical History 3:7:1-2.
