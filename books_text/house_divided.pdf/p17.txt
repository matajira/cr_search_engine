Publisher Foreword xvii

Hunt, the legacy of Scofield has come to fruition: a cultural rose
which is all thorns and no blooms. The seminary professors can
protest that this is not the “real” dispensationalism, but this com-
plaint assumes that the movement’s scholars have produced a co-
herent alternative to pop-dispensationalism. They haven’t.

Dispensationalists say that Christians in principle are impotent
to change things in the ‘lower storey,” and to attempt to do so
would be a waste of our scarce capital, especially time. While the
academic leaders of dispensationalism have been too embarrassed
to admit what is obviously a consistent cultural conclusion of their
view of history, the popularizers have not hesitated, especially in
response to criticisms by the Reconstructionists. Writes dispensa-
tionalist newsletter publisher Peter Lalonde regarding a friend of
his who wants Christians to begin to work to change the “secular
world”:

It’s a question, “Do you polish brass on a sinking ship?” And if
they're working on setting up new institutions, instead of going
out and winning the lost for Christ, then they're wasting the
most valuable time on the planet earth right now, and that is the
serious problem in his thinking. '

Because this attitude toward social change has prevailed with-
in American Christianity since at least 1925 (the aftermath of
the Scopes “Monkey Trial”), 12 those who attempt to dwell only
in the lower storey” — non-Christians — have had few reasons to
take Christians very seriously. American Christians have been in
self-conscious cultural retreat from historic reality and cultural
responsibility for most of this century. '’ Meanwhile, as non-
Christians have become steadily more consistent with their own
worldview, they have begun to recognize more clearly who their ene-

11, “Dominion: A Dangerous New Theology,” Tape 1 of Dominion: The Word
and New World Order.

12. George Marsden, Fundamentalism and American Culture: The Shaping of
Twentieth-Century Evangelicalism, 1870-1925 (NewYork: Oxford University Press,
1980), Chapters 20-23,

13. Douglas W. Frank, Less Than Conquerors: How Evangelicals Entered the Twen-
tieth Century Grand Rapids, MI: Eerdmans, 1986).
