B54 House Divided

presupposition in the drafting of the Constitution, calling it
“genius,” no one bats an eye. This is theological and historical
schizophrenia. But if it is true that the book of Deuteronomy was
the most quoted book prior to the drafting of the Constitution,
then why cannot our nation return to that former “genius” and
once again build on similar presuppositions?

@)
Exodus 20 I Timothy 1
Commandment 7: You shall not Adulterers and perverts
commit adultery (homosexuals)

One may see from the above chart that Paul [in 1 Timothy
1:8-11] had the Ten Commandments in mind. Homosexual sin is
viewed by Paul as a violation of the moral law of God given at
Mt. Sinai. But additionally, scholars have recognized for a long
time that the Decalogue has its roots in the creation teaching of
Genesis. . 13

Comment: House and Fowler see a relationship between the Ten
Commandments in the Old Testament and the New Testament.
But the Ten Commandments are not specific. They merely sum-
marize the law. They do not define adultery or what constitutes a
“pervert” or a “homosexual.” How does the Christian know when
adultery, perversion, or homosexuality has taken place? Where
does one find a prohibition against homosexuality in the seventh
commandment? What if homosexual marriages are legalized by
the State? Would this relationship be valid as long as there were
no homosexual adultery? Is the prohibition of bestiality included
in the adultery prohibition? The New Testament has very little to
say on these matters. The case laws of Exodus, Leviticus, and
Deuteronomy, define these crimes in great detail. In fact, Wayne
House has written an excellent exposition of Exodus 21:22-25
showing that the unborn child is considered a human being de-

13, Richard A, Fowler and H, Wayne House Civilization in Crisis: A Christian
Response t Homosexuality, Euthanasia, and Abortion (2nd rev. ed; Grand Rapids,
MI: Baker Book House, 1988),D. 131.
