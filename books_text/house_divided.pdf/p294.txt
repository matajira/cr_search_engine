252 House Divided

Athanasius applies prophecies of the triumph of Christ to the
Church age and even rhetorically asks: “But what king that ever
was, before he had strength to call father or mother, reigned and
gained triumphs over his enemies?” He then writes: ‘All heathen
at any rate from every region, abjuring their hereditary tradition
and the impiety of idols, are now placing their hope in Christ, and
enrolling themselves under Him.” “But if the Gentiles are hon-
ouring the same God that gave the law to Moses and made the
promise to Abraham, and Whose word the Jews dishonored, —
why are [the Jews] ignorant, or rather why do they choose to ig-
nore, that the Lord foretold by the Scriptures has shone forth
upon the world, and appeared to it in bodily form, as the Scrip-
ture said. ... What then has not come to pass, that the Christ
must do? What is left unfulfilled, that the Jews should not disbe-
lieve with impunity? For it, I say, which is just what we actually
see, — there is no longer king nor prophet nor Jerusalem nor sacri-
fice nor vision among them, but even the whole earth is filled with
the knowledge of God, and the gentiles, leaving their godlessness,
are now taking refuge with the God of Abraham, through the Word,
even our Lord Jesus Christ, then it must be plain, even to those
who are exceedingly obstinate, that the Christ is come, and that
He has illumined absolutely all with His light. ... So one can
fairly refute the Jews by these and by other arguments from the
Divine Scriptures.”!°° “It is right for you to realize, and to take as
the sum of what we have already stated, and to marvel at exceed-
ingly; namely, that since the Saviour has come among us, idolatry
not only has no longer increased, but what there was is diminishing
and gradually coming to an end: and not only does the wisdom of
the Greeks no longer advance, but what there is now fading away.
... And to sum the matter up: behold how the Saviour’s doctrine

98, Athanasius, Incarnation, Sec, 36:1, He cites sections from Num, 24:5-17;
Isa, 8:4; Iga, 19:1 (Sec. 33 [context= Sees, 30-31]); Dan, 9:24ff.; Gen, 49:10 (Sec.
40); Isa. 2:4 (Sec, 52:1); 11:9 (Sec, 45:2; Discourse Against the Arians 1:59); Psalm
110:1 (Discourse Against the Arians 2:15:14, 16); etc.

99, Athanasius, Incarnation, Sec, 37:5.

100, ibid., 40:5, 7.

   
