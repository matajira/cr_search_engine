How Should We Then Decide? 57

are simply more thorough and consistent in carrying it out). The
potential danger of cultural relativism can thus be seen in dispen-
sationalism. It would always be a temptation to move right on
over from dispensationalism into full-fledged moral relativism. In
time it simply cannot be avoided.

Dispensationalism must be rejected, then, before the Church
is dragged down into the mud of lawless, licentious relativism by
the likes of dispensationalists House and Ice! And speaking of
licentious moral relativism, there are some other “possibilities”
that need to be considered about authors House and Ice. You will
notice that they are friends, see each other socially, are both of the
same sex, and both oppose the Old Testament penal sanction of
death for homosexuality. The same can certainly be said of most
practicing homosexuals. The mud-slinging fallacy would lead us
to warn the reader, then, against the “possibility” that our authors
are moral perverts, and lead us to conclude that such immorality
is the constant danger of dispensational thinking.

Enough. The course of discussion in this section has been pur-
posely facetious, not at all seriously meant, and completely with-
out merit (except as a reductio ad absurdum). 1 know very well that
authors House and Ice are not liberals, Romanists, or perverts.
But can you imagine how offended they would be, and how out-
landish my scholarship would be, if my published critique of their
dispensational position included claims like “There is the potential
danger of liberalism and perversion in dispensationalism”? I
would hope never to treat them in that fashion or to try to pass off
such comments as a serious analysis and critique of generic dis-
pensationalism. Our common Lord, in calling us to observe the
morality of “the Law and the Prophets,” said: ‘All things therefore
whatsoever you would have men do unto you, even so do you also
unto them” (Matt. 7:12). In that spirit, | wish to repudiate the
kind of criticism which has been used (for purposes of illustration)
in this section — and call upon brothers House and Ice to do like-
wise. None of us gets treated fairly with the mud-slinging fallacy.
Likewise, I exhort the serious reader to disregard the unfortunate
sections of Dominion Theology which utilize this baseless line of
