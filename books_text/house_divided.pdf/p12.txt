xii House Divided

This covenantal and therefore Jegai link between heaven and.
earth is explicitly denied by modern fundamentalism. Fundamen-
talism denies the continuing authority of God’s law. Thus, funda-
mentalism faces the same dilemma that humanism faces: a radical
break between the upper storey and the lower storey.

The Two-Storey World of Fundamentalism

Fundamentalism’s lower storey is the world of work, econom-
ics, professional training, art, institutions, authority, and power,
ie., the “secular” realm. This realm is governed, not in terms of
the Bible, but in terms of supposedly universal “neutral reason”
and natural law. (So far, this is basically the thirteenth-century
worldview of Thomas Aquinas and the medieval scholastic phi-
losophers.) The Bible supposedly does not speak directly to this
realm, we are assured by both the fundamentalists ( We’re under
grace, not law!”) and the secular humanists (“This is a pluralistic
nation !”). Thus, there is no theological or judicial basis for Chris-
tians to claim that they are entitled to set forth uniquely biblical
principles of social order. Above all, Christians are not supposed.
to seek to persuade voters to elect political rulers who will enforce
biblical laws or principles. This means that rulers must not be
identifiably Christian in their social and political outlook. Chris-
tians are allowed to vote and exercise civil authority only insofar
as they cease to be explicitly biblical in their orientation. In short,
only operational humanists should be allowed to rule. This is political
pluralism, the reigning political gospel in our age — in an era
which believes that only politics is gospel. °

Crumbs from Humanism’s Table

This view of the world — “the world under autonomous man’s
law” — leads Christians to an inescapable pessimism regarding the
church’s present and its earthly future, for this view asserts that
Christians will always be under the humanists’ table, eating the

3, Gary North, Political Polytheism: The Myth of Pluralism (Tyler, TX: Institute
for Christian Economica, 1989),
