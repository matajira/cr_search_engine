The Mission of the Kingdom Ait

6. Christians are to live and act in every area of life — inner-
personal, personal, social, political— with body, soul, mind, and
strength (Mark 12: 37) to the glorify of God (1 Cor. 10:31; Col.
3:17), for they will give an account of every word and deed (Matt.
12:36; 2 Cor. 10:5).

7. God’s redemption provided in Christ is designed to bring
the world as a system to salvation (John 1:29; 3:17; 1 John 2:2)
and redeem mankind (John 12: 31; 1 Tim. 2:6).

8. Thus, Christians cannot omit cultural endeavors as they
seek the redemption of all of life to God’s glory.
