Publisher% Foreword xxv

To escape this problem of radical discontinuity, ie., New Tes-
tament church vs. Old Testament prophecy, Ryrie appealed to
Erich Sauer, but in fact Sauer’s argument rests squarely on the ar-
guments of postmillennial Calvinist critic O. T. Allis. The church
was indeed founded at Pentecost; the events of Pentecost were
merely transitional. No radical discontinuity y should be assumed
here, Ryrie insisted. So did Allis. 9 Ryrie also used Stare-type ar-
guments — insisting on a radical discontinuity, church vs. Israel
— against Allis. This theological juggling act was not a successful
intellectual defense of traditional dispensationalism; it was noth-
ing less than abject surrender. Ryrie in effect picked up a white
flag and identified it as dispensationalism’s regimental colors. He
publicly gave away the farm.

Theologians inside the dispensational camp apparently recog-
nized what Ryrie had done in the name of defending the traditional
system. I think this is the reason why there was no subsequent at-
tempted academic defense of dispensationalism until House and
Tee, a generation later, wrote Dominion Theology. But they no
longer defend original Scofieldism. Neither do their published col-
leagues at Dallas Seminary. (Professor Robert Lightner still car-
ries the old white flag in the classroom at Dallas, but the Christian
book-buying public has never heard of him.)

Quite frankly, no one is sure just what the “new, improved”
dispensational theology looks like. There has been no public pre-
sentation of the revised system. Chafer’s Systematic Theology has
been pulled out of print by its publisher, Dallas Seminary. The old
theological system was bled to death, drop by drop, by a thousand
qualifications, but nothing has taken its place. There has been an
embarrassed silence about this moribund condition for at least
two decades. House and Ice have therefore opened a very danger-
ous can of worms.

19, Ryrie cites Sauer’s argument that the “mystery” of Ephesians 3:1-12 - the
gentiles as fellow-heirs with the Jews in salvation - was not a radically new idea,
but only comparatively new, i€,, no radical discontinuity. Ryrie, Dispensatienal-
ism Today (Chicago, IL: Moody Preas, 1965), p. 201, This is of course Allis's argu-
ment against all dispensationalism: Prophecy and the Church (Philadelphia, PA:
Presbyterian and Reformed, 1945), pp. 91-102,
