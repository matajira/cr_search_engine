164 House Divided

revolts against Him at the end (Rev, 20: 7-9)? 2 This involves a
second humiliation of Christ.

As will be shown in the next chapter, Christ’s rule has already
been established; He does presently rule spiritually over His
kingdom.

The Pan-Ethnic Character of His Kingdom

A distinctive feature of dispensationalism is that the Millen-
nial kingdom will be fundamentally Jewish in character, even to
the point of rebuilding the temple, setting up David’s tabernacle,
re-instituting the Jewish sacrificial system, and exalting the Jews:
‘This is the point: once Israel is restored to the place of blessing
and the tabernacle of David is rebuilt, then will follow the third
phase in the plan of God. That period will be the time of the mil-
lennium, when the nations will indeed be converted and ruled
over by Christ .”/+

Dispensationalism surprisingly teaches such things as: “The
redeemed living nation of Israel, regenerated and regathered to
the land will be head over all the nations of the earth. . .. So he
exalts them above the Gentile nations. . . . On the lowest level
there are the saved, living, Gentile nations .“ * “The Gentiles will
be Israel’s servants during that age. ... The Gentiles that are in

12. Pentecost, Things to Come, pp. 547-51,

13. One endorser of House and Ice’s work has even brazenly stated: “In fact,
dominion - taking dominion and setting up the kingdom of Christ -is an imposs-
bility, even for God. The millennial reign of Christ, far from being the kingdom,
is actually the final proaf of the incorrigible nature of the human heart, because
Christ Himself can’t do” it. Dave Hunt, “Dominion and the Cross,” Tape 2 of
Dominion: The Word and New World Order (Ontario, Canada: Omega-Letter, 1987),
See also Dave Hunt, Beyond Seduction: A Return to Biblical Christianity (Eugene,
OR: Harvest House, 1987), p. 250,

14. House and Ice, Dominion Theology, p. 169. Regarding the rebuilding of the
temple and the reinstituting of the sacrificial system, House and Ice exercise a
wise silence, although their system 25 presented clearly allows for such (cf. pp.
142, 169, 174, 175). For more detail, see Pentecost, Things o Come, Chapter 30,

15, Herman Hoyt, “Dispensational Premillennialiam,” in Robert G.Clouse,
‘The Meaning of the Millennium: Four Views (Downer’s Grove, IL: Inter-Varsity
Press, 1977), p. 81.
