92 House Divided

(@) any command which expresses God’s will in generic, axio-
matic or abstract terms not tied to any particular culture, but ex-
pressed in some particular natural language [stipulating “precept”
for this notion],

(7) any moral standard, guideline, or demand arising from the
essential nature of God and conceived extra-linguistically — that
is, not expressed in some particular natural language but expres-
sible in any [stipulating “principle” for this notion].

Tn light of this linguistic scheme, we can try to state precisely the
conflict between dispensationalists and theonomists.

Both schools of theology love God’s revelation, wishing to be
guided by it and obey it, especially as it is clearly and savingly ar-
ticulated in Scripture. Thus they both take notice of Scriptural
imperatives, sorting out those which have no continuing authority
for our practice or conduct today (like directives) '? from those
which do. Neither maintains that the codes revealed in the Bible
are as such to be obeyed today. 20 Both would agree, moreover,
that (whether in the Old and/or New Testaments) the moral obli-
gation of people living outside of the biblical cultures is to the reg-
ulations arising from the details of these codes, as well as to the
precepts of Scripture; 7! when properly derived, the regulations
carry the same force as the precepts and can either be expressed in
the very words of the precepts or help to further define and apply
those precepts.

Theonomists maintain that the Mosaic” code and regulations
contain some items which are not based upon necessary moral
principles, but rather God’s sovereign good pleasure; as such they

( 19. Eg., Shnet’s instruction for Peter to go find a coin in the mouth of a fish
(Matt. 17:27).

20, E.g,, ‘Greet one another with a holy kiss” (Rem. 16:16); women are not to
have braided hair or gold jewelry (1 Peter 3:3), etc.

21, Eg,, “Servants, obey in all things them that are your masters according to
the flesh,” with “whatsoever you do, work heartily as unto the Lord, and not unto
men” (Col, 3:22-23).

22, [(Bahnsen) focus on the Mosaic code here, not out of preoccupation, but
simply because this is the crux of the debate,
