The Theological Concept of Law 95

They really cannot have their cake and eat it too. For this rea-
son it is not surprising that within the very same paragraph in
which they assert that “the codes of Israel reflect the character of
God,” they go onto say that “the whole” of that law has /allen with
the coming of Christ! *' This is tantamount, were House and Ice
to be logically consistent, to saying that the character of God has
fallen.

The second reason why the conceptual scheme of dispensation-
alism is theologically unacceptable is that it runs counter to the
teaching of Scripture itself. Are the regulations and precepts of
the Mosaic law to be called “God’s law” only because He sover-
eignly ordained them and they came from Him? Scripture says
much more, 32

The righteousness and perfection toward which the “com-
mandments” of “the Law and the Prophets” (Matt. 5:17-19) guide
us was identified by Jesus when He said, ‘you shall therefore be
perfect as your heavenly Father is perfect” (v. 48). God is morally
perfect (Deut. 32:4; Psalm 18:30) and His law is declared to be the
same (Psalm 19: 7; cf. James 1:25). Both Moses and Peter concur in
their inspired teaching that the commandments lead us to emulate
the very holiness of God Himself “You shall be holy, for I the
Lord your God am holy” (Lev. 19:2; 1 Peter 1:15-16). The regula-
tions and precepts of the Mosaic law are properly conceived of as
God’s holy character coming to expression. Those regulations and
precepts can no more change or be cancelled than the essential
and immutable holiness of God could be altered.

31, Ibid, p. 89.

32, House and Ice say emphatically that we must differentiate “God’s law”
from “the law of Moses” (House and Ice, Dominion Theology, p. 100, etc.). How-
ever, when we read the Old Testament Scriptures, we find that God shows a spe-
cial jealousy and is adamant to maintain that the law given through Moses is His
law, The law of Moses is identified over and over again as the law of Jehovah
(e.g,, Deut, 30:10; Josh, 24:26; 2 Kings 10:31;17:13; 21:8; 1 Chron, 22:12; 2 Chron.
6: 1:21; Ezra 7:6, 12, 14, 21; Neh, 8:8, 11 10:28, 29; Psalm 78:1; 81:4;
89:30; 119:34, 77, 92, 97, 109, 174; Isa, 1:10; Jer, 6:19; 9:13; 16:11; 26:4; 31:33;
44:10; 22:26; Dan, 6:5; Hos, 4:6; Bt).

     
