5
THE FAILURE OF ACCURATE PORTRAYAL

A survey of ways in which House and Ice mislead their readers by
misrepresenting Reconstruct tenis.

Before moving on to the only relevant question in the dispen-
sationalist dispute with Reconstructionism — the question of Bibli-
cal credentials — we must pause briefly to register a complaint
against the way in which House and Ice deprive their readers of a
clear picture of Reconstructionist aims and beliefs. Because they
are so often off target in describing Reconstructionism as a posi-
tion, one can have little confidence in their personal commentary
or critique.

This objection does not arise from any party spirit or defens-
iveness about Reconstructionism. It is merely the application of a
generally recognized standard of scholarly integrity. No book is
worth publishing which contains repeated failures to portray ac-
curately what it is discussing. No book can be taken seriously
which has not been adequately researched. No book can be deemed
a relevant critique of another position when the object under
review is regularly misrepresented or examined only in its weak-
est formulations. For these reasons Dominion Theology is, simply
from the standpoint of scholarship, nearly pointless. It may work
as propaganda or diatribe, but it fails as analytical reflection and
assessment. Let me use a variety of different kinds of illustrations
from the book.

Incoherent Description
It is expected that a reviewer, taking all of the available evi-
dence into account and reading statements in the best light, will

67
