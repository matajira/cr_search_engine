30 House Divided

dorse the position of cultural relativism in ethics (‘they were morally
valid for that time and place, but invalid for other people and
other times”) — which is diametrically contrary to the testimony of
Scripture (Mal. 3:6; Psalm 89:34; 111:7; 119:160; Eccl. 12:13; Rem.
2:11). But on the other hand, to affirm that the principles of ethics
found in the Old Testament law are binding in our day and age
might suggest to some people that no differences between Old and
New Covenants (two different “dispensations” or administrations
of the covenant of grace), or between an ancient agrarian society
and the modern computer age, have been recognized. After all, in
the Old Testament we read instructions for holy war, for kosher
diet, for temple and priesthood, for cities of refuge at particular
places in Palestine, for goring oxen and burning grain fields. Ob-
viously there are some Kinds of discontinuity between these provi-
sions and our own day. We should stop to analyze them.

Some of these discontinuities are redempiive-hisiorical in charac-
ter (pertaining to the coming of the New Covenant and the fin-
ished work of Christ), while others are cudiural in character (per-
taining to simple changes of time, place or lifestyle). The latter
are unrelated to the former. There are cultural differences, not
only between our society and the Old Testament, but also between.
modern America and the New Testament (e. g., its mention of
whited sepulchers, social kisses, and meats offered to idols)? —
indeed, there are cultural differences even within the Old Testa-
ment (e. g., life in the wilderness, in the land, in captivity) and.
within the New Testament (e. g., Jewish culture, Gentile culture)
themselves. Such cultural differences pose important hermeneutical
questions — sometimes very vexing ones since the “culture gap”

3, Authors House and Ice seem to have completely overlooked this fact when
they took itinto hand to criticize the theonomic position for creating the difficulty
of bringing the Old Testament into the twentieth century. These that this
cultural updating is a hard thing to do: “This raises questions of subjectivity and
the danger of, in effect, adding to God’s Word” (Dominion Theology, p. 39), From
this standpoint it is just as Gifteult to apply the ancient culture of the New Testa-
ment to the twentieth century! If the objection by House and Ice against theon-
omy i" be be taken seriously, then, we should really reject the uge of the entire

ible today.
