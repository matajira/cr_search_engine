PUBLISHER’S FOREWORD
by Gary North

Devout postmillenarianism has virtually disappeared,
Alva J. McClain (1956)1

Dispensationalists should be open to, sensitive to, and ready io enter-
tain any future development of theology based on @ proper theological
method, giving primary consideration to the ongoing work of interpreting
the Scripture. Many dispensationalists are encouraging this, and that is
why development can be seen within the system.

Craig A. Blaising (1988)"

Dispensationalism, as @ theologically defensible system, is now in the
aorist tense,

Gary North (1989)

Behind my rhetoric, there is always a bedrock theme lurking.
The cover of this book reveals my theme. (Gary DeMar suggested
the title, while I selected the cover. The two authors provided the
text.) In the early writings of Francis Schaeffer, we read of mod-
ern philosophy’s two-storey universe. The bottom storey is one of
reason, science, predictable cause and effect, ie, Immanuel
Kant’s phenomenal realm. This view of the universe leads in-
evitably to despair, for to the extent that this realm is dominant,

1, Alva J, McClain, ‘A Premillennial Philosophy of History,” Bibliotheca Sacra
(1956), p, 113,

2, Craig A. Biaising, Development of Dispensationalism by Contempor:
Dispensatonalists Bibliotheca Sacre (July-September 1988), p25. ay

ix
