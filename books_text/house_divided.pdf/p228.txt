The Presence of the Kingdom 183

young ass, sat thereon; as it is written, Fear not, daughter of Sion:
behold, thy King cometh, sitting on an ass’s colt” (John 12:12-15).
Here Christ is not only declared to be “king,” but He accepts the pub-
lic lauding of Himself as king in that it was in fulfillment of prophecy
(Zech. 9:9) and despite Pharisaic rebukes (Matt. 21:15-16).

During His trial and at the inquiry of Pilate, Christ specifi-
cally admits His kingship and the presence of His kingdom: “Jesus
answered, My kingdom is not of this world: if my kingdom were
of this world, then would my servants fight, that I should not be
delivered to the Jews: but now is my kingdom not from hence.
Pilate therefore said unto him, Art thou a king then? Jesus an-
swered, Thou sayest that I am a king. To this end was I born, and
for this cause came I into the world . . .” Wohn 18:36-37a; cp.
Matt. 27:11; Mark 15:2; Luke 23:3).

Although He defines His kingdom as something other-
worldly y, rather than essentially political (as was Caesar’s king-
dom),?* He nevertheless indicates His kingdom is present: He
speaks of “my kingdom” (v. 36a). He claims to have His own “ser-
vants” (even though they do not fight with sword to defend Him,
v. 36 b). He clearly states “I am king” (v. 37a). And, as we might
expect, given our previous study of Mark 1:14-15, He states that it
was for that very purpose He was born into the world (v. 37 b)!

Christ Was Enthroned as King

The very first of the enthronement passages in the post-resur-
rection age associates Christ’s enthronement with His exaltation,
which began with His resurrection and culminates with His ses-
sion at the right hand of God. Of David's prophecy anticipating
his seed’s sitting upon the Davidic throne, Peter proclaims:

Therefore being a prophet, and knowing that God had sworn
with an oath to him, that of the fruit of his loins, according to the
flesh, he would raise up Christ to sit on his throne; he seeing this
before spake of the resurrection of Christ, that his soul was not
left in hell, neither his flesh did see corruption. . . . Therefore

24, See Chapter 11 on “The Nature of the Kingdom,”
