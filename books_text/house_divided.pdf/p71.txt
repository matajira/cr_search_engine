The Conflict of Visions at

personal redemptive transformation which needs to take place in
each one of us before the larger worldview concerns found in the
Old Testament model can ever be realized. 23

Dispensationalists, by contrast, do not presuppose continuity
between the moral standards of the Old and New Testaments, but
rather restrict moral authority to what is said in the New Testa-
ment. Since the New Testament does not communicate extensive
details about socio-political justice or explicitly call for Christian
involvement in social reform, dispensationalists tend to have very
little — or very little distinctively Christian — to offer in terms of
dealing with socio-political problems. 24

The second reason dispensationalists have eschewed social in-
volvement and reform is that according to their eschatology, such
efforts will ultimately be futile.

The premillennial concept of the present age makes the inter-
advent period unique and unpredicted in the Old Testament.

23, Lest critics misconstrue my (Bahnsen’s) words and paint me as one who
hereby diminishes, undervalues, or obscures the surpassing importance of personal
salvation, please notice that I am not for a moment suggesting that New Testament
attention to the accomplishment and application af redemption for God's people
(with a view to the individual's standing before God and eternal destiny) is “merely”
an instrument io getting onto “what is really important,” viz. social transforma-
tion. “God forbid that I should glory save in the cross of our Lord Jesus Christ,
by whom the world is crucified unto me and I unto the world” (Gal6:14).

24, This observation reminds us that it is ethics, and not simplyeschatology,
that leads to divergent attitudes toward social involvement, Theonomic ethics
points to moral obligations in the socio-political sphere which any Christian, re-
gardless of eschatological convictions, should want and seek to obey. Ice and
louse do not seem to recognize this. Broaching the question “whether believers
should be involved in this world,” they simplistically speak of “the postmillennial
root” of “the Reconstructionist agenda” (House and Ice, Dominion Theology, pp.
358-59), The ethical root independently produces the same agenda, Further-
more, many believers of Reformed persuasion believe that Christ calls us to the
transformation of every area af life, including society and politics, and yet are
not postmillennial in eschatology. One can in his theological reasoning very well
be committed to the reconstructionist ethical vision and standards for Christian
social reform without adopting postmillennialism — contrary to a statement made
by Ice on page 9, but then contradicted on pages 354-55, (It is truc that, as an
encompassing abel commonly used of "s0ns~ rather than a social agenda —“Re-
constructionist” is a word denoting someone who is both a theonomist committed
to social transformation and a postmillennialist.)
