The Nature of the Kingdom 165

the millennium will have experienced conversion prior to admis-
sion.” 6 “Israel will be a glorious nation, protected from her ene-
mies, exalted above the Gentiles. .. .”!” House and Ice concur:
“God will keep his original promises to the fathers and will one
day convert and place Israel as the head of the nations.” '° The
“saved” (who are those “for whom Christ died,” Rem. 14:15) will
“be on the lowest level” in Christ’s rule! They will be servants!
Surely this is a Zionism of the worst sort in that on the mere basis
of race, saved Israel is to be exalted over the saved Gentiles.

To hold the historic Christian view of the Church as a new
Israel forever, over against dispensationalism is not in any way
suggestive of a Reconstructionist “charismatic connection” with
the cultic Manifest Sons of God, as they suggest. 20

Yet in Scripture Christ’s kingdom is distinctly represented as
being pan-ethnic, rather than Jewish. This misreading of Scrip-
ture is a crucial error of dispensationalism that makes it distasteful
to many evangelical. In defense of the Reconstructionist view-
point that “the Bible does not tell of any future plan for Israel as a
special nation” we offer the following.

The Setting Aside of National Israel

While on earth Christ clearly and forthrightly taught that God
would set aside national Israel as a distinctive, favored people in
the kingdom. In Matthew 8:i1-12 in the context of the Gentile cen-
turion’s faith, He expressly says that the “sons of the kingdom

16, Pentecost, Things to Come, p. 508,

17, John Walvoord, The Millennial Kingdom (Grand Rapids, MI: Zondervan,
1959), p. 136.

18, House and Ice, Dominion Theology, p. 175.

19, It is difficult to see how they handle such passages as Isaiah 19:20-25,
which teach that “In that day shall Israel be the third with Egypt and with Assyria,
even a blessing in the midst of the land” (Isa, 19:24), This seems to comport well
with the New Testament principle of equality (Gal. 328).

20, House and Ice, Dominion Theology, Appendix A.

21, Ibid., p. 51,
