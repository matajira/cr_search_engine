xxxii House Divided

the main reason I had made such provocative accusations in the
first place: to flush into the open some dispensational scholar—
not just a Hal Lindsey or a Dave Hunt, but a true academic spokes-
man of the movement. It took years of continual verbal harass-
ment on my part to pressure one of them to take up the challenge.
For some reason, he then allowed. Rev. Ice to join in this effort.
He therefore bears equal responsibility for Rev. Ice’s text — a re-
sponsibility that I surely would fear to share. Professor House un-
wisely took the bait and went into print. Now he gets to confront
Dr. Bahnsen.

In 1988, he had also told Gary DeMar, my former book distri-
bution manager David Dunham, and me that he was ready to de-
bate any theonomist publicly, and he complained that none of us
would take up his challenge. 34 He subsequently agreed to appear
in a debate with one of us at the Simon Greenleaf School of Law
in Orange County, California, in the spring of 1989. There was
some question as to who would be his opponent. Then Dr. Bahn-
sen accepted the challenge, but he insisted that it be a full-scale
debate, one on one, with cross-examination. Professor House im-
mediately declined the offer and withdrew, specifically refusing to
submit to cross-examination. He suggested instead a presentation
of prepared speeches and a mutual sharing of views. Dr. Bahnsen
refused to agree to this, and the whole project was dropped. Per-
haps this book will persuade Professor House of the wisdom of his
decision to decline. Then again, perhaps not.

What Professor House faced was the dilemma that the entire
dispensational academic world has faced since Ryrie wrote Dis-
pensationalism Today: How to meet the critics head-on without
visibly losing the battle? Oh the other hand, how to maintain the
illusion of being capable of meeting all intellectual challenges
while remaining cooped up in the classroom, where non-dispensa-
tionalist scholars are never allowed to speak? The basic solution
for a long time at Dallas Seminary was to allow Professor Lightner

34, This took place in July, 1988, at the Christian Booksellers Association’s
convention in Dallas.
