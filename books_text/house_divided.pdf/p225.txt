180 House Divided

the passage in either one of two ways: ' (1) They might attempt to
apply the dispensational hermeneutical equivalent of the deus ex
machina: the “imminence” concept. That is, this “at hand” state-
ment may merely speak of the kingdom’s readiness to break forth
into history at any moment. /” But to stretch such a nearness out.
for almost 2000 years (thus far!), would vacate the term of all sig-
nificance. In addition we should note that on another occasion
Christ specifically promised His hearers “there be some of them
that stand here, which shall not taste of death, till they have seen
the kingdom of God come with power” (Mark 9:1). Some of those
very persons standing before him would not die before the event!
Which one of them is still alive today? Or (2) they might bring in
the “new hypothesis” of dispensationalism: the postponement con-
cept. '® But this runs counter to the idea of “the time is fulfilled.”
Jesus never says, “The time is postponed,” as per dispensational-
ism. Does not God determine the times (Dan. 2:21; Acts 1: 7)? Do not
Ice and House vehemently argue that “Matthew presents Jesus as the
fulfillment and realization of ai/ that the Old Testament anticipates.*?°

Thus, the Gospels emphasize on several occasions the near-
ness of His kingdom (Matt. 3:2; 4:12, 17; 10:7; 16:28; Mark
1:14-15; 9:1; Luke 21:31). After the Gospels there is no longer any
preaching of the kingdom as at hand because:

 

The Kingdom Was Established During Christ's Ministry
Clear and compelling evidence exists that the kingdom did in
fact come in Christ’s ministry. Perhaps one of the clearest state

16, The older dispensational ploy of distinguishing between the “kingdom of
heaven” and the “kingdom of God” cannot be brought into assist, Dispensation-
alist Walter C. Kaiser observes: ‘In the past the usual way that this distinction was
demarcated was by designating the eternal kingdom as the kingdom of God and
the earthly program as the kingdom of heaven. It is a joy to note that we are now
well beyond this state in argument” (in Feinberg, ed., Continuity and Discontinuity,
p. 294), It is recognized as a false construct even by such dispensationalists as Ryrie,
Dispensationalism Today, pp. 1708. and Pentecost, Things to Come, pp. 443-44,

17, House and Ice, Dominion Theology, pp. 231-32, 420; see Chapter 14 below
on the Victory of the Kingdom for further discussion of this doctrine.

18, See ibid, pp. 173, 229,

19, Bid, p. 166.

20, Ibid., p. 104 (emphasis mine),
