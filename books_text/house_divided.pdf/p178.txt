The Civil and Cultural Use of the Law 131

mentioned) at certain points in the New Testament (e. g., John
18: 28-32). Given those alternative explanations, one cannot auto-
matically conclude that the civil sanctions were deemed abrogated.
Why does the New Testament not explicitly condemn bestiality?
To follow the reasoning of House and Ice, we would be led to an-
swer that the prohibition has been abrogated — which shows the
absurdity of arguing from silence.

Why did Paul not direct the Church to apply the Mosaic penal
sanction to the man guilty of incest? |! A number of reasons may
be relevant, but perhaps the most important that we can imagine
is that God has not authorized the Church to function in that way;
only the civil magistrate bears not the sword in vain. Besides, the
actual civil government at the time would not likely have cared to
punish the individual in this case, anyway. House and Ice re-
spond to this second consideration by saying that Christians
should have raised a prophetic voice against the unrighteousness
of their civil government. !? Well, let us assume that this is so. Do
we know that they did not raise that prophetic voice? The absence
of any mention of (or call for) it in the text cannot tell us the an-
swer to our question. Do we know that they raised a prophetic
voice to rebuke the unrighteousness of their civil leaders for failing
to punish rape? Well, there is no indication of it in the text, and
surely the city of Corinth had people who were guilty of this
crime. Should we conclude now that rape is not to be a punishable
offense in the New Testament?

The fact is that House and Ice are indulging in all too easy
fallacious reasoning when they conclude that Paul set aside the
enforcement of the Mosaic law simply because he did not urge the
application of its penal sanctions when dealing with situations
within the Church, When Paul did speak to the subject of the public
restraint of criminal behavior, he quite openly commended the
goodness of God’s law (1 Tim. 1:8-10; cf. Heb. 2:2). Silence cannot
reasonably outweigh his definite statements elsewhere, nor does it

M1, Ford, p. 118,
12, Ibid., pp. 121-22
