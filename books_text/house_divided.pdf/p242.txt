198 House Divided

Following upon this claim to universal authority, He delivers
to His few followers the obligation and plan for world conquest:
“Go therefore and make disciples of all the nations, baptizing
them in the name of the Father and the Son and the Holy Spirit,
teaching them to observe all that I commanded you” (Matt. 28:19-
20). The command of the resurrected Christ who possesses “all
authority” is for His followers to bring all nations to conversion
and baptism. This is precisely the expectation of so many of the
Old Testament prophecies, which foresaw all nations flowing to
Mount Zion (e. g., Isa. 2:1-4; Mic, 4:1-4), and which anticipated
“no more shall any man teach his neighbor, ‘Know the Lord, for
they shall all know the Lord” (Jer. 31:34; ep. Isa. 11:9).

In addition, the Commission urges our “teaching them to ob-
serve ail things whatsoever I have commanded you .” House and.
Ice assert that “worldwide evangelism is the calling of the church
in this age, not Cultural Christianization. ”* (Yet they deny the
success of the worldwide evangelism, despite its being based on
“all authorit y“ and promoted with Christ’s age-long presence:
“Scripture indicates that a majority of people will not come to
Christ during the church age.”)*

In essence, though, they undermine their own view against
cultural Christianization by admitting on the very next page:

Premillennialists believe that the New Testament does have s0-
cial and cultural ramifications, But they also believe that the em-
phasis on social and cultural issues reflects the purpose God has
for this age, namely the individual duties of a Christian before a
watching world, rather than the redemption and conversion of
institutions, A look at Gentry’s examples [with biblical refer-
ences — KLG] in the above citation tells how individual believers
are supposed to behave in relationship to the different spheres of
life: marriage, charity, employer-employee relationships, citi-
zenship, and finances, Nowhere in the New Testament does it
teach the agenda of Christianizing the institutions of the world, 24

2, Ibid., p. 160.
23, Ibid., p. 145, cp. pp. 159, 164, 236, 351,
24, Ibid., p. 155,
