Ethical Lapses 317

From Whitby’s statement they disclaim all legitimacy of postmil-
lennialism, as if all postmillennialist follow Whitby and his “new
hypothesis.”

Impugning of Motives

House and Ice are so bent on portraying Reconstructionism in
a bad light that they even stoop to a form of psychoanalysis in
determining why so many are becoming Reconstructionists: “I
believe many are attracted to the dominion position because they
have an agenda, such as politics or social reform, for which they
believe Reconstructionism provides the vision to lead them to suc-
cess. ... Most are attracted to dominion theology through the
back door, rather than through the front door of biblical study.”**
They make this statement on the basis of bare assertion: they pre-
sent no statistics, or exit polls from persons entering Reconstruc-
tionist bookstores, or anything of the sort.

They do so again regarding Chilton’s preterism: “Perhaps the
real reason Chilton has chosen the preterist approach, since most
postmillennialists have not been preterists, is that it is the most
antipremillennialist interpretative option on the market .““Here
Chilton is portrayed as a malcontent seeking anything that is as far
away from premillennialism as possible.

Poisoning the Well

In the very Preface of the book, House and Ice begin poison-
ing the well by suggesting that Reconstructionism is antithetical
to biblical Christianity. With a personal testimony Ice speaks of
his turning away from Reconstructionism to Scripture: “Once I
realized the antithesis of the two positions, I had to side with
Scripture and leave behind Reconstructionism .”25

Almost as early in their book, they introduce what theonomy
means to Reconstructionists. As they begin, they immediately

24, Ibid., pp. 9-10 (emphasis mine),
25. Ibid, p. 275,
26, Ibid., p. 9.
