The Preterist Interpretation of the Kingdom 263

How could it evidence the situation under Domitian in 4.D. 95?

Fourth, in a strange misnomer, House and Ice label the evi-
dence drawn from the Seven Letters and from Revelation’s allu-
sions to emperor worship as “external evidence”! 35 All the “exter-
nal” arguments they present for a late date in that section have
been answered in Before Jerusalem Fell and will not be rehearsed
here, °° But we must stand again in wonder at the blatant self-con-
tradiction in their argument! House and Ice dogmatical y argue
that Revelation is to be interpreted from a futurist viewpoint; that
is, they aver that its prophecies in Revelation 4:1-22:5 regard dis-
tantly future events. 37 But then they “prove” a late date by point-
ing to emperor worship in the text of Revelation and apply it to
Domitian! The references to emperor worship that are used by
late-date advocates are found in Revelation 13 primarily, as their
major sources, H. B. Swete and Charles R. Erdman, show.”
Which is it? Are those references speaking of a Domitianic
emperor worship (as used in the late-date argument)? Or are they
referring to the centuries-distant Great Tribulation (as used in the
futurist approach to Revelation)?

Fifth, there are strong internal indicators of Revelation’s pre-
A.D. 70 composition. For example, in Revelation 17 an angel under-
takes carefully to explain to John (Rev. 17:7, 9a) one of the dramatic
visions (Rev. 17: 3), which otherwise would have been difficult to
understand (Rev. 17: 6-7). The reason for the difficult y was that
the seven heads of the beast are said to have a doudie referent, not
a single one: seven mountains and seven kings (Rev. 17:10-11). The
seven mountains would immediately speak of Rome, the famed.
seven-hilled city, which was in control of the seven cities of Asia

35. Ibid,, p. 256, Scholars consider extemal evidence to be that drawn from tradi-
tion, not from withii the work in question. See Guthrie, New Tistament Introduction,
(Gd ed.; Downers Grove, IL; Inter-Varsity Press, 1970), p. 956; W. G, Kiimmel,
Introduction to the New Testament (17th ed.; Nashville, TN: Abingdon, 1973), pp.
466-67. Their error points out a degree of carelessness in their method.

36, See Gentry, Before Jerusalem Fell, Chapters 12, 16, 17, and 19,

37, House and Ice, Dominion Theology, pp. 260f., see particularly pp. 261 and.
278, where Walvoord and Tenney, respectively, are cited.

38, Ibid., p. 280 (notes 29-30),
