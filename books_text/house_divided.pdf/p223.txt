178 House Divided

cise active rulership, and (3) he must have a kingdom, or domain
over which to rule. Also we must recognize the multi-dimensional
significance of the “kingdom.” Christ’s kingdom is both spatially
and temporally vast, including both heaven and earth (cp. Matt.
28:18) and involving a progressive development of it in history. '*
The source-throne of His kingdom is heaven (John 18:36), and
thus the kingdom is in heaven, as well as on earth. In one respect
it requires the new birth and sanctification to enter it (John 3:3;
Matt. 7:21; 18:3) and precludes the unrighteous (Matt.5:20; 1
Cor. 6:9-10; Eph. 5:5). Yet, in another respect it contains even the
wicked (Matt. 13: 41ff. ). Contrary to the dispensational view,
which sees the kingdom coming after the resurrection of the
saints, Jesus even teaches that the “kingdom” may be entered into
by one who has an amputated foot or hand, or a gouged eye (Mark
9:43, 45, 47) — surely not a resurrected individual. Thus, at times
the theological “kingdom” refers to the visible church; at other
times to the invisible church; at other times to heaven, all of which
are ruled by the King of kings, Jesus Christ.

That Christ presently is a king and is even now ruling over
His kingdom is evident upon the following considerations.

The Approach of the Kingdom

It is evident from Scripture that with the coming of Christ
(particularly after His formal, public manifestation at His bap-
tism) the Kingdom has drawn near. There is a repeated emphasis
on the proximity of the kingdom in His ministry. In Mark 1:14-15
we read: “Now after that John was put in prison, Jesus came into
Galilee, preaching the gospel of the kingdom of God, and saying,
The time is fulfilled, and the kingdom of God is at hand: repent
ye, and believe the gospel” (cp. Matt. 4:12-17). Several aspects of
this passage bear closer scrutiny.

First, notice carefully that He asserts “the time” is fulfilled. Of
what “time” does He speak? Obviously the prophetic time which

13, See the Chapter 14, “The Victory of the Kingdom,” where the gradual de-
velopment of itis treated.
