270 House Divided

1, The Charge of Arbitrary Exegesis. In response to Jordan’s see-
ing Matthew 24:2-34 as referring to the fall of Jerusalem, and the
following verses as referring to the Second Advent, they ask:
Why, on the basis of the hermeneutics Jordan has used to this
point in his interpretation of the Olivet discourse, does he sud-
denly make an arbitrary leap to the second coming of Christ?”¢!
And, “How can Jordan, after taking the references to ‘coming’ in
verses 1-35 as referring to Christ’s coming in judgment in a, ». 70,
turn around and say that starting at verse 36 through the end of
the chapter, it refers to the second coming. Hither he is wrong
about the first 35 verses, and they do refer to the second coming,
or he should take verse 36 and following as a reference to the a. v.
70 destruction.”®

They apparently heard Jordan’s tapes, for they cite them and.
they rehearse a portion of his argument.® They also have read
Kik’s book.®* They even mention the key reason themselves, as
found in Chilton’s work, calling it the “main reason”!®> Jordan,
Kik, and Chilton are careful to give the reason — which we have
outlined above (Matthew 24: 34) — as Sustifying a change of sub-
ject. Yet they still charge preterists with arbitrariness. There are tex-
tually derived indicators; the change of focus is not in the least “arbitrary.”

Now what of their arbitrariness regarding the Olivet Discourse?
“Luke shifts from the a.D. 70 destruction of Jerusalem in 21:20-24
to the second coming of Christ in 21:25-28 .”°° Where is their textual
cue? True, they take Luke 21:28 as indicating a “redemption,”
which they hold as a reference to the Second Coming. But (1) this
is based solely on their own arbitrary assertion that the term must

61, House and Ice, Dominion Theology, p. 268.

€2, Ibid., p. 298. In Jordan’s review of Dominion Theology, we reads “They want
to know why I (and others) take the ‘coming’ in Matthew 24:30 in a different
sense than in verse 37. Well, partly because two completely different Greck words
are used!” James B, Jordan, Review of Dominion Theology (Tyler, TX: Biblical
Horizons, 1988), p. 14.

63, House and Ice, Dominion Theology, p. 297.

64, Ibid., p. 442.

65, Ibid., p. 285. See aleo: p, 54.

66, Ibid. , p. 291.
