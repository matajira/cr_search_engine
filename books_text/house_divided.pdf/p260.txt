The Victory of the Kingdom 217

Verse 11 asks: “I say then, they did not stumble so as to fall, did
they? May it never be! But by their transgression salvation has
come to the Gentiles, to make them jealous .” By the fact of the
demise of God’s special favor to the Jew due to their transgression
(.e., the rejection of the Messiah), they have fallen, encouraging
the message of salvation to spread to the Gentiles. In verse 12 we
read: “Now if their transgression be riches for the world and their
failure be riches for the Gentiles, how much more will their fulfill-
ment be !” Here the expectation is that the number of faithful Jews
will be brought back into fullness (p/eroma) with God.

But there is more. In verse 15 Paul speaks of their rejection as
“the reconciliation of the world.” In verse 25 he expects massive
conversions among the Gentiles: “A partial hardening has hap-
pened to Israel until the fullness of the Gentiles has come in.” The
Gentiles will be converted in full numbers. This “reconciliation of
the world” and “the fullness of the Gentiles coming in” seem to be
equivalent concepts, betokening worldwide revival.

Interestingly, the worldwide fullness of the Gentiles will pro-
voke the Jews to a jealousy for salvation (vv. 25, 11). This will lead
the Jews to mass conversions, as verse 12 indicates. This in turn
will lead to further Gentile conversions: “How much more will” the
Jews’ fullness bring riches to the world of Gentiles (v. 12). This
ends with a situation where “all Israel will be saved” (v. 26).

The Gradualistic Principle of Biblical Gospel Victory

The theological truths presented above require the introduc-
tion of an important redemptive-historical principle of divine ac-
tion. Contrary to covenantal postmillennialism, the dispensation-
alist holds catastrophism as the basic modus operandi of the kingdom.
That is, in their theological system, the kingdom of Christ in all of
its attendant glory will invade history in terms of “cataclysmic in-
terventionism” '° introduced by wars and rumors of wars, as it is
imposed on a recalcitrant world (via the Battle of Armageddon).

19, House and Ice, Dominion Theology, p. 232, cf. pp. 239, 269, 295,
