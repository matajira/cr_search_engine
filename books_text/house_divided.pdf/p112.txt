How Should We Then Decide? 63

reasons. ) As a follower of Calvin, the Puritans, and the Westmin-
ster Confession I would be glad to sit around and compare ‘past
failures” with a dispensationalist any day.”

Second, there is another wrinkle on the polemic about “past
failures” which needs to be ironed out. If postmillennialism were
true, suggest House and Ice, then we should see success for the
kingdom-building efforts of past Reconstructionist-type groups.
Since we do not, the “pattern” is clear that Reconstructionism is
false. “In fact, the church has not been able even to come close to
the conditions Reconstructionists say will exist over the face of the
globe before Christ returns .”** Now, do House and Ice weigh and.
then reject the possibility that this (biased) observation of theirs
maybe proves something else: viz., that we might not be close to the
time when Christ will return? No, they simply beg that question.
Apparently the fallacious nature of the rebuttal of any millennial
school on the basis of “present historical conditions” eludes our
authors here. (For instance: The distinctive, biblical, precursor
signs of Christ’s return as conceived by dispensationalism were not
really present in 1850 — since Christ did not in fact return at that
point — and “therefore” in 1850 a believer could have correctly ob-
served the characteristics of his particular age and drawn the “ob-
vious” conclusion that dispensationalism was false!)

33. House and Ice taunt Reconstructionist efforts to take seriously past prob-
lems and to understand what corrections are needed: “There has always been
some excuse,” the authors say (House and Ice, Dominion Theology, p, 348). But
offering an explanation (“excuse”) is in the nature of the case — for any school of
thought - the only alternative to repudiating the position. Given their long list of
past embarrassments and failures, do dispensationalists House and Ice abandon
dispensationalism or try to account for the mistakes made with it? Do they taunt
themselves now with “there's always some excuse”?

But now what do dispensationalists do in response to their long list of past
failures and embarrassments? Apparently, unless they are willing to repudiate the
theological position, they examine and explain how they feel the mistakes came
about. That is only natural. Does it make sense to taunt them now, saying “oh
yeah, there’s always some excuse”?

34, House and Ice, Dominion Theology, p. 336,
