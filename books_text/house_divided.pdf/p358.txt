320 House Divided

ham by faith, and thus includes all believers, whether Gentiles or
Jews, who were united in the same Church. On the other hand,
the Israel of the flesh can claim only the name and the race... .”34

Too Broad a Tar Brush

In Appendix A the authors attempt to bring together the
heretical charismatic cult known as the “Manifest Sons of God”
and Reconstructionism by comparing under fourteen separate
headings citations of Reconstructionists with those by adherents to
the cult. Yet they confess “Reconstructionists are well within the
stream of historic, Christian orthodoxy. They believe in the Trin-
ity, the inerrancy of Scripture, the deity and humanity of Jesus,
total depravity, salvation by grace through faith, and godliness in
the Christian walk. They are orthodox, Reformed Calvinists in
most areas of theology.”** In point of fact, the issues they point to
are quite innocent items held by all sorts of evangelical — many of
them even by dispensationalists! Let us cite chery major points,
with their Manifest Sons documentation, and then supply dispen-
sational and/or evangelical quotes in the place where they had
Reconstructionist statements. The error of such triviality will be-
come readily evident.

“1, Adam’s Lost Dominion,

‘MSOG: ‘Some believe that when Adam and Eve committed
‘grand treason” and lost dominion over the earth. .. .‘ Reconstruc-
tionist: Why doesn’t God seem to own [the earth] now? Why are

34, John Calvin, Galatians, Ephesians, Philippians, and Colossians in David W,
Torrance and Thomas F, Torrance, eds., Calvin? New Tistament Commentaries, 12
vols. (Grand Rapids, MI: Wm, B, Eerdmans, 1965), vol. 11, p. 118. “The New
Testament evidently regards the Church as the spiritual counterpart of the Old
Testament Jerusalem, and therefore applies to it the same name, L. Berkhof,
Systematic Theology (Grand Rapids, MI: Wm, B, Eerdmans, 1941), p. 558, cp. pp.
571ff, See also Charles Hodge, Systematic Theology, 3 vols. (Grand Rapids, MI:
Wm. B, Eerdmans, [1873] 1973), vol. 3, pp. 549-52; J. A. Alexander, Commentary
on the Prophecies of Isaiah, (Grand Rapids, MI: Zondervan, [1875] 1954), p. 71.

35, House and Ice, Dominion Theology, p. 384.
