xvi House Divided

millennial optimism to disguise a thoroughgoing pessimism. They
keep pointing to the glorious era of the millennium in order to de-
fend their use of optimistic language, never bothering to point out
that the seven years that precede it will destroy the results of gospel
preaching during the entire Church Age. After all, every Christian
will have been removed from the earth at the Rapture (an explicit
denial of the historical continuity predicted in Christ’s parable of
the wheat and tares: Matthew 13:20, 38-40). McClain’s essay is
representative of what has passed for world-and-life scholarship
within dispensationalism since 1830.

While McClain may have fooled those who read Bibliotheca
Sacra regularly, the troops in the pews have not been fooled. Dave
Hunt is willing to say publicly what dispensationalism means,
and without any apologies. Dispensational theology obviously
teaches the defeat of all the church’s cultural efforts before the
Rapture, since the millennium itself will be a cultural defeat for
God, even with Jesus reigning here on earth in His perfect body.

Tn fact, dominion - taking dominion and setting up the king-
dom for Christ — is an impossibility, even for God. The millennial
reign of Christ, far from being the kingdom, is actually the final
proof of the incorrigible nature of the human heart, because
Christ Himself can’t do what these people say they are going to
do. . 10

Here we have it without any sugar-coating: there is no con-
nection between the upper storey of God’s spiritual kingdom and
the lower storey of human history, not even during the millen-
nium. The two-storey world of fundamentalism is so radically
divided that even God Himself cannot bind the two together. That
is an impossibility, says Hunt. In the best-selling writings of Dave

10. Hunt, “Dominion and the Cross,” Tape 2 of Dominion: The Word and New
World Order (1987), published by Omega Letter, Ontario, Canada. See his similar
statement in his book, Beyond Seduction: “The millennial reign of Christ upon
earth, rather than being the kingdom of God, will in fact be the final proof of the
incorrigible nature of the human heart,” Bgond Seduction: A Return to Biblical Chris-
tianity (Eugene, OR: Harvest House, 1987), p. 250.
