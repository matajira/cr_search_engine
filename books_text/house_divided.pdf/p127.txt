8 House Divided

which he may not go, whereas other schools of thought have no
way of arguing against the state growing into a “beast” that “law-
lessly” claims every area of life as its jurisdiction (cf. 2 Thess. 2:3,
7; Rev. 13:15-17).

. Theonomy most assuredly does not endorse capital punish-
ment for rebellious “adolescents .”4! The Old Testament law —
which Jesus explicitly upheld (Matt. 15:3-6) — dealt with an indi-
vidual old enough to be a drunkard who can beat up his father
(Exodus 21:15; Deut. 21:18-21). Nor do theonomists believe that the
death penalty should be imposed by the civil magistrate for “apos-

sy.”#2 Neither does theonomic ethics hold that in the later days of
gospel prosperity (what House and Ice call “the final state of
global conquest”) some who are now deemed heretics will be “re-
classified from targets for conversion to targets for prison.”* As
footnoted above, the law of God does not authorize civil magis-
trates to judge or punish either heresy or the spiritual condition of a
person’s heart (not in the past, much less now or later).
eReconstructionists do not in practice need a ‘fusion of
church and state”** or “require the state to be . . . intermingled
with the church”** — becoming “merely an extension of . . . the
visible church”* — by expecting the state to punish heresy or to
“punish what the church condemns.” This is groundless non-
sense since theonomists do not believe the state has the right or
duty to punish heresy in the first place. Nor would theonomists re-
quire the officials of the state automatically to submit and kowtow

41, House and Ice, Dominion Theology, p, 16,

42, Ibid., pp. 40, 73, 78.

43, Ibid., p. 65.

44, Ibid., p. 98.

45, Ibid, p. 339,

46. Ibid., p. 93,

47, Ibid. House and Ice are so blinded by this false and groundless projection
of how a theonomic state would operate that they find it “astounding” that a the-
onomist has no problem whatsoever with the 1788 revision of the Westminster
Coniession which clearly removed any hint of state interference in ecclesiastical
matters (p. 97) — but it is true (notice that the authors offer no refutation or
pointed interaction whatsoever with the detailed analysis and argumentation re-
garding this issue in Bahnsen, Theenomy in Christian Ethics, pp. 526-37, 541-43),
