The Victory of the Kingdom 221

House and Ice even speak of America in the year 4.D. 40,225 in
their straw man scenario of a Reconstructed America. 7° In other
words, on that (hypothetical) basis we are now only 1/20 of the
way there |
,

Gradualism and Imminence

Itis much more difficult, however, for dispensationalists to
justify their vigorous assertions of the “any-moment” expectation
or the “imminence” of the coming of Christ, 27 when, in fact, al-
most 2000 years have come and gone. What in the world are we to
make of 2000 years of “eagerly awaiting Christ’s any-moment com-
ing”? 2s Dave Hunt, an endorser of House and Ice, attempts to
resolve the problem, with conclusions that totally evacuate the
meaning of an “any-moment” Return of Christ:

Why has it taken so long for our Lord to return? Could it be
another 2000 years, or even more, before His promise is ful-
filled? . , , [Slince previous “dispensations” in human history —
such as the period from Adam to the Flood, from the Flood to
the Promised Land, and the Jewish era prior to the birth of the
church at Pentecost — have occupied similar lengths of time, it
hardly seems unreasonable that the church should be on earth
for 2000 years as well.

That Christ has not yet returned does not change the fact
that the early church, in obedience to His clear commands, was
waiting and watching for Him to come at any moment, 29

26, Ibid, p. 63.

27, See Pentecost, Things to Come, pp, 168, 169, 203; John F, Walvoord, The
Rapture Question (Grand Rapids, MI: Zondervan, 1957), p. 192. Interestingly, dis-
pensationalist Alan Patrick Boyd has written that the early church fathers “had
no concept of imminency or of a pretribulational Rapture of the Church... .
[Thus] the findings of the thesis regarding imminency would invalidate the [Dis-
pensationalists’] historical claims regarding imminency in the following
writings:” Pentecost, Things to Come, pp. 168ff.; 203 and Walvoord, The Rapture
Question, p. 192; Alan Patrick Boyd, “Dispensationrd Premillennial Analysis of
the Eschatology of the Post-Apostolic Fathers (Until the Death of Justin
Martyr),” unpublished master’s thesis, Dallas Theological Seminary, 1977, p. 90
and footnote 1,

28, House and Ice, Dominion Theology, p. 232 (emphasis mine), see also pp. 166
and 231,

29, Dave Hunt, Whatever Happened to Heaven? (Eugene, OR: Harvest House,
1988), p. 39.
