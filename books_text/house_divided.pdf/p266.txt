The Victory of the Kingdom 223

dose of grace to Christianize the world.”*3 We should learn from
history that every time Christians have tried to establish the king-
dom on earth, it has led to disastrous results. We believe the rea-
son for this lack of success is that God has not given the church the
necessary tools and graces to establish an earthly kingdom .”*+
Elsewhere Ice’s testimonial reports his conviction: “I now know
that God has not been pleased to give the necessary graces to his
church for the kind of victory dominionists decree .”°°

These are incredible statements in that they greatly diminish
the richness of the gracious gifts of God to His people. Did Christ
not command His disciples to wait in Jerusalem “for power on high”
(uke 24:46-49)? Did He not inform His followers that their faith
(which overcomes the world, 1 John 5:3-4) was such that it could
remove mountains (Matt. 17:20)? Did not the apostles greatly re-
joice in the super-abundant grace of God and unsearchable riches
of Christ (Rem. 5:17; Acts 4:33; 1 Pet. 4:10; Eph. 1:7, 18; 2:7; 3:8),
declaring that God had blessed us with %11 spiritual blessings” (Eph.
1:3) because of Christ’s ascension and pouring out of His won-
drous gifts upon His people (Eph. 4:811)? Was it not by that grace
that they felt they could do “all things” through Christ and that
God would supply all they had need of “according to his riches in
glory by Christ Jesus” (Phil. 4:13, 19)? In short, were they not con-
vinced that “with God nothing shall be impossible” (Luke 1:37)?
We have ample gifts and graces to get the job done, in obedience
to the Great Commission. *° Let us just quickly cite a few.

33, Ibid., p. 340,

34, Ibid., p. 351, Ironically, dispensationalism sees even a kingdom person-
ally, visibly administered by Christ as ending in disaster! Pentecost,Things to
Come, pp. 547-53, Also see footnote 13, Chapter 11 above.

35, House and Ice, Dominion Theology, p. 7.

36, House and Ice take offense at the notion that the “victory,” “dominion,”
“conquest,” and the like will be won by Christians (the body of Christ) acting ac-
cording to the leading of His Spirit on the basis of His redemptive labors, em-
ploying spiritual graces (and not armed aggression!) rather than by Christ’s
physical return to the earth: “Reconstructionists often say they do not believe
that they are to bring in the kingdom. While it is true that they believe the king-
dom was established by Christ at his first coming, they clearly believe that some
phase of the kingdom is to be mediated by Christ through the agency of the
