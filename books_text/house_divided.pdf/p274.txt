The Victory of the Kingdom 231

14, God the Father delights in the salvation of sinners (Eze.
18:23; Luke 15:10).

15. The Gospel is nothing less than “the power of God unto
salvation” (Rem. 1:16; 1 Cor. 1:18, 24).

16. Satan’s binding was effected in principle in the ministry of
Christ (Matt. 12:28-29), thus casting him down from his domi-
nance (John 12: 31; Luke 17:10) on the basis of Christ’s redemptive
labor (Col. 3:15).

17. Christians may resist the devil, causing him to flee (lames
4:7); they may even crush him beneath their feet (Rem. 16:20) be-
cause “greater is he that is in you, than he that is in the world”
(1 John 4:4).

18. Dispensationalism is intrinsically pessimistic in that it
denies any hope that the gifts Christ gave the Church might turn
back evil; evil is prophetically inevitable. The church age must
end in apostasy.

19. Postmillennialism is intrinsically optimistic in the long
run. The church age will end in victory.

 
