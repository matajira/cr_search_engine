408

Dallas Theological Seminary, xiv,
yodi, »0rvi-xii, xliii, xiv, 6, 220,
316

Date of Revelation, 259-64

Date-Setting, 350, 361-65, 374

Days of Vengeance, 257, 307

Destruction of Jerusalem (4.p. 70),
272-73
See Preterism

Didache, 170, 244

Discontinuity, xivaow
Theonomy, 30-32, 174-75

Dispensationalism, xv-xix
Activism, 100d
Appeal, 1oexdi
Christian Reconstruction, 5-6
Death, xix-xx, xxi, 204i, xi

350-51
Discontinuity, xiveaw
Ethical Tendencies, 15-19
Gap Theory, 276, 279
Great Commission, 198
History 1codii-rccvii, 13-15,
61-63, 280
Historical Premillennialism, 234-38
Imminence, 221-22
Intramural Disagreements, 61-63
Jews, 164-71
Kingdom of God, 159-60, 187-91
Law 92-93.
Liberalism, 75, 76
Pessimism, xx-0xdi, 19-27, 225-29,
346-47
Pietism, 19-27
Postponement of the Kingdom, 180
Revision, sucdii-ocvi, div-xiv,
175-77
Van Til, 80-83

Dispensationatism Today, xxiv, xiii, 175,
353

Doctrine of God
Law 85-101

Dominion, xvi, 150-51, 202, 223-24

 
 

House Divided

Dominion Theology: Blessing or Curse?,
xix, 20d, ov, 2xVii, 200x, 2000,
xliii, x1, 6, 7, 8, 9, 13, 29, 32
Argument from Silence, 130-32
Carelessness, 303-8
Church, 293-94
Church Fathers, 239-40, 243-45
Civil Law, 126-28, 130-32
Contradictions, 89, 90, 94-95, 111,
117-18, 262-63, 289-92

Dangers of Christian
Reconstruction, 50-54, 295-96,
304

Date of Revelation, 259-64

Emotional Appeals, 294

Ethical Lapses, 311-41

Fallacies, 50-59, 108-9, 287-300

God's Law and Nations, 116-22,
351-56

Great Commission, 194-200

Guilt by Association, 58-59, 318-20

History of Eschatology, 233-54

Imminence, 222

Inaccuracy, 67-83, 292-94

Intramural Disagreements, 60-63

Kingdom and Israel, 164-71

Law, 110-11

Matthew 5:17-20, 103-10

Mission, 193-96, 208-9, 222-23

Nature of Kingdom, 159-60,
292-93

Noahic Covenant, 128-30

Non Sequitur, 299-300

Olivet Discourse, 264-67

Origins of Postmillennialism,
245-52, 316-17, 318-19

Personal Attacks, 64-65, 85, 317

Pessimism, 225-29

Pietism, 19-27

Postmillennialism, 141-47

Premillennialism and
Dispensationalism, 234-38

Presence of Christ in Kingdom,
162-64
