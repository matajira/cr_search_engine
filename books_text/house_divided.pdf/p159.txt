The Jurisdiction of the Law il

‘under the law’ as a rule of life.” '° This is a serious misreading.

Unlike Galatians 3, Remans 6:14 does not refer to “the law” of
Moses (cf. Gal. 3:19) or to the Mosaic law as a particular adminis-
tration of God’s covenant (cf. Gal. 3:17, 24). There is nothing like
this in the immediate textual context of Remans 6:14 to supply a
specifying sense to Paul’s words, and to be technically precise, one
should observe that Paul there does not speak of being under “the
law”— but rather to being “under law” (generically, without any
definite article). He teaches that those w-hose personal resources
are merely those of law, without the provisions of divine grace, are
for that reason under the inescapable dominion of sin; “there is an
absolute antithesis between the potency and provisions of law and
the potency and provisions of grace.”2° The “dominion of law”
from which believers have been “discharged” is forthrightly ex-
plained by Paul to be the condition of being “in the flesh [the sin-
ful nature] ,” being “held in” by “sinful passions which bring forth
fruit unto death” (7:1-6). From this spiritual bondage and im-
potence, the marvelous grace of God through the death and resur-
rection of Jesus Christ has set the believer free. It has not set him
free to sin against God’s moral principles.

Now then, when Paul speaks of not being “under law,” even
House and Ice cannot consistently interpret him to mean Taw” in
the sense of a “rule of life” (moral demands) since éhey themselves
insist that believers are under a law in that sense, “the law of Christ .”*
Their interpretation would have Paul denying that the believer
has any such “law” ( = rule of life), thus contradicting themselves.
Moreover, “law” in Remans 6:14 cannot refer to the Mosaic admin-
istration or dispensation in particular, for as we have seen, “under
law’ is equivalent to being under the dominion of sin. House and.
Ice would have to say that all those saints who lived under the law
of Moses, then, were under sin’s dominion — which is absurd and.

19, Ibid, p. 118,
20. John Murray, The Epistle to the Romans, 2 vols. (Grand Rapids, MI: Wm,
B, Eerdmans, 1959), vol. 1, p. 229. The reader is also recommended to study
Murray's tine discussion of “Death to the Law (7:1-6)" for as excellent an exegeti-
cal treatment as can be found (pp. 239-47).
21, House and Ice, Dominion Theology, pp. 85, 179, etc.
