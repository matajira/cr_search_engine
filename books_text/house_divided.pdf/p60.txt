8 House Divided

not be ignored. It is seriously presented and serious in its charges.
It deserves a serious reply. Reconstructionism should be publicly
vindicated of the charges which Dominion Theology has made
against it. Accordingly the two authors of this volume agreed to a
joimt project in order expeditiously to produce a book which
would set forth the Reconstructionist outlook, answer the charges
made against it by House and Ice, and rebut the contrary reason-
ing and theology upon which House and Ice relied. We are not
out to make them appear to be heretics. We are, however, con-
vinced that they can be shown to be in error.

The Purpose of “House Divided”

In the pages which follow we will address the two major dis-
tinctives which were challenged by House and Ice: theonomic ethics
and postmillennial eschatology. Both of the present authors have
influenced each other’s contributions and stand behind each other's
work. For the reader's information, though, the chapters found in the
section entitled “The Ethical Question” were authored by Bahnsen,
while the chapters found in the section entitled “The Eschatological
Question” were composed by Gentry (as well as the section entitled
The Scholarly Question,” the Conclusion, and Appendix B).

In the two sections of the book on ethics and eschatology we
aim to juxtapose the dispensational and Reconstructionist views
on particular issues. We offer biblical substantiation for the
Reconstructionist perspective in ethics (theonomy) and for the
Reconstructionist perspective on eschatology (postmillennialism).
We also will give extended attention to the arguments which were
published in Dominion Theology against both of these elements of
the Reconstructionist system.

Our conclusion is that House and Ice are not persuasive and.
are demonstrably in error. Even further, as indicated in the chap-
ters entitled “How Should We Then Decide?” and “The Failure of
Accurate Portrayal” and in Part 3 entitled “The Scholarly Ques-
tion” we have serious misgivings about the repeated misrepresenta-
tions of our sincerely held position and about the kind of reasoning
which House and Ice often use to oppose that position. By the end
of our analysis and answer, we humbly and teachably believe that
