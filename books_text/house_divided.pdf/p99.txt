50 House Divided

with fallacious thinking and logical shortcomings which consider-
ably weaken the effort and value of Dominion Theology. The cautious
reader will, consequently, not find their reasoning trustworthy.

“You're Wrong Because You Might Be”

Of all the complaints which are brought against Reconstruc-
tionism in Dominion Theology, by far the most strained and scur-
rilous is the argument that the position, although innocent of
some fault, is nonetheless guilty because it might lead to that
fault. Over and over again House and Ice indicate that Recon-
structionism should be rejected because there is in it “the possibil-
ity” or ‘the danger” or ‘the tendency” or “the temptation” or “the
potential” for some specified transgression to result from it.

The title of Chapter 15 in Dominion Theology is “The Dangers of
Christian Reconstruction.” The list of things heretical and wicked
which are laid at the feet of Reconstructionists is catastrophic:
moralism,’[yet] a spirit of compromise which abandons veracity
for unprincipled pragmatism,’leading the Church astray into
apostasy,° permeating Christianity with humanism, '° intermingl-
ing Christianity with non-Christian thinking or merging with the
world’s system, '! making the preaching of the gospel secondary, 12
bringing destructive non-Christian thought and influence into the
Church, creating anti-Christian institutions, ‘* [yet] inter-
mingling Church and state, 15 synthesis with the world, '* [yet] en-
couraging a severe backlash against Christianity. '”

7. Ibid., pp. 349-50,

8, id, p. 341,

9, Ibid., pp. 335, 342; cf. pp, 374-75, 377.
10, Bid, pp. 340, 349.

11, Ibid., pp. 341, 344; cf, p. 390,
2, Ibid., p. 356,

13, Bid, p. 344,

14, Bid, p. 340.

15, Ibid., p. 339.

16, Ibid., pp. 342, 344,

17. Ibid, p. 388,
