Editor's Introduction xi

wrong are therefore moral concepts, and all morality is religious.
But the myth of so-called natural law—common law, common
morality — has done its work in the field of civil law. Christians do
not look to God’s revelation in the Bible to tell them what kinds of
laws are legitimate and which kinds are not. So Christians have
abandoned law and politics to their rivals, who also operate in
terms of a religion— the religion of humanism, which is defended
everywhere in terms of the myth of neutrality.

Is there neutrality between heaven and hell? Will those in hell
have a vote in what happens in heaven? Will God give “equal time
to Satan” throughout eternity? Should Christians be required to
allow atheists to have equal time in the pulpit each Sunday? If
Christians do not want the problem of “equal time for Satan” to
arise, they must start limiting the state. They must start voting to
limit the money that the state can tax and spend on state-
approved projects.

Everything on earth is a battle between the principles of the
Bible and anti-Biblical principles. A war is in progress. But Chris-
tians have refused to face this fact for well over a century in the
field of education. It would involve giving up “free” humanist slop
in the trough.

Nowhere is this war more clearly in progress than in the battle
for the minds of men. And the major battlefield is the field of tax-
payer-financed education. Men are battling for the loyalty and
obedience of the next generation. And for a hundred years, Chris-
tians have been losing the battle. Why? Because they have de-
cided to grant to their rivals the fundamental point: the myth of
neutrality. They have accepted as morally valid—indeed, morally
preferable—the tax-supported compulsory school. Since the
1840’s in the United States, this institution has been the targeted
prize of God-hating, humanist kidnappers, beginning with Mas-
sachusetts’ Horace Mann, who used the God-hating, man-deify-
ing, evil school system of apostate Prussia as his model for the
public schools.

Christians want their education, but they want it cheap. So it
nas cost them almost everything, just as it cost Eve.
