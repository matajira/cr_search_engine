What the Church Can Do 147

they would inherit wells which they had not dug. The schools be-
ing built by the government can and should be privatized. The
meek shall inherit these buildings and turn them into centers of
Christian learning.

Get yourself a financial partner if possible. He can put up the
money for a building and lease it to you. He gets tax benefits, a
monetary return on his investment, and the satisfaction of seeing
the kingdom of God advanced. You get a modern, efficient build-
ing in which to run a school. I explain in my manual how to build

“so that the school will be profitable. (I'll give you a hint. Don’t
build with small classrooms.)

Teachers

Hiring the right teachers is really important. You may have
an excellent curriculum, a handsome building, and plenty of stu
dents, but if you don’t have a good staff, you are not going to get
the job done.

Let’s start with the director of the school. The title may be
“principal,” “headmaster,” “director,” or whatever. The person who
manages the school is the key employee. You may be that person,
or you may select someone else. The director of the school should
be committed to the Christian school philosophy. He should be
able to organize and be an effective executive. The most impor-
tant duty of the director is to hire competent staff. The director
does not need to have a degree in school administration.

If the school is controlled by a board, the most important deci-
sion they will make is hiring the director. The director should be
competent and should be adequately paid. A skillful manager will
earn his salary by running the school in a professional and busi-
nesslike manner.

There is a rule known as the 80-20 rule. It says that 80% of
your orders in a business will come from 20% of your customers.
Also 80% of-your profits will come from 20% of your products. It
is my experience that 80% of your problems in a school will stem
from 20% of your students, and 80% of your problems with teach-
erg will come from 20% of the teachers. The lesson to be learned is
to make the correct decisions in hiring.
