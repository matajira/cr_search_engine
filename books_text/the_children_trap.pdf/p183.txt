What the Civil Government Can Do 163

produce detailed reports, they may decide not to go to so many
meetings. This helps defend your local school system against na-
tional “change agents.”

Third, imitate Texas. Get the Board to pass a local “no pass—
no play” rule for sports. Make it apply to all extra-curricular activ-
ities. The public’s education money should not be spent on cir-
cuses that are performed by students who are flunking their
courses. It cheats the taxpayers, and it surely cheats the athletes,
who are being encouraged by their coaches and cheering fellow
students to short-change their futures for the sake of a letterman’s
jacket. It is a terrible trade.

Fourth, always protest when the high school principal is about
to fire a coach just because the coach has produced some losing
teams. Defend the coach if he has done a decent job teaching his
physical education classes to the average student. Teaching PE is
legally and officially why he is on the school payroll, not to win
after-school games. It is wrong to take money from taxpayers for
one purpose (teaching PE) when in fact the money is being used
for another purpose (winning games). The issue is quality educa-
fion, not winning tearns. If the hometown folks want winning
teams, use ticket revenues to hire coaches who produce winning
teams. Don’t use tax money to subsidize them. Besides, there is
nothing like a losing sports record to cool the vocal minority of
sports fanatics’ support of the local high school. (They want other
taxpayers to subsidize their entertainment.)

Fifth, do whatever you can to keep alive the question of stu-
dent safety. If there are rumors of drugs on campus—if!!!—then
encourage the school board to do everything possible to cooperate
with law-enforcement officers. This is priority one. First, it is a le-
gitimate task of civil government to enforce safety laws, and drug
laws are safety laws. Second, keep a spotlight on the school. If
things are bad, the public deserves to know. Christians on the
school board should become the defenders of decency, the sup-
porters of student safety. Keep reminding parents how bad things
are becoming in the public schools, if things really are getting
worse. Never become an accomplice of public school bureaucrats
