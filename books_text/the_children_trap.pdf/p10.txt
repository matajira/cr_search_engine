x The Children Trap

that exploded with a big bang 15 billion years ago—and you have
attacked some taxpayer's deeply held religious convictions. Label
another idea true—the evolution of mankind from lower animals,
for example —and you have attacked a different taypayer’s deeply
held religious view. You are using his money to indoctrinate his
children with ideas that he despises. Without exception, the major
victims today are conservative Christians whose children are
under deliberate religious and intellectual attack by taxpayer-
financed schools.

This has been going on at Christian taxpayers’ expense for
over a century, with nary a squeal of protest from the victims, cer-
tainly not from the 1925 Scopes “monkey trial” in Dayton, Tennes-
see until the mid-1960's, when the independent (non-parochial)
Christian school movement began to revive.

Why have Christians gone along with this obvious confidence
game? Three reasons. First and foremost, because they had
already bought the myth of socialist education, namely, that it is
the moral and financial responsibility of the state to educate chil-
dren, not the moral and financial responsibility of parents.

Second, Christians want their religion, but they want it cheap.
They refuse to tithe to the church, so they wind up tithing (and
paying far more than ten percent) to the state. They have tithed
their children to the state! “Free education! Free education!” The pigs
have sent their offspring to the slaughter for well over a century.

Third, Christians have been afraid to deny the myth of neu-
trality in the fields of politics and civil law. They have feared be-
coming labeled “theocrats” by their God-hating enemies, so they
have bought the party line regarding neutrality. The party line is
the humanist party line. They bought it first with respect to civil
law, so it was an easy step for the humanists to sell it to them in
the field of education.

Civil Law

Every law is religious. Every law tells certain people that they
are not allowed to do certain things, er that they must do other
things. Every law is based on an idea of right vs. wrong. Right and
