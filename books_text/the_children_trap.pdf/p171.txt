What the Church Can Do 151

The church should start a Christian school as a last resort. It
should cooperate with independent Christian schools locally. It
should even be willing to help a congregation member start a
Christian school.

The church must protect. If it needs to set up scholarship
funds for students, then that is legitimate. But the closer the
school is to full parent control, the less likely the lines of responsi-
bility, accountability, and authority will be blurred.
