72 The Children Trap

that the students have received an education. After all, they have
attended school for the prescribed number of years and have now
received a diploma. Many of them can not read their diploma,
but who cares? The state says they are educated, They have grad-
uated from accredited schools with certified teachers,

5. Because school attendance is compulsory, the state must
provide it. And it is all free! Free public education! How many times
have we heard this? Yet there is nothing free about it. Public edu-
cation is very expensive. It wastes the lives of students —the major
cost of public education, It reduces the God-given authority of the
parents, thereby reducing their sense of personal responsibility for
their children— another hidden. cost. Finally, the money to pay for
it does not come from charity either. Compulsory school atten-
dance leads directly to compulsory financing (spelled “t-a-x-e-s”).

Compulsory Education is a Failure

“Prove it,” you say. I could cite all kinds of statistics, data,
reports, studies, books, and articles from within the educational
establishment to prove the existence of massive failure. But every
literate person who has read anything in the newspapers about
public education already knows about these reports. The public
schools are getting a D on their report cards, even though the peo-
ple grading them are generally favorable towards taxpayer-
financed education. President Reagan’s special commission on
education found the public school so terribly deficient that its
report was dismally entitled “A Nation at Risk.”

I could also find plenty of proof from other sources as well. I
don't intend to do that. Others have made the point. Rudolph
Flesch wrote his famous Why Johnny Can't Read in the 1950's. Years
later he wrote Why Johnny Still Cant Read. Johnny can't spell either,
And he can’t add and subtract or do a whole lot of things. So why
beat a dead horse? We all know about the failure of the public
schools,

The debate begins when we try to assess proper responsibility
for the failure. The defenders refuse to admit that it is the com-
pulsory nature of modern education that has led to the failure,
