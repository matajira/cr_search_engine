Parents in the Driver’s Seat 4t

deal of learning can and does go on in this type of teaching situa-
tion. One of the best ways to learn is by teaching, and I am sure
the parents learn a great deal right along with their children.

I need to stress at this point that there are rival views regard-
ing home schools. In fact, some of these groups fight more over
when a child should start his formal education than what he should
be taught. I believe in the traditional Protestant view that children
should be catechized at a very early age, and taught to read very
early —at age four or five. They should be reading their Bibles at
age six. They can do it, and they shoudd do it. Deliberately to keep
the Bible a closed book for young children when they can be
taught to read it is simply sinful. Yet there is actually a small and
highly vocal movement of Christian home schoolers who follow a
system based on the idea that children not be formally taught to
read and compute until age nine or ten, unless they ask to be
taught these skills.

I totally reject the idea—an idea that has its roots in human-
ism (the Montessori and/or Mormon movement)—that parents
should postpone giving their children formal, structured, highly
disciplined instruction until the children are nine or ten years old,
or even older. Children should learn very early that time is an ir-
replaceable resource, that it is limited, and that to please God, we
have to get to work early and hard. They should learn to appreci-
ate the power and results of structured, disciplined work. They should
learn this by example, too, We parents need to be structured and
disciplined in our habits.

Jesus instructed the priests in the temple when He was a child
of twelve (Luke 2:41-47). He is the Biblical model, not some bap-
tized version of humanism’s “free-form education for the naturally
innocent child” model.

The Division of Labor

Sometimes when I advocate that parents should be responsi-
ble for the education of their children, I have been told that this is
not practical. Many persons think that the only way parents can be
responsible is for every family to teach the children at home. That
is not the case,
