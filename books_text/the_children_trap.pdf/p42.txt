22 The Children Trop

As we look at the curriculum of the public schools in contrast
with the Christian schools, we shall see more clearly the difference
religion makes in education.

Public Schools Are Rival Religious Institutions

Whoever controls the education of our children will also deter-
mine the content of that education. If the state is in control, it will
dictate what is taught. If parents are in control, their philosophy
will be reflected in the curriculum,

Suppose the civil government were dominated by Christians.
The schools under that government would then reflect the Chris-
tian philosophy of the civil rulers. We may be tempted to see this
as the solution to the secular humanism in the schools.

“Let's get control of the government. Appoint or elect Chris-
tians to the public school board. Then make the curriculum reflect
Christian values.”

This plan will not work. It is not our job to recapture the pub-
lic schools. They were never “ours” in the first place. In principle,
they always belonged to the state, not to parents. He who pays the
piper calls the tune. The state called the tune from the beginning.
Parents are supposed to call the educational tunes.

We should work to develop a consistently Biblical basis for
civil government. God is to rule in this area as in all areas of life.
‘We should strive to clect righteous persons to public office. How-
ever, this is not the solution for the problem of what is taught in
the schools. It is not the answer for the short term or the long
term. Our goal should be to attain Biblical ends by Biblical means.
Education is to be controlled by the parents. Parents are to see
that the content is Biblical. Fathers are to bring up their children
in the “nurture and admonition of the Lord.” The children are to be
taught the Bible. The Bible teaches that parents, not civil govern-
ment, are responsible for education. So our goal is to have both
Biblical content and Biblical control.

We could spend the next 25 years trying to wrest control of the
public schools from the humanists. Meanwhile, our own children
will be taught humanism if they go to the state schools. We must
