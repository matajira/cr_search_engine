108 The Children Trap

learned that something they don’t approve of can be taxed out of
existence. This is the context of Marshall’s remark: the State of
Maryland was trying to tax an agency of the Federal government.
Marshall said this was unconstitutional; the Federal government
has greater sovereignty than the State of Maryland.

But what about the sovereignty of God? What about the sov-
ereignty of a rival government, God’s church? Should the State of
Maryland, or any other civil government, be allowed to tax it? If
we say yes, then we are implicitly announcing that we believe in
the superior earthly sovereignty of the state to the church.

We think of taxes as a means of raising revenue for the govern-
ment, but the tax code is frequently used as 4 means to promote
social goals. Promoting one social goal always involves sacrificing
someone else’s goal. There are no free lunches in life.

Under the Internal Revenue Code, certain organizations can
be exempt from Federal income taxes. Among these are schools
and churches. We understand how the government can control an
institution by taxing it. How are tax-exempt organizations con-
trolled? The government controls them by threatening to take
away their tax-exempt status. When that happens, the organiza-
tion has to pay income taxes on its profit. Contributions to the or-
ganization are no longer deductible on the donor's tax return.
There are numerous other benefits enjoyed by tax-exempt
groups. On the local level the church or school will not have to
pay real estate taxes.

A school saves the taxpayers an enormous amount of money.
If the cost per pupil in a government school is $3,000 per year,
then a 500-student Christian school would save the public
$1,500,000 per year. We would like to think the public appreciates
this, and that the politicians would do everything they can to en-
courage such schools. That is not the case, however. The secular
humanists who control governments want to control the educa-
tion of the children. They don’t care who pays the bill, just so long
as it doesn’t come out of their own pockets.

A new way of looking at tax-exemption has come into vogue.
Legislators and government bureaucrats call the money that
