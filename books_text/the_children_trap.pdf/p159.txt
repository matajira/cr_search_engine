What the Family Can Do 139

If have space in the classroom, I would rather have a student sit-
ting there paying half price than to have a vacant seat. The school
has certain fixed costs. It is best to have full classrooms.

Perhaps you don’t like to ask for a discount. It is better to ask
for one than to force your neighbor to pay for your children’s edu-
cation at the public school. At least it is voluntary.

9. Get the grandparents to help.

The grandparents may be barely making it themselves. On
the other hand, with their children grown and the house mortgage
paid, they may have some extra money. Perhaps you are in line to
inherit some money when they die. It might be in everyone’s best
interest to use some of that wealth to help educate your children. I
have known many cases in which the grandparents paid the tuition.

10. Set up a family trust.

You'll need to see your CPA and lawyer about this one. The
tax laws are constantly changing, and I am not offering tax ad-
vice. I do know that some families set up trusts for their children.
As of 1986, the income from the trust goes to the child. Since it is
taxed at a lower rate, the benefit is obvious. This device has been
used for a long time to provide college education. Problem: the
1987 rules may have abolished this strategy for children under age
14, At any rate, consult your tax experts for the possibilities.

11. What about tuition vouchers?

In recent years, many parents have been working to get a
voucher system for schools. The idea is that the Federal govern-
ment would provide a voucher for each child of school age, The
voucher would be worth a stated amount of money. The parents
could spend the voucher at the school of their choice, whether
public or private. The school would then turn the voucher over to
the Federal government for payment.

This idea sounds attractive. The voucher system is supposed
to provide more competition among schools. This would lead to
improved standards for schools as they compete for the voucher
money. The system would give the poor a way to afford private
