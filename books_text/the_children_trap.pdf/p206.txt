186

state-supported compulsory
education, xvii

“The Status Seekers,” 74

stewardship, 14

Stoics, 81

stranger within the gates, 63

structuring the Christian school, 48

student safety, 163

suicide, 21, 33, 86

Supreme Court, xiv, 7, 11, 27, 50, 83,
107, 110, 115-16

Sutton, Ray, 94

Sweden, 16, 116

Swett, John, vii

sword, power of the, 67-68

TANSTAAFL, 116

tax code, 108

tax exemption, 107

tax financed welfare, 37

tax-exempt interest on bands, 120

tax-exempt organizations, 108

tax-exemption, 108-10

tax-immune, 110

taxes, business, 118

taxes, income, 119

taxes, motor vehicles, 119

taxes, real estate, 117

taxpayer-financed education, xi, 152

taxpayer-financed school, ix, x

teacher competency tests, 155

“Teacher in America,” 5

teacher salaries, 148

teacher strikes, 169

teachers unions, cooperation of
166-69

teachers unions, militant, 52

teacher-training institutions, 58

teachers, hiring, 147-49

teachers, grading the, 164, 167

teachers, true market value of, 165

Ten Gommandments, xiv, 19, 83

tenured teachers, 168-69

The Children Trap

termite tactics, 159

testing, 133

tests, standardized, 133, 164

Texas Board of Education, 155-56, 163
Texas, football scores, xvii
“theocrats,” x

theological liberalism, 153

Thoburn, Robert, viii, 97, 112
time-wasting reform efforts, 170

tithe to church, x

 

tithing children to the state, x
tithing to the state, x

Tobiah, 16

totalitarian state, 30

tourism, 70

Trinity, 3t

truant officer, 6

tuition fees, 150, 155

tuition vouchers, no, 139-41
“The Twelve-Year Sentence,” 6

U.S. Congress, xii

Unitarian, xvi

United Methodist Church, 153
United States Army, 98

Use Permits, 14

“values clarification,” 84

Van Til, Cornelius, 81

venereal disease, xvii

verbal inspiration, 18

Virginia State Board of Education,
155-56

Virginia, Commonwealth of, 17, 69

Virginia, University of, 97

voluntary education, 62

W, T, Woodson High School, 26
Wall Street Journal, 93

Walters, Henry, 133

Ward, Lester Frank, 84-85
Washington Post, ill
