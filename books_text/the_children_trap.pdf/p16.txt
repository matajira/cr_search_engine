xvi The Children Trap

And who can deny that these beliefs are religious? Certainly
this was clearly recognized by early leaders such as Horace Mann,
who frankly stood for “nonsectarian” religious teaching in the pub-
lic schools. But it was soon discovered that there could be no “non-
sectarian” religious teaching in America, because religion had
been poured into sectarian molds and had hardened into sectarian
forms. Thus Horace Mann’s brand seemed to many evangelical
Protestants to be suspiciously “Unitarian,” and at best what passed
as “nonsectarian” religious teaching seemed to many Unitarians,
Roman Catholics, and others to be evangelical Protestantism.
Even the Bible was ruled out, for it could not be read in the public
schools except in “sectarian” English translations.

Here are the roots of the dilemma posed by the acceptance of
the practice of separation of church and state on the one hand, and
the general acceptance of compulsory public education sponsored
by the state on the other, Here is the nub of the matter that is all
too often completely overlooked. . . .3

In other words, the public schools in the United States took
over one of the basic responsibilities that traditionally was always
assumed by an established church. In this sense the public-school
system of the United States is its established church But the situa~
tion in America is such that none of the many religious sects can
admit without jeopardizing its existence that the religion taught in
the schools (or taught in any other sect for that matter) is “true” in
the sense that it can legitimately claim supreme allegiance. This
serves to accentuate the dichotomy between the religion of the na-
tion inculcated by the state through public schools, and the religion
of the denominations taught in the free churches.®

In that same year, 1963, Rushdoony wrote:

The extensive emphasis on moralism and patriotism in state-
supported schools was in fulfilment of this purpose in their crea-
tion, to become the “catholic” [universal] church of the people of
America and the moral identity of the body politic. This aspect of
educational history is in abundant evidence; it has been neglected

5. Ibid., p. 67.
6, Ibid., p. 68.
