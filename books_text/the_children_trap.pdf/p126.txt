106 The Children Trap

15. Graduates of unaccredited high schools can still get into top-
flight colleges and universities.

16. Certification of teachers is another threat to private schools.

17. The founders of humanist philosophy themselves could not
have taught as state-certified teachers.
