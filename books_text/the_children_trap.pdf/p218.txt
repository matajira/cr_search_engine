198 The Children Trap

God. It isn’t possible to ramrod God’s blessings from the top
down, unless you're God. Only humanists think that man is God.
All we're trying to do is get the ramrod away from them, and melt
it down. The melted ramrod could then be used to make a great
grave marker for humanism: “The God That Failed.”

The Continuing Heresy of Dualism

Many (of course, not all!) of the objections to the material in
this book series will come from people who have a worldview that
is very close to an ancient church problem: dualism. A lot of well-
meaning Christian people are dualists, although they don’t even
know what it is.

Dualism teaches that the world is inherently divided: spirit vs.
matter, or faw vs. mercy, or mind vs. matter, or nature vs. grace.
What the Bible teaches is that this world is divided ethically and per-
sonallp: Satan vs. God, right vs. wrong. The conflict between God
and Satan will end at the final judgment. Whenever Christians
substitute some other form of dualism for ethical dualism, they fall
into heresy and suffer the consequences. That's what has happened
today. We are suffering from revived versions of ancient heresies.

Marcion’s Dualism

The Old Testament was written by the same God who wrote
the New Testament. There were not two Gods in history, mean-
ing there was no dualism or radical split between the two testa-
mental periods. There is only one God, in time and eternity.

This idea has had opposition throughout church history. An
ancient two-Gods heresy was first promoted in the church about a
century after Christ’s crucifixion, and the church has always re-
garded it as just that, a heresy. It was proposed by a man named
Marcion. Basically, this heresy teaches that there are two completely
different law systems in the Bible: Old Testament law and New
Testament law (or non-law). But Marcion took the logic of his
position all the way. He argued that two law systems means two
Gods. The God of wrath wrote the Old Testament, and the God of
mercy wrote the New Testament. In short: “two laws-two Gods.”
