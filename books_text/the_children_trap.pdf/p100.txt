80 The Children Trap

tion. They want to foreordain the future. This is why they estab-
lish planning commissions. They want to be omnipotent (all-
powerful). This is why they try to control everything.

The Religion of the Public Schools

“Then Paul stood in the midst of the Areopagus and said,
‘Men of Athens, I perceive that in all things you are very religious’”
{Acts 17:22).

John Dewey is considered to have had more influence on mod-
ern public school education than any other person. Dewey was a
professing atheist and a socialist. Yet he stated that it is up to
America’s public schools to save our threatened religious heritage.

That sounds strange, coming from the pen of a person who
was an unbeliever. We have heard it said so often, “Religion has
been taken out of the public schools.”

‘Was Dewey wrong? Did he want religion in the schools? If so,
why have his followers thrown out any traces of the Christian relig-
ion? No, Dewey’s program was right on target. Dewey wanted the
religion of humanism in the state schools, and that is exactly what we
have in the United States today. The public schools are anti-
Christian. They are not anti-religious.

The original model for public schools around the world is still
operating, the Ecole Polytechnique of Paris. It was created by the
successors of the French Revolution. In 1795, the last year of the
guillotining of thousands of Frenchmen, the government of
France centralized all secondary education, meaning all higher
education. They created a series of schools (“ecoles’) that were
firmly under government control.

The Ecole Polytechnique had been set up in 1794, Under the new
administration, it became the science and technology institute for
France. The ruling vision was that of engineering. Men were
taught to view all of society as a vast machine. They were taught
that “social engineering” is possible, and that a top-down planning
system could bring productivity and power to France. This is the
ancient power religion of Egypt and Satan, but it was dressed up
in scientific clothing. It was dressed up in the language of scien-
tific neutrality.
