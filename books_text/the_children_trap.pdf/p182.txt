162 The Children Trap

other civics teachers, or reassign them to other schools or courses.
Hire two part-time instructors to read the true & false portions of
the exams, and he would read the papers (required for A and B
students) and the essay portions of the exams. Double his salary.
Save tens of thousands of dollars every year! The district refused.
They could have replaced the other high-paid civics instructors,
paid him and the part-timers, and pocketed the difference. I think
they feared that he could pull it off, so they did not allow the ex-
periment to go forward.

Lesson: you can get more students in those classrooms if the
teachers are competent, Why hire incompetent teachers?

Second, personally investigate the curriculum. Get a list of
every textbook in use. Use the services of Mel and Norma Gabler
in Longview, Texas to help spot really bad public school text-
books. They are authors of the book, What Are Thep Teaching Our
Children? ($5.95). You need to read this. Order from:

Educational Research Associates
P.O, Box 7518
Longview, Texas 75607

They also publish a six-page rating sheet of some of the most
popular public school textbooks over the past decade. The price is
$10. You will also receive their 50-page handbook describing the
typical materials they have on hand.

(Discounts on their books are available to parent groups. Such.
groups can purchase a box of 48 copies of What Are They Teaching
Our Children? for $72, a very good deal. These books can be used
in election campaigns for school board.)

Demand that teachers and principals produce all public docu-
mentation handed out during any district-financed trips to profes-
sional meetings that would lead to changes in the local curricu-
tum. The board needs to be given full written reports from all
those who attend taxpayer-financed meetings of any kind. See if
these people can write a coherent sentence,

‘This may not get the information you need, but if they have to
