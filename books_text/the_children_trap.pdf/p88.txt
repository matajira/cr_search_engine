68 The Children Trap

word is “polis.” The Greek word has survived in such names as
“Indianapolis” and “Decapolis.” The famous Greek temple, the
Parthenon, was built on the Acropolis, which means the highest
part of the city.

In ancient Greece, there were city-states such as Athens and
Sparta. Today we have cities, towns, counties, states, and other
units of civil government. In addition, there is the ever-present
Federal government, not to mention efforts to establish one-world
government.

The police are hired by governments. Generally we associate
the police with local governments. Because the police have so
much power (that’s why we usually refer to them as the police
Jorce), we consider it desirable to keep them under the control of
our counties and cities. A few years ago the bumper sticker, “Sup-
port Your Local Police” was promoted to block the idea of a na~
tional police force.

For the same reason that we want local control over the police,
we might argue that we should have local control over our
schools. Let’s have “neighborhood” schools controlled by locally
elected officials. I agree that if the schools are going to be run by
the government, then local control is best. But why should gov-
ernment at any level operate the schools? Civil governments are
given the power of force, the police power, to punish evildoers.

‘The public school system is based on the idea of force. It should
be called a government school system because it is run by the govern-
ment. In effect, the civil government or public schools are operated
by the police. It does not matter that the persons in charge in the
classrooms are called teachers or that there are principals and
superintendents over them. They are able to operate as they do
only because they have the police power, the power of force.

Coercive Education

The public school system operates on the principle of compul-
sion in two respects: compulsory attendance laws to force the stu-
dents to come, and compulsory taxation to force the taxpayers to
pay the bill, The two go together. Because school attendance is
