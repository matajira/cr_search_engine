Index 187

wealth-redistribution schemes, 118

welfare pf children, 36

welfare state, 36, 54

Western civilization, 86

Westminster Theological Seminary, 56

“What Are They Teaching Our
Children”, 162

whorehouses, xiii

“Why Johnny Can't Read,” 72

“Why Johnny Still Can't Read," 72

widow Jones, 17-18

Williams, Roger, 42

work ethic, 88
wortd-and-life view, 64
write your state representatives, 159

“You Are Extraordinary,” 42

Zabel, Orville, 115

zoning boards, 6

zoning commission meetings, 158
zoning laws, 17, 110, 113-14
zoning restrictions, 43

zoning, Master Plan, 111
