8 The Children Trap

there is. God is the Creator. He is Sovereign. He rules over all.
He has made all things out of nothing. He is self-sufficient. Man,
on the other hand, is a creature. He is dependent on his Creator.

In this book, I hope to convince you that parents are responsi-
ble for the education of their children and that education is to be
God-centered. I will write from experience. I will cite facts. I will
use reason. I may come up with a hundred and one different ways
to try to convince you. But in the last analysis, the only case I can
make is from Scripture.

The ultimate authority for all things is God. God has revealed
Himself in two ways. One is natural revelation. The other is
supernatural revelation.

Natural revelation is God's revelation in nature. “The heavens
declare the glory of God; and the firmament shows His handi-
work. Day unto day utters speech, and night unto night reveals
knowledge” (Psalm 19:1-2).

The design of a snowflake reveals God. The regular appear-
ance of Halley’s Comet reveals God. The seasons, the transition
from day to night reveal God. All the world knows that God ex-
ists, but as Paul says, “because, although they knew God, they did
not glorifiy Him as God” (Romans 1:21).

What Paul is saying is that the non-believer has a revelation
from God, but he chooses to worship man instead. That is hu-
manism. The other way in which God makes Himself known is by
supernatural revelation. Natural. revelation is sometimes called
general revelation because it comes to everyone in the world.
Supernatural revelation is referred to as special revelation be-
cause it comes to a special people, the saved. God has preserved
His supernatural or special revelation for us in the Bible.

The Bible is God’s Word. It is true. The Scriptures are profit-
able for all things. The Bible is a lamp to our feet and a light to
our path (Psalm 119:105). The Bible is our authority. The Bible is
perfect. It is without error. The Bible is infallible. It can not be
wrong.

I want to show from the Bible what the principles are that
should guide us in education. If we depart from the Bible and
