RECOMMENDED READING LIST

There are many places to get help in educating your children. IF
you are teaching your children at home, I recommend you contact
Christian Liberty Academy Satellite Schools (CLASS), 502 West
Euclid Avenue, Arlington Heights, Illinois 60004. Their phone
number is 312-259-8736. They have a home school program enroll-
ing thousands of children. Those teaching in the home will find
Samuel L, Blumenfeld’s book, How to Tuior, useful. It can be pur-
chased from Mott Media, P.O. Box 236, Milford, Michigan 48042.

Parents teaching in the home will find any of the books listed
below helpful. They can be purchased from Fairfax Christian
Bookstore, P.O. Box 6941, Tyler, Texas 75711. Their phone num-
ber is 214-581-0677 or 214-561-7944. Request a catalog. Many
other fine books and materials are available through them.

Books and Materials

The Messianic Character of American Education, by Dr. Rousas J.
Rushdoony, traces educational philosophy from Horace Mann
down to the present. Rushdoony shows how humanist educators
look upon the public schools as the new messiah, designed to save
tmankind from all its ills. Rushdoony is an important writer and
thinker who has authored over 30 books. This book is widely used
in conservative Christian colleges.

Child Abuse in the Classroom, edited by Phyilis Schlafly, consists
of excerpts from the testimony of parents and students before the
United States Department of Education in connection with the
protection of pupil rights. If you want to read some. firsthand testi-
mony of what is going on in the government schools, this book is

173
