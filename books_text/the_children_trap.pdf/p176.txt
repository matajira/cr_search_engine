156 The Children Trap

the State Board of Education voted overwhelmingly to request the
Virginia General Assembly to give the State Board the power to
identify and approve all non-public schools in Virginia. The
Board of Education wanted the authority to grant this approval
based on rules, regulations, and other criteria that it would prom-
ulgate from time to time. Talk about a blank check! Just sign here
on the dotted line, General Assembly.

Christians in Virginia pressured the legislature to defeat that
power move decisively. The Board passed its resolution in June.
By December (before the legislature went into session), they had
rescinded their own resolution. Christians defeated them by lob-
bying the legislators and by putting the legislators on public
record. It was an election year, and thus great timing for us. Peti-
tions were the most effective weapon we used. In the petitions, we
attacked the public school system and called on the General
Assembly and the Governor to reaffirm Virginia’s long tradition
of educational freedom.

In Texas, a mass rally by Christians was held in the state capi-
tal in 1986 within a few days of the school board's almost invisible
public announcement of hearings concerning the board's assertion
of control over private schools, an authority never granted by the
state legislature. This overnight mobilization took place in large
part due to-a series of radio broadcasts by Christians, especially
Marlin Maddoux’s satellite broadcast, “Point of View.” He had
been tipped off about the announcement of public hearings, and
he shocked the school board by calling forth thousands of Chris-
tians who tried to get in to be heard.

Another Texas radio personality, George Grant, the author of
the Biblical Blueprints book on private welfare (In the Shadow of
Plenty), immediately produced a follow-up tape recorded radio
message that warned Christians in Texas of this tyrannical move
by the State Board of Education. This message was so effective
that word of it got to an out-of-state leader of a nationally known.
Protestant youth ministry that is involved in producing home
school materials, but also involved in selling out the home school
movement to state education boards around the nation. He fran-
