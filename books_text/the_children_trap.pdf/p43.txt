God Owns the Children 23

see that they get a God-centered education now. If Christian par-
ents will act now, and fay now, they can have Christian education.
It is a realistic goal. Taking over the whole governmental struc-
ture in the country will take much longer.

In the next chapters I want to show how the humanists are
working to control our children.

Summary

Neither the family nor the state owns children. God is totally
sovereign over all history. He is the Creator. Therefore, He owns
everything.

Nevertheless, He delegates limited sovereignty to human in-
stitutions. Which human institution has been granted primary
control over children, the state or the family? The Bible teaches
that it is the family, as the next chapters clearly demonstrate.

Are schools to be agents of the state or the family? The Biblical
position follows from the initial starting point: families have
stewardship over children, and therefore over schools. The human-
ists deny this. They deny God’s sovereignty, they deny the crea-
tion view or origins, and they affirm the sovereignty of man. This
means in practice the sovereignty of mankind’s most powerful
earthly institution, the state.

The battle between Christianity and humanism is going on in
the debate over education. Christians and humanists have rival
views of God, man, law, and time. Obviously, the schools are
training places for such views. Thus, there can be no reconcilia-
tion between these views. All education is therefore ethical; there
can be no ethically: neutral education.

The public schools are built on a myth: the myth of neutrality.
This is how they keep Christian views out of the classroom. But as
this anti-Christian bias becomes more obvious to Christians,
Christians figure out that the neutrality doctrine is a myth.

Thus, Christians must seek to build up Christian schools, and
steadily replace taxpayer-financed education. We should get civil
government out of the education business.

In summary:
