64 The Children Trap

the household. It is the parents’ God-given responsibility to train
their children, not the state’s.

Lrresponsible Parents?
But what about the following response?

“But we know that some parents are irresponsible. They do not
know God. They care nothing about God. They will teach their
children a false view of God. They will not obey God and train
their children in the admonition of the Lord. Thus, it is our re-
sponsibility as Christian citizens to compel these short-sighted par-
ents to send their children to church.”

Are there irresponsible parents? Of course. Will these parents
refuse to send their children to church? Most certainly. Should
their children hear the Gospel? Absolutely; God will hold both the
parents and the children responsible on judgment day. But are
these arguments a sufficient reason for taking the children away
from the parents one day a week to indoctrinate them — that is the
word—in a rival religion, even if the religion is Christianity? I
don’t think so, and I don’t think you do, either.

But the humanists require Christians to send their children to
state-licensed schools, five days a week, six hours a day, and then
encourage the children to get involved in after-school extracurric-
ular activities.

Let’s ask another important series of questions. Are there par-
ents who will not give their children religious instruction? No,
there aren't. Every parent gives his children religious instruction.
There is no neutrality. They may give them instruction in a relig-
ion other than Christianity. Maybe they will teach them Judaism,
or Islam, or Buddhism. Maybe they will teach them alcoholism,
or revolutionism, or debauchery. Maybe (probably) these days it
will be humanism. But some world-and-life view will be taught in
the home. There is no escape from a worldview. Men cannot
think or act without one.

Therefore, any attempt on the part of the state to require a
child to listen to Christian preaching once a week is an assault on
