10
WHAT THE CHURCH CAN DO

But if anyone does not provide for his own, and especially for
those of his household, he has denied the faith and is worse than an
unbeliever (1 Timothy 5:8).

The church should begin to examine members who are send-
ing their children into public schools. Pressure should be placed
on them by the elders. This would include discussions, loaning
them a copy of this book or the books by R. J. Rushdoony on edu-
cation, and similar materials. If a church has a two-tiered mem-
bership structure which acknowledges the difference between
communicant members (such as children) and voting members,
then it should limit voting members to those who refuse to send
their dependent children to public schools.

Clearly, no elder or deacon in such a church should send his
children into the public schools. It would set a very poor example.

The elders should begin to search out local Christian schools
and examine their curriculum materials. The church can recom-
mend schools to members. This is a legitimate service of the
church, for the church is the protector of families. It is simply pro-
viding specialized information that members may not possess.
The pastor should have far more experience in evaluating Chris-
tian education than the average member. If not, it is time to finda
new pastor.

But what if the local Christian schools are inferior? What is
the church’s role as protector of families? Is it time for the church
to start a school? After all, you can’t beat something with nothing.

143
