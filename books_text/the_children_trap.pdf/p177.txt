What the Civil Government Can Do 157

tically called the directors of Christian radio stations all over
Texas, begging them to refuse to broadcast this “dangerous, heret-
ical” message by Grant. I am happy to say that most of them ig-
nored this leader’s fear-driven advice. A few capitulated, however.

This leader is terrified of the coming inescapable basic con-
flicts with humanists in the field of Christian education. He is not
alone in his fear of the state or his willingness to compromise for
the sake of a little more time and some temporary peace. His view
of time teaches him that the church will inevitably lose in its at-
tempt to defeat Satan’s forces. His theology says that there is no
hope until Jesus comes and “raptures” His people out of trouble
just before things get terrible on earth. He has no faith in the
church; therefore, he recommends capitulation before the state.
He is trying to buy time until Jesus comes and physically delivers
us from our earthly problems, since we are incapable of solving
them through His grace and by His law. Such views have been
dominant in American fundamentalism for over a century.

Bible-believing American Christians have been on the defen-
sive intellectually for a century, and especially since the famous
Scopes “monkey” trial in 1925. From then until about 1965, they
spent most of their intellectual time running from the humanists
or granting humanists many of their presuppositions concerning
Biblical interpretation, especially of the first eleven chapters of
Genesis. They gave the humanists a free ride. Or more to the
point, they allowed the humanists to kidnap their children, pick
their pockets, and send them to the back of the bus—in this case,
the public school bus. At long last, this is beginning to change.
This little book may speed up Christian resistance.

Humanists aren’t used to being put on the defensive. They
have been having their God-hating way for a long time. God’s
people have been “eating, meeting, and retreating.” Our religion
has been escapist in nature. We have been satisfied to worship on
Sunday, have prayer meeting on Wednesday, choir practice on
Thursday, and some personal witnessing at other times. But we
haven't challenged the humanists on their own turf. We've been
rather quiet. We've been hiding in the woodwork somewhere.
