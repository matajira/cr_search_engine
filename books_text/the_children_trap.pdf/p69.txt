He Whe Pays the Teachers Will Control Education 49

effectively runs the school. This affords the parents a considerable
amount of control.

Yet a different arrangement is the profit-seeking, owner-
operated private Christian school. I operate this kind of school, so
am very familiar with it. We have a taxpaying school. The private
Christian school which I operate has no board.

A closely held corporation could also run a school, either on a
taxpaying or tax-exempt basis. Or the corporation could be pub-
licly held.

Control Through Enrollment

How does a parent control the education of his child in these
various forms of organization? The parent has the power of
choice. He may choose among the various schools available in his
community. If he doesn’t like any of them, he has the option to
educate his children at home, start another school, or move to
another community.

The government school allows no choice. You are assigned a
school district based on where you live. District lines may be
redrawn at will. The school may be closed, and the children sent
to another one. The government may bus your child across town.
When I was in Louisville, Kentucky, several years ago, families
told me that some of their children went to local schools, while
others were bussed miles away in different directions. For the
most part, the government dictates which school your child will
attend.

Deciding to patronize one Christian school rather than
another is the most important choice the parent makes. The par-
ent is in control. In a free market economy, the consumer is king.
Consumers make millions of choices in the marketplace every
day. They choose this brand of soap rather than that one. They
buy this make of car rather than another. They decide what they
want to buy, how much they will pay, and from whom they will
buy. They have the money, so they have control. Ifa seller says to
a consumer, “Take it or leave it,” the consumer can usually leave
it. The seller gets no sale.
