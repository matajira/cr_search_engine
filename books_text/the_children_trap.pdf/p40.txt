20 The Children Trap

his soul and master of his fate. Man is his own saviour through
the almighty state or the almighty something else—but never
Almighty God.

Biblical morality teaches that the child is to honor his father
and his mother. The Christian school is an extension of the home.
Parents choose a school for their children and support that school.
The school works closely with the parents to carry out the goals of
the parents.

The government schools, on the other hand, undermine the
authority of the parents by substituting the state and state-
employed teachers in the parents’ place. The school is not seen as
a delegated agent of the parents, but as the delegated agent of the
state. He who pays controls.

The Biblical prohibitions against murder, adultery, theft, false
witness, and covetousness are daily taught in a Christian school.

The public schools have a problem. How can they speak out
against theft? Certainly they cannot consistently oppose theft be-
cause it is contrary to God’s Word. They can appeal only to expe-
diency or the word of man. Since the government schools are
funded with money taken from the taxpayers against their will
and contrary to the Word of God, those schools have a real prob-
lem in opposing theft.

This is why free market economics is not being taught in gov-
ernment schools. A socialistic system can hardly teach free market
economics. The state is buying the education it wants. It buys the
attitude on the part of students that it wants. That attitude is one
of obedience to the state.

Children are always quick to ask why they should do this or
why they should not do that. What can the government schools
say? “Do this because the majority of people think you should.”
Or: “Do that because I say so.” They can only appeal to feeling,
numbers, force, or something of that kind. The Christian teacher
points to the Word of God. God is the ultimate authority.

The Effect of Christian Teachings

The Bible teaches that the child has inherited a sinful nature.
He is born with this sin nature. The humanist believes the child is
inherently good, or at least not inherently evil. Because the Chris-
