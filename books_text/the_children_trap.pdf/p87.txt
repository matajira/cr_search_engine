Education Should be Voluntary 67

The power of the sword is strictly limited in the Bible. One of
the cliché’s popular among conservatives is that we believe in “lim-
ited government.” The problem is that we seldom say what we
mean by the term. Limited to what? Some persons say the gov-
ernment ought to do for the people what the people cannot do for
themselves. Who is to say what people cannot do for themselves? By
what standard does a society properly make such a determination?

The only proper guide to follow is the Bible. What are the lim-
itations on civil government that are set forth in the Word of God?
Paul informs us in Romans 13 that the civil ruler is to be “an
avenger to execute wrath on him who practices evil.” “For rulers
are not a terror to good works, but to evil,” Paul states in 13:3.
(For more details on Biblical civil government, see Gary DeMar’s
book in the Biblical Blueprints Series, Ruler of the Nations.)

Should the Police Run the Schools?

The answer to this question ought to be obvious. Of course the
police should not be running the schools. The police should be out
catching criminals and bringing them to justice. The duty of gov-
ernment is to “punish evildoers.”

Now suppose we ask the question another way. Should the county
(or city) run a school system?

“Well, of course it should,” many Christians will reply. “After
all, how else are the children going to get educated?” We may be
given a dozen reasons why the government should be in the edu-
cation business. The police power of the civil government sup-
posedly must be applied to parents to compel them to educate
their children.

Then comes the next argument: “Since some parents just can-
not afford to obey this law, the civil government has a moral and
legal obligation to supply free schools.” How “free” are these
schools? As “free of charge” as your property tax bills, paid until
you die, whether or not you have school-age children.

The Police Power

It is helpful to know where the word “police” originated. This
familiar word comes from the Greek word for “city.” In Greek the
