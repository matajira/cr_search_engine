16 The Children Trap

come, If it did, everyone would quit working, and there wouldn’t
be any income to tax. So the government exempts some income
from taxation. Then it taxes the remainder at a rate below 100%.
The top rates have in the past been over 90%, and currently are
50%. While it is encouraging to see the top rates coming down,
the fact remains that the government claims the right to tax all
income at 100%. (This is actually done in Sweden. Film producer
Ingmar Bergman fled Sweden in the 1970’s when the government
taxed him 102% of his previous year’s income.)

Consider private property. When we buy a house, we like to
think that we own it. The deeds I get for my property state that
my wife and I are “tenants in common” or “tenanis by the entirety.” I
always thought a tenant was a person who was renting from
someone else. Who is our landlord? You guessed it. The state lays
ultimate claim to our property. Just as the kings claimed that all
the land belonged to them in ancient times, so modern states
claim ownership over the land. As they claim the right to tax
100% of our incomes, so they claim the right to unlimited taxation
on our real estate.

The humanistic claim to ownership of land is seen in the story
of Tobiah and Sanballat. It is recorded in Nehemiah 5. The two
men had driven the people of Jerusalem to the brink of slavery
through confiscation, conscription, and abusive taxation.

Nehemiah, the godly governor of the province, confronted
their sin and exposed their power grasping schemes. The issue for
him was sovereignty. For that matter, the issue for Tobiah and
Sanballat was sovereignty, too. But whose sovereignty? For Nehe-
miah, God’s sovereignty over the land precluded power plays. For
Tobiah and Sanballat, their sovereignty gave them absolute free
reign.

The humanistic state today, like the administration of Tobiah
and Sanballat, claims the right of “eminent domain.” What it
wants, it takes. At least in the United States, the Constitution
provides that the taking is supposed to be for public uses, and
compensation is to be paid the landowner. But this is not always
honored in fact by the courts.
