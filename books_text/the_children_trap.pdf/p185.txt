What the Civil Government Can Do 165

grading, (4) the preparation of the student for his college board
exams. Have the students attach a photocopy of their SAT or
ACT scores on the sheet, but not their names.

This system works for high school graduates. What about
grade school teachers and junior high teachers? Just go to the
record books. The computer can do it, or be programmed to do it.
See the grade point averages of those students at the next level up.
Check out the grades of the first-year junior high students to see
how well the elementary school teachers are doing. Check out the
grades of first-year high school students to see how the junior high
school teachers are doing.

If some teachers consistently produce above-average or below-
average students, take appropriate action,

When these teachers are ready for raises, start using student
ratings to evaluate who deserves a raise and who should be en-
couraged to seek employment elsewhere.

Anyone who says that students are not competent to rate
teachers has forgotten how well he knew when he was a senior, By
having the students include a photocopy of their board scores
(without names), you can find out if the poorer prepared students
rate poor teachers well and hard teachers poorly. But I can pretty
well assure you that most students going to college will agree on
which teachers did their homework.

I know of a person who is presently developing the appropri-
ate computer program to rate teachers. Contact him at

Grading Teachers
P.O. Box 8204
Ft. Worth, TX 76124
You will then push the board to make these “report cards”
available to students, parents, and the press. If the schools were
truly free market institutions, parents would be given the right to
select the teacher of their children. They would have to pay extra
to get popular, competent teachers, This would be a true merit
pay system, This is one reason why universities pay high salaries
