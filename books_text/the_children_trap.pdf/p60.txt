40 The Children Trap

there are in these home schools. This is a good thing; neither do the
humanist planners. In some cases, the parents teach their own chil-
dren. In other cases, two or more families cooperate in the task.

What are we to think of home schools? I applaud them. There
are some Christian schools that oppose home schooling. Maybe
they don’t want the competition. I don’t know. I think Christian
schools should encourage home schools in every way. They should
work with home schoolers instead of against them.

The Christian Liberty Academy in Illinois has a very large
home school program. The number of children in the home
schools is far greater than those enrolled in the regular academy.
(The Christian Liberty Academy also has the largest Christian
school physical facility in the country; they bought it from the
public schools in 1985 for $1.5 million, with the money they had
made from selling home school materials.) There is no reason the
two should not work together to accomplish the goal of Christian
education.

The Christian school which I operate had its roots in a “home
school” some 27 years ago. I was teaching in a private school. My
wife was home with several small children. She wanted to teach
the oldest two at home. One was three, the other was four. My
wife invited several families in the neighborhood to send their
children over for two and one-half hours each day to be in-
structed. One of the mothers watched our two year old at her
house. The arrangement worked to everyone’s advantage.

After leaving the private school where I taught, I started a
small school in the basement of a house. We had six students ini-
tially. This expanded to 10 students at the end of the semester.
That fall I opened Fairfax Christian School with 32 students,
ranging from kindergarten through the eighth grade. My wife and
I taught them all, including two of our own. We held classes in
our home. We had two classrooms downstairs. We lived upstairs
“over the shop.” It was a neat arrangement.

The strength of the home school movement is the direct con-
trol exercised by parents. The course of study can be tailored to
the specific needs of the child. The program is flexible. A great
