What the Civil Government Can Do 153

government out of the education business. According to the Bible,
education is a parental responsibility. It is not the place of govern-
ment to be running a school system,

We should not waste time trying to improve the public schools
merely for the sake of improving them. God didn’t tell Joshua to
reform the way the Canaanites were living in the land. God
wanted no compromise. We should not compromise either. To use
a popular modern term, the schools need to be “privatized.”

Maybe this sounds like a pipe dream to you. I would simply
point out that the majority of the Israelites opposed going in to
possess the land. At an earlier period, when the spies brought
back their majority report about the “giants” who inhabited the
land, the people were afraid. They rejected the minority report
which urged them to obey God (Numbers 13-14). The result was
40 years of wandering in the wilderness. When they finally de-
cided to go in to possess the land, God gave them victory.

The intrusion of civil government into education did not take
place overnight. I do not think we Christians can close down the
government school system overnight either, That must be our
goal. We must convince ourselves first. Then we will be in a posi-
tion to convince others.

Christians need to go on the offensive. The United Methodist
Church considered removing the song “Onward, Christian Sol-
diers” from their church hymnal in 1986. They thought that the
song was too militaristic. That church was captured long ago by
theological liberalism. We can understand their dislike of the
song, even if we don’t agree with their theology. The song points
to the victory of Christ’s church on the earth. They don’t want
Christians marching. Sadly, fundamental Christians sing the
song, but “eat, meet, and retreat” in practice. What is amazing is
not that a bunch of liberal Methodists wanted to get rid of it.
What is amazing is that pietistic fundamentalists still sing it; it is
totally at odds with their view of the impossibility of the earthly
progress of the church prior to the second coming of Christ. The
song is correct; the pietists are incorrect. It is time that we go for-
ward to possess the land,
