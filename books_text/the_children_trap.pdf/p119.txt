Christian Schools Must Resist State Certification 99

worry about our children getting into college. The colleges that
are worth attending will appreciate young people who are inter-
ested in something other than beer and pot.

I remember visiting prestigious Stanford University at the
time of the student riots in the late 1960's. It looked as though it
had been bombed out. Broken windows had been boarded up. It
was a war zone. The colleges in that era learned to welcome stu-
dents who weren’t going to tear the place apart.

One of my own sons (who new runs our Christian school) was
graduated from our high school at age 15. When he went off to col-
lege, he took college level entrance tests, and received a full year
of college credit. He was a sophomore in college at age 15, and
graduated at age 18. One of his complaints about college was that
the students were immature.

What about transferring to the public schools? This has to be
the biggest bugaboo of all. We have found that our students are
often put ahead a full grade when they transfer. On the other hand,
we often have to put them back a grade or two grades when they
transfer to us from the public school.

We need to realize that the government schools want every
child they can get their hands on. It means more money and more
power for them. They are nat going to make it difficult to transfer
back to their schools. All they want to do is to create the impres-
sion that it is some kind of “problem” so parents don’t opt for the
Christian schools in the first place.

Is There Any Danger in Accreditation?

If a school feels accreditation is really important to them, I
recommend joining with other like-minded schools and having a
private accrediting agency. There is danger in getting state ac-
creditation or in being accredited by a private organization that
has a humanistic philosophy.

State accreditation involves state control. Furthermore, we
should not want humanists passing upon the standards of our
schools. The accrediting agency is going to influence (in a subtle
manner in many cases) the curriculum, the educational! philoso-
