120 The Children Trap

Another hidden cost is the tax-exempt interest on the bonds
sold to finance construction of new government schools. The in-
terest rate is lower than market rates because the bondholder
doesn’t have to pay income taxes on the interest. That means that
someone else gets hit with a higher tax bill. The government is
going to collect a certain sum of money one way or the other.

Selling bonds is a favorite way to get school buildings right
away. Citizens forget that the bonds have to be repaid. Often
these bonds are not repaid for 30 to 50 years. Because of inflation
the bondholders frequently never get paid the true value of the
money they loaned. The long-term bonds mean that the next gen-
eration is paying for the education of our children.

Summary

There are hidden costs in education. The humanists on the
one hand do their best to impose hidden costs on their rivals, the
Christian schools. On the other hand, they atternpt to hide the
hidden costs they impose on the taxpayers for maintaining public
education.

Christians have not paid sufficient attention to the moral costs
of public education. They certainly have not understood the hid-
den economic costs. It should be the goal of Christians to assess
the true cost of private education compared to public education.
When they better understand the true costs involved, they will be-
come less resistant to the moral and religious case against the gov-
ernment school system.

In summary:

1. Christians have not counted the full costs of taxpayer-
financed education.

2. The cost is not simply the money paid out to finance these
schools.

3. The humanists are trying to increase the costs of private
education.

4, Too many parents are avoiding this “free” (zero-tuition) service.

5. Tax-exempt schools are threatened with the removal of their
tax-exempt status unless they conform to Federal standards.
