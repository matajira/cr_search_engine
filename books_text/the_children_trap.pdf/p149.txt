What the Family Can Do 129

appears —and it is spreading in many Christian institutions, in-
cluding seminaries.

The family is a separate institution from the church. The
church should not take over family functions, any more than it
should try to dominate the state. The church is a ministry of
grace. It is God’s institution on earth to carry on public worship,
to proclaim the Gospel, and to carry out the Great Commission.
The church is to proclaim the Word of God and apply it in the
lives of believers. The church should facilitate parents so they can
adequately carry out their responsibility to educate their children,
along with other things. The church should also encourage us to
fulfil our duty to labor in our vocations, to be involved in civil
government, etc,

The minister teaches that the earth should be subdued to the
glory of God. The members then carry out that dominion man-
date.

Recently, the minister of a large church told me that most of
the problems he had in his ministry centered around the Christian
school the church is operating. He sees a serious problem with a
church-run school. Some church schools have discipline problems
because an attempt to deal with problems in the school has reper-
cussions in the church. The admissions policy of the school will
probably have to provide for acceptance of any child in the
church,

In some church schools all teachers have to belong to the spon-
soring congregation. This makes it difficult to get the best-qualified
staff.

Perhaps the major disadvantage with a church school is that
the school is all too often looked upon primarily as an evangelistic
outreach of the church, Evangelism becomes the main thrust of
the school. This may hurt the academic progress of the school.
Christian children: should have the very best education. The pri-
mary purpose of a Christian school should be to train covenant children in all
areas of knowledge. If children come to a saving knowledge of Christ
in a Christian school, that is well and good. But the Christian
school is not just another way to win the lost,
