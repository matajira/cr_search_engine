148 The Children Trap

In past years, we had a teacher surplus. Now a teacher short-
age is developing. But never forget, the word “shortage” should
never be used without keeping this qualifier in the back of your
mind: “at some price.”

IT have always found that there are plenty of teachers around.
Teachers often prefer a Christian school because working condi-
tions are so much better. The pay may not match the public
school, but there are other considerations. Semi-retired or retired
persons may be good prospects for teaching in a Christian school.
The teachers don’t all have to be full-time employees. Many
mothers are qualified and willing to teach or assist with the
school, but are able to work only half a day.

Labor costs constitute the largest item in the school budget.
Over the years, we have tried to keep such costs at 45-50% of the
total budget. Most schools probably find a larger percentage
going for staff salaries.

I learned many years ago that high salaries for employees, low
costs to the consumers, and high profits to the owners go together.
This is true in a free market situation. I have found that a Chris-
tian school can operate this way. We have been able to pay high
salaries (especially by private school standards), while keeping
our tuition fees modest. At the same time, we have realized a
good profit.

Too many Christian schools pay teachers less than they
should. The teachers should not be expected to subsidize the
school. It is the responsibility of the parents to pay for the cost of
educating their children. Many retired Roman Catholic nuns are
on government welfare now because they worked for extremely
low wages as teachers in parochial schools, and adequate provi-
sion for their retirement was not made. This is a shame to the
Church.

A prevalent attitude among many Christian schools is that
teachers will be “more dedicated” if they are paid less. I disagree
with this policy, Teachers should be paid according to the market.
Unfortunately, the presence of government in education means
that we do not have a completely free market. If we had a truly
