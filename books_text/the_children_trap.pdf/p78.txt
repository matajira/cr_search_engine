58 The Children Trap

The teacher-training institutions also exercise important influ-
ence, That is why the humanists want their graduates to have a
monopoly on the public school pulpits. The courts exercise tremen-
dous influence, virtually dictating school policy in some areas.

The little red school house with your friendly neighbors setting
policy is jong gone in America. (So, increasingly, are friendly
neighbors. Do you know the names of the family members of the
people who live in the houses four doors on each side of you? How
many times have you eaten dinner with them?)

In short, humanist kidnappers are demanding ransom, and
Christian parents are paying it. But they never send the children
back, They keep them, and then try to get the parents to send
them on for “advanced kidnapping”: a humanistic college.

Buying Control

When you enroll your child in a Christian school, the situation
is different. You can directly influence and control the education
of your child, In the Christian school I operate everyone knows
that “Mr. T” is the headmaster. If there is a question about curric-
ulum, transportation, discipline, report cards, or whatever, the
parent knows who is responsible.

Earlier, I showed how parental control makes for good disci-
pline. The same is true for academics. The parent in effect says,
“Produce, or I will place my child in another school.” He looks for
results, not excuses. That huge playing field at your local public
high school is an excuse. “Look at our terrific facilities. How can
you complain?” The issue isn’t a school’s facilities; the issue is the
quality of the education received and retained.

Parents don’t have to worry about all the ingredients that go
into making a school a good one, When I go to the grocery store, I
do not concern myself with how they manage to give me such var-
iety and quality for the money I pay. That is their problem, If they
don’t produce, I go elsewhere.

But suppose the government had a monopoly on groceries,
Can you imagine what it would be like? I can. There would be
high prices, inferior merchandise, short hours, and long lines.
