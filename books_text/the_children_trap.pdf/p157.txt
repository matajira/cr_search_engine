What the Family Can Do 137

tant than ever. If they have the knowledge of the Lord and an un-
derstanding of His creation, they will be prepared to earn a living.

5. Get an extra job.

You probably work 40 hours per week on your regular job. If
you can’t cut spending in order to finance your children’s educa-
tion, then the only alternative is to increase your income. One
way to do that is to take on an extra job. People “moonlight” to
buy a boat or to afford a vacation. Why not work some extra
hours each week to pay for schooling? There is no law of the
Medes and Persians (and certainly none in the Bible) that says we
can work only 40 hours per week. I would never have made it fi-
nancially on 40 hours per week.

The 40-hour work week is the salaried man’s trap. It is a mark
of present-orientation. It is the mark of a grim retirement. It is
“part-time retirement in advance.” Avoid it.

6. Perhaps your wife can earn some money.

Your wife should not work outside the -home if the result is the
neglect of the children. When consideration is given to costs of
transportation, clothing, food, taxes, babysitting, etc., the extra
income of a working wife may be illusory. A part-time job may
work out best. A part-time job at the Christian school may be
ideal.

At our school, we employ mothers to drive our buses. We use
small buses that can be kept at home. The mothers may bring
preschool-age children with them on the bus. Their own enrolled
children are with them on the bus. The child’s vacation corre-
sponds with the mother’s vacation.

Other part-time jobs at the school might be teaching, assisting
teachers, secretarial, or custodial work. A mother might handle a
full-time position as a teacher, assistant teacher, or secretary.

A mother could work somewhere other than at the school also.
She might be able to work right in her home. My oldest son has
electronic typesetting equiprnent. Several miles away a lady
works in her home at an IBM word processor. She types the
manuscript in her home. Then it is transmitted by phone to my
