What the Church Can Do 149

free market in education, Christian school teachers would be paid
considerably more, but they would have to perform far more
efficiently.

How can we pay our Christian school teachers higher salaries
in the present situation? The answer lies in becoming more pro-
ductive. It is fashionable in public school circles to blame declin-
ing educational standards and poor discipline on large classes.
The educational establishment has lobbied for more money to
provide smaller pupil-teacher ratios. The fact is that pupil-teacher
ratios have been getting smaller in this country, while standards
were declining. A prestigious report out of Harvard University
several years ago concluded that students in large classes do just
as well if not better than those in small classes.

Many Christian schools have the same erroneous notion
about pupil-teacher ratios. Some even boast about the small
classes they have. Such schools will have to do one of two things.
Either their tuition will be quite high, or they will pay very low
salaries. If the salaries are high and the tuition is low, the money
will have to.come from contributions.

It is greater productivity that has lowered costs and improved
living standards in our economy. More efficient ways need to be de-
veloped to teach children. Lowering the pupil-teacher ratio is going
in the opposite direction of greater efficiency and productivity.

Transportation
My advice to someone starting a school is to avoid transport-
ing the students, if possible. I have a chapter in my manual on
starting schools that deals solely with transportation matters.
Transportation is expensive and occupies much of the attention
and time of an administrator,

Curriculum
Detailed curriculum is beyond the scope of this book. I have
38 pages in my manual on curriculum, There are plenty of books
and materials available from different organizations that are ac-
tive in developing Christian schools,
