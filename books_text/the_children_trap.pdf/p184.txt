164 The Children Trap

who desperately want to avoid admitting how terrible things have
become under their administration. Tell the truth. Accentuate the
negative. Maybe some parents will then pull their children out of
the system.

Sixth, do whatever you can to get the schools to submit to lots
of standardized tests—the more, the better. You need statistical
proof of the decline of your local schools, and there is probably
lots of decline! If the results show that your schools are falling
below the national average, focus all the public attention you can
on the collapsing quality of education in your local schools. Keep
reminding parents how bad things are becoming. Maybe they will
pull their children out of the system.

Seventh, try to get the votes on the board for a total financial
audit. If there was an audit recently, then insist on another one by
an outside firm. This costs money, but in institutions that cost as
much as schools do, the auditors need auditors. It will paralyze
the bureaucrats with fear, especially if they are crooked.

The school bureaucrats will hate you. The sports fanatics will
hate you. So what? Your job is not to get them to like you. Your
job is to represent the interests of the students and the taxpayers.
You can do both by cutting costs, tightening up on school pro-
cedures, and giving raises only to high-merit (B and A “average”)
teachers.

Who are these teachers? Find out.

Grading the Teachers

Try to get the board to impose a merit pay system on teachers.
This will throw panic into the hearts of third-rate teachers.
Maybe they will quit.

Who knows how well a given teacher teaches? The honor roll
(B’s or better) students know. The elite scholarship society (A- or
better) knows even better. Maybe you can get the board to allow
these top students to rate teachers on a form provided by the
board. If necessary, get the recently graduated seniors to fill in the
forms. Have them rate each teacher on an A through F basis on
(1) lectures, (2) ability to answer questions, (3) fairness in
