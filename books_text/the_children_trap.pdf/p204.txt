184

compulsion, principle of, 68

compulsory attendance laws, 43,
63, 68-71, 74, 75, 77, 90

compulsory taxation, 68

control of, 22

creationism, xii, 29, 161, 171

curriculum of, 22, 171

drug education, 87

established church in America, new,
33, 66, 159

God excluded, 27

god of?, 26

hidden costs of, 107

humanistic, essentially, 152

lowest common religious
denominator, 152

neutrality; myth of, philosophy of,
viii, ix-xiv, 23, 77, 78, 82, 84,
87-88, 92

police power, 67

political control of, 57

pragmatism, 88

prayer in, xii, 26,170 °

“privatization” of, 153

PTA, 53, 56

“public enemy number one,” 123

real purpose, 89

religion of the, 80

teligious institutions, 22

school bond elections, 118

sex education, 87, 159

“staying in the system,” xii

tax financed welfare, 37

taxpayer-financed school, ix, x

teacher’s unions, militant, 52

teacher-training institutions, 58

Ten Commandments, xiv, 19, 83

“values clarification,” 84

public schools, cases concerning

California legislature, vii

Chicago school system, 3

Fairfax County Public Schools, 3,
34

The Children Trap

Kentucky, Commonwealth of, 83
Louisville, Kentucky, 49
Maryland public schools, 26
'W. T. Woodson High School, 26

public schools, problems of the
academic results, poor, 57
AIDS, xvii
boring, 33
child molesters, 4
discipline, lack of, 53
drugs, xvii, 4, 53, 65, 75, 87, 159,

163
environment of the, xiv
moral problems, 86
murders, 53
pregnancy, teenage, 87
“rising tide of mediocrity,” 52
sex crimes, 3
suicide, 21, 33, 86
venereal disease, xvii
work ethic, 88

pupil-teacher ratios, 149

pupil-teacher ratio, raise, 161

P ritan(s), xvii, 69

racial quotas, 109

radio stations, 155, 157

“raptures” His people, 157

Reagan's special commission on
education, 72, 169

Reagan, Nancy, 33

The Real Truth, 29

real world, problems of, xiii, 86

register to vote, 158

Rehoboam, 91

religion of the public schools, 80

religion, establishment of, 95

religion, free exercise of, 95

religious presuppositions, 152, 157

return of Christ, 33

Rickenbacker, William F., 6

robes, black, 82

Roman Catholics, 115
