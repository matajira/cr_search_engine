76 The Children Trap

atmosphere is different. The school now operates in a climate of
voluntary exchange between those operating the school and the
parents who are engaging the services of the school to aid in carry-
ing out their God-given responsibilities.

Christians Are Responsible

‘We are in a war for the hearts and souls of men. It has been
going on since Eden.

Christ, by His death and resurrection, regained the deed to
the world that Adam had forfeited to Satan. Christians are there-
fore to exercise greater and greater dominion. In a “fair fight,”
meaning an unsubsidized, noncoercive competitive struggle,
Christianity will defeat all rivals. Unfortunately, few Christians
seem to believe this. Jesus said that He had been given all author-
ity in heaven and earth (Matthew 28:18). Christians for a long
time haven’t seemed to understand that, either, This is at last be-
ginning to change.

If your children were competing against Communists for a
job, would you vote to have your taxes raised, in order to give the
money to the sons and daughters of Communists? If the Com-
munists were illiterate, and you were willing to finance your chil-
dren’s education, would you regard this as a bad thing?

Why not allow people to follow their views concerning God,
man, law, and the future? Why not allow people to finance their
own visions of the future? Why not allow responsible Christians
to triumph over their irresponsible enemies?

The only answer that Christian intellectuals give is some ver-
sion of: “Through the coercive power of civil government, Chris-
tians morally and legally owe a tax-subsidized education to God’s
enemies.” We don’t. We owe them equal justice before God’s re-
vealed law, nothing else. Civil law should not compel any parent
to send his children to school, any more than it should compel
them to send their children to church or Communist indoctrina-
tion schools.

Abolish the public schools, and Christians will take over the
country and the world that much faster. This is what worries
