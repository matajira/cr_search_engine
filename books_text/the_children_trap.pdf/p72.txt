52 The Children Trap

A child learns to read in order that he may read the Bible for
himself. He also needs to be able to read books, magazines, and
newspapers. A person needs information in order to improve his
standard of living. Reading is an important means of gaining
knowledge. A Christian school that produces superior readers is
providing a valuable service to the parents.

The same can be said about the study of numbers. Numbers
represent relationships that exist in God’s world. Arithmetic and
mathematics are vital to an understanding of the world. The com-
puter is an important tool in modern business. The advances in
scientific technology could not have taken place without the study
of mathematics. The study of physics, chemistry, biology, history,
art, music, etc. all serve to enrich our lives.

Educating our children is an investment. It has a monetary ben-
efit. When we choose a school for our children, we want to know
how that school will help prepare our children for the future. We
also equip him to support us in our old age, if this becomes neces-
sary (Exodus 20:12). This is one reason why your child’s tuition
bill should not be regarded as part of the family’s tithe. Tithes go
to God; investments are made with after-tithe income.

The school that is excellent will be rewarded. Parents will de-
termine which of several schools provides the best return on their
educational dollars. The schools that do a poor job will be weeded
out over time. The ones that operate most efficiently will expand.
This is how pocketbook control works.

A few years ago a Presidential commission dealing with educa-
tion in America reported that we have a “rising tide of mediocrity”
in the public schools. This.is exactly what we should have expected,
and what critics of the public schools predicted a century ago,
Parents have the greatest incentive to see to it that their children
get a quality education. The government schools don’t permit
effective pocketbook control by the parents.

This system was deliberately designed by professional, state-
hired educators to reduce parental control. Incompetent teachers
are retained in the public schools because they have tenure. They
are also protected by militant teacher unions with political power.
