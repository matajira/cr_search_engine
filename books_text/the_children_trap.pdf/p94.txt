74 The Children Trap

Another question comes to mind. Why are all these families
voting against the public schools with their pocketbooks? Why are
they paying taxes to the public schools for “the best education
money can buy,” and then shelling out tuition money on top of
that in order to send their children to a private school? Are they so
rich that they don’t miss the money? Are they snobs who send
their kids to private schools for status?

Let me answer some of these questions. The parents who are
choosing the private schools for their children are not rich, For the
most part, they are just average on the economic ladder. Vance
Packard in The Status Seekers admits that the rich are sending their
kids to the public schools. The idea that private schools are just
for the rich may have been true several decades ago. It isn’t today.
Parents are selecting the non-public schools because they are con-
vinced their children are getting a better education in those schools. These
parents are making decisions based on personal experience with
both kinds of schools. They are more aware of the teaching that
takes place in the government schools. As a result, they are mak-
ing the sacrifices necessary to obtain a better education for their
children.

On Playing Hookey

When I was in elementary school, I learned about playing
hookey, That was a rarity in our four-room country school
because we had a mean principal. If you got a licking in school,
you could also figure on getting another one when you got home.
The parents backed the principal.

Times have changed. We still have compulsory school laws,
but hookey playing has greatly increased. In many of our large city
school systems, as many as one-fourth of the students will be
truant on any given day. The public schools of the District of Col-
umbia decided to have school on Good Friday in 1986. (Probably
they think having a holiday then is mixing education with Christi-
anity.) Fifty per cent of the students didn’t come to class. Forty per
cent of the teachers failed to attend!

They say you can lead a horse to water, but you can’t make
