54 The Children Trap

to this. It is the loving care of a parent extended to the classroom
by teachers who stand in the place of the parents.

The government school today has only force. The kids are
forced to come, and taxpayers are forced to pay the bill. The
teachers are controlled by the state, not by the parents. The teach-
ers do not get cooperation from the parents because the parents
have not freely chosen to place their children in the school. When
a child misbehaves in a government school, what is the teacher
supposed to do? The teacher can not appeal to the authority of
God or His Word. This is not allowed in our secularized govern-
ment schools. The schools have difficulty suspending or expelling
the children. That goes contrary to the statist philosophy of com-
pulsory education. They compel the children to attend school. It
is difficult to turn around and compel the children to stay away.

The teacher in the government school tries to get parental co-
operation with an unruly child. Many of the problem children in
the schools come from homes in which the parents don’t care any-
way. That is often the source of the problem. The homes are often
without a father. This situation itself is frequently caused by the
welfare state with its anti-family policies. So the teacher gets little
or no cooperation from the parents,

The situation in the Christian school is different. The parents
place their children in the school voluntarily. The children are ac-
cepted by the school in a free market transaction. The parents are
paying the tuition. They won’t stand for disruptive classrooms
that cheat their children out of the education they are paying for
with hard-earned money. They know that there is no such thing as
a free lunch, and there is no such thing as free education. Because
they are paying directly for the school, they have much greater in-
terest in a good atmosphere for learning. .

In short, money talks.

Children misbehave in Christian schools also. Children are
born with sinful natures, and perfection is not reached on this
earth even by the most sanctified. The difference in the Christian
school is that through the regeneration of the Holy Spirit, the
child receives a new nature. The law of God can be freely taught.
