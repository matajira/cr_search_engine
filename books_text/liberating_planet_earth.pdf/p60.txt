52 Liberating Planet Earth

favor this interpretation, nor discuss all of its applications. Still, I
need to survey this outline, and then compare it with Satan’s ver-
sion, for Satan also uses a very similar covenant structure.

The Covenant’s Structure

Both Kline and Sutton argue that this same structure is found
in the suzerainty treaties of the ancient world. The king (suzerain)
would initially announce his sovereignty over a nation, demand
loyalty, impose sanctions for disobedience, offer protection for
obedience, publish a law code, and establish the rules of succes-
sion. Sutton believes that these treaties were simply imitations of
a fundamental structure of human government which is inherent
in man’s relationship with God.

1. Transcendence/Immanence (presence)

The heresy of deism argues that God is so far above His crea-
tion that He has no personal contact with it. He started it, almost
as a watchmaker winds up a clock, but then He no longer inter-
feres with it. God becomes wholly impersonal to His creation in
history.

The heresy of pantheism argues that God is identical with His
creation, and is immersed in it. He cannot control it because He
is bound to it. He is not sovereign over it. This god, too, is im-
personal.

The Bible rejects both views: total impersonal transcendence
and total impersonal immanence.

Biblical Transcendence

God is the Creator, He is therefore above His creation and
radically different from it. He shares no common being with it. In
Genesis 1:1 we read, “In the beginning God created the heavens
and the earth.” He is the Creator God. He is not part of the Crea-
tion. Thus, the Bible announces the Creator/creature distinction. This
distinction is fundamental to every aspect of life. God is not to be
in any way confused with His creation. He is not part of a hypo-
thetical “chain of being” with His creation. As the Psalmist put it:
