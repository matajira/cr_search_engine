TABLE OF CONTENTS

Introduction. ..
1. Christ and Liberation .
2. The God of Liberation .
3. The Enemies of Liberation .
4, The Covenant of Liberation ...
5. The Liberation of the Individual .
6.
7.
8.

   
   
   
  
 
 
 

. The Liberation of the Family ..

. The Liberation of the Church

. The Liberation of the State ..

9. The Liberation of the Economy

10. The Inevitability of Liberation ..........
Conclusion... .

Bibliography ..

Scripture Index .....

Subject Index... 0.6.6 eee e cece eee eee eee

What Are Biblical Blueprints? ................

 

 

vii
