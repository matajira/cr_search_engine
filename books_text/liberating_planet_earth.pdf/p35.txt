Christ and Liberation 27

How can we get back our birthright?

There is only one way: by submitting to the One who has
reclaimed man’s birthright, the perfect man Jesus Christ, the only
begotten Son of God, who came to earth for a task: to liberate the
earth. And how did He accomplish this awesome task? By living a
life as a man in perfect conformity to God’s law.

All right, that explains how Jesus Christ reclaimed His inher-
itance as the second-born earthly son. But how does this work itself
out in history? Why did this make any difference in history?

Simple: because the inheriting Son died, and rose again. He
gained His inheritance by a perfect life, and then He gave it away
to His people. He established a new covenant with them. Or, as
we also say, He established a new testament with them. A testament
is a document transferring an inheritance to the lawful heirs.
Christians now inherit through Him. The rightful heir, Jesus
Christ, laid down His life for His friends, demonstrating perfect
love. There is no greater love than this (John 15:13).

Satan in principle lost his Adam-given authority the day
Christ died. Christians re-inherited it in principle the day Christ
rose from the dead.

Summary

We are in a war. This war is a war for the hearts, minds, and
souls of men. The issues governing this war are ethical: right vs.
wrong. The rival commanders are personal: God vs. Satan. The
armies are made up of loyal followers who covenant themselves
(chain themselves) to one of the two commanders. Every army has
achain of command. There cannot be an army without a chain of
command. Each army has a set of rules. Each commander calls
his followers to live and die in terms of their assignments. Each
commander promises his followers rewards. But only God can
promise rewards after physical death. He promises eternal judg-
ment for His enemies.

God promises liberation. So does Satan. One of them is lying.
The most momentous intellectual decision that an individual or a
civilization can make is this one: to decide which one is lying. The
