58 Liberating Planet Earth

tion. Presumably, so do subordinate humans. Satan needs a chain
of command in a way that God does not. God uses a chain of com-
mand in history, but He does not need one. He is not dependent on
man or angels to exercise His power, knowledge, and authority.

The problem facing Satan is that he needs the chain of com-
mand, It needs to function as an alternative to God's perfect
power and knowledge. To imitate the omnipotence and omni-
science of God, Satan must impose the threat of judgment. He is a crea-
ture, yet to maintain his kingdom, he needs the power of God.
Thus, he must selectively impose power~not all at once and
everywhere, but as best he can, sometimes directly but usually at
a distance through his followers. No principle of restraint is sup-
posed to stand in his way.

Thus, Satan’s kingdom is a top-down kingdom. It is a king-
dom of tyranny by its very nature.

Common to Communist regimes is a system of street infor-
mants, apartment informants, and even child informants. Every
word and action of every citizen is supposed to be monitored, to
see to it that everyone conforms to the latest Communist Party
line. Why all this control? Because Satan trusts no one under
him. He is a rebel; his subordinates are also rebellious. He knows
their motivations.

He needs total information. Everyone is pressured to inform
on everyone else. No one can trust anyone else. This reduces
everyone to a condition of a slave. Each person is dependent on
what his superiors tell him, yet he is at the mercy of lying subordi-
nates. Lying becomes a way of life; it is perhaps the chief form of
rebellion in the society of Satan. By lying, people try to escape
control, The whole system, top to bottom, and bottom back to
top, is based on deception. Satan is a liar; he builds a kingdom of
lies.

3. Anti-Ethics/pro-magic (manipulation)

Satan seeks power above all. So do his followers. To gain
power, they need to violate God’s law. They cannot do this per-
fectly. God restrains them. Furthermore, any creature that denies
