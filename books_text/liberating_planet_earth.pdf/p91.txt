The Liberation of the Family 83

children of its wards. This is a primary justification for the state’s
existence today. It must administer the inheritance for the benefit
of children. But the children are perpetual servants, increasingly de-
pendent upon the coercive wealth redistribution of politics. They
become a growing army of dependents. The state’s bureaucrats
do not recognize what every human parent must eventually rec-
-ognize, namely, that he is going to become weak, and that he
must encourage independence on the part of his heirs if he is to
secure safety for himself in his old age. The state, by making men
permanent children, guarantees its own demise, for the children
cannot forever support the “trustee state,” if the state has, in
effect, institutionalized the voters.

The family és a trustee. By acknowledging the legitimacy of
the laws of the family, men honor God, although the unregenerate
do so unwittingly and in spite of their professed theology of auton-
omy before God. External blessings flow to those who honor
God’s laws. By establishing a tradition of honoring parents, sons
increase the likelihood that in their old age their own children will
protect them from the burdens of old age. The risks that life poses
to the old are therefore minimized. The familistic welfare struc-
ture is reciprocal and personal, It is undergirded by revealed law and
by family tradition, It need not rely heavily on the far weaker sup-
port of sentiment—an important aspect of the religion of human-
ism. The growth of capital within the family increases each suc-
ceeding generation’s ability to conquer nature to the glory of God,
including the infirmities and vulnerabilities of old age.

The State as Kidnapper

The statist pseudo-family cannot permit this sort of challenge
to its self-proclaimed sovereignty. The modern state has therefore
laid claim to ownership of the children through the tax-supported
public school system. Children are obviously a form of family cap-
ital. They are to be trained, which involves costs to the parents.
But the parents have a legitimate claim on a portion of the future
assets of the children. The relationship involves costs and benefits
for both generations. Neither side needs to buy the love of the
