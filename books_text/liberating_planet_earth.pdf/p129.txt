The Liberation of the Economy 121

The Biblical perspective on marriage, like the Biblical per-
spective on the foundations of economic growth, points to both
ideas: the relationship between work and reward, and the central
importance of the family bond. Men are told to have faith in the
work-reward relationship, which encourages them to take risks
and invest time and effort to improving their own personal work
habits.

The Bible tells us that such efforts will not go unrewarded,
whether on earth or in heaven (1 Corinthians 3). The habits of dis-
cipline, thrift, long hours of effort, investment in work skills, and
the instruction of children in this philosophy of life will not be
wasted, will not be “capital down the drain.” On the contrary, the
Bible teaches that suck an approach to life is the very essence of the domin-
ion covenant. Therefore, when philosophies contrary to the philoso-
phy of Biblical accumulation and dominion are encountered,
Christians should recognize them for what they are.

When men are taught that the capitalist (free market, mean-
ing voluntary exchange) system is rigged against them, that they
have a legal and moral right to welfare payments, and that those
who live well as a result of their own labor, effort, and forecasting
skills are immoral and owe the bulk of their wealth to the poor, we
must recognize the source of these teachings: the pits of hell. This
is Satan’s counter-philosophy, which is expressly intended to
thwart godly men in their efforts to subdue the earth to the glory
of God.

This radically anti-Biblical philosophy is not simply a matter
of intellectual error; it is a conscious philosophy of destruction, a sys~
tematically anti-Biblical framework which is calculated to under-
cut successful Christians by means of false guilt and paralysis. That
such teachings are popular among Christian intellectuals in the
latter years of the twentieth century only testifies to their abysmal
ignorance— indeed, their judicial blindness (Matthew 3:14-15)—
concerning Biblical ethics and economic theory. Christians have
adopted the politics of envy from the secular humanists, especially
in college and seminary classrooms. We live in an age of guilt-
manipulators, and some of them use Scripture to their evil ends,
