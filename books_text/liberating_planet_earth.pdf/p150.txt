142 Liberating Planet Earth

tians have for too long seen themselves as the dogs sitting beneath
the humanists’ tables, hoping for an occasional scrap of unenriched
white bread to fall their way. They worry about their own com-
petence. They think of themselves as second-class citizens, And
the humanists, having spotted this self-imposed “second-class citi-
zen” mentality, have taken advantage of it.

The Five Doctrines for Dominion on Earth

Believers have for over a century retreated into antinomian
pietism and pessimism. This retreat began in the 1870's. They
have lost the vision of victory which once motivated Christians to
evangelize and then take over the Roman Empire. They have
abandoned faith in one or more of the five features of Christian
social philosophy that make progress possible: (1) the absolute
sovereignty of the Creator God; (2) God’s covenant that governs
all men; (3) the tool of the covenant, Biblical law; (4) Biblical pre-
suppositionalism—the self-attesting truth of an infallible Bible,
which is the ultimate judge of everything; and (5) the dynamic of
eschatological optimism. We should conclude, then, that either the
dissolution of modern humanist culture is at hand, or else the re-
generate must regain sight of their lost theological heritage: do-
minion optimism and Biblical law.

The Communists have a perverted version of all five points,
This is why they are such powerful rivals to Christians. First, they
believe in the sovereignty of man, as manifested in our day by the
Communist Party, the “vanguard of the proletariat,” which is in-
fallible. Second, they believe in a covenant: membership in the
Communist Party, which is rigorously hierarchical. Third, they be-
lieve that socialist law, socialist institutions, and socialist every-
thing are the product of a unique philosophy. They believe in
their exclusive way of accomplishing things. Fourth, they believe
in the providence of the impersonal forces of dialectical history,
which their leaders alone understand perfectly in any historical
period. The Party executes infallible judgments because it has ac-
cess to their “holy writ”: Marxism-Leninism. They do not appeal
to any other logic, any other source of authority except their own
