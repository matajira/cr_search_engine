60 Liberating Planet Earth

5. Inkeritance/Continuity (survival)

Satan wants to build up his kingdom. To do this, he must
build his power year after year, generation after generation. But
he cannot do this. God always cuts short the kingdoms of evil.

For I, the Lorp your God, am a jealous God, visiting the ini-
quity of the fathers on the children to the third and fourth genera-
dion of those who hate Me, but showing mercy to thousands [of
generations—G.N.], to those who love Me and keep My com-
mandments (Exodus 20:5-6).

Thus, Satan must speed up the process. He must take huge
risks, the way that gamblers and debtors do. He must do in a few
years what the steady preaching of the gospel can accomplish in
centuries. Thus, Satan’s kingdoms always collapse, whereas
Christian culture continues to expand its influence.

Satan can offer his followers no long-term inheritance for their
children. God can offer His followers a long-term inheritance for
their children. God’s people can believe in the long-term future;
Satan’s followers cannot. God’s people become future-oriented;
Satan’s people can do so only when heavily influenced by the
worldview of Christianity.

Summary

Dominion is by covenant. So is power. Satan’s covenant is a
poor imitation of God’s. He must accomplish through violent rev-
olution, theft, torture, and high risks what God’s people can ac-
complish through steady hard work, thrift, faithfulness, honesty,
reliability, and prayer.

The kingdom of God is very different from the kingdom of
Marx. Wherever Communists take over, they impose their cove-
nant of power. They imitate God by trying to speed up the pro-
cesses of history. They work fast, for Satan’s time is short (Revela-
tion 12:12). They are doomed in eternity and doomed in history,
but they continue to struggle against the limits of God’s creation.

That in our day they have been forced to adopt the language
of the Bible in order to breathe new hope and new vitality into
