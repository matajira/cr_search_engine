The Liberation of the Church 91

are really like. I will deliver you into their jurisdiction for a few
years.”

There are those today within the church who tell us that Old
Testament laws are inherently tyrannical. They tell us that the
church can live under any system of political law in the world,
and still survive. But one system of law is supposedly forever for-
bidden: Biblical law. That would be intolerable. That would
mean that Christians were exercising dominion. ‘The power relig-
ionists do not want to hear that; neither do the escape religionists.

What we learn in the Old Testament is just the opposite:
tyranny was the product of every rival law system in the ancient
world except Old Testament law. The God of the Bible is the God
of liberation. His law therefore produces liberation. Yet the critics
of the Old Testament law system claim that civil rule in terms of
Old Testament law would produce tyranny.

‘We can see just how effective the humanists have been in per-
suading Christians to give up their heritage of liberty for a mess of
bureaucratic pottage.

So whenever the church begins to declare God’s holy stand-
ards of civil rule, the state is outraged. “How dare you! It is your
job to keep the people quiet,” says the present ruler. “It is not your
job to speak out on political questions. They are of no concern to
the church.”

The revolutionaries are equally outraged. “It is your job to
preach revolution, not reform,” says the Marxist liberation theo-
logian. “It is not your job to preach peaceful change, the recon-
struction of society by the preaching of the gospel, and the decapi-
talization of the state. No, the goal is to capture the state,
strengthen it, and make it even more powerful.”

The escapists are also outraged. “Look, we come to church to
have our spirits soothed. You keep bringing up unpleasant topics.
There is nothing we can do about any of the world’s problems out-
side the four walls of the sanctuary. Preach Jesus, and Him
crucified —and be sure to leave Him hanging on the cross, where
He belongs.” ,

The preaching of the full-scale gospel scares those who believe
