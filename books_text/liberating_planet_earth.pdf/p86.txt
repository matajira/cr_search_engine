78 Liberating Planet Earth

estate (Deuteronomy 21:15-17). Why does the eldest son inherit
this double portion? A reasonable explanation is that he is the per-
son with the primary responsibility for the care of his parents.

The English system of primogeniture, in which the eldest son
inherited all of the landed estate, was clearly unbiblical, and the
breakdown of that system in the nineteenth century was a step
forward. Such a system places too much responsibility on the eld-
est son, leaving the other children bereft of capital, but also psy-
chologically free of economic obligations toward the parents. It
cuts off most of the children from the mutual obligations of the
covenantal family.

Economic obligations should flow in both directions: toward
the children in the early years, toward the parents in the later
years, and back toward the children at the death of the parents,
when the family’s capital is inherited by the survivors. In short,
children inherit, but parents must first be provided for.

The Continuity of Capital

The Biblical law-order is a unity. Blessings and responsibilities
are linked. Without the coherence of comprehensive Biblical law,
blessings can become curses. We have a good example of this in
this commandment. Assume that a son honors his parents during
their lifetime. He receives the blessing of long life. At the same
time, he is not careful to teach his own children the requirements
of this commandment. He wastes his own estate, neglecting the
spiritual education of his children. He has nothing to live on in his
old age. His fortune is gone, and his own children know it. The
break in the family between generations is now a threat to him.
Knowing that he has abandoned them by squandering the family
estate, his children abandon him to poverty in his old age, when
he most needs assistance. The blessing of long life then becomes a
curse to him. He slowly rots away in abject poverty.

Capital, if familistic in nature, is less likely to be squandered.
In a truly godly social order, the familiar rags-to-riches-to-rags
progression of three generations, from grandfather to grandchil-
dren, is not supposed to become typical, despite the fact that the
