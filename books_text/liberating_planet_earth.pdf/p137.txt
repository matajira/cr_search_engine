10

THE INEVITABILITY OF LIBERATION

And you shall remember the Lorp your God: for it is He who
gives you power to get wealth, that He may establish His covenant
which He swore to your fathers, as it is this day (Deuteronomy 8:18).

This verse is crucial to understanding the relationship between
Biblical law and Christian progress over time. God grants gifts to
covenantally faithful societies. These gifts are given by God in
order to reinforce men’s confidence in the trustworthiness of His
covenant, and so lead them to even greater faithfulness, which in
turn leads to additional blessings. Visible blessings are to serve as
confirmations of the covenant. God therefore gives men health and
wealth “that He may establish His covenant.” When men respond
in faith and obedience, a system of visible positive feedback is created.

Biblical history is linear. It has a beginning (creation), mean-
ing (sin and redemption), and an end (final judgment). It was
Augustine’s emphasis on linear history over pagan cyclical history
that transformed the historical thinking of the West.* But the Bib-
lical view of history is more than linear. It is progressive. It involves
visible cultural expansion. It is this faith in cultural progress
which has been unique to modern Western civilization. This opti-
mistic outlook was secularized by seventeenth-century Enlight-
enment thinkers,? and by the Communists, and its waning in the

1. Charles Norris Cochrane, Christianity and Classical Culture: A Study in Thought
and Action from Augustus to Augustine (New York: Oxford University Press, [1944]
1957), pp. 480-83.

2. Robert A. Nisbet, “The Year 2000 and AJl That,” Commentary { June 1968),

3. FN, Lee, Communist Eschatology (Nutley, New Jersey: Craig Press, 1974).

129
