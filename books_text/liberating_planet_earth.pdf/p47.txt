3

THE ENEMIES OF LIBERATION

“No man can serve two masters; for either he will hate the one
and love the other, or else he will be loyal to the one and despise the
other. You cannot serve God and mammon” (Matthew 6:24).

Bondage is an inescapable concept. It is never a question of
“bondage vs. no bondage.” It is always a question of bondage to whom.

Jesus warned against serving mammon, What was mammon?
Was it money? Yes. Was it power? Yes. Was it anything in the
heart of man that man raises up above God? Yes. Jesus was sim-
ply repeating the challenge that the prophet Elijah had made to
the people of Israel almost 800 years earlier.

And Elijah came to all the people, and said, “How long will you
falter between two opinions? If the Lord is God, follow Him; but if
Baal, then follow him.” But the people answered him not a word
(1 Kinga 18:21).

The people were trying to “play it safe.” They were unwilling
to choose God on the basis of God’s word, or even on the basis of
all the miracles God had shown them when He delivered their
ancestors from Egypt 700 years earlier. No, they wanted to see
which sacrificial animals the fire would consume, Elijah’s or the
false prophets’, They wanted a sign from God. They worshipped
power, so they wanted a sign of power.

Worshipping Power

That is the same challenge today: worship God or worship
power. Not that God doesn’t have power. He has total power. But
He wants men to worship Him because He is righteous, not sim-

39
