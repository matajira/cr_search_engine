The Liberation of the State i

other man’s labor, as if those fruits were unrelated to that man’s
personal responsibility before God as a steward.

Totalitarianism vs. the Tithe

Totalitarian societies develop from the attempt of socialist
planners to mold the economy into a centrally directed frame-
work, Nothing must deviate from the central economic plan,
since human freedom will thwart any such plan. Thus, the power
to redistribute wealth in accordance with some preconceived stat-
ist program eventually destroys human freedom and therefore
thwarts responsibility to act as a steward under God. Covetousness,
when legislated, becomes a major foundation of totalitarianism.

The civil government is to be restrained by Biblical law. The
warning of Samuel against the establishment of a human kingship
stands as a classic statement of what earthly kingdoms involve.
The king will draft sons to serve in his armed forces. He will con-
script daughters to serve as cooks and confectioners. He will con-
fiscate the best agricultural land. He will impose a tithe on the
flocks. In short, the king will collect a tithe for himself (1 Samuel
8:11-19), The Hebrew state, Samuel promised, will be such a bur-
den on them that they will cry out to God to deliver them, but He
will not do it (v. 18). By denying God and His law-order, the
Hebrews placed themselves under the sovereignty of man, and
this sovereignty was centralized in the civil government. It is an
ungodly state which demands tax payments as large as ten per-
cent, God’s tithe. How much worse is a state that requires more
than God's tithe. Such a state has elevated itself to the position of
a god. It is a false god. It is demonic.

Social Cooperation

When men do not trust their neighbors, it becomes expensive
for them to cooperate in projects that would otherwise be mutually
beneficial to them. They hesitate to share their goals, feelings,
and economic expectations with each other. After all, if a man is
known to be economically successful in a covetous society, he
faces the threat of theft, either by individuals or bureaucrats. He
