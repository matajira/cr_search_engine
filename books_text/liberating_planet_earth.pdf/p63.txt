The Covenant of Liberation 55

Creator and Sustainer. Men are to make their own mistakes and
successes. Each man is to work out his salvation (or damnation) in
fear and trembling (Philippians 2:12). Other men are to sit in
judgment over him only when he commits public evil. They are
not to command him as imitation gods. They are not to issue
comprehensive commands and monitor him constantly. That is
God's job, not man’s.

Thus, God’s hierarchy produces social freedom. It relieves
mankind from any pretended autonomy from God’s total sover-
eignty. Men are not to seek to create predestinating hierarchies.
They can leave their fellow men alone, so long as God’s institu-
tional laws are obeyed in public,

3. Law/Dominion (stipulations)

The third aspect of the covenant is its e¢Aical quality. The terms
of submission are ethical. The union between covenant-keepers
and their God is an ethical union, The disunion between cove-
nant-breakers and God is equally ethical: they are rebels against
His law. Adams fall into sin did not take place because he lacked
some essence, some aspect of “being.” He was created perfect. He
fell willfully. He knew exactly what he was doing. “Adam was not
deceived,” Paul writes (1 Timothy 2:14a).

This emphasis on ethics separates Biblical religion from pagan
religion, Man is supposed to exercise dominion, but not autono-
mous power, He is not to seek power through magic, or through
any attempted manipulation of God or the universe. Dominion is
based on adherence to the law of God—by Christ, perfectly and
definitively, and by men, subordinately and progressively. Thus,
ethics is set in opposition to magic.

4. Judgment/Oath (sanctions)

The fourth aspect of the covenant is its judicial character. The
essence of maturity is man’s ability to render God-honoring judg-
ment. God renders definitive judgment; man is to render analo-
gous judgment~ judging events as God's creatures, yet always
with God’s standards (laws) in mind.
