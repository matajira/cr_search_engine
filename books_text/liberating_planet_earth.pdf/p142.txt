134 Luberating Planet Earth

This means that with an increase in consistent living, the ethi-
cal aspects of the separation between the saved and the lost be-
come more and more fundamental. Unbelievers recognize more
and more how much they hate God and how different they are
from Christians, but the increasing self-understanding on the part
of the power-seeking unbeliever does not lead him to apply Satan’s
philosophy of ultimate meaninglessness and chaos; it leads him
instead to apply Satan’s counterfeit of dominion religion, the re-
ligion of power.

The unbeliever can achieve power only by refusing to become
fully consistent with Satan’s religion of chaos. He needs organiza-
tion and capital — God’s gifts of life, knowledge, law, and time—in
order to produce maximum destruction. Like the Soviet Union,
which has always had to import or steal the bulk of its technology
from the West in order to build up an arsenal to destroy the West,>
so does the Satanist have to import Christian intellectual and
moral capital in order to wage an effective campaign against the
church.

First, the Christian exercises dominion by becoming more
consistent with the Christian faith that he holds, meaning morally
and logically consistent with the new man within him, and there-
fore by adhering ever more closely to God’s law. Biblical law is the
covenant-keeper’s fully self-consistent tool of dominion.

Second, the covenant-breaker exercises power by becoming
inconsistent with his ultimate philosophy of randomness. He can
commit effective crimes only by stealing the worldview of Christians.
The bigger the crimes he wishes to commit (the ethical impulse of
evil), the more carefully he must plan (the moral impulse of right-
eousness: counting the costs [Luke 14:28-30]). The Christian can
work to fulfill the dominion covenant through a life of consistent

5, Antony Sutton, The Best Enemy Money Can Buy (Billings, Montana: Liberty
House, 1986). On the technological dependence of the Soviet Union on commer-
cial Western imports, see also Sutton, Western Technology and Soviet Economic Devel-
opment, 3 Volumes (Stanford, California: Hoover Institution Press, 1968-73);
Charles Levinson, Vodka Cola (London: Gordon & Cremonesi, 1978); Joseph
Finder, Red Carpet (New York: Holt, Rinehart & Winston, 1983).
