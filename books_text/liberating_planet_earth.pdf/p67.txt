The Covenant of Liberation 59

all law cannot exercise power. Try to run an army or a revolution
without a chain of command, it is impossible. But a chain of com-
mand requires law of some kind. For example, Stalin in the late
1930's purged the top officers of the Red Army. This weakened the
army so badly that Hitler’s forces very nearly defeated the Soviet
Army in the second half of 1941.

Satan acknowledges no fixed principles, for fixed principles
point to God. Satan is the original evolutionist. He has always
sought power unrestricted by law. All humanist systems preach
relativism. There are no fixed moral laws, or any other laws. Law
changes with circumstances, the humanist says.

What Satan offers in place of law is magic. His people try to
gain power, not by obedience to God’s ethical laws, but rather by
magical manipulation or political tyranny. They use formulas, in-
cantations, or other rituals. They manipulate people by adopting
demonic symbols. They reject the Biblical covenant’s third point,
permanent ethics.

4. Judgment/Oath (sanctions)
The Bible says that the church will judge the angels (1 Corin-

thians 6:3). Satan resents this. Thus, he seeks to impose earthly
judgment before the final judgment. He seeks to imitate God.
God rewards some and sends others to hell, Satan imitates this.

Satanic governments invariably impose torture. They set up
large numbers of prison camps, “re-education centers,” and other
institutions of terror, Satan creates a close imitation of hell on
earth, not simply to increase his power, but because he wishes to
imitate God. But he cannot match God. He can kill only the body,
not the soul. He wants all men to fear this bodily death above all.
This is the opposite of the kingdom of Christ. Jesus said:

Whatever I tell you in the dark, speak in the light; and what
you hear in the ear, preach on the housetops. And do not fear those
who kill the body but cannot kill the soul. But rather fear Him who
is able to destroy both soul and body in hell (Matthew 10:27-28).
