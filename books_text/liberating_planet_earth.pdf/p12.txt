4 Liberating Planet Earth

doned his belief in God. In an essay he wrote in 1843, at age 25,
Marx said: “Religion is the sigh of the oppressed creature, the
sentiment of a heartless world, and the soul of soulless conditions.
It is the opium of the people”? In that same essay, he argued for
humanism, the idea that mankind is the highest form of being— in
other words, that man is god. “The criticism of religion ends with
the doctrine that man is the supreme being for man.”§ Again, “The
emancipation of Germany is only possible in practice if one adopts
the point of view of that theory according to which man is the
highest being for man.”4 (The emphases were his; I have added
nothing.)

We now know what Marx was: an atheistic humanist. What
about Jesus? What did Jesus say about Himself? At His trial be-
fore the Jewish leaders, the high priest asked Him: “Are you the
Christ, the Son of the Blessed?” The Jews did not mention the
name of God; they used such words as “Blessed” as substitutes.
Jesus knew what He was being asked: “Are you the Son of God?”
He replied:

“I am. And you will see the Son of Man sitting at the right
hand of Power, and coming with the clouds of heaven.” Then the
high priest tore his clothes and said, “What further need do we
have of witnesses? You have heard the blasphemy! What do you
think?” And they all condemned Him to be worthy of death (Mark
14:61-64).

Jesus’ words were blasphemy, unless He really was the Son of
God, By Hebrew law, He was worthy of death (Leviticus 24:16),
unless He really was the Son of God. By His resurrection from the
dead (Matthew 28) and His ascension to heaven (Acts 1:9-11) to
stand at the right hand of God (Acts 7:56), He proved that He
was what He said He was. He was God walking on earth. He said
plainly, “I and My Father are one” (John 10:30).

2. Karl Marx, “Contribution to the Critique of Hegel's Philosophy of Right,”
in T. B. Bottomore (ed.), Karl Marx: Early Writings (New York: McGraw-Hill,
1964), pp. 43-44.

3, Ibid. p. 52.

4, ibid, p. 59.
