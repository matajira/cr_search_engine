108 Liberating Planet Earth

lic. If men refuse to submit to regulations that cannot be enforced,
one by one, by the legal system, then that system will be de-
stroyed. Court-jamming will paralyze it. This is a familiar phe-
nomenon in the United States in the final decades of the twentieth
century.

It is possible to bring down any legal system simply by taking
advantage of every legal avenue of delay. Any administrative sys-
tem has procedural rules; by following these rules so closely that
action on the part of the authorities becomes hopelessly bogged
down in red tape (procedural details), the protestors can paralyze
the system. Too many laws can produce lawlessness. The courts
can no longer enforce their will on the citizens. At the same time,
administrative agencies-can destroy individual citizens, knowing
that citizens must wait too long to receive justice in the courts.
The result is a combination of anarchy and tyranny: the antinom-
ian legacy.

Recognizing Our Limitations

What we can and should strive for is to conform our human
law codes to the explicit requirements of the Ten Commandments
and the case-law applications of Biblical law. The answer to our
legal crisis is not to be found in the hypothetical perfection of for-
mal law, nor can it be found in the hypothetical perfection of sub-
stantive (ethical) justice. Judges will make errors, but these errors
can be minimized by placing them within the framework of Bibli-
cal law. Before God gave the nation of Israel a comprehensive sys-
tem of law, Jethro gave Israel a comprehensive system of decen-
tralized courts. By admitting the impossibility of the goal for
earthly perfect justice, Moses made possible the reign of imper-
fectly applied revealed law: perfect in principle, but inevitably
flawed in application. The messianic goal of a perfect law-order,
in time and on earth, was denied to Moses and his successors.

One of the most obvious failures of the modern administrative
civil government system is its quest for perfect justice and perfect
control over the details of economic life. The implicit assertion of
omniscience on the part of the central planners is economically
