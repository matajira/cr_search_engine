82 Liberating Planet Earth

must adopt the mentality of the gambler. He has to “go for the big
pay-off.” He must sacrifice everything for capital expansion, risk-
ing everything he has, plus vast quantities of borrowed money, on
untried, high-risk, high-return ventures. He must abandon
everything conventional, for an investor earns only conventional
returns (prevailing interest rate) from conventional ventures. The
man’s world becomes an endless series of all-or-nothing decisions.

Trusteeship: Which Family?

The continuity of capital is obviously threatened by the rise of
the familistic state. It establishes itself as the trustee for all men,
from womb to tomb, It is therefore entitled to support by those
who receive its protection. Like a father, or better yet, like a dis-
tant uncle who guides the fortunes of an orphaned nephew, the
state must administer the funds, always taking a large portion of
those funds as a necessary fee for service performed.

As men steadily begin to perceive the implications of the fam-
ilistic state, they seek to hide their assets from its tax collectors.
Men try to find ways to pass along wealth to their heirs, and the
state relentlessly searches for ways of closing off escape hatches.
The new “parent” must not be deprived of its support from every
member of the family. And once the capital is collected, it is dis-
sipated in a wave of corruption, mismanagement, bureaucratic
salaries, and politically motivated compulsory charity programs.
Men see the erosion of their capital, and they seek to hide it away.
They recognize what the pseudo-family of the state will do to the
inheritance of their children. Still, because of entrenched envy,
they do not turn back, They and their parents and grandparents
accepted the philosophical justifications of “soaking the rich” by
means of the ballot box, but now that price inflation has pushed
everyone into higher tax brackets, they are horrified by what they
find. They have now been snared themselves, but they seem un-
able to turn back, for to turn back would involve an admission of
the immorality and inefficiency of the “soak the rich” programs of
twentieth-century democratic politics.

The modern messianic state would like to make permanent
