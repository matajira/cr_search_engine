156 Liberating Planet Earth

government and personal liberty. It offers an excuse to evil people
for their continued wrong-doing. The sinner says, in effect: “'m
not depraved; Pm deprived.”

Such a view of man transfers authority to elitist scientific plan-
ners who supposedly have escaped from the impersonal determin-
ing influences of man’s environment. They alone are said to be
able to reshape social institutions in order to reduce sin by creat-
ing a better environment. Understand: to have escaped from the
determining environment is to have become trans-human, yet
this ability to transcend the environment must be asserted in prin-
ciple if the central planners are to be freed from the chains of some
sort of depersonalized predestinating process. It is the old lure of
Satan: “You shall be as God” (Genesis 3:5). It is a system of the div-
ination of the elite. It is a system of bondage from the top down. Yet
it is promoted by liberation theology in the name of democracy.

Sovereign God, Responsible People

In contrast, Christianity affirms that it is God who controls all
of history, not the impersonal evolutionary forces of nature itself,
It is God who regenerates men, not the state. It is not man’s im-
personal god who holds each person responsible for everything he
thinks, says, and does.

Because man’s environment is personal and providentially
sustained by God, men possess the opportunity to exercise decen-
tralized dominion over nature, since mankind is created in the
image of God. Without this image of God in man, mankind
could not maintain control over very much of the environment. A
person’s mind could not be assumed to correspond with his external
environment. Humanists whe assume that a man’s mind does pos-
sess this power, especially by means of mathematics and scientific
experimentation, cannot explain the origin of this unreasonable
connection except by appealing to either miracles or randomness—
a Darwinian randomness that somehow has produced cosmic order.

1. Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for
Christian Economics, 1982), Appendix A; “From Cosmic Purposelessness to Hu-
manistic Sovereignty.”
