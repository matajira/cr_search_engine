The Liberation of the State 105

pandemoniac. For example, in criminal prosecutions we find as
long as five months spent in the selection of a jury; the same
murder charge tried five different times; the same issues of search
and seizure reviewed over and over again, conceivably as many as
twenty-six different times; prosecutions pending a decade or more;
accusations routinely sidestepped by an accused who makes the
legal machinery the target instead of his own conduct (p. 3).

Where have modern secular humanistic courts failed? Flem-
ing cites Lord Macauley’s rule: the government that attempts
more than it ought ends up doing less that it should. Human law
has its limits. Human courts have their limits.

The law cannot be both infinitely just and infinitely merciful;
nor can it achieve both perfect form and perfect substance. These
limitations were well understood in the past. But today’s dominant
legal theorists, impatient with selective goals, with limited objec-
tives, and with human fallibility, have embarked on a quest for
perfection in all aspects of the social order, and, in particular, per-
fection in legal procedure (p. 4).

The requirements of legal perfection, Fleming says, involve
the following hypothetical conditions: totally impartial and com-
petent tribunals, unlimited time for the defense, total factuality,
total familiarity with the law, the abolition of procedural error,
and the denial of the use of disreputable informants, despite the
fact, as he notes, that “the strongest protection against organized
thievery lies in the fact that thieves sell each other out” (p. 5). The
defenders of costless justice have adopted the slogan, “Better to
free a hundred guilty men than to convict a single innocent man.”
But what about the costs to future victims of the hundred guilty
men? The legal perfectionists refuse to count the costs of their
hypothetical universe (p. 6).

The whole system procrastinates: judges, defense lawyers,
prosecutors, appeals courts, even the stenographic corps (p. 71).
Speedy justice is no longer a reality. Prisoners appeal constantly
to federal courts on the basis of Aabeas corpus: illegal detention
because of an unconstitutional act on the part of someone, any-
one. In 1940, 89 prisoners convicted in state courts made such an
