130 Liberating Planet Earth

twentieth century threatens the survival of Western humanistic
civilization.*

Dominion Theology vs. Pessimism

Victory in history is an inescapable concept. There can be no
question of victory, either of covenant-keepers or covenant-
breakers. The only question is; Who will win? If covenant-
breakers rebel against Biblical law, and they become externally
consistent with their own anti-God and anti-Biblical law presup-
positions, then they will become historically impotent,

There is no neutrality anywhere in the universe. But since
there is no intellectual and moral neutrality, then there can be no
cultural, civic, or any other kind of public institutional neutrality.
So which kind of worldview produces productive people? The
liberation offered by Jesus Christ or the liberation offered by Karl
Marx? Which offers positive blessings from the hand of God?
Which will produce judgment from God?

Some Christians argue that it is the reprobate who will be
nearly victorious in history, not Christians. Only at the end of
time do the covenant-breakers have to face the fact of their defeat,
when God brings His final judgment.

Consider what this means, It means that Christianity does not
work. Here is what the pessimists are saying:

“As Christians work out their own salvation with fear and trem-
bling (Philippians 2:12), improving their creeds, improving their
cooperation with each other on the basis of agreement about the
creeds, as they learn about the law of God as it applies in their own
era, as they become skilled in applying the law of God that they
have learned about, they become culturally impotent. They be-
come infertile, also, it would seem. They do not become fruitful
and multiply. Or if they do their best to follow this commandment,
they are left without the blessing of God—a blessing which He has
promised to those who follow the laws He has established. In short,

4. Robert Nisbet, History of the Iden of Progress (New York: Basic Books, 1980),
ch. 9 and Epilogue.
