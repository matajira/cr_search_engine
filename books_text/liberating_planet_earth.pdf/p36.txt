28

most momentous public decision a person or a civilization can

Liberating Planet Earth

make is to choose which army to join. We do not prove our libera-
tion simply by intellect; we prove it covenantally: by a public affir-

mation to come under the discipline of the Supreme Allied Com-
mander, Jesus Christ, the Liberator. He is the true anchor. His

chain to that personal anchor is our link to stability and eternal
peace. His chain is His law.

The reasons why it is Christ, and Christ alone, who is the true

liberator of the earth are these:

1.

All power has been delivered by God to His Son, Jesus

Christ.

2.
Him.
3.
4.

Christ calls men to freedom through ethical bondage to

The road to liberation is a moral battlefield: the heart.
There is no permanent peace treaty between the com-

manders.

5.
6.
7.
8.
9.
10.
il.
12.
13.
14.
15.
16.

There is no possible peace treaty between the armies.
Cease-fires are not peace treaties.

The battle is between rival law systems.

There is no moral or legal neutrality.

God, the Creator, owns the earth.

God delegated this ownership ta Adam.

Adam “sold his birthright” to Satan.

Christ reclaimed it at Calvary.

Christ delegates ownership to His people.

His people are to win back the earth through obedience.
Modern Christians have been retreating from responsibility.
To get back in the battle for the earth, Christians must re-

affirm the original covenant with God through Christ.
