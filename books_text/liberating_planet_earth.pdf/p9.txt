INTRODUCTION

Then Jesus said to those Jews who believed on Him, “If you
abide in My word, you are My disciples indeed. And you shall
know the truth, and the truth shall make you free” { John 8:31-32).

I originally wrote this book as an evangelical tool to be used
primarily by Spanish-speaking Christians in their struggles
against atheism, Communism, and the popular socialist religion
known as liberation theology. Nevertheless, this book is more
than an anti-Communist tract. We must be more than anti-
Communists. We must be able to offer a comprehensive, work-
able alternative to Communism. Nothing less than this will be
successful. Marxism is the most consistent and powerful secular
religion of all time; it can only be successfully challenged by an
even more consistent and more powerful Biblical religion.

I realized that the book could also serve English-speaking peo-
ple as an introduction to Christianity—not the traditional “one
hour and three prayers per week” sort of Christianity, but the
Christianity of the Bible. This Christianity presents a comprehensive
challenge to the modern world, and it also offers comprehensive solu-
tions to the complex problems of our day.

Christianity has not survived for almost two thousand years be-
cause it is culturally irrelevant. It captured and then transformed the
dying Roman Empire in the fourth century. It laid the foundations
of modern science during the late medieval period (1000-1500),
and developed it in the early modern period (1500-1700). Kings
governed in the name of Christianity, and others were overthrown
in the name of Christianity. It is proper to speak of Christian civi-
lization, but for well over a century, such language has seemed

1
