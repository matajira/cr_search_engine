5
THE LIBERATION OF THE INDIVIDUAL

‘Jesus answered them [the Jews], “Most assuredly, I say to you,
whoever commits sin is a slave of sin. And a slave does not abide in
the house forever, but a son abides forever. Therefore if the Son
makes you free, you shall be free indeed” ( John 8:34-36).

Adam was a son of God. He forfeited his sonship when he
placed himself ethically under a new ruler, Satan. Christ, the sec-
ond Adam, came to earth in order to restore this forfeited sonship
to His people. It is the Son of God who makes men free. He
brings them to God the Father,

Jesus said to him, “I am the way, the truth, and the life. No one
comes io the Father except through Me” (John 14:6).

God in His grace adopts people to be part of His ethical, regener-
ate family (John 1:12). They are no longer disinherited sons. There is
no doubt that there is a universal Fatherhood of God, and a uni-
versal brotherhood of man. Men are all brothers—just like Cain
and Abel. They are envy-filled, hateful brothers. They are all cre-
ated as men, in the image of God.

And He has made from one blood every nation of men to dwell
on all the earth, and He has determined their preappointed times
and the boundaries of their habitation, so that they should seek the
Lord, in the hope that they might grope for Him and find Him,
though He is not far from each one of us; for in Him we live and
move and have our being . . . (Acts 17:26-28a).

The liberal theologians and humanists have drawn deadly
conclusions concerning the universal Fatherhood of God and the

62
