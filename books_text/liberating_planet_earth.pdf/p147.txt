The Inevitability of Liberation 139

of cultural reconstruction. It must be seen as operating now, in
New Testament times, It must be seen that there is a relationship
between covenantal faithfulness and obedience to Biblical law—
that without obedience there is no faithfulness, no matter how
emotional believers may become, or how sweet the gospel tastes
(for a while). Furthermore, there are external blessings that follow
covenantal obedience to God’s law-order.

How Can Christians Lose?

Christians overcome the world in the same way that they over-
come sin in their own lives: by obeying God. We do not become
less sinful by imitating the world of sin.

And de not be conformed to this world, but be transformed by
the renewing of your mind, that you may prove what is the good
and acceptable and perfect will of God (Romans 12:2).

Imitate me, even as I also imitate Christ (1 Corinthians 11:1).

Neither do we become more powerful by imitating the hu-
manist’s power religion. The Christian is called to ethical seif-
consciousness, Out of this comes increasing self-understanding.
Ethics is the fundamental issue, not philosophical knowledge, and
not political or military power,

The increase in the ethical understanding of Christians results
in their increasing understanding of the Bible’s principles of
knowledge. Christians think God’s thoughts after Him, as crea-
tures made in His image.

For though we walk in the flesh, we do not war according to the
flesh. For the weapons of our warfare are not carnal, but mighty in
God for pulling down strongholds, casting down arguments and
every high thing that exalts itself against the knowledge of God,
bringing every thought into captivity to the obedience of Christ (2
Corinthians 10:3-5).

The issue is ebedience, not philosophical rigor. Obedience in the
long run is what brings the church increasing wisdom and in-
creasing philosophical rigor.
