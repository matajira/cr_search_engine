46 Liberating Planet Earth

by their withdrawal from politics and culture. These two groups
have also been pacifists. Today’s Christian escapists (pietistic fun-
damentalists) want only to defer the power vs. escape confronta-
tion, until Jesus comes back again and solves society’s problems
by means of His power. This theology of deferred social responsi-
bility has become visibly bankrupt in the 1980's.

Miranda’s Challenge

‘The Marxist liberation theologians recognize this escapist im-
pulse in popular Christianity. They ridicule Christianity by argu-
ing that this escape religion is the essence of Christianity, the only
alternative to revolutionary socialism. This is a false choice. The
Marxists ignore dominion theology. They pretend that this alter-
native does not exist and has never existed. But it does exist.

The Marxist liberation theologian José Miranda preaches
common ownership of all goods. He challenges Christians who
defend the present non-Communist social order. They are all
escapists, he says. They are all defenders of a socially irrelevant
and historically impotent faith. He is self-conscious about the in-
effectiveness of escapist Christianity:

Now, the Matthean expression “the kingdom of the heavens”
was the only one serving the escapist theologians as pretext for
maintaining that the kingdom was to be realized in the other
world. Not even texts about glory or entering into glory provided
them any support, for the Psalms explicitly teach, “Salvation sur-
rounds those who fear him, so that the glory will dwell in our land”
(Psalm 85:10).?

Hence what paradise might be, or being with Christ, or Abra-
ham’s bosom, or the heavenly treasure, is a question we could well
leave aside, because what matters to us is the definitive kingdom,
which constitutes the central content of the message of Jesus. The
escapists can have paradise.?

2. José Miranda, Communism in the Bible (Maryknoll, New York: Orbis Books,
1982), p. 14.
3. ibid, p. 15.
