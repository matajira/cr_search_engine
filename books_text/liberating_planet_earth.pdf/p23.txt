Introduction 15

nantal faithfulness to the God who liberates the righteous, and
who will at the day of judgment condemn the unrighteous to
spend eternity in a fiery prison. Better to spend time in earthly
prisons than in the eternal one. There can be liberation from
earthly prisons, for the prison experience has always been a
prelude to periods of great dominion for the righteous. There can
be no liberation from the eternal prison, and no dominion.
