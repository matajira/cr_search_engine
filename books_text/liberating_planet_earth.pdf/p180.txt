172 Liberating Planet Earth

civil, 107
covenant &, 55, 119
disrespect for, 109
dominion &, 10, 55, 92
family, 24
God's vs. Satan’s, 21
Holy Spirit & 136-37
Jesus &, 18, 150
lever, 132-33
liberation &, 35-36
liberty &, 150
life &, 35
love &, 18
natural, 21, 149
Old Testament, 34, 91, 99
peace &, 113
perfection, 104-6
prosperity &, 141
self-government, 113
teaching, 94
tool of dominion, 13t, 134, 138-39
Satan’s, 103

Levellers, 122

Har, 18

liberation
from Satan, 26-27
from sin, 3, 8
God's sovereignty, 33
Jesus &, 149
law &, 37
movements, 17
promised, 27
yoke &, 17

liberation theology
Biblical jaw &, 35-36
Christianity vs., 154
economy &, 116
exodus, 30
kingdom of God, 45-46
Latin America, 1, 45-46
Marxist, 5-6, 64-65
true, 14

liberator, 14, 19, 28

liberty, 149, 130
life, 19, 75
lifespan, 75
love, 17-18
lust, 20

man (autonomous), 19, 53
Mao, 140
marriage, 121
Marx, Karl, 3-4, 19, 36, 65-66, 123
Marxism
atheism, 3-4, 29
Christianity vs., 5, 29
charch &, 90
class struggle, 114
control, 25
corruption &, 6
dead religion, 6
dialectical history, 53
dialogue, 7
Egypt, 152
elite, 13
false liberation, 37, 53
Miranda, 46-47
murdering colleagues, 64-65
progress, {14
theft, 154
masochism, 105
material forces, 53, 66
Medo-Persia, 14
meek, 146
mercy, 12
Mexico, 90
Micaiah, 90
might, 79
mind, 21
mine, 124-25
Miranda, José, 46-47, 67-68
missions, 12, 13
Moloch state, 125-26
Moses, 13, 100, 101, 103, 108
Mount Carmel, 90
Mt. Sinai, 29
