The Inevitability of Liberation 137

obey the law.® The Spirit’s empowering is a fundamental distinc-
tion between the two covenantal periods. But this greater empow-
ering by the Spirit must be made manifest in history if it is to be
distinguished from the repeated failure of believers in the Old
Covenant era to stay in the “positive feedback” mode: blessings
... greater faith . . . greater blessings, etc. It is this positive
feedback aspect of Biblical law in New Testament times which
links Biblical law with optimism toward the future (dominion the-
ology).

Does the great power of the Holy Spirit really mean anything
in history? If we were to argue that the greater empowering of the
Holy Spirit in the New Testament era is only a kind of theoretical
backdrop to history, and therefore Biblical law will not actually be
preached and obeyed in this pre-final-judgment age, then we
would really be abandoning the whole idea of the Holy Spirit's
empowering of Christians and Christian society in history. Yet
people argue this way: “Yes, the Spirit empowers Christians to
obey Biblical law; however, they will not adopt or obey Biblical
law in history.”

Will the progressive manifestation of the fruits of obeying Bib-
lical law also be strictly internal and not external? If so, then what
has happened to the positive feedback aspect of covenant law?
What has happened to empowering by the Holy Spirit?

I argue that the greater empowering by the Holy Spirit for
God’s people to obey and enforce Biblical law is what invalidates
the implicit anti-dominion position regarding the ineffectiveness
of Biblical law in New Testament times. If Christians obey God's
law, then the positive feedback process is inevitable; it is part of
the law-governed aspect of the creation: “from glory to glory” (2
Corinthians 3:18). If some segments of the church refuse to obey
it, then those segments will eventually lose influence, money, and
power. Their place will be taken by those Christian churches that
obey God's laws, and that will therefore experience the covenant’s

6. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
Texas: Institute for Christian Economics, 1985), pp. 159-62, 185-86.
