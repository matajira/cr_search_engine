106 Liberating Planet Earth

appeal, In 1970, the figure was 12,000 (p. 27). Thus, concludes
Fleming:

The consequence of this expansion of federal power over state
criminal procedure through the creation of fiat prohibitions and
rigidly ritualistic rules has been to elevate formalism to constitu-
tional right, to complicate every significant phase of criminal pro-
cedure to the point where in some instances the system of criminal
law has difficulty functioning and in others it turns loose persons
who are patently guilty (p. 97).

Salvation by Law

The quest for perfect justice leads inevitably to arbitrary juris-
prudence and public lawlessness. Joseph in Pharaoh's jail, Daniel in
the lions’ den, and Jesus on the cross all testify to the imperfec-
tions in human courts of law. Nevertheless, godly men can live
with imperfect justice, just as they live with imperfections in all
other spheres of human life, because they know that perfect jus-
tice does exist and will be made manifest on the day of judgment.

Life is too short to demand perfect justice on earth; better by
far to have speedy justice handed down by godly amateurs than to
suffer with the clogged courts of messianic humanism. We need
not wring our hands in despair because men’s courts, in time and
on earth, fail to meet the standards of perfection which will reign
supreme in God’s court. We are not saved either by the perfect
spirit of the law or the perfect letter of the law. We are surely not
saved by imperfect imitations of the spirit and letter of the law. We
are not saved by law.

Salvation by law is an ancient heresy, and it leads to the
triumph of statist theology. Christianity is in total opposition to
this doctrine. As R. J. Rushdoony writes in his book, Politics of
Guilt and Pity (Craig Press, 1970):

The reality of man apart from Christ is guilt and masochism.
And guilt and masochism involve an unshakable inner slavery
which governs the total life of the non-Christian. The politics of
the anti-Christian will thus inescapably be the politics of guilt. In the
politics of guilt, man is perpetually drained in his social energy and
