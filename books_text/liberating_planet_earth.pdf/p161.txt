Conclusion 153

And why would this promise come true? Because of faithful-
ness~ God’s and Abraham's, Abraham had been faithful:

“. . . because Abraham obeyed My voice and kept My charge,
My commandments, My statutes, and My laws” (Genesis 26:5).

Did Isaac obey? Yes. “So Isaac dwelt in Gerar” (Genesis 26:6).
Gerar was not the final goal; the promised land was. But Isaac
was to be content in Gerar for the time being, until God moved.
him somewhere else.

We, too, are to remain in Gerar.

We are not to ignore the present evil around us. We are not to
pretend that evil does not exist. We are certainly not to teach that
evil is good, or that oppression is compassion. We are to work and.
pray for a better day on earth when all men will recognize evil for
what it is. We are to work and pray for the day promised by Isaiah:

The foolish person will no longer be called generous, nor the
miser said to be bountiful. For the foolish person will speak foolish-
ness, and his heart will work iniquity: to practice ungodliness, to
utter error against the Lorn, to keep the hungry unsatisfied, and he
will cause the drink of the thirsty to fail. Also the schemes of the
schemer are evil; he devises wicked plans to destroy the poor with
lying words, even when the needy speaks justice. But a generous
man devises generous things, and by generosity he shall stand (Isa-
jah 32:5-8).

Not by revolution shall the generous person stand, but by gen-
erosity. A better world on earth is coming:

Then justice will dwell in the wilderness, and righteousness re-
main in the fruitful field. The work of righteousness will be peace,
and the effect of righteousness, quietness and assurance forever.
My people will dwell in a peaceful habitation, in secure dwellings,
and in quiet resting places (Isaiah 32:16-18).

Faithful Christians are not to preach perpetual contentment
with moral evil, either personal or social, but neither are we to
preach instant liberation through revolution and violence. Chris-
tians are to preach life, not death. We are to be content with life as
