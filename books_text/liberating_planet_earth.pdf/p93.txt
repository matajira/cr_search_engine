The Liberation of the Family 85

By abandoning the principle of family responsibility, the mod-
ern messianic state wastes a culture’s capital, destroys inherit-
ance, and makes more acceptable both euthanasia (which reduces
the expense of caring for the unproductive elderly) and abortion
(which reduces the expense of training and caring for the unpro-
ductive young). Lawless men, in their productive years, refuse to
share their wealth with dying parents and squalling children.
They look only at present costs, neglecting future benefits, such as
the care which the unborn might provide them in their old age.
They have faith in the compassionate and productive state, the great social
myth of the twentieth century. They want its benefits, but they
never ask themselves the key question: Who will pay for their re-
tirement years? The shrinking number of children, who are even
more present-oriented, even more conditioned by the statist edu-
cational system, even more unwilling to share their wealth with
the now-unproductive aged of the land? With the dissipation of
capital, the productive voters will resist the demands of the elderly.
The generations go to war against one another: the war of politics.

The Coming Bankruptcy

The pseudo-family state is an agent of social, political, and
economic bankruptcy. It still has its intellectual defenders, even
within the Christian community, although its defenders tend to be
products of the state-supported, state-certified, and state-aggran-
dizing universities. This pseudo-family is suicidal. It destroys the
foundations of productivity, and productivity is the source of all
voluntary charity. It is a suicidal family which will pay off its debts
with inflated fiat currency. Its compassion will be limited to paper
and ink.

The impersonalism of the modern pseudo-family, along with
its present-orientation~a vision no longer than the next election
—will produce massive, universal failure. It has already done so.
The great economic experiment of the twentieth century is almost
over, and all the college-level textbooks in economics, political
science, and sociology will not be able to justify the system once it
erodes the productivity which every parasitic structure requires
