The Liberation of the Individual 67

and then these revolutions are overturned. But I am not here to
discuss the failures of Marxist theory, which are legion. I have
done that job elsewhere.! This book is about liberation theology.

Marxist liberation theology teaches that man’s nature is
changed as a result of the revolution. The change in man’s nature is
outside-in. This is the essence of Satanism. This is pure environ-
mental determinism, right out of the mouth of Adam after his re-
bellion. The Satanists want to create a new humanity through
social engineering and the transformation of the social, political,
and economic environment, The creation of a new humanity is a
religious impulse, mimicking the Bible. The Bible preaches ethical
reconciliation—a new humanity born by God’s imputation of
Christ’s perfect humanity (though net His divinity) to individual
sinners. This is to lead to evangelism: inside-out ethical renova-
tion that produces the spread of reconciliation.

Therefore, if anyone is in Christ, he is a new creation; old
things have passed away; behold, all things have become new.
Now ail things are of God, who has reconciled us to Himself
through Jesus Christ, and has given us the ministry of reconcilia-
tion, that is, that God was in Christ reconciling the world to Him-
self, not imputing their trespasses to them, and has committed to
us the word of reconciliation. Therefore we are ambassadors for
Christ, as though God were pleading through us: we implore you
on Christ’s behalf, be reconciled to God (2 Corinthians 5:17-20),

Outside-in

Compare Paul’s vision of personal regeneration, voluntarism,
evangelism, and world service as ambassadors of a risen Christ
with the outside-in remaking of mankind that is recommended by
Marxist liberation theologian José Miranda:

Our revolution is directed toward the creation of the new
human being. But unlike the attackers, we seek to posit the neces-
sary means for the formation of this new human being. And the in-

1. Gary North, Marx's Religion of Revolution: The Doctrine of Creatice Destruction
(Nutley, New Jersey: Craig Press, 1968).
