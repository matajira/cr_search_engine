Subject Index

teumphant?, 138
tyranny of, 17
skins, 23
slap, 133
slavery, 18, 86, 149, 151-52
(see also Bondage)
slaves, 3
sleep, 119
social gospel, 127
socialism, 2, 119-21
Son of God, 4 (see also Jesus)
sovereignty, 118, 124
Soviet Union, 14, 134 (see also Russia)
South America, 7
Sowell, Thomas, 120n
Stalin, 59
state
church &, 96
“compassion,” 85, 86
corporate, 122
covenant, 89-90, 102-3
decapitalize, 86
dependence on, 86
destroyer, 126
education, 84
“family,” 85
favors, 86
“kidnapper,” 83-84
messianic, 82-83
Moloch, 125
owner, 127
protector, 123-24
restrained, If
salvation, 107, 126
Satan's covenant, 74
socialism, 2
taxes, 84, 111, 124
thief, 127
tithe, 124, 127
violence (monopely), 110
welfare, 122
stealing (see Theft)
submission, 26-27, 54-55

175

subordination, 48
superstructure, 66
Sutton, Antony, 134n
Sutton, Ray, 51, 74

taxes, 84, 111, 113, 124
tax rebellion, 31

talent, 80

talents, 45

tariffs, 126

technology, 134

Ten Commandments, 33, 36, 108
testament, 27

theft, 109, 110, 112, 116, 122, 126
Third World, 13

thrift, 127

time, 127, 129

tithe, I11, 124, 127

torture, 61

totalitarianism, 111

trade, 112

transcendence, 52-53

treaties, 28

treaty, 20

trespassing, 22

Trotsky, Leon, 13

trust, 70

trustee, 82-83

truth, 24, 68

Turnbull, Colin, 133

tyranny, 17, 30, 31, 32, 140

Ukraine, 32
uncertainty, 125
union with God, 43
United States, 12

Yan Til, Cornelius, 53
victory, 21, 130, 132, 144, 147
vineyard, 125

violence, 110

voting, 122-23

vow, 50
