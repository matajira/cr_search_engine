32 Liberating Planet Earth

were members of a Christian group called the Old Believers.
They had been harshly persecuted intermittently by the state
church, the Russian Orthodox Church, since the late 1660's.
These Old Believers “went underground,” hiding their worship
activities from the authorities and conducting their religion as
they saw fit. They moved as far away from the centers of power as
they could. In 1883, the Czar made it illegal for Old Believers to
establish their own schools. Education was to be in the hands of
the established church, and the religious leaders believed that the
children of the Old Believers could be lured away from their par-
ents’ religion. Only after the defeat of Russia by the Japanese in
1905 did things improve for the Old Believers. But even in
January of 1914, shortly before World War I broke out, the Min-
ister of Education placed restrictions on hiring Old Believers as
teachers.

In their resentment against the Czar and the state church,
they sometimes participated in the periodic revolts against the
Russian State. When the Czarist system began to crumble after
1905, the Stare had already lost the support of a major segment of
its most religiously conservative citizenry. This loss of support
helped to produce the Bolshevik revolution, which placed the Old
Believers under far greater persecution than the Czar had ever
imposed. By retreating from almost all positive social action for
centuries, they eventually sealed their doom.

Something similar happened when the Nazis invaded the
Ukraine in 1941, The persecution of Ukrainians (in Western
Russia) by the Soviets in the 1930's had been horrendous; they
had literally been starved to death. It was during these years that
Nikita Khrushchev earned his reputation as “butcher of the
Ukraine.” The Ukrainians at first joined the Nazis by the mil-
lions. They hoped for liberation from their Russian Bolshevik
masters. But the Nazis imposed another tyranny every bit as bad
as Stalin’s. What had appeared to be liberation became just
another horrible tyranny, with occultism, racism, and socialism as
the new religion rather than Bolshevism’s atheism and Commu-
nism. It does no good to try to leap out of frying pans into fires.
