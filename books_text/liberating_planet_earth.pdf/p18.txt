10 Liberating Planet Earth

here and now, so that Christians will gain the wisdom and experi-
ence they need to obey God even better in eternity beyond the
grave. Christians need blueprints for social healing.

Let me say right from the beginning that this little book is not
a guide for Christians about how to escape responsibilities in this
world. God does not call us to abandon earthly responsibilities.
He calls us to exercise dominion over every aspect of the earth in
His name, to His glory, and by His law:

Then God said, “Let Us make man in Our image, according to
Our likeness; let them have dominion over the fish of the sea, over
the birds of the air, and over the cattle, over all the earth and over
every creeping thing that creeps on the earth” (Genesis 1:26).

Christians need to understand that their obedience to Christ
in history also produces fruit in eternity. Good works bring heav-
enly rewards from God.

For no other foundation can anyone lay than that which is laid,
which is Jesus Christ. Now if anyone builds on this foundation
with gold, silver, precious stones, wood, hay, straw, each one’s
work will become manifest; for the Day will declare it, because it
will be revealed by fire; and the fire will test each one’s work, of
what sort it is. If anyone’s work which he has built on it endures,
he will receive a reward. If anyone’s work is burned, he will suffer
loss; but he himself will be saved, yet so as through fire (1 Corinthi-
ans 3:11-15).

There is nothing wrong with wanting rewards in heaven, just
so long as we understand that such rewards are the fruit of right-
eousness done on earth by the grace of God and for the glory of
God. We must begin with the desire to please God, not the desire
to earn rewards. Paul warns us all that we should fear God,

who will render to each one according to his deeds: eternal life to
those who by patient continuance in doing good seek for glory,
honor, and immortality (Romans 2:7).

Every reader should be aware that the Bible is a book about
commitment, hard work, faithfulness, and justice in history. But it
