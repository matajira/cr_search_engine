34 Liberating Planet Earth

He is a God of power and of ethics. Both of these features of
God’s being are revealed by His act of freeing the Hebrews from
their Egyptian masters. Both love and awe are due to Him. The
events of life are controlled by a God who can bring His words to
pass.

The Hebrews had this as the historical foundation of their
faith in God and His law-order. This law-order is summarized in
the Ten Commandments that follow the introduction. The com-
mandments are the foundation of righteous living. The whole of
Old Testament law serves as a series of case-law applications of
the ten. Thus, they must be regarded as the basis of social institu-
tions and interpersonal relationships. Whatever the area of life
under discussion—family, business, charitable association, mili-
tary command, medicine, etc. ~ Biblical law governs the actions
of men.

Men can choose to ignore the requirements of the law. But
God dealt in Egypt and the Red Sea with those who flagrantly and
defiantly rejected the rule of His law. The Israelites had experi-
enced firsthand the institutional effects of a social order governed
by a law-order different from the Bible’s. They had been enslaved.
The God who had released them from bondage now announces
His standards of righteousness—not just private righteousness
but social and institutional righteousness, Thus, the Ged of libera-
tion is simultaneously the /aw-giver. The close association of Bibli-
cal law and human freedom is grounded in the very character of
God.

The Hebrews could not have misunderstood this relationship
between God’s law and liberation. God identifies Himself as the
deliverer of Israel, and then He sets forth the summary of the law
structure that He requires as a standard of human action. The God
of history is the God of ethics. There can be no Biblical ethics apart
from an ultimate standard, yet this standard is fully applicable to
history, for the God of history has announced the standard. Ethics
must be simultaneously permanent and historically applicable.
Permanence must not compromise the applicability of the law in
history, and historical circumstances must not relativize the uni-
