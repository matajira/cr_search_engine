The God of Liberation 37

theology—undermines the church’s ability to reconstruct social
institutions in terms of God’s revealed word.

Why, then, have the Bible-believing churches remained silent
for so long? Why have they allowed Marxists to steal the idea of
liberation and social transformation? One important reason is
that the churches are not agreed on the need to offer a positive al-
ternative to humanism. Many churches prefer to hide in the his-
torical shadows until Jesus comes again physically to deliver His
people from bondage. They do not believe that God has given us
the authority and tools to deliver the world from bondage in His
name before He comes again physically. I call this sort of Christi-
anity the escapist religion. (See Chapter Three.)

Summary

Liberation and the law of God go together. God’s announce-
ment to His people that He is the God who delivered them from
Egypt, and then His presentation of the ten commandments,
makes this connection between freedom and Biblical law inescap-
ably clear. To abandon faith in the reliability of God’s law is to
abandon faith in what the Bible proclaims as the only basis of
liberation, namely, liberation under the sovereign power of God,
who sustains the universe and calls all men to conform themselves
to His ethical standards in every area of life, in time and on earth.

In summary:

1, The Marxist liberation theologians improperly appeal to
the example of the exodus,

2. The Israelites did not adopt tactics of armed revolution
against the Egyptians.

3. Christians must not work with or aid Marxist, humanist
revolutionary movements.

4, The God who liberates is the God who controls history.

5. This God is the God of power and ethics.

6. God has given His people His Jaw in order to liberate them
from sin in every area of life.

7. God has given His people His law in order to enable them
to exercise dominion in every area of life.
