6
THE LIBERATION OF THE FAMILY

Honor your father and your mother: that your days may be long
on the land which the Lor your God is giving you (Exodus 20:12).

The family is a covenantal institution. Marriage vows under
God involve taking self-maledictory oaths: husband and wife
swear loyalty to each other, with the death penalty for adultery
hanging over them (Leviticus 20:10). They say in Christian cere-
monies, “till death do us part.”

The family covenant is structured along the same lines as
God's covenant structure. It begins with transcendence and im-
manence (presence). God’s transcendence and presence are seen
in the representative position of the father as God’s lawful family
agent.

Wives, submit to your own husbands as to the Lord. For the
husband is head of the wife, as also Christ is head of the church;
and He is the Savior of the body, Therefore, just as the church is
subject to Christ, so let the wives be to their own husbands in
everything (Ephesians 5:22-24).

There is a hierarchy in the family: husbands over wives; par-
ents over children (Ephesians 6:1-3).

There is law in the family: the parents under God teach the
children daily in God's law (Deuteronomy 6:6-7).

There is judgment in the family: parents are required to inflict
punishments, even physical pain, in order to train children in
godliness.

Finally, there is inheritance. The children, if they are obedient

73
