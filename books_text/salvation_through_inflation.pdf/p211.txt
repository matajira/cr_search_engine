Dividends Under Capitalism 179

tude, Douglas then misidentified the source of dividends. He
confused dividends with interest.

Interest is what borrowers pay to “hire” money or goods for
a period of time. Because present goods are more valuable to
us than those same future goods are, a borrower has to pay the
lender something extra when the lending period has expired
for the privilege of using the money in the meantime. Every
lender discounts in his own mind the present value of future
goods in comparison to the present value of those same goods
available today. To offset the lender’s inescapable mental dis-
count, the borrower has to agree to pay an extra quantity of
future goods to gain access to the lender’s supply of present goods,
ie., the goods that his loan money would otherwise have made
available to him.

Let me offer this example. Rolls-Royce automobiles do not
change style very often. They still look like a 1953 car. I shall
therefore use the example of a Rolls-Royce: no change in taste
by consumers or change in style by the producer. (I could also
use the example of London’s black cabs, which look like 1938
automobiles, but who among us would want to own one?)

Let us say that you just won a Rolls-Royce. You subscribed to
a magazine and this was the grand prize. All taxes have been
paid on it. You are now given a choice. Would you like the
Rolls-Royce delivered right now or in five years? 1 know your
answer: right now. Why? Because a Rolls-Royce is worth more
to you right now than the same Rolls-Royce delivered five years
from now is worth to you right now. Present goods are more
valuable to us than identical future goods, other things remain-
ing the same. Because of this, the person who wants you to put
off delivery for five years will have to offer you something extra
for the privilege. This is your interest payment. It is a payment for
forfeited use over time.

All this was known by economists as early as 1884, when the
Austrian economist Eugen von Béhm-Bawerk wrote The History
and Critique of Interest Theories. In Chapter 12, “The Exploitation
