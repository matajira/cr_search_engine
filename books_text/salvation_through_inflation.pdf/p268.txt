236 SALVATION THROUGH INFLATION

Incoherence

I have argued that you should not commit yourself to any
political reformer whose reform cannot be put into simple,
straightforward language. Any political reform that is not clear
would be dangerous if implemented. Those doing the imple-
menting could then claim almost anything in the name of the
reform. You must demand clarity from every economic reform-
er, Do not trust his reform if he cannot express himself clearly. With
this in mind, consider Major Douglas’ description of his theory:

The fundamental idea which it is necessary to grasp is that
you cannot get existing and future credit-power into the hands
of the community, unless the distribution of purchasing-power,
both in respect of capital increases, as well as in respect of ulti-
mate products, is only taken back from the community in the
proportion that consumption bears, not only to these products,
but to capital production as well, using capital in just as wide a
sense as the credit-issuer uses it.

The result of this is that as a condition of such a state of
affairs, prices of ultimate commodities would have to be fixed,
not with regard to what they would fetch, but with regard to the
above ratio, which would result in a price which would be a
fraction of cost; the difference being made up to the entrepre-
neur by an issue based on the actual capital still remaining as a
result of effort represented by total “cost.”*

You do not understand this, do you? This is not because you
are stupid. Do not blame yourself. Major Douglas was confused.

One of the reasons why Social Credit has attracted such
religiously and philosophically diverse supporters (but not
economists) is that the confusion of his presentation allows
people to imagine that somehow or other, “Douglas believed
what I believe.” Some advocate of Social Credit may say that it
is truly conservative. Others say Social credit is “truly, deep

8. Credit-Power and Democracy, pp. 48-47.
