70 SALVATION THROUGH INFLATION

from a bank in a nation not yet under a Social Credit govern-
ment. It would have meant the bankruptcy of all Scottish banks,
which would have had zero borrowers at this mandated interest
rate. It would have meant the nationalization of all Scottish banking.
The government would overnight have become the sole legal
lender. This, of course, is the whole point of Social Credit: the
nationalization of credit. Douglas knew exactly what he was
recommending, but he never called it what it really was: the
forced nationalization of banking and credit.

This is a program to destroy private property. A government
places a price control - in this case, a price floor - on commer-
cial interest rates, thereby bankrupting many businesses and all
private banks within its borders. This is a system for destroying
the capital invested in many businesses and all privately owned
banks. It is the deliberate, systematic destruction of private
wealth without compensation.

Why is Social Credit regarded as conservative?

Price Controls

Price controls are established by governments in order to
thwart the decisions of buyers and sellers, i.e., to thwart con-
sumer sovereignty. Price controls make private bargaining
illegal. This is why the French Revolutionary government im-
posed them.

When a price ceiling is set below the price that would have
prevailed on the free market, sellers start withholding supplies,
while too many buyers show up. Shortages are the result. Simi-
larly, when a price floor is set above the price at which ex-
changes would take place voluntarily, suppliers bring more
goods to market than there is demand. Gluts are the result.
Thus, only when prices are set by the government where the
free market would have set them anyway does the economy
avoid either shortages and gluts.

We come to clause 5: “Simultaneously, an announcement to
be published that any or all business undertakings will be ac-
