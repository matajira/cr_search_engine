Social Credit's Blueprint 65

This proposal does not say that the government will pay
anything to those who relinquish control over their real estate.
Perhaps this is to be left to the discretion of the politicians.
However, the word “re-allot” indicates that the government will
not sell the property. Thus, it would seem that the government
will not pay for the property, either. If it does, it will pay the
owner with fiat money, as we shall see. In any case, the lan-
guage of the reform indicates that private owners will not be
able to designate lawful heirs. The government will decide who
is to gain ownership of real estate. Bureaucrats will “re-allot”
the real estate to “suitable applicants.” Suitable to whom? Not
the original owners.

There is another crucial market arrangement that will be
made illegal: retirement funds. A retirement plan is adminis-
tered by a trustee or a holding company. Under Social Credit,
this will become illegal. “As from the initiation of this scheme,
the holding of any stock, share, or bond by a holding company
or trustee will not be recognized.”

He did not offer any explanation for either of these two
prohibitions. He expected his followers to take his word for it.

Why is Social Credit regarded as conservative?

The National Dividend

Having shut down the sale of privately owned real estate and
having made mutual funds and pension funds illegal, the gov-
ernment then should announce a levy, Douglas said. Presum-
ably, this will be strictly a statistical levy, not the actual confisca-
tion of ownership rights. I say presumably; Douglas did not
make this clear.

This levy. seems to have been a statistical device for establish-
ing how much new money would be created. Douglas used a
1% figure as an example, but nothing in his system ever estab-

17, Ibid., p. 206.
