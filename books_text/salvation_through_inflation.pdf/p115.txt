Who Represents the Consumers? 83

summarize perfectly the essence of all pragmatism: “That is
moral which works best.” But is this statement true?

Had he had rearranged this sentence, the Christian social
theorist could wholeheartedly agree: “That which is moral
works best.” But this restructured statement is not what Major
Douglas was trying to convey, as his Preface so clearly reveals.
In fact, my restructured statement is precisely what Douglas
was doing his best to deny. He came before his readers dressed
in the garb of a neutral scientist, a hands-on kind of fellow - an
engineer, if you will. He said he was not going to burden the
reader with a lot of moral ranting and raving. On the contrary,
he said he intended to discuss only practical matters.

Elsewhere, he made it clear that what he really disliked was
the kind of morality that is found in biblical law:

It is my own opinion that until it is clearly recognised that the
only sane objective of an industrial and productive system is to
deliver goods as, when, and where desired with the minimum of
trouble to anyone, and that the moment you begin to mix this
clear-cut objective up with moral considerations, so called, in-
cluding a strong dash of Mosaic law, you produce, maintain, and
increase friction, inefficiency, and mental and physical distress,
and that if you persist, as we are persisting, in this confusion of
objective, you will eventually arrive at a situation involving the
serious elements of breakdown.

There is something we should understand about people who
come to us with a proposed reform that supposedly is based
entirely on practical concerns. These programs invariably sneak
a disguised morality through the back door. While the reformer
stands at the front door discussing practical reforms with you,
his accomplice is in the back of your house going through your
closets and cabinets. So, let us get one thing clear from the
beginning: there is no ethical neutrality. Every reformer comes in

3. Warning Democracy (2nd ed.; London: Stanley Nott, 1984), p. 11.
