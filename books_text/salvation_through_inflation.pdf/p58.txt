26 SALVATION THROUGH INFLATION

punishments, seems to involve, fundamentally, a general condi-
tion of scarcity and discontent. You cannot reward an individual
with something of which he has already sufficient for his needs
and desires, nor can you easily find a punishment which will be
effective in a world in which there is no incentive to crime. We
might legitimately expect, in such a Society, a mechanism which
would ensure a continual, and, if rendered necessary by the
advancement of science, an artificial disparity between demand
and supply of material goods and services, together with an
organisation which would prevent any infringement of the rules
by which this disparity is maintained.

We do, in fact, find exactly such a state of affairs in the world
to-day. The exact methods by which the financial organisation
produces the illusion of scarcity will demand our attention al-
Most at once, and at some length; the organisation by which
these arrangements are enforced is, of course, familiar in the
form of the Common Law?

Social Credit rejects “the illusion of scarcity.” It also rejects
the necessity of economic sanctions in a post-reform world. This
means that there should be neither profit nor loss in a decent
economy. But there is a problem here: profit and loss are sanc-
tions that guide production. As Ludwig von Mises argued, such a
zero-profit world would be populated only by omniscient be-
ings. Profit and loss result from men’s lack of perfect knowledge re-
garding the future. If every person perfectly knew the economic
future, there would be neither profits nor losses."

In such a world, there would also be no need for money.!!
If I knew perfectly the future condition of supply and demand,
I would not need money. Money is a means of hedging myself
against what I do not know about the future. I keep cash in my
wallet because I never know when I may want to buy some-

9. Social Credit (8rd ed.; London: Eyre & Spottiswoode, 1983), pp. 78-79.

10. Ludwig von Mises, Hunan Action: A Treatise on Economics (New Haven
Connecticut: Yale University Press, 1949), p. 29).

11. Jid., p. 250.
