234 SALVATION THROUGH INFLATION

short-term rates temporarily because those who use money do
not recognize that the newly created money does not in fact
represent additional new supplies of gold or silver or other
valuable commodity. When the fractional reserve banking sys-
tem creates new money by lending fiat money either to produc-
ers or to consumers (rarely considered by Douglas), this money
has not been made available by depositors. No depositor has
voluntarily delayed consuming some scarce economic resource
today in order to deposit money in the bank so that the bank
can lend money to someone else for a period of time. There-
fore, the banking system’s injection of fiat credit money is infla-
tionary. This is the fault of the government: it allows fractional
reserve banking. It wants cheap loans for its own purposes, so
it legalizes private counterfeiting: fractional reserve banking.

Any artificial lowering of interest rates by means of an the
issue of fiat money, either by the government or the banking
system, will be followed by a rise in rates during the resulting
inflationary boom period and the credit crunch (preliminary to
a recession) that follows.® Fiat money creates the boom, but the
result is either a recession when the increase of money ceases,
or mass inflation and an inflationary collapse if the government,
with or without a central bank acting as its agent, continues to
spend new fiat money into circulation.

Estimating the Value of the Nation’s Capital

The credit masters will control the money supply under
social credit. As we saw in his blueprint for Scotland, the credit
masters will first take an inventory of all the goods and services
in the economy, including the estimated value of everyone's
work in the future. This inventory will be expressed in mone-
tary terms. Then they will assign an arbitrary percentage to this
total value of national capital. Based on this percentage, the

6. Ludwig von Mises, Human Action: A Treatise on Economics (New Haven, Con-
netticut: Yale University Press, 1949), ch. 20.
