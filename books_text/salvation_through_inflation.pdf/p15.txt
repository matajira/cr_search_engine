Foreword xv

promise solutions, especially quick-fix, one-shot, “turn-key”
solutions — solutions they hope will patch things up temporarily
until Jesus returns in glory to take them bodily to heaven.

Different Christian subdivisions embrace different reform
projects. What is significant is that all of these proposed reforms
have been imported from the humanist camp. Academic neo-
evangelical Christians tend to become enthusiastic promoters of
liberal humanist fads that fell out of favor a decade or two
earlier. Fundamentalist Christians, in contrast, tend to be skep-
tical about social reform in general, so they are less likely to
adopt these decade-old reform schemes. This does not make a
fundamentalist immune to discarded humanist fads. It makes
him vulnerable to much older discards — discards so ancient
that even the humanists have forgotten about them. Unaware
that these ideas were exposed as erroneous or even fraudulent
two or more generations earlier, a fundamentalist may embrace
these ideas as the wave of the future, to the extent that he
believes that this dispensation has a future. A good example of
this sort of long-forgotten discard is Social Credit.

Social Credit and “Real Bills”

The Social Credit movement began in 1917. The fundamen-
tal idea of Social Credit is that capitalism suffers from a major
flaw: it does not create sufficient bank credit to allow consumers
to buy the entire output of industry. The origin of this idea
goes back to the long-forgotten “real bills” doctrines of the early
nineteenth century. This was an arcane debate’ between two
groups of English economists: the Banking School (“real bills”
advocates) and the Currency School (anti-inflation). What is not
understood is that Social Credit is a logical extension of the case for
real-bills banking, bul without private banks. Under Social Credit,
the State would take over the bankers’ economic function:
creating credit and deciding which businesses should receive it.

The terminology of “real bills’ commercial banking was
popularized by Adam Smith in 1776, who referred to a “real
