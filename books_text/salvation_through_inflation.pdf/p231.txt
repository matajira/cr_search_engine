Social Credit Means State Monopoly Credit 199

nationalization. But if the experts can use political power io national-
ize the allocation of credit, thereby “conscripting” credit, what is the
economic difference? The difference is merely rhetorical.

Men can resist the evil side of socialism, Douglas insisted.
How? By his magic pill, the creation of a State monopoly bank
which will issue below-cost, zero-interest loans to favored pro-
ducers who are registered with the State: his Just Price
scheme.” He ended his portion of Credit-Power and Democracy
with these stirring words: “Into the temple of this faith the
money-changers have entered; and only when they have been
cast out shall we have peace.”*® The problem is, slogans are
not a reliable institutional defense against State monopoly pow-
er. Karl Marx also wanted a State monopoly bank in the transi-
tion stage to his recommended Communist society. Why? Be-
cause he understood that they who control the allocation of capital
thereby control the economy. Lf this control is forced on producers
and lenders by the State, then the State has become the true
owner. The tools of production are in the hands of the State.
This is the essence of socialism.

Decapitalization Through Mass Inflation

During the French Revolution, the government confiscated
the monasteries and landed estates of those who had fled the
country. This decapitalized Christians and conservatives. The
government then issued paper money with these lands serving
as collateral. The problem was, as these lands appreciated in
value because of this monetary inflation, their collateral value
rose. This allowed the government to issue even more money.
Year by year, the government destroyed the French economy
by means of monetary inflation.” But the politicians through-

25. See Chapter 4, above.
26, Zid.. p. 146.

. 27. Andrew Dickson White, Fiat Money in France (Irvington, New York: Founda-
tion for Economic Education, [1912] 1959).
