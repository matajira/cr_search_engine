Testing the Prophets 21

“Poetry!” says the Christian geologist employed by a state uni-
versity. But he is not employed to teach poetry. He is employed
to teach evolution through impersonal natural selection, and
this is what he does. He forgets Jesus’ fearful warning: “But I
say unto you, That every idle word that men shall speak, they
shall give account thereof in the day of judgment. For by thy
words thou shalt be justified, and by thy words thou shalt be
condemned” (Matthew 12:36-37).

3. The Unity of Truth: Biblical Law

The governing principle of the Christian creationist is that
the Bible is true; therefore, Darwinian science must be incor-
tect. This is his operating presupposition. It governs his search
for evidence. He begins with God’s Bible-revealed word, not
with the supposedly neutral mind of man. Truth is one, the
Christian presuppositionalist says; thus, science must be restruc-
tured to conform to biblical truth. There is a hierarchy of truth.
The Bible is at the top of this hierarchy. Men are required by
God to pronounce judgment in terms of the Bible.

This places the Bible above all other supposed truths. There
are no things indifferent. There are many things that are not
yet understood to be biblically relevant, but this is a matter of
our poor understanding, not the Bible’s lack of jurisdiction.
What we know is that everything is relevant to Christian faith
because everything is under God’s judgment. There is nothing
outside of God’s judgment. God brings judgment in terms of
His revealed law; thus, everything must be under God’s law.
The truths of science must be sifted and rearranged, or per-
haps discarded, in terms of biblical revelation. Bible-rejecting
people may discover things that are true, but they can do this
only because they are not being consistent with their God-deny-
ing religion’s presuppositions. They make these discoveries only
because they first steal a concept of cause and effect that comes
only from God’s creation and God’s revelation of himself in
history. Whatever they say that is true is a gift of God: common
