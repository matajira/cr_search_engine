Pound, Ezra, 32-33, 37, 207
pragmatism, 83
price competition, 130
price controls, xx, 52, 70-72
prices
counterfeiting, 51
expanding output, 121
falling, 119-40
Ford cars, 126-27
information &, 111-13
producers &, 115, 118
ratification, 229
ratification of, 115-16
relative, 201-2
supply & demand, 130-31,
195
zero?, 122
producers (accredited), 153
production, 106-7
productivity, 173
demand, 113
fallmg prices, 121
machines, 170-71
poverty &, 216
profits
consumers’ hammer, 76
Douglas’ blueprint, 71, 216
entrepreneurship, 76
innovation &, 76
Jesus’ view, 142
money supply &, 123
perfect knowledge, 128
quest for, 76
residual, 71, 76, 176
sanctions, 87
source of, 175-77, 244-45
property, 44
property taxes, 73

297

prophets, 17

Protocols, 222

Proudhon, P J., 14-15

public interest, 194

punishment (see sanctions)

purchasing power, 130, 135,
241

Puritanism, 213, 238-39

Quigley, Carroll, 6-7

real bills, xv-xviii

Real Credit, 96-99, 100, 150,
187, 203, 243

real estate, 64-65

recession, 101-2

Red Dean of Canterbury, 38-39,
180

reform, 189

reformers, 56

regeneration, 241

relative prices, 201-2

representation, 89-90, 102, 191,
193

Republican Party, 5-6

responsibility, 206

retirement, 65

revolution, 29

reward (see sanctions)

Rhodes, Cecil, 7

rich, 173, 254-55

tisk, 189

Robison, John, 11

Rockefeller, Nelson, 6

Rolls-Royce, 179

Rothbard, Murray, 107, 257

Round Table, 7
