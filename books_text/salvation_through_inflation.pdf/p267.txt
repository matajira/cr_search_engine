Conclusion 235

credit masters will issue fiat money every month to all citizens
except wealthy ones. This is the National Dividend.

The credit masters will also provide all business loans. These
loans will go only to government-registered companies. These
loans will be interest-free. Meanwhile, private banks will have to
charge at least 25 percent. This will pressure private companies
to register with the government to get access to the cheap,
interest-free government credit, i.e., social credit. Most business-
es will register with the government. They will be given the
money they need to begin production and complete it. Prices
and wages will be regulated by the credit masters. So will pro-
fits. This is the Just Price.

What will the National Dividend and the Just Price do to
market prices? They will raise them. If the government sets
price control ceilings, this will create shortages. It will lead to
black markets for the price-controlled goods.”

Furthermore, as the money value of consumer goods rises,
so will the money value of capital goods. This will increase the
national capital base. This will require further issues of National
Dividend fiat money, which will raise prices again, which will
increase the monetary value of the nation’s capital, and so
forth. An inflationary spiral is inevitable unless the credit mas-
ters decide not to take any more inventories of the nation’s
capital. But then how will technological progress be capable of
adding to the value of capital and therefore also increase size of
the National Dividend? Douglas never even raised this ques-
tion. Neither have the other credit reform inflationists.

The system was offered as scientific. It is not scientific; it is
merely inflationist. An illusion of science is conveyed by the
very incoherence of Major Douglas’ language. Most people do
not expect to be able to understand scientists.

7. Prices and Price Controls (Irvington, New York: Foundation for Economic
Education, 1992).
