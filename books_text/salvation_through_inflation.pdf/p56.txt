24 SALVATION THROUGH INFLATION

unwilling even to face this possibility, Jet alone admit it in pub-
lic. I do not expect defections by Social Credit leaders.

I am far more interested in persuading people who may be
loosely connected to the modern Social Credit movement, but
who have not yet committed themselves to it psychologically. I
want them carefully to think through exactly what Major Doug-
las taught rather than what his recent disciples claim that he
taught. I also want people to think through the inevitable eco-
nomic implications of what he taught. These implications are
neither conservative nor Christian, as we shall see. In short,
those who have become interested in the Social Credit move-
ment should study Major Douglas’ writings very carefully be-
fore they commit themselves to it — a movement claiming to be
scientific but also, in the opinion of several leaders of the move-
ment, ethically Christian. My book will help introduce readers
to what Major Douglas really taught.

His disciples will probably argue that he taught nothing like
this, so I offer readers this challenge: compare my citations with
the original sources. Also, examine the literature of modern
Social Credit to see whether these passages are dealt with in a
scientific manner. See if the responses to my book offered by
present-day defenders of Social Credit are thorough, clear,
well-documented, and dispassionate, as befits a scientific move-
ment. See if they address the issues raised in this book. If you
discover that their responses do not meet these criteria of rigor-
ous scientific discourse, then you will be ready to answer my
next question:

Is Social Credit Scientific or Utopian?

My answer is straightforward: utopian. There are very few
traces of science in the writings of Major Douglas. For one
thing, his books contain almost no footnotes to other men’s
books. There are no references to professional economics jour-
nals. There are no references to technical journals. There are
very few statistics, and nothing of an integrated nature: eco-
