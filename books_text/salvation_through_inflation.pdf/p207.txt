Dividends Under Capitalism 175

distribution of wages.”"® This was incorrect. It still is.

Dividends in today’s economy, Douglas said, go to a “large
body of shareholders, . . .""” This was incorrect. If his estimate
was correct, namely, that 94% of national income went to wag-
es, then there clearly was no “large body of shareholders” in his
day. Prior to the late twentieth century, the number of share-
holders in corporations was a tiny fraction of any nation’s popu-
lation, Only with the rise of tax-deferred pension funds and
mutual funds has this situation changed.'*

Profits

What is the source of profits? This has baffled many econo-
mists. Profits are a residual that is left over after all expenses
have been paid. Where do they come from? From the differ-
ence (spread) between what the producer has paid out for all
operating expenses and what he has taken in from buyers. But
how can there be a spread between costs and income in a sys-
tem that pays everyone the value of his output?

The answer is: entrepreneurship. An entrepreneur is an eco-
nomic forecaster who buys resources in order to sell them for
more later. He believes that some resource factors are priced
too cheaply compared to what they would be worth if every
producer could correctly forecast the future state of consumer
demand. That is, the entrepreneur buys up raw materials,
labor, and all other factors of production that will be used to
produce a product or service for future consumers. He guesses
that consumers in the future will be willing to pay more for the
product than what rival producers presently estimate.

The only way he can make a profit is if his competitors,
meaning other producers, have not spotted the opportunity. As

16. Credit-Power and Democracy, p. 48.
17, Ibid., p. 43.

18. Peter F Drucker, The Unseen Revolution: How Pension Fund Socialism Came to
America (New York: Harper & Row, 1976).
