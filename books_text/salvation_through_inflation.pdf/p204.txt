172 SALVATION THROUGH INFLATION

Douglas vaguely understood this. He said that “probably 94
per cent. of the purchasing power which constitutes the distri-
bution system of this country, is wages and salaries, and, on the
whole, this percentage of the total tends to increase, and divi-
dends collectively tend to decrease. . . .”"* But if this is true,
then Social Credit is irrelevant. If 94% of national income is
going to wage earners, and this percentage is rising, there is
hardly any break in the flow of funds to consumers. With this one
admission, Douglas introduced what he regarded as statistical
evidence of the ultimate success of finance capitalism. His re-
form would not be needed. He never seemed to recognize just
how damaging this admission was.

If this growing percentage of national income accrues to
wages, what will be the source of Social Credit’s proposed Na-
tional Dividend? Wage-earners are steadily absorbing the bulk
of production. Consumers are buying up their own production!
But excess production — production not being gobbled up by
wage-earners — must accompany the mailing of government
checks if society is to avoid mass inflation. We know that divi-
dends under capitalism are related closely to the return on
capital. Yet dividends represent a small percentage of national
income, as Douglas freely (and fatally) admitted. Social Credit
cannot overcome this limitation on dividends except by reduc-
ing wages as a percentage of national income. This is exactly
what Major Douglas promised — the replacement of wages by
the National Dividend — but he never indicated how this vast
increase in the post-reform productivity of capital will be ac-
complished by the State credit masters. He merely announced
that this will surely occur once society adopts Social Credit:

The scientific organisation of industry and the introduction of
increased quantities of solar energy into the productive system
means, and can only mean, the displacement of human labour

14. Warning Democracy (2nd ed.; London: Stanley Nott, 1934), p. 86.
