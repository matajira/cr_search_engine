Social Credit Means State Monopoly Credit 203

growing number losers in the final stages, as price inflation
destroys the value of savings; and we see the almost universal
crisis in the deflationary depression that inevitably follows the
period of the fiat-money-generated boom. But very few people
understand economics well enough to blame the depression on
the prior monetary inflation and its distortion of relative prices,
as well as its subsidy to businesses that began projects that
proved unprofitable when the monetary inflation slowed, end-
ed, and turned into deflation, when depositors make cash with-
drawals from banks and hoard the cash.

Who Decides What the Market Can Produce?

We return to the question of the limits on the creation of fiat
money by the State’s monetary authorities. Douglas wrote: “The
only sane limit to the issue of credit for use as purchasing-power is the
limit imposed by ability to deliver the goods for which it forms an effec-
tive demand, providing that ihe community agrees to their manufac-
ture.” But who is to decide this limit on the ability of the eco-
nomy “to deliver the goods”? He never said.

Furthermore, who is to determine if “the community agrees
to their manufacture”? The credit masters act in the name of
the community, but how can they know what the community -
meaning every individual — needs or wants? As he wrote: “Cen-
tralised financial credit is a technical possibility, but centralised
veal credit assumes that the desires and aspirations of humanity
can be standardised, and ought to be standardised, and ought
to be standardised. . . . [NJo man, or body of men, however
elected, can represent the detailed desires of any other man, or
body of men.”** If Real Credit cannot be defined scientifically
- Douglas offered several incompatible definitions” - then on

85. Credit-Power and Democracy, p. 101.
36. Ibid, p. 87.
37. See above, pp. 96-98, 136.
