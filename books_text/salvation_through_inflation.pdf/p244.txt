212 SALVATION THROUGH INFLATION

2. That the credits required to finance production shall be sup-
plied, not from savings, but be new credits relating to new pro-
duction.

3. That the distribution of cash credits to individuals shall be
progressively less dependent upon employment. That is to say,
that the dividend shall progressively displace the wage and sala-
ryt!

Here is the long-term economic goal of Social Credit: “The
dividend shall progressively displace the wage and salary.” He
believed that there should be no connection between labor and
income, between working and eating. He knew exactly who his
opponent was in this recommendation: the Apostle Paul.

You will see, [ think, without difficulty that the solution of this
situation is easy if you will only divest it of any preconceived
ideas of social morality, and turn your back on such ideas as “if
a man will not work neither shall he eat,” a sentiment which in
my opinion was merely a statement of fact in the conditions
under which it was written and not intended to be a canon of
ethics.

What did Paul say? “For even when we were with you, this
we commanded you, that if any would not work, neither should
he eat. For we hear that there are some which walk among you
disorderly, working not at ali, but are busybodies. Now them
that are such we command and exhort by our Lord Jesus
Christ, that with quietness they work, and eat their own bread.
But ye, brethren, be not weary in well doing. And if any man
obey not our word by this epistle, note that man, and have no
company with him, that he may be ashamed” (II Thessalonians
3:10-14). This sounds exactly like a canon of ethics. But it was ethics,

11. Ibid, p. 42.
12. fbid., p. 36.
