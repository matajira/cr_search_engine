246 SALVATION THROUGH INFLATION

To understand what Major Douglas failed to understand, we
must first be clear in our minds regarding what is true. We
must analyze capitalism’s system of rewards. Specifically, we
need to understand why labor, as is true of every other factor
of production, is paid very nearly the value of its output under
competitive capitalism. If men were omniscient, the owner of
every factor of production would receive exactly the value of his
factor’s output. Profits reveal underpaid factors of production.

Free Market Wages Approach the Value of Labor’s Output

What if some employer is paying his work force below-mar-
ket wages? Some of his workers will move to another place of
employment if they discover that another producer is paying
more, and this move will alert those workers who remain be-
hind that their employer is not paying them the full value of
their output. They will ask for more money. If they do not
receive the raise, more of them will quit. The employer will lose
to his competitors these crucial factors of production.

Any producer who discovers that his competitor is paying his
workers too little can and will try to lure the best workers away
from the competitor. This is not because employers are always
people with charitable inclinations. It is because they discover
that their competitor is buying a factor of production at a be-
low-market price. Wise competitors move in and bid up the
price of that factor of production. Remember the words of my
economic policy parrot: “high bid wins.” Given free movement
of labor and open competition, workers will tend to be paid the
full vaiue of their output. The profit motive of both the workers
and their employers will see to this. Producers compete against
producers, while laborers compete against laborers.

The System as a Whole

I will assume that Major Douglas was referring to the factory
system as a whole. His criticism was that the system of produc-
