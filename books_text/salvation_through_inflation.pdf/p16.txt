xvi SALVATION THROUGH INFLATION

bill of exchange drawn by a real creditor upon a real debtor,
and which, as soon as it becomes due, is really paid by the
debtor; . . .”* The process works as follows: a bank offers credit-
tissues a bank note or writes a check) to a businessman who is
owed money in the future. The bank discounts the promised
future payment today, and the businessman must pay the face
value of this “real bill” to the bank when the debtor pays the
businessman. The discount constitutes the rate of interest.

“Elastic” Counterfeit Currency

Banking School economists called for a so-called “elastic
currency,” one which can expand or contract according to “the
needs of business.” They regarded short-term bank credit as an
appropriate response to the demand by business for capital.
They did not perceive that short-term money creation by a fractional
reserve banking system has unwanted repercussions, such as the cre-
ation of a boom-bust economic cycle. They also did not perceive
that “the needs of business” cannot be separated from “the
price of loans,” meaning the interest rate.

Fractional reserve banking creates credit (money) out of
nothing. The law allows the banking system to lend out more
money than the banks have taken in from depositors: fractional
reserves. Thus, in the short run, banks can lower the short-
term interest rate by increasing the supply of loanable funds:
monetary inflation. Businesses then take advantage of this short-
term subsidy by borrowing more money than they would have
borrowed otherwise. So, the so-called “needs of business” rise
when interest rates are forced down by fiat money (called fiduci-
Gry money) created by the banking system. As these “needs”
rise, banks create more money to satisfy these “needs.”

The process of borrowing and money creation becomes self-
reinforcing until the increase in the money supply begins to

8. Adam Smith, Wealth of Nations, edited by Edwin Cannan (New York: Modern
Library, [1776] 1987), p. 288.
