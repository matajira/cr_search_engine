Major Douglas’ A + B Theorem 243

single-cause theory, he offered a single formula. This formula
was not a formula to guide the State’s credit masters. Rather, it
was a formula which he believed had, once and for all, revealed
the central flaw of finance capitalism. It was supposedly the
very encapsulation of his critique of finance capitalism. It has
been called the A + B Theorem.* He believed that it revealed,
in stark, scientific neutrality, the central technical flaw of capi-
talism. He used it to augment his verbal critique.

What was this verbal critique? He argued that capitalism
suffers from a fundamental flaw: the inability of the private
banking system’s finance credit to keep pace with what he called
Real Credit. When producers repay business loans, he said, they
thereby extinguish finance credit, meaning money, thus depriv-
ing consumers of the purchasing power necessary for them to
buy the full output of industry. Thus, workers under capitalism
cannot afford to buy back all of their production. There is not
enough money — “tickets,” as he referred to money — remaining
in the economy at the end of the production process to enable
producers to sell all of the output of the system of production.
This supposed break in the flow of funds must lead to losses for most
producers. Producers will eventually reduce output unless this
break in the flow of funds is overcome by the issue of fiat mon-
ey. This is an underconsumptionist criticism of capitalism.

The curious fact is this: Douglas’ A + B Theorem did not
analytically link his verbal critique of capitalism's supposed
break in the flow of funds with the supposed problem suppos-
edly identified by the Theorem: a break in payments to the
“organizations” that supply factories with raw materials. As we
shall see, this was a completely separate argument: the identifi-
cation of another break in the flow of funds. Douglas was incor-
rect on both counts. There is no break in the flow of funds
under capitalism. But the A + B Theorem hypothesized a

4. The Douglas Manual, edited by Philip Mairet (London: Stanley Nott, 1934), pp.
68-74.
