Falling Prices and Capitalist Profits 131

price is simply a question of supply and demand,” Douglas
wrote in his first book.'® He assured his readers that “it is
frequently assumed that a mere glut of goods will bring down
prices quite irrespective of any intrinsic economy involved in
large scale production.” This assumption is false, he said. “Un-
less these goods are all absorbed, the result may be exactly
opposite. . . .”!* In short, sellers will not lower their prices in
order to clear their inventories. For some reason — he never
explained what ~ sellers prefer to tie up their capital in unsold
inventory instead of getting at least some money for the pres-
ently unsold goods. In this view of sellers’ resistance, he shared
a fundamental first principle with John Maynard Keynes.

John Maynard Keynes

Hobson was an underconsumptionist, just as Douglas was.
He argued that capitalism is flawed by its inability to sell goods.
He rejected the nineteenth century's orthodox economic theory
of pricing: that there is no permanent gap between supply and
demand. Keynes [pronounced CANES], the most influential
economist of the twentieth century, favorably quoted Hobson in
this regard.”” But Keynes went far beyond mere quotation; he
praised Hobson to the skies. Keynes wrote of Hobson’s book,
The Physiology of Industry (1889), that “Mr. Hobson has flung
himself with unflagging, but almost unavailing, ardour and
courage against the ranks of orthodoxy. Though it is so com-
pletely forgotten to-day, the publication of this book marks, in
a sense, an epoch in economic thought.””! ] know of no econ-
omist and no economics book that Keynes praised more enthu-
siastically. He devoted six pages to analyzing Hobson's book.

18. Economic Democracy (2nd ed.; London: Cecil Palmer, 1921), p. 57.

19, Ibid., p. 87.

20. John Maynard Keynes, The General Theory of Employment, Interest, and Money
(New York: Macmillan, 1936), p. 129n.

21, Jbid., pp. 364-65.
