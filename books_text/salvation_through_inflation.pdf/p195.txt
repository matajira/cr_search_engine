A False Prescription 163
Summary

1, Major Douglas complained that the few control the many in
today’s capitalism.

2. The view that “the conspiracy” controls “the people” apart
from their consent is not biblical.

3. It is dangerous to “throw the rascals out” before widespread
morality and responsibility has prepared the way.

4. Social Credit insists that individuals can attain great wealth
with very little labor.

5. The Bible teaches that men must labor six days a week.

6. Douglas correctly identified the division of labor as the key to
wealth.

7. He did not identify the basis of capitalism’s social division of
labor: consumer sovereignty.

8. He offered a five-point diagnosis and a one-point prescrip-
tion.

9. He argued that credit is the heart of modern capitalism.

10. Bankers control credit.

11. The State should control credit, he concluded.

12. This confuses individual control (consumer sovereignty)
with political control (State sovereignty).

13. The problem with modern credit is fractional reserve bank-
ing and central banking, not credit as such.

14, Who will control the credit controllers?

15. Social Credit requires a one-state world government bank-
ing system to create capital scientifically.

16. Douglas defined money as tickets.

17. Tickets are legal claims to specific items.

18. Money is the most marketable commodity.

19. The value of money is indeterminate until actual transac-
tions take place.

20. Money is not a ticket.

21. A ticket is a legal claim on a specific item or service.

22. Money is not a legal claim.

23. Money allocates scarce resources; tickets do not.
