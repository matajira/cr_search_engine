170 SALVATION THROUGH INFLATION

ther, shouldn't the government make shovels illegal, so that
construction firms will have to hire even more men who use
only stainless steel teaspoons? Would society be richer if such a
law were enacted and rigorously enforced?

The argument that machines create unemployment are
always variations of the teaspoon argument. Such arguments
assume that most of those men who are freed up by labor-sav-
ing machines never get another job, never find other areas in
which to serve their fellow men in the production system.
These arguments do not view capital-intensive economic im-
provements as a means of producing greater wealth for all, or
almost all, members of society.

Douglas argued the opposite: machines should be encour-
aged because they will produce unemployment. He wanted to
take men out of the work place. He wanted the government to
send “dividends” to them to make their early retirement possi-
ble. But this argument rested on the assumption that capitalism
is capable of huge and rapid increases in productivity merely
through the substitution of Social Credit (fiat money issued by
the government). He never provided a single statistic to prove
this assertion; he merely stated it repeatedly, using different
figures every time, as we have seen."

Both views are false: “abolish machines” and “abolish labor.”
The substitution of machines has reduced the need for men to
labor in order to obtain a traditional income, but this added
productivity opens up new possibilities for increased productivi-
ty and therefore increased income. Men stay in the work force
in order to buy the ever-increasing output of capitalism.

The Lure of Dividends

The early Machine Age, which Douglas regarded as a good
thing, was financed by a tiny number of people who bought
equipment that employed laborers. The population of industri-

10. See above, pp. 109-10, 136. See below, p. 173.
