228 SALVATION THROUGH INFLATION

their recommended reform will improve on what the market
already accomplishes. The inflationists claim that a scientifically
organized injection of fiat money will accomplish this.

The problem is, there is no formula that provides the money
manipulators with a reliable, “scientific” guide. There is no
formula to get inside the minds of men, to reveal their innate
capacities and their range of desires. There is no way to collect
national price data that will reveal the true state of the “real”
economy ~ Douglas’ Real Credit, for example — but will not be
affected by the subsequent issuing of fiat money. There is no
way for politicians or bureaucrats to estimate scientifically the
degree to which the new money is inflationary. There is no way
for the money manipulators to be sure who wins and who loses
from each injection of fiat money, each at a specific point and
time in the economy. The economic effects depend entirely on
people’s present conditions and their expectations about the
economic future. No one knows what these conditions and
expectations are today or will be tomorrow.

Social Credit Is Not Unique

Social Credit is not a unique reform proposal. It is one
among many: Proudhonism, “free silver,” greenbackism (in the
late nineteenth-century United States), Lawsonomy, Gesellism,
Keynesianism, and many others. They all promote the same
economic premise: “The voluntary exchanges of free men un-
der a system of predictable civil law are not sufficient to bring
forth the immense natural abundance of nature and society.”

Major Douglas argued that inherent in every economy is this
built-in tendency for centralization of authority over production
and distribution. The only question for society to settle is this
one: Which elite group should exercise this power, private,
profit-seeking bankers or State-appointed scientific credit mas-
ters?

A free market economist rejects this assessment. The con-
sumers have final earthly sovereignty over distribution, and
