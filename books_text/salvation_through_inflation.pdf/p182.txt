150 SALVATION THROUGH INFLATION

opment and modification of production, by the public acting in
their interest as individuals.'*

The formula for setting prices is not clear, is it? He refers
only to Chapter X of Economic Democracy (1920). On page 130 of
Credit-Power and Democracy, he did offer a “ratio of real credit-
production to credit-consumption.” Here is the ratio:

Capital (appreciation) issue per annum +
credit-issues (cost of goods produced) per annum
divided by
depreciation + cost of goods consumed per annum

He said that the unit governing this ratio is arbitrary. It is
not associated with gold. “The only possible standard which can
be applied with accuracy to the measurement of economic
values is that of ratio, a standard which does not require that
we postulate anything at all about the unit used to establish the
ratio, except that it is the same unit.”

Problems With the Formula

Think of the problems here. First, where do the central
bankers get the money in order to make a loan? Not from
depositors. There is no discussion in Social Credit of voluntary
deposits into the State bank. To allow private lenders (bankers)
to control the supply of credit would transfer sovereignty over
credit to consumers. This is what Social Credit opposes. Thus,
in Social Credit, the presence of lenders — people willing to do
without present consumption — is not to become the basis of
borrowing. Instead, a government bank creates credit: fiat money.

Second, is this ratio historical or predictive? Does the ratio
express only the capital that has been consumed over the last 12

14. Credit-Power and Democracy, pp. 92-93.
15. Jbid., pp. 180-31.
