Preface xxix

writings of Major Douglas promote the free market, economic
productivity, and sound money. Social Credit economics, as
originally formulated by Major Douglas, does nothing of the
kind. After you read this book, you will better understand why
I am so adamant about this. I am writing this book in order to
warn conservative people who have been misled about the true
message of Major Douglas.

There will, of course, be attempted refutations of my book,
mostly in the form of lengthy book reviews in low-circulation
newsletters. Once again, let me remind the reader (and my
future critics) that I am responding to Major Douglas, not to
his well-meaning but economically untrained disciples. This
may displease certain of my critics who feel slighted because I
have paid no attention to them, but my strategy is deliberate. Lf
Major Douglas was wrong, they surely are wrong. I do not have
to refute any of them if I can refute him. (Still, if one of the
movement's leaders responds with a book, and if he has the
blessings of his peers, I shall respond: see Appendix B.}

More to the point, J do not have to persuade them if I can per-
suade you. Martin Luther did not persuade a single Pope to join
the Protestant Reformation, but he did persuade many follow-
ers of many popes. We should not expect the “popes” of any
movement to be persuaded by their movement's critics. Leaders
of a movement have too much to Jose psychologically for them
to be persuaded by critics, no matter how tiny their movement
is. My failure to persuade them is neither here nor there; my
failure to persuade you, however, would be most unfortunate.

The Emperor’s New Clothes

When you read refutations of my book written by this or
that representative of Social Credit, please keep in mind the
story of the emperor who had no clothes. Although he insisted
that he was well-dressed, and the clothing makers also insisted
that he was well-dressed, and all the people except one small
boy insisted that he was well-dressed, he was not well-dressed.
