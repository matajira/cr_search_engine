Major Douglas’ A + B Theorem 261

the spending of Group A plus the spending of Group B. The
money income fo each group equals the money outflow from
each group, assuming that people in Group B do not decide to
light their cigars with the paper money or checks they receive
from those producers who buy factors of production. Even if
they did use all their money to light cigars, this reduction in
the total money supply would lead to lower prices for the rest
of us. Wage-receiving workers could then afford to buy addi-
tional goods and services. There is still no break im the flow of
money payments. Besides, ail of those rich cigar-smokers would
eventually run out of cigars. When they at last stopped burning
money and started buying more cigars, this would end the
supposed break in the flow of money, even if there were one,
which there isn’t.

As I have shown, the A + B Theorem has nothing to do with
Douglas’ other explanation of the break in the flow of pay-
ments: his theory of private banking, producer loans, and the
extinguishing of money upon debt repayment. The two analy-
ses are conceptually different. Douglas made no attempt to fit’
them together into a coherent discussion of capitalism’s sup-
posed tendency to underconsumption. Nevertheless, he still
proclaimed a single cause of capitalism’s crisis: insufficient
funds.

Without a single cause, there is no need for a magic pill.
Without a psychological need for a magic pill, nobody will
embrace Social Credit as the solution to society’s ills. So, the
defenders of Social Credit cling fanatically to Major Douglas’
imaginary single cause. They need a market for the magic pill
that Social Credit’s proposed credit reform alone can provide.

The weavers who sold the emperor his invisible clothes knew
it was all a hoax. They were in it for the money. In contrast,
Social Credit’s defenders honestly believe they are in the magic
pill business. They honestly believe in the A + B Theorem. My
suggestion to the reader: don’t buy any magic pills. There are
