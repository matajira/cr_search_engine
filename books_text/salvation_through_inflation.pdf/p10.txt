x SALVATION THROUGH INFLATION

Credit movement and has been persuaded (though not by
reading the complete works of Major Douglas) that Social Gred-
it economics is Christian. He has been woefully deceived about,
this. I see my task as that of instructor in biblical economic
truth. What the Bible teaches about economics in general and
monetary policy specifically is utterly opposed to Social Credit
economics. I intend to prove this to him. This may be you.

I receive no money from the sale of this book. I wrote it free
of charge as a service. Why? As we ask in the United States:
“What's the catch?” There is no catch. I was trained to be a
teacher. Teachers teach. Once they begin a career teaching, it
is difficult for them to stop. But unlike most teachers, I teach in
front of a computer screen, not in front of a classroom.

I have devoted my adult life to studying the Bible. I have
also devoted much of my career to studying economics. Having
learned some things, I feel compelled to teach them to others.
The things I have learned are simple, such as the old rule,
“You rarely get something for nothing,”® and its corollary,
“you sometimes get nothing for something.” As a businessman,
I have learned that the solutions to personal poverty are hard
work, long hours, personal thrift, honesty, and a commitment
to serve others by selling them what they want at a price they
can afford. Prayer is also a gooda idea. These are also the solu-
tions to world poverty. I have learned, above all, that there are
no shortcuts to lasting wealth. This principle applies to societies
as well as to individuals.

2. The exception is salvation, a free gift of God. “For by grace are ye saved
through faith; and that not of yourselves: it is the gift of God: Not of works, lest any
man should boast” (Ephesians 2:8-9). But salvation ceases to be free once it is freely
received. “For we are his workmanship, created in Christ Jesus unto good works,
which God hath before ordained that we should walk in them” (Ephesians 2:10).
Grace is a form of debt. We receive more from God than we can ever pay for, but we
are nevertheless required by God to pay what we can in order to verify the reality of
our faith. “But wilt thou know, O vain man, that faith without works is dead?” James
2:90). “Ye see then how that by works a man is justified, and not by faith only”
(James 2:24),
