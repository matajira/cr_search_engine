Dividends Under Capitalism 169

would issue fiat money based on the monetary value of the
nation’s assets. This would replace the present system of corpo-
rate dividends to investors. In other words, Douglas adopted
the same word, dividend, to describe two radically different
systems: one compulsory and the other voluntary; one govern-
ment-mandated and the other a matter of private decision-
making. One system is based on a person's decision to turn his
money over to a corporation in the hope of future income and
capital gains, while the other is based on the government's issu-
ing of fiat money in terms of a statistic: the estimated money
value of all the assets inside the nation’s borders.

Machines and Productivity

Major Douglas argued that “the increased utilisation of me-
chanical power and machinery . . . tends to contract the area of
the distribution of wages.”* Why should this be true? Some
people are released from a particular type of service by the
substitution of a machine, but humans are remarkably versatile.
They are not highly specific capital goods. In short, men are
not machines. The fact that a man loses one job to a machine
does not mean that he cannot get another job, such as building
more machines or repairing machines or selling the products of
machines. If machines replaced men in general rather than
specific men working on specific tasks, hardly anyone would be
employed today. Machines in Douglas’ view are like slaves prior
to the mid-nineteenth century: low-paid servants.

But don’t machines replace men? Yes: specific men in speci-
fic jobs. But machines also make men more productive. For
example, a bulldozer can be used to build a road, and the road
opens up new markets. If we argue that bulldozers lead to
unemployment, shouldn’t we conclude that the government
ought to make the use of bulldozers illegal in order to further
the hiring of lots of men using shovels? Taking this even far-

9. Credit-Power and Democracy, p. 43.
