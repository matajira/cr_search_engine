The Origins of Social Credit 35

One question the reader needs to get answered is this: Did
Major Douglas advance beyond the stage of “untrained amateur
economist” after 1917? His economic ideas must stand on their
own. His defenders cannot legitimately appeal to his supposed
engineering skills as in some way validating the scientific nature
of his economics. Why not? First, because there is no record of
his engineering work. Second, and far more important, because
engineering is a completely different science from economics. It
deals with inanimate objects, not human action. Human action
involves personal responsibility before God and man. If men
were machines, the two sciences might be linked, but man is
not a machine. We should not speak of the “mechanics of soci-
ety” except in the loosest metaphorical sense.

Douglas presented his first essay on economics in an engi-
neering journal, the Organizer, in 1917. This was an obscure,
limited-circulation journal. The editor of this journal, Holbrook
Jackson, then introduced Douglas to his old friend, A. R. Or-
age, editor of New Age. Meanwhile, Douglas had two articles
published in the English Review in 1918 and two more in 1919.
In January, 1919, Orage republished Douglas’ December Eng-
ish Review article. New Age delivered a far larger audience to
Douglas than he had previously enjoyed.'* New Age continued
to publish Douglas’ articles during the 1920's. This journal was
his primary outlet in the initial period of Social Credit.

‘Who Was A. R. Orage?

New Age had begun in 1894, when it was distinctively Chris-
tian in tone. Its circulation rose to 56,000. A new editor took
over in 1898, a socialist, Joseph Clayton. Circulation then fell.
He sold it in .1907."5 Orage immediately took over as editor of
the weekly journal. Its circulation continued to fall: from 23,500

14, bid, p. 62.
15, Mhid., p. 63.
