Who Represents the Consumers? 91

Who are these hidden minority providers? He did not say,
at least not in this chapter. This is a shame. Since the moral
and analytical heart of his criticism of existing capitalist society
was the presumed all-powerful authority of private bankers, it
certainly would have been useful if he had identified those
hidden minority producers — the people he wanted to replace
the bankers. It would have provided greater comfort to readers
had he openly identified these true representatives of what he
called “the ultimate Terror,” meaning the democratic major-
ity that can never be trusted.

The Green Shirts

We can now identify who the victims are, Douglas said: the
little people who are being victimized by the financial thieves
who hide inside the economic system. That such an appeal has
been able to recruit followers to what is sometimes called the
Populist fringe is well known. The Populist movement in the
United States used just this sort of appeal in the late nineteenth
century.” This was a farmer-based political movement, whose
supporters were in debt and who wanted relief through mone-
tary inflation, either through the substitution of a silver stan-
dard for the gold standard or the substitution of a fiat money
standard.” Father Coughlin, a popular Roman Catholic radio
preacher in the late 1930's, recruited millions of listeners with

22, Ihid., p. 7.

28. Norman Pollack (ed.), The Populist Mind (Indianapolis, Indiana: Bobbs-
Merrill, 1967).

24. Allen Weinstein, Prelude to Populism: Origins of the Silver Issue, 1867-1878 (New
Haven, Connecticut: Yale University Press, 1970); John D. Hicks, The Populist Revolt:
A History of the Farmer's Alliance and the People’s Party (Lincoln: University of Nebraska
Press, [1931] 1959), ch. 18. The main publicist was William H. “Coin" Harvey, a
defender of “free silver” The main politician was William Jennings Bryan, who was
the unsuccessful Democratic Party nominee for President of the U.S. three times:
1896, 1900, 1908. See the monthly magazine, Money, published by the Money
Publishing Company of New York, 1897-1900.

 
