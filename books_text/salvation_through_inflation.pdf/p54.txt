22 SALVATION THROUGH INFLATION

grace.® This is my operating presupposition.

4. The Unity of Truth: Natural Law

The final approach also assumes that there is only one sys-
tem of truth in this world, but with this difference: the unity of
truth is based on man’s mind, not biblical revelation. If “sci-
ence” says that something is true — that is, if some scientist
publicly announces it - and this proposed truth does appear
rational or logical to the Christian observer, he concludes that
this scientific truth must be consistent with the Bible. After all,
we must not proclaim two different kinds of truth. So, he be-
gins to search for any kind of connection between the Bible and
the newly discovered truth. He seeks to validate his rationally
derived truth by an appeal to the Bible. In other words, he
seeks to baptize his scientific truth with the Bible. Sometimes
this baptism is achieved by sprinkling; in other cases it requires
fuli immersion.

The presupposition of intellectual and moral consistency
governs this quest, but the quest is governed by the presump-
tion that the scientific discovery must govern our interpretation
of the Bible. It is not that the Bible is seen as false, or that it is
seen as not speaking to the issues raised by the scientific discov-
ery. The investigator does not relegate biblical revelation to the
realm of things indifferent or things internal. Rather, he as-
sumes that the Bible's truth is subordinate to science. There is
a hierarchy of unified truth, but natural law (science) is at the
top. The truth-searcher assumes that the Bible speaks with less
precision than science or philosophy. This has been the ap-
proach of most Christian intellectuals throughout history. This
is the approach of those Christians who call themselves political
pluralists.”

6. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987).

7. For a critique of this position, see Gary North, Political Polytheism: The Myth of
