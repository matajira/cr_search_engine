282 SALVATION THROUGH INFLATION

George Knupffer’s book appeared, The Struggle for World Power:
Revolution and Counter-Revolution.™

In the 1950's, the American Mercury, which had been a liter-
ate and highly influential magazine in the 1920’s when it was
edited by H. L. Mencken, was purchased by members of what
can legitimately be called the fringe Right. Under the new
editors, it began to move toward anti-semitism. The editors also
began publishing a series of articles on the money question in
1957. These were compiled and sold as an inexpensive pam-
phlet, Money Made Mysterious (1959).

In the mid-1960’s, by far the most sophisticated of the post-
World War II efforts appeared: W. E. Turner’s Stable Money:
The Conservative Answer to the Business Cycle.” It appeared in
1966, the same year as H. E. Kenan’s self-published book, The
Federal Reserve Bank: The Most Fantastic and Unbelievable Fraud in
History. Many books and pamphlets were written by Wycliffe B.
Vennard, Sr. Omni Press published dozens of reprints of fiat
money books in the 1960's.

In the 1970's, the books continued to appear. A dentist,
Edward E. Popp, wrote Money — Bona Fide or Non-Bona Fide in
1970. The next year, Charles S. Norburn and Russell L.
Norburn used the vanity publisher (you pay, they publish)
Vantage Press to bring out Mankind’s Greatest Step: A New Mone-
tary System. Charles Norburn followed in 1983 with Honest Mon-
ey: The United States Note and in 1984 with Honest Government: A
Return to the U.S. Constitution.” Also in 1971 came June Grem’s
The Money Manipulators, dedicated to Wycliffe B. Vennard.*

In 1980, Theodore R. Thoren and Richard F. Warner pub-
lished The Truth in Money Book through Truth in Money, Inc.

24, Third edition; London: Plain-Spealer Publishing Co., 1971.
25. Fe. Worth, Texas: Marvin D, Evans Co., 1966.
26. Port Washington, Wisconsin: Wisconsin Education Fund.
_ 27. Asheville, North Carolina: New Puritan Press.
28. Freeman, South Dakota: Pine Hill Press.
