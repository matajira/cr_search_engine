Falling Prices and Capitalist Profits 123

capitalist system? His answer: bank credit.

His argument was that those who bought capital goods are
doomed to failure under capitalism unless there is a subsequent
increase in the money supply. Even if there were no increase in
the supply of goods and services, he said, the capitalist economy
would still fail. Why? Because without a constantly increasing
money supply, there cannot be profits: “Bearing this in mind,
we can understand that it is impossible for a closed community
to operate continuously on the profit system, if the amount of
money inside this community is not increased, even though the
amount of goods and services available are not increased.”*

Was he correct? No. Then what did he fail to understand?
That all of a nation’s money supply is always in someone’s hand
(cash), or his bank account (credit), or hidden under his mat-
tress (cash). It does not mysteriously disappear. I therefore ask:
Why is there an inherent shortage of money under capitalism?

Douglas’ Attempted Answer

In the 1933 edition of Secial Credit, Douglas responded to
critics who had identified this flaw in his theory. He wrote that
the “orthodox theory” of the economy “assumes that the mon-
ey, equivalent to the price of every article which is produced, is
in the pocket, or the bank pigeon-hole of somebody in the
world.”® This is exactly what orthodox monetary theory teach-
es, or at least taught until John Maynard Keynes’ General Theory
of Employment, Interest, and Money came along in 1936. After that,
Keynesianism became the new orthodoxy.

Some people save part of their total holdings of money. In
Douglas’ day, orthodox economists taught that the money sup-
ply is sufficient to clear the market of consumer goods and
services if prices remain flexible in a downward direction. This
clearing will occur even if some people spend money on capital

8. Monopoly of Credit, p. 24.
6. Social Credit, p. 83.
