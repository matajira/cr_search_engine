250 SALVATION THROUGH INELATION

course, the orthodox capitalistic view. . . .”! Consider, he said,
a “Labour corporation — for instance, a trade union ~ as being
in a position to make up the costs and consequently the selling
price of this commodity on orthodox principles.”" (If you do
not know what this sentence means, do not feel stupid; neither
I nor his disciples know what it means either.) Fact: “orthodox
principles” do not teach that a trade union makes up these
costs, unless we are talking about orthodox Marxism, which
attributes all productivity to human labor. Orthodox economics
regards labor as only one factor of production. Land is the
other.®
We come now to the heart of the A + B Theorem:

In this case using small letters, group 5 would include all the
costs of living ~ i2., the overhead charges of the men who are
the “machines” for the production of Labour ~ and group @
would be their direct remuneration, and the “factory cost” of the
commodity would again bea + b. Let us call this “factory-cost-of-
Labour” ¢. Now ¢ cannot be greater than A in the preceding
formula for material-production cost, and yet if human beings
are to buy A + B with their earnings and dividends, A + B must
be included in ¢. Again we see that this is only possible by the
inclusion of an external factor - credit — which allows an ostensi-
bly stable value ¢ to mean different things at successive intervals
of time."*

Here he finally mentioned credit. But the A + B Theorem
has nothing analytically to do with credit. It has to do with
payments made to “other organizations,” including the repay-
ment of bank loans but also including payments for “raw mater-

11. Credit-Power and Democracy, p. 22.
12. Bid., p. 28.

18, Capital is a product of land plus labor over time.

14, Ibid., p. 28. Perhaps you may disagree with me, but this is not what E call

clear writing. Let us do our best to give Major Douglas the benefit of the doubt. Let
us assume that there is some sort of logic in this argument.
