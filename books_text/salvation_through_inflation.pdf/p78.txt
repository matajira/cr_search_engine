46 SALVATION THROUGH INFLATION

Bible teaches, our external prosperity is related to our external
obedience to God’s law.

The Grace of God

How can we obey God? Only through God’s grace. The
Bible is very clear about this: grace always precedes obedience. We
do not earn our way into heaven. We do not earn God's favor.
God graciously grants us saving faith, and He also grants us
opportunities to obey him by performing good works: “For by
grace are ye saved through faith; and that not of yourselves: it
is the gift of God: Not of works, lest any man should boast. For
we are his workmanship, created in Christ Jesus unto good
works, which God hath before ordained that we should walk in
them” (Ephesians 2:8-10). In short, the Bible teaches that grace
precedes obedience. We are always in debt to God. We are
always paying off our obligations to Him.

The only reason we are granted the grace to continue to pay
is because Jesus Christ, the Son of God, died on the cross in
order to pay God full price for our sins. “But God commendeth
his love toward us, in that, while we were yet sinners, Christ
died for us” (Romans 5:8). “For the wages of sin is death; but
the gift of God is eternal life through Jesus Christ our Lord”
(Romans 6:23).

This means that while there can be no full escape in history
from the painful effects of the curse on us and on the earth,
there can and should be a progressive escape from the most
burdensome of these curses.* Why is it impossible to escape
fully from the curse of God in history? Because there cannot be
moral perfection in history, other than the moral perfection of
Jesus Christ. “If we say that we have no sin, we deceive our-
selves, and the truth is not in us” (I John 1:8).

Nevertheless, “If we confess our sins, he is faithful and just

4, Gary North, Js the World Running Down? Crisis in the Christian Worldview (Tyler,
‘Texas: Institute for Christian Economics, 1988).
