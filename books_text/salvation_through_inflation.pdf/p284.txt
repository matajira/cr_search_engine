252 SALVATION THROUGH INFLATION

I shall not ridicule this argument by commenting that no work-
er in a Rolls-Royce automobile factory is likely ever to want or
buy the visible product of his labor. This is because no worker
produces an entire Rolls Royce. He may produce a bumper, or
perhaps a headlight, but not the whole car. He has no use for
a Rolls Royce bumper or headlight.

Then what did he mean? The error in Douglas’ analysis was
that he spoke of factory payments to two kinds of recipients -
workers (A) and suppliers (B) — but he focused his attention (and
the reader's attention) solely on the money (purchasing power)
paid to the factory's workers. He ignored the money paid to the
factory's suppliers. He wrote as if spending by the workers
employed by the factory’s suppliers was nonexistent, and there-
fore the purchases made by the capitalist system’s entire work
force would not clear the market of consumer goods. Suppliers
of raw materials to factories somehow do not count in the A +
B Theorem as being part of the capitalist system. This, Douglas
said, is the central flaw of capitalism. This, he said, is why capi-
talism must eventually collapse without Social Credit.

The question is: Does any of this correspond to the real
world?

What Major Douglas Forgot: Organizations Are Fictions

Major Douglas made a fundamental mistake in his analysis.
He neglected to honor the most important task in economic
analysis. He neglected to “follow the money.”

Think about the logic of the A + B Theorem. Group A -
individuals ~ receives part of the money. Group B - organizations
~ receives the remaining part. Those who receive the wages and
dividends (Group A) do not get all of the money, he said, so
they cannot buy back the whole of their production. Thus, he
concluded, capitalism is plagued with underconsumption: waste.
“The existence of a poverty problem face to face with an unem-
ployment problem and side by side with a marvelously effective
production system ought to direct our attention unfailingly to
