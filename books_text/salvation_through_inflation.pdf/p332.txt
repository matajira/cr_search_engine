300 SALVATION THROUGH INFLATION

statistics, 61, 151, 171
stock market, 87-88
style, xii

Taft, Robert, 6
talents, 141

tariffs, 112

tax (inflation), 67
taxes, 73, 112, 214-15, 232
‘Taylor, Frederick, 109
teaspoons, 170
technicians, 95-94
tenant for life, 74-75
theater, 116
theosophy, 39
thermodynamics, 125
“thieves,” 90-91, 103
Thomas, Dylan, 38
thrift, 132-35, 152
tickets, 154-58, 166
“tickets,” 231-32
time’s arrow, 125
totalitarianism, 187
trade unions, 72-73
trespass, 44

truth, 20-21, 22

underconsumption, 13-14, 132,
243, 252-53, 165
unemployment, 169-70

unions, 72-73, 250
usury, 141
utopian, 24-27

valuation, 200

value, 159, 220

Veblen, Thorstein, 93-94
vision, 55

wages
A+ B Theorem, 247-48
Douglas on, 72-78, 177, 180-
81
Jowering, 172-73
machinery &, 169-70
Marx on, 181
United States, 171
warehouse receipt, 64, 153, 155,
156, 232
water/wine, 19
wealth, 43, 115, 173, 147
wealth formula, x
wealth redistribution, 202, 205
Welch, Robert, 5
welfare State, 78
wine/water, 19
work, 101, 144, 174-75, 211-
12
World War I, 102
