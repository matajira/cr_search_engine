86 SALVATION THROUGH INFLATION

system, since he wanted people to work much less and have
much more. He therefore also repudiated the trade unions’
claim that Labor - capital L ~ should control industry.'' Who,
then, should control industry?

To discover the answer, Douglas identified each claimant to
this authority. The foreman controls the workshop, but he
answers to the manager. The manager says that he is responsi-
ble to the chairman. The chairman says the shareholders are in
control. So, the age-old institutional question is still the
same: Who's in charge here?

Can the shareholder make decisions for the corporation? No,
Douglas answered: “. . . a shareholder in a trust-capitalistic
manufacturing enterprise has no power to change the funda-
mental policy of the concern, which is to pay its way as a means to
the end of maintaining and increasing its financial credit with the
banks.”!® The shareholder is therefore institutionally impotent.
Douglas did not mean the owner of one share of stock. He
meant shareholders in general.

Douglas was incorrect about this. His error is widely shared,
however."* Shareholders can and do remove managers who
displease them. They do this in two ways. First, they sell their
shares to different shareholders, who then appoint a new man-
agement team. As they sell, share prices fall on the stock mar-
ket. This makes it less expensive for new owners to buy up
additional shares and replace the old management team, Sec-
ond, shareholders replace managers by giving new managers
the right to vote their shares by proxy. Both approaches are
common. The first approach is sometimes ridiculed by existing
managers as “predatory” or “corporate raiding” or “vulture
capital.” They resent these corporate take-overs, for take-overs

LL. Credit-Power and Democracy, pp. 3-5.
12. Ibid., p. 4.
18. Hid., p. 6.

14, The classic statement of this error is Adolph A. Berle and Gardiner C.
Means, The Modern Corporation and Private Property (New York: Macmillan, 1983).
