12 SALVATION THROUGH INFLATION

pension of gold and silver payments by the banks.’®

In the late nineteenth century, American critics of what was
called an Eastern bankers’ conspiracy again began to gain con-
verts, Very often, these critics were members of what became
known as the Populist movement. The critics began to appear
around 1867, two years after the American Civil War (1861-65)
ended.” A bank run against gold had begun very early in the
war. Banks suspended payment in gold, and this decision was
supported by the governments of both the North and the
South. Both governments also suspended payment. Both gov-
ernments continually issued fiat money to pay for the war.'®

After the Civil War, the victorious Northern government
decided to return to stable money conditions and to re-establish
a gold standard, which was achieved in 1879." This was the
Greenback era, a “greenback” being green-colored paper mon-
ey which offered no redeemability in gold coin.” The era
really began in 1861, when the northern banks suspended
specie redemption.” This long and erratic process of with-
drawal from wartime fiat money policies led to a series of reces-
sions, beginning in 1867 and continuing through the late
1890's. It is impossible to halt the expansion of a prior period
of fiat money expansion without causing a recession,” a pain-

16. Murray N. Rothbard, The Panic of 1819: Reactions and Policies (New York:
Columbia University Press, 1962), pp. 94-96.

17. Allen Weinstein, Prelude to Populism: Origins of the Silver Issue, 1867-1878 (New
Haven, Connecticut: Yale University Press, 1970).

18. North, “Greenback Dollars,” pp. 132-47, See also Bray Hammond, Sovereignty
and an Empty Purse: Banks and Politics in the Civil War (Princeton, New Jersey: Prince-
ton University Press, 1970); Richard Cecil Todd, Confederate Finance (Athens: Univer-
sity of Georgia Press, 1954).

19. Walter T: K. Nugent, The Money Question During Reconstruction (New York:
Norton, 1967).

20. Irwin Unger, The Greenback Era: A Social and Political History of American
Finance, 1865-1879 (Princeton, New Jersey: Princeton University Press, 1964).

21. Wesley Clair Mitchell, A History of the Greenbacks (Chicago: University of
Chicago Press, [1903] 1968).

22, Mises, Human Action, ch. 20.
