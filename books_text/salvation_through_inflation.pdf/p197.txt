9

DIVIDENDS UNDER CAPITALISM

Woe unto them that call evil good, and good evil; that put dark-
ness for light, and light for darkness; that put bitter for sweet,
and sweet for bitter] (Isaiah 5:20).

Major Douglas was a reformer. He insisted that “Capitalism
in its present form has always been fundamentally bad.”' Not
kind of bad, or sort of bad, or bad in some areas, but fundamen-
tally bad. But why? By 1920, capitalism had made people far
richer than they had been in 1820 or 1720. Why is it funda-
mentally bad?

At the heart of capitalism, he argued, is a single problem, a
single evil to be eliminated: the bank credit system. This system
of credit supposedly does not provide sufficient money for
workers to buy all the goods produced by the economy. Thus,
capitalism suffers from chronic underconsumption. It is this
credit-based restraint on the system that supposedly keeps
capitalism from becoming productive beyond men’s dreams.

We have seen in Chapter 8 that Major Douglas made a
fundamental error: equating money with tickets, and then
concluding that when a bank loan is paid off, this shrinks the

1. Credit-Power and Democracy (London: Cecit Palmer, 1920), p. 40.
