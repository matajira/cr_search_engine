Sanctions: From Economics to Politics 215

trol over capital to the State: the pool of capital from which all
future dividends would be paid on an equal per capita basis.
With respect to modern taxation, he predicted that modern
economic theory was about to go through a transformation: the
disappearance of any theory of rewards and punishments.

In passing it may be noted how the power of taxation has
grown into a form of oppression beside which the modest efforts
of the robber barons of the Middle Ages must appear crude.
While the system is fundamentally based upon a theory of re-
wards and punishments, modern financial methods, in conjunc-
tion with the taxation system, would appear to suggest that the
acquisition of the reward is proper ground for the imposition of
punishment in the form of taxation which will distribute the
reward amongst those who have not worked for it. 1 have very
little doubt that in this we are witnessing not merely the decay of
the financial system, but of the whole theory of rewards and
punishments as applied to economics."

That final sentence is crucial. From Adam Smith to the pres-
ent, economists who have defended the free market system
have focused on the power of the consumer to reward some
producers by buying from them, and to punish other producers
by not buying from them. A theory of sanctions is central to all
economic theory, just as it is central to all social theory.’® It is
worth noting that Douglas did not say that rewards and punish-
ments would disappear only if his proposed reform were enact-
ed. He said that sanctions were disappearing from all forms of
economic theory. What evidence he had for this statement
remained his secret. 1 am aware of no economist who has not
suggested a system of rewards and punishments. Scarcity in a
fallen world itself is a sanction. To escape its cursed effects, men
require a system of economic sanctions: positive and negative.

18. The Monopoly of Credit, p. 72.
16. See Gary North, Sanctions and Social Theory, forthcoming.
