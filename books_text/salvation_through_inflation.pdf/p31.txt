Preface Xxxi

assertions. Do not be taken in by the old debater’s trick: “If
your case is weak, shout. If it is really weak, pound the podium,
too.” If the arguments you read ~ mine or my critics — are not
clear, or if they are not substantiated by word-for-word quota-
tions from Major Douglas, suspend judgment.

Let me put it bluntly: dike the emperoy Major Douglas were no
clothes. His disciples are equally naked. But you should not take
my word for this yet. You should read this book carefully. If
you are persuaded that I am correct, then you should also read
any responses. Remember what the Bible teaches about decid-
ing whose story is correct:

He that is first in his own cause seemeth just; but his neighbour
cometh and searcheth him (Proverbs 18:17).

Listen carefully to both sides. Then make your own decision.
But bear this in mind as you read: if one of those who pleads
his case is clear, quotes from the original sources, and does not
exaggerate his claims, while his opponent uses convoluted
language, doesn’t cite the original sources, and substitutes
accusations for simple logic, believe the first person.

If defenders of Social Credit write that I should have consid-
ered the writings of Disciple A or Disciple B, my response is
simple: if they still defend the ideas of Major Douglas as origi-
nally presented by him, then I do not need to refute any disci-
ple. If, on the other hand, he made a lot of mistakes, let the
disciple identify them, correct them, and present a restructured
version of Social Credit. 1 will respond to him. But the fact is,
Social Credit is like a religious sect. The disciples quote only the
master, I have followed their lead.

Conclusion
Let me ask you a question: Are you commitied to a search for
truth? If you are, do not become upset with anything in this
book until you have finished reading all of it. Do not pre-judge
