138 SALVATION THROUGH INFLATION

way to argue from the economic record that the gold standard
seriously inhibited economic growth. The opposite is far easier
to prove, namely, that the gold standard created the monetary foun-
dation of long-term economic growth. What created the major eco-
nomic crises were wars and the suspension of gold payments by
the banks and the governments involved. This is not what
Major Douglas wanted to prove, and more than John Maynard
Keynes wanted. On this point, they were agreed — and equally
wrong. But Keynes knew the history of English prices.*” Major
Douglas never mentioned it.

England went off the gold standard in 1931. The United
States went off the gold standard domestically in 1933 and
internationally in 1971. From 1931 until the present, price
inflation has been with the West. The fact is, however, there
was tremendous economic growth under the gold standard, as
Douglas knew but refused to mention. What he had to explain
was how this could be true if his economic theory is also true.
His A+B Theorem was designed to explain why the profit
system cannot work under capitalism. It failed.

The trouble is, even in the depression conditions of 1930 or
1935, the international capitalist economy had made the West
incredibly rich in comparison to the world of 1730 or 1830. It
did so, according to Douglas’ economic analysis, while laboring
under the oppressive burden of the commercial banking sys-
tem. Douglas never once addressed this anomaly. He should
have.

Conclusion

Major Douglas, like so many critics of capitalism (and occa-
sionally even friends), believed that capitalism could not deal
with its inherent tendency toward falling prices. He insisted

87. Keynes,A Tract on Monetary Reform (London: Harcourt, Brace, 1923), pp. 11-
12,

38. See Appendix A.
