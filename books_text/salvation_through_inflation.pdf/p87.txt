4

SOCIAL CREDIT’S BLUEPRINT

Where there is no vision, the people perish: but he that keepeth
the law, happy is he (Proverbs 29:18).

Vision is mandatory for the survival of God’s people: an
accurate view of this world and the world to come. Gad’s law is
equally mandatory: a concept of ethical cause and effect that
connects the affairs of this world and God’s judgments to come,
both in this world! and the next. Why judgments, both positive
and negative? Because there must always be personal motiva-
tion for reform, whether personal or social. Men always ask this
question: “What's in it for me and those I love?” They recog-
nize that there is no escape from sanctions,’ and they prefer
positive to negative sanctions.

Successful reform requires a blueprint. This blueprint sket-
ches the details of the final goal: the vision. There must also be
a program that links cause and effect with one's vision of the
future. There must be a way to get from here to there over
time. That is, there must be a map, or better put, a manual of

1. Kenneth L. Gentry, Jn, He Shall Have Dominion: A Postmillennial Eschatology
(yler, Texas: Institute for Christian Economics, 1992), chaps. 6, 10.

2. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed.; Tyler,
Texas: Institute for Christian Economics, 1992), ch. 4.
