Scarcity and Wealth 43

replace capitalism with socialism. The questions we must answer
are these: Which kind of monetary reform did Major Douglas
propose? Was his a socialist solution? Mercantilist-Fascist? Was
he a free market reformer? Or was he something else?

To evaluate the wisdom of any reformer’s proposals, we
need a standard. I have declared in Chapter 1 that this stan-
dard must be the Bible. We need to survey what the Bible
teaches on wealth and money before we examine the details of
Social Credit. Those who claim that Social Credit is consistent
with the Bible must prove their case from the Bible. They have
not accomplished this task, as of early 1993. I intend in this
book to prove the opposite: Social Credit is inconsistent with
the Bible, as well as inconsistent with the logic of economics.

The Biblical View of Wealth

The Bible begins with the doctrine of creation: God created
the world (Genesis 1). This means that everything that mankind
possesses is a gift of God. Christianity calls such gifts grace. So,
each person begins life as an heir of the grace of God, what is
sometimes called common grace. Jesus said of God the Father in
heaven: “He maketh his sun to rise on the evil and on the
good, and sendeth rain on the just and on the unjust” (Mat-
thew 5:45b). Men do not earn these blessings.

Oumership-Stewardship

God gave to Adam and Eve the ownership of the world:
“And God said, Let us make man in our image, after our like-
ness: and let them have dominion over the fish of the sea, and
over the fowl of the air, and over the cattle, and over all the
earth, and over every creeping thing that creepeth upon the
earth. So God created man in his own image, in the image of
God created he him; male and female created he them. And
God blessed them, and God said unto them, Be fruitful, and
multiply, and replenish the earth, and subdue it: and have
