My Challenge to Social Credit Leaders 275

escape the hard work of systematic organization and reform. It
has no blueprint except the unworkable Scottish reform plan.

If I am incorrect about the economics of Social Credit, one
of you should be able to write a book to answer Salvation
Through Inflation, line by line, argument by argument. But I
warn you: you had better make certain in advance that your
representative really does speak for your movement. I will write
a book in response. If I can refute him, I thereby have refuted
the rest of you. So, it would not be fair for the rest of you to
come back with this lame response after my response appears:
“Answering that book doesn’t count. Its author really doesn’t
represent Social Credit.”

I want to see a Foreword to your designated representative's
book: a signed statement from two leaders per nation — Eng-
land, Canada, Australia, New Zealand, and South Africa —
which affirms that his book constitutes your collective response
to Salvation Through Inflation. If there is no signed Foreword
testifying to your designation of the author’s position as the
intellectual representative of Social Credit, I shall pay no atten-
tion to the book. No one else should, either. If you cannot
agree on what is wrong with Salvation Through Inflation, then
Social Credit is not a developed economic system or movement.

Here is my challenge to all of you: choose your representa-
tive, identify publicly him as your representative in the book’s
Foreword, and the two of us will then do intellectual combat.

Please understand: my designated weapon is the book. I
expect one of you to respond in a book, not just a series of
highly selective newsletter articles written only to one’s flock.
Let your followers see if one of you can handle my arguments.
I don’t think any of you can. I now offer you an opportunity to
prove me wrong. Will you accept my challenge?

This enterprise will cost you no money. The Institute for
Christian Economics will finance it. Just have your designated
representative write a response to Salvation Through Inflation, up
to 150 double-spaced manuscript pages (Word Perfect 5.1
