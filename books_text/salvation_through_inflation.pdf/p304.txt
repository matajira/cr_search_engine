272 SALVATION THROUGH INFLATION

suade the former prophets to abandon their system of prophe-
cy? Never! They just draw up new prophecy charts. Or their
disciples do. I think it is the same with Social Credit. Thus, you
have the following options in dealing with Salvation Through
Inflation: the silent treatment, the “water torture” strategy, the
“North has misinterpreted Douglas” strategy, and the strategy
of incoherence.

The Silence Treatment

You can pretend that I did not write this book. Say nothing.
“North? Who's North? Never heard of him.” Most of my critics
over the years have taken this approach. A variation of this
strategy is this: “I will not dignify nonsense with a response.”

But I don't think you will adopt the silence strategy. Leaders
of religious cults are incapable psychologically of remaining
silent when someone attacks The Founder in a full-length book
devoted to challenging The Founder. Neither will you remain
silent. In fact, I think several of you will respond quite rapidly
in your newsletters, perhaps even coordinating your responses
just to keep your answers consistent. I think some of you will
make it your life's work to refute this book, month after month.
If this book is correct, then anyone who has read it and does
not agree with it must justify himself in public to his little band
of disciples. So, you will have to respond. But are any of you
intellectually capable of writing a whole book to refute my
book? That is the question. More about this later.

The Newsletter Version of the Water Torture

This is the approach I think you will choose. Just keep reply-
ing to me in one article after another: drip, drip, drip, until
your readers have had enough. “No more! No more! We won't
believe North’s arguments any more. Just stop it!”

The main problem with this approach is that { have written
a book, not a newsletter article. If it becomes obvious to your
