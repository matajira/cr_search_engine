64 SALVATION THROUGH INFLATION

money. It is the same problem that the French Revolutionaries
faced. The more money the government prints, the higher
prices rise. The higher prices rise, the more money people
need to make ends meet, and the more collateral for the issue
of new money the government possesses. The government then
prints more money. There is no theoretical limit to this.

If a bank issues a warehouse receipt to metal stored in its
vault, someone can redeem the note and receive a fixed weight
and fineness of gold, silver, copper, or whatever the warehouse
receipt promises to pay. If the issuer prints up more receipts
than he has in reserve, he can be forced into bankruptcy if
more people show up to redeem their notes than he has items
in reserve. This threat acts as a brake on the issue of unbacked
warehouse receipts. (In a nation that outlaws fractional reserve
banking, such unbacked warehouse receipts would become ille-
gal.)'® But there is no comparable brake for an issue of un-
backed paper money, which is not a redeemable warehouse
receipt.

Why is Social Credit regarded as conservative?

Shutting Down Private Markets

Major Douglas claimed in his 1937 lecture, “The Policy of a
Philosophy,” that Social Credit is more than a monetary reform.
He was telling the truth. His recommended reform also in-
volved the destruction of the markets for privately owned real
estate. He made this clear in clause 2: “No transfer of real
estate directly between either persons or business undertakings
will be recognised. Persons or business undertakings desiring to
relinquish the control of real immovable estate will do so to the
Government, which will take any necessary steps to re-allot it to
suitable applicants.”"®

15, Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Worth, Texas: Dominion Press; Nashville: Thomas Nelson Sons, 1986), chaps. 8, 9.

16. Social Credit, p. 206.
