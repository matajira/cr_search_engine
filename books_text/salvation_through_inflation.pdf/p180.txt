148 SALVATION THROUGH INFLATION

to redeem the claims. This is a fraudulent practice and infia-
tionary, but it is not inherent in banking. It is possible to have
banking without fractional reserves.

Fifth, the existence of bank money supposedly places power
in the hands of bankers. He should have said monopolistic, State-
licensed central bankers. Local commercial bankers have very little
authority over the economy. They respond to market opportu-
nities: taking in deposits and making loans.

Sixth, his solution: “The public, as individuals, can only
acquire control of the policy of the economic and industrial
system by acquiring control of credit-issue and price-making.
The organ of credit-issue is the bank, and the meaning of
price-making is credit-withdrawal.""' Notice his words: ihe
public, as individuals.

How can the public, as individuals, acquire such control? This
cannot be done politically by nationalizing the banks, since
politics is always representational. There must be another way
if individuals are to regain control. There is a way for voters to
achieve this goal, but Douglas believed this would not be suffi-
cient: voters must outlaw fractional reserve banking. Banks promise
to pay depositors cash on demand. Then they lend the money
on the assumption that not all depositors will demand cash on
the same day. This constitutes fraud: promising to pay on de-
mand what cannot be delivered on demand because it has been
transferred to someone else. The civil government should pros-
ecute banks or anyone else who issues an open-ended, pay-on-
demand legal claim to gold or silver or any other commodity if
that person does not possess the commodity specified in the
contract, meaning the warehouse receipt.

Tk. fbid., p. 90.

12, Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Worth, Texas: Dominion Press; Nashville: Thomas Nelson Sons, 1986). This is notan
argument against commodity futures speculation. A commodity futures contract is
not open-ended; it does not promise to deliver goods on demand. It promises to
deliver goods at a specified time in the future, and, at such time, commodity con-
