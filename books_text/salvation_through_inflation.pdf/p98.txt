66 SALVATION THROUGH INFLATION

lished a limit on this figure — no formula, no principle of law,
nothing. But 1% sounds small. For public relations purposes,
he no doubt thought this would do just fine. He called this “an.
arbitrary figure.” Arbitrary it would be! This levy was part of a
plan that he called “The Initial National Dividend,” or clause
3. He explained:

(8) For the purpose of the initial stages an arbitrary figure,
such as 1 per cent of the capital sum ascertained by the methods
outlined in clause (1), shall be taken, and a notice published that
every man, woman, and child of Scottish birth and approved
Jength of residence, with the exception mentioned in the para-
graph that follows, is to be entitled to share equally in the divi-
dend thus obtained. . . . The dividend to be paid monthly by a
draft on the Scottish Government credit, through the Post Office
and not through the banks.'*

He insisted that “no interference with existing ownership, so
called, is involved in such a proceeding.”"’ We are back to the
issuing of unbacked credit: fiat money.

He called this a dividend. What is a dividend? In the world
of private enterprise, a dividend is a payment from a business
to an owner of a share of corporate stock or other ownership
claim. A business earns a return on the capital invested, and it
pays out a portion of these earnings to investors. Instead of
borrowing money from a bank, businessmien decide to sell a
portion of the ownership to investors. A bank insists on pay-
ment. The payment of dividends, however, is left to the discre-
tion of the business’ owners. People buy shares for two reasons:
receiving dividend payments and receiving capital gains. Capi-
tal gains are increases in the market value of the shares.

‘There are limits to dividends. The main one is the solvency
of the company. There is a relationship between the investment

18. ibid., p. 207.
19. Bid., p. 207.
