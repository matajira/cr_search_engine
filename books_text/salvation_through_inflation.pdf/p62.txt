30 SALVATION THROUGH INFLATION

reform, I have quoted him at considerable length in this book.
The critic must not create stick men to destroy in place of real
men. The reader needs to be confident that I am not distorting
what Major Douglas wrote. If we are to take him at his word,
we need to know what his words were. While this strategy of
extensive citation has made this book longer than I would have
preferred, I think it will make the book more believable, even
though it will take longer to read.
