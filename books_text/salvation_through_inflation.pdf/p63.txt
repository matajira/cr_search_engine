2

THE ORIGINS OF SOCIAL CREDIT

That we henceforth be no more children, tossed to and fro, and
carried about with every wind of doctrine, by the sleight of men,
and cunning craftiness, whereby they lie in wait to deceive (Eph.
4:14),

Major Clifford Hugh Douglas (1879-1952) is presented by
some of his recent disciples as a man who offered an inherendy
Christian reform of capitalism. They insist that he was not a
socialist and that his economic system does not produce any-
thing like socialism. We need to test these claims. We must
begin with a brief study of the man and his early disciples.

Douglas launched Social Credit in 1917. He served as an
engineer in the Royal Flying Corps during World War I. Social
Credit eventually became a political movement. It attracted
some remarkable people during its first two decades — some of
the oddest remarkable people in England. With only one possi-
ble exception, none of them was an economist.'

1. The exception may have been Georges Henri-Levesque. He wrote a 20-page
pamphlet, Social Gredit and Catholicism, in 1936. According to Eric Butler, who wrote
a later introduction which was published in the 1967 reprint by Omni Publications,
Hawthorne, California, the author was a graduate of the Schoo! of Social and Political
Sciences in Lille, France, and served as Professor of Economics at Laval and Montreal
Universities.
