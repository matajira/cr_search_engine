Major Douglas’ A + B Theorem 259

the nation. The national government then appoints an elite
group of credit masters who will decide which privately owned
businesses will receive credit. (Interestingly, he never discussed
where consumer credit would come from. With all the private
commercial banks shut down by the government, who would
issue consumer loans? Perhaps Douglas believed that the Na-
tional Dividend payments would replace consumer credit.)

Money paid today to “organizations,” as he put it, would still
have to be paid to the same kinds of organizations after his
proposed reform: payments for raw materials and “other exter-
nal costs.” Money will be paid to businesses. There will still be
bankers to repay: the government’s credit masters. There will
still be payments to the owners of capital: the national dividend.
As today, these payments will be made to individuals; organizations
are a legal fiction.

The Flow of Funds After “The Revolution”

Capitalism’s problem, as he explained it, is not a shortage of
money as such; rather, it is a problem of distribution: the
breakdown in the flow of payments to individuals, i.c., consum-
ers. So, the critic of Social Credit has a right to ask this ques-
tion: Why won't the A + B Theorem also operate on the far
side of Social Credit’s reform? The critic is also entitled to ask
this question: Why didn’t Major Douglas ever discuss this prob-
lem in those passages where he brought up the A + B Theo-
rem? In short, why does the supposed dilemma of the A + B Theorem
apply only to taday’s capitalism?

If the answer is “bankers,” then the defender of Social Credit
must answer this question: Why won't Social Credit’s money
masters also have to be paid? What about raw materials owners,
whether public or private? Defenders must also answer this
question: Why is interest paid by private banks to their depositors any
more of a break in the flow of payments than Social Credit’s
automatic dividend paymenis to individuals? Why is money received
from interest payments today a break in the flow of funds, but
