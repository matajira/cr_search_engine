190 SALVATION THROUGH INFLATION

with good intentions. The question is: What built-in legal safe-
guards does the proposed reform possess that will insure that
the reformer’s good intentions will become social realities?

Consumers’ Control Over Credit

Major Douglas stated, but could hardly prove, that his sys-
tem would produce the consumers’ control over credit. 1 ask:
Which consumers? How? Under free market capitalism, the
supply of credit is determined by competition. Lenders com-
pete against lenders to supply funds, while borrowers compete
against borrowers by offering future rewards to lenders. The
result of this bargaining process is the rate of interest. The
lenders must assess the likelihood that the borrowers will repay
the loans. These loans may be consumer foans or business
(producer) loans. If they are producer loans, then the future
consumers of the goods produced will determine which pro-
ducers will be able to receive additional loans. Consumers decide
who fails and who succeeds in business. This is what the free market
economist means when he says that consumers control credit.
They control the potential borrowers’ eligibility for credit.

This is not what Major Douglas meant. He meant that voters
will authorize politicians who will appoint bureaucrats who will
determine who gets business loans. The bureaucrats will decide
which manufacturer is producing desirable goods, and which is
producing unneeded luxuries. The bureaucrats’ tastes and
forecasts will determine who gets the money to produce goods
and services, not the economic forecasts of bankers whose
banks’ survival is on the line. An elite of unelected bureaucrats
will make these investment decisions.

Elitism
Douglas was an elitist, and said so repeatedly. In the 1934

edition of his book, Warning Democracy, he wrote about the
great evil of “the continuous extension of the voting franchise,
