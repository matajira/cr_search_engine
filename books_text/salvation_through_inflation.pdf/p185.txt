A False Prescription 153

Centralizing Power

What is clear is that Social Credit does not reduce State
power; it increases it. It does not return sovereignty to consum-
ers by preventing the fraud of fractional reserve banking; it
removes sovereignty from consumers and transfers it to a group
of central planners in the banks.

Warning: don’t “throw the rascals (bankers) out” until you
have replaced them with someone reliable, namely, individuals
who are in control of their own decisions. Don’t exchange one
set of power-seekers for another.

Douglas called for an economic system in which “the public”
is sovereign. But here is the problem: he defined the public
politically, not economically. He offered a political solution: the
public ~ political — control over credit. He did not call for free
market control of credit, with the State serving only as a police-
man to prevent fraud, such as fractional reserve banking. In
the name of individualism, he called for statism:

But by controlling both credit-issue and price-making the public
acquires control of policy with all its attributes - the effective
appointment and removal of personnel, amongst others. The
essential nature of a satisfactory modern co-operative State may be broad-
ty expressed as consisting of a functionally aristocratic hierarchy of
producers accredited by, and serving, a democracy of consumers.!”

“Accredited by”? This means licensed by the State. A democracy of
consumers needs no State manipulation of credit and banks. It
requires only the legal right to. make bids to buy and sell. If a
man wants to loan someone else some money, this is allowed. If
someone wants to store an ounce of gold in a warehouse for a
fee and issue a receipt for this single ounce of gold (and no
more), this is allowed. If a person wants to put his money in a
bank and have the banker lend it out at interest, this is allowed.

17. Credit-Power and Democracy, p. 94.
