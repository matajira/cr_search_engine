The Origins of Social Credit 39

to his critics as the Red Dean of Canterbury. He served as Dean
of Canterbury for over three decades, 1931-1963. He was on
the Board of The Datly Worker, the Communist newspaper. He
wrote a series of defenses of Communism, including Soviet
Success (1947) and China’s New Creative Age (1953). A few years
after he preached at Orage’s funeral he was hired by the Fabi-
an socialist publisher Victor Gollancz to be the general editor of
the Christian Book Club.” This was the follow-up to Gol-
lancz’s hugely successful Left Book Club, begun in 1936."
Dean Johnson's own book, The Socialist Sixth of the World (1939),
went through a remarkable twenty printings by 1944. In that
book, he reminisced about the influence of Major Douglas in
his life:

It was at this time [the early 1920's], with these new interests,
that I came across Major Douglas and the Social Credit Move-
ment, perceiving at once what appeared to me to be the essential
correctness of his analysis and its bearing on social problems. If
later I have moved on to other solutions, it has been on moral
and practical rather than technical grounds, and because a wider
horizon had, in the meantime, opened up. Social reformers will
always owe a debt to Douglas.”

This praise came from the most notorious clerical sympathiz-
er of Stalin in the West during the twentieth century. It makes
a conservative wonder just how conservative Social Credit really
is. It makes a Bible-believing Christian wonder just how Chris-
tian Social Credit really is. Lf Social Credit is Christian in the
way that Hewlett Johnson was Christian, then Bible-believing
Christians will have problems with Social Credit. A word to the
wise is sufficient.

$5. Rose L. Martin, Rebian Freeway (Chicago: Heritage Foundation, 1966), p. 55.
36. Paul Johnson, Mntellectuals (New York: Harper & Row, 1988), p. 279.

87. Hewlett Johnson, The Socialist Sixth of the World (London: Victor Gollancz
Ltd., 1944), p. 40.
