280 SALVATION THROUGH INFLATION

petitions favoring the program." He was threatened with a
30-day jail sentence for his activities, but President Roosevelt
signed his pardon." Father Charles E. Coughlin attracted mil-
lions of listeners to his radio broadcasts. His publications in-
cluded The New Deal in Money (1933); A Series of Lectures on
Social Justice (1935);'° and Money! Questions and Answers
(1936)."7

I hesitate to mention the writings of the bizarre Alfred Law-
son, whose Lawsonomy movement gained followers throughout
the 1930's. He had begun to write as early as 1904: Born Again.
His 1931 booklet is representative of the decade’s fiat money
reformers: Direct Credits for Everybody. Lawson's books are still
kept in print by his disciples.

An important political figure in this period was Republican
Party Congressman Louis McFadden. He had been elected to
Congress in 1914, served as Chairman of the House Banking
Committee from 1920 to 1931, and was defeated by a Democrat
in 1932." Ironically, an even more vocal critic of the Federal
Reserve was soon to become Chairman of the Banking Commit-
tee, this time a Texas Democrat: Wright Patman. He served in
this position until his death in March, 1976. He was responsible
for the detailed three-volume public hearings on the Federal
Reserve System published by the Banking Committee in 1964,
The Federal Reserve System after Fifty Years, and The Federal Reserve
System, a study prepared for the Joint Economic Committee of
Congress, published by the Joint Economic Committee just
after his death. ({ joined the staff of Congressman Ron Paul, a

14. Francis Townsend, New Horizons (An Autobiography) (Chicago: J. L. Stewart
Publishing Co., 1948), p. 207.

18, Dbid, p. 211.

16, Both published by the Radio League of the Little Flower, Royal Oak, Michi-

n,

* 17, Royal Oak, Michigan: National Union for Social Justice.
18. Collected Speeches of Congressman Louis T. McFadden (Hawthorne, California:
Omni Publications, 1970).
