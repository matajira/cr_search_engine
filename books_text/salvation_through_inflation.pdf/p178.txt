146 SALVATION THROUGH INFLATION

the consumer. This is the belief of almost all critics of the free
market. He said that millions of people owe their livelihood to
the armaments industry. “That is to say, the producer controls
the consumer.”* But how does the producer control the con-
sumer? There is surely a consumer of armaments: the State.
The State can make these purchases because it taxes the real
producers, that is, income-earning people. The State takes a
portion of the fruits of their labor and spends it on weapons.
This does not prove that the producer controls the consumer
under capitalism. On the contrary, the consumer — in this case,
the State - controls the producer: the armaments industry.
Only in the sense that the armaments industry may have per-
suaded politicians to buy more armaments can we conclude that
the producer controls the consumer.

This inversion of the free market’s system of consumer sov-
ereignty can be accomplished only by thwarting the free market
through empowering the political process. Voters substitute the
State for the free market. It redistributes wealth by compulsion.
Sometimes this is legitimate. Societies do need national defense.
But let us not blame capitalism for this political decision to buy
armaments. Let us not say that the producer controls the con-
sumer.

A False Diagnosis

Major Douglas offered a five-point diagnosis of society's
economic problem. Then he offered a one-point prescription:
fiat money issued by an elite group of credit masters.’

He made a series of assumptions. First, cooperation and real
capital are the outstanding features of the machine age. This is
correct, but he might better have called this the social division
of labor.

6. Ibid., p. 88.
7. See Chapter 10, below.
