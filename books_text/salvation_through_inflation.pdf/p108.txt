76 SALVATION THROUGH INFLATION

quite true: ownership under God does “vest in the individual.”
But the problem for Douglas was that his system did not recon-
cile his view of communal ownership — no real estate sales, no
private banking, price controls, etc. - with this rare defense of
individual ownership.

Douglas began his economic analysis with a false socialistic
premise: “Natural resources are common property, and the
means for their exploitation should also be common proper-
ty.> What are these means? Anything bought and sold
through the use of credit, including human labor. Further-
more, Douglas maintained that improvements in economic
productivity must be distributed to the entire community, not
just to the innovator and those consumers who buy from him.
In his first book, Douglas announced that “all improvements in
process should be made to pay a dividend to the communi-
ty." This is the heart of economic error of Social Credit: it
would put the nation on the dole. Entrepreneurship would die.

The free market rewards consumer-satisfying innovators with
profits. Profit is a residual that remains after all costs have been
paid for. Profit stems, as we have seen, from the fact that some
entrepreneurs recognize that, in terms of future demand, cer-
tain producer goods are underpriced today. They buy these
underpriced resources, produce consumer goods, and sell them
to the highest-bidding consumers. This is the basis of economic
progress: the quest for profit forces producers to cut costs and find
better ways of serving consumers. Remove the profit motive, and
you reduce the innovation motive. Also, you remove the “ham-
mer” that the consumer holds over producers: the right to buy
from someone else, thereby producing losses for the inefficient
producer. Social Credit removes this hammer from the consum-
er and places it in the hands of the State’s credit masters. Social
Credit also removes savers’ and investors’ control over business

88. Economic Democracy, p. 112.
89. Ibid., p. 103.
