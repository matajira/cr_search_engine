98 SALVATION THROUGH INFLATION

important.” Yet in his 1930 definition, he never mentioned
rate. Why this became important in the interim, Douglas did
not say. This second definition is present-oriented: actual deliv-
ery. The 1930 definition related to capacity: potential delivery.
The two concepts are different.

Neither potential capacity nor actual capacity can be ascer-
tained by a committee, government or private. Amazingly,
Douglas admitted this: “Centralised financial credit is a technical
possibility, but centralised real credit assumes that the desires
and aspirations of humanity can be standardised, and ought to
be standardised. . . . [No man, or body of men, however elect-
ed, can represent the detailed desires of any other man, or
body of men.” This admission is crucial; it effectively des-
troys the theoretical possibility of a group of technicians who
can coordinate Real Credit with State credit. In short, this admis-
sion destroys Social Credit’s number-one reform proposal, the technical
heart of Douglas’ system. This admission left Douglas without any
working definition of real credit. Yet the concept of Real Credit
was crucial to his critique of capitalism, which he said results
from a discrepancy between Real Credit and financial credit.

The Achilles Heet

From a practical standpoint, this failure of Douglas to specify
exactly how Real Credit can be calculated scientifically is the
Achilles heel of Social Credit. If the State's credit masters can-
not identify exactly how much Real Credit there is in society,
and then match exactly the issue of investment money and the
national dividend so that the total money supply equals Real
Credit, the proposed monetary reform collapses. It becomes
nothing more than a slogan for scientifically unrestricted issues
of fiat money. It places the power of money creation and capital
investment into the hands of the State, but without providing a

39, The Monopoly of Credit (London: Chapman & Hall, 1931), p. 21.
40. Credit-Power and Democracy, p. 57.
