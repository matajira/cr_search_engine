118 SALVATION THROUGH INFLATION

not have perfect knowledge. We may hesitate to buy or sell
because we expect to get a better deal shortly. When this hope
is thwarted long enough, we will usually make the deal.

Producers do not set prices except tentatively, to test the
market. The consumers are sovereign. If consumers refuse to
buy at listed prices, it does not matter that producers have the
legal authority to set prices. A listed (i.c., hoped-for) price is an
advertisement to sell; until a sale takes place at the legally an-
nounced price, this price is nothing more than an offer to sell.
Only a consumer can translate this offer into a realized sale.
Thus, the consumer is sovereign, not the producer. The con-
sumer has the ability to say “no.”

Summary

1. To discover economic control, follow the money.

2. Douglas claimed that capitalism operates at 5 percent of
maximum efficiency.

3. There is no proof of any such estimate.

4. He blamed the distribution system for this failure.

5. The problem is a lack of purchasing power.

6. This criticism is wrong.

7. The reason goods do not sell is as follows: (1) buyers refuse
to buy at the price asked by the sellers; (2) sellers refuse to
lower the price. (Same argument, stated two ways.)

8. Government causes economic contractions: prior monetary
inflation, tariffs and import quotas, price floors, and new taxes.
9. This shrinks markets and therefore shrinks productivity.

10. There are six things a seller can do when an item does not
sell.

11. A permanent lack of sales has nothing to do with a lack of
credit, but instead with a failure of sellers to lower prices.

12. Printing money will not increase everyone’s wealth.
