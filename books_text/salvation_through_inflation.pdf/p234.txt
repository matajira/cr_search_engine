202 SALVATION THROUGH INFLATION

not just that injections of fiat money tend to raise prices in
general (or keep prices in general from falling); it is that these
injections of new money raise some prices sooner than others.
Those people who get their hands on the new money first have
a competitive advantage over those who get access later, after
prices have risen, after resources have been bought by those
who got access earlier. This is the issue raised by Professor
Mises in 1912," by Professor Knight in 1921, and by Pro-
fessor Hayek from 1931 through the 1970's."* (Hayek died in
1992, still writing: an amazingly long and productive career)
Speaking of monetary inflation, Knight wrote in 1921:
“When inflation occurs, therefore, purchasing power is not
created, but merely transferred from the previous owners of
circulating medium to the persons into whose hands the new
currency is placed for its first expenditure. The enormous role
played in history by inflationism and the persistence of the
heresy rest upon the fact that the effects of the expenditure of
the new money are more conspicuous than the diminished
effects of that which already existed.”"“ Monetary inflation does
not create wealth; it merely redistributes it. We can easily identify
the winners; we tend to ignore the losers, at least during the
early stages of an economic boom — a boom created by false
signals, namely, lower interest rates produced by the injection
of new fiat money. At the end of the process, most people lose.
Monetary inflation is not economically neutral. Its price
effects are not simultaneous. It creates winners and losers over
time. We see winners in the early stages of the boom; we see a

 

31. Ludwig von Mises, The Theary of Money and Credit (New Haven, Connecticut:
Yale University Press, [1912] 1953), pp. 159-43.

82. Frank H. Knight, Risk, Uncertainty and Profit (New York: Harper Torchbooks,
[1921] 1965), p. 166n.

83. FA. Hayek, Prices and Production (London: Routledge & Kegan Paul, 1931);
Hayek, Monetary Theory and the Trade Oycle (New York: Augustus M. Kelley, [1933]
1966); Hayek, A Tiger by the Tail, edited by Sudha R. Shenoy (London: Institute of
Economic Affairs, 1972).

34. Knight, Risk, p. 166n.
