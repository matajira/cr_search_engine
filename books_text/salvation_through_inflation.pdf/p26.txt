Xxxvi SALVATION THROUGH INFLATION

am a Consumer.”® It is all well and good for a political move-
ment to enlist the skills of amateurs to write tracts, but if during
its entire history spanning seven decades the movement has
been unable to attract or train up a single professional econo-
mist to defend what is explicitly a program of economic reform,
then that political movement is not in a strong position. It is
unlikely ever to be taken seriously by a majority of voters. It
will have to content itself with attracting people who are inter-
ested in devoting their time and their emotional reserves to
hopelessly lost causes. This cause was lost by 1939. Its name
lingers on in a Canadian political party, but its original idea no
longer has a significant audience. My view is that life is too
short to pursue a lost cause that was lost for a good reason.

Competing Groups Without an Integrated System

There is another reason for my refusal to deal with the later
disciples of Major Douglas. I am not sure which ones are the
important ones. Some are in Canada. Others are in Australia.
There are Social Credit leaders in New Zealand, South Africa,
and England. None of these organizations has much political
power. There is no single Social Credit organization that can
offer a believable claim to be the best representative of the
Social Credit tradition. I never shoot at a blurry target.

Men claiming to be the heirs of Major Douglas’ vision have
offered many interpretations of what Major Douglas really
meant, Problem: they do not agree with each other about this
“true meaning.” (Perhaps they will all now be able to agree on
one point: that my book is simply terrible. The question is: Will
they agree on the specific economic reasons why my book is
terrible? Who knows, maybe my book will at last bring a little
unity to the fragmented Social Credit organizations!)

5. Maurice Colbourne, The Meaning of Social Credit (4th revised edition; Edmon-
ton, Alberta: Social Credit Board, 1935), pp. 9, 10.
