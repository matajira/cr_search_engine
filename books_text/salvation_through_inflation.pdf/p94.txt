62 SALVATION THROUGH INFLATION

semi-manufactured. materials.” Now get this: “No distinction
between public and private property."

Printing Money

But he did not stop there. We human beings are also a form
of capital. So, he said, “Add to this the sum representing the
present commercial capitalised value of the population.”
What are you worth, my friend? How would any government
agency estimate what you are worth? More to the point, once
estimated, what use will the agency make of this estimation?
Douglas recommended the following use:

From the grand total thus obtained a figure representing the
price value of the Scottish capital account could be obtained.
Financial credit to any equivalent can be created by any agency
such as a Scottish Treasury empowered by the Scottish peo-
ple.

Understand what this means. Douglas recommended the
monetization of the nation’s estimated capital. That is, the govern-
ment should create spendable money on the basis of this statis-
tical estimate of the total wealth of the country. Some percent-
age of the total value of the nation’s capital should then be used
to authorize an issue of new money.

This was an old scheme by the time Douglas proposed it.
Early in the French Revolution, the government confiscated all
the land and buildings owned by the monasteries, and later it
confiscated the estates of people who had fled France. Then the
government issued paper money, with the confiscated property
serving as collateral. This is not to say that these pieces of paper
were redeemable in land. They were not. Frenchmen could not

11. Social Gredit (8rd ed.; London: Eyre & Spouiswoode, 1933), p. 205.
12, Ibid, p. 205.
13. Jbid., pp. 205-6,
