XXX SALVATION THROUGH INFLATION

He was not dressed at all. Be sure that those defenders who
insist that Major Douglas was well-dressed are not themselves
like all those people along the highway when the emperor
paraded by: self-deceived.

To defend yourself against economic nonsense disguised as
a refutation of this book, ask yourself the following questions
when you read my book and also the refutations:

“Does the author extensively quote Major Douglas?”

“If he doesn’t, why not?”

“Do the quotations prove the author’s case (pro or con)?”

“Does the author make sense?”

“Does Major Douglas make sense?”

“Do I really understand all this?”

“Tf not, is this my fault or the author's?”

“Might it be Major Douglas’ fault?”

“Ts the author using verbiage — gobbledygook — instead of
rational arguments?”

“Did Major Douglas use gobbledygook?”

“Am I being subjected to a ‘snow job’?”

Here is my suggestion: do not believe anything that is not crystal
clear. Suspend judgment until you understand exactly what you
are being told. If you cannot summarize an author’s arguments
in your own words, then do not become his disciple until you
can. Do not become a vocal critic, either. You must be sure of what
you believe. When you can verbally summarize your opinion to
someone who knows nothing about Social Credit, you have a
grasp of the topic. If you can’t, you don’t.

The fact that one or two of Major Douglas’ disciples may
write a book responding to mine is irrelevant if the response is
not clear. If a book does not answer my arguments in a clear,
logical fashion, quoting word for word the writings of Major
Douglas in order to prove the case against me, then pay no
attention to it. Do not be deceived by big words and lots of
