Introduction 9

exempt foundations and liberal internationalism in foreign
policy. But he had not seen its connection to the Round Table
group, with the latcer’s connections throughout the Anglo-Am-
erican world. Smoot’s book sold over a million copies, mostly by
mail order. Most educated Americans had never heard of the
CFR in 1962, including the vast majority of conservatives. The
CFR had successfully hidden in plain sight since 1921. When
Skousen and Allen identified the tight connections among the
CFR, high-level American banking, American foreign policy,
and an international conspiracy originating in London, the
“troops” of the conservative movement responded positively.
The leadership, however, reacted negatively.

Antony Sutton was the author of a massive three-volume
study of the almost total reliance of the Soviet Union on im-
ports of Western technology, Western Technology and Soviet Eco-
nomic Development (1968-1973) published by the prestigious and
well-funded anti-Communist foundation, the Hoover Institu-
tion. In 1973, he wrote National Suicide: Military Aid to the Soviet
Union. This book merely extended his thesis regarding the
transfer of technology to the USSR, but it was too close for
comfort in the view of the directors of the Hoover Institution.
They fired him. Arlington House, a small conservative publish-
ing company, published the book. (He extended this thesis in
The Best Enemy Money Can Buy, published in 1986. 1 wrote the
book's Foreword.) In 1974, he shifted his focus from the fact of
this massive transfer of technology to the politics that pre-
ceeded it: Wall Street and the Bolshevik Revolution (Arlington
House). The next year came Wall Street and FDR (Arlington
House), a study of President Franklin D, Roosevelt’s connec-
tions to the New York banking establishment. But the next year
Sutton went too far even for Arlington House: Wall Street and
Hitler, published by ‘76 Press, which had published None Dare
Call it Conspiracy. Sutton kept going: Trilaterals Over Washington
T (1978) and Trilaterals Over Washington H (1982). Finally came
America’s Secret Establishment: An Introduction to the Order of Skull
