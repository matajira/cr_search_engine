72 SALVATION THROUGH INFLATION

with the necessary bill forms for treatment in this manner, with
the result chat their prices will be 25 per cent, at least, higher
than those of registered firms. (It is obvious that the larger the
discount rate can be made, the greater will be the handicap of
the non-registered firms.)"””

This is coercion, pure and simple. It is a law against those
individuals with money seeking out other individuals who want
to borrow. This law monopolizes credit transactions inside the
nation’s banks, and then it bankrupts all private banks inside
the nation. If passed and enforced, the nation’s businesses
would become totally dependent on either foreign lenders or
the national government and its paid agents, State-employed
credit masters.

Why is Social Credit regarded as conservative?

Make-Work, Plans

Clause 8 specifies: “The hours of Government offices will be
reduced to four hours per day. To meet the temporary conges-
tion of work, additional staff will be employed. . . .”* This will
be a second shift of workers doing identical work. In other
words, it mandates the training of two or more sets of bureau-
crats when only one set can accomplish the same task.

Why is Social Credit regarded as conservative?

Cutting Wages

Clause 9: “Wage rates in all organised industries will be
reduced by 25 per cent where such reduction does not involve
a loss to the wage-earner exceeding 20 percent of the sums
received in the form of national dividend. . . . Any trade union
violating a wage agreement to render its membership liable to
suspension of national dividend, and any employers’ organisa-

27. Social Credit, p. 209.
28. sbid., p. 240,
