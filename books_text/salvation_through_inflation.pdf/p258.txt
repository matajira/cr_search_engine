CONCLUSION

Unselfish aspirations, good intentions, beautiful phrases ~
none of these by themselves will affect the issue by so much as
one hair’s breadth.

C. H. Douglas (1920)!

... a bad system is still a bad system no matter what changes are
made in personnel.
G. H. Douglas (1921)?

I agree entirely with Major Douglas on these two points.
First, good intentions are not sufficient to save a bad system
from producing bad results. Second, the issue is not the moral
character or the efficiency of the personnel running the system.
The issue is the moral character of the system. By this I mean
both its moral character and its efficiency. The reason why I
reject Social Credit is because Major Douglas’ good intentions
could not overcome the really bad system he recommended as
a substitute for free market capitalism.

The overall moral issue of political economy in its broadest
sense is the economy's conformity to God’s law. Most econo-
mists do not believe this. Neither do most political reformers.
The secondary issue of political economy is the system’s built-in

1. Credit-Power and Democracy (London: Cecil Palmer, 1920), p. 85.
2. Economic Democracy (2nd ed.; London: Cecil Palmer, 1921), p. 40.
