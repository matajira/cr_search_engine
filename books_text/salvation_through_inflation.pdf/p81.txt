Scarcity and Wealth 49

Riches and honour are with me; yea, durable riches and righ-
teousness. My fruit is better than gold, yea, than fine gold; and
my revenue than choice silver. I lead in the way of righteous-
ness, in the midst of the paths of judgment: That I may cause
those that love me to inherit substance; and I will fill their trea-
sures (Proverbs 8:18-21).

The tongue of the just is as choice silver: the heart of the wicked
is little worth (Proverbs 10:20).

How much better is it to get wisdom than gold! and to get un-
derstanding rather to be chosen than silver! (Proverbs 16:16).

A word fitly spoken is like apples of gold in pictures of silver
(Proverbs 25:11).

Historically, individuals have chosen gold and silver as mon-
ey. These metals are rare. They are expensive to mine. They
are attractive physically. Coins of pure or nearly pure gold or
silver cannot be mass produced. This controls the money sup-
ply. One of the attributes of money is high value in relation to
weight. Gold and silver possess this attribute.

False Mixtures, False Weights

The Bible identifies debasement as an evil: “Thy silver is
become dross, thy wine mixed with water” (Isaiah 1:22). Isaiah
equated the mixing of dross (cheap) metals with silver and then
calling the finished product silver as a form of moral debase-
ment. He prophesied a coming judgment against Judah, and
he described it as a metallurgist’s fire:

And I will turn my hand upon thee, and purely purge away
thy dross, and take away ail thy tin: And I will restore thy judges
as at the first, and thy counsellors as at the beginning: afterward
thou shalt be called, The city of righteousness, the faithful city.
Zion shail be redeemed with judgment, and her converts with
