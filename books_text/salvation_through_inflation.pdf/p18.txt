xv SALVATION THROUGH INFLATION

credit, not just businesses, What they rarely acknowledge is that
the rise of consumer credit has accomplished just this, but
without a reform of the banking system or monetary policy.
They choose to ignore this development, perhaps because their
real economic goal is to eliminate interest from all loans: a
utopian economic demand which goes back to Aristotle.*

The intellectual roots of Social Credit are in the real-bills
school of monetary opinion. This was first pointed out by Louis
Spadaro in his 1955 doctoral dissertation, but who read it even
then? Only his dissertation advisors.®

As the reader will see, I regard the real-bills doctrine as
erroneous and fractional reserve banking as immoral: legalized
counterfeiting. Thus, 1 am also opposed to Social Credit eco-
nomics and all of its inflationary first-cousins.”

To Immunize the Elect

Why should 1 devote a book to the idea of Social Credit?
First, because the movement's primary victims today are Chris-
tians. They have been deceived. They deserve to know the
truth. Second, because in the 1930's, in the midst of a world-
wide economic collapse, the monetary reform ideas of Social
Credit and other very similar inflationist schemes spread like
wildfire in the English-speaking world. Economic errors always
spread rapidly in periods of economic breakdown. I regard this
book as a kind of inoculation for a coming period of economic
breakdown.

5. The Old Testament forbade interest on charitable loans made to fellow
believers and resident aliens, as a requirement of mercy, but it did not abolish
interest on business loans, which were not morally mandatory On this point, see
Gary North, Tels of Dominion: The Case Laws of Exodus (Tylet, Texas: Institute for
Christian Economics, 1990), ch. 23.

6, Louis Spadaro, “Salvation Through Credit Reform: An Examination of the
Doctrines of Proudhon, Solvay, and C. H. Douglas,” unpublished Ph.D. dissertation,
Graduate School of Business Administration, New York University, 1955.

7. See Appendix C, below: “A Bibliography of Fiat Money Reforms.”
