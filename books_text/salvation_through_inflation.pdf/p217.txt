10

SOCIAL CREDIT MEANS
STATE MONOPOLY CREDIT

Nationalisation without decentralised control of policy will quite
effectively instal {sic] the crust magnate of the next generation in
the chair of the bureaucrat, with the added advantage to him
that he will have no shareholders’ meeting.

C. H. Douglas, (1921)!

I regard this citation as the most accurate policy assessment
Major Douglas ever wrote. He made this much clear: without
the decentralization of economic policy-making, Social Credit is
just one more political dead end, one more call to “throw the
rascals out (and put in ours)!”

With this suggestion in his first book, Douglas presented
himself and his disciples with Social Credit’s most important
challenge: to devise the blueprint for a State-administered
alternative to private capital markets, one which would not lead
to centralized control over national capital in the hands of just
this type of bureaucratic tyrant. As I hope to show in this chap-
ter, Douglas never provided so much as a hint of how Social

1. Economic Democracy (2nd ed.; London: Cecil Palmer, 1921), pp. 27-28.
