Major Dougias’ A + B Theorem 255

work of thine hand: and thou shalt lend unto many nations,
and thou shalt not borrow” (Deuteronomy 28:12).

What do the not-so-rich need? They need ways to invest
their money as effectively as the rich do. This is what modern
capitalism has provided: stock markets, bond markets, money
markets. Thousands of mutual funds have been created since
the late 1950's. This process has democratized capitalism.

The point is, when an employer pays a worker, the worker
does something with the money. There is no break in the flow
of funds.

He Tried to Answer This Objection

In The Monopoly of Credit, Douglas responded to this objec-
tion. “Now the first objection which is commonly raised to this
statement, is that che payments in wages which are made to the
public for intermediate products [capital goods - G.N.] which
the public does not want to buy and could not use, when added
together, make up the necessary sum to balance the B pay-
ments, so that the population can buy all the consumable prod-
ucts.”"* He then referred the reader to a diagram on the pre-
vious page which supposedly explained why this objection is
false.

There was a problem with this defense: the diagram proved
no such thing. The diagram was an attempt to trace the pay-
ments made to the owners of each factor of production at each
stage of the production process. Douglas tried to prove that the
money spent today on wages will not allow wage-earners to
purchase all the consumer goods available today in the market.
Why not? Because the money spent by consumers supposedly
will not cover the manufacturers’ costs of production. (Note: he
ignored savings and interest in his diagram, a key mistake.) I
reproduce his diagram on the next page.

19. The Monopoly of Credit (London: Chapman & Hall, 1931), p. 32.
