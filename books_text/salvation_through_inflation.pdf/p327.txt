205
inflationism, 227-28
information, 111-13
innovation, 76
Insiders, 4
intentions, 189-90, 192-93, 194,
226
interest
Bible &, xviii
defined, 179
diagram, 257
theory of, 233-34
zero, xviii, 4
interest rate, xvi-xvii, 69, 135
inventory, 186, 200, 234-35,
258-59
investors, 206
invisible hand, 85

Jackson, Andrew, 11

Jastram, Roy, 137

Jesus
demons, 143-44
“formalism,” 213-14
parable of employer, 178
parable of talents, 141-42
profits, 142
representation, 87
words of, 81-82

jet propulsion, xxv

Jews, 4, 13, 220-23

John Birch Society, 5

Johnson, Hewlett, 38-39, 180

Johnson, Lyndon, 5-6

Just Price, 199, 206, 235

Keynes, John Maynard
deflation, 120

Index

295

disciples of, xxiv-xxv
Douglas &, xii, xxii, 131-32,
139, 196

effective demand, 196-97

mixed economy, 196

policy, 134-35

strange bedfellow, 15

success of, xix

thrift, 132

underconsumption, 132
Knight, Frank, 176, 202

Jaw, 21-22, 46, 63

Law, John, 227

liability, 168-69, 171

loans, 100-1, 160-61, 173-74,
190

“lubrication,” 231

Luther, Martin, xxix

machinery, 169-70

magic pill, 199, 242, 261

magic word, 81-82

Mairet, Philip, 37

majority rule, 193

Inanagers, 88-89

Marx, Karl, 5-6, 42-43, 56, 84,
149, 180-81, 194, 196, 199,
a7

metal, 233

millennium, 217-18, 224

mining, 233

miracles, 19

Mises, Ludwig von
central planning, 192
consumer sovereignty, 95
profit & loss, 26
underconsumption, 14
