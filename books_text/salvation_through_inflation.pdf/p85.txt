Scarcity and Wealth 53

copper ~ is opposed to what the Bible requires. The same holds
true for central banks and commercial banks that issue more
receipts to metal than they have in reserve, i.e., fractional re-
serve banking.

Major Douglas did not begin with the Bible. He did not
begin with the doctrine of creation. He did not begin with the
idea that all of our wealth is the gift of God. He never men-
tioned the laws of God revealed in the Bible and God’s promise
of visible blessings to those societies that obey these laws. He
recommended a monetary reform that would place the control
over money in the hands of the State. He recommended that
the State issue money and credit irrespective of any increase in
the State’s supply of gold or silver. Let me offer my book’s
conclusion in advance: Social Credit requires what the Bible expressly
prohibits.

Summary

1. Economic theory is supposed to help us understand how
wealth is created.

2. Adam Smith concluded that free societies with few govern-
ment controls over the economy produce more wealth.

3. Critics of capitalism claim that more government controls are
required to create more wealth.

4. Some critics of capitalism argue that monetary reform is the
key to prosperity.

5. To evaluate a reformer’s proposals, we need a standard.

6. The proper standard is the Bible.

7. To evaluate a proposed economic reform presumes that the
Bible has something authoritative to say about economics.

8. The Bible teaches that wealth begins with God’s creation:
grace.

8. Fora detailed discussion, see Gary North, Hanest Monzy: The Biblical Blueprint
(for Money and Banking (Ft. Worth, Texas: Dominion Press; Nashville: Thomas Nelson
Sons, 1986).
