Falling Prices and Capitalist Profits 133

he referred to traditional views of savings as ‘absurd.’ He boldly
wrote, ‘The more virtuous we are, the more determinedly
thrifty, the more obstinately orthodox in our national and
personal finance, the more our incomes will fall...” ”**
Douglas was a pioneer in this hostility to personal thrift. He
argued, as Keynes did, that thrift reduces present consumption,
and therefore makes an economic slump worse. Instead of
praising thrift because it increases the supply of consumer
goods in the future, Douglas attacked thrift because it reduces
the demand for present consumer goods. Like Keynes, he
ignored the obvious fact that saving increases the demand for
producer goods, and thereby increases income for those in-
volved in the production of such goods. Douglas wrote:

If I have an income of £500 per annum and I save, as the
phrase goes, £100 per annum of this sum, either by the simple
process of putting it in a bank, or by the investment of it in an
insurance policy, I decrease my expenditure by 20 per cent., and
I certainly provide myself with money for use at some future
time. But there is no physica! saving corresponding to this mon-
ey saving. In fact, owing to the interconnection of the financial
system with the producing system, there is probably an actual
destruction of wealth due to the fact that I do not spend the
whole of my income. More goods would have been drawn from
the shops, more orders would have been given to the manufac-
turers to replace those goods, and consequently a real ability to
produce more goods per unit of time would have been created,
probably by an extension of manufacturing facilities, had I spent
my income. But if I save my money, only one of two things can
possibly happen in the world of actualities: either goods which
have been produced will not be bought and will therefore be
wasted, or in anticipation of the fact that 1 should not buy them
they will never have been produced. That, I think, is an accurate
description of the result of financial saving and insurance, so far

25. Mark Skousen, The Structure of Production (New York: New York University
Press, 1990), pp. 244-24. He cited Keynes, General Theory, p. 111.
