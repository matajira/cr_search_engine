APPENDIX A

MAJOR DOUGLAS’
A + B THEOREM

From this disparity between purchasing power and goods avail-
able arises almost every material economic ill from which the
world suffers to-day, including in that category the imminent risk
of devastating wars.

C. H. Douglas (1934)!

From whence come wars and fightings among you? come
they not hence, even of your lusts that war in your members?
(James 4:1).

The cause of man’s predicament is sin. What is sin? It is
knowing what to do morally but then failing to do it. “There-
fore to him that knoweth to do good, and doeth it not, to him
it is sin” (james 4:17). So, we can speak of a single cause of our
problems, but it is a very broad cause. It is not some technical
omission. Tinkering with the economy will not save it or man-
kind. Neither will a single “revolutionary” transformation. But
some men still want to tinker, while others call for revolution.
What is needed is regeneration.

1. Warning Democracy (2nd ed.; London: Stanley Nott, 1984), p. 108.
