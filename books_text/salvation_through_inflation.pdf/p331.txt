innovation, 76

inventory, 200, 258-59

long-term goal, 212

magic pill, 145, 149

main problem of, 192

millennium, 217-18, 237-38

mistakes of, 244-46

mixed economy, xxi

money as tickets, 154-58

moral heart of, 205

National Dividend, 65-68,
172

National Socialism, 205

natural resources, 76

number-one criticism, 111

number-one reform, 98, 197

one man show, xxiv

one-State world, 151

philosophy, 33

political sovereignty, 153

post-reform A & B, 259-60

price controls, 70-72

private credit &, 150

profit theory, 176-77

profits, 123

profits guaranteed, 216

public interest, 194

purchasing power, 111

questions, 27-28

ratio, 150

real bills &, xv-xviii

Real Credit, 96-99, 100, 243

religious movement, xxviii

representation, 191

retirement funds, 65

sanctions, 77

scarcity, 25-26

small, xi

Index

299

socialistic, 75-78, 205
State bank monopoly, 69-70
State power &, 153
strategy of, 271-74
supreme mystery of, 249
technicians, 93-94
underconsumption, 100, 165
unique?, 228
utopian, 24-27
valuation, 200
wages, 72-73, 172-73
welfare State, 78
why Social?, 186
work, 211-12
(see also Douglas, C. H.)
socialism
Douglas &, 197
guild, 145
heart of, 188
premises, 75-78
society, 186-87
sovereignty, 87-88, 144-46, 153
(see also consumer sover-
eignty)
Spadaro, Louis, xviii, xxvii, 14-
15, 229
specialization, 113, 117
spoons, 170
State
balancing agent, 227
bureaucracy, 188-89
counterfeiting, 51
debt-free, 204
planning, 227
price control, 52
society =, 186-87
source of assets, 204-5
wealth redistribution, 205
