20 SALVATION THROUGH INFLATION

entific conclusions. This is just another example of neutral sci-
ence, they say, analogous to chemistry. They are committed to
a philosophy of neutralism, for this allows them to be local
church members in good standing and state university faculty
members in good standing. Problem: when your bread is but-
tered by the spiritual heirs of Charles Darwin, you are sorely
tempted to proclaim the nutritional value of the meal.

Other Christians immediately recognize the threat to their
faith that such a time table represents, for the Bible clearly
indicates that the creation took place about 4,000 years before
the birth of Jesus. They are willing to test the geologic prophets
by means of the Bible’s time frame. For their trouble they are
dismissed as theological neanderthals by academically inclined
Christians. Why? Because at this point they have refused to
take the familiar strategy of the “things indifferent.” They have
judged this brand of science by the Bible and have found the
former defective. They have tried the spirit of Darwinism and
have concluded: “Thou art weighed in the balances, and art
found wanting” (Daniel 5:27).

Christian Darwinians have pronounced the same judgment
against the creationists. The Darwinians have weighed biblical
chronology in the balance of the evolutionary time scale and
have found biblical chronology wanting. So, there can be no
reconciliation between the two views. One of the two kingdoms
must fall: the Bible’s or Darwin's.

The governing presupposition of the historical geologist or
Darwinian biologist who claims to be a Christian is that there
are two kinds of truth. There is scientific truth, which governs
the external affairs of men — economic, professional, and intel-
lectual - and there is also mythic or symbolic truth, which
governs Christians on Sunday morning. It is clear which truth
takes priority: the one which generates a monthly paycheck,
meaning the one which is consistent with one’s formal academic
certification, i.e., humanist-approved truth. The Bible teaches
that the earth was created before the sun, moon, and stars.
