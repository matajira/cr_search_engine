266 SALVATION THROUGH INFLATION

be reproduced verbatim without his permission. He did not
abide by the law.

Instead, without my permission, he published my privately
penned, line-by-line notes, which I had written in the margins
of his manuscript. I did not put these comments in the form of
a detailed scholarly response, ready for verbatim citation. In-
stead of revising his manuscript’s text to counter my criticisms
~ which is why I replied to him in the first place — he published
his 110-page book, The North-South Dialogue. It is a completely
different manuscript from the one he originally sent me. Mr.
Pinwill used my notes as a series of launching pads for another
round of criticism. He quoted in bold type face my brief mar-
ginal notes, as if they constituted a detailed formal response.

Jam not charging him with a moral failure. Mr. Pinwill is
probably a very fine fellow. He just knows nothing about aca-
demic etiquette and international copyright law. This is the
problem facing any movement which is run by amateurs. They
don’t know what they're doing.

I am no fool, gentlemen. I was not going to provide Mr.
Pinwill with any new launching pads by sending him additional
private letters. When J decide to reply publicly to an idea, espe-
cially to a critic of my writings, I do so in a very thorough
manner, as several recipients of my responses are willing to
verify (in private).* Had Mr. Pinwill’s book been worth the
effort, I would have responded in print in early 1991, when he
sent me a copy. I ignored it because I intended eventually to
complete a project which I had begun talking about in 1965,
when I was 23 years old: the refutation of Major Douglas. But
I kept putting off the project, year after year. I continued to do
so even after I received Mr. Pinwill’s book. Then came the
unsigned article in the New Times (October 1992).

8. See, for example, my response to the faculty of Westminster Theological
Seminary, Westminster's Confession: The Abandonment of Van Til's Legacy (Tyler, Texas:
Institute for Christian Economics, 1991).
