Who Represents the Consumers? 99

legal, theoretical, or practical way to restrict the expansion of
money other than political pressure. It substitutes the pressures
of politics - almost always inflationist - for the gold standard's
well-known pressure on bankers and government treasuries:
the threat of a run on gold reserves. It does this in the name of
monetary science — a science without the absolutely crucial
definitions of Real Capital and Financial Capital and the statisti-
cal relationship between them.

Second, if Real Credit is not capable of bring defined in
terms of something other than money, the issue of money by
the State will raise the monetary value of Real Credit. This will
make mandatory another issue of Just Price money and Nation-
al Dividend money, to keep pace with rising Real Credit value,
which will raise the monetary value of Real Credit... .

This is the fate of every monetary reform that relies on the
issuing of money by the State. No fiat money reform proposal
can separate the value of the collateral from the monetary
effects of the credit on the collateral. In this respect, Social
Credit is the representative example of all other fiat money
credit reforms that attempt to issue sufficient currency to match
the increase in output. The only way to estimate the increase in
output in a money-based modern economy is by way of a sys-
tem of statistical estimations of money prices received by pro-
ducers. But this estimate is affected by the issue of money.

The Target of His Criticism

The failure of capitalism, Douglas said, is the failure of fi-
nancial credit to match the output of Real Credit. The banks
create financial credit. The banks supposedly extend credit only
to producers, he said - conveniently overlooking the existence
of modern consumer credit. The “result of these creations of
credit granted to producers only, instead of to consumers” he
insisted, “is to produce a rise of prices which nullifies the addi-
