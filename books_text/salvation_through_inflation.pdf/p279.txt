Major Douglas’ A + B Theorem 247

tion in a capitalist economy does not allow the workers as a
whole to buy the totality of the goods they have produced. That
is, workers are being exploited. For once, he mentioned consumer
credit. The credit system does make money available for the
workers to buy the fruits of their labor, but they must go into
debt. This creates a system of growing indebtedness. Douglas
quoted the mysterious (and incoherent) H.M.M., who wrote:

The goods we buy are produced on borrowed money; the money
we buy them with goes to extinguish the debt; but it itself is
derived from credits that have been borrowed from the banks,
and consequently its value must reappear in selling prices some-
where, and be recovered again from the consumer if the banks
are to be repaid their advances. It is clear, therefore, that one
credit is only cancelled by the creation of another and larger
credit.

(Over two decades later, H.M.M. reciprocated the favor, citing
Major Douglas as the man who “had analysed the problem into
its basic elements, and devised a solution for it that fitted all the
facts, and was watertight in every particular; . . .*)

One thing needs to be mentioned in this context: services. In
a growing economy, services steadily replace manufacturing as
the primary source of wealth. Decades ago, management expert
Peter Drucker called this new phenomenon “the knowledge
economy.” This phrase has become widely accepted. The wages
paid to knowledge workers account for most of the expendi-
tures in a modern economy. For example, the raw materials in
a computer disk on which magnetic impulses are embedded
cost less than a dollar. The disk may sell for two hundred times
this because of the information contained in those magnetic
impulses. So, even if Douglas’ theoretical analysis were correct,
as time goes on it would become increasingly irrelevant eco-

5, Credit-Power and Democracy (London: Cecil Palmer, 1920), p. 24.
&. H.M.M., The Struggle for Money (Glasgow: William Maclellan, 1987), p. 14.
