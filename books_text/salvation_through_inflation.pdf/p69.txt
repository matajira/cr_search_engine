The Origins of Social Credit 37

cause of economic hardship. It was this idea that also stirred
Ezra Pound, a frequent contributor to New Age. They both
regarded Social Credit as a means of destroying the power of
the credit-manipulators.

Orage was not merely a disciple of Douglas; he was a collab-
orator. Douglas included a 60-page commentary on his ideas,
written by Orage, in his second book, Credit-Power and Democracy
(1920).

Philip Mairet was one of the early partisans of Social Credit.
He compiled The Douglas Manual in 1934. He was one of the
four founders of the Chandos Group, named after a local res-
taurant, which was begun in 1926 to discuss social and political
affairs from the point of view of Social Credit.* This group
occasionally attracted such luminaries as the socialist G. D. H.
Cole, Lewis Mumford, and the poet T. S. Eliot.* Mairet wrote
A. R. Orage: A Memoir in 1936. He was not a hostile witness
against Orage. The book was reprinted in 1966 by University
Books, an American firm specializing in academically oriented
studies of the occult. It was distributed through the Mystic Arts
Book Club, which was the mail-order outlet for University
Books. University Books published it because Orage was a
dedicated mystic, a senior disciple of the occultist Gurdjieff.

In 1922, Orage resigned as editor of New Age to become, in
Mairet’s words, “a missionary of the gospel according to G. I.
Gurdjieff.”* In New York City, he became — again, in Mairet’s
words - “Gurdjieff’s right-hand man. . . .”* This was not a
major theological break from his past. By the time he took over
as editor of New Age in 1907, he had become a Theosophist. He

22, “Review of Ezra Pound and Italian Fascism.” The Economist (17 Aug. 1991), p.
33.

23. Finlay, Social Credit, pp. 168-70.

24, Ibid, p. 170.

25. Philip Mairet, “Reintroduction,” A. R. Orage: A Memoir (New Hyde Park, New
York: University Books, 1966), p. vi.

26. Ibid., p. vi.
