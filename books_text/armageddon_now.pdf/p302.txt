270

Ober, Douglas, 177, 180, 182

oil, 89, 142, 168, 182-83, 211-13

Olson, Arnold T., 191, 196-97, 199,
211

Omar, 35

Oneida community, 24

Open Bible Church, Los Angeles, 89

opportunism, 215, 218

optimism, $6

Or, William W., 123, 131, 167

orthodox Jews, 191

Otis, George, 213

Our Hope, 13, 36-37, 39-40, 43,
45-50, 56-57, 73, 90, 106, 115,
118, 125-26, 128, 135, 145-46, 150,
155-58, 160, 173, 178, 181, 184,
186

Palestine
boundaries, 46, 105, 134-35,
167-68, 196, 198
invasion, 14, 17, 20, 27, 29, 78,
87, 118, 128, 144, 152, 156,
171, 177, 184, 202-03
Napoleon, 20
restoration, 17-18, 30, 35-36, 63,
9, 73
Woodrow Wilson, 42
World War I, 38, 41
Palestine Arab Congress, 64
Palestine War
see War of 1948
Pankhurst, Christabel, 59, 63-64, 82
Panton, D. M., 75, 90, 152, 176
Parker, T. Valentine, $6
partition, 100-01, 126, 128-29, 135,
165

peace, 37, 74, 81, 111-12, 128, 147,
149, 154, 162-63, 167, 173, 180,
182-83

Pearl Harbor, 124, 147

Pentecost, J. Dwight, 200

Pentecostal Evangel, 13, 36-40, 42,
44, 46, 49, 51, 59-60, 62, 67,
69-71, 73, 75, 78-79, 81-83, 88,
90-92, 94, 97, 100, 102, 105-06,
108-10, 112, 115-17, 121, 124-28,
130, 132-34, 136-39, 141, 143,
146-48, 150-52, 155-56, 162-63, 166,
169, 171-73, 176, 178, 182-83, 190,
195-96, 199, 208, 212

People’s Church, Toronto, 83

Persia, 107, 180, 208

Armageddon Now!

Persian Empire, 25
pessimism, 12, 52, 53, 57, 80, 178
Pettingill, William L., 128
Philadelphia Lutheran Theological
Seminary, 31
Philadelphia Prophetic Conference,
45, 50
Photius, 204
Pierson, Arthur T., 57
pietism, 60
Pliny, 27
Plymouth Brethren, 167
Poland, 40, 117-18, 137, 144-45
Pont, Charles E., 173, 184-85
Pontus, 27
popular opinion, 140-41
Portugese inquisition, 164
postmillennialism, 12, 18
Poysti, N. J., 108
premillenarians
anti-Arab, 71, 101, 103, 138-39,
172-74, 193, 195, 201, 211
anti-communism, 108
anti-Russian, 60, 85, 107, 145,
147, 149-50, 152, 159, 175-76,
178, 186-87, 203, 214
anti-Semitism, 16, 74, 95-97, 138,
192, 205-06, 217
anti-United Nations, 184-85
attacked, 56-87
Brookes, James H., 30
defined, 11-13
expectations, 36, 86-87, 119, 126,
134, 155, 162, 189, 202, 215
extensive, 43, 56
Mede, Joseph, 17
moderates, 89, 95, 134, 197, 217
pessimism, 37, 163
Scofield, 16
secular press, 38, 155
self-confidence, 39, 106, 114, 117,
123, 130-31, 154, 162
United States, 23
World War I, 53
Presbyterian, 110
Presbyterian Guardian, 177
Presbyterians, 23, 110
Price, Charles S., 98, 107, 113-14
Princeton Theological Seminary, 169
Proclus, 24, 177
prohibition, 93
Prophecy, 87, 99, 102, 104, 107-08,
10-11, 118, 120
Prophecy Investigation Society, 98
