Armageddon—Almost 39

to the wildest explanations of current events supposedly from
the standpoint of Bible prophecy.” Editor R. A. Torrey then cited,
for the sake of example, extensive passages from Our Hope in
which the author categorically affirmed that the war was not the
battle of Armageddon nor was it the Great Tribulation, the chief
rationale being that the Jews were not yet back in Palestine. The
word of caution expressed by The King’s Business was derived
from historical experience: “Had we lived a century ago, when
Napoleon was at the zenith of his power, we might well have
supposed that the final drama of prophecy had begun.!2

As Palestine appeared more and more in the news, however,
such sobering advice only served to augment the excited expecta-
tion of restoration. Only a few months later, Torrey himself cited
an article from Literary Digest, “A New Kingdom of Israel,”
which suggested that in the general opinion the best solution for
Palestine was the establishment of a Jewish state under the pro-
tection of one of the great powers. He also quoted extensively
from the London Daily Chronicle the “novel suggestion” that the
new Jewish state be placed under United States protection. All
of this he found to be “exceedingly interesting to students of
prophecy, and especially to those who had been looking for the
speedy return of the Jews to the Holy Land.”!5

With somewhat less caution, the Evangel tended to draw
freely from the colorful Jewish press such items as the following
with a London, July 22, 1916, dateline: “The British Government
has made it known officially that the Powers will strive after
the war towards the restoration of Palestine to the Jews.” Ante-
dating the Balfour Declaration by more than a year, this report
was based solely on an informal correspondence of Sir Edward
Grey, the Secretary of Foreign Affairs, to the effect that Jewish
interest in Palestine would not be overlooked.4* A year later an
Evangel writer justified this persistent apocalyptic attention to the
Middle East: “Even the secular daily newspapers and the popular
worldly magazines talk glibly about the speedy prospect of the
Turk being driven out of Jerusalem and Palestine; and the Holy
City and land being given to returning Jews, under the protection
of Britain and her allies.” The conclusion was then drawn that the
premillenarian prophetic scholars had “made no mistake.”45

Our Hope reported that Jewish restoration was “more than
