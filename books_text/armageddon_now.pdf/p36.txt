XxKvi Armageddon Now!

“columnist for Probe Ministries, a Christian think tank.” In a
column headlined “Throwing Stones at Israel” he observed that
“recent political events and changing religious attitudes are
eroding . . . support.”

Since 1948, Israel has been the persecuted minority fighting the
surrounding Arab nations bent on its destruction. Now Israel
is seen as the aggressor against the underdog Palestinians.

Israel has lamely argued that the Geneva convention is
not binding in the West Bank because it was never legally
part of Jordan and therefore not “enemy territory’... .

But attempts to justify its actions have failed, and many
of Israel’s supporters are becoming more critical.

Anderson then blamed part of this shift on the development
of “Kingdom or Dominion theology” of Earl Paulk of Atlanta,
Gary North of Tyler, Texas, and their many Reconstructionist
disciples in leadership positions. He then responded, “Israel is a
nation with prophetic destiny,” and calls for virtually uncondi-
tional support:

Christian leaders must rekindle support for Israel by
teaching about the nation, the Jewish people, and God’s
covenants. Our support does not mean we agree with all of
Israel’s political decisions, but in this 40th anniversary year,
Christians should demonstrate steadfast support for the na-
tion of Israel (my italics).

Ed McAteer, founder and president of the Religious Round-
table identified his position for Christianity Today (1/13/90) as
“unconditional love.” A news feature headlined “New Develop-
ments in the Middle East rekindle debate among Christians over
support: for Israel” reported how the Roundtable since 1981 has
sponsored the annual “National Prayer Breakfast in Honor of
Israel” at the National Religious Broadcasters’ convention in an
attempt to influence media. A “Proclamation of Blessing” read
at the event in 1988 stated, “As Bible-believing Christians, we
believe there exists an iron-clad bond between the State of
Israel and the United States of America.”

This tendency to look at Israel’s actions through millennium-
tainted glasses had been taken to task in an important contribu-
tion by Christianity Today (1/22/82), in an article, “Israel Today:
What Place in Prophecy?” by Mark M. Hanna. Hanna was
Associate Professor of Philosophy of Religion at Talbot Theo-
