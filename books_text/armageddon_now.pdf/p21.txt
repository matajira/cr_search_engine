Publisher's Preface xxi

It will be interesting to read the 1992 “revised editions” of
these books. They will be frantically rewritten in response to the
unpredicted events of August 19-21. Think of Grant Jeffrey’s
book, Armageddon: Appointment With Destiny (1988), published
by an obscure press in Ontario, Canada, which proclaimed
“144,000 in print” just before the book was picked up by Hal
Lindsey’s publisher, Bantam Books (located at 666 Fifth Avenue,
New York City.) Section 3 of Mr. Jeffrey’s book is titled,
“Russia’s Appointment With God.” Chapter 7 is “Russia’s Day
of Destruction on the Mountains of Israel.” A comparison of
the 1991 edition with the (easily predictable) forthcoming revised
edition will prove educational, I feel certain.

Failed visions require extensive revisions. Let me list a few
of what I call the “harvest” of soon-to-be-revised books:

Dave Hunt, Global Peace and the Rise of Antichrist (Harvest
House, 1990)

E. Davidson, islam, Israel and the Last Days (Harvest House,
1991)

Jerry Johnson, The Last Days on Planet Earth (Harvest House,
1991}

Peter Lalonde, One World Under Anti-Christ (Harvest House,
1991)

Chuck Smith, The Final Curtain (Harvest House, 1991)
To this list we can add:
Thomas §. McCall and Zola Levitt, The Coming Russian
Invasion of Israel, Updated (Moody Press, 1987)
Robert W. Faid, Gorbachev! Has the Real Antichrist Come?
(Victory House, 1988)
Erwin W. Lutzer, Coming to Grips with the Role of Europe
in Prophecy (Moody Press, 1990)
Gary D. Blevins, The Final Warning! (Vision of the End
Ministries, 1990)
Paul McGuire, Who Will Rule the Future? A Resistance to
the New World Order (Huntington House, 1991)
Edgar C. James, Armageddon and the New World Order
(Moody Press, 1991}
Ed Hindson, End Times, the Middle East and the New
World Order (Victor Books, 1991)
Who are these people? Have they devoted their lives to
careful biblical scholarship? Where are the scholars of dispensa-
