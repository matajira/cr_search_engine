Armageddon—Alwost 47

Jew was ready to return, it would have been occupied.” The
restoration of the Jews was now to settle everything. The writer
explained all the past by saying, “The Jew and Palestine have
been the underlying cause of the whole Eastern question that
has agitated Europe for a hundred years.” He thus ignored any
mundane theory such as imperialism or Russia’s need for a warm
water port.*?

The support for restoration and Zionism was almost universal
among premillenarians, but there were those who had second
thoughts. Protestant missionaries in the Middle East of the old-
line denominations were usually resistant to Jewish nationalism.
This was especially true of those missionaries who worked among
the Arabs. A writer in The King’s Business recognized the reality
of the situation and admitted, “The Zionists are going to oppose
us,” but he was optimistic that the true Christianity of the pre-
millennialists would “lead and direct them aright” when the Zion-
ists discovered that true Christianity was not a persecuting
Christianity.“ Qur Hope’s doubts, on the other hand, seemed to
stem from a reassessment of its earlier enthusiasm:

What the Jews are doing today is an infidel movement which
will land them in the darkest night, We mention these facts
for the sake of so many of God’s people who begin to see
these things, but think that the ever increasing efforts of
Zionism to regain the land is the restoration of which the
prophets have spoken. It is not. The true restoration and
blessing of Israel comes with the Coming of the Lord.‘9

This did not mean that God’s hand was not in everything; it just
implied that not everything that happened was good or right.
The unusual aspect here is that a critical attitude was expressed
toward Israel’s actions; such antipathy was usually reserved for
Russia and her accomplices, in spite of the fact that Russia, too,
was part of God’s movements according to the premillenarian
philosophy of history.

The best representative expression of this attitude toward
Russia was embodied in the wartime book, At Hand, by the
English author, Walter Scott. In it he reiterated views he had ex-
pressed nearly forty years before:
