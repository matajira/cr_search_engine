Jerusalem! What Now? ib

Arnold T. Olson, summarized his indictment by saying, “The
Arabs, the community of nations, and the church are all guilty
of making Israel’s survival during these 25 years more difficult—
yet there is the miracle of survival.”! Christian Life proclaimed
that the occupation of Jerusalem and the Temple site as a result
of the miracle of the 1967 war was a sign that the Gentile domi-
nation over Israel would rapidly come to an end.19?

On the other hand, there were expressions of doubt about
the moral position of the Israelis after twenty-five years. Eternity
endorsed the “incredible historical record of Israel’s revival and
the weight of biblical prophecy,” but gave a two-part warning
to “our fellow premillennial believers.” The first was that ex-
cessive dogmatism on prophetic details should be avoided: “His-
tory is littered with the torn-up pages of prophetic scholars who
have linked antichrists, wars and other biblical elements with spe-
cific historical events.” The author was clearly concerned with
the ever-expanding history of the premillennialists’ credibility prob-
lem. The second warning was to avoid glorifying the state of
Israel. In the midst of the Watergate scandal, the invidious com-
parison was drawn: “Politically Israel is no purer than Washington,
D.C.” The author pointedly observed that Israel’s leaders were
not inerrantly inspired and that they needed occasional rebuke
and rebuff just like all political chieftains.1°

Christianity Today similarly editorialized that although Ameri-
cans might admire the determination and decisiveness of Israeli
foreign policy, they should recognize that an inflexible pro-Israel
stand might not always correspond with justice. The editor also
pointed out that Arab control of oil production meant that an
inflexible pro-Israel stand might not always correspond with the
American national interest either. Such realistic assessments
evoked criticism from the Jews at the time of the oil embargo
by Arabs after the 1973 Yom Kippur War. Jews assumed that the
only explanation for oppesition to the Israeli position was a caving-
in to the Arab blackmail—it never occurred to them that there
might be a sincere resistance to Israel’s stubbornness in retaining
Jerusalem and other conquered lands. Among the premillenarian
press, however, there was never any questioning of the right of
Israel to exist as a nation in Palestine.
