Selected Bibliography 257

“Significant Books About Jews.” Christian Life, XXXV (August, 1973),
50-54,

Smith, David E. “Millenarian Scholarship in America.” American Quar-
terly, KVII (1965), 535-49,

Smith, Wilbur M. “The Prophetic Literature of Colonial America.”
Bibliotheca Sacra, C (January, April, 1943), 67-82, 273-88,

Snowden, James H. “Summary of Objections to Premillenarianism.” The
Biblical World, LUI (1919), 165-73.

The Sunday School Times.

“Two Views of the Arab-Israeli Controversy.” His, XV (March, 1955),
9-12,

United Evangelical Action.

Watchman Examiner.

Wiersbe, Warren W. “Two Giants of Bible Interpretation.” Moody
Monthly, LXXIV (February, 1974), 61-64. (Partly about Arno C.
Gaebelein)

Yost, Charles W. “The Arab-Israeli War: How It Began.” Foreign
Affairs, XLVI (January, 1968), 304-20.

UNPUBLISHED WORKS

Adams, Bobby E. “Analysis of a Relationship: Jews and Southern
Baptists.” Unpublished Th.D, dissertation, Baptist Theological Semi-
nary, Fort Worth, Texas, 1969.

Adler, Leslie Kirby. “The Red Image: American Attitudes Toward
Communism in the Cold War Era.” Unpublished Ph.D. disserta-
tion, University of California, Berkeley, 1970.

Cantwell, Emmett Howell. “Millennial Teachings Among Major Baptist
Theologians from 1845 to 1945,” Unpublished Th.M. thesis, South-
western Baptist Theological Seminary, Fort Worth, Texas, 1960.

Chenoweth, Maurice Gene. “The Politics of Four Types of Christian
Chiliasts.” Unpublished Ph.D. dissertation, University of Minnesota,
1965,

Culver, Douglas Joel. “National Restoration of the Jewish People to
Palestine in British Non-Conformity, 1555-1640." Unpublished
Ph.D. dissertation, New York University, 1970.

Goen, Clarence C. “A Survey History of Eschatology.” Unpublished
ThD, dissertation, Southwestern Baptist Theological Seminary, Fort
Worth, Texas, 1969.

Harrington, Carrol Edwin, “The Fundamentalist Movement in America,
1870-1920." Unpublished Ph.D. dissertation, University of Cali-
fornia, Berkeley, 1959.

Huff, Earl Dean. “Zionist Influences upon U.S. Foreign Policy: A
Study of American Policy Toward the Middle East from the Time
