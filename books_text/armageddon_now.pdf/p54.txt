22 Armageddon Now:

ported many prophetic endeavors; he also wrote and edited works
on prophetic themes. Twenty laity and clergy were invited to
discuss prophecy in general and the second advent and the res-
toration of the Jews in particular. In the discussions the only appeal
allowed was to the authority of Scripture. The prophetic con-
ference was to be a means of theological development of the
premillennial movement for the next century, until it was sup-
Planted by the Bible institute movement in the twentieth century.
One of the leading figures of the Albury conferences was Edward
Irving, a popular London preacher of the 1820s and the founder of
the Catholic Apostolic Church. The conclusions reached by the
conferences included the beliefs that the present age would end
in a cataclysmic judgment and the Jews would be restored to
Palestine, that “the 1260 years of Daniel 7 and Revelation 13
ought to be measured from the reign of Justinian to the French
Revolution,” and that “the vials of wrath (Revelation 16) are
now being poured out and the second advent is imminent.”*? Also,
the widespread influence of premillenarian John Darby and the
rapid growth of his Brethren movement in the 1830s are further
indication of the breadth of interest in Jewish restoration.

The belief in the restoration of Jews was not confined to
those of premillennial views nor to those of inferior social status.
The idea was pervasive in Britain, and as Sandeen observes, “There
can be no question that the millenarian movement played a sig-
nificant role in preparing the British for political Zionism.’**
Attendants at Edward Irving’s services included members of Parlia-
ment and cabinet ministers. Samuel Taylor Coleridge was certainly
not a premillennialist. Nevertheless, believing that the millennium
had begun with Constantine and would culminate in a perfected
earth, Coleridge awaited the return of the Jews to Palestine. He
claimed that his views were not based on Daniel or the Apocalypse,
but on “undisputed” passages: “I fully agree with Mr. Irving as
to the literal fulfillment of all the prophecies which respect the
restoration of the Jews (Deuteron. xxv.1-8)."5*

Lord Shaftesbury was a vigorous promoter of Jewish settle-
ment in Palestine. In his view such settlement would develop the
land between the Mediterranean Sea and the Euphrates River.
As a millennialist, he believed that Palestine actually belonged to
the Jews and should be returned to them, ideally as Christian
