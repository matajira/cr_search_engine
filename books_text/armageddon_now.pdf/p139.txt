 

 

 

 

Mobilizing for Armageddon

There is nothing to hinder now the hordes of Russia
sweeping down through both Turkey and Persia to the very
gates of Jerusalem, There is the valley af Armageddon.

—Charles S. Price
The Battle of Armageddon

The same consistency which was reflected in premillenarian
attitudes toward Israel was also manifested in an unremitting
antipathy toward Russia. Part of that antipathy was derived from
the ideological conflict with communism, generating discussions of
atheism and atrocities; but most of it centered, as before, upon
the prophetic character of Russia herself, focusing upon the
arms race and Armageddon.

Premillenarians maintained that the best proof of Russia’s
atheism came from the mouths of the Communists themselves.
Zinoviev was quoted by Prophecy as having said, “Our next move
will be to climb into heaven and drag God from His throne.”
To this the Prophecy writer replied, “Shaking their fists in the
very face of the Eternal God, they say: ‘I am against thee, O God

107
