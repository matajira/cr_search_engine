160 Armageddon Now!

east and the entry of Japan into the war seems to be a step in
that direction.”®? But Japan’s star quickly dimmed. After the
war, Our Hope suggested China and India as likely candidates,
because communism was making rapid strides there.® The advent
of the Cold War shifted attention to eastern Europe, and the
Russian satellites, as well as the Far East, were included among
the “many people with thee.” Louis T. Talbot in The King’s
Business gave a different twist than others; in his system, Japan,
China, and India would be opposed to the Russian confederation.®
With the fall of China to the Communists, attention tended to
center upon that nation as the dominant figure in the Far East:
“It is possible that the sweeping victories of the Communist army
in China indicate the groundwork for the federation of these
northern and eastern nations is being laid.” In a message at
the 1949 Founder’s Week Conference at Moody Bible Institute,
Wilbur M. Smith drew attention to an article on Gog and Magog
by Walter Scott in the June 1888 issue of The Prophetic News
and Israel’s Watch:

Russia has for ages meditated on the conquest of Asia and
India and China. Great Britain, with the United States, stands
face to face with this Russian power, and these two sides
will come into one final awful struggle. We judge that the
tide of Russian conquest will flow on to the frontiers of
China.... We believe, from the place assigned to Russia
in the Word of God, that her legions will sweep over the
plains and mountains of Asia and become the dominant
power over all the East until she falls forever on the moun-
tains of Judea.87

The point Smith made was that only the inspired Word of God
could have enabled Scott to make such an accurate prediction.
(Smith did not suggest what spirit inspired another writer he
quoted to identify in 1844 “the kings of the east” as the officers
of the East India Company.)
