 

 

 

 

Balfour Betrayed

And in this hour, authoritative voices in every land are tell-
ing us in no uncertain tones that it is not a matter of years,
but of months, when the battle flags of the nations will again
unfurl, and the scourge of the earth will be on the march—
to Armageddon! :

—Louis S. Bauman, The King’s Business, 1934

The present return of the Jews to their own land is but
the beginning of that prophesied.... It will gather momen-
tum as the months go by, but this is certainly the regathering
prophesied as the final one, which is a remarkable and
unmistakable sign that we are in the latter days.

—Leonard Sale-Harrison
Moody Bible Institute Monthly, 1936

The menace of the 1930s arms race was the concern of the
whole world, but for the premillenarians it was the preparation
for Armageddon. Moreover, Armageddon itself was hailed as a
“wonderful” and “glorious” fulfillment of prophecy. The apogee
of rising expectations was reached with the Nazi-Soviet Pact of

86
