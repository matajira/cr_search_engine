126 Armageddon Now!

over their gullibility—as their credibility declined with each suc-
ceeding generation.

As in the 20s, prophetic students continued to plug Musso-
lini as the leading candidate for Russia’s antagonist—the Antichrist.
Most were not dogmatic about Mussolini, but they were sure
about Rome. As Prophecy expressed it:

The Word of God takes account of two definite combina-
tions of nations, which play an important part in the closing
hours of this age. The most important is the confederacy
of the Roman states, led no doubr by Italy. The other is
led by Russia.58

According to a fascinating account in The Sunday School Times,
Mussolini himself was interviewed by two Belgian premillenarians,
Mr. and Mrs. Ralph C. Norton. Mussolini was asked:

“Do you intend to reconstitute the Roman Empire?" He
answered, “One cannot revive a dead empire, nor recall it
into being. We can only revive its spirit, and be governed
by the same discipline.”

Mr. Norton began to speak to the Premier of the teaching
of the Bible regarding the reforming of the Roman Empire,
telling him it was predicted of God, and speaking of the
alliance of northern nations that would likewise take place.

Mussolini leaned back in his chair and listened fascinated,
and asked, “Is that really described in the Bible? Where is
it found?"

Louis S. Bauman responded to critics of his position on Musso-
lini by asserting, “We candidly admit our opinion that if the
Lord were to call His people away... today, Mussolini would
appear as the outstanding candidate for the job of the Antichrist.”
Alva J. McClain later expressed what was probably an even more
widely-held view: “While I am not even suggesting that Musso-
lini is the Willful King in person, surely it ought to be clear
that in his Fascist methods and policies we have a remarkable
correspondence with the things mentioned in Daniel 11:39.
Arno C, Gaebelein did not risk an identification of the Antichrist,
but he did believe that the Antichrist was probably already in
the world.°? The Elim Evangel expressed the opinion that the
Spanish Civil War might provide a pattern for Mussolini's inter-
vention in the Middle East (which would lead to Armageddon),
pointing out that:
