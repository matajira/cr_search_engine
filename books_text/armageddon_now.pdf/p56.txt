24 Armageddon Now!

premillennial teachings, and even made the faithful thereafter a
bit shy of date-setting. As Sandeen again has so aptly observed,
“Tt took a long time for Americans to forget William Miller.”*+
The founder of the utopian Oneida community, John Humphrey
Noyes, who came to believe that the second advent had occurred
in aD. 70, described the general mood of the era:

It is certain that in 1831, the whole orthodox church was in
a state of ebullition in regard to the Millennium. A feeling
of expectation on this point lay at the bottom of the tri-
umphant march of revivals which shook the land for several
years from that time. The Millerites have since met with
unbounded ridicule; but it should be remembered that all
that portion of the churches who were spiritual, who be-
lieved in revivals, and who were zealous and successful in
laboring for them had a fit of expectation as enthusiastic
and almost as fanatical as the Millerites.*°

In the 1850s premillennialism recovered from the setback suf-
fered as a result of Miller’s errors——and accordingly, writers began
to risk works on prophetic themes again. Jacob J. Janeway, a the-
ology professor at the Presbyterians’ Western Seminary, published in
1853 Hope for the Jews: or The Jews Will Be Converted to the
Christian Faith; and Settled and Reorganized as a Nation in the
Land of Palestine. Janeway believed the Jews would be restored
“under the reign of the promised Messiah.’** The issue of the
preconditions for the restoration was to become an item for
discussion over the years. Janeway contended that if the Jews
returned in their present state of unbelief they would have no
peace or security. He discussed their historical persecution, and
then by way of contrast the renewed interest in returning, men-
tioning that ‘a society of Jews has been formed in London, with
the view of stirring up their countrymen, in all lands, to seek
a re-possession of the land,’4?

For those so predisposed, crises—foreign or domestic—stimu-
late apocalyptic concern. Even as far back as the late Roman
Empire the patriarch Proclus (434-47) was interpreting the Rosh
and Meshech of Ezekiel 38:2 (arv) as the invasion of the em-
pire by the Huns.48 Likewise the Crimean War, just as the Na-
poleonic wars before, accelerated the production of prophetic
materials. A preacher of the Scottish National Church, John Cum-
ming, published in 1855 two works which became the seedbed
