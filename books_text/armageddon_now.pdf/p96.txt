64 Armageddon Now!

Baptist’s Watchman Examiner referred to her presentation as
“impassioned” but offered no critical comment, Being a false
prophetess, however, did not disqualify her in premillenarian cir-
cles; in 1927, the Moody Bible Institute Monthly carried her specu-
lative views on the restoration of Israel and the formation of a
ten-kingdom confederacy in Europe and Asia.”

In Palestine itself, the restoration was strongly resisted by the
Arabs, and violence prevailed in Palestine throughout the decade,
occasional rioting sometimes breaking out into open warfare.
The Arabs had supported the British during the war with full
expectation of achieving national independence from the Turks.
The success of the British colonel, Lawrence of Arabia, was pos-
sible only because of such a belief. At the time of the capture of
Jerusalem, General Allenby had announced, “The object of war
in the East on the part of Great Britain was the complete and
final liberation of all peoples formerly oppressed by the Turks
and the establishment of national governments and administra-
tions in those countries deriving authority from the initiative and
free will of those people themselves.”*1

Gradually the Arabs became aware of the British betrayal; riot-
ing broke out in the spring of 1920, and in 1921 the Palestine
Arab Congress sent a formal protest delegation to London. Brit-
ain’s uncertain policy of supporting Zionist goals while at the
same time appearing to favor Arab independence movements ne-
cessitated various shifts in policy statements. This was further
complicated by conflicts within the government over what the
goals should be, let alone how they should be implemented. The
Arabs had looked forward to the implementation of President
Woodrow Wilson’s Fourteen Points through the peace settlements,
including the self-determination of nations. Such a lofty dream
soon dissolved into the nightmarish reality of British and French
imperialism in the Middle East. This reality had been embodied
in the secret Sykes-Picot agreement during the war that carved
the former Turkish territories into British and French spheres
of influence. Under the League of Nations these became “man-
dates,” a hypocritical name for colonies—with vague benevolent
goals in some distant future. President Wilson had sent a com-
mission to the Middle Hast prior to the Versailles Treaty. This
King-Crane Commission had advised the American peace dele-
