236 Armageddon Now!

103, John Cumming, The End: or The Proximate Signs of the Close of This
Dispensation, p. 275.

CHAPTER 8
L. Louis S. Bauman, “Gog & Gomer, Russia & Germany, and the War,”
The Sunday Schoot Times, LXXXI (December 16, 1939), 912.
2. The Pentecostal Evangel, August 17, 1940, p. 7. (Hereafter cited as
Evangel)
3. Arno C. Gaebelein, “The Problem of Gog and Magog,” Our Hope, XLVIL
(1940-41), 455.

4. Louis §. Bauman, Light from Prophecy as Related to the Present Crisis,
pp. 40, 42.

5, Evangel, February 24, 1940, p. 11. Bauman, “Gog & Gomer,” pp. 911-12.
6, Evangel, August 17, 1940, p. 7.

7. John Hess McComb, “Europe and the Bible,” The King’s Business, XXXL
(May, 1940), 167-68.

8. Evangel, October 5, 1940, p. 7; October 19, 1940, p. 10.

9. Evangel, December 28, 1940, p. 10,

10. Harry Rimmer, The Coming War and the Rise of Russia, pp. 59, 81,
IL. W. W. Fereday, “Armageddon,” Our Hope, XLVII (1940-41), 399,
42. Evangel, July 12, 1941, p. 10.

13. Aro C. Gaebelein, “The New Great World Crisis,” Our Hope, XLVI
(1941-42), 89.

14, Our Hope, XLVI (1939-40), 234.

15, Gaebelein, “The New Great World Crisis,” p. 91.

16. Evangel, August 9, 1941, p. 10.

17. Evangel, August 27, 1941, p. 13.

18, Evangel, October 4, 1941, p. 10.

19. Louis § Bauman, Russian Events in the Light of Bible Prophecy, pp.

513-14, Ernest Gordon, “Russia and Chiistianity,” The Sunday School Times,
LXXXIV (June 27, 1942), 513.

20. Evangel, February 20, 1943, p. 16 (italics mine).

21. Evangel, January 10, 1942, p. 10.

22. Bauman, Russian Events, p. 84,

23. Tbid,, p. 173.

24. The Sunday School Times, LXXXIV (November 21, 1942), 937-38.
25. Evangel, May 29, 1943, p. 8.

26. As cited in Evangel, September 11, 1943, p. 13.

27. As cited in Evangel, February 26, 1944, p. 8.

28, Evangel, September 30, 1944, p, 16.

29. As cited in Evangel, November 18, 1944, p. 16,

30. Dan Gilbert, The Red Terror and Bible Prophecy, pp. 18-20.
31. Evangel, January 6, 1940, p. 9.

32. Evangel, March 9, 1940, p. 11.
