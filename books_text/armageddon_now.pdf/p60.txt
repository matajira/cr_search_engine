28 Armageddon Now!

because one does not like to dogmatize; but is it not remark-

able that here the prediction is that Rosh, Meshech, Tubal, or

Russia in all its divisions, and the descendants of Gomer,

or the Germans, should coalesce, and form as one this great

confederacy? Does it not look as if the fulfilment were tak-

ing place before our eyes? Is not Prussia practically allied

to Russia?...I may ask, is it not at least, as the most

sceptical before me will admit, a remarkable coincidence,

that the prediction in Ezekiel, that Rosh, Meshech, Tubal,

Gomer and all his bands, should be united together in one

great confederacy, is at this moment a historical fact?4!
Cumming then identified the leader of the resistance to this con-
spiracy, the “merchants of Tarshish,” as England.®? Unfortunately,
all opposition was to be in vain for “Russia will burst forth, over~
come all resistance, march to Palestine.”¢?

There was a large streak of fatalism in Cumming: “There
is not a soldier fallen in the Crimea who has not first finished
his mission.”** But he did not apply this concept to nations,
moralizing that “because Russia is now fulfilling a prophecy,
Russia is not therefore doing right or fulfilling her duty.”®° “God
predicts in His word what He does not applaud in His law.”¢
Again it should be emphasized how extremely rare in the apoca-
lyptic tradition is such an obvious insight. Cumming’s fatalism
certainly did not make him a pacifist, however. He was extremely
happy that England’s projected role would be both “right in the
light of duty” and “indicated in the light of prophecy.”®" “I have
no sympathy therefore with those who think that war is and
can be never a nation’s duty.”8 “We are told that God will bless
them that bless Israel,” said Cumming, expressing what was to
become a monotonous theme among premillennialists supporting
the restoration. God “has promised a special blessing upon them
that pity His ancient people, and try to do them good instead
of trying to do them harm.” Cumming particularly castigated anti-
Semitic fatalism: “There is a very stupid notion abroad, that
because God has predicted that the Jews for 18 centuries shall
be a scoff, a by-word, and be spit upon, and be treated as the
offscouring of the earth, they therefore do well to treat the Jew
with cruelty.” Cumming demanded morality of all—Russians, Jews,
and anti-Semites—not a passive, fatalistic resignation to the pre-
dictions of prophecy.®
