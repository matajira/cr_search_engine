Mobilizing for Armageddon 117

Bauman then commented, “Those who know the ‘sure word of
prophecy’ expect nothing less than that some day in the not far
distant future, the seeming enemy nations, Russia and Germany,
will shake hands, whether Stalin and Hitler do or not.’*> Moody
Monthly's editor quoted a recent book, Plot and Counterplot in
Central Europe, by Marcel W. Fodor: “Many augurs, with the
safe instinct of illogical people, see the spectre of this German-
Russian friendship looming on the horizon.” Fodor found it dif-
ficult to believe that Hitler himself could make such a drastic
reversal, but he said, “It would be a mistake to dismiss it as
impossible.”"*6

This crescendo of expectation grew as the fateful year of 1939
arrived. Yet in the January issue of The King’s Business, com-
mentator Dan Gilbert criticized the New Republic editorial that
said Hitler was now in a position to “make friends with Soviet
Russia, and participate in a new and probably bloodless parti-
tioning of Poland.” Gilbert said, “This development does not
mean, though, that a Nazi-Soviet accord is in prospect, or even
in sight. It is not—not by a long shot.” Nevertheless, Gilbert did
believe the long-term direction was unity. He concluded by saying:

This very trend of events has been predicted plainly by
Biblical scholars writing in The King’s Business during
past months, and even years. Louis S. Bauman, for one,
showed conclusively and convincingly how Germany and
Soviet Russia were due eventually to “get together.” This
is a striking indication that Bible prophecy is, indeed, “history
written in advance.” Gaining his light from God’s Word,
Dr. Bauman was able to see far ahead—into the future-—~
which was closed to the most brilliant minds which relied
solely on their own penetrating, but wholly human and
therefore limited, insight and foresight‘?

Such an “I told you so” attitude characterized the exultant
response of the premillennialists to the surprise announcement of
the Nazi-Soviet pact. The Evangel observed: “Students of prophecy
have long awaited the day when this alliance would come to pass,”
and pointed out that in Mein Kampf Hitler had set forth his
personal philosophy that an alliance with Russia would be fatal
to Germany-—“But man’s plans cannot thwart God’s decreed pur-
poses.”48 Louis S. Bauman in The Sunday School Times showed that
as early as November 7, 1917, in Christian Herald, James M. Gray
