234 Armageddon Now!

36. Our Hope, LIL (1946-47), 95-96; 172.

37. T. A. Lambie, “Palestine Focus of World Attention,” The Sunday School
Times, LXXXVII (May 4, 1946), 401 (italics mine).

38. Morris Zeidman, “The Restoration of the Kingdom,” Evangel, May 1,
1948, p. 2 (italics mine).

39. Aaron J. Kligerman, “Palestine—Jewish Homeland,” The Southern Pres-
byterian Journal, VIL (June 1, 1948), 17.

40. Louis T. Talbot and William W. Orr, The New Nation of Israel and the
Word of God! (Los Angeles: The Bible Institute of Los Angeles, [1948]), p. 8
Al. ‘The King's Business, XXXIX (July, 1948), 4.

42. Evangel, Tune 12, 1948, p. 8.

43, Evangel, July 3, 1948, p. 8.

44, Our Hope, LV (1948-49), 27.

45, Evangel, June 5, 1948, p. 10.

46, The King's Business, XXXIX (October, 1948), 4.

47, Evangel, July 2, 1949, pp. 5-6.

48, Annual of the Southern Baptist Convention, 1948 (Nashville: Executive
Committee, Southern Baptist Convention, 1948), p. 34.

49, John G, Snetsinger, “Truman and the Creation of Israel,” p. 139, On the

struggle between the State Department and Congress see Frank E, Manuel,
The Realities of American-Palestine Relations, particularly pp. 318-19.

50, Evangel, June 5, 1948, p. 10.
$1, Evangel, September 25, 1948, p. 9.

52. Morris Zeidman, “The Commonwealth of Israel,” Evangel, November
23, 1948, p. 3.

33. T. A. Lambie, “Future Blessings in Palestine,” The Sunday School Times,
XCI (uly 16, 1949), 633.

54, Alliance Weekly, LXXXIV (March 5, 1949), 146.
55. Evangei, October 23, 1948, p. 12.
56. Evangel, April 30, 1949, p. 9.

57. William W. Orr, “The Bible in the News," The King’s Business, XL (June,
1949), 6.

58. Evangel, September 4, 1948, p. 10.
2, George Carmichael, “Rebuilding Palestine,” Evangel, February 19, 1949,

 

a "Y, 8. Grant, “Things to Come," Evangel, May 21, 1949, p. 2.

61, Our Hope, LV (December, 1948), 627-28.

62. T. A. Lambie, “God’s Answers to Questions on Palestine," The Sunday
School Times, XCI (June 18, 1949), 557.

63. Evangel, December 28, 1940, p. 10.

64. Evangel, May 3, 1941, p. 10.

65, Evangel, January 2, 1943, p. 10.

66. The King’s Business, XXXVILL (October, 1947), 4.

67. W. E. Vine, “Ao Ancient Prophecy,” Evangel, Tune 18, 1949, p. 12.

68. £. L. Langston, “The Present War and Prophecy,” Evangel, May 25, 1940,
p. 5. Louis T. Talbot, “Palestine, Russia and Ezekiel 38," The King’s Business,
XXXIX (February, 1948), 17,
