 

The Certainty of Survival 171

seem out of proportion to the provocations suffered—admit-
tedly a two-eyes-for-an-eye policy of retaliation—the Chris-
tian is duty-bound to apply the measuring stick of moral
values as he knows them,

God does not need to use questionable methods in bring-
ing about the fulfillment of prophecy, Prophecy is history
seen in advance, but it is not necessarily morally desirable
or approved. Israel’s return to its land is clearly foretold in
the prophetic Scriptures, Prophetic students can detect in
the record indications that the return will be in unbelief.
Possibly that unbelief will encourage methods incompatible
with the Christian sense of justice. Such events, even though
prophesied in advance, need not confuse the Christian. The
fulfillment of God’s promise will be an Isaac of miracle
rather than an Ishmael of improvisation.*+

Such moderation was the exception rather than the rule,
however. A 1957 book by William L. Hull was more in keeping
with most premillenarian views; it interpreted American policy
in the Suez Crisis as encouraging Russia’s inevitable invasion of
Palestine, and asserted dogmatically, on the basis of the setting
up of the state of Israel, “We live in the /ast days.”25 The liberal
Protestant press on the other hand continued to criticize the
immoral behavior of Israel in creating the refugee problem and
to impugn the morality of Israel’s very existence. John C. Bennett,
editor of Christianity and Crisis and a professor at Union Theo-
logical Seminary, commented on the Suez struggle: “Israel’s ag-
gression was provoked, but the existence of Isracl has been a
continuous source of provocation.”*

In 1958 Israel celebrated the tenth anniversary of its inde-
pendence. The Evangel provided commemorative comment in an
editorial, “The Miracle of Israel.” The editor observed that no
matter what view one might take of the Arab-Israeli dispute, he
could not help but be impressed with the progress made in ten
years. He then castigated those who criticized the restoration
as “a political scheme concocted by materialistic Jews,” and ob-
served that “God’s hand works where it is not seen.” For him,
it was truly God who had brought these Jews together and pro-
vided them sanctuary; in the struggle God was “standing in the
shadows keeping watch above His own.” “We must look beyond
