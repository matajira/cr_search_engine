Publisher's Preface xix

Persian Gulf, as grave as it is, also offers a rare opportun-

ity to move toward an historic period of cooperation. Out

of these troubled times, our fifth objective—a new world

order--can emerge: a new era, freer from the threat of ter-

ror, stronger in the pursuit of justice, and more secure in

the quest for peace. An era in which the nations of the

world, east and west, north and south, can prosper and

live in harmony.

A hundred generations have searched for this elusive

path to peace, while a thousand wars raged across the span

of human endeavor. Today that new world is struggling to

be born. A world quite different from the one we’ve known,

A world in which the rule of law supplants the rule of the

jungle,??
Are we going to see “ticking clock” prophecy books that pro-
claim George Bush as the Beast of Revelation, whose number is
6667? Is he the Antichrist?’ Has the Republican Party become
Satan’s engine of worldwide enslavement? The average conserva-
tive American dispensationalist may have trouble believing that
he voted in 1988 for the Antichrist. After all, Jerry Falwell
supported Bush. Bush even spoke at Liberty University’s gradu-
ation ceremonies in June, 1990. In short, thinks the voter, why
didn’t someone who is an expert in fulfilled Bible prophecy
warn the Church about Bush in early 1988? No one did. The
leaders of dispensationalism are mostly conservative Republicans.
This is why we did not see “Bush the Beast” books in 1991.
This is why we probably will not see any in 1992. Or 1993.
(Maybe in 1998.)

Perhaps we will see a scenario like this one. There will be a
civil war in Russia. Or maybe there won’t be. The “New Russia”
will join the “New Europe.” Or maybe it won’t. But the im-
portant thing is that the Rapture will take place in the year
2000. Then the surviving Russians will join with the U.S. and
the U.N. to invade the nation of Israel. The Great Tribulation
will begin. This is all inevitable—either one scenario or another.
There is nothing a Christian can do to stop it. There is noth-
ing a Christian should do to stop it. If the technological or
geopolitical possibility of the invasion of national Israel is post-
poned indefinitely, then the Rapture is also postponed indefinitely,

17. “Text of President Bush’s Address to Joint Session of Congress,” New
York Times (Sept. 12, 1990).
