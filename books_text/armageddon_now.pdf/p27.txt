Foreword xxvii

This is perhaps best illustrated by the continued popularity
of Hal Lindsey’s book, The Late Great Planet Earth. As of
1988, one hundred printings had produced more than twenty-
five million copies in thirty languages. According to the New
York Times Book Review it was the best-selling non-fiction
book of the 70’s. Moody Monthly (5/88) reviewed the Late
Great phenomenon saying, “Lindsey had consciously used
prophecy as a pre-evangelism tool” (my italics). Lindsey is
quoted as saying, “I use it to shock people, then hit them with
the gospel.”

In his book, Lindsey had said, “A generation in the Bible is
something like forty years. If this is a correct deduction, then
within forty years or so of 1948, all these things could take
place.” The Moody article observed: “The phrases ‘something
like forty years’ and ‘could take place’ may save Lindsey's inter-
pretation, but it’s better to say he was wrong.” The author then
outlined how Lindsey had changed his interpretation, teaching
since 1973 that the Tribulation would come upon the generation
that saw aii the signs begin to appear, not just the rebirth of
national Israel. Lindsey now teaches that “all the signs are
present today.” Instead of making the obvious observation that
it only takes one false prophecy to make a false prophet, the
article amazingly concludes by praising the book: “Lindsey did
shake them, and God’s kingdom has benefitted.” It is, how-
ever, a very questionable benefit that undermines the credibility
of the Church, even if it is true that after reading the book
one million people have turned to Christ. The ends still do not
justify the means, even if there are a million of them.

The popularity of prophetic themes has been continuous,
stimulated by various events since the 1973 Yom Kippur War.
These events include the 1979 Egyptian-Israeli treaty, the 1982
invasion of Lebanon, the on-going revolt of the Palestinians
since 1987, the development of the Star Wars defense system,
the Soviet invasion of Afghanistan, the break-up of the Russian
Empire, and Iraq’s invasion of Kuwait. The 1988 failure of a
generation of forty years since the 1948 restoration of the state
of Israel to produce any significant culmination was tragically
re-enforced by Edgar Whisenant’s eighty-eight brash reasons
why Christ would return in September, 1988 (88 Reasons Why
the Rapture Will be in 1988). We saw a renewed flurry of
