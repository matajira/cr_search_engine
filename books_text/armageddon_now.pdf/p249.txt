Epilogue 217

compared to the obscure, Platonic, allegorical interpretations of
the early Greek church fathers.

In commenting on Hal Lindsey’s The Late Great Planet Earth,
a premillenarian writer in the United Evangelical Action has
charged, “Lindsey may be so busy looking to the future that he
hasn’t profited from the past.”! Premillenarians could profit im-
mensely from a greater consciousness of their own history.

Determinism. The response to Jews and Israel has demon-
strated that the premillenarians are guilty of the charge of de-
terminism even to the extent of heretical antinomianism. They
have expected and condoned anti-Semitic behavior because it was
prophesied by Jesus. Their consent (even though given while spew-
ing pro-Zionism out the other side of their mouths) makes them
blameworthy with regard to American as well as Nazi and Soviet
anti-Semitism. Neither as a body nor as individuals has their cry
against such inhumanity been more than a whimper. On the other
hand, the establishment of the state of Israel has been unquestion-
ingly approved with little or no consideration of the effect on
the native Arab population. Even if the right of Israel to exist
as a nation is granted, the situation still demands that the decision
be made on the basis of just and moral considerations rather than
merely on the grounds that it fulfills prophecy. Israeli conquests
have been applauded as proof of the legitimacy of literalism—the
nation was compelled toward prophetic borders. Usual definitions
of aggression and violation of international law have been ignored
in favor of prophecy.

If Russian aggression had been consistently treated with the
same determinism as Israel was, the Iron Curtain would have
been hailed as a wonderful sign of the end. This might have been
the case had America not been involved on the European front
in World War II and had the Cold War not been depicted as a
direct threat to the United States. Premillenarians also found
themselves in the dilemma of approving the prophetic expansion
of Russia while disapproving the Russian threat to Israel at Arma-
geddon. This is evidently the explanation for the confused role
assigned to the United States in the various writings on prophecy.
In general, premillenarians called for resistance to every hint of
Russian expansion, demanding conformity to international law
and justice, rather than consenting to prophetic considerations.
