Armageddon—Almost 7

of postmillennial optimism; they had not only looked toward the
culmination of the age in Armageddon, but anticipated “wars
and rumors of wars” as signs of the approaching end. R. A. Torrey,
dean of the Bible Institute of Los Angeles, wrote in 1913 before
the war ever began:

While we talk peace, we are increasing our navies and our
armies. We are squandering untold millions in schemes for
destroying the lives of our fellow men and for protection
and extension of our own nation. We talk of disarmament,
but we all know it is not coming. All our present peace plans
will end in the most awful wars and conflicts this old world
ever saw!

Such predictions, when fulfilled, produced more than just an
I-told-you-so attitude; they served believers as demonstrated proof
of the validity of their whole literalistic theological system. During
the war, Arno C, Gaebelein, editor of Our Hope, reprinted his
prewar doomsayings. In 1909 he had said, “To believe that the
age is improving, that the church and the nations are going to
establish universal peace is unscriptural,’"# and in 1911, “How
could some men expect peace in these days when every nation
trembles and makes the most colossal preparation for war!”
The premillennialist may legitimately be castigated for being a
pessimist, yet he sees himself as: an optimist looking for the
blessed hope of the rapture and the subsequent reign of the
Messiah. Any sign of the end is a signal to rejoice: “Look up...
for your redemption draweth nigh” (Luke 21:28). As Torrey
wrote for the Moody Bible Institute publication during the war,
“As awful as conditions are across the water today, and as awful
as they may become in our own country, the darker the night
gets, the lighter my heart gets.’*

The term Armageddon was figuratively applied to the war in
the popular, secular press. But some premillenarians saw the
war as the beginning of a literal, prophetic Armageddon. The
Weekly Evangel, the official publication of the recently organized
(1914) premillenarian denomination, the Assemblies of God, cited
Revelation 16:14-16 as the chronology for the end-time: verse 14
spoke of a world war, verse 15 spoke of a secret rapture, and verse
16 spoke of Armageddon. “We are not yet in the Armageddon
struggle proper, but at its commencement, and it may be, if
