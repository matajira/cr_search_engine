The Twenties Roar Faintly—Bear, Beast, and Young Lions 81

powers fighting the battles of democracy at the polls or engaging
in crusades in the vain hope of bringing world peace.”®? Such
attitudes can be partially explained as a conservative reaction
against the social gospel’s emphasis upon the salvation of human
communities rather than upon the rebirth of individuals. Never-
theless, the following assertion (which was made at the time of
the Washington Disarmament Conference in 1922) can be under-
stood only as a logical extension of theological pessimism: “It is
true, many prayers were offered on Armistice Day for the divine
guidance of the Disarmament Conference, but may we at this
time consistently pray for world peace when the answer we seck
would be in contradiction to God’s program revealed in His
Word?”*! One wonders what had become of the psalmist’s advice,
“Pray for the peace of Jerusalem” (Ps. 122:6).

In the rush to Armageddon, a question arose in the 20s con-
cerning who would fulfill the role of the Antichrist, the opponent
of Russia. The contenders for the part (in this era) were the
League of Nations and Mussolini. It must be kept in mind that
the term Antichrist is used very loosely and broadly. John, the
only writer to use the term, had said, “As ye have heard that
antichrist shall come, even now are there many antichrists”
{I John 2:18). Hence, the premillenarians could speak of com-
munism or liberalism as embodying the spirit of Antichrist which
was already at work in the world, while at the same time they
expected a personal Antichrist who would somehow coincide with
the other apocalyptic symbols, particularly in the Books of Daniel
and Revelation. The formation of the League of Nations produced
immediate speculation. The following appeared in the Prophetic
News and the Evangel, and was reprinted in a collection which
went through at least five editions:

The World War thus originated by demon teachings has
produced the result predicted in Revelation 16:14. It has
gathered together all the kings of the earth and of the whole
world. It has gathered them into a league of nations which
will become the preparation of the nations for Armageddon,
The gathering or leaguing of the nations together is the signal
that the end is in sight. The Peace Conference at Paris had
unconsciously set the stage for Antichrist and Armageddon.®?
