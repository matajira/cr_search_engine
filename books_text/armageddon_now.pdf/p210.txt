178 Armageddon Now!

father of actor-evangelist Marjoe Gortner and former president of
Glad Tidings Bible Institute, J. Narver Gortner, wrote in 1952 of
“Russia’s Origin, Character, and Doom,” assuring his readers that
Germany would be associated with Russia in the invasion of
Palestine, and that “we need not be in doubt as to the ultimate
outcome of the controversy that is now going on.”51

Concerning the threat of war, Our Hope was not quite as
pessimistic as others; it responded in 1955 to rumors of impending
atomic attack by predicting that there would be war, but probably
not now. The editor somehow discovered military realism at work
in history along with the hand of God: “The present military
strength in America is a reasonable assurance of peace for the
time being, that is, with Russia.”* Hopeful optimism took still
another form in an article which three months later spoke of
atomic activity, somber figures in the Kremlin, and world-wide
subversion: “Yet the present ominous signs, though evident tokens
of perdition for the ungodly, are harbingers of redemption for
the children of God....The dark hour precedes the dawn.”

Even prior to the interest stimulated by the Suez Crisis and
the Hungarian revolt, The Sunday School Times was pleading
God’s case against Gog. Six accusations were listed: (1} Russia
leads the world in atheism; (2) Russia leads the world in blasphemy;
(3) Russia leads the world in defiance of Almighty God; (4) Russia
is the greatest persecutor of Christians the world has ever known;
(5) Russia is the greatest persecutor of the Jews; (6) Russia leads
the world today in mass immorality.* After Suez and Hungary,
the case against Russia was even greater in the premillenarian
mind, The Evangel asserted confidently that Russia would march
into Palestine against the Jews and there meet her doom. A
lengthy article title proclaimed: “Ambitious Russia: Without Doubt
Her Greed Will Lead to Her Downfall on the Mountains of
Israel.” The author believed that it was clear that Russia had
decided to oppose Israel, but he knew from Scripture that Israel
was going to be Russia’s undoing.5¢

One attempt to grapple with the problem of moral responsi-
bility versus determinism was presented in the Evangel in a reprint
of an article by Kenneth de Courcy, editor of the Intelligence
Digest. His concern was evidently stimulated by the renewed
threat of atomic attack as the result of Russia’s launching of the
