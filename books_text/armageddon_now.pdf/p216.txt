184 Armageddon Now!

and prosperity. The United Nations and collectivism were move-
ments that perhaps foreshadowed just such an arrangement.®°
Various organizations and institutions were suggested as possible
harbingers of the coming western confederation that was to fulfill
the prophecy of a revived Roman Empire, including NATO, the
United Nations, and the United States of Europe.

Louis §. Bauman in 1950 regarded NATO as the power that
would prevent Russia from invading Europe in her program of
expansion, thus forcing her into Palestine. A few years later Charles
E. Pont in The World’s Collision credited Bauman with predicting
even many years before NATO the rise of such a power: “We
believe the North Atlantic Pact a potent forerunner of the ‘ten
kings’ if not the real ten.”81 A plan for a European Defense Com-
munity was developed in 1952 to meet the Russian threat, but
by 1954 it became obvious that, because of fear of a rearmed
Germany, France would not ratify the plan. After EDC failed,
Our Hope observed that it was the passing of another shadow,
but that it did demonstrate how a western confederation might
develop.*? Later it was said that the EDC had come to life in
the new form of NATO and that it was a shadow of the coming
revived Roman Empire.

A fear of the United Nations colossus continued to haunt all
premillenarians, as they feared the universal government of the
coming Antichrist. Even if they cast him in the role of protago-
nist against the monster Gog, they also cast him as the Beast
which the Messiah would defeat and cast into hell. Louis S.
Bauman remarked: “When the Antichrist shall attain this ‘power’
over the ‘nations,’ then the whole world will become indeed ‘United
Nations,’ united in one great super-government.”®*

As early as 1950, Bauman had spoken also of a United States
of Europe as a possible fulfillment of the revived empire. And
in 1956, during negotiations prior to the Treaty of Rome in 1957
which formed the European Economic Community, Our Hope
made the following observation:

Thus, when we see six nations of Europe (France, West
Germany, Italy, Belgium, Luxembourg, and the Netherlands)
uniting in common economic and defense schemes, and
more than that number considering seriously the formation
of a union of their nations, we think of the ten-kingdom
