Mobilizing for Armageddon 1

History, described the size and organization of the Russian army
and drew this response:

For what is this great army preparing? We believe the
answer is—a great conflict in Palestine as described in
Ezekiel 38 and 39, The Jew will be the center of the picture.
Russia may repudiate the Lord... but it will have to reckon
with Him when this great conflict takes place.t?

Furthermore, all this was an immediate prospect. In a seg-
ment headed “Russia to Lick World by 1934,” Prophecy carried
a report by a Dr. Jenkins who had been sent by the Federated
Council of Churches to review conditions in Russia. In an inter~
view, Stalin had told him, “Don’t you know that the world powers
are uniting to crush Russsia?...If we can keep them back till
1934, we will be ready for them.”!8 At the end of 1934, in an
article entitled “ ‘Prepare Warf’ ” Louis S. Bauman was saying,
“Authoritative voices in every land are telling us in no uncer-
tain tones that it is not a matter of years, but of months, when
the battle flags of the nations will again unfurl, and the scourge
of the earth will be on the march—to Armageddon!” One of
the “authoritative voices” cited was Secretary of the Treasury
Henry Morgenthau, who had said, “War in Europe in 1934
seems to me inevitable.” Bauman’s message was based on Joel
3:9, 10: “Proclaim ye this among the Gentiles; Prepare war... .
Beat your plowshares into swords, and your pruning hooks into
spears.” He referred this prophecy ta Armageddon. His theme was
that there was no hope for mankind to avoid war and that, there-
fore, the puny efforts of the peace movements were futile, wasted
efforts. “We dare affirm that the mission of every true messen-
ger of the most high God is not to preach the gospel of peace
to the unregenerate nations as such, but to preach the gospel
of peace by the blood of Christ to the unregenerate individual”—
Bauman would have none of the social gospel.2?

A more belligerent attitude was voiced by the World’s Chris-
tian Fundamentals Association in its 1937 statement on “World
Peace”:

We believe that war is contrary to divine will, and that it
is indeed a world menace, We Jament the selfishness which
sets men at one another's throats, We deplore the destruc-
tion of life and property incident to civil, national, or world
