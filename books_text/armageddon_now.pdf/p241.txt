Jerusalem! What Now? 209

conflict, but had to rely more upon what remained of the big-
power threat of Russia.

The continued drive toward European unity has served to
fertilize premillenarian speculation on the revived Roman Empire
which under the Antichrist would ultimately resist Russia or play
some other great role at the end. At the beginning of the decade
of the 60s The King’s Business, still fascinated by the role of the
United Nations, spoke of a pattern that was emerging on the
international scene that could easily be the setting for the advent
of the Antichrist."t In 1963 John F. Walvoord in The Sunday
School Times wrote of Europe’s economic revival as a prerequisite
to political revival and of the possibility of a ten-nation unifi-
cation.” This pattern foreshadowed for him the fulfillment of
end-time prophecies. Richard DeHaan emphasized the significance
of the formation of the Common Market as the revived Roman
Empire. Hal Lindsey in 1970 said he believed the Common Mar-
ket to be the revived Roman Empire and that predictions of a
United States of Europe by the year 1980 might need to be fore-
shortened in view of rapid developments. He asked, “Is it any
wonder that men who have studied prophecy for many years
believe that the basic beginning of the unification of Europe has
begun?”™ In spite of setbacks to European unity in the years
following, Moody Monthly in a 1974 article, “Prophecy and the
Common Market,” was still optimistic about the probable revival
of the Roman Empire.*

Where the United States was to fit into all these rapidly ap-
proaching events continued to be of interest to some writers.
“Is the United States in Prophecy?” was extensively discussed
in The King’s Business by the president of Detroit Bible College,
Roy L. Aldrich. After commenting on various scholars’ identifi-
cation of Tarshish in Ezekiel 38:13 as Great Britain, Aldrich
concluded:

The idea that Great Britain is to be identified as Tarshish,
and that the United States is one of the young lions, is only
a theory; and one which is contradicted by most of the
evidence. It would seem to be the part of wisdom not to
attempt to identify any modern nation as Tarshish; at least,
not until more definite evidence is available.#
