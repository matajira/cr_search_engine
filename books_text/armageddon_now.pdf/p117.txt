The Twenties Roar Faintly—Bear, Beast, and Young Lions 85

faced a clear and present danger of war from which America
could not escape. The lion and eagle were destined to stand with
God and fight Beasts and bear on behalf of God’s chosen chil-
dren. One 1929 article, “The Spiritual Values of Armageddon,”
quite aptly summarized premillenarian concern over the mo-
mentous spiritual forces struggling in the universe.

In an article in The Sunday School Times of May 7, 1927,
the writer pointed out how the political situation in Palestine
is shaping itself for that very struggle; Great Britain and the
signatories of the League of Nations pledged to defend Pales-
tine in case of invasion, while Russia and the newborn Asiatic
League of Nations hang like a threatening war cloud on her
northern horizon and turn envious eyes on the vast potential
and actual wealth of that little land which trusts in the pro-
tection of the hated merchants of Tarshish or capitalistic
nations. Ezek. 38:9-13....

Now suppose Russia should invade Palestine. This would
be not merely a challenge to the order of things existing
among the nations, but a challenge to God’s purpose and
order. It would provide a clean-cut test of Russia’s national
morality. Jehovah has promised Palestine to the seed of
Abraham forever, Gen. 13:15; 15:18. The title lies in openly
preserved documents for any one to read. If Russia tried
with eyes open to amend the covenant, would not this be a
declaration of war against Jehovah, the most immoral act of
which a nation could be guilty? And ali the nations who
abetted her would share in her guilt.
