Selected Bibliography 253

Parkes, James, Whose Land? A History of the Peoples of Palestine.
Baltimore: Penguin Books, 1970.

Petry, Ray C, Christian Eschatology and Social Thought: A Historical
Essay on the Social Implications of Some Selected Aspects in
Christian Eschatology to A.D. 1500. New York: Abingdon, 1956.

Pont, Charles E, The World's Collision. Boston. W. A. Wilde Company,
1956.

Price, Charles S. The Battle of Armageddon, Pasadena, CA: Charles S.
Price Publishing Company, 1938,

The Prophetic Word in Crisis Days. Findlay, OH: Dunham Publishing
Company, n.d.

Riggs, Ralph M. The Path of Prophecy, Springfield, MO: Gospel Pub-
lishing House, 1937,

Riley, William Bell. Wanted—A World Leader! Minneapolis (?): 1939.

Rimmer, Harry. The Coming War and the Rise of Russia, Grand Rapids:
William B, Eerdmans, 1940,

. Palestine the Coming Storm Center. Grand Rapids: William B.
Eerdmans, 1940.

Roberts, Oral. The Drama of the End-Time, Tulsa: Oral Roberts, 1963.

Rodinson, Maxime. Israel and the Arabs. Translated by Michael Perl.
Baltimore: Penguin Books Inc., 1968.

Ryrie, Charles Caldwell. Dispensationalism Today. Chicago: Moody
Press, 1965.

Safran, Nadav, The United States and Israel. Cambridge, MA: Harvard
University Press, 1963 .

Sale-Harrison, Leonard. The Coming Great Northern Confederacy: or
The Future of Russia and Germany. London: Pickering and Inglis
Ltd., 1928; Wheaton, IL: Van Kampen Press, 1948,

. The Remarkable Jew; His Wonderful Future; God’s Great
Timepiece, Thirteenth edition. New York: Sale-Harrison Publica-
tions, c. 1940. Reprinted as God and Israeli. Wheaton, IL: Van
Kampen Press, 1954,

Sandeen, Ernest R. The Roots of Fundamentalism: British and American
Millenarianism, 1800-1930. Chicago: University of Chicago Press,
1970.

Schor, Samuel. The Everlasting Nation and Their Coming King. London:
Marshall, Morgan and Scott, 1933.

Scofield, Cyrus Ingerson. Addresses on Prophecy. Swengel, PA: Bible
Truth Depot, 1910.

, end Arno C, Gaebelein. The Jewish Question. New York: Our

Hope, 1912.

, ed. The Scofield Reference Bible. New York: Oxford University

Press, 1909.

 

 

 

 
