Copyright © 1991
Dwight Wilson

Printed in the United States of America

Published by
Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711

 

Library of Congress Cataloging-in-Publication Data

Wilson, Dwight.
Armageddon now! : the premillenarian response to Russia and
Israel since 1917 / Dwight Wilson.
Pp. cm.
Originally published: Grand Rapids, Mich. : Baker Book House,
1977.
Includes bibliographical references and index.
ISBN 0-930464-58-3 {hard : alk. paper) : $25.00.—ISBN
0-930464-57-5 (pbk. : alk. paper} : $9.95
1. Mitlennialism—Controversial literature. 2. Bible—Prophecies—
Soviet Union—History. 3. Bible—Prophecies—Israel— History.
1. Title.
[BT891.W54 1991)
236',9—de20 91-39999
CIP
