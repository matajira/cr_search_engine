Before Balfour and Bolsheviks 35

fulfillment of prophecy by the founder of the premillenarian
Christian Missionary Alliance denomination, A. B. Simpson. In
Jeremiah 16:16 Simpson saw the “hunters” as Russia hunting the
Jews out of their places of exile and saw the “fishers” as Zionism
drawing the Jews back to Palestine.» He used elaborate calcula-
tions to support the significance of Zionism. He found that Daniel’s
1,260 years subtracted from the 1897 of the founding of Zionism
brought one to 637, the date that Omar under Mohammed had
captured Jerusalem. Therefore, Zionism signified the end of the
times of the Gentiles. Simpson was not sure how long it would
take God to accomplish the restoration—but he was sure it had
begun.1

These samplings of prophetic writing demonstrate that the
ptemillennialists approached the World War I era with definite
predispositions. They were pro-Zionist and anti-Russian. The cul-
mination of this predestined restoration of Israel and rise of Russia
was to be the battle of Armageddon. This, then, was the frame of
mind with which the premillenarian confronted the war and its
results.

There were also other sources of anti-Russianism and pro-
Zionist feeling in American life which, indeed, served to reinforce
premillenarian sentiment as well as soften resistance to premil-
lenarian ideas. There was strong sentiment opposing Czarist vio-
lation of human rights in Russia’s imprisonment and exile of
revolutionaries and in her anti-Semitic outbursts, At Easter in
1903 anti-Semitic violence broke out in Kishinev. Russian mis-
treatment of Russian Jews who as American citizens had re-
turned to visit Russia also produced negative public reactions.
These anti-Russian sentiments were encouraged by Jewish propa-
ganda groups, and American businessmen also encouraged an-
tagonism toward Russia as railroad and marketing interests in
Manchuria and north China found themselves in competition with
Russian interests.%%
