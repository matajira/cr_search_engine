The Certainty of Survival 173

“All such efforts have turned upon the would-be destroyer to
destroy him!’*? Premillenarians depicted Arab nationalism as a
sinister demonic force, whereas Israeli nationalism was an en-
nobling divine cail back to the Holy Land. If nationalism, however,
can be equated with pride, then in terms of a truly Christian ethic
it would seem that Arnold Toynbee’s analysis of nationalism
as the greatest sin of the twentieth century is particularly ap-
propriate.33

The Arab position in prophetic history was given a new twist
in a 1956 book by Charles E. Pont, The World's Collision. This
work was honored with an introduction by E. Schuyler English,
the new editor of Our Hope after the death of Arno C. Gaebelein
(and later an editor of the revised Scofield Reference Bible). Pont
believed that the Arabs would not be allied with Russia at the
time of her invasion of Palestine, as most writers said, but rather
would be on the side of the United States and Britain.** The
variety of possibilities which these Biblical scholars were able to
come up with was truly amazing, especially in light of the con-
fident dogmatism with which they asserted their own particular
views.

Prior to the Suez Crisis, the Evangel had shown some sympathy
for the cause of the Palestinian refugees, refusing to ascribe blame
for the situation. The editor extolled the peace that would eventu-
ally exist between Israel, Egypt and Assyria according to Isaiah
19:25. But there were inherent anti-Arab feelings even in this
attempted expression of moderation: “As we look into Bible
prophecy we see many dire predictions concerning Egypt’s future
and many golden prophecies regarding Israel.” Whatever predis-
Position that one might have to sympathize with either side in
the dispute would be overridden, however, by the anti-social
gospel admonishment, “Our responsibility as Christians is not to
worry over international affairs but to preach the gospel to every
human being.” After the Suez Crisis, though, the bedrock of
the true premillenarian attitude was thrust to the surface as the
editor said: “Today the Arabs are charging that the Jews are
trespassing on land that has been Arab for centuries. Is it not
more correct to say that the Arabs for centuries have been tres-
passing on land that God gave to the Jews millenniums agot”?®
