Publisher’s Preface
by Gory North

 

 

 

 

This work on the whole will be more a tool in the
hands of antipremillenarians then a help to premillenarians,
but premillenarians willing to be self-critical will find his
presentation is at least partially justified. Students of proph-
ecy will do well to study this volume carefully for its com-
prehensive view of premiliennial interpretation over the last
sixty years and to learn from it many important lessons ap-
plicable to interpretation today.

—John F, Walvoord, 1980?

Dr. Walvoord’s prediction that Dwight Wilson’s Armageddon
Now! would become a tool for anti-premillenarians is perhaps
the most accurate prediction he made during his long career. It
was an easy prediction to make. That a postmillennial publisher
has now republished Dr. Wilson’s book is no more remarkable
than the fact that a mainly amillennial publishing firm, Baker
Book House, published it in the first place. The manuscript
was seen as a hot potato by dispensational publishing houses
back in the mid-1970’s. The book still is.

Unfortunately for his disciples but fortunately for his bank
account, Dr. Walvoord did not take seriously the “many im-

1, J, F. Walvoord, “Review of Armageddon Now!,” Bibliotheca Sacra (April/
June 1980), pp. 177-78.
