From Sinful Alliance to Christian Realism 157

The recurring issue of who would oppose the rise of Russia
and the northern confederacy had taken some interesting twists
during the war and after. Louis $. Bauman in 1940 had continued
to see a revived Roman Empire headed up by Mussolini as the
outcome of the war. The war was to be a final struggle between
democracy and autocracy, and autocracy would win and establish
the Empire. According to Bauman, most premillenarians saw this
as the interpretation of the image’s iron and clay feet in the vision
of Daniel 2.77 Bauman explained that it was necessary for Musso-
lini to drop out of the Rome-Berlin-Tokyo axis in order to fulfill
the prophecies.?1 The whole subject was quietly dropped as the
tide of war turned against Italy.

Other premillenarians had better luck with their predictions.
In Our Hope, W. W. Fereday predicted an alliance between
Britain and Italy after World War If as part of the anticipated
revival of the Roman Empire; and although it did not come about
until 1973 when Britain joined the European Economic Com-
munity, it could be said that he was not completely wrong.”
Harry Rimmer in 1940 forecast a new League of Nations as a
result of the war—and the rise of a universal dictator. The
United Nations has arrived, but there is no dictator yet.

After the war, Arno C. Gaebelein projected a United Nations
of Europe as the embodiment of the revived Roman Empire.’
The East-West split of Europe foreshadowed for him the outlines
of the two confederacies. The formation of the North Atlantic
Treaty Organization (NATO) was interpreted to be the alliance
of ten kings in the empire.*® Harry A. Ironside, with reference
to NATO and the United Nations, asserted that “the ten king-
doms are already in process of organization.”77 Gaebelein sug-
gested in 1949 a kind of revisionist interpretation of NATO and
the Cold War when he said:

These defense measures being taken by the nations of the
West may be the very thing that will incite Russia to war
eventually. ...

In a way, the Atlantic Pact is a step toward a world of
peace, but not in the manner that Mr, Truman expects;
for it may easily be an instrument in bringing about the
aggression on the part of the East which will precede the
coming of the Lord. Actually, thea, the Atlantic Pact may
be, first of all, the first step toward war!7*
