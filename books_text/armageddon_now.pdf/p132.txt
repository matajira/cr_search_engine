100 Armageddon Now!

had been prophesied. The Turks had surrendered the city out of
fear of the airplanes flying overhead. This fulfilled the prophecy
of Isaiah 31:5: “As birds flying, so will the Lord of hosts defend
Jerusalem; defending also he will deliver it; and passing over
he will preserve it.”°* Encouraged by the large number of what
they considered fulfilled prophecies, the premillenarians anticipated
further fulfillments.

In response to the Arab strike and terrorist activities of 1936,
the British sent a Royal Commission to Palestine to investigate.
Headed by Earl Peel, the commission reported on July 7, 1937,
that the differences between Arabs and Jews were irreconcilable
and recommended the partition of Palestine into two independent
states, The cities of Jerusalem, Bethlehem and Nazareth were to
be in a separate mandated area under British control. The Arab
Higher Committee rejected the proposal and eventually the British
government also concluded it to be impractical. The premillenarian
response was varied. The Evangel initially cited one writer who
interpreted the partition as an obstruction of the prophetic scheme
because God had promised the land to Israel (not Ishmael) and
had promised to make the Jews one nation {not two states).%%
Ten weeks later the Evangel quoted from the Advent Herald,
which found the partition to be a fulfillment of Zechariah 13:8:
“And it shall come to pass that in all the land, saith the Lord,
two parts therein shall be cut off and die (the land—two thirds—
will languish under Arab control) but she third shall be left
therein.” Partition was also depicted as the fulfillment of Joel
3:1, 2: “For, behold, in those days, and in that time, when I
shall bring again the captivity of Judah and Jerusalem, I will
also gather all nations, and will bring them down into the valley
of Jehoshaphat, and will plead with them there for My people
and for My heritage Israel, whom they have scattered among the
nations, and parted my land.” At one point Britain seemed to
be extolled for establishing the independent state of Israel; at
another she was castigated for fulfilling this passage and playing
the role of Antichrist.°° Here is another illustration of the di-
lemma of the premillenarians—whether to apply the test of ethics
or the test of determinism. With Britain they were not consistent,
