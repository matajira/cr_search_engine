Foreword xxxvii

logical Seminary. He bluntly asserted: “Israel today is not the
people of God....” He lamented Jerry Falwell’s apparently
unqualified support of Israel and his association with Menachem
Begin, and then chided evangelicals for an attitude that “vir-
tually precludes the possibility of Israel’s culpability in any of
its decisions.” Jerry Falwell responded by reiterating Israel’s
special position and claiming that it was possible to “recognize
Israel’s God-given right to the land... without having to con-
done every specific act of the State of Israel.” This is the
standard pattern of giving lip-service to morality in international
affairs; but in premillenarian practice, since God has fore-
ordained the restoration, Israel’s means justify God’s ends. But
as Hanna pointed out, “Although the crucifixion of the
Messiah was foretold in the Old Testament... , that did not
absolve his murderers of their guilt.”

This brings to mind Jewish philosopher Martin Buber’s
statement given even prior to Israel’s recent expansionism:

Only an internal revolution can have the power to heal our
people of their murderous sickness of causeless hatred, It is
bound to bring complete ruin upon us. Only then will the
old and young in our land realize how great was our
responsibility to those miserable Arab refugees in whose
towns we have settled Jews who were brought from afar;
whose homes we have inherited, whose fields we now sow
and harvest; the fruit of whose gardens, orchards and
vineyards we gather; and in whose cities that we robbed,
we put up houses of education, charity, and prayer while
we babble and rave about being the “people of the book”
and the “light of the nations.” Ghud's Ner 1/61).

In 1977, John Wesley White said, “William Scranton,
United States Ambassador to the United Nations, said in March
1976 that Israel’s reoccupation of Jerusalem in 1967 was illegal
insofar as the United States government is concerned. Insofar
as God’s Word is concerned, it is both legal and inevitable.”
(WWII, p. 156). In a 1984 interview Pat Robertson updated
this attitude in response to Christianity Today’s question,
“Should Christians approve any action taken by Israel to ac-
quire additional iand? This includes the present disputed area of
the West Bank as well as the ‘Dan to Beersheba’ perimeters.”
Robertson replied affirmatively saying, “It would certainly in-
clude the Golan Heights area, and would take the border past
