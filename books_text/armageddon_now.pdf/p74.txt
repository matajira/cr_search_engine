42 Armageddon Now!

This was then the crescendo of anticipation into which the
Balfour Declaration was introduced. The essentials of the text
are contained in one sentence, the latter half of which was con-
sistently neglected in the premillennialists’ response:

His Majesty's Government views with favour the establish-
ment in Palestine of a national home for the Jewish people,
and will use their best endeavors to facilitate the achieve-
ment of this object, it being clearly understood that nothing
shall be done which may prejudice the civil and religious
rights of existing non-Jewish communities in Palestine, or the
rights and political status enjoyed by Jews in any other
country.

President Woodrow Wilson had been a party to the preparation
of the Balfour Declaration and he enthusiastically endorsed it.
Prophetic considerations were evidently influential in his decisions,
or at least supported him in those decisions. As one student of
Zionism and American foreign policy has analyzed him: “As the
son of a Calvinist minister he had been raised on Old Testament
tales of Jewish exile and longing for a return to Israel.”** Indeed,
Rabbi Stephen Wise reported him to have said, “To think that I,
a son of the manse, should be able to help restore the Holy Land
to its people!”2”7 Wilson too did not attach much significance to
the limitations of the lattér part of the declaration, for he told
Wise, “Palestine will be yours.”

The response of the Evangel representing the more emotional
wing of premillennialiasm was euphoric;

Do not we, who are looking for the coming of our Lord,
and the “New Jerusalem,” feel a thrill go through us as
we read of the dry bones coming together (Ezek. 37), and
the words of Christ when He said, “Now learn a parable of
the fig tree’? ,., Hallelujah, our summer is nigh!

Another writer mixed his ecstasy with a patriotic Manifest Destiny:

Today we are thrilled when we think of the sacred privi-
lege that has been granted to our beloved country in being
allowed by God to enter into the Holy City, and to thus
deliver it from the thraldom of both the Turk and the
Moslem....We are, therefore, likely to see in the near
future a Jewish State in Palestine. It may be in a month
or two, it may be in a year or two; but the intention is to
restore the land to the people and the people to the land,9°
