Footnotes 245

82. Charles 3. Woodbridge, “Armageddon and the Millennium," Moody
Monthly, LXIII (September, 1962), 69-75, DeHaan, “Russian Communism,”
p.9

83. S. Maxwell Coder, “The Future of Russia,” Moody Monthly, LXIV
(September, 1963), 74.

84, Wilbur M. Smith, “A Prediction Concerning India,” Moody Monthly,
LXVII (February, 1967), 58-59.

85, DeHaan, Israel and the Nations in Prophecy, p. 122.

86. Lindsey, The Late Great Planet Earth, p. 72.

87, John F. Walvoord, “Converging Signs of Our Times,” Moody Monthly,
LXIV (July-August, 1964), 22.

88. The Prophetic Word in Crisis Days, pp. 122-23.

89. “Russia’s Cold War on Christians,” Moody Monthly, LXV (December,
1964), 16.

90. Evangel, July 15, 1973, p. 24.

91. Ayer, “Anti-Christ,” p. 25.

92. John F. Walvoord, “End-Time Prophecies,” The Sunday School Times,
CV (August 3, 1963), 548.

93. DeHaan, Israel and the Nations in Prophecy, p. 50.

94. Lindsey, The Late Great Planet Earth, pp. 94, 96-97.

95. Edgar C. James, “Prophecy and the Common Market,” Moody Monthly,
LXXIV (March, 1974), 24-27.

96. Aldrich, “Is the United States in Prophecy?” p. 23.

97. Frank M. Boyd, “Will Russia Attack Israel?” Evangel, September 3,
1967, p. 3.

98. Lindsey, The Late Great Planet Earth, pp. 184, 95, 163.
99. Evangel, January 17, 1973, pp. 30-31.

100, Flood, “Israel: Land of the Return,” p. 23, Nadav Safran, The United
States and Israel, p. 271.

101, Olson, “Israel After 25 Years,” pp. 14-17.

102. Sevener, “Israel: The World's Timetable,” p. 32.

103, “Reflections on Israel’s Birthday,” Eternity, XXIV (May, 1973), 12.
104. Christianity Today, XVIL (May 11, 1973), 27.

105, Evangel, November 18, 1973, p. 31. Christianity Today, XVII
(October 26, 1973), 119.

106. “Christians and Israel,” Time, December 31, 1973, p. 57.

107. Judith Banki, Christian Responses to the Yom Kippur War, p. 114.
108, Ibid.

109. Earl Raab, “Is Israel Losing Popular Support? The Evidence of the
Polls,” Commentary, LVIL (January, 1974), 26-29.

110, Donald Barnhouse, “Worldview,” Eternity, XXIV (December, 1973),
Si.

111. Banki, Christian Responses to the Yom Kippur War, p. 42. “Christians
and Israel,” Time, December 31, 1973, p. 57.

112. Malcom Conch, “Let’s Not Let Israel Down,” Moody Monthly, LXXIV
(une, 1974), 30.
