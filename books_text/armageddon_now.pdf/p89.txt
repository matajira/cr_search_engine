Armageddon—Almost 57

churches of the United States are facing a crisis in religion.”®?
“Its injurious effect can already be seen in hundreds of churches
throughout the United States.”83 But the premillenarians gleefully
quoted their critics. Our Hope cited the lament of the president
of Auburn Theological Seminary:

The spirit of premillennialism is dividing the whole Protestant
Church of America.... Doctor Chapman and Billy Sunday
and nearly ali the evangelists hold this doctrine. Here is a
school of thought that is captivating the church of today.S+

The focus of all this criticism was the failure of the pre-
millenarians to join the great cause of the social gospel and to
help usher in the great age of the Kingdom of God on earth.
The social gospel was, and has continued to be, rejected by
premillenarians on a theological basis. Another point of criticism
was much more telling, however, and has seemingly grown more
goading through the years. This was the censure of the generally
nonpolitical bent of the premillenarian and his attitude toward the
state and toward social improvement. This prevailing attitude of
the premillenarian had been expressed in Nathaniel West’s 1889
book, The Thousand Years: “What we are pleased to call the
Christian State, is simply the Christian-Beast, either the Horns
of the Beast, or the Toes of the Colossus!—a Beast whose power
is in check for the present, but soon to be unchecked and drive
Christianity back to the wall.”85 The charge against such a pre-
disposition was irresponsibility:

Nineteen centuries have passed by, during which, according
to adventism, this new age has been imminent. There is
nothing in premillennial teaching to compel us to believe
that the world may not need to wait nineteen or twice nine-
teen centuries more, since, according to men like Dr. Sco-
field and Dr, Pierson, “imminent” with premillennialists
means simply “next on the docket,” whether near or remote.
For an indefinite period, then, adventism has nothing to
suggest to us but a passive pessimism over against a pagan
and hopelessly evil social order.86

Premillenarians may have been irked by the criticism, but they
were not chagrined, not at the time anyway. They went merrily
on their doomsaying way, interpreting not every falling leaf—as
