Before Balfour and Bolsheviks 27

for a multitude of later writers on the subject. He cited Pliny,
the Roman writer, for the information that “Hierapolis, taken by
the Scythians, was afterwards called Magog.” The Jewish historian
Josephus was quoted: “The Scythians were called by the Greeks
Magog.” No sources were given for the assertion that Caucasus
is derived from the words Gog and chasan and means “Gog's
fortified place.” According to Josephus the Moscheni inhabiting
the Moschi mountains east of the Black Sea were founded by
Meshech. To Herodotus was attributed the information that the
Muscovites came from Pontus in Asia Minor. It was claimed
that the Araxes River was called Rosh in Arabic and that Russian
was derived from Rosh. From these rather obscure historical prem-
ises Cumming made an illogical leap: “We arrive at the con-
clusion that Rosh, Meshech, Tubal, find their descendants at this
moment in the northern and southern parts of Russia.”%

Russia’s allies in this great end-time confederacy were similarly
revealed:

Now it happens that Xenophon, Pliny, Strabo, Cicero, Jose-
phus, and the modern Bochart, all state that Gomer’s three
sons settled in the territories of Asia Minor; but as a matter
of history the sons of Gomer soon extended beyond these. ...
Advancing again along the Danube, these same descendants
of Gomer peopled what is now called Germany, the name
being derived from Gomer....Some of the descendants of
Gomer again spread into Gaul. . , 5?

 

After having identified the Biblical names geographically, Cumming
added a time element:

Now the prophecy of Ezekiel is that Gomer—that is, Ger-
many, which is the great or the father-nation of all the
rest—will be added to the prince of Rosh, Meshech, and
Tubal; and that this combination of Germany and Russia
will be the chief part of the great confederacy or conspiracy
of the last days, that will go forth to cleave its way, as this
chapter indicates, to the land of Palestine, there and then
to perish for its crimes under the judgments of God.

That much has proved to be standard premillenarian fodder
clear down to the present day, but each era has given to it a con-
temporary flavor just as Cumming did.

I do not say our existing complications are the fulfilment;
