18 Armageddon Now!

Charles I was adversely identified by various writers as the little horn
of Daniel 7:8, as one of the ten horns of the Beast of Revelation
17:12-15, and as the king of the north of Daniel 11:15-19.1?
As far as Israel was concerned, Tillinghast’s interpretation led
him to identify the 1,290 days of Daniel 12:11 as the years
between 366, when the Jewish Temple was supposedly destroyed,
and 1656, when he expected the Jews to be converted and return
to Palestine.? Similarly, John Canne predicted that the Jews in
1655 would both defeat the Turks, who controlled Palestine, and
return to their homeland, where they would be converted in 1700.
Mary Cary forecast the conversion and return for 1656.1* Such
speculation among English Puritans declined after the restoration
of the monarchy. The American, Increase Mather, in a 1669
work entitled The Mystery of Israel’s Salvation Explained and
Applyed, affirmed the following beliefs:

That after the Jews are brought into their own land again,
and there disturbed with Gog and Magog (not John’s, but
Ezekiel’s Gog and Magog, at the battle of Armageddon}
who shall think with great fury to destroy the converted
Israelites...

The Jews who have been trampled upon by all nations,
shall shortly become the most glorious nation in the whole
world, and all other nations shall have them in great esteem
and honor, Isa, 60:1, 3...

That the time will surely come, when the body of the
twelve Tribes of Israel shall be brought out of their present
condition of bondage and misery, into a glorious and won-
derful state of salvation, not only spiritual but temporal.’*

 

Eschatological interests continued to concern the American Puritans
in the eighteenth century, but the focus was not premillennial.

America’s first major postmillennial thinker was Jonathan Ed-
wards. His views paralleled those of Daniel Whitby (1638-1725),
the founder of modern postmillennialism. This scheme became
the standard American interpretation from which the twentieth-
century premillennialists would dissent. The restoration of Israel
continued to be a prevailing theme even of the postmillennialist
literature. Whitby’s 1703 work, Paraphrase and Commentary on
the New Testament, included “A Treatise of the Millennium:
Shewing That It Is Not 4 Reign of Persons Raised from the Dead,
but of the Church Flourishing Gloriously for a Thousand Years
