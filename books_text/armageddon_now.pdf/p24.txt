xxiv Armageddon Now!

The sad thing is that fringe Rapture scenarios are becoming
wilder and wilder, mixed with pyramidology, UFO's, and other
occult materials.18 As the year 2000 approaches, this “invasion
by the fringe” will escalate. Both Dr. Wilson and I are more
concerned with this invasion and its consequences than in
embarrassing those who in the past have made preposterous
predictions in good but misguided faith. This escalation of ex-
pectation of the Rapture will tend to paralyze the Church as an
institution of salt and light as the 1990’s unfold. When this ex-
pectation is fueled by occultism, it cannot have anything but
negative consequences.

A clock is indeed ticking. It is the clock of responsibility.
We have all been given assignments by God and enough time
to complete them in life (Eph. 2:10). Christian institutions have
been given assignments by God through their officers, This is
why eschatology matters. This is why the Institute for Christian
Economics sometimes publishes books on eschatology. A person’s
time perspective is important for the tasks he begins, the capital
he invests, and the rate of return he targets. The shorter the
time remaining, the more capital we need when we begin our
tasks and the higher the rate of return we need to complete
them. This is also true of God’s Church. Each person, each
church, each family, each civil government, and each organiza-
tion must decide how much available time seems to remain.
Our goals and plans, both personal and institutional, should re-
flect this assessment.

False prophecies regarding an inevitably imminent Rapture
distort this assessment.

Tyler, Texas
August 30, 1991

18. William M, Alnor, Soothsayers of the Second Advent (Old Tappan, New
Jersey: Revell, 1989), Part IV.
