134 Armageddon Now!

15:18 gives Israel alk of this ‘Land, and the edict of God

to Joshua was that he should walk upon it and claim it.5t
But no solution to this Scriptural dilemma was offered. Morris
Zeidman, writing in the Evangel, expressed the personal struggle
shared by these moralists:

My sympathies are with the Jews, and I believe that Pales-
tine has been promised by both God and men to the Jewish
people; but the methods that are being used today to
make Palestine a Jewish state cannot be considered Christian
and therefore I believe are not of God. ..,

It is most difficult for the Christian to take part or
take sides in the politics of the world, The Christian cannot
approve of the tactics used in Palestine today.

As time passed, the general trend, though, was approval of
Israel’s accomplishment. T. A. Lambie in The Sunday School Times
of July 16, 1949, asserted, “As I read my Bible, the events that
are taking place are the beginning of the fulfillment of prophecy
for the final days.”53 Even the most moderate Alliance Weekly
called the new state of Israel of the “utmost importance,” and did
risk stating the premillenarian consensus that “the time is draw-
ing near for the fulfillment of the ancient predictions.” But a word
of caution was quickly added: “But to us, as to the Apostles, the
same word comes: ‘It is not for you to know times or seasons’—
they rest still in the authority of the Father.”5+

The city of Jerusalem was especially important to the pre-
millenarian mind; the Evangel noted particularly a special decree
by the Israeli cabinet announcing that Jerusalem would be con-
sidered part of Israel.55 The old part of the city would not be
taken until 1967, but the government soon announced that the
new part (which was in Israel’s control) would be the capital
of the new state in spite of opposition by the United Nations to
such a provocative decision. The Evangel’s response to this action
reflected the premillenarian attitude for the next nineteen years—
anticipation of Israel’s annexation of Jerusalem: “When the Jews
gain control of Jerusalem, the times of the Gentiles will be
ended.”55 And The King’s Business added, “We feel that this is
another important step in the great chain of events.”57

Other steps would eventually lead, according to premillenarian
thought, to expansion all the way from the river of Egypt to the
