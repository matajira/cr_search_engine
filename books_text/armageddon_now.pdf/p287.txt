Selected Bibliography 255

Toon, Peter, ed. Puritans, the Millennium, and the Future of Israel:
Puritan Eschatology 1600-1660. Cambridge: James Clarke and
Co., 1970,

Torrey, Reuben A. The Return of the Lord Jesus. Los Angeles: The
Bible Institute of Los Angeles, 1913.

———. What War Teaches; The Greatest Lessons of the Year 1917.
Los Angeles: Biola Book Room, 1918.

Toynbee, Arnold J. A Study of History. Six volumes. New York: Oxford
University Press, 1939.

Trumbull, Charles G. Prophecy's Light on Today, New York: Fleming
H. Revell, 1937.

Tuveson, Ernest Lee. Millennium and Utopia: A Study in the Back-
ground of the Idea of Progress. Berkeley: University of California
Press, 1949,

. Redeemer Nation: The Idea of America’s Millennial Role.
Chicago: University of Chicago Press, 1968.

Vernadsky, George. Ancient Russia. New Haven, CT: Yale University
Press, 1943,

Walker, William H. Will Russia Conquer the World? Miami: Miami
Bible Institute, 1960.

Walvoord, John F. Israel in Prophecy. Grand Rapids: Zondervan, 1962.

West, Nathaniel. Daniel’s Great Prophecy, the Eastern Question of the
Kingdom. New York: The Hope of Israel Movement, 1898.

. Premillennial Essays of the Prophetic Conference Held in the

Church of the Holy Trinity in New York City. Chicago: Fleming

H. Revell, 1897,

» The Thousand Years: Studies in Eschatology in Both Testa-
ments, Fincastle, VA: Scripture Truth Book Company, n.d., 1889
reprint.

Williams, William Appleman. American-Russian Relations, 1781-1947,
New York: Rinehart and Co., Inc., 1952,

Wise, Stephen. Challenging Years: The Autobiography of Stephen Wise.
New York: G. P, Putnam's Sons, 1949.

 

 

 

PERIODICALS
The Alliance Witness
Annuals of the Southern Baptist Convention 1845-1953 with Index of
Convention Proceedings.
“Bible Prophecy and the Mid-East Crisis.” Moody Monthly, LXVIL
(July, 1967), 22-24,

Bowman, John Wick. “The Bible and Modern Religions, II, Dispensa-
tionalism.” Interpretation, X (April, 1956), 170-187.
