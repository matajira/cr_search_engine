166 Armageddon Now!

to dispossess Arabs of their land even before 1948, He cited the
King-Crane Commission report to President Woodrow Wilson in
1919 which had mentioned the Jewish objective of complete dis-
possession. On the refugee issue, he quoted an American mission-
ary’s eyewitness report that the Jews had sent trucks equipped
with loud-speakers through the Arab areas threatening Arabs with
annihilation if they did not leave. United Nations documents were
offered as evidence that the Israelis had continued to expel and
displace thousands of Arabs from the border zones even after
the 1949 armistice agreements." Such presentations did not sud-
denly flush premillenarian prejudices away, but they did begin
slightly to erode confidence in the righteousness of Israeli expan-
sion. This broadening process of new knowledge undermined the
self-confidence of narrow-mindedness.

The Evangel attempted to dull the cutting edge of Toynbee’s
criticism by reporting the response of Abba Eban, the Israeli
Ambassador to the United States. Toynbee claimed that the tra-
ditional Jewish view had been that any restoration had to stem
from divine initiative—not from human. endeavor. Eban had re-
plied: “It is true that the Hebrew orthodox doctrine of history
describes the restoration as a Divine purpose, but it also describes
it as a Divine purpose which human effort should strive to ac-
celerate....If something is willed by God, then it is the duty
of man in his material life to strive for its fulfillment.”

Striving for fulfillment included conspiring with the former
imperialistic powers, Britain and France, to attack Egyptian terri-
tory in 1956. Britain hoped to regain control of the Suez Canal
which had been nationalized by Egypt; France hoped to elimi-
nate Egyptian support of Algerian rebels in North Africa; and
Israel wished to eliminate the Egyptian military threat and ha-
Tassment and to annex more territory. Egypt’s President Nasser
had been building his military might with Czechoslovakian arma-
ments and, as a consequence, the United States had withdrawn
American financing of the Aswan Dam. Nasser had, in turn,
responded by nationalizing the canal to get revenue and to assert
his independence of the West. In accordance with a secret agree-
ment signed at Sévres, France, on October 23, the Israelis at-
tacked Egypt on October 29 and the British and French occupied
