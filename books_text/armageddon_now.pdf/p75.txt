Armageddon—Almost a3

Clearly some people let their hopes outweigh the terminology
in the declaration; in a pamphlet on “The Lessons of 1917,” R. A.
Torrey referred to “England’s declaration that Jerusalem is to be
turned over to the Jews’*1—something more than merely “the
establishment in Palestine of a national home.” Our Hope ob-
served that, of course, the restoration of Israel had to come to
pass “some time”—it was “impossible that this should not come
to pass.” The Jewish movement was referred to as “the sign of
all signs.”

The considered response of W. W. Fereday in an editorial in
Our Hope a year and a half later was perhaps the best synthesis
of the many implications of the Balfour Declaration for the
premillenarian:

Palestine for the Jews. The most striking sign of the times
is the proposal to give Palestine to the Jews once more. They
have long desired the land, though as yet unrepentant of
the terrible crime which led to their expulsion therefrom.
November 2, 1917, was a red-letter day in the world’s history
when the British Foreign Secretary addressed his now fa~
mous letter to Lord Rothschild on this subject. Prophetic
Scripture supposes the Jewish people back in the land dur-
ing the next crises, Thus in Rev. xi. a temple is divinely ac-
knowledged in Jerusalem, and Dan. ix. 27 speaks of a treaty
to be made by the head of the Western Empire with the
people. Isaiah xviii. 1-6 distinctly speaks of an effort to be
made by a maritime Power to restore the Jews to Palestine
apart from God. Man is busy, but God is not moving, yet
He considers in Heaven his dwellingplace. There is a mass
of Prophetic Scripture yet to be accomplished, but no proph-
ecy can be accomplished until Palestine is again in Jewish
hands, Prophecy revolves around the despised Jew: and if
Jewish restoration is imminent (as it appears to be), how
near we must be to the fulfillment of every vision!

The premillenarian responses may be contrasted with that of
the Christian Century, which Hertzel Fishman has suggested em-
bodied the mainstream Protestant reaction of that era. Fishman
says, “In the month following the publication of the Balfour
Declaration in November, 1917, the Christian Century had warned
against taking seriously the millennarians who were euphoric with
‘Second Coming propaganda.’” The warning was felt to be neces-
sary due to the “wide vogue” of premillenarian doctrine.“ This
