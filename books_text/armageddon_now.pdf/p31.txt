Foreword xxxi

raptured away before any such event. And in the second place,
they believe the Bible teaches of on-going prophetic events in
the post-Armageddon era. As the lead in Billy Graham's article
“The Grand Finale” in Charisma (4/84) says: “Will the earth
end in nuclear holocaust? If so, over what will Christ reign for
1,000 years?” Graham says, “if won't heppen. God has other
plans for the human race! Life is not going to be brought to a
catastrophic end. God’s intervention will see to that.”

Fortunately or providentially (depending on one’s frame of
reference), the world survived the Reagan era, and the Cold
War has currently been de-fused. Whether the credit should go
to the mildness of Mikhail Gorbachev or the tough stance of
Ronald Reagan is left for historians to debate. The premillenar-
ians’ determinism that leads to inevitable war has certainly not
contributed much toward peace. They continually give lip-service
to “pray for the peace of Jerusalem,” but they have expected
war—sooner or later. The tendency is for such doomsaying to
become self-fulfilling if the doomsayers attain power.

This prevailing determinism, however, was given an innova-
tive twist in the response to Israel’s invasion of Lebanon in
1982. David A. Lewis offered a modified determinism in his
book Magog 1982 Canceled. He explained that the Russians
had set a date for the invasion of Israel and had stockpiled
two billion dollars’ worth of arms in Lebanon, which were then
captured by the Israelis, who had thereby forestalled the inva-
sion of Israel by Magog (Russia). They thus prevented the war
which would have led directly into Armageddon. Lewis cites an
anonymous U.S. government official as saying, “Israel saved
the whole world from a bloodbath,”

Lewis recalls how Pat Robertson in April had predicted the
June invasion saying, “It is almost certain that Israel will at-
tempt to preserve its interests by striking against southern
Lebanon and Syria.” Robertson had said in February, “The on-
rush of events toward the end of the year may see the world
in flames.” Lewis told of reports of orthodox rabbis also pre-
dicting a Russian invasion, but concluded that “depending on
many factors God may allow the battle to take place at one
time, or if other factors prevail, based on human action, the
battle could take place at another time-in relationship to the
end-time scenario” (my italics). In this case the Israelis had
