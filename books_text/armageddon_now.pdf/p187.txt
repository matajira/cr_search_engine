 

From Sinful Alliance to Christian Realism 155

element this moral crusade was not moderated by any realistic
fear of retaliation or martyrdom. It was antinomianism—while
refusing to leave everything fatalistically in the hands of God
(since everything was predetermined according to prophecy any-
way), proponents of this philosophy failed to nourish any healthy
skepticism (of their own moral crusade) that could have pro-
duced sensible caution. Since all was predetermined, how could
they possibly go wrong?

As the Cold War began to develop, detailed analyses appeared,
such as Dan Gilbert’s Russia’s Next Move: In the Light of Bible
Prophecy. He noted that secular writers could not understand
Russia’s interest in moving against Iran, but prophecy students
knew that it was in fulfillment of Ezekiel’s prophecy (38:5)
that Persia would be an ally of Russia. One of Russia’s next moves
would be an occupation of Turkey as she moved south to annex
Ethiopia and Libya, another fulfillment of Ezekiel 38:5. “A
study of Soviet diplomacy indicates that Stalin is now in the
process of building the very Empire outlined in Ezekiel 38-39."

This sense of impending crisis paralleled the growing hostilities
in Palestine and resulted in renewed expectations of Armageddon.
The Evangel underscored columnist Dorothy Thompson’s piece
headed “Armageddon” which had stated: “It is entirely possible
that World War III will start in ninety days.“ In April, 1948,
an Evangel article, “God and the World Crisis,” stated that very
likely the Arabs would ask help from Russia, and Russia would
accept the opportunity. “Nine countries of Eastern Europe are
already united. The stage is setting for the great battle of Arma~
geddon.” This supposedly meant that the United Nations was
doomed.®t Anticipation heightened in 1948 as Israel declared her
independence, the Communists took over Czechoslovakia, and the
Berlin blockade was established. Our Hope’s observation on the
blockade was that the end might be near, and “the Moscow Bear’s
paw will crush all opposition east and southeast, but not toward
the west.”62 At the end of November, the Evangel was explaining
why the nations of the world were insanely spending one-third
of their incomes on the arms race: they were under the diabolic
influence of the three unclean spirits of Revelation 16:13 which
were gathering the nations to the battle of Armageddon.
