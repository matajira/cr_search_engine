116 Armageddon Now!

The proof which he offered for Gomer was “the migration of
Ashkenaz, son of Gomer, who peopled the land of Germany.”*®
But by the middle of 1937 such dogmatizing turned to speculation
about the actual probability of such an alliance. The Evangel
asked, “A Soviet-German Alliance?”

For years students of prophecy have seen the prediction of
a Russo-German alliance in Ezekiel, chapters 38, 39, Recent
antagonism between the two countries has not favored this
interpretation, But a change seems to be taking place. Ac-
cording to one newspaper correspondent, “prospects are
at last beginning to emerge, in the opinion of informed
observers here (in London}. With the whole of Europe's
diplomatic alignments evidently undergoing one of their
periodic overhauls, it is felt that the possibility of Germany
and Russia gravitating towards one another again cannot
be excluded.""9

Advent Witness was cited by the Evangel for the analysis that
the purpose of Stalin’s current anti-Semitism was to curry favor
with Hitler and attract German technical advisors.40 According
to Prophetic News, Germany’s General von Ludendorff was strong-
ly advising Hitler to come to terms with Russia and use Russian
resources“! Revelation grasped at straws, speculating that the
recent marriage of Prince Louis Ferdinand of Prussia, the grand-
son of the former Kaiser, to Grand Duchess Kira of Russia,
daughter of the Czarist pretender, might be the basis of a revived
monarchy that would ally the two countries“? Advent Herald
claimed that a secret plot had been discovered that had advocated
overthrowing Hitler and collaborating with the Russians.‘

Such hopes were supported by similar speculation from the
secular press. In 1939, the Evangel cited a Paris correspondent:
“When you come down to brass tacks, there is no obstacle now
to Russo-German friendship, which Bismarck advocated so strongly,
save Hitler’s fanatic fury against what he calls ‘Judeo-Bolshevism."
But Hitler is not immortal and dictators can change their minds
and Stalin has shot more Jews in two years of the purge than were
ever killed in Germany.”** Louis §. Bauman in Moody Monthly
quoted Princess Catherine Radziwill’s article in Liberty: “The
Joseph Stalin of today is no Bolshevik....Nor do I think I shall
be much astonished when, one day not far hence, ‘enemies’
Joseph Stalin and Adolf Hitler decide, publicly, to shake hands.”
