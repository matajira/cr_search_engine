Balfour Betrayed 93

than the Hebrews, and we predict that history will repeat itself.”
This editorial was the occasion for an interesting exchange with
a reader. The editor had observed that the Jewish minority in
Palestine would never quietly submit to the will of the Arab
majority there. One reader took issue in a letter to the editor,
saying it was not easy to see why the majority should not rule in
Palestine just as they did in democracies in the United States and
the United Kingdom. The editor’s terse rebuttal was a straight-
forward statement of the premillenarian mind which believes that
the normal rules of international law do not apply to the Jews.

The British limitation of the Jewish population in Palestine
is doubtless good human statesmanship for the time being,
but when Great Britain or any other nation begins to deal
with the Jew in Palestine it enters on supernatural territory,
and the ordinary rules of statesmanship do not work,

Our friend thinks that the Jews in their God-given land
should govern themselves, or be governed by the princi~
ples that maintain in a democracy like the United States;
but with all his intelligence and piety he has read the Scrip-
tures to small purpose when he thus thinks. The Jews were,
are, and always shall remain a peculiar people, and the
reason why a “reversion” should be “sanctioned” in their
case, and why it certainly will come to pass, is because Je-
hovah has so ordained it,?7

This idea, that God blesses those who help the Jews and curses
those who oppose them, regardless of circumstances, was also
a major theme of Louis S. Bauman’s 1934 book, Shirts and Sheets8

There is some irony in these suggestions of the proper direc-
tion for governmental policy, for the premillenarians along with
other conservative Christian groups opposed church involvement
in politics and social action as advocated by the liberal churches—
the so-called social gospel. But this kind of issue involved pre-
tillenarians at a very deep emotional level. They had likewise
involved themselves in the political issue of national prohibition
during the 1920s and early 30s and to a lesser extent (in the
periodical literature, at least) in opposing the Catholic presi-
dential candidate, Al Smith, in the 1928 election. By and large,
however, they were reluctant to let their approval or disapproval
of policies grow to the level of political action.*®
