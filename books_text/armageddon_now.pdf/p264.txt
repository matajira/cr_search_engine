232 Armageddon Now!

35. Arno C, Gaebelein, The Conjlict of the Ages: The Mystery of Lawless-
ness: lis Origin, Historic Development and Coming Defeat, p. 144.

36. Chalmers, “Russia and Armageddon,” p. 6.
37. Evangel, August 4, 1934, p. 7 (italics mine).

38. Leonard Sale-Harrison, “The Approaching Combination of Nations As It
Affects Palestine,” Moody Monthly, XXXVIL (September, 1936), 18.

39. Evangel, June 26, 1937, p. 9.

40. Evangel, October 23, 1937, p. 7, citing Advent Witness.
4. As cited in Evangel, December 11, 1937, p. 10.

42. As cited in Evangel, August 20, 1938, p. 5.

43. As cited in Evangel, Febtuary 25, 1939, p. 16.

44, Evangel, April 22, 1939, p. 9.

45. Bauman, “Russia and Armageddon,” p. 287.

46. Moody Monthly, XXIX, (September, 1938), 287.

47. Dan Gilbert, “Views and Reviews of Current News,” The King’s Business,
XXX (January, 1939), 8.

48. Evangel, September 16, 1939, p. 9.

49, Louis S. Bauman, “Gog & Gomer, Russia & Germany, and the War,”
The Sunday School Times, LXXXI (December 16, 1939), 911-12,

50. Arno C. Gaebelein, “The Great Coming North-Eastern Confederacy,”
Our Hope, XLVI (1939-40), 234-35.

51. Evangel, November 11, 1939, p. 6.

52, Evangel, December 23, 1939, p. 11.

53. Prophecy, Ul (December, 1931), 14.

54. Evangel, March 25, 1933, p. 5, and June 16, 1934, p. 5. Louis T. Talbot,
“The Anny of the Two Hundred Million,” The King’s Business, XXII (Oc-
tober, 1932), 424,

55. Evangel, September 11, 1937, p. 7. See also Evangel, February 5, 1938,
p.5.

56. H. A. Ironside, “The Kings of the East,” The King’s Business, XXIX
(January, 1938), 9.

57. Bauman, “Russia and Armageddon,” p. 286.
58. Prophecy, III (November, 1931), 22 {italics mine).
59. As cited in Evangel, September 3, 1932, p. 10.

60. Louis S. Bauman, “Socialism, Communism, Fascism,” The King’s Busi-
ness, XXVI (August, 1935), 293.

61. McClain, “The Four Great Powers of the End-Time,” p, 97.

62, Gaebelein, The Conflict of the Ages, p. 144,

63. As cited in Evangel, June 5, 1937, p. 7.

64. Our Hope, XLI (1934-35), 377.

65. As cited in Evangel, August 8, 1936, p. 11.

66. Steil, “The Trend Toward Armageddon,” pp. 2-3.

67, Bauman, “Socialism, Communism, Fascism,” pp. 293-94,

68. McClain, “The Four Great Powers of the End-Time,” pp. 96-100.
