92 Armageddon Now!

attitude has proved to be mistaken. In 1933 it reported the thirty-
three-year lease of 17,500 acres for colonization by the Jews
with an option to renew the lease for two additional periods
of thirty-three years. The writer then blithely editorialized: “We
believe that, long before the first thirty-three years expire, the
Divine Ruler will come who will give them the land forever. Ezek.
37:25-28,"22

Fulfillment of prophecy was not the only basis of premillenarian
support for the restoration; there were also selfish motivations.
As the Evangel bluntly put it, “Some may ask, ‘What is the practical
value to us of all these things happening to the Jews?’ There is
much value: for great blessing comes to the Gentiles in the
restoration of the Jews.’?8 Zechariah 8:23 was cited as supporting
evidence; “In those days shall it come to pass, that ten men...
shall take hold of the skirt of him that is a Jew, saying, We
will go with you: for we have heard that God is with you.” But
according to the usual premillenarian interpretation of this pas-
age—certainly according to The Scofield Reference Bible—this
Tefers to a time after the church has been taken out of the world.
Hence whatever happens to the Jews at that time will be of value
to “the Gentiles,” but certainly too late to be of any “practical
value” to the church.

Another argument was an appeal to American nationalism:
“Every nation on the face of the earth that has persecuted and
mistreated the Jew has had to pay for it....That is the reason
America is enjoying more prosperity than the rest of the world.”
It was also explained that the real reason the Czar had been shot
by the Bolsheviks was his terrible treatment of the Jews.** In a
1938 comment on President Roosevelt’s decision to allow Jewish
tefugees from Germany to come to America, the Evangel stated:
“Nations, like individuals, reap as they sow. And we are confi-
dent that the nation which grants refuge to the wandering sheep
of the house of Israel shall in nowise lose its reward.” This ex-
pectation of reward was based on Genesis 12:3: “I will bless
them that bless thee, and curse him that curseth thee.”25

This same argument was used in opposing Britain’s decision
to limit Jewish immigration to Palestine. Moody Monthly prophe-
sied doom: “Pharaoh issued a similar edict in Egypt some millen-
niums ago, but it worked more disastrously for the Egyptians
