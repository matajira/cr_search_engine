Footnotes 231

4, Evangel, February 12, 1938, p. 7.

5. Louis S. Bauman, “Socialism, Communism, Fascism,” The King’s Business,
XXY¥I (July, 1935), 252.

6. N. J. Poysti, “What Is Bolshevism?” Evangel, February 1, 1936, p. 4.

7. Frederick Childe, “Christ's Answer to the Challenge of Communism and
Fascism,” Evangel, October 31, 1931, p. 1.

8 As cited in Evangel, February 22, 1930, pp. 4-5.

9. Paul B. Peterson, “A Desperate Situation in Russia,” Evangel, March 8,
1930, p. 9.

10, As cited in Evangel, January 21, 1933, p. 5.

11. Evangel, May 13, 1933, p. 4. See I Kings 22:12.
12. Prophecy, IL (December, 1931), 4-5.

13. Evangel, August 4, 1934, p. 7.

14. Evangel, August 1, 1936, p. 11.

15. John Robertson Macartney, “The Spread of Communism in Our Land,”
Moody Monthly, XXXV (May, 1935), 413.

16. Bar J. Steil, “The Trend Toward Armageddon,” Evangel, July 18, 1936,
pp. 2-3,

17. Evangel, July 2, 1932, p. $.

18. Prophecy, IV (November, 1932), 22.

19. Louis S. Bauman, “ ‘Prepare War!” The King's Business, XX¥ (Decem-
ber, 1934), p. 425.

20. Ibid, p. 424.

21. “Where Fundamentalists Stand,” The King’s Business, XXVIII (March,
1937), 90.

22. Evangel, October 2, 1937, p. 7.

23. Thomas M. Chalmers, “Russia and Armageddon,” Evangel, April 14,
1934, p. 1.

24. See also Charles S. Price, The Battle of Armageddon, p. 59.

25. Louis S. Bauman, God and Gog: or The Coming Meet Between Judah's
Lion and Russia's Bear, p. 38.

26. Louis S$. Bauman, “Russia and Armageddon,” The King's Business, XXIX
(September, 1938), 286.

27. Price, The Battle of Armageddon, p. 49.

28. Louis T. Tatbot, “Russia: Her Invasion of Palestine in the Last Days and
Her Fioal Destruction at the Return of Christ,” p. 18; “The Judgment of God
upon the Russian Confederacy,” p. 15. Both of these published sermons are
available at the Biola College Library, La Mirada, California,

29. Bauman, “Russia and Armageddon,” p. 287. Alva J. McClain, “The
Four Great Powers of the End-Time,” The King’s Business, XXIX (February,
1938), 100,

30. Bauman, God and Gog, p. 35.

31. Price, The Battle of Armageddon, p. 5.

32. Evangel, October 11, 1930, p. 4.

33. Childe, “Christ's Answer,” p. 8 (italics mine).
34. Moody Monthly, XXXII (March, 1932), 328.
