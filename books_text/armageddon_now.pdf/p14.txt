xiv Armageddon Now!

not cost him his academic reputation within dispensational
circles {he had none outside these circles), there seemed to be
no reason not to try to cash in again. Feeding frenzies must be
fed, after all. Apparently, publishing highly specific interpreta-
tions of Bible prophecy—interpretations that are disproved with-
in a year or two, and possibly six months—has something im-
portant to do with spreading the gospel. So, Dr. Walvoord
allowed Zondervan to republish this revised 1974 potboiler, and
it has sold (as of late August, 1991) 1,676,886 copies.2 But the
theological cost of this publishing coup was high: Walvoord’s
explicit abandonment of the “any moment Rapture” doctrine of
traditional dispensational eschatology. This is the man who had
taught systematic theology at Dallas Theological Seminary from
1952 until 1986, and who had served as president of the semi-
nary for most of this period.

To complete Walvoord’s move to dispensensationalism, his
publisher announced. his latest book, Major Bible Prophecies: 37
Crucial Prophecies That Affect You Today, in August, 1991. The
timing, as we shall see, was perfect... for non-dispensational
critics. The publisher, Zondervan, sent out a flyer in August
mentioning the book, assuring bookstore managers that it was
“being released this month.” I have a dated copy of this flyer
in my files. But as of the final week of August, a week after
the failed coup in the Soviet Union, its release date had been
postponed until September.

This delay is very interesting. First, if the book mentions
the coup, then we will know that an emergency rewrite took
Place—what we might call “severe damage control.” Only the
discovery of a major error—an error that seriously endangers a
publisher’s reputation—is sufficient to persuade a large publisher
to allow the author to revise a typeset book’s page proofs, if
such a revision causes the book to miss its scheduled date of
publication. If the coup is mentioned, it would then be most
interesting to examine the “inevitable fulfillments” that were cut
from the original manuscript. Second, if the book speaks as
though Russia were still a threat to Israel, then we will know

12. Press Release, “Kudos,” Zondervan Publishing House (August, 1991).
This figure may not include returned copies which ought to be quite high, given
what happened in the USSR in August.
