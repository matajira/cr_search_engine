58 Armageddon Now!

medievalists—but nearly every international event as God-ordained
and prophetically significant.

Although World War I had not produced the expected Arma-
geddon—not yet anyway—premillenarians were not disappointed.
The Balfour Declaration made the Jewish restoration seem immi-
nent, Allenby’s victory at Jerusalem augured the end of the times
of the Gentiles, and the Soviet giant seemed a foreboding threat;
the stage was set, once again, for the dramatic end.
