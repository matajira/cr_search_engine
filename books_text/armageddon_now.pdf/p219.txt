The Certainty of Survival 187

free. Since both political parties in America were anti-Russian,
there was probably little effect on voting patterns, with the possible
exception of the 1956 election when Adiai Stevenson openly
advocated a unilateral ban on nuclear testing. Even that is merely
guessing for premillenarians avoided public statements on such
social-political issues.
