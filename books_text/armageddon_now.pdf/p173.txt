The Century’s Greatest Event 41

States and Israel lists the reasons that most Americans supported
Israel rather than the Arabs: (1) nationalistic self-determination
gave the Jews a right to exist; (2) the Jews were underdogs due
to persecution; (3) Jewish propaganda overwhelmed Arab views;
(4) democracy was preferred over totalitarianism; (5) Ameri-
cans felt akin to the Jewish pioneering spirit; and (6) the drive
for achievement was admired.** It may be added that anti-Semites
preferred that Jews have their own state rather than immigrate
to the United States. And even apart from premillenarianism,
Safran points out that Americans gave priority in Palestine to
Jews because of their direct association with the Bible, rather
than to Arabs, about whom they knew very little.** An example
of this is the strong nonmillenarian stance expressed by Repre-
sentative Helen Gahagan Douglas: “Jews in Palestine today are
making the Bible’s prophecies come true.”8?

So the premillenarians did not have a monopoly on Christian
support for Israel; what was unique was their deterministic, non~
moral approach and their eagerness. This eagerness was not ex-
pressed in direct political action on Israel’s behalf, but rather
this “greatest event” was heralded in press and pulpit as a sure
sign of the end—to goad Christians into action and sinners into
the Kingdom. D. Malcomb Leith in his study of Christian support
for the state of Israel claims that although premillenarians took
no political action, their arguments provided a rationale for sup-
port of the humanitarian appeal that was characteristic of Chris-
tian action groups such as the American Palestine Committee
and the Christian Committee on Palestine.”

Various sidelights of the premillenarian preoccupation with
Israel continued to be manifest, such as the latter rain, the Al-
lenby legend, the development of Palestine, and the rebuilding
of the Temple. Louis T. Talbot recounted that for years the
early rains had been “insufficient” and the latter rains had been
of “little value,” but a few years ago the latter rains had been
increased so that the land in some places was able to produce
three crops a year."! The Evangel tied the origin of this “miracu-
lous change in climate” to the first meeting of the Zionist con-
gress in 1897.2
