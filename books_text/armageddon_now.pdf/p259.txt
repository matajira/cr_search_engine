Footnotes 227

58 Norman Cohn, Warrant for Genocide: The Myth of the Jewish World-
conspiracy and the “Protocols of the Elders of Zion,” p. 158.

59. D. M. Panton, “Latest Preparations for the Anti-Christ,” Evangel, Sep-
tember 4, 1920, pp. 6-7.

60. Cook, “The International Jew,” p. 1087.

61. The King's Business, XII (August, 1921), 795.

62. The King’s Business, XI (September, 1920), 858.

63. The King's Business, XU (June, 1921), 575.

64. The King’s Business, XVII (May, 1926), 262.

65, Charles J. Wachlte [Waeblite?], “The Red Terror," The King’s Business,
XIHI (September, 1922), 908-09.

66. O. R. Palmer, “Christ and Antichrist in Russia,” Moody Monthly,
XXIV (October, 1923), 59.

67. The King’s Business, XV (March, 1924), 133.

68. Alton B. Parker, “Recognition of Soviet Russia?” Moody Monthly,
XXIV (March, 1924), 347.

69. Evangel, February 21, 1925, pp. 6-7.

70. Evangel, November 29, 1924, p. 9.

71. Evangel, April 30, 1927, p. 3.

72. FP. E. Howitt, “Israel and Other Lands in Prophecy,” Evangel, March 10,
1928, pp. 2-3.

73. Frodsham, “Signs of the Approaching End,” p. 2.

74. Evangel, December 13, 1924, pp. 6-7.

75. The King’s Business, XIV (February, 1923), 136.

76. Evangel, July 26, 1925, pp. 6-7.

77. Stantey H. Frodsham, “The Revival of the Roman Empire,” Evangel,
December 11, 1926, pp. 4-5.

78. Evangel, April 30, 1927, p. 3.

79. Leonard Sale-Harrison, The Coming Great Northern Confederacy: or
The Future of Russia and Germany, pp. 20-23.

80. I. R. Wall, “Christ and Antichrist,” The King's Business, XX (November,
1929}, 525.

81. D, Grether, “Disarmament and the Signs of the Times,” Moody Monthly,
XXII (February, 1922), 806.

82. eames McAlister, “Startling Signs of the Times," Evangel, July 10, 1930,
pp. 1-3.

83. The King's Business, XE (October, 1920), 926-27.

84. Arthur W. Frodsham, “The Return of the Lord. The Signs of the
Times,” Evangel, February 18, 1922, p. 3.

85. Pankhurst, The Lord Cometh!, p. 94,

86. Evangel, February 7, 1925, p. 6.

87. W. Percy Hicks, “Proposed Revival of the Old Roman Empire,” Evangel,
March 20, 1926, p. 4.

88. Wall, “Christ and Antichrist,” pp. 524-25.

89. Oswald J, Smith, “Is Antichrist at Hand?” Evangel, October 30, 1926,
Ppp. 2-3,
