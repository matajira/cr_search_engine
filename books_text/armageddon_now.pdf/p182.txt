150 Armageddon Now!

meet destruction at the hands of God and His people. But after
the alliance little was said about Russia’s being defeated at Arma-
geddon (except in theoretical discussions).

During the war a very confusing theological debate did de-
velop about Armageddon. Was the battle of Armageddon of Reve~
lation 16:16 the same as the battle in the Valley of Jehoshaphat
of Joel 3:12? Was the Gog and Magog invasion of Revelation 20:8
the same as that in Ezekiel 38? The Evangel asserted the battle
in the Valley of Jehoshaphat and the battle of Armageddon were
the same, one of the first events to follow the rapture of the
church.®} Harry Rimmer, following Nathaniel West, taught that
Armageddon would not take place until after the millennium,
but that the battle of Hamon-Gog, or the battle of the Valley of
Fehoshaphat, was the first battle on the agenda. This did not make
his stand on Russia any different from the others. They all be-
lieved “the Russians are coming”; but instead of “Armageddon
Now!” it was “Jehoshaphat Now! Later, Louis T. Talbot's system
Placed the battle of Armageddon at the end of the Great Tribu-
lation which was to be the seven-year-period following Russia’s
defeat—that defeat being imminent in his expectation also.85 Arno
C. Gaebelein in Our Hope regarded Armageddon as the next
battle in God’s plan, but in his view Russia was not to be repre-
sented there. Her defeat was to come at the very beginning of
the millennium. Although he did not believe Russia’s invasion
was to be immediate, he did believe that current events were lead-
ing toward that invasion and that if Russia won the war, Stalin
would soon be a great universal dictator.*6 This was certainly
a confusing array of prophetic interpretations, nevertheless they
all maintained the continuity of premillenarian antipathies toward
Russia, particularly as the Cold War began to develop.

Within a year after the end of the war, Our Hope was lashing
out against the conduct of American foreign policy toward Russia.
Gaebelein’s periodical was the most vocal of the major premil-
lenarian magazines, probably because it was not an official organ
of any institution. Accordingly, he was concerned only with his sub-
scribers’ opinions rather than with those of a constituency that
might possibly be offended by his political stance, He was later
accused of “harping too much on Russia,” but he vowed to con-
