The Century’s Greatest Event 127

that the Jews were assured of a national home in Palestine.* At
the beginning of 1943 the Evangel quoted Agnes Scott Kent,
who had said, “There has been among Christian leaders a steadily
growing conviction that the many reverses suffered by the United
Nations in the war have had direct connection with our failure
to get in step with God in His clear purpose that Palestine is for
the Jews. Thus it is of highest moment that the abrogation of the
infelicitous White Paper is now under official consideration.”!7
When Winston Churchill reaffirmed in 1943 the policy of the
White Paper limiting immigration, the news was not welcomed.
As George T. B. Davis had written in The Sunday School Times:
“Immigration of the Jewish people to Palestine might be stopped
for a time, but...neither Great Britain nor any other govern-
ment could stop the return of the Jews to Palestine, any more
than they could stop the beating of the waves upon the seashore.”!9
When the policy was again reaffirmed in 1944, the Evangel fore-
casted: “God will reopen the doors in His own time, never fear,
His prophetic plan for Palestine will be fulfilled in spite of all.”
Such determinism was also put into practice:

Many friends of Israel, including earnest Christians, have
petitioned our government to intervene in the affairs of the
British government regarding the “White Paper,” on the
basis of which the British have restricted Jewish immigra-
tion to Palestine. The Voice, however, mentions two points
that should be kept in mind: (1) When it is God's time to
restore His people to the land, none will be able to prevent
them. (2) The restoration in which we might help would
demonstrate little kindness in view of the awful affliction
awaiting them there.2t

The implication was that nonactive support was the wise policy.
Here again, premillenarians shied away from political involve-
ment due to their anti-social gospel bias, Only after the 1944
election was over did the Evangel and The Sunday School Times
happily report that both presidential candidates were supporting
a national home for Jews.

During the war the Jews in Palestine had continued to develop
their military forces, both the official Hagana (defense) organi-
zation and the guerrilla forces, the Stern Gang and the Irgun
Zvei Leumi. After the war, the British found themselves unable
