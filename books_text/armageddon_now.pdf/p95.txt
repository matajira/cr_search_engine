‘The Twenties Roar Faintly—Bear, Beast, and Young Lions 63

of The King’s Business were admonished: “Christians should pray
for the restoration of the Jews to their land, for thus, as well
as by the saving of souls, we may help to hasten the day of the
Lord’s coming.” “All believers in the Divine Word of Promise
do well to pray that the Lord will bring another exodus to pass,
in gathering from the nations of the earth His people.”1*

There was some question, however, over the method that God
would use in accomplishing His purposes. Were the Zionists to be
the means or not? The King’s Business was skeptical of a scheme
by the Zionists to raise $125,000,000 for land purchases in Pal-
estine. The journal pointed out that, according to prophecy, it
would not be necessary for the Jews to buy the land. The impli-
cation was that the Zionists were not in keeping with the will of
God. “When God’s time comes, they will not have to buy the
Jand.”!5 Only a few months later, though, the same columnist of-
fered the Zionist movement as an example of God’s working through
gradual methods to accomplish His plans for Israel.1¢

One popularizer of interest in Israel, restoration, and prophecy
was Christabel Pankhurst, whose colorful background as a leader
of Great Britain’s militant suffragettes gave her an intriguing appeal
among the conservative premillenarians. She spoke throughout
the United States, including engagements at Moody Bible Institute
and the Bible Institute of Los Angeles. In her 1923 book, The
Lord Cometh!, she included the Zionist plan for the return of the
Jews, along with the rejection of the Turk from the land of Pal-
estine, as one of the “signs that are now heralding the end of
the Age.”17 The New York Times reported her 1925 lecture series
in New York’s Ascension Memorial Episcopal Church: “Concern-
ing the return of Christ, Miss Pankhurst said one thing was con-
clusive—the Jew was back in Palestine and his national home
being restored. This was ‘the supremely important factor in world
affairs today’ because it was ‘the decisive practical guarantee that
the Son of God is soon to appear.’ ”8 Not content with the usual
vague generalities, she placed her credibility on the line by pre-
dicting, “The year 1925 will see a big advance toward the final
crisis of the closing age. A last effort to save the world situ-
ation by human means will be made. A number of nations will
confederate and will accept the headship of a dictator, who will
be the Anti-christ of prophecy.”!° This report in the Northern
