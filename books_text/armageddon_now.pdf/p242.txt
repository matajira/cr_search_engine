210 Armageddon Now!

He believed that the identification of Tarshish as Spain was more
in keeping with what little evidence there was. A few years later,
Frank M. Boyd in the Evangel identified Tarshish as Tartessus
in southern Spain near Gibraltar, and reconciled the two views:
“In either case, we know who has possessed Gibraltar for many
years, and we are acquainted with the ‘lion’ symbol of our passage
as referring to Britain—The British lion.’ ”*7

The Late Great Planet Earth was very definite in its predic-
tions; the United States was to decline in power until it became
only an adjunct of the European power and would succumb to
the leadership of the Antichrist fighting against China in the great
battle of Armageddon.* Implicit in Hal Lindsey’s chronology was
the conclusion that the United States will have nothing to fear
from the Soviet Union at that time. Russia was to be destroyed
by God at the very beginning of the Armageddon struggle, where-
as the United States would be around later to participate in the
Armageddon main event.

1973 was the twenty-fifth anniversary of Israel’s independence,
and many magazines offered commemorative pieces which charac-
terized the premillenarian response to Israel (as well as to Russia
occasionally). The Evangel began the memorial year by editorial-
izing the old determinism.

As a race they are unique, because they have a divinely
determined destiny. God chose them for a purpose, and that
purpose must be fulfilled. Until then, the nation may be
despised, and it may even be decimated, but it cannot be
destroyed,00

Moody Monthly explained that Evangelicals identified with the
Israelis because of the Jews’ initiative, the common Old Testament
roots, sympathy for the persecuted, and their prophetic significance.
This self-analysis closely parallels that of Nadav Safran cited
earlier. United Evangelical Action, the publication of the Na-
tional Association of Evangelicals, took the occasion to castigate
the liberals and particularly the World Council of Churches for
their unfriendly attitude toward Israel, citing Rabbi Marc Tan-
enbaum’s censure of the liberal Protestant community for failing
to support their Jewish neighbors on nearly every priority Jewish
concern except for the problem of Soviet Jewry, The author,
