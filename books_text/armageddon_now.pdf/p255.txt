Footnotes 223

22. The Weekly Evangel, February 3, 1917, p. M1.

23. Scott, At Hand, p. 77.

24. W. E. Blackstone, “The Times of the Gentiles,” The Weekly Evangel,
May 13, 1916, pp. 6-9.

25. The Weekly Evangel, May 19, 1917, p. 7.

26. Hawthorne Quinn Mills, “American Zionism and Foreign Policy,” p. 28.
27. Stephen Wise, Challenging Years: The Autobiography of Stephen Wise,
Bp. 186-87,

28, Ibid,, p. 197.

29. The Weekly Evangel, January 5, 1918, p. 5.

30. W. Fuller Gooch, “The Termination of This Age.” The Weekly Evangel,
March 23, 1918, p. 12.

31. Reuben A. Torrey, What War Teaches: The Greatest Lessons of the
Year 1917, p. 4.

32. Our Hope, XXIV (1917-18), 438.

33. W. W. Fereday, “After the Great War,” Our Hope, XXVI (1919-20), 34.
34. Hertzel Fishman, American Protestantism and a Jewish State, p. 28,

35. The Weekly Evangel, December 22, 1917, p. 3.

36. A, B, Simpson, Editorial, The Alliance Weekly, XLIX (December 22,
1917), 177,

37. A, B. Simpson, “The Fall of Jerusalem in the Light of Prophecy,” The
Alliance Weekly, XLIX (February 16, 1918), 306-7.

38. F. L. Langston, “The Chosen People and the Chosen Land,” The Weekly
Evangel, March 23, 1918, pp. 2-3.

39. A.B. Thompson, “The Capture of Jerusalem,” Light on Prophecy: A
Coordinated, Constructive Teaching, Being the Proceedings and Addresses at
the Philadelphia Prophetic Conference, May 28-30, 1918, p. 160.

40. Gaebelein, Christ and Glory, p. 157.

41. The King’s Business, X (October, 1919), 927, See also The Christian
Workers Magazine, XIX (1918-19), 807.

42. The King’s Business, X (August, 1919), 786; Dov Ashbel, One Hundred
and Seventeen Years (1845-1962) of Rainfall Observations, p. 118.

43. The Weekly Evangel, February 26, 1946, p. 6.

44. Joseph W. Kemp, “The Jewish Tragedy," The Christian Workers Maga-
zine, XVIL (1916-17), 180.

45. Our Hope, XXHIL (1916-17), 751.

46. Our Hope, XXV (1918-19), 140.

47. FB. A. Wight, “The Restoration of the Jews,” The Alliance Weekly, XLIX
(January 12, 1918}, 230-31.

48. The King’s Business, X (October, 1919), 927.
49. Our Hope, XXVI (1919-20), 506.

50. Scott, At Hand, p. 77.

Si. Ibid., p. 70.

52. Our Hope, XXIII (1916-17), 44.

53. Our Hope, XXII (1916-17), 110-13,

54. Our Hope, XXIII (1916-17), 185, 559.

 
