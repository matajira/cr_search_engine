The Certainty of Survival 185

power that will come into being and play such an im-
portant part in the prophetic Scriptures—the revived Roman
Empire3¢
Every development in Europe or the United Nations that could
somehow be credibly interpreted as fulfillment of these fringe-
area prophecies served to reinforce premillenarian belief in the
great drama of Russia’s impending invasion of Israel.

Some premillenarians carried on in the 50s the attempt to
find a specific reference to the United States in the prophesied
program of end-time events. The United States as one of Britain’s
“young lions” continued as a favorite theme. Louis T. Talbot
included the United States in prophecies that he ascribed to
England. England was to be one of the ten toes in Nebuchadnez-
zar’s dream:

There are, of course, two great toes and eight lesser ones.

So, also, in that great ten-toed kingdom, there will be ten

nations of different powers and strength. And the two great

nations will doubtless be England and Italy. The Antichrist

will be head over the government in Italy, ruling from

Rome,86
Perhaps Charles E. Pont was a little self-conscious as he asked
his readers in The World’s Collision: “Does this sound like a lot
of symbol stretching? We do not think so.” Pont believed that
World War III was inevitable and was strongly opposed to any
nuclear disarmament, fearing it would be to the disadvantage of
the United States.&

That the old Stars and Stripes would float aloft in the mil-
lennial kingdom had been Louis S, Bauman’s belief in 1942, but
in 1950 he was predicting the submergence of the United States
into the United Nations under the leadership of the Antichrist:
“Already ‘Old Glory’ is battling to maintain its pre-eminence above
the flag; in that day of international control, the old emblem of
human freedom will have to fold its wings and perish with Liberty!
Nothing less than this will satisfy the prophetic picture.”88 Much
more optimistic was William L. Hull in fsrael—-Key to Prophecy.
He did not believe that Russia and America would fight at all;
the United States had shown in the Suez Crisis that Russia could
bluff and get what she wanted without fighting.®° Another totally
different view was that of retired Lieutenant General William K.
