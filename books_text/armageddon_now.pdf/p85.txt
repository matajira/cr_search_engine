Armageddon—Almost 53

Babylonian—we are, of course, a witness to our own departure
and failure from the original grant of government conferred upon
the Gentiles.”°8 Such views were, however, the exception rather
than the rule. Dr. Case made further accusations implying traitor-
ous activity on the premillenarians’ part: “A premillennialist might
well want Germany to win” because that would be a step toward
a worse world—a prospect which the pessimistic premillennialist
expected immediately prior to the catastrophic end of the world
and his hoped-for new kingdom. Case refused to accept the pre-
millenarians’ own analysis that they did not oppose the war, since
they viewed the war as happening by “divine permission.”®® Such
invective did not undermine the work of the premillenarians at
all—even their critics came to their defense in this particular
circumstance,” They probably relished the attention directed to
their cause.

The main thrust of premillenarian anti-communism was op-
position to communism’s endemic atheism—an identification that
does not begin with Karl Marx, but can be traced back at least
to the French Revolution. This aspect of their opposition, how-
ever, was one which they shared with most Christians—certainly
all conservative Christians. The form of the opposition was quo-
tation of statistics, reporting of incidents, or direct statements
from the mouths of real, live Communists. For example, The
Christian Workers Magazine cited Literary Digest’s quotations
of a newspaper report of the Senate Judiciary Committee's findings
on Bolshevism:

It has confiscated all church property, real and personal... .
It has suppressed Sunday-schools and has expressly for-

bidden the teaching of all religious doctrine in public, either

in schools or in educational institutions of any kind.72
Churches, Sunday schools, and Bible institutes were the main
harvest fields of all premillenarians—the threat of such suppression
was terrifying. Tertullian’s words, “The blood of the martyrs is
the seed of the church,” was a cliché of current circulation, but
none advocated that now was a time to start from scratch and
replant. The most devastating type of criticism of communism by
the premillenarians took the form of quoting remarks such as
the following, which was ascribed to the Russian revolutionary,
Michael Bakunin:
