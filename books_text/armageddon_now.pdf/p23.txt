Publisher's Preface xxiii

2, 1990, with the invasion of Kuwait by Iraq, is simply the lat-
est example of this phenomenon, as Dr. Wilson points out in
his Foreword. The Christian bookstores of America were filled
with books on prophecy, including the reprint of John Walvoord’s.
Decades of false predictions about the imminent return of Christ
to “rapture” His saints have made no visible impression on the
vast majority of the victims. The victims do not learn from ex-
perience. They keep getting misled, both by charlatans and by
well-meaning promoters who honestly believe that they have
uncovered some neglected key to Bible prophecy. In the case of
John Walvoord, he merely picked up a rusting prophetic key
that he had discarded years earlier, shined it up, and sent it to
his publisher. It sold like hotcakes.

But the Church is still here, isn’t it? And Soviet Commu-
nism isn’t.

There is a heavy price to be paid for all of this, and the
fading reputation of the American evangelical church is part of
that price. Dispensational fundamentalists are increasingly re-
garded by the humanist media as “prophecy junkies”—not much
different psychologically from those supermarket tabloid news-
Paper readers who try to make sense of the garbled writings and
ever-incorrect interpretations of the followers of Nostradamus,
whose name is also selling lots of books these days. When sec-
ular newspaper reporters start calling Christian leaders to ex-
pound on Bible prophecy and its relationship to the headlines,
and then call occultists and astrologers for confirmation, the
Church of Jesus Christ is in bad shape. Read Armageddon
Now! to find out just what bad shape this is, and has been
for over seven decades,

John Walvoord’s “ticking clock” book and others just like
it in 1991 were the equivalent of General Schwarzkopf’s satura-
tion bombing strategy: they flattened orthodox dispensationalism.
Almost immediately after their publication, General Schwarzkopf’s
strategy in Iraq buried the very short-lived “Babylon Literally
Rebuilt” dispensationalism. Then, six months later, the failed
Soviet coup buried “bear from the North” dispensationalism.
What is left? Not much. Dispensationalists must now begin to
rebuild the ruins. This task should keep them occupied for a
long time—possibly for a millennium.
