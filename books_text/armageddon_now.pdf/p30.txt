XX Armageddon Now!

anybody believes that global war is imminent” (Charisma 4/84).

Various authors continued to reassure the world that the
Russians were definitely the end-time villains. John F. Walvoord,
president of Dallas Theological Seminary (the alma mater of
Hal Lindsey) published in the (now defunct) Fundamentalist
Journal (1/84) adaptations of his 1967 book The Nations in
Prophecy, in which he offered “conclusive” evidence that
Russia is the culprit in Ezekiel’s prophecy in chapters 38 and
39. Pat Robertson, host of television’s 700 Club and aspiring
U.S. Presidential candidate, in a Christian Life (11/84) inter-
view said, “The immediate event of greatest significance in the
Middle East is the obvious growing influence of the Soviet
Union, which one day will lead the invasion of Israel... ac-
cording to Ezekiel 38.”

The 1980 election brought to the office of Commander in
Chief of the U.S. nuclear arms Ronald Reagan, reportedly a
believer in the inevitability of an imminent Armageddon. A
1984 audiotape produced by Joe Cuomo entitled Ronald Reagan
and the Prophecy of Armageddon testified ‘to the President's
understanding of the world in which he carried the nuclear but-
ton. PTL televangelist Jim Bakker cited Reagan as saying in
198{, “We may be the generation that sees Armageddon.” In
1983 the President expressed his belief that “It seems that all
signs are pointing to Armageddon.” Various associates believed
that his theological system had a great impact on his policies.
He had read both Hal Lindsey's book which he discussed with
staff members and also Pat Robertson’s The Secret Kingdom.
He expressed the belief that the Russian’s might very well be
the “great power of the North” described in Ezekiel, and in
his public statements referred to Russia as “The Evil Empire.”

One of the most frightening aspects of all of this is the
phenomenon I have identified in chapter 9 as “The Certainty
of Survival.” The theory of the nuclear defense system is based
upon the fear of mutual destruction of anyone who might be
tempted to initiate a nuclear holocaust, The premillenarian men-
tality, however, is not deterred by the fear of nuclear war.
Television pastor Jerry Falwell says he fears neither Armaged-
don nor nuclear war. In the first place, the pre-tribulation pre-
millenarians believe it most likely that the Church will be safely
