From Sinful Alliance to Christian Realism 149

smile meekly and let him do it; he will anyway. By and by
he will decide to run all of Europe and then the world. Let’s
hope for at least a decade of peace before he is ready. When
it comes, the present conflict will resemble a Methodist love
feast in comparison.27

Then in September, 1944, there was speculation that Russia
was about to open a consulate in Jerusalem. The Evangel took
the opportunity to recollect that Russia’s claim to be a protector
of Palestine had led to the Crimean War and declared that this
renewed interest was significant? A couple months later former
Ambassador to Russia, William C. Bullitt, predicted in Life maga-
zine a war between the United States and Russia within fifteen
years.® Also in 1944, a diatribe against Russia by Dan Gilbert
appeared, entitled The Red Yerror and Bible Prophecy. He be-
lieved that Russia’s battle against Germany was just a temporary
diversion from Russia’s true goal in Palestine. He declared that
the war against Palestine had already begun with Russian perse-
cution of the Jews. The objective was to desecrate the Holy Land
and turn it into an “antireligious museum.” He claimed that
already 22,000,000 Jewish and Christian believers had been killed
by Russian Communists in their battle against God. This then
was the background that premillenarians brought to the situation
that developed after World War II. It was not necessary for
Russia to act belligerently; her mere existence was sufficient provo-
cation to the premillenarian mind which expected a postwar in-
vasion of Palestine leading to Armageddon.

Armageddon had been a particular topic of interest at the
beginning of the war. The popular author, Roger Babson, spoke
of Russia as the controlling factor in the world crisis and of
Stalin as the world’s greatest statesman. This elicited the Evangel’s
comment that the reign of Antichrist and the battle of Arma-
geddon were “swiftly approaching.”' As the war spread to the
Near East, and Britain had a million troops set to defend the
Suez Canal, it was said, “With so much military activity in and
around Palestine it seems that the nations are hastening on to
their last great conflict at Armageddon.”5? Before the Russian
alliance with Britain and the United States, the Evangel was saying
that Russia and her allies would march upon Palestine and there
