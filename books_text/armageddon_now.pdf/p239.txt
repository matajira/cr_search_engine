Jerusalem! What Now? 207

for Soviet Jews. In his article, “Let My People Go,” he pleaded
for Christians to persuade their congressmen to use economic
pressure on Russia to wring concessions for the Russian Jews.”
This could possibly be interpreted as a symptom of a growing
“neo-evangelicalism” which demonstrated some humanitarian con-
cern and social action in spite of the social gospel onus, but more
likely it was only one small cry in the wilderness, not an omen
of a popular ground swell of premillenarian social involvement.

As the fear of an imminent Russian invasion declined in the
United States, mention of the battle of Armageddon was also greatly
reduced. Another reason for the lessened interest in Armageddon
was that the theology that had evolved did not necessarily identify
the Russian invasion as that particular battle. Armageddon was
still impending, however, and was not forgotten. As the Vietnam
War developed, the question arose whether this new war would
lead to Armageddon. An article in The King’s Business assured
readers that it would not, but explained the prophetic significance
of the current crisis:

Well, for one thing, we are feeling intensely the threat of
Red China, We have seen the development of the great north-
ern power Russia. Egypt is presently a developed power
engulfed in an Arab alliance. We are witnessing the devel-
opment of a great western confederacy in the form of a
European Common Market. And now we are faced with the
recognition of a vast Eastern power that is not only com-
munistic but also atheistic, Is not the stage set?"8

The Late Great Planet Earth had an extensive discussion of
the battle of Armageddon. Lindsey said that the current build-up
of the Russian navy in the Mediterranean Sea was another sig-
nificant sign of the possible nearness of Armageddon.” The Rus-
sians, however, were to be defeated by supernatural power at the
very beginning of the actual battle of Armageddon which was
to be fought by the combined forces of western civilization under
the leadership of the Antichrist against the vast hordes of the
Orient under the leadership of the Red Chinese.8° The eastern
powers alone would wipe out one-third of the earth’s population,
but then Christ would appear to save mankind from self-destruction
and would set up an earthly millennial kingdom.*!
