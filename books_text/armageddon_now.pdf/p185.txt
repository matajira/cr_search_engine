From Sinful Alliance to Christian Realism 153

cy,” by Merril T. MacPherson, pastor of the Church of the Open
Door in Philadelphia and former president of the Independent
Fundamental Churches of America. He categorically stated that
the next world war would involve Palestine, and cited former
Governor of Pennsylvania, George Earle, who had warned that
unless America struck the first blow before Russia had perfected
her atomic bomb, very likely within five years there would not
be more than 10 per cent of the American people left. MacPherson
believed that only the threat of massive atomic retaliation had
contained Russia’s aggressive plan so far. He concluded by asking:

When will Russia strike? This we do not know, but we be-

lieve that it will be as soon as she judges herself to be

capable of making some sneak atomic attacks upon our

own great nation, One thing certain, according to God's

Word, is that one day—perhaps in the near future—Russia

will strike.5*
There is no doubt that anti-Russianism was rampant among all
Americans in late 1947. In this same issue, Christian Life editors
quoted figures from a September 26 Gallup Poll which showed
that 53 per cent of all American adults believed that World War
TIL would break out within ten years. Christian Life had made
its own poll of its readers which showed a comparable figure of
49 per cent. Though no figure is available for the general public,
26 per cent of the Christians expected war in five years, while
25 per cent of the readers did not expect war with Russia at
all in the foreseeable future. One of the latter group responded:
“Who thought up this hate-mongering?”™

Probably the worst example of “hate-mongering” appeared

as an editorial in The Southern Presbyterian Journal by L. Nelson
Bell, father-in-law of evangelist Billy Graham and eventually the
first executive editor of the periodical Christianity Today. The
editorial, entitled “Christian Realism,” began with the premise
that communism would continue until it was stopped by force,
and then concluded:

The solution—tet the American Government issue notice to
Russia that she is to start the immediate evacuation of
all territories into which she has expanded since 1942.
Further that at the end of one week one atomic bomb will
be dropped in a sparsely settled area of European Russia
