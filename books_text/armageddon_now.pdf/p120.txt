88 Armageddon Now!

Jeremiah, ‘Let tears run down like a river day and night’?” The
response was a rhetorical question, reflecting the silver lining
which the premillenarians discovered in even the darkest clouds:
“Are we not justified in believing that something remarkable will
happen soon concerning the Jew, the Temple area site, and that
ancient Wailing (Western) Wall?” Perhaps an even more ap-
propriate figure than the silver lining would be the one which
the premillenarians themselves frequently used, “The darkest part
of the night is just before the dawn.”

The various periodicals continued to lament the plight of
the Jews in their conflict with the Arabs and in their dealings with
the British. The Evangel reported that there were strong protests
egainst the anti-Jewish attitude of the Royal Commission that
had investigated the 1929 riots, and then approvingly cited the
policy recommendations of Hamilton Fish of the House Committee
on Foreign Relations: “The re-establishment of a Jewish National
Home is a moral obligation assured by the governments of the
world with Great Britain as the executor of that obligation. She
must administer that obligation in the sense accepted by other
people or she must confess her inability to do so, so that proper
action may be taken.”* Aaron Judah Kligerman, writing in the
Moody Monthly, admitted that the historical position of the church
for centuries had been that the Jews had lost their chance and
that there was to be no restoration of a national Israel, but
only a spiritual Israel—the church. He proceeded to establish the
“true” teaching of Scripture—that the restoration was yet in the
future. Kligerman then asserted that the restoration had already
begun with the Balfour Declaration:

There may be much politics connected with that declaration.
England may not be sincere. The Arab may be stirred again
and much more blood may yet be shed, Be as these may,
I believe that “he that scattered Israel will gather him, and
keep him, as a shepherd doth his flock” (Jer. 31:10)7

The Christian speculated that the “awful blow” of the Jewish
setbacks might cause them to turn to God.* The extreme literalism
of the Evangel interpreted the attempt to limit immigration as
the fulfillment of Isaiah 27:12, “Ye shall be gathered one by one,
© ye children of Israel.”? The King’s Business expressed a very
