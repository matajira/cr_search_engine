212 Armageddon Now!

In October, 1973, on the holiest of Hebrew holidays, Yom
Kippur, Israel was suddenly attacked by Syria and Egypt. The
Israelis were nearly defeated in the early days of the war, and most
likely would have been if Syria had not failed to follow up her
initial thrust. But after a counterinvasion of Egypt by the Israeli
army, a cease-fire was arranged. Both sides were recipients of
massive military aid—the United States resupplying Israel after
some hesitation, and the Soviet Union energetically supporting
the Arabs. The result was a temporary Arab embargo on oil sup-
plied to many western nations, creating a distressing energy
shortage,

Some premillenarians slid into the traditional deterministic
response, the Evangel observing confidently that no one had solved
the problem yet, but God would eventually, and Christianity Today
reporting John F. Walvoord’s assertion that Israel would never
be destroyed.15 Others, however, feared for the very existence of
Israel.

A few liberal Protestant voices continued to question the
very right of the state of Israel to exist. One of them said, “It
is quite conceivable that Israel may have to die for world peace.’
But although officers of national organizations were usually offi-
cially quiet, there was more support for Israel at the local level
than there had been in 1967.1% Judith Banki writing for the
American Jewish Committee has explained the continued official
antipathy of liberal Protestant bodies:

It may derive from Christian theological presuppositions
about the mission of Judaism—not so much the “old” Chris-
tian anti-Semitism, which held that the Jews must remain de-
spised and dispersed throughout the earth because of their
murder and rejection of the Messiah, but the “new” theo-
logical anti-Semitism, which holds, in effect, that Jews
should not be permitted the trappings of normal nationhood
to which other peoples are entitled because their prophetic
tradition calls them to a more universal mission.

Polls of general opinion showed the same pattern of response as
in the June War; 47 per cent were sympathetic to Israel, 6 per cent
to the Arabs, and the rest were uncommitted. A Commentary writer
observed, “In the gross, Americans sympathize with Israel, but
