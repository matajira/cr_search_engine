Jerusalem! What Now? 193

He asked, “How can a Christian applaud the murder of a brother
Christian by Zionist Jews?” and then wryly commented, “An equal
or even greater horror is that so many Christians applaud crimes
against the Arab Muslim.”!? His article reflected the view of
many missionaries and scholars who had been in direct contact
with Arab Christian communities, but their pleas made little
impact on the general premillenarian mind-set.

Israel has made a conscious attempt to exploit the reservoir
of good will among the premillenarians in addition to courting
the favor of all Christian communities. Evangelist Oral Roberts
in his 1963 book, The Drama of the End-Time, recalls that when
he was a guest of the Israeli government, several government of-
ficials sat with him all day long because they wanted to assure
him of his welcome, even though Prime Minister David Ben-Gurion
had been unable to keep an earlier appointment due to an emer-
gency. Roberts’ appointment was postponed, whereas others were
simply canceled. The Israelis’ time was not spent in vain; the
television evangelist returned to vibrantly spread the message:

When I departed from Israel, I did not lose the spell that

had fallen over me. Even now I feel the surge, the rise, the

swell, the thrill of deep emotion. There is something going

on in Israel, It is of eternal consequence, and the spiritual

significance of that something leaps in my biood like a flame.

God's ancient people are carving out an empire. They are

literally creating it with their own hands, That's what the

Bible told us they would do. The meaning of it in terms

of a coming great world revival and the Second Coming

of Jesus has thrilled me to the very fiber and core of my

being.

Evangelist Billy Graham’s associate, Cliff Barrows, recounts
@ similar extraordinary welcome at a special showing of the Billy
Graham film His Land for Prime Minister Golda Meir® This
film has been criticized for portraying an unfavorable image of
the Arabs while profiling the Jews in glowing terms.2? But Rabbi
Tanenbaum called the film “perhaps the most beautiful, sympa-
thetic portrayal of the people of Israel restored to their ancestral
land that had been made by any Christian since the creation
of the Jewish State,”2t
