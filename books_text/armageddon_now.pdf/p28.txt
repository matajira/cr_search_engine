xxvill Armageddon Now!

prophetic speculation as a result of the invasion of Kuwait, as
the enormous sales of John Walvoord’s reprinted book on the
Middle East indicated. As we approach a.p. 2000 or the third
millennium, we can expect more. (4.D. 2000 is the last year of
the 20th century, the third millennium not beginning until 2001.)
As I predicted in the 1977 “Introduction,” we are beginning to
see the first fruits of these pregnant points of passage, and
there certainly has been no lack of stimulating fertilization from
events in the Middle East.

Change

Although the on-going themes remain the same, in retrospect
there are significant changes that I see in the last fifteen years.
One of those changes is the growing significance of the media:
television, videotapes, and audiotapes. The historian of ideas
has always had the speculative problem of assessing the rela-
tionship between the quantity of output on the written page
and the extent of its impact on the constituent public opinion.
However, the problem of assessing the dissemination and
growth of religious ideas appears even greater now with the
growth of television and the burgeoning of religious program-
ming. The difficulty of documenting the impact of the radio
revivalists has been with us for a generation, but for the per-
son trained for research in libraries and raised on religious
periodicals, it is a trying transition into the world of televange-
lists and tape ministries. There is not the kind of information
that is readily accessible in academic libraries. How does one
go about verifying what one of his friends reports seeing on
the late night television? How many people saw it? How many
of them believed it?

Religious television did, however, suffer a dramatic decline
in the late 1980's. Christianity Today (2/3/89) reported that the
number of households watching religious programming declined
from 7,306,000 in 1984 to only 4,262,000 in 1988. That is still
a sizable audience, particularly for purposes of our discussion,
considering that Jim Bakker, Jimmy Swaggart, Jerry Falwell,
Pat Robertson, Billy Graham, and Lester Sumrall are all pre-
millenarians, Graham and Sumrall have not been victims of the
fall-off in viewer support. Except for Jim Bakker, now in jail,
these all remain the leading opinion-makers in the popular mind
