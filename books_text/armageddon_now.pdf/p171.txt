The Centory’s Greatest Event 139

Arabs’ jealousy of Jewish economic success. The consistent refrain
was that the land was promised to Isaac, not Ishmael.” The
premillenarian attitude was epitomized in the Evangel’s analysis of
the situation following the first round of the war for independence.

Even though there may be a temporary truce between the
Moslems and Israel because of the pressure of the United
Nations, this truce will in no wise change the attitude of
“perpetual hatred” (Ezek. 35:5) that the Moslem nations
have toward the people of Israel... .

We are bidden, “Pray for the peace of Jerusalem: they
shall prosper that love thee.” Psalm 122:6, But the Holy
Spirit prays: “Let them all be confounded and turned back
that hate Zion.” Psalm 129:578

A revealing contrast exists between the revulsion at the tragic
news that the Arabs had destroyed all twenty-seven synagogues in
Jerusalem during the 1948 war and the cavalier nonchalance
which greeted the prospect of the Jews’ destruction of the Mosque
of Omar: “It would be no surprise to prophetic students if this
were to take place,”

When the Security Council voted to admit Israel to the United
Nations, Egypt was the only member on the council to vote no.
Egypt’s vote was called a reflection of the deep-rooted hostility
which Zechariah 14:16-19 foresees occurring in the millennium.®®
This was sheer prejudice for the prophecy merely threatens
drought on any nation which does not go up to Jerusalem to
worship. The only reason Egypt is singled out in Zechariah is
that it does not rain there. So she is threatened with plague
instead of drought if she does not go up to Jerusalem to worship.
The prophecy does not imply that Egypt’s animosity toward the
Jew is greater than that of any other nation. Such was the pre-
millenarian disposition to find little, if any, good in the Arabs.

As @ condition for membership in the United Nations, Israel
agreed to abide by United Nations resolutions and to interna-
tionalize the city of Jerusalem.8t Resolution 194 (IIL) of Decem-
ber 11, 1948, called for allowing the return of all refugees who
wished to live at peace. But Israel has permitted neither inter-
nationalization nor repatriation. Unlike the liberal Protestant press
which continually charged the Israelis with responsibility for cre-
ating the refugee problem, the premillenarians evidently had little
