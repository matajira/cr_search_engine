124 Armageddon Now!

arranged plan of history. Various explanations, however, were
offered in attempts to second-guess God’s ultimate purpose in
this new rehearsal for Armageddon. After Pearl Harbor, with
America involved in the war, the Evangel advised its readers,
“If you would Jearn God's purpose in the present World War,
watch Palestine,” and suggested that the war might lead to the
final conflict of Armageddon.’ As the end of the war approached,
the Evangel cited the Prophetic News of London that just as the
purpose of World War I had been the conquest of Palestine and
the promise of the Balfour Declaration, the chief outcome of
World War IL might be the “emergence into prominence in poli-
tics and war of ‘Gog.’” The Evangel commented, “It is amazing
how Russia has been transformed, in a few short years, from a
backward nation into one of the first rank. There is no doubt
that Russia could become a formidable threat to Palestine and
any combination of states which might array themselves on
Palestine’s side.”? In 1950, in an article entitled “Israel, God’s
Last-Day Miracle,” the author focused on the Jews as the central
purpose of the war: “Just as it had taken World War I to pre-
pare the promised land for the Jewish people, it took World
War II to prepare the people for the promised land.” The Nazi
persecution had been the impetus to drive them back to Palestine.
On the other hand, a Moody Monthly article analyzed the war
as a judgment on the nations themselves for their “sordid and in-
human” treatment of the Jews, including Great Britain in par-
ticular: “None can ever deny the debacle of dishonor, the British
White Paper of May, 1939, in which a great power sought to
appease the Nazis, and yet, four months later, found herself in-
volved in the deadliest war of her history.”* In spite of such a
variety of ideas, the premillennialists nevertheless agreed that
whatever the purpose, it was all predetermined by God.

When it came to the continuing question of the restoration,
however, the proper sequence of predetermined events remained
a matter of debate. The question was whether the restoration
was to take place before or after the return of Christ—or whether
it was to take place before or after the Jews’ acceptance of Jesus
as Messiah. The issue was not whether the Jews would be re-
stored to Palestine (that was a foregone conclusion), but whether
