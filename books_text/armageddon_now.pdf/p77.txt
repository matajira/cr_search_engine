Armageddon—Almost 45

“Surely with the British flag flying today over Jerusalem, we are
touching the time when Jerusalem shall be no longer trodden down,
but shall be the home of the free.”®8

Another response to the fall of Jerusalem was to call con-
ferences. Over three thousand gathered for the meetings of the
Philadelphia Prophetic Conference, May 28-30, 1918. A high-
light of the conference was a lecture, “The Capture of Jerusalem,”
by A. E. Thompson, whe had been the pastor of the American
Church in Jerusalem at the beginning of the war. He pointed out
that this event had been expected by prophetic students for years:
“Even before Great Britain took possession of Egypt, there were
keen-sighted seers who foresaw the day when God would use
the Anglo-Saxon peoples to restore Jerusalem.”*® Another con-
ference convened in New York in Carnegie Hall after the armistice
at the call of Arno C. Gaebelein, editor of Our Hope. The fall of
Jerusalem was interpreted there also: “To us true Christian be-
lievers it is the sign that the times of the Gentiles are rapidly
nearing their close.’ Still another convention, the Jewish Pro-
phetic Conference, was held in Chicago the following year. Two
thousand people expressed their premillenarian concern for the
Jews by passing resolutions:

Be it resolved that we pledge ourselves daily to pray... that
the day may speedily come when the dark shadows of 2000
years may flee away, and the long promised day of right-
eousness and peace may come for Israel's race... .

AND FURTHERMORE be it resolved that we express in
every other possible way our sympathy to the Jewish people
in the present crisis, doing whatsoever we can in their behalf,
and that a copy of these resolutions be sent to the President
of the United States.*1

Not only did the political and military machinations of the
war seem to fulfill premillenarian predictions, but writers also
appealed to physical phenomena to reinforce their system of in-
terpretation. The prophets Joel (2:23) and Zechariah (10:1)
had spoken of a “latter rain.” In a note at Zechariah 10:1, Sco-
field in his reference Bible had interpreted this latter rain as both
physical and spiritual—but yet in the future. The King’s Business,
however, quoted statistics showing that the average annual rain-
fall in Palestine had already gradually increased. In the 1860s
