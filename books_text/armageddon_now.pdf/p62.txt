30 Armageddon Now!

prior to the second advent. Generally, the futurists’ eschatology
paralleled the teaching of John Nelson Darby, the founder of
the Brethren movement, but scholars have been unable as yet
to show for all futurists a direct relationship to that movement.

Reflecting this new emphasis was Maranatha: Or, The Lord
Cometh, a work by James H. Brookes, pastor of the Walnut
Street Presbyterian Church in St. Louis. He was a friend and
teacher of C. I. Scofield and had a strong influence on the pre-
millennial movement. His chapter on the “Return of the Jews”
was basically an interpretation of Danicl’s prophecy. As a matter
of fact, he called this prophecy a “remarkable revelation... con-
cerning the destiny of the Jews.”75 Brookes pleaded for a literal
interpretation of Scripture and pointed out that the Promised
Land of Israel was an unconditional promise."* The history of
Israel, he said, corresponded to the seventy weeks in the vision
of Daniel 9. He argued, however, that the term weeks was a
mistranslation, which should have been rendered “heptads” or
“sevens”—in this case, “sevens” of years.77? Sixty-nine of these
seventy “weeks” of years covered the period from 453 B.c. to
AD. 29, that is, from the command to rebuild Jerusalem to the
rejection of Jesus as the Messiah and His crucifixion.”® The last
“week” of seven years is a future period of tribulation for the
Jewish people just prior to the second advent of Christ.”® This
is basically the same analysis that is noted in The Scofield Reference
Bible at Daniel 9:24. In contrast to Jacob J. Janeway in Hope
for the Jews, Brookes contended that the Jews would be par-
tially restored to Palestine prior to their conversion, but only after
they have allied themselves with the Antichrist.8° This point was
to take many twists along the winding premillennial path. Brookes
concluded the chapter with a characteristically premillenarian
eulogy of God’s chosen people:

Found among all nations, and yet not mingling with any,
they are still a distinct and peculiar people, surviving the
sweeping revolutions of the past, and reserved for a sublimer
destiny than the genius of the most ambitious statesman has
sought to attain for his country. ...

... Hebrew history...is a dark enigma indeed unless
studied in the light of God’s prophetic word..., Already
they are largely controlling the course of current events by
their splendid intellectual endowments, for not only is it
