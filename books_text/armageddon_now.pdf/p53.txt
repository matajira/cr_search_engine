Before Balfour and Bolsheviks 21

Is then England the great maritime power, to which the high
office of converting and restoring a large part of his ancient
people is reserved by the Almighty? ... Zngland may, or may
not... , This however I may safely say, that, the more true
piety increases among us, the more likely will it be that
England is the great maritime power in question.28

Russia, England's ally in the Napoleonic wars, was assigned
a role in this end-time scenario; she was the “king of the north”
of Daniel 11, whom Faber depicted as benevolent. He observed
that Russia had risen “in the inconceivably short space of a little
more than a century from barbarous insignificance to immense
power and influence,”®° and that “it will be a tremendous instru-
ment in the hand of God.” In commenting on Revelation
16:17-21, he said: “The necessary conclusion seems to be, if I
be right in supposing the northern king to be Russia, that the
hail-storm of the seventh veil means some dreadful invasion of
the papal Roman Empire and her northern allies during the time
that Antichrist is engaged in prosecuting his conquests in Palestine
and Egypt.”! At the battle of Armageddon the “tremendous
instrument” of Russia would prove ineffective; but when Anti-
christ attacked “the maritime power,” the Word of God would
oppose and defeat him.?? The identity of Gog and Magog in
Ezekiel 38 Faber found inscrutable. He readily dismissed the
Turks as a possibility,** and finally discharged the Russians too:
“The Russians and Muscovites seem to be colonies of Rosh and
Meshech or (as the name may be pronounced) Mosch; but I know
not, that we have any reason for supposing that they are here
intended.”** He then gave himself some good advice which, in
this context, he took to heart: “But let us forbear to speculate
on this obscure subject, further than we have the express warrant
of Scripture.”%5 His caution, however, did not prevent him from
identifying the “merchants of Tarshish,’ who oppose Gog, as
that same “great maritime nation” that resisted Antichrist, an
obvious reference to England again.®¢

The Napoleonic empire passed from the scene, but interest in
the restoration of the Jews continued. The first of a series of
prophetic conferences was held at the Albury Park estate of
Henry Drummond in 1826. Drummond was a wealthy banker and
member of Parliament (1810-13, 1847-60) whose patronage sup-
