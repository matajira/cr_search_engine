260 INDEX

Photography, 59, 198, 237
Pinckney, Mrs, Charles, 17
Piper, J. L., 157

Pitts, Lendall, 24

Pittsburgh Plate Glass Co., 221
Plastics industry, 234-235
Polaroid Corp., 237

Polk, James K., 92
polymerization, 235

Pope Manufacturing Co., 212
Population increase, 224

Porter brothers, 59

Postwar reconversion, 247
Potomac Co. 36

Powel, Samuel, 16

Price fixing, 154-155, 165,231, 249
Principio Iron Works, 22
Prindle, Karl Edwin, 234
Printing presses, 103-104
Private enterprise, 1-23
Progressive Party, 204

Proximity fuse, 246

Public utilities, 186, 195-196
Pujo Committee, 182-183
Pullman, George M., 79, 97
Pullman Palace Car Co., 165, 181
Pure Oil Co., 155

Putnam, Col. Israel, 29
Putnam, Silas, 50

Quakers, 2, 113

Rackham, Horace, 211

Radar, 251-252

Radio Corporation of America, 186,
199, 236, 253

s

Radio industry, 199-200, 220

Railroad industry, 74, 79-80, 114, 133-
134, 138, 144, 146, 159, 170-177

Ramsey, Joseph A. 170

Randolph, Edward, 3

Raskob, John J., 216

Raytheon Co., 246

Rebates, 146, 152

Refrigeration, 102, 221

Renin Rand Corp. 200,252

Reo Co., 209

“Report on Manufactures,” 40-41
Research and development, 155, 250-

253
Reuther, Walter, 232, 257

Revere, Paul, 11,24,57-59

Revolutionary War, see American Reyo-
lution

Revolver, 89-92, 128

Reynolds Aluminum Co., 221

Rickenbacker, Eddie, 238

Roads, 35, 63-64, 206-207, 220

Robbins & Lawrence Co., 93

Roberts, George B., 172

Robinson, Moncure, 70

Rockefeller, John D., 146-156, 163,
175, 179-183, 257

Rockefeller, William, 175

Rodney, Adm. George B., 30

Roebling, John A., 115

Rotfe, John, 21

Romney, George, 249

Roosevelt, Franklin D., 224-225, 230,
awl

Roosevelt, Nicholas, 71
Roosevelt, Theodore, 177, 204
Root, Elisha King, 89, 91, 93
Roper, Elmo, 226

Rowe, John, 9

Rowland, Henry, 191

Royal Dutch Shell Co, 156
Rubber industry, 103, 201
Rumsey, James, 51,71
Russell, George R., 48

Ryan, Thomas Fortune, 174, 194

Salem, Mass., industry in, 3, 10, 45-46,
49

Sanders, Thomas, 188

Sarnoff, David, 200

Schiff, Jacob, 166, 174, 181

Schlesinger, Arthur

Schoenberger, Dr. Pee 117

Schuyler, Gen. Philip, 66

Schuyler, Robert, 145

Schwab, Charles M., 160

Scott, Thomas A., 145, 157

Securities and Exchange Commission,
231

Self-starter, 215

Seligman, J. & W. Co., 165, 214

Seward, William Henry, 118

Sewing machine, 81, 107-111, 123-124,
262

Shaw, Maj. Samuel, 44

Shays’ Rebellion, 37

Shell Oil Co., 156

Sherman, William Tecumseh, 119

Sherman Antitrust Act, 155, 165,250

Sherman Silver Purchase Act, 182
