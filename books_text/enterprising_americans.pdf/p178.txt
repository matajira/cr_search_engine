THE GILDED AGE 153

Pennsylvania and New York. And when the newspapers ferreted
out the South Improvement contract and published it, the frightened
railroads quickly promised that all future oil shipments would be
on a basis of equality for everybody.

Tn response to the angry clamor, the Pennsylvania legislature
revoked the South Improvement Co.’s charter in April of 1872—
and that particular adventure in exacting special tribute from a
common earner to throttle competition was at an end. But Rocke-
feller had already used the tacit threat of the South Improvement
contract to bring the Cleveland refiners into his combination—and
these eggs, once scrambled, remained an inextricable part of the
dish, Moreover, Rockefeller’s resolve to “stabilii” the oil market
by eliminating competition remained inexorable, and the Standard
empire continued to grow. It was “common knowledge” to the oil-
men of the eighties that Standard had a firm grip on some 90
per cent of the refining business. It also controlled all the major
pipelines, and all the oil cars on the Pennsylvania Railroad. Its
tank-car control was to increase through its domination of the
Union Tank Line, called Rockefeller’s “secret weapon” by Albert
Z, Carr.

How had all this been accomplished? The enabling trick was
the “trust,” an ingenious legal device that had been cooked up by
a young Pennsylvania lawyer named Samuel C. T. Dodd. Dodd had
first come to Rockefeller’s notice as a vigorous opponent of the
South Improvement Co. Asked to become the Standard Oil lawyer
at a time when his voice was failing and incapacitating him for
courtroom work, the young Pennsylvanian took the job with the
understanding that he would keep the company within the law in
all future attempts to bring stability to oil marketing. Dodd knew,
of course, that property could be turned over to “trustees”: it was
done every day by people who wished to pass on their estates to
wards and minor children with some continuing provision for wise
control. Why not, so Dodd argued, why not adapt this ancient
device to the peculiar circumstances of the oil business? Why not
let the less able oilmen put their properties in “trust,” with Standard
Oil management acting as custodian?

The first Standard Oil trust was a small committee of nine men
headed by Rockefeller himself and Flagler. To this committee of
