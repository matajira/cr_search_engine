108 . THE ENTERPRISING AMERICANS

ter of fact, a Frenchman, Barthélemy Thimonnier, had made a suc-
cessful machine in 1829, and was already busy with a contract for
uniforms for the French Army when a mob, thinking the livelihood
of French tailors was at stake, broke in and destroyed his new
equipment. Eighty machines in all were wrecked in this savage
outburst. Two or three years later, in New York City, a man by the
name of Walter Hunt put the eye in the head of a needle and used
it to push a thread through cloth to interlock with a second thread
carried by a shuttle. This was the true secret of the modem sewing
machine-but Mr. Hunt’s daughter Caroline felt sorry for the hand
sewers who would be put out of business by her father and declined
to use the invention in her proposed corset-making establishment.
Hunt, who had other projects on the fire (he invented the safety
pin, a street-sweeping machine, paper collars, and a repeating rifle),
didn’t even bother to try for a patent on his machine until it was
too late.

Meanwhile Elias Howe blundered ahead, trying to make a ma-
chine that would copy the motion of his wife’s arm as she sewed
for himself and his three children. A double-pointed needle with
the eye in the middle didn’t work, but Howe was on the trail of
Hunt’s principle. By 1844 he had hit upon the notion of using two
threads to make a stitch that would be interlocked by a shuttle.
Impressed by Howe’s claims, a friend in the coal and wood business,
George Fisher, staked the earnest young man to board and room
for his family, provided him with a garret to work in, and advanced
him $500 for materials and tools. And in July of 1845, Howe’s
first practical machine sewed all the seams on two suits of wool
cloth.

What followed would have broken almost any man’s heart. Rac-
ing against the deftest professional seamstresses, Howe’s machine
won in a classic demonstration at the Quincy Hall Clothing Manu-
factory in Boston. The inventor got a patent on an improved sec-
ond machine in 1846, thanks to the expenditure of $2,000 by his
benefactor Fisher, but the tailors and garment-makers of America,
fearing their employees’ displeasure, would have none of it. Faced
with a dead loss of the capital he had already advanced, Fisher
pulled out of the parmership, and Howe, weary of battering his
head against local prejudice, decided to try his luck in England,
