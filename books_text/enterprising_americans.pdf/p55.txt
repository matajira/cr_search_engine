30. THE ENTERPRISING-AMERICANS

indeed, the colonists won largely by combining frontier guerrilla
tactics with watchful waiting until the French fleet cut off Corn-
wallis at Yorktown at a crucial moment. Nevertheless, Washing-
ton’s straggling army had to be supplied through seven hard years
of constant maneuver, and here businessmen continued to play
their part. When the colonists couldn’t get by on British cannon
seized at Ticonderoga, they smuggled their armaments through
the West Indies. This, in the language of Helen Augur, was the
“secret war’’—and the colonial shippers, old hands at dodging the
King’s customs collectors, took hold here with alacrity. The free
Dutch port of St. Eustatia in the Caribbean became Washington’s
favorite arsenal—and so well did it serve the Colonies as a point
of transshipment for gunpowder forwarded from Europe that the
British Admiral Rodney, sent out to command in the Leeward
Islands in 1780, said of “Statia” that “this rock of only six miles
in length and three in breadth has done England more harm than
all the arms of her most potent enemies, and alone supported the
infamous American rebellion.”

Tn Statia, the profit on gunpowder rose during the war to 700
per cent. The gunpowder came into the Dutch island in tea chests
and in rice barrels tucked into the holds of ships that had sup-
posedly cleared in Europe for Africa or the Mediterranean. The
agent for the state of Maryland on Statia was Abraham van
Bibber; together with Richard Harrison, an American factor on
the French island of Martinique, he forwarded to the Continental
Congress some of the first war supplies to reach the Colonies.
Along with the gunpowder would go a constant dribble of civilian
goods—linen and English thread stockings, French gloves, and
French sugars. Through certain secret arrangements that are
credited to Benjamin Franklin, the English island of Bermuda kept
sending salt—and cedar sloops—to the mainland of America
throughout the war.

Meanwhile, as envoys in Europe, able men like Silas Deane
of Wethersfield, Connecticut, teamed up with Ben Franklin to
form a sort of private State Department-cum-Board of Trade. By
intrigue and negotiation these secret committeemen combed the
capitals of the Old World for aid. Closer to Washington’s con-
stantly shifting’ field headquarters, a breed of native quartermas.ters
