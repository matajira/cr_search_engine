F.o.B. DETROIT - 267

even well into the 1900-10 decade, consisted of two civilizations,
each of which seemed permanently walled off from the other. The
towns were railroad-connected, but the American highway system
had undergone a marked retrogression since the days of the first
toll roads. Everywhere in eastern and middle America the country
roads were impossible quagmires in early spring; in summer there
were dust and deep ruts to contend with; in winter ice and snow
frustrated the smooth tires of the time. Rich townsfolk, hankering
for the nostalgic pleasures of the countryside, might dare these
inconveniences first on bicycles and then in the early cars, equipping
themselves with goggles and dusters for Sunday afternoon jaunts
or for longer AAA-sponsored “Glidden tours” into the wilds of
New Hampshire or Maine. Farmers, who couldn’t afford the first
cars anyway, were not disposed to vote a nickel for surfaced roads
to help the pioneering Detroit and New England automobile “crack-
pots.” (The first rural mile of concrete pavement in the United
States was not destined to be laid down until 1908, by the Road
Commission of Wayne County, Michigan.) Yet paradoxically it
was rural America that needed cars far’ more than the city slickers
who bought the first models.

The difficulties of bridging rural and urban America are well
illustrated by the early career of Ransom Eli Olds, who, without
disparagement to Henry Ford, was the first man to try to give the
farmer what he needed. A machinist who had built a three-wheeled,
steam-driven road vehicle as early as 1893, “Ranny” Olds had per-
suaded a Michigan copper-and-lumber millionaire, Samuel L.
Smith, to finance the Olds Motor Works in Detroit in 1899. (Of
$200,000 in paid-in capital, Olds himself contributed $400 and
Smith the remainder. ) Smith looked to make most of his money
from marine engines, but Olds had it in mind to produce a one-
cylinder buggy to retail for less than $700. Just as he was ready to
begin its manufacture, the Olds factory in Detroit burned down;
but the single completed model of the “curved dash” runabout—
the “Merry Oldsmobile” of the popular song—was saved from the
flames by a brave timekeeper. Without factory resources at his com-
mand, Olds proceeded to farm out the manufacture of his engines
and parts to various firms in Detroit (the Dodge brothers, Horace
and John, and Henry Leland of later Cadillac fame were among
