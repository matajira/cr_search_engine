236 . THE ENTERPRISING AMERICANS

went into toothbrush bristles, tennis racquets, fishing rods, and
self-lubricating bearings.

Other du Pent triumphs of the thirties included Lucite, synthe-
sized musk oil (a basis for fine perfumes), and the merchandising
of neoprene, the basis for a synthetic rubber. Meanwhile Union
Carbide, which had bought the Bakelite Co., the earliest hard-
plastic manufacturer in the country, for $11 million in stock, was
also expanding its oxygen and acetylene plants, and proliferating
with chemicals and alloys. Behind Allied Chemical & Dye and
Union Carbide & Carbon there were a profusion of lesser com-
panies: Dow Chemical, a bulk chlorine producer, which perfected
styrene for synthetic rubber; American Cyanamid, the first developer
of a nitrogen-fixation process; Monsanto, which moved by way of
coal-tar-based organics into petrochemicals and plastics; and the
oil companies, which developed the Houdry catalytic-cracking
process. In addition, there were fertilizer companies which pos-
sessed the industrial skills that would erupt in a vast array of
fungicides, herbicides, soil conditioners, defoliants, and insecticides
after World War IL In 1934 agricultural-chemical production
amounted to 100 million pounds; after the war the poundage
would soar to a yearly two billion.

While chemistry was leaping out of the test tubes of the thirties,
industrial physics was hardly quiescent, and there were also
developments on that strange frontier where physics and chemistry
meet. Rumors of an atom-smashing cyclotron came from the Uni-
versit y of California laboratory of Dr. Emest O. Lawrence, and
this suggested new sources of industrial power. General Electric,
on the advice of Dr. Arthur H. Compton, went into fluorescent
lighting; Carrier went ahead with air conditioning. Electronics hit
a commercial plateau period in the thirties as radio continued to
prosper; but Vladimir Zworykin of R.C.A. worked throughout
the decade to clarify the television image projected by his icono-
scope, and Philo Farnsworth, a free lance, developed independent
television patents. The FCC, which professed to have protective
feelings about the average citizen’s investment in his radio receiving
set, dawdled over granting a commercial television license until
1940—so the first leap forward in putting television sets into homes
was postponed by government fiat. But in Britain, where there
