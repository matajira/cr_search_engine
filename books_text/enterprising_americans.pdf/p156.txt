THE CIVIL WAR AND ITS AFTERMATH - 131

 

used posters and broadsides and popular songs; they importuned
people in hotels and trains; they invaded newspaper offices and
insisted on editorials as well as the more usual forms of journalistic
publicity. With such Barnum techniques, Cooke managed, at one
point, to market $600 million in bonds im 180 days. The 1862
