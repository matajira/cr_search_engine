26 . THE ENTERPRISING AMERICANS

but by the time of the Hanoverian Georges it had become a prime
British necessity (in 1767 an English writer complained that “as
much superfluous money is expended on tea and sugar as would
maintain four millions more subjects on bread). The new neces-
sity was grown in the “sugar islands” of the British West Indies,
which bulked considerably larger than all the North American
Colonies together in the British Mercantile Plan. In all, the British
investment in Caribbean island plantations—some 60 million in
pounds sterling in 1775—-was six times the debt owed to London
or Bristol agents by southern planters and the merchants of Boston,
Newport, and Philadelphia.

The sons of pioneer sugar planters had over the years been
returning from the islands to buy rotten borough seats in the
British Parliament, leaving their “attorneys” to handle plantations
through overseers. In the early 1770’s some seventy absentee
plantation lords were sitting for county boroughs in the House of
Commons. As a spectacular part of English life, with money to
spend on brilliant equipages drawn by horses whose hoofs were
shod in silver, these lords had identified themselves with the
Mercantilist System as no Virginia tobacco grower, in debt to his
Bristol factoring house, could possibly consider doing.

The protection of British Caribbean sugar, then, was a political
“must” almost from the very start. For a time, the colonists
were satisfied to trade for British “sugar island” molasses without
making much of an issue of it. But over the years the Dutch and the
French sugar plantations in the West Indies became more efficient
producers than the British plantations in Barbados and Jamaica—
and the New Englanders found they could get greater quantities
of molasses from the “foreign” islands in exchange for their own
exports of dried cod, barrel staves, horses, hay, and the slaves
which they picked up for rum in the famous “triangular” extention
of trade to West Africa.

In the England dominated by Robert Walpole, the bluff country-
man who was the Whig prime minister during much of the first part
of the eighteenth century, the mercantilist philosophy was tempered
by a widespread willingness to wink at smuggling. Walpole’s spirit of
indulgence extended to sugar and molasses. In general, the impor-
tance of the various acts of trade and navigation was not that
