THE PRE-cIVIL WAR SPEEDUP [dl

tinued to sprout from its Thousand-and-One Beginnings in the
older seaboard states as well as on the open western frontier. In
Philadelphia and in Hartford, Connecticut, men formed companies
to insure against fire and marine disaster. In Delaware the du Ponts
had developed their black-powder industry-but much of it went
quite unmartially to blast rock out of the way for the railroad and
canal builders and to uncover new veins of coal in Pennsylvania. In
Wall Street, Jacob Little, the first stock-market “bear,” devised the
art of going short in the market during the depression years after
1837. But others were bucking the trend and shrewder financiers were
looking to new inventors and developers. In 1839, William Harn-
den started an express service that was to become big business; and
Wells & Co. shortly extended express delivery to the cities of the
new Middle West preparatory to the later push of Wells Fargo
across the Great Plains. Men picked at the surface of the land for
lead in the Galena region of Illinois, for copper and iron in northern
Michigan, for marble and granite in Vermont, and for brownstone
in the quarries of the Connecticut Valley. And to make things
easier for the financiers, the miners who thronged to California
after 1849, either crossing the plains or taking Commodore Vander-
bilt’s ships to an isthmian portage in Nicaragua, were shortly ship-
ping $50 million in gold to help support new eastern projects.

The Era of a Thousand-and-One Beginnings abounded with
other types of pioneers. There was that curious man, Samuel Kier,
for instance, who got the idea that “rock oil’’—then used only for
medicinal purposes-could be used in lamps, and thereby opened
the way for the development of petroleum. A character named Ben-
jamin T. Babbitt (whose name later fascinated Sinclair Lewis)
built a monster soap factory in New York City, and was so success-
ful in selling his product from brightly colored carts loaded with
musicians that he put the word “bandwagon” into the language.
Another strange genius, called a madman by his neighbors, was
Frederic Tudor, a Boston Yankee who had refused to go to Harvard
because it was “a place for loafers.” Tudor had the crazy idea that
he might pack ice from a pond in Saugus and ship it in sawdust to
tropical countries. He actually built a great business transporting
natural ice from Massachusetts ponds and Maine rivers to the West
Indies and faraway Calcutta. One of the by-products of Tudor’s
