264 + THE ENTERPRISING AMERICANS

plain through two and one half centuries on the North American
continent. It will defend itself in its homeland even as it triumphs in
the extended West European Common Market, in the new Jap-
anese-Australian iron-and-steel Axis, and in the various Taiwans,
Hong Kongs, Ivory Coasts, and Brazils that have had the sense
to free the enterprise to work his assured magic.
