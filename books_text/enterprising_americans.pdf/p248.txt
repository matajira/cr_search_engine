12 The New Frontier of the
Depressed Thirties

Great expectations end in unprecedented world-wide defla-
tion.

The New Deal restores “con nfidence” in just about everything
except the profit system.

The “Big Three” of chemistry open up an astonishing world,
Optics, electronics, metal alloys, and the continuous-strip
mill,

The aircraft industry sprouts its wartime wings.

On the afternoon of October 24, 1929, Richard Whitney, brother
of a Morgan partner, and himself acting president of the New York
Stock Exchange, stalked to the U.S. Steel post and said bravely: “T
bid 205 for 10,000.” It was, as events proved, a wholly quixotic
gesture. Five days later “Big Steel” closed at $174 for the day, and
a general liquidation was on. The drop in stock values that began
on Black Thursday was not to be halted even by a Morgan-Rocke-
feller consortium, and within a matter of weeks some $30 billion of
paper value had gone up in smoke.

Tf 1929 had only marked a stock-market crash such as Wall
Street had often experienced since brokers first gathered under the
famous buttonwood tree in 1792, the history of the U.S. and the
world might have been very different. After all, the destruction of
paper values in tokens of ownership doesn’t change a single machine
tool, and paper values may come back. Black Thursday, however,

223
