64 + THE ENTERPRISING AMERICANS

Manifestly, the new world of manufacturing that Eli Whitney
had called into being at the turn of the nineteenth century de-
manded something better than canoes, rutted roads, and trails
marked by the tomahawk backings of the Indians. Mass manu-
facture was an anomaly—indeed, it was a commercial impossi-
bility—without quick access to market. The dilemma of the man-
ufacturer, using the first crude Whitney assembly-line system, is
weil illustrated by Eli Terry, the Connecticut clockmaker.

The year is 1803, and Terry, the teacher of a long line of Yankee
clockmakers, is already making clocks in his Naugatuck Valley
factory for which he has no storage space. With four clocks ready
for sale Terry has to tear himself away from his mill, load the clocks
into saddlebags, and take off over the hills toward “York State,”
walking beside his horse because the load is too heavy to permit a
passenger. The clocks are offered at $25 each on the installment
plan; when cash is entirely lacking they are “sold” for corn meal,
beeswax, sailcloth, or woven cloth, commodities that can be bar-
tered on the way home or passed on to workmen in lieu of cash
wages. Four years later Terry has a bigger mill—and has adopted
the full Eli Whitney technique of punching out standardized and
interchangeable wheels and clock faces. He is now prepared to
sell a clock for $5—but he still has no good way of getting his
merchandise to the customer.

Tn modem idiom, that was a hell of a way to run a railroad.
Terry, in fact, could have used a railroad-r any other method
of smooth and certain transportation. Actually, men like Terry
didn’t have too long to wait. Although progress may have seemed
slow to people of the time, transportation facilities in fact were
laid down with startling rapidity between the founding of the fed-
eral republic and its near extinction in the Civil War. First came
the toll roads, then the canals, then the rise of the river and lake
steamers, and then the first railroads, which later generations were .
to consolidate and merge into huge transcontinental empires; and
as the line of civilization pushed West great cities like Cincinnati
and Chicago emerged out of prairie and wilderness. Government
took a hand in this development, for the American people, though
they had resented British mercantilism, were not averse to govern-
ment help when it came to getting goods to market. However, when
