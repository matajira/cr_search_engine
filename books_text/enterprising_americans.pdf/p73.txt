48. THE ENTERPRISING AMERICANS

the architect of the Capitol in Washington and Boston’s State
House).

On her second voyage the Columbia under Captain Robert
Gray poked her way, in 1792, into the mouth of a great river,
the “Oregon” of William Cullen Bryant’s “Thanatopsis.” Having
opened a valid U.S. claim to the Oregon country, the Columbia
took furs from the Columbia River Indians for copper and
cheap bolts of cloth, and was off again to China. Many another
Boston ship followed her in the years to come, varying the sea-
otter peltry cargoes by picking up sandalwood in the islands of
“Owyhee.” Through trading with the Cantonese by way of the
American Northwest, a Cape Cod boy like Captain Bill Sturgis
could become a Boston merchant of high consequence and, later,
a manager of investment capital.

When the sea-otter pelts and the Hawaiian sandalwood ran out,
Sturgis and others turned to trafficking in California hides. To quote
George R. Russell, a merchant scholar of the era, the typical Yankee
merchant was soon sending his “merchandise all over the earth; [he]
stocks every market; makes wants that he may supply them; covers
the New Zealander with Southern cotton woven in Northern looms;
builds blocks of stores in the Sandwich Islands; swaps with the
Feejee cannibal; sends the whaleship among the icebergs of the
poles . . . piles up Fresh Pond [a reference to exported ice] on
the banks of the Hoogly . .. and makes life tolerable in the
bungalow of an Indian jungle.” American merchant shipping had
its many “downs” before the English eventually took the business
away from both Boston and New York with the advent of the tramp
steamer. But, despite the interruptions caused by the Jeffersonian
Embargo of 1807, the Non-Intercourse Act, the War of 1812, and
the long depression of 1837-43, shipping continued for a full
half-century to be a mainstay of the young Republic’s economy.

During the early Napoleonic troubles, when the British and
the French were locked in a deleterious trade war that accompanied
the military campaigns, the Yankees took over a major part of
the carrying trade of the world. And for a period after the
Napoleonic wars Americans fought the British successfully for
their share of world commerce. It was not until the discovery of
gold in California in 1848 that American businessmen began to
