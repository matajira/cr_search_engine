THEPRE-CIVIL WAR SPEEDUP 109

where he fell on new misfortunes. William Thomas, the agent who
had paid £250 for his share of Howe’s English rights, kept all the
subsequent royalties for himself, making a profit of a million dol-
lars. Meanwhile Howe was so poor he had to pawn his clothes to
pay for the cab that took his sick wife to the dock when she de-
cided to go home. He had later to borrow money to reach his wife’s
bedside in Cambridge, Massachusetts? and was just in time to see
her die of consumption. It was a sad homecoming for him in other
ways, for while he had been in England his basic idea had been
widely copied.

In spite of his mischances, however, the hapless ex-farm boy did
have the basic sewing-machine patent; and by 1851 he had man-
aged to interest new partners who were willing to carry on suits
for the rapidly spreading infringements. At this point Howe ran
up against Isaac Merrit Singer. This singular man, who has been
described as “charming but vain, creative but concupiscent, tal-
ented but dilatory,” had been a wandering journeyman mechanic
and a ham actor. He had patented a wood-carving machine, and
knew something of basic mechanical movement when he chanced
to watch the attempts to repair a primitive sewing machine in a
Boston shop in 1850. Completely broke himself (his wood-carving
machine had been wrecked in a boiler explosion), Singer got out a
pencil and paper. The next day he returned to the shop with a
sketch of a machine that rested on a table with the cloth supported
horizontally in a position to be guided by the operator. A
presser foot held the cloth firmly against the upward lift of the
needle. With forty borrowed dollars Singer worked day and night
in a shop belonging to other men, sleeping but three or four hours
a night and eating only one meal a day. In eleven days he had the
Singer sewing machine. At one point it looked as though the thing
were a flop, until it flashed upon him that he had forgotten to
adjust the tension on the needle thread before trying it.

Bitter patent litigation now broke out between Howe and Singer,
who had as his chief lieutenant a bright lawyer and money raiser,
Edward Clark, of New York City. Eventually Howe got a favorable
decision from the Massachusetts courts assuring his rights to his
invention of the eye-pointed needle. The only way, however, that
Howe could capitalize on his victory was to collect royalties from.

vertical
