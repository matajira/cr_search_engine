142. THE ENTERPRISING AMERICANS
lusty change. Speaking of the Boston business scene, which he knew
at first hand, he wrote; “Before ‘Appomattox’ the banker and
merchant appeared upon State Street, the business center, about
ten in the morning, conventionally dressed, precise in movement
and habituated to archaic methods. Within six months after the
fall of the Confederacy the financial centers of the ‘Hub,’ vitalized ,
by the inflow of new and very red blood, had taken on the aspect
which is familiar to this generation. Everything that interfered
with serviceable activity was set aside. Tall hats and long coats
disappeared. ... New names appeared at the head of great
industrial enterprises. Boys who had gone to the War as junior
officers had brought back honorable titles which vouched for
responsibility, character, and daring. . . . You can’t, if you will,
hold down a Captain, a Colonel . . . who has earned and won
the admiration of the public, and who has tested his worth.” As
for Howells’ own fictional businessman and colonel, the self-made
paint manufacturer Silas Lapham, he is, though a trifle coarse by
Brahmin standards, a thoroughly likable and honest fellow. Since
Howells was not an inventive novelist, somebody must have sat
for the portrait.

If Howells had been writing of post—Civil War New York City,
the center of the new Kingdom of Push, he might have mused
upon the fading of such old families as Beekmans, Rhinelanders,
and Brevoorts from the active business scene. As Burton J. Hen-
drick, our pioneer historian of the Age of Big Business, has pointed
out, the U.S. was to hear less and less henceforward from landlord
millionaires like William B. Astor (worth $6 million), or James
Lenox ($3 million). The old New York merchant aristocracy—
William Aspinwall ($4 million from shipping), John Haggerty
($1 million from auctioneering), Japhet Bishop ($600,000 in
hardware), William L. Coggeswell ($500,000 as a wine importer)
—was on its way to superannuated respectability. A. T. Stewart,
who had made $2 million in dry goods, and Phineas T. Barnum
($800,000 from exhibiting Jumbo and Tom Thumb and acting as
impresario for singer Jenny Lind ) would still manage to ‘stir
others to emulation, but in the coming age the word “merchant
prince” had an increasingly archaic ring.

In the swift change from old to new, one representative of the
