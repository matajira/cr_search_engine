Author’s Preface (1991)

w  uenI wrote The Enterprising Americans back in 1959 and
1960, which ran in thirteen consecutive issues of Fortune Magazine,
the American businessman was without honor. He was a Robber
Baron, or, if not that, a Babbitt, at best likeable but crude. These
stereotypes annoyed my friend John Davenport, a managing editor
at Fortune. As I have told the story in my original introduction to
the book, he had been looking for a business history to give his
many daughters. They had asked for something more than Matthew
Josephson’s The Robber Barons or the many histories that looked
on trade with disdain. I was quite ready to volunteer as a cicerone
for John Davenport’s girls, for I had experienced years of forced
immersion in corporation stories for Harry Lute, who had started
Fortune in the depression in hopes that he might make business
respectable.

The book that I produced must have started something, for Har-
per and Row, its publisher, kept it going for almost thirty years as
the word “enterpriser” gave way to the Frenchified “entrepreneur.” I
revised it once to catch up with new developments in electronics, the
spread of shopping malls, and the birth of computers. Silicon Valley
was something definitely American, with new business sprouting
from garages and backyards. Silicon Valley gave the microchip to
Japan, but it is still improving on it for homegrown uses.

The question naturally arises: why not further revisions? One
reason for not continuing revisions ad infinitum is that the Ameri-
can example has proved all too contagious. It is now “The Enter-
prising World.” General Electric, Thomas Edison’s old company,
thought it had an inside track on all household appliances. But it
found it couldn’t compete with single-minded Koreans in making
and marketing microwave ovens. It had to work overtime to pre-
serve its lead in compressors for refrigerators.

A big chapter in The Enterprising Americans is titled “F.O.B.
Detroit.” But Detroit is struggling to sell in competition with SAAB

Ix
