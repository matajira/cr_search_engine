THE CIVIL WAR AND ITS AFTERMATH « = 137

1860's, that first showed how a western road could originate
freight. The original proprietors of the Kansas Pacific had no
idea of how they were going to earn enough from their road to live
on, But in the South, far off in the strip of Texas between the
Nueces and the Rio Grande, cattle had mu'tiplied during the
Civil War. The Texans, who had learned how to protect their herds
from the Apaches and Comanches by use of the horse and the
Colt revolver, tried to find buyers for their stock in Missouri in
the first days after the Civil War, only to discover that war-
hardened border ruffians made it impossible to move their herds
with any safety past the Ozark Mountain country, where men
used to the open plains could be ambushed. It was at this point
that a business strategist from Illinois, a cattle buyer named
Joseph G. McCoy, looked at the map. Journeying in 1867 to
Abilene, Kansas, a “small, dead place, consisting of about one
dozen log huts . . . four-fifths of which were covered with dirt
for roofing,” McCoy brought with him enough lumber from Han-
nibal, Missouri, to build pens and loading chutes for cattle. He
then sent a scout to the South to intercept Texans who were hoping
somehow to get past the border ruffians to a market in Sedalia
or St. Louis. The first Texas drover that his scout encountered felt,
McCoy said, that “it was almost too good to be believed” that
“someone was about to afford a Texan... any other reception
than outrage and robbery.” The Texans came in force over the
open plains to Abilene, first of the cow towns—and from this
time forward it was Chicago, which was at the other end of
McCoy’s rail connections, that got the swiftly growing cattle
business as the whole area of the High Plains became the new
Cattle Kingdom. (St. Louis lost out when the president of the
Missouri Pacific threw McCoy out of his office.) The fact that
cattle money was the first northern money to move south of the
Mason-Dixon Line after the Civil War has prompted historian
Walter Prescott Webb to ask, “Who can say that Abilene was
less significant than Appomattox?” Or, as the historian might have
asked, “Who can say that the Kansas Pacific was less significant
than the Central and Union Pacifies?”

Even with cattle, however, the immediate post-Civil War surge
of railroad building through the empty country west of the 98th
