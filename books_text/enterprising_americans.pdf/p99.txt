74, THE ENTERPRISING AMERICANS

land made the connecting-to-seaboard links of the Erie Canal and
the Welland-St. Lawrence system seem inadequate. Copper and
iron ore had been uncovered in northern Michigan and miners were
clamoring for transport. As part of the lake traffic system, Chicago
was already an important town of 25,000 people in 1847, before a
a line of railway had reached it.

Even as, with the clipper ships, the 1850’s marked the culmina-
tion of the age of sail on the oceans, so they marked the peak of
steamboating on western waters. The river steamer went through a
period of rococo ornamentation and then settled down into a tradi-
tion of stately passenger carriers. The Yorktown, for example, had
forty private cabins-or twenty-four more than were provided by
the Queen of the West, the British steamer built for the India trade.
At one time there was more steamer tonnage on the western rivers
of the U.S. than the British Empire had on all the oceans, and even.
after the Civil War had ended, riverboats-the Natchez, the Rob-
ert E, Lee—were making new records on the run between New
Orleans and St. Louis. Long before the riverboats reached their
peak traffic, however, they were doomed by the arrival of the “iron
horse,” which was preceded, as early as 1826, by the horse-drawn.
tramway financed by Thomas Handasyd Perkins to carry granite
for the construction of the Bunker Hill Monument.

Railroading, like most other things in American business, grew
out of small and confused beginnings. Even while operating ships,
the redoubtable Colonel John Stevens built a minuscule steam loco-
motive to run on a small circular track in his Hoboken yard, and
obtained a charter to construct a railroad across New Jersey. In
1825, George Stephenson in England proved that his famous
“Locomotion No. 1” could pull a ninety-ton train of thirty-four
wagons at a speed of ten to twelve miles per hour. Two years later
the Delaware & Hudson Canal Co. tried to use a British importa-
tion, the Stourbridge Lion, on its short “anthracite railroad” to
carry coal from the Carbondale mines to the canal itself. The en-
gine proved too heavy for the trestles, which ignominiously col-
lapsed under its weight. In the same year, two Baltimore bankers,
Philip Evan Thomas and George Brown, obtained a charter for
America’s first successful railroad line, the Baltimore & Ohio. They
broke, ground for their railroad on July 4, 1828. Soon, at a cost of
