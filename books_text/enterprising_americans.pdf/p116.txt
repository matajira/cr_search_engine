FRONTIER AND FACTORY - 91

The gun the Rangers were using was the Colt revolver in its
first primitive guise, the brain child of a remarkable man. Born in
Hartford, Connecticut, in 1814, Sam Colt had persistently annoyed
the neighbors wherever he lived as a boy by his harum-scarum
experiments with black powder. In Ware, Massachusetts, he blew
a raft out of a pond during a Fourth of July celebration, and had
to be shielded from a soaked and angry crowd by young Elisha
King Root, who was one day to become his production genius. On
another occasion he blew the windows out of a school building.
The proper place for such a boy seemed to be at sea, so Sam was
packed off at the age of sixteen on a brig bound for Calcutta. In
the Indian Ocean, while watching the helmsman’s wheel spin over
and catch when a spoke came into line with the desired shift in the
brig’s direction, the mysterious cross-education that makes for new
inventions sparked something in Colt’s mind. A whittler, the young
Colt soon had a wooden model of a pistol with a revolving cartridge
cylinder ready for practice whirls at lining up bullets with a station-
ary barrel. He got his first patent in England in 1835 (the year
before the birth of the Texas Republic), and a year later a Wash-
ington patent followed. With a promise of $230,000 in capital,
some of which never materialized, Colt founded the Patent Arms
Manufacturing Co. in Paterson, New Jersey, the town that had
been projected by Alexander Hamilton. Despite the early orders
for revolvers from the Texans, the company soon fell into bank-
ruptey.

What saved Colt was the coming of the Mexican War in 1846
and a hurried trip to New York by Captain Samuel Walker of the
Texas Rangers to look for arms. Together the slight, taciturn Texas
Ranger and the flamboyantly genial Colt set out on a tour of the
New York City gunshops. Everywhere they got the same answer;
military volunteers had cleaned the shelves out. Whereupon Walker
remarked cryptically that it was just as well. What the Texas Ranger
captain wanted from the charming Mr. Colt was a newly designed
gun, with a trigger guard and loading attachments that wouldn’t
come loose in the middle of a fight. The lack of a factory bothered
Colt less than the criticism, which he swallowed because he knew
that Walker’s interest meant his rehabilitation as a manufacturer.

To get around a recalcitrant War Department, Walker appealed
