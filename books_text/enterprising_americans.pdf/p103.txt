78 +. THE ENTERPRISING AMERICANS

basis for a through line from Albany to Buffalo, no attempt was
made to compete with the Erie Canal for long-haul business. The
seven roads had different equipment, did not sell through tickets,
made no effort to dovetail their schedules, and refused to use com-
mon stations. The situation approximated the French economist
Bastiat’s sarcastic description of a discontinuous railroad, organ-
ized to provide superior livings for porters, not to give service to
shippers and passengers. The dilatoriness of New York State busi-
nessmen in bringing railroads into New York City helped Boston
to become one of the more successful railroad terminals. Three
different lines made Boston the “hub” of a wheel by linking it with
Lowell, Worcester, and Providence; and by throwing an early line
over the Berkshire, Bostonians made their own connection with
the West.

Despite the anti-railroad propaganda floated by the canal com-
panies, which solemnly warned Congress about the menace to life
and limb represented by the spark-belching iron horse, the railroads
were soon carrying the more perishable parts of the cargo that had
once been entrusted to the waterways. There were still people who
questioned the superiority of the rails, and there was, to be sure,
plenty of business to be shared by both types of carrier for a long
time to come. But eventually the canals were to be relegated to an
inferior position, doomed to a mule-gaited trade of hauling such
proletarian substances as ice, granite, gravel, limestone, coal, and
brick. The iron horse proved itself not only a useful carrier but a
profitable one, and money poured into the roads from the eastern
cities. Textile and shipping profits went into the early lines radiating
out from Boston. In Pennsylvania the Scrantons, owners of iron
foundries, invested in the Delaware, Lackawanna & Western, and
the anthracite roads were financed by coal-mine owners. The Mo-
hawk & Hudson was financed by New Yorkers, who listed it on the
Stock Exchange as early as 1830. The New York money market
was utilized by the New Jersey railroad builders; and the Second
U.S. Bank took one-quarter of the stock of the Philadelphia &
Reading.

The huge wonder of the railroad to our ancestors is summed up
in Squire Hawkins’ ecstatic outburst to his wife in Mark” Twain’s
and Charles Dudley Warner’s The Gilded Age. Speaking of the
