62 . THE ENTERPRISING AMERICANS

and the gunshops of Eli Whitney and Simeon North, were fated to
remain “sleepers” in most manufacturing businesses for some time
to come. The industrial revolution in America was still waiting for
the national market that had been guaranteed, in legal form at
least, by the Constitution. And the market itself was waiting for
the magic wand of easy transportation to touch it to life, as Samuel
Slater knew when he put $40,000 earned from cotton-thread pro-
duction into tumpike stock.

The task of the next generation of enterprises was to break
through to the West, to tie America together with roads, canals,
and river transportation, and even, perhaps, with the high-pressure
steam carriage that Oliver Evans had tinkered with in his later
years. Eli Whitney’s most important invention—which was nothing
Jess than the invention of a method-could wait its day.
