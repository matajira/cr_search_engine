INTRODUCTION XXII

as later arrivals came on the scene. Meanwhile new products and ‘
processes continually rose to compete with the old. Aluminum,
even when there was only one company in the field, had to fight
it out with wood at one extreme and steel at the other. Du Pent
artificial fibers freed the textile business from dependence on cotton,
silk, and wool. The railroads were controlled more effectively by
competition from automobile and truck and airplane than they
were by the ICC. From telephones to television, the electrical revo-
lution leaped from dependence on wires to dependence on wave
lengths in God’s free ether. Came, too, the supermarkets and con-
sumer credit, washing machines, home freezers, and the split-level
ranchhouse which never looked upon a longhorn steer.

So it went—a success story if there ever was one. How to account
for it? It is true that Americans inherited a rich and almost empty
continent that had hitherto been peopled by a few nomadic red
men. It is also true that the ever expanding western “frontier” pro-
foundly shaped American institutions, as William Graham Sumner
and Frederick Jackson Turner among others have emphasized.
Even so, the inviting presence of an unspoiled continent meant
little when taken by itself. Siberia, though admittedly a more diffi-
cult country, was equally unspoiled, but the early Russian settlers
never managed to cut the apron strings that bound them to St.
Petersburg. And Spaniards to the south of the British colonists in-
herited equally empty’ spaces yet never developed those spaces in
the manner that came naturally to the descendants of the Cavaliers
and Puritans.

The point is that before there can be a real frontier there must
be creative frontiersmen. And here the American story owes much
to its selective inheritance from the Old World. In crossing the
Atlantic the “new men” of America left behind them much that
was to impede European development in the seventeenth and
eighteenth centuries—vestiges of feudal master-servant relation-
ships, traditions of absolute monarchical allegiance, a society of
“status” and classes as opposed to a society of free men. Yet the
American development is also entirely incomprehensible without
reference to what its frontiersmen brought with them across 3,000
miles of ocean. They enjoyed, after all, a common language; they
were heir to three hundred years of the development of English
