274 INDEX

Bishop, Japhet, 142

Bishop, J. Leander, 143

Bissell, George H., 149

Blake, Lyman R, 111
Blanchard, Thomas, 102
Bland-Allison Act, 182

Blue Ridge Corp., 228

Boeing Airplane Co., 241

Bond sales, 130-132, 138, 169
Boone, Daniel, 63

Borden, Gail, 110

Berth, Christie, 220

Boston Associates, 56

Boston Massacre, 10

Boston Tea Party, 10,24,29
Bowditch, Nathaniel, 47, 56
Bowdoin, George S. 173
Bradford, William, 2

Brandeis, Louis, 181
Brandenberger, Jacques Edwin, 234
Brass industry, 57,59, 190
Breed, Ebenezer, 111

Breer, Carl, 219

Brewster, James, 102

Briscoe, Benjamin, 204

British Mercantile Plan, 26, 262
British Navy, 7, 10-11, 17,44-45
Brown, George, 74

Brown, Capt. James, 11

Brown, Moses, 53

Brown, Nicholas & Co. 11
Brown, Obadiah, 11

Brown, Syivanus, 55

Brown & Sharpe Mfg.Co., 211
Brown University, 12

Brush, Charles F., 190, 195
Bryan, William Jennings, 165, 182
Buell, Abel, 61

Buick, David D., 202

Bulfinch, Charles, 45,47
Burgess, Hugh, 104

Byrd, William, IT, 20

Cadillac automobile, 92, 204, 207, 211.
Calhoun, John C., 132

Callis, Dr. Conrad C., 235

Cambria Iron Works, 117, 158-159
Canals, era of, 66-67, 70

Candee, L. Co, a0

Canning industry, 1

Capital, 7-8, 1, ‘oa, 138, 184-201
Capital expenditures, 122

Carnegie, Andrew, 117, as 56-162

Carnegie, Thomas M., 157-159

Cartwright, Edi
Cattle, 137-138
Cellophane, 234, 250
Central Pacific Railread, 134-137
Champion, Albert, 203,

, Roy D., 208,217
Charch, William Hale, 234
Chase, Salmon P., 129
Chase Manhattan Bank, 244
Chase National Bank, 181
Chemical industry, 233-234,237
Chemstrand Corp., 250
Chesapeake & Ohio Railroad, 76, 172,

248

Cheves, Langdon, 120

Chevrolet car, 204, 215-217

Chicago, Burlington & Quincy Railroad,
175-176

Chicago, growth of, 64, 73-74, 79-80,

i

China clipper trade, 4445,47,49.

Chinese labor, Central Pacific, 135

Chrysler, Walter P., 214,2:9

Civil War, 81, 119-139

Clark, Edward, 109

Clark, V. 8., 57

Clay, Henry, 66, 100, 133

Clinton, De Witt, 66-67

Clipper ships, 44-45

Clock industry, 61,64, 93

Clothing industry, 124

Coal mining, 69,71

Cochran, Thomas, 166

Codfish trade, 6, 29

Coffee trade, 18, 46

Coffin, Charles A. 195

Coffin, Howard Earle, 209, 238

Coggeswell, William L., 142

Coleman, William, 158

Cones, Christopher, 66

Collins Co. of Collinsville, $8,92,99

Collins Radio Co., 241

Colonial Air Transport, 239

Colonial trade, 1-23

Colt, Samuel, 61,84, 89,91-92

Colt revolver, $1,90-92,99, 128, 137

Columbia Broadcasting System, 200

Columbus Oil Co., 157

Commonwealth Edison Co., 186
