Index

Abbott, Horace, 129

Acme Oil Co,, 152

Adams, James Truslow, 225
Adams, John, 28,61
Adams, John Quincy, 66
Adams, Sam, 24

Adams Express Co., 181

Aetna Fire Insurance Co., 168

Africa, trade with, 3, 12, 17, 26,44,46

Air conditioning, 236

Airplanes and airlines, 206,238-241

Allen, Ethan, 112

Allied Chemical Corp., 234,236

Almy & Brown Co., 53-55

Aluminum Company of America, 221,
27

Aluminum industry, 183-184

Amalgamated Copper Co., 180

American Airlines, 240

‘American ‘Bridge Co., 177

American Can Co, 166

American Cyanamid Co. 236

American Hide & Leather Co., 180

American Motors Corp., 248

American Optical Co., 237

American Revolution, 10, 16, 18,24-42,

American Rolling Mill Co., 237

American Smelting & Refining Co., 180

American Steel & Wire Co., 177, 179

American Sugar Refining Co. 165, 174,
180

American Telephone & Telegraph Co.,
181, 186, 189, 198-199,252,262

American Tin Plate Co., 177

American Tobacco Co., 165, 174, 196

Anderson, John W., 211

Anderson, Joseph Reed, 128

Andrews, Samuel, 150

Appleton, Nathan, 55

Archbold, John D., 152

Arkwright, Sir Richard, 53

Arien Realty Co., 262

Armour. Philiy D.. 125
Armstrong, Maj. Edwin H., 199
Aspinwall, William, 142

Astor, John Jacob, 83-84
Astor, William B., 142

Automation, 107-108

Automobile industry, 102, 183,202-222,
242, 248

Avery, C. W., 212

Aviation industry, 238-241

Ax industry, 88-89,92,99, 112

Babbitt, 220,224

Babbitt, Benjamin T., 101

Babson, Roger, 196

Baker, Gourge F., 173, 181

Baldwin Locomotive Works, 181
Baltimore & Ohio Railroad, 74-76, 145,

Baltimore Turnpike, 65-66

Banking, growth of, 129-130, 166, 182

Bankers Trust Co., 181

Bank of the United States, 39, 40, 66,
78,120

Barker, George F., 190

Barnum, Phineas T., 142

Baruch, Bernard, 184,212, 228,231

Bausch & Lomb Optical Co. 237

Bell, Alexander Graham, 183

Bell Telephone Co. 189

Bell Telephone Laboratories, 246, 252

Benz, Carl, 205

Berle, Adolf, 243

Bernays, Edward, 256

Bessemer process, 116-118, 128, 145,
156, 159-160

Bethlehem Steel Co., 117, 129, 183, 256

Bicycles, 206, 212

Biddle, Nicholas, 120

Big business, 146-162,204,243

Bingham, William, 86

273
