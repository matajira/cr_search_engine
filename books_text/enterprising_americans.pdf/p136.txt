THE PRE-civi1L WAR SPE EDUP Lit

machine pegging had evaporated, as shoe manufacturers pirated
the various mechanical pegging devices there wasn’t much money
in them. By the time a manufacturer had paid for the etemal litiga-
tion his pegging profits had vanished. Consequently, when Lyman
R. Blake in South Abingdon, Massachusetts, managed to do the
heavy work of sewing soles to uppers mechanically, the industry
was ready for his invention.

Tronically, the Blake invention, patented in 1858, became known
as the “McKay Sewing Machine’’-so called because Gordon
McKay, an engineer, bought Blake out for cash and a royalty that
was to total $70,000. (Eventually, through patent renewal, Blake
got far more money. ) McKay had the financial resources to fight
patent suits—and he also had the will and the skill to adapt Blake’s
machinery to large-scale operations. He helped devise special ma-
chines to melt the wax on thread as it passed through heavy
leather, and he substituted steam-driven factories for the old piece-
work system that had been in force ever since Ebenezer Breed, the
Quaker, had started the wholesale shoe business in Lynn in the
eighteenth century.

McKay refused to sell his machines outright, preferring to lease
them and to collect royalties on every pair of shoes made. As a
bonus, the client who leased a McKay machine got a small part of
the McKay company capital stock. As patents ran out, McKay kept
picking up other inventions: he was, at one time, a partner of
Charles Goodyear Jr., the son of the inventor of vulcanized rubber.
In time the so-called “Goodyear welt” was to become synonymous
with quality shoemaking, and when S. V. A. Hunter, secretary-
treasurer of the company, suggested that the welt be advertised as
“better than” hand sewing, an important mercantile slogan was
born. Out of the McKay industrial leasing and various mergers
United Shoe Machinery was eventually created—and so low did this
company keep its royalties that it encountered little organized oppo-
sition for a long period.

The growing industrialization of the nation in the forties and
fifties argued the necessity of an iron and steel industry. But here
the U.S. lagged for years behind Britain, partly because the British
government in the eighteenth century had tried to hold the Ameri-
cans down. Much of the colonists’ pig iron was in fact exported to
