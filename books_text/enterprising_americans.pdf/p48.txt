FREE ENTERPRISES BEFORE THE REVOLUTION - 23

ciating colonial currencies, that made Washington one of the rich-
est men in eighteenth-century America, Washington’s shrewdness
in conducting continual hedging operations against inflation is
clearly reflected in a letter to his stepson, John Parke Custis, that is
dated October 10, 1778. “A Moment’s reflection,” so the embat-
tled colonial commander-in-chief wrote, “must convince you of two
things: first that Lands are of permanent value, that there is scarce
a possibility of their falling in price, but almost a Moral certainty
of their rising exceedingly in value; and secondly, that our Paper
Currency is fluctuating; that it has depreciated considerably, and
that, no human foresight can, with precision, tell how low it may
get as the rise or fall of it depends on contingencies which the ut-
most stretch of human sagacity can neither foresee, nor prevent.”

Though he always retained the predilections of landed gentry in
his mode of living, Washington had the instincts of a modern busi-
ness developer. With twenty other Southerners he organized a com-
pany in 1763 to drain the Great Dismal Swamp south of the Vir-
ginia port of Norfolk. He ran the company himself as managing
director from 1763 to 1768, and eventually his executors collected
dividends of $18,800 from 1810 to 1825 on the Great Dismal
Swamp project; they finally sold the Washington share in it for
$12,000.

Working on the Dismal Swamp drainage canals had set Washing-
ton to thinking about an all-water route into the Ohio country. But
such ideas were rudely shoved aside by the onrush of political
events, With the end of the French and Indian War in 1763, and
the removal of old threats, Britain came to the conclusion that
there was no further reason for allowing the colonists to expand
westward. A buffer zone was no longer needed. This was not only
a blow to the dreams of a young Washington; it was one more
signal that before Americans could further expand their own enter-
prises they must provide themselves with a new political framework
in which business could operate. And as Washington himself was
to discover one day in 1775, the frame had to wait the issue of war.
