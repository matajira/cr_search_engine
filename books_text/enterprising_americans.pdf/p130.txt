THE PRE-CIVIL WAR SPEEDUP - 105

devised various processes for dressing rags and pulp and for doing
away with the old “loft drying” of paper sheets.

The great mutation in business communications came with the
invention of the telegraph, the work of a painter, Samuel F. B.
Morse, whose father Jedidiah had written the first American geog-
taphy book, In reality the telegraph could have been made by any-

 

Courtesy Smithsonian Institution

Elias Howe, Jr.

one, for it consisted of the commercial application of known prin-
ciples. Actually, the first fruit of Morse’s experimentation with
sending electric charges over wires was a submarine cable insulated
with tar that he sank in New York Harbor in 1842. This led Morse
to predict that a cable would someday span the Atlantic-and,
indeed, Cyrus Field, bankrolled in part by Peter Cooper of Tom
