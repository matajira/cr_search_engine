THE GILDED AGE 145

den Erie and the well-managed New York Central, the Pennsylvania
system was built up in the sixties and the seventies under the wise
and circumspect direction of J. Edgar Thomson. If Thomson was
not averse to turning a quick deal to his own private advantage
on occasion, he kept this aspect of his character quite apart from
his rigorously ethical concern for the Pennsylvania’s economic
health. In 1859 the Pennsylvania consisted largely of the Main
Line from Philadelphia to Pittsburgh—but within ten short years
Thomson had expanded the system until it comprised nearly a
thousand miles in the state of Pennsylvania itself and (by virtue
of leasing the Pittsburgh, Fort Wayne & Chicago line) had reached
the shores of Lake Michigan. Not satisfied with the single terminus
of Chicago in the West, Thomson formed a holding company
to acquire other lines, notably the Cleveland & Pittsburgh Railroad
and the so-called “Pan Handle” route, which linked Pittsburgh with
both Cincinnati and St. Louis. Thomson’s management ended
in 1874, but he passed on to his successor, Thomas A. Scott (who
was, incidentally, Andrew Carnegie’s friend and boss), a property
quite capable of weathering the long depression that began in 1873.
To quote John Moody, the Pennsylvania “was the first American
railroad to lay steel rails and the first to lay Bessemer rails; it was the
first to put the steel firebox under the locomotive boiler; it was
the first to use the air brake and the block signal system; it was
the first to use in its shops the overhead crane’’—and from 1859
to the end of the nineteenth century (and after) it never skipped
a dividend. Moreover, unlike its great competitor for freight origi-
‘nating in the new Pittsburgh area, the Baltimore & Ohio Railroad
(which in times of stress paid dividends out of capital), the Penn-
sylvania paid all its dividends out of earnings, with the stockholders
themselves keeping a tight rein on management.

The records of both the New York Central and the Pennsylvania
were in marked contrast to that of the Erie, whose mulcting by
Drew, Fisk, and Gould is often cited as “typical” of Gilded Age
railroading. And few post-Civil War railroads were as badly served
as the pre-Civil War New York and New Haven Railroad, whose
president-Robert Schuyler—issued 17,752 shares of unauthorized
stock and sold them to his own brokerage house before skipping
the country.
