2. THE ENTERPRISING AMERICANS

By 1765 the Browns are in the iron business, in which they
flounder until they hire a foundry master from Baron von Stiegel’s
Lancaster Furnace in Pennsylvania to put their own Hope Furnace
in Rhode Island on a paying basis. This iron business is part of a
growing complex of blast furnaces and forges in the Colonies—by
1775 there will be more furnaces and forges in America than in
England and Wales, and statisticians will shortly be estimating that
America is producing almost a seventh of the world’s output of
iron pigs and bars. The Browns’ ledger books begin in 1723, the
records kept by later son-in-law partners and their descendants
have continued in uninterrupted sequence until today, covering a
span of over two centuries. The estate still owns property in Ohio—
and keeps books pertaining thereto. And the name of Brown, of
course, is enshrined in a great Rhode Island university.

Newport, which flourished earlier than Providence as the first
commercial capital of Rhode Island, was a cosmopolitan center
with an atmosphere of great tolerance. Jews were free to worship
there in what is said to be the first synagogue in America, and both
Bishop Berkeley, the philosopher, and Ezra Stiles, an early Yale
president, found the place much to their liking. But in spite of its
tolerant charm Newport represents one of the darker strands of our
early business history. It was not alone in the development of the
slave trade with West Africa: ships from Massachusetts frequently
visited the Guinea coast, where they crammed black men into their
‘tween-deck spaces for export to the sugar lands of the West Indies
and the tobacco plantations of the South. Newport ships, however,
made a more or less regular thing of the traffic in human flesh. In
1770, Samuel Hopkins wrote in his reminiscences that “Rhode
Island has been more deeply interested in the slave trade, and
has enslaved more Africans than any other colony in New Eng-
land.” Later, Mr. Hopkins amplified his words to point specifically
to Newport, whose “trade in human species has been the first wheel
of commerce . . . on which every other movement in business
has depended.”

Why should it have been Newport that led in the development of
a grisly business? It will not do to accuse the early Newporters of
being less sensitive than other people of their time; they were not.
Newport moved into the slave-running vacuum for reasons that can.
