212 . THE ENTERPRISING AMERICANS

alloy, Ford had the basic material for critical stress parts that he
needed for the lightweight Model T. During the 1908-09 season
the Ford company continued to produce other models. But the
Model T, as Ford himself put it, soon “swept them all out.” Origi-
nally priced at $825 for the roadster and $850 for the touring car,
it began an astonishing eighteen-year career with prices on some
models eventually lowered to as little as $260. Production increased
throughout World War I, even in the years after America’s entry
when Ford turned to the output of Eagle Boat subchaser and other
war material. Late in 1918, Bernard Baruch, then head of the War
Industries Board, threatened to cut Ford and other automobile
makers off from their supply of automobile steel. But the war was
over before the threat materialized and the Model T rolled on to
make Ford a billionaire.

While the key to Ford’s success was his ability to ferret out the
mass market, other things also contributed. At the very beginning
he and Couzens decided not to pay royalties on the famous Selden
patent under which U.S. internal-combustion car engines were origi-
nally made—a decision upheld after a long court fight. For the rest
Ford, never himself much more than a cut-and-try mechanic, bor-
rowed heavily from others. The principles of mass production for
which Ford has been given so much credit were well known before
the Model T got started. The Pope Manufacturing Co. of Hartford
had organized the production of bicycles and electric cars by as-
signing individuals to single repetitive tasks; Henry Leland of Cadil-
lac had pioneered in the matter of interchangeable parts; and Olds
and Durant were both ahead of Ford in making lavish use of sup-
pliers. It was Walter E. Flanders, a hard-living, hard-drinking
genius, who started Ford on the way to the modern assembly line
by rearranging the machines at the Piquette Avenue plant in 1908.
The moving production line, which was introduced at the High-
land Park factory in 19 13-14, and which was later carried to in-
credible pitches of assembly and subassembly refinement, was the
work of many men including C. W. Avery, William Klann, Carl
Erode, Charles E. Sorensen, and William Knudsen. Ford design,
always more utilitarian than aesthetic, owed as much to the metal-
lurgist C. Harold Wills as to Ford himself. In economic matters
Ford’s decision to raise the minimum wage to $5 per day has been
