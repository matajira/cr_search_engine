THE RISE OF THE MONEY POWER 179

Morgan & Co., there were the First National Bank of New York,
the New York Security & Trust Co., and Kidder, Peabody of Bos-
ton, And there were rich men and daring promoters by the score—
the Moore brothers of Chicago, William B. Leeds, Daniel G. Reid,
and many others. One by one they were herded into line by Judge
Gary, Morgan’s trusted agent who was persuaded to quit his Chi-
cago law practice and take over as chairman of U.S. Steel. “Bet-a-
Million” Gates tried to hold Morgan up for the stock of the
American Steel & Wire Co. but was finally prevailed upon to sur-
render his properties for a sum that was within reason.

If this had been all that there was to U.S. Steel, the fright caused
by its creation might not have been so pronounced. But the final
shiver of apprehension was provided when Rockefeller wealth
turned up as an integral part of the great combination.

John D. Rockefeller had picked up the rich Mesabi ore deposits
in the nineties when the Merritt brothers of Minnesota, lacking
capital for exploitation, had been unable to hold on to their great
discovery. When Gary suggested to Morgan that the Mesabi ore
must be made a part of U.S. Steel, Morgan demurred; he didn’t like
John D. Rockefeller, and wished to have no part in dealing with
the man. But U.S. Steel without Mesabi would have been extremely “
vulnerable. Gary’s calm logic persuaded Morgan to swallow his
prejudices and a price was extracted from Rockefeller. It was more
than Gary had originally been prepared to pay, but Morgan, who
always refused to haggle, accepted it without blinking. Possession
of the Mesabi gave U.S. Steel a source of ore that was to last through
two world wars—and it put “Rockefeller influence,” representing
Rockefeller stock, on the U.S. Steel board. Thus the two great titans
—Morgan and Rockefeller—were united not only in the field of
railroad domination but in the steel trust that would presumably be
virtually the whole source of metal for railroads and all other big
industry.

The spectacle of U.S. Steel and the Northern Securities Co.,
which were created at virtually the same time, was too much for a
country that looked back to small-scale business with an acute nos-
talgia. And when the muckraking journalists began to issue forth
in full cry in the years after 1903, the alarm about a prospective
“benevolent feudalism” (socialist W. J. Ghent’s ironic phrase for it)
