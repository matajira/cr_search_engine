220. THE ENTERPRISING AMERICANS

the market, it has actually worked in a profound sense to save
America for the little man. Not for nothing has Christy Berth called
the automobile the “power-plant and transportation tool of a free
people.” The sixty million cars that are now traveling the American
roads mean that no one is chained to any single way of life. And
the great automotive “oligopoly” (meaning “few companies to
sell” ) has even served to “de-oligopolize” other sectors of the econ-
omy that were once considered lost to monopoly. For example, the
voracious demands of the automobile manufacturers for varieties
of steel—-162 separate kinds went into a single automobile through-
out the nineteen fifties-have helped to decentralize the steel indus-
try by making it possible for specialty companies to exist and grow.
The automobile supports scores of tool-makers not only in the De-
troit area but in Rockford, Hlinois, and in Cincinnati, Ohio. Almost
every car has its radio, an immense boon to the electronics manu-
facturers. And even though the big auto companies like to own
their own parts supply divisions, independent parts suppliers still
account for hundreds of the separate 15,000 bits and pieces that
go to make up a modern automobile.

For better or worse, the whole American landscape has been
changed by the car, and not merely because of the popularity of
outdoor advertising. One out of every six or seven Americans em-
ployed depends on the automobile or its passengers, and most of
these people perforce have to be situated by the great American
road. The motel business has had a phenomenal growth; so have
moving picture drive-ins; so, despite the motels, has the resort hotel
business. The growth of suburbia, made possible by the car, created
the typical figure of George F. Babbitt, the gregarious and wistfully
appealing “realtor” of Sinclair Lewis’ Zenith. At a recent count
more than half a million gasoline service stations lined the Ameri-
can highways or the street corners in American suburbs and towns.
Because of the car-enforced development of the suburban shop-
ping center, Sears, Roebuck, which once depended on selling by
the mail order catalogue, has had to build itself anew around the
big regional store specializing in appliances, hardware, paint, and
tires.

The low-priced car and truck, which brought the farmer much
closer to his customers and enabled him to market his foods while
