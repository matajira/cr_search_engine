THE GILDED AGE- 147

until the time of its dissolution into a number of component com-
panies by the Supreme Court in 1911. Just why the first industrial
giant should have been so hated is a mystery if the question is
tackled from the standpoint of the consumer. Buyers always liked
the company’s product—they proved it by rushing to substitute
petroleum kerosene for the old coal-oil and whale-oil illuminants.
And buyers did not have any particular reason to complain of
Standard’s pricing policy: not only did kerosene cost less than the
older fluids, but it had to meet the competition of the Welsbach
gas burner and Mr. Edison’s carbon-filament electric-light bulb.
Standard Oil could not have imposed a lighting monopoly even if
it had tried.

If the consumer had no real quarrel with Standard Oil, however,
other producers had. Standard offended a nation’s traditional com-
petitive ethics—and the company made itself thoroughly hated
by people who never bothered to square their liking for the products
of mass production with the fact of big enterprise itself. In his main
objective—which was to achieve what the economists were soon
to be calling “economies of scale*—the youthful John D. Rocke-
feller was right. But his very zealotry provoked fear: a people
who were used to small regional businesses could not understand
Rockefeller’s passion for nipping off all the buds on the rosebush
of oil so that his own American Beauty Rose, the Standard, could
grow to absolute perfection. Besides the zealotry, which seemed
inhuman, there was the Rockefeller secrecy. In pursuing his ob-
jective, with muffled footfalls and sudden blows in the dark, the
silent little man from Cleveland seemed to have come out of the
Sicily that spawned the Mafia.

Curiously, the “monster” who provoked such spasms of fear
was, in actuality, a very simple person. As the son of old Bill
Rockefeller, a genial cancer quack from upstate New York, John
D. was bent on becoming what his father was not. He lived simply
(he never owned a yacht or a race horse), he followed his mother
in reading the Bible, he was generous with his money (notably to
the Baptist University of Chicago) even before the pioneer public-
relations counselor Ivy Lee convinced him that he should be
ostentatious about his benefactions, his family life would have
been worthy of emulation anywhere, and wherever he could he
