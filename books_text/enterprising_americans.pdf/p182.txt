THE GILDED AGE - = 157

erator on his own account, he ingratiated himself with Tom Scott,
the superintendent of the Pennsylvania Railroad’s new Pittsburgh
division, by giving special attention to his messages. Scott liked
the “little white-haired Scotch devil” and put him on the railroad
payroll as his private operator at $35 a month. When a railroad
accident tied up traffic one day in Scott’s absence, Andy broke the
tules all over again and got things running by issuing “train orders”
in his boss’s name.

Instead of firing Andy as he should have for an incredible breach
of railroad discipline, Scott was entranced by the boy’s nerve. He
began to throw investment opportunities Carnegie’s way. Borrowing
money from a bank, Carnegie took a one-eighth share in the
Woodruff Sleeping Car Co.—and, fortuitously or not, Woodruff
was soon building more and more sleepers for the Pennsylvania.
During the Civil War the wide-awake Andy helped Scott co-
ordinate the railroad and telegraph services of the War Depart-
ment. He kept his eyes and ears open for investment opportunities
so well that, in 1863, he was able to record an annual income of
$47,860, with only $2,400 coming from his railroad salary. The
biggest chunk of dividends—$17,868—came from the Columbia
Oil Company, into which he had bought with his sleeping-car
profits.

Carnegie’s memorandum of income for 1863 mentions $4,250
through “T.M.C. from Kloman,” and $7,500 from J. L. Piper and
Aaron Shiffler, manufacturers of iron railway bridges. “Kloman”
was, of course, the maker of an excellent railroad-car axle, and the
“T, M. C.” who is so cryptically mentioned was Thomas M. Car-
negie, Andy’s brother. Andy had joined the Kloman and the Piper
and Shiffler iron companies as a sleeping partner, presumably in
order to keep his dual connection with the Pennsylvania and two
of its suppliers from becoming the subject of gossip.

Carnegie first took an active part in the Kloman axle company
by accident, when he tried to mediate a quarrel between the sus-
picious Andrew Kioman and a partner. This air of casual adventi-
tiousness seemed to set the tone for Carnegie’s connection with the
iron-and-steel business for years to come. Yet underneath the
apparent negligence there was a definite pattem to everything the
young capitalist did after he finally quit the Pennsylvania Railroad
