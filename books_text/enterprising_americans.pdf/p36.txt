FREE ENTERPRISES BEFORE THE REVOLUTION - 11

his trade under the stiffened imperial attitude, he and his fellow
shipowners of Salem haughtily replied that “they were His Majesty’s
Vice Admirals . .. and would do that which seemed good to
them.” And when the British marched on Salem in February of
1775 to seize some stored cannon some two months before Paul
Revere’s ride and the Battle of Lexington, it was Captain Richard
Derby who refused the British the use of a drawbridge into town.
After all, he owned most of the cannon personally. “Find the can-
non if you can,” he roared across a stream. “Take them if you can.
They will never be surrendered.”

Down the coast, in Rhode Island, the Browns of Providence, a
family dynasty like the Hancocks of Boston and the Derbys of
Salem, had been doing business as merchants for a half-century
before the Revolution. Captain James Brown ran a general mer-
chandise shop, operated rum distilleries, owned a slaughterhouse
and ships, acted as a banker, and traded with South Carolina,
where he bartered for rice with Rhode Island cattle. His younger
brother, Obadiah, who took over the business when James died in
1739, built a mill to grind “chocklit.” Obadiah owned an early
business textbook, A Guide to Book Keepers According to the
Italian Manner, and presumably inaugurated the art of double-
entry bookkeeping in America. It was Obadiah who undertook to
rear James Brown’s sons—and the quartet of “Nicky, Josey, John,
and Mosey” Brown was to dominate Rhode Island trade and com-
merce for a long generation.

In the 1760's, Nicholas Brown & Co. (the business partnership
of the four brothers) is the leading candle-maker of the Colonies,
selling its product in New York and Philadelphia and as far away
as the Caribbean under the Brown copperplate label. At one point
the Browns try with other candle-makers to establish price control
through the “United Company of Spermaceti Chandlers,” one of
the earliest American “trusts.” The United Company set prices on
“head matter” from sperm whales and maintained the selling price
of completed candles. Within two years the “trust” collapsed, which
was only poetic justice. The Browns themselves hated the British
monopolies; it was brother John who led the seizure and burning
of the British schooner Gaspee when it pursued smugglers into shoal
water once too often.
