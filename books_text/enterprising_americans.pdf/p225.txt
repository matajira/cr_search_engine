200. THE ENTERPRISING AMERICANS

to satisfy his humanitarian impulses, Conrad had started playing
phonograph records over his radio to cheer sick radio amateurs in
hospitals. When a Pittsburgh store started selling crudely made
receivers to pick up the programs, Conrad proposed that Westing-
house go into the business of manufacturing receiving sets. West-
inghouse thought well of the idea—and, to make a market for its
product, it set up a broadcasting system, later called KDKA,
which became famous by broadcasting the Harding-Cox election
returns in 1920,

This success of KDKA whetted the appetite of the giants. In
1922, big A.T.&T. beamed the first sponsored broadcast from
its own station WEAF in New York. Four years later it sold its
stations and made available its studio-to-station telephone lines to
the R.C.A. group, whose general manager, David Sarnoff, formed
the National Broadcasting Co. to operate the network. When
R.C.A. bought Victor Talking Machine, which made a high-grade
combination radio-phonograph, it looked like the permanent
monopolist of the U.S. radio industry. This development was
checked, however, when William S. Paley, a Philadelphia cigar
maker, financed the Columbia Broadcasting System, Inc. Soon
other companies came into existence in the radio field. Sets were
made by Zenith, Philco, and others—and, with the Federal Com-
munications Commission parceling out wave lengths, small re-
gional broadcasting networks began to share the air as affiliates of
the bigger systems. In the forties television broadcasting was added
to radio, but without any drastic realignment among the companies
that had grown out of the seed planted by Edison on the day he
noticed the puzzling “Edison effect.”

The total effect of Thomas A. Edison, of course, went far beyond
these new communication media. For moder electronics, the junior
offshoot of electricity, has made possible such things as frequency
modulation and the transistor; the great computers manufactured
by Remington Rand and LB.M.; and all sorts of control devices
that affect the modem home and factory and have projected man
into the age of space. Edison himself was not to see these wonders,
and the millions he made in his later years through a family com-
pany, Thomas A. Edison, Inc., came chiefly from his phonograph
and dictating-machine manufacture and his movie patents. His last
