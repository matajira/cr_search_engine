Bibliography

Note: The material used in the preparation of this book ranges widely—as

the story of American business itself does-over the economic, social, and

political life of the United States, The following list of sources and recom-
mended reading includes, therefore, the work of historians, biographers, and
statisticians, company reports and autobiographies, and even the evidence
presented by some novelists, Some works, however, are so broad in scope

and contain so much basic material that is indispensable to the study of the
development of a business civilization (and were so continuously consulted

and referred to) that they are listed separately—ander Z. General—at the

start of this bibliography, followed by a list of the other sections to which
certain books particularly apply. Other sources have been grouped under

eight broad subject and period headings. Generally these headings do not

refer to specific chapters but follow their chronological sequence.

I, GENERAL

Documentary GUIDES am Encrctoprpras : Adams, J. T., and Coleman,
R. V. (eds.) ; Dictionary of American History (5 vols., 1940; secs, II through
VIII); Bogart, E. L., and Thompson, C. M. (eds.): Readings in the Eco-
nomic History of the United States (1916; Sees. Il through VII); Daniells,
Lorna M.; Studies in Enterprise ( 1957; sees. I through 1X); Flugel, F., and
Faulkner, H. U. (eds. ): Readings in the Economic and Social History of the
United States 1773-1829 ( 1929; sees. IL, III, IV); Historical Statistics of the
United States 178%1957, Department of Commerce (sees. H through IX);
Johnson, Allen, and Malone, Dumas (eds.): Dictionary of American Biog-
raphy (11 vols., 1957-1 958; sees, I through Ix); Larson, Henrietta M.:
Guide to Business History ( 1948; sees, 111 through VIID; Paullin, C. O.:
A tlas of the Historical Geography of the United States ( 1932; secs. IL
through V); Seligman, E, R. A, (cd.); Encyclopedia of the Social Sciences
(25 vols., 1930-1935; sees. 11 through VID.

Historrs: Beard, C. A., and Beard, M. R.: The Rise of American Civiliza-
tion (4 vols., 1927, 1941, and 1942; sees, III through VID; Bining, Arthur
Cecik The Rise of American Economic Life (1955; sees. V through VII);
Bishop, J. Leander: A History of American Manufactures from 1608 to
1860 (2 vols., 1864); Clark, Victor S.: History of Manufactures in the
United States (3 vols., 1929; sees. I through VID); Cochran, Thomas C.,
and Miller, William: The Age of Enterprise ( 1942; sees. Il through VI);
Cochran, Thomas C.: Basic History of American Business ( 1959; sees. V,
VI, VIII ); Dewey, David Rich: Financial History of the United States

265
