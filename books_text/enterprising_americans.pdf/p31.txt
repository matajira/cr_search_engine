6 + THE ENTERPRISING AMERICANS

Breton Island, which guarded the approach to the Grand Banks
fishing grounds. After forty-nine days of siege the supposedly im-
pregnable fortress fell. To the colonists’ distress, Louisburg was
returned to the French after the war. Nevertheless, the colonists
got something out of the Louisburg expedition, for the British Par-
liament refunded the expenses of it to the New Englanders in gold
specie, to help put the local currencies on a sound basis for the
first time in half a century.

Pepperrell himself did extremely well out of the whole show: he
was the first native-born New Englander to become a baronet. The
baronetcy tied him to the British Crown and made him a magnifico
beyond local compare. In 1747 he built four ships of war for Eng-
land. At the age of fifty-one, at the height of his resplendence, he
is not only a chief justice and a commander of militia; he is also a
president of the governor’s council, a colonel in the regular British
Army, a superintendent and accountant of the recruiting service,
a commissioner of Indian affairs (he has had a long experience in
patrol duty as a boy during the Indian wars), he is an owner of
sawmills, and he is still extensively engaged in the fisheries. He is
fond of gay plumage, appearing at his log landings along the Saco
River in bright scarlet. During this period he lives in a style befitting
his baronetcy; his house, as Hunt’s Lives of American Merchants
tells us, has “walls hung with costly mirrors and paintings, his side-
boards loaded with silver, his cellar filled with choice wines, his
park stocked with deer, a retinue of servants, costly equipage, and
a splendid barge with a black crew dressed in uniform.” His por-
trait is painted by John Smibert, who preceded Copley as the fash-
ionable portraitist of the colonial merchant aristocracy. The town
of Saco becomes Pepperrellboro-and between the Piscataqua and
Saco rivers, a distance of some thirty miles, Sir William can travel
through Maine entirely on his own soil.

While Sir William was expanding his Maine dynasty (which
petered out with the Revolution when Sir William’s Loyalist grand-
son fled to England) other and better known New Englanders were
extracting wealth from the sea. An effigy of the “sacred cod” hangs
suspended in the Massachusetts House of Representatives to this
day for a very good reason: as early as the 1740’s, a Salem or Bos-
ton ship carrying a load of cod from the Newfoundland banks to
