18 . THE ENTERPRISING AMERICANS

makers. The boldest trader in the Huguenot community was Henry
Laurens, the son of a saddlery merchant. As a go-between who was
prepared to perform any service for a pyramiding economy, Laurens
found he could double his capital much faster than by raising crops
on land he himself owned. As a wholesale commission merchant,
factor, and independent trader, Laurens dealt in rum, sugar, Ma-
deira wine, coffee from Guadaloupe, indigo, slaves, indentured
servants, and such odd items as marble mantels. Sometimes he
sailed his own ships; sometimes he took pieces of cargo in ships
owned by others. As a banker for an economy that depended on
notes of hand and bills of exchange, Laurens frequently took his
pay from planters by accepting liens on next year’s crops of rice
and indigo, sometimes risking as much as £1 0,000 on future
plantings.

At the age of forty, Laurens bought a Santee River plantation
and settled down to raise rice and indigo; he acquired more rice
lands on the Georgia coast; he created a 3,000-acre estate at Mep-
kin, on the Cooper River about thirty miles from Charles Town—
and, when King George III’s Townshend Act duties eventually bore
down heavily on Charles Town importers, he tongue-lashed the
colonial merchants from his retirement into a more zealous en-
dorsement of nonimportation of English goods. When the Revolu-
tion came, he acted as president of the Continental Congress. He
was captured by the British in 1780 (he was then fifty-six years
old), and returned as an exchange prisoner at the end of the
hostilities.

In contrast to Carolina rice, indigo, and naval stores, the tobacco
of Virginia, Maryland, and the Albemarle country of North Caro-
lina never received special consideration from the home country,
King James I wrote an angry screed against it; and under later kings
it was saddled with high import duties, which necessarily limited
its market even though duty was remitted on the proportion of the
crop that was re-exported from Britain to the European continent.

In addition to these man-made political drawbacks, tobacco
quickly exhausted the soil on which it was grown; the plots had
to be abandoned after four or five years, finding new use mainly
as “school lands.” When the price of slaves went up (owing to
competition for them in the rice and indigo country), the Virginians
