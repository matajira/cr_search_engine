86 . THE ENTERPRISING AMERICANS

Massachusetts, New Jersey, and Maryland, the good close-to-sea-
board timber was petering out. But to the north, in Maine (the only
place where the American pioneer moved east ), there were mil-
lions of acres of virgin white pine. The riches on the upper stretches
of the Penobscot River attracted the eager attention of William
Bingham of Philadelphia, who snapped up two million acres of
Maine timberland at a cut-rate price of 12.5 cents an acre. Stewart
Hoibrook, the historian of the lumber industry, reports that loggers
hacked for a century at the Bingham purchase without exhausting
it. The land rush in Maine centered on Bangor, which, in the
1830's, became the first of our sawmill boom towns. With saloons
on every corner to keep the men from the woods happy at the end
of a log drive, Bangor was an anomaly in God-fearing New Eng-
land. But it became the prototype of a hundred different lumber
towns, reaching all the way from Maine to Aberdeen, Washington.

Moving west with the various migrations of the timber kings,
Maine men helped staff the logging crews that cut wide swathes
through the lake states of Michigan, Wisconsin, and Minnesota and
made the leap to the early-twentieth-century empire of Douglas
fir in the far Northwest. New men came to the business from time
to time—the Shevlins and the Weyerhaeuser of Minnesota being
notable examples—but the down-Easters, whether they were
Canucks, Swedes, Irish, or Yankees, pioneered all the more difficult
arts involved in riding the white-water rivers and breaking the log
jams. It was a harum-scarum life in which death was the normal
penalty for a single slip-and the business of supplying release (in
women, dance halls, and liquor) for lonely loggers who had just
come in after a winter in the woods was in itself the source of many
a sawmill-town fortune.

To lighten the danger of riding the rivers in the days before the
Civil War, a Maine blacksmith named Joseph Peavey invented the
lumberman’s cant hook, which still bears his name. Character-
istically, Joe Peavey disclosed the nature of his invention to a fellow
blacksmith during an evening session over a bottle of Medford
rum—and soon discovered that someone had stolen his secret and
beaten him to Washington with a patent application. Such a minor
disillusion couldn’t keep the Peavey family out of lumber; at the end
of the nineteenth century an Ira Peavey bossed the construction of
