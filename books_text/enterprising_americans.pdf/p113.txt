88 . THE ENTERPRISING AMERICANS

whacking” oxen, or by skidding machines attached to aerial pulleys
set high in commanding trees, or by sleds run by donkey engines.
Oddly enough, the woodsmen out in the bush, who were quite used
to watching the circular saw chew up timber in the mills and who
employed two-men Disston crosscuts themselves to cut fallen trunks
into pieces, went on using the trusty ax to hew standing timber
until the 18 80's. It seemed against nature to use a crosscut saw
horizontally. For a full half-century some 40,000 axes were needed
at any given moment in America to keep the white pine falling
and these axes ordinarily had to be replaced after a month of
repeated sharpening. (The woodsmen actually kept the blades
honed to the point where they could shave with them on Sundays. )

So, by the law of reciprocity that linked the frontier with the
mechanical East in that pre-Civil War period, a little company in
Connecticut—the Collins Co. of Collinsville on the Farmington
River, which still makes machetes for use on banana and coffee
plantations in Central America—grew fat in supplying axes to
voracious loggers all the way from Maine to Minnesota. The com-
pany was started by Samuel and David Collins, two young brothers
who, as storekeepers in Hartford, Connecticut, had been selling
British-imported steel to blacksmiths who forged it into ax blades.
In an inspired moment David Collins decided that country smithies
could not provide enough axes to keep a good store going, either
as a supplier of steel or a retailer of finished ware. Accordingly,
the Collinses bought an old gristmill on the Farmington and rigged
up some machinery to blow air to the forges and to turn the grind-
stones. This was in 1826, just before the big Maine lumber boom
got under way. Two years after the Collinses had turned out their
first sharp axes, they were using triphammers to pound the metal
into shape. Soon, with the use of huge grindstones from Nova
Scotia, they were producing ten axes a day—which was mass pro-
duction by the standards of those times.

But ten axes a day, or three thousand a year, were not enough
to keep the sawmills of Bangor fed with logs, to say nothing of the
mills of Williamsport, Pennsylvania, and Saginaw and Bay City,
Michigan. The practical physics and chemistry of providing early
tool steel that was neither too hard nor too soft resisted short cuts,
and the processes never would have been speeded up to the point
