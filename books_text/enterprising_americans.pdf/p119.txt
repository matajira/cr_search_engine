94 . THE ENTERPRISING AMERICANS

the factories. The, British came, saw, were conquered—and when
they sailed for home they left an order for 150 machine tools.

Behind the new breed of frontier plainsmen came the farmers,
sometimes before the railroads, sometimes after the extension of a
tailhead into “nowhere.” Used to small clearings, the westward-
faring husbandmen were often ill equipped both mentally and
physically to handle the huge acreage of tough and sticky sods that
covered the prairie states, where grass had been growing since the
Ice Age. It was not that plows had not already been invented: it
was now quite a long time since the New Jersey farmers of Bur-
lington County had rejected neighbor Charles Newbold’s cast-iron
plow (patented in 1797) out of fear that it would “poison the soil.”
Thomas Jefferson had worked out the mathematical dimensions of
the moldboard plow, but this protean man was so interested in a
thousand different things (he also invented the swivel chair and he
made what may have been the first American dumb-waiter) that
he never got around to manufacturing it. After the War of 1812
Jethro Wood of New York State devised and patented a cast-iron
plow that came in three parts, any one of which could be replaced
without forcing the farmer to buy a whole new instrument. The
Wood plow worked in eastern soils—but cast iron could not be
brought to the proper pitch of strength and polish to break and
handle the matted prairie grass roots and clinging loams of the
West.

Enter, at this time, John Deere, a Vermont-born blacksmith who
moved from Grand Detour, Illinois, to Moline in the 1840's, and
whose name still graces one of the great agricultural-machinery
companies. Deere had noticed that plows improvised by John Lane,
a Chicago blacksmith, out of steel saws could cut and turn the
toughest earth. (Lane had made the first American steel plow in
Rockport, Illinois, as early as 1833.) As scientifically inclined as
Thomas Jefferson, Deere worked on the problem of the best mold-
board curve for the soils of Illinois and Iowa, and as fast as the
problem of getting good steels could be solved, his manufacturing
business grew. Later came “soft-center” plows, devised by John
Lane’s son, in which the ordinary brittle steels were supported by
more malleable metals; and in 1855, James Oliver of South Bend,
Indiana, came up with a method for chilling the working surface
