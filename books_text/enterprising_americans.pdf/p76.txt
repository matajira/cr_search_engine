THE QUEST FOR CAPITAL . 51

in the West. James Rumsey, a Virginian, put a paddle-wheel
boat on the Potomac, and Oliver Evans of Delaware constructed
an amphibious monster called Orukter Amphiboles, which could
run by steam on both land and water and do duty as a dredge. But
it was Robert Fulton, as everyone knows, who got credit for
making the first commercially practicable steamboat with the
Clermont, which became an acceptable Hudson River packet some
years after the boats of Fitch, Rumsey, and Evans had been re-
tire

    

Courtesy the Historical Brown Brothers

Society of Pennzyivania

Oliver Evans Eli Whitney

In other inventions of Oliver Evans a prescient man might have
looked decades ahead to the beginnings of multiple drilling and the
Detroit assembly line. When he was twenty-three years old, in
what is now Wilmington, Delaware, Evans cut his leg with a
scythe and had to spend some time in bed. This was in 1777, in the
middle of a war. With nothing to do but think, he designed a
machine for making “cards,” as the instruments for combing out
cotton and wool fibers for spinning were called.

The blacksmiths in Evans’ neighborhood considered the card-
making machine a “useless gimcrack.” But despite the fact that he
failed to get his first invention patented, Evans persisted in his
