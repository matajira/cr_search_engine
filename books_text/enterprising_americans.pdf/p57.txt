32. THE ENTERPRISING AMERICANS

   

 

 

Courtesy Independence National Historical Park Collection
Robert Morris

tion, masterminding many of the deals with the West Indies. He
also continued to grow rich by carrying on his own private trans-
actions in tobacco and other commodities. Though Thomas Paine
attacked Morris for this kind of “conflict of interest,” Washington
as well as most other people was willing to overlook his habit of
mingling private and public business.

Toward the end of the war Morris was made Superintendent of
Finances. As such he gave America its first lessons in practical
banking when he founded the Bank of North America—-described
