THE AGE OF EDISON: 201

fling came in the field of botanical cross-breeding when, with financ-
ing from Ford Motor and Firestone Tire & Rubber, he con-
ducted thousands of experiments with the guayule plant and with
honeysuckle, milkweed, and goldenrod in an attempt to develop a
home-grown substitute for rubber from the East Indies. The de-
velopment of cheap synthetic rubber from coal and oil derivatives
rendered all these botanic experimentations commercially useless.
But this in no way dimmed the homage that was paid to Edison as
the folk hero of electricity when he died in 1931, two years after
the fiftieth anniversary of his invention of the light bulb.

So pervasive is the force that was unleashed by Faraday and then
applied by Edison that it is virtually impossible to imagine what
the world would be like today without it. For a time it even looked
as though electricity was to be the agency that would put the U.S.
on automobile wheels. The first taxicabs in America were electri-
cally propelled and small electric ranabouts were popular in the
early years of the century. When Thomas Edison tumed his atten-
tion to experiments designed to perfect a good rechargeable long-
life storage battery, it was with the idea of making a cheap electric
runabout good for long-distance travel. But Edison was beaten out
in this particular phase of his activity by his good friend Henry
Ford, who came up with a better means of cheap transportation. It
was Ford, using the gasoline-driven internal-combustion engine,
who completed the job of taking the U.S. out of the age of steam,
and gave the next decisive turn to American business.
