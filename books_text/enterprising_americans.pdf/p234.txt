F.O.B. DETROIT . 209

tively smooth towpath of the Erie Canal, where he was held up by
the mules. He managed to reach the door of the Waldorf-Astoria
in seven and one-half days, so dirty and disheveled that he was
shunted by a suspicious doorman to the service entrance. Not liking
the long-term prospects of a car that had to seek out canal towpaths,
copper man Smith insisted that Olds return to heavier and more
expensive makes. Because of their basic disagreement, Olds de-
parted from the company—which, however, would carry his fame
through the years in the name of a G.M. division. He also gave his
initials to the Reo Company, which in later years limited itself to
the manufacture of trucks.

Other tinkerers backed by shoestring financiers were rapidly to
appear and disappear on the American scene. The automobile
graveyard is dotted with the names of hundreds of cars—Altham,
Ajax, Crestmobile, Grant-Ferris, Lear, Mohawk, Niagara, Regas,
Waterloo, Wolverine, Yale, Zentmobile area few early casualties—
that were never much more than a gleam in a mechanic’s eye. But
Roy Chapin and Howard Earle Coffin, graduates of the original
Olds company, got the Hudson Motor Co. going with the help of
$60,000 provided by Detroit department-store owner J. L. Hudson.
John North Willys, a bicycle salesman, took the $80,000 worth of
debts that represented the Overland Co. of Indianapolis, shifted the
company’s production to Toledo-and was off winging on the basis
of the trust which dealers and parts-makers were willing to repose
in his new Willys-Overland model. The success of Willys, which
led to the parallel success of the Electric Autolite Co., quickly
turned Toledo from a sleepy town into a modem manufacturing
community. The Studebaker of South Bend, Indiana, convinced
that wagons were on the way out, made a few cars on their own and
acted as sales agent for a shoestring company called E-M-F, which
had made $1,600,000 on 8,312 automobiles during the first seven-
teen months of its existence. Emboldened by the earnings on
their stock interest in the E-M-F, the Studebaker company soon
took over its manufacture. Packard was started in Warren, Ohio,
by a cable manufacturer and was subsequently moved to Detroit
by Henry Joy, the son of a Michigan Central Railroad lawyer. With
capital provided by Detroiters, Joy made $1,300,000 on 1,188 ex-
pensive Packards in the single depression year of 1907.
