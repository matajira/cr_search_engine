THE GILDED AGE: 159

for an Iowa railroad, Carnegie left Coleman and brother Thomas
to dig up capital for their new enterprise as best they could; he,
Andy, would have nothing to do with it. Though the Bessemer
patent situation had long since been straightened out, and though
good Bessemer rails were already being made at Cambria and in
Chicago, Andy thought an investment in Bessemer converters
would be “pioneering.” And his rule, often stated, was that “pio-
neering don’t pay.”

But while Carnegie was wandering around Europe, two things
happened. First of all, he managed to dispose of $6 million of the
railroad bonds, which gave him a profit of $150,000 that he had
to invest somewhere. Second, he discovered that the British be-
lieved in the Bessemer rail; indeed, they delighted to point to one
particular piece of Bessemer track that had been doing business on
the Midland Railway at Derby for fifteen years and was still far
from needing replacement. Sailing quickly for home, Carnegie
offered to put all of his European commissions into Coleman’s and
Thomas Carnegie’s venture. This sum was more than enough to
give Andy belated command of the Edgar Thomson project. By
1878, when the Camegie enterprises were recapitalized, Carnegie
had 59 per cent of the stock in his own hands.

Another remote control coup of Carnegie’s was responsible for
bringing the greatest steel man of the age, Captain Bill Jones, into
the Edgar Thomson management. Loafing in New York, Carnegie
picked up some gossip about a labor dispute at Cambria which,
obscurely, seemed to involve the company’s operating bosses.
Knowing that Cambria had had a long experience with the Besse-
mer process, Carnegie hastened to Pittsburgh with the suggestion
that the heads of the Cambria departments be hired to run the
Edgar Thomson. This is what brought Jones into the Carnegie
organization. And along with Jones came a whole corps of trained
Bessemer men.

By such intelligent opportunism Camegie dominated his indus-
try even though he knew little of the technical details of steelmak-
ing. His chronic absence from Pittsburgh gave him all the more
opportunist y to sell Pittsburgh’s products; he was, as Burton Hendrick
has pointed out, “perhaps the greatest commercial traveler this
country has ever known.” When it was to his advantage to do so,
