BUSINESSMEN JOIN IN AN UNBUSINESSLIKE WAR . 35
and letting their apples rot in the orchards. The year 1786 might,
with pardonable exaggeration, be called the year in which no
business was done.

This is not to say that the American people starved in the post-
Revolutionary period, after all, 90 per cent of the population still
lived close to the soil. The farmer had his beans, his peas, and his
Indian corn; he let his lean pigs forage in the woods for acorns;
and when he added to his farm buildings he generally used wooden
dowels instead of costly nails to fasten his beams together. The
spinning wheel provided him with clothes, which he stained with
sumac or butternut dyes—and if his sheep were too poor to supply
much wool, such woolen yarn as was available could be mixed
with linen fibers from flax to provide the rough cloth known to
our ancestors as “linsey-woolsey.” People ate out of wooden trench-
ers shaped by the fireside during long winter evenings; homemade
moccasins were used for shoes—and when cash was needed for
such things as sewing needles or for salt, or to pay the land tax, the
farmer burned some wood and leached the ashes to make pot ashes
or potash, an article that usually commanded a good price.

But while life went on, the means of economic expansion were,
to say the least, limited. In the 1780’s the roads were still so
abominable that most travel was by water, though horses and
stages clopped and jolted at four or five miles an hour in good
weather over the sketchy highways from Portsmouth, New Hamp-
shire, to Baltimore. In the South, land travel was even more diffi-
cult. Though Kentucky and Tennessee were attracting settlers, a
general movement to the West must wait upon something better
than canoes, pack horses, and even broad-horn flatboats. There
was, too, the matter of the Indians, who were being egged on
against American settlers by British commanders who had not
yet departed from U.S. soil at Detroit and Niagara. As for would-be
manufacturers, if they had capital they couldn’t transport their
goods. And a people used to farming in the crudest conventional
ways hardly helped improve things by regarding the inventor
in quest of a mechanical short cut as an “indolent” and “God-less”
man.

The rundown condition of the country after the Revolution—
and the inadequacy of the Confederation to deal with it—was well
