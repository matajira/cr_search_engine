210 . THE ENTERPRISING AMERICANS

But it was the coming in 1908 of Henry Ford’s cheap and sturdy
Model T that made the car available to all classes and that finally
broke down the barrier between urban and rural America. The
Ford saga has been told many times and from many angles, with
Henry Ford himself providing a neat, rational explanation for every
major decision made by his company. That the reasons were, for
the most part, ex post facto, as is claimed by Ford’s man Charles
Sorensen, hardly makes any difference, for Ford, until he grew old
and cantankerous, was a brilliantly intuitive man. Like an inspired
somnambulist, he felt his way to his goals without knocking over
the tables and the crockery. As the son of a Dearborn, Michigan, .
farmer, he hated farm work, preferring to hang around the black-
smith shop. His self-assigned homework consisted of taking watches
apart and putting them together again-and when he was a young
runaway mechanic in Detroit he eked out his board money by re-
pairing watches at night. Called back from his first sojourn in the
big city to take up forty acres of woodland offered to him by his
father, he supported himself and his bride by setting up a portable
sawmill. The farming he saw around him depressed him as 90 per
cent waste motion; as he said later, “the worst factory in Europe is
hardly as bad as the average farm barn... . A farmer doing his
chores will walk up and down a rickety ladder a dozen times. He
will carry water for years instead of putting in a few lengths of
pipe.”

One of Ford’s most frequently asserted latter-day explanations
was that it had always been his “most constant ambition” to de-
velop the tractor power that would save the farmer from himself;
cars were a secondary consideration. In the late eighties he made
himself a steam car that ran, hoping to pull his own plow with it,
but was forced to discard it as unsafe. He also built himself a minia-
ture four-cycle gas engine, which he gave away. Moving to Detroit
a second time to take a job as engineer and machinist for the Edison
Illuminating Co., he angered his electricity-minded employer by
continuing to experiment with gas engines. Though he won some
fame as a racer with his own model, beating Alexander Winton at
the Blue Ribbon Race Track in Grosse Pointe in 1901, he was still
a seedy business failure at forty. A lumber dealer, William H. Mur-
phy, had backed him successively in the Detroit Automobile Co. in
