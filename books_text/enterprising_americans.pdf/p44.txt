FREEEN terprisers BEFORE THE REVOLUTION. 19
were in real trouble, though they eventually found it profitable to
breed slaves for sale to the owners of new cotton lands on the Gulf.
Freight costs, insurance, commissions, merchants’ profits, and the
interest on borrowed funds, all of which were dominated by Eng-
lish companies, remained high for the tobacco growers—and with
Britain monopolizing the trade there was no way of seeking new
markets to meet shifts in the price.

Listen to Thomas Jefferson on the woes of the tobacco planter;
“It is a culture productive of infinite wretchedness.” (Jefferson was
not referring to what smoking or snuff-taking did to users of the
“sot weed.” ) Continuing his damnation, Jefferson said: “Those
employed in it are in a continual state of exertion beyond the
powers of nature to support. Little food of any kind is raised by
them; so that the men and animals on these farms are illy fed, and
the earth is rapidly impoverished.” As for the dependence of Vir-
ginia tobacco farmers on English merchants and Scottish factors,
which kept Virginia in hock to the suppliers of overseas credit, Jef-
ferson was equally contemptuous. “These debts,” he remarked, “had
become hereditary from father to son, for many generations, so
that the planters were a species of property, annexed to certain mer-
cantile houses in London.”

Yet if tobacco culture earned such opprobrium, it produced tine
houses as well as shanties with clay-lined chimneys, able men as
well as spendthrifts. The earlier eighteenth-century planters lived
well, if precariously, on the proceeds of their wasteful staple crop;
and throughout Virginia “tobacco notes”—or warehouse receipts
validated by inspectors—passed as money. This money, of course,
fluctuated in value with the state of the crops and markets, but it
nevertheless was an accepted medium of exchange. Local Anglican.
clergymen, for instance, were regularly paid in tobacco notes; and
when the Virginia state legislature temporarily substituted depre-
ciated paper currency they raised a loud protest.

Tobacco also bred leaders for the Revolutionary period, because
it took qualities of command to run a ‘plantation. Supervisors and
overseers had to be watched; slave and indentured-servant labor
had to be stimulated to action. Cooperage for the hogsheads had
to be done at the plantation mills; the hogsheads themselves might
have to be rolled a mile or more to shipside. Since there were few
