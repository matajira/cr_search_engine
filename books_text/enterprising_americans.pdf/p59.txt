34. THE ENTERPRISING AMERICANS

had taken an enormous economic toll. Despite much privateering,
external trade had been completely disrupted, and in Nantucket
grass had grown in the streets as the whaling vessels rotted at the
piers. Privateering profits, save with the Derbys of Salem, Girard
of Philadelphia, and a few others, disappeared as the British Royal
Navy swept the small, inadequately armed American vessels from.
the seas. The New England fisheries needed new ships before
the “sacred cod” could move again in commerce—and then there
would be the troublesome matter of finding new markets for
fish and lumber to replace British Caribbean island ports that had
been placed out of bounds by King George’s Parliament. As for
the southern states, which might have paid for imports with
shipments of tobacco, rice, and indigo, their plantations had either
deteriorated during the war or been overrun and devastated by
the British. The fields could not be revived overnight.

Meanwhile internal domestic commerce was falling to pieces
for want of a strong central authority. Every state tried to get
the drop on its neighbors—for example, New Jersey, “a cask
tapped at both ends,” found itself squeezed between the trade
discriminations practiced at its expense by Pennsylvania and New
York. Connecticut levied duties on imports from Massachusetts
and, in turn, discovered that New York had no intention of letting
firewood from Stamford and New Haven move through Hell Gate
into the East River free. New Yorkers and New Hampshiremen
started quarreling over the rights to Vermont maple-sugar-bush
country. Even state boundaries were matters of dispute. Connecti-
cut, for instance, claimed that certain land between the Susque-
hama and Delaware rivers had been granted to it by the British
Crown and tried to enforce this claim against an outraged Penn-
sylvania,

The whole chaos was compounded by a spotty paper-money
inflation in every state save Connecticut (“the land of steady
habits”) and Delaware. The inflation was kept within reasonable
limits in the middle states, but Rhode Island, an old offender in
the matter of paper money, went hog-wild. When the merchants of
Newport and Providence closed their stores in 1786 rather than
sell goods to Rhode Island farmers for worthless paper, the farmers
retaliated by burning their com, pouring their milk on the ground,
