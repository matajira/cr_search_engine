THE AGE OF EDISON: 199

The development of radiobroadcasting and modem television
owes an indirect debt to Edison’s constant tinkering. Back in the
early eighties the inventor noticed the leakage of current across a
gap in his vacuum tube, and following this lead he developed the
“Edison effect” lamp, a device that made use of the mysterious
impulses (not yet called electronic ) to rectify voltages. Thus he un-
wittingly became “the father of modem electronics.” The Edison-
effect lamp was studied in London by Sir John Ambrose Fleming in
experiments that resulted in the so-called “Fleming valve,” which
the Italian inventor Guglielmo Marconi was eventually to use as a
tadio-wave detector. Though it was not perfectly understood for a
long time, the new science of electronics set other investigators to
work. In America, Lee De Forest turned the Edison effect and
Fleming’s valve to good advantage in making his audion tube, which
was to bring commercial radio within reach. Taking off from the
work of his predecessors, Irving Langmuir of General Electric per-
fected a high-vacuum tube that greatly improved the whole business
of transmission and made good long-distance broadcasting possible.
And, to cap the dazzling succession of electronic inventions, Major
Edwin Howard Armstrong designed his amplifying regenerative
circuit, which did away with earphones and so made household
reception easy and pleasant.

The early electronics business, like electrical manufacturing, was
beset by the incessant quarrels of litigious inventors. Order was
eventually brought out of chaos by the old method of mergers and
by the formation of patent pools to permit cross-licensing of neces-
sary devices. Oddly, the U.S. government itself took the lead in
promoting a radio trust, thinking that a big all-inclusive company
was desirable to keep the British-owned Marconi system from get-
ting control of the American airwaves. Called the Radio Corpora-
tion of America, it was set up in 1919 by Owen D. Young of the
General Electric Co. and was cross-licensed to use patents held by
G.E., by A.T. & T., and, at a later date, by Westinghouse Electric.

Radiobroadcasting was actually first undertaken by KDKA in
Pittsburgh, a station that grew out of the after-hour “radio ham”
avocations of Westinghouse Electric’s Dr. Frank Conrad. In his
private laboratory over a garage, Conrad had set up a wireless to
get Arlington time signals from Washington. To amuse himself and
