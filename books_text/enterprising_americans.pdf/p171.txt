146 . THE ENTERPRISING AMERICANS

For decades after the Civil War the railroads were to remain the
greatest business of the nation, and even as late as 1898, as Bernard
M. Baruch was to note, “something like 60 per cent of the securities
listed on the Big Board were of railroads.” (By 1914 they had
declined to less than 40 per cent of the Stock Exchange listings,
by 1925 to 17 per cent, and by 1957 to 13 per cent.) For better or
worse, the railroads became implicated with the pioneer giant

 

Courtesy the Library of Congress

Andrew Carnegie

corporations in other fields-with Western Union (for it was along
their rights-of-way that telegraph lines were strung), with Andrew
Carnegie’s successive steel companies (the Pennsylvania was such
a good customer of Carnegie’s that he named a big mill after the
tailroad’s J. Edgar Thomson), and with Rockefeller’s Standard
Oil through tank car manipulation and the notorious rebate system.

The first big “trust” was the Standard Oil Co., which grew so
fast and with such seeming disregard for popular criticism of its
tactics that it found itself a prime political target from the 1870's
