56. THE ENTERPRISING AMERICANS

being captured and detained by a British frigate in the first weeks
of the War of 1812, he joined with Patrick Jackson in chartering
a company to make cotton fabrics.

This was the beginning of the famous Boston Associates, a group
that came to include most of the Lowell clan and their connections
(Amorys, Cabots, Higginsons, Jacksons, Russells, Lees, and others
of the old trading aristocracy), as well as the new merchant tribe
of Lawrences, who were eventually to intermarry with the Loweils
to produce a Harvard president, an astronomer, and a cigar-smok-
ing free-verse poetess. But before the “clan” was willing to put
much money into the venture, Francis Cabot Lowell had to prove
to its cagey members that he was not mad. After raising $100,000
and buying an old paper mill at Waltham, Lowell put his mathe-
matical abilities to work to reproduce—and improve upon—the
spinning and weaving processes he had watched in England. Na-
thaniel Bowditch paid his own mathematician’s tribute to Lowell’s
intricate calculations for the so-called “double speeder,” which was
mechanically fleshed out at the Waltham mill by the brilliant Paul
Moody of Amesbury.

By 1814, at a time when all America was hungry for the cloth
it had imported from England before the period of embargoes, non-
intercourse, and war, Lowell and Patrick Jackson were ready to
ecard and spin thread and weave cloth all under one roof. Soon the
Waltham mill was turning out some thirty miles of cotton cloth
in a day and paying 10 to 20 per cent in dividends, and at this
point the members of the “clan” started buying shares with a mad-
ness all their own. After Francis Cabot Lowell’s early death, Jack-
son, who became the new soul of the enterprise, spent long hours
in the acquisition of Merrimack River mill sites as the need for
waterpower grew. Out of these sprang the cities of Lowell and
Lawrence, and when Slater, too, expanded his operations to include
cloth making (he built plants at the Amoskeag development in
Manchester, New Hampshire), the U.S. was soon deep in the first
textile phase of the industrial revolution.

The pay was at least relatively good at the beginning as the
Lowells and Patrick Jackson brought in farm girls to their company-
town structures on the Merrimack to work for dowry money; in-
deed, the English novelist, Anthony Trollope, spoke of Lowell as
