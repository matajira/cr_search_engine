58 . THE ENTERPRISING AMERICANS

enslaving U.S. sailormen. Accordingly, the old Liberty Boy put
aside his work of making silverware and bought an old powder mill
at Canton, not far from Boston on the Neponset River.

To get the proper rolls from England, Revere put up $25,000 of
his own savings plus $10,000 the federal government lent him on
a promise that he would resheathe the Constitution’s bottom.
Revere learned how to use his new rolls in the course of turning
out six thousand feet of sheathing for the dome of Bulfinch’s new
Massachusetts State House. Copper for the hull of “Old Ironsides”
followed in due course.

The venture so pleased the old patriot courier and silversmith
that he wrote a poem about it:

At early morn I take my round,

Invited first by hammer’s sound;

The Furnace next; then Roleing-Mill;

‘Till Breakfast’s call’d, my time cloth fill...
Not distant far from Taunton road

In Canton Dale is my abode. .. .

The italics are Revere’s own—and the exultation that runs through
the poem is a far cry from the dry irony of Revere’s own descrip-
tion of his famous ride on April 18, 1775. Revere never thought
his ride amounted to much (his own account of it, stressing his
stupidity, is utterly unlike Longfellow’s later poetic version) but
he was sure his copper “roleing mill” was a truly patriotic contribu-
tion. His assessment of the comparative value of the ride and the
ability of the Canton mill to provide sheet metal in large quantities
may be correct. Ride or no ride, the Minute Men would have as-
sembled anyway. But if it hadn’t been for Revere’s decision to
become America’s first big industrialist in metal, the U.S. Navy
would not have been ready to take on the British in the War of
1812.

Revere’s mill led the way in an industrial breakthrough. In Con-
necticut, other men contributed to this same development. Well
before the Revolution two Irishmen, William Pattison and his
brother Edward, had set up as manufacturers of tin kitchenware in
asmail shop in Berlin, a village a few miles south of Hartford.
