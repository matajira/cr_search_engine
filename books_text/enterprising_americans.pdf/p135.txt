110. THE ENTERPRISING AMERICANS

Singer, who in fact, along with a number of others, had taken over
the actual business of manufacturing sewing machines. More patent
wars developed among the manufacturing companies until finally
a patent pool was developed to settle the differences between some
of the rivals, which got together in what became known as “the
Combination.” From the Combination, Howe as well as Singer
reaped fortunes. Gradually I. M. Singer & Co. forged ahead, de-
veloping a fully matured consumer installment plan, a franchised
agency system, and a huge international trade. The Singer name
became a byword in Indian huts in the Andes, in jungle hovels in
the tropics, and in cabins by northern glaciers.

The lift the housewife got from the sewing machine was part and
parcel of a wider change. In Philadelphia, in 1837, Louis Antoine
Godey was already publishing Goedey’s Lady's Book, with the re-
markable Sarah Josepha Hale as the “Lady Editor.” The Lady Edi-
tor was annoyed by the menial position of pre-Civil War women
and proceeded to put the flattering term “domestic science” into
the language. As it turned out, “domestic science” was to make pro-
digious use of the sewing machine—and around the dress patterns
displayed by the women’s magazines mass-circulation media were
to grow to join the new penny press in the dissemination of infor-
mation. “Domestic science” also meant the tin can. In 1847 a
can-making machine had been invented—and later, with 240 cans
passing through a crimping machine in a minute, gold and silver
miners in California and Colorado and explorers in the Far North
shared garden peas or Maine lobster meat with the housewife all
year round. In 1856 Gail Borden devised the process for evaporat-
ing milk and putting it into a can—a foreshadowing of the day
when Elsie the Cow, one of Madison Avenue’s more engaging crea-
tions, would impress the principles of “domestic science” on her
skittish offspring.

Meanwhile, as early as the 1850’s, the sewing machine had been
used to do the stitching on shoe uppers. But in the Massachusetts
shoe towns—Lynn, Haverhill, Marblehead—and in Hartford and
Philadelphia, the business of matching soles and uppers was still
being farmed out to household workers. A successful shoe-pegging
machine had been invented in 1833, but hand sewers and peggers
threatened boycott and mayhem. Even after the prejudice against
