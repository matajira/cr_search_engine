BIBLIOGRAPHY - 269

Y. THE CIVIL WAR, THE COMING OF INDUSTRIALISM

Allen, Hugh: Rubber’s Home Town ( 1949); Appel, Joseph A.: The Business
Biography of John Wanamaker ( 1930); Belles, A. S.: Industrial History of
the United States ( 1887); Berth, Christy: Masters of Mass Production
( 1945); Berth, Christy: True Stee! ( 1941); Bruce, P. A.: The Rise of the
New South (1905); Bruce, Robert J.: 1877: Year of Violence (1959);
Burlingame, Roger: Engines of Democracy ( 1940); Burlingame, Roger:
March of the Iron Men (1938); Business History Review-Staff of eds: Oil's
First Century (1960); Crow, Carl: The Great American Customer (1943);
Dodd, William E.: The Cotton Kingdom ( 1920); Dorian, Max: The du
Ponts from Gunpowder to Nylon (1962); Eckenrode, H. J., and Edmonds,
P. W.: E. H, Harriman ( 1933); Erickson, Charlotte: American Industry and
the European Immigrant 1860-1885 (1957); Fite, Emerson D.: Social
and Industrial Conditions in the North During the Civil War ( 1910); Fritz,
John: 4 utobiography of John Fritz ( 1912); Giddens, Paul H.: The Birth
of the Oil Industry ( 1938); Girdler, Tom, in collaboration with Boyden
Sparkes: Bootstraps: The A utobiography of Tom M. Girdler (1943); Hacker,
Louis M., and Hendrick, Benjamin B.: The United States Since 1865 (1932) ;
Helper, Hinton Rowan: The Impending Crisis (1857); Hendrick, Burton J.:
Life of Andrew Carnegie (2 vols., 1932); Henry, Robert Selph: This Fasci-
nating Railroad Business (1942); Holbrook, Stewart H.: Holy Old Macki-
naw (1938); Holbrook, Stewart H.: iron Brew ( 1939); Holbrook, Stewart
H.: The Story of American Railroads (1947); Hough, Emerson: The Pass-
ing of the Frontier, A Chronicle of the Old West (Chronicles of America
series, 1920); Howells, William Dean: The Rise of Silas Lapham (1889);
Hoyt, Edwin P.: The Vanderbilt and Their Fortunes (1962); Jennings,
W. W.: Twenty Giants of American Business ( 1953); Keir, Malcolm: March
of Commerce (Pageant of America series, 1927); Kirkland, Edward C.:
Men, Cities and Transportation 1820-1900 (2 vols., 1948); Lane, Wheaton
J.: Commodore Vanderbilt ( 1942); Lewis, Lloyd, and Smith, Henry Justin:
Chicago. The History of Its Reputation ( 1929); Lewis, Oscar: The Big Four
(1938 ); Lewton, Frederick L.: A Brief History of the Sewing Machine
(1929); Longstreet, Stephen: A Century on Wheels: The Story of Stude-
baker ( 1952); Mahoney, Tom: The Great Merchants (1955); Minnigerode,
Meade: Certain Rich Men ( 1927); Morley, Felix: Freedom and Federalism
(1959); Nevins, Allan: Abram S. Hewitt with Some Account of Peter
Cooper (1935 ); Osborn, Norris G. (cd.): History of Connecticut (4 vols.,
1925 ); Redlich, Fritz: History of American Business Leaders, Vol. 1: Iron
and Steel ( 1940); Robbins, R. M.: Our Landed Heritage: the Public Do-
main 1776-1936 ( 1942); Thompson, Holland: The New South (Chronicles
of America series, 1919); Twain, Mark, and Warner, Charles Dudley: The
Gilded Age ( 1873); Wasson, Gordon: The Hall Carbine A ffair ( 1948);
Webb, Walter Prescott: The Great Plains (1931); Weeks, Lyman Horace:
History of Paper Manufacturing in the United States (1916); Wilkins, Thur-
man: Clarence King ( 1958); Wright, C. D.: Industrial Revolution in the
United States ( 1897).

See also general sources indicated for this section,
