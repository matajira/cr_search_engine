9 The Rise of the Money Power

Trustification sears the public.

“Jupiter” Morgan reorganizes the rails,

He collides with Gould and Harriman,

He pours Rockefeller’s Mesabi ore into U.S, Steel's farnaces.
He glares down the Pujo investigation.

Was the U.S. Steel Corporation was put together in 1901, the
impact on public opinion was swift and almost physically tangible.
Expressing the prevalent consensus of horror, President Arthur
Twining Hadley of Yale remarked that if public sentiment would
not regulate such monster businesses, there would be “an emperor
in Washington within twenty-five years.”

U.S. Steel was indeed a monster to a nation whose older citizens
still recalled the days when iron and the village blacksmith had
been almost synonymous terms. Capitalized at $1.5 billion, “the
Corporation” added to Andrew Carnegie’s ingot-producing mills
a formidable majority of the nation’s most important steel-fabricat-
ing companies. What whipped up public apprehension still more
was the fact that the financier J. P. Morgan had not only linked all
these properties together, but by a final coup had brought John
D. Rockefeller’s Mesabi ore fields, the richest in the world, into his
seemingly stock-watered combination. President Hadley had dis-
cerned his forthcoming American “emperor” at some distance

163
