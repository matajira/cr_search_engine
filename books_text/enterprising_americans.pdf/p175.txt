150 “ tHe ENTERPRISING AMERICANS

took off from a standing start and by 1869 was producing 4,800,000
barrels yearly. Meanwhile the price of crude oil zoomed and
plummeted crazily as the race between new wells and new oil
uses went first one way and then another.

Watching the boom-bust careers of well drillers as they wild
catted, the young John D. Rockefeller decided that the producing
end of oil was nothing in which a sane man should invest his funds.
His advice to the Cleveland businessmen was to keep out of the
oil country; refining was obviously a much safer thing. He himself
returned home to put a few thousand dollars into a small refinery
run by a man named Samuel Andrews, who seemed to have better
refining methods than others in the business. When the new firm
of Rockefeller & Andrews promised to be far more profitable
than the commission-merchant business, Rockefeller lost no time
in transferring his eggs to a single basket. The pious, orderly young
man quickly took on new partners, men who had capital and a
knowledge of refining; and with every enlargement of the business
the unit cost of producing and marketing a gallon of kerosene
was decreased.

Under its various names (it had become Rockefeller, Andrews
& Flagler by 1868), the Rockefeller company pushed its integration.
forward, backward, and sidewise, making its own barrels in its
own cooperage plants, shipping its products in large quantities,
and plowing most of the profits back into the business. By 1870
Rockefeller and his partners were doing about a fifth of all the
refining in the Cleveland area. The partners celebrated their suc-
cess by organizing the Standard Oil Co. of Ohio, with Rockefeller
taking 2,667 shares of the new stock and Henry M. Flagler, Samuel
Andrews, S. D. Harkness, and brother William Rockefeller taking
1,333 shares each.

Standard of Ohio was advantageously placed to weather the fall
in prices that came with the seventies; and with the bankruptcy of
many marginal concerns it would in any event have achieved a
continually expanding share of the business of providing a growing
country with kerosene. “But this was not enough for Rockefeller,
who dreamed of bringing permanent stability to his appallingly
chaotic trade. He hated what he called the “idiotic, senseless destruc-
tion,“ “the wasteful conditions” of competition. By convincing his
