144 . THE ENTERPRISING AMERICANS

Taking some of the $20 million he had made on the rivers and
oceans, he quietly bought up the shares of the Harlem Railroad
running out of New York City and the Hudson River Railroad
leading north to East Albany. This gave him a right-of-way all
along the east bank of the Hudson, with terminal facilities right
in the heart of Manhattan Island. Later, using guile to depress
the shares of the New York Central, which ran between Albany
and Buffalo (the wily Commodore had publicly announced his
discovery of an ancient law on the statute books that made it
illegal to deliver his own Hudson River Railroad passengers to
the Central’s depot across a bridge at Albany), the old ex-ferryman
from Staten Island got control of the Central at a price he could
afford. Stock control of the Canada Southern, the Lake Shore,
and the Michigan Central eventually followed-and the first im-
portant integration of east-west systems was thereby accomplished.

A lusty and superstitious fellow, Vanderbilt took a thirty-year-
old second wife at the age of seventy-five, bickered with his children
(ten out of thirteen survived both his tempers and his death), sum-
moned up the ghost of the dead Jim Fisk to get advice on stock
manipulation (it must have been good, for when Vanderbilt him-
self died in 1877 he was worth some $100 million), and proposed
that a large monument should be reared in New York’s Central
Park to commemorate the two greatest Americans, George Wash-
ington and Cormelius Vanderbilt. But amid all his egotistical
cavorting the Commodore relaid the Central’s tracks from New
York to Chicago with new steel rails, built strong steel bridges,
threw away the picturesque pre-Civil War Iron Horses in favor of
a drabber but more efficient type of locomotive, and cut the time
of the New York-Chicago run from fifty hours to twenty-four.
Whether the Commodore’s son, William H. Vanderbilt, ever actually
said, “The public be damned!” is a point still disputed by historians.
But on one thing there can be no dispute: the public was served
by the new trains on the Commodore’s New York-Chicago tracks.
If Cornelius was a robber baron, the country needed more like
him. Old “Cornee!” may have watered the Central’s stock. But as
fast as he watered it he solidified it—and the worst that can be
said about him is that he was a shrewd capitalizer of future earnings.

Meanwhile, to the south of the territory covered by the ring-rid-
