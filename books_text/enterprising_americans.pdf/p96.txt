EARLY AMERICA GOES PLACES . 71

served him in slices to every Englishman present. But even though
bonds were repudiated the canals were a boon to businessmen gen-
erally. Prior to the canals it cost as much to haul a ton of goods
thirty miles by wagon as it did to transport it 3,000 miles by ship
across the ocean. Coal mines were useless to iron mines unless
they were right next to each other, a proximity normally denied by
nature. And at 30 cents a ton-mile, it cost more to haul wheat 200
miles into Philadelphia than it could be sold for in the city at local
market prices. The American canals changed all this. They in-
volved high initial capital costs—normally $20,000 to $30,000
per mile. But they brought coal and iron together, and knocked
the cost of hauling wheat down to 1 or 2 cents per ton-mile.

The most important function of the trans-Allegheny canals,
however, was to provide the vital link between the tidal rivers of
the East and the river-and-lake traffic of the surging new West.
Here the steamboat took over. Steamboats Come True is the way
the title of a book about the early inventors of steam-powered river
craft has put it—but they never really came true for John Fitch or
James Rumsey or William Henry, who couldn’t find sufficiently
tugged boilers to make their boats commercially successful. It re-
mained for two bitter competitors, Robert “Toot” Fulton of New
York and Colonel John Stevens of Hoboken, to prove that river-
boats could be sturdy enough to make money over the years.

Fulton, a painter, submarine experimenter, and man of the world,
owes his fame, of course, to the successful voyage of the Clermont
up the Hudson in 1807. In building the Clermont, Fulton cadged a
Boulton & Watt engine and boiler out of the British, who normally
refused to grant export licenses for such things. He also had the
good sense to reject steam-operated oars, ducks’ feet, and “endless
chains” with attached boards, in favor of Nicholas Roosevelt’s sug-
gestion that he use paddle wheels. Beyond this Fulton had the en-
terpriser’s instinct for cultivating backing that would be effective
in making his enterprise successful. He was a close friend of Chan-
cellor Robert R. Livingston, of the politically powerful New York
State Livingstons, and eventually married one of Livingston’s cou-
sins. Livingston had seen the potential profits in steamboats and
before the turn of the century had actually obtained a state monop-
oly for operating them on New York waters. Some time before the
