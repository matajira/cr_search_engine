190 . THE ENTERPRISING AMERICANS

produced by movement against a diaphragm, why couldn’t it be
reproduced by playing recorded indentations back again via a
needle against a second diaphragm? Edison’s own deafness had
caused him to use a needle held between a diaphragm and his sensi-
tive finger so that he could judge the amplitude of vibrations when
he was tinkering with his telephonic devices. Thus his infirmity
sparked the line of investigation that led to “stored” voices. Edison
thought of his first phonograph device, consisting of embossed re-
cordings on tinfoil, as an aid to all sorts of dictation, or for making
phonographic “books” for blind people, or for various odds and
ends including the teaching of spelling and elocution, the preserva-
tion of the sayings of great men, and as an auxiliary in the trans-
mission of permanent records over the telephone. Almost casually
Edison mentioned that it might also be “liberally devoted to music.”

And then, having reaped a useful harvest of royalties by exhibit-
ing the machine as a scientific curiosity, Edison forgot his first
wholly original brainchild for ten years. A friend, Professor George
F. Barker, had urged Edison to look into the uses of electricity for
light, and when the inventor heard that Jablochkoff “candles,” a
Russian arc light, were being used to illuminate the Paris Exposition
of 1878, he was off on a new career, which would in three decades
transform the U.S. economy beyond recognition.

Edison was not the first American to use electricity for lighting:
that distinction goes to Charles F. Brush of Cleveland and William
Wallace and Moses G. Farmer of New England. These men had
used ring-wound dynamos to light arc lamps on Cleveland streets,
in John Wanamaker’s big department store in Philadelphia, and in
Naugatuck Valley brass mills. Brush had learned how to use
automatic shunt coils to by-pass burned-out lamps, which made
uninterrupted lighting from a central dynamo or generator a com-
mercially feasible thing. But arc lamps, which were much too
intense to be used in private homes, had a severely limited economic
future. Edison, after a visit to Wallace’s brass mill in Ansonia,
which used the Wallace-Farmer lighting equipment, saw that light-
ing “had not gone so far but that I had a chance.” The problem, as
he saw it in an intuitive flash, was to “subdivide” light so that it
could be brought into private houses. Few people in America
thought this was practical, for electricians as yet had only an imper-
