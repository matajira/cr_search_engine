THE QUEST FOR CAPITAL 61

through fire, Whitney came up with another idea that saved his
sanity and salved his pride. He had learned something about the
construction of machine tools in his efforts to produce cotton gins,
and had, presumably, watched the New Haven mint of Abel Buell
stamp out identical copper coins. Now Whitney decided to make
muskets by constructing in advance a full line of accurately guided
tools, and stamping out full mill runs of interchangeable parts.

The idea was epoch-making, though actually it had been antici-
pated (and dropped) in France by a gunsmith named Le Blanc.
In 1798, Whitney got a hearing with U.S. Army officials and ob-
tained a sizable contract. It took him a year to tool his factory, and
when the time for delivery of the first 4,000 muskets and equipment
came, Whitney had only 500 on hand. At this critical point he put
on a demonstration before President John Adams and Thomas
Jefferson, then Vice President, in which he disassembled the guns,
scrambled all the parts, and put a new batch of weapons together.
The demonstration gained him a needed reprieve and the government
advanced him most of the $134,000 needed to fulfill his contract.

In 1812, Whitney’s success was assured by a second agreement,
for 15,000 muskets; and visitors from all over the world began to
come to Hamden, Connecticut, to goggle at his tiny factory at the
Lake Whitney fall line of the Mill River. Other gunmakers—Sim-
eon North of Middletown and Berlin and, eventually, Sam Colt
of Hartford—picked up Whitney’s manufacturing ideas. The
clockmakers of New Haven and the nearby Naugatuck Valley,
seeking mass methods for punching out clock faces, also came to
listen and to learn. Whitney’s first crude milling machines for chip-
ping and planing metals caught the eye of toolmakers for the tex-
tile business. And, presently, a new generation of machinists and
inventors emerges in Windsor, Vermont, and in Providence, Rhode
Island (home of Brown & Sharpe), to create the machine-tool in-
dustry—the “industry that is behind all industry’’-which today
makes the multiple drills and presses and automated monsters of
the modem age.

Thus the vital seed corn of ideas was sown. Yet the pioneer
mass-production methods, which appear in startling if primitive
clarity in the cotton mills of Slater and Francis Cabot Lowell, the
flour mills of Oliver Evans, the brass factories of the Naugatuck,
