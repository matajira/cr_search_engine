THE GILDED AGE « 149

In 1860, John D. Rockefeller, then a twenty-year-old junior
partner in the Cleveland commission firm of Clark & Rockefeller,
visited Pennsylvania’s Venango County, where the first drilled oil
well had yielded its black wealth the year before. He came as
the agent for Cleveland businessmen who had been impressed
with the young merchant’s sobriety and his ability to judge a balance
sheet. What the businessmen wanted was some guidance to invest-
ment possibilities in a region that had brought the bonanza feeling
far closer to the East than it had ever been before.

What Rockefeller found when he got to the Oil Creek region
of Venango was enough to fill his fastidious soul with acute distaste.
Oil, in that last pre-Civil War year, was a rafEsh, up-and-down
business, and had been so from the very start. It had had its origins
in medical quackery as white men, posing as Indian doctors, put
the skimmings from Pennsylvania creeks into eight-ounce bottles
and hawked them as a sure cure for “cholera morbus, liver com-
plaint, bronchitis, and consumption.” Using by-product oil from
salt wells, the greatest of the “Seneca oil” Bamums, Doc Samuel
Kier, had made a big enterprise of selling the stuff as “medicine”
long before anyone had thought of drilling a well directly to get
at it.

The transformation of Pennsylvania “rock oil” into an illuminant
dates from a day in the 1850’s when George H. Bissell of New York
took a specimen to Yale’s professor of chemistry, the younger
Benjamin Silliman, and asked him to analyze it in his laboratory.
In a scientific classic, Silliman reported that “rock oil” could
be refined into a better illuminant than oil squeezed from coal tar,
cannel coal, asphalt, or “albertite” bituminous rock, all of which
went under the generic name of coal oil. But how to get the oil out
of the earth? Bissell had seen a picture of a salt-well derrick on a
Kier Seneca oil advertisement but he had not acted on it. One of
Bissell’s associates, the New Haven banker James Townsend, was
the man of action who dispatched a New Haven Railroad conductor
named Colonel E. L. Drake (he had a free railroad pass) to
Titusville in Venango County with instructions to dig a well
directly into oil-bearing strata. Using a salt-well driller’s tools,
Drake made his soon-to-be-famous strike in 1859. Despite the
distractions of the Civil War the Oil Creek region of Pennsylvania
