THE GILDED AGE « = 155

was the first really to push research, hiring a German chemist
named Herman Frasch, who showed him how to refine a market-
able product from the sulfurous crude “skunk oil” of the new
Lima-Indiana field, the first to be opened up outside the Appalachian
district. And in overseas markets Standard was the leader in
providing “oil for the lamps of China.”

For all the fearsome Rockefeller power, however, other men
fought him and remained in business. The Pews of Sun Oil, a com-
pany that got its start in Ohio in the mid-eighties, built up a compact
and powerful rival organization. Lewis Emery Jr., who hated the
whole Rockefeller tribe, built two pipelines (one for refined oil,
one for crude ) from the Pennsylvania fields to the Delaware River
for his Pure Oil Co., and, unlike other pipeline builders, kept his
creation out of Rockefeller hands. The emergence of these com-
panies proved that Standard Oil could be fought right on Rocke-
feller’s own home grounds. Then, after 1901, the year in which a
Yugoslav immigrant named Anthony Luchich—or Lucas-pounded
his drill deep into a salt dome at Spindletop on the Gulf Coast of
Texas and opened up a geyser of oil that caught fire and burned
for days, the Rockefeller could no more dominate oil than King
Canute could dominate the tides. Two Pittsburghers, John H.
Galey and Colonel J. M. Guffey, had backed Lucas financially, and
when they in turn ran out of money, Guffey went hat in hand with
the Spindletop prize to the Mellons. No strangers to oil (a Mellon
had once built a pipeline and had unloaded it on Rockefeller at a
profit of $2 million), the Mellons liked the prospects. The result
was the Mellon-backed J. M. Guffey Petroleum Co., the forerunner
of Gulf Oil. This time the Mellons, who kept 40 per cent of Guffey
for themselves, had no intention of selling out to Rockefeller.

Standard tried to operate in Texas through the Waters, Pierce
Oil Co., a southwestern marketing concern, but it was too late to
head off the newcomers. Soon the Texas Co.—the forerumer of
Texaco-was in the field. Gulf and Texas moved quickly into the
new gasoline market at a time when Standard was still pretty much
committed by its technological investment to kerosene. The rise of
the new independents was accomplished some years before the
Supreme Court invoked the Sherman Act of 1890 to force the dis-
solution of the Standard Oil holding company into its constituent
