Yy

om IDA e Dw

11.
12.
13.

Contents

Author's Preface (1991)
Publisher's Foreword, Gary North

Introduction

. Free Enterprises Before the Revolution
. Businessmen Join in an Unbusinesslike War

. The Quest for Capital and the Sprouting of Invention

Early America Goes Places

. Frontier and Factory

. The Pre-Civil War Speedup

. The Civil War and Its Aftermath
. The Gilded Age

. The Rise of the Money Power
10.

The Age of Edison

FOB. Detroit

The New Frontier of the Depressed Thirties
The Modern World of Enterprise
Bibliography

Index

yp. Sw =

43
63

100
119
140
163
185
202
223
243
265

273
Wr
