AT PUBLISHER’S FOREWORD

titers will. Then comes the day of reckoning. We consumers vote
“yes” or “no” with our money, assuming that the state allows us this
fundamental freedom. To make the free market work, whether in
our role as consumers or producers, we must be given the right — the
legal immunity from violent retaliation — to collect and then allocate
the fruits of our labor. As the employer in Jesus’ parable of the
workmen asked: “Is it not lawful for me to do what I will with mine
own?” (Matt. 20:15 a).

‘What we as consumers want for ourselves, we must also allow to
the businessman who seeks to serves us. Without this same liberty to
keep the money left over after expenses are met, the entrepreneur
cannot be enticed to serve us as we choose, rather than as some gov-
ernment bureaucrat chooses. The legal right to profit from the fruits
of one’s successful innovation, and the simultaneous threat of reap-
ing the expensive rewards of failure, are the heart of the great
American experiment in economic freedom. Without the latter, you
camnot attain the former. This is the lesson that Eastern Europe
needs to take to heart, not the temporary excesses of junk bond
speculation. So does the average American.

By reprinting John Chamberlain’s Enterprising Americans, 1
hope to remind readers of this lesson.

(My thanks go to the late Alfred H. Darlow, whose generous
legacy to the Institute for Christian Economics has made possible
this book as part of a reprint series. May others with a similar vision
share the fruits of their labor with their fellow men: to keep liberty
alive in the land.)
