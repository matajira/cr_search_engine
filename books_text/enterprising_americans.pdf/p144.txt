7 The Civil War and Its Aftermath

 

The illusions that led to Fort Sumter,
Machine-reaped wheat feeds the North,

The Tredegar Works rolls armor plate for the South’s iron-
clads,

Supersalesman Jay Cooke hawks war bonds,
Four shopkeepers create a continental market,

Jor before the onset of the Civil War, William Tecumseh Sher-

‘man, a transplanted Ohioan then serving as superintendent of a
military academy in Louisiana, addressed a somber warning to a
southern friend, “You people speak so lightly of war,” he said.
“You don’t know what you are talking about... . You mistake
. . . the people of the North. They are a peaceable people, but an
earnest people and will fight too, . . . Besides, where are your men
and appliances of war to contend against them? The northem peo-
ple not only greatly oumumber the whites at the South, but they are
a mechanical people with manufactures of every kind, while you
are only agriculturists-a sparse population covering a large extent
of territory, and in all its history no nation of mere agriculturists
ever made successful war against a nation of mechanics. .. . The
North can make a steam engine, locomotive or railway car; hardly
a yard of cloth or a pair of shoes can you make. You are rushing
into war with one of the most powerful, ingeniously mechanical
and determined people on earth-right at your doors. You are
bound to fail... .“

119
