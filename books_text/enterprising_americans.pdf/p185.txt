160 . tHe ENTERPRISING AMERICANS

the opportunistic Carnegie entered steel price and production pools.
When the advantage ran out, however, he was the first to quit. He
absorbed the Duquesne Steel Co. after spreading what today would
be termed commercial libel against it. He forced his partners to
sign an “ironclad” agreement that they would return their stock to
the company at book value if, by any chance, they proposed either
to retire or resign. He kept the steel tariff as high as he could for as
long as he could, and if a railroad would give him a rebate he was
not averse to accepting it. But none of these things really accounted
for the basic profitability of the Carnegie enterprises. What counted
was that Carnegie kept the organization on its toes: the cost of pro-
duction was constantly lowered by men whom Carnegie often set at
each other’s throats to make new records. Young men like Charlie
Schwab—the “young geniuses” of Carnegie’s verbal adulation—
were generously rewarded for enterprise, acquiring partnerships
by acquiring stock they paid for on easy terms out of earnings.
Whenever a depression came, Carnegie bought out his rivals,
ending up in the nineties with a completely integrated company
that owned or leased its Mesabi ore beds, its limestone and coke
sources, its Great Lakes freighters, its docks and railroad lines, and
its great mills at Braddock, Homestead, Duquesne, and Beaver
Falls. Despite a fierce quarrel with Frick, and despite his inability
to tolerate anybody near the throne very long, Carnegie could still
truly suggest for his epitaph, “Here lies the man who knew how to
get around him men who were cleverer than himself.”

The proof of the Carnegie pudding was in the eating. Profits,
which had been at the rate of $2 million per year in the early
eighties, had jumped to $5 million in 1890, and had risen to $40
million by 1900. This immense profitability derived from the fact
that Carnegie had a grip on the bulk of crude-steel production in
the U.S. Even so, his position was not entirely impregnable. For
one thing, the Chicago steel area, where Captain Eber B. Ward
had rolled Bessemer rails as early as 1865, was producing ingots,
and in the late nineties this production increased with the organiza-
tion of the Federal Steel Co. More seriously, Camegie found him-
self harassed by the creation of wire, tube, tinplate, bridge-making,
and other fabricating companies which threatened to build basic-
ingot capacity in preference to buying from Carnegie mills. If
