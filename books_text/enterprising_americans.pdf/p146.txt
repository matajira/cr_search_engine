THE CIVIL WAR AND ITS AFTERMATH - 121

Texas border grievances, had not resulted in the acquisition of fur-
ther lands that were really adapted to slaveholding. The new terri-
tories of New Mexico and Utah permitted slavery, but as Daniel
Webster had predicted, virtually no Southerners took their slaves
there. Brigham Young, the Mormon leader, though not theoretically
opposed to slaveholding, told Horace Greeley that Utah could not
afford slaves.

Thus right up to the outbreak of the Civil War the slave system
was still highly concentrated in the open Delta and river-bottom
regions, and its operation there was in turn highly concentrated in
relatively few hands. In 1850 there were some 1,800,000 blacks
in the South as compared to 2,100,000 whites, which would have
theoretically permitted three or four slaves per family. But the
profits created by the blacks were channeled into the coffers of the
three or four thousand families that received three-fourths of the re-
turns from southern exports. A thousand great families shared in
an income of $50 million a year; the remaining 660,000 southern
families divided $60 million a year. To keep pace with expanding
opportunities, the richer planters kept plowing their profits into
the purchase of new field hands, whose value was compounded
every time a slave reproduced himself. The owner of a hundred
slaves might conservatively estimate his wealth at upwards of
$100,000 in the 1850's. A prime field hand brought $1,500 and
more in the palmiest days of cotton culture—and the old tobacco
states of the upper South shared in the new prosperity by becoming
slave breeders. Planters lived well and variously, dipping snuff,
attending barbecues, visiting town on court days, traveling by car-
riage for great distances to visit their kin, feuding and dueling when
they were angered, reading Greek and Latin classics (and the be-
loved Waverly novels of Sir Walter Scott) in their libraries, and
listening to the new theorists of “Greek democracy” expound the
virtues of a republic reared on the backs of an inferior class.

Yet this well-being was also in a way febrile: though the laws of
genetics seemed to guarantee a permanently expanding wealth, the
profits of cotton culture mysteriously disappeared from the southern
banks. In 1850 cotton and other southern crops sold for $120 mil-
lion—yet total bank deposits in the South amounted to only $20
million. In 1860, when southern crops were valued at $200 million
