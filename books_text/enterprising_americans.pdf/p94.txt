EARLY AMERICA GOES PLACES: 69

system of three-cornered trading which brought cotton in New
York ships from Charleston or Savannah to Liverpool, where it was
exchanged for British manufactured goods. These, in turn, were
carried to New York. Reloading at North and East River docks,
the ships would then be off to Charleston again, or maybe to New
Orleans. The enterprise of New Yorkers kept the southern cotton
planters from developing their own shipping and marketing institu-
tions. But to become truly imperial in a trading sense, however,
New York needed more than cotton as an export commodity. The
Erie Canal, which brought produce from as far away as the Ohio
Valley within two weeks’ reach of New York Bay, supplied the
answer. Henceforward Cincinnati (or Porkopolis, as it also came
to be called) actually had closer physical ties with New York than
it had with Baltimore. And the building of Ohio canals connecting
the southward-flowing Scioto and Miami rivers with Lake Erie
ports completed New York City’s call on westem products. With a
shrewd eye to changing values, John Jacob Astor, who had made
his first fortune in the fur trade, transferred much of his wealth
into New York City real estate. The Erie Canal helped to boom
the fortunes of older landed families in Manhattan, such as the
Goelets. And in creating a metropolis the Erie Canal did its part
in making the first great department store tycoon, Alexander Turney
Stewart, an early millionaire.

The Erie Canal fired the state of Pennsylvania to improve its
own water routes to the West. Long before the War of 1812, Penn-
sylvanians had seen the importance of bringing anthracite coal to
market in New York and Philadelphia. To this end they built the
Delaware and Hudson Canal, leading from Honesdale in Penn-
sylvania to the Delaware, and thence to Rondout on the Hudson.
A second artery, the Lehigh Canal, designed to supplement slack-
water navigation on the Lehigh River, was built by Josiah White
of Philadelphia and a group of associated wire manufacturers who
had found Mauch Chunk anthracite fuel to their liking. They
organized the Lehigh Navigation Co. (capital, $55 ,000) to build
dams and sluices along the river and a connecting nine-mile road
to the mines. Later, their two companies were merged as the Lehigh
Coal & Navigation Co., and set an important precedent as the first
instance of interlocking companies in U.S. business history.
