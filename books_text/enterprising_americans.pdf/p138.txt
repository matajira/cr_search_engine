THE PRE-CIVIL WAR SPEEDUP 113

border in New York. The Quakers of Philadelphia formed early
joint stock companies to build forges and furnaces in the Reading
region; and in New Jersey, where there were small amounts of
really good ore in the mountains near the New York line, an ambi-
tious German named Peter Hasenclever, using London capital,
splurged way beyond his credit in setting up a huge complex of
furnaces and mines for something variously called the London
Company or the American Iron Company.

Hasenclever went broke and was discharged by his irate stock-
holders; but a Scotsman, Robert Erskine, took over the enterprise
in 1771 and made something of Hasenclever’s pioneering. He found
that Hasenclever had provided him with many valuable “firsts”—
furnaces whose inside walls were made of durable slate, forges
whose hammers were operated by strong overshot waterwheels, and
artificially dammed reservoirs to provide a continuity of water
power in dry months. Adding some wrinkles of his own, such as
the country’s first magnetic ore separator (an oak drum fitted out
with magnets ), Erskine put the American Iron Company in shape
in time to provide iron for Washington’s armies. Washington him-
self thought so much of Erskine that he made him his chief engineer
and map-maker, appointing him as official geographer to the Con-
tinental Army.

Tn all that pertained to the extraction of ores from bogs and
pockets and small seams, and the subsequent melting and shaping
of the mined ore into pig iron, the early American iron industry
was first-rate. But the colonists always had to reckon with the Brit-
ish government’s refusal to countenance a local iron fabricating
industry. J. Leander Bishop speaks eloquently of the “dexterity of
Americans in the manufacture of scythes, axes, nails, etc.,” but “the
flood of foreign iron... at the close of the war” kept American
production from growing rapidly. Aside from a few slitting mills
which provided iron for nails and other close-to-home building
items, there was nothing much of a real iron manufacturing business
in the America of the late eighteenth and early nineteenth centuries.

What gave impetus to developing something better was the com-
ing of the railroads. When Peter Cooper started work on his first
locomotive, the Tom Thumb, in Baltimore he found a dearth of
fabricated iron pipe, and in fact built his engine largely from scrap.
