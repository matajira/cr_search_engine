258 . THE ENTERPRISING AMERICANS

In spite of our international troubles we had become a trillion-
dollar economy. Our very affluence, however, created disturbing
expectations. The consumer, with money in his pocket and a bigger
margin of time in which to enjoy life, started to ask finicky questions
about quality. Ralph Nader, a far greater master of public relations
than Ivy Lee and Edward Bernays, put himself at the head of a
consumers’ lobby, with rather mixed impact on the economy.’ The
consumers’ movement is here to stay, and it will put new life into
Better Business Bureaus and the journalism that has built its success
on the pioneering of such magazines as Consumer Reports. But
“Naderism” killed off a small economy-type car, the rear-engine
Corvair, that had already been made roadworthy by correction of
its original defects. Moreover, Nader zealotry, a good thing on
balance as long as it sticks to proselytizing within the free market,
could have disastrous inhibiting effects if it were to result in a super-
agency designed to police the American corporation. Ever since
the formation of the Interstate Commerce Commission in the
eighteen-eighties, the lesson of government regulation is that it
creates more problems than it solves. The natural history of a
regulatory agency, as the late Robert Young often complained, is
that it becomes prey to politics. As often as not, the industry to
be regulated manages to get working control of the agency itself.
The collapse of the eastern railroads in the sixties was a pointed
commentary on the “regulatory idea. The consumer of mass travel
would manifestly be better off today if railroads had been left free
to tear up unused tracks, to trim the crews of Diesel trains, and to
combine in order to compete with trucks and airplanes. The merger
of the New York Central and the Pennsylvania might have worked
if it had come a generation earlier with a good railroad man such as
Alfred Perlman in charge of combined operations.

Environmentalism, the natural complement of the consumer
movement, has also proved a mixed blessing. The ecologists, busy
with their stop signals, perform a necessary warning service. Com-
mon sense should tell us that it is dangerous to construct atomic
energy plants along the line of the San Andreas fault in California,
or to drill indiscriminately for oil in the geologically unstable bot-
tom of the Santa Barbara Channel. Moreover, there is more than
sentimentalism involved in the protection of endangered species
