114. THE ENTERPRISING AMERICANS

Tt was thus borne in on him that iron manufacturing might make
him some money to add to what he was getting out of his glue and
gelatin business. Throughout the 1830's and the early 1840's,
Cooper, a public-spirited gentleman, was busy with a hundred proj-
ects, from public-school education to the organization of the Croton
River water supply for Manhattan Island. But he also found time
to build an iron foundry on Thirty-third Steet, near Third Avenue,
in New York City, where he experimented further with using an-
thracite coal instead of charcoal in the smelting of the ore. At the
same time he put his profits from glue into railroad securities. The
New York iron foundry was too far away from coal and iron sources
to be a profitable producer of iron for railroads, and Cooper, who
believed in supporting his investments, meditated the transfer of his
mill equipment to the valley of the Delaware.

He might have done nothing about this if he had not had an able
son, Edward, who had demonstrated inventive and mechanical tal-
ents as a boy. Edward, in turn, had an even more able friend,
Abram S. Hewitt, who ultimately married Peter Cooper’s daughter.
Though he had a prejudice against partnerships, Peter Cooper
yielded to the point of becoming a silent partner with his son and
son-in-law-to-be in a new mill at Trenton, New Jersey. Hewitt, who
had transcendent organizing and merchandising ability, took hold
of the selling while Edward Cooper remained in the shop-and
within a year the new mill had a contract with the Stevens’ Camden
& Amboy Railroad for replacing unsatisfactory English rails. The
Camden & Amboy contract, which yielded $180,000 in cash, posed
many problems, both of mill technology and of raw-material sup-
ply. Edward Cooper had to learn from scratch how to roll Robert L.
Stevens’ T-rail, with its flanged bottom. But inside of a year he was
producing the rails, and contracts with the Hudson River Railroad,
the New York & Harlem, and the Rutland & Burlington followed.
Tn 1848 the Trenton mills paid a dividend of 20 per cent.

Eastern iron posed problems for rail-makers, whose early rail
tops splintered easily. Hewitt, whose avocation ‘it was to tramp the
forested hills of the upper New Jersey counties, found himself look-
ing into the old shafts of abandoned colonial mines in search of an
iron that would be “close-grained and tenacious.” He found such
ore in the disused Andover mine, which had once been in the pos-
