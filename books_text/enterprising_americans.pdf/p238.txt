F.o.8. DETROIT . 213

attributed (with no direct denial by Ford himself) to his partner
Couzens, who had become appalled by the inefficiencies created by
labor turnover. Reporters who interviewed Ford soon after the
event, however, have insisted that Couzens’ suggestion of a $5 wage
was meant ironically, and that Ford deserves credit for taking it up.

Despite the quarrels over “firsts,” it was Henry Ford who fused
all the basic mass production ideas together. In the end he not only
outstripped all competitors but also acquired the means to buy out
every last one of his financial partners. Strelow, the carpenter, and
Malcomson both sold out early. Couzens left the company in 1915,
not because of any business-policy discord but simply because he
couldn’t abide Ford’s pacifistic attitude toward the war in Europe.
When Ford eventually arranged to take over Couzens’ stock in
1919, he paid $40 million for it (and Couzens had already had $11
million in dividends on his early investment of a very few thou-
sand). Couzens’ sister Rosetta, who sold out at the same time, col-
lected $265,000 on her original $100, which had already brought
her $90,000 in dividends. The estate of candy-maker Gray got $26
million on the original $10,500. As for Horace and John Dodge,
who had precipitated Ford’s decision to get rid of his minority
stockholders by suing him for a distribution of earnings and by
enjoining him from going ahead with the decision to build the big
steelworks at the River Rouge in Dearborn, they departed with
$12,500,000 each. They had already had more than $17 million
each in dividends, much of which they had put to work in their
own Dodge Motor Co., whose sturdy cars provided some competi-
tion for the Model T even though they were in a higher price range.

The financial strain on Ford that had been caused by buying his
partners’ stock and by building the huge plant at the Rouge was
compounded by the 1920-21 postwar depression, but the Ford
Motor Co. survived without giving hostages to the bankers, whom
Ford always detested. Ford squeaked through by going full steam
ahead with his factory rans and shipping huge numbers of cars—
for cash—to his dealers, whose local credit was sufficient to carry
them through the short depression period. The dealers groused
about being badly used, but they had made such a good thing out
of the Ford franchise for twelve golden years that virtually none of
them cared to risk losing favor with the Dearborn autocrat. And
