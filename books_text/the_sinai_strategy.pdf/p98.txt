4
SABBATH AND DOMINION

Remember the sabbath day, to keep it holy. Six days shalt thou labour, and
do all thy work: But the seventh day is the sabbath of the LORD thy
God: in it thou shalt not do any work, thou, nor thy son, nor thy
daughter, thy manservant, nor thy maidservant, nor thy cattle, nor thy
stranger that is within thy gates: For in six days the LORD made heaven
and earth, the sea, and all thai in them ts, and rested the seventh day:
wherefore the LORD blessed the sabbath day, and hallowed it (Ex,
20:8-1)).

We come now to one of the most difficult of all exegetical and ap-
plication problems in the Bible, the question of the meaning and en-
forcement of the sabbath. Only the proper interpretation and ap-
plication of the tithe principle is equally as difficult and controversial
an economic question. Both issues involve the question of what man
is required to forfeit in order to honor God.

Several questions must be considered. First, what is the meaning
of “rest”? Second, what is the meaning of “sabbath”? Third, is the
Lord’s Day the same as the sabbath? Fourth, what, was the focus of
the sabbath in Old Testament times: rest or worship, or both? Fifth,
how extensive were the restrictions against working in Old Testa-
ment times? Sixth, are these same restrictions still required by God
in New Testament times? Seventh, who or what agency is to enforce
sabbath requirements in New Testament times? In short, where is
the docus of sovereignty for sabbath enforcement? Eighth, if the Old
Testament’s prohibitions had been enforced throughout the history
of the West, could the modern, industrialized West ever have come
into existence?

In order to keep this introductory chapter sufficiently short and
uncluttered with technical problems, I have decided to add an ap-
pendix on the economics of sabbath-keeping. I cover questions four

72
