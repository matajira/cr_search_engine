262 THE SINAI STRATEGY

‘Thou shalt not do any work, thou, nor thy servant,’ but you compel
your servants to break God’s law, and to sell their souls for gain. You
are sinners against the light... . You are traitors to your country... .
Was it not Sabbath-breaking that made God cast away Israel? .. .
And yet you would bring the same curse on Scotland now. You are
moral suicides, stabbing your own souls, proclaiming to the world that
you are not the Lord’s people, and hurrying on your souls to meet
the Sabbath-breaker’s doom.”

Sabbatarians should heed McCheyne’s warning. Those who
stand in pride because of their sabbatarian position ought to con-
sider the implications of that position. God will not be mocked!
When the provisions of the Westminster Confession of Faith are rig-
orously enforced, then the sabbath debate can take on some mean-
ing other than the playing of theological games. Then, and only
then, will the issues be drawn clearly and honestly.

Enforcement Should Begin at the Top

When the elders of the church begin at home to follow the sabbatar-
ian standards of the Old Testament, and when they impose such
standards on their recalcitrant wives who enjoy their stoves, their
hot running water, and their air-conditioning systems, then non-
sabbatarians will be impressed. Let them turn off their electrical ap-
pliances, or purchase 24-hour power generators (no “lighting fires,”
please), or install solar-powered cells on. their roofs, in order to pro-
vide the power. Let them turn off the natural gas, or else purchase
butane in advance. Let them cease phoning their friends for “Chris-
tian fellowship,” so that the lines might be kept open for truly emer-
gency calls, Let them stop using the public mails on Friday, Satur-
day, and Sunday, so that mail carriers and sorters will not have to
miss their observance of the Lord’s day. Let them, in short, shut
their eyes to the offenses of others until the church, as a disciplinary
force, begins to enforce more rigorous requirements on ail the mem-
bership, starting at the top of the hierarchy and working down from there.
Let all self-righteousness be abandoned until the full implications of
the economics of sabbath-keeping are faced squarely by the church’s
leadership, Until then, the debate over the sabbath will remain an
embarrassment to Christ’s church,

Rethinking the sabbath question will involve a rethinking of the

26. Ibid., p. 600. See also his “Letter on Sabbath Railways,” (1841), pp. 602-5.
