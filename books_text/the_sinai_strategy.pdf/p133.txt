Familistic Capital 107

rugged individualism, or social Darwinism. But his frame of refer-
ence was both very short and very long: the individuals lifetime and
the eternal evolutionary process. In between, the laws of Individual-
ism, Private Property, and Accumulation of Wealth do not apply.
Once a man dies, the State must move in and confiscate the bulk of
the dead man’s estate—in the name of the people, With this doc-
trine, Carnegie broke with nineteenth-century social Darwinism.
“There are but three modes in which surplus wealth can be dis-
posed of. It can be left to the families of the descendants; or it can be
bequeathed for public purposes; or, finally, it can be administered
during their lives by its possessors.” Carnegie overwhelmingly favored
the third approach. He absolutély rejected the idea that rich men
should bequeath. their capital to their children: Children should be
provided for “in moderation.”*> Leaving one’s wealth in a will to the pub-
lic often does not work, since the executors may not use the funds as
the testator had hoped.*” What is needed, therefore, is a massive tax on
inherited wealth. “The growing disposition to tax morc and more heavily
large estates left at death is a cheering indication of the growth of a
salutary change in public opinion, . . . Of ail forms of taxation, this
seems: the wisest. Men who continue hoarding great sums all their
lives, the proper use of which for public ends would work good to the
community, should be made to feel that the community, in the form of
the state, cannot thus be deprived of its proper share. By taxing
estates heavily at death the state marks its condemnation of the selfish
millionaire’s unworthy life.”® A man’s children must be expropriated.
Here is the theology of the pseudo-famtly. The public, or commu-
nity, is narrowly (and improperly) defined as the civil government,
meaning organized political power. The State is entitled to “its
proper share,” and that share is large. The hoarder is living “an un-
worthy life.” He set forth no limits on such taxation: “It is desirable
that nations should go much farther in this direction. Indeed, it is
difficult to set bounds to the share of a rich man’s estate which should
goat his death to the public through the agency of the state, and by
all. means such taxes should be graduated, beginning at nothing
upon. moderate. sums to dependents, and increasing rapidly as
amounts swell. . . .”29 This vision has been universally accepted by

36. Ibid., p. 3
37. Tbid., p. 4.
38. Idem.
39. Iden.
