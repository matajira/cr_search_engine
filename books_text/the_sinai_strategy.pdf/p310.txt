284 THE SINAI STRATEGY

Certain professionals, such as policemen and firemen, are
already granted a kind of unofficial “Lord’s day-desecration
voucher.” ‘hey are not brought before the elders for working on
Sundays. They are also paid by the civil government for their Lord’s
day-desecration activities. But this is an unofficial exemption. Sab-
batarian churches (as far as I have been able to determine) make no
official institutional alterations for these members to celebrate com-
munion, These members are simply ignored. Elders “shrug off” the
whole problem. Why not face the problem, and rethink the whole
question of legitimate employment on the Lord’s day, and legitimate
communion meals on other days of the week?

Admittedly, corporations and small firms should see to it that no
one employee is stuck permanently with Sunday (Firstday) assign-
ments. This assignment should be rotated, so as not to disrupt men’s
worship on a permanent basis. But labor on the Lord's day is not
automatically to be regarded as Lord’s day desecration.

Priestly Exemptions

There is always the standard solution to the general problem
posed by the steel industry example: the “works of necessity” argu-
ment. Perhaps this really is the right approach. The sabbatarian
argues that steel is vital to the economy. Such an argument certainly
seems reasonable. Then, since there appears to be no way to pro-
duce steel on any basis except seven days per week, the steel] industry
should receive a special dispensation from the church and the State
which allows it to go on producing. While the Lord's day is profaned,
the profaners are held guiltless. The owners (share-holding in-
vestors), managers, and laborers are treated as Christ said that God
treated the priests in the temple: They are held innocent. Those as-
sociated with steel production have become “honorary priests.” They
are laboring in a vital industry, so this constitutes an assignment
from God, comparable to God’s assignment to his priests in Moses’
day. They become exempt from the Lord’s day prohibitions.

The church or State which takes this posilion has decided to
become involved in endless appcals from industries that want to be
reclassified as “priestly” in nature: vital to the economy and also in-
nately seven-day operations because of the nature of the markets
they face. What predictable, legal criteria would the State use to
determine such questions? What constitutes a vital industry? Which
industries, now just starting out, will (and should) be allowed by the
