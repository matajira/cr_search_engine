Sabbath and Dominion 83

law. Furthermore, the day of Pentecost came seven weeks later, ac-
cording to Mosaic law (Lev. 23:16). The disciples were meeting
together, and the Holy Spirit came upon them (Acts 2:1-5), Speaking
in many foreign languages, they communicated the gospel to a miul-
titude (2:6), each in his own language (2:8). Critics charged that
they were drunk with “new wine” (2:13). Peter’s response is signifi-
cant: “For these are not drunken, as ye suppose, seeing it is but the
third hour. of the day” (2:15). In other words, it was about three
hours after the dawn. Peter was saying that these men had not had
time to get drunk. People were not gathering to hear the gospel three
hours after sundown, for then Peter’s words would have been mean-
ingless. Obviously, an evening of drinking might have preceded a
nighttime outpouring of the Spirit. If we assume that dawn was
around six o'clock in the morning, then “the third hour of the day”
would have been about 9 a.m. This corresponds to the Roman sun-
dial, which marked noon as the sixth hour. 2

The Communion Meal

There are other pieces of data that point to a sunrise-to-sunrise
Lord’s day. Jesus met with His disciples on the evening of His resur-
rection (John 20:19), eating with them (Luke 24:41-43).. This com-
munion meal took place after the sun had gone. down. He had
already eaten with two disciples at Emmaus, approximately seven
miles from Jerusalem (Luke 24:13: Berkeley Version), and this meal
took place as the sun was setting (Luke 24:29-30). These two disci-
ples then walked from Emmaus to Jerusalem in order to meet with
the other disciples, Then Jesus appeared to the whole group (Luke
24:33-34). Yet this meeting is described as having taken place “the
same day at evening, being the first day.of the week” (John 20:19a).
John was not using the Hebrew day, sundown to sundown, as his
measure of the first day.

Paul’s lecture to the church at Troas took place on the Lord’s day.
“And upon the first day of the week, when the disciples came. to-
gether to break bread, Paul preached unto them, ready to depart on
the morrow; and continued his speech until midnight” (Acts 20:7).
He departed at the “break of day” (20:11b).

The evening meeting was a communion feast, as was the first

12. Leon Morris, The Gospel According to Join (Grand Rapids, Michigan: Eerd-
mans, 1971), p. 158n,

 
