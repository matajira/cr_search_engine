Introduction 13

wages promptly paid, would revolutionize the face of economic life
for multitudes of workers in some parts of the world. And all of these
are drawn from the economic legislation of God’s redeemed people,
Israel.”2!

Such statutory actions would indeed revolutionize the face of
economic life for multitudes of workers: It would guarantee their
continuing unemployment in all legal markets. It would, if enforced
universally, transfer a monopoly grant of power to industrial econo-
mies, and specifically to the monopolistic trade unions, whose mem-
bers cannot-stand the wage competition which is offered by Third
World employees. “Statutory” is Dr. Wright’s key word, and it is this
word which was not used in the Old Testament. God, not the State,
is sovereign. God issued His economic laws, and it is market compe-
tition and self-government under God’s law, not statutes; that are
supposed to govern men’s economic actions in the vast majority of
cases, as The Dominion Covenant volumes on the Pentateuch will dem-
onstrate.

What is noticeable is Wright’s hostility to the binding character
of Old Testament law fiterally transferred to today’s political institu-
tions, for what that law would bind is the messianic State. Predic-
tably, we find antinomianism—hostility to the continuing validity of
God's revealed law—in close association with statism and a mania
for legislation. What the Bible warns-against above all—the di
zation of man—and what-its law-order undermines whenever it is
taken seriously, the modern antinomians have implicitly accepted.
The divinized State that the Bible’s law-order militates against is the
sacred cow of the intellectuals today, In short, there is a relationship
between false gods and high taxes. These armchair socialists proclaim
their allegiance to the “paradigmatic principles” of Old Testament
Jaw, but not its State-restricting specifics. They proclaim the “princi-
ple of the tithe,” and then go on to promote massive compulsory tax-
ation by the State. In short, they are devoted to Old Testament laws
only on an ad hoc basis: whenever such verbal allegiance can be mis-
directed to glorify the authority of the State.

This same sort of social antinomianism also characterizes the so-
called “cosmonomic” Christianity of the Dutch Calvinist philoso-
pher, Herman Dooyeweerd and his followers. In their hands, the
Bible becomes a manual of guild socialism or worse.?? Liberation

 

ini-

 

21. Bid. p, 80,
22, See Appendix C; “Social Antinornianism.”
