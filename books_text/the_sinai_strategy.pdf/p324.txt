298 THE SINAI STRATEGY

retical question I have raised concerning music in the park is not
hypothetical; it became a serious political issue.

The Middle Classes

Wigley argues that the appeal of sabbatarian ideals io middle
class people gave it great strength. A new appeal, based on practical
benefits rather than an appeal to the Bible, became increasingly
prominent within sabbatarian circles. He calls this social sabbaiarian-
ism. “Social Sabbatarianism was of enormous importance. It moulded
and enlivened the controversy for almost fifty years. It allowed the
Sabbatarians to avoid authoritarianism and to champion the work-
ing classes. It allowed Nonconformists to assuage their consciences
and to defend the civil observance of the Sabbath. It allowed M.P’s
to reconcile their /aissez-faire principles with their religious values, for
no legislation was called for, merely the defense of the status quo” (p.
72). But it was a departure from the earlier sabbatarianism, and on
many occasions, defenders of a religious sabbath refused to join with
social sabbatarians in “the great cause.” As the pragniatic arguments
weakened, especially as the century wore on and more leisure time
was made available to workers, the religious sabbatarians recognizéd
the epistemological weakness of social sabbatarianism, “Six days
shalt thou labor” became five and a half, and in this century, five;
Sunday amusements also appeared to be beneficial, so the pragmatic
arguments no longer carried as much weight. But the religious sab-
batarians had been pre-empted by 1900, and few people listened to
them any longer.

The leisure of the high-capital late ninetcenth century was not
characteristic of the low-capital era of the late eighteenth. There is
no doubt that the industrial revolution increased’ the number of
working days in the late cighteenth century. For example, the Bank
of England (the private central bank) steadily reduced the number of
holidays from 46 in 1761 to 4 in 1836 (p. 74). The 12-hour, 6-day in-
dustrial workweek became the norm as the industrial revolution
gathered strength. Sabbatarians’ could’ appeal to overwork as one
reason jor a legislated sabbath. But they steadfastly refused to pro-
mote a law which would prohibit masters from working servants on
Sundays. The theological justification: acts of mercy and necessity
(p. 78}. This was an exemption for rural lords at the competitive ex-
pense of the industrial managers.

Wigley presents the interesting case of W. H. Smith, the Chris-
