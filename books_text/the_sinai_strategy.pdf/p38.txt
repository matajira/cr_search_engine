12 THE SINAI STRATEGY

than the centralized solution.”'7 He sees that the Bible does teach
such a decentralized and privatized view of society, so he rejects
from the start any suggestion that this blueprint is still morally or
legally binding on Christian societies.

InterVarsity Press in 1983 published one of its typically statist
tracts in the name of Jesus. The author, a British Ph.D. from Cam-
bridge who is now teaching theology. in India, rejects the idea that
Old Testament law is still literally binding in New Testament times.
“In the economic sphere, the Old Testament paradigms provide us
with objectives without requiring a Qteral transposition of ancient
Israelite practice into twentieth-century society.”!* In other words,
Old Testament law, which drastically limited the centralization of
power by the civil government, is no longer supposed to bind the
State.

Here is the two-part argument which virtually all of these wolves
in sheep’s clothing have adopted. First, the law’s objectives are still
binding, and the State must see to it that these objectives are achieved.
Second, the means established by Old Testament law to achieve these
objectives are rejected as being old fashioned or inappropriate for to-
day’s complex society, namely, men acting as individuals or as agents
of the church, voluntary charitable societies, or families. In short, he
proposes what virtually all academic Christian social commentators
have proposed in this century: the substitution of the State for society. It is
a common error in this century, and an exceedingly pernicious
one.!9

Wright states that “there are societies where the conditions of
allegedly ‘free’ employees are pitiably more harsh and oppressive
than those of slaves in Israel,”?° (He does not mention the giant slave
societies created by the Communists.) “In such situations, the
paradigmatic relevance of the Old Testament economic Jaws con-
cerning work and employment can be taken almost as they stand. To
introduce statutory rest days and holidays, statutory terms and con-
ditions of employment, statutory protection from infringement of
personal rights and physical dignity, statutory provision for fair

17. Gladwin, in Clouse, op. cit, p. 181.

18, Christopher J. H. Wright, An Eye for an Eye: The Place of Old Testament Ethics
Today (Downers Grove, Llinois: InterVarsity Press, 1983), p. 89.

19. Robert A. Nisbet, The Quest for Community (New York: Oxford University
Press, 1952), p. 99.

20. Wright, pp. 79-80.
