The Yoke of Co-operative Service 131

political scientist James Wilson says, “I cannot imagine any collec-
tive action we could take consistent with our civil liberties that would
restore a moral consensus . . .”8

There is one step, however, which could be taken without violat-
ing civil liberties. In fact, it would increase civil liberties by reducing
the size of the State. It is the step which’ the politicians believe that
they dare not consider, yet which must eventually be considered: the
abolition of all forms of State welfare payments, especially aid to de-
pendent children. This is the recommendation of Charles Murray,
whose 1984 book, Losing Ground, reveals the extent of the moral and
social bankruptcy of the Federal welfare programs. Murray makes
clear what is taking place. The State is subsidizing immorality, and
immorality is. disrupting the society, In 1960, approximately 224,000
children in the United States were born to single mothers; in 1980,
over 6653000 of these children were born.’ This increase has been
especially marked within the black community. From 1950 through
1963, just before the “Great Society's War on Poverty” began, black

- legitimate births rose slowly from 17% of all black births to 23% . In

1986, a staggering 48% of all live births among blacks were to single
women." Furthermore, a growing proportion of all illegitimate chil-
dren are being born to teenagers."! This, it should be pointed out, has
taken place during the period in which compulsory “sex education”
courses were being established in the. government school systems.

In 1950, about 88% of white families consisted of husband-wife
households, and about 78% of black families did. In:a single year,
1968, the percentage for black families slipped from 72% to 69% , and
in the next five years, it dropped another six percentage points. By the
end of 1980, the proportion was down to 59%.' As Murray says, “a
change of this magnitude is a demographic wonder, without precedent
in the American experience.”"3 “As of 1980, 65 percent of all poor
blacks who were living in families were living in families headed by a
single female. The’ parallel statistic for whites was 34 percent.”

8, James Q, Wilson, Thinking About Crime (New York: Basic Books, 1975), p.
206.

9. Charles Murray, Losing Ground: American Social Policy, 1950-1980 (New York:
Basic Books, 1984), pp: 125-26.

10. Did., p. 126.

UL, Mbid., p. 127.

12. Ibid., pp. 129-30.

13. Zbid., p. 130.

14. Ibid., p. 132.

 

 
