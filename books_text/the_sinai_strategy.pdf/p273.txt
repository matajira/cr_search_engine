The Economic Implications of the Sabbath 247

that the same situation still exists. The debates went on in the Refor-
mation, too. The Old ‘Testament sabbath laws were absolute in the
sanction involved — the death penalty — and they were negative in effect.
‘They told men what not to do, one day in seven. The New Testa-
ment’s emphasis shifted on the day of resurrection. The first day of
the week is now a day of communion between God and His church.
It involves a positive, loving corporate celebration. It involves
preaching (Acts 20:7-12), singing (Matt. 26:30; Col. 3:16), praying
(I Cor. 14:15), and a communion feast (I Cor. 11).

The testimony of the church is that there is indeed a very special
day of celebration, of feasting and sharing the blessings of salvation,
If the early church in Israel had wanted rest more than the experi-
ence of true communion, it would have met for communion on the
Hebrew sabbath, since the Roman authorities acknowledged the
right ‘of the priests to require a day of rest. But the early church
broke with rest on the first day of the week in order to celebrate com-
munion on the evening of that first day. They rested on the Hebrew
sabbath, worked on the Lord’s day, and gathered together in the
evening. They rested— assuming they did rest, which seems reason-
able—on a day different from the day of worship, at least in Israel.
In gentile cities in the Empire, they probably could not rest even one
day in seven. But they celebrated on Sunday evening after work.!°

The historical circumstances of the early church necessitated
compromises with the sabbath principle. Had there been no break
from the Old Testament requirement of a full day of rest one day in
seven, the church would have been bottled up in Israel, since the
Roman Empire did not honor the rest principle. Had the legal
obligation of resting on the sabbath been the binding obligation,
then the early church, dwelling in Israel, would have had to take two
days off: the Hebrews’ day (legally binding) and the Lord’s day
(religiously binding). But this would have violated the more impor-
tant pattern of one day of rest and six days of labor.!! The church, in
short, was forced ta break with the Hebrew sabbath. God, in His
grace, abolished the Hebrew sabbath on the day of resurrection, so

10, “It is certain that the eucharist was at first an cvening meal. The name (dep-
non) implies this.” Wilfrid Scott, in Roger T. Beckwith and Wilfrid Stott, The Chrés-
tian Sunday: A Biblical and Historical Study (Grand Rapids, Michigan: Baker Book
House; 1980), p. 89.

11. Tam defining “merciful labor” as that activity which gives rest to others, both
animals and humans, I argue in this appendix that it is morally and legally valid to
self merciful labor on the Lord’s day.
