166 THE SINAI STRATEGY

Future-Orientation

Ina society in which the rights of private property are honored,
men can make decisions concerning their assets that will influence
future generations in a conscious, calculating way. Familistic capital
is protected by the prohibition against theft. Men’s time perspectives
can then focus on the long-term prospects of their capital, just as
Abraham's vision did. Which system of property management tends
to be more concerned with the future, private ownership or com-
munal ownership? Demsetz addresses himself to this issue, and he
concludes that private ownership tends to be far more future-
oriented. By communal ownership, he means “a right which can be
exercised by all members of the community.”*! He points to a phe-
nomenon made famous by biologist Garrett Hardin, the “tragedy of
the commons,”* although he does not use this terminology. “Sup-
pose that land is communally owned. Every person has the right to
hunt, till, or mine the land. This form of ownership fails to concen-
trate the cost associated with any person’s exercise of his communal
right on that person, If a person seeks to maximize the value of his
communal rights, he will to tend to overhunt and overwork the land
becausé some of the costs of his doing so are borne by others. The
stock of game and the richness of the soil will be diminished too
quickly.”# People may agree to reduce the demands they are mak-
ing, as individuals, on the land, but the costs of negotiating are high,
and so are the casts of policing the agreement,

“If a single person owns the land,” says Demsetz, “he will attempt
to maximize its present value by taking into account alternative
future time streams of benefits and costs and selecting that one
which he believes will maximize the present value of his privately
owned rights. We all know that this means that he will attempt to
take into account the supply-and-demand conditions that he thinks
will exist after his death, It is very difficult to see how the existing

41, Demsetz, “Toward a Theory of Property Rights,” in Economics of Property
Rights, op. cit., p. 37.

42, Garrett Hardin, “The Tragedy of the Commons,” Science (13 Dec. 1968);
reprinted in Garrett de Bell (ed.), The Enutronmental Handhaok (New York: Ballen-
tine, 1970). Hardin calls for more government control over pollution and resource
depletion. In contrast, C. R. Batten.cails for css government control aod greater at-
tention to defining private property rights: Batten, “The Tragedy of the Commons,”
The Freeman (Oct. 1970).

43, Demsewz, in Economics of Property Rights, p. 38.
