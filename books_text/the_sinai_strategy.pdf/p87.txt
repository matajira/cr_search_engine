Oaths, Covenants, and Contracts él

be seen to be a misuse of God’s name. Yet it may not be. The under-
lying message is theologically profound: without subordination
under God men will find themselves subordinate to the tyranny of
other men, The acceptance of this principle by the Christian West
has always been the foundation of Western liberty. !* The phrase, “by
God, they will be governed,” is a warning—the forceful communi-
cation of a political condition to be avoided at all costs.

Thus, to require civil punishment for every possible infraction of
the third commandment would be to require perfection on the part
of the listeners and the judges. It would also waste precious public
resources in endless litigation. But there is no doubt that continued
obscenity, as well as illegitimate cursing, must be regarded as an at-
tack on Christian social order. A jury of twelve people (the Anglo-
Saxon traditional number) has the right to evaluate the cir-
cumstances and contexts, and then make a civil judgment.

Language and Dominion

James Jordan argues that language is the first stage of dominion.
“If we do not have a word for a certain thing, we cannot readily come
to grips with it.” Language is also the first stage of creativity. “God
created via the Word. All cultures and societies are shaped by verbal
concepts. This is why proclamation of the Gospel is the foundation
stone of Christian society.”1©

When rebellious men seek to infuse their language with power,
they often turn to the use of obscenities. Rushdoony links this with
the pagan quest for power from below. “Godly oaths seek their
confirmation and strength from above; ungodly swearing looks
below for its power. Its concept of the ‘below’ is Manichaean to the
core: it is material. Hence, ungodly swearing finds its power, its
‘below, in sex and in excrement. The association is significant. Even
while protesting the ‘Puritanism’ of Biblical morality, the ungodly
reveal that to them sex and excrement are linked together as powers
of the ‘underworld’ of the unconscious, the primitive, and the
vital.”17

i4. R. J. Rushdoony, The Foundations of Social Order: Studies in the Creeds and Councils
of the Early Church (Fairfax, Virginia: Thoburn Press, [1968] 1978), ch. 7: “The
Council of Chalcedon: Foundation of Western Liberty,”

15. Jordan, Law of the Covenant, p. 132.

16, ibid., p. 133.

17. Rushdoony, Institutes, p. 109.
