What Is the ICE? 367

We publish the results of our findings in the newsletters.

The ICE sends out the newsletters free of charge. Anyone can sign up
for six months to receive them. This gives the reader the opportunity
of seeing “what we're up to.” At the end of six months, he or she can
renew for another six months.

Donors receive a one-year subscription. This reduces the extra
trouble associated with sending out renewal notices, and it also
means less trouble for the subscriber.

There are also donors who pledge to pay $10 a month. They are
members of the ICE’s “Reconstruction Committee.” They help to pro-
vide a predictable stream of income which finances the day-to-day
operations of the ICE. Then the donations from others can finance
special projects, such as the publication of a new book.

The basic service that ICE offers is education. We are presenting
ideas and approaches to Christian ethical behavior that few other or-
ganizations even suspect are major problem areas. The Christian
world has for too long acted as though we were not responsible citizens on earth,
as well as citizens of heaven, (“For our conversation [citizenship] is
in heaven” [Philippians 3:20a].) We must be godly stewards of all our
assets, which includes our lives, minds, and skills.

Because economics affects every sphere of life, the ICE's reports
and surveys are relevant to all areas of life, Because scarcity affects every
aréa, the whole world needs to be governed by biblical requirements
for honest stewardship of the earth’s resources. The various publica-
tions are wide-ranging, since the effects of the curse of the ground
(Genesis 3:17-19) are wide-ranging.

What the ICE offers the readers and supporters is an introduc-
tion to a world of resporisibility that few Christians have recognized.
This limits our audience, since most people think they have too
many responsibilities already. But if more people understood the
Bible’s solutions to economic problems, they would have more
capital available to take greater responsibility and prosper from it.

Finances
There ain’t no such thing as a free lunch (TANSTAAFL). Some-
one has to pay for those six-month renewable free subscriptions, Existing
donors are, in effect, supporting a kind of intellectual missionary
organization. Except for the newsletters sent to ministers and
teachers, we “‘clean” the mailing lists each year: less waste.
