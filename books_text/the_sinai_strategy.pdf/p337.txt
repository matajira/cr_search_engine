Covenantal Law and Covenantal Love Sil

you again. And why beholdest thou the mote that is in thy brother's eye,
but considerest not the beam that is in thine own eye? Or how wilt thou say
to thy brother, Let me pull out the mote out of thine eye; and behold, a
beam is in thine own eye? Thou hypocrite, first cast out the beam out of
thine own eye; and then shalt thou see clearly to cast out the mote out of thy
brother’s eye (Matt, 7:1-5).

Notice what He did not say. He did not say that it is all right to go
through life with motes (small chips) in our eye. The eye is the most
sensitive organ in the body. A mote in an eye could blind it, or seri-
ously interfere with our vision. Jesus did not condone sin in any
form, Sin is 2 horror; it jeopardizes our very existence. It should not
be allowed to remain in your eye, and we are required by God to do
what we can to help remove chips from our neighbors’ eyes. We are
to use biblical law to assist them. What Jesus was saying is that we
need to be constantly on the lookout for motes in other people’s eyes,
so that we can help them remove them. But to accomplish this, we must
first get rid of the beams in our own eyes. We must be able to go to
the other person and tell him; “Look, I used to have a really bad
beam in my eye, and it blinded me. But through the grace of God, I
was able to remove it. I see that you're suffering from the same
thing. Let me show you how God’s word speaks to your minor prob-
lem, just as it spoke to my major one.” In other words, “I've been
there. I know what it is. It leads to blindness and agony.”

This is the approach of the most successful alcoholic rehabilita-
tion program in history, Alcoholics Anonymous. When a man at last
chooses to become sober, and is faced with a terrible craving to
drink, he calls his sober friends who were former alcoholics. He goes
to those who have suffered what he is suffering. He doesn’t telephone
his Aunt Tilly, who never touched a drink in her life, unless Aunt
Tilly has a known prayer life which produces healings and near-
miracles whenever she prays. Besides, he probably already asked
Aunt Tilly to pray for him, and he still got drunk. So he calls the
men who suffered from beams (Jim Beams?) and who successfully
solved their problem.

There is a tendency in twentieth-century fundamentalism,
evangelicalism, and pietism. for law-hating, responsibility-avoiding
Christian people to piously assert, “It is not our responsibility to
judge. We must show mercy to everyone. We are sinners, too.” This
is the worst kind of hypocrisy. What they are really saying is that they
