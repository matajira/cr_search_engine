Oaths, Covenants, and Contracts 59

the self-maledictory oath have the lawful authority under God to ex-
tract a God-naming oath:!! Nevertheless, these three monopoly gov-
ermments do possess this authority. To deny this authority to them is
implicitly to assert the autonomy of man. This is why the Westminster
Confession of Faith (1646) states: “Yet it is a sin to refuse an oath
touching any thing that is good and just, being imposed by lawful
authority” (XXIL:1ED.

Such a denial has been made at various times by various
Anabaptist groups from their beginnings in the carly sixteenth cen-
tury. In the name of a “higher obedience to the words of Jesus,” they
refuse to invoke God's name and therefore simultaneously refuse to
place themselves under God’s potential curses, as administered on
earth by the civil government. They thereby implicitly assert the au~
tonomy of the individual conscience from any .God-ordained civil
government. Sometimes radical Anabaptists have explicitly made
this assertion of their own autonomy, but it is always implicit in their
theology.

 

Penalties

The question eventually must be raised: Is it a criminal offense to
take the name of the Lord in vain? When people curse their parents,
it unquestionably is a capital crime (Ex. 21:17), ‘The son or daughter
is under the lawful jurisdiction of the family. The integrity of the
family must be maintained by the threat of death. Clearly, cursing
God (blasphemy) is a comparable crime, and is therefore a capital
crime (Lev. 24:16).

What about the integrity of the church? What if someone who is
not a member of the church publicly curses the church? Is the State
required to apply the same sanction? The person may not be
covenantally subordinate. to the particular church, or any church,
unlike the subordinate child who curses a parent. There is no specific
reference to any civil penalty for cursing anyone but a parent or
God, nor is there any civil penalty assigned for using God’s name in
vain. Then is there a gencral prohibition against cursing? On what
grounds could a church prosecute a cursing rebel?

One possible answer is the law against assault, Battery involves

11. In the Old ‘lestament, it appears, the father of che bride was the sovereign
agent of a marriage, Thus, marriage vows were taken before him by the couple. See
James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler, Texas:
Institute for Christian Economics, 1984), p. 150.
