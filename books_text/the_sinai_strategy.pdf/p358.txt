332 THE SINAI STRATEGY

E. R. Geehan, Jerusalem and Athens: Critical Discussions on the Philosophy
and Apologetics of Cornelius Van Til (Presbyterian & Reformed, 1971), re-
vealed just how hostile he was to biblical presuppositionalism, He
replied to Van Til's criticism of his work as not going far enough in its
confrontation with “natural law” doctrines. He, too, used the same
old tactic: “. . . you have misunderstood what I mean . . .” (p. 74).
No, Dr, Van Til understood precisely what Dooyeweerd meant—a
magisterial accomplishment, given the frequently obscure nature of
Dooyeweerd’s verbiage. (I agree entircly with Nash’s observation re-
garding Dooyeweerd: “Good thinking is never complimented by and
should not be accompanied by poor communication,”?)

Dooyeweerd’s system is a collection of philosophically empty “self
attesting” boxes (categories supposedly derived from logic, not the
Bible) into which anyone can pour any content whatsoever. This is
especially true of the political and economic categories. Nash is cor-
rect: “Apart from his presupposition that the cosmos is a divinely cre-
ated world order, it might be objected that his law spheres are only
fabrications of his own mind.” Most of his followers have poured so-
cialism and antinomianism into these empty boxes. In fact, I con-
tend that it was the very emptiness of Dooyeweerd’s categories which
attracted his followers~and his verbiage, which they have devel-
oped into an art. (Doubt me? Take a look at almost any book pub-
lished in Canada by Wedge Books.)

Van Til put his finger on the problem when he wrote that “the en-
tire transcendental method hangs in the air except for the fact that it
rests upon the fullness and unity of truth accepted on the authority of
Scripture,”* Dooyewecrd’s system hangs in the air because it does
not begin with the presupposition of the necessity and adequacy of
biblical revelation for all philosophical inquiry. In short, argued Van.
Til, either you start with the Bible as your standard, or you begin
with man’s mind as the standard. You will inescapably end up with
whatever you began with presuppositionally. Dooyeweerd’s whole
system does not begin with the sclf-attesting authority of the Bible.

2. Ronald H. Nash, Dooyeweerd and the Amsterdam Philosophy: A Christian Critique of
Philnsophical Thought (Grand Rapids, Michigan: Zondervan, 1962), p. 105.

3. Ibid., p. 104.

4, This criticism appeared in the little-known syllabus by Van ‘T'll, Christianity in
Conflict, Volume UI, Part 3, “Biblical Dimensionalism,” a 59-page, single-spaced cri-
tique of Dooyeweerd and the Amsterdam school, Anyone who would like a photo-
copy of this essay can order it from Geneva Ministries, 708 Hamvasy, ‘lyler, Texas
75701; $10.
