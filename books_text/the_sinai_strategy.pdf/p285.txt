The Economic Implications of the Sabbath 259

should be pointed out) in all of its pre-industrial rigor, then he
should encourage his elders to enforce the provisions. Of course, the
provisions of the Confession do not even approach the requirements
of Numbers 15, Exodus 31:15, and Exodus 35:2-3, i.e., the true bibli-
cal standards in the eyes of a consistent sabbatarian, but at least they
are something. If the creeds are valid in their 1646 interpretation,
then 1646 standards of enforcement ought to be applied. If such
standards are not applied, then it is a clear admission that the church
no longer recognizes as valid the 1646 definition of the sabbath.

Buying Fuel

Let us pursue the charge against the “restauranteers” with rigor.
Those same people who make the charge pride themselves on their
Lord's day observance because they do not go out to restaurants on
the Lord’s day. They do not shop in supermarkets. They have stored
up provisions to eat at home. Prior shopping is quite proper, if one is
a sabbatarian, for it is of the very essence of Lord’s day-keeping that
one store up provisions in advance of the Lord’s day. But the Old
Testament required more than the mere storing up of food. The
passage we have referred to, Numbers 15, makes it explicit that not
only food but the fuel was to be stored up in advance; fuel for heating
the home, cooking the meals, and lighting the room had to be pro-
cured in advance, It was a capital offense m the eyes of a righteous
and holy God to gather sticks —fuel-on His sabbath,

The modern Puritan-Scottish sabbatarian thinks that his is the
way of the holy covenant of God simply because he buys his food
early, and cooks it on Sunday, while he regards his brother in Christ
as sinning because the latter eats at a restaurant on Suiday. But
under the provisions of Numbers 15, both crimes appear to be
equally subject to death, for both the restaurant-goer and the meal-
cooker have paid specialized fuel producers to work on the Lord’s day.
There is this difference, however: the man who enters the restaurant
is not self-righteous about his supposed keeping of the Lord’s day,
and he has made no charges against his fellow Christians. He would
seem to have violated the sabbath provisions of Numbers 15, but that
is the ‘extent of his guilt. The modern sabbatarians I have met too
often violate the Lord’s day and the commandment against gossip,
or at least they indulge in the “judgment of the raised eyebrow and
clicking tongue.” They neglect Christ’s warning: “Judge not, that ye
be not judged. For with what ye judge, ye shall be judged. . .”
