92 THE SINAI STRATEGY

the temptation of dating Christ's return, something which has
always led the church into the pitfall of short-term thinking and
planning. At this point, we are not sure what these blessings will be.
What we are sure of is that the Jews as a covenantal, identifiable
{but not necessarily national) people will be converted to Christian-
ity before the end of time, and that historically unprecedented bless-
ings will be the direct result.

The best answer is: we cannot be sure in advance. If blessings
abound, if revival comes, and if we can reasonably date the changes
around 2000 (or 2070), then we can imagine the end of time around
3000, but we cannot be sure. Symbolism is not “chronology in ad-
vance.” Expectations are one thing; precise timetables are another.

Conclusion

The sabbath points to the fulfillment of the dominion covenant,
as well as the judgment by redeemed mankind of the enemies of
God. The rest which was long ago promised by God is symbolized in
the sabbath. A weekly sabbath is God’s “earnest”— His down pay-
ment—on the cosmic sabbath to come. Ours is a Firstday sabbath,
or Sunday sabbath, in New Testament times. We begin the week
with rest, as Adam was supposed to but did not. Adam wanted to
create by his own efforts the conditions of man’s rest, and he never
rested again.

The economic implications of the sabbath are extensive. This is
why of necessity I have added an appendix on the topic. The key
question, however, is this: Jn New Testament times, where is the locus of
Sovereignty for the enforcement of sabbath law? If I am correct in my con-
clusion that Paul has lodged this sovereignty with the individual con-
setence rather than with church government or civil government, then
there is no legitimate role in New Testament times for “blue laws,” or
other sabbatarian legislation. This conclusion represents a major
break with historic Protestantism and should be understood as such.
It is a major theological step which needs to be discussed in detail by
Christian commentators.

If commentators decide that mine is not a legitimate conclusion
from Paul's writings, then the locus of sovereignty issue must be
dealt with in detail. Who is to impose sanctions? What sanctions?
Under what conditions? How will those who must impose sanctions
deal with the multiple economic problems raised by compulsory leg-
islation? These problems are discussed in greater detail in Appendix
