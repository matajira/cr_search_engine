Soctal Antinomianism 327

on which we can rely. And so we must sit quietly and wait for the
mystical intervention of the Holy Spirit to guide us in all of our pri-
vate community decisions; God has seen fit to leave us without any
concrete standards in such matters. This, I am compelled to con-
clude is antinomianism. It is strangely like the mystical brand of
Christianity that is called Penielism. I am unable to see how it is
even remotely Reformed.

This does not mean that Troost has.no recommendations for the
contemporary world. Naturally, he does not derive them from the
Bible, and apparently the “common sense” of the unregenerate world
has given him no aid. In fact, he does not specify any source for his
recommendations. Nevertheless, he is able to conclude that “It is
part of our religion to engage whole-heartedly in the battle for a just
distribution of income (nationally, but also internationally, through
foreign aid), for just property relations, and for a just economic
order, Iris part of our religion because we are called to it by Him
who gave his life for this world . . .” (p. 35). Unfortunately, he does
not specify which sphere of life is involved here. Does he mean merely
that the church should give private charity (a teaching made explicit
by the Scriptures), or does he mean that as Christians we are obli-
gated to promote the political projects of land redistribution and for-
eign aid sponsored by our civil governments? If he means simply pri-
vate charity, then he is saying nothing new. If he means public proj-
ects of political coercion, then he must show us on what grounds
such a conclusion is justified; certainly the Bible teaches no such doc-
trine, and even if it did, roost does not accept the Bible’s testimony
in such matters.

He goes on: “lhe World Council of Churches itself is sponsoring
a study ona large scale dealing with society and social problems, in
connection with which a book is to appear entitled The Theological
Foundation of a Christian Social Ethics. Unfortunately it appears to me
that historic Reformation Christianity (‘Calvinism’) is not making
much of a contribution to this study and reflection” (p. 36). Naturally,
the World Council can engage in such activities; it is a humanistic
organization which is not bound to work within the framework of
limits established by the Bible. It has no difficulty in producing all
the humanistic, secular documents that it wants to distribute. But
given the presuppositions which Troost holds, that the Bible offers
no concrete social proposals, and that “common sense” of the fallen
world is equally helpless in aiding the thinker in his work, how could
