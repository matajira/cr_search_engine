62 THE SINAI STRATEGY

What actually takes place is that this particular quest for power
from below brings verbal impotence. The shocking words cease to
shock. They become boring. Yet the speaker finds that he can no
longer express himself without them. He has grown accustomed to
calling attention to his ideas and opinions by means of obscenities,
but his language becomes steadily dcbased. His words no longer at-
tract. They even repel.

Henry Miller wrote books that shocked the public, and brought
in the censors, in the middle third of the twentieth century. His
Tropic of Cancer and other books were suppressed for a time as porno-
graphic. He defended his books against this accusation, What he
was, he claimed, was a writer of obscenity, not pornography. He
stated boldly that Tropic of Cancer “is not a book. This is libel, slander,
defamation of character. This is not a book, in the ordinary sense of
the word. No, this is a prolonged insult, a gob of spit in the face of
Art, a kick in the pants of God, Man, Destiny, Time, Love, Beauty

. what you will,”18

There can be no doubt about Miller’s impulse. It was religious to
the core. He had in mind the salvation of man. In a well-titled 1946
essay, “The Time of the Assassins,” he stated his soteriology quite
well: “The road to heaven leads through hell, does it not? To earn
salvation one has to become innoculated with sin. One has to savour
them all, the capital as well as the trivial sins. One has to earn death
with all one’s appetites, refuse no poison, reject no experience how-
ever degrading or sordid.”'® This theology is familiar. It is a resur-
tection of the chaos cults of the ancient world.

His vision of the artist was messianic, but always within the
framework of the religion of revolution: “Ultimately, then, he [the
artist or poet] stands among his own obscene objurations like the
conqueror midst the ruins of a devastated city. He realizes that the
real nature of the obscene resides in the lust to convert, . . . Once
this vantage point is reached, how trifling and remote seems the ac-
cusations of the moralists! How senscless the debate as to whether
the work in question was of high literary merit or not! How absurd
the wrangling over the moral or immoral nature of his creation!”2¢
In short, “I am my own savior.”?!

18. Henry Miller, Tropic of Cancer (New York: Grove Press, [1934] 1961), p. 2.

19. Miller, “The Time of the Assassins” (1946), in Selected Prose (London: Macgib-
bon & Kee, 1965), II, p. 122.

20. Miller, “Obscenity and the Law of Reflection,” (1947), in ibid., I, p. 366.

21. Miller, The World of Sex (New York: Grove Press, 1959), p. 20.
