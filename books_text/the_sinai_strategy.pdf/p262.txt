236 THE SINAI STRATEGY

ing, even though milk in this instance was a by-product of acts of
mercy? A strict sabbatarian would clearly have to insist that milk
which is produced as a by-product of an act of mercy be given to the
poor, or spilled on the ground, in order to make certain that such
merciful work remained exclusively merciful and not an excuse for
profit-seeking (cost-reducing) sabbath violations,

Then there is the question of full pay for “normal” works of mercy
or necessity performed by professionals, including people who are
paid by the civil government: police, firemen, military forces, ete.
Should those who perform such services on the sabbath be paid for
that day’s work? Christ defended the right of a man to pull a beast of
burden out of a ditch, but does this imply that individuals can legiti-
mately operate “beasi-retrieval” companies at a profit on the sab-
bath? Thomas Gouge, a contemporary of Owen and Baxter in
seventeenth-century England, praised as shining examples several
Christian physicians who refused payment for Sunday labor.? More
than this: Should the civil government make it illegal for people to
receive payment for emergency services? And if it does, won't this
reduce the number of emergency services offered, and thereby ren-
der it more dangerous to suffer an emergency on Sundays? These
are questions that strict sabbatarians must cventually deal with.

B. The Division of Labor in Rural Israel

Modern mass production, with its capital-intensive mechaniza-
tion, is characterized by a high division of labor. Until the late-
nineteenth century, agricultural societies were characterized by a
comparatively low division of labor. In such societies, production is ini-
tially for the family unit, Surplus goods can be traded or sold, but there
is not much surplus. Men work primarily for horne consumption,

The workweck is scheduled in terms of the needs of the family.
Wives can bake extra loaves on the day before the sabbath without
disrupting normal production and distribution patterns. Husbands
can cut extra wood for the fire on any day of the week. In ancient
Isracl, people structured their workweek’s rhythm in terms of the
sabbath. This did not involve a major interruption of supplies of
needed goods and services. Where men are not continually serving
each other through production for a market, but where they serve

 

 

2, On Gouge, see Richard Schlatter, The Sucial Ideus of Religious Leaders, 1660-1688
(London: Oxford University Press, 1940), pp. 129, 137.
