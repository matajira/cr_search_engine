The Rights of Private Property 149

Falling prices are not the end of the story. Second, prices subse-
quently start to rise, because buyers can no longer locate sellers of
new goods. Too many sellers have gone out of business. The
burglars hit them, too. The costs of production rose for them, since
producers are owners, too. Furthermore, thieves find that owners
have bought burglar alarms, locks, and guard dogs. The costs of be-
ing a burglar also rise, so there is Jess burglary. The availability of
stolen goods drops. The initial discounts disappear. Stolen goods
start to command higher prices. Fewer goods are bought and sold,
but for those necessities that do remain on the shelves, their prices
will be much higher. Consumer dollars will be chasing a smaller number of
goods, so prices of these goods tend to rise. If the crime wave persists,
prices of goods actually brought to market rise higher than they had
been before, since fewer goods are available. Most people continue
to be worse off as a result of the crime wave.

We need to ask ourselves; How are poor people benefited if
prices-are pushed inifially lower by criminal behavior (reduced de-
mand coupled with lower prices for stolen goods)? How are they
benefited when the uncertaintics associated with theft must be dealt
with? What benefit is the high rate of theft in, say, New York City’s
black ghetto, Harlem? I have visited apartments in Harlem, with
their expensive doors and intricate locks. It is profitable for sellers of
anti-burglary devices, but not for any other law-abiding citizen.
Prices of other consumer goods are initially lowered because of
money that must be spent on locks, burglar alarms, and insurance.
But they do not remain low, Buyers need to lure sellers into high-risk
markets where theft is common. People in Harlem wind up having
to pay far higher prices than in other areas of New York City because
costs of doing business are high (you might get killed), and it is ex-
pensive to lure sellers into the area. Consumer choices are drastically
limited, there are no supermarkets in Harlem; only small “mom and
pop” stores that issue credit and know their customers. Harlem's
problem jis not capitalism; -Harlem’s problem is that too many
criminals and people with short-run perspectives live (and prey)
there. 13

What if the crime wave ends? We now come to phase three. There
will still be an increase in prices, as‘buyers seek to lure back potential

13. On short-run perspectives in black ghettos and the grim effects, see Fdward
Banfield, The Unheavenly City: The Nature and Future of Our Urban Crisis (Boston: Little,
Brown, 1970), pp. 53-54, 124-28,
