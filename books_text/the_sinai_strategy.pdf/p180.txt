154 THE SINAI STRATEGY

means the abolition of private property) when it abolishes the property
qualification for electors and representatives. ...Is not private
property ideally abolished when the non-owner comes to legislate for
the owner of property? The property qualyication is the last political
form in which property is recognized.”*

Lord Macaulay, the English historian-statesman of the mid-nine-
teenth century, was a defender of the classical liberal society, mean-
ing a society marked by constitutionally limited civil government
and by private property. In 1857, he wrote a letter to an American,
H. S. Randall, in which he discussed his doubts about pure democ-
racy in general, and Jeffersonian democracy in particular. He made
a number of predictions concerning the fate of private property
under a rule of universal suffrage. Some of these predictions have
come true in the United States; they did so.during the New Deal of
the 1930's. Other remarks seem more appropriate in describing his
beloved England, especially since the 1930's.

‘The fundamental issue, he argued, is the question of self-restraint,
or as I have put it elsewhere in this book, self-government. He de-
spaired at the ability of the poorer members of society to refrain from
using their numerical superiority at the ballot box to extort the prop-
erty of richer men. Because of the difficulty in obtaining copies of the
book in which this letter appeared, I have decided to reproduce it in
full, except for a brief introductory. paragraph, in which Macaulay
thanked Randall for his gift of some books on the history of colonial
New York State, and a concluding paragraph on Thomas Jefferson.
The doubts raised by Macaulay are with us still, and will continue to
be problems for stable political orders for as long as: 1) all men can
vote; 2) some men have little property; 3) the Christian teachings
against envy, covetousness, and theft are not universally honored. (I
have taken the liberty of breaking this letter into paragraphs; the
original constitutes the longest sustained paragraph I have ever
come across.) Macaulay wrote:

You are surprised to learn that I have not a high opinion of Mr.
Jefferson, and T am surprised at your surprise. I am certain that I never
wrote a linc, and that I never, in Parliament, in conversation, or even on
the hustings—a place where it is the fashion to court the populace— uttered
a word indicating an opinion that the supreme authority in a state ought to

20. Karl Marx, “On the Jewish Question” (1843), in 'I. B. Bottomore (ed.), Karl
Mars; Early Writings (New York: McGraw-Hill, 1964), pp. 11-12.
