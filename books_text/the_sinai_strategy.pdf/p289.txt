The Economic Implications of the Sabbath 263

whole of Western industrial civilization. It will certainly involve the
questioning of the last two centuries of rapid economic growth. Strict
sabbatarians should at least be aware of the possible effects of their
proposals. If the world should be conformed to Christian standards
of biblical law, and if the standards of the Hebrew sabbath practice
are, in fact, still the rule for the Christian dispensation, how would
these standards be imposed on the population at large? Would it not
make impossible our modern version of industrial, specialized soci-
ety? In other words, if such standards had been enforced for the past
two centuries, could this civilization, which most modern Christians
accept as far as its technological conveniences are concerned, have
come into existence? How much of our economically profitable, effi-
cient, “sabbath-desecrating” technology would we have been forced
to prohibit by civil law? The costs, I suspect, would be considerable.
It is time for strict sabbatarians to count those costs.

Fire has served as man’s major technical tool of dominion, and
has been challenged as a primary tool only in the twentieth century,
first by electricity and then by the electronic computer.” Lewis
Mumford has discussed the three-fold uses of fire: light, power, and
heat. His essentially evolutionistic interpretation ‘could easily be
reworked to conform to biblical imagery. “The first artificially over-
came the dark, in an environment filled with nocturnal predators;
the second enabled man to change the face of nature, for the first
time in a decisive way, by burning over the forest; while the third
maintained his internal body temperature and transformed animal
flesh and starchy plants into easily digestible food. Let there be light!
With these words, the story of man properly begins.” Thus, fire has
been basic to the dominion covenant from the beginning. That the
kindling of a sabbath fire was prohibited in the Old Testament is un-
derstandable; it is the very essence of work. To kindle a fire, or to
gather sticks for a fire, would have symbolized man’s autonomy in
the dominion process, the essence of lawlessness, Furthermore, as
symbolic of God’s glory cloud, fire unquestionably served the
Hebrews as a reminder of God’s power, in addition to being a pri-

27, Jeremy Rilkin, Aigny (New York: Viking, 1983), ch. 1.
28. Lewis Mumford, Znterpretations and Forecasts: 1922-1972 (New York; Harcourt
Brace Jovanovich, 1973), p. 425.
