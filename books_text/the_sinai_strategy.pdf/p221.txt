10
COVETOUSNESS AND CONFLICT

Thou shalt not covet thy neighbour's house, thou shalt not covet they
neighbour's wife, nor his manservant, nor his maidservant, nor his ox,
nor his ass, nor anything that is thy neighbour’s (Ex. 20:17).

Covetousness, biblically speaking, refers to an illicit craving of
another person’s possession, including his station in life. It can also
involve the actual theft of someone else’s property, either by force or
by fraud ( Josh. 7:21; Mic. 2:2). The Westrninster Larger Catechism
(1646) devotes only a few lines to the tenth commandment, having
covered outright theft in the eighth commandment: “Lhe sins forbid-
den in the tenth commandment are, discontentment with our own
estate; envying and grieving at the good of our neighbour, together
with all inordinate motions and affections to any thing that is his”
(Answer 148), The Westminster Assembly understood the centrality of
envy in all covetousness: the desire to hurt the person who is better off,
as a result of grieving because of another's success or benefits.
Nevertheless, covetousness involves more than envy.

The term “envy” has several connotations. First, in American
slang, it is used harmlessly. Upon learning of another person’s job
promotion, or success in some area, the speaker may say, “I sure
envy your good luck.” The speaker is not saying that he wishes that
the other person were not successful. He is saying only that he appre-
ciates the magnitude of the other person’s success, and that he would
like ‘ta be equally successful. Generations of Christian’ preaching
against the sin of covetousness have produceda harmless usage for
the term.

A second connotation would better be described by the modern
usage of the English word jealousy. When a person says that another
person is envious, he really means that the other person is jealous.
The other person is said to desire someone else’s property or station

195
