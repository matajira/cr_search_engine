86 THE SINAI STRATEGY

of the libertarian restriction against life-long contracts, which most
libertarians equate with slavery and therefore reject as immoral and
illegal.*9

The Sabbath Millennium

As we approach the close of the twentieth century, and the com-
ing of a new millennium, we can expect to see a growing apocalyp-
ticism, both Christian and humanistic. The year 2000 has been a
focus of concern by humanists since at least the era of the French
Revolution.'* The year 2001 inaugurates the third millennium after
Christ, and almost simultaneously, we expect to see the seventh
millennium (if the world is just about 6,000 years old), This new
millennium can easily be correlated with the “third day-seventh day”
symbolism of rest and resurrection. We are in a very real sense ap-
proaching a new Sinai, a new sixth-day covenant which will in-
augurate the seventh-day millennium. A new Sinai should be
marked by a rediscovery of Old Testament law, which is precisely
what has happened since 1973, when Rushdoony’s /nstituies of Biblical
Law was published. For the first time in New Testament church
history, there is a systematic attempt to defend and apply the prin-
ciples of Old Testament law to New Testament society, but without
the mixture of Greek categories of natural law, If this truly is the
“evening before the Sabbath,” then we can expect the millennial sab-
bath to follow. James Jordan’s analysis of the symbolism of “third
day-seventh day” is significant in this regard:

The process of covenant renewal with man dead in sins and trespasses
must involve resurrection. . . . To be cleansed, therefore, is to undergo
resurrection. This is the meaning of the cleansing rituals of Leviticus 41-15,
and other places. The covenant can only be reestablished with resurrected
men, so the people were to cleanse themselves before the third (sixth) day,
when the covenant was to be made (Ex. 19:10-14). .. .

And so, God drew near on the third day (after God’s announcement to
Moses on the fourth day), which was the sixth day of the week, to renew
covenant with men, It was not the New Covenant that God was renewing
at Sinai, but the Old Adamic Covenant, It was the Old Covenant tem-
porarily and provisionally reestablished in the sphere of temporary, provi-
sional, ceremonial (New Covenant) resurrection. It was temporary; but
just as the original Adamic Covenant had pointed forward to sabbath rest,

13, See Chapter 7, subsection: “Libertarian Contracts.”
14. Robert Nisbet, “The Year 2000 and All That,” Commentary ( June 1968).
