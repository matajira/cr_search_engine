Appendix C
SOCIAL ANTINOMIANISM

Antinomianism — the denial of the validity of the concrete appli-
cation of Old Testament law in this era—has influenced modern
Christianity to such an extent that virtually no Christian seminary
even teaches a single course against it. Anglo-Israelite sects do pay
attention to biblical law, which is, I believe, the reason that Garner
Ted Armstrong’s “The World Tomorrow” had such a huge radio au-
dience and why he was more interesting than any orthodox Chris-
tian broadcasting in the late 1960’s and early 1970’s. He could com-
ment successfully on the collapse of modern culture because he had
concrete alternatives to offer.

Social antinomianism makes itself manifest in many ways. In the
Reformed Protestant circles, the Dutch Calvinist movement associ-
ated with the name Herman Dooyeweerd was briefly influential in
this regard, 1965-75. Always searching for the “truc Christian atti-
tude,” the radical young neo-Dooyeweerdians proclaimed almost
complete freedom from the restraining hand of concrete biblical law.
Thus, attitude is substituted for obedience to revealed law. The non-
Dooyeweerdian churchmen were unable to refute the radicals pre-
cisely because they held a similar, though less rigorous, antinomian
philosophy. Their instincts may have been conservative, but their
operating presuppositions did not allow them to challenge successfully
the young radicals. The leaders of the neo-Dooyeweerdians, located
primarily at the Free University of Amsterdam and the Institute for
Christian Studies in Toronto, combine a preference for government
intervention and orthodox Christian language. The following article
criticizes this combination. Troost’s answer appeared in the same
issue (Oct. 1967) of the International Reformed Bulletin. It did not con-
vince me. Similar terminology and identical antinomianism have
become universal in the “radical Christian” Anabaptist circles.*

*From this point on until “Troost’s Response,” this appendix is a reprint of my
original article in International Reformed Bulletin.

322
