jargon, 334-36
Jealousy, 90, 195-96
Jefferson, Thomas, 48, 154
Jeremiad, 272, 274-75
Jericho, 17
Jerusalem, 88-89, 168, 324
Jesus
activism, 227
battle, 221
covenant, 21)
dominion, 221
Dominion Man, 16
eating meal, 83
lawyer &, 307-8
name, 179
new world order, 88
oaths, 58
O. T. Law, 306-7
physician, 20-21
power of death, 119
redemption, 2
resurrection, 81
revolutionary, 221
temptations, xxii-xxiii
trial of, xxiii-xxiv
yoke, 126
Jews, 90, 142 (see also Israel)
Johnson, Lyndon, 219
Jordan, James, 18n-19n, 59n, 266n
language, 61
third day, 86-87
Joseph, 66
journey, 279-80
jubilee, xxi
jubilee year, 129
Judaizers, 243
judging, 310-11, 312-13, 321
judgment, xvi-xvii, 50
judgment day, xvi-xvii
jury, 61
justice, 216

kidnapping, 134
kill, 116

kingdom of God, 37
kingdoms, 45-46
kings, 17

Index 353

Kline, Meredith, 56n, 76n
knowledge, 151, 162, 185-86, 190, 194
Kohath, 42

Laban, 315
labor, 77, 126, 142, 143
Ladner, Gerhart, 35n
laissee-faire, 298
lamb, 45n
lambs, 239
land, 75, 85, 166, 293
Langley, Baxter, 297
language, 60
lashing, 60
Latouche, Robert, 224
law
annulment, 4
antinomianism, 318
blessings, 112
case-law, 18
death, 317
dominion &, xii
evangelism, 315
freedom, 24
government &, 319
grace, 152
hatred of, 220
history, 19
internalization, 206-7
life, 20
love &, 308-12
offensive strategy, 226
outmoded, 14
peace, 2it
permanent, 2
predictable, 15, 285
principles, 3, 306, 309
prohibition, 26
State &, 13
lawlessness, 197
law of averages, 47
law-order
biblical, 97, 202
decalogue &, 18
Egypt's, 19
polytheism, 21
social co-operation, 206
unity, 97
