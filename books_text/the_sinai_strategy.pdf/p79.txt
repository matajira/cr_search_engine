Oaths, Covenants, and Contracts 53

ments before judicial bodies exclusively appointed by God to execute
His judgment, the institutional church, the civil government, and
(under certain circumstances) the family. Each agency can legiti-
mately enforce sanctions against oath-breakers, and they do so in the
name of God, .The civil government can lawfully impose physical
sanctions, and the institutional church can lawfully impose spiritual
sanctions, The family can impose both physical sanctions (the rod)
and spiritual sanctions (the father’s blessing), There are three forms
of curses: civil, ecclesiastical, and familial.

This is not to argue that sanctions are not simultaneously spiritual
and physical. The civil government enforces its law by fines, physical
punishment, or execution. Yet these physical punishments point to a
future and permanent spiritual punishment by God, for the oath-
breaker is involved in a violation of God’s law. Civil authority is ulti-
mately derived from God, so a violation. of Bible-based civil law nec-
essarily involves a transgression of the covenant. Similarly, the
church. enforces its spiritual sanctions by separating people physi-
cally from the sacraments. This punishment points to an eternal
future punishment which involves physical separation from God and
His resurrected church—punishment which is unquestionably
physical in nature (Rev. 20:14), and not simply spiritual.

‘The essence of the third commandment is the defense of God’s
three institutional monopolies that can legitimately pronounce
curses in God’s name: the church, the civil government, and the
family. The civil government pronounces the curse of earthly punish-
ment in the name of God, and so does the family (Gen. 49:3-7). The
church pronounces the curse of efernal punishment in the name of
God. Individuals and associations other than these three monopolies
are prohibited by God from exercising autonomous power by invok-
ing God’s name in a curse.

Curses as Imperatives

Consider a familiar violation of this commandment in English,
the expletive, “God damn you.” It is used thoughtlessly, usually in
anger. Biblically speaking, the phrase is an imperative: the invoca-
tion of the ultimate biblical curse, a calling upon God to execute His
jadgment to destroy eternally a personal opponent. It should not be
understood as simply a breach of good manners, a violation of
biblical etiquette. It is a verbal expression of personal outrage or
disgust which is prohibitedprecisely because it invokes the ultimate
