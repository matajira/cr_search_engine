The Rights of Private Property 141

harmony included hierarchy, for Eve was functionally subordinate to
Adam (though not ethically inferior).? The God-designed harmony
of interests was never an equalitarian relationship. It is not equali-
tarian in the post-Fall world. The church, as the body of Christ, is
similarly described in terms of an organic unity which is supposed to
be harmonious, with each “organ” essential to the proper functioning
of the whole, yet with each performing separate tasks (I Cor. 12). All
are under Christ, the head of the church (Eph. 5:23).

God’s universe is orderly. There is a God-ordained regularity in eco-
nomic affairs. There is a predictable, lawful relationship between per-
sonal industriousness and wealth, between laziness and poverty.
“How long wilt thou sleep, O sluggard? When wilt thou arise out of
thy sleep? Yet a little slecp, a little slumber, a little folding of the
hands to sleep: So shall thy poverty come as one that travelleth, and
thy want as an armed man” (Prov. 6:9-i1). “Wealth gotten by vanity
shall be diminished: but he that gathereth by labour shall increase”
(Prov, 3:11), This applies to individuals, families, corporations, and
nations, Not every godly man or organization will inevitably prosper
economically, in time and on earth, and not every evil man will lose
his wealth during his lifetime (Luke 16:19-31), but in the aggregate,
there will be a significant correlation between covenantal faithfulness and
external prosperity. In the long run, the wealth of the wicked is laid up for
the just (Prov. 13:22). This same principle applies to national, cul~
tural, and racial groups (Deut. 8). Covenantal law governs the sphere
of economics. Wealth flows to those who work hard, deal honestly
with their customers, and who honor God. To argue, as the Marxists
and socialists do, that wealth flows in a free market social order
towards those who are ruthless, dishonest, and blinded by greed, is to
deny the Bible’s explicit teachings concerning the nature of economic
life. It is a denial of the covenantal lawfulness of the creation.

The Theology of the Welfare State
Critics of the capitalist system have inflicted great damage on
those societies that have accepted such criticisms as valid. Men have
concluded that the private property system is rigged against the poor
and weak, forcing them into positions of permanent servitude. His-
torically, on the contrary, mo social order has provided more opportunities
for upward social mobility than. capitalism, The remarkable advance of

2. Ibid., pp. 91-92.
