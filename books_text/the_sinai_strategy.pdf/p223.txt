Covetousness and. Conflict 197

cided to undergo the pierced ear ritual (the shedding of blood) in
order to become part of a family (Deut. 15:16-17). He was unsalable.
But other servants could be sold. Why, then, the prohibition against
coveting these others?

Since bargains are made constantly, including the sale of Esau’s
birthright, which Jacob unquestionably desired, what sense can we
make of the commandment? ‘The passage in Micah throws light on
the usage of the Hebrew word for coveting. Covetousness involves
uncontrolled lusting, a desire that can be satisfied orily by possessing
the other mian’s property. It is the kind of lusting that is involved in
adultery, where. the desire cannot legitimately be fulfilled, yet it per-
sists. It is the desire that results in lawlessness when it is thwarted,
the desire which cannot take “no” for an answer, “Woe unto them
that devise iniquity, and work evil upon their beds! When the morn-
ing is light, they practice it, because it is in the power of their hand.
And they covet fields, and take them by violence; and houses, and
take them away: so they oppress a man and his house, even a man
and his heritage” (Mic. 2:1-2). It is the kind of desire that resulted in
Ahab’s unlawful confiscation of Naboth’s vineyard (I Ki. 21). The
man with power uses that power, despite the protection given to the
original owner by the biblical Jaws regarding property.

The prohibition against covetousness therefore does not deal
primarily with envy, meaning envy in the sense of resentment. The
covetous person‘really zs intent upon obtaining the other man’s prop-
erty. Covetousness, in the biblical view, is an illicit form of jealousy. The
attack against the other man’s property is not motivated by a desire
only to tear down his property, but to confiscate it.

The covetous person resents his own station in life. Someone else
possesses what he wants. He is dissatisfied with the role he is playing
in God's plan for the ages. It is this resentment against one’s station
in life. which Paul condemns (1 Cor. 7:21-22), One person desires
another’s good looks, prestige, or worldly possessions. He teels
thwarted by his own limitations, and therefore thwarted by his envi-
ronment, God has thwarted his personal development, the covetous
man is asserting. The Bible teaches that the other person is working
out his salvation or damnation before God. His property must be
respected. Nevertheless, the covetous man thinks that he can ap-
propriate for himself the fruits of the other man’s labor, as if those
fruits were unrelated to that man’s personal responsibility before
God as a steward,
