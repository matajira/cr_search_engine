204 THE SINAI STRATEGY

means to improve our condition in life.”’ He cited Philippians 4:11:
“T have learned, in whatsoever state I am, therewith to be content.”
He could have continued quoting Paul’s words: “I know both how to
be abased, and I know how to abound: every where and in all things
T am instructed both to be full and to be hungry, both to abound and
to suffer need. I can do all things through Christ which strengthen-
eth me” (Phil. 4:12-13). Any external condition is acceptable to the man who
is content with his present role in God's plan jor the ages. But having little is
usually the condition against which men rebel. Paul is clear on this
point: “But godliness with contentment is great gain, For we brought
nothing into this world, and it is certain we can carry nothing out.
And having food and raiment let us be therewith content” (I Tim.
6:6-8). The rich have many temptations (1 Tim. 6:9-10).

Hodge saw the other aspect of covetausness: envy, Again, I think
this aspect is overemphasized in explaining this verse, although the
fact that commentators have focused on it in the past testifies to the
importance of Christian preaching against cnvy, even though in the
context of the tenth commandment it is not completely appropriate.
Hodge's words show that he fully understood the meaning of envy as
resentment, and that he distinguished this aspect of envy from cove-
tousness as the desire to confiscate another man’s property for one’s
own use. “The second form of evil condemned by this command-
ment is envy. This is something more than an inordinate desire of
unpossessed good. It includes regret that others should have what we
do not enjoy; a feeling of hatred and malignity towards those more
favoured than ourselves; and a desire to deprive them of their advan-
tages. This is a real cancer of the soul; producing torture and eating
out all right feelings. There are, of coursc, all degrees of this sin,
from the secret satisfaction experienced at the misfortunes of others,
or the unexpressed desire that evil may assail them or that they may
be reduced to the same level with ourselves, to the Satanic hatred of
the happy because of their happiness, and the determination, if pos-
sible, to render them miserable. ‘here is more of this dreadful spirit
in the human heart than we are willing to acknowledge. Montes-
quieu says that every man has a secret satisfaction in the misfortunes
even of his dearest friends. As envy is the antithesis of love, it is of all
sins that most opposed to the nature of God, and morc effectually

 

7. Charles Hodge, Systematic Theology, 3 vols. (Grand Rapids, Michigan: Berd-
mans, [1872] 1960), TIT, p. 468.
