360

Sowell, Thomas, 134-35, 186n, 190
Spence, Lewis; 3in
Spencer, Herbert, 106, 299
spirit, 1-2
Spirit
bird, xi
Spirituality, 3, 45
sports, 293
Spurgeon, Charles, 300
State
Bakunin, 22n
capital punishment (see capital
punishment)
children, {11-12
coercion, 327
corporate, 152
decapitalization, 115
divinized, 13
functions, 25t
kidnapper, 113
messianic, 175
Moloch, 175
one-world, 285-86
police power, 125
power religion, 313
pseudo-family, 107, 109-10, il4, 132
sabbath, 250-55, 265, 276, 303
saciely &, 12
sovereign, 176
sovereignty, 13, 171
thief, 176
tithe, 202
trustee, 112
violence, 199
welfare, 131-32, 141-43, 172, 207-9
worship, 252
steel, 267-68, 284, 292
stewardship, 140, 164, 163, 167, 176
sticks, 78, 256-57, 267, 282
stoning, 78, 116, 122-25, 257, 267-68
strange fire, 264-66
success, 273n, 274
subordination, 126, 224
Sumer, 34
sun, 82
Sunday, 79n, 241-42, 244, 287
amusements, 293, 302
Observance Act, 294

THE SINAI STRATEGY

recreation, 302
travel, 295
Victorian, 292
(see also Lord’s day)

sundial, 83

superiority, 214

supermarket, 149, 283

Supreme Court, 250n

suzerain, x

swearing, xvi, 51, 58

Sweden, 172

symbols, 33

tabernacle, 33

tables, xiv

talent, 101-3

talismans, 34, 36

talk, 234

tariffs, 99, 105, 152, 268

taverns, 271, 277

taxation, 107, 110, 113, 171, 172-75,
202, 206, 207

tax collectors, 309

taxes, 13

Taylor, Edward, 271

Taylor, Richard, 216-17, 222

technology, 260, 263, 267-68, 282,
294

television, 288

temple, 239, 265

temptation, xxii-xxiii

ten commandments
capitalism, 212-14
case laws, 7
economic growth, 224
humanists, 5
liberty, 15
numbering, 25-26
structure, xiv-xxii
tenure, 6

testimony, 55

testing, 186

Tertullian, 248

theft
definitions, 158-59
eighth commandment, 139
insecurity, 144
