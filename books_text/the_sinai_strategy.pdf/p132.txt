106 THE SINAIL STRATEGY

but I had found the truth of evolution. ‘Ail is well since all grows bet-
ter’ became my motto, my true source of comfort. Man was not cre-
ated with an instinct for his own degradation, but from the lower he
had risen to the higher forms. Nor is there any conceivable end to his
perfection. His face is turned to the light, he stands in the sun and
looks upward.”2? Not the family, not God’s covenantal law-order, but
the impersonal processes of evolution will bring progress. Wealth is
thc product of competition, and the fittest survive. Carnegie was a
devoted follower of Herbert Spencer’s brand of evolutionism, with
this exception: unlike Spencer, he did not fear the effects of charity.
He saw charitable activities as obligations of the rich, who would
give direction to the masses. What he called “the law of competition”
raises society and is the source of progress. “But whether the law be
benign or not, we must say of it, as we say of the change in the.con-
ditions of men to which we have referred: It is here; we cannot evade
it; no substitutes for it have been found; and while the law may
sometimes be hard for the individual, it is best for the race, because
it insures the survival of the fittest in every department.”33 Cosmic im-
bersonalism rules the world of man.

Carnegie pulled no punches in his defense of elitism. “We accept
and welcome, therefore, as conditions to which we must accom-
modate ourselves, great inequality of environment, the concentra-
tions of business, dustrial and commercial, in the hands of a few,
and the law of competition between these, as being not only bene-
ficial, but essential for the future progress of the race, Having ac-
cepted these, it follows that there must be great scope for the exercise
of special ability in the merchant and in the manufacturer who has to
conduct affairs upon a great scale.”** The market process is to be left
alone; individualism must be the ruling principle of production and
distribution. Communism cannot work. We musi defend the ideas of
“Individualism, Private Property, the.Law of Accumulation of
‘Wealth, and the Law of Competition; for these are the highest
results of human experience, the soil in which society so far has pro-
duced the best fruit.”55

So far, Carnegie sounds very much like the typical proponent of

32. Livesay, pp. 74-75.

33. Camegie, “Wealth,” (1889); in Gail Kennedy (eds), Democracy and the Gaspel of
Wealth (Boston: D.C. Heath, 1965), p. 2.

34. Idem.

35. Ibid., p. 3.
