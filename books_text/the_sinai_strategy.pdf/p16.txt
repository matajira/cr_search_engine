xvi THE SINAI STRATEGY

and keep His commandments. Keep Hts commandments, therefore,
and gain His mercy. He is in power over men, and He is in a posi-
tion as a judge to dispense punishments and mercy. In short, obey.

3. Ethics/dominion (stipulations)

Do not take the name of God in vain. As I argue in the third
chapter, by using the name of a society’s god, rebellious men seek to
invoke power. It is an attempt to manipulate that god in order to get
him to do the will of man. God warns us against using His name in
this way. To do so is to use His name in vain.

This does not mean that there is no power associated with God's
name. On the contrary, there is immense power. This is why men
are not to invoke this power autonomously. God promises to honor
His name when it is used lawfully by church authorities, which is his
ordained monopoly. The church alone can legitimately declare ex-
communication in the name of God. Thus, what we call “swearing”
(profanity) is an unlawful attempt to manipulate God by rebellious
men who assume the position of His ordained monopoly, the church.
(See chapter three.)

The magician believes that “words of power” can be used to ma-
nipulate external events. Man seeks power by manipulating his en-
vironment, He attempts to become master of the creation by the use
of secret phrases or techniques known only to initiates, whether
witch doctors or scientists. Men seek power through manipulation rather
than by ethics, obedience, and service to others.

The prohibition on the misuse of God’s name cuts off magic at
the roots. The commandment, being negative, is nonetheless posi-
tive: ethical. We are considering the priestly function here, however;
the ethical and dominical aspects are more clearly seen in the eighth
commandment, which parallels the third.

4, Judicial/evaluational (sanctions)

Sutton argues that the sabbath was the day of evaluation in the
Old Testament. As I argue in chapter four, following James Jordan's
exegesis, the sabbath was also the day of judgment by God. On that
day, Satan tempted man. Thus, there had to be judgment. There
was supposed to be judgment of Satan by Adam provisionally, and
then by God upon His return that afternoon. Instead, Adam sided
with Satan against God's word, and God returned to judge both man
