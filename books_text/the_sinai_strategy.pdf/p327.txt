The Economie Implications of the Sabbath 301

Army bands “have done untold harm” (p. 141). The LDOS thereby
forfeited the support of Nonconformist leaders. It also opposed the
use of automatic vending machines in the 1880's, despite the fact that
no labor was involved (p. 153). In short, the issue was not rest; the
issue was the marathon sabbath—self-denial for God, not external
convenience and rest. As the Society announced in response to a
1909 attempt to pass a pro-sabbatarian law that had been drafted by
the National Hygenic League, “This determination to settle the
whole matter on the basis not of Divine Law but of personal conven-
ience, with a flavouring of humanitarian sentimentality . . . a most
threatening danger” (p. 165).

What the LDOS and strict sabbatarians had accomplished in
over a half century of non-cooperation with other “tess: rigorous”
brethren and allies was to demonstrate the political irrelevance of
their position. They had taken the “moral high ground” —so high
that they were in the heavens and of little earthly use to the cause of
restful Sundays. They had adopted the marathon sabbath position of
certain segments the non-industrial seventeenth-century English
Puritans, and had suffered the consequences.

Conclusion

Several questions should be raised. Did the failure of the Lord’s
Day Observance Society come because of their political intransi-
gence? If so, was this intransigence biblically valid? Were the leaders
correct in refusing to compromise with anything that was not “pure
marathon sabbath”? Was the LDOS doomed at the end because of
an increasingly secular society? Or was it doomed from the begin-
ning because the “marathon sabbath” is neither biblical nor suitable
in an urban civilization?

There really were public debates over the timing-of the sabbath,
the locus of sovereignty of enforcement (Milton’s locus: conscience),
travel on Sunday, feast day vs. fast day, “seven-day-per-week” tech-
nologies, railways, public utilities, defining “mercy and necessity,”
servants’ labor on Sundays.(“mercy and necessity” . . . for the mas-
ters), mail delivery and shipping, newspapers, museums, bakeries,
and the appropriateness of cooking on Sunday. There were debates,
but there were no definitive answers. Eventually, people grew tired
of debates that could not be resolved, and the honoring of the sab-
bath became a matter of conscience. Milton’s suggestion became a
social reality.
