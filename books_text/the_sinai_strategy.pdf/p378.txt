352 THE SINAI STRATEGY

heart, 199
hell, 215
Henderson, John, 297
hierarchy, 141
high priest, xxiv
history, xxi
ethics, 19
God &, 17
icons, 34, 35
law, 19
linear, 40
Hodge, Charles, 203
Hoeksema, Herman, 203, 205
holidays, 298
Holland, 85
holy convocations, 231
holy days, 294
honesty, 316
Hosea, 128
humanism
antinomianism, 317
autonomy, 39
Christian compromise, 208
Dooyeweerd, 331
God’s law vs., 121, 317
graven images, 49, 118
idols, 39
lost self-confidence, 227
millennium, 89
satanism, 39
salvation by law, 15
sentiment, 112
human rights, 169-70, 175
Huns, 156
Huntsinger, Jerry, 193

icon, 33-37
iconoclastic controversy, 34n-35n
ideas, 188
idleness, 249, 273, 274
idle talk, 232
idolatry, 49, 67, 118
idols, 30, 32
illegitimacy, 131
image
man, 30
point of contact, 30

pre-Christian, 31
punishment, 40

recall, 34
separation, 27
social order, 38

image of God, 30, 49, 115-19
images, 25-50
immigration, 100, 104
immortality, 95
imperatives, 53-54
impersonalism, 113-14, 124
impotence, 30, 62, 223
imputation, 320
Incarnation, 31, 34
indexing, ix
indifference, 192
industrial revolution, 261, 273, 278,
298
inflation, 207
information, 187
information costs, 194
Ingram, T. R., 56
inheritance, 107, 113
insects, 291
institutions, 53
invocation, 51-52
intellect, 192-93
InterVarsity Press, 10, 12, 317
Ireton, 153
Trish, 302
Isaac, 94
Istael
conquest, 30
conversion, 90-91
diaspora, 29
“fullness,” 90-91
judgment, xxiv
tural, 268-69
sabbath, 236-38
sacrifices, 264
separation, 29

Jacob, 82, 94, 178, 197
Jagger, Mick, 172n
Jaki, Stanley, 225
James I, 293

‘Japan, 212
