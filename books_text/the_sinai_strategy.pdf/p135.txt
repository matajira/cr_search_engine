Familistic Capital 109

of wealth thus becoming the mére agent and trustee for his poorer
brethren, bringing to their service his superior wisdom, experience,
and ability to administer, doing for them better than they would or
could do for themselves.”#

He warned against “indiscriminate charity.” Echoing Benjamin
Franklin, he said, “In bestowing charity, the main consideration
should be to help those who will help themselves. . . .” In short, “In-
dividualism will continue, but the millionaire will be but a trustee
for the poor; entrusted for a season with a great part of the increased.
wealth of the community, but administering it for the community far
better than it could or would have done for itself.”*

The irony of this is that Carnegie, in order to finally cash in his
investment and begin giving away $300,000,000 (Cramer estimates
that he eventually gave away $350 million, including $62 million in
the British Empire), sold out to J. P- Morgan, knowing that Mor-
gan was about to establish a giant conglomerate steel trust that was
composed of companies whose profits were being wiped out by Car-
negie’s cost-cutting tactics.“* Carnegie had served as a trustee of the
masses by his activities as an entrepreneur; he abdicated that posi-
tion of trusteeship in the. name of philanthropy, where he had few
skills. As a producer, he had increased the wealth of millions of steel
users, allowing them to benefit from his efforts as they chose. He
never understood this, or if he did, his elitism overwhelmed his un-
derstanding.

 

Darwinian Elitism

The sort of blatant elitism which Carnegie espoused did not sur-
vive the onslaught of socialistic and interventionist thought which
has been the predominant position of twentieth-century intellec-
tuals. Elitism survived, but not blatant, visible elitism. His sort of
modified social Darwinism still contained too strong an element of
individualism. Once Carnegie acknowledged the validity of confis-
catory estate taxation by the civil government — obtaining “its proper
share”—his case for private property was lost, The pseudo-family
could not wait until the death of the testator. It could not rely on his

41. “Wealth” p. 7.

42. Ibid, p. 8.

43. Cramer, p, 429.

44. Robert Hessen, Steel Titan: The Life of Charles M. Schwab (New York: Oxford
University Press, 1975), pp. 111-18.
