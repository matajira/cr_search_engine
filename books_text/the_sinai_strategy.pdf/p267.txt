The Economie Implications of the Sabbath oA

assignment by God to a priest is in question, the sabbath may be
profaned without blame. Again, it must be profaned. But then we
face some very difficult questions: How can we tell when human
health and life are at stake? Who is the true priest? What is a God-
given assignment?

A. Transformation: The Lord’s Day

In the New Testament, the first day of the week is called the
Lord’s day (Rev. 1:10), but it is never called the sabbath. Unquestion-
ably, there was a shift from the seventh day of the week to the first.
The evidence also points to a shift from sundown-to-sundown cele-
bration to a sunrise-to-sunrise- celebration. These are very important
changes. They involve a radical break with the Hebrew sabbath,
F_N. Lee, in his defense of the New Testament sabbath, argues ex-
plicitly that the entire system of Mosaic sabbaths and holy days was abolished
by Christ, He cites Paul’s epistle to the Colossians: “Let no man
therefore judge you in meat, or in drink, or in respect of an holyday,
or of the new moon, or of the sabbath [days]” (2:16). (The last word,
“days,” was added by the King James translators; it should read sim-
ply, “sabbaths.”) Lee concludes:

Now these ceremonial sabbaths, listed in Leviticus 23 together with the
Israelite Sinaitic weekly sabbath, are all called “feasts” of holy convocation, or
“holy days”; and all involve the keeping of a “sabbath” day or a “day of holy
convocation” on. which “no servile work is to be done,” or a “day of solemn
rest.” They were all a shadow of the things to come, namely the benefits of the
New Testament in Christ; and they were all blotted out and nailed to His
cross... . So Paul means exactly what he says. Iris useless to argue (as 8.D.
Adventists do) that Si, Paul here means the ceremonial sabbaths by his words
“or the sabbath (days),” for St. Paul has just a few words beforehand (in the
very same verse) dealt with such cerernonial sabbaths under the blanket term.
“holy day” ~the same term (heoriai) used in the Septuagint of Lev. 23-to refer to
ail the (Sinaitic) sabbaths—both the ceremonial sabbaths and the “weekly”
sabbath of Israel, Lev. 23:2-3. . . . If it is argued that Paul means (only) the
ceremonial sabbaths in Col. 2:16 where he refers to “the sabbath day(s),” then
which days is he referring to under the blanket term “holy days” just mentioned
previously in the very same verse? The two.can hardly be synonymous, for
Paul would then be repeating himself, saying in effect: “Let no man therefore
judge you... in respect of a ceremonial sabbath or a new moon or a
ceremonial sabbath,’ when the latter phrase would simply be idle repetition. ®

6. EN. Lee, Zhe Covenantal Sabbath (London: Lord’s Day Observance Society,
1972), pp. 28-29.
