The Economie Implications of the Sabbath 287

and ate the Lord’s Supper.

We have sufficient revelation to know that the normal pattern of
the week is to rest one day and work for six. Must we always work
six days? The Mosaic law said yes, in general, but it also established
other feast days and days off. There are problems of applying God’s
word to specific cases.

These were precisely the problems faced by new Christians in
Romans 14:5, They are our problems, too. Which days off are legiti-
mate? Which day should men take off during the week? None? One
(Sunday or Saturday)? Two (Saturday and Sunday)? Three (Satur-
day, Sunday, and Monday, given the trade unions’ pressure to create
three-day holidays whenever an American national holiday rolls
around)? What is the answer? What is the incontrovertible,
conscience-binding answer that all Christians must respect, since it
is so clear exegetically and historically? Which is the morally and
legally binding day for rest?

The answer is not so easy to produce. Sunday (the Roman name
for Firstday) is the common day of worship and therefore preferable,
although the early church could not always adhere to it as the day of
rest. But some members will have to work on Sunday, at least part of
the day, In practice, the churches tend to acknowledge this economic
reality, so long as the individual shows up one Sunday out of three or
four. Why make exceptions at all? Why not get every member to
quit his job if it requires Sunday labor? Because the church officials
are more realistic when they count the tithes than when they write
their sabbatarian tracts. They do not want trouble. They acknowl-
edge in practice what their tracts deny: men do have legitimate call-
ings that appear to be seven-day operations by economic necessity.
Cows need milking, and churches need their tithes.

This raises the question of legitimate leisure. What should men
do for leisure? Also—a question virtually never discussed by sabba-
tarians— what kinds of leisure are legitimate on the other six days of
the week? God's law gives no indication that the six days of labor in a
normal week were to involve leisure activities. With the exception of
national (nonweekly) sabbaths, and the various feast periods (Deut.
14:23-29), men were told to work six days a week, Yet it is obvious
that people cannot long sustain a life of zero leisure six days a week
—not if they are to maintain their productivity. They sleep, they eat,
and they chat. They teach their children (Deut. 6:7). Presumably,
families enjoy some hours of leisure during the day. But the Bible
