The Value of a Name 185

to the producer for repair or a refund. The local buyer may find it
relatively inexpensive to confront the seller directly, since his trans-
portation costs are low. Foreigners, especially in the ancient world,
are not the beneficiaries of many of the advantages that domestic
buyers possess. They are not citizens of the country where the pro-
ducer dwells. In the ancient world, this made it almost impossible
for foreign buyers to gain justice in another nation’s legal system,
since foreigners had no legal rights, not being part of the civic relig-
ion, Thus, the reputation of the producer, or the importer-trader,
was important in establishing foreign markets for the products of a
nation’s citizens.

What we can readily understand is that a close relationship be-
tween morality and the family name, between a sense of craftsman-
ship and the family name, or between both morality and craftsman-
ship and an identifying mark on the product, must have made it eas-
ier for a nation to gain an international reputation. Foreigners would
learn of the high quality products produced by the citizens of some
foreign culture. The reputation of that nation would be enhanced.
This was true of Israel’s laws:

Behold, I have taught you statutes and judgments, even as the Lorp my
God commanded me, that ye should do so in the land whither ye go to pos-
sess it. Keep therefore and do them; for this is your wisdom and your un-
derstanding in the sight of the nations, which shall hear all these statutes,
and say, Surely this great nation is a wise and understanding people (Deut.
4:5-6).

What is true of a law-order is also true for products. When con-
sumers can more readily identify products that satisfy them, the effi-
ciency of the market is greatly enhanced. The diviston of labor is limited
by the extent of the market, Adam Smith wrote in Chapter 3 of Wealth of
Nations (1776), and by increasing brand-name identification, pro-
ducers thereby contribute to extending the market, Men become
familiar with buying in the marketplace, which is important in the
transition between a primitive society, with its low division of labor,
to a modern society. Brand names transmit knowledge in an effective,
rapid, and summary fashion, and knowledge is what the Bible com-
mends again and again. Brand names help consumers to economize on
knowledge, which is the most important and valuable of all com-
