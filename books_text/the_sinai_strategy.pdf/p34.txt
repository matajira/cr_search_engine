8 THE SINAI STRATEGY

Roman Catholic Economic Antinomianism

The Lay Commission on Catholic Social Teaching is a conserva-
tive group which is made up of some of the most famous American
Catholic social thinkers and political figures. Its study of Catholic
economic thought announces on the opening page: “. . . Christian
Scripture does not-offer programmatic guidance for the concrete in-
stitutions of political econorny.”® It then cites “the great Catholic
economist Heinrich Pesch, S.J.” who proclaims that morally ad-
vanced societies will be better prepared to endure hard times, but
“this does not mean that the economist should theologize or moralize
in the treatment of his subject matter or, what is worse, try to derive
an economic system from Holy Scripture.”!° This document was
written specifically to counter the ultra-liberal proposed first draft of
Catholic bishops regarding the U.S. economy. And what first princi-
ple regarding biblical authority governs the liberal bishops? The
same as the one adopted by the Catholic lay conservatives: “Al-
though the Bible does not and cannot give us simple and direct an-
swers to today’s complex economic questions, it can and must shape
our vision of the meaning of economic life.”' The conservatives cite
the free market economists who they like, while the liberals cite the
anti-free market non-economists who they like. No one appeals to

biblical law.

Conservative Protestant Economic Antinomianism

Conservative Protestant philosopher Ronald Nash is opposed to
liberation theology and Christian socialism. His book, Social Justice
and the Christian Church (1983) is a ringing defense of capitalism. But
not biblical capitalism. He appeals not to the Bible, but to universal
standards of logic, universal truth that can be recognized by all
right-thinking people. He begins with the implicit but unstated pre-
supposition that the Bible is not sufficiently self-attesting and clear to
provide generally agreed-upon conclusions; an appeal to universal
logic is therefore necessary:

9. Toward the Future: Catholic Social Thought and the U.S. Economy (North Tarry-
town, New York: Lay Gommission, 1984), p. ix

10, Zbid., pp. ix-x.

41. “First Draft— Bishop’s Pastoral: Catholic Social Teaching and the U.S. Econ-
omy,” Origins, Vol. 14 (Nov. 15, 1984), p. 343. Published by the National Catholic
News Service, Washington, D.C.
