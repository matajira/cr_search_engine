246 THE SINAI STRATEGY

as Israel was. This is especially truc in the American South, where
“blue laws” that prohibit certain businesses from operating on Sun-
day, or that prohibit certain products from being sold in supermar-
-kets on Sunday, are voted into law time after time by covenant-
denying Southern Baptists, Methodists, and Church of Christ mem-
bers. I cannot explain this; I only report it.)

Second, there is the problem of the Lord’s day as primarily a day
of worship. If the Old "Lestament’s sabbath-enforcing civil law is still
binding in New Testament times, and if the Lord's day is understood
as predominantly a day of worship (as the Westminster Confession
and most pastors assert), then the civil magistrate ought to enforce compul-
sory worship on all members of a (covenanted) society upon threat of death.

The New England Puritans went at least part of the way down
this path. They legislated compulsory worship, and they banished
sabbath violators from Massachusetts and Connecticut in the early
years. Even this half-hearted attempt to imitate the Old Testament
only lasted a few years. There were more and more church absent-
ees, until by the middle of the seventeenth century, the churches of
New England could not have held the whole population, had every-
one decided to visit on some Sunday morning.? Eventually, “blue
laws” replaced the threat of banishment for failure to attend church
in New England.

Modern sabbatarians have refused to become consistent. They
do not pressure the civil government to establish a death penalty for
Lord’s day desecrations, and they certainly avoid the obvious con-
clusion concerning the Lord’s day as a day of worship, namely, com-
pulsory church attendance, enforced by the civil government.

 

The New Testament Church’s Celebration

What Paul was asserting should be clear to anyone who reads
Romans 14, Not only do those outside the church have varying opin-
ions concerning a day of rest, or special holidays; even those inside
the church have varying opinions, We sce in the twentieth century

 

9. Garl Bridenbaugh writes: “A consideration of the number and scating capaci-
ties of village meeting houses and churches demonstrates the shecr physical impossi-
bility of crowding the entire village populations into their houses of worship. At no
time alter 1650 does it seem possible for the churches of Boston to have contained
anywhere near a majority of the inhabitants; in 1690 little more than a quarter of
them could have attended church simultancously had they been so disposed” Briden-
baugh, Cities in the Wilderness: The First Gentury of Urban Life in America, 1625-1725
(New York: Capricorn, [1938] 1964), p. 106.
