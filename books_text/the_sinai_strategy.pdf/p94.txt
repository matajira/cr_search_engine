68 THE SINAI STRATEGY

character as a sovereign agency to the recipient. For example, the
transfer of the seal of government involves also the transfer of gov-
ernmental sovereignty to the recipient. But this transforms the
contract-making ability of the recipient organization into a
covenant-making ability. ‘

In short, a covenant testifies to the existence of a higher sovereignty.
Biblically sanctioned self-maledictory oaths are administered by
such a sovereignty; it possesses more than a contractual sovereignty.
It possesses covenanial sovereignty. This is why the explanations of the
origins of civil government by all so-called social contract theorists
are categorically incorrect. The three governments ordained by God
— ecclesiastical, civil, and family —were not the product of a hypo-
thetical historical social contract among sovereign individuals. They
are the covenantal creations of the Creator God, They are not or-
ganizations that were created by the equivalent of business con-
tracts.

From “Brotherhood” to “Otherhood”

This is a phrase adopted by the sociologist-historian Benjamin
Nelson.?” He uses the so-called “Weber thesis” to provide an inter-
pretation of the transition from feudalism to capitalism. The histori-
cal documentation and debates surrounding this thesis are not the
main issue at this point.?8 What is important is the concept of the
non-covenantal voluntary association.

As the West became increasingly Christian during the Middle
Ages, men could deal with each other because they belonged to a
universal church. Christian associations steadily replaced pagan
brotherhoods and tribes. The medieval world was a world of mutual
loyalties, very often written down. Feudal contracts were military
and civil covenants, however. What steadily replaced these cove-
nants was the contract, especially the business contract.

The Protestant Reformation destroyed the ecclesiastical unity of
the medieval world, but it did not destroy trade. On the contrary,
trade increased.2° Men who did not share membership in a common

27. Benjamin Nelson, The Idea of Usury: From Tribal Brotherhood to Universal Other-
hood (2nd ed.; University of Chicago Press, 1969).

28. Gary North, “The ‘Protestant Ethic’ Hypothesis.” The Journal of Christian Re-
construction, IIL (Summer 1976).

29. On the growth of trade and commerce in this period, sec the magisterial
study by French historian Fernand Braudel, Civilization and Capitalism, 15th-18th Cen-
