40 THE SINAI STRATEGY

rebellious ritually. They do not celebrate their faith by adopting the
ancient rituals of satanism, namely, by making graven images.?*
Worshipping graven images would make manifest their ultimate
theology, so in this respect Christianity has influenced hurnanism
and has restrained it.

II. The Compounding Process

We come now to the reason given for the prohibition against con-
structing graven images. The reason: is that God is a jealous God.
What kind of God is that? It is a God who visits the iniquity of the
fathers on subsequent generations of ethical rebels. It is also a God
who shows mercy io generations of covenantally faithful pcople. The
presence or absence of graven images testifies to the spiritual condi-
tion of the two ethically distinct and rituadly distinct types of people.

The heart of the description of the jealous God is the covenantal
process of compound growth growth unto judgment and growth unto
dominion. History is linear. It develops over lime. What goes before
affects what comes after. Nevertheless, it does not determine what
comes after, God determines both the “before” and the “after.” God is
sovereign, not the forces of history. But the criteria of performance
are ethical. We know which covenant we are in by evaluating the ex-
ternal events of our lives in terms of God’s list of blessings and curses
(Deut. 28).

A. The Iniquity of the Children

“[Flor I the Lorp thy Gad am a jealous God, visiting the iniquity
of the fathers upon the children unto the third and fourth genera-
tions . . .” (Ex. 20:5b). This: verse is frequently misunderstood. It
does not say that God punishes sons for the sins of their fathers. The
Bible’s testimony concerning the responsibilities of children for the
sins of their fathers is clear: “The fathers shall not be put to death for
the children, neither shall the children be put to death for the
fathers: every man shall be put to death for his own sin” (Deut.
24:16). This principle was reaffirmed by Ezekiel: “Che soul that sin-

24. C. 8. Lewis’ magnificent novel, That Hideous Sirength (1945), presents a liter-
ary prophecy of a coming fusion of power-seeking modern science and power-
seeking ancient demonism. This experiment ends in the novel with the destruction
of the scientists: one by a suicidal but consistent application of modern dualistic psy-
chology (Frosi), another as a blood sacrifice to a demonic. god-head whose scientific
“creator” never suspected (unti! the moment of his death) that it was anything but a
strictly scienGfic phenomenon (Filostrata).
