Preface xiii

4, Judicial/evaluational (sanctions)

The fourth aspect of the covenant is its judicial character. The es-
sence of maturity is man’s ability to render God-honoring judgment.
God renders definitive judgment in His word, the Bible, and renders
final judgment at the end of time. Man is to render analogous judg-
ment progressively through time. During the creation week, God
said “It is good” after each day. He evaluated His own work, and He
rendered judgment verbally. God is the supreme King, but also the
supreme Judge. When He decfares a man innocent, because of His
grace to the person through the gift of saving faith, God thereby im-
putes Christ’s righteousness to him,? Without God’s declaration of
salvation, meaning without the imputation of Christ’s righteousness
to overcome the imputation of Adam’s sin, there is no salvation.

When a covenant is “cut,” men are reminded of both the bless-
ings and the cursings attached to the covenant. There are oaths and
vows. There are covenant rituals. There are visible signs and seals.
We see this in the church (baptism, Lord’s Supper), the family (mar-
riage ceremony), and in civil government (pledge of allegiance,
oath-taking of officers).

5. Legitimacy/inheritance (continuity)

Finally, there is. the legitimacy/inheritance aspect of the cove-
nant. There are covenantally specified standards of transferring the
blessings of God to the next generation. In other words, the cove-
nant extends over time and across generations. It is a bond which
links past, present, and future. It has implications for men’s time
perspective. It makes covenantally faithful people mindful of the
earthly future after they die. It also makes them respectful of the
past. For example, they assume that the terms of the covenant do
not change in principle. At the same time, they also know -that they
must be diligent in seeking to apply the fixed ethical terms of the cov-
enant to new historical situations. They are respectful of great his-
toric creeds, and they are also advocates of progress, creedal and
otherwise. They believe in change within the fixed ethical terms of the cov-
enant,

2, John Murray, Redemption Accomplished and Applied (London: Banner of Truth
‘Trust; Grand Rapids, Michigan: Eerdmans, 1961).
