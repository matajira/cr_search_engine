The Economic Implications of the Sabbath 229

how it should (or should not) be observed today, must be governed
by-the words of Exodus 31:15 and Exodus 35:2. In short, if we argue
that the death penalty is no longer to be’ imposed on people who
work on the Lord’s day (as I do), then we must present a case that
the requirements of the Old Testament sabbath have been fulfilled
by Christ and-are now annulled, and that God has substituted new
rules to govern the Lord’s day (which is what I attempt to do in this
appendix), On the other hand, if someone denies that there has been
a fundamental break between the Old Testament sabbath and the
New Testament Lord’s day; then he must demonstrate exegetically
how it can be that the God-ordained death penalty has been abolished,
but the moral and even ecclesiastical requirements concerning. the
observation of the Lord’s day have. remained essentially the same.

Why did God regard: a violation. of His sabbath as a capital
crime? We have seen the answer in Chapter 4: violating the sabbath in-
volves a denial of the mandatory nature of rest for mankind. Such a violation
involves the assertion of autonomy on the part of man. Such an asser-
tion brings spiritual and eternal death. But why did God wait until
after the Exodus to announce that working on the sabbath is a
capital crime? Probably because Hé wanted Israel first to under-
stand what it meant to live under the domination of a self-
proclaimed: god-man who did not allow God’s people to rest. In the
recapitulation of the ten commandments in Deuteronomy, God gave
them’ a different reason for honoring the sabbath: they had been in
bondage to Egypt, and God had delivered them from this bondage
(Deut. 5:15), He brought death to Egypt’s firstborn; He would do the
same to them if they failed to honor His covenant with them.

A key question then has to’ be considered: Why in New Testa-
ment times has the church never advocated such a harsh penalty? I
hope to answer this question at the end of this appendix. The fun-
damental answer is that there has been a shift in the locus of sover-
eignty for sabbath enforcement: from civil government and ecclesi-
astical government to self-government (the individual conscience).

We have come at last to the really difficult issues, the issues of ap-
plied theology. We must consider these preliminary issues:

I. What was the Old Testament Sabbath?
A, What were men supposed to do on the O.T. sabbath?
B. What were the economic implications of the Mosaic
sabbath, especially with respect to the division of labor?
