inward desire, 205
market value, 143-52
slander, 186-88
uncertainty, 144
waste, 144
theologians, 6-7
theophany, 31, 82
Third World, 213-15
thrift, 129, 143
time, 48, 50, 102, 176
tithe, 13, 72, 159, 171, 176, 202,
287, 303
tokens, 129
toleration, 314
Toleration Act, 297
tongue, 63
tools, 32
Toronto School, 331
totalitarianism, 201
trade
contracts, 68-69
division of labor, 268-69
foreign, 184-85, 268-69
illegitimate, 196
Passover, 280-81
Reformation, 68-69
Puritans, 272
sabbath, 285-86
trade unions, 99
trains, 261, 276, 288, 296
transportation, 268
travel, 295
treaty, x, 27, 2
tree, 73
‘Trinity, xix, 127, 128, 133
Troas, 83
Tropic of Cancer, 62
Troost, A.
antinomian, 323
guilt, 337
jargon, 334-36
mysticism, 326, 329
pietism, 324
socialism, 328
Trotsky, 48
Truman, Harry, 207n
trustee, 109, 110, 111-114

Index 361

trust, 63

trusts, 99

truth, 332

Tumer, F. J., 156n, 157n
typesetting, ix

tyranny, 319, 320

uncertainty, 144, 160, 161-63, 176
unemployment, 13

union, xii

Union Seminary, 207n, 218
university, 225

University of Houston, 137
Unwin, J. D., 133

USSR, 5, 22, 198

value, 146-50
vandals, 156

Van Til, Cornelius, xin, 10, 332, 333n
Velikovsky, 1., 17

verbal binding, 54

verbiage, 332, 335

victory, 227

videotape, 288

vigilantes, 124

vineyard, 199

violence, 63, 171, 197, 199 206, 254
virginity, 129, 178

Voline, 22n

voluntarism, 191-92

vote, 153-54

vow, 33, 57, 128-29 (see also oath)

Walker, George, 264

Walmsley, Joshua, 297

wants, 188

Ward, Lester, 110

warfare, 28

warlords, 120

waste, 144, 163, 167

wealth, 101-2, 233-34

wealth redistribution, 199, 208

Weber thesis, 68, 225

week, 75, 242-43

welfare, 112, 114, 131, 132, 141-43, 202,
206

welfare State, 207-9
