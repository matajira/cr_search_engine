10 THE SINAI STRATEGY

is. Some people do not accept the Bible; presumably, all rational people
will accept the findings of human reason. (Problem: given the low
sales of his book, and the low sales of hundreds of other pro-free mar-
ket economic books equally grounded in universal human reason,
there are apparently a lot more irrational people around than people
who refuse to accept the Bible. There are more people who just won't
face “the universal truth” than those who reject biblical truth.)

As a devoted follower of the Protestant philosopher Gordon
Clark, Dr. Nash rejects the idea of Van Til’s presuppositional, Bible-
based (i.e., “proof-text”) approach to the intellectual defense of
Christianity. He relies instead on the hypothetical natural, unbiased,
and reliable reasoning abilities of natural (unregenerate) man. In
short, he appeals to biblically unaided (autonomous) reason because
of his personal preference and philosophical commitment. He then
discovers what he regards as inescapably clear free market principles
in the conclusions of autonomous human reason, Unfortunately,
“radical Christians” somehow have escaped from this inescapably
clear set of economic conclusions.

Liberal Evangelical Economic Antinomianism

Nevertheless, we find the same sort of “anti-proof text” reasoning
in the camp of the “radical-Christian’ Protestants, the left-wing tar-
gets of Nash’s book. In a symposium on Christian economics pub-
lished by the neo-evangelical Protestant (and increasingly politically
liberal) InterVarsity Press in 1984, three of the four contributors
were defenders of more State planning and authority over the econ-
omy. I was the lone critic of the State. All three of the anti-market es-
sayists explicitly denied that the Bible gives us any specifics concern-
ing economics.

The fact that our Scriptures can be used to support or condemn any eco-
nomic philosophy suggests that the Bible is not intended to Jay out an eco-
nomic plan which will apply for all times and places. If we are to examine
economic structures in the light of Christian teachings, we will have to do it
another way, 13

The Old Testament gives detailed laws regulating economic relationships.
Although we need not feel bound by these laws, the general concern of justice and

13, William Diehl, “Phe Guided-Market System,” in Robert Clouse (ed.), Wealth
and Poverty: Four Christian Views of Economics (Downers Grove, Ulinois: Inter Varsity
Press, 1984), p. 87,
