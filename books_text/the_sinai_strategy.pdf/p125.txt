Familistic Capital 99

success seek political power in order to restrict their competitors
from displacing them. This phenomenon has been described as
“pulling up the ladder after you’ve reached the top.” Primogeniture
was one such restriction, which held together the great landed
estates of England for many centuries. Other sorts of restrictions
prevail in the modern “mixed” economy, all of them hostile to the
great engine of progress under capitalism, price competition. These
restrictions include: tariffs or import quotas,’ prohibitions against
price competition (price floors) in the name of protecting market sta-
bility,* or protecting the consumer from trusts,? minimum wage laws
(another price floor), restrictions against advertising (still another
kind of price floor),!! compulsory trade unionism," restrictions on.

5. Gabriel Kolko, The Triumph of Conservatism; A Reinterpretation of American History,
1900-1916 (New York: Free Press of Glencoe, 1963). Kolko is a “New Left” historian.
He argues that the American Progressive movement, which promoted government
regulation of the trusts in the name of protecting the consumers, was supported by
large businesses that were sceking legislated protection from new competitors. For fur-
ther evidence on this point, see James Weinstein, The Corporate Ideal in the. Liberal
Stats, 1900-1918 (Boston: Beacon Press, 1968); Clarence Cramer, American Enterprise:
Free and Not Sa Free (Boston: Little, Brown, 1972), chaps. 10-14.

6. Gary North, “Price Competition and Expanding Alternatives,” The Freeman
(August 1974).

7. Gary North, “Buy American!” The Freeman ( January 1981). ‘The relationship
between monopolies and tariffs was explained as long ago as-1907: Franklin Pierce,
The Tariff and the Trusts (New York: Macmillan, 1907),

8. Mary Peterson, The Regulated Consumer (Ottawa, Illinois: Green Hill, 1971);
Dan Smoot, The Business End of Government (Boston; Western Islands, 1973),

9. D. T. Armentano, Antitrust and Monopoly: Anatomy of a Policy Failure (New York:
John Wiley & Sons, 1982), Harold Fleming, Ten Thousand Commandments: A Story of
the Antitrust Laws (New York: Prentice-Hall, 1951). See also Pierce, The Tariffs and the
Trusts, op. cit,

10, Walter Williams, The State Against Blacks (New York: McGraw-Hill New
Press, 1982), ch. 3. This book also covers occupational licensing, regulation by the
states, taxicab licensing, and trucking regulation.

11. Yale Brozen, Aduertising and Society (New York: New York University Press,
1974); George Stigler, “The Economies of Information,” Journal of Political Economy,
LXLX (June 1961); Yule Brozen, “Entry Barriers: Advertising and Product Differ-
entiation,” in Harvey J. Goldschmidt, ef al. (eds.), dustrial Concentration: The New
Learning (Boston: Little, Brown, 1974); David G. ‘Tuerck (ed.), The Pulitical Economy
of Advertising (Washington, D.C.: American Enterprise Institute, 1978); Tuerck
(ed.), éssues in Advertising: The Econonues of Pursuasion (Washington, D.C.; American
Enterprise Institute, 1978)

12. Gary North, “A Christian View of Labor Unions,” Bibtical Economics Taday, 1
(April/May 1978); Philip D. Bradley (ed.), The Public Stake in Union Power (Char-
lottesville: University of Virginia Press, 1959); Sylvester Petro, Power Unkimited: The
Corruption of Union Leadership (New York: Ronald Press, 1959).

 

 
