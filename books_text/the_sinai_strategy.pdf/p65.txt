Graven Images and Compound Judgment 39

education (progressive education), scientific planning (Darwinism),
or scientific management (Taylorism).2°

Satan did not tempt Adam and Eve to worship him openly; he
only asked them to violate the law of God. The violation of Gad’s law
was the equivalent of worshipping Satan. Only when he approached
Christ did he ask to be worshipped (Luke 4:7), The worship of man and
his works ts essentially the worship of Satan. In short, man the idol-maker
and idol-worshipper is man the Satan-worshipper. Humanism is in-
escapably satanism, which is why satanism revives during periods of
humanistic dominance.?!

The construction of a world order which is opposed to the one set
forth by God is therefore theologically comparable to constructing a
graven image. There may be no official graven image at first. Men
may not be asked to bow down to it at first. But the substitution of
the ordinances of man for the ordinances of God is the heart of idol-
worship. It is an assertion of man’s autonomy, which ultimately results
in the subordination of man to the ordinances of Satan. The society of Satan
does not need graven images to make it operational.??

It is a testimony to the impact of Christianity on Western culture
that graven images have ali but disappeared. Humanists have
adopted faith in the original promise of Satan to Eve, namely, the
impossible offer of autonomy to man, but they do not bow down to
graven images. To make a profession of faith in man’s autonomy is
to become ethically subordinate to Satan (but inescapably under the
overall sovereignty of God).?3 Men who believe that they worship no
god have nevertheless conformed themselves sufficiently to Satan’s
standards to warrant eternal punishment, and to that extent, Satan
is pleased. In worshipping the works of their own hands, they refuse
to worship God. Their idols are not explicitly religious or explicitly

20. Gary North, “From Cosmic Purposclessness to Humanistic Sovereignty,” Ap-
pendix A in The Dominion Covenant: Genesis (Tyler, ‘lexas: Institute for Christian Eco-
nomics, 1982).

21. On the occultism of the Renaissance, see Frances Yates, Giordano Bruno and the
Hermetic Tradition (New York: Vintage, [1964] 1969). On the occult background of
nineteenth-century revolutionism, see James Billington, Fire in the Minds of Men:
Origins of the Revolutionary Faith (New York: Basic Books, 1980). On the link between
humanism and occultisin in the United Stares, especially after 1964, sex Gary
North, None Dare Gall It Witchcraft (New Rochelle, New York: Arlington Housc,
1976).

92, RJ. Rushdaony, “The Society of Satan,” (1964); reprinted in Biblical Econom-
ies Teday, 1 (Oci./Nov. 1979).

23. Gary North, Dominion Covenant: Genesis, p. 92.

 
