50 THE SINAL STRATEGY

“Anyone who pretends to be interested in magic or the occult for rea-
sons other than gaining personal power is the worst kind of hypo-
crite.”45 This is the heart and soul of all Baal worship. But Satan’s
rule is doomed. It can grow in influence culturally for short periods,
but ultimately temporal judgment comes, as it came to the Egypt-
ians. The capital investment of the idol-worshippers is eventually
squandered, destroyed, or inherited by the faithful.

On the other hand, when men worship God, they place them-
selves within a covenantal framework which is guaranteed for
“thousands of generations.” They can také dominion over the exter-~
nal realm because they operate in terms of God's tool of dominion,
His law. Time and continuity are not the enemies of God's people,
for long-term growth eventually brings prosperity to the spiritual,
covenantal heirs of the faithful. The continuity of faith over time brings
the continuity of expansion over time, spiritually and culturally.

Men are to seek covenantal dominion, not autonomous. power.
Dominion comes through obedience to God. God possesses ultimate
authority. Man cannot escape being subordinate to something ulti-
mate, and this ultimate something is God. By refusing to make
graven images, the ancient Hebrews ritually affirmed that their cov-
enantal yoke was imposed by God, not by themselves.

To whom will a man or society be subordinate: God or Satan?
Will a man become part of God’s hierarchy or, as C. 8. Lewis puts it
in his Screwtape Letters, part of Satan’s “lowerarchy”? Whose covenan-
tal yoke will men wear, Christ’s.or Satan’s? There is no escape from
yokes; the question is: Whose? The issue of hierarchy and obedience
is crucial in this commandment. God commands men to worship
Him, and not to attempt to escape subordination to Him by seeking
autonomy. Worshipping anything other than God is an affirmation
of autonomy, for man autonomously determines for himself that he
will worship something other than God. The second commandment
prohibits man from setting up any visible manifestations of a repre-
sentative of any supernatural authority other than God.

45. Anton Szandor LaVey, The Satanic Bible (New York: Avon, 1972), p. 51.
