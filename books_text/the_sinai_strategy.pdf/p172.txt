146 THE SINAT STRATEGY

horrified. In the name of Jesus, he attacked the idea of the biblical
sanction of private property, and my defense of the economics of
crime prevention, with the following line of argumentation: “The
less thievery there is, the more the value of private property in-
creases and the less able the poor are to buy it. In capitalism, the
more ‘moral’ a people are, the more the poor are oppressed.”!! He is
not joking. He expects us to take him seriously. The culprit is capi-
talism, in Gish’s view. It is capitalism which hurts the poor, even
when crime goes down, since capitalism lowers the value of goods
when crime goes up. Less thievery means that the poor are exploited
in capitalisrn. If this is the best that Christian communalists and
egalitarians can come up with in their ideological struggle against
private ownership, then the intellectual battle is just about over.

T cannot resist citing Oscar Wilde’s definition of a cynic: “a man
who knows the price of everything and the value of nothing.” This is
Mr, Gish’s problem. What Mr. Gish does not understand is that
thieves reduce the value of everyone’s property, both rich and poor, but
especially the poor who live in ghettos where crime is rampant.
Gish’s comment reveals that he has failed to understand the eco-
nomic reasoning behind Allen and Alchian’s conclusion. It is not
that prices necessarily go up when crime is reduced (although they
may), thereby excluding the poor; it is that the value of goods goes
up, including the value of property owned by the poor. The poor get
richer, not poorer. Gish confuses increascs in the value of property
with increases in the cost of living, Mr. Gish is so red-faced in his
hatred of capitalism that he cannot understand a simple economic
argument. If the poor now enjoy property that is worth.more, why
are they oppressed under capitalism? They aren’t, unless they are eaten
up by envy, and hate to see the rich also get richer-~—hate it with such
intensity that they would give up their own increases-in order to tear
down the rich.

The decrease in the valuc of property as a result of theft would
also occur in a socialist economy. Official prices might not change —
who knows. what a socialist planning board might do to prices in
response to crime? —-but the value of goods would drop. This’ has
nothirig to do with the structure of a particulat economy; it has
everything to do with the effects of crime on people’s assessment of

11. Art Gish, “A Decentralist Response,” in Robert Clouse (ed.), Wealth and Pov-
erty: Four Christian Views on Economics (Downers Grove, Illinois: InterVarsity Press,
1984), p. 75.
