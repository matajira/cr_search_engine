148 THE SINAI STRATEGY

Value and Price

Let us consider the effects of a wave of theft on market prices. If
we understand what is going on in this scenario, we probably have a
firm grasp of economic theory. It can serve as a good example. Mr.
Gish may even understand it. Under capitalism, any additional self-
government and self-restraint against theft will tend initially to raise
the market price of goods above which prevailed prior to the wave of
thefts. So will any cost-effective increases in the civil government's
war against thieves. Let us look at the sequence of events.

First, the wave of thefts begins. Assume that it is national in
scope and horrendous. People are afraid to leave their homes. They
reduce the number of shopping trips. They put more money in the
bank, since banks are perceived to be safer against bank robbers
than homes are against burglars. In other words, they decide to buy
fewer stealable goods. Demand for consumer goods thcrefore drops.

On the other hand, the supply of available goods initially rises.
Stolen goods that would not have been offered for sale by their
owners at the older, higher prices, begin to enter the resale markets.
These goods carry price discounts. Honest producers of goods must
compete by lowering their prices. Production of new goods drops.
New goods producers begin to go into bankruptcy and start selling
goods at huge discounts. Then, after they sell off inventories, some
of them stop producing.

In short, prices drop because the value of goods has dropped.
Why has the value of goods dropped? Because the costs of ownership
have risen. If you raise costs, you should expect reduced demand.
This is what we do see: the demand for consumer goods drops. This
is especially true for poor people, who are more vulnerable to theft
and violence in their communities. The value of their presently owned
goods is drastically reduced because the costs of ownership for them
have been drastically increased.

As I have already pointed out, Mr. Gish is not used to this sort of
economic reasoning, so he resorted to his knee-jerk policy of criticiz-
ing capitalism for the evils of both increased crime and decreased
crime. In good times and bad times, capitalism is evil. He is not
alone in his hostility to capitalism. It is the characteristic feature of
literati everywhere. 2

12. Ludwig yon Mises, The Anti-Capilatist Mentality (Princeton, New Jersey: Van
Nostrand, 1956).
