Conclusion 219

Lyndon Johnson — crass, calculating, coercive, and above all, unstyl-
ish—removed much of their hope in the older faith. Furthermore,
the rise of alternative theologies undermined the oldcr theological
liberalism; politically pessimistic (Reinhold) Niebuhrism, non-
rational Barthianism, and New Age transcendentalism. ‘Vhe spirit-
ual odyssey of Harvard theology professor Harvey Cox is represen+
tative, though somewhat flamboyant: from outright secular human-
ism (old liberal-style rationalism) to irrationalism to liberation thcol-
ogy.!* Cox was the leading theological weather vane of the decade,
1965-75, and every four years, he switched posi

In place of the old secular socialism has arisen a new critique of
capitalism. Capitalism is evil, we are now informed by the critics,
because it is too growth-oriented. Economic growth is a liability.
More than this: economic growth is a sin, We find the “simple life style”
people advocating on principle a reduced division. of labor and lower
per capita income, especially for rich nations — that is, the nations in
which guilty readers can afford to buy mass-produced, low-cost
paperback diatribes and monthly magazines,

Paralleling the transformation of the secular socialists, the
church has produced “radical Christianity,” sometimes known as
“liberation theology.” In some senses, these are two different move-
ments. The latter movement tends to be more Marxist; the former is
more likely to be made up of Anabaptist pacifists. Sometimes their
memberships overlap. The. more hard-core liberation theologians
tend to be Roman Catholic. The radical Christians are usually Prot-
estants: neo-evangelicals, sometimes Reformed (seminary professors
and younger seminary graduates), and especially Anabaptists.

We find so-called radical Christians (who are openly the spiritual
heirs of the radical Anabaptist sects of the sixteenth century) espous-
ing the “small is beautiful’ philosophy of “neo-Gandhian” E. F.
Schumacher, author of Buddhist Economics, as well as Smail Is
Beautiful. Schumacher’s recommended economic system is consistent
with his religious presuppositions; the “radical Christians” are either

 

 

 

14. Harvey Cox, The Secular City: Secularization and Urbanization in Theological Per-
spective (New York: Macmillan, 1965); The Feast of Fools: A Theological Lissay on Festivity
and Fantasy (Cambridge, Massachusetts: Harvard University Press, 1969); The Se-
duction of the Spirit: The Use and Misuse of People’s Religion (New York: Simon & Schus-
ter, 1973)

15. E. J. Mishan, The Costs of Economie Growth (New York: Praeger, 1967);
Mishan, The Economic Growth Debate: An Assessment (London: George Allen & Un-
win, 197).

    
