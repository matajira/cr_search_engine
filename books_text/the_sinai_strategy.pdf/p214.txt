188 THE SINAI STRATEGY

self and His creation. ‘There are too many volunteer agents
(gossips), and the market for false rumors is wider and more easily
accessible than the market for truth, with its greater precision and its
comparative lack of rebellion-filled excitement. False rumors are like
mistresses: more exciting initially than wives, but more deadly. This
is why Proverbs compares false knowledge with harlotry (Prov.
7:6-23), and compares wisdom with the honest woman crying in the
streets, ignored by the inhabitants of the city (Prov. 1:20-33).

Because men are evil, the transmission of false reports against
the just citizens is subsidized. This subsidy by the ungodly—their
preference for falsehood — reduces the per capita wealth of a society,
for decisions made in terms of false information are far less likely to
produce beneficial results at the lowest possible costs.

Advertising

Advertising is not well understood by social commentators.
There has been a great deal of criticism aimed at advertising in gen-
eral and the advertising industry in particular.? Many sorts of eco-
nomic evils are laid at the door of advertising, especially the creation
of new wants—wants that become “needs” in the minds of the masses.
This is an odd criticism, coming as it does from cducated people.
What was the university, or the inventor's laboratory, other than a
means of creating hitherto unappreciated opportunities (“wants”) for
those who had not previously considered them? How can we im-
agine the operation of the famed institution, “the marketplace of
ideas,” apart from men’s quest for better arguments, morc effective
presentations, and improved communications?

Property Rights in a Name
Christian commentators have failed to recognize the biblical
foundation of advertising. All advertising stems from the commandment

7. Criticism of advertising has been a constant theme in the writings of John
Kenneth Gailbraith. Cf, The Affluent Society (Boston: Houghton Mifilin, i958), pp.
155-58; The New Industrial State (2nd ed.; Boston: Houghton Mifflin, 1971}, pp.
203-9, 273-74. He comments: “The educational and scientific estate and the larger
intellectual community tend to view this effort [modern advertising] with disdain”
{p. 293). For a self-professed Christian's similar disdain, see Ronald Sider, Rick
Christians in an Age of Hunger (Downers Grove, Illinois: Inter-Varsity Press, 1977),
pp- 46-50, This book had gone through eight printings by 1980, three years aficr its
original publication. I cite it, not because it is the only neo-evangelical book to take
such a position, but because it is the representative book.
