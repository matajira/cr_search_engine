God's Monopoly of Execution 119

by man’ the verse reads $y man his blood is to be shed?’ We stumble
over the ‘by man’ due to its obtrusion and conspicuousness. Man’s
being made as God's image explains the infliction of the death penalty
by man.” In other words, “the proper question at Genesis 9:5f. is:
what right has man to retaliate against the murderer? Genesis 9:6
gives the rationale: man is God’s image.”*

Bahnsen’s interpretation is an attempt to force us to choose be-
tween two views: 1) the image of God in man as the cause of the death
penalty—the reason why such a harsh penalty must be imposed —
and 2) the image as the justification of the civil government’s God-
given authority to inflict the penalty. I do not choose between the two
interpretations; I choose them both. The image of God in man
makes sacred the life of man, assuming he has not transgressed the
law in a capital crime, but it also legitimizes the execution of the
transgressor in the case of murder. Both the reason _for the death pen-
alty against murderers and the requirement of capital punishment by
the civil government are explained by the presence of the image of
God. But there is a stronger emphasis on the image of God in man as
the reason why murder must be punished by the death penalty, as I
have already argued (footnote 2): the execution of man-killing ani-
mals required by Genesis 9:5 points more clearly to the magnitude
of the crime than it points to the right of the civil government to
inflict the supreme earthly penalty. But ultimately it points to both.

Delegated Monopoly

God has shared His monopoly of execution with men. The final
power of death is held by Jesus Christ. “I! am he that liveth, and was
dead; and, behold, I am alive for evermore, Amen; and have the
keys of hell and of death” (Rev. 1:18). 1t is He who triumphed over
death (I Cor. 15). Christ is the go’ e/, the kinsman-redeemer who is
also the family avenger of blood (Num. 35:19). Satan himself could
not take Job’s life without God’s permission ( Job 3:6). Only the crea-
ior of life has the original right to destroy life; only He can establish
the standards by which man’s life may be legitimately removed, in-
cluding the standards of execution by the civil government.

The biblical view of the State unquestionably and irrefutably
affirms the right and obligation of the State to execute men, for the
Bible sets forth God’s law. God has delegated this power to the State.

4, Ibid., p. 444,
