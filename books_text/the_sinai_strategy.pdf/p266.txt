240 THE SINAI STRATEGY

a handful of the fruit of the ground (Deut. 23:23-25). Jesus was not
criticized for having taken the corn. He was criticized for having
taken the corn on the sabbath.

What was Christ’s answer? He pointed to David's taking of the
showbread from the temple on the sabbath (Matt. 12:3-4). Here was
a far more culpable act, for it was not lawful for David or his follow-
ers to eat the showbread, since it was reserved for the priests (v. 4).
Yet it was necessary that they be fed. They were godly men involved
in an important work. The priest himself had suggested that David
take the hallowed bread (I Sam. 21:21-24)}. Yet God commanded that
this showbread be set before Him at all times (Ex. 25:30; Num. 4:7).
But the needs of men were more important in this instance, a fact recognized
by the priest. On the one hand, the priest had to offer sacrifices. On
the other hand, David had to flee from the wrath of Saul. Both re-
quirements were cases of necessity. But the priest told David to eat
the showbread. How, then, could the priests of Jesus’ day legiti-
mately criticize Him?

Jesus’ healing of the man with the withered hand was a work of
mercy. Traditional Christian sabbatarianism has always made ex~
ceptions of these two works, necessity and mercy. But necessity and
mercy impose even greater pressures on men’s actions than merely
offering exceptions to the sabbath requirement against labor. Neces-
sity and mercy require positive action. This is acknowledged by the
Wesiminster Confession of Faith (1646), a pro-sabbatarian’ docu-
ment, which forbids men to think “about their worldly employments
and recreations,” and requires them to take up “the whole time, in the
public and private exercises of His worship, and in the duties of ne-
cessity and mercy.”5

‘The priests of the Old Testament profaned the sabbath, yet they
were blameless. The office of priest, coupled: with a mandatory
assignment from God, permitted the profaning of the sabbath. In-
decd, it requires it. Yet David was not a priest, nor were his men. This
points to the truth of Christ’s words, that the “Son of man is Lord
even of the sabbath day” (Matt. 12:8). In His incarnation, as the son
of man, Christ ruled the sabbath. The account in Mark is even
clearer: “The sabbath was made for man, and not man for the sab-
bath” (2:27-28). When human life and health are at stake, the sab-
bath may be profaned without blame. It must be profaned. When an

5. Westminster Confession of Faith, XXI: VIII.
