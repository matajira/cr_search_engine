232 THE SINA1 STRATEGY

and the specific details of these public worship services are nowhere
described in the Old Testament.

A. T. Lincoln has fairly described our present state of knowledge
concerning the celcbration of the Hebrew sabbath in Old Testament
times: “The sabbath was not a day of total inactivity but was meant
to provide rest and refreshment from the regular work of the six
other days. It is truc that this rest provided opportunity for devotion
to the worship of God, that the Sabbath was called a ‘holy convoca-
tion’ (Lev, 23:2-3), that an additional burnt offering was required on
every Sabbath (Num. 28:9, 10), and that since it was done from obe-
dience to God the resting itself could be considered an act of wor-
ship, but cultic worship was not a major focus of the Sabbath institu-
tion for Israel as this is reflected in the Old Testament.”! This is my
concern: lo discern the major focus of the Old Testament sabbath. It
was rest, not worship.

The Hebrews were supposed to delight themselves in Gad. In the
oft-quoted words of Isaiah: “If thou turn thy foot from the sabbath,
from doing thy pleasure on my holy day; and call the sabbath a
delight, the holy of the Lorp, honourable; and thou shalt honour
him, not doing thinc own ways, nor finding thine own pleasure, nor
speaking [thine own] words: Then shalt thou delight thyself in the
Lorn; and I will cause thee to ride upon the high places of the earth,
and feed thee with the heritage of Jacob thy father: for the mouth of
the Lorn hath spoken it” (Isa. 58:13-14). They were to acknowledge
the God-centered nature of creation.

What did it mean, “doing thy pleasure”? We are not told, except
in reference to commercial activities and the common household
chores of cooking, gathering sticks, and carrying burdens in and out.
Idle talk was forbidden. But what kind of talk, specifically, consti-
tuted idle. talk, “speaking [thine own] words”? We are not told. As far
as the written record indicates, neither were they.

‘Vhe law said nothing about the legality, or even propriety, of the
following activities: napping in the afternoon, walking in a garden
(park), listening to music, going for a (non-commercial) swim,
floating in a small boat, and having sexual relations with one’s
spouse. In short, there are no guidelines in the law concerning the

1. A. ‘I. Lincoln, “From Sabbath to Lord’s Day: A Biblical and Theological
Perspective,” in D. A. Carson (ed.), From Sabbath to Lord's Day: A Biblical, Historical,
and Theological Investigation (Grand Rapids, Michigan: Zondervan Academe, 1982),
p. 352.
