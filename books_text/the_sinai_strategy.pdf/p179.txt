The Rights of Private Property 153

it has led to reduced productivity and an increase in bureaucracy.
The politicians are simply not competent enough to plan for an en-
tire economy..? To promote such a system of State planning and pro-
tection of industry is an illegitimate use of the ballot box, meaning
democratic pressure politics.

Property and Voting

Let us consider an example which has been debated from the
Puritan revolution of the 1640's until today: the property qualification for
voting. At the Putney Debates of Cromwell’s New Model Army in
1647, Ireton, Cromwell’s son-in-law, debated Rainsborough, the rep-
resentative of the democratic faction, the Levellers. (The Levellers
were not communists, but they were committed to a far wider fran-
chise. The communists in the English Revolution were the Diggers,
who called themselves the “True Levellers.”!®) Rainsborough argued
that since all m2n are under the Jaws of a nation, they deserve a
voice in the affairs of civil government, Ireton countered with a ring-
ing defense of property rights. A man must have some stake in soci-
ety, meaning property to defend, if he is to be entrusted with the
right to vote. Men without permanent interests in the society—
property, in other words—are too dangerous when handed the
power of civil government. The property qualification is crucial to
preserve society in a democratic order. “And if we shall go to take
away this, we shall plainly go to take away all property and interest
that any man hath either in land by inheritance, or in estate by
possession, or in anything else. . . .”!9

Two centuries later, Karl Marx concluded much the same, ex-
cept that he favored the abolition of the property qualification for
voting, precisely because it would destroy private property: “. . . the
state as a state abolishes private property (i.e., man decrees by political

 

1976); Charles Higham, Fading With the Enemy: An Expost of the Nazi-American Money
Plot, 1933-1949 (New York: Delacorte Press, 1983), There is little evidence that Ger-
man big business fmanced Hitler: Henry Ashby Tumer, Jr., German Big Business and
the Rise of Hitler (New York: Oxford University Press, 1985).

17. The Politics of Planning: A Review and Critique of Centralized Planning (San Fran-
cisco: Institute for Contemporary Studies, 1976). See also Don Lavoie, National Eco-
nomic Planning: What Is Left? (Cambridge, Massachusetts: Ballinger, 1985), a detailed
criticism of the idea of central planning.

18. Christopher Hill, The World Turned Upside Down: Radical Ideas During the
English Revolution (New York: Viking, 1972), ch. 7.

19. A. S. P. Woodhouse (ed.), Puntanism and Liberty, Being the Army Debates (1647-9)
(London: Dent, 1938), p. 53.
