Covenantal Law and Covenantal Lave 313

not” pictists — the defenders of the escapist religion—is to deny the ex-
istence of this covenant. But if Christians deny the existence of a
law-covenant—if they deny that all men are under God’s dominion
covenant —and if they deny that there are eternal laws that serve as
standards by.which all men are required to perform, then how is the
sinner ta be confronted with the reality of his sin? If Christians are
incapable of helping unregenerate men see their sins, and if they are
therefore incapable of assisting newly regenerate men to overcome
their newly perceived sins, then what happens to church discipline?
The institutional answer of the pietists to this question has been éo
deny the necessity of church discipline. The consistent ones go so far as to
deny the legitimacy of much of civil government, too. They deny the
death penalty for capital crimes. Some of them do everything possi-
ble to promote the State as a substitute parent, but a parent without
arod of discipline. Others simply deny all civil law whatsoever — and
therefore are compelled to deny the continuing authority of the Old
Testament.?

The power-seeking religionists understand the centrality of judg-
ment and discipline, but they have substituted the State in God’s
place. Thus, they seek to expand the centralized power of the State,
and to extend the State’s power over every area of life. They seek to
worship their God, human power, by incorporating it into a political
monopoly. Théy understand the fixed relationship between sover-
eignty, power, and judgment. As agents of collective mankind, they seek
to become agents of the power State. They seek ever-increasing op-
portunities to exercise judgment.

This is why the power religionists always find allies with the es-
capist religionists. The escapist religionists point to the power of hu-

2. Mark McGulley, “Faith and Freedom: A Fifth View of Christian Economics,”
Nomos, II (Winter 1984}. McCulley calls his anti-Old Testament, anti-civil govern-
ment position the “economics of Christian exile.” This is well-named. Evxile is the
essence of the escapist religion. He ends his article with a partial citation of John
Wesley: “earn all you can; give all you can.” He deliberately ignores Weslcy’s third
principle, save all you can, which is the foundation of economic growth and lincar
development unto dominion. McCulley is hostile to such a view, for he understands
the thrift principle well cnough to see where it Jeads in principle, and where it has
led in the past: to modern industrial capitalism. He does not hate capitalism, unlike
so many of his Anabaptist colleagues; he hates growth-oriented industrial produc-
tion. This is why he praises as followers of Jesus’ New ‘lestament ethic “Ballou, the
Hopedale community, and a few ‘come-outers’ ” in the post-Civil War Christian era.
“Down on the farm” communalism has long been the final resting and retreating
place for pacifist Anabaptists. The revolutionary Anabaptists have generally headed
for the cities, in order to consolidate power.

 
