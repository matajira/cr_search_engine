xiv THE SINAI STRATEGY

The Ten Commandments

What Sutton had not seen when he wrote his first draft was that
the ten commandments adhere to this same structure, even its very
numbering. More than this: once we recognize that this structure
undergirds the ten commandments, we come to a remarkable in-
sight: there really are two “tables” of the law. No, there was not a
pair of stones, with five laws written on each. There were two tablets
with all ten written on each. They served as copies, one for God and
one for Israel, in much the same way as a modern sales receipt,
which is implicitly modeled after God’s covenant. But there were two
separate sections of the ten commandments (literally: ten “words”
(Deut. 4:13]). They were arranged along this same covenantal pat-
tern in two separate sections, 1-5 and 6-10.

In the Bible, there is to be a two-fold witness to the truth. Convic-
tion for a capital crime requires two witnesses (Deut. 17:7; Num.
35:30). Satan in the garden sought two human witnesses against
Gad, to test God’s word and therefore challenge it. There are two
angelic witnesses for every demon, for Satan only took a third of the
angelic host with him (Rev. 12:4), Revelation 8 provides a deeply
symbolic description of God's earthly judgment. He sends angels to
judge one-third of: trees, sea, creatures, ships, rivers, waters, sun,
moon, stars. In short, two-thirds are spared. This is the testimony to
God's victory, in time and on earth. The double witness pattern is
basic to covenantal law and historic judgment,?

What we find is that the very structure of the ten commandments
serves as a two-fold witness to the structure of the covenant. Sutton
subsequently concluded that the first five-part pattern deals with the
priestly functions, while the second five-part pattern deals with the
kingly.

I. First Table (priestly)

The traditional distinction between the “two tables of the law” is
based on 1) what man owes to God, namely, proper worship (first
table) and 2) what man owes to his fellow man (second table). The
problem has always come with the fifth commandment, which re-
quires children to honor parents. This one seems to violate any

3. Gary North, “Witnesses and Judges,” Biblical Economics Today (Aug. /Sept.
1983).
