Familistic Capital 101

Another result is the reduction of per capita productivity, and there-
fore per capita wealth, even for those who appeared initially to be
favored by the legislation.

Compound Interest

The importance of the continuity of capital ¢an be seen in any
example involving compound interest. Let me say from the begin-
hing: wé cannot expect to see this compound interest phenomenon
continue uninterrupted in any family forever. We also cannot expect
to see annual rates of growth over 1% for centuries at a.time. As I like
to point out, the 4 billion people on earth in 1980 would multiply to
over 83 trillion in a thousand years, if the rate of population growth
were 1% per annum. But the fact remains, the longer the compound
growth phenomeérion continues, the smaller the annual percentage
increase needs to be in order to produce spectacular results.

Let us assume that we are dealing with a given monetary unit.
We can call it a alent. A young married man begins with 100 talents.
Say that he multiplies this capital base by 2% per annumn., At the end
of 50 years, the couple has 269 talents. Let us assume that the heirs
of the family multiply at 1% per annum, on the average, throughout
each subsequent family’s lifetime. After 250 years, if the growth
rates both of people and capital persist, the total family capital base
is up to 14,126 talents. Divided by 24 family units, each family now
has 589 talents. This is almost a 6-fold increase per family unit,
which is considerable. We now have 24 family units, with each fam-
ily possessing almost six times the wealth that the original family
started out with, even assuming that each heir has married someorie
who has brought no capital into the marriage.

What if the capital base should increase by 3%? At the end of 50
years, the original couple would have 438 talents, over a 4-fold in-
crease, ‘This is quite impressive. But at the end of 250 years, the fam-
ily would possess 161,922 talents, over 1,600 times as large. Even
divided by 24 family units, the per family capital base would be
6,747 talents, or over 67 times larger than the original capital base of
100 talents.

Consider ‘the implications of these figures. A future-oriented
man—a man like Abraham—could look forward to his heirs’
posséssing vastly greater wealth than he ever could hope to. attain
personally. This is the kind of vision God offers His people, just ashe
offered to Abraham: heirs two or three generations later who will be
