5
FAMILISTIC CAPITAL

Honor thy father and thy mother: that thy days may be long upon the land
which the LORD thy God giveth thee (Ex. 20:12).

Paul tells us that this is the first commandment to which a prom-
ise is attached (Eph. 6:3). What does it mean, “that thy days may be
jong upon the land which the Lorp thy God giveth thee”? It is a
promise given to the nafion. It is a collective promise, not an in-
dividual promise as such. God does not promise that every single
child who shows honor for his parents will enjoy long life, nor does
He assure us that every single dishonoring child will die young. Esau
went against his parents’ wishes when he married Canaanite women
(Gen. 26:34-35), yet he lived to be at least 120, for he and Jacob
buried Isaac, who had died at age 180 (Gen. 35:29), and they had
been born when Isaac was 60 years old (Gen. 25:26). Joseph was
alive at this time, and the Bible speaks of Joseph as the son of Jacob’s
old age (Gen. 37:3). In the case of Esau, a dishonoring child lived
into old age. Abel, who honored God, and who presumably honored
his parents as God’s representatives, was slain by his violent brother,
who in turn survived to establish a pagan civilization (Gen. 4).

What God does promise is that a socdety in which the majority of
“men do honor their parents will be marked by the long life expectancy
of its members. This longer life span will be statistically significant.
The society will enjoy, for example, lower life insurance premiums
in every age bracket compared with the premiums in cultures that
are marked by rebellion against parents. In other words, the risk of
death in any given year will be lower, statistically, for the average
member of that age bracket. Some will die, of course, but not so many
as those who die at the same age in a parent-dishonoring culture.

The promise is significant. It offers long life. The very first prom-
ise that is connected to a commandment is long life. This is in-

94
