234 THE SINAI STRATEGY

booths, and so forth. Instead of profit-seeking labor, we find works of
mercy.

In a predominantly rural society, most people could enjoy the
sight of their fields. They could go for a stroll in the “garden.” In an
urban society, people can go for a stroll to view front lawns. They
can visit friends for a chat. But then we are back to another bother-
some question: What constitutes idle talk? Talk about families? Talk
about sports events? Talk about politics? “lalk about the stock
market? We are not told. Conscience must be our guide. But cons-
cience is difficult to put into concrete legislative proposals. In fact, it
is because men have not universally defined “idle talk,” that they
resort to the language of conscience or circumstance.

If we take the Old Testament legislation seriously, we are faced
with a conclusion that tends to alienate the guili-manipulated and
socialism-influenced Christian: the rich were allowed to enjoy
recreation activities that were legally prohibited to the poor, who
were not allowed to lease or rent such recreation implements or op-
portunities on the sabbath. It might be argued that the law allowed a
man to buy a “seven days a week” ticket to recreation opportunities,
but if someone had to collect tickets on the sabbath, or in some way
monitor his profit-seeking operation on the sabbath, then any judge
who understood basic economics would have shut down the opera-
tion as a sham, an attempt to escape the clear-cut prohibition on
commercial activities on the sabbath. It paid to be rich on the sab-
bath. (Of course, it normally pays to be rich on the other six days of
the week, too.)

Carrying burdens in and out of doors was illegal (Jer. 17:22).
Profit-seeking work was illegal. But leisure is a consumer good. It
must be paid for by forfeited income — income not earned during the
leisure period, Leisure could be “stored up” in effect. It was legiti-
mate to enjoy leisure on the sabbath, but only that kind which could
be “stored up” in the form of capital goods: privatc gardens, private
lakes, and so forth.

“Works of Mercy” in Rural Society

Israel was a rural society. Certain daily chores are works of ne-
cessity on a farm, such as milking and caring for the animals, But
what was done with the milk? Was it thrown away? Was it saved
only for other animals? Was it given to the poor? If it was sold at a
profit, then milking constituted profit-seeking activity, i.c., engaging
