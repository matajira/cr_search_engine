358

reward, 143
rhythm, 237, 269, 278, 290, 299
Ridderbos, Herman, 335
Tigidities, 134-35
risk, 64, 65, 152, 161, 257
rites, 28-29, 32
ritual, xiii, 35, 37, 40, 306
riots, 155
Road to Sefdom, 201
robber barons, 103
Roberts, Paul C. 173-74
Rolls Royce, 173-74
Roman Catholicism, 8, 35
Romans, 265
Rome, 29, 47
Roosevelt, Teddy, 169
Rothbard, Murray, 135-36
rumors, 187-88
rural, 234, 236-38, 268-69, 278,
282-83, 290
Rushdoony, 18n, 19n.
church, 56
gambling, 47
humanists’ God, 31
Institutes, 269n.
oaths, 55-56
power from below, 61
third commandment, 51
Ryle, J. GC. 288

sabbath

Adam &, 74

annulment, 237

calling, 239

capital, 237

ceremonial, 24t

changes, 252

civil government, 250-55

civil law, 79-80

conscience, 228, 242-45, 250

costs, 257

consumption, 235

covenant, 245

death penalty, 78-80, 229, 238, 252,
258, 255, 265, 267, 276

desecration, 288

dominion covenant, 92

THE SINAI STRATEGY

false witness, xxi

family, 235

fire, 230

fuel, 259

grace, 77

guilt, 277

healing, 238-39

judgment day, xvi-xvii

leisure, 234, 286-89, 298

locus of enforcement, 255

Lord of, 240

meaning of, 72, 76-78

mercy, 234-36, 240

manna, 230-31

marathon, 235, 273-75, 293, 301

moral, 242

mosaic, 242

music, 289

not Lord’s day, 278

ostracism, 254

pleasure, 245

profaning, 239, 240, 248, 284, 290,

291

profit, 237

recreation, 232-34, 245

rest, 232, 300, 301

rhythm, 237

rich man, 233-34

rural, 234

sanctions, 92

Satan, 73-74

strange fire, 264-66

symbol, 76

talk, 234

technology, 260, 263, 267-68, 282,

294

timing, 81:85

violations, 78, 238

week, 242-43

worship, 232, 303

(see also Lord’s day)
Sabbath millenium, 86-92
sabbatical year, 41, 85
subordination, 49
sacraments, 53
sacred fire, 20
sacrifices, 45n
