6 THE SINAI STRATEGY

eternal validity of an eternal law-order of an eternal God. They
worry about offending the defenders of “a contextualist and existen-
tialist ethic,” meaning their old professors at Yale Divinity School (or
wherever). They survey the strongholds of these situation ethicists,
and rather than seeing the looming collapse of humanist civilization
as the greatest opportunity in man’s history for the triumph of God’s
law as the only possible substitute for this collapsing moral order,
they urge faithful Christians to restrain themselves. Why, such
efforts might embarrass these waffling theologians among their
peers, and their peers are not the tithing people in the pews who pay
their salaries, but the tenured atheists in the prestige divinity schools
that awarded them their coveted (and low market value) doctorates.
(I've got a Ph.D. myself; I know how little it is worth these days.)

In short, these fearful, obscure, and academically irrelevant
drones, with their Ph.D.’s, their tenured seminary positions, and
their minimal prospects for future employment if righteous Chris-
tian people ever purge the seminaries of heretics, now see what is
coming: a revival of interest in God’s law, and the rapid development
of political skills on the part of those who take God’s law seriously.
They see their liberal, pleasant, tenured little world on the verge of
disaster, for those naive people who have funded their rebellion— the
little people in the pews—may soon catch on to their game. The
court prophets are once again in trouble on Mt, Carmel. They saw
what happened last time, and they are not happy about it.

Sadly, they have allies in the conservative camp: those who
preach the irrelevance of the ten commandments in New Testament
times. But pietism’s influence is also waning in the latter decades of
this century, The ecclesiastical irrelevance of the older pictistic fun-
damentalism is becoming pronounced, What has taken place in the
United States since 1980-really, since 1965—has exposed the
nakedness of the fundamentalist antinomians. They had no con-
crete, specifically biblical social answers for the radicals of the late
1960’s, and they knew it. They went into retreat in the 1970's, and
they are now being ignored into oblivion.

At last, conservative Christian laymen, and even a growing
number of pastors, are beginning to see the light. They are begin-
ning to understand the choices laid before them:

God's law or chaos
God’s law or tyranny
God's law or God’s wrath
