314 THE SINAI STRATEGY

manist man, who is ultimately satanic man, and they conclude that this
power is an aspect of Satan’s control over the earth until Jesus comes
again. When these escapist religionists are confronted with the respon-
sibilities associated with the dominion covenant, they recoil in horror.
Dominion, in their eyes, is too much like autonomous man's power. To
adopt such a view of Christianity would mean that they would have to
become involved in a head-on, lifetime confrontation with Satan’s
earthly kingdom of power, They would have to begin to exercise judg-
ment. They prefer to stay in the shadows of history in the name of a
“higher spirituality” or a “higher calling” from God. They prefer to avoid
the visible, civilizational confrontations, Thus, the power religionists can
enlist the retreatists as allies in their war against covenantal religion.
The standard ploy of the theological liberals in the United States
from the late nineteenth century until they consolidated ecclesiasti-
cal power in the 1920’s and 1930's in the North, and in the 1950's and
1960’s in the South, was to criticize all heresy trials—where they
were going to be the victims—in the name of institutional peace and
toleration. They directed this incomparably successful appeal to the
weak-hearted souls in the churches. These people wanted institu-
tional peace above all. Until the liberals gained complete control and
shoved them aside, these conservative battle-avoiders had a majority
in évery major denomination. Decade by decade, the liberals quietly
consolidated power: in seminaries, in colleges, and in the churches’
various boards, especially the missions boards, When the theologi-
cally committed conservatives finally realized what had happened, it
was too late. They could no longer gather theologically committed
troops for a fight. The theelogy of a majority of the conservatives
was “peace at any institutional price.” So they paid the highest possi-
ble price: the capture of their churches by the opponents of biblical
Christianity. In‘ the churches with a strong hierarchy, the liberals
eventually pushed out the orthodox pastors, with the exception of
the Missouri Syned Lutherans.’ In the decentralized associations,
they simply isolated the orthodox men from the seats of power. This
has always been the humanists’ strategy. With only a few exceptions,
it has worked superbly in this century, The archetype was the cap-
ture of the Presbyterian. Church, U.S.A. (Northern).* This strategy

 

3. Kurt E. Marquart, Anatomy of an Explosion: A Theological Analysis of the Missouri
Synod Conflict (Grand Rapids, Michigan: Baker Book House, 1977).

4, Sce Gary North, Rotten Wood: The Capture of the Presbyterian Church, U.S.A.
(forthcoming).
