44 THE SINAI STRATEGY

God, with respect to the external blessings that He was about to give
them, and were reminded of their covenantal responsibility to obey
His law and teach it to their children (Deut. 6:5-9).

And it shall be, when the Lorn thy God shail have brought thee into the
jand which he sware unto thy fathers, to Abraham, Isaac, and to Jacob, to
give thee great and goodly cities, which thou buildest not, and houses full of
good things, which thou filledst not, and wells digged, which thou plantedst
not; when thou shalt have eaten and be full; Then beware Jest thou forget
the Lory, which brought thee forth out of the land of Egypt, from the house
of bondage (Deut, 6:10-§2).

The compound growth rate of evil is temporary. Such growth is
always brought into judgment by God. The “positive feedback” of
growth is always overturned by the “negative feedback” of judgment
—sometimes overnight, as in the case of Babylon when it fell to the
Medo-Persian Empire (Dan. 5). The compound growth rate of
righteousness is long term. More than this: it is perpetual. God shows
mercy to thousands of generations, meaning throughout history and
(symbolically) beyond history. But this growth process does include
history; generations are historical phenomena. There can be ‘inter-
mittent departures from faith which interrupt the growth process.
But the contrast is between a brief period of three or four unrighteous
generations and a stupendously long period of mercy to those who love
God and keep His commandments. The magnitude of the growth
period of mercy and mercy’s works is enormous, compared with the
growth period of evil.

Exponential Righteousness

The implication should be obvious: the capital base of righteousness
will grow to fill the earth over time. Even a little growth, if compounded
over a long enough period of time, produces astronomically large
results —so large, in fact, that exponential growth points to an even-
tual final judgment and an end to time, with its cursed, scarce crea-
tion.*¢ The righteous widow’s two mites (Luke 21:2-4), if invested at
1% per annum over a thousand generations, would be worth more
than all the wealth on earth. In other words, the concept of “a thou-
sand generations” is symbolic; it means everything there is, a total
victory for righteousness. Furthermore, this victory is no overnight

30. See Gary North, Moses and Pharaoh, ch. 17; “The Metaphor of Growth:
Ethics”; ef. North, Dominion Covenant: Genesis, pp. 174-76.

 
