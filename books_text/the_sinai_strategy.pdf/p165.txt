8
THE RIGHTS OF PRIVATE PROPERTY
Thou shalt not. steal (Ex. 20-15).

It has long been recognized by Christian commentators that the
biblical case for private property rests more heavily on this passage
than‘on any other passage in the Bible. Individuals are prohibited by
biblical law from forcibly appropriating the. fruits of another man’s
labor (which includes his ideas), or his inheritance. The civil govern-
ment is required by the Bible to defend a social order based on the
rights of private ownership. The various laws requiring restitution
that are found in Exodus 22 explicitly limit the State in its imposition
of sanctions against thieves, but there can be no doubt that it is the
civil government which is required to impose them.

Rights of ownership mean that God transfers to specific men and
organizations the sole and exclusive ability to use specific property
for certain kinds of ends, and the State is to-exélude others from the
unauthorized use of such property. Property rights therefore refer to
legal immunities from interference by others in the administration of
property. The duties associated with dominion are more readily and
effectively achieved by individuals arid societies through adherence
to the private property system, which is onc reason why the Bible
protects private ownership. Private property is basic to effective dominion.

The only conceivable biblical argument against this interpreta-
tion of the commandment against theft would be an assertion that
the only valid form of ownership is ownership by the State, meaning
control by bureaucracies established by civil law. But to argue along
these lines demands evidence that the Bible, both Old Testament
and New Testament, authorized the public (State) ownership of all
goods. There is not a shred of evidence for such a view, and massive
evidence against it. The tenth commandment prohibits coveting the
property of a neighbor, which is plain enough. The biblical social

139
