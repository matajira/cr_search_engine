The Economic Implications of the Sabbath 305

concerning the exact ways in which any profit-seeking firm or indi-
vidual must honor the sabbath principle. God will be the final judge,
not the earthly institutions of government. There are objective
standards, but they must be interpreted subjectively, person by per-
son, in the New Testament era. We have been given specific revela-
tion to this effect with respect to the sabbath, and we must honor this
revelation.

It solves few if any concrete sabbatarian problems to read into
Leviticus 23:3 an Old Testament sabbath version of the New Testa-
ment’s requirement of positive worship on the Lord’s day. The testi-
mony of the Old Testament is clear: Seventhday was generally (and
possibly even exclusively) a day of rest, except for a few priests in the
temple. It is equally futile to read into Exodus 35:3 a highly symbolic
and hypothetical interpretation concerning “strange fire.” Clear texts
should be used to interpret obscure texts. Even more to the point,
clear texts should not be turned into obscure texts. Exodus 35:2 is
clear: death for working on the sabbath. Exodus 35:3 is also clear:
no kindling of fires. An apologetic for a hypothetical “less rigorously
enforced Old Testament sabbath” which is then said to be in con-
tinuity with a church government-enforced and State government-
enforced New Testament Lord’s day—an apologetic based on
“strange fire” —is clear to practically nobody, which is why we find no
similar line of argumentation in the historic creeds. It also fails to ex-
plain the sharp discontinuity which was announced by Paul in
Romans 14:5 and Colossians 3:16.

In short, if Paul’s words are not taken at face value, a whole
series of problems arises. Few churches have been willing to face
these problems squarely over the last two hundred years, and none
has been willing so far to deal forthrightly with the question of the
death penalty in Exodus 35:2. There is no way, biblically speaking,
to escape the necessity of imposing the death penalty on persistent
sabbath violators, unless we interpret Romans 14 as having changed
the locus of enforcement from the civil government to the individual
conscience. If Paul was not speaking about the Old Testament sab-
bath in that passage, then Exodus 31:15 and 35:2 are still morally
and legally binding, and Christians must forthrightly call for the
civil government to abide by God’s sabbatical standards, and to
begin executing sabbath breakers.
