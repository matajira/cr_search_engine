54 THE SINAI STRATEGY

power of Gad for distinctly personal ends. In other words, it is an at-
tempted misuse of power. Even Michael the archangel avoided such lan-
guage when he confronted Satan himself, for he dared not “bring
against him a railing accusation, but said, The Lord rebuke thee”
(Jude 9b).

This is not to say that all cursing is prohibited. The command-
ment reads, “thou shalt not take the name of the Lard thy God in
vain.” God’s name may be invoked for God-ordained ends. Cursing
within a God-ordained context is therefore legitimate. Indeed, it is
mandatory, Excommunication is such a curse. Jesus established cer-
tain procedural rules of church discipline in Matthew 18. “Verily I
say unto you, Whatsoever ye shall bind on earth shall be bound in
heaven: and whatsoever ye shall loose on earth shall be loosed in
heaven,” (v, 18), This clement of verbal “binding” is the very essence of
cursing, for it is more than verbal. It is real — eternally real. Thus,
the loose language which we call cursing is prohibited, precisely be-
cause biblical cursing is the language of binding. The power to curse
is the exclusive possession of the church, for the church is God’s
monopolistic human agency of eternal judgment, both for men and
angels (I Cor, 6:3).

Adam did not pronounce the preliminary judgmental curse
against Satan in the garden, but he was required by God to have
done so. Mankind does not escape the task of executing judgment
because of the Fall, Redeemed mankind—the church—becomes
both the earthly and heavenly agent of pronouncing judgment
against Satan, his angelic followers, and his human followers. God
executes final judgment, but redeemed mankind pronounces prelim-
inary judgment. God's “earnest” or down payment to His church of
this coming authority—an original authority which was given to
Adam in the garden—is the church’s present ability to bind and
loose on earth as it is in heaven and will be at the final judgment.

The church in effect issues a warning: “God damn you, unless you
repent.” It is an smperative—a calling upon God —since the phrase is
not in the active voice, “God damns you.” It is to serve as a warning
to the offender, not a final judgment. It calls upon the heavenly
Judge to honor the church’s word, which He promises to do. The
church in its carthly capacity cannot execute judgment against a
man’s soul, for the possibility of repentance always exists, as far as
the church can see. Instead, the church casts out the man’s body
from the presence of the church, and therefore from the sacramental

 
