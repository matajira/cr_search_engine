100 THE SINAI STRATEGY

agricultural production," state licensing of the professions,'* zoning
laws,45 and the most blatant and universally accepted restriction,
immigration quotas.© All of these statist economic restrictions
reduce people’s freedom of movement— geographically, economi-
cally, and socially. They all involve the misuse of the otherwise legiti-
mate monopoly of State power in order to restrict individual and
social progress and personal responsibility.'7 The result of such
legislation, if continued and enforced, is the universal destruction of
freedom, as the State regulators steadily squeeze away the monopoly
profits received by the early members of the protected group. This is
especially true of State-licensed professionals, such as physicians.'*

 

13, William Peterson, The Great Farm Problem (Chicago: Regnery, 1959); Clarence
B, Carson, The War on the Poor (New Rochelle, New York: Arlington House, 1969),
ch, 4: “Farmers at Bay.”

14. Reuben A. Kessel, “Price Discrimination in Medicine,” journal of Law and Eco-
nomics, 1 (October 1958); Milton Friedman, Capitalism and Freedom (Chicago: Univer-
sity of Chicago Press, 1962), ch. 9: “Occupational Licensure.”

15. Bernard H. Siegan, Lond Use Without Zoning (Lexington, Massachusetts: Lex-
ington, 1972),

16. Gary North, “Public Goods and Fear of Foreigners,” The Freeman (March
1974), An example of special pleading favoring immigration restrictions is Roy L
Garis, Immigration Restriction: A Study of the Opposition to and Regulation of Immigration
Into the United States (New York: Macmillan, 1928). Such restrictions, had they been
passed into law and enforced prior to 1924, would have greatly reduced American
economic growth. On the multiple cultural and economic contributions of several
immigrant groups— Germans, Irish, Ttalians, Jews, Blacks, Puerto Ricans, Mex-
icans, and orientals ~ see Thomas Sowell, Ethnic America: A History (New York: Basic
Books, 1981), He covers similar material in a condensed way in The Economics arid
Politics of Race: An International Perspective (New York: William Morrow, 1983), but
adds new material on the performance of immigrant groups in other societies. On
the spectacular economic miracle of the city uf Miami, Florida, as a result of heavy
immigration from Cuba after 1960, sce George Gilder, The Spirit of Enterprise (New
York: Simon & Schuster, 1984), ch. 5.

The political-economic problem today is twofold: 1) new immigrants in a de-
mocracy are soon allowed to vote, and 2) they become eligible for tax-financed “wel-
fare” programs. In the Old Testament, it took several generations for members of
pagan cultures to achieve citizenship (Deut. 23:3-8), and there were very few pub-
licly financed charities, the most notable being the third-year tithe (Dent. 14:28-29),
Thus, mass democracy has violated a fundamental biblical principle — thal time is
needed for ethical acculturation of pagan immigrants ~ and che result of this trans-
gression has been xenophobia: the fear of foreigners, especially immigrant newcom-
ens. Cf. Gary North, “I'wo-Tier Church Membership,” Christianity and Civilization, 4
(1985).

47. Walter Adams and Horace M, Gray, Monopoly in America: ‘The Government as
Promoter (New York: Macmillan, 1955); George Reisman, The Government Against the
Economy (Ottawa, Ulinois: Caroline House, 1979),

18. Gary North, “Walking Inco a Trap,” The Freeman (May 1978).

 

 
