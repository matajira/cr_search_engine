Sabbath and Dominion 93

A. This issue has been skirted for centuries, There has been no con-
sistent answer —sabbatarian, “continental sabbath,” or otherwise —
concerning the final locus of sovereignty for sabbath enforcement.
Until it is faced and dealt with in a manner sufficiently clear for the
writing and enforcement of sabbatarian statutes, in church or State,

 

the issue will remain muddled and an exegetical embarrassment for
Christians, [t will not be resolved successfully by the election of
Christian politicians. They need guidelines for sabbath legislation,
and these guidelines have yet to come forth from the 2,000-year-old
church.
