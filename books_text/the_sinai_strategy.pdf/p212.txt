186 THE SINAI STRATEGY

modities (Prov. 3:13-20).5

Slander and Theft

The prohibition against bearing false witness is theacentric. Men
are to give an honest account of God, God’s work, and God's plan for
history. The commandment requires men to adhere to the God-
interpreted facts of history. The existence of this theocentric com-
mandment against distorting the truth concerning God has created a
unique property right: the right to a name. A man is entitled to his good
name. Slander is therefore a form of theft. The civil government has an
obligation to defend the right of an individual to use a particular
name, both personal and corporate, both familial and institutional.
The civil government must also defend that name against false witnes-
ses. In doing so, the civil authoritics thereby reduce consumers’ search
costs, for the property right to a name, trademark, or other identify-
ing mark encourages men to build up their capital by establishing
good reputations for themselves. This helps to increase the sale of
quality products, or price-competitive products, and it also reduces
search costs for the consumers.. Buyers can make decisions more
effectively (less wastefully) because of the availability of brand names.

Another neglected aspect of brand names is that a brand name
makes possible scientific testing by independent research organiza-
tions. Brands establish an identifiable subclass of goods (a particular
product line) which can then be compared scientifically by means of
random selection from this and other competing products in that
same class. The performance of a randomly selected product from
an identifiable company can be compared with the performance of
other randomly selected products that are produced by competitors.
The results of these tests may be purchased by consumers. This
helps consumers ta make cost-effective decisions about which prod-
ucts to purchase.

If potential competitors were allowed to adopt identical identify-

5. By far the finest book on the economics of knowledge is Thomas Sowell,
Knowledge and Decisions (New York: Basic Books, 1980). Also useful is the specialized
study by the legal theorist, Henry Manne, Insider Trading and the Stock Market (New
York: Free Press, 1966). A bit narrow in focus, but important in dealing with the
question of knowledge and the stock market, is the symposium edited by Manne,
Economic Policy and the Regulation of Corporate Securities (Washington, D.C.: American
Enterprise Institute, 1969), especially the essay by Harold Demsetz, “Perfect Compe-
tition, Regulation, and the Stock Market.” Cf. Frederick G. Klein and John A.
Presto, News and the Markel (Chicago: Regnery, 1974).
