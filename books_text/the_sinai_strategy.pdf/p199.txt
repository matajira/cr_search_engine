The Rights of Private Property 173

The Wall Street Journal has provided a classic example of how the
State’s existing tax policies discourage investment. Say that a very
rich man wants to invest a million dollars. He takes the money and
invests in a new business—80% of which fail within the first five
years, Let us say that he thinks the business will make him 10% on
his money in the second year of operation, but nothing in the first
year (a reasonable presumption). Let us also say that his estimations
are rewarded. At the end of the second. year, he has back $100,000
profit for his small corporation, Here is what happens to his corpora-
tion in New York City. “Of the hundred thousand dollars in profit,
the city clears away roughly $5,700, leaving $94,300. The state
clears away about 10 percent of that, leaving $84,870. The IRS,
levying at progressive rates, snatches $38,000, leaving $46,870, Our
good rich person then pays himself a dividend. Being rich, our man
is of coursé in the highest personal income-tax brackets, and after
paying 4.3 percent to the city ($2,015) has $44,855 left. The state clips
him for 15 percent of that ($6,728) and leaves him $38,127. Uncle
Sam ‘nicks’ him for 70 percent of that, which is $26,689, leaving him
with $11,438. Thus, on the investment of | million dollars in capital
and two years of hard work in assembling the enterprise that is risky
to begin with, this lucky fellow who turned a profit of $100,000 has
$11,438 to spend. He has given up. two years on his yacht to gain
$5,719 in annual income.”5? Now, it may not have been quite this
bad.** But the point is clear, despite the slight exaggeration of the
Wall Street Journal essay: the higher the tax level, the less people are
going to invest in risky, future-oriented, employment-producing
capital assets.

Paul Craig Roberts has described a very real decision facing ‘a
rich man in the late 1970's: “Take the case of a person facing the 70
percent tax rate'on investment income. He can choose to invest
$50,000 at a 10 percent rate of return, which would bring him $5,000
per year of additional income before taxes. Or he can choose to
spend $50,000 on a Rolls Royce. Since the after-tax value of $5,000

57. Wall Street Journal editorial; cited in George Gilder, Wealth and Poverty, p. 174.

58, Federal tax laws in the United States during that period allowed deductions
from taxable income for taxes paid at the state and local level. Donald Regan, the
Secretary of the Treasury under President Reagan, announced immediately after
Reagan's successful bid for a second term of office (November 1984) that the
‘Treasury was proposing a new Federal tax rule which would deny the deductibility
of local taxes from Federally taxable income.
