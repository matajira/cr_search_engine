336 THE SINAI STRATEGY

Do I exaggerate? Am I unfair? Judge for yourself:

A detailed elaboration of this is not given in my essay. I did cite certain
results: i.c., that we, under the guidance of what we learn in Holy Scrip-
ture, must see and experience our earthly property rights as relative both in
regard to God as well as in regard to our fellow men, In other words, in our
‘unraffling’ we have to maintain a religious distance, or, as it is better phrased,
as not possessing our possessions (I Corinthians 7:29-31). However, one
cannot deduce from this religious basic attitude any concrete right of prop-
erty, as many ‘progressive theologians’ think they can do. This can be done
neither in civil property rights, nor in public government rights, nor yet in
rights of private enterprise. These concrete and temporal relations of justice
le on the niveau [?] of our temporal earthly life in which that which is con-
cretely just hic et-nunc and that which is love for neighbor in concrete is co-

 

determined by the normative social, cconomical-and other principles.
These principles are not ~as the natural law tradition thinks —given as posi-
tively formulated prescriptions but must be searched out from the complex
normative structures of the situation (p. 54).

Do you remember the story of the king who was led by his own
vanity to buy a set of “invisible clothes” by a bunch of “con artists”?
Then he went out in his new clothes to lead a parade. No adult in the
awe-struck crowd would admit that the king was stark naked.
Finally, a little boy asked his father why the king was wearing no
clothes. His father saw the light, and yelled, “The king has no
clothes!” The king’s vanity was given a decisive blow by the howls of
laughter that followed the innocent lad’s remark. Dooyeweerd, for
all his competence in exposing the myth of neutrality in humanists,
philosopher by phitosopher, was the self-deceived victim of his own
academic pride. He adopted-a non-biblical starting point—a refer-
ence point devoid of biblical content, which he called the “heart” ~
and he also adopted humanism’s hostility to biblical law. So have his
followers. Troost is a good example. I prefer to serve as the little boy
for the petrified crowd of Dutch Calvinists who stand in awe of the
Dooyeweerdian verbiage, and who seem incapable of saying out
loud: “These academic con men are naked!”

Conclusion

Troost feels inhibited by Mosaic law. So do all sinners. But in-
stead of repenting, and calling for the reconstruction of socicty in
terms of God's law, Troost rejects biblical law. It is not normative in
