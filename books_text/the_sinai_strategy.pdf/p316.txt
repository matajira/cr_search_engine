290 THE SINAI STRATEGY

Some of the employees must work on Sunday, but their labor makes
it easier for families to enjoy a meal together without putting bur-
dens on the wives. Most of the employees are given another day off.
They take a day of rest on a day other than Sunday, just as members
of the early church did.

Is a Business a Person?

Here are some fundamental questions. If the firm splits working
schedules for Sunday laborers, allowing them to attend morning or
evening worship services, has it profaned the Lord’s day blamefully
or blamelessly? If workers take another day off, has the firm forced
its employees to violate the Lord’s day? If so, then everyone who
spends money in that restaurant on Sunday is as guilty as the pro-
prietor and the employees. Second, is the firm to be treated as a per-
son? Must a firm remain closed one day each week, even when
employees are given alternate days of rest?

The problem exists, especially in an urban, industrial society,
because of the high division of labor and high specialization of pro-
duction. Companies serve the needs of large numbers of people. The
rhythm of rural, subsistence farming can be more easily geared to a
six-day workweek than the rhythm of an industrial society. The
workweek's rhythm in an industrial soctety ts necessarily flatter, since its
members are far more dependent upon the availability, moment by
moment, of services of other citizens than is the case in a Jow division
of labor rural society. Urban dwellers do not produce many goods
for their own personal use; they produce specialized services or
goods for sale. If we can legitimately buy natural gas or electricity
from a public utility in order to cook our Sunday meal, then why is it
illegitimate to buy a meal at a restaurant? Either both acts are
blameful violations of the Lord’s day, or neither, assuming that the
selling firm does not require seven days of consecutive labor from in-
dividual-employees.

Cooking on the Lord's Day

The Mosaic sabbath in the wilderness seems to have required the
baking of manna on the sixth day; they ate cold cakes on the sab-
bath, Will strict sabbatarians call for the death penalty of anyone
cooking on the Lord’s day? If not, why not?

The issue of cooking on the Lord’s day isa difficult one. ‘he
Hebrew women probably cooked their manna cakes on the sixth day
