Familistic Capital 103

conventional returns (prevailing interest rate) from: conventional
ventures. The man’s world becomes an endless series of all-or-
nothing decisions. ° He “puts it all on the line” time after time.

Carnegie and the Patron State

During the final decades of the nineteenth century in the United
States, a group of entrepreneurs collectively known in the history
textbooks as “the robber barons” created the modern industrial econ-
omy.” Never before had a nation experienced economic growth on a
scale as great as the United States experienced from 1870 to 1900.
Enormous fortunes were made and lost and made again. Output
quadrupled between 1867 and 1897, while the population doubled,
from 37 million to 72 million. The wholesale price index fell by a
hundred points, or 60%, from 168 to 68—and this was accomplished
in spite of the tripling of the money stock, from $1.3 billion to $4.5
billion.# The vast increase in per capita output meant an increase in
per capita wealth doubling in one generation, something that few
people living in previous periods of man’s history could have be-
lieved possible.

The secret of success for the entrepreneurs of the late nineteenth
century was éfficiency. They cut costs. They cut prices. They broadened
their markets by making goods available to millions of buyers who
could not have bought at the older prices. By increasing the size of
their firms, by adapting new techniques of cost accounting, and by
discovering new sources of power, raw materials, and communica-
tions, these men created a whole new world. The social costs were
high for some groups (e.g.,Chinese immigrant males in California

19. This is the world of modem entrepreneurship. Only a few people can make
huge fortes. Still, the rest of us benefit from their initiative and uncertainty-
bearing: Gilder, The Spirit of Enterprise.

20. Matthew Josephson, The Robber Barons: The Great American Copitalists,
1861-1901 (New York: Harcourt, Brace, [1934] 1962). For a raore balanced view of
this cra, see Edward C. Kirkland, “The Robber Barons Revisited,” American Histori-
cal Review, LEVI (October 1960). For a specific case study, sce Allan Nevins’ multi-
volume study of John D. Rockefeller.

21. There was, however, a drop in the so-called velocity of money — turnover, or
money transactions per unit of time—of 50%, This helped offset the price inflation
effects of the monetary expansion. The moncy and income data are from Milton
Friedman and Anna J. Schwartz, A Monetary History of the United States, 1867-1960
(National Bureau of Economic Research, published by Princeton University Press,
1963), charts 3 and 8, pp. 30, 94-95, The population figures are found in Historical
Statistics of the United States, Colonial Times to 1957 (Washington, D.C.; Bureau of the
Census, 1960), p. 7, Series A 13.
