Covenantal Law and Covenantal Love 309

to recognize. There is a deeply rooted tradition of interpreting Jesus’
words as if they stood in opposition to the law which God delivered
at Mount Sinai. This tradition is wrong. It is perhaps the most
dangerous heresy in twentieth-century Christianity. It flies in the
face of Jesus’ warning: “Think not that I am come to destroy the law,
or the prophets: I am.come not to destroy, but to fulfil: For verily I
say unto you, Till heaven and earth pass away, one jot or one tittle
shali'in no wise [way] pass from the law, till all be fulfilled.
Whosocver therefore shall break one of these least commandments,
and shall tcach men so, he shall be called the least in the kingdom of
heaven: but whoseever shall do and teach them, the same shall be
called great in the kingdom of heaven. For I say unto you, That ex-
cept your righteousness shall exceed the righteousness of the scribes
and Pharisees, ye shall in no case enter into the kingdom of heaven”
(Matt. 5:17-20). .

Who were the scribes? They were the Jewish lawyers. In Mark’s
account of the lawyer’s question to Jesus, it says that he was a scribe
(Mark 12:28). This scribe apparently had not heard Jesus’ original
statement at the “Sermon on the Mount,” or if he had, he had forgot-
ten about it. Jesus did not vary His views. Doing righteously to other
mien is the esserice of biblical law, for we do our righteous acts unto
God (Matt. 25:34-40). His first answer to the lawyer did help to
clarify the theocentric foundation of the law. commanding neighborly
love. But the Pharisees should have understood already that it was
not a particular Old Testament law which was the focus of His
ministry, but the underlying principle of all God's laws. This, in fact, was
what distinguished Jesus’ teaching from the common culture's first principles.
The common doctrine in Israel-was that men should love their
friends and hate their enemies,

Ye have heard that it hath been said, Thou shalt love thy neighbour, and
hate thine enemy. But I say unto you, Love your enemies, bless them that
curse you, do good te them that hate you, and pray for them which
despitefully use you, and persecute you. That ye may be the children of
your Father which is in heaven: for he maketh the sun to rise on the evil and
on the good, and sendeth rain on the just and on the unjust. For if ye love
them which love you, what reward have ye? Do not even the publicans [tax
collectors} do the same? And if ye salute your brethren only, what do ye
more than others? Do not even the publicans do so? Be ye therefore perfect,
even as your Father in heaven is perfect (Matt. 5:43-48).
