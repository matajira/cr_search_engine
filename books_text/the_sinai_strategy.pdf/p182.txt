156 THE SINAI STRATEGY

through such critical seasons as I have described. Through such seasons the
United States will have to pass in the course of the next century, if not of
this. How will you pass through them? I heartily wish you a good deliver-
ance. But my reason and my wishes are at war, and I can not help forcbod-
ing the worst.

It is quite plain that your Government will never be able to restrain a
distressed and discontented majority. For with you the majority is the Gov-
ernment, and has the rich, who are always a minority, always at ils mercy.
The day will come when in the State of New York a multitude of people,
none of whom has had more than half a breakfast, or expects to have more
than half a dinner, will choose a Legislature. Is it possible to doubt what
sort of Legislature will be chosen? On one side is a statesman preaching pa-
tience, respect for vested rights, strict observance of public faith. On the
other is a demagogue ranting about the tyranny of capitalists and usurers,
and asking why any body should be permitted to drink Champagne and to
ride in a carriage, while thousands of honest folks are in want of necessar-
ies. Which of the two candidates is likely to be preferred by a working-man
who hears his children cry for more bread?

I seriously apprehend that you will, in some such season of adversity as
T have described, do things which will prevent prosperity from returning;
that you will act like people who should in a year of scarcity devour all the
seed-corn, and thus make the next a year not of scarcity but of famine.
There will be, 1 fear, spoilation. The spoilation will increase the distress.
The distress will produce fresh spoilation.

Your Constitution is all sail and no anchor, As I said before, when a so-
ciety has entered on this downward progress, either civilization or liberty
must perish. Either some Caesar or Napoleon will seize the reins of govern-
ment with a strong hand, or your republic will be as fearfully plundered and
laid waste by barbarians in the twentieth century as the Roman Empire was
in the fifth; with this difference, that the Huns and Vandals who ravaged the
Roman Empire came from without, and that your Huns and Vandals will
have been engendered within your own country by your own institutions.

This is an eloquent statement. It is easy enough to pick apart
some of his specific arguments. For example, the territory of the
United States is still predominantly either rural or wilderness, with a
very thin population per square mile. The myth of “open spaces” as a
factor in reducing class warfare in the U.S. is just that, a myth” (and

21. G. Onto Trevelyan (ed.), The Life and Letters of Lord Macaulay (New York:
Harper & Brothers, 1875), H, pp. 408-10.

22, The American historian whose name is generally associated with this theory
is Frederick Jackson Turner, a highly influential teacher at the University of
Wisconsin and Harvard in the late nineteenth century, and a man who wrote almost
