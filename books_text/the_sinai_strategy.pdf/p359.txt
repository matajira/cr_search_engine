Social Antinomianism 333

Therefore. . . .

Dooyeweerd was upset by this “therefore.” Yet his response. shows
perfecily well how accurate Van Til’s criticism had been. He categor-
ically denied that any critique of humanism’s presuppositions should
begin with a confession of Christian presuppositions: “. . . this tran-
scendental critique is obliged to begin with an inquiry into the inner
nature and structure of the theoretical attitude of thought and exper-
ience as such and net with a confession of faith. In this first phase of
the critical investigation such a confession would have been out of
place” (p. 76). He begins with the autonomous mind of man. This is
why Dooyeweerd was a scholastic in his methodology, despite his at-
tempt to refute all medieval scholasticism by means of his critique.
He shared humanism’s methodological presuppositions concerning
the obligation of good, rational men to begin debating without any.
reference to the Bible and the God who wrote it. Dooyeweerd’s use of
a non-biblical concept of the “heart” was the very heart of his human-
ism and antinomianism.> Van Til’s response to Dooyeweerd’s essay.
returns to his original theme, namely, that Dooyeweerd had given
away the presuppositional case for biblical truth by his methodologi-
cal starting point,

Troost argues that he had written his dissertation against the an-
tinomianism of situation ethics. The question is: Did he simply sub-
stitute another brand of antinomianism? My answer was (and is),
“Yes, he did.” Either you affirm revealed biblical law as a permanent

5, It is not simply that Dooyewcerd’s exposition is incomparably verbose and
filled with jargon; it is that it is devoid of revelational content, including biblical law.
But Van Til was not concerned about Dooyeweerd’s implicit antinomianism; he was
concerned about the lack of biblical content for Dooyeweerd’s philosophical categor-
ies. Sadly, Van Til was himsclf almost as weak on the question of biblical law as
Dooyeweerd was. He was not a theonomist, which is why he was always unwilling
to promote publicly the writings of Rushdoony, and why he expressed reservations
in private concerising Rushdoony’s approach —and, by implication, the approach of
the whole Christian Reconstruction movement. Rushdoony was taking Van Til’s
presuppositionalism into areas that made Van Til nervous; Van Til carefully avoided
tapies outside of traditional apologetics: Christian Reconstruction did not exist ina
finished outline in 1971, when Jerusalem and Athens was published; not until Rush-
doony's Znstitutes of Biblical Law appeared. in 1973 did the capstone of the system ap-
pear. Van Til was always enthusiastic about Greg Bahnsen's apologetics, but he re-
mained judiciously silent about Bahnsen’s Theonomy in Christian Ethics (1977), Van
‘Til’s writings were necessary for the creation of the Reconstruction movement (pre-
suppositionalism), but not sufficient (biblical law). In this sense, the Reconstruc-
tionists have criticized Van Til in much the same way as Van Til criticized Dooye-
weerd: he did not go far enough in his adherence to biblical revelation,
