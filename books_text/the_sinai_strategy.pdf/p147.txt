God's Monopoly of Execution 121

derer might well decide to take the risk. There were eight thousand
murders in 1960, but only fifty-six executions; thus, a murderer’s
chances of being executed were only about one in one hundred forty.
After 1960 the number of executions dropped sharply, thus improv-
ing his chances,”*

Scholars debate endlessly about whether or not the death penalty
deters crime. Mafia members apparently have weighed the evidence
and have discovered that swt, predictable execution does indeed influ-
ence people’s behavior. Those who act as informers to the civil au-
thorities wind up dead. This has made it difficult for civil authorities
to find witnesses who will testify in court against criminal syndicates.
The use of the threat of execution by secret societies of many: varie-
ties indicates just how effective the death penalty is in modifying
people’s behavior. Criminal societies, unlike modern scholars, may
not have access to statistical data and complex explanations, but
their members think they have adopted an effective approach to the
“deviant behavior” problem, They may not have many footnotes,
but they are still nearly immune to successful prosecution by the civil
government. Capital punishment works well for them.

Humanism has steadily eroded the rule of God's law. The hu-
manists have, again and again, substituted alternative punishments
for those specifically required by the Bible. They have substituted
long-term imprisonment for. economic restitution to the victim by
the criminal. They have substituted life imprisonment for the death
penalty. They have substituted parole in three years for life impris-
onment. The results have been disastrous.?

Society wants social order, Without this order, too many scarce
economic resources must be assigned to crime prevention and safety
programs. What voters want is a system of prevention which main-
tains personal freedom for the innocent and -which does not
bankrupt civil government.

There is little doubt that the vast majority of crimes go unpun-
ished. Very few criminals are apprehended; few of these are brought
to trial; few of these are convicted; few of these serve complete sen-
tences, But eventually, most criminals are caught. When they are
“off the market,” they are not victimizing the innocent. How can so-
ciety reduce the number of very serious crimes, given the reality of

8. Ibid., p. 192.
9. Jessica Mitford, Kind and Unusual Punishment: The Prison Business (New York:
Knopf, 1973),
