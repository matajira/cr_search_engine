 

 

Index

saints, 35
saints days, 294
Salvation Army, 300-1
salvation by law, 15
Samuel, 202
sanctification, 216, 310, 311, 312
sanctions, 53
sanctuary, 218
Sandinistas, 319
Satan
army of, 45
church &, 54
covenant, 211
crushed, 123
defeat of, 3, 227
dominion, 45-46
humanism, 39
impotence, 89
judgment, 73-74
kingdom, 227, 314
kingdom of, 45-46
Michael vs., 54
society of, 213
stoning, 123
temptor, xxii-xxiii
Saturday, 300
Saul, 240
Schaeffer, Francis, 89
Schoeck, Helmut, xxn, 196, 203n
schools, 112
Schumacher, E. F. 219
Scotland, 261-62, 269, 275
screens, 95
scribes, 309
sculpture, 31
seal, 68
search costs, 183, 184, 186
secrecy, 203
self-discipline, 221
self-government, 14, 15, 67, 145, 154,
226, 313
self-maledictory oath, 55, 56-58, 66,
70, 71
self-worship, 49, 18
sellers, 184-85, 194
seminaries, 6
sentiment, 112

359

separation, 29, 127
sermon on mount, 308-12
serpent, 33
service, 127, 237, 239
servant, 129
servants, Hil, 295-96
Shafarevich, Igor, 319
Shay’s Rebillion, 48
shock, 62
shopping, 251, 259, 297
showbread, 240
shuffleboard, 271
Sider, Ronald, 216-17, 337
Sileven, Everett, 112n
silversmiths, 32
simple life style, 221-22
sin, 2, 311, 313
Sinai, 87, 309
sityation ethics, 5, 26
slander, 186-88
slave, 196-97
slavery, 4, 41, 129, 136, 202, 326
slaves, 76
sleep, 141
sloth, 203
Smith, Adam, 185
Smith, W. H., 298-99
smorgasbord religion, 253
snowball effect, 45
soccer, 286
society, 12
social co-operation, 315-16
social contract, 71
social gospel, 11, 217-18
social mobility, 198-99
social order, 55, 212, 316
social sabbatarianism, 298
socialism, 134-35, 146-47, 150-51, 201,
208-9, 218, 328
sovereignty
consumer, 164-65
covenants, 66-67
earthly, 159
God, 13 (see also God: sovereignty)
locus of, 72, 92
man’s, 49, 135, 202, 303
parental, 96-97
State, 13, 171
