Preface xvii

and Satan. But it was indeed judgment day. The sabbath was there-
fore a day of rendering judgment on the efforts of the previous six
days. Men halted their normal labors and rested, as God had rested
after His six-day efforts. In the New Testament, the church cele-
brates the Lord’s Supper on the Lord’s day, which is analogous to
(but not the same as) the Old Testament sabbath. Each church
member is to examine himself for the previous week’s transgressions,
making confession before God before taking communion (I Cor.
11:28-32). Paul’s words are forthright: “For if we would judge our-
selves, we should not be judged. But when we are judged, we are
chastened of the Lord, that we should not be condemned with the
world” (vv. 31-32). Those who judge themselves falsely can thereby
come under God’s earthly judgment, which is why Paul points to
sickness and even death within the Corinthian church (v. 30). In
short, the fourth commandment is judicial.

T also argue that the sabbath millennium is coming, and that this
will be an era of rest and judgment, when God’s people will take do-
minion by exercising godly judgment, thereby bringing Satan and
his host under preliminary condemnation, in preparation for God’s
return at the end of the millennium to render final judgment.

5. Legitimacy/inheritance (continuity)

Honoring father and mother is required because of the testamen-
tal nature of the covenant. Men grow old and need care; they also
transfer wealth and authority to successors. To this fifth command-
ment a promise is attached: long life in the land which God gives to
us. This commandment seems to be man-oriented, and also a
uniquely positive law, in contrast to the priestly negatives of the first
four.* Nevertheless, if we see this law as essentially priestly in scope,
then it places the family under the overall protection of the church,
or in Old Testament times, under the protection of the priests. The
priesthood, not the civil government, is the protector of the primary
agency of welfare, the family, and therefore the church is the second-
ary agency of welfare, should the family prove incapable of pro-
viding for its own.

This is why Jesus cited the fifth commandment when He criti-
cized the Pharisees for giving alms in public but not taking care of
their parents (Mark 7:10-13). They were being unfaithful to their

4. The’sabbath law was essentially negative: no work.
