The Economic Implications of the Sabbath 299

tian founder of the giant book store chain. Smith invoked the “acts of
necessity and mercy” exemption when he decided to publish the
names of the dead and wounded during the Crimean War in 1855.
On the other hand, he refrained from walking outdoors on a visit to
Canada, to avoid giving the impression of being a sighiseer..On the
other hand, he complainéd when his Sunday evening bath was late,
blaming the assertive attitudes of Canadian hotel workers (p. 78).
Convenience, predictably, triumphed over theology.

What about the Sunday operation of profit-seeking public utili-
ties? This is not a hypothetical example. In the 1840’s, some private
water companies had left parts of London without water on Sundays
(p. 82).

In the 1870's, numerous secular organizations formed iccture. so-
cieties. Libraries began to remain open on Sundays. So did free art
galleries (p. 131). The sabbatarians opposed all such violations. -In
1884, Herbert Spencer, the evolutionist and defender of pure daissez-
faire, remarked that a dispute over the opening of a reading room on
Sunday could split a mechanics’ institute’ (p. 1).

By the late 1880's, sabbatarians had generally lost public support.
In 1896, the government finally voted to allow the opening of the
British museums and national galleries on Sundays (p. 147). There
was no opposition from the churches or the denominational news-
papers. The twentieth century, especially after World War I, saw the
end of most relics of the 1677 and 1780 laws.

Wigley’s summary of the problem is remarkably similar to my
own discussion of the economic questions raised by the sabbath in an
industrial civilization, especially with respect to the differing rhythms
of the workweek. “Sabbatarianism was an inappropriate way to pro-
vide rest, for it applied'a simple, essentially pre-industrial, religious
prescription to a complex, essentially urban, social problem. Sabba-
tarians avoided the difficulties which a complete cessation of labour
would have produced for themselves by requiring servants to work
and applying the formula ‘acts of necessity and mercy,’ but failed to
appreciate that society at large similarly needed the work of some
railwaymen, shopkeepers and the like, whose work rhythm ran
counter to that of the rest of the community, Sabbatarianism thus
jastified some Sunday work, but regarded the unjustified as sin for
condemnation, rather than as‘a problem suitable for social reform”

(p. 79).
