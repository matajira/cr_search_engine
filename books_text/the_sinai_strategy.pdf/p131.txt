Familistic Capital 105

largest stee] producer in the world, and was in a position to bankrupt
most of his competitors. He was producing steel for $12 a ton, and
hiding behind the tariff which allowed him to sell at $23.75 a ton. ®
Carnegie, the “rugged individualist,” was a high tariff man (as every
steel producer in recorded U.S. history has been), and agitated
strongly for the McKinley tariff in the early 1890’s.% Of course, his
U.S. competitors also hid behind that import wall, but it did them
no good with Carnegie’s ability to underprice them. J. P. Morgan
took the firm, merged it with others, and the result was the U.S.
Steel Company. Morgan stopped Carnegie’s competition by buying
him out. Immediately, the new trust announced a price increase to
$28 a ton,

What did Carnegie do with the money? Over the next 19 years,
he gave it all away.) This is what scts him apart from all the other
“robber barons” of his day. He had achieved his goal. He had
become fabulously wealthy in an era of falling prices and no income
taxes, when the average family earned less than $1,000 a year. But
he did not pass this wealth along to his heirs, for he leit little money
behind. He married late, at age 53, and had one daughter. His vi-
sion was limited to what he could accomplish with his capital within
his own lifetime. He knew that only an elite could do much in one
brief lifetime.

Carnegie’s “Essay on Wealth”

Carnegie set forth his opinions concerning wealth and the
responsibilities of those who possess it in a memorable essay, pub-
lished in 1889. Predictably, he did not see wealth as the product of
adherence to a biblical law structure, given his rejection of all super-
natural religion, He was a religious evolutionist. He once described
his conversion to Darwinism: “Light came in as a flood and all was
clear. Not only had I got rid of the theology and the supernatural,

28. fbid., p. 427.

29, Mark D, Hirsch, William C, Whitney: Modern Wanwick (New York: Archon,
[1948] 1969), p. 410.

30. Cramer, p, 428

31. He gave millions away to create libraries throughout the Linited States. I did
some of my research on this chapter in a Camegie library. Interestingly enough, the
copy of Carnegie’s autobiography, listed in the card catalogue, was missing from the
shelves, probably permanently. He also set up several tax-exempt foundations, The
total gifts may have reached $300,000,000, given the interest returns on the original
principal, which he also gave away.
