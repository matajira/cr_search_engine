Introduction 15

Conclusion

We see the “privatized” nature of the biblical social order in the
eighth commandment: “Thou shalt not steal.” But we also find the
foundational principles of a free market economy in all the other
nine commandments. The ten commandments are as fine a state-
ment of the principles of liberty, including liberty of voluntary ex-
change, as we can find anywhere in the history of man. The Old
Testament is an anti-statist document. It limits the civil government
in the interests of personal self-government. Limited civil government
is one of the two political preconditions of a free market economy.
The other political precondition is predictable law which places limits
on civil government, which the ten commandments and the case
Jaws also provide.

The Bible does not teach a doctrine of salvation by law. In both
the Old Testament and the New Testament, the doctrine is clear:
“The just shall live by faith” (Hab. 2:4). The Bible teaches dominion
under God, but it does not teach salvation by law. In contrast, all
other religions teach either salvation by law or salvation by mystical
escape, with the techniques of asceticism and mysticism serving as the
“laws” that save man.?4 Humanism teaches salvation by law, and
most forms of humanism in the twentieth century have been statist,
for the State is clearly the highest and most concentrated form of
power. Salvation by the State, or by an agency of the State,” is the
common faith of twentieth-century humanists. This is why the Bible
is repugnant to twentieth-century humanist man.

In the ten chapters that follow, you will learn more about the
relationship between the ten commandments and economics. You
will also learn more concerning the relation between the ten com-
mandments and the dominion covenant.?6 The ten commandments
certainly have implications outside of the realm of economics, but
they surely have implications at least for economics. When men see
how relevant the ten commandments are for economics, they should

24. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985), Introduction.

25. R. J. Rushdvony, The Messianic Character of American Education (Phillipsburg,
New Jersey: Presbyterian & Reformed, [1963]).

26, Gary North, The Dominion Covenant: Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1982).
