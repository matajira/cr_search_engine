Conclusion 213

Faith in the productiveness of rest

Faith in the covenantal family (family name)

Optimism ‘concerning the future (linear history)

The possibility of compound economic growth

Defense of the private ownership of both the means of
production and the fruits of production

The sanctity of covenants and the analogous and derivative
legitimacy of contracts

Social ‘co-operation through private contracts

Contentedness ‘as a way of life

The illegitiinacy of covetousness and envy

The legitimacy of civil government as a monopolistic agent
of law eriforcement, but not wealth redistribution

Penalties against violence and verbal assault

Penalties against slander and theft

Salvation by grace, not law (or legislation)

We compare these premises with the underlying premises of
backward societies; and we find almost a perfect reverse image. The
society of Satan also has first principles. A list of the major “tenets of
backwardness” is provided by: P. T. Bauer, a specialist in develop-
mental economics, and a devout Roman Catholic: “Examples of sig-
nificant attitudes, beliefs and modes of conduct unfavourable to ma-
terial progress include lack of interest in material advance, com-
bined with resignation in the face of poverty; lack of initiative, self-
reliance and a sense of personal responsibility for the economic fu-
ture of oneself and one’s family; high leisure preference, together
with a lassitude often found in tropical climates; relatively high pres-
tige of passive or.contemplative life compared to active life; the pres-
tige of mysticism and of renunciation of the world compared to ac-
quisition and achievement; acceptance of a preordained, unchang-
ing and unchangeable universe; emphasis on performance of duties
and acceptance of obligations, rather than on achievement of results,
or assertion or even a recognition of personal rights; lack of. sus-
tained curiosity, experimentation and interest in change; belief in
the efficacy of supernatural and occult forces and of their influence
over one’s destiny; insistence on the unity of the organic universe,
and on the need to live with nature rather than conquer it or harness
it to man’s needs, an attitude of which reluctance to take animal lifé
is a corollary; belief in personal reincarnation, which reduces the sig-
