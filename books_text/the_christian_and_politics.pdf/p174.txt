 

162 THE CHRISTIAN AND POLITICS

Another is that they are more interested in politics.
Neither is true. They tend to vote the way their
parents do, They are less likely to vote than their
parents.

As far as work is concerned, students in the ele-
mentary grades will do much more than high school
students. High school students have many interests
and activities. You will find the elementary students
eager to work.

Young people are very good at stuffing, sealing,
and stamping envelopes. They also like to work at
the polls. Morton Blackwell, a conservative, is an
expert in the area of youth campaigns. He ran Presi~
dent Reagan’s youth campaign. He was recently a
special assistant to the President at the White
House. He holds whole seminars just on youth cam-
paigning.

BUILDING COALITIONS Liberals are fond of de-
crying one-issue groups. The fact is that many peo-
ple have a special issue that is important to them.
It is wise to put together coalitions of these
groups. Some coalitions are: taxpayers, Stop ERA,
prolife, pro-gun, right-to-work, and pro-defense.
‘There are other groups, Some citizens may be op-
posed to a public housing project that threatens their
neighborhood. They may be concerned about the
crime problem, transportation, or other matters.
Christians can work with many other groups to
elect candidates. For example, we worked with Mor-
mons who were especially concerned about the -
ERA. Many Roman Catholics are anti-abortion. By
