THE NEBRASKA SCHOOL WAR 185

the tax-supported classroom.

What we have here isn’t a failure to communi-
cate. What we have here is a war between religious
groups for the control of tax money, and even more im-
portant, for control over the minds of those who will be
asked by the State in the future to provide even more tax
money. Ashes to ashes, debt to debt: forever.

It is a war. First, it is a war concerning sover-
eignty over children: parents vs. the elite managers
of the State. Second, it is a war for tax money.
Third, it is a battle for the mind. Fourth, it is a war
over the content of education. Ultimately, it is a bat-
tle over the source of authority over truth: “Thus
saith the Lord!” In short, just who is the Lord? And
whose responsibility is it to decide and enforce this
Lord’s word in the classroom?

The National Education Association knows the
stakes. Sadly, very few parents do. Not even Chris-
tian parents. Not even Christian-parents who send
their children to Christian schools— schools that seek
accreditation, certification, and licensure from the
representatives of a very different “lord.” So it turns
out that these who have begun to resist are obscure
pastors in obscure churches that educate only small
groups of students. But, then again, revolutions are
not generally begun by 51% of the voters. And this
revolution is every bit as much of a revolution as the
revolution begun by God at Sinai, Jesus in the
Roman Empire, Luther at Wittenberg, and Sam
Adams in. Boston. It is the same sort of revolution
spoken of by John Adams, in a Jetter to Jefferson
(1815):
