194 THE CHRISTIAN AND POLITICS

by one outside pastors are brought in, one at a time,
to take over, The North Platt jail will fill up with
ministers, And then the state may put licns against
the church’s property, and against Pastor
Gelsthorpe’s property. He has vowed that he will not
budge. The school will stay open, no matter what his
sheriff does. His church is the North Platt Baptist
Church, P.O. Box 641, North Platt, NB 69101. The
church is goimg to need money for a lawyer, as you
can imagine.

Hard core? You bet. What we need is a massive
hard core. I'd call it the Hard Corps. Nebraska has
bitten off more bad publicity than it will believe pos-
sible. Meanwhile, a banking scandal is about to rock
the state.

But Nebraska in Number One in college foot-
ball. And in America, that’s the most important
religion of all. Bread and circuses have always had
worshippers.

But it gets better (or worse)! Rev. Don Brush of
Ainsworth will be in court on February 6. His coun-
ty prosecutor has advised the judge that two types of
defense must not be allowed: 1) freedom of religion
under the First Amendment; and 2) the academic
performance of his school, as measured by inde-
pendent professional testing. The judge has yet to
rule on her request.

And what did the Supreme Court say? In 198i,
the Sileven case went to Federal appeals court and
was thrown out. It’s a state educational matter, not a
religious matter. And the Supreme Court declined to
hear the case. No big Constitutional issues were in-
