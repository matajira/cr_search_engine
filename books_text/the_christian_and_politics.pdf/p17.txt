RELIGION AND POLITICS DONT MIX! 5

ment over his slip of the tongue.

Using God’s Name in vain was a no-no, the
violation of a tradition rooted in the oldest legislative
body in the Western Hemisphere. Committee
meetings and private conversation were another
matter. The words of these elected officials betrayed
what was really in their hearts, Nor did God’s Word
tnake any difference when it carne to legislation. The
religion of secular humanism was evetywhere evi-
dent. A legislator would-argue passionately for the
appropriation of tax money te kill innocent babies
created in the image of God while being careful not
to use God’s Name in vain.

Politics has to do with civil government. Politi-
cians get elected, make laws, tax, spend, regulate,
and control. Every law passed and every spending
decision is based on some moral system. All morality
is based on a religion. So who says politics and
religion don’t mix!

The Bible says, “Thou shalt not steal.” That is
the basis of laws against theft. The Bible says, “Thou
shalt not bear false witness against thy neighbor.”
That is the basis of laws against libel and slander.
Make no mistake about it. What we believe religi-
ously will affect our political beliefs and practices.

An excellent example of this can be seen in the
development of the Constitution of the United
States. The United States has continued under the
Constitution longer than any other country in the
world today. Despite reinterpretation and misinter-
pretation, the Constitution is still our governing doc-
ument because it was so well devised to begin with.
