152 THE CHRISTIAN AND POLITICS

Conservatives arc more successful with direct
mail because conservatives are issue-oriented. Such
people give more generously. I found this to be true
in Northern Virginia politics where Larry Prau,
John Buckley, and I were successful in raising funds.

People give out of current income. I took my first
fund-raising course at a seminar next to the Potomac
River. One of the speakers said the river is like cur-
rent income. The water is flowing by. If you do not
get your bucket out and dip it up, it will be gone for-
ever, This helps to explain why fundraisers come
back again and again. They want to get some of
your current income. If they miss it, it will be gone
forever.

I also learned that the best source of contribu-
tions is from those who have already given. This is
contrary to. what we might expect. If I had not
learned this, I would probably say that we should
not go after the person who already gave. Let’s solicit
those who have given nothing. The fact is that those
who have given before are most likely to give again.
T have confirmed this by my own experience. There
is a rule in life called the 80/20 rule. Eighty percent
of the sales will be genérated by 20 percent of the
salesmen. Eighty percent of the problems in school
will be caused by 20 percent of the students. I think
this is true with contributions. Eighty percent of the
money will come from 20 percent of the con-
tributors.

Those who have already given are most likely to
give again. Don’t go to the well too often though.
Appeals should be properly spaced.
