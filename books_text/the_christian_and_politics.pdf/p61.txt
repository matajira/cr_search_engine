THE ROLE OF CIVIL GOVERNMENT 49

They are called “public servants” though someone
has suggested that the last thing they want to do is to
serve the public. All too often those in office either as
elected officials or as part of the Civil Service take
advantage of their positions to lord it over those
whom they are supposed Lo serve, But then we can
find ministers who lord it over the flock instead of
serving.

The civil ruler is called a minister of God. As
such. his responsibility is to God. He may or may not
be elected by man. That does not affect the fact that
he is to be God’s minister and to govern in a way that
is pleasing to the Lord. As a minister of God, the
civil ruler must answer to God just as a minister in
the church must answer to God. A church minister is
not to please his congregation at the expense of
God’s Word. A government minister is not to take a
poll of his constituents to find out what positions are
popular in order to stay in office, He is to do what
God has ordained him to do.

So much has been written about the separation
of church and state in this country that most persons
think Christianity should. have no relationship to
civil government. The church and state have two
different roles. The family has yet a different role.
The church is not to replace the family nor is the
family to be a substitute for the church. Each is a
separate institution in-the sight of God. Yet we do
not speak of the separation of church and family. We
realize both are related to each other. Both are under
God and have direct responsibilities to God.

Likewise the church and the state are separate
