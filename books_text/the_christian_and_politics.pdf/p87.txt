THE BREAD AND BUTTER ISSUES 75.

Modern inflating is done mostly through the
Federal Reserve System, The money supply. is
steadily increased. This money is called “fiat” money.
“Fiat” is the Latin word which means “let there be.”
Just as God created by saying “Let there be light,” so
the money creators say, “Let there be money.” In
other words, they are playing god.

Man can not create wealth by fiat. Increasing the
money supply results in a corresponding decrease in
the value of all money. ‘his is why the dollar has
steadily lost its value. Inflation is a form of silent
burglar. It steals from its victims. Its victims are
those on fixed incomes. Its victims, as often as not,
are the poor and elderly.

The Bible requires honest weights and measures.
The Constitution of the United States is a hard
money document. Congress was given the power to
coin money, not to print it. John Witherspoon, a
Presbyterian clergyman who became president of
Princeton and who was one of the signers of The
Declaration of Independence, wrote an Essay on
Money which called for a sound Biblically based
money. Witherspoon taught many of the early
leaders of cur country.

The -reader who wants to learn more about
Christian economics is urged to read the various
books of Dr. Gary North. North is writing a
monumental economic commentary on the Bible.
The first volume is based on Genesis and is called
The Dominion Covenant.

Taxes are an important aspéct of economics.
That will be dealt with in a separate chapter.
