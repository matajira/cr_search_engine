WELFARE 89

money!

The effect of government welfare is to establish a
class of persons who stay on it permanently. In an-
cicnt Rome the right to welfare was made
hereditary. If your parents were on welfare, you
could stay on it for life. The welfare state is destruc-
tive of human character.

THE BIBLICAL ANSWER The Bible provides the
way out of the welfare problem. The answer is the
tithe. An excellent treatment of the tithe is found in
R. J. Rushdoony’s' The Institutes of Biblical Law.

It is not our purpose here to rehash what others
have already covered thoroughly elsewhere. Most
Christians have heard about tithing. The major
problem is getting Christians to tithe.

Until Christians take uthing seriously, we can
not expect to rid ourselves of the curse of the welfare
state. Christians have gone along with the humanis-
tic idea that the civil government could do it better,
or at least cheaper. The heavy burden of taxes proves
otherwise.

Rushdoony identifies a poor tithe as well as the
Lord’s tithe in the Bible. The poor tithe was used for
those in need. The Lord’s tithe which went to the
Levites was used for educational as well as other
social purposes. The Levites tithed one-tenth to the
priests for the furtherance of worship.

It is encouraging to see Christians today again
assuming responsibility in a variety of fields. There
are prison ministries, Christian schools, and welfare
programs designed to help those in need.
