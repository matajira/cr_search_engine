11

LAND USE LAWS

If you own a parcel of land arid are not permitted
to use it for certain purposes, it may not be of much
value to you. Land use laws are being. used to
restrict citizens in various ways. They are being used
to prevent churches from building.and to curtail the
Christian school movement.

The theology behind zoning and planning is
statist. Man wants to play God. God has a plan for
the universe. He foreordains whatsoever comes to
pass. He owns everything. God has a purpose for
everything He does.

Through obedience to God’s commandments
man can have dominion over the earth, Ged put
man in paradise, He forfeited his right to be there
because of sin. Paradise will be restored only
through Jesus Christ.

Statist man wants to legislate paradise. He
belicves in salvation through politics. Education in
government schools is an important part of that
legislation.

Environmentalism is a basic tenet of the human-
