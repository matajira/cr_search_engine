174 THE CHRISTIAN ANO POLITICS

are very important. Most persons who get elected
to Congress serve in lower offices first. Some
start out in the local civic association. Three of
the men I served with not long ago in the Vir-
ginia General Assembly are now in Congress.

You might run for the school board, town coun-
cil, county supervisor, or a state office. Or you might
be a “kingmaker’ and find someone else to run. You
might serve as campaign manager or fill some other
important post in the campaign.

CONCLUSION God’s Word tells us that we reap
what we sow. We are experiencing difficulties today
because we have left the field of politics to others.

We need to put our hand to the plow in that part
of God’s kingdom. God is King and Lord over all.
Every knee shall bow to Jesus Christ.

We are to be steadfast and unmoveable in the
work of the Lord for we know that our labor is not in
vain in the Lord.

God has promised victory.

To Him be the glory forever.
