780 THE CHRISTIAN AND POLITICS

the county, and had been brought in specifically to
try this case. Their crime? None; the county court
refuses to chargé them with a crime. A crime, you
see, is protected by such Constitutional provisions as
the Fifth Amendment. A crime has a fixed sentence
attached to it. These men are in jail for what could
literally be Lfe imprisonment, without possibility of parole.
Their offense? Putting their children in a
non-State-certified Christian school.

Their wives have fled the state of Nebraska.
They are living across the border in Iowa. The
pastor of their church, and formerly the headmaster
of the school, is Rev. Everett Sileven. He, too, is a
fugitive in Iowa. The Nebraska police desperately
tried to catch the women and their twelve children,
If the civil authorities had caught them, their chil-
dren almost certainly would have been taken from
them and put into foster homes. This increases
parents” incentive to comply with rules and regula-
tions.

Immediately, the: word went out. A team of
pastors from several states flew inio the tiny town of
Louisville to take charge. All of a sudden. the little
Christian school in the Faith Baptist church had
three co-headmasters. They began to call other
pastors. Soon the school had dozens of co-
headmasters; then over 150, They told the sheriff to
arrest them, Guess what? They’re still headmasters,
And the flow of pastors into the town is still coming.
Hundreds and hundreds of ministers, mostly Bap-
tists and mostly from small churches, are coming to
Louisville to see the battle first-hand. My own
