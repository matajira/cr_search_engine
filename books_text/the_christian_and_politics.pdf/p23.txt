FELIGION AND POLITICS DONT MX! 31

(cheaper for them, not us).

Before I get off the subject of liberals, let me say
something about the word “liberal.” It comes from
the Latin -word that means freedom, The classic lib-
eral was one who believed in freedom. The theologi-
ca] and political liberal believes in the opposite. He
does not wish to leave man free to develop his calling
under God. He wants to enslave man by means of
big government, heavy taxes, and more regulation.
The liberal religious leaders set up the National and
World Councils of Churches because they wanted
unity at the expense of truth. They wanted a one-
world church and they have been at the fore in work-
ing for a one-world government. This is the Tower of
Babel revisited.

One more thing I want to point out about “liber-
als” is that they are not generous. At least they are
not generous with their own money. They like to be
liberal with other people’s money. I was attending a
hearing beforc the Fairfax County Board of Super-
visors several years ago when the subject of public
housing for the poor was on the agenda. I witnessed
a long parade of liberal ministers begging the county
supervisors to appropriate money for the poor.
These clergy were so concerned about the poor.
They quoted verses from the Bible. When I got up to
speak I reminded them of another person who pre-
tended to be so concerned about the poor. His name
was Judas Iscariot. I also shared my experience
while living in one of those paradises on earth, a gov-
ernment housing project. I suggested that if they
were so concerned about the poor, they ought to give
