LAND USE LAWS 103

have, how.much money you make, how many chil-
dren you have, and a long list of additional informa-
tion. The government wants to be omniscient, Sta-
tistics is the Achilles hee! of government. Even with
the computer there is no way they can keep up. They
will never be omniscient.

Through planning the statist wants to create a
beautiful environment in which man may live. It
docs not work. Again and again government plan-
ners have made stupid errors. It is not because the
planners lack ability or integrity. It is just that they
are trying to do the impossible.

I have had considerable experience with plan-
ning and zoning. I think I could write a whole book
just on that subject. I was the general contractor for
three school buildings that we constructed during
the 1960's.

We had to get a use permit to use the land for a
school. I purchased the property several miles from
any concentration of people so that no one would ob-
ject. I bought a large tract and steadily added more
land (currently 34 acres). The idea of use permits is
to protect other people from us. I wanted to protect
myself from the neighbors.

I looked at 30 or 40 sites before selecting the one
1 thought would be best. Everyone who visits the
campus comments on what a nice location it is. 1
wanted a buffer between the school and our
neighbors so I told the Beard of Zoning Appeals that
T planned a 50 foot buffer.

The Board had never set foot on the property.
They sat in an office three miles away and made
