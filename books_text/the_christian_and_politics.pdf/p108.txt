96 THE CHRISTIAN AND POLITICS

They had hyperinflation. If we continue as we are,
there is no reason to think we will not suffer the same
fate.

Private retirement programs are available that
will provide the benefits of Social Security at half the
cost. They are contractual arrangements.

Christians need to influence the political process
in order to rid ourselves of the Social Security tax. It
is putting an enormous burden on our children. The
Bible teaches that parents should lay up for their
children. It does not permit parents spending their
children’s money and leaving a legacy of debt as we
are doing.

Meanwhile we will not stand by while the elderly
starve as the humanists will contend. We must
assume our responsibilities in the manner prescribed
in Scripture.

PROPERTY TAXES There were no property taxes
in colonial times. The Colonists resisted a tax on tea.
They rejected taxation without representation.
(They ought to see taxes with representation.) They
knew King George must be stopped or he would be
taxing their land next. At the beginning of our coun-
try there were-no real estate taxes, no income taxes,
and no Social Security taxes, not to mention others.
The country made tremendous economic progress
without these taxes. They got along very well, thank
you. The property or real estate tax strikes at family
security. Property is the foundation of liberty and is
essential to econémic: progress and security. Many
families have worked hard all their lives to buy a
