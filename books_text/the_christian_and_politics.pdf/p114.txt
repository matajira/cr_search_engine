102 THE CHRISTIAN AND POLITICS

ist religion. Man is an animal to be conditioned by
his environment. Statist man yearns for a paradise
on earth. By passing laws he plans to créate this
paradise. As George Bernard Shaw, the English so-
cialist, said, “Under socialism you may not live at
all, but if you do, you will be forced to live well.”

When men reject the idea that God plans the
world, they can turn only to man as an alternative.
Thus we live in the age of the planner. Especially at
the local level we have massive codes dealing with
planning, zoning, use permits, and the like.

The concept of planning is not lacking at the
state and Federal levels. When I served on the Con-
servation and Natural Resources Committee in the
Virginia House of Delegates, I learned about some
of the plans. They wanted a satellite to pass over the
state every few hours in order to know what was
going on. A bill to regulate sand dunes would have
affected land 50-100 miles from the shore. I enjoyed
the opportunity to join with conservative legislators
to kill that bill.

God knows all things. The planner wants to be
omniscient, too. Governments are constantly collec-
ting data for this reason. As a school director I am
frequently asked for information. Some of the statis-
tics they want are not even available to me. The Fed-
eral census is taken every 10 years as provided for in
the Constitution. The original purpose. was to enu-
merate the population to determine representation
in Congress.

The census now goes far beyond that. The gov-
ernment wants to know how many bathrooms you
