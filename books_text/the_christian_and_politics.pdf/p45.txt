“ITS A MATTER OF SURVIVAL” 33

Ministers and parents find it very difficult to

counteract the humanism the children are getting in
the government schools. In the early days of Fairfax
Christian School, a family complained to me be-
cause their son was learning economics from a Bibli-
cal perspective. “You are putting ideas into his head
faster than we can get them out,” they informed me.
As parents they were not required-to send their son
to study under me. The problem with the govern-
ment schools is that parents are forced to send their
children to schools that they do not agree with. The
curriculum is forced upon the children.
“Tris true that parents may send their children to
a private school. This is based on several assump-
tions, however. The local authorities must permit
the school. The right not to send a child to a govern-
ment school had to be established. in court many
years ago. The other assumptions are that the pri-
vate school is not so controlled by the state that it is
nothing more than a public school supported with
private funds, and, secondly that the parents can
afford a Christian or private school. They have to
keep paying for the government school whether or
not they choose to use it.

This brings us to another point of attack on the
family by the humanists. Marx knew that he must
put an economic squeeze on the family to destroy it.
He advocated central banking so that the value of
money could be manipulated by the state. The Fed-
eral Reserve System established in the early part of
this century has been the engine of inflation in this
country. Inflation has eroded savings and weakened
