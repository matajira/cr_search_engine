‘RUNNING A CAMPAIGN 151

to run successfully than a well-known incumbent.

One source of funds is the political action com-
mitice, There are thousands of these PACS as they
are called. They operate at national, state, and to
some extent at local levels. Some of them not only
raise and distribute funds to candidates with whom
they agree, but also provide campaign training. The
campaign schools they conduct may be worth much
more than the funds they give.

The telephone is useful in fundraising. It enables
your campaign to talk personally to: potential con-
tributors. It is especially effective as a follow-up to
direct mail.

Direct mail fundraising is the most effective way
that I have found. Perhaps you find yourself on
many mailing lists and receive numerous requests
for donations. That indicates the effectiveness of this
kind of fundraising.

There is an art to direct mail fundraising.
Among conservatives, Richard Viguerie is known as
the most successful. Richard told me that he spends
several hours every day learning more about it.

I shall pass on ta you some of the things I have
learned. Conservatives have more success than
liberals in raising money. Liberals, as I have men-
tioned before, like to spend other people’s money.
They are not “liberal” with their own. I am told,
however, that the most successful fundraising letter
was written by none other than George McGovern.
It was a long one at that (about 22 pages if 1 recall
correctly). I am told that long letters, if well written,
will raise more money than short letters.
