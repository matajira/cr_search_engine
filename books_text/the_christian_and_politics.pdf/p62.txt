50 THE CHRISTIAN AND POLITICS

institutions. Both are under God. In the Pledge of
Allegiance to the Flag we acknowledge that we are
one nation under God. God is sovereign over the
nations. The government is on the shoulders of
Jesus, the Messiah (Isaiah 9:5-6), God raises up na-
tions and God brings nations down. The king’s heart
is in the hands of the Lord. The rulers of the carth
may take counscl against the Lord and against His
Annointed One, but all in vain. Ged will only laugh
at them. He shall break them with a rod of iron
(Psalm 2), Only.God is sovereign. For decades the de-
bate has been carried on in our country—which is
sovereign, the Federal government or the states? The
word “sovercignty” was never attributed to govern-
ments, local or otherwise, by our founding fathers.
They realized that only God ts sovereign. Our sover-
eign God has ordained men on earth to be civil rulers.
The state is: under. God, not God under the state.

A MINISTRY OF JUSTICE What then is the differ-
ence between the role of the church and that of civil
government? The church is to be a ministry of grace.
The function of the church is to preach the gospel, to
evangelize, to teach God’s Word in all its power and
fullness. The church is the body of Christ. It gathers
to worship Him. As a ministry of grace, the church
deals with the sin of man by declaring the wrath of
God poured out against sinners. The church sets
forth the sacrificial death of Christ on the cross as the
only answer for man’s sin and calls sinners to repent
of their sins and accept Jesus Christ as Saviour and
Lord,
