4) THE CHRISTIAN AND POLITICS

highest part of the city. That was the most easily de-
fended part of the ancient city. The Athenians
wanted to protect the temple because their political
system was based on their religion.

I observed this when I served a term in the
Virginia House of Delegates. The laws we made
were a reflection of our religious views. As a Chris-
tian I consciously tried to influence the legislation in
terms of my Christian faith. The issues ranged from
abortion and the ERA to budget matters.

The religious views of the lawmakers ran the
gamut from Bible-believing Christianity to secular
humanism. And secular humanism is a religion.
Every session of the legislature was opened with
prayer. Often the visiting minister was the pastor of
one of the legislators. It wasn’t difficult to tell the
conservative ministers from the liberal ones. They
prayed differently.

I noticed that the liberal legislators attended the
liberal churches and the conservative legislators at-
tended conservative churches. This will not always
be the case because people are inconsistent. I’ve been
around ‘politicians enough to know that their
religious views and that of their supporters influence
their political views.

I observed another interesting thing about the
legislators. Not only did they have a minister in to
pray before each session, but they were also very
strict about taking God’s Name in vain while speak-
ing on the floor. One day a prominent leader of the
majority party uttered a profane ‘word. He quickly
corrected himself and showed obvious embarrass-
