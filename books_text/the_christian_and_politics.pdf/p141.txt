MY EXPERIENCE IN POLITICg «129

States. Cooke was 22 when his father died at the age
of 98. The Confederate Congress had met in the
building where we held our sessions.

My tenure as an elected official was short. [
always voted in terms of what I belicved was right,
never just to assure my re-election. I was the only
member of the 140 member legislature to receive a
100% mark for my conservative voting record during
the two years J was there.

I was like the new kid in school. They gave me a
seat right in the corner next to Mr. Speaker. I liked
that seat. You couldn’t see as well. It wasn’t close to
“power alley” where the committee chairmen sat.
But it was next to the pages’ bench and those young
people were always right there to run errands.

The nicest thing about that location was that the
visiting ministers would be seated right in-front of
me after they gave the opening prayer. The Speaker
always invited them to. stay awhile and would give
them a gift. These visiting ministers didn’t know me
from Adam. I was one of two ministers in the House
of Delegates. I had my desk full of R. J. Rushdoony’s
Law and Liberty, and would give a copy to the visiting
minister, I liked doing a little missionary work on the
side.

After a few weeks one of the liberal legislators
asked for one of the books I was giving out. Then the
press got interested.

LEGISLATIVE ACCOMPLISHMENTS When they
held an orientation day for the freshmen legislators,
we were told that a good legislator didn’t necessarily
