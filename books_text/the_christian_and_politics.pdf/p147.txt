MY EXPERIENCE INPOLITICS «135

care centers from licensure. The bill was carried
over from the first year I was in the Assembly.to the
second year,

We went into session at noon the first day. The
church licensure bill was to be voted on in committee
the very next morning. If it got PBI’d (Passed By
Indefinitely) there would be no likelihood of passage.
‘We had to work fast. We counted noses. There were
20 on the committee. We could count on about five
or six votes for sure.

We did everything we could. And we prayed.
The next morning two of the delegates took a walk.
The vote was 9-9. The opponents couldn’t PBI it.
The proponents couldn't get it to the floor, [t was
sent to a subcommittec.

We worked really hard on that bill. The Virginia
Council of Churches was lobbying on the other side.
Concern was expressed about fire and safety in the
day care centers. We agreed the churches should
abide by local codes. The matier of staff
qualifications and child-staff ratios became an im-
portant issue.

We proposed that staff qualifications, the pro-
gram, child-staff ratios, and other such matters
should be disclosed to the parents, but not to the
state. The hard core liberals didn’t like this, but we
were persistent. We got our.bill. The day-care
centers did not need to be licensed. Parents were to
be given the information they had every right to ex-
pect anyway. State bureaucrats were kept out of the
church,
