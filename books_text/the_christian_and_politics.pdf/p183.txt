ORGANIZING CHRISTIANS ° 171

In addition to this, voting records should be stud-
ied. It is always a good idea to talk with other Chris-
tians who are active in politics to learn more about the
candidates. Politicians can be very difficult to pin
down on issues, They try to please everybody. Some
of them will tell you what they think you want to hear.

GETTING REGISTERED It is appalling that many
Christians are not registered to vote. It is simple to
get registered. Registration laws will vary among the
states. In Virginia, for example, one does not
register with a political party. A registered voter can
vote in either the Republican or Democrat primary
regardless of what his party preference is. You need
to find out what the law is in your state and be sure
your people are informed.

In Virginia a person can register and vote in a
spring primary if he is going to be 18 years of age or
older by the time of the general election. [ don’t
know the laws of other states. If that is the case in
your state, take advantage of it. See that the 17 year
olds who are eligible get registered and vote.

Figure out the most effective way to get your peo-
ple registered. Christians leaders are working on a
plan to register 10,000,000 new voters in 1984. If you
can gei the registrar to come to your church
building, then do it. If you have to set up a registra-
tion party and lead the people by the hand to
register, then do it.

MAXIMIZE YOUR INFLUENCE =A businessman likes
to maximize his profits, An investor wants to max-
