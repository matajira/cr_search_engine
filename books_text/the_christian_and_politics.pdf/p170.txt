158 THE CHRISTIAN AND POLITICS:

registered’ to vote, especially after the books have
closed for registration. They can’t vote for you even
if they want to. I'm not saying you should ignore the
unregistered voters or those who seldom vote. I am
saying to concéntrate on those who do vote. Hunt
where the ducks are!

In order to find out who has voted before, you
get a list from the Electoral Board. In our state you
have to be a candidate in order to get such a list. In
our-state, the Republican Party will sell- lists and
labels. The lists will tell you how old the person is.
They will tell you when they voted, telephone
mumbers, and other helpful information,

When I ran for Congress, we had the largest
turnout in the history of the Republican Party in a
Congressional primary in Virginia. ‘There were five
candidates. The turnout? About nine percent of the
registered voters. All one needed was a plurality of
the votes to win the nomination,

Very few people vote in primaries. In our area
you can win nomination by getting just one in 20
registered voters to support you. A typical precinct
in our area will have about 2000 registered voters, If
just 50 families (at an average vote of two per
household) could: be located in each precinct among
the registered voters, one can get nominated.

CAMPAIGN LITERATURE Campaign literature is
important. Many people want to read something
about a candidate before they will vote for him.
They are more likely to believe something that is in
print than if it is mercly spoken. The campaign
