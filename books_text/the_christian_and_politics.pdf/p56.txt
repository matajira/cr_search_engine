44 THE CHRISTIAN AND POLITICS.

His commandments. In keeping God’s command-
ments we are occupying and exercising dominion
over the earth. Why not victory?

General Douglas MacArthur stated that in war
there is no substitute for victory. The Christian is to
put on the whole armor of God. That armor did not
include any provision for the back. The Christian is
not to retreat but to go forward. Jesus said He would
build: His church (Matthew 16) and the gates of hell
would not prevail against it. The Greek word for
“prevail” means that the church is on the offensive,
not the defensive. The church will go forward vic-
toriously and the gates of hell will not be able to
withstand that offensive.

For all too long Christians have had an attitude
of defeat. A conspiracy of evil men does not control
the world. Ged is in complete control and has as-
sured us of victory. It is time to put the other side on
the defensive. We need to frame the issues in posi-
tive, Biblical terms, not just react to what the secular
humanists are doing.

In the area of. politics victory is both desirable
and necessary. Humanism has no valid answers to
man’s problems. Humanism is part of the problem.
The problems of the economy, crime, war, the envi-
ronment, education, drug-addiction, and others
have only worsened under the stewardship of the hu-
manists. Christians have the answers in the applica-
tion of God’s Word. In the succeeding chapters of
this book, I want to set forth a Christian approach to
politics. In the final chapters I shall show how the
Christian can get involved. Jesus said He would
build His church (Matthew 16) and the gates of hell
would not prevail against it.
