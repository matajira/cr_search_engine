92 THE CHRISTIAN AND POLITICS

As a result the people would ery out. God would
not hear them. They would have to accept the
burden they had brought on themselves.

THE USA TODAY The United States is in a similar
situation today. When our country was established,
the culture was fundamentally Christian. The Gon-
stitution provided for strictly limited government.
There were no government schools or welfare state
schemes. Taxes were low. Since that time we have
departed from our Christian heritage. We have turtied
away from Biblical law and adopted man’s law in its
place: With this has come enormous tax increases.
We are crying out under the burden. God will not
hear us until we return to His law.

We shall analyze some of the major taxes. We
shall show how they are unbiblical and suggest
Christian alternatives.

THE INCOME TAX The income tax did not exist un-
til the early part of the Twentieth Century. It was
necessary to amend the Constitution in order to have
this tax.

The Federal income tax is a graduated tax. This
means that as the taxable income increases, the rate
of taxation increases. Another name for this kind of
tax is the progréssive income tax.

The tithe is a proportional or flat rate tax. No
matter how little or how much'a person’s income
was, God required only a tenth. For the king to de-
mand a tenth in Samuel’s time was to put the state
on a par with God. That was tyranny.
