 

 

732 THE CHRISTIAN AND POLITICS

reason. One of my sons was running at the same
time for the Virginia State Senate. He was only 22 at
the time and had an excellent chance to win. He had
defeated the former Secretary of the Commonwealth
(and a prominent pro-ERA supporter) in the
primary in what was the biggest upset in the state.
Now he was running against the Senate Majority
Leader, a prominent Fairfax County attorney. John
got 45% of the vote. I was more interested in his
campaign than my own.

Although I was defeated, the conservatives in
our area made tremendous progress. In the race for
the five at-large seats I came in sixth and lost, but
just ahead of me was Larry Pratt. He won my seat.
Larry had been my campaign manager when | ran
for Congress. I was his minister and had baptized
him. I recruited him to run for the House of
Delegates and gave him his first campaign contribu-
tion.

Larry was as opposed to the ERA as I was. In
addition to Pratt, we got two other men elected from
Northern Virginia who were anti-ERA. Now there
were three. The libbers put great pressure on one of
them and he defected. Only Pratt and John Buckley
were left. The libbers went after Pratt and Buckley
as they had gone after me. Only more so. They ran
vicious radio ads. Thousands of dollars, much of it
from out of state, were poured into the campaign.
They defeated Pratt and Buckley. In the process four
other delegates got elected from Northern Virginia
who opposed the ERA! We were making progress.
