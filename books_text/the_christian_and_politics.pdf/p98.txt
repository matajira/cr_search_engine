86 THE CHRISTIAN AND POLITICS

voluntary associations. Millions of immigrants came
to our shores. They obtained jobs, learned a new
language, and became self-sustaining without the
aid of government.

Civil governments did not get involved, even on
the local level, until fairly recently. The Christians
and churches began to abdicate their responsibility.
Politicians, sensing another opportunity to buy votes
with the people’s own money,.jumped at the oppor-
tunity to “do something” for the poor. The welfare
state was on its way.

Then the Federal government got into the act.
With the vast resources of a graduated income tax
and printing press money, why not? The welfare
state got rolling in the 1930’s, but this was nothing to
compare with what followed. Today the budget for
welfare is the third largest budget in the entire
world, outranked only by the total budget of the
United States and that of the Soviet Union.

The old Department of Health, Education, and
Welfare (HEW) did not come into existence until the
Eisenhower Administration. A separate cabinet level
department now looks after education, Welfare in-
cludes a vast array of programs: aid to dependent
children, medicare, social security, and the job
corps, to name a few. Despite the complaint that the
present administration in Washington is stingy, the
welfare programs keep expanding.

EFFECTS OF THE WELFARE STATE ‘The cost of
the welfare state is threatening the economic stability
of the country. The deficit in the Federal budget is
