6

CRIME AND PUNISHMENT

“Whoso sheddeth man’s blood, by man shall his blood
be shed: for in the image of God made he man.”
Genesis 9:6

This is generally known as the law and order
issue in politics. Politicians like to talk about how
they are tough on criminals. Crime is a serious prob-
Jem in America. Year after year the statistics paint a
grim picture. Some drop in crime has been ex-
perienced in the past year or so. J think this is due to
several factors,

Citizens are beginning to realize that they should
not depend on the government to do everything for
them. They are turning away from institutional help
and relying more on themselves. In the crime area
they are setting up Neighborhood Watch programs
and these are helping to reduce crime. There is-a
gradual return to restitution and capital punish-
ment. This reflects the influence in our society of
those who understand where our penal system went
astray in the first place. Hopefully this trend will
