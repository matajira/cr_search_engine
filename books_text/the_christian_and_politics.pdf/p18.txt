6 THE CHRISTIAN AND POLITICS

Biblical Christianity was the undergirding faith
of our nation when the Constitution was adopted.
‘That religious faith manifested itself in this remarka-
ble document. The founding fathers wanted to bind
the Federal government by the chains of the Consti-
tution. They knew man is a sinner and when a lot of
sinners get together in government they can accom-
plish much mischief. The Constitution limited the
power of government in many ways. The Federal
government was given only those powers delegated
to it by the States. Within the Federal government
power was divided among three branches — legislative,
executive, and judicial.

The Constitution contains a system of checks
and balances. The President can veto acts of Con-
gress, but Congress can override the veto. The Pres-
ident appoints judges and other officials but only
with the’ advice and consent of the Senate. The
House of Representatives can impeach a President,
but only the Senate can convict. The Supreme Court
was designed to check the power of both. the ex-
ecutive and legislative branches by interpreting the
laws on the basis of the Constitution. The Constitu-
tion itself could be amended only by the approval of
the legislatures of three-fourths of the States.

The genius of the Constitution was to provide for
decentralization of political: power. This was to
guard against a powerful central government that
could become tyrannical. For the Christian the fam-
ily is the central governing institution. This is decen-
tralized government because there are thousands or
millions of families in a nation.
