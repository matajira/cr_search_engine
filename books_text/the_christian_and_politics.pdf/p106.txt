94 THE CHAISTIAN AND POLITICS:

The loopholes in the tax code, the varying inter-
pretations, and the complexities of tax law have
resulted in a growing disrespect for law in our coun-
try. Someone has said the income tax has made more
liars out of people than anything since the game of
golf was invented!

The income tax goes contrary to Biblical law on
many counts. It is not surprising that Karl Marx ad-
vocated a graduated income tax as part of his plan to
bring about Communism.

Christians need to work toward abolition of this
tax. The lowering of the top rates is a small but
significant step in the right direction. The indexing
of the tax will provide some relief from inflation
which has been pushing people into higher brackets
even though they really aren’t making any more
moncy.

Humanists will express shock that anyone would
touch this sacred cow. After all, where would they
get money to run the government? Conservatives
may argue that we wouldn't have enough for an ade-
quate national defense. My answer would be that we
got along fine without this tax until the Twentieth
Century. We never lost a war. And that’s more than
we Can say now with the enormous taxes we're pay-

ing.

SOCIAL SECURITY TAXES § I have nothing good to
say about Social Security taxes. This tax was in-
stituted during the 1930’s. It was set up by the Fed-
eral Insurance Contributions Act (FICA):

It is not insurance. The Supreme Court says so.
