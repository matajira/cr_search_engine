FOREIGN Poticy 1117

President Franklin Roosevelt extended diplo-
matic recognition to the Sovict Union. The Soviet
Union is controlled by the Communist Party. The
Communist Party consists of a small percentage of
the Soviet citizens. It is the only political party’ per-
mitted. The Sovict Union is.a totalitarian dictator-
ship that understands only one thing —force.

Communism is parasitic: Left to itself, it would
collapse. Approximately half the people in the Soviet
Union are engaged in agriculture. They work on col-
lective farms owned by the government. They are
unable to feed themselves and must import food.
The United States selis them grain, often at cut rate
prices.

The Soviet Union is a military encampment.
They spend an exhorbitant portion of their gross na-
tional income on the military. We sell them our
superior technology. They steal the rest.

The Soviet Union represses its citizens. It con-
ducts espionage around the globe, promotes guer-
ria warfare to undermine other governments, and
invades other countries when that suits their pur-
poses. One country after another has been taken
over by the Communists. They captured Eastern
Europe. They took over North Korea and China.
They have moved into Africa. Cuba is Communist.
They have moved into South and Central America.

STOPPING COMMUNISM Communism can not be
stopped by appeasement. The British appeased
Adolph Hitler. Hitler was stopped eventually, but
only at great cost in lives and property.
