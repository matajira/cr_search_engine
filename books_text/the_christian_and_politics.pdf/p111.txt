Taxes 99

day’s humanists like.) Marx opposed private prop-
erty. He hated Christianity and wanted to destroy it.
He knew this involved undermining the family.

The inheritance tax is an attack upon the family.
The family is to have dominion over the earth. This
requires capital. Marx was not opposed to capitalism.
He wanted the capital to be in the harids of the state.
God wants capital in the hands of families. Parents
were to leave the inheritance to their children.

Today’s society is present-oriented because it is
under the influence of a philosophy called existen-
tialism. Only the present is important to the existen-
tialist. The Bible teaches that we should be future-
oriented. We are.to save for the future and provide
an inheritance for our children.

The inheritance tax, like the income tax, attacks
the concept of wealth. Humanists want the govern-
ment to be big and wealthy. They want people to be
poor. The inheritance tax has been responsible for
wiping out many a family farm or business. The tax
rates are graduated. If a person dies, the tax has to
be paid in a few months. Properties often have to be
sold when the market is down.

Efforts to avoid the inheritance tax by giving
away wealth are often in vain. There is a gift tax,
too! The government is very creative when it comes
to taxes. Maybe they can’t fix potholes, but they are
fairly good with loopholes. At present one can give
away $10,000 per year to another person without
paying a gift tax.

Inflation, the hidden tax fostered by govern-
ment, has pushed up the dollar value of estates. To

 
