54 THE CHRISTIAN AND POLITICS

era stated that either men will be governed by the
Bible or by tyrants. We need to restore the Biblical
foundations. Before we can teach them to others we
must learn them ourselves.

TOTALITARIANISM AND ANARCHY In our era we
have seen the rise of totalitarian states. Dictators
rule in many countries of the world. In the United
States we have moved toward centralized control of
the people in Washington. One-world government
advocates push for the United Nations as a solution
to world problems.

On the other hand, there are those who decry big
government and advocate little or no government at
all. The statists want a powerful central government
with total control. The anarchist says we should
have no government at all. For the statist the group
is.all that is important. For the anarchist only the in-
dividual counts and he should be allowed to do
anything he wishes “as long as he does not harm
anyone else.”

Both of these ideas are at work in our society.
Both are wrong. Man does not live in isolation. The
individual is important, but so is the group. The in-
dividual is important, but so is the family as a unit.
The citizens of a state are not more important than
the state nor is the state more important than its
citizens. The Christian. believes in the Trinity. God
is one God. Yet God exists in three Persons, God the
Father, God the Son, and God the Holy Spirit. All
three Persons in the Godhead are equally God. In
the Trinity we find there is ultimate unity and there
