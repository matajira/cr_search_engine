THE BREAD AND BUTTER ISSUES 73

medical care, free parks, and free lunches from gov-
ernment. Everything has a price. Even our salvation
cost something. It is free to.us, but Jesus paid the
price.

A good place to begin is with “free” education.
The public (government) schools are supposed to be
free. There is nothing free about them, They are very
expensive. Add up the capital costs of land and build-
ings, the operating costs, the social costs, and a host
of hidden costs (such as tax-exempt interest on the
bonds for buildings) and you will have a tidy sum.

Politicians always like to talk about the benefits
they obtain for their constituents. They rarely if ever
talk about the cost: Everything is always free, free,
free. The Christian in politics must not bear false
witness against his neighbor. TANSTAAFL!

INFLATION What is inflation? The average person
would say the answer to that is easy. Inflation is ris-
ing prices. Not so. Inflation is an increase in the sup-
ply of purchasing medium. We generally refer to
that purchasing medium as money.

Who inflates? If one says that inflation is rising
prices, then the answer would probably be “the big
bad businessman.” Or the laborer who asks for
higher wages. If inflation is properly seen as an in-
crease in the money supply, then the answer will be
different. The government controls the money supply
so it is responsible for inflation,

Governments like us to believe that they are
fighting inflation, Governments that want to expand
and control the people benefit from inflation, So why
