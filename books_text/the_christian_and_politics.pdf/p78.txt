 

6G THE CHRISTIAN AND POLITICS.

tally killed someone could flee there for protection.

The law of God provided severe penalties for ly-
ing at trials. Perjury was punishable by death in a
trial on a capital crime. At least two witnesses were
needed to convict someone of a crime.

GOVERNMENT CAUSED CRIME The humanist
state has helped to breed crime. Respect for the law
has lessened because the law is often arbitrary. The
position is taken that “ignorance of the law is no ex-
cuse.” With tens of thousands of laws, many of them
contradictory, it is impossible to know what the law
is. The laws are changed at will by legislators and
subject to constant reinterpretation by the courts.

Criminals are released on technicalities. Some
lawyers are more concerned with earning a fee than
furthering justice.

Another factor in the growth of crime is the
secularization of the schools. Biblical morality has
been removed from the teaching of the children. A
law in Kentucky providing for the posting of the Ten
Commandments in the public schools (at private ex-
pense) was declared unconstitutional by the
Supreme Court. When government schools were
established it was actually argued that they would be
cheaper because they would virtually eliminate
crime!

Biblical solutions have worked in the past and
they will work again in the future.
