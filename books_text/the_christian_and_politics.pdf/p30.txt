18 THE CHRISTIAN AND POLITICS

Why not have two fine candidates running against
each other? Then let the liberals take their pick. This
is possible if Christians will fulfill their responsibility
under God. In a later chapter I plan to show how
this can be done.

Why have so many Bible-believing Christians
left the field to the humanists? To find the answer we
need to go back to ancient history. The Persians had
a dualistic notion of the universe. They believed that
light was opposed to darkness, good was opposed to
evil, and spirit was opposed to matter. In their think-
ing light, good, and spirit were associated on the one
hand, and darkness, evil, and matter were associ-
ated on the other. In other words they equated mate-
rial things with evil.

Persian dualism was adopted by the Greeks. The
Greeks were conquered by the Romans, but many of
their ideas influenced their conquerors. The Chris-
tian church expanded throughout the Roman Em-
pire. Persian dualism was brought into the church,
The idea that material things were evil and spiritual
things were good began to affect the life of the
church: Men began to withdraw from the world to
live in monasteries because the world was material,
They wished to become more saintly or spiritual.
One man spent 30 years on a pillar and was known
as a “pillar saint.”

The clergy did not marry because this involved a
physical relationship and that was considered to be
on a lower level of sanctification.

The idea that material things should be equated
with evil has continued to influence the church in
