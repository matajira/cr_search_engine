6 VICTIM'S RIGHTS

The social question of questions in man’s history is this one,
although it is seldom asked formally: “Heaven or hell on earth?” It
has to be one or the other in every society, progressively revealed
over time.!! There is no neutral third position.!? Tt is this crucial
question which political pluralists devote their lives either to avoid-
ing or denying."

 

Harsh for Whom?

I have written this book in order to challenge the arguments
of the critics of Old Lestament law. My goal here is simple to
describe though not so simple to execute: to demonstrate that the
negative penaltics of the Old Testament case laws were not harsh
but just, not a threat to society but rather the necessary judicial
foundation of civic freedom, I do not attempt to prove here that
most of the Old Testament’s civil laws are still binding today.
That argument has been made elsewhere,'* and will continue to
be refined as God’s Church continues to move toward the final
judgment. Instead, I examine a specific group of Old Testament
case laws that in one way or another upheld the interests of
victims of criminal acts. It is my argument throughout this book
that the Old Testament was harsh on criminals because it was “soft on
victims.”

God’s Old Testament laws, both ecclesiastical and civil, pro-
tected the victims’ interests. These laws were harsh on criminals

11. Amilennialists teach that Satan’s kingdom will be progressively dominant in
history. In this sense, they are “pre-mils without earthly hope.” Premillennialists
deny the progressive character of this development. ‘They see Christian progress as
discontinuous; prior to the Second Coming and Jesus’ establishment of an earthly
millennium, covenant-breaking men are said to be progressively dominant in society.
After Jesus returns, Christians are said to be dominant, But there is n0 neutrality
with respect to historical development; one kingdom is steadily ovcrooming the efforts
of the other throughout history, ail sides agree,

12. Tam writing a book on this-subject, Heann or [fell on Earth: ‘The Sociology of
Final Judgment.

13. Gary North, Political Polytheism: The Myth of Pluralism (Lyler, Texas: Insticute
for Christian Koonemics, 1989).

14, Greg L, Bahnscn, Theonomy in Christian Ethics (2nd ed.; Phillipsburg, New
Jersey: Presbyterian & Reformed, 1984), Bahnsen, By This Standard: The Authority of
God's Law Today (Tyler, Texas: Institute for Christian Economics, 1985).

 
