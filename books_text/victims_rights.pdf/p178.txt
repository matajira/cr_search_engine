166 VICTIM'S RIGHTS
allow the guilty man to live.

Conclusion

This law of criminal negligence is much broader than simply
oxen and owners; it applies to all cases of death to innocent parties
that are the result of negligence on the part of owners of notorious
beasts or notorious machinery — capital that is known to be risky
to innocent bystanders. Automobiles, trucks, certain kinds of oc-
cupations, nuclear power plants, coal mines, and similar examples
of dangerous tools are covered by this general principle of per-
sonal liability. This law should not be understood as applying to
workers who voluntarily work in dangerous callings and who have
been warned in advance of the risks by their employers, nor
should it be used as a justification for the creation of a messianic
State that attempts to discover criminal negligoncc im cvery casc
of third-party injury, despite the lack of knowledge of risks by the
owners or experts in the field.

Personal liability insurance is a development of the West that
allows criminally negligent people a greater opportunity to escape
the death penalty by means of high payments to the heirs of their
victims. Purchasing such insurance is not to become mandatory,
except in cases related to the use of State-financed capital (e.g.,
highways). Nevertheless, the risk is so high — execution — and the
cost of premiums so low in comparison to the risk, that personal
liability coverage is available to most people. Only the very poor,
who would not normally own “oxen” (expensive capital equip-
ment}, or people convicted repeatedly of criminal negligence or
actions that would lead to convictions for criminal negligence
(e.g., drunk driving), or people who manage or own businesses
that create high risks for innocent bystanders, would normally be
excluded from the purchase of such insurance coverage. They
would have to learn to handle their “oxen” with care.
