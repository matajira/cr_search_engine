258 VICTIM’S RIGHTS

to the civil government for so-called “victimless crimes.” For ex-
ample, a person is prohibited from driving a car at 70 miles an
hour through a residential district or school zone, There are po-
tential victims who deserve legal’ protection. The speeding driver
is subjecting them to added risk of injury or death. Clearly, it is
more dangerous statistically for children to attend a school located
near an unfenced street on which drivers are travelling at 70 miles
an hour rather than 25, The imposition of a fine helps to reduce
the number of speeding drivers. Because they increase risks to
families, drivers who exceed the speed limit can legitimately be
fined, since the victims of this increased statistical risk cannot be
specified. These fines should be imposed locally: to be used to
indemnify future local victims of unsolved hit and run incidents.

The State is not to use fines to increase its operating budget
or increase its control over the lives of innocent citizens. The State
is to be supported by tax levies, so that no conflict of interest
should occur between honest judgment and the desire to increase
the State’s budget. The proper usc of fines is the establishment of
a restitution fund for victims of crimes whose perpetrators cannot be located
or convicted, analogous to the Old Testament sacrifice of the heifer
when a murderer could not be found (Deut. 21:1-9). Such a fund
is a valid use of the civil law. Even if law enforcement authorities
are unable to locate and convict a criminal, the victim still de-
serves restitution, just as God deserved restitution for an unsolved
murder in Israel in the form of a sacrificed heifer. A reasonable
way of funding such a restitution program is to collect money
from those who have been successfully convicted by law enforce-
ment authorities.

Hayek’s Three Principles

Lex talionis binds the State. This so-called “primitive” principle
keeps the State from becoming arbitrary in its imposition of penal-
ties. Citizens can better predict in advance what the penalty will be for a
specific crime. This is extremely important for maintaining a free
society. The three legal foundations for a free society, Hayek
argucs, are known general rules, certainty of enforcement, and
equality before the Jaw. I argue that the principle of “eye for eye”
