Guardian of the Oath 239

In the New ‘lestament, this priestly sacrifice was made by
Jesus Christ at Calvary. The various animal sacrifices in the Old
Testament representationally prefigured this ultimate sacrifice (Heb.
9). A question legitimately can be raised: Is any post-Calvary
public mark of contrition lawfully imposed by the church on the
perjurer? If so, on what basis?

If the perjurer is a church member, he has partaken of the
Lord’s Supper throughout the period following his false testimony
to the court. This placed him in jeopardy of God’s negative sanc-
tions (I Cor. 11:30). He ignored this threat, thereby implicitly
adopting the same false theology of God’s minimal sanctions,
previously described. The church’s officers deserve to know of the
transgression, and can lawfully assign a penalty. This penalty
should not exceed the value of a ram in the Old Testament
cconomy.

If the perjurer is not a church member, he is still dependent
on the continuing faithfulness of the church to preserve God’s
common grace in history. The. State can lawfully function in
non-Christian environments, but only becausc of the common
grace of God mediated through His church and its sacraments.
Offering these representative sacrifices in the Old Covenant was
the permanent responsibility of God’s church. ‘his is why Israel
had to offer 70 bullocks annually as sacrifices for the symbolic 70
pagan nations of the world (Num, 29:12-32), plus a single bullock
for herself on the eighth day (Num. 29:36).°

What this means is that the church is the guardian of the covenantal
oath. This is an inescapable conclusion from the fact that only the
church has the authority to accept the perjurer’s sacrifice in atone-
ment for the false oath. The State cannot offer this release from
guilt. The oath involves the formal calling down of God’s negative
covenant sanctions on the oath-taker. He who uses God’s name

 

offered for consideration by the legislative representatives of the people, has remained
immune to American Civil Liberties Union protests; so has the office of prison
chaplain, where the law’s negative sanctions are imposed.

9. When Israel fell in A.D. 70, she had become like all the other pagan nations.
She could no longer offer efficacious sacrifices for them or for herself, Fram that point
on, only the church’s offering of bread and wine could scrve as a representative
covering for the world.
