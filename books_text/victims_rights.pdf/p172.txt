160 VICTIM'S RIGHTS

Murder has to be punished by the death penalty (Num. 35:31).
In this case, the ox is executed, so the general principle of “life for
life” is maintained, Genesis 9:4-6 is not violated by Exodus 21:28-
30. The owner, because he is not directly guilty of committing a
capital crime, although fully responsible before the law for the
actions of his beast, can escape execution. It is not stated that the
judges make this decision: death or restitution. The victim’s fam-
ily probably makes this decision. Perhaps both judges and family
do.’ Restitution is owed to the relatives, as heirs of his estate;
legally, the payment is made to the dead victim. The economic
incentive of the, family is clear; money is more useful than the
death of the victim.

The restitution payment normally would be high. A man has
to pay. There is no escape. If he cannot pay what is demanded,
either through liability insurance, personal capital, or selling him-
self into slavery, then he dies. Restitution is mandatory.

The development of. personal liability insurance is one way
that Western society has dealt with the problem of the cata-
strophic judgment. The question then arises: Should criminal negli-
gence be covered? The civil government musi face the questions
raised by economic analysis. If the criminal is criminally negli-
gent, yet covered by liability insurance, can the insurance firm
be forced by law to pay, even if its contract with the convicted
person says that it must? Is a third-party payment to the victim
in the name of the criminal an immoral contract and therefore
illegal? Does it reduce the economic threat of personal bankruptcy
to such an cxtcnt that criminal negligence is thereby subsidized?
Is criminal negligence a legitirate event to insure against? Should
such contracts be made illegal — not just unenforceable in a court
of law, but illegal?

There is another problem. If the “deeper pocket” of the insur-
ance company is available for the victim’s family to reach into,
will they demand “all the traffic will bear,” irrespective of justicc?
If the owner were not insured, would the victim’s family ever
demand such a high restitution payment? In the absence of insur-
ance, the victim’s heirs would probably have to limit their de-

7. §. J. Finkelstein, The Ox That Gored (Philadelphia: American Philosophical
Society, 1981), p. 29
