II
PROTECTING THE VICTIMS

If a man shall steal an ox, or a sheep, and kill it, or sell it; he
shail restore five oxen for an ox, and four sheep for a sheep... . If
the theft be certainly found in his hand alive, whether it be ox, or ass,
or sheep; he shall restore double (Ex. 22:1, 4).

We think of the criminal’s victimis as being people who have
lost their animals or money. But there are other victims: the
animals themselves. This is analogous to the crime of kidnapping.
The restitution system that the Bible establishes for oxen and
sheep reflects this special concem by God for helpless animals.
What makes sheep and oxen special is their status in the Old
Testament as symbolically helpless animals. So, biblical law pro-
tects both the animals and their owners. Let us consider each in
tum,

A. Symbolically Helpless Animals

Why the requirement of five-fold restitution for a slaughtered
or sold ox? Oxen require training, meaning a capital investment
by the owner, in order to make them effective servants of man in
the tasks of dominion, but so do other animals, such as horses and
donkeys, yet only two-fold restitution is required for them. Also,
a thief who is found with a living ox in his posscssion pays only
double restitution. What makes a slaughtered or sold ox different?
Answer: the ox is symbolic of the employed servant. It is my contention
that this symbolism has more to-do with its five-fold restitution
penalty than the value of its training does.

The Jaw forbids the muzzling of oxen when they are working

182
