Kidnapping 69

form of idolatry, worshipping another God. The kidnapper steals
God’s property—a person made in His image—and seeks io
profit from the assct. This is the essence of the crime of Adam, to
be as God (Gen. 3:5),

Future Deterrence

The death penalty is final: Its beneficial effects for society are
twofold: it restrains the judgment of God on society, and it pro-
vides a deterrence effect — deterring the criminal from future crime
(he dies), deterring other criminals.from committing similar crimes
(fear of death), and deterring God from bringing His covenant
judgments on the community for its failure to uphold covenant
law (fear of God’s wrath). Capital punishment is God’s way of
telling criminals, whether convicted criminals or potential crimi-
nals, that they have. gone too far by committing certain crimes.
Tt also. warns the community that God’s law is to be respected.
Obviously, there is.no clement of rehabilitation for the convicted
criminal in the imposition of the death penalty. The State spceds
the convicted criminal’s march toward final judgment. The State
delivers the sinner into the presence of the final and perfect Judge.'*

If we interpret the presence of the pleonasm as making the
death penalty mandatory, irrespective of the wishes of the victim,
then we create a problem for the victim. A mandatory death penalty
may actually increase the risk to the victim, once the criminal act has taken
place, First, the victim may have seen the criminal. His positive
identification of the kidnapper and his testimony against him can
convict him. Second, should the criminal begin to suspect that
he is about to he caught by the authorities, hc may choose to kill
the victim and dispose of the body. By disposing of the evidence
of the crime, the victim loses his life, while the criminal reduces
his risk of being detected. This is a good reason to suppose that

13, One reason why the tarture of a convicted criminal prior to his execution is
immoral is that it symbolically arrogates to the State what God reserves exclusively
for Himself: the legal authority to torture people for eternity. It is a right that God
exercises exclusively. By lorturing a person prior to his execution, the State asserts
that its punishments are on a par with God’s, that the State's penalties are ta be
feared as much or more than God's. On the State as torturer, see Edward Peters,
‘Torture (London: Basil Blackwell, 1985), ch. 5.
