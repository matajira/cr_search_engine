Introduction 5

them from power and influence for the last three centuries.

Historical Judgments and the Final Judgment

Christians today hate. the idea that God holds individuals
personally responsible for obeying His revealed law, and so they
also deny that God holds society collectively responsible for obey-
ing His law. They deny the possibility of the following:

And it shall be, if thou do at all forget the Lorp thy God, and walk
after other gods, and serve them, and worship them, I testify against
you this day that ye shall surely perish. As the nations which the Lorp
destroyeth before your face, so shall ye perish; because ye would not be
obedient unto the voice of the Lory your God (Deut. 8:19-20).

Such preliminary historical negative judgments are intended
by God to remind men of His final judgment at the end of history.
Covenant-breaking man hates the thought of the latter, so he
denies the possibility of the former. Things happen randomly, not
covenantally, he prefers to believe. In preference to.a belief in the
final judgment, selfproclaimed autonomous man, will believe in
the sovereignty of anything elsc, from crecping things (Rom. 1:22-
23) to impersonal random forces that produce creeping things out
of lifeless chemicals (Darwinism). He much prefers to believe in
the cosmic death of the universe rather than the eternal resurrec-
tion of all men, some to eternal joy in the presence of God, and
the rest to God’s eternal torturing,!°

‘The doctrine of final judgment affects every other theological
doctrine, although modern theologians in. all camps are almost
perversely unwilling to admit this possibility or even debate it. It
also affects social theory, which social theorists have been equally
unwilling to discuss. There can be no society without a doctrine: of final
judgment. This doctrine is an inescapable concept. The question
is never, “Final judgment or no final judgment?” The question is:
“Who will administer this final judgment?” ‘There are also two
secondary questions: “Who speaks representatively in history for
this judge? Who administers his preliminary, representative judg-
ments?”

10, Gary North, Is the World Running Dawn? Crisis in the Christian Worldview (Tyler,
Texas: Institute for Christian Economics, 1988), ch. 2.
