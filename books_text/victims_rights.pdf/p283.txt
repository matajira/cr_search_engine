Emptying the Prisons, Slowly 271

was more than ample reason for parole revocation.”*! Manson’s
parole officer stated in court that he could not remember whether
Manson had been on probation or parole; the man was responsi-
ble for overseeing 150 persons.”? Manson had actually begged to
be allowed to remain in jail when they released him in 1967; at
that time, he was 32 years old, and had spent-17 years in penal
and reform institutions.

Humanists look at the “eye for cye” principle, and react in
horror, They do not react with cqual consternation when they
confront the problem of the late twenticth century’s increase in
violent crime. Statistics on crime for the United States are readily
available and comprehensive, and I am including a brief survey
of this material in order to present an overview of the crisis facing
Western, humanist culture. At the end of an age, we expect to see
an increase in criminal behavior, as lawlessness becomes a way
of life for a dedicated, pathological minority, while religious and
cultural relativism and self-doubt render citizens and their elected
authorities helpless to stem this tide of consistent lawlessness.
Gilbert Murray, the great student of Greek civilization, character-
ized the last days of Greek religion as “the failure of nerve.”?+
This secms to fit late-twenticth-century Western humanism quite
well.

The prison is a burcaucracy, not a market-oriented institu-
tion. It is run by the State through taxes; it is.a bureaucratic
management system, not a profit management system.”° Men are
trained to follow orders, not to innovate, take risks, and meet
market demand. There are many arguments against prisons, as
revealed by an enormous bibliography on alternatives tu pris-

21. Vincent Bugliosi, Helter Skelter: ‘The True Story of the Manson Murders (New York:
Norton, 1974), p. 420.
22. Ibid., p. 419.
23. Ibid., p. 146.

24. Gitbert Murray, The Five Stages of Greck Religion (1925 edition), reprinted by
AMS Press and Greenwood Press.

25. See Gary North, “Statist Bureaucracy in the Modern Economy,” in North, An
Introduction io Christian Economics (Nutley, New Jersey: Craig Press, 1973), ch. 20. See
also Ludwig von Mises, Bureaucracy (Spring Mills, Pennsylvania: Libertarian Press,
[1944] 1983}.

 
