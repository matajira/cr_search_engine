Restitution, Repentance, and Restoration 187

they can-be brought into the covenant. Judgment in history is one
way of bringing them in. God told Ezekiel something very similar
regarding the role of a prophet:

Son of man, I have made thee a watchman unto the house of Israel:
therefore hear the word at my mouth, and give them warning from me.
When L say unto the wicked, Thou shalt surely die; and thou givest him
not warning, nor speakest to warn the wicked from his wicked way, to
save his life; the same wicked man shalt die in his iniquity; but his blood
will T require at thine hand. Yet if thou warn the wicked, and he turn
not from his wickedness, nor from his wicked way, he shall die in his
iniquity; but thou hast delivered thy soul (Ezek. 3:17-19).

The Old Testament prophet served as an agent of God’s
ecclesiastical and final courts. The witness in ancient Israel served
as an agent of God’s civil court. The covenant-keeper today has
the office of prophet, for he possesses God’s word. He can lawfully
bring an ecclesiastical covenant lawsuit against God’s enemies
within the church and a warning of eternal judgment to those
outside. He is also a prince, for he can be a witness in civil trials.
He can bring a civil covenant lawsuit against civil law-breakers.
He is the defender of God’s interests. If he refuses to serve in these
officcs, then he becomes God’s victim. The blood of the guilty
will be on his hands, God warned Ezekiel, as well as the blood of
the innocent,

Concern for the Victim

Concern for the victim rather than with rehabilitation of the
criminal often marked what today are dismissed as “primitive”
socictics.’ English common law has also tended to focus on retri-
bution, not the rehabilitation of the criminal. It seeks to punish
men in specific ways for specific evil acts. Therc is a tight relation-
ship between the nature of the crime and the kind of punishment
administered by the civil authorities: Justice is concerncd with
measures, which is why the goddess of justice is pictured as a
blindfolded woman holding scales. Punishment is measured out in

5. Stephen Schafer, Compensution and Restitution to Victims of Crime (2nd ed.; Montelair,
New Jersey: Patterson Smith, 1970).
