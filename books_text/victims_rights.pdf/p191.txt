The Pitfalls of Negligence 179

responsible, Beasts are not cxpected tu understand property rights;
the owner must fence his property, or cover his pit, or pay restitu-
tion to the dead beast’s owner. He cannot legitimately pass on the
tisks associated with uncovered pits to his neighbors.

The civil government has an analogous responsibility to pro-
tect those who use the property which belongs to, or is admini-
stered by, the State. Thus, speed limits, crossing guards, and
school ‘safety patrols are created. Patrol cars monitor traffic in
neighborhoods. Fines are collected from speeders and other traffic
violators. Why fines? Because there are limits on the knowledge
of law enforcement authorities; thus, fines are used as a way to
collect restitution payments from known violators, and to make
payments to victims of unknown violators.

Responsibility is personal, and it involves every area of author-
ity exercised by any individual. The civil government has the
obligation of setting forth principles of judicial interpretation that
will prevail in any civil court. The court will look at the circum-
stances surrounding the injured party, and determine who was
responsible. If the property owner was attempting to pass on
involuntarily to innocent third parties the risks of ownership, the
court will find the owner guilty. All property owners know this in
advance, and they can take steps to reduce their legal risks by
reducing involuntary risks borne by innocent third parties.

The Bible docs not warrant the establishment of a huge bu-
Teaucracy to define every area of possible risk, promulgate minute
definitions of what constitutes unlawful uses of property, and
describe in detail every penalty associated with a violation. The
Bible certainly docs not indicate that the civil government is
warranted to step in and proclaim a potentially injurious action
illegal, except in cases where the violator could not conceivably
make restitution to all the potential victims (e.g., fire codes) or in
cases of repeated violations (the “notorious ox” principle), The
Bible simply reminds property owners of the consequences of
creating hazards to life and limb for third parties who were not
consulted in advance concerning their willingness to bear the
risks, The property owner is assumed to be competent to make
judgments for himself concerning the consequences of his actions,
and then take the steps necessary to reduce his risks.
