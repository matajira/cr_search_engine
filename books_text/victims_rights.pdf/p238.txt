226 VICTIM'S RIGHTS

that are written in this book, that thou mayest fear this glorious
and fearful name, THE LORD THY GOD; then the Lorn will
make thy plagues wonderful, and the plagues of thy seed, even
great plagues, and of long continuance, and sorc sicknesses, and
of long continuance” (Deut. 28:58-59).

The necessity of making restitution reminds the covenanted
nation to fear the God who exacts a perfect restitution payment
to Himself on judgment day, and who brings His wrath in history
as a warning of the final judgment to come. He brings His wrath
either through lawfully constituted civil government or, if civil
government refuses to honor the terms of His covenant, through
such visible judgments as wars, plagues, and famines. This is why
the nation was warned to fear God, immediately after the presen-
tation of the Ten Commandments: *. . . God is come to prove
you, and that his fear may be before your faces, that ye sin not”
(Ex. 20:20b).

Jesus was not departing from the biblical view of judicial
sanctions when He warned: “Fear him which is able to destroy
both soul and body in hell” (Matt. 10:28b). It is eternal punish-
ment which is to serve as the covenantal foundation of all judicial
sanctions. Civil government is supposed to reflect God’s govern-
ment. Public punishments deter evil. They remind men: better
temporal punishment that leads to repentance (personal and na-
tional) than eternal punishment that does not lead to repentance
(personal), Repentance is possible only in history.

 

Capital Punishment

Phillips is consistently incorrect when he writes: “Modern
theories of punishment are therefore totally inapplicable when
considcring reasons why ancient Israel executed her criminals, for
the punishment was not looked at from the criminal’s point of
view. This extreme penalty was not designed to deter potential
criminals, nor as an act of retribution, but as a means of prevent-
ing divine action by appeasing Yahweh's wrath.”® If criminal law
was “not looked at from the criminal’s point of view,” then why
does the Bible repeatedly refer to the fear of external punishment

9. Phillips, Ancient Israel’s Criminal Law, p. 12.
