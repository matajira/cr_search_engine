206 VICTIM'S RIGHTS

agrees.? ‘The Jewish scholar, Cassuto, argues along similar lines:
“He shall pay five oxen. for an ox, and four sheep for a sheep —Icss for
a sheep than for an ox, possibly because the rearing of a sheep
does not require so much, or so prolonged, effort as the rearing
of herds.”* In fact, this interpretation is quite traditional among
Jewish scholars.>

This interpretation seems to get support from the laws of at
least one nation contemporary. with ancient Israel. The Hittites
also imposed varying penalties according to which animal had
been stolen. Anyone who stole a bull and changed its brand, if
discovered, had to repay thc owner with seven head of cattle: two
three-year-olds, three yearlings, and two weanlings.6 A cow re-
ceived a five-fold restitution payment.’ The same penalty was
imposed on thieves of stallions and rams.’ A plow-ox required a
ten-fold restitution (previously 15).° The same was true of a draft
horse."® Thus, it appears that trained work animals were evalu-
ated as being worth more to replace than the others. Anyone who
recovered a stolen horse, mule, or donkey was to receive an
additional animal: double restitution.!! The origmal animal that
had received training was returned; thus, the thief did not have
to pay multiple restitution.

It secms reasonable to conclude that the Bible’s higher pay-
ment for a sheep or ox is based on the costs of retrainmg an
equivalcnt animal. But what seems reasonable at first glance tums
out to he mistaken,

3. Walter C. Kaiser, Jv., Toward Old Testament Ethics (Grand Rapids, Michigan:
Zondervan Academie, 1983), p. 105.

4. U. Gassuto, A Commentary on the Book of Exedus, translated by Israel Abrahams
Gerusalem: The Magnes Press, The Hebrew University, [1951] 1974), p. 282.

5. See the citations by Nchama Leibowitz, Studies in Shemot, Part 2 (Jerusalem:
World Zionist Organization, 1976), p. 364.

6. “Hittite Laws,” paragraph 60. Ancient Near Eastem Texts Relating to the Old
Testament, edited by James B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton
University Press, 1969), p. 192.

2. ddem., paragraph 67.
§. Idem., paragraphs 61, 62.
9, Idem., paragraph 63.
10. Ide., paragraph 64.
IL, Idem., paragraph 70.
