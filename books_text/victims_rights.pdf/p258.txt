246 VICTIM'S RIGHTS

It is the task of biblical exegesis to establish the ethical and legal
foundations that enable civil judges to do the following: 1) identify
the winners and the losers; 2) adjudicate cases properly in the
sight of God; and 3) determine what is fair compensation to the
losers from any unauthorized winners. One thing is certain: we
cannot hope to attain a perfectly safe world. There are always
risks in life. We are mortal. We are not omniscient.

Transferring Risk

Each owner is also responsible for whatever actions his ani-
mate or inanimate objects do that injure others. A fire that a man
kindles on his land must be kept restrained to his property. If the
fire spreads to his neighbor’s field, he is fully accountable for all
the damages. Men therefore have an incentive to take greater care
when using potentially dangerous tools or techniques.

The problem of pollution should be subsumed under the gen-
cral principle of responsibility for fire. A fire is a physical cause
of physical damage. From the case-law cxample in Exodus 22:5,
it is clear that the fire which a man starts is his responsibility. He
cannot legally transfer risks to his neighbor without his neighbor’s consent.

The Bible is not talking here about some shared project in
which both men expect to profit, such as burning fields to get rid
of weeds or unwanted grass. In such a mutually shared project,
the casc-law example of the man who rents his work animal to a
neighbor, but who stays with the animal the whole time, is appli-
cable. The neighbor is not required to pay anything beyond the
hiring fee to the owner (Ex, 22:14-15). If the animal is hurt or
killed, the neighbor owes nothing.

There is no doubt that the fire-starter is responsible for all
subsequent fires that his original fire starts. Sparks from a fire can
spread anywhere. A fire beginning on one man’s farm can spread
over thousands of acres. Fire is therefore essentially unpredictable.
Its effects on specific people living nearby cannot be known with
precision, The uncertainty, meaning the statistical unpredictability,
of specific, individual consequences is the factor that governs the
rule of restitution for damage-producing fircs, as well as laws
relating to the regulation of fire hazards.
