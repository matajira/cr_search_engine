Introduction 13

table to return the shot: “Wham! Uniquely Christian social rele-
vance!” The ball goes faster and faster, back and forth, until the
man at last drops from exhaustion. The outcome of the game is
resolved only by the unwillingness or inability of the player to
continue. It has nothing to do with the strength of either position.
The astounding thing is that each author seems utterly oblivious
to the fact that he is “playing both sides of the table.” Each time
he hits the ball, he acts as though he expects someone else on the
other side to return it. It is as if the author writes two separate
manuscripts, one pro-biblical law and one against, as a kind of
academic exercise, and then some inebriated editor mistakenly
assembles them into a single volume. Amazingly, these books sell!
And the reviewers seem blissfully unaware of the intellectual schizo-
phrenia they are reviewing. T suspect that they are suffering from
the same bizarre affliction.*

Conclusion

There is today a ncar-universal agreement within the Chris-
tian community that the case laws of the Old Testament do not
and should not apply to today’s societies. We are assured that there
are no biblical blueprints for “sccular” social institutions. This
position is generally defended in the name of religious pluralism.
It has been the dominant political idea within even the most
conservative Christian circles in America for well over a century.?+
Is this position true? It rests squarely on the concept of religious
neutrality in social theory. Can it be true if the Bible is true?

Tt is the reader’s moral responsibility to determine for himself
which approach seems the most biblical in the search for appro-
priate civil sanctions: 1) going to-the Bible; 2) going to Harvard
University and the Harvard Law School or one of their humanist-

38. Gary North, “The Intellectual Schizophrenia of the New Christian Right,”
Christianity and Civilization, 1 (1982), pp. 1-40; Kevin Craig, “Social Apologetics,” ébid.,
pp. 41-76.

34. Gary Scott Smith, The Seeds of Secularization: Calvinism, Culture, and Pluralism in
America, 1870-1915 (Grand Rapids, Michigan: Christian University Press, 1985). With
the exception of Bob Jones University, I am unaware of any officially Bible-based,
PhD. granting university. Still, the publishing company’s name has a nice ring to it.
The firm is a subdivision of William B. Eerdmans Company.

   
