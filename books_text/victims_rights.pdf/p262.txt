250 VICTIM'S RIGHTS

brick houses, was spared.’! Other towns adopted “brick-only”
building codes for chimneys (and even for entire homes in the
eighteenth century), Publicly financed chimney sweeps inspected
chimneys, the single greatest cause of fires in these years.'° New
York had weekly chimney inspections for twenty years, beginning
in 1697, and the city experienced no major fires.!© Such measures
represented an infringement on personal freedom, and they in-
creased costs to taxpayers, but they were necessary to help protect
people from each other’s mistakes — mistakes for which the per-
son responsible could not have afforded to pay.

No Omniscience

Men are not omniscient; therefore, information must be paid
for. Accurate information is even more expensive. Any approach
to economics that does not honor this principle from start to finish
will be filled with errors.!”

Individual sparks from a fire arc unpredictable in their effects.
We can make guesses about the overall effects of a fire, but an
area of uncertainty is inescapable. Living next door to a fire-
starter may be tolerable. Farmers start fires to burn grasses or
timber, for example. We do not call for a complete banning of all
open fires. We do make people responsible for damage produced
by fires that they start. The greater the danger of fire, the more
concerned nearby residents must be. Sometimes, the public bans
fires altogether.

Because no one can know everything, it is impossible to pre-
serve life by eliminating every possible danger before any action
can be taken. It would make human action impossible. We are
not God; socicty must not expect people to perform as if they
were God. Thus, there must always be limited legal liability in
life. Nevertheless, for those actions that are known to be danger-
ous, people must be made legally responsible for their actions.
This does not justify holding people fully responsible for actions

 
  
 

+p. 56.
+p. 207.
17. Thomas Sowell, Knowledge and Decisions (New York: Basic Books, 1980).
