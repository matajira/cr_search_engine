Kidnapping 77

awareness of a future, it is of something fixed, fated, beyond his
control; things happen fe him, he does not make them happen.
Impulse governs his behavior, either because he cannot discipline
himself to sacrifice a present for a future satisfaction or because
he has no sense of the future. He is therefore radically improvi-
dent: whatever he cannot use immediately he considers valueless.
His bodily needs (especially for sex) and his taste for ‘action’ take
precedence over everything else~and certainly over any work
routine.”2*

‘A law-order must recognize present-oriented people for what
they are. The kidnapper may be somewhat more future-oriented
than the lower-class man. He makes plans, counts costs, and takes
risks: But he discounts. the long-term consequences of his acts.
He does not care about the effects on the victim, his family, or the
community, It is this radical lack of concern for the lives and callings of
other men that makes him a menace to society. To-catch his atten-
tion, to convince him of the seriousness of his crime, the Bible
stipulates the death penalty. Richard Posner, an economist and
also a judge for the U.S. Court of Appeals, acknowledges the
validity of-relationship between a criminal’s time perspective and
the need for capital punishment, but only in a footnote: “Notice
that if criminals’ discount rates arc very high, capital punishment
may be an inescapable method of punishing very serious crimes.”?*

The total discontinuity involved in the execution of the kid-
napper favors continuity in the lives of the innocent. It is the innocent
people of society who deserve continuity, not the kidnappers. The
decision to prosecute, or to specify a penalty other than death, is
in the hands of the victim or his survivors. The victim is allowed
by biblical law to bargain with the kidnapper in order to obtain
his freedom. (The.kidnapper would havc no way to get even with
a victim who subsequently changed his mind and called for the
death penalty.)

23. Edward Banfield, The Heavenly City Revisited (Boston: Little, Brown, 1973), p.
61,

24,.Richard Posner, Eoonomic Analysis of Law (Boston: Little, Brown, 1986), p.
22n,
