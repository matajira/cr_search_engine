The Costs of Private Conflict 87

compensation for the injured person’s loss of earnings (time).
Fifth, compensation for the embarrassment or indignity suffered
by the victim,” Not all five will be found in each case, of course.?

The judicially significant point is that the person who wins
the conflict physically becomes the loser economically. The one
who is still walking around after the fight must finance the physi-
cal recovery of the one who is in bed. The focus of judicial concern
is on the victim who suffers the greatest physical injury. Biblical
law and Jewish law impose economic penalties on the injury-
inflicting victors of such private conflicts. As Maimonides put it,
“The Sages have penalized strong-armed fools by ruling that the
injured person should be held trustworthy. . . .”4

Games of Bloodshed

The murderous “games” of ancient Rome, where gladiators
slew each other in front of cheering crowds, violated biblical law,
The same is true of “sports” like boxing, where the inflicting of
injuries is basic to victory, The lure of bloody games is decidedly
pagan. Augustine, in his Confessions, speaks of a former student of
his, Alypius. ‘lhe young nian had been deeply fond of the Circen-
sian games of Carthage. Augustine had persuaded him of their
evil, and the young man stopped attending. Later on, however,
in Rome, Alypius met some fellow students who dragged him in
a friendly way to the Roman amphitheater on the day of the

2. Baba Kamma 8:1, The Mishnah, edited by Herbert Danby (New York: Oxford
University Press, [1933] 1987), p. 342.

3, Emanuel B, Quint, Jewish Jurispridence: its Sources and Modern Applications, 2
vols. (New York: Harwood Academic Publishers, 1980), 1, p, 126, Maimonides
wrote: “If one wounds another, he must pay compensation to him for five effects of
the injury, namely, damages, pain, medical treatment, cnforced idleness, and humili-
ation. ‘These five effects are all payable from the injurer’s best property, as is the law
for all who do wrongful damage.” Moses Maimonides, The Book of Torts, vol. 11 of
The Cue of Maimonides, \4 vols. (New Haven, Connecticut: Yale University Press,
[1180] 1954), “Laws Concerning Wounding and Damaging,” Chapter Onc, Section
One, p. 160. Maimonides made one strange exception: if a person deliberately
frightens someone, but does not touch him, he bears no legal liability, only moral
liability. Even if he shouts in a person's ear and deafens him, there is no legal
liability. Only if he touches the person is there legul liability: iid, Chapter Two,
Section Seven, pp. 165-66.

4, Tarts, Chapter Five, Section Four, p. 177.
