Restitution and Deterrence 223

transgressions for which the Bible specifies restitution payments
to victims. Such acts are criminal acts against the community.
Why? Because if they go unpunished, God threatens to curse the
community. Thus, criminal law in the Bible was not enforced “in
the name of the community,” but ix the name of God, so as to
protect the community from God’s wrath.

Restitution to God

Phillips is consistent in his crrors, at least; he also argues that
Hebrew covenant law was exclusively criminal law, mcaning that
its goal was solely the enforcement of public morals, rather than
civil law (torts), in which restitution to the victim was primary.‘
This definition, if correct, would remove from covenant law all
biblical statutes that require restitution to victims. What he is
trying to do is-scparate thc case laws of Exodus from the Ten
Commandments. If believed, this argument would make it far
easier for antinomians to reject the continuing validity of the case
laws in New Testament times, for the case laws of Exodus and
other books rest heavily on the imposition of restitution payments
to victims. The antinomians could publicly claim allegiance to the
Ten Commandments, but then they could distance themselves
from. the specific applications of these commandments through the
case laws, for they have concluded that the case laws are uncon-
nected to the Decalogue because these are “civil” laws rather than
“criminal” laws.° Phillips writes: “But it is the contention of this
study that Israel herself understood the Decalogue as her criminal
law code, and that the law contained in it, and developed from it,
was sharply distinguished from her civil law.”®

If true,. then all you need to do to escape from the covenantal,
State-enforced requirements of the Decalogue is to make the Ten
Commandments appear ridiculous. This he attempts in Chapter
Two. “Initially only free adult males were subject to Israel’s

4. Bbid., pp. 10-11.

5. Phillips says that the “Book of the Covenant,” meaning Exodus 21-23, was a
product of David’s reign, with some of it quite possibly written by David himself.
Ibid., ch. 14.

6. Ibid, p. 1.
