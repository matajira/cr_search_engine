Introduction iy

ments” (John 14:15). Jesus also said: “He that is not with me is
against me; and he that gathereth not with me scattercth abroad”
(Matt. 12:30). By systematically applying this inescapable biblical
principle of non-neutrality in several areas of social theory and
policy,”' and by presenting Bible-based evidence for it, verse by
verse, the Christian Rcconstructionists have earned the hostility
of American humanism,” conservatism,” fundamentalism?! tra-
ditional pentecostalism,?> conservative Lutheranism,” Calvinistic
Presbyterianism,”” and neo-evangelicalism.”* (Other groups would
be equally outraged, except they have yet to hear about us.)

The New Schizophrenia

If the critics are correct, then this book will reveal just how
far Christian Reconstructionists arc from the truth, and how close
the humanists are to the truth. Either the Word of God-establishes
the judicial standards for society or else the word of man does.
There is no third position. This is the reason why those Christians
who insist that the Christian Reconstructionists are wrong about
the legitimacy of biblical civil law are so frequently (i.e., always)
found to be supporters of this or that humanist social program,
baptizing it in the name of Christianity. They have no explicitly

21. See, for example, the ten-volume set T edited, the Biblical Blueprints Series,
published by Dominion Press, Ft. Worth, Texas, 1986-87.

22. Frederick Edwords and Stephen McCabe, “Getting Out God’s Vote: Pat
Robertson and the Evangelicals,” The Humanist (May/June 1987). The article tinks
Ghristian Reconstructionism with Robertson's political ambitions.

23, Anson Shupe, “Prophets of a Biblical America,” Wall Street Journal (April 12,
1989), editorial page:

24. House and Tee, Dominion Theology; Lindsey, Road to Holocaust.

25. The Assemblies of God passed u resolution in 1987 declaring “Dominion
“Theology” a heresy.

26. “Reconstruction and Post-Millenniatism: Mixing Law and Gospel —
Constructing an Earthly Kingdom,” Ghristian News (Oct. 10, 1988), p. 10,

27. Meredith G, Kline, Gomments on an Qld-New Error,” Westminster Theological
Jounal, XLT (Fall 1978), a critical review of Greg Bahnsen's Theonomy in Christian
Bahics

28. Douglas E. Chismar and David A. Rausch, “Regarding Theonomy: An Essay
of Concern,” journal of the Evangelical Theological Society, vol. 27 (September 1984);
Rodney Clapp, “Democracy as Heresy,” Christianity Today (Feb. 20, 1987).
