90 VICTIM’S RIGHTS

against individual violence. Lawlessness is (o be suppressed. Man
is not told to give vent to his feelings of violence; he is told to
overcome them through self-discipline under God. Wars and vio-
lence come from the lusts of men (Jas. 4:1). These bloody lusts
are to be overcome, not ritually sanctioned. The celebration of
communion is God’s sanctioned bloody ritual which gives men
symbolic blood, but the Bible forbids the drinking of actual blood
(Lev. 3:17; Deut.12:16, 23; Acts 15:20).

Biblical Law Confronts the “Honorable Duel”

The Bible informs us that the civil government is to protect
human life. _ Each man is made in God’s image, and men, acting
as private citizens, do not bave the right to attempt to attack God
indirectly by attacking His image in other men. Men are not
sovercign over their own lives or over the lives of others; God is
(Rev. 1:18). God delegates the right of execution to the civil
government, not to individual men acting outside a lawful institu-
tion in the pursuit of lawful objectives.

The private duel is just such a threat to human life and safety.
Fighting is a threat to social peace. It is disorderly, willful, venge-
ful, and hypothetically autonomous. It poses a threat to innocent
bystanders (Ex. 21:22-25). It can destroy property. When a death
or serious injury is involved, a duel can lead in some socie-
ties — especially those that place family status above civil law — to
an escalation of inter-family feuding and blood vengeance.

‘The premise of the duel or the brawl is the assertion of the
existence of zones of judicial irresponsibility. Men sct aside for them-
selves a kind of arena in which the laws of civil society should not
prevail, There may or may not be rules governing the private
battlefield, but these rules are supposedly special, removing men
from the jurisdiction of civil law. The protection of life and limb
which is basic to the civil law is supposedly suspended by mutual
consent. “Common” Jaws supposedly havc no force over “uncom-
mon” men during the period of the ducl. Somchow, the law of
God does not apply to private warriors who defend their own
honor and seek to impose a mutually agreed-upon form of punish-
ment on their rivals.
