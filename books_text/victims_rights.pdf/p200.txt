188 VIGTIM’S RIGHTS

terms of the severity of the crime.

In contrast, modern humanistic theories of jurisprudence, in
the name of humanitarianism, to a great extent have promoted a
messianic view of the State. Prof. Lon Fuller has summarized the
contrasting views, and the heart of the controversy is the assertion
of the ability of the State to recreate man: “The familiar penal or
retributive theory looks to the act and seeks to make the miscreant
pay for his misdeed; the rehabilitative theory on the other hand,
sees the purpose of the law as recreating the person, or improving
the criminal himself so that any impulses toward misconduct will
be eliminated or brought under internal control. Despite the hu-
mane appeal of the rehabilitative theory, the actual processes of
criminal trials remain under the domination of the view that we
must try the act, not the man; any departure from this conception,
it is feared, would sacrifice justice to a policy of paternalistic
intervention in the life of the individual.”* This fear is well-
deserved: continual interventions into the lives of men by a self
professed omniscient paternalistic State are precisely where a legal
theory of “trying the man rather than his acts” do lead. A jury
can make the criminal “pay for his crime” by paying the victim;
members of the jury can make reasonable estimates of the eco-
nomic effects of the convicted criminal’s acts. On the other hand,
jurors cannot read the convicted criminal’s mind. All they can
do is draw conclusions regarding his intentions based on the cross
examination of public testimony and evidence that was obtained
legally. When men try to read other men’s minds, the result is
tyranny.

Restitution by the criminal to the victim is one way of restor-
ing wholeness to the victim. It also reduces the likelihood of
private attempts at vengeance.’ It is a way of dealing with guilt.
In this sense, il is also-a means of restoring wholeness to the
criminal.

6. Lon Fuller (1969), cited by Richard E, Laster, “Criminal Restitutioy
Survey of Ils Past History and an Analysis of Its Present Usefulness,” Uni
Richmond Law Review, V (1970), p. 97. Laster’s study concludes thal. the role of the
victim in criminal law has steadily diminished (p. 97).

7. Laster, ibid., p. 75.

 
   
