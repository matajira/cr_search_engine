rigor of, 22, 28
sandions, 39
social order, 296-97
social order and, 2
Ten Commandments, 223
under, xi
universalized, 58
lawlessness, 271
legalism, 280
legal predictability, 93
legislation, 172, 178
legitimacy, 95, 101
leniency, 278-79
Levi, Fdward, 202
Lewis, C. 8., 121
lex tationis (see eye for eye}
liability, 147-49
corporate law, 157-59
church, 157
knowledge and, 155, 159
legistation and, 171-72
limited, 155-66
limited liability, 138-39, 250
linear history, 114
Locke; John, 56
loopholes, 32

    

magic, 273

Maimonides, 87n, 142, 147, 189n,
195n, 210, 244n

manslaughter, 92

Manson, Charles, 270-71

Mardi Gras, 88

Marxism, 96

Masonic oaths, 111

Massachusetts, 152, 202-3, 249

measures, 186-87

mercy (see forgiveness)

Minnesota, 190, 201

monopoly, 81

murder, 7, 20, 21, 47
capital punishment, 55n, 160
covenant lawsuit, 184
execution, 146

General Index 311

expiation, 185
God, 65-66

Massachusetts, 146

Noah’s covenant, 53

no ransom, 117

State as agent, 40.

trial of Jesus, 37

unsolved, 36, 185

Murray, John, 55n, 56
mutilation, 134, 136, 255-56

Nachmanides, 117
National Victim Center, 3
nations, 54-55
natural law, 56
negative sanctions, 9
negligence

costs &, 152-53

externalities,

insurance, 165

knowledge &, 159-60

pit, 167
neighbor, 231
neo-evangelicals, 280
neutrality, 10-11, 182, 245-46, 267
Newton, Isaac, 4
Noah’s covenant, 53-58
“No Trespassing,” 170, 174
nuclear power, 163

oath, 110-11, 239-40
omniscience, 18, 250-51, 260
ownership, 143, 167-68
bundle of rights, 212, 216
Tawful tile, 216-18
legal immunities, 217
right to exclude, 217
ox, 116, 131, 137, 139-40
executed, 160
owner's negligence, 152-54
self-disciplined, 142
stoning, 143-47
training, 194

parents, 15, 18n
