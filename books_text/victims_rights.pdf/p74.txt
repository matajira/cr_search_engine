62

VICTIM'S RIGHTS

his father’s nakedness: hoth of them shall surely be put to death;
their blood shall be upon them (Lev, 20:11).

And if a man lie with his daughter-in-law, both of them shall
surely be put to death: they have wrought confusion; their blood
shall be upon them (Lev. 20:12).

If a man also lie with mankind, as he lieth with a woman,
both of them have committed an abomination: they shall
surely be put to death; their blood shall be upon them (T.cv.
20:13).

And if a man lie with a beast, he shall surely be put to
death: and ye shall slay the beast (Lev. 20:15}.

And if a woman approach unto any beast, and lie down
thereto, thou shalt kill the woman, and the beast: they shall
surely be put to death; their blood shall be upon them (Lev.
20:16).

A man also or woman that hath a familiar spirit, or that
is a wizard, shall surely be put to death: they shall stone them
with stones: their blood shall be upon them (Lev. 20:27).

And he that blasphemeth the name of the Lorn, he shall
surely be put to death, and all the congregation shall cer-
tainly stone him: as well the stranger, as he that is born in
the land, when he blasphemeth the name of the Lorn, shall
be put to death (Lev. 24:16).

And he that killeth any man shal] surely be put to death
(Lev. 24:17).

I the Lorn have said, I will surely do it unto all this evil
congregation, that arc gathered together against me: in this wilder-
ness they shall be consumed, and there they shall die (Num.
14:35).

For the Lorp had said of them, They shall surely dic in the
wilderncss. And there was not left a man of them, save Caleb the
son of Jephunnch, and Joshua the son of Nun (Num. 26:65).

And if he smite him with an instrument of iron, so that
he die, he is a murderer: the murderer shall surely be put
to death. And if he smite him with throwing a stone, where-
with he may die, and he die, he is a murderer: the murderer
shall surely be put to death. Or if he smite him with an hand
weapon of wood, wherewith he may die, and he die, he is a
