Conclusion 281

over a century, American fundamentalists have had no influence
on the social and political events of the day, save only for the
illfated: political experiment in the United States called Prohibi-
tion (1918-33). Prohibition’s visible failure to keep people from
drinking alcohol, and the’ voters’ subsequent repeal of the Eigh-
teenth Amendment, drove fundamentalists into the American po-
litical wilderness for two generations. How can people who prefer
anything and anyone to Moses be expected to speak out effectively
in the name of the Lord on social and cultural issues? They can’t,
they haven't, and they don’t. They won’t, either, until they redis-
cover and recommit to Exodus 21-23. It is here that we find the
theological foundation for a thoroughly biblical reform of the
criminal justice system, which today surely és criminal.

The Reform of the Criminal Justice System

What would be the marks of civil: justice during an era of
biblical justice? Victims would scc the restoration of their stolen
assets, while criminals would sce their ill-gotten capital melt away
because of the financial burden of making restitution payments.
The dual sanctions of curse and blessing — part four of the biblical
covenant model? —- are invoked and imposed wherever the princi-
ple of restitution is honored in the courts, both civil and ecclesias-
tical, Restitution brings both judgment and restoration, which affect
individual lives and social institutions.

There are limits to biblical restitution. First,-the full value of
whatever was stolen is returmed by the thief to the original owncr.
Second, the thief makes an additional penalty payment equal to
the value of the item stolen. To encourage criminals to admit their
guilt and seek restoration before their crimes are discovered, the
Bible imposes a reduced penalty of 20% on those who admit their
guilt voluntarily (Lev. 6:2-5).

There are two explicit exceptions to the payment of double
restitution. The law singles out oxen and shccp as deserving
special protection in the form of five-fold and four-fold restitution
in cases where the stolen animals are killed or sold, Because oxen.

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4.
