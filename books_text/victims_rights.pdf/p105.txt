The Costs of Private Conflict 93

the only instance of a substitution of payment for the death sen-
tence involves criminal negligence — the failure to contain a dan-
gerous beast which subsequently kills a man—but not willful
violence (Ex. 21:29-30).. The autonomous shedding of man’s blood,
even to “defend one’s good name,” is still murder. There is the
perverse lure of such “conflicts of honor.”

Tt is clear that if a biblically honorable man refuses to fight
because the civil law supports his position by threatening him
with death should he successfully kill his opponent, he can avoid
the fight in the name of personal self-confidence. He says, in effect,
*T know I can probably kill you; therefore, | choose not to enter
this fight because I will surely be executed after I kill you.” Thus,
he can avoid being regarded as a coward. This breaks thc central
social hold that the code duello has always possessed: the honorable
man’s fear of being labeled a coward. But in order to deflect this
powerful hold, the State must be willing to enforce the death
penalty. on victors.

Courts and Vigilantes

Legal predictability is crucial to the preservation of an orderly
socicty. The breakdown of predictable justice in any era can lead
to a revival of blood vengeance. Those who are convinced that the
court system is unable to dispense justice and defend the innocent
are tempted to “take the law into. their own hands.” The risc of
vigilante groups that take over the administration of physical
sanctions always comes at the expense of legal predictability. This
is a sign of the breakdown in the legal order, and it is accompa-
nied by a loss of legitimacy by “establishment” institutions,!

 

15. This appears to be heginning in lange cities in the United States. Citizen's
patrols became common in certain Jewish districts in the New York City area in the
late 1960's. A parallel group of inner-city youths sprang up in the late 1970's, the
Guardian Angels, initially composed mostly of Puerto Ricans. This group has spread
across the United States. By 1988, its leaders claimed 60 chapters and 6,000 mem-
bers. Citizen’s patrols have now spread to black neighborhoods and middie class
neighborhoods, especially in response to the advent of “crack” houses: the modero
equivalent of the opium dens of the nineteenth century. Tn some cases, local police
departments do cooperaic with these cilizer’s patrols, and to this extent they are not
pure vigilante organizations. Sce “Neighbors Join to Rout the Criminals in the
Streets,” /asight (Nov. 28, 1988), pp. 8-21

 
 
