CONCLUSION

Blessed are the undefiled in the way, who walk in the law of the
Lorn. Blessed are they that keep his testimonies, and that seek him
with the whole heart. They also do no iniquity: they walk in his ways.
Thou hast commanded us to keep thy precepis diligently. O that my
ways were directed to keep thy statutes! Then shall I not be ashamed,
when I have respect unto all thy commandments (Ps. 119:1-6).

These words of the Psalmist do not express the sentiments of
modern man. Modern man despises biblical law, for it threatens
to restrain him and the vain works of his imagination. Biblical law
places restraints on the State as well as on individual evil-doers.
It defines evil in terms of the revealed laws of a God who threatens
covenant-breakers with eternal torture, a God who knows the
hearts and minds of every person, and who judges them accord-
ingly. Modern man does not want to be reminded of such a God
or the judgment to come, so he renounces biblical law. He cor-
rectly sees biblical law as a curse on his dreams of autonomy.

A society that renounces biblical law has two choices: to
attempt to construct a legal order that is cither more rigorous
than biblical law or more lenient. Such a society inevitably turns
aside from God’s law. It violates God’s commandment: “Ye shall
observe to do therefore as the Lorp your God hath commanded
you: ye shall not turn aside to the right hand or to the left” (Deut.
5:32). Anarchism beckons on one side, while a one-world socialist
State beckons on the other.

Tf covenant-breaking man identifies with the criminal, he will
prefer judicial leniency. He knows what is coming for him in
cternity if God’s word is true, so he sides ethically, emotionally,
and philosophically with the criminal, in whose camp God also

278
