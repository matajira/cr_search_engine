9
THE PITFALLS OF NEGLIGENCE

And if a man shall open a pit, or if a man shail dig a pit, and
not cover it, and an ox or an ass fall therein; the owner of the pit shall
make it good, and give money unto the owner of them; and the dead
beast shall be his (Ex. 21:33-34).

Here is another variation of the restitution principle. A man
digs a pit for some reason, and fails to cover it. This is negligent
behavior. He knows that unsuspecting people or animals could
fall into the pit and be harmed, His failure to go to the expense
of covering the pit is an example of what economists call “exter-
nalities.” He imposes the risk of an injured beast on the owner of
the beast. By saving time and money in not covering the pit, he thereby
transfers the economic burden of risk to someone else. This is a form of
theft, Someone who cannot benefit from the use of the pit is
expected to pay a portion of its costs of operation, namely, the
risk of injury to any animal that might fall into it. This is the
meaning of economic “externalities”: those who cannot benefit
from an economic decision are forced to pay for part of the costs
of operation.

Biblical civil law. settles the question of property rights and
the responsibilities of ownership. Because the Bible affirms the
rights of private ownership ~ meaning Jegal immunities from inter-
ference by either the State or other private citizens in the use of
one’s property — it therefore imposes responsibilities on owners. The
law regulating uncovered pits is not an infringement on private
property rights. On the contrary, it is an affirmation of such rights.
By linking personal cconomic responsibility to personal, private

167
