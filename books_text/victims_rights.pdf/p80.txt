68 VICTIM'S RIGHTS

anything, the kidnapper who intends to.sell-the victim into servi-
tude has an economic incentive not to harm the victim, since an
injury would presumably reduce the market value of “the prop-
erty.” Yet the kidnapper potentially faces the most fearful penalty
that society can inflict. Why such a concern for this crime?

Sacrilege

To steal from God involves sacrilege.. Rushdoony has made
an interesting study on the meaning and implications of sacrilege,
and his general comments apply in the case of kidnapping. “Theft
is basic to the word, and sacrilege is theft directed against God.
It is apparent from this that the idea of sacrilege is present through-
out Scripture. . . . The concept of sacrilege rests on God’s sover-
eignty and the fact that He has an absolute ownership over all
things: men and the universe are God’s property. The covenant
people arc doubly God’s property: first, by virtue of His creation,
and, second, by virtue of His redemption. For this reason, sin is
more than personal and morc than man-centered. It is a theologi-
cal offense.”® So serious is the crime of sacrilege that it is com-
pared by Paul to adultery and idolatry (Rom. 2:22), both of which
were capital crimes in the Old Testament.' (The code of Ham-
murabi specified the death penalty for those who stole the prop-
erty of either church or State, and also for those who received the
stolen goods.)!!

Because sacrilege is theft, it requires restitution.!? Since sacri-
lege is theft against God, it requires restitution to God. In this
case, the crime is so great that the maximum restitution is the
death of the criminal. No lower payment can suffice if the State
prosecutes and convicts in God’s name. The implied assertion of
autonomy by the criminal, who seeks to play God, represents a

9. R. J. Rushdoony, Law and Society, vol. 11 of Institutes of Biblical Law (Vallecito,
California: Ross House Books, 1982), p. 28.

10, Bid, p. 31.

lL. GH, paragraph 6; Ancient Near Eastern Texts, p. 166. There was an exception:
if the person stole an ox or a sheep from church or State, he paid thirty-fold
restitution; it was ten-fold restitution if the arimal had belonged to a private citizen:
CH, paragraph 8, idem.

12, Rushdoony, Law and Soniety, p. 33.
