208 VICTIM'S RIGHTS

the sum of its expected future rents, discounted by the rate of
time preference for present over future goods, which is the rate of
interest. In short, the capital value of a good is the ‘capitalization’
of its future rents in accordance with the rate of time preference
or interest.”"5 This is not a difficult concept to grasp; unfortu-
nately for human freedom and productivity, very few people have
ever heard about it.

This process of capitalization means that the higher the pre-
vailing interest rate, the smaller the cash payment that a buyer
will offer for a piece of land today: the buyer applies a higher
discount to its expected stream of income.’ Always bear in mind,
however, that no one knows for certain what the future value of
an assev’s output will be, nor does anyone know preciscly how
much the interest rate will fluctuate over the expected productive
life of the assct. Obviously, no one is sure just what the productive
life of any asset will be. Market forecasting involves a great deal
of uncertainty.

What is most important to understand at this point is that
this discounting process applies to aii capital goods (including
durable consumer goods) in the market; it is not simply the
product of a money economy. Monetary exchanges are as bound
by the process of discounting expected future income (rents) as
are all other transactions. Put a different way, the phenomenon of
interest is basic to human action; it is not the product of a money economy.

Uncertainty is the origin of what some economists call en-

13. Murray N. Rothbard, Introduction; Frank A. Fetter, Capital, Interest, and Rent:
Exssays in the Theory of Distribution (Kansas City, Kansas: Sheed Andrews and McMeel,
1977), p. 13.

14, If we expect a lower rate of interest in the future than presently prevails, we
will be willing to pay the prevailing cash price, since the annual rate of return will
be discounted subsequently by a smaller number. Thus, we buy today at a mice, fal
“discount for cash,” and we will be able to sell the property later on for a smaller
discount for cash when the rate of discount (interest) drops. If we expect rates to rise,
we will only buy at less than the prevailing cash market price, which means, of
caurse, that we will not be able to buy it, since the owner can sell it for more to
someone else. The new buyer will then suffer cconomic losses, if our expectation is
correct, He will get a smaller “discount for cash? when he buys today, and if he
wants to sell later on, he will have to accept a larger discount, since the rate of
interest will have risen. The market value of his land will drop.
