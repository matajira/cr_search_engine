covering, 25
objective, 144

ox, 140

removal, 181
sense of, 24, 37
witnesses and, 30

Hammer, Armand, 72n, 100n

Hammer, Julius, 100n

Hammurabi, 67, 68, 74, 106n, 151

harshness, 3-5, 51

Hayek, 225, 258-63

Hayek, F. A., 74-75

heaven, 6

heifer, 185

hell, 6, 266

heresy, 21

hierarchy, 15, 18, 26-27

highway safety, 174

Hirsch, §. R., 38, 50-51, 59, 70n,
117-18, 168-69

history, 76

Hittites, 206

Hobbes, Thomas, 56

homocides, 288-89

honor, 92

Horowitz, George, 51n

horses, 199

House, Wayne, 53-55, 56n

humanism, 8, 9, 1-12

husband, 42-43, 99

hypocrisy, 177-78

Ice, Thomas, 53-53, 56n
idolatry, 68
impersonalism, 274-75
incorrigible sons, 272
insurance, 124-25, 247
automobile, 162
economics, 161-62
negligence, 160-61, 165
nuclear power, 163
screening risk, 165
intellectuals, 100-1

General Index 309

interest, 208
intolerance, 96
irresponsibility, 90
Islam, 114, 254
[srael
conversion of, 43
death penalty, 43
history, 189
kingdom, 60
Rome and, 43

jail, 7
jealousy, 123n
Jesus
antinomian?, 4
atonement, 284
city on a hill, 1
covenant lawsuit, 184-85
debtor's prison, 265
gored slave price, 151
grace, 25
Jews and, 2
jubilee laws, 82
kinsman —redeemer, 184-85
laws and, 1
merey of, 60
restitution, 190, 204
resurrection, 41
sanctions fo God, 25
Sermon on Mount, 1
slave — sin, 150-51
wrial, 37
vietim, 35
wages parable, 259-60
Jewish law, 168-69
Jews, 2
Jordan, 47
Jesus as slave— son, 150-51
stoning, 142
unclean beast, 141-142
Jordan, James, 39n, 41, 106n
Joseph, 44, 47-48, 60
jubilee, 40
jubilee laws, 81-82
