202 VICTIM’S RIGHTS

are covered in some states, In cases of death or permanent disabil-
ity, maximum payments were anywhere from $10,000 to $50,000.
Average payments in 1978 were $3,000 to $4,000 (with the price
of gold in the $175-240 per ounce range), Nonviolent crimes were
not covered, nor were property losses in violent crimes. Only a
small percentage of citizens are aware of these laws; only a small
percentage (1% to 3%) of victims received such payments. Also,
the states compensated victims from state treasuries; the criminals
did not make the payments.

Edward Levi, who served as President Gerald Ford’s Attor-
ney General (1974-76), pinpointed the origin of the penitentiary:
the ideal of the savior State. “While the existence of jails dates
back to medieval times, the idea of penitentiaries is mod-
ern — indeed, it is American, Largely it is the product of the
Quaker notion that if a wrongdoer were separated from his com-
panions, given a great length of time to think about his misdeeds,
and with the help of prayer, he would mend his ways. This
late-18th-century concept was the beginning of what has come to
be known as the rehabilitative ideal.”!* Here is the great irony: it
was Quaker theology that led both to the freeing of the slaves and
the imprisoning of criminals whose productivity should be put
into service of their victims. Prior to the rise of Quaker jurispru-
dence, Roger Campbell reports, “Massachusetts law in 1736 pro-
vided that a thief should be whipped or fined for his first offense.
The second time he was apprehended and proven guilty of that

12. ELS, News and World Report (July 24, 1978). Predictably, state officials wanted
the federal government to fund most of these payments. A bill to pay 90% of such
costs through the Law Enforcement As ce Administration (L.E.A.A.), the Vie-
lims of Crime Act, passed the U. enate in 1973, but did not pass the House and
was not signed into law. Hearings were held in 1972 and 1973. It was a sufficiently
important topic to be included in Major Issues System of the Congressional Research
Service of the Library of Congress heginning in 1974: Grime: Compensation for Victims
and Survivors, Issue Brief 74014,

13. Edward H. Levi, speech at the dedication ceremony for the Federal Bureau
of Prisons Detention Genter, Chicago, Tllinois, October 15, 1975; cited in Roger F.
Campbell, Justice Through Restitution: Making Criminals Pay (Milford, Michigan: Mott
Media, 1977}, p. 63. By far, the best historical account of this transition from town
punishments to the state penitentiary system is David J. Rothman’s prize-winning
study, The Discovery of the Agglum: Social Order and Disorder in the New Republic (Boston:
Little, Brown, 1971).

 

 

 

 
