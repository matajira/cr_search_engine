44 VICTIM'S: RIGHTS

cannot lawfully request a reduced penalty, such as the forfeiture
of her dowry to him, rather than insist on her execution. But is
he so restricted? God spared Israel time after time. It would seem
reasonable that the injured husband might prefer a lesser penalty,
just as God did with Israel. Maybe he still loves her. Maybe this
is her first transgression. He feels decply injured, but not cnough
to have her executed. Perhaps she is a good mother. Perhaps he
wants to keep her as his wifc. Perhaps not. What if he wants a
divorce? This would be granted by the State. He could also re-
quire her. to transfer her dowry to him.

By showing mercy to his wife, he raust also show mercy to
her consort. In the case of adultery involving another man’s wife,
the two adulterers must receive the same negative sanction. The
judges are not permitted to show partiality to persons in rendering
official judgment. The victimized husband who decides to prose-
cute is acting as a judge, for if the adulterers are convicted, he
specifies the penalty. If he wants total vengeance against the
man, he must also demand the same penalty for his wife. If he
shows leniency to her, he must show the same leniency to him.
Why? Because in our capacity as God-ordaincd judges, men arc
not to show partiality, or as the Bible says, “respect of persons”
(Deut. 1:17; 16:19; TI Sam. 14:14; Acts 10:34). When Joseph
decided as a just man to put Mary away privately, he necessarily
also decided not to scek civil justice against any suspected consort.

The Bible does not dircctly discuss the question of leniency
by the victim. The pleonasm “dying, they shall die” is attached
to this crime of adultery (Lev. 20:10). Nevertheless, I am arguing
that.the victim can specify a lesser penalty for the adulterers. If I
am correct, then in such cases, the criminals do not “surely die”
at the hands of the court. But if they are not automatically exe-
cuted upon conviction, then what does the presence of the pleo-
nasm mean? Why is it found in some biblical texts specifying
capital punishment, but not in all of them? The pleonasm is there
for emphasis, the lexicographers say.!* ‘Then what exactly does it
emphasize? Not the absolute necessity of the death penalty in

14, Genesius? Hebrew Grammar (Oxtord, [1910] 1974), sect. 1138, p. 342; cited by
Jordan, The Death Penalty in the Mosaic Law (Tyler, Texas: Biblical Horizons, 1988), p. 9.
