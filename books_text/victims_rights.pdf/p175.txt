The Principle of Limited Liability 163

guaranteed coverage for accidents connected with the generation
of electricity through nuclear power. Power companies are gov-
crnment-licensed public utilities that possess regional monopolies.
The “Price-Anderson” legislation of the 1950’s sets relatively low
ceilings for financial liability by such firms — $560 million per
accident"! — and then the federal government collects the pre-
mium money. By limiting its liability, the federal government
forces residents who live ncar nuclear power sites to co-insure
against a disaster, since there is a maximum pay-out per accident.
The larger the local population that could be affected, the more
each resident co-insures, for the lower the per capita payments
would be. Taxpayers also co-insure: in case of an accident, the
tiny federal nuclear accident insurance fund could not pay off
more than 2 percent of a single $560 million damage suit. Money
taken from the federal government’s general fund would have to
make up the difference. Because of this federal legislation, public
utilities have been able to expand the use of nuclear power genera-
tion. Jn this sense, today’s nuclear power industry has not been
the product of a free market economy; it has been the product of
special-interest Icgislation in the form of liability maximums and
compulsory State insurance coverage.

Anti-nuclear power advocates tend to be anti-free market, and
usually blame the free markct for the nuclear power industry.
Nuclear power proponents usually arc pro-free market, so they
seldom talk about the statist nature of the subsidy. But when the
chips are down, the pro-nuclear power people accept federal sub-
sidies to their program as being economically and ideologically
valid. Writes nuclear power advocate Petr Beckman: “Yes, the
American taxpayer has paid $1 billion to research nuclear safety,
and I consider that a good investment. . . .”!2 He also argues
that the Price-Anderson insurance program makes money for the
federal government because power companies pay premiums to
Washington, along with money sent to private insurance pools.
“You call that a subsidy?” he asks.!5 Of course it is a subsidy.

11, idem.

12. Petr Beckman, The Health Hazards of NOT Going Nuclear (Boulder, Colorado:
Golem Press 1976). p. 154.

13, Wid., p. 156.
