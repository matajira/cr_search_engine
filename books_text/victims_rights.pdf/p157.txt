The Ransom for a Life 145

goings-on, nor are they aware that in ancient Athens, the courts
tried inanimate objects, such as statucs that had fallen and killed
someone. If convicted, the object was. banished from the city.”
Why the silence? Why don’t these stories get into the textbooks?
As Humphrey asks: “Why were we never told? Why- were we
taught so many dreary facts of history at school, and not taught
these?” 3

He answers his own question: modern historians can make
little sense out of these facts. There seems to be no logical explana-
tion, for the way our ancestors treated guilty animals. What is a
guilty animal, anyway — a legally convicted guilty animal? How
can such events be explained? Finkelstein cites the theory of legal
scholar Hans Kelsen that such a practice points to the “animism”
of early medicval Europe, since to try an animal in court obvi-
ously points to a theory of. the animal’s posscssion of a soul.!4
Kelsen says that this reflects early Kurope’s older primitivism.
Finkelstein then attacks Kelscn’s naive approach to an under-
standing of this practice. In contrast to primitive socicties, it is
only in the. Wesi that such legal ‘sanctions against offending ani-
mals have been enforced. “Only in Western socicty, or in socictics
based on the hierarchic classification of the phcnomena of the
universe that is biblical in its origins, do we see the curious
practice of trying and executing animals as if they were human
criminals.”!* Then he makes a profound observation: “What Kelsen
has misunderstood here — and in this he is typical of most West-
cm commentators — is the sense, widespread in primitive socictics
{as, indeed in civilized societies of non-Western derivation), that
the extra-human universe is auéonomous and that this autonomy
or integrity is a quality inherent in every species of thing.”!®
Because Western society long denied such autonomy to the crea-

 

12. W. W. Hyde, “The prosecution of animals and lifeless things in the middle
ages and modern times,” University of Pennsyloania Law Reviav (1936). Finkelstein is
somewhat suspicious of these accounts.

13. Humphrey, “Foreword,” p. xv.

14, Finkelstein, Ox That Gued, p. 48. He cites Kelsen, General Theory of Law and
State (1961), pp. 3-4.

15. Finkelstein, of. cit; p. 48.

16. Mid, p. 51.
