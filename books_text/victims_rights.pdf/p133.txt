The Ransom jor an Eye 121

this threat more eloquently than C. 8. Lewis: “To be taken with-
out consent from my home and friends; to lose my liberty; to
undergo all those assaults.on my personality which modern psy-
choytherapy knows how to deliver; to be re-made. after some
pattern of ‘normality’ hatched in a Viennese laboratory to which
I never professed allegiance; 10 know that this process will never
end until either my captors have succeeded or I grown wise
enough to cheat them with apparent success — who cares whether
this is called Punishment or not? That it includes most of the
elements for which any punishment is feared — shame, exile, bond-
age, and years eaten by the locust —is obvious. Only enormous
ill-desert could justify it; but ill-desert is the very conception
which the Humanitarian. theory has thrown overboard.”!°

The State represents Ged in history in His capacity as cosmic
Judge (Rom. 13:1-7). When a civil government's leaders say that
the State represents any other agent or principle, the State has
begun its march toward cither tyranny or impotence. Either it
will bring judgment on men and other states in the namc of its
deity, its official source of law,!! or else some other State will bring
judgment on it and those governed by it in the name of a foreign
deity. Only a rare nation like Switzerland can defend its borders
for centuries, and thén only by renouncing all thought of conquest
in the narnc of defense and international neutrality.”

The mark of this transformation of the State is when the State
insists. on imposing the punishment in terms of the supposed
“needs of society,” meaning ultimately the needs of the State’s
officers. When the State collects fines for use by the State rather
than to pay victims, when it imposes prison scntences paid for
by the taxes of law-abiding citizens, and when it insists that every

10. C. § Lewis, “The Humanitarian ‘Theory of Punishment,” in Lewis, Gad in the
Dock: Essays on Theology and Ethics, edited by Walter Hooper (Grand Rapids, Michi-
gan: Eerdmans, 1972), pp. 290-91

IL. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 4.

12. Tt had better have high mountains, civil defense, un armed population, and
services such as private banking and a geographical “King’s X? facilities for over-
thrown rulers. -See John McPhee, La Place de la Concorde Suisse (New York: Farear,
Strauss, Giroux, 1984).
