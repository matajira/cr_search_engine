Restitution, Repentance, and Restoration 191

The program was limited primarily to non-violent criminal
offenders who were considered able to pay, which generally meant
white middle-class criminal offenders.!? This limits the empirical
reliability of the conclusions concerning the overall effectiveness
of the program. Also, the amount of restitution was limited to the
amount of the economic loss by the victims, not two-fold restitu-
tion, as required by the Bible. The original state-level trial pro-
gram was dropped in 1976, but the principle has been instituted
at the local level. Judges in every jurisdiction now impose restitu-
tion as a penal sanction.

The Summary Report states that “Most judges and probation
officers favored the use of restitution. Similarly most judges and
probation officers expressed the belief that restitution had a reha-
bilitative effect.” Furthermore, “most victims believed that restitu-
tion by the offender to the victim is the proper method of victim
compensation. Victims who were dissatisfied tended to be those
who felt that they had not been involved in the process of ordering
or aiding in the completion of restitution.” And’ perhaps most
revealing of all, “Most offenders thought that restitution as or-
dered was fair.”!3 Only ten of the offenders (14.4%) would have
preferred a fine or a jail sentence.'* It is understandable why we
have seen a renewed interest in restitution as a form of punish-
ment.!>

Conclusion

Social order requires that there be legal means of the restora-
tion of peace between criminal and victim. If the crime is so
horrendous that no economic restitution payment from the crimi-

12. Idem.

13, dem,

14. Pid, p. 26.

15, Joe Hudson and Burt Galloway (eds.), Considering the Victim: Readings in Restitu-
tion and Victim Compensation (Springfield, Iinois: Charles C. Thomas, 1975);
©. Hobart Mowrer, “Toss and Recovery of Gommunity,” m George M, Gazda (ed.),
Innovations to Croup Psychotherapy (Springfield, Titinois: Charles C. Thomas, 1975).
Such interest has never been entirely absent; Irving E. Cohen, “The Integration of
Restitution in the Probation Services,” Journal of Criviinal Law, Criminolugy and Police
Science, XXXIV (1944), pp. 315-26.
