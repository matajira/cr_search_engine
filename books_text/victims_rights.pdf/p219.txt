Restoring Full Value 207

Discounted Future Value and Capitalization

We need to consider carefully the argument that the higher
restitution penalty is related to the increased difficulty of training
domestic animals. No doubt it is rue that the owner must go to
considerable effort to rctrain a work animal. But is a sheep a work
animal? Does it need training? Obviously not. This should warn
us against adopting such am argument regarding any restitution
payment that is greater than two-fold.

It is quite true that the future value of any stolen asset must
be paid to the victim by the thief. What is not generally under-
stood by non-economists is that ihe present market price of an asset
already includes its expected future value. Modern price theory teaches
that the present price of any scarce economic resource reflects the
estimated future value of the asset’s net output (net stream of
income, or net rents), discounted by the market rate of interest
for the time period that corresponds’ to the expected productive
life of the asset.'? For example, if J expect a piece of land to
produce a net economic return (rent) equivalent to one ounce of
gold per year for a thousand years, I would be foolish to pay a
thousand ounces of gold for it today. The present value to me of
my thousandth ounce of gold is vastly higher than the present
value to me of a thousandth ounce of gold a thousand and one
years in the future. When offering to buy the land, I therefore
discount that expected income stream of gold by the longest-term
interest rate on the markct. So do all my potential competitors
{other buyers). he cash payment for the land will therefore be
substantially less than the expected rental payments of one thou-
sand ounces of gold.

This discounting process is called capitalization. When we capi-
talize something, we pay a cash price — an actual transaction or
an imputed estimation — for a future stream of income. Capitali-
zation stems from the fact, as Rothbard argues, that “Rents from
any durable good accrue at different points in time, at different
dates in the future. The capital value of any good then becomes

12, Murray N. Rothbard, Afar, Eumomy, and State (New York: New York Univer-
sity Press, [1962] 1979), ch. 7.
