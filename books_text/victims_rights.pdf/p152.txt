140 VICTIM'S RIGHTS

of the owner, The owner should have placed restraints on the
beast, or else he should have placcd warnings for bystanders.

Why shouldn’t bystanders recognize that the animal is dan-
gerous? Why are they considered judicially innocent? Don’t peo-
ple know that bulls charge people and gore them? They do know,
which is why the Hebrew usage, as in English, indicates that “ox”
in this case must refer to a castrated male bovine. ‘The castrated
beast is not normally aggressive. It is easier to bring under domin-
jon through training. In this sense, a castrated male bovine is
unnaturally subordinate.

As an aside, the question of unnatural subordination (lack of
male dominion) can also be raised with respect to the prohibition
against eunuchs worshipping in the congregation (Deut. 23:1).
Presumably, this was because cunuchs could not produce a fam-
ily, and to that extent they were cut off from the future. Rush-
doony writes (unfortunately using the present tense): “Because
eunuchs are without posterity, they have no interest in the future,
and hence no citizenship.”* This was true enough in ancient
Israel, where land tenure, bloodlines, political participation (eld-
ers in the gates), and the national covenant were intermixed. The
New Testament forever abolishcd this biological-gcographical
intermixture. Spiritual adoption‘ became forthrightly the founda-
tion of heavenly citizenship (Phil. 3:20), and therefore the only
basis of church membership. The baptism of the Ethiopian eunuch
by Philip the deacon (Acts 8)° indicates that the Old Testament
rule jost all meaning, once Jesus, the promised seed, had come
and completed His work,

‘The goring ox is alsv judicially guilty, He is therefore treated
as a responsible moral agent ~ not to the extent that a man is, of
course, but résponsible noncthcless, We train our domestic ani-
mals. We beat them and reward them. Modern scicntists call this

 

3. R. J. Ruskdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973}, p. 100.

4. John 1:12; Romans 8:15; Galatians 4:5; Kphesiaus 1:5.

5. That a deacon performed this baptism, as well as many others in Samaria,
creates a presently unsolved theological problem for all denominations that specify
elders as the only ordained church officers with a lawful call to baptize.
