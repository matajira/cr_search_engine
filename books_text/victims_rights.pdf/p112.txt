100 VICTIM’S RIGHTS

versial passages in the Old Testament. Prior to the 1960’s, when
the abortion issuc again began to be debated publicly in the
United States after half a century of relative silence,! only the
sccond half of this passage was controversial in Christian circles:
the judicial requirement of “an eye for an eye.” The abortion
aspect of the argument was not controversial, for the practice of
abortion was illegal and publicly invisible. A physician who per-
formed an abortion could be sent to jail.? It was clearly under-
stood by Christians that anyone who caused a premature birth
in which the baby died or was injured had committed a criminal
act, despite the fact that the person did not plan to cause the
infant’s injury or death. ‘The abortion described in the text is the
result of a man’s battle with another man, an illegitimate form of
private vengeance for which each man is made fully responsible
should injury ensue, either to each other (Ex. 21:18-19) or to
innocent bystanders. If this sort of “accidental” abortion is treated
as a criminal act, how much more a deliberate abortion by a
physician or other murderer! Only when pagan intellectuals in
the general culture came out in favor of abortion on demand did
pro-abortionists within the church begin to deny the relevancy of
the introductory section of the passage.

This anti-abortion attitude among Christians began to change
with the cscalation of the humanists’ pro-abortion rhetoric in the
early 1960's, Christian intellectuals have always taken their ideo-

1. Marvin Olasky, The Pres and Abortion, 1838-1988 (Hillsdale, New Jersey: Law-
rence Erlbaum Associates, 1988), ch. 6. ‘T'his book shows that in the late nineteenth
century, the battle aver abortion, as revealed in the press, was widespread.

2. Julius Hammer, the millionaire physician father of (later) billionaire Armand
Hammer, in 1920 was sent to Sing-Sing prison in Ossining, New York, for perform-
ing an abortion in 1919. The woman had died from the operation. Hammer was
convicted of manslaughter. (IF all women died after an abortion, there would be fewer
abortions performed.) Predictably, several physicians protested the law, bul lv no
avail. Armand Hammer, Hammer (New York: Putnam’s, 1987), pp. 74-82. Contrary
ta Hammer's recollections, the press was hostile to Julius Hammer. Sce Joseph
Finder, Red Carpet (New York: Holt, Rinehart & Winston, 1983), p. 18, (This book
wag reprinted in paperback by the American Bureau of Economic Research, Ft.
Worth, Texas, in 1987), Julius Hammer had been a member of the Socialist Labor
Party, a precursor of the American Communist Party. He became a millionaire by
trading in pharmaceuticals with the USSR. He actually served aa commercial attaché
for the USSR in the United States. Zbid., pp. 12-16.

  

 

 
