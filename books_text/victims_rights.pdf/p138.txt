126 VICTIM’S RIGHTS

he fears disfigurement. In any case, he places a high premium on
his eye. He bids 100 ounces of gold to retain it.

Once the victim receives an offer from the criminal, he may
change his mind about his commitment to sceing the criminal
disfigured, Perhaps he did not suspect that he could get this much
money ftom the criminal, Perhaps his wife has seen the wisdom
of taking the money. He may conclude that he would much prefer
100 ounces of gold to the joy he would receive in secing (with his
remaining cyc) his enemy brought low, After all, seeing his enemy
part with 100 ounces of gold is also seeing him brought low, and
the event brings other benefits, such as all the pleasures or secu-
rity the 100 ounces of gold can buy. So he accepts the counter-
offer. The criminal! keeps his eye.

In this case, the criminal is the high-money bidder. The
victim values the gold more than he values the criminal’s cye.
The criminal places more value on his eye than the gold. Each
man gcts what he most prefers. The criminal has bought the right
to determine what happens to his own body. He has bought the
right to avoid mutilation,

Consider the victim’s other possible choice. He is still out-
raged at what has befallen him, He wants the criminal to share
the same physical limitation. He is unwilling to acccpt the finan-
cial countcr-offer. Now, economically speaking, the criminal had
just placed 100 ounces of gold into the victim’s lap. He had been
willing to pay. The victim is not impressed, or not sufficiently
impressed, He figuratively hands the 100 ounces of gold back to
the criminal. “Keep your filthy moncy, you butcher! Keep your
only remaining cye on your moncy.” The victim has now matched
the moncy bid of the criminal. He has forfeited the 100 ounces of
gold that he might have r cd. He places a higher value on his
legal ability to blind the other man’s eye than he does on 100
ounces of gold. So the victim gets what he values most, the joy of
sccing the other man lose his eye. But he pays 100 ounces of gold
for this pleasure. The pleasure is biblically legitimate, but it is
expensive.

The ‘criminal’s 100 ounces of gold did not constitute a high
enough bid. The victim might have agrecd for more than the 100

 
