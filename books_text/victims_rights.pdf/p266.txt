254 VICTIM'S RIGHTS

derived sovereignty, men have defined as an ultimate, original
sovereignty. To combat this false interpretation, biblical law re-
strains the officers of the State by imposing strict limitations on
their enforcement of law. It is God’s law that must be enforced,
and this law establishes criteria of evidence and a standard of
justice. This standard is “an eye for an eye.” A popular slogan in
the modem world promotes a parallel juridical principle: “The
punishment should fit the crime.”

The Punishment Should Fit the Crime

Why should the punishment fit the crime? What ethical prin-
ciple leads Western people to believe that the Islamic judicial
practice of cutting off a pickpocket’s hand is too severe a punish-
ment? After all, this will make future pickpocketing by the man
far less likely. Why not cut off his other hand if he is caught-and
convicted again? People who have grown up in the West are
repelled by the realization that such punishments have been im-
posed in the past, and are still imposed in Muslim societies.2? Why
this repulsion? Because they are convinced that the punishment
exceeds the severity of the loss imposed on the victim by the thicf.

Proportional Restitution

The Bible teaches that the victim must have his goods re-
stored two-fold (Ex. 22:4,7), four-fold (for stealing a sheep), or
five-fold (for stealing an ox) (Ex. 22:1). ‘he seven-fold restitution
of Proverbs 6:31 appears to be a symbolic statement regarding the
comprehensive nature of restitution. The hungry thicf who is
destitute and who steals food must repay “all the substance of his
house,” meaning that what little he owns is forfeited when the
normal two-fold restitution payment is imposed. A rich man who

2. This is Islam’s Shari‘a law. It is officially the civil law in Mauritania, where
such amputations are still imposed: Roger Sawyer, Slavery in the Twentieth Century
(London: Routledge and Kegan Paul, 1986), p. 15. Shari’a was reimposed in Sudan
in 1988, Complains M. Ismail of Arlington, Virginia in a letter to the edilor: “As a
Sudanese, J feel that the’ previous legal code, which was an adoption of the British
secular code, was a colonial yoke that disfigured our national independence.” Hash-
ington Times (Oct. 3, 1988). Better to disfigure pickpockets than Sudan’s national
independence, Mr. Ismail is saying.
