178 VICTIM'S RIGHTS

public are not immediately perceptible. They may require auto-
mobile companies to install seat belts that buyers do not want to
pay for, and which occupants subscquently refuse to use, but
politicians are not about to pass a law that would impose fines
on families for refusing to install smoke detectors in their own
homes. The first piece of legislation would not gain the reprisal
of voters; the second probably would. Third, because of the rise
of State-financed health care, politicians can justify intrusions into
the lives of citizens on this basis: “Because taxpayers must pay for
injuries that are the result of carelessness, it is the responsibility
of the State to force people to be more careful.” A good example
of a compulsory personal safety law is the law requiring motorcy-
clists to wear crash helmets. In a free market social ordcr, if a
cyclist sustains head injuries in a one-man crash, he hurts only
himself, But because of the spread of socialized medicine, politi-
cians can justify helmet laws politically. This line of reasoning can
be used to pass almost any kind of safety Icgislation in the name
of reducing potential accidents. Safety laws become in principle
open-ended if their justification is the possible burden to taxpayers
that an injury might produce. The socialization of health care can
lead, step by step, to the socialization of all of life.

This is not to say that it is always wrong to require owners
to pay more in order to save lives, but the Bible provides us with
the proper guidelines, not some hypothetically universal utopian
principle that would neccssitate the creation of a messianic State.
The general principle is simple: those who own a known dangerous
object are legally responsible for making it safer for those who are either
immature or otherwise unwarned about the very real danger.

Conclusion

Ownership is a social function. There is a link between the
costs and benefits of lawful ownership. He who benefits from the
use of private property must also bear the costs of ownership. He
cannot legitimately pass on the costs to other people who have
not voluntarily agreed to accept these costs. He is also responsible
for the risks of physical damage that he imposes on them without
their prior knowledge and consent.

The pit-digger must cover the pit or be responsible for the
consequences. The owner of an unpenned notorious ox is equally
