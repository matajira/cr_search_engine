The Ransom for a Life 141

training “behavior modification.” In other words, we deal with
them on the assumption that they can learn, remember, and
discipline themselves. Anyone who has ever scen a dog that looks
guilty, which slinks around as if it has done something it knows
is wrong, can safely guess that the dog Aas done something wrong.
{t may take time to find out what, but the search must begin.
The dog knows.

An Ethically Unclean Beast

The goring ox is to be ircated as if it were an unclean beast.
It has become an cthically unclcan beast. Because of its ethical
uncleanness, it is still subject to this punishment in New Testa-
ment times, despite the New Testament’s abandonment. of the
category of physical-and ritual uncleanness. James Jordan com-
ments on the biblical meaning of unclean animals:

   

All unclean animals resemble the serpent in three ways. They eat “dirt”
(rotting caniion, manure, garbage). They move in contact with “dirt”
(crawling on their bellies, fleshy pads of their feet in touch with the
ground, na scales to keep their skin from contact wich their watery
environment). They revolt against human dominion, killing men or
other beasts. Under the symbolism of the Old Covenant, such Satanic
beasts represent the Satanic nations (Lev. 20:22-26), for animals are
“images” of men, To eat Satanic animals, under the Old Covenant, was
to “eat” the Satanic lifestyle, to “eat” death and rebellion.

The ox is a clean animal. The heifer and the pre-pubescent bullock
have sweet temperaments, and can be sacrificed for human sin, for their
gentle, non-violent dispositions reflect the character of Jesus Christ.
When the bullock enters puberty, however, his temperament changes
for the worse. He becomes orncry, testy, and sometimes downright
vicious. Many a man has lost his life to a goring bull. The change from
bullock to bull can be seen as analogous to the fall of man, at least potentially.
If the ox riscs up and’ gores a man, he becomes unclean, fallen. . . .

The unnaturainess of an animal's killing a man is only highlighted in
the case, of a clean, domesticated beast like the ox. Such an ox, by its
actions, becomes unclean, so that its flesh may not be eaten. . . -

 

The fact that the animal is stoned indicates that the purpose of the
law is not simply to rid the earth of a dangerous beast. Stoning in the
Bible is the normal means of capita] punishment for men. Its application
to the dnimal here shows that animals are to be held accountable to
