88 VICTIM'S RIGHTS

bloody games. He swore to himself that he would not even look,
but he did, briefly, and was trapped. “As he saw that blood, he
drank in savageness at the same time. He did not turn away, but
fixed his sight on it, and drank in madness without knowing it.
He took delight in that evil struggle, and he became drunk on
blood and pleasure. He was no longer the man who entered there,
but only one of the crowd that he had joined, and a true comrade
of those whe brought him there. What more shall I say? He
looked, he shouted, he took fire, he bore away with himself a
madness that should arouse him to return, not only with those
who had drawn him there, but even before them, and dragging
others along as well.”> Only later was his faith in Christ able to
break his addiction to the games.

In the city of Trier (Treves) in what is today Germany, alien
hordes burned the town in the early fifth century, murdering
people and leaving their bodies in piles. Salvian (the Presbyter)
records what took place immediately thereafter: “A few nobles
who survived destruction demanded circuses from the emperors
as the greatest relief for the destroyed city.”6 They wanted the
immediate reconstruction of the arena, not the town’s walls, so
powerful ‘was the hold of the bloody games on the minds of
Roman citizens.

Chaos Festivals

Roger Caillois, in his book, Man and the Sacred (1959), argues
that the chaos festivals of the ancient and primitive worlds served
as outlets for hostilities. These festivals are unfamiliar to most
modern citizens, or in the case of the familiar ones, such as Mardi
Gras in New Orleans, Carnival in the Caribbean, or New Year’s
Eve parties in many nations, they are not recognized for what
they are. He writes: “It is a time of excess. Reserves accumulated
over the course of several years are squandered. The holiest laws

5. The Confessions of St. Augustine, trans, by John K, Ryan (Garden City, New
York: Image Books, 1960}, Book 6, ch. 8.

6. Salvian, The Gavemance of God, in The Writings of Salvin, the Presbyter, Joremiah
F. O'Sullivan, trans, (New York: Cima Publishing Co., 1947), Bk. VI, Sect. 15, p.
178. Salvian was a contemporary of St. Augustine, in the fifth century, This was
probably written around A.D. 440.
