Protecting the Victims 195

the answer is in the negative. We are forced to conclude that the
distinguishing characteristic between a slaughtered stolen donkey
and a slaughtered stolen sheep has nothing to do with the com-
parative economic value of each beast’s output. Instead, it has a
great deal to do with the sheep's synbotic subordinate relationship to
the owner.

Of Sheep and Men

In the Bible, animals image man.* Sheep are specifically com-
pared to men throughout the Bible, with God as the Shepherd
and men as helpless dependents. The ‘Twenty-Third Psalm makes
use of the imagery of the shepherd and sheep. David, a shepherd,
compares himself to a sheep, for God is described as his shepherd
(Ps, 23:1). Christ called Himself the “good shepherd” who gives
His life for His sheep (John 10:11). He said to His disciples on
the night of His capture by the authorities, citing Zechariah 13:7,
“All ye shall be offended because of me this night: for it is written,
T will smite the shepherd, and the sheep of the flock shall be
scattered abroad” (Matt. 26:31). He referred to the Jews as “the
lost sheep of the house of Israel” (Matt. 10:6), echoing Jeremiah,
“Tsrael is a scattered sheep” (Jer. 50:17a) and Ezekiel, “And they
were scattered, because there is no shepherd: and they became
meat to all the beasis of the field, when they were scattered” (Ezk.
34:5). Christ spoke of children as sheep, and offered the analogy
of the man who loses one sheep out of a hundred. The man
searches diligently to locate that one lost sheep and rejoices if he
finds it. “Even so it is not the will of your Father which is in
heaven, that one of these little ones should perish” (Matt. 18:14).

It is thus the Aelplessness of sheep rather than their value as
beasts of burden or dominion that makes four-fold restitution
mandatory.? Shepherds regard sheep as their special responsibil-

2. Animals in men’s image: ibid., p. 122. He cites Prov. 6:6; 26:11; 30:15, 19,
24-31; Dan. 5:21; Ex. 13:2, 13, When I use the noun “image” as a verb, I am
reminded of one cynic’s remark: “There is no noun in the English language that
cannot be verbed.”

3, Maimonides ignored all this when he insisted that if a thief “butchers or sells
on the owner’s premises (au animal stolen there), he need not pay fourfold or fivefold.
But if he lifts the object up, he is liable for theft even before he removes it from the

 
 

 
