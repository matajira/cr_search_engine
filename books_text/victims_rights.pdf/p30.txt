18 VICTIM'S RIGIITS

of witness. The witness is authorized to bring relevant information
to one of these covenantal judges, so that the judge can initiate
the covenant lawsuit against the suspected violator.t The witness
plays a very important role in the prosecution of God’s covenant
lawsuits. Without at least two witnesses, it is illegal to execute
anyone (Deut. 17:6). Aiso, the affirming witnesscs in a capital
lawsuit must be the first people to cast stones (Deut. 17:7).

The Biblical Hierarchical Structure

Adam was allowed to do anything he wanted in the garden,
except cat from the forbidden tree. There was a specific sanction
attached to that crime, a capital sanction. This reveals a funda-
mental hiblical judicial principle: anything is permitted unless it is
explicitly prohibited by law, or prohibited by an extension of a case law's
principle. This principle places the individual under public law,
but it also relies on self-government as the primary policing de-
vice. It creates the bottom-up appeals court character of biblical
society. Men are judicially free to act however they please unless
socicty, through its various covenantal courts, has been author-
ized by God's Bible-revealed law to restrict certain specified kinds
of behavior.

The bottom-up appeals court structure of the biblical hierar-
chy is in opposition to the principle of top-down bureaucratic
control. Under the latter hierarchical system, in theory nothing
is permitted except what has been commanded. The decision-
making private individual is tightly restricted; the centralized State
is expanded. This is the governing principle of all socialist eco-
nomic planning, It assumes the ommiscience and omnicompetence
of distant central planners.°

 

 

 

4. The hostility of siblings against “tattletales” in a family is easily explainable:
youthful Jaw-breakers resent judgment. They resent witnesses whose action brings
the dreaded sanctions. But what about parents? Parents who side with the etitics of
“tattletales” are thereby attempting to escape their God-given role as judges, They
are saying, in principle, “We don’t want to know about it. We don’t want to serve
as judges, despite our position as God’s designated representative agents in this
family.”

5. Gary North, Mara’s Religion of Revolution: Regeneration Through Chaos (Tyler,
‘Texas: Institute for Christian Economics, [1968] 1989), Appendix A: “Socialist
Economic Calculation.”
