222 VIGEIM’S RIGHTS

criminal law.

The Bible encourages the legitimate division of labor in identify-
ing all types of criminal behavior, including such acts of injustice
as breaking contracts or polluting the environment. The Bible
recognizes that the State is not God. It is not omniscient. The
initiation of public sanctions against all criminal acts therefore
must not become a monopoly of civil officers. Citizen's arrest and
torts — where one person sucs another in order to collect dam-
ages ~arc modern examples of the outworking of this biblical
principle of the decentralization of law enforcement. All govern-
ment begins with self-government. The bottom-up, appeals court
structure of covenant society (Ex. 18) is protected by not requiring
that agents of the civil government initiate all civil government’s
sanctions against criminal behavior. Nevertheless, all disputes
into which the State can legitimately intervene and settle by judi-
cial decision must be regarded in a biblical commonwealth as,
criminal behavior, There is no biblical distinction between crimi-
nal law and civil law.

{t is therefore preposterous to argue, as liberal scholar Anthony
Phillips argues concerning the Mosaic law, that “A crime is
a breach of an obligation imposed by the law which is felt to
endanger the community, and which results in the punishment
of the offender in the name of the community, but which is not
the personal concern of the individual who may have suffered
injury, and who has no power to stop the prosecution, nor derives
any gain from it.”? It is preposterous because every transgression
of the civil Jaw that goes unpunished by the authorities raises the
threat of God’s judgment on the community, which is why un-
solved murders required expiation in the Old Testament: 1) the
sacrifice of a heifer (Deut. 21:1-7); and 2) the elders were required
to pray, “Be merciful, O Loxp, unto thy pcople Israel, whom thou
hast redeemed, and lay not innocent blood unto thy people of
Israel’s charge. And the blood shall be forgiven them” (Deut.
21:8). The State must regard as crimes against God all public

3. Anthony Phillips, Ancient Israel's Criminal Law: A New Approach to the Decalogue
(New York: Schocken, 1970), p. 10.
