252 VICTIM'S RIGHTS

assessing the costs and benefits of their actions. Therc is no escape
from this economic responsibility. “No decision” is still a decision.
Tf an asset is squandered, the owner loses.

The chief failure of what is commonly referred to as collective
ownership is that no individual can be sure that his assessment
of the costs and benefits of a particular use of any asset is the
same assessment that those whom he represents would make. The
tendency is for individuals who are legally empowered to make
these representative decisions to decide in terms of what is best
for them as individuals. There is also a tendency for the decision-
maker to make mistakes, since hc cannot know the minds and
desires of the community as a whole.

The common property tends to be wasted unless restraints
on its use are imposed by the civil government. The “positive
feedback” signals of high profits for the users are not offset by
equally constraining “negative feedback” signals. Users of a scarce
economic resource benefit highly as immediate users, yet they
bear few costs as diluted-responsibility collective owncrs. Thus,
in order to “save the property from exploitation,” the civil govern-
ment steps in and regulates users. This Icads to political conflicts.

The biblical solution to this problem is to establish clear
ownership rights (legal immunities) for property. The individual
assesses costs and benefits in terms of his scale of values. He
represents the consumer as an economic agent only because he has
exclusive use of the property as legal agent. He produces profits or
losses with these assets in terms of his abilities as an economic
steward. The market tells him whether he is an effective agent of
the competing consumers.

The legal system simultaneously assigns responsibility for the
administration of these privately owned assets to the legal owners.
It becomes the owners’ legal responsibility to avoid damaging
their neighbors through the use of their privately held property.
