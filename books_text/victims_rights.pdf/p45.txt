The Covenant Lawsuit 33

below: “The Victim’s Decision, [pp. 46-48].”) The victim’s deci-
sion is final until God intcrvencs dircctly — sickness, calamity,
death, or at His Second Goming—to bring His own covenant
lawsuit. Thus, the witnesses in John 8 were violating yet another
principle of the Mosaic law. The whole incident was one of utter
lawlessness and rebellion, which is the characteristic feature of
every challenge to the God-given authority of Jesus Christ.

 

Conclusion

Furidamental to the concept of biblical jurisprudence is the
idea of the covenant lawsuit. Ultimately, God brings this lawsuit
against all mankind. He’ brings it against each person for each
sinful act. Only the substitute sacrifice by Jesus Christ at the cross
allows God to overlook these sins in individuals: eternally for His
covenant people, and in history for covenant-breakers.

The victim initiates God’s covenant lawsuit against the person
who injured him. He acts as God's authorized agent. The goal of
the victim is restitution in history: to the victim, but also represen-
tatively to God.

The victim has the legal authority not to press charges. He is
allowed to show mercy in history to the criminal. Jesus did this
at Calvary. The civil government has no independent authority
to bring this lawsuit unless it can show that the victim is incompe-
tent or unable to make this decision (a minor, a feeble-minded
person, or deceased). One other exception: if the State can prove
that the criminal is threatening the victim, thereby making the
victim into an accomplice to thwart God’s civil justice. Otherwise,
there should be no restrictions against settling out of court.

 
