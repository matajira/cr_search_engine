168 VICTIM’S RIGHTS

ownership, biblical civil law identifies the legal owner of the pit,
namely, the person who is required to pay damages should an-
other person’s animal be killed by a fall into the unsafe pit. He
receives some sort of advantage from the pit, and therefore he
must bear the expense of making it safe for other people’s animals.

“Pit” is a classification used for centuries by the rabbis to
assess responsibility and damages. The Mishnah specified that
any pit ten handbreadths deep qualifies as deep enough to cause
death, and therefore is actionable in cases of death. If less than
this depth, the pit is actionable in case of injury to a beast, but
not if the beast died.! Writes Jewish legal scholar Shalom Albeck:
“This is the name given to another leading category of tort and
cavers cases where an obstacle is created by a person’s negligence
and left as a hazard by means of which another is injured. The
prime example is that of a person who digs a pit, leaves it uncoy-
ered, and another person or an animal falls into it. Other major
examples would be leaving stones or water unfenced and thus
potentially hazardous. The common factor is the commission or
omission of something which brings about a dangerous situation
and the foreseeability of damage resulting. A person who fails to
take adequate precautions to render harmless a hazard under his
control is considered negligent, since he is presumed able to fore-
see that damage may result, and he is therefore liable for any such
subsequent damage.”?

Samson Raphael Hirsch, the brilliant mid-nincteenth-century
Jewish Torah commentator, analyzed the economics of negligence
under the general heading of property, and property under the
more general classification of guardianship. “Man, in taking pos-
session of the unreasoning world, becomes guardian of unreason-
ing property and is responsible for the forces inherent in it, just
as he is responsible for the forces of his own body; fer property is

3. Raba Kama 5:5, The Mishnak, edited by Herbert Danby (New York: Oxford
University Press, [193] 1987), p. 338.
2. “Avot Nezikin: (2) Pil,” The Principles of Jewish Law, edited by Menachem
Elon (Jerusalem: Keter, 19752), col. 326. This compilation of articles taken fom the
inyclopedia Jadaica was published as Publication No, 6 of the Institute for Research
in Jewish Law of the Hebrew University of Jerusalem.
