Restoring Full Value 217

itly and possibly explicitly pretended to be transferring an asset
that he did not possess: lawful title. The thief did not possess lawful
title to the property. This illuminates a fundamental principle of
biblical ownership: whatever someone does not legally own, he cannot
legally sell. Owncrship is not simply possession of a thing; it is
possession of certain legal immunities associated with the thing. It
involves above all the right to exclude. Writes economist-legal theo-
rist Richard Posner: “A property right, in both law and econom-
ics, is a right to exclude everyone else from the use of some scarce
resource.” This right to exclude was never owned by the thief
therefore, he cannot transfer this bundle of legal immunities to the
purchaser. The purchaser can legally demand compensation from
the thief, but he does not lawfully own the stolen item. The civil
authorities can legitimately compel the buyer to transfer‘the prop-
erty back to the thief, who then returns it to the original owner,
or else compel him to return it directly to the original owner.

The explicit language of the kidnapping statute provides us
with the legal foundation. of this conclusion regarding the transfer
of ownership. “And he that stealeth a man, and selleth him, or if
he be found in his hand, he shall surely be put to death” (Ex.
21:16). Even to have a stolen man in your possession was a capital
crime, unless you could prove that you did not know that he or
she was stolen. Just because a kidnapper sold you a stolen person
as a slave did not mean that this person would remain in your
possession as a slave. The same is true of other property.

English common law does not recognize this biblical stan-
dard. Receiving stolen goods was not made a crime by statute
until the nineteenth century. Common law had recognized no
such crime; it took statute law to make it a crime.”’ While it is
no doubt true that it is expensive to research every title before
making a purchase, especially in a pre-modern society, the re-
sponsibility to do so is biblically inescapable if the buyer wishes

26. Richard A. Posner, The Economics of’ Justice (Cambridge, Massachusetts: Har-
vard University Press, 1983), p. 70.

27, Wayne LaFave and Austin Scott, Jr, Handbook on Griminal Lae (Minneapolis,
Minnesota: West, 1972), pp. 681-91: “Receiving Stolen Property.” My thanks to
Prof, Gary Amos for this reference.
