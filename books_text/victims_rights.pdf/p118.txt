106 VIGTIM’S RIGHTS

who have therelore confidently voted against abortion as the true
barbarism and for biblical law as the sole long-term foundation
of Christian civilization. But most Christians have self-consciously
suppressed any temptation to think about this dilemma, one way
or the other. The thin picket lines in front of abortion clinics testify
to the thoughtlessness of Christians in our day. (So do the thin
shelves of the Christian bookstores.)!®

Restitution and Vengeance

The “eye for an eye” principle is known by the Latin phrase,
lex talionis, or “\aw of retaliation.” The English word, “retaliate,”
is derived from the same Roman root as “talionis.” Today, “retali-
atc” means to inflict injury, but earlier English usage conveyed a
broader meaning: 0 pay back or return in kind, including good will.
According to one source, the éex talionis was a Roman law that
specified that anyone who brought an accusation against another
citizen but could not prove his case in the courts would suffer the
same penalty that he had sought to inflict on the defendant.?!
(This was a perverted version of the biblical principle of the law
governing deliberate perjury, found in Deuteronomy 19:16-21,
which concludes with a restatement of the “eye for eye” require-
ment in verse 21. The law reads: “Then shall ye do unto him [the
false witness], as he had thought to have done unto his brother:
so shalt thou put the evil away from among you” [v. 19].2? Only

19, James Jordan’s book, The Law of the Covenant: An Exposition of Exodus 21-23
(Tyler, Texas: Institute for Christian Economics, 1984), was removed from the
shelves of a local Christian bookstore in Tyler when the store’s owner discovered
that Jordan had called for the execution of the aborting physician and the mother.
The owner dared nol take the heat for selling a book which announced: “Until the
anti-abortion movement in America is willing to return to God’s law and advocate
the death penalty for abortion, God will not bless the movement. God does not bless
those who despise His law, just because pictures of salted infants make them sick”
{p. 135).

20, Soe the Oxford English Dictionary: “retaliate.”

21. Cyelopacdia of Biblical, Thevlowical, and Exclesiustical Literature, edited by John
McClintock and James Strong (New York: Harper & Bros., 1894), vol. X, p. 165:
“'Talionis, Lex.”

 

22. The same rule applied in Hammurabi's Code: “If a seignior came forward
with false testimony in a case, and has not proved the word which he spoke, if that
case was a case involving life, that seignior shall be put to death, If he came forward
