236 VICTIM'S RIGHTS

sinner in history attacks various aspects of the creation in his
attempt to defy God, since God cannot be attacked directly. He
violates earthly boundaries in his rebellion against God. For cx-
ample, Adam and Eve could not attack God directly; instead,
they violated the boundary that God had placed around the for-
bidden tree.

This leads us to a significant conclusion: the very existence of an
earthly victim calls God's heavenly court of justice into session. If the
existence of a boundary violation becomes known to the victim,
this discovery automatically invokes an earthly civil court of jus-
tive.4 This invocation may not be a formal public act, but God,
as the sovereign King of the commonwealth, calls it into session
historically. When the victim learns of the violation, hc is sup-
posed to begin a search for incriminating evidence. Crimes are
not to go unpunished in God’s social order, for they arc inhcrently
attacks on Him. Crimes are to be solved in history if the costs of
conviction are not prohibitive, i.e., if too many resources are not
drained from the victim or the court in solving a particular crime.
The world is under a curse: the curse of scarcity (Gen. 3:17-19).
There are limits to anti-crime budgets. In a world of scarcity,
including scarcity of accurate knowledge, there cannot be perfect
justice. Justice in history is purchased at a price.' If the victim
thinks it will take too many of his own resources to identify and
convict the criminal, or if he thinks his accusation could be turned
against him later for lack of evidence gathered by the court, he
has the option of refusing to pursue the matter. He can let God
settle it in eternity. He can rest confident in God’s perfect justice.
Rushdoony said it well: “History culminates in Christ's triumph,
and ctemity settles all scores.”®

 

 

4, Tf someone other than the victim first di
the victim or the person most likely to be the victim. To fail to do this is judicially
to become an accomplice of the criminal,

 

rs the vielation, he is io inform:

5. The ultimate price of perfeot justice was paid by Jesus Ghrist’s act of compre-
hensive redemption at Calvary. Without this representative payment, God’s perfect
justice would have demanded the end of the Adamic race at the conclusion of
‘Adams trial.

6 R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 123.
