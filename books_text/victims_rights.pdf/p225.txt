Restoring Full Value 213

trial must be borne. ‘To encourage the criminal to tell the truth,
there has to be a threat hanging over him: the possibility that
someone with the missing information wil] come to the judges and
present it. ‘Uhus, if the thief remains silent about the person who
bought the shcep or ox, he bears greater risk.

The Silent Thief

A silent thief faces an additional threat. Assume that the
original owner demands four-fold (sheep) or five-fold (ox) restitu-
tion. Still, the thief says nothing because he knows that if he
admits that he sold the beast, he will also have to pay the victim-
ized buyer 120 percent, yet the original owner may nevertheless
refuse to deal with him, and may demiand (as is his legal right)
either four-fold or five-fold restitution. Once the thief has sold a
stolen sheep or ox, the victim can legally demand the higher
penalty payment. The victim is owed the four-fold or five-fold
restitution whether or not the thief locates the stolen beast, buys
it back, and returns it to its original owner. The very act of selling a
stolen ox or sheep invokes the law's full penalty, It is very much like the
crime of kidnapping; the family of the kidnapped victim or the
judge or the jury can legally insist on the death penalty even if the
kidnapper offers to identify the person to whom the victim had
been sold into bondage.

Why would the thief remain silent about the whercabouts of
the stolen animal? One reason might be his fear of revenge from
an accomplice in the crime. Laying this motivation aside, let us
consider other possible motivations for the thief’s remaining silent.
First and foremost, the thief may believe that he will not be
convicted of the crime. After all, the beast is missing. It is not in
the thief’s possession. Second, he may believe that the victim is
hard-hearted and will insist on the maximum restitution payment
even if the thief can get the beast back by identifying the de-
frauded buyer and paying him the purchase price plus a penalty
payment of 20 percent.

He remains silent. He may be convicted anyway. If so, he
now faces a new problem: he not only owes four-fold or five-fold
restitution to the victim, he could also wind up owing the victim-
