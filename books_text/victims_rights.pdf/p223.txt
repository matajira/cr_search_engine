Restoring Full Value 21h
Vietin’s Rights

“Ifa man shall steal an ox, or a sheep, and kill it, or sell it;
he shall restore five oxen for an ox, and four sheep for a sheep”
(Ex. 22:1). What if a stolen sheep or ox had been sold by the
thief? The thief may know where the animal is. If the authorities
convict him of the crime, would he be given an opportunity to
buy back the stolen animal and return it to the owner, plus the
100 percent penalty, and thereby avoid the four-fold or five-fold
restitution penalty? This would seem to violate the third goal of
proportional restitution (see pp. 230-31): increasing the risk for
thicves who steal shecp or oxen, and who then dispose of the
evidence by destroying them or selling them, thereby making it
more difficult to convict them in court. The thicf would still have
to pay the four-fold or five-fold penalty, unless the victim decides
otherwise. The fundamental judicial principle here is victim’s rights.
The victim decides the penalty, up to the limits of the law.

‘Lhe victimized original owner should always have the author-
ity to offer the convicted criminal an alternative which is morc to
the victim’s liking. Perhaps he is emotionally atlached to the
missing ox, especially if he personally traincd it. He may even be
attached emotionally to the stolen sheep — less likely, T suspect,
than attachment to an ox that he had personally trained. What if
he offers to accept double restitution if 1) the criminal will tell him.
where the sold beast is, and 2) the beast is returned to him alive?
What if the thief then tells the victim’ and the civil authorities
where the missing beast is? The authorities would then compel
the new owner — who, legally speaking, is not truly an owner, as
we shall sce — to return the animal to the original owner.

The buyer of the stolen beast now has neither beast nor the
forfeited purchasc price. He has become the thief’s victim. The
thief therefore owes him some sort of restitution payment, The
question is: How much? This is a difficult question to answer. It
would be either a 20 percent penalty or a 100 percent penalty. T
believe that it is a 20 percent penalty.

Timely Confession Receives Its Appropriate Reward
Here is my reasoning. Say that the convicted thief confesses
