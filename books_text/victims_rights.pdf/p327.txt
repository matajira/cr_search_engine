salvation, 283
satictions, 57
slave monopoly, 127
subpoena, 23n
trustee, 176
unlimited power, 57
vengeance, 108, 111, 253
victim, 60, 70
stay of execution, 25
stewardship, 242
stolen goods, 216-18
stoning, 105n, 142, 143-47, 273
subpoena, 23n, 37
swimmers, 245
swimming pool, 171
Switzerland, 121
Sutton, Ray, 26, 27
syrnbolism, 199-200, 230

table tennis, 12-13
Tamar, 38-39

tattle tales, 18n

Ten Commandments, 223
Texas, ix

theft, 167

theocracy, 241

thief (silent), 213-15
thumb, lidn, 116
time, 76-77

tide, 216-18

torture, 5, 7, 21, 69n
toxic waste, 159
treason, 144

trust funds, 175-76
‘Tyndale, William, +
tyranny, 58

uncertainty, 208-8, 246
unclean beast, 141-42

value, 207

vengeance, 104, 108-9
legitimate, 131
price tag, 122
public, 110

General Index 315

victim’s, 122
victim,
covenant lawsuit, 16, 186, 237
court’s agent, 235
estimating sanctions, 129-30
forgiveness, 36, 37
God, 16
God’s agent, 36
God’s representative, 16-17, 122, 186
husband, 42-45
innocent buyer, 211
Jesus Christ, 35
mercy by, 20
prosecutor, 38
protection, 48-49
pursues justice, 236-37
restitution, 109
wife, 45-46
victimless crimes, 40, 50, 177
victims
animals, 193-200
“soft on,” 6
witnesses?, 36
victim’s rights
abortion, 102
civil court, 237
divorce, 27-28
forgiveness, 34-35
maximum sanctions, 57
mercy, 59
plea bargaining, 219
protection of innocent, 46
rebellious son, 22
sanctions, 47
specifies penalty, 211
specifies sanctions, 122
vigilantes, 93-95
violence, 66, 85, 98

war, 81
Wenham, Gordon, 234
West
future-oriented, 114-15
impersonalism, 275
