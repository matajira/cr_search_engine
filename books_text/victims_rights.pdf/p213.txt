Protecting the Victims 201

B. Owners

The penalty paid to the victim by the criminal compensates him
Jor his trouble, while it simultancously scrves as a deterrent to future
criminal behavior, Biblical restitution achieves both goals —
compensation of the victim and deterrence of criminal behavior
—by means of a single judicial penalty: restitution. Jn contrast,
modern humanistic jurisprudence has until quite recently ignored
the needs of the victim by ignoring restitution,

This two-fold purpose of criminal law was ignored by modern
American jurisprudence until the 1960’s, when the subject of
restitution to victims at last became a topic of discussion among
legislators and law enforcement authorities.” The Department of
Corrections of Minneapolis, Minnesota, began a “restitution and
release” experimental program in 1974, the Minnesota Restitu-
tion Genter, in which criminals involved in crimes against other
people’s property compensate the victims. Only 28 men were
admitted to the experiment during its first year. Violent criminals
were not accepted.!!. By 1978, 24 of the 50 states in the United
States had adopted some form of compensation to victims of
violent crimes.

This policy had begun in the mid-1960’s in Great Britain. In
the United States, the first state to introduce such a program was
California, in 1965. Such costs as legal fees, money lost as a result
of the injured person’s absence from work, and medical expenscs

 

returns to its vomit: the “later addition” thesis. He contrasts the ewo-fold restitution
requirement with the four-fold’and five-fold requirements. The higher penalties are
evidence of an earlier law. . the older rule makes a rather primitive distinction
between thefl of an ox and theft of a shcep: for one ox you have to give five, but for
one sheep only four. No such distinction occurs in the later rule. Whatever kind of
animal you steal, you have to restore two for one.” Daube, Studies in Biblical Law
(Cambridge: At the University Press, 1947), pp. 94-95. He uses a similar line of
argumentation to distinguish Exodus 21:28-31 from 21:35-36: ibid, pp. 86-87.

10. Gf Burt Galaway and Joe Hudson (eds.), Offender Restitution in Theory and
Action (Lexington, Massachusetts: Lexington Books, 1977); Randy E. Barnett and
John Hegel ITI (eds.), Assessing the Criminal: Restitution, Retribution, and the Legal Process
(Cambridge, Massachusetts: Ballinger, 1977). See the special feature, “Crime and
the Victim,” in Trial (May/June 1972), the national legal news magazine of che
American ‘Lrial Lawyers Association.

V1, Los Angeles Times (April 21, 1974).

 

 
