Personal Responsibility and Personal Liberty 945

priest, Jesus Christ.

Fences Reduce Conflicts

The Bible affirms that those who violate fences or property
lines must make full restitution to the economically injured neigh-
bor. The assessment of harm is casicr to make than under com-
mon ownership. “His cows ate this row of corn in my cornfield.”
The owner of the damage-producing animals is responsible. Re-
sponsibility and qwnership are directly linked under a system of
private property rights. Under a system of private ownership,
property lines are in effect cost-cutting devices, for they serve as cost-
assessing devices. Without clearly defined property rights for men,
and therefore clearly defined responsibilities, the rights of “prop-
erty” — God’s living creatures and a created environment under
man’s dominion (Gen. 9:1-17)*— will be sacrificed.

Carefully defined property rights also help to reduce social
conflicts. Dales writes: “Unrestricted common property rights are
bound to lead to all sorts of social, political, and economic friction,
especially as population pressure increases, because, in the nature
of the case, individuals have no legal rights with respect to the
property when its government owner follows a policy of ‘anything
goes.’ Notice, too, that such a policy, though apparently neutral
as between conflicting interests, in fact always favours one party
against the other. Technologically, swimmers cannot harm the
polluters, but the polluters can harm the swimmers; when prop-
erty rights are undefined, those who wish to use the property in
ways that deteriorate it will inevitably triumph every time over
those who wish to use it in ways that do not deteriorate it.”5
Common ownership of large bodies of water, when coupled with
an opportunity to pass on private costs of polluted production,
increases the extent of water pollution. It is a bad system for the
swimmers of this world.

In questions of legal responsibility, there can be no neutrality.

4. Gary North, The Dominion Covenant: Genesis (Qnd ed.; Tyler, ‘Texas: Institute
for Christian Economics, 1987), ch. 14: “The Ecological Covenant.”

5, J. H. Dales, Pollution, Property, and Prices: An Essay in Poliey-Making and Economics
(Toronto: University of Toronto Press, 1968), p. 67.
