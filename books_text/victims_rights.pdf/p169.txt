The Principle of Limited Liability 157

available only at the end of history. At that point, it will not only
be available, it will be inescapable.

This passage therefore has implications for the concept so
popular in modern economies, that of limited liability. The modern
corporation is protected by limited liability laws. In case of its
bankruptcy, creditors cannot collect anything from the owners of
the corporation’s shares of ownership. The corporation is liable
only to the extent of its separate, corporate asscts.

Legitimate Limitations -

Certain kinds of economic transactions that limit the liability
of either party, should one of them go bankrupt, are valid. For
example, a bank that makes a loan to a church to construct a
building cannot collect payment from. individual members, should
the church be unable to meet its financial obligations. It can
repossess the building, of course, something that few banks relish
doing. It is bad publicity, and a church building is a kind of white
elephant in the real estate world: only churches buy them, and
almost all of them are short of funds. This is why bankers prefer
to avoid making loans to churches, other things being even re-
motely equal.”

‘The same sorts of limited liability arrangements ought to be
legally valid for other kinds of associations, including profit-

 

See Mary Douglas and Aaron Wildavsky, Risk and Culture: An Essay on the Selection of
Technological ond Environmental Dangers (Berkeley: University of California Press, 1982).
2. A wise banker would recommend to the church’s officers that church mem-
bers refinance their homes or assume debt using other forms of collateral, and then
donate the borrowed money to the church. This ties the loans to personal collateral
that_a banker can repossess without appearing to be heartless. It- makes church
members personally responsible for repayment. (Co-signed notes are also acceptable
fram the banker’s point of view, but questionable biblically: e.g., prohibitions against
“surety.”) Members cannot escape their former financial promises by walking away
from the church, It also keeps he church out of debt.as an institution, whieh is godly
testimony concerning the evil of debt (Rom. 13: Since a loan is not taxable to the
recipient ‘as income (in U.S. tax laws), and since repayments on interest for home
loans are tax-deductible, and since donations (o a church are tax-deductible, the
borrowers receive (ax advantages through this arrangement, The interest payments
would not constitute a tax advantage if the church borrowed the money, since income
to churches is not normally taxable, This approach is illegal in the state of Texas,
however; it is illegal to refinance your home in Texas, except (6 make home improve-
ments —a very stupid law that is left over from the older “populist” mentality.

 

  

 
