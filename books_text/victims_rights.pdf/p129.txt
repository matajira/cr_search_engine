The Ransom for an Eye uy

Jewish Commentaries

Traditional: Jewish explanations of the Jex talionis principle
point to a payment in lieu of physical mutilation. Nachmanides
wrote in the thirteenth century concerning “eye ‘tachath’ (for)

eye”:

It is known in the tradition of our rabbis that this means monetary
compensation. Such a usage [of the term tachaih to indicate] monetary
compensation is found in the verse: And he that smiteth a beast mortally shalt
(pay for it; life ‘tacheth’ life [Lev. 24:18], [in which case lacheth surely
indicates monetary compensation]. Rabbi Abraham ibn Ezra commented
that Scripture uses such a term to indicate that he really is deserving of
such a punishment, [that his eye be taken from him], if he does not give
his ransom. For Scripture has forbidden us to take ransom for the life of a
musrderer, that is guilty of death [Num. 35:31], but wc may take ransom from
a wicked person who cut off any of the limbs of another person. There-
fore we are never to cut off that limb from him, but rather he is to pay
monetary compensation, and if he has no money to pay, it lies as a debt
on him until he acquires the means to pay, and then he is redeemed.?

Nachmanides’ citation of Abraham ibn Ezra indicates that he was
disturbed by the. literal wording of the “eye for eyc™ stipulation.
By refusing to call for a literal application of the verse in the case
of a poor criminal, and also by their refusal to call for indentured
servitude as a way to repay the debt, these two Jewish medieval
commentators softened the threat of the punishment.

There are difficulties with this interpretation. It is ingenious,
but it has no explicit biblical precedent, and it may therefore be
incorrcct, even though it appears to conform to the implicit mean-
ing of “cyc for cye.” It involves speculation that relies heavily on
the precedent of cconomic restitution in the case of the ox that
gores someone to death (Ex. 21:30)—a separate case law that
may not apply to the dex falionis law of Exodus 21:24-25. But this
view became common in the interprétation of Jewish law. Rabbi
Samson. Raphael Hirsch commented on Exodus 21:25 in the early
nineteenth century: “. . . the taking of this legal canon literally,

7. Rabbi Moses ben Nachman [Ramban], Commentary on the Torah: Exodus (New
York: Shiloh, [12672] 1973), p. 368.
