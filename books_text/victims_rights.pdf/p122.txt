110 VICTIM'S RIGHTS

prosperity is clearly taught in the Bible (Deut. 28:1-14). So is the
relationship between covenant-breaking and calamity (Deut. 28:15-
68). This system of sanctions applies to the whole world, not just
in Old Testament Israel. Deny this, and you have also denicd the
possibility of an explicitly and exclusively Christian social theory,
Christians who deny the continuing relevance of Deuteronomy
28's sanctions in post-Calvary, pre-Second Coming history should
be warned by David’s admission that he had been foolish to doubt
these relationships. The concept of slippery places is not often dis-
cussed, but it is very important. God scts people high in order to
make them slide, visibly, before the world. God said to Pharaoh:
“For now I will stretch out my hand, that I may smite thee and
thy people with pestilence; and thou shalt be cut off from the
earth. And in very deed for this cause have I raised thee up, for
to show in thee my power; and that my name may be declared
throughout all the earth” (Ex. 9:15-16). The temporary prosperity
of the wicked must not be viewed as evidence that would call into
question the long-term relationship between covenant-breaking
and destruction.

Conclusion

Vengeance is legitimate, but not as a private act. Tt is always
to be covenantal, governed by God’s institutional monopoly, civil
government. James Fitgjames Stephen said it best: “The criminal
law stands to the passion of revenge in much the same relation
as marriage to the sexual appetite.”*! The private vendetta is
always illegitimate; public vengean ometimes legitimate. ‘There
are many examples of private vengeance not sanctioned by God:
gangster wars, clan feuds, the murder of those who testify against
a criminal or syndicate, and murders for breaking the code of
silence of a sccret society. It is a crime against God Himself to
take any oath that testifies to the right of any private organization
or voluntary socicty to inflict physical violence, especially death,
for brcaking the oath or any other violation of the “code,” even if

 

31. James Fitzjames Stephen, A History of the Criminal Law in England (London:
Macmillan, 1863), TT, p. 80. Cited by Emest van den Haag, Punishing Criminals:
Concerning a Very Old and Painful Quastion (New York: Basic Books, 1975), p. 12.
