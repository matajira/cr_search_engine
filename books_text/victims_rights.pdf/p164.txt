152 VICTIM'S RIGHTS

be an application of the principle of Deuteronomy 24:16: “The
fathers shall not be put to death for the children, ncither shall, the
children be put to death for the fathers: every man shall be put
to death for his own sin.”

Criminal Negligence

We know from the text that the ox’s owner had been warned
about the dangerous ox, yet he did nothing visibly to restrain it.
Why would an owner neglect a warning from someone else re-
garding the threat of his ox to others? There are scveral possible
reasons. First, he may not trust the judgment of the person bring-
ing the warning. The beast may behave quite well in the owner’s
presence. Is he to trust the judgment of a stranger, and not trust
his own personal experience? But once the warning is delivered,
he is in jeopardy. If the beast injures someone, and the informant
announces publicly that he had warned the owner, the owner
becomes legally liable for the victim’s suffering.

The owner may be a procrastinator. He fully intended to
place restraints on the ox, but he just never got around to it. This
does not absolve him from full personal liability, but it does
explain why he failed to take effective action.

Another reason for not restraining the ox is economics. It
takes extra care and cost to keep an unruly beast under control.
For example, over and over in colonial America, the town records
reveal that owners of pigs, sheep, and cattle had disobeyed previ-
ous legislation requiring them to pen the beasts in or put rings in
their noses. Apparently, the authorities were unable to gain com-
pliance, for this complaint was continual and widespread through-
out the seventeenth century.2> The costs of supervising the ani-
mals or maintaining fences in good repair were just too high in
the opinion of countless owners. Even putting a ring in the beasts’

25, Because a scrious penalty could be imposed on the liable owner, the informant
would have to have proof that he had, in fact, actually warned the owner of the
beast’s prior risconduct. Otherwise, the perjured testimony of one man could ruin
the owner of a previously safe beast which then injured someone,

26, Carl Bridenbaugh, Cities in the Wilderness: ‘The First Contury of Urban Life in
America, 1625-1742 (New York: Capricorn, [1938] 1964), pp. 19, 167, 323.
