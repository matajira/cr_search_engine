Other books by Gary North

Marx’s Religion of Revolution, 1968 [1989]

An Introduction te Christian Economics, 1973
Unconditional Surrender, 1981

Successful Investing in an Age of Envy, 1981

The Dominion Covenant: Genesis, \982

Government By Emergency, 1983

The Last Train Out, 1983

Backward, Christian Soldiers?, 1984

75 Bible Questions Your Instructars Pray You Won't Ask, 1984
Coined Freedom: Gold in the Age of the Bureaucrats, 1984
Moses and Pharaoh: Dominion Religion Versus Power Religion, 1985
Negatrends, 1985

The Sinai Strategy, 1986

Conspiracy: A Biblical View, 1986

Unholy Spirits: Occultism and New Age Humanism, 1986
Honest Money, 1986

Fighting Chance, 1986 [with Arthur Robinson]
Dominion and Common Grace, 1987

Inherit the Earth, 1987

The Pirate Economy, \987

Liberating Planet Earth, 1987

Healer of the Nations, 1987

Is the World Running Down?, 1988

Puritan Foonomic Experiments, 1988

Political Polytheism: The Myth of Pluralism, 1989
Trespassing for Dear Life, \989

When Justice Is Aborted, 1989

The Hoax of Higher Criticism, 1989

The Judeo-Christian Tradition, 1990

Tools of Dominion: The Case Laws of Exodus, 1990
Millennialism and Social Theory, 1990

Christian Reconstruction, 1990 [with Gary DeMar]
Books edited by Gary North

Foundations of Christian Scholarship, 1976

Tactics of Christian Resistance, 1983

The Theology of Christian Resistance, 1983

Editor, Journal of Christian Reconstruction (1974-1981)
