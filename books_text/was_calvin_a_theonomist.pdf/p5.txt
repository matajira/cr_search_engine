Was Calvin a Theonomist?

then, is that we presume our obligation to obey any
Old Testament commandment unless the New Tes-
tament indicates otherwise. We must assume conti-
nuity with the Old Testament rather than disconti-
nuity. This is not to say that there are no changes
from Old to New Testament. Indeed, there are —
important ones. However, the word of God must be
the standard which defines preciscly what those
changes are for us; we cannot take it upon ourselves
to assume such changes or read them into the New
‘Testament.”6

This position has produced a certain amount of
exegetical bobbing and weaving. “There are,” Bahn-
sen writes, “cultural discontinuities between biblical
moral instruction and our modern society. This fact
does not imply that the ethical teaching of Scripture
is invalidated for us; it simply calls for hermeneutical
sensitivity.”? “Hermeneutical sensitivity” allows a de-
gree of latitude — how much, no one can say in ad-
vance. But every intellectual and judicial system even-
tually adopts a similar qualification; the human mind
is neither digital? nor unfallen. Nevertheless,
theonomists are at a comparative disadvantage in
terms of creating a systematic apologetic system, since
they assert that the Bible is relevant for every area in
life, not just in great shining platitudes, but specifi-
cally. This makes for a complex, detailed, and diffi-
cult apologetic.*

In general, however, the precision of the defini-
tion of theonomy supplied by Bahnsen has led to an
extensive output of theological works that apply it to
a whole host of biblical-theological issues, including
social theory.

6, Greg L. Bahnsen, By This Standard: The Authority
of God’s Law Today (Tyler, Texas: Institute for Christian
Economics, 1985), p. 3.

7. Greg L. Bahnsen, “The Reconstructionist Option,”
in Bahnsen and Kenneth L. Gentry, House Divided: The
Break-Up of Dispensational Theology (Tyler, Texas: Institute
for Christian Economics, 1989), p. 32.

8, Modern computers “think” digitally; they are in fact
giant morons, not giant brains. A. L. Samuel, quoted by
Nicholas Georgescu-Roegan, The Entropy Law and the Eco-
nomic Process (Cambridge, Massachuseus: Harvard Univer-
sity Press, 1981), p. 92.

9. It also leads to the multiplication of critics who do
not read it before they go into print with their criticisms.

3
