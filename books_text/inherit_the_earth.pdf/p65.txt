Scarcity 57

Civil law is not enough. Men also must be self-restrained in
their hearts. It’s not enough simply to give men the opportunity to
work on a free market in search of profit. Larceny is in the hearts
of most men. Mankind advances to the extent that mankind disci-
plines itself. Men must be disciplined —se/f-disciplined.

Law enforcement cannot be directed from a centralized top-
down bureaucracy, precisely because there aren’t enough eco-
nomic resources available to watch all men night and day in
everything that they do, let alone trying to examine their hearts
and motives. All government must be based first on self-government
under law. Discipline must be imposed on law-breakers by local in-
stitutions, Government must be imposed from the bottom up,
with a series of appeals courts to judge the evil public actions of
men (Exodus 18; Matthew 18). There is no other way to create
social order. It would bankrupt society to attempt to achieve the
kind of control over men’s passions and outward actions that the
modern central planner attempts to impose.

Conclusion

Scarcity is a curse of God on man, the ground, and the
animals, It was imposed by God because of man’s moral rebel-
lion. Man rebelled against God, specifically by stealing what was
announced in advance as being “off limits” to man. Men, there-
fore, were cursed by a special kind of burdensome scarcity by
which they would be pressured to labor harder than they would
have been forced to labor before the Fall. They were given a scar-
city of lifespans, as well, so that they would have to work even
harder to achieve their goals, They don’t have the same amount of
time in front of them that they otherwise would have possessed.

Scarcity is a blessing because it pressures men to cooperate
voluntarily with each other, If men are willing to obey the law of
God, and to establish those kinds of legal institutions of private
and familistic capital that the Bible requires, then it becomes pos-
sible for them to overcome many of the limits of scarcity.

Today, Western citizens live in an economic world in which we
don’t work nearly so hard as our grandfathers did, just as they
