36

CoH

Inherit the Earth

. Socialistic central planning requires a tyrannical elite.
. Individual responsibility requires individual initiative.
. Individual initiative requires personal liberty.

. Obedience to God is the basis of liberty.

10.

Reconciling differences requires a system of appeals courts

(plural).

Mt.

cies.

12,
13.

Men are responsible (subordinate) to several human agen-

No one human institution is absolutely sovereign.
Submission to authority is absolutely necessary. Man must

serve someone,

14.
15,

Leadership begins with “followership.”
Man operating independently from God (autonomy) results

in failure and defeat.

16.

Wealth flows toward those who accept personal responsibil-

ity for their actions.

17.
18.

Responsible action requires a concept of law and ethics.
Biblical law is the basis of responsible dominion.
