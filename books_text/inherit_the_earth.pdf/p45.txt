Til, Law/Dominion

3
THEFT

You shall not steal (Exodus 20:15).

The third principle of a Biblical covenant is the principle of
ethics-dominion. The basis of long-term authority is obedience to
God's law. This principle of dominion through moral obedience is
related to economics in numerous ways, but nothing is clearer
than the Bible’s prohibition against theft. The eighth command-
ment (seventh, if you’re a Lutheran) prohibits theft. This unques-
tionably is the basis of a defense of the idea of private property.

More important, as we learn in the tenth commandment,
God’s law requires the protection of family property. The tenth
commandment prohibits coveting anything that is our neighbor's,
It prohibits the mental origin of grasping, greedy evil. The eighth
commandment prohibits theft-—a visible manifestation of this
coveting process. It establishes for all time that it’s illegal and im-
moral for an individual to steal property which belongs to some-
one else. As we shall see, it’s equally illegal in God’s sight to get
the State to steal for you. The commandment doesn’t say, “You
shall not steal, except by majority vote.”

Stolen Fruit
The most important single example of theft that we have in
the Bible is the theft by Adam and Eve of the fruit of the tree of the
knowledge of good and evil (Genesis 3). God set a legal boundary
around that tree. He told them that they could eat from any tree
in the garden, with the exception of this one tree (Genesis

37
