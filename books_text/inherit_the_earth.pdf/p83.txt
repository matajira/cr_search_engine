‘No Trespassing!” 78

dependent men points to. Man is God’s image and God's lawful
representative on earth.

The Landmark

The landmark is what established the boundary lines of a par-
ticular family’s property. We use a similar technique today: sur-
veying. When we apply this Biblical law today, we make it illegal
to tamper with court records that identify particular plots and
their owners. We even have title insurance, so that if some irregu-
larity in the history of the ownership of the property is discovered,
and someone else can prove that he owns it, the initial buyer is
paid for his loss by the insurance company.

The person who owns a piece of land has the right to exclude
most people most of the time. There are a few exceptions to this
rule. In emergencies, the police, as officers of the court who have
been issued court orders or warrants, have the legal right to in-
trude on otherwise protected private property. But the owner has
the legal right to keep people from coming onto his property most
of the time,

The fence is a sign of this right, or the locked gate. The locked
door on a home is another example. The idea is that “a man’s
home is his castle”—a legal fortress which must be respected.

When some property owner sticks a “No Trespassing” sign at
his gate, or somewhere inside the boundaries of his property, his
wishes are legally enforceable. He has the legal right to keep peo-
ple off his property. The egal right to exclude someone from using your
property is the essence of all ownership.

There are limits on this right of exclusion. For example, Bibli-
cal law says that a traveller who walks along the highway has the
Tight to pick food from privately owned farms. He does not have
the right to place the food in baskets or in the folds of his garment,
but he has the right to whatever he can carry away (Deuteronomy
23:25). Jesus and His disciples picked corn on the sabbath, but
the Pharisees didn’t criticize them for stealing, only for taking
corn and rubbing it together on the sabbath (Luke 6:1-5).

Nevertheless, there are only a few cases of such exceptions to
