TABLE OF CONTENTS

Part I: BLUEPRINTS
Tntroduction..........

1. God Owns the World .. .
2. Dominion by Subordination

  
 
  
 
 
 
 

 
  
 

3. Theft .......... 37

4. Scarcity........ 49

5. Inheriting the World . 60

6. “No Trespassing!”........- 0.600 cece eee eee ee 74

7. Debt Bondage .......... 00. c cece cece eee 86

8. Let’s Make a Deal . 99

9. Profit and Loss......... All

10. The Legacy of Knowledge . 121
Conclusion. ..... 262.6 eee e cece cece cece renee eens 133

Part II: RECONSTRUCTION
11. Family Responsibilities ..........
12. Church Responsibilities .
13. State Responsibilities............
Bibliography ...........
Scripture Index .
Subject Index.........
What Are Biblical Blueprints? ................---45. 181

  
  
  
 

vii
