Bibliography 167
Shafarevich, Igor. The Socialist Phenomenon. New York: Harper &
Row, 1980.
Simon, Julian. The Ultimate Resource. Princeton: Princeton Uni-
versity Press, 1981.
Sowell, Thomas. Knowledge and Decisions. New York: Basic Books,
1981.
. Markets and Minorities. New York: Basic Books,
1981.

Sutton, Ray. That You May Prosper: Dominion by Covenant. Ft.
Worth, Texas: Dominion Press, 1987.
