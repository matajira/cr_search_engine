154 Inherit the Earth

1. How did the world economy get into such a mess?

2, What Biblical economic principles were violated, 1913 to the
present?

3. How do we return to Biblical economic principles?

4. Who should finance reconstruction?

5. What should I do with my money?

6. What should I do if I lose my job?

7. How can I afford to tithe?

Pastors and deacons are almost completely unprepared to take
leadership today. No one really expects them to. They are consid-
ered unnecessary by most people. After all, the Bible-believing
church has had little or nothing to say about economic issues
throughout this century. Economics has been considered “off lim-
its” to preachers in conservative churches.

This will change, and it will change fast, when the crises hit.
At that point, those who begin to exercise responsibility will posi-
tion themselves as leaders in the national and perhaps even the
worldwide transformation which may lie ahead. Churches had
better begin now to preach God's principles of success, and God's
principles of responsible giving.
