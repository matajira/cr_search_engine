V._Inheritance/Continuity

5
INHERITING THE WORLD

Who is the man that fears the Lorp? Him shall He teach in
the way He chooses. He himself shall dwell in prosperity, and his
descendants shall inherit the earth (Psalm 25:12-13).

The fifth and final principle of a Biblical covenant is the princi-
ble of legitimacy-inheritance, It could also be called the principle of
continuity, In the field of economics, the principle of inheritance
governs the transfer of wealth from parents to children. But the
Biblical model of this family transfer is the transfer of property
from God to His people.

Adam and Eve were given the garden of Eden to subdue and
guard (Genesis 2), They were to use this experience as a training
period; from the garden, they were to go out and subdue the
whole world. Note: possession was not automatic. They had been
given the whole world as their lawful legacy from God, but they
could not possess it free of charge. They had to earn it, just as
children are supposed to demonstrate their ability to manage
money before taking possession of the family inheritance.

The First-Born Son

Adam was the first-born earthly son of God, made in his
Father’s image (Genesis 1:26-27). The New Testament records the
family line of Jesus, and ends with Adam, who was “the son of
God” (Luke 3:38). The words “son of” were inserted by the trans-
lators of the King James edition, but this really was the meaning of
the Greek. In tracing Jesus’ lineage, the Greek reads: “Joseph,
which was [the son] of Heli, which was [the son] of Matthat . . . ,”

60
