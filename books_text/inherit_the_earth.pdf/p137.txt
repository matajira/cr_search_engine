The Legacy of Knowledge 129

Do consumers best represent their interests as voters, or do they
best represent their interests as spenders? Should their representa-
tion be politically indirect, through the ballot box, or should it be
economically indirect, through the businessman's profit-and-loss
statement?

T’s clear that in a decentralized free market, its the
profit-and-loss statement which binds together all the talents of all
the individuals who are attempting to produce in order to meet
the needs of consumers. The only other alternative is for a hand-
ful of government officials to make decisions — officials who may
be thousands of miles away from the consumer’s decision-making
in a particular store on a particular afternoon.

How good is the information possessed by bureaucratic plan-
ners three or four thousand miles away? It isn’t comparable to a
company’s profit or loss, either as an incentive or as accurate in-
formation concerning consumer tastes. Consumers are satisfied
when businesses have the best information concerning their
needs, and have an incentive system which forces them to con-
form their productive efforts according to the information pro-
vided by the consumer. When we are talking of the accuracy of infor-
mation, and when we are talking of the strength of the incentives, noth-
ing integrates the decisions of acting individuals better than a
profit-and-loss statement.

Furthermore, who is to say that bureaucrats really act as the
representatives of consumers? Isn’t it a Iot easier to believe that
they act as representatives of their own best interests? Who en-
forces discipline on the bureaucrats? In a sacialist State, how do
consumers rapidly and effectively enforce their will on the plan-
ners? At the next election? But what if the bureaucrats have life-
time jobs? What if they use their knowledge of bureaucratic rules
to thwart voters? In short, who polices the economic police? Who rules
the rulers?

The genius of the free market is that it allows us to make deci-
sions that benefit us even if we are members of a political minor-
ity. We need 50% plus one vote to win a political election. We only
need money to “win” the election at the supermarket. We get what
