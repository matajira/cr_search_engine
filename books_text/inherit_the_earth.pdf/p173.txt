BIBLIOGRAPHY

Anderson, Digby, editor. The Kindness that Kills: The Churches’
Simplistic Response to Complex Social Issues. London: SPCK, 1984.

Banfield, Edward. The Unheavenly City Revisited. Boston: Little,
Brown & Co., 1974

Chilton, David. Productive Christians in an Age of Guilt-Manipulators:
A Biblical Response to Ronald J. Sider. 4th edition. Tyler, Texas:
Institute for Christian Economics, 1986.

Davis, John Jefferson. Your Wealth in God's World: Does the Bible Sup-
port Free Enterprise? Phillipsburg, New Jersey: Presbyterian &
Reformed, 1984.

Friedman, Milton and Friedman, Rose, Free to Choose. New York:
Harcourt Brace Jovanovich, 1980.

. Tyranny of the Status Quo. New York: Harcourt
Brace Jovanovich, 1984,

Gilder, George. The Spirit of Enterprise. New York: Simon &
Schuster, 1984.

Griffiths, Brian. The Creation of Wealth. London: Hodder &
Stoughton, 1984.

—______... Morality and the Market Place: Christian Alternatives to
Capitalism and Socialism. London: Hodder & Stoughton, 1982.

Hayek, F. A. The Road to Serfdom. Chicago: University of Chicago
Press, 1944,

Hazlitt, Henry. Economics in One Lesson, New York: Crown,
[1946].

Hodge, lan. Baptized Inflation: A Critique of “Christian” Keynesianism.
Tyler, Texas: Institute for Christian Economics, 1986.

165
