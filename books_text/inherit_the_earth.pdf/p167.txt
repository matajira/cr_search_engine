 

State Responsibilities 159

the family and the church. This means tithing. This means saving
for retirement, sending your children to a Christian school, and
supporting parents who are in need. Alternative institutions must
be built up that will steadily take responsibility for the welfare
“services” the State has taken upon itself.

Fourth, it means getting involved in decaf social action. This
may begin with picketing an abortion clinic. It may mean picket-
ing stores that sell pornography—yes, even “soft-core, middle-
class pornography.” It may begin with setting up (and financing) a
crisis pregnancy center. It means beginning.

Fifth, these sorts of commitments should escalate to political
involvement. Politics, like every other area of life, is an area of
Christian responsibility and Christian dominion. But in almost all
cases, this commitment should begin with Jocal politics.

Localism

Christians seem to want to run for governor before they serve
as dogcatchers. They want to be Congressmen before they serve
as County Commissioners. They grab prematurely for the robes
of authority. This is what Adam did in the garden. He wanted al!
knowledge, even at the expense of disobeying God. Christians
want to run before they crawl.

This is not the Biblical way. We are to be trained in service at
lower levels before we serve in higher levels. This is God’s require-
ment for church officers (1 Timothy 3). It should be the normal
path for civil officers, too.

We must also understand that a primary political goal is decen-
tralization, Those activities that the national civil government now
controls should be the responsibility of state or local civil govern-
ments. Power should be lodged closer to home. So should taxes.
The national civil government is essentially an appeals court, It is
to settle disputes that lower courts find too difficult.

Jethro, Moses’ father-in-law, made this plain in his advice to
Moses, which Moses adopted as law in Israel.

And you shall teach them [the people] the statutes and the laws,
and show them the way in which they must walk and the work they
