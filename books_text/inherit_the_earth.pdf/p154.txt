146 Inherit the Earth

Eventually the addict either suffers the agony of withdrawal
from the drug, or else he dies. The question for every Christian
family is this: How can I reduce my dependence? How can I become a
responsible person? The answer must begin at home, and it must
begin immediately. Families must not continue to search for some
other agency to finance their endless desires: at below-market
prices. They must not continue to believe that some loving gov-
ernment official is going to solve their problems free of charge.
They must forever abandon faith in that endless promise: “'m
from the government, and I’m here to help you.”

If the government ever publishes a book to show bureaucrats
how to serve the public, it will be a cookbook.

Conclusion

There is no escape from responsibility. Christians are now in a
position to serve as examples to the families of the world, They
need to begin to get their houses in order. This includes economic
order,

Families need to prepare for the worst. When God brings judg-
ment on this civilization, those who have confidence in a sovereign
God and His reliable principles of righteous action will possess the
courage of their convictions. They will be in a position to lead.

Today, things seem to be bumping along rather well. Few peo-
ple believe that they need God. They are like the fools described
by God, fools who have said in their hearts: “My power and the
might of my hand have gained me this wealth” (Deuteronomy
8:17). They have placed their faith in the State; they have placed
their futures in the hands of God-despising humanist bureaucrats.
They will suffer the terrible consequences,

Christians dare not share this faith in the State, They must turn
to God and His eternal principles of righteous action as their source
of success. They must not become modern versions of the Israelites
in the wilderness who worshipped a golden calf that their leaders
had constructed for them. They must begin taking the steps neces~
sary to get as far away from modern calf-worship as they can, ac-
cepting no more hand-outs from its priests, the bureaucrats.
