96 Inherit the Earth

join with other debtors in order to gain political control over
money. The debtors have a reason to band together and to main-
tain the illusion that they actually met the obligations of their
loans. They do this by pressuring the civil government into creat-
ing inflated, unbacked, “fiat” money. Fiat money is the State’s
“word-created” money which private citizens cannot by law turn
in to the government and receive in exchange at a legally specified
rate gold or silver or some other specified commodity. The State
can print up all this money that it wants. It is unrestrained by the
fear of a run on the Treasury’s gold, When huge quantities of this
unbacked money are spent into circulation by the State, debtors
can dump this newly created money into the laps of the creditors.
This is a false repayment, but it satisfies the legal demands of the
debt contract.

When massive debt is indulged in across the board by the ma-
jority of the society, there follows an almost irresistible political
pressure on the part of the debtors to inflate the currency. This
leads to the destruction of values, the destruction of cooperation
in the economy, the destruction of foreign credit, and on and on,
It results, in other words, in very bad long-term consequences. It
is, to put it bluntly, a form of theft.

Thus, if societies are to reduce the political threat of mass in-
flation, they must place limits on the legal ability of men to indebt
themselves long term. This is one reason why Israel was required
to cancel all debts in the seventh year. A thirty-year bond,
whether issued by the State or a corporation, is opposed to Bibli-
cal law. So are thirty-year mortgages, although most Americans
use long-term mortgages to finance their homes. The rise of mass
inflation in peacetime has accompanied the rise of long-term debt.

Conclusion
What the Bible sets forth is a system of limited debt and a
warning against debt in general.
Most people are under the impression that the Old Testament
had very rigorous laws, but the New Testament is merciful and
has much more lax requirements. Actually, this is almost the re
