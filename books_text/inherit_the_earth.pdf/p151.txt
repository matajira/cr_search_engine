Family Responsibilities 143

tian schools, but if they refuse, then at least they should send their
children to private schools. The State pays for “free” education in
order to capture the minds and votes of the next generation.
Christians have no excuse: they must immediately pull their chil-
dren out of humanistic, State-financed schools. (See Robert
Thoburn’s book in this series, The Children Trap.)

The third step is for families to take over the retirement func-
tion. Social Security is a political lie, a statistical monster which
will not pay off to those who are entering the work force today,
and probably not for anyone under age 50. It is a huge tax scheme
which is now the number-two source of tax revenues for the
United States government. It’s going bankrupt. Everyone knows
this today —even economists (who are usually the last to find out
anything practical).

If older people have believed this lie, and have become eco-
nomically dependent on Social Security checks, then they must
immediately acknowledge that they have sold themselves into slavery.
They should try to go back to work. The money from Social
Security should then be donated to some charity. (Never return @
government check to the government if it’s owed to you by law:
the bureaucrats will only use it to buy more support from some-
one else.) If possible, and at considerable pain, Christians must
remove themselves from dependency on the State. We call drug
dependence “addiction.” That's exactly what Social Security is: ad-
diction to the State.

The fourth step is for families to begin supporting their older
members who need care. This is what the fifth commandment re-
quires: honoring parents financially (Exodus 20:12).

The fifth step is for families to train their children in the prin-
ciples set forth in the Biblical Blueprints series. They need to train
up a generation of Christians who will not be compromised by the
man-worshipping State.

The sixth step is for families to get out of debt. This increases
their independence. They can take greater responsibility, and
more profit-seeking chances, if they owe no one anything
(Romans 13:8).
