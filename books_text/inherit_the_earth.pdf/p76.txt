68 Inherit the Earth

foundations that rich men set up in order to beat the tax collec-
tors. Second, in the civil government, it has been the equally face-
less Civil Service-protected bureaucrats who run the multi-billion
dollar government programs. Socialism always benefits government bu-
reaucrats and the politicians who vote for the spending programs.

In short, the demand for high inheritance taxes has almost
nothing to do with the actual amount of taxes collected by the gov-
ernment. It has everything to do with envy: pulling down the success-
Sul for the sheer joy of destruction. The inheritance tax is envy-based,
and those who vote for it are envious.

The political popularity of extremely high inheritance taxes on
the rich has been universal throughout Western civilization over
the last hundred years. The justification for high inheritance taxes
is that the children of the rich have done nothing to earn the
money. Politicians argue that the parent may have been produc-
tive in some venture, or lucky in some venture, but this has noth-
ing to do with the merit of the children.

Short-Run Thinking

The economic error in such reasoning is that it is extremely
short run in perspective. One of the motivations of future-
oriented businessmen is to lay up a large amount of capital to be
distributed among his children. The idea is to pass the skills of
capital development on to the children, so that they, too, can con-
tinue to expand their dominion over whatever they have been en-
trusted with by the parents. The idea is to expand the dominion of
the family by means of a constantly increasing capital base.

Parents understand that if the sons or daughters don’t learn
the skills of wise management, these children will eventually lose
the money. A wise parent trains his children in the administration
of money, service to the community, making a profit in a competi-
tive market, and building up the capital base. If the parent fails to
do this, it’s obvious that the family’s capital base will not survive
the second generation, or at least not the third. (The only truly
rich family in the United States that has continually expanded its
capital for as long as 150 years is the DuPont family.)
