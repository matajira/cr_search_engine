Let's Make a Deal 107

if some item or service is legitimate for a person to offer another
individual, there should be no legal restraints placed on anyone
else to make a competing offer.

There is no doubt that in some fields, some individuals in cer-
tain foreign nations can make better deals to consumers around
the world, We think of Japanese electronics, or Japanese cameras,
as classic examples. We need to remind ourselves that in 1952 or
1953, Japanese craftsmanship was a joke internationally, and the
words “Made in Japan” didn’t carry much weight. But we allowed
Japanese producers to produce the best goods and services that
they could. They got better at it, and the whole world has bene-
fited from their efforts.

But the Japanese are not world competitors in the field of agri-
culture. On the contrary, they are importers of agricultural prod-
ucts. They are importers especially of American agricultural
products. They are also importers of American timber products
and other raw materials that they need. If we refuse to allow them
to sell their cameras, their electronic equipment, and their auto-
mobiles to American citizens, then they can’t buy dollars. If they
can’t buy dollars, how will they be able to buy soybeans and meat
and timber and the other products that they need in order to sus-
tain their life style?

“Deals Not Allowed!”

If we erect import barriers against Japanese products, we there-
by automatically erect export barriers against outgoing American
products. If we American consumers don’t spend our dollars on
Japanese goods, then the Japanese consumers can’t get dollars to
buy American goods. This is obvious, yet few voters on either side
of the border understand it.

It works both ways. If they impose quotas and other restraints
on what Americans will be allowed toexport into Japan, then the
Japanese consumer isn’t going to get as good a deal. Because he
isn’t allowed by his government to buy American products, the
American seller of goods isn’t going to get access to as many yen,
and we consumers will not be able to buy as many products from.
Japanese producers.
