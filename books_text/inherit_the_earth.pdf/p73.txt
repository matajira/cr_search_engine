Inheriting the World 65

their inheritance wisely—by exercising dominion. They must
prove themselves faithful sons.

The jubilee year was fulfilled by Christ, as He announced.
Israel’s 50-year land-holding cycle is abolished forever, just as the
slaying of animals in the Temple is abolished forever. Jesus ful-
filled the terms of the jubilee year. The Jews’ inheritance in
Palestine was taken from them. There is no more need for the
jubilee year as a means of repossessing land, for there is no more
inheritance based on the original military conquest of Canaan.
We have a better jubilee today: world dominion. We hold title to the
whole world; now we need to fulfill the terms of the dominion as-
signment in order to prove ourselves faithful sons.

Responsible Sonship

The system of inheritance in the Old Testament was based on
a fundamental principle: the rightful heir is the responsible heir.
Normally, the first-born son inherited a double portion (Deuter-
onomy 21:17). That is, the inheritance would be divided up in
terms of its value among the sons, with an added unit. If a man
had six sons, the inheritance would be divided into seven parts,
with the eldest son inheriting two units.

Why a double portion? Because the eldest son would have the
greatest responsibility for the support of the parents. Why not
give the daughters an inheritance? They did receive the inherit-
ance, if there were no surviving sons (Numbers 27:1-11). If there
were sons, they didn’t inherit. Why not? Because they were to be
given a dowry of money and goods at the time of marriage. Their
husbands were responsible for the support of a different set of
parents. Presumably, if a son-in-law agreed to contribute to the
support of her parents, then he could become a legitimate heir.

Consider what this meant for the first-born son. He would be
required to take care of his parents in their old age. He was there-
fore entitled to the double portion. The parents understood that
they could not live forever. They understood that they were under
the curse of God because of the sin of Adam. They therefore had
to make preparations with respect to the building up of their sav-
