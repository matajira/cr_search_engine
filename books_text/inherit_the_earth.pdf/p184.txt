176 Inherit the Earth

Free market
auction concept, 16-17
contrast to fascism, 12
decentralized, 129
increases knowledge, 122
miracle of, 123
private property system, 56
scarcity and, 125
self-regulating, 127
social order and, 56
unity and diversity of, 130
Fuel Employment Act, 69
Future, 127

Gambling, 94
Garden of Eden, 76
Gethsemane, 62
Gilder, George, 53
God
central planner, 134
creator, 10
fear of, 45
Holy Ghost, 31
image of, 10
monopolist, 38
omniscient, 19
perfect knowledge of market, 134
Tight of exclusion, 80
sovereignty, 11, 13
sustainer, 133
Trinity, 12
Goods and services, 26
Grace, 99-100
Grace Commission, 67
Grant, George, 72, 141
Great Depression, 153

Harvard Business School, 102
Health care, 3
Heli, 60
Hewett, Sylvia, 32
Hierarchy
family, 32

inescapable concept, 34
multiple, 30
principle of, 23
state, 33
History, 11
Holy Ghost, 31
Humility, 27, 5t

Illegitimate heirs, 69
Importing, 107
Impotence, 26
Incentive, 105
Income Tax, 39
Industrial Revolution, 51
Inescapable concepts, 6
Inflation
modern mass, 92
Germany, 93
way of life, 136
theft and, 47
Information, 129
Inheritance
Christian, 64
dominion by, 65
double portion, 65
Jubilee restoration of, 63
knowledge and, 12t
principle of, 60
Promised Land and, 53
rightful heir, 65
short-run thinking, 68
tax on, 67
two-way respons;
Inheritance Tax, 67-68
Insubordination, 29-30
Insurance
amount needed, 150
health, 151
term, 150
Interest, 90
Investor, 15

  

ty, 66

Japanese production, 107-108
