118 Inherit the Earth

for the church, The business is not guided by a formal appeals
court hierarchy, the way the church is. Nevertheless, business
does have guidelines, just as the church has the revealed guide-
lines of the Bible. The business operates in terms of a unity, even
though it may have many people working for it.

Consumer Sovereignty

Why do consumers rule in a free market economy? Because
they have the money. They earn it as producers by serving con-
sumers; as consumers, they reward or punish producers. The rule
of success is clear: serve the consumers. If you don’t, they will take
their business (their money) elsewhere.

The means of control which consumers possess is the
profit-and-loss system. Remove this, and you remove the author-
ity of consumers to offer incentives to producers to serve con-
sumer interests. This is why bureaucratic socialist societies are
centralized. They substitute the rule of government officials for
the rule of the consumer. They give a few chosen officials the legal
right to offer rewards and punishments. Thus, producers begin to
serve the demands of these few central planners.

Consumers then lose their authority in the economy. They are
almost powerless to replace the bureaucrats. They can no longer
“vote” with their wallets to reward one producer over another. By
abolishing profit and loss in a competitive free market, the social-
ists centralize economic power into their own hands. It is initially
the triumph of politics over economics; then it becomes the
triumph of bureaucracy over politics.

Who are the losers? Everyone except (1) the favored producers
who are chosen by the bureaucrats to receive government finan-
cial aid and (2) the officials who provide this aid. Who finances
this system? Consumers as taxpayers, They are compelled to fi-
nance their own destruction as sovereign consumers.

When people vote for programs of socialistic wealth redistri-
bution, they condemn themselves to slavery. God will not be
mocked. When they voluntarily abandon their role as sovereign
judges of the economy, they are then judged by the bureaucratic
