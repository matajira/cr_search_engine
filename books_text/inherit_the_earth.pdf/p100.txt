92 Inherit the Earth

Western banks and Western governments have lent money to fi-
nance the schemes of politicians in backward nations. These
backward nations now threaten the very banking system of the
West. If they should default on the loans overnight, the West's
banking system might collapse.

This seems to oppose what the Bible teaches. It seems now
that the creditor is servant to the debtor. It's now a disadvantage
to be a creditor, and an advantage to be a debtor. Why is the mod-
ern world seemingly a refutation of the Biblical principle of debt
and servitude?

 

Modern Mass Inflation

The primary difference is that in the modern world, the State
has the power to create money. This was not the case in ancient
Israel. Because the State has this monopoly of money creation,
