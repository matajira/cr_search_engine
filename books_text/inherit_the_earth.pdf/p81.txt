Inheriting the World 73

through the generations, The modern welfare State is a satanic at-
tempt to seize the capital of modern man, just as Satan seized
Adam’s inheritance. It is Satan’s last-gasp effort to trick Christians
out of their lawful inheritance. To the degree that they adopt his
evil theory of the State as welfare agent and therefore lawful heir,
God’s program of world-wide dominion is delayed.

The Biblical principles of inheritance must be obeyed if Chris-
tians are to exercise their dominion responsibilities, They must
acknowledge that:

1,
2.
3.
4.

God is the absolute owner of all things.

He deeded this inheritance to Adam.

Adam’s moral rebellion led to his disinheritance,

Jesus, as the true Son of God, inherited the kingdom of

God—the whole world.

Oar MHa

. His death passed His inheritance to His ethical brethren.
. Adoption comes with God's saving grace.

. Adopted children inherit God’s kingdom.

. This lawful title to the world is to be collected by Christians.
. The hasis of collecting the inheritance is godly labor, thrift,

and leaving an inheritance.

10.
. Inheritance taxes are demonic.

. The family is the primary agency of welfare.

. Charity begins at home, and spreads out,

. Where there is responsible behavior, authority follows.

. Christ completely fulfilled the jubilee year,

. Land tenure is no longer governed by the provisions of the

The welfare State is demonic.

jubilee year,

17.

die.

18,

Immoral children must be disinherited before the parents

The most competent and morally faithful child should in-

herit a double portion,
