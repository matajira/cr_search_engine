Theft 43

ing the wealth of its citizens through heavy taxation? Or would
they rather live in some backward agricultural or backward pagan
civilization?

Would most people rather be kings in some backward area, or
would they rather be middle-class people who enjoy all the bene-
fits of modern medicine, modern technologies, and all the other
benefits we have in Western societies? Perverse men who love to
exercise power over other people would rather be kings in back-
ward societies, but most people would rather be middle-class citi-
zens in wealthy and growing societies.

Varieties of Theft

There are many forms of theft. It’s not just walking into a per-
son’s house and taking something. It’s not just sticking a weapon
in a man’s back and demanding that he turn over his wallet.

Fraud is theft. You advertise a product as being of a certain
quality, and it isn’t. You advertise that your product performs in a
certain way, and it doesn’t. You tell a person that if he performs a
certain amount of work at a certain level, you'll pay him a bonus.
But you don’t.

It works for employees as well as employers. An individual
says he'll work very hard for a particular wage, but he doesn’t. A
person goes to work on the job, and then spends time talking on
the phone to friends, or takes time out of the day to pursue his
own affairs. He steals from his employer.

There are many ways to steal from an individual, but all of
them involve the same basic impulse. The thief denies to someone else
the right to pursue his own life in his own way. He denies the other per-
son the right to keep the benefits he earns by his own hard work
and risk-taking. The thief hinders the other person in the pursuit
of his lawful goals. Theft is stealing the other man’s tools and
therefore the other man’s goals-~ lawfully pursued goals.

Present-Orientation
This is why theft makes people very present-oriented. They
hold on to what they have in the present instead of sacrificing for
the future. They decide that the important thing is to enjoy the
