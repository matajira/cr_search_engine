192 Inherit the Earth

tutional church. They argued that the institutional church is the
only manifestation of God’s kingdom.

This led to two opposite and equally evil conclusions, First,
power religionists (“salvation through political power”) who ac-
cepted this definition of God’s kingdom tried to put the institu-
tional church in charge of everything, since it is supposedly “the
only manifestation of God’s kingdom on earth.” To subdue the
supposedly unredeemable world, which is forever outside the
kingdom, the institutional church has to rule with the sword. A
single, monolithic institutional church then gives orders to the
state, and the state must without question enforce these orders
with the sword, The hierarchy of the institutional church concen-
trates political and economic power. What then becomes of liberty?

Second, escape religionists (“salvation is exclusively internal”)
who also accepted this narrow definition of the kingdom sought
refuge from the evil world of matter and politics by fleeing to hide
inside the institutional church, an exclusively “spiritual kingdom,”
now narrowly defined. They abandoned the world to evil tyrants.
What then becomes of liberty? What becomes of the idea of God’s pro-
gressive restoration of all things under Jesus Christ? What,
finally, becomes of the idea of Biblical dominion?

When Christians improperly narrow their definition of the
kingdom of God, the visible influence of this comprehensive king-
dom (both spiritual and institutional at the same time) begins to
shrivel up. The first heresy leads to tyranny dy the church, and the
second heresy leads to tyranny over the church. Both of these nar-
row definitions of God’s kingdom destroy the liberty of the respon-
sible Christian man, self-governed under God and God's law.

Zoroasters Dualism

The last ancient pagan idea that still lives on is also a variant
of dualism: matter vs. spirit. It teaches that God and Satan, good
and evil, are forever locked in combat, and that good never trium-
phs over evil. The Persian religion of Zoroastrianism has held
such a view for over 2,500 years. The incredibly popular “Star
Wars” movies were based on this view of the world: the “dark” side
of “the force” against its “light” side. In modern versions of this an-
