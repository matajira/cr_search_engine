124 Inherit the Earth

But L only hinted at an even more important aspect of the divi-
sion of labor: the division of intellectual labor. It isn’t simply physi-
cal labor that’s involved; it’s also mental labor.

As individuals, we all possess highly specialized knowledge.
We all have certain unique information, unique viewpoints,
unique perspectives by which we view the world. Men are not all-
knowing, either individually or as a species. We can never know
everything (not even in heaven). Only God knows everything.
Therefore, one of the ways that men can increase the total knowl-
edge which can be brought to bear on any particular subject or
problem is by cooperative mental labor. In other words, there is a divi-
sion of intellectual labor.

Prices Are Signals

The most important institution ever developed by man for
putting to use in the best way the division of intellectual labor is
the competitive free market. Because of the information that
prices provide in a competitive market, people can make judg-
ments about the true conditions of supply and demand. Prices are
signals. Prices are information. Without them, we are flying
blind.

Who controls prices? Consumers. It is through their competitive
bidding that resources are pushed and pulled through the econ-
omy. The high bid wins. Consumers compete against each other
in the free market, and the result is a wide variety of prices. Con-
sumers competing against each other in the free market have collective author-
ity over prices. They set the economic goals of the overall economy,
and their means of mutual competition, competitive money bid-
ding (the auction principle), produces the technical means by which
they control producers: market prices.

As producers, people can decide what it is that would be most
profitable for them to produce, given their particular skills and re-
sources. As consumers, they are given information about what is
the best price available in order to achieve their goals. In other
words, the price system and the profit-and-loss system reduce
waste (increase efficiency) in the society. It makes men more effi-
