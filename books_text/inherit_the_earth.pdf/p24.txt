16 Inherit the Earth

seller to determine what the community (participants in the mar-
ket) really wants is this: high bid wins. The economy is essentially a
giant auction,

Let's say that an individual decides that his property should be
used either to build an apartment house or to grow food. People
want living space, and an apartment complex is a way in which
people can get inexpensive living space. By using the land as a site
for the construction of an apartment complex, the owner makes it
impossible for the land to be used for growing food. He denies
other users access to the property. This is the essence of all owner-
ship: denying access to an asset. It is the legal ability to say, “This use,
not that one.” Such a decision should always be made in terms of
this principle: “I’m responsible.”

What if a farmer also wants this property? What economic in-
centives can the farmer offer to the land owner to persuade him
that he, the farmer, should be allowed to take control of the prop-
erty? Obviously, the best way for him to do it would be to offer the
owner a lease or a rental payment or even cash on the line in order
to gain exclusive use of the property.

In this example, one group of consumers benefits, and a
different group loses. Some consumers are more interested in in-
creasing the supply of apartment house space, thereby making
space cheaper, while others are more concerned with increasing
the supply of food, and thus making food cheaper.

The apartment house builder acts as a middleman for the peo-
ple who want to rent the space. Similarly, the farmer acts as a
middleman for people who want to buy food cheaply. Each group
of consumers is represented, economically speaking, by an agent. He
is not a legal agent, but he is an economic agent. He does not hold
a piece of paper signed by all the members of the group that says:
“This man is our lawfully designated representative.” He is simply
a person who is willing to put his own money (or money he has
borrowed and is responsible for) on the line in the hope of selling the
property's economic output to the special interest group that he believes will
pay him the highest price. The builder and the farmer each want to
sell or rent the use of the land to “his” group of consumers, not
