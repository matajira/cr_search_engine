Inheriting the World 67

6:6-7). Faithful parents recognized that they had an obligation
both to themselves and to the community at large that their chil-
dren would be instructed in the Word of God.

Faithfulness to God’s law was the basis of inheritance in Israel. It
was the realization that parents owed the children a godly inherit-
ance, and that children owed the parents a safe retirement. Thus,
there was no large generation gap, because the basis of mutual service
was the Word of God. Each group could expect payment from the
other. At the same time, each group understood its obligations to
the other. It was a demonstration of the Biblical principle that sue-
cess comes through service (Mark 9:35), and that capital comes
through long-term efficient faithfulness (Matthew 25:14-30).

Inheritance Taxes

One of the most disastrous developments of the twentieth cen-
tury is the almost universal acceptance of the moral acceptability
and political necessity of very high inheritance taxes on the rich.
This outlook is primarily the result of envy: the hatred of those
who are better off, and the willingness to tear them down, even if
it hurts those who do the tearing.

Voters know that there are very few rich people. They know
that when they vote for politicians who in turn make laws allow-
ing the State to seize rightful inheritance in the form of taxes,
there will not be enough confiscated wealth to benefit voters.
There simply aren’t enough rich people in the world. Even if all
inheritances of wealthy people were transferred to the civil govern-
ment, the amount of money would be so small in comparison with
the taxes taken from middle-class people, that no one would even
notice the money. The Grace Commission estimated that if all per-
sonal income above $75,000 a year in the United States were col-
lected as taxes, this extra revenue would operate the Federal gov-
ernment for only ten days. (And the next year, rich people would
hide their income or stop working and taking risks to earn it.)

So who actually gets the rich man’s inheritance? Two groups,
First, in the private sector in twentieth-century America, it has
been the faceless bureaucrats who run the multi-million-dollar
