62 Inherit the Earth

field because He created it. He even stocked it with capital assets.

“And the vinedressers took his servants, beat one, killed one,
and stoned another. Again he sent other servants, more than the
first, and they did likewise to them” (Matthew 21:35-36). The hus-
bandmen were thieves. They wanted the fruits of the field. They
were dividing up the land and its fruits “in the name of the People.”
No more would the distant landiord take advantage of them!

“Then last of all he sent his son to them, saying, ‘They will
respect my son.’ But when the vinedressers saw the son, they said
among themselves, ‘This is the heir. Come, let us kill him, and
seize his inheritance.’ And they caught him, and cast him out of
the vineyard, and killed him” (Matthew 21:37-39). The husband-
men imitated God, Who had cast Adam out of the garden. The
Jews later fulfilled His parable when they dragged Him from the
garden of Gethsemane into a “kangaroo court,” tried Him, and
then had Him executed by the Roman authorities.

What was the point of the parable? That the Jews had slain
the prophets, and would slay Him, too. Judgment would come
soon thereafter. “Therefore I say to you, the kingdom of God will
be taken from you and given to a nation bearing the fruits of it”
(Matthew 21:43). In short, they had given up their inheritance. They
had imitated Adam, rebelled, and would be scattered. They had
taken property from its rightful owner, and God promised to take
away what would have been theirs.

The implication was world-transforming: Christ’s people have in-
herited the kingdom. This kingdom is the whole world.

Jesus and the Jubilee Year

Jesus began His public ministry when He entered the
synagogue at Nazareth, and stood up to read the scroll of the
Book of Isaiah which had been handed to Him. He read from the
section we designate today as Isaiah 61. Jesus read these words:
“The Spirit of the Lord is upon Me, Because He has anointed Me
to preach the gospel to the poor. He has sent Me to heal the
brokenhearted, To preach deliverance to the captives And
recovery of sight to the blind, To set at liberty those who are op-
