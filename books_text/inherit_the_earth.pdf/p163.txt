3
STATE RESPONSIBILITIES

You shail do no injustice in judgment. You shall not be partial
to the poor, nor honor the person of the mighty. But in righteous-
ness you shall judge your neighbor (Leviticus 19:15).

The civil government possesses a monopoly: the monopoly of
violence. It alone has the right to inflict fines, physical punish-
ment, and death on those who violate the laws of God and those
statutes written by men in conformity to God’s legal principles.

The problem is, this monopoly of violence can be misused.
Men from the beginning of time have sought political power in
order to control their personal rivals. They have used violence
against their competitors, all in the name of justice.

The result has been an ever-increasing State. Each group
wants special economic favors from the State: direct financial aid,
as well as indirect financial aid: taxpayer-financed insurance from
loss, restrictions in trade for competitors (and therefore consum-
ers), higher rates of taxation for rivals, State-granted professional
licenses that exclude rivals, tariffs and quotas against foreign-
manufactured goods, price “floors” (in agriculture, this scheme is
called “parity”) that make voluntary bargaining illegal, price “ceil-
ings” that make voluntary bargaining illegal, and on and on. Each
scheme is advocated as a necessary exception to the general prin-
ciples of economic freedom. Each one is promoted in the name of
“the public interest.” Each one involves the theft of money, free-
dom of choice among alternatives, or future innovations.

(I guarantee you, a substantial percentage of my readers are

155
