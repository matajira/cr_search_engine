126 Inherit the Earth

cous judgment, it takes many years of study, many years of
competition.

What the competitive free market allows us to do is to get the
very best of men’s productivity into the marketplace, so that we
¢an sort out who does the best work, who is worthy of imitation,
who should be copied, and what line of production is the most im-
portant. We could not have known this in advance. It is onty the
existence of a competitive free market which enables us to find out
what the potentials are of other men and other approaches to a
problem, and to find out what kinds of goods and services might
be offered.

The Reconciliation of Individual Plans

Because each individual, whether acting as a consumer or asa
producer, asserts his will and demonstrates his abilities in the
marketplace, and because his performance is evaluated by the
market by means of profit and loss, people can work out their
plans logically. Nevertheless, there is no single earthly unified plan,
as if there were a single physical planner present in the production
process. On the contrary, the plans of acting individuals are very
often in opposition to each other. Nevertheless, through the com-
petitive action of the free market, the competing plans of each act-
ing individual can be reconciled in a productive way through mar-
ket competition.

Buyer and Seller

When a buyer and seller come together, they may have very
different goals in mind. They may have very different plans.
Because they can compete with one another, and because they can
bargain with one another, and because each of them can search
out someone else who may be able to be substituted for the person
in front of them, there is a possibility open for the working
together of these various plans.

Both the buyer and the seller want to make a deal. If they
enter into a voluntary exchange, then each person has done so
expecting to be better off. They both expect to win. There is no
