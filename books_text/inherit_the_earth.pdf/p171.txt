State Responsibilities 163

If the Present System Continues

We are headed for national bankruptcy. It is only a question of
time, It is also a question of which form the bankruptcy will take.
Will it be an open declaration that “the Federal government can-
not honor all of its welfare, debt, and military obligations”? Or
will it be a disguised default: mass inflation, price controls, and
rationing of goods and services? Will we first get another round of
tax increases, especially a national sales tax (or “value added tax,”
the VAT)?

I predict mass inflatios., followed by price controls and ration-
ing. I call this “government by emergency.” I even wrote a book
with that title, with suggestions as to how families and churches
can protect themselves from this form of national bankruptcy.

No State is able to imitate God successfully. No State is a
savior. Salvation by law, especially State law, is the devil’s own lie.
This means that there will eventually be judgment on every State
which has asserted near-divine status. Christians should do their
best to avoid any sort of economic dependence on such a State, so
that when it falls, they will not be crushed by it or dragged down
by it.

All the programs will go bankrupt: Social Security, Federal re-
tirement, the repayment of Treasury debt, the subsidizing of
nearly bankrupt businesses (especially the big multinational
banks), the creation of State-protected monopolies (including
trade unions), and all the rest. When the State is bankrupt, it will
be impotent. It will have to be trimmed back to its original God-
given functions: the protection of life and property from violence.

During the crisis, Christians will be called upon as never be-
fore to exercise charity and godly dominion. At that point, we will
see a massive shift of power. Power will go to those who exercise
responsibility and charity, and who can show men how to put
their lives back together. If it isn’t the church, Christian charities,
and Christian families, who will it be?
