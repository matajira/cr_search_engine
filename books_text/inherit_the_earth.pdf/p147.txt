it
FAMILY RESPONSIBILITIES

‘When you sit down to eat with a ruler, Consider carefully what
is before you; And put a knife to your throat If you are a man
given to appetite. Do not desire his delicacies, For they are decep-
tive food (Proverbs 23:1-3).

In our century, families have “sat down with the ruler,” the
State. They have enjoyed the State’s many delicacies: “free” edu-
cation, “free” retirement benefits, “free” medical care, and “free”
everything else. Families have been gluttons at the State’s table.
They have stuffed themselves with deceitful meat. Now they are
suffering indigestion.

If something isn’t done about it, they will become totally de-
pendent on the State, at precisely the point in history when the
State is about to go bankrupt because everyone is demanding
more than it, or the taxpayers, can fulfill. What can be done to
liberate families from this dependence? Christian reconstruction.
What is Christian reconstruction? It’s a total restructuring of pro-
grams like welfare in terms of God’s revealed laws, which are
found in the Bible.

There are two basic principles of politics that must be under-
stood well in advance of any program of Christian reconstruction:

1. You can’t beat something with nothing.
2. Authority fows toward those who take responsibility.

This is a book on Biblical economic principles, Readers might
expect me to argue that what we need to restore a free market
economy is a political revolution~-a revolution in people’s think-

139
