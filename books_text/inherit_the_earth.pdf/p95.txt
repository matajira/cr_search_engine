Debt Bondage 87

of our existing capital, we owe Him no formal payment. In short,
“no gain, no pain.”

Debt and Subordination

Not so with the indebted person. There may be no future
gain, but there will be future pain. The debtor has placed himself
under a master. He has sold a portion of his future increase. He
has asserted in principle that he will get this increase. He has
risked becoming a visible servant because he has contractually
boasted concerning his economic future.

The Bible teaches very clearly that man cannot serve two mas-
ters. He either serves God or he serves mammon, the god of
greed. The meaning is obvious: God is the absolute ruler and
owner of all creation, and the only person to whom man should be
in debt. When a man borrows money from another individual, he
promises to return that asset, and he usually also promises to
return additional assets (interest). He therefore has made a vow to
that other individual, He has made a promise. If the promise is a
legal transaction, as it usually is in debt relationships, he has
pledged not only his name and sacred honor to that individual,
but he has also pledged his future.

The Old Testament took debt very seriously. Old Testament
law allowed an individual and his family to be sold as temporary
servants in order to repay a debt (Leviticus 25:39-43). He became
the social equivalent of a sojourner, a foreign believer who lived in
Israel (Leviticus 25:40)—a humiliating prospect for a Hebrew. If
an individual borrowed money or goods from another individual
in Israel, that individual to whom the debt was not repaid could
compei the authorities to offer the debtor up for purchase. A third
party could come in, pay the creditor whatever was owed to him,
and take the debtor into bondage.

The Year of Release

There were time limits on how long he could be forced to
serve. In Deuteronomy 15, we are given information about this
limited period of time that a Hebrew could be put into slavery: “At
