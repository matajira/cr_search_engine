76 Inherit the Earth

exclusion. Property ownership is supposed to be widely dispersed
in a Bible-based society, and this means that many people are to
have near-exclusive use of their property.

Obviously, the principle of boundaries and the right of exclu-
sion applies to other forms of property besides land. Therefore,
we need to consider the concept of the boundary.

The Original Boundary

God set Adam and Eve in the garden. “Then the Lord God
took the man and put him in the garden of Eden to tend and keep
it” (Genesis 2:15). What does “to keep” mean? It means to keep
something away from someone else. To keep the garden away from
whom? From the intruder, Satan. They were to maintain it under
God’s authority as His appointed agents.

This means that they were required to put a kind of “No Tres-
passing” sign inside the garden against all those who would chal-
lenge the law of God. Satan then came to them and tempted them
to disobey God, to accept the devil’s interpretation of the law
rather than God's.

What was the law's requirement? That they respect the
boundary God had placed around the tree of the knowledge of
good and evil. It was “off limits” to them. They could not touch it
(Genesis 3:3) or eat it. It didn’t belong to them.

God had excluded them. This pointed to His position as the origi-
nal and ultimate owner of the property. It reminded them that
they were under God’s rule. They were His subordinates. But it
also served to remind them of their responsibilities as keepers of
the garden. They, too, were to serve as guardians. They were to
keep out any intruder. Because God, as supreme and absolute
owner, could legally keep them away from His property, so were
they given power by God’s law to keep Satan away from their prop-
erty (meaning God's property which He had entrusted to them).

The moment that they stole God’s property by invading the
forbidden boundary, they had in principle abandoned the garden,
as well as the world outside, to the devil. If they could rightfully
assert their power by violating God’s property, then Satan could
