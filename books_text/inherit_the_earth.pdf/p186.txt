178 Inherit the Earth

New world order, 56
Noah, 72

Nuclear Energy, 3
Nuclear power, 4

Oaths, 24
Obedience, 29
Omnniscience, 19-20
Original Sin, 144
Ownership
Biblical vs. socialist, 2t
collective and individualistic, 12
costs, 18
free market system and, 12
expensive, 18
individual, 25-26
mutual, 82
moral responsibility of, 20
original, 11
overlapping principle, 12
private vs, collective, 20
religious concept, 11
social function, 14, 78
socialistic debate, 12
theocentric, 11, 19
Trinitarian, 12
voluntary, 13-14

Parable of Absent Landlord, 61
Parenthood, 83
Parents
knowledge, 121
responsibility, 66
Pastors, 151-152
Payne, Robert, 80n
Personal Responsibility, 26
Pharaoh, 27
Pin-Making Factory, 113
Plato, 83
Political revolt, 5, 139
Politicians, 9
Politics
basic principles, 139

local, 159
salvation by, 142
Pollution, 3
Pornography, 106
Poverty, 72, 152
Power
decentralized, 72
grabbing for, 27
not absolute, 140
recipients of, 78
Predictions, 127
Price
competition, 53-54
controls, 163
signals, 124
Private property
importance of, 40
reinforced by God, 77
Privatization, 162
Process of discovery, 2t
Production techniques, 54
Profit
indicator of success, 116
loss and, 18, 115
Profit and Loss Statement, 125
Profit-Sharing Plan, 53
Providence, It
Property
administration of, 3
decisions and, 18
family, 24-26
human rights and, 26
management of, 10, 140
New Testament view of, 24
Old Testament view of, 24
overlapping ownership, 12
ownership and, 38
private, 15
proper distribution of, 20
rights of exclusion, 75-76
shared, 14
Prostitution, 106
