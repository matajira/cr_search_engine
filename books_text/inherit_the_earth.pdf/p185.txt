Jethro, 159

‘Jezebel, 39

Jonah, 6¢

Joseph, 60, 148

Jubilee
abolished, 65
fiftieth year, 63
fulfilled in Christ, 65
military conquest and, 63
never celebrated, 63
symbolical meaning, 63
world dominion and, 65
year of, 62

Judgment
autonomy and, 95
covenant and, 6
inescapable, 11
outworking of sin, 144
scarcity and, 49-50

Khrushchev, Nikita, 81
Knowledge
decisions and, 129
inheritance and, 12
limited, 19
mobilization of, 125
production of, 125

Labor
cooperation af, 114
division of, 24, 108
Lake Erie, 4
Land
apartment houses and, 16
boundaries and, 75
farming, 15
multiple uses of, 15
Landmark, 75
Larceny, 48
Leadership, 7
Lease, 10
Lenin, Vladimir, 34
Life Insurance, 149

Subject Index 177

Limited knowledge, 19
Loans
business, 89
charitable, 88-69
Lobbying, 158
Localism, 159
Loss, 18

Mammon, 87
Man
depravity of, 56
self-discipline of, 57
Market
plans, 126
prices, 24
tewards and punishments, 127
Marriage
benefits, 25
bourgeois, 80
sanctity of, 79
Marxism
assumptions of, 41
communist form of, 80
Marx, Karl, 46, 80
Mass production, 51.
Matthac, 60
Medicare, 151
Melchizedek, 148
Middle Ages, 51
lle Class, 51, 53
Miracle, 123
Money
protected, 93
self-defined currency, 93
Monopoly, 157
Moses, 27, 159
Muhtiple authorities, 119, 140

 

Naboth, 39
National Socialists, 12
Nazi Party

German, 12

Iealy, 12
