50 Inherit the Earth

An example of something which was scarce prior to the Fall is
time. It was not that Adam couldn’t live long enough to achieve
his goals, but he still had to do one thing al a time. To choose to do
one thing meant that he couldn’t do something else. So he had to
allocate (distribute) his time. He had to make decisions about
what he was going to do and the order in which he would do it.
Therefore, time was never a free (zero price) resource. He would
have to give up achieving certain things that he would do in one
period of time in order to achieve something else.

Scarcity became a burden, a curse, just as labor became
a curse to man. Consider Adam’s pre-Fall labor. He worked in
the garden. He worked to subdue the earth. Adam named the
animals (Genesis 2:19), and he had at least some idea of beauty,
for he was to dress and protect the garden (Genesis 2:15).
But after the Fall of man, God cursed Adam’s labor and man’s
environment.

The Curse of the Ground

Why did God curse the environment? First of all, as we have
seen in Chapter Two, He cursed it in order to show Adam what it
was like to suffer disobedience in the lower ranks of the chain of
command. God wanted to show Adam what it was like to have the
world rebel against him, just as he had rebelled against God.

Another reason for cursing Adam was to remind Adam of the
penalties of disobedience. His work would no longer be the com-
pletely joyful and fulfilling occupation that it had been before the
Fall. The world was now cursed. Weeds and thorns and briars
would grow up; they would poke him and stab him. This also was
a form of punishment. The world would not be as easy to rule as it
would have been had there not been a rebellion against God.

Furthermore, it would take more of his time and effort to sub-
due the earth. He would have to pay more— give up more benefits
and pleasures—in order to achieve his goals in life. In other
words, there is no doubt that the curse of the world was a restrain-
ing factor on man and a judgment on man.
