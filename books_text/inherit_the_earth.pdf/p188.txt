180 Inherit the Earth

Tithe and, 148
Value Added Tax (VAT), 163
Television, 156
Theft
ballot box theft, 45
democracy and, 39
effects of 41-42
Eighth Commandment and, 37
freedom of, 38
larceny, 48
present oriented, 43
restraint of, 45
varieties of, 43
Thoburn, Robert, 143
Tithing
amount of, 148
church and, 148
poor tithe, 149
response to debt, 86
Tod, Robert, 102, 109
Tobias, Andrew, 150
‘Trade, 106

Transaction, 127
Transcendence, 74
Trinity, 12, 31, 13

Usury, 88

Value Added Tax (VAT), 163
Violence, 157

Voluntary ownership, 13
Voting, 161

Wealth formula, 52
Welfare, 71-72, 141
Welfare State
Biblical response to, 71-72
divorce and, 79
Satanic, 73
Western Technology, 3
Whitney, Eli, 54
Women’s Liberation, 32
Work, 102
World dominion, 65
Worship, 28
