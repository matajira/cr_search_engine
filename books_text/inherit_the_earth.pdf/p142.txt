134 Inherit the Earth

{omniscient} and present with the creation (omnipresent). This
places the Bible in total opposition to all forms of compulsory
State socialism. Modern socialism claims for the State what the
Bible claims only for God: omniscience (perfect knowledge). Modern
socialism also transfers to the State a degree of power which the
Bible says belongs only to God: total power, The total power of the
State is a necessary and inevitable aspect of total planning. Socialist
theory therefore requires the omnipotence of the State.

The Bible teaches that only God can be regarded as a reliable
central planner, because He is the Creator and heavenly Sus-
tainer of the world. He alone possesses perfect knowledge and
total power. Whenever these two aspects of God’s Being are trans-
ferred to any human institution, the end result is tyranny. In the
case of economics, the end result is also economic chaos, the loss
of productivity, and increasing misery for everyone who isn’t a
high official in the State.

The Bible sets forth social requirements that can only produce
a capitalist economy. It is not simply that Christian ethics agree
with capitalism’s ethics; rather, it is that Biblical Christianity can
Jead only to a society which is necessarily capitalistic. Capitalism
is the historical product of Christianity, and where capitalism is
abandoned, the judgment of both God and the consumers will be
visited upon the economic order. Men will seek ways to hide what
they already own instead of producing even more. The only win-
ners will be the State planners, and only for a few generations,
until the spiritual and economic capital of that society is eroded
and destroyed.

The socialist understands implicitly that the heart and soul of
modern capitalism is the private ownership, profit-and-loss sys-
tem. Where profit and loss exist within the framework of the pri-
vate ownership of the means of production, and when civil gov-
ernment does not interfere with private enterprise, consumers can
flourish and prosper, if that is their goal. Yet this system stands as
a testimony against all assertions that the State or any other repre-
sentative agency of power possesses the characteristics or the
capacities of God Almighty.
