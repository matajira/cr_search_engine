Let’s Make a Deal 109

our fellow man as a living sacrifice in order to serve them better,
and if it makes it egal for us to make each other better deals,
then the law has interfered with our ability to serve God best and
to be served by other men who are attempting to serve man or
God as best they can. At the very least, they are trying to serve
their own self-interest, and the best way for them to do that is to
make me a better deal.

It is true that some foreign nations subsidize companies that
produce certain exports. Certainly the United States does
(Export-Import Bank, etc.). Should we pressure the Federal gov-
ernment to impose tariffs against these subsidized products that
Americans want to buy?

To answer, let me ask this question: If a foreign government
wants to send me a check in the mail, should the U.S. govern-
ment be allowed to intercept the check and tear it up? No? Well,
the principle is the same. If intercepting foreign checks to me in-
terferes with my freedom, then so does a retaliatory tariff against
State-subsidized foreign goods. Eventually, sending me checks
will prove to be stupid on the part of the foreign nation; then it
will stop. This wastes the money of foreign taxpayers. So does fi-
nancial assistance to exports. It may be good short-term politics,
but it’s bad economics. Eventually, foreign voters catch on. The
practice stops.

Conclusion

What the Biblical principle of economic freedom requires is
that each man be allowed to make honest, competitive offers to
possible buyers of his goods or services. What freedom means is
that I, as a consumer, am allowed by law to make any offer to buy
goods and services at any price J think I can get them for. What
freedom therefore must also mean is that I, as a producer, am
allowed by law to make any offer to sell goods or services at any
price I think I can get for them. Economic freedom is the principle of
competitive service. Never forget what four-year-old Robbie Tod
understood: every buyer is a seller, even a “wage-earner” (a seller
of services and a buyer of money), and every seller is a buyer,
