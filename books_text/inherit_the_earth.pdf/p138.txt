130 Inherit the Earth

we want when we buy; we seldom get what we want when we vote
especially eight months later, after “our” candidate has won.

Never forget Stan Evans’ Law: “When our friends win the
election, they aren’t our friends any more.”

What we have in a free market economy is the integration of
many plans and many talents of many participants. Unity arises
out of diversity. The freedom of individuals to pursue their own
callings before God results in a system of maximum production,
and maximum consumer sovereignty, because of the existence of
money, and the existence of profit-and-loss statements that ac-
countants can present to business owners who make the plan.
Money is, in fact, absolutely necessary in a high division of labor
economy.

What is the function of the civil government in regulating an
economy? Primarily, it is to punish fraudulent or violent practices
and thereby reduce the number of such practices. It establishes
the rules of competition, and it enforces these rules. It is to con-
form itself to the Biblical blueprints regarding civil government,
and in the field of economics, it is to honor the principles of labor,
free trade, competitive bidding, and to enforce voluntary con-
tracts. It is also supposed to ensure honest weights and measures
(honest money). If all participants know the rules in advance,
they can then make their plans accordingly.

Conclusion

‘We are told by God that we must increase our knowledge. We
are to seek knowledge. And we are to transfer knowledge and the
skills of searching for more knowledge to our children.

To obey God, we must therefore teach our children to respect
the law of God, We must teach them to abide by the laws of own-
ership, the laws against coveting and theft, and the law of inherit-
ance. If we do this, we shall transfer to them the moral and legal
foundations of the most important source of new knowledge ever
developed in man’s history: the free market economy.

The free market is the institutional arrangement which offers
men incentives to bring to their fellow men the best knowledge
