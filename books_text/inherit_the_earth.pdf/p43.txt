Dominion by Subordination 35

Conclusion

God has entrusted to mankind the administration of the earth.
We are to do all things to the glory of God. We are to submit our-
selves to the law of God. The law is our tool of authority, and the
law is the standard God will use to judge us on the final day.
Therefore, as responsible agents, we must govern ourselves and
all those over whom we have been given lawful authority, We are
to govern ourselves in terms of the same law-order that we expect
the final judgment to bring over us (Matthew 7:1).

Those people who are graciously chosen by God to become
His adopted children (John 1:12)—those who have publicly
declared their belief in Jesus Christ as the only acceptable sacrifice
which satisfies the eternal wrath of God, and who are covenanted
to (under the discipline of) a local church—are no longer under
the curse of God's law. But they don’t deny their need for the law's
protection. They want God’s protection, which necessarily means
that they want Biblical law’s protection. This means that they
want protection 4y lawfully established institutions that are based
on and restrained by God’s revealed law. They want protection
Jrom those institutions that deny that they are under God’s author-
ity and God’s law. They want freedom under God and God’s law;
they want to avoid tyranny under some other god and some other
law.

In short, Christians are supposed to recognize that authority is
inescapable, and therefore that hierarchy is inescapable, Its always a
question of whose authority and what kind of hierarchy.

To understand the nature of responsible ownership before
God, we need to acknowledge these Biblical principles:

1. Men are responsible primarily to God.

2. God is the only true central planner.

3. The primary agency of economic planning is the family, as
the primary owner of property.

4, The primary agent of the family is the husband.

5. Socialistic central planning is demonic; it is man’s attempt
to replace God.
