What Are Biblical Blueprints? 193

cient dualism, the “force” is usually seen as itself impersonal: indi-
viduals personalize either the dark side or the light side by “plug-
ging into” its power.

There are millions of Christians who have adopted a very pes-
simistic version of this dualism, though not in an impersonal
form. God’s kingdom is battling Satan's, and God’s is losing. His-
tory isn’t going to get better. In fact, things are going to get a lot
worse externally. Evil will visibly push good into the shadows.
The church is like a band of soldiers who are surrounded by a
huge army of Indians. “We can’t win boys, so hold the fort until
Jesus comes to rescue us!”

That doesn’t sound like Abraham, Moses, Joshua, Gideon,
and David, does it? Christians read to their children one of the
children’s favorite stories, David and Goliath, yet in their own
lives, millions of Christian parents really think that the Goliaths
of this world are the unbeatable earthly winners. Christians
haven’t even picked up a stone.

Until very recently.

An Agenda for Victory

The change has come since 1980. Many Christians’ thinking
has shifted. Dualism, gnosticism, and “God changed His program
midstream” ideas have begun to be challenged. The politicians
have already begun to reckon with the consequences. Politicians
are the people we pay to raise their wet index fingers in the wind to
sense a shift, and they have sensed it. It scares them, too. It should.

A new vision has captured the imaginations of a growing army
of registered voters. This new vision is simple: it’s the old vision of
Genesis 1:27-28 and Matthew 28:19-20. It’s called dominion.

Four distinct ideas must be present in any ideology that ex-
pects to overturn the existing view of the world and the existing
social order:

A doctrine of ultimate truth (permanence)
A doctrine of providence (confidence)
Optimism toward the future (motivation)
Binding comprehensive law (reconstruction)
