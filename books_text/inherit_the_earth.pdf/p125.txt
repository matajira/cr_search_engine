Profit and Loss 11?

offering these highly developed special skills to consumers in the
marketplace. This enables each of us as a consumer to call upon
men throughout the economy to serve us in their capacity as spe-
cialized producers. We become the beneficiaries of the highly spe-
cialized production skills of thousands, and even hundreds of
thousands, of producers.

A society couldn’t operate if everybody wanted to be a pro-
fessor of economics. A society couldn’t operate if every member of
the society wanted to do precisely the sare task in life. The
church would be equally devastated if everyone wanted to per-
form the same service within the church. This was Paul’s message
in 1 Corinthians 12. He was calling upon each member of the
church to perform his own service as a God-fearing individual
who is under the authority of Christ, so that every member of the
church would become a beneficiary of the specialized skills of all
the other members. This same principle operates in every organi-
zation, and especially in the free market.

The competitive free market therefore provides a means of combining the
many different specialized talents of ail producers in the economy. It does so
by offering consumers the sovereign power of entering the market
and influencing producers to serve consumers, This is a system
which we call consumer sovereignty. The consumer pays, and
therefore the consumer determines what gets produced next time.
The old slogan, “He who pays the piper calls the tune,” describes
very well the system of consumer sovereignty in the free market.

The development of double-entry bookkeeping in the four-
teenth century in Italy was one of the most important develop-
ments in the history of the world. It’s through double-entry book-
keeping that businessmen can calculate the success or failure of
their endeavors. They can tell whether certain aspects of their
business are profitable, or whether they ought to be radically
changed or even eliminated. Without the success indicator of
modern accounting, it would be impossible to manage the mod-
ern capitalist economy.

Readers should recognize why the profit-and-loss system oper-
ates for business in much the same way as church courts operate
