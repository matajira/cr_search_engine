IV. Judgment/Sanctions

4
SCARCITY

Then to Adam He said, “Because you have heeded the voice
of your wife, and have eaten from the tree of which ] commanded
you, saying, ‘You shall not eat of it: Cursed is the ground for
your sake; In toil you shall eat of it All the days of your life. Both
thorns and thistles it shall bring forth for you, And you shall eat
the herb of the field. In the sweat of your face you shall eat bread
Till you return to the ground, for out of it you were taken; for
dust you are, And to dust you shail return” (Genesis 3:17-19).

The fourth principle of a Biblical covenant is the principle of
judgment-evaluation. The covenant specifies penalties for breaking
the terms of the covenant. What the economist calls scarcity was
God’s temporal curse on Adam and Adam’s environment, Adam
broke God’s covenant, so God imposed punishment.

The economist has a technical definition of a scarce resource.
If, at zero price, there is a greater demand for the resource than
supply of the resource, then it’s a scarce resource. This means that
it’s an economic good.

Some goods aren't economic goods. For example, air is not
normally an economic good. You don’t have to pay for it. But
cooled air or filtered air is an economic good, and people will pay
to get it. They pay the electric company, and they pay air condi-
tioning firms to get it. But if, at zero price, demand is equal to
supply, or less than supply, then we are not talking about a scarce
economic resource. Simple enough?

Scarcity probably didn't originate as a result of the curse on
man. Scarcity became a durden on man, however, after the Fall of
man in the garden.

49
