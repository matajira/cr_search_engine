134

THE THIRD TIME AROUND

continned his work throughout the tumultuous Stalin
years.

Lars Tanner was raised in a large working-class Norwe-
gian Lutheran home in Oslo. When he was a teenager,
his family moved to Stockholm where he lived and
worked as a cobbler for the rest of his life. A series of
devastating miscarriages suffered by his wife provoked
him to delve into the Bible’s teaching about children.
Startled by what he discovered, he began to wonder
why the church had not taken a stronger stand the pre-
vious year when many restrictions on abortion were
lifted, Beginning with a small newsletter printed on a
crude press in his cellar, he dedicated himself to an
educational campaign that would eventually span
nearly four decades—testifying before hospital commit-
tees and government hearings, lecturing to secondary
and university students, lobbying candidates for politi-
cal office, and organizing protests at abortuary sites. By
the time of his death in 1975, he had laid the ground-
work for a resurgent prolife movement in Sweden and
had set an example of faithfulness for thousands of
Christians throughout Scandinavia.

Born in Northern Ireland to a wealthy Presbyterian
family, Amy Beatrice Carmichael became one of the best
known missionaries of the first half of the twentieth
century. Her ministry took her first to Japan, then to
Ceylon, and finally to the Dohnavur province of India.
Although sarti and inmolation had been legally banned,
to her horror she discovered that ritual abortion and
female infanticide were still quite common. In addi-
tion, many of the young girls that she had come to
work with were still being systematically sold off as
slaves to the nearby pagan temples in order to be
raised as cult prostitutes. She immediately established a
ministry to protect and shelter the girls. Although she
had to suffer the persecution of various Hindu sects
and the bureaucratic resistance of the British colonial
government, Carmichael built an effective and dynamic
ministry renowned for its courage and compassion.
