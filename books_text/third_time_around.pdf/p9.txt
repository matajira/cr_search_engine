ACKNOWLEDGMENTS

 

ost writers can truthfully say, “l hate to write, but I
love having written.” IT know at least that this
writer can truthfully say that.

This wasn’t a book that I wanted to write. It wasn’t even
a book that 1 wanted to have written. Not only was my
schedule already overcrowded and my calendar overcom-
mitted, but my mind, will, and emotions were overex-
tended. I certainly believed that the message of the book
needed to be told, but I didn’t see how I could possibly be
the one to tell it. But, like Ananias, the reluctant witness of
the book of Acts, 1 was pressed into service. The spiritual
responsibility was incscapable, no matter how I squirmed
or rationalized.

Thankfully, in His good providence, God provided a
number of dear friends, colleagues, and yokefellows, who
encouraged me along the way so that my indentured ser-
vice might be joyous and not burdensome.

My friend and colleague, Mark Horne, labored faith-
fully with me on this project. OF all the books that I have
written, this is the first onc that God has afforded me the
privilege of having someone help me with the research—to
check facts, to dig up odd out-of-print details, and to share
the vision for the work with me from start to finish. Mark
did all these things and nore—particularly in the “needle-
in-a-haystack” research for chapter 5, Mere thanks cannot
begin to convey the gratefulness I feel for his ministry in
my life.

 

 
