Bibliographic Resources 2ii

Schaelfer, Francis A., CG. Everett Koop, Jim Buchfuchrer, and
Franky Schaeffer V. Plan for Action: An Action Alternative for
Whatever Happened to the Human Race, Old ‘Vappan, NJ: Revell,
1980.

Schaeffer, Franky. A Time for Anger: The Myth of Neutrality
Westchester, IL Crossway Books, 1982.

Schaeffer, Franky, and Harold Fickett. A Modest Proposal: For
Peace, Prosperity and Happiness. Nashville, TN: Thomas Nelson,
1984,

Schaff, Philip. ed. A Select Library of the Nicene and Post-Nicene Fa-
thers of the Christian Church. 14 vols. Grand Rapids, MI: Wm.
B, Eerdmans, 1983.

Schaff, Philip, and Henry Wace, eds. A Select Library of the Nicene
and Post-Nicene Fathers of the Christian Church: Second Series. 13
vols. Grand Rapids, MI: Wm. B. Eerdmans, 1983.

Scheidler, Joseph M. Closed: 99 Ways to Stop Abortion. Westchester,
IL: Crossway Books, 1985.

Schlafly, Phyllis, ed. Child Abuse in the Classroom. Westchester, IL:
Crossway Books, 1986.

Schlossberg, Herbert. Idols for Destruction, Nashville, TN: Thomas
Nelson, 1983.

Seaman, Barbara. The Doctor’s Case Against the Pill. Garden City,
NY: Doubleday, 1980.

Shannon, Thomas A., and Jo Ann Manfra. Law and Bioethics:
Texts with Commentary on Major U.S. Court Decisions. Ramsey,
Nj: Paulist Press, 1982.

Shapiro, Howard I. The Birth Control Book. New York: Avon, 1977,

Sheils, W. J. The English Reformation: 1530-1570. London: Long-
man Press, 1989.

Shettles, Landrum, and David Rorvik. Riles of Life: The Scientific
Evidence for Life Before Birth. Grand Rapids, MI: Zondervan,
1983.
