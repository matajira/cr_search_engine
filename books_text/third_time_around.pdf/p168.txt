156 THE THIRD TIME AROUND

em oe

With sharp and perceptive wisdom, theologian R. C.
Sproul has summarized the challenge that faces the con-
temporary church in its pro-life efforts. He said:

The struggle against abortion is difficult, but it is worthy.
‘Lhe longer it lasts; the more babies will be slain. The
Jonger laws allowing abortion-on-demand remain in ef
fect; the more likely it is that society will be hardened in
heart. Continuing the struggle against abortion is not
enough. We must accelerate our efforts until no human
child is destroyed under the sanction of law.5

He goes on to say:

The urgency of the abortion issue requires us to protest
to the very limit that our consciences allow.®

 
