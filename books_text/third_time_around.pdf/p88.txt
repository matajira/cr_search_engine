76 THE SECOND TIME AROUND

It was the Lord who put it in me to sail from here to the
Indies. The fact that the.gospel must be preached to so
many lands—that is what convinced me. . . . Charting
the scas is but a necessary requisite for the fulfillment of
the Great Commission of our glorious Savior.”

The last mandate of Christ to His disciples, commonly
known as the Great Commission, was to comprehensively
evangelize all the world. He said:

  

 

“All authority in heaven and on earth has been given to
me. Therefore go and make disciples of all nations, baptiz-
ing them in the Name of the Father, and of the Son, and
of the Holy Spirit, and teaching them to obey everything I
have commanded you. And surely I will be with you always,
to the very end of the age.” (Matthew 28:18-20)

The implications of this mandate are revolutionary and
have literally altered the course of world history.

Jesus asserts that all authority in heaven is His (see
Psalm 103:19). The heights, the depths, the angels, and the
principalities are all under His sovercign rule (see Psalm
135:5-7), But all authority on carth is His as well (see
Psalm. 147:15-18). Man and creature, as well as every inven-
tion and institution, are under His providential superinten-
dence (see Psalm 24:1). There are no neutral areas in all
the cosmos that can escape His authoritative regency (see
Colossians 1:17).

On that basis, Christ says believers all across the wide
gulf of time are to continually manifest His Lordship—
making disciples in all nations by going, baptizing, and
teaching. This mandate is the essence of the New Cove-
nant, which in turn, is just an extension of the Old Cove-
nant: Go and claim everything in heaven and on earth
for the everlasting dominion of Jesus Christ (see Genesis
1:26-28).

It was this mandate that originally emboldened thosc
disciples to preach the gospel—first in Jerusalem and
Judea, then in Samaria, and finally in the uttermost parts

 
