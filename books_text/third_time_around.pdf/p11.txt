Acknowledgments xb

Susan Olasky, Bishop Austin Vaughn, Janos and Wilma
Féldényi, Paul deParrie, Jack Willke, Father Paul Marx,
David, Shepherd, Father Gordon Walker, Angela de
Malherbe, Marijo Zivkévic, Robert Whelan, Randy Terry,
Jerome Lejeune, Pat Mahoney, Art Tomlinson, and Franky
Schaeffer for their sacrificial efforts for the unwanted and
unloved, the despised and rejected,

Despite the fact that we are living in a time and a cul-
wre where Martha Quinn’s “Closet Classics” on MTV very
nearly represent the limits of our historical interest, my
publishers—and my dear friends—Robert Wolgemuth,
Mike Hyatt, Dan Wolgeruth, and David Dunham believed
in this project. Not only did they believe in it, but they also
waited patiently for it past four deadlines and three cata-
logs. That’s commitment.

My midnight serenaders—Kemper Crabb, Felix Men-
delssohn, Bob Bennett, Johann Sebastian Bach, Craig
Smith, Robert Schumann, Michael Card, Frederic Chopin,
Dennis Welch, Franz Peter Schubert, and Ted Sandquist—
soothed my soul and smoothed my path.

Tt is amazing how Christian koinonia can transform duty
into joy.

Now that this project is behind me—and what needed
to be said is actually said—I can affirm that I’m glad that I
had to write it.

But, in alt honesty, I'm even gladder to have written it.

Feast of Wenceslas
Fort Lauderdale, Florida
