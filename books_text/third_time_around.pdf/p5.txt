To All the Faithful Through the Ages:
Who Have Had to Go Round and Round
In the Struggle for Life
Not Once, Not Twice, But Three Times

And
To My Own Beloved Children:
Whe Will Have to Take Up the Cause
The Next Time Around

Dalnt qui dedit
