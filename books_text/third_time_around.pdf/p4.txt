 

 

The mission of Wolgemuth & Hyau, Publishers, Inc, is to publish
and distribute books that lead individuals toward:

© A personal faith in the one true God: Father, Son, and Tloly
Spirit;

© A lifestyle of practical discipleship; and
@ A worldvicw that is consistent with the historic, Christian faith.
Moreover, the Company endcavors to accomplish this mission at a

reasonable profit and in a manner which glorifics God and serves
His Kingdom.

 

 

 

 

© 1991 by George Grant, All rights reserved.
Published January 1991, First Edition.
Printed in the United Staces of America,

97 96 95 9493 929190 87654321

 

No part of this publication may be reproduced, stored in a retrieval
system, or transmitted in any form by any means, electronic, mechani-
cal, photocopy, recording, or otherwise, without the prior written per-
mission of the publisher, except for brief quotations in critical reviews
or artich

 

Unless otherwise noted, all Scripture quotations are from the author's
own translation,

Scripture quotations marked NKJV are from the New King James Ver
sion of the Bible, © 1979, 1980, 1982, 1984 by Thomas Nelson, Inc.,
Nashville, Tennessee and are used by permission.

Wolgemuth & Hyatt, Publishers, Inc.
1749 Mallory Lane, Suite 110
Brentwood, Tennessee 37027

Library of Congress Cataloging in-Publication Data

Grant, George, 1954-

Third time around : a history of the pro-life movement from the

first century to the present / George Grant. — Ist ed.
p. cm.

Includes bibliographical references and index.

ISBN 0-943497-65-5

1, Abortion—Religious aspects—Christianity. 2. Pro-life
movement—History. I. Title.
HQ767.25.G73 1991
263.8°36667°09—dce20 90-20888

cir
