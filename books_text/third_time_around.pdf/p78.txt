66 ‘THE SECOND TIMF AROUND

thenuc pro-life ethic. As at no other time, the unity of the
bride of Christ was shattered during this momentous
epoch. The Reformation and the Counter-Reformation un-
leashed one cataclysm after another on the church. Even
so, every authentically Biblical branch of the body? re-
mained unanimously committed to the sanctity of life—and
they remained committed to a single-minded protection of
life. And so as before, only the church was able to offer
hope to the hopeless and help to the helpless.

Servanthood

As in the medieval epoch, Christians did not merely op-
pose child-killing procedures, they worked hard to provide
compassionate and effective alternatives for women and
children in crisis. They dived their message; they didn’t just
preach it. They understood only too well that Christianity is
covenantal, and thus it is ethical.

Urgency

Jt has never been easy defending the defenseless. The costs
have always been high. But when lives are at stake, Chris-
ans have always been willing to face the odds, even when
they themselves were placed in terrible jeopardy. When the
judgment of God is nigh, they have always been at the fore-
front of the struggle for life—even when they. themselves
had to risk everything that they had and everything that
they were.

Patience

It was a life-and-death urgency that motivated the pro-life
heroes of the Reformation and the Counter-Reformation to
risk all for the unborn, the abused, the abandoned, and
the neglected. But it was a sane and solid patience—stand-
ing foursquare on the promises of God—that enabled
them to sustain their efforts from one generation to the
