Castles in the Air: The Renaissance and the Enlightenment 67

next. And ultimately, it was the wedding of the former with
the latter that brought them their consummate success.

me

The great pro-life legacy forged by the earliest Chris-
tians during the patristic and medieval eras not only sur-
vived during the Renaissance and the Enlightenment, but
it also thrived. The result was that lives were saved, women
were protected, and children were nurtured. In addition,
the culture continued to bathe in the good providence of
Almighty God.
