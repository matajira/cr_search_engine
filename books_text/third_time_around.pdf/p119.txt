Life and Liberty: The American Experience 107

tify with several children “saved from the butcher’s
knife.” Nearly half a century later, her cfforts were rec-
ognized in Albany by Theodore Roosevelt as the pri-
mary catalyst for the statc’s tougher legislation and stif-
fer enforcement.

The National Police Gazette was undoubtedly the most
sensational newspaper in America. Popular, brash, and
controversial, the Gazette was edited by George Wilkes, a
tough veteran of New York City journalism. Though the
paper regularly filled three of its eight pages with ad-
vertising—including a strong complement from patent
medicine manufacturers—Wilkes refused to provide
any space to abortionists. He regularly wrote editorials
calling for the criminalization of the industry—calling
the practitioners of the procedure “human fiends,”
“professional murderers,” and: “child destroyers.” He
chastised officials for not taking stronger action and
predicted a “day of vengeance” for all those who stood
idly by. While other newspapers were silent, Wilkes con-
tinued to awaken consciences; in fact, the prominent
pastor Gardiner Spring and the famed editor Louis Jen-
nings would both later credit Wilkes with having pro-
voked them to action.

Born the youngest of thirteen children in northern
Italy, Frances Cabrini came to America in 1889 to work
among the Italian immigrants of Chicago. In the course
of twenty-eight years of ministry there, she established
six schools, four hospitals, seven orphanages, two mater-
nity homes, and twenty-three prison outreaches. A
woman of extraordinary energy, passion, commitment,
and zeal—she lead an educational campaign among im-
migrant women to help them avoid the pitfalls of abor-
tion and radical feminism. Traveling extensively
throughout the continent, she brought her pro-life mes-
sage of hope in Christ to thousands of thirsty souls.

In 1856, Samuel Taylor was a prototypical mild-man-
uered small town pharmacist. A family man, commu
nity leader, and lifelong Methodist, he had a natural
