Life and Liberty: The American Experience Hi
Patience

Knowing that whatever they did would establish a prece-
dent for generations to come, the doctors, lawyers, and
churchmen who led the pro-life cause were careful, cau-
tious, and patient. They wanted to make certain that their
efforts would not be in vain. They rested in the security. of
God’s promises and the surety of God’s providence.

Pn a

Lives were saved, families restored, and a remarkable
foundation of liberty was laid for future generations by the
men and women who dedicated themselves to the cause of
the sanctity of human life. America at last seemed poised to
fulfill her promise—as the land of the free and the home
of the brave.
