May the Circle Be Unbroken: The Lessons of History 167

vialism, and provincialism. It is standing as one against the
tidal wave of perversity and perniciousness in the world. It
is presenting a united front against the minions of death
and destruction. It is:

Being like minded, having the same love, being of one
accord, and being of one mind. (Philippians 2:2)

Attaining to the unity of faith, and of the knowledge of
the Son of God, to a mature man, to the measure of the
stature which belongs to the fullness of Christ.
(Ephesians 4:13)

Being diligent to preserve the unity of the Spirit in the
bond of peace. For there is one body and one Spirit, just as
you were also called in one bope of your calling; one Lord,
one faith, one baptism, one God and Father of all who is
over all and through all and in all. (Ephesians 4:3-6)

Being of one accord and with one voice glorifying the
God and Father of our Lord Jesus Christ. (Romans 15:6)

Whenever the pro-tife movement has been successful
throughout history, the church has been its wellspring and
its mainspring. It derived its strength, its authority, its
anointing, and its unction from the sacramental life of the
body. The pro-life movement was the church. And the
church was the pro-life movement.

If we are going to effectually defend the sanctity of
life—either this time around or even next time around—
then we must learn this fundamental lesson. Nothing—not
the false hope of political expediency, not the hollow ideal
of popular accessability, and not the deceptive allure of in-
stitutional pragmatism—nothing must be allowed to deter
or detour us from the centrality of the church.

Servanthood

The gospel calls us to live as if people really matter. It calls us
to live lives of selfless concern. We are to pay attention to
