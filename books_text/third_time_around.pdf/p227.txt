Bibliographic Resources 215

Wilson, A. N. Lilaire Belloc. London: Penguin Books, 1984.
Wilson, Ellen, An Even Dozen. New York: Human Life Press, 1981.

Wilson, Mercedes Arzu, Love and Fertility: The Ovulation Method,
The Natural Method for Planning Your Family. New Orleans, LA:
Family of the Americas, 1986.

Woiwode, Larry. Born Brothers. New York: Penguin Books, 1988.

Womer, W. L., ed. Morality and Ethics in Earty Christianity. Phila-
delphia, PA: Fortress Press, 1987.

Young, Curt. The Least of These. Chicago: Moody Press, 1984.
