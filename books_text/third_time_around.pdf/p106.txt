94 THE SECOND TIME AROUND

As we entered the room a young girl emerged there-
from. She seemed to be about twenty years of age, a little
more than five feet in height, of slender build, having
blue eyes, and a clear, alabaster complexion. Long
blonde curls, tinted with gold, drooped upon her shoul-
ders, and her face wore an expression of embarrassment
at the presence of strangers. She retreated to the end of
the hall, and stood there for a moment, and then went
to another part of the house. In a few moments . . 5

He went on to describe his conversation with
Rosenzweig. The doctor was more than candid about the
arrangements and proceeded to quote a flat fee of two
hundred dollars—quite a sum in that time. When St. Clair
asked about the aborted child, the doctor replied:

“Don’t worry about that, my dear sir. I will take care of
the result. A newspaper bundle, a basket, a pail, a resort
to the sewer, or the river at night? Who is the wiser?”

When St. Clair persisted with his questioning,
Rosenzweig suddenly became suspicious and indignant, vio-
lently driving the reporter off the premises at pistol point.
As he made his retreat, he caught a final glimpse of the
doctor's office:

As I passed through the hallway I saw the same girl who
had left the partor when I made my first visit to the
house. She was standing on the stairs, and it was the
same face I saw afterward in the morgue. I positively
identify the features of the dead woman as those of the
blonde beauty before described.”

St. Clair’s boldness was not without risk. Abortion was
big business. And abortionists were men and women of
great power and influence. They attempted to pressure the
young reporter and his editors at the Jimes—with veiled
threats, proffered bribes, and political innuendo—to back
away from his story.8 They were even able to put into mo-
tion the forces of the infamous and corrupt machine at
Tammany Hall.?
