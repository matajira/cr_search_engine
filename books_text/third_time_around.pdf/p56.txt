44

THE FIRST TIME AROUND

married Malcolm of Canmore, king of the Scots, They
had a wonderful romance and a happy marriage, rear-
ing six sons and two daughters. A strong-willed queen,
she was noted for her solicitude for orphans and the
poor and for her rabid intolerance of slavery, abandon-
ment, and abortion. Her son David became one of the
best Scottish kings, and like his mother came to be re-
vered as a saint for his pro-life concerns and his selfless
generosity toward the needy. Her daughter married
Henry I of England and brought a similar saintly influ-
ence to the British royal line. Thus, by the time she
died im 1093, Margaret had not only left her gracious
mark on her subjects, she had left it on the whole of
the fabric of history.

julian Katarva was a Dalamatian nobleman who

through a tragic error was responsible for the deaths of
several members of his family. He never fully recovered
from that awful accident and devoted the rest of his life
to the care of the sick, the troubled, or the suffering.
He endowed several hospitals all over central Europe
during the cleventh century. He also went on a per-
sonal crusade against those who took life lightly: abor-
tionists, mercenaries, highwaymen, and occultists. Sev-
eral times he risked his own life by rescuing children or
women in distress. Eventually, he became known as Ju-
lian the Hospitaller.

The beautiful and beguiling Elizabeth of Bratislava was
the daughter of che Hungarian king, Andrew I, Her
marriage at the age of fourteen to Ludwig of
Thuringia, though arranged for political reasons, was a
happy one and the couple had three children. In 1227,
Ludwig died suddenly after joining a band of crusaders
bound for the Holy Land. Grief stricken for some
months, the young Elizabeth finally vowed to give the
rest of her life in service to the needy. She helped to
establish one of the first foundling hospitals in Europe,
as well as several orphanages and almshouses.
