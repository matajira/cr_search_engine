42

THE First TIME AROUND

engaged in public service and civil affairs. He married
young and faithfully raised his children in the nurture
and admonition of the Lord. Despite the fact that he
was entirely untrained in theology, his unflagging piety
and wisdom encouraged the people of Alexandria to
call him at the age of fifty to be their patriarch and
pastor. He threw himself into his new responsibilities
with characteristic zeal. He injected new life into that
old church by establishing innumerable ministries to
the needy. He endowed several health care institutions,
including the very first maternity hospital. He founded
several homes for the aged and infirm. He opened hos-
pices and lodges for travelers. He tore down the rem-
nants of the old infanticide walls outside the city with
his own hands and called on his parishioners to join
him in defending the sanctity of human life in the fue
ture. So prolific were his good deeds that he eventually
became known as John the Almsgiver.

During much of the seventh century, Ademnan of Iona
was an influential Irish missionary, pastor, author, and
civic leader. In 697, he was instrumental in the drafting
and enacting of legislation designed to protect women
and children from the brutal oppression of the times,
including coercive abortion. To this day, this pro-life
legal precedent, called Adamnan’s Law, is the founda
tion upon which much of Ireland’s family policy is
built.

As a young woman, Sathild of Chelles was carried away
from her English home by pirates and indentured to
the court of Clovis II, ruler of the Frankish kingdom.
Her great beauty attracted the attentions of the king,
and he made her his wife in 651. Some years later,
upon the king’s death, Bathild became regent for their
eldest son, Chlotar III. Utilizing the. powers of her of
fice, she stridently opposed the profligate slave trade
and the practice of infanticide, exposure, abandon-
ment, and abortion. Her benefactions resulted in a fun-
damental reorientation of the French legal code that
