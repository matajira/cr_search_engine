2 THIRD TIME AROUND

movement didn’t even exist until 1973. You're just. a bunch
of extremists, opportunists, and Johnny-come-latelies.”

My turn. The camera zoomed in to catch my reaction. I
just smiled. “Ah, but once again, there is where you are so
very wrong: The pro-life movement is not a recent phe-
nomenon or innovation,” I said. “It is (wo thousand years
old. You see, the pro-life movement was inaugurated on a
rugged old cross, on a hill called Calvary—it is best known
as Christianity, Caring for the helpless, the deprived, and
the unwanted is not simply what we do. It is what we are.
Always has been. Always will be.”

Cut. Break for commercial and the prerecorded outro
wrap.

Stung by his obvious setback, the director of Planned
Parenthood gathered up his gaggle of supporters and pro-
ponents and stormed out of the television studio. The host
of the local program sauntered off to some comfortable
corner to sulk and primp. And my small pro-life contingent
in the audience became unusually ecstatic.

{ have to say that that reaction took me by surprise.

Certainly, I wasn’t surprised that my adversary was in-
censed. J wasn’t even surprised by the cool dismissal of my
host. What caught me off guard was the serendipitous reac-
tion of my friends in the pro-life movement:

“Wow. I'd never heard anything like that before.”

“Was there veaily a prolife movement before 19732”

“I didn’t even know that abortion was an issue before
Roe v. Wade.”

“Where can I read about the kind of pro-life activity
Christians have been involved in in the past?”

Over the next several months, as I traveled around the
country—and even around the world—working with pro-
life leaders from virtually every major organization, across
all ecclesiastical boundaries, and in every strata of life, I was
rather astonished to discover a similar unfamiliarity with
our sich pro-life heritage. People who had been valiant in
the battle for life were generally unaware of the fact that
