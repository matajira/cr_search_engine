Bibliographic Hesources 209

Painc, Thomas, Common Sense and Other Fssays. New York: Signet
Classics, 1977.

Paul, Ron. Abortion and Liberty. Lake Jackson, TX: The Founda-
tion for Rational Economics and Education, 1983.

Payne, Franklin E., Jr. Biblical/Medical Ethics. Milford, MI: Mott
Media, 1985.

Perloff, James. The Shadows of Power. Belmont, MA: Western Is-
lands, 1988.

 

Phan, Peter C. Social Thought. Wilmington, DF: Michael Glazier,
Inc., L984.

Pieper, Josef. Leisure: The Basis of Culture. New York: Mentor
Books, 1952.

Planned Parenthood. How to Talk with Your Child about Sexuality.
Garden City, N¥; Doubleday and Co., 1986.

Plymouth Rock Foundation. Biblical Principles: Concerning Issues of
Importance to Godly Christians. Plymouth, MA: Plymouth Rock
Foundation, 1984.

Pollock, John. A Fistful of Heroes: Great Reformers and Evangelists.
Grand Rapids, MI: Zondervan, 1988.

Pomeroy, Wardell. Boys and Sex. New York: Dell, 1981.
Pomeroy, Wardell. Girls and Sex. New York: Dell, 1981.

Powell, John. Abortion: The Silent Holocaust. Alen, TX: Argus Com-
munications, 1981,

Prawn, Charles D. The Bible and Birth Control. Monongahela, PA:
Zimmer Printing, 1989.

Prestige, G. L. Fathers and Heretics. London: SPCK, 1940.

Reagan, Ronald. Abortion and the Conscience of a Nation. Nashville,
TN: Thomas Nelson, 1984.

Reardon, David C. Aborted Women: Silent No More. Westchester, IL:
Crossway Books, 1987.

Riis, Jacob A. The Making of an American. New York: MacMillan,
J9ls.
