Life and Liberty: The American Experience 105

° Horatio Robinson Storer was a third generation Boston
physician and a specialist in obstetrics and gynecology.
He became concerned about the booming abortion
trade in 1855 and immediately sought ways to utilize
his family influence to halt the slaughter. He wrote to
his friends and contacts in the profession all over the
country inquiring about the abortion Jaws in cach of
their states. He then began to bombard medical socie-
ties and professional journals with information, resolu-

He confronted, cajoled, and
preached, He lobbied, argued, and prayed. [ic became
the lightning red for the national debate over abortion.
He wrote two widely circulated and popularly lauded
books——Why Not? A Book for Every Woman and Is lt I? A
Book for Every Man—which detailed the “criminality and
physical evils” of child-killing procedures. Clearly, he
was the greatest and most influential pro-life advocate
of his day. The American Medical Association ulti-
mately appointed him chairman of a select committee
to oppose child-killing procedures altogether. And thus
was born America’s first national right to life organiza-
tion—as an ancillary of the AMA no less.

 

 

* Another doctor who entered the pro-life fray was the
venerable Hugh Hodge, who was one of the most promi-
nent researchers in the field of embryology in the nine-
teenth century. He often exhorted bis medical students
at the University of Pennsylvania to protect innocent
human life. His research had convinced him beyond
any shadow of a doubt that life began at conception
and that the destruction of that life before or after
birth was unmitigated murder. In 1854, he began to
lobby the American Medical Association, in coopera-
tion with the young Dr. Storer, to call a halt to the
slaughter of children in the clinics of the land. Al
though it took many years, Dr, Hodge never wavered—
his faith in Christ bolstered him through every urial—
and ultimately he was successful: the AMA strongly de-
nounced the brutal practice in September 1871.

 

 
