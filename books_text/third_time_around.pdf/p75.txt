Castles in the Air: The Renaissance and the Enlightenment = 63

followers had rebuffed the Renaissance of barbarism at
least to some degree.

Education was the privilege of the very few and the very
rich until feen Baptist De La Salle began his great work
midway through the seventeenth century. After giving
up a life of case, he cated himself to teaching the
children of the very poor. He opened day schools, Sun-
day schools, vocational training schools, teachers’ col-
leges, and continuing education centers—actually
pioneering many modern techniques and concepts—in
more than fourteen cities throughout Europe. He
fought hard against the insipid humanistic tendencics
within the scholastic community, and he deplored the
corresponding decline in Christian morality. He be-
lieved that if youngsters could be educated in accor
dance with gospel principles, the barbarities of abor-
tion and abandonment would disappear; but if they
were not given the opportunity to advance, such wick-
edness would eventually prevail—because, as he often
quipped, evil desires nothing better than hopelessness
and ignorance.

 

 

 

Francis Di Girolamo was consecrated to the work of the
gospel as a very young man. The victim of a very cruel
and harsh childhood himself, he pledged his life to the
alleviation of the pains and woes of the poor and desti-
tute. In search of sinners, he penetrated into the pris-
ons, the brothels, the galleys, thc back Janes, and the
tenements of urban Naples. He rescued hundreds of
children from deplorable conditions and crusaded.
against a contraband abortifacient trade that was enjoy-
ing a new resurgence. Once he burst into a laboratory
where parricides were being concocted and, like Jesus
in the Temple, literally decimated the room, overturn-
ing the equipment and scattering the drugs single-
handedly. Over the years, he opened a thrift shop for
the poor, several almshouses, and a foundling hospital.
At his funeral in 1716, the poor from miles around
thronged the church in grief.
