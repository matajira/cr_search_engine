102 THE SECOND TIME AROUND

Appealing to the “Supreme Judge of the World” for
guidance, and relying on His “Divine Providence” for wis-
dom, the framers committed themselves and their posterity
to the absolute standard of “the laws of nature and nature’s
God.” And the essence of that standard, they said, were the
inalienable, God-given, and sovereignly endowed rights of
life, liberty, and the pursuit of happiness. A just government
exists, they argued, solely and completely to “provide
guards” for the “future security” of that essence. Take it
away, and the rule of law is no longer possible.

Thomas Jefferson asserted that, “The chief purpose of
government is to protect life. Abandon that and you have
abandoned al.”

Abraham Lincoln pressed the same issue home when
he questioned the institution of slavery on the basis of the
sanctity of all human life and the rule of law:

I should like to know if taking this old Declaration of
Independence, which declares that all men are equal
upon principle, and making exceptions vo it, where it
will stop. If one man. says it does not mean 4 Negro, why
not another say it does not mean some other man?50

Because the commercial promoters of abortion were so
diligent in their assault on the unborn, the aged, and the
infirmed, the rule of law in the nineteenth century was
thrown into very real jeopardy. No one was absolutely se-
cure, because absoluteness was thrown out of the constitu-
tional vocabulary. Because the right to life was abrogated.
for at least some citizens, aff the liberties of all the citizens
were at risk because suddenly arbitrariness, relativism, and
randomness entered the legal equation. The checks against
petty partiality and blatant bias were disabled.

That was hardly the rule of law. Instead, it was the bru-
tal imposition of fashion and fancy by privileged interlop-
ers. It was the denial of everything that the great American
experiment in liberty had thus far stood for. Left un-
