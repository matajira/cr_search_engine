122 THE THIRD TIME AROUND

All children born, beyond what would be required to
keep up the population to a desired level, must necessar-
ily perish, unless room is made for them by the deaths of
grown persons. Therefore we should facilitate, instead of
foolishly and vainly endeavoring to impede, the opera-
tions of nature in producing this mortality; and if we
dread the too frequent visitation of the horrid form of
famine, we should sedulously encourage the other forms
of destruction, which we compel nature to use. Instead of
recommending cleanliness to the poor, we should encour
age contrary habits. In our towns, we should make the
streets narrower, crowd more people into the houses, and
court the return of the plague. In the country, we should
build our villages near stagnant pools, and particularly en-
courage settlement in all marshy and unwholesome situa-
tions. But above all, we should reprobate specific reme-
dies for ravaging diseases; and restrain those benevolent,
but much mistaken men, who have thought they were
doing a service to mankind by projecting schemes for the
total extirpation of particular disorders.!®

The followers of Malthus believed that if Western civili-
zation were lo survive, the physically unfit, the materially
poor, the spiritually diseased, the racially inferior, and the
mentally incompetent had to be eliminated. The question
was how? A few believed the solution to that dilemma was
political—restrict immigration, centralize planning, reform
social welfare, and tighten citizenship requirements. Some
others thought the solution was technological—control ag-
ricultural production, regulate medical proficiency, and na-
tionalize industrial efficiency. But the vast majority of
Malthusians felt chat the solution was genetic—restrict or
remove “bad racial stocks,” discourage charity and benevo-
lence, and “aid the evolutionary ascent of man.” Through
selective breeding, eugenic repatterning, and craniometric
specificity, they hoped to purify the bloodlines and im-
prove the stock of the “highest” and “most fit7—or Aryan—
race. Through segregation, sterilization, birth control, and
