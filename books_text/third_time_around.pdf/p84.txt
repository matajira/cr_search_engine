72 THE SECOND TIME AROUND

The mandate of Holy Writ is plain. We must clothe the
naked, feed the hungry, shelter the shelterless, succor
the infirmed, and rescue the perishing. I can do no less
and still be faithful to the high call of our Sovereign
Lord.$

Apparently, her crusade began to exact a toll on the
traditionalist Hindu movement because early in 1893,
Swami Dayanand Sarasvati, the leader of Arya Samaj, ap-
pealed to Queen Victoria’s viceroy to have Anna stopped.
In an attempt to keep the peace, the British administrator
ordered Anna to refrain from any activities that were not
“directly related to the operation of the missionary out-
post.” Anna replied saying that rescuing innocent human
life was indeed “directly related” to her mission work and
that, in fact, it was “directly related to any form of Christian
endeavor, humanitarian or evangelistic.”

Impatient and dissatisfied with the viceroy’s meek han-
dling of Anna, Sarasvati dispatched an angry mob of his
followers to the mission compound. They burned several
of the buildings to the ground, raped a number of the
young girls who had come to live there, and tortured and
killed Anna.

But that was not the end of Anna’s impact. The “clash
of absolutes” that she provoked highlighted for all the
world to see the unbridgeable gulf between Christian ethics
and heathen brutality.6 Her daring example sparked a re-
vival within the missionary community in Judia and her
journals, published shortly after her martyrdom, made a
stunning impact throughout England. Perhaps most im-
portantly of all, her commitment stimulated and mobilized
the church to call on the government to fundamentally
alter the essence of the policy of non-interference—not just
in India, but wherever the gospel went out around the
globe—and to enforce a universal legal code rooted in the
Christian notion of the sanctity of life.

Anna Bowden was a lady, an “elect lady” (2 John 1). And,
India in. the nineteenth century was just the place for her.
