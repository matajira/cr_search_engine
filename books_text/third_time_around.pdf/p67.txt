Castles in the Air: The Renaissance and the Enlightenment = 55

ing on that heritage for the future. Nothing was sacred any
longer. Everything—every thought, word, and deed: every
hope, dream, and aspiralion; every tradition, institution,
and relationship—was redefined.

No society can long stand without some ruling sect of
principles, some overriding values, or some ethical stan-
dard. Thus, when the men and women of the sixteenth
through the eighteenth centuries threw off Christian
mores, they necessarily went casting about for a suitable al-
ternative. And so, Greek and Roman thought was exhumed
from the ancient sarcophagus of paganism. Aristotle, Plato,
and Pythagoras were dusted off, dressed up, and rehabili-
lated as the newly tenured voices of wisdom. Cicero, Sen-
eca, and Herodotus were raised from the philosophical
crypt and made to march to the wune of a new era.

Every forum, every arena, and every aspect of life began
to reflect this newfound fascination with the pre-Christian
past. Art, architecture, music, drama, literature, and every
other form of popular culture began to portray the themes
of classical humanism, pregnable naturalism, and antino-
mian individualism.

And they began to extol their pre-Christian values as
well, including the values of abortion, infanticide, abandon-
ment, and exposure. A complete reversion took place. Vir-
tually all the great advances that the medieval era brought
were lost in just a few short decades.

By the middle of the epoch, as many as one out of
every three children was killed or exposed in French, Ital
ian, and Spanish cities. In Toulouse, the rate of known
abandonment as a percentage of the number of recorded
births varied from an approximate mean of 10 percent at
the beginning of the cra to a mean of about 17 percent at
its end—with the final decades consistently above 20 and
olten passing 25 percent for the whole population of the
city. In the poorer quarters of the city, the rate may have
reached as high as 40 percent. In Lyons before the French
Revolution, the number of cast-off, unwanted infants was
