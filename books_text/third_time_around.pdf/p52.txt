40 THE First TIME AROUND

The voice of the church universally announces the holy
sanctity of all life. Protection is secured against all aggres-
sors, both within and without the domain of paterfamil-
jas im accordance with the enactments of the most de-
vout, pious, and august princes and lords.®

Likewise, a century later when Ivo of Chartes and Gratian
compiled more than four thousand previous canons into
their respective Decretums, affirmation of the culture’s
longstanding commitment to life was a foregone conclusion:

Fearfully and wonderfully made are all the works of the
Almighty. The possessor of all reins, He has covered each
soul fram the womb. To presume upon that sacred trust
is but anathema.?

In addition to these juridical pronouncements, a vast
treasury of pro-life homiletical material was produced by
the most articulate spokesmen of the church throughout
the medieval epoch. No doubt those sermons impassioned
the minds and informed the hearts of the common pcople
in addition to keeping the issue ever before them. A place
on the church’s liturgical calendar was even set apart for a
pro-life emphasis—the Feast of the Holy Innocents, cele-
brated on December 28 in the West and December 31 in
the Fast.

Vision and Service

Medieval Christians understood only too well that it was
not enough for them to merely affirm the pro-life policies
of the state or confess the pro-life dogmas of the church.
Like Dympna, they comprehended their responsibility to
practically and tangibly act on those policies and dogmas.
They knew that just as God had shown them mercy, they
were to demonstrate mercy to others (see 2 Corinthians
1:3-7). They knew that just as God had adopted them
when they were cast off and rejected, they were to recipro-
cate (sce Deuteronomy 24:17-22).
