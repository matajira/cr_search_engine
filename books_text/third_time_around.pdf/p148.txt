136 THE THIRD TIME AROUND

Conclusion

Historian and social commentator Milan Hubl has point
edly and bitingly argued that:

The first step in liquidating a people is to erase its memory.
Destroy its books, ils culture, and its history; before long
the community will forget whac it is, and what it was.*9

It seems that during much of the twentieth century, the
memory of the church was erased. Its books, its culture,
and its history were all but destroyed in the mad rush to-
ward modernity, The community of faith forgot what it was
and what it should have been. The result was that, despite
the heroic efforts of a remnant of dissenters, the needy, the
innocent, and the helpless lost their one sure advocate.

Predictably, this failure of Christian efficacy was rooted
in the denial of five essential principles:

Orthodoxy

When believers began to abandon orthodoxy in a whole-
sale fashion, they simply exchanged the Christian system of
moral standards that had brought Western culture to full
flower for ancient pagan values. When the ethical system of
the Bible is jettisoned, men are left to their own devices
and thus quickly revert to destructive primal passions.
There is no other alternative. There is no third way. There
is no middle ground (see Matthew 12:30). Thus, when the
church at the beginning of the twentieth century began to
do “what was right in its own eyes” (Judges 21:25), chaos
reigned and the innocent suffered.

The Church

Disunity racked the church during much of the twentieth
century, not just an institutional and a denominational dis-
unity, but a fundamental disunity of focus and purpose.
Working at cross purposes with itself, the church tragically
