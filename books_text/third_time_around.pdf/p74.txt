62

THE SECOND TEME AROUND

* Otto Blamhardt was one of the earliest Luthcran mission-
aries to Africa, Setting out alone in the first decade of
the seventeenth century at a time when most of that
great continent still remained unexplored and un-
charted, he took the message of Christ and His glori-
ous grace to several primitive jungle tribes—learning
their languages, caring for their sick, and training their
young. When he encountered gruesome child-killing
rites in several of the tribes, he was not startled in the
least-——witnessing aborlion and exposure during his
childhood in the slums of Hildeshelm had prepared.
him for the depths of depravity in fallen man. That is
not to say that he accepted it as a matter of course. On
the contrary, for the rest of his life, he devoted himself
to the fight for human life. By the time of his death in
1632, he was known by the natives of the region as the
“Father of the Jungle” because so many of them liter-
ally owed their lives—their very existence—to his faith-
fulness.

 

« John Eudes was born im Normandy at a time when anti-
clerical and anti-Christian sentiment was at a fever
pitch, Because his parents remained pious, he tasted
the biter draught of discrimination early in life. Not
surprisingly, as an adult he dedicated himself to the
care of the persecuted—refugecs, the feebleminded,
Jews, the sick, Huguenots, and mendicants. He even or
ganized’ teams of Christian women to care for women
reclaimed from prostitution. His fearless and selfless
care of the dying distinguished him during two virulent
epidemics that swept through France in 1634 and 1639.
But his greatest work involved his crusade against child-
killing. Like so many faithful Christians before him, he
took seriously and literally the injunction to “rescue
those being dragged away to the slaughter” (Proverbs
24:11), Innumerable times he stopped abortionists
from plying their black arts by placing his own body
between the intended victim and its executioner. He
dicd at the age of seventy-nine content that he and his

 

 
