356 THE SECOND TIME AROUND

approximately one-third the number of births. During the
same period in Paris, children known to have been aban-
doned accounted for between 20 and 30 percent of all reg-
istered births. In Florence the rate ranged from a low of 14
percent of all babies to a high of just under 45 percent. In
Milan, the opening of the age witnessed a rate of 16 per-
cent. By its closing the percentage had actually risen to
about 25 percent. In Madrid, the figures ranged anywhere
from 14 to 26 percent, while in London, they were between

11 and 22 percent. Fragmentary evidence suggests compa-
rable rates throughout the rest of Europe, wherever resur-
rected paganism was extolled.

_ Once again, the horrid barbarity of promiscuous child-
killing became a matter of course in this poor fallen world.
In some circles it even became a badge of some honor:
JeanJacques Rousseau, for example, boasted that he had
abandoned ali five of his illegitimate children while Mozart,
Fielding, Goethe, Chatterton, Diderot, Swift, Goya, Vol
taire, and Defoe all explored the idea in their works.

Clearly, the minions of death were back. And they were
back with a vengeance.

Although a number of communities tried to maintain a
legislative hedge of protection around the innocents, the
wholesale infiltration of pagan concepts into the culture—
and even into the church—made it practically impossible.
The rising tide was just too much.

The Reformation and Counter-Reformation.

Throughout the entire fifteenth century, cries for the refor-
mation of the church came from every sector. The church
had become impotent; it was entirely unable to halt the
rapid slide into the godlessness, materialism, and hedonism
of the ancient pagan philosophies. Slowly but surely, the
church had lost its grip.

In the East, the fall of Constantinople and the subse-
quent captivity of much of Orthodoxy had made that once
