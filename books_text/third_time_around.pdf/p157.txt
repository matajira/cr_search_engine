Rescue the Perishing: The New Pro-Life Emergence 145

the Ad Hoc Committee in the Defense of Life on Capi-
tol Hill.

« In 1975, a number of evangelical leaders meet in the
home of Billy Graham for two days in order to formu-
late a Scriptural response to the crisis at hand. The re-
sult of the meeting is the formation of a new pro-life
education and advocacy organization—the Christian
Action Council.

« In 1978, the American Life League is formed to paral-
lel—and extend the limits of—the work of the National
Right to Life Committee.

« In 1979, Francis Schaeffer releases his remarkable book
and film series Whatever Happened to the Lfuman Race?,
but is met at first with a stony silence. Even so, the stum-
bering conscience begins to awaken in evangelicalism.

. A year later, Moody Monthly, a Christian family magazine
ssues a clarion call to evangelical action: “Evangelical-
ism as a whole has uttered no real outcry. We've organ-
ized no protest. Do we need time to think abortion
through? Isn’t seven years enough? The Catholics have
called abortion the Silent Holocaust. The deeper horror
is the silence of the Evangelical.”? Within months the
silence begins to shatter.

 

  

« Shortly thereafter, in 1981, Schaeffer releases another
ground-breaking book, A Christian Manifesto, which be-
gins to galvanize the nascent pro-life forces.

e In 1985, the film The Silent Scream, hosted by former
abortion rights leader Bernard Nathanson, shatters the
illusion that the procedure is anything less than child-
killing—and thus thrusis the debate onto the national
stage.

Despite these landmark events, it seemed for a time
that evangelical indifference would continue indefinitely.
But suddenly, an avalanche of concern transformed the
church. By 1985, twenty-eight Protestant denominations, as-
sociations, and missions had recanted their earlier pro-
