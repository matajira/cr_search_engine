A New Consensus: The Medieval Church 37

accomplished explorers from Amerigo Vespucci and Marco
Polo to. Vasco da Gama and Christopher Columbus; it pro-
duced some of the greatest minds and most fascinating
lives mankind has yet known (were the list mot so sterling it
might begin to be tedious) Copernicus, Dante, Giotto,
Becket, Gutenberg, Chaucer, Charlemagne, Wyclif, Magel-
lan, Botticelli, Donatello, Petrarch, and Aquinas.

But of all the great innovations that medievalism
wrought, the greatest of all was spiritual. Medieval cul-
ture—both East and West-~was first and foremost Christian
culture. Its life was shaped almost entirely by Christian con-
cerns, Virtually all of its achievements were submitted to
the cause of the gospel. From great cathedrals and gracious
chivalry to long crusades and beautiful cloisters, every man-
ifestation of its presence was somehow tied to its utter and
complete obeisance to Christ’s kingdom.

Of course, the medieval church had its share of danger-
ous and scandalous behavior. It had gross libertines and
rank heretics. It had false professors and bold opportunists.
It had brutal ascetics and imbalanced tyrants. But then,
there was no more of that sort of heterodoxy than we have
today in evangelical, catholic, or orthodox circles—and
perhaps, considering recent headlines, a good deal less.

At any rate, spiritual concerns played a Jarger role in
the lives of medieval man and women than at almost any
other time before or since. And, as might be expected, that
all-pervading interest was evidenced in a prominent con-
cern for the protection of innocent life.? Dympna was by
no means alone in her concern,

Courtly Noblesse

As the medieval Christian culture progressively permeated
the European continent and beyond, mercy and justice for
the weak, the helpless, the infirm, the unborn, and the
poor. were given new and vilal expression. The tired old
holdovers from paganism—abortion, infanticide, abandon-
