CONTENTS

 

Acknowledgments / ix

Introduction / 7

Part 1: The First Time Around

1. In the Valley of the Shadow of Death:
An Anthropology / 9

The Thanatos Factor « The Life of the World » Conclusion

2. How Firm a Foundation: The Apostolic Church / 17
The Biblical Mandate « The Consensus of the Church + Word and

Deed + Conclusion

3. A New Consensus: The Medieval Church / 33
The Dark Ages + Courtly Noblesse + Vision and Service + Conclusion

Part 2: The Second Time Around

4. Castles in the Air: The Renaissance and
the Enlightenment / 51

The Renaissance Relapse» The Reformation and Counter-Reforma-
tion « Action and Truth « Conclusion

5. To the Uttermost: The Great Missions Movement / 69

Gotonizing the Globe + The Great Commission + To the Uttermost +
Conclusion

vit
