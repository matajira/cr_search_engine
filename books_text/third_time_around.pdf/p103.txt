6

LIFE AND LIBERTY:
THE AMERICAN EXPERIENCE

‘ alea iacta est’

 

The accursed everyday life of the modernist is instinct with the four
sins arying to heaven for vengeance, and there is nu humanity in it,
and no simplicity, and no recollection.

 

Hilaire Belloc

The Modern world is full of the old Christian virtues gone mad. The
virtues have gone mad because they have been isolated from each
other and are wandering alone. Thus some scientists care for truth;
but their truth is pitiless. And thus some humaniiarians care only
for pity; but their pity—L am sorry to say—is often uniruthful.

G, K. Chesterton.

di n July 1871, Augustus St. Clair was given an ex-
tremely dangerous undercover investigative as-
signment for the New York Times: he was to
infiltrate and ultimately expose the city’s prosperous and
profligate medical malpractice industry—the common euphe-
mism for the abortion trade.

For several weeks, he and a “lady friend” visited a num-
ber of the most heavily trafficked clinics in New York, pos-
ing as a couple facing a crisis pregnancy. They were
shocked with what they saw.

 
 

a
