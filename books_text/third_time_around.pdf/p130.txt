118 ‘THE THIRD TIME AROUND

derstanding of mankind—made in the very image of God
but, at the same time, fallen. With penetrating perception
and prophetic precision, he said:

Progress has brought us both unbounded opportunities
and unbridled difficulties. Thus, the measure of our civi-
lization will not be that we have done much, but what we
have done with that much. I believe that the next half
century will determine if we will advance the cause of
Christian civilization or revert to the horrors of brutal
paganism. The thought of modern industry in the hands
of Christian charity is a dream worth dreaming. The
thought of industry in the hands of paganism is a night-
mare beyond imagining. The choice between the two is
upon us.

He saw all too clearly the dangers that faced the West-
ern world as it marched so boldly, so confidently into the
twentieth century. That is why he devoted himself to reme-
dying as many of those dangers as he possibly could
throughout his life. His reforms focused on the needs of
the poor working man, on the struggling immigrant, on
the miserable tenement dweller, on the helpless widow
and orphan, and on the pioneering homesteader. But
above ali elsc, his reforms focused on the needs of the
ordinary family—which was facing extraordinary new pres-
sures and changes. In his State of the Union message in
1905, he underscored this priority saying:

The transformation of the family is onc of the greatest
sociological phenomena of our time; it is a social ques-
tion of the first importance, of far greater importance
than any merely political or economic question can be.®

He went on to describe his simple agenda for protect-
ing the family against the encroachment of those men and
women he called “the foes of our own household.” He said:

There are those who believe that a new modernity de-
mands a new morality, What they fail to consider is the
harsh reality that there is no such thing as a new moral
