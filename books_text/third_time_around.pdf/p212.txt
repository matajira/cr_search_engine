200 BIBLIOGRAPHIC RESOURCES

Doherty, Dr. Peter. Abortion: Is This Your Choice? London: Faith
Pamphlets, 1982.

Douglas, J. D., Walter A, Elwell, and Peter Toon. The Concise Dic-
tionary of the Christian Tradition: Doctrine, Liturgy, and History.
Grand Rapids, MI: Zondervan, 1989.

Dowley, Tim, ed. Eerdman’s Handbook to the Histor) of Christianity.
Grand Rapids, MI: Wm. B. Eerdman’s Publishing Co., 1977.

Downs, Robert B, Books That Changed the World, New York: Men-
tor Books, 1983,

Drexler, Eric K. Engines of Creation: The Coming Era of Nanotechno-
logy. New York: Anchor Press/ Doubleday, 1987.

Drogin, Elasah. Margaret Sanger: Father of Modern Society. New
Hope, KY: CUL Publishers, 1980, 1986.

Duby, Georges. Revelations of the Medieval World. Vol. 2 of A His-
tory of Private Life. Cambridge, MA: Belknap Press, 1988.

Dwight, Rev. Henry Otis., Rev. H. Alen Tupper, and Rev. Edwin
Munsell Bliss. The Encyclopedia of Missions, New York: Funk
and Wagnalls Co., 1904.

Eisenstein, Zillah. The Radical Future of Liberal Feminism. New
York: Longman Co., 1981.

Elliot, Elisabeth. A Chance to Die: The Life and Legacy of Amy Car-
michael. Old Vappan, NJ: Revell, 1987.

Ellis, William. Journal of William Ellis. Tokyo: Tuttle, 1827.

Espinosa, J. G. Birth Control: Why Are They Lying to Women? Wash-
ington, DC: Human Life International, 1980.
Evans, Debra. Without Moral Limits: Women, Reproduction and the

New Medical Technology. Westchester, IL: Crossway Books,
1989,

Exley, Richard. Abortion: Pro-Life by Conviction, Pro-Choice by Default.
Tulsa, OK: Honor Books, 1989.

Farmer, David Hugh. The Oxford Dictionary of Saints. New York:
Oxford University Press, 1987,
