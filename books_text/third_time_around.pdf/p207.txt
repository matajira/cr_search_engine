BIBLIOGRAPHIC RESOURCES

Airés, Philippe, and Georges Duby, cds. A History of Private Life. 5
vals, Cambridge, MA: The Belknap Press, 1989.

Alcorn, Randy. is Rescuing Right? Breaking the Law to Save the Un-
born. Downers Grove, IL: InterVarsity Press, 1990.

Andrews, Joan, with John Cavanaugh-O’Keefe. I Will Never Forget
You: The History of the Rescue Movement in the Life of Joan An-
drews. San Frar.cisco: St. Ignatius Press, 1989.

Andrews, Joan. You Reject Them, You Reject Me: The Prison Letters of
joan Andrews, Brentwood, TN: Wolgemuth & Hyatt, Publish-
ers, Inc., 1988.

Andrusko, Dave. fo Reseue the Future: The Pro-Life Movement in the
1980's, Harrison, NY: Life Cycle Books, 1983.

Andrusko, Dave. Windew on the Future: The Pro-Life Year in Re-
view—1986. Washington, DC: The National Right-To-Life
Committee, 1987.

Anketberg, John, and John Weldon. When Does Life Begin? And 39
Other Tough Questions About Abortion. Brentwood, TN:
Wolgemuth & Hyatt, Publishers, Inc., 1989.

Delarge Press, 1928.

 

Anoulih, Jean-Marc. Monsieur Vincent, Paris:

Aramony, William. The United Way: The Next Hundred Years. New
York: Donald I. Fine, 1987.

Athanasius. On the Incarnation. Crestwood, NY: St. Vladinnir’s Sem
inary Press, 1944.

 

Auwater, Donald. Dictionary of the Saints. Updated and revised by
Catherine Rachel John. London: Penguin Books, 1983.

4195
