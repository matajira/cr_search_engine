10 THE Firsy TIME AROUND

death. It was a gruesome panorama that defiled their
senses and haunted their every waking thought.

It was a nightmare come to life.

‘The carnage was beyond their comprehension. The loss
was beyond their measure. But the memory of it would be
carved onto the fleshly tablets of their hearts with a dull,
familiar blade.

It was an all too recognizable scene—it still is.

It could describe for us the savage anguish of Israelite
mothers after Pharach ordered the deaths of all their male
children (see Exodus 1:15-22). It could describe for us the
desperate terror that raged through Gilead after the ma-
rauding army of Ammon ripped open their pregnant
women and raped their daughters (see Amos 1:13-15). It
could describe for us the dreadful pall that gripped the fam-
ilies of Bethlehem after Herod slaughtered all their infant
sons in a fit of jealousy (see Matthew 2:16-18), Or, it could
describe for us any one of thousands of other vignettes that
litter our cultural subconscious like parched bones in the
howling wilderness—from the ovens of Auschwitz to the
gulags of Siberia, from the prisons of Teheran to the streets
of Beirut, from the hospitals of Bloomington to the
abortuaries of New York.

Indeed, such tragedy is a well-worn human landscape.
Such calamity clutters the pages of human history. Such
pathos persistently torments the hodge-podge ideals of
human hope. Repiayed again and again and again, it has
become a semiotic symbol of the end of man and the end
of his doing.

Like a moth drawn to a candle flame, man deals in
death. He always has. He always will. For that is his nature
in this poor fallen world.

The Thanatos Factor

Sadly, because all men without exccption are sinners, the
most fundamental factor in understanding anthropology
