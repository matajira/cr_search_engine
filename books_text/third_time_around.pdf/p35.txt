How Firm a Foundation: The Apostolic Church 23

life and immortality to light through the gospel. (2 ‘Tim-
othy 1:10, NKJV)

‘The Bible declares the sanctity of life in its exposition
of cthical justice (see Genesis 9:6, Exodus 20:13; Exodus
21:29-95; Leviticus 94:17; Isaiah 1:15; 1 Peter 3:7).

I call heaven and earth as witnesses today against you,
that I have set before you life and death, blessing and
cursing; therefore choose life, that both you and your
descendants may live. (Deuteronomy 30:19, NKJV)

The Bible declares the sanctity of life in its exhortation
to coyenantal mercy (see Deuteronomy 10:18; Isaiah 1:17;
Isaiah 58:6-7; Acts 5:20; Titus 2:11-14; James 1:27).

[f you faint in the day of adversity, your strength is small.
Deliver those who are drawn toward death, and hold
back those stumbling to the slaughter. If you say, “Surely
we did not know this,” does not He who weighs the heart
consider it? He who keeps your soul, does He not know
it? (Proverbs 24;10-]2a, NKJV)

From Genesis to Revelation (sce Genesis 2:7; Revelation
22:17), in the books of the Law (see Exodus 4:
19:16), in the books of history (see Judges 13:2~
uel 16:7), in the books of wisdom (see Psaim 68:5-6; Prov-
erbs 29:7), in the prophetic books (sec Amos 1:13;
Jeremiah 1:5), in the Gospels (see Matthew 10:31; Luke
1:15, 41-44), and in the epistles (see Galatians 1:15; 1 Co-
rinthians. 15:22), the pro-life message of the Bible is abso-
lutely inescapable,

It was this “Word of Life” (Philippians 2:16) that Basil
had believed——and it was this Word that he acted on faith-
fully.

  

The Consensus of the Church

Basil was not alone in his affirmation of the Biblical mes-
sage of life or in his condemnation of child-killing. In fact,
