86

THE SECOND TIME AROUND

ciety was formed later that same year, he became its
first missionary, leaving almost immediately for India.
Carey’s remarkable work im literacy training and Bible
translation proved to be innovative, efficient, and fruit-
ful—serving as a model for many missions agencies to
this day. But he did not limit his efforts to academic
work alone. He quickly discovered that India’s long-
standing legal tradition gave parents the right to kill
their children—a right commonly claimed, especially in
the case of infant girls. Even the various Hindu sects
that forbade the taking of animal life permitted the
murder of newborn daughters. Carey went on an all-
out campaign to persuade the British government to
outlaw the barbaric practice, and ultimately, despite
fierce opposition, he succeeded. Shortly before his
death, he personally drafted the reform legislation that
prohibited child sacrifice at the yearly festival at Gunga
Saugor. And, even now, the statute criminalizing infan-
ticide is called the “Carey Edict.”

Nan Mullins was a dedicated and effective Southern
Baptist missionary to China. But, in the era of the great
model Baptist missionaries like Lottie Moon and Annie
Armstrong, Nan was a real enigma. She did not seem
to adhere to the standard rules for single women out
on the mission field. She eschewed western-style cloth-
ing, for instance, having been influenced by J. Hudson
Taylor’s ideas, In addition, she did not hesitate to ex-
tend her ministry beyond the traditional bounds of
teaching and nursing—believing that personal evange-
Hsm, discipleship, and social activism were integral to
her call. Chief among her concerns was the low regard
for life among the rural Chinese. Female infanticide
was especially prominent. For eight years Nan labored
among the people, caring for them and bearing testi-
mony to the basic tenants of the Scriptures. It was not
until she lay on her deathbed, however, that the re-
gional governor in Nanchung yielded to her lifelong
desire: criminalizing all forms of child-killing. He
granted it he said, because “All her life Miss Mullins
lived selflessly for our people. If I could restore her to
health and life, I would. But since I cannot, I will give

 
