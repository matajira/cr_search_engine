A New Consensus: The Medieval Church 43

survived virtually unchanged until the Enlightenment a
millennium Tater.

Boniface of Crediton spent the first forty years of his life
in quiet service to the church near his home in Exeter.
He discipled young converts, cared for the sick, and ad-
ministered relict for the poor. He was a competent
scholar as well, cxpounding Bible doctrine for a small
theological center and compiling the first Latin gram-
mar written in England. But in 718, Boniface left the
comfort and security of this life to hecome a missionary
to the savage Teutonic tribes of Germany. For thirty
years, he not only proclaimed to them the gospel of
light, he portrayed to them the gospel of life. Stories of
his courageous intervention on behalf of the mnocent
abound. He was constantly jeopardizing his own life for
the sake of the unborn, the young, the vulnerable, the
weak, the helpless, the aged, the sick, and the poor—
often throwing his body between the victims and their
oppressors, When he was well over seventy, he and his
et upon by heathen Frieslanders and

 

companions were
put to the sword.

    

The legendary generosity and charity of Wenceslas of Bo-
hemia is no mere Christmas fable. The young prince
lived a life fraught with conflict and tragedy. Both his
mother and grandmother—victims of court intrigue
and anti-Christian conspiracy—were murdered when he
was young. He himself was the object of several assassi-
nation attempts and revolts. Yet, despite such adversity,
he was a model Christian regent. He outlawed aban-
donment. He criminalized abortion. He reformed the
penal system. And he exercised great compassion on
the poor. When he was finally killed by rival heathen
elements in the court, he was grievously mourned by
his subjects.

Margaret of Scotland was another royal pro-life benefac-
tor. Granddaughter of Edmund Ironside, king of the
English, her family sought refuge in Scotland following
the Norman conquest in 1066. Four years later, she
