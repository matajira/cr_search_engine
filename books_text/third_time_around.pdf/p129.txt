Abominations and Admonitions: The Making of Modernism 117

duced steamships, railroads, streetcars, bicycles,
rollerskates, the air brake, the torpedo, telephones, tele-
graphs, transatlantic cables, harvesting machines, threshers,
cotton gins, cooking ranges, sewing machines, phono-
graphs, typewriters, electric lights, illuminating gas, photo-
graphs, x-rays, motion pictures, and cottonsced oil. Accord-
ing to journalist Edward Byrn, the comparisou of the start
of the century with the end of the century was “like the
juxtaposition of a distant star with the noonday sun.” And
popular historian M. J. de Forest Shelton exclaimed in all
truthfulness that there was “more difference between
Napoleon’s day and ours than between Napoleon’s and Ju-
lius Caesar's. One century against eighteen.”* Agreeing, the
English intellectual Frederic Harrison, said:

Take it all jn all, the merely material, physical, mechani-
cal change in human life in the hundred years, from the
days of Watt and Arkwright to our own, is greater than
occurred in the thousand years that preceded, perhaps
even the nwo thousand or twenty thousand years.”

Despite bittcr labor agitation, anarchist strikes, commu-
nist insurgencies, and the emergence of terrorism as a po-
litical weapon; despite attempts on the lives of the Prince
of Wales, the German Emperor, and the Shah of Persia;
despite successful assassinations of King Umberto of Italy
and President McKinley; despite the uproar of the Dreyfus
affair in France and the Robida scandal in Austria, despite
the imminent passing of the old world order—and its ac-
companying spector—despite everything, a robust optimism
pervaded the thinking of most Westerners. Their advances,
after all, had been stunning. So, for the most part, they
shared the opinion of the New York World in its prediction
that the new century would “mect and overcome all perils
and prove to be the best that this steadily improving planct
has ever seen.”®

Theodore Roosevelt had a saner view, a more realistic
view of his time. It was a view balanced by a Christian un-

 

 
