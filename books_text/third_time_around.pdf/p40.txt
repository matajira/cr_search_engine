28

THE FIRST TIME AROUND

decades of the second century he offered refuge to
thase fleecing the persecution against the church. He
succored the sick, cared for the poor, and saved aban-
doned children from certain death. Bede the historian
records his brutal martyrdom on Holmhurst Hill after
he wied to intercede on behalf of a pitiful family of
refugees.

Late in the third century, Afra of Augsburg developed a
ministry to the abandoned children of prisoners,
thieves, smugglers, pirates, runaway slaves, and brig-
ands. Herself a former prostitute, she cared for the de-
spised and the rejected with a special fervor, taking
them into her home, creating an adoption network,
and sacrificing all she had—that out of her lack they
might be satisfied. Ultimately, her work came under
the scrutiny of the authorities, and she was martyred
during the great persecution of Diocletian.

Gearge of Diospolis, patron of both England and Leba-
non, was a Christian soldier who gained fame after sev-
eral daring rescues of children in distress. He was
known as the “Dragonslayer,” not so much because of
exploits with rare and dangerous reptiles, but because
of his willingness to snatch innocent life out of the jaws
of death. Eventually, he too fell victim to Diocletian’s
wrath in the persecution of 304, and was beheaded in
Nicomedia. Later, innumerable legends made much of
his exploits—romantically associating him with damsels
and dragons—but it was his willingness to risk all for
the sake of the sanctity of life that earned him his place
in history.

Barlaam of Antioch was a cobbler for the imperial forces
who devoted all his free time to the care of orphans
and widows in his church. Because he himself had been
saved from the infanticide wall outside the city, he was
especially concerned for exposed children. Even
though he was not a pastor or church leader, his good
deeds were so widely known that the enemies of the
faith sought to have his witness silenced. During: the
