4 THIRD TIME AROUND

church to the fall of Rome in 476, and 1 have measured
the Medieval Era from the end of the empire in the West
to the fall of Constantinople in 1453—which also was the
year the Hundred Years War between England and France
ended. The Renaissance and Enlightenment is noted from
the dawning of the sixteenth century to the beginning of
the French Revolution in 1789; the Missions Movement,
from the dawning of the nineteenth century to the out
break of hostilities during the Great War in 1914. And I
have measured the modern era from the beginning of the
cwentieth century to the present. Of course, these broad
brush strokes overly simplify the remarkably complex jour-
ney of men and civilizations through the ages, but they will
have to do for our limited purposes here.

Legends, fables, and apocryphal stories abound in any
history—and church history is certainly not an exception.
Thus, finding uncontested and fully documented sources
has proven to be a daunting exercise. Early on in the proj-
ect, | determined to steer a middle course between aca-
demic skepticism and wide-eyed hagiography by accepting
only canonical or consensus accounts of the lives and activi-
tics of pro-life heroes.

Only direct quotations are cited in the endnotes, How
ever, a bibliography has been supplied for those readers who
wish to go back to some of the original sources that I relied
on for this survey, It is my hope, in fact, that this book will
serve as a prod for many to explore the inestimable riches of
our past.

The English author and lecturer, John H. Y. Briggs, has
poignantly argued that an historical awareness is essential
for the health and well-being of any society; it enables us to
know who we are, why we are here, and what we should do.
He says:
