Bibliographic Resources 207

of Life and te Do Good While They Live. Boston, MA: Sabbath
School Society, 1845.

MeCoy, Kathy. The Teenage Body Book: A Guide to Sexuality. New
York: Simon and Schuster, 1983.

McDowell, Josh. How to Help Your Child to Say “No” to Sexual Pres-
sure, Waco, TX: Word Books, 1987.

McDowell, Josh, and Dick Day. Why Wait? What You Need to Know
About the Teen Sexuality Crisis. San Bernadino, CA; Iere’s Life
Publishers, 1987.

McNeil, William H. Plagues and Peoples. London: Penguin Books,
1985.

Meeks, Wayne A., ed. Library of Early Christianity. 8 vols. Philadel
phia, PA: Westminster Press, 1987.

Meeks, Wayne A. The Moral World of the First Christians. Vol. 6 of
Library of Early Christianity. Philadelphia, PA: The Westmin-
ster Press, 1986.

Meluwer, Milton. Rescue: The Story of How Gentiles Saved Jews in the
Holocaust. New York: Harper and Row, 1988.

Merton, Andrew H. Enemies of Choice: The Right-io-Life Movement
and Its Threat to Abortion. Boston, MA: Beacon Press, 1981.

Mohr, James C. Abortion in America. New York: Oxford University
Press, 1978.

Montgomery, John Warwick. Slaughter of the Innocents, Westches-
ter, Il: Crossway Books, 1981.

 

Morris, Charles. The Marvelous Record of the Closing Century. Phila-
delphia, PA: American Book and Bible House, 1899.

Morse, Arthur D. While Six Million Died: A Chronicle of American
Apathy. Woodstock, NY: The Overlook Press, 1983.

Mosbacker, Barrett L. School-Based Clinics and Other Critical Issues
in Public Education. Westchester, IL: Crossway Books, 1987.

Muggeridge, Malcolm. Something Beautiful for God: Mather Theresa
of Calcutta. San Francisco, CA: Harper and Row, 1971.
