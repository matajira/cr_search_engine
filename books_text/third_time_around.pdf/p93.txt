To the Uttermost: The Great Missions Movement 81

nent proclaiming the good news of Christ. He under-
stood only too well, though, that the purpose of mis-
sions extended far beyond merely extending the offer
of heaven to the hapless and hopeless. In his widely
influential book, Missionary Travels and Researches in
Southern Africa, he wrote, “The indirect benefits, which
to the casual observer lie beneath the surface and are
inappreciable, in reference to the wide diffusion of
Christianity at some future time, are worth all the
money and labor that have been extended to produce
them.”!3 Among those “indirect benefits” in Living-
stone’s work were the dramatic curtailment of both the
native abortion and slave trades. A legend in his own
time and a paradigm of missionary efficacy ever since,
Livingstone demonstrated the power of the authentic
church in the face of the horrors of heathenism.

¢ When Hugh Goldie jommed a mission station in Old
Calabar on the West Coast of Africa early in the nine-
teenth century, he was horrified by many of the things
he found there. The living conditions of the people
were utterly deplorable. Their nutrition was abomin-
able. Their hygiene was disgraceful. Their social and
commercial arrangements were in utter disarray. But it
was their cavalier attitude to the sanctity of human life
that most disturbed him. Although they had recently
abandoned the centuries-old practice of human sacri-
fice, they still freely practiced abortion, abandonment,
and infanticide. Goldie was met with stiff opposition by
the tribal chicfs—and even by many of his fellow mis-
sionaries who felt that his pro-life convictions would
compromise their evangelistic efforts, He stood firmly
on the integrity of the whole counsel of God. Finally, as
a result of his lifelong crusade for life, tribal decrees in
1851 and 1878 banncd the terrible customs. He went
on to bis eternal reward having “run the race, fought
the fight, and held the course” (2 Timothy 4:7).

* Joseph Damien de Veuster was born in the fowlands of Bet-
gium in 1840. After posting a promising academic re-
cord, he disappointed his family’s expectations of a
