192 NOTES.

47. Ruth A. Tucker, Guardians of the Great Commission: The Story of Women
in Modern Missions (Grand Rapids, MI: Zondervan, 1988), 134.

48, Hunter B. Blakely, Religion in Shoes (Birmingham, AL: Southern Uni-
versity Press, 1989), 190-191.

49. Ibid.
50. Edgar Allan Poe, Complete Works (New York: Student's Library, 1966),
48.

Chapter Eight: Rescue the Perishing:
The New Pro-Life Emergence

1. Where there is life, there is hope.
2. Moody Monthly, May 1980.

3.Charles R. Swindoll, Sanctity of Life: The Inescapable Issue (Daltas, TX:
Word Publishing, 1990), xiv.

4. Quoted in Michael T. Ramey, Spurgeon: The Philanthropist (Kansas
City, MO: Calvinist Baptist Publication Society, 1951), 165.

5.R. C. Sproul, Abortion: A Rational Look at an Emotional Issue (Colorado
Springs, CO: NayPress, 1990), 156,

6. Ibid.

 

Chapter Nine: May the Circle Be Unbroken:
‘The Lessons of History

1, The deeds of the

2,Quoted in Martin Forbes, History Lessons: The Importance of Cultural
Memory (New York, Palamir Publications, 1981), 61.

3, Quoted in Calvin Long, The Progressive Bra: The Roaring Nineties to the
Roaring Twenties (New York: Birch Tree Press, 1958), 146,

4,A suzerian was a tribal chieftan in the ancient Middle East. Thus, the
patiern of suzerianity is the treaty structure that those chieftans im-
posed on conquered territories. Such treaties established 1) who was

2) how the society was to be ordered; 3) what the code

of ethics would be; 4) how judgment was to be exacted; and 5) how

succession or inheritance was to be arranged.

saints,

 

 

 

.For a delightful historical sightseeing tour of this view—and its im-
plications in ecclesiology—see Allen A. Fecrar, The Trinitarian Cove-
An Overview Through the Centuries (Oxford, UK: Campus Publ
ations Society, 1979).

nae

 

6.For the comparative merits of the different models, see Meredith
Kline, Treaty of the Great King: The Covenant Structure of Deuteronomy
(Grand Rapids, MI: Wm. B. Ecrdmans Publishing Co., 1963); and
