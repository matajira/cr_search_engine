wiki CONTENIS

6. Life and Liberty: The American Experience / 91

How the West Was Won + Law and Life + Protecting Rights +
Conclusion

Part 3: The Third Time Around
7. Abominations and Admonitions:
The Making of Modernism / 115

Progressivism « Turning a Deaf Ear + Standing Against the Tide +
Conclusion

8. Rescue the Perishing:
‘The New Pro-Life Emergence / 139
Whatever Happened to Evangelicals? » Reinventing the Wheel s
Seizing the Day + Conclusion

Part 4: The Next Time Around

9. May the Circle Be Unbroken:
The Lessons of History / 159

A Covenantal Legacy « Orthodoxy « The Church + Seruanthood « Ur-
gency» Patience»
Conclusion

10. Eternal Vigilance:
Handing on the Legacy / 177
Our Sense of History + Conclusion

Notes / 185
Bibliographic Resources / 195
Subject Index / 217

About the Author / 223
