PART 1

 

THE
FIRST TIME
AROUND

Time after time mankind is driven against

the rocks of the horrid reality of a fallen cre-

ation. And time after time mankind must

learn the hard lessons of history—the lessons

that for some dangerous and awful reason we

can't seem to keep in our collective memory.
Hilaire Belloc

Weak things must boast of being new, like so
many new German philosophies. But strong
things can boast of being old. Strong things
can boast of being moribund.

G. K. Chesterton
