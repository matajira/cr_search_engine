Introduction 7

as well. In. fact, we hope that it will prove to be helpful as a man-
ual for action (James 1:22).

Christian philosopher Cornelius Van Til has said, “the Bible is
authoritative on everything of which it speaks. And it speaks of
everything.” Even of such mundane matters as poverty and wel-
fare. Thus, to evoke Scripture’s blueprint for our cosmopolitan
culture’s complex dilemmas is not some naive resurrection of
musty, dusty archaisms. “More than that, blessed are those who
hear the word of God, and keep it!” (Luke 11:28) for, “.. . the
Scripture cannot.be broken” (John 10:35).
