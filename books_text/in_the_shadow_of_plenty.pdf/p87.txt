Charity Begins at Home 7

pray to Him, and He hears their prayers. No intermediary is nec-
essary. God's people are saints. This means that they have direct
access to God’s sanctuary through prayer. God never establishes
human institutional barriers between Himself and His people’s
prayers. God warns His people: “You shall not afflict any widow
or fatherless child. If you afflict them in any way, and they cry at
all to Me, T will surely hear their ery; and My wrath will become
hot, and [ will kill you with the sword; your wives shall be wid-
ows, and your children fatherless” (Exodus 22:22-24). But He does
require intermediary institutions in order to restrain evil men. The
three main ones are church, family, and state.

There was a brief time in Israel's history when its structure of
authority did resemble an empire: the period of the exodus.
Moses was in complete authority..He was their representative be-
fore the pharaoh and before God. But then God removed this
empire-like system. We can date the shift in Israel’s institutional
structure “from empire to Kingdom”: Jethro’s wisé counsel to his
son-in-law, and Moses’ wise concurrence.

Decentralization

Thus, the principle of decentralization that Jethro advocated
was not merely a matter of convenience, effected just for the sake
of. Moses. It was an essential part of God’s plan for His covenant
people. Israel was not to be an empire. It was to be a decentralized
Kingdom of righteousness ruled by the Sovereign Lord. This is
evident throughout Scripture.

When God gave instructions for the construction of the taber-
nacle, the plan for the division of labor was obvious. Clans,
tribes, families, and individuals all had their different tasks (Ex-
odus 35:1-35). The extravagant complexity of the work absolutely
precluded the idea that one or two or even a few might be able to
complete it alone. It would take teamwork. It would take a coordi-
nation of all the people’s gifts, resources, skills, and abilities.

Then, once the tabernacle was erected and dedicated, it was
necessary to divide the various tasks of maintaining and trans-
porting the massive structure. Again, ai/ the people would need to
