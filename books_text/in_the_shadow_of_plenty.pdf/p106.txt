90 In the Shadow of Plenty

laid on him. And if you tell this business of ours, then we will be
free from your oath which you made us swear.” Then she said, “Ac-
cording to your words, so be it.” And she sent them away, and they
departed. And she bound the scarlet cord iri the window. Then
they departed and went to.the mountain, and stayed there three
days until the pursuers returned. The pursuers sought them all
along the way, but did not find them { Joshua 2:12-22),

In essence, what Rahab was asking was that she and her
household be included in the circle of God’s covenant. The phrase
she used, begging that they “deal kindly” with her, generally ap-
plied to God’s covenant love with His people, or the people’s bond
with one another. .Rahab was not just trying to make a deal. She
wasn’t trying to pull an “fll-scratch-your-back-if-you-scratch-
mine” ploy. She was genuinely submitting herself to the God of
“heaven above and earth beneath.”

She had heard “of the great and marvelous works of the Lord”
(Deuteronomy 2:25; 7:23, 11:25; Revelation 15:3), and she delieved
what she had heard (Romans 10:17). She then made a confession
of faith (Romans 10:9), and began to do righteousness, authenti-
cating that faith with deeds (James 1:22, 3:25). Like the children
of Israel on the night of Passover, she marked her home with the
scarlet emblem of God’s provision (Exodus 12:7-13), and then took
refuge within, waiting for the redemption of her life by God’s
grace (Exodus 12:21-36).

She was a believer, any.way you cut it.

Later, when the walls of Jericho came a-tumblin’ down,
Rahab came out of her house, she and her entire household
(Joshua 6:22-23) to dwell “in the midst of Israel” for the rest of her
life (Joshua 6:25). From ‘then on, she would enjoy the privileges
of the covenant along with all the rest of the people, submitting to
its justice, partaking of its inheritance, and-resting in its security.

Rahab the harlot, though once separate from the Light and
Life of the ages, “alien from the commonwealth of Israel and
stranger to the covenant, having no hope and without God in the
world” (Ephesians 2:12), was all at once “brought near” (Ephesians
2:13). Because of god’s-providential grace, and her responsive
