The Unbroken Circle 89

So before they lay down, she came up to them on the roof, and
said to the men: “I know that the Lord has given you the land, that
the terror of you has fallen on us, and that all the inhabitants of the
land are fainthearted because of you. For we have heard how the
Lord dried up the water of the Red Sea for you when you came out
of Egypt, and what you did to the two kings of the Amorites who
were on the other side of the Jordan, Sihon and Og, whom you ut-
terly destroyed. And as soon as we heard these things, our hearts
melted; neither did there remain any more courage in anyone
because of you, for the’ Lord your God, He is God in heaven above
and on earth beneath” ( Joshua 2:8-11),

‘There could be no doubt about it, Rahab was a believer. She
was the most unlikely of candidates for the Kingdom of God, but
by her faith (Hebrews 11:31} and by her deeds (James 2:25), she
demonstrated the sincerity of her words.

The spies were impressed.

But then, Rahab went one step further still.

“Now therefore, I beg you, swear to me by the Lord, since [
have shown you kindness, that you also will show: kindness with
my father’s house, and give me a true token, and spare my father,
my mother, my brothers, my sisters, and all that they have, and
deliver our lives from death,” So the men answered her, “Our lives
for yours, if none of you tell this business of ours. And it shall be,
when the Lord has given us the land that we will deal kindly and
truly with you.” Then she let them down by a rope through the
window, for her house was on the city wall; she dwelt on the wall.
And she gaid to them, “Get to the mountain, lest the pursuers meet
you. Hide there three days, until the pursuers have returned.
Afterward you may go your way.” Then the men said to her, “We
will be blameless of this oath of yours which you have made us
swear, unless, when we come into the land, you bind this line of
scarlet cord in the window. through which you let us down, and
unless you bring your father, your mother, your brothers, and all.
your fathers’ household to your own home. So it shall be that who-
ever goes outside the doors of your house into the street, his blood
shall be on his own head, and we will be guiltless. And whoever is
with you in the house, his blood shall be on our head if a hand is
