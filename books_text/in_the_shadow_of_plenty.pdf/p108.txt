92 In the Shadow of Plenty

Of course, with special privilege came special responsibility. It
the sojourner was to réap the rewards of Israel’s theocratic repub-
lic, then he would have to function as a responsible, obedient citi-
zen, Like any other member of the covenant he would have to
honor the Sabbath (Exodus 20:10), the Day of Atonement (Levit-
icus 16:29), and the Feast of Unleavened Bread (Exodus 12:19).
He shared the prohibitions on eating blood (Leviticus 17:10-13),
immorality (Leviticus 18:26), idolatry (Leviticus 20:2), and blas-
phemy (Leviticus 24:16). He came under the shelter of God's
promises because he obeyed God's commands.

Nothing could stay God’s hand from blessing those who hon:
ored Him, just as nothing could stay His hand from judging those
who dishonored him. Thus, if the sojourner wished to share in the
privileges of God’s chosen people, he would have to honor God by
keeping His Word.

Rahab, though she was not from the company of the faithful,
came into the midst of it and submitted to God's rule, depending
on His Word to live. Though not of God’s household, she entered
in, abiding by its standards, and thus obtained its securities. Her
life and liberty could not have been had any other way. Israel was
an opportunity society, but only for those who observed ‘the
“rules.”

Likewise, Ruth was not from the company of the faithful. She
was a Moabitess. A sojourner. But the charity of God’s land of
bounty and table of bounty was not closed to her. She was given
the opportunity to labor, to work, to glean, because she had com-

*” mitted herself to the terms of the covenant, the God of the cove-
nant, and the people of the Covenant (Ruth 1:16-17). The struc-
tures of charity in Israe], designed to take care of their own, ex-
panded their reach to include her. Because the deeds of her mouth
and works of her hands proved that she would depend on the
‘Word of God to live, she was granted the privileges of the com
munity of faith. She was brought into the circle of the covenant,
yet the circle remained unbroken.

This gracious provision of God is illustrated time after time
throughout Scripture,
