50 In the Shadow of Plenty

Yes, you yourselves know that these hands have provided for
my necessities, and for those who were with me. I have shown you
in every. way, by laboring like.this, that you must support the
weak. And remember the words of the Lord Jesus, that He said,
“It is more blessed to give than to receive” (Acts 20:34-35).

He made certain to drive that same point home at every op-
portunity. He emphasized it when he wrote to the Corinthian
church the first time (1 Corinthians 4:12, 9:14-15), and again in his
second letter (2 Corinthians 11:7, 12:13). He noted his labors twice
in the first letter to the Thessalonian church (1 Thessalonians 2:9,
4:11), and again in his second letter (2 Thessalonians 3:8). And in
every other New Testament letter, save one, he touched upon the
issue either directly or. indirectly.

In fact, one of his favorite designations for his brethren in the
ministry was “co-laborer” or “fellow-worker,” just to make certain
that the point got across (Romans 16:3, 9, 21; Philippians 2:25,
4:3; Philemon 1, 24).

The Biblical Work Ethic

The truth of it is though, Paul wasn’t even all that unique
among the heroes of the faith in his commitment to diligent labor.
God very often used workmen, common ordinary laborers, in the
enactment of his glorious plan of redemption. He used shepherds
like Jacob (Genesis 30:31-43) and David (1 Samuel 17:15). He used
farmets like Amos (Amos.7:14) and Gideon (Judges 6:11). He
used merchants like Abraham (Genesis 13:2) and Lydia (Acts
16:14). He used craftsmen like Aquila (Acts 18:2-3) and Oholiab
(Exodus 31:6). He used artists like Solomon {1 Kings 4:32) and
Bezalel (Exodus 31:2-5). And the disciples He chose to convert the
nations of the earth in the first century were laborers, men of low
esteem: tax collectors and fishermen (Acts 4:13).

The reason for this is that work is the means by which God’s -
people obtain their promised dominion of the earth. Eventually,
the “craftsmen? will overcome the “horns” (Zechariah 1:18-21), In
other words, those who work diligently at their God-ordained
tasks will overcome those who grasp for power by force and decep-
