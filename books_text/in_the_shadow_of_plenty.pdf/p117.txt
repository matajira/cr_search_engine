Exceeding What Is Written 101

awake in vain, It is vain for you to rise up early, To sit up late, To
eat the bread of sorrows; For so He gives to His beloved sleep”
(Psalm 127:1-2).

The prayer life of Nehemiah pointed to the fact that he wanted
to be accountable. Accountable for his actions. Accountable for
his intentions. Accountable for the fruit of his labor. Accountable
to God. He wanted more than anything else to do God’s will.

He: had no desire to have God simply “okay” Azs plans. He
wanted to do what Ged wanted him to do, nothing more and.noth-
ing less. Prayer held him accountable to that. It gave him the
resolve to stick to that. Prayer gave him access to God’s will, God’s
way, God’s purposes, and God's plan.

Nehemiah was confident that God would give him success
(Nehemiah 2:20). He was sure that God would give him strength
(Nehemiah 6:9). He knew that God would give him favor (Nehe-
miah 2:18). He was, in fact, absolutely unwavering in his opti-
mism, because the work was conceived by God, not by him
(Nehemiah 2:12), It was God’s project, not his (Nehemiah 7:5}.

Nehemiah didn’t pray in order to get something. He prayed in
order to ée something ( James 4:3). He wanted to be conformed to
God's will. He wanted to de used in God’s work. He wanted to be
obedient.

Prayerful Obedience

Nehemiah’s conformity to God's purposes is evidenced not
only in his prayer life, but in his emphasis on the Scriptures as
well. In fact, his prayer life ultimately-led him to the Word with
renewed commitment, Because he so desired to do only the will of.
Ged, and because the Bible is the written and revealed will of
God, it was only natural that Nehemiah’s prayer life would be in-
extricably tied to Scripture.

He gave prominent place to the work of God’s Law in the life
of the people (Nehemiah 8:1-8). He gave the Bible proper perspec-
tive (Nehemiah 8:9) and appropriate’ priority (Nehemiah 6-1-3).
He encouraged its reading (Nehemiah 8:18), its exposition (Nehe-
miah 8:13), and its application (Nehemiah 8:14-18). He made cer-
