82 In the Shadow of Plenty

men were charged with thé task of caring for the physical needs of
the membership. They made certain that food distribution was
even and efficient (Acts 6:1-6), they cared for the special needs of
the widows (Acts 6:1; 1 Timothy 5:2-16), and they took care of any
basic pastoral needs that might distract the elders from their work
of teaching and intercession (Acts 6:4). These deacons were to be
“men of good reputation, full of the Holy Spirit and wisdom” (Acts
6:3). They were to be “reverent, not double tongued, not given to
much wine, not greedy for money, holding the mystery of the faith
with a pure conscience” (1 Timothy 3:8-9). They were to be “hus-
bands of one wife, ruling their children and their own houses well”
(1 Timothy 3:12). After all, if they could not manage their own
households, how could they be expected to manage the household
of faith? Possible candidates for the office of deacon were to be
“tested,” then, if they were found to be “beyond reproach,” they
could begin their service (1 Timothy 3:10).

‘These were stern requirements.

And for good reason. “For those who have served well as
deacons obtain for themselves a good standing and great boldness
in the faith which is in Christ Jesus” (1 Timothy 3:13).

The work of the deacon, and thus the office of the deacon, was
extremely important to the health and welfare of the church. It was
not to be taken lightly.

If the church could not take care of its own, then what was to
be made of its claim of dominion over the whole earth? If the
church could not nurse its own, how could it claim to be the nur-
sery of the kingdom? If the church could not structure itself so
that it encouraged and even facilitated good deeds, how could it
possibly bear a message of glad tidings and great joy, of peace on
earth and good will toward men?

Just as judgement begins with the house of God (2 Corin-
thians 5:10), so charity must begin with the house of God (1 Corin-
thians 13:1-13). In the company of the faithful, charity and its
fruits must be evidenced,

The disciples in the early church knew this, and structured
their life together accordingly. Thus, they could readily say,
