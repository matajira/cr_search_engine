16 In the Shadow of Plenty

What did charity prove in the life of Jesus? It proved that He
cared for men. .It proved that He loved them. It proved that He
was willing to put His life on the line. It proved that He was being
fully obedient to His Father. Finally, it proved that His Words had
authority, because they were being put into action.

Shortly after the announcement of His messianic authority in
the synagogue at Nazareth, Jesus healed.a paralyzed man. Jesus
stood in front of the Pharisees and lawyers, who were watching to
see if He would in any way commit a transgression of God’s law.
The paralyzed man had been brought to Him in a unique way:
his friends had broken a hole in the roof and lowered him down,
to avoid the crowd around Jesus.

So when He saw their faith, He said to him, “Man, your sins
are forgiven-you.” And the scribes and the Pharisees began to rea-
son, saying, “Who is this who speaks blasphemies? Who can for-
give sins but God alone?” But when Jesus perceived their thoughts,
He answered and said to them, “Why are you reasoning in your
hearts? Which is easier, to say, ‘Your sins are forgiven you,’ or to
say, ‘Rise up and walk’? But that you may know that the Son of
Man has power on earth to forgive sins”— He said to the man who
was paralyzed, “I say to you, arise, take up your bed, and go to
your house.” Immediately he rose up before them, took up what he
had been lying on, and departed to his own house, glorifying God.
And they were all amazed, and they glorified God and were filled
with fear, saying, We have seen strange things today! (Luke
5:20-26).

Jesus first drew attention to the man’s sins. Judgement, Then
He forgave him. Grace. And finally, in demonstration of His au-
thority to judge and forgive, He raised the man up. Charity, Word
was accompanied by deed,

Notice, that after Christ ministered to the man in this fashion
the entire crowd was “amazed.” They all “glorified God and were
filled with fear.” Seeing Word and deed together, they said with sheer
astonishment, “We have seen strange things today.”

Jesus authenticated the words of His mouth with the deeds of
