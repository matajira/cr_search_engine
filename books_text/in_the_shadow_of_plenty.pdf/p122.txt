106 In the Shadow of Plenty

Summary

Nehemiah was a man of prayer. He looked to God and to God
alone for his direction.

He was also a man of the Word. He knew that it was essential
to the success of any endeavor to operate within the limits set by
Scripture.

God tells us what to do, when, where, how, and why. To.fol-
low any other course than the course God has set for us insures ut-
ter failure. Nehemiah understood that.

Unfortunately, most of our efforts on behalf of the poor are en-
tirely uninspired, They exceed what is written, or they fall short
of what is written, or they ignore what is written. And so they fail.

Poverty “relief” programs always lead to more poverty unless
they seek and do God’s will. Thus in order to be effective, our char-
ity outreaches must be marked by diligent prayer and strict obe-
dience to Scripture.

No matter what task we undertake,. Nehemiah’s example is in-
structive. We must not add to, or take away from, God's revelation
{Deuteronomy 4:2; Proverbs 30:6; Revelation 22:18-19). God has
given us His blueprints. Let’s just follow the plan. Nothing more.
Nothing less.
