4
HI HO, HI HO, IT’S OFF TO WORK WE GO

For even when we were with you, we commanded you this: if
anyone will not work, neither shall he eat” (2 Thessalonians 3:10).

When Jesus issued a call for “laborers” for the harvest (Luke
10:2), he sure got one in Paul of Tarsus!

Paul labored in times of poverty and in times of prosperity
(Philippians 4:12), He labored amidst persecution and popularity
(Acts 14:8-19), He labored at home and abroad (Acts 11:25-30).
He labored in season and out (2 Timothy 4:1-8), He labored with
any number of friends and helpers, and he labored alone (2 Tim-
othy 4:9-12). He continued to labor though beaten with rods,
stoned, and shipwrecked (2 Corinthians 11:25). He labored on fre-
quent journeys despite dangers from rivers, dangers from rob-
bers, dangers from the Jews, dangers from the Gentiles, dangers
in the wilderness, dangers on the sea, and dangers from false
brethren (2 Corinthians 11:26). He labored through many hard-
ships: sleepless nights, hunger, thirst, cold, and exposure (2 Cor-
inthians 11:27). Paul labored on and on and on.

He learned the importance of labor early in his life, long before
his conversion. Though he was set aside by his education and skills
to be a teacher of the Law (Acts 22:3), he still was required to learn a
trade. And tentmaking was the trade he learned (Acts 18:2-3).

Tentmaking is hard work.

Later, when he began to labor for the harvest, he continued'to
support his ministry through tentmaking. Speaking to the Ephes-
ian elders, he said,

49
