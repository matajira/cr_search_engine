66 In the Shadow of Plenty

our nation a “Land of Opportunity” once again. We must integrate
gleaning opportunities into every aspect of our society: in the pri-
vate sector, in the marketplace, and in the church.
