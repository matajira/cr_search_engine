Dominion by Service 43

and as a result, the entire royal fleet was lost in Ezion- Geber
(2 Chronicles 20:35-37).

God’s intention was not simnply.to make Israel insular and iso-
lationist (Isaiah 56:9-12). Neither were His prohibitions an ex-
pression of prejudice (Acts 10:34). They were moral, not political.
They were ethical, not cultural.

Israel was to nurture the nations of the earth (Isaiah 49:6).
Israel was to be a light to the nations (Isaiah 42:6). That meant
that God’s people had to be a “peculiar people” (Deuteronomy
14:2).. They had to be a “nation of priests” unto the Lord (Exodus
19:6). They had to be utterly uncompromised and uncompromis-
ing (Deuteronomy 18:9-13).

Otherwise, they would not be able to lead all the peoples of the
earth to truth and righteousness (Matthew 15:14). They would not
be able to be the “nursery of the Kingdom” ( Jeremiah 5:30-31).
Since dominion is accomplished through service, whenever they
allowed evil doers to serve as their security, they yielded their au-
thority and dominion to them.

Thus, if believers are “to go and make disciples of the nations,
baptizing them in the name of the Father and the Son and the
Holy Spirit, teaching them to-observe all that (Christ has) com-
manded” (Matthew 28:19-20), then we must abstain from all unholy
alliances, relying solely and completely upon the Lord.

The Nursery of the Kingdom

When the church inherited Israel’s charge to nurse the nations
of the earth with the waters of life (Revelation 22:17), the bread of
life (John 6:31; 1 Corinthians 11:24), and the Word of life (I John
1:1), she also inherited the prohibition against unholy alliances.

Do not be unequally yoked together with unbelievers. For what
fellowship has righteousness and Jawlessness? And what commun-
ion has light with darkness? And what accord has Christ with
Belial? Or what part has a believer with an unbeliever? And what
agreement has the temple of God with idols? For you are the tem-
ple of the living God. As God has said: “T will dwell in them and
walk among them. I will be their God, And they shall be My peo-
