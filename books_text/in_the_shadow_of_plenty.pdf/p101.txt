Uplifting the Uplifters 85

dispossessed and the stranger in need (Romans 12:13; 1 Kings
:7:7-16). They were to pool their resources with other families in
order to tackle large charitable projects, too monumental for any
one family (Acts 4:32-35; 2 Corinthians 8:1-5).

According to a survey conducted by U.S. News and World Report,
the family now ranks 17th in the list of “institutions that affect the
nation,” following civil government, television, bureaucracy, news-
papers, and advertising! But to the early pioneers of the Christian
faith, the family ranked tst. The church relied on a responsibly and
charitably structured family in order to fulfill its vision.

In every way, shape, and form imaginable, the church was ~
established in abiding love so that charity could flourish. Congre-
gational, interpersonal, and familial structures were created so
that not only would charity begin at 4ome, but that charity would
begin at home!

Changing Priorities

Somehow in the intervening years we've lost that emphasis.

We haven't had to face a famine. At least, not here in Amer-
ica. Just a little unemployment.

And even at that, we’ve been buried under an avalanche of
need. With no apparatus to deal with it.

The church has failed her widows, her. orphans, her elderly,
her ill and infirmed.

The church has failed to disciple young men to live lives of
diligence, industry, and productivity.

The church has failed to mobilize deacons for the work they
are supposed to do.

The church has failed to catalyze the gifts of the body for good
deeds. .

The church has failed to train her families for victory amidst
hardship and calamity.

Instead, we’ve placed: a heavy premium on such things as
building programs and media ministries. Spiritual gifts have been
harnessed for personal peace and satisfaction rather than for serv-
ing the flock.
