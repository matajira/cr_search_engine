What the Government Should Do 133

must be ousted from their places of influence and power. We must
vote them out. But, that may-take quite a while.

So, what do we do in the meantime? And how do we prepare
for the transition?

First, the church must take up her philanthropic mantle now,
even before the welfare state has been dismantled. No real politi-
cal action can be taken to transfer welfare responsibilities to the
church if the church is unprepared, inexperienced, and unin-
spired. Conservatives have been rabble-rousing for years about
how churches and families can do a better job of taking care of the
poor than the government. It’s time to prove i. No one in Wash-
ington is going to take significant steps toward eliminating public
welfare pragrams if we don’t start developing real and reliable alter-
natives. If welfare were abolished tomorrow, millions of Americans
would suffer calamitously. The short-term chaos would virtually
offset any long-term benefits. The church isn’t ready to take up
her responsibilities, so she needs to get ready, Only then can we ex-
pect tangible political reform.

Second, we must begin a program of privatization similar to
the Thatcher initiatives in Great Britain, For more than five years
now the ruling Conservative Party there has been slowly, quietly,
and efficiently returning billions of dollars of government prop-
erty and resources to the private sector. Socialized welfare pro-
grams are gradually but systematically being surrendered into the
hands of the citizenry. The entire economy is inching toward the
healthy decentralized free market system that made Britain great
in the first place. Similar measures in the U.S. could see the grad-
ual transfer of public and subsidized housing to the tenants. They
would become homeowners instead of federal dependents. Don’t
you know that the quality of those projects would instantly im-
prove? Privatization could arrange for the transfer of federal
properties to various functioning charities. It could also provide
for the transfer of responsibility from the well-heeled but ineffect-
ive government bureaucracies to the cash-poor but efficiently run
private sector. Washington would save billions, the charities
would gain essential properties and resources, and the poor would
