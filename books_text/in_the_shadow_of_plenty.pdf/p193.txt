What Are Biblical Blueprints? 177

‘We present the case that God offers comprehensive salvation — re-
generation, healing, restoration, and the obligation of total social
reconstruction because the world is in comprehensive sin.

‘To judge the world it is obvious that God has to have stand-
ards. If there were no absolute standards, there could be no
earthly judgment, and no final judgment because men could not
be held accountable.

(Warning: these next few paragraphs are very important.
They are the base of the entire Blueprints series. It is important
that you understand my reasoning. I really believe that if you un-
derstand it, you will agree with it.)

To argue that God's standards don’t apply to everything is to
argue that sin hasn’t affected and infected everything. To argue
that God’s Word doesn’t give us a revelation of God’s requirements
for us is to argue that we are flying blind as Christians. It is to
argue that there are zones of moral neutrality that God will not judge,
either today or at the day of judgment, because these zones some-
how are outside His jurisdiction. In short, “no law-no jurisdiction.”

But if God does have jurisdiction over the whole universe,
which is what every Christian believes, then there must be univer-
sal standards by which God executes judgment. The authors of
this series argue for.God’s comprehensive judgment, and we declare
His comprehensive salvation. We therefore are presenting a few of
His comprehensive blueprints.

The Concept of Blucprints

An architectural blueprint gives us the structural require-
ments of a building. A blueprint isn’t intended to tell the owner
where to put the furniture or what color to paint the rooms. A
blueprint does place limits on where the furniture and appliances
should be put—laundry here, kitchen there, etc.—but it doesn’t
take away our personal options based on personal taste. .A blue-
print just specifies what must be done during construction for the
building to do its job and to survive the test of time. It gives direc-
