The Foundation of Peace 117

peace. We may have to work with few, or even no resources. Like
Jonathan (1 Samuel 14:6). We may have to improvise, utilizing
less than perfect conditions and less than qualified workers and less
than adequate facilities. Like Nehemiah (Nehemiah 1:20). We
may have to battle the powers that be, the rulers and the princi-
palities. Like Peter, James, and John (Acts 4:20). We may have to
go with what we've got, with no support, no notoriety, and no co-
operation. Like Jeremiah ( Jeremiah 1:4-10). We may have to start
“in weakness, in fear, and in much trembling” (1 Corinthians 2:3),
without “persuasive words of wisdom” (1 Corinthians 2:4). Like
the Apostle Paul (1 Corinthians 2:1).

Instead of allowing their limitations and Habilities to discour-
age and debilitate them, the heroes of the faith went to work—
God’s power being made manifest in their weakness (1 Corin-
thians 1:26-29).

It is time for us to go to work,

Dominion doesn’t happen overnight. Peace isn’t won in a day.
So the sooner we get started, the better off we'll be. The sooner we
get started, the quicker the victory will come. In order to get from
here to there, we need to set out upon the road. At the very least.

There will never be an ideal time to degin the work of charity.
Money is a/ways short. Volunteers are always at a premium. Facil-
ities are always either too small, or too inflexible, or in the wrong
location, or too expensive. There is never enough time, never
enough energy, and never enough resources,

So what?

Our commission is not dependent upon conditions and restric-
tions. Our commission is dependent only upon the unconditional
promises of Gad’s Word. God has called us to peace (1 Corinthians
7:15),-to be peacemakers (Matthew 5:9), “so then let us pursue the
things that make for peace” (Romans 14:9).

We should just go. Do what we ought to. We should make
peace. Starting now.

There shall be no poor among you, since the Lord will surely
bless you in the land which the Lord your God is giving you to
possess as an inheritance, if only you carefully obey the voice of the
Lord your Gad, te observe with care all these commandments
which I command you today (Deuteronomy 15:4-5 NAS).
