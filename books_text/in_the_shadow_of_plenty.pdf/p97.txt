Uplifting the Uplifters 81

through us to God. For the administration of this service not only
supplies the needs of the saints, but also is abounding through
many thanksgivings to God, while, through the proof of this minis-
try, they glorify God for the obedience of your confession to the
gospel of Christ, and for your liberal sharing with them and all
men, and by their prayer for you, who long for you because of the
exceeding grace of God in you. Thanks be to God for His indes-
cribable gift (2 Corinthians 9:6-15). °

The disciples understoad clearly that in the providence of God
they had been given a tremendous opportunity to give evidence to
the world of the power of the Gospel. For them, “love your neigh-
bor as yourself” (James 2:8) wasn’t simply a slogan. It was the
authenticating mark of their faith (1 John 4:12),

Again, Paul commanded them, .

Bear one. another's burdens, and so fulfill the law of Christ . . .
Do not be deceived, God is not mocked; for whatever a man sows,
that he will also reap. For he who sows.to his flesh will of the flesh
reap corruption, but he who sows to the Spirit will of the Spirit
reap everlasting life. And let us not grow weary while doing good,
for in due season we shall reap if we do ‘not lose heart. Therefore,
as we have opportunity, let us do. good to all, especially to those
who are of the household of faith (Galatians 6:2,7-10).

To the great aggravation of the enemies of the Gospel, the dis-
ciples did indeed bear one anothers’ burdens, even in times of
great hardship amidst famine conditions, so that “nor was there
anyone among them who lacked” (Acts 4:34).

Facilitating Good Deeds

The early church was ready, willing, and able to “do good to
all, especially to those who (were) of the household of faith” (Gala-
tians 6:10), because their very structure —congregational, famil-
ial, and interpersonal—encouraged and even facilitated such
good deeds.

Besides the elders, who were ordained to the task of teaching
(Titus 1:9), guarding (Acts 20:28}, and ruling (Hebrews 13:17),
the church was also served by deacons (i Timothy 3:8-13). These
