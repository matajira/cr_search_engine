74 In the Shadow of Plenty

“God sets the lonely in families” (Psalm 68:6 NIV).

Gleaning was operated and regulated not by the magistrates
of the state, not by philanthropic agencies, and not even by a local
bureaucracy. It was operated and regulated by individual families
under the rule of God’s Law (Ruth 2:4-16). This meant that the
landowners could dispense charity unhindered and unencum-
bered. Accountability and flexibility were made possible. Local
conditions could be taken into account, and personal attention
was maximized. By thus keeping charity decentralized, de-
institutionalized, and family-centered, everyone was saved from
the hassles of graft, corruption, and red tape.

Like gleaning, the interest-free loan was another aspect of
Biblical charity that was operated and regulated by individual
families under the rule of God's Law (Exodus 22:25-27; Leviticus
25:35-37). There was no supervision by an over-arching state
agency, there were no administrators, there were no forms to fill
out, no lines to stand in, and there was no standardized institu-
tion to conform to, The loans were simply the means by which
godly families met pressing needs with available resources, above
and beyond the requirements of law or responsibility.

Of course, besides gleaning and interest-free loans, Biblical
charity could also be dispensed through private giving. Obvi-
ously, this too was a function of the family, independent of any
outside influence or regulation except the influence and regulation
of God’s Law. This was the approach the Good Samaritan took on
the road to Jericho (Luke 10:30-37), and it was the impulse that
motivated Barnabas and other philanthropists. in the first Jerusalem
church when emergency relief became necessary (Acts 4:32-37),

Charity really: does begin at home! But it doesn’t end there.

Notice: just because charity was not under the regulatory jur-
isdiction of the state did not mean that families were free to do (or
not to do) whatever they pleased. Gleaning was operated and reg-
ulated by individual families under the rule of God's Law. Private giv-
ing was operated and regulated by individual familiesunder the rule
of God's Law.

A natural family, an Adamic home, is just as impotent as a
