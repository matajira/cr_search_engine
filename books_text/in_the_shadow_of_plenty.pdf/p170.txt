154 In.the Shadow of Plenty

will cease; whether there is knowledge, it will vanish away (1 Cor-
inthians 13:4-8).

Love involves “compassion, kindness, humility, gentleness,
and patience” (Colossians 3:12-14). It involves singlemindedness
(Philippians 2:2). It involves purity of heart, a good conscience,
and “a faith unfeigned” (1 Timothy 1:5). It involves diligence
(2 Corinthians 8:7), knowledge (Philippians 1:9), service (Gala-
tians 5:13), righteousness (2 Timothy 2:22), sound judgement
(Philippians 1:9), and courtesy (1 Peter 3:8). Love is the royal Law
(James 2:8). It is the capstone of Godly character (1 Corinthians
13:13). It is the message that we have heard from the beginning
(1 John 3:11).

Interestingly, the word that the King James translators chose
to use in each of these passages was “charity.” That word catches a
special dynamic of meaning that dove” has lost in our day of muddy
definitions. “Charity” accurately communicates the fact that love is
not simply a feeling. Love is something you de. Love is an action.
Love is a commitment, and an obligation, and.a responsibility. Love is
charity.

Thus, we are to prove “the sincerity of love” (2 Corinthians
8:8), and we are to do it by following “after charity” (1 Corinthians
14:1), by having “fervent charity” among ourselves (1 Peter 4:8),
and by being.an example to others “in charity” (1 Timothy 4:12).
For “charity shall cover the multitude of sins” (1 Peter 4:8).

Though I speak with the tongues of men and of angels, and
have not charity, 1 am become as sounding brass, or a tinkling
cymbal. And though I have the gift of prophecy, and understand
all mysteries, and all knowledge; and though I have all faith, so
that I could remove mountains, and have not charity, I am noth-
ing. And though I bestow all goods to feed the poor, and though I
give miy body to be burned, and have not charity, it profiteth me
nothing (1 Corinthians 13:1-3 KJV).

There is simply no getting around it.
It is a Christian necessity to do the work of charity, to love not
just “in word or in tongue, but in deed and in truth” (1 John 3:18).
