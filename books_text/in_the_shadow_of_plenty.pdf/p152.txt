12
WHAT THE CHURCH SHOULD DO

In the mid-nineteenth century, the prominent English econo-
mist John Stuart Mill pointed out that the difficulty of leaving
relief entirely to private charity is that such charity operates “un-
certainly and casually... . (it) lavishes its bounty in one place,
and leaves people to. starve in another.”

His charge has yet to be adequately answered. That must change.

If conservative Christians are going to oppose the federal dole,
then they must come up with workable alternatives. They must
realize that it is foolish and fruitless to try and fight something
with nothing.

We need alternatives, We need models,

Application must be undertaken. It is not enough to know that
the socialistic and humanistic experiments in the “war on poverty”
have been dismal failures. It is not enough to know that the “social
Gospel” approach to ministry proposed by liberal Christians is in
adequate and impotent. It is not.even enough to formulate
dynamic theological affirmations from our academic. ivory towers.
We must get our hands dirty in the work of caring for the poor,
correctly, sanely, and Biblically. We must translate the basic prin-
ciples of Biblical charity into basic strategies for Biblical charity.

Despite the magnitude of the Biblical evidence to the contrary,
the church has, over the last generation, acted as if charity was not
particularly important. We have allowed the powers and princi-
palities, the governments and bureaucracies to steal away from us
this particular duty, simply because we haven't seen it as central
to the Gaspel mission.

136
