FOR FURTHER READING

David Chilton, Productive Christians in an Age of Gutlt-Manipulators,
(Tyler, Texas: Institute for Christian Economics, 1981; 3rd edi-
tion, 1985).

Gary DeMar, God and Government: A Biblical and Historical Study,
(Adanta, Georgia: American Vision Press, 1982).

. God and Government: Issues in Biblical Perspective,
{Adanta, Georgia: American Vision Press, 1984).

—____. God and Government: The Restoration of the Republic,
(Atlanta, Georgia: American Vision Press, 1986).

George Gilder, Wealth and Poverty, (New York, New York: Basic
Books, 1981).

George Grant, Bringing in the Sheaves: Transforming Poverty into Pro-
ductivity, (Atlanta, Georgia: American Vision Press, 1985).

. The Dispossessed: Homelessness in America, (Fort
Worth, Texas: Dominion Press, 1986).

——_____.._ Ts the Work: Action Alternatives for Biblical Charity,
(Atlanta, Georgia: American Vision Press, 1987).

James B. Jordan, The Sociology of the Church, (Tyler, Texas: Geneva
Ministries, 1986).

Lawrence Mead, Beyond Entitlement: The Social Obligations of Citizen-
ship, (New York, New York: The Free Press, 1986).

Charles Murray, Losing Ground: American Social Policy 1950-1980,
{New York, New York: Basic Books, 1984).

Gary North, The Sinai Strategy: Economics and the Ten Commandments,
(Tyler, Texas: Institute for Christian Economics, 1986).

159
