Word and Deed Evangelism
Following God’s model he said:

Cry aloud, spare not; Lift up your voice like a trumpet; De-
clare to My people their transgression, And the house of. Jacob
their sins . . . Is this not the fast that I have chosen: To loose the
bonds of wickedness, To undo the heavy burdens, To let the op-
pressed.go free, And that you break every yoke? Is it not to share
your bread with the hungry, And that you bring to your house the
poor-who are cast out; When you see the naked, that you cover
him, And not to hide yourself'from your own flesh? Then your
light will break out like the morning, Your healing shall spring
forth speedily, And your righteousness shall go before you; The
glory of the Lord shall be your rear guard. Then you shall call, and
the Lord will answer; You shall cry, and He will say, “Here I am.”
Tf you take away the yoke from your midst, The pointing of the
finger, and speaking wickedness, If you extend your soul to the
hungry, And satisfy the afflicted soul Then your light shall dawn
in the darkness, And your darkness shall be as the noonday. The
Lord will guide you continually, And satisfy your soul in drought,
And strengthen your bones; You shall be like a watered garden,
And like a spring of water, whose waters do not fail. Those from
among you shail build the old waste places; You shall raise up the
foundations of many generations; And you shall be called the
Repairer of the Breach, The Restorer of Streets to dwell in (Isaiah
58:1, 6-12).

13

God made His evangelistic program clear to Isaiah. First, he

was to tell the people of Judah that they were in sin: “Declare

to

My people their transgression.” Second, he was to reveal the way
out: They were to fast in repentance. Finally, he was to point them

toward righteous charity: They were not to starve themselves in a

ritual fast, but to loosen the bonds of wickedness, to let the op-
pressed go free, to feed the hungry, to invite the homeless into

their homes, to provide clothing for the naked.

Once again, here is God’s plan of evangelism: First, announce
the judgement of sin; second, proclaim the good news of hope;
and third, take up the work of charity. First, wrath against sin. Sec-
ond, grace covering over sin. And third, charity soothing the hurts of sin.
