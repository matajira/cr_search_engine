What the Church Should Do 141

Why? Because Christians have abandoned the world. We
have abandoned our God-ordained commission to be salt and
light (Matthew 5:13-16), We have abandoned our dominion man-
date (Genesis 1:28). Instead, we have emphasized a view of spirit-
uality that is based on the pagan teachings of Plato. Accordingly,
a sharp division is generally made between the “spiritual” and the
“material.” Since the “spiritual” realm is considered superior to the
“material,” all things physical, all things temporal, and all things
earthly are spurned. Art, music, and ideas are ignored except for
their value as propaganda. Activities that do not significantly con-
tribute to piety are neglected. The Christian intellect is held sus-
pect, if not altogether shunned. And pleasures of the flesh, re-
gardless of how innocent or sacred, are condemned outright.

According to this popular perspective; all our efforts should be
directed toward producing individualistic piety. Bible study,
prayer, church attendance, and evangelism compose the totality
of tasks for the Christian. Anything and everything else is a dis-
traction and is worldly. Certainly, with this fortress mentality, we
would never condone confronting the prevailing assumptions of
the fallen culture nor constructing a program of reform for society.

As righteous as all this may sound at first hearing, it is patently
unScriptural. Although the Biblical mandate includes as an in-
tegral aspect of its plan for victory deep devotion, piety, and holi-
ness (Matthew 5:48), it also requires us to think hard about the
nature of Christian civilization (1 Peter 1:13), to try to develop
Biblical alternatives to humanistic civilization (Matthew
18:15-20), and to prophesy Biblically to the cultural problems of
our age (Isaiah 6:8). These things, too, are true piety.

According to the Bible, “The earth is the Lord’s, and all its
fullness” (Psalm 24:1), But, in the minds of many believers, only
the spirit is the Lord’s. All else is tainted beyond reclamation by
the stench of sin. According to the Bible, Jesus is Lord over the to-
tality of life (Colossians 1:15-17; Hebrews 1:2-3). But, in the minds
of many believers, Jesus is Lord, but only over a “religious cubby-
hole” in life. According to the Bible, Christians are to confront,
transform, and lead human culture (Matthew 5:13-16). But in the
