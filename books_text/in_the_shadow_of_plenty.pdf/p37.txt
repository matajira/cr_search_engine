Word and Deed Evangelism 21

For by grace you have been saved through faith, and that not
of yourselves; it is the gift of God, not of works, lest anyone should
boast. For we are His workmanship, created in Christ Jesus for
good works, which God prepared beforehand that we should walk
in them (Ephesians 2:8-10).

God ‘saves us by grace. ‘There is nothing we can do to merit
His favor. We stand condemned under His judgement. Salvation |
is completely unearned (except by Christ), and undeserved (ex-
‘cept to Christ). But we are not saved capriciously, for no reason
and no purpose. On the contrary, “we are His workmanship, cre-
ated in Christ Jesus for good works.” We are “His own possession”
set apart and purified to be “zealous for good deeds.” Word and
deed are inseparable. Judgement is answered with grace. Grace is
answered with charity. This is the very essence of the evangelistic
message.

So, Paul tells Titus he must order his fledgling ministry
among the Cretans accordingly. He himself was “to be a pattern of
good deeds” (Titus 2:7). He was to teach the people “to bé ready
for every good work” (Titus 3:1). The older women and the
youngér women were to be thus instructed, so “that the word of
God may not be dishonored” (Titus 2:5); and the bondslaves,
“that they may adorn the doctrine of God our Savior in all things”
{Titus 2:10). They were alt to “learn to maintain. good works, to
meet urgent needs, that they may not be unfruitful” (Titus 3:14).
There were those within the church who professed “to know God,
but in works they deny Him, being abominable, disobedient, and
disqualified for every good work” (Titus 1:16). These, Titus was to
“rebuke . . . sharply, that they may be sound in the faith” (Titus
1:13). He was to “affirm constantly, that those who have believed
in God should be careful to maintain good works” (Titus 3:8).

Asa pastor, Titus had innumerable tasks that he was responsi-
ble to fulfill. He had administrative duties (Titus 1:5), doctrinal
duties (Titus 2:1), discipling duties (Titus 2:2-10), preaching du-
ties (Titus 2:15), counseling duties (Titus 3:1-2), and arbitrating
duties (Titus 3:12-13). But intertwined with them all, fundamental
to them all, were his charitable duties.
