150 In the Shadow of Plenty

dominion through seroice. It also needs to recover a full understand-
ing of just what the Bible demands —from families, civil govern-
ments, and churches. It must restore the proper use and teaching
of the Sacraments, and it must train families to exercise charity. It
must stand as the protector of the families of church members. It
must not assume more responsibility than is proper, and therefore
it must limit its charitable giving, for it is not to accumulate un-
warranted power. Power flows to those who exercise responsibil-
ity, as the creators and defenders. of the messianic state under-
stand so well.

Above all, the church must use its teaching ministry to chal-
lenge the moral and economic foundations of modern humanism,
which in turn undergirds socialism. The messianic state must be
challenged from the moral high ground before it attempts. to
weaken the church by replacing the church’s welfare functions
and the welfare functions of families.

Summary

Due to passive and inadequate theologies within the church,
Western civilization is facing a series of grave crises that threaten
to destroy everything that we hold near and dear.

The enemies of the Gospel have been able to.gain control of
our culture simply because the church has abandoned the Word—
the Word revealed, the Word incarnate; and the Word’ made
manifest.

The central agenda of the church then for the days ahead is
simple: Return to the Word.

When the church adheres to the Word she can help the poor in
ways no other institution— public or private— possibly can: she
renews men’s minds, she. readjusts them to reality, and she
reforms their lifestyles.

Thus, the church must reassert her proper place in society:
mobilizing deacons, instructing families, discipling sinners, en-
couraging other churches, establishing shelters, etc.

The transformation of our society depends on the faithfulness
of the church in this critical matter.
