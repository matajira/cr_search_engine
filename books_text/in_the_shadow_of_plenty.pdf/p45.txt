The Samaritan Legacy 29

82:4). Without hesitation. Without a second thought. Without
looking for excuses. He just did his job. He did what he ought to
have done. He did what he had to do morally, He demonstrated
Law and love in action,

He realized that charity was part of the job of righteousness, of
evangelism. And it was fés job. There was no way around it.

It didn’t matter what the priest did or didn’t do. It didn’t mat-
ter what the Levite did or didn’t do. It only mattered that God had
encoded compassion into His unchangeable Word and that He
had caused the Samaritan to cross paths with the victim pilgrim.

Charity was fis job.

A Model for the Church

When the early Christians read or heard the story of the Good
Samaritan, they were devastated, as Jesus knew they would be.
This went beyond Law-keeping! Real spiritual power would be
needed to imitate the Good Samaritan, and they knew that “with
men it was impossible”— until that is, they realized just Who the
Good Samaritan really was.

You see, by itself this story only condemns us. Who is equal to
the task? Only the Good Samaritan, Jesus Himself (see John
8:48), a man from “Galilee of the gentiles,” (Matthew 4:15), Like
the God-fearing Samaritans of 2 Chronicles 28:8-15, Jesus had
come to “free the captives.”

The early Christians realized that what the Law could not do
(the priest and the Levite), Jesus had done. After all, the priest
had a legitimate worry: Contact with an unclean person would
defile him and prevent him from exercising his appointed task.
But Jesus had broken the restrictions of the ceremonial Law. A
new age had come!

The early Christians also realized that the inn in the parable
was the church, and the innkeeper symbolized pastors. But more
than that, they realized that the Holy Spirit had been poured out,
placing them in ethical union with Christ the Good Samaritan,
and enabling them to imitate His grace and mercy.

The parable no longer condemned them. Rather, it liberated
