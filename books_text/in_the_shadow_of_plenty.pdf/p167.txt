13
WHAT FAMILIES SHOULD DO

Jesus said, “If you love Me, keep my commandments” { John
14:15). And again, “He who has My commandments and keeps
them, it is he who loves Me. And he who loves Me will be loved
by My Father, and I will love him and manifest Myself to him”
(John 14:21).

Similarly, the Apostle John wrote,

Now by this we know that we know Him, if we keep His com-
mandments. He who says, “I know Him,” and does not keep. His
commandments, is a liar, and the truth is not in him. But whoever
keeps His word, truly the love of God is perfected in him. By this
we know that we are in Him. He who says he abides in Him ought
himself also to walk just as He walked. Beloved, I write no new
commandment to you, but an old commandment which you have
had from the beginning. The old commandment is the word which
you heard from the beginning (1 John 2:3-7).

The unmistakable mark of a faithful people is obedience.
Believers are proved as “doers of the word, and not hearers only,
deceiving yourselves” (James 1:22). They keep the commands of
God's Word.

Thus James could ask,

What does it profit, my brethren, if someone says he has faith,
but does not have works? Can faith save him? If a brother or sister
is naked and destitute of daily food, and one of you says to them,
“Depart in peace, be warmed and be filled,” but you do not give
them the things which are needed for the body, what does it profit?
Thus also faith by itself, if it does not have works, is dead. But

151
