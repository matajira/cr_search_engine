The Foundation of Peace M3

trembled; and the earth quaked, so that it was a very great trembl-
ing (1 Samuel 14:8-15).

‘The odds were against him. One man with his armor bearer,
against the entire Philistine garrison! It was suicidal.

Maybe. It dooked that way. But then, looks can be deceiving.
Appearances are sometimes quite out of line with facts.

So, what were the facts?

Jonathan knew that the land belonged to God, not to the Phil-
istines (Psalm 24:1). He. knew that God had placed the land into
the care of His chosen people, the Jews (Joshua 1:2). He knew
that they had sure and secure promises that if they would obey
God’s Word and do God’s work, they would be prosperous and
successful (Joshua 1:8), that every place which the sole of their
feet trod would be granted to them ( Joshua 1:3), and that no man
would be able to stand-before them all the days of their lives
{Joshua 1:5). He knew that.if the people would only “dwell in.the
shelter of the Most High,” in the “shadow of the Almighty” (Psalm
91:1), He would deliver them “from the snare of the fowler And
from the perilous pestilence” (Psalm 91:3). He would cover them
“with his feathers” (Psalm 91:4), and protect them from “the terror
by night” and “the arrow that flies by day” (Psalm 91:5). And
though a thousand fall at their left hand, ten thousand to the
tight, affliction would not approach them; they would only look
and see “the reward of the wicked” (Psalm 91:7-8). They would be
protected from the.teeth of the devourer, encompassed with super-
natural power (Psalm 91:10).

These were the facts.

Though it looked as if God’s people were broken, scattered,
defeated, and woe begotten, in truth they were more than conquer-
ors (Romans 8:37), They were overcomers (1 John 5:4).

Philistine dominion was fiction. Israel cowering in fear was
foolish fantasy. Pessimism about their ability to stand and not be
shaken (Hebrews 12:28) was novel nonsense.

Jonathan knew that.

So, he acted. He acted boldly. He acted decisively. He acted
on the basis of the éuth and reliability of God’s Word, not on the
