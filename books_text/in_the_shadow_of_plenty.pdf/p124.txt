108 In the Shadow of Plenty

uttered this phrase, the citizens of Judah.appeared to be anything
but God's faithful covenant people. They were treading the dark-
some path of wickedness.

Their worship had deteriorated into meaningless ritual (Isaiah
1:14:15). They had become proud and complacent (Isaiah 32:10).

-They had entangled themselves in unholy alliances (Isaiah

30:1-3), Their hearts were inclined to “iniquity: To practice un-
godliness, to utter error against the Lord, To keep the hungry un-
satisfied, And he will cause the drink of the thirsty to fail” (Isaiah
32:6). They were flirting with disaster (Isaiah 5:13-17). For, “There
is no peace for the wicked” (Isaiah 48:22).

Thus the God of peace commanded the prophet of peace to re-
iterate, once and for all, the program for peace, saying,

Cry aloud, spare not; Lift up your voice like a trumpet; Tell
My people their transgression, And the house of Jacob their sins
. . . Is this not the fast that I have chosen: To loose the bonds of
wickedness, To undo the heavy burdens, To let the oppressed go
free, And that you break every yoke? Is it not to share your bread
with the hungry, And that you bring to your house the poor who
are cast out; When you see the naked, that you cover him, And not
hide yourself from your own flesh? Then your light shall break
forth like the morning, Your healing shall spring forth speedily,
And your righteousness shall go before you; The glory of the Lord
shall be your rear guard. Then you shall call and the Lord will an-
swer; You shall cry, and He will say, “Here I am.” If you take away
the yoke from your midst, The pointing of the finger and speaking
wickedness, If you éxtend your soul'to the hungry And satisfy the
afflicted soul, Then your light shall dawn in the darkness, And
your. darkness shall be as the noonday. The Lord will guide you
continually, And satisfy your soul in drought, And strengthen your
bones; You. shall be like.a watered garden, And like a spring of
water, whose waters do not fail. Those from among you shall build
the old waste places; You shalt be called the repairer of the
breach, the restorer of streets to dwell in (Isaiah 58:1, 6-12).

Did the people want peace, perfect peace, the peace that sur-
passes all understanding? Did they want to reconstruct their cul-
ture, restore the foundations, and reclaim their lost legacy? Then
