114 In the Shadow of Plenty

seemingly impossible circumstances that faced him. He acted on
faith and not on sight. He acted realistically, knowing that God's
definition of things is the read reality, the only reality. He acted with
passion and zeal for the things he knew to be God’s will.

And God honored him. He blessed Jonathan with great suc-
cess, Unbelievable success.

Jonathan stood against the tide. By all rights, he should have
been crushed under its weight, but instead, the tide turned! He
won the day and saved the nation,

Faith and Victory

“Now faith is the substance of things hoped for, the evidence of
things not seen” (Hebréws 11:1). “By it the elders obtained a good
testimony” (Hebrews 11:2}. Against all odds, against all hope they
obtained victory. They snatched glory out of the jaws of despair.
They hurdled insurmountable obstacles to “lay hold” of the good
things of the Lord (Hebrews 6:18). By faith, they believed God for
the remarkable, for the impossible (Matthew 19:26; Hebrews
J1:1-40): Abraham (Genesis 12:1-4), Sarah (Genesis 18:11-14), Isaac
(Genesis 27:27-29), Jacob (Genesis 48:1-20), Joseph (Genesis
50:24-26), Moses (Exodus 14:22-29), Rahab ( Joshua 6:23), Ruth
(Ruth 1:16-17), Gideon ( Judges 6:1-8:35), Barak (Judges 4:1-5:31),
Samson ( Judges 13:1-16:31), Jephthah (Judges 11:1-12:7), David
Samuel 16:1:17:58), Isaiah (Isaiah 1:1-6:13), Samuel, and all the
prophets (1 Samuel 1:1-28; Hebrews 11:32). For by faith they “sub-
dued kingdoms, worked righteousness, obtained promises, stopped
the mouths of lions, quenched the violence of fire, escaped the edge
of the sword, out of weakness were made strong, became valiant in
battle, turned to. flight the armies of the aliens” (Hebrews 11:33-34).
Though they were mocked and persecuted, imprisoned and tor-
tured, impoverished and oppressed, they were unshaken and even-
tually obtained God’s great reward (Hebrews 11:35-40).

Therefore we also, since we are surrounded by go great a cloud
of witnesses, let us lay aside every weight, and the sin which so
easily ensnares us, and let us run with endurance the race that is
set before us, looking unto Jesus, the author and finisher of our
