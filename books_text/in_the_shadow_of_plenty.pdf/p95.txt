Uplifting the Uplifiers 79

ship with the people of the covenant. When the people were obe-
dient, then He blessed the earth with fruitful abundance (Deuter-
onomy 28:1-14; Isaiah 4:2; Hosea 2:21-23). But when they were
rebellious, He cursed the earth with empty desolation (Deuteron-
omy. 28:15-68; Leviticus 26:14-35). The disciples knew that God
used famine and calamity throughout Israel’s Jong tumultuous
history to indicate his displeasure and to warn the people to re-
pent (1 Kings 17:1; Haggai 1:5-11).

Thus, when famine struck Judea a short time after the church
was inaugurated in the first century, none of the disciples was
taken by surprise.

They knew why.

And they knew for what purpose.

The “why?” was easy. God was judging Jerusalem for her un-
belief (Matthew 23:37-38).

The “for what purpose?” was equally easy. God was providing
them with an opportunity for their testimony (Luke 21:13).

Jesus had said, “By this all men will know that you are my dis-
ciples, if you have love for one another” (John 13:35).

The famine gave them the opportunity to let “all men know.” It
gave them the opportunity to demonstrate their love, visibly and
tangibly.

And in these days prophets came from Jerusalem to Antioch.
And one of them, named Agabus, stood up and showed by the
Spirit that there was going to be a great famine throughout all the
world, which also happened in the days of Claudius Caesar. Then
the disciples, each according to his ability, determined to send
relief to the brethren living in Judea. This they also did, and sent it
to the elders by the hands of Barnabas and Saul (Acts 11:27-30).

When the famine struck, they sprang into action. There was
no need for long drawn out emotional appeals. They were ready,
willing, and able to love; not merely “in word or in tongue, but in
deed and in truth” (1 John 3:18). For the-righteous man, “. . . will
not be afraid of evil tidings; His heart is steadfast, trusting in the
Lord. His heart is established; He will not be afraid, Until he
sees his desire upon his enemies. He has dispersed abroad, He has
