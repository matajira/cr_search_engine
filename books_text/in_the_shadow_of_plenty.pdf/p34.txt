18 In the Shadow of Plenty

Word and Deeds in History

Whenever and wherever the gospel has gone out, the faithful
have emphasized the priority of good works, especially works of
compassion toward the needy. They have matched the message of
judgement and hope with charity, Every great revival in the his-
tory of the church, from Paul’s missionary journeys to the Refor-
mation, from Athanasius’ Alexandrian outreach to America’s
Great Awakening, has been accompanied by an explosion of
Christian care. Hospitals were established. Orphanages were
founded. Rescue missions were started. Almshouses were built.
Soup kitchens wére begun. Charitable societies were incorpor-
ated. The hungry were fed, the naked clothed, and the homeless
sheltered. Word was wed to deeds.

This fact has always proven to be the bane of the church's ene-
mies. Apostates can argue theology. They can dispute philosophy.
They can subvert history. And they can undermine character. But
they are helpless in the face of the church’s extraordinary feats of
selfless compassion.

Not only did Augustirie (354-430) change the face of the
church with his brilliant theological treatises; hé also transformed
the face of Northern Africa, establishing works of charity in thir-
teen cities, modeling authentic Christianity for the whole of the
Roman Empire,

Not only did Bernard of Clairveaux (1090-1153) launch the
greatest monastic movement of all time, sparking evangelical fer-
vor throughout-France and beyond, he also established a charita-
ble network throughout Europe to care for the poor, a network
that has survived to this day.

Not only did John Wyclif (1329-1384) revive interest in the
Scriptures during a particularly dismal and degenerate era with
his translation of the New Testament into English, he also un-
leashed a grassroots movement of lay preachers arid relief workers
that brought hope to the poor for the first time in over a century.

Not only.did Jan Hus (1374-1415) shake the foundations of the
medieval church hierarchy with his vibrant evangelistic sermons,
