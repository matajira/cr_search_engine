Wheat the Church Should De 149

charities. Members would rely on the deacons to make such in-
vestigations. It is a simple matter of good’ stewardship.

Fifth, churches can mobilize and utilize existing resources
within the community networking businesses, service groups, and
civic coalitions. For example, the deacons can set up and main-
tain an employment placement service that would benefit both
local businesses and the poor. Or, they can approach landlords
with low occupancy rates in their apartment complexes, propos-
ing to place poor families in those units in exchange for mowing

* the lawns, cleaning the grounds, and light maintenance work.

God!’s authority must be upheld and the authority of His Word
must be established. The church serves as a judge. He who pays
the piper calls the tune. He who feeds the piper orders the meal. If
someone is under the table of the Lord begging for crumbs, he
had better understand just why he is under the table, and just how
risky it is to receive scraps from God’s table. Paul warned all such
dependent sinners about the risk of remaining under the table and
refusing God's saving grace:

Beloved, do not avenge yourselves, but rather give place to
wrath; for it is written, “Vengeance is Mine, I will repay,” says the
Lord. “Therefore if your enemy hungers, feed him; If he thirsts,
give him a drink; For in so doing you will heap coals of fire on his
head” (Romans 12:19-20).

Conclusion

The church is to do the work of charity, but the modern
church has not yet shown itself capable of doing it. Paralyzed by
theologies of escapism, pessimism, and experientialism, the
church lost its position of influence in society and saw humanism
take its place. And charity was thus yielded over to bureaucracies
and governments.

In order for the church to regain its place, in order for it to ac-
tually do the work of charity, providing answers, alternatives, and
models, it must return to the Word, She must return to the Word re-
vealed, the Word incarnate, and the Word made manifest.

The church also needs to understand the basic principle of
