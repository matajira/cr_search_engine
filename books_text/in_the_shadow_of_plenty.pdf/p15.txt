Editor's Introduction xv

Redemption

How do we obtain exodus from our present slavery to the
state? How do we gain our redemption from bondage (“redemp-
tion”: to buy back)? By faithful obedience to God and His decrees.
By taking up the work of Biblical charity. By tithing once again,
and then by adding gifts and offerings.

But, some will protest, the task is too great, and besides we
already pay far more than a tithe in taxes. True enough. Slaves do
face tremendous obstacles and struggle against tremendous liabil-
ities. But we sold ourselves into political slavery because we ac-
cepted the political conclusions of a false religion: that the state
brings earthly healing and. salvation, not God. So, having sold
ourselves into slavery, we must now reverse our allegiances and
change our conclusions.

We owe ten percent to God’s church. That isn’t our redemp-
tion money; that’s the day-to-day, run-of-the-mill minimum pay-
ment that we always owe. (By the way, this is ten percent of in-
come after taxes. We don’t owe a tithe on money stolen from us be-
fore we get it. The farmer doesn’t pay a tithe on that portion of his
crop that the locusts ate, nor do we pay taxes on the money that
the tax collectors took.) Not only do we owe the normal tithe to
the church (and to no other institution), we also need to dig
deeper-into our wallets and start supporting works of Biblical
charity. This after-tithe money is the money we should earmark for
the almshouses, the food banks, the independent foreign missions
agencies, the crisis pregnancy centers, the rescue missions, and
the sheltering homes. First, ten percent to the church; second, ex-
tra money for Biblical charity.

But we also owe God’s church service. Dollars are not enough.
We must put our hand to the plow and:do the real labor of charity.
We can not effect reconstruction by proxy. We must, as Grant.so
aptly points out “transform poverty into productivity.” The only
way we can do that is to go 4o work.

We work our.way up and out of slavery with our gifts and
offerings and with our service. We prove to the world that we don?
