EDITOR’S INTRODUCTION
by Gary North

“But would you let people starve?”

This question is the standard last-ditch response by a person
who has heard half a dozen arguments, with the supporting evi-
dence, that public welfare financed by taxation (1) increases the
number of people who are poor, (2) increases the dependence on
the state of welfare recipients, (3) reduces per capita productivity
in the society and therefore reduces per capita wealth, (4) creates
resentment among voters, (5) keeps growing despite the failure of
the programs, and (6) benefits mostly middle-class welfare bu-
reaucrats. The evidence is clear; hardly anyone disputes it any
more (ten years ago, they still did).

Nevertheless, millions of voters (including Christians) cannot
follow with their hearts where their. minds are taking them. So
they argue that the person who is bringing them this unpleasant
information is himself callous. He is heartless. He doesn’t care
about the poor. “Would you let people starve?”

Now, George Grant has answered that question. He has
avoided the trap of liberal humanism by facing squarely the com-
plete failure of the welfare system. He has also avoided the trap of
conservative humanism by embracing the necessity to demonstrate
cormpassion and justice. In short, he has done what has seldom—
if eyer—been done before: He has answered the poverty question
Biblically. And he has answered with more than mere words.

George Grant is the pastor of a small church that has effect-
ively implemented the principles described in this book in their
local community. In fact, they have so effectively implemented
them that they have gained national media attention and notoriety

xi
