8
THE UNBROKEN CIRCLE

Now Joshua the son of Nun sent out two men from Acacia
Grove to spy secretly, saying, “Go, view the land, especially
Jericho.” So they went, and. came to the house of a harlot named
Rahab, and lodged there. And it was told the king of Jericho, say-
ing, “Behold, men have come here tonight from the children of
Israel to search out the country.” So the king of Jericho sent to
Rahab, saying, “Bring out the men who have come to you, who
have entered your house, for they have come to search out all the
country.” Then the woman took the two men and hid them; and
she said, “Yes, the men came to me, but I did not know where they
were from. And it happened as the gate was being shut that when
it was dark, that the men went out. Where the men went I do not
know; pursue them quickly, for you may overtake them.” (But she
had brought them up to the roof and hidden them with the stalks of
flax, which she had laid in order on the roof.) Then the men pursued
them by the road to the Jordan, to the fords..-And as soon as those
who pursued them had gone out, they shut the gate (Joshua 2:1-7).

She was a harlot.

An outcast even from her own people, she was especially
despicable in the sight of God’s covenant people.

But Rahab repented.

She put hei trust in Almighty God. And by grace through
faith, she was saved, she and her entire household.

Rahab didn’t stop at that though. Not only did she risk her life
to save the spies, not only did she create a diversion for them, but

she confessed her faith in the Lord. She threw in her lot with them

and Him!

88
