140 In the Shadow of Plenty

so close to the brink of destruction? Very simply, we have yielded
to a humanistic world and life view, a view that contravenes the
Word of God at every turn.

When we violate God’s Law of gravity, we suffer very clear
and obvious consequences. Is it any wonder that when we violate
God’s Laws of economics, or civics, or psychology, or philosophy,
or science, or. morality, we should similarly suffer clear-and ob-
vious consequences? Not hardly.

Humanism is wreaking havoc in catastrophic proportions sim-
ply because it is out of step with reality. Only God’s Laws fit what
is there. Thus, only God’s Laws can lay foundations for peace,
prosperity and genuine social security.

Humanism, and the culture which it spawns, are but dust in
the wind.

The Failure of the Church

So. Christianity fas been overthrown as the foundational au-
thority in Western culture. No longer is the Bible the final court of
appeal in matters of law, economics, or ethics, Thus, the very
nature of Western culture is undergoing a dramatic metamor-
phosis. It is undergoing a comprehensive philosophical and moral
reversal. This radical reversal is not occurring because our civili-
zation has been overrun by bands of barbarians from the hinter-
lands. It is not occurring because the Communists have been suc-
cessful in infiltrating and sabotaging our governmental appar-
atus. It is'not occurring because Madison Avenue has corrupted
our youth. It is occurring because of the ineptness of the church,
It is occurring because we Christians have failed.

Despite multi-million dollar budgets, despite a gargantuan
constituency, despite a millennium-long legacy, the church has
failed. Why? Why, on the very doorstep of the largest, most pow-
erful, richest, best organized, and most vocal evangelical Chris-
tian movement ever, is this immoral, unstable, and humanistic
mentality able to control the cultural apparatus? By all rights, evil
ought to be converted and contained by the church, but the very
opposite is true. Why? Why have Christians been so ineffective
and unproductive in the world?
