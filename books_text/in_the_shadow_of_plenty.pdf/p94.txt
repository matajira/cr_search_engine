7
UPLIFTING THE UPLIFTERS

Therefore, as we have opportunity, let’ us do good to all, espe-
cially to those who are of the household of faith (Galatians 6:10),

Jesus told his disciples that following his ascension to the right
nand of the Father, there would be difficult times ahead for them.
There would be tribulation (John 16:33), persecution (Luke
21:12), and natural calamity (Matthew 24:7). There would even
be times of famine and pestilence (Matthew 24:7).

But, He told them that these events, as horrible, as heart
wrenching as they would be, would also serve as an opportunity to
spread the Gospel with great power and effect.

Then He said to them, “Nation will rise against nation, and
kingdom against kingdom. And there will be great earthquakes in
various places, and famines and pestilences; and there will be fear-
ful sights and great signs from heaven. But before all these things,
they. will lay their hands on you and persecute you, delivering you
up to the synagogues and. prisons, and. you will be brought before
kings and rulers for My name’s sake. But it will turn out for you as
an occasion for testimony” (Luke 21:10-13).

Famine and calamity, like any of the other events in nature or
history, were understood by the disciples to fit into the eternal pro-
gram of divine providence (Amos 4:6; Revelation 7:8; Romans
8:28). They knew that God possessed.and controlled the “forces of
nature” just as surely as He flung the cosmos into existence at the
creation (Psalm 104:1-35). They knew that He exercised His
power over these “forces” in direct correspondence to His relation-

78
