Time Enough 147

sary sacrifices in life that it takes to be successful if one is told
that his efforts will not leave anything of significance to the next
generation, if in fact there will be a next generation, which is
said to be highly doubtful. Mr. Lewis and his pre-tribulational
dispensational colleagues have paraphrased homosexual econo-
mist John Maynard Keynes’ quip, “In the long run we are all
dead.” They say, “In the short run, we Christians will all be
Raptured, and the Jews in Israel will soon wish they were dead,
which two-thirds of them will be within seven years after we
leave.” (This view of the Jews is still taught by the retired, 30-
ycar president of Dallas Theological Seminary.)”

Mr. Lewis’ position on politics and social involvement is one
more example of the long-term operational alliance between the
escape religion and the power religion. Both sides are agreed:
Christians should not scek office as civil magistrates, except as
judicially neutral agents, Yet at the same time, all but Liberty
University’s Norman Geisler (a former Dallas Theological Semi-
nary professor) and the academic political pluralists (e.g., Rich-
ard John Neuhaus) admit there is no neutrality. This is schizo-
phrenic.” This schizophrenia has left Christians intellectually
helpless in the face of an officially neutral, officially pluralistic
humanist juggernaut. This has been going on for over three
centuries.” (An Islamic juggernaut might provide a cure.)

Continuity and Time

The two divisive millennial issues are continuity and time. The
self-conscious premillcnnialist denies continuity and shortens
time. He forthrightly declares, as Lewis has declared: “We are in
the final era prior to the coming of Jesus and the establishing of
the visible aspect of the Kingdom — the Millennium. We have no
time to waste on wild experimentation with possible futures and

29, John F. Walvoord, Israel in Prophecy (Grand Rapids, Michigan: Zondervan
Academie, [1962] 1988), p. 108.

30. North, Moses and Pharaoh, pp. 2-5.

31. Gary North, “Ihe intellectual Schizophrenia of the New Christian Right,” Chris-
tianity and Civilization, 1 (1983).

32, Gary North, Political Polyiheism: The Myth of Pluralism (Tyler, Texas: Institute for
Christian Economies, 1989), Part 3.
