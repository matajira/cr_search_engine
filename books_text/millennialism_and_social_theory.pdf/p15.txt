Preface xv

ward.) The Bible therefore “lays down the law” for every field: family
life, sociology, health, economics, education, politics, biology, geology,
and even mathematics.‘ The experts in each of these fields, as well
as all the others, are required by God to go to the Bible in
search of their particular field’s operational first principles, as
well as for some of the actual content (facts) of their fields. They
do not lay down the faw to the Bible. Their particular fields of
study do not dictate to the Bible the theory and content of truth.

This means that the Bible is relevant for social theory. So,
where are the books that explain what the biblical view of social
theory is? There have been hybrids in history, of course, such as
Scholasticism’s attempted fusion of Stoic natural law theory and
the Bible. More recently, there has been liberation theology’s
attempted fusion of Marxism and biblical rhetoric. But there is
only one self-conscious body of literature that relies solely on
the Bible in order to establish its first principles of social theory:
theonomy or Christian Reconstruction. This book is my attempt
to show you why this is the case.

A Brief Word of Encouragement to Secular Academics

Because of the title of this book, there may be a few secular
academics who decide to read it. These days, millennialism has
become a “hot topic” in academic circles. Already, this Preface
has lost some of these readers. They have closed the book in
disgust. “Why, this book is written by a Bible-believing Chris-
tian! Furthermore, I was expecting a detailed study that would
include references to at least 73 recent articles in German theo-
logical journals.”

Look, I am on my way to heaven. J am not about to read 73
(or even ten) scholarly articles by liberal German theologians.’
In the realm of academics, such a task is about as close to hell
on earth as anyone can come. Besides, the reason why some

4. Vern S, Poythress, “A Biblical View of Mathematics,” in Gary North, (ed.), Founda-
tions of Christian Scholarship: Essays in the Van Til Perspective (Vatlecito, California: Ross
House Books, 1976).

5, There is one exception; Henning Graf Reventow, author of The Authority of the
Bible and the Rise of the Modern World (London: SCM Press, [1980] 1984), which I regard as
one of the half dozen most important history books written since World War II.
