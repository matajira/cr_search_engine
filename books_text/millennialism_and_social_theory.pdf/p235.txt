The Sociology of Suffering 219

appear as though man can save himself by his own efforts, i.e.,
that he is not the recipient of God’s grace. It would also leave
him without permanent standards. We need both doctrines.

It is my argument in this study and in my book, Dominion
and Common Grace, that these same dual concepts of definitive
and progressive sanctification apply to corporate groups, espe-
cially covenantal associations, and above all, the Church. Thus,
the fact that the Church has been definitively granted Christ's
moral perfection does not deny the possibility and moral neces-
sity of its progressive sanctification in history. Similarly, the fact
that there is progressive sanctification in history does not in any
way deny the fact of Christ’s perfection, which was definitively
granted to the Church at the point of its covenant-based cre-
ation. This applies also to the family and the State.

This simple concept completcly baffics Professor Gaffin. He
has read Dominion and Common Grace, for he offers a brief, exeg-
etically unsupported sentence criticizing its cover, but, predict-
ably, refuses to refcr to its thesis or its documentation, and even
this he confines to a footnote.* He ignores the book’s documen-
tation. (It should be noted that in his essay against Christian
Reconstruction, Gaffin does not once cite any Reconstructionist
author in the body of the text, and includes only three brief
footnote references, one to the book cover and two to David
Chilton’s Paradise Restored. In fact, most of the essays in this
compilation are remarkably devoid of actual citations of our
writings, except Bahnsen’s Theonomy, To say that this is a pecu-
liar way to respond to a movement that has published well over
one hundred volumes of books and scholarly journals, plus 25
years of newsletters is, to say the least, revealing. But, as I al-
ways say, you can't beat something with nothing. I think the
faculty at Westminster Seminary understands this, so they have
avoided direct confrontations with the primary sources of Chris-
tian Reconstructionism.)’ Here is Dr. Gaffin’s position:

6. Gaffin, p. 216n.

7. This has been going on for well over two decades. For my comments on the
practice, see my Publisher's Foreword to Greg L. Bahnsen and Kenneth L. Gentry, Jr,
House Divided: The Break-Up of Dispensationat Theology (Tyler, Texas: Institute for Christian
Economics, 1989), pp. xxxvii-xli, “Dealing With the Academic Black-Out.”
