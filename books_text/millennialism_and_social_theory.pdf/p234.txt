218 MILLENNIALISM AND SOCIAL THEORY

in this life, it is no more of an eschatological certitude that we
shall be persecuted in the future than that God will put His
words in our mouths when we are delivered up before judges.
Persecution of the Church by covenant-breakers is no more
assured eschatologically in the future than the Great Tribula-
tion is — and the Great Tribulation is behind us. I doubt that
Gaffin believes that we can wait on God to put words in our
mouths rather than hire defense attorneys. | presume that he
would admit that this prophecy applied only to the transition
era between Pentecost and the fall of Jerusalem. But persecu-
tion of Christians? This is supposedly a category of Christian
existence, an eschatological imperative. And if he follows Van
Til, progressive persecution is eschatologically inevitable.

I prefer progressive sanctification to progressive persecution.

Progressive Sanctification

Because this doctrine is so often ignored by Christians, espe-
cially those few who bother to comment on the covenantal
meaning of New Covenant history, I nced to remind the reader
one more time of the biblical doctrine of sanctification. God
grants the perfect humanity of Christ to cach individual convert to
saving faith in Christ. This takes place at the point of his or her
conversion. Subsequently, this implicit, definitive moral perfec-
tion is to be worked out in history, We are to strive for the
mark. We are to run the good race (strive to win it, by the way,
not in hope of a covenantal tie, i.e., pluralism).* We are to imi-
tate Christ's perfect humanity, though of course not His divini-
ty, which is an incommunicable attribute.

The doctrine of definitive sanctification taken by itself would
mean that an individual is perfect. Certain perfectionist sects
and cults have taught this, but this is clearly not Christian or-
thodoxy. “If we say that we have no sin, we deceive ourselves,
and the truth is not in us” (John 1:8). On the other hand, if
progressive sanctification is not balanced by the doctrine of
definitive sanctification as a pure gift of God, then it would

5. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute for
Christian Economics, 1989).
