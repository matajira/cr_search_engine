The Society of the Future 97

short, there will be a coming age in which covenantal wisdom is
paralleled by long lite. But it will still be an age of death and
sin. This is the reason why the exercise of covenantal wisdom
will be possible. It will still be possible to distinguish good from
evil, long life from short life, cursings from blessings.

No verse in the Bible makes it plainer that amillennial eschat-
ology cannot possibly be true. Even if we take these words sym-
bolically (i.e., as rhetoric), they still have to apply to history, for
sinners will not be present in the post-resurrection world. They
will not participate in the post-resurrection New Heaven and
New Earth. Isaiah made it perfectly clear: he was talking about
history. These words cannot possibly apply to the post-resurrec-
tion world. There will be a coming era of blessing in history.
God’s positive historical sanctions on covenant-keepers will bring
victory to His earthly kingdom. It is quite understandable why
Archibald Hughes, an amillennial theologian, mentioned this
passage in only two brief sentences in his book, A New Heaven
and a New Earth.’ This passage refutes his eschatology. (So much
the worse for Isaiah, apparently!) Herman Ridderbos is wiser
still: he never even mentioned the verse in a book on the king-
dom of God that is over 500 pages long.® (He compensates for
this omission by citing hundreds of German liberal theologians,
most of whom outlived their theories.)* One man, however, has
accepted the challenge. We therefore need to examine his exe-
gesis in detail to see whether the amillennial interpretation of
Isaiah 65:20 can be sustained.

 

ment of the Aris draws up artistic guidelines is attacked as illiberal.

2. Archibald Hughes, A New Heaven and a New Earth (Philadelphia: Presbyterian &
Reformed, 1958). The book's subtitle defines away the problem: An Jnroductory Study of the
Coming of the Lord Jesus Christ and the Etemal Inheritance. But what of the Church's historical
inheritance? For amillennialists, there is none except, possibly, escalating persecution.

$, Herman Ridderbos, The Coming of the Kingdom (Philadelphia: Presbyterian &
Reformed, 1963).

4. Itis one of the tragic wastes of sharp theological minds that they fee! compelled to
shadow bax with dead German theologians all of their lives. ic makes them nearly as
incoherent as their opponents, and nearly as impotent. Ridderlos followed The Coming of
the Kingdom with a somewhat less turgid book, Pau: An Outline of His Theology (1975), a
562-page outline in small print, He smote dead Germans, hip and thigh, throughout his
career. No one noticed. No one ever does, especially living German theologians.
