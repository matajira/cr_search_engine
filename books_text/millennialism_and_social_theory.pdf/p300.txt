284 MILLENNIALISM AND SOCIAL THEORY

required a closed, unfree society in ancient Israel. The follow-
ing unstated assumption is made all too often by anti-theonomic
commentators, and it is probably presumed by the vast majority
of contemporary Christians: “Israel was a tyrannical nation.”
But it was the other pagan city-states of ancient history that
were closed, not Israel. In a biblically covenantal society, there
will be one law for all residents with respect to the imposition
of negative civil negative sanctions (Ex. 12:49). I have called this
system Athanasian pluralism.”

When this kind of corporate, judicial subordination to God takes
place, society can expect God’s corporate blessings. This is the
only foundation of long-term positive feedback in history: from
victory unto victory. This is the vision of compound ethical growth
in history. It is basic to covenantal postmillennialism.

How to Overcome the Continuity of Evil

God's kingdom and Satan’s are locked in mortal combat.
Both kingdoms seck continuity. Both seek victory, Neither is
ready to surrender to the other. But the terms of battle, like the
terms of surrender, are covenantal. This is not a battle that will
be decided in terms of political power or any other kind of
power, It is not a power play. It is an ethical batile in history based
on rival covenantal conumitmenis. If it were a power play, the con-
flict would have ended in Eden. There are, however, negative
corporate sanctions that are applied by God in history to His
covenantal enemies. These sanctions are applied because of
corporate covenant-breaking by people in history. He breaks
the continuity of corporate evil. He may replace one society’s
corporate evil with another society’s corporate evil, but He docs
not allow the compound growth of the same social evil,

Mcanwhilc, He shows kindness unto thousands of generat-
ions of those who love Him and keep His commandments. This
is God’s compound growth process for covenant-keeping in
history. Little by little (with occasional discontinuities), God’s
kingdom expands over time.

7. Gary North, Political Polytheiem: The Myth of Pluralism (Tyler, Texas: Institute for
Christian Economics, 1989), pp. 594-97.
