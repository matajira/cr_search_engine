Our Blessed Earthly Hope in History 299

It will be a time of despair for billions of people. This is the
softening-up process that has always been necessary in advance
for widespread repentance.

Will the crises come? Let me ask another question. If crises
do not come, and women continue to execute 50 to 60 million
unborn infants a year, worldwide,"" what docs this say about
the God of the Bible? If this level of transgression does not
bring massive negative sanctions in history, then the Random
News common grace amillennialists are correct: the sanctions of
God are ethically inscrutable in history. And if this is true, there
cannot be any explicitly biblical social theory that would differ-
entiate a covenant-keeping society from a covenant-breaking
society. God’s kingdom would be aborted by Satan in history.

An economic crisis would be ideal. Few people would die,
but millions of people in the West would be filled with fear.
They might then turn to God for deliverance. The false god of
this age, material prosperity, would be publicly dethroned. Its
prophets, the economists and politicians, would be scattered.
The reigning paradigms of this era would be broken. In such a
crisis, a new worldview could become dominant. But it would
have to present a comprehensive, consistent social theory to
deal with the nature of the crisis. Neither amillennialism nor
premillennialism can offer such a theory.

Must the crises come? No. It is conceivable that God will
launch His era of millennial blessings by adopting a unique,
historically unprecedented technique: covenantal revival apart
from widespread negative sanctions. We should not assume that
He will do this, but He might. This would produce an unprece-
dented disinheritance/inheritance: the transfer of the assets of
today’s worldwide satanic kingdom directly to God’s kingdom
by means of billions of individual conversions to saving faith.
“Save souls and assets!” I personally pray that He will do this,
but He does not answer all of my prayers favorably.

We need a great revival. What kind of revival must it be? A
controversial one. The theologically conservative Presbyterian

18, World Population and Fertility Planning Technologies: The Next 20 Years (Washington,
D.C.: Office of Technology and Assessment, 1982), p. 63.
