226 MILLENNIALISM AND SOCIAL THEORY

Riessen's sociology of suffering.'*®

The Meek Shall Inherit the Earth, Not the Wimps

Gaffin rejects triumphalism, as do all amilennialists. It has
been absent for generations, but now Christian Reconstruction-
ism has revived it. Reconstructionists expect God's highly divi-
sive historical sanctions. They expect New Covenant history to
realize visibly the promise of Old Covenant cultural restoration.

Behold, I will gather them out of all countries, whither I-have
driven them in mine anger, and in my fury, and in great wrath; and
I will bring them again unto this place, and I will cause them to
dwell safely: And they shall be my people, and I will be their God:
And I will give them one heart, and one way, that they may fear me
for ever, for the good of them, and of their children after them:
And I will make an everlasting covenant with them, that I will not
turn away from them, to do them good; but I will put my fear in
their hearts, that they shall not depart from me. Yea, I will rejoice
over them to do them good, and I will plant them in this land
assuredly with my whole heart and with my whole soul. For thus
saith the Lorn; Like as I have brought all this great evil upon this
people, so will I bring upon them all the good that I have promised
them (Jer. 32:37-42),

This is what David taught:

Fret not thyself because of evildoers, neither be thou envious
against the workers of iniquity. For they shalt soon be cut down like
the grass, and wither as the green herb. Trust in the Lor, and do
good; so shalt thou dwell in the land, and verily thou shalt be fed.
Delight thysclf also in the Lorn; and he shall give thee the desires
of thine heart. Commit thy way unto the Lorp; trust also in him;
and he shall bring it to pass. And he shall bring forth thy righteous-
ness as the light, and thy judgment as the noonday. Rest in the
Lorp, and wait patiently for him: fret not thyself because of him
who prospereth in his way, because of the man who bringeth wicked
devices to pass. Cease from anger, and forsake wrath: fret not thy-
self in any wise to do evil. For evildoers shall be cut off: but those
that wait upon the Loro, they shall inherit the carth. For yet a little

16. See Chapter 5, above,
