Pessimillennialism 79

Scriptural interpretation. The trivium was grammar, logic, and
rhetoric. There is good evidence that the very development of
the child’s mind as he becomes an adult is tied closely to this
structure.” The Bible expositor must carefully deal with the
grammar of each text, but not to the exlusion of the underlying
theological structure or its symbolism (literary framework).
Similarly, the examination of the subtle symbols and allegories
of the Scripture — the rhetorical component of the texts — must
not be attempted apart from both grammar and theology. To
ignore both grammar and theological structure leads directly to
the allegorical methodology of the Roman Catholic Church that
the Reformation challenged. What both the grammatical-histori-
cal school and the “wild blue yonder” symbolic interpretation
school reject is the idea that the Pentateuch and the Book of
Deuteronomy provide us with God’s master plan, a five-point
theological structure. They resent the “procrustian bed” of the
covenant model, preferring instead other text-stretching beds,
such as the six loci of seventeenth-century Protestant Scholas-
ticism®’ or the five points of Calvinism.*' While these can be
derived theologically from the whole of Scripture, they are not
found in the actual structure of any biblical text. The five-point
covenant model is.

Common Grace

Cornelius Van Til, the Dutch-American Calvinist philoso-
pher, was a defender of what I have called common grace amil-
lennialism. There are two general schools of thought within this
movement: Van Til’s and Meredith G. Kline’s, Van Til’s col-
league at Westminster Theological Seminary. Van Til’s view is
self-consciously pessimistic. Kline’s is officially neutral with
respect to progress in history. I categorize thesc rival views as
(1) Bad News for Future Christian Man and (2) Random News

29. Dorothy L. Sayers, “The Lost Tools of Learning” (1947); reprinted in Journal
of Christian Reconstruction, 1V (Summer 1977).

30, Theology proper (God), hamartiology (sin), Christology, soteriology (redemp-
tion), ecclesiology (Church), and eschatology (last things).

31. Total depravity of man, God's unconditional election, limited (specific) atone-
ment, irresistible grace, and the perseverance of the saints.
