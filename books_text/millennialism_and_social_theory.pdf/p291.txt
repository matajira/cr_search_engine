Will God Disinherit Christ’s Church? 275

dom come. Thy will be done in earth, as it is in heaven” (Matt.
6:10). But it is also answered progressively in history.

This is the society of man’s long-term earthly future, no
matter what difficulties Christians may experience betwecn now
and then. God’s kingdom comes; His will is done.

For proclaiming such a view of history, the Reconstruction-
ists have come under heavy fire from pietists. An English pastor
announced to his magazine’s readers: “Reconstructionist writers
all scorn the attitude of traditional evangelicals who see the
church as something so completely distinct and separate from
the world that they seek no ‘authority’ over the affairs of the
world.”* He is correct on this point, though on few others in
his essay. This is exactly what we scorn, and for explicitly theo-
logical reasons.

That the Church is distinct from the world institutionally is
not a major insight. It alone lawfully administers the holy
sacraments. But what has this got to do with the other half of
his assertion, namely, that they (Christians) therefore need seek
no authority over the affairs of this world? It is because the
Church is distinct from this world that God has called Chris-
tians, as His discipies, to baptize nations, bringing whole societ-
ies under God's authority: the Great Commission (Matt. 28:18-
20). If the Church had its origins in this world, we could not
lawfully claim to represent God legally in history. We would be
of this world, and therefore incapable of bringing a heaven-
originated process of healing and restoration to this world. It is
the very distinctness of the Church and its God-assigned task of
discipling the nations that authorizes Christians progressively to
seek authority over the affairs of this world.

A False Definition of God’s Kingdom
We do not argue, as this critic argues to defend his own
position of cultural isolation, that “The kingdom of God is the

church, small as it may sometime appear, not the world. . . .”
This definition of the kingdom of God is the Roman Catholic

40. Peter Masters, “World Dominion,” Sword & Trowel (May 24, 1990), p. 18.
