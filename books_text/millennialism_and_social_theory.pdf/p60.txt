44 MILLENNIALISM AND SOCIAL THEORY

twin idols of mankind: idols of history and idols of nature.**
Covenant-keepers have not accepted this worldview in theory,
but most have accepted it in practice. Rendom historical sanctions
become the operational equivalent of no historical sanctions. This
leaves humanism's sanctions as an effective monopoly in history.
Furthermore, throughout most of man’s history, these sanctions
have been closely associated with the exercise of State power.
‘Those who have affirmed meaningful sanctions only in history
have sought and gained political power. Modern Christians, in
contrast, have denied that God’s predictable, covenantal sanc-
tions apply to history, and have also denied their legitimate
enforcement by the covenant-keeping representatives of God.
They have sought, first, to shun all political power and, second,
to escape its effects. This leaves covenant-breakers in control of
society by default. This covert alliance between humanists and
pietists has led to the visible triumph of the power religion over
the escape religion.

‘The power religion and the escape religion are united in their
determined opposition to the idea of God’s covenant sanctions
in history. Understandably, the defenders of humanist thcoc-
racy — the religion of autonomous man — are outraged by the
message of biblical theocracy. Man, they insist, must rule in
history whenever nature departs from her throne (or is pushed
off by man). Less recognized is the fact that the defenders of
Gad’s random historical sanctions have by default accepted the
moral legitimacy of this humanist theocracy, at least in the form
known as political pluralism — what I have called right-wing
Enlightenment political theory.**

There is no neutral cultural and social vacuum. Either cove-
nant-keepers will make and enforce the laws of socicty, or else
covenant-breakers will. There is no third alternative, long-term.

82. Herbert Schlossberg, Idols of Destruction: Christian Faith and Its Confrontation with
Anarican Society (Washingwon, D.C.: Regnery Gateway, [1989] 1990), p. 11.

83. Gary North, Moses and Pharaoh: Dominion Religion ws. Power Religion (Tyler, Texas:
Institute for Christian Economics, 1985), pp. 2-5.
. Gary North, Political Polytheism: The Myth of Pluralion (Tyler, Texas: Institute for
ian Economics, 1989), pp. 398, 540.

 
