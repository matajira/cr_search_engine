Q. 191. What do we pray for in the second petition?

A. In the second petition, (which is, Thy kingdom come,) ac-
knowledging ourselves and all mankind to be by nature under
the dominion of sin and Satan, we pray, that the kingdom of sin
and Satan may be destroyed, the gospel propagated throughout
the world, the Jews called, the fulness of the Gentiles brought
in; the church furnished with all gospel-officers and ordinances,
purged from corruption, countenanced and maintained by the
civil magistrate: that the ordinances of Christ may be purely
dispensed, and made effectual to the converting of those that
are yet in their sins, and the confirming, comforting, and build-
ing up of those that are already converted: that Christ would
rule in our hearts here, and hasten the time of his second com-
ing, and our reigning with him for ever: and that he would be
pleased so to exercise the kingdom of his powcr in all the
world, as may best conduce to these ends.

Larger Catechism
Westminster Confession of Faith (1646)

We who are reckoned as “conservatives” in theology are seri-
ously misrepresented if we are regarded as men who are hold-
ing desperately to something that is old mercly because it is old
and are inhospitable to new truths. On the contrary, we wel-
come new discoveries with all our heart; and we are looking, in
the Church, not merely for a continuation of conditions that
now exist but for a burst of new power.

J. Gresham Machen (1932)*

*Machen, “Christianity in Conflict,” in Vergilius Ferm (ed), Contempo-
rary American Theology (New York: Round ‘lable Press, 1932), 1, pp. 269-70.
