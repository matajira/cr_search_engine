350 MILLENNIALISM AND SOCIAL THEORY

You: What assessments?

Smurd: The assessments to cover the operational costs of the
trust fund.

You: You mean the trust fund makes no profits?

Smurd: Not in the last three hundred and seventy years.

You: When did it make any profits?

Smurd: From the year 100 A.D. until about 325. Then again,
very briefly, from 1517 to 1618.

You: After that?

Smurd: Afier that it has been all downhill.

You: You mean that all the beneficiaries have lost money?

Smurd: No. Only those who obey.

‘You: You mean the Christians.

Smurd: Precisely.

You: And what about the non-Christians?

Smurd: They have done quite well.

You: So, why not get them to pay opcrational costs?

Smurd: This is not what the Trustor wants.

You: Then it pays to be part of the non-Christian benefi-
ciaries.

Smurd: Not forever.

You: What changes things?

Smurd: The final distribution.

You: But what about on earth?

Smurd: There are complications.

You: What kind of complications?

Smurd, Well, for one thing, Armageddon.

You: Armageddon?

Smurd: Yes, Armageddon.

You: What is Armageddon?

Smurd: That’s when the people who haven’t paid any assess-
ments come and rob everyone who has.

You: We ail get robbed?

Smurd: The fortunate ones, yes.

You: What about the unfortunate ones?

Smurd: You don’t want to know.

You: Yes, I do.

Smurd: No, you don’t.
