178 MILLENNIALISM AND SOCIAL THEORY

historical defeat. Dispensational theology teaches the same thing
about the cultural efforts of Christians during the so-called
“Church Age.”*” (An exception: the premillennialist who has
adopted Van Til’s even more pessimistic vision.) But Mucther’s
view is worse, for being both Calvinistic and amillennial, it
offers no hope for Christians in history, not even the Rapture.
Applying Rushdoony’s dictum, John R. Muether is basically a
premillennialist without earthly hope.

Where Is His Exegesis?

Two additional comments are in order regarding Muether’s
theological position. First, his view of God’s inscrutable sanc-
tions in history is precisely what needs to be demonstrated
exegctically, not mcrely asserted authoritatively. Muether does
not even attempt such a task, in this essay or in a book-length
study.” Neither had his seminary colleagues at the time of
publication of Muether’s essay in the summer of 1990, seven-
teen years after the publication of Rushdoony’s Institutes of
Biblical Law, thirteen years after the publication of Bahnsen’s
Theonomy in Christian Ethics. Prudence can be abused.

Second, Muether does not cite a single book, author, or
theological tradition in the essay. He cites only two Bible verses.
One is Jeremiah 29:7, “And seek the peace of the city whither
I have caused you to be carried away captives, and pray unto
the Lorp for it: for in the peace thereof shall ye have peace.”
This is precisely what the Christian Reconstructionist would
advise (o those in cultural captivity, but he would deny that this
captivity period is permanent in history; surely it was temporary
under the Old Covenant. Muether, in contrast, sees the cultural
exile of Christians as a permanent historical condition. In short,

37. Douglas Oas has correctly noted the similarities bewween Kline’s thesis of the
common grace “intrusion period of the New Covenant cra and dispensationalismn's
“Church Age” or “great parenthesis.” Oss, “The Influence of Iermencutical Frameworks
in the Theonomy Debate,” Westminster Thenlogical journal, LI (Fall 1989), p. 240n.

38. Like all common grace amillennial theologians, he needs to respond in a detailed
fashion to my book, Dominion and Common Grace, Like most of his common grace academ-
ic peers, he refuses to acknowledge its existence. ‘This is the seminary professor's game
of “let’s pretend.”
