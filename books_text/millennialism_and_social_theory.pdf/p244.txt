228 MILLENNIALISM AND SOCIAL THEORY

Professor Gaffin has managed to reverse the sociological
order of events at Calvary. In his sociology of suffering, the
crucifixion follows Christ’s death and resurrection. He argues as
clearly as anyone ever has that our historical condition is to be
crucified with Christ; resurrection is strictly a post-historical
experience. But Gaffin has a problem: Jesus Christ announced the
Great Commission only after His resurrection. Gaffin’s sociology of
suffering would reverse Matthew 27 and 28. For Gaffin, the
Great Commission is a message of cultural crucifixion. In all
honesty, the Roman Catholic crucifix should be Gaflin’s symbol
of the Great Commission, not the empty cross of Protestantism.
The crucifix is appropriate for the Roman Church, which is also
amillennial. Those of us who are postmillennial much prefer
the symbol of the empty cross. It conforms to our eschatology.
So does the empty tomb.

Yes, we take up our cross to follow Him. But that burden is
easy (Matt. 11:30). It is not a burden so crushing that Christians
arc beaten down historically. Carrying the cross of Christ means
extending His kingdom in history, not being pushed out by Sat-
an’s Ieaven. It is Satan’s doom in history to suffer progressive
frustration, not the Church's. It is his representatives who are
called upon to suffer as God’s kingdom unfolds in history. Christ
was nailed to the cross so that Satan could be nailed to the wall.

I began this chapter with Luke 22, on the Lord’s Supper and
our lawful exercise of authority in history. Gaffin also places the
Lord’s Supper at the heart of his eschatology. “According to
Jesus, the church will not have drained the shared cup of his
suffering until he returns.” Gaffin’s theological problem is
not with postmillennialism as such; it is with what Jesus taught
about the judicial implications of His Supper.

He adds this rhetorical question: “Is it really overreacting to
say that such triumphalism is repugnant to biblical sensibili-
ties?”’ Now, there are perfectly good uses for rhetorical ques-
tions, even aggressive questions. But there are risks, too. Your
larget may have an opportunity to respond. He may re-work

18. Ibid., p. 218.
19, Jbid., p. 216.

 
