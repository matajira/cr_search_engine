The Sociology of Suffering 231

the covenantal promise of God to enforce His law by means of
direct sanctions (Deut. 28) was chronologically limited to the Old
Covenant cra, and even then, only inside national Israel (except
for that one confounding case of Nineveh). It means that Dr.
Gaffin is as embarrassed as all the other pessimillennialists are
by the obvious implications of their cschatologics. They do not
want to be called cultural defeatisis just because they happen to
be cultural defeatists. They want to clothe themselves in the
optimistic language of postmillennialism. So, the amillennialist’s
strategy is to spray verbiage all over the page. (The premillen-
nialist keeps talking about how great it is going to be on the far
side of Armageddon.)

There is another academic strategy, however: to offer no
cultural alternative, but criticize the present humanist world
order relentlessly. This does not change anything, but at least it
allows Christians, in Gaffin’s words, to get in a few licks.*

The Consequences of Christ’s Resurrection and Ascension

For many years, I have taunted non-theonomists with this
slogan: “You can’t beat something with nothing.” They have
said nothing public in response, but they have not needed to.
Their implicit answer is clear; it is based self-consciously on
their two (or three) pessimillennial eschatologics: “With respect
to social theory, we know we have nothing culturally to offer,
but since God does not really expect the Church to defeat any-
thing cultural in history anyway, nothing is all we need.”

The more intellectually sophisticated among them have
contented themselves with writing critical analyses of modern
humanist culture. By implication, they are calling Christians to
avoid the pits of Babylon. But calling Christians to “Come out
from among them!” without also providing at least an outline of
a cultural alternative to come i to (i.e., to construct) is simply to
mimic the fundamentalism of an earlier era: no liquor, no ciga-
rettes, no social dancing, and no movies. It is a scholarly version
of fundamentalism’s old refrain: “We don’t smoke; we don’t

28, DBid., p. 222n.
