192 MILLENNIALISM AND SOCIAL THEORY

tent, comprehensive, Bible-based social theory. They insist that
God will not bring unique negative sanctions against covenant-
breakers this side of Christ’s bodily return from heaven. He will
also not bring unique positive sanctions to bless the cultural
efforts of covenant-keepers prior to the Second Coming. I ask:
Then who in his right mind would devote years of study and
lots of personal capital to working out biblical principles of
politics, education, economics, or anything else, given this kind
of theology? We now have three centuries of accumulated evi-
dence that provides the answer: hardly anyone.

This is the practical problem. It is a problem that pessimil-
lennialists refuse to deal with. This is especially true of dispen-
sational premillcnnialists, who have no tradition of academic
excellence behind them, and who have not been pressured by
their peers to deal with real-world social issues.

The Dispensational View of History

Alva J. McClain wrote a five-and-a-half-page essay on “A
Premillennial Philosophy of History” for Dallas Seminary’s
Bibliotheca Sacra in 1956. McClain was the president of Grace
Theological Seminary, a school which, along with Dallas, has
dominated the training of dispensational pastors. His book on
the dispensational kingdom would soon become a standard. He
was a highly influential academic figure in these circles.

This essay should be read by every dispensationalist, not to
learn what this view of history is, which the essay never says,
but to Jearn that a major theologian of the movement did not
bother to describe it. In this essay, McClain rejected postmillen-
pialism, although he did admit that “Classical postmillennialism
had plenty of defects, but it did make a serious attempt to deal
with human history.”'® He then dismissed -- in one paragraph
per error — the following: modern liberalism, neo-orthodoxy,

16. Alva J. McClain, Th.M., D.D. [honorary], L.L.D. honorary], “A Premiflenniat
Philosophy of History,” Bibliotheca Sacra, vol. 118 (April-June 1956), p. 112. A journal that
hhas its authors list honorary degrees after their aames is a journal aimed at readers with
too much respect for academics and not enongh knowledge about what the various
academic degrecs represent. It is aimed at a moveinent with an academic inferiority
complex. Bibliotheca Sacre no longer does this, but it did in 1956.
