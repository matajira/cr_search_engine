Denying God’s Predictable Sanctions in History 171

News for Future Christian Man” view of history, will have none
of this. It is just too postmillennial for him to accept. Theologi-
cally, he is correct: Hoekema’s language here is the language of
postmillennial continuity, but it camouflages a Manichean view
of history. As I keep saying, chapter after chapter, this is
schizophrenic. It ie also intellectually dishanest.

Muether’s Verbal Legerdemain

Muether's language of God’s historical inscrutability, of this
world’s historical open-endedness, is a carefully contrived illusion,
an example of verbal legerdemain. On the one hand, he says
that the Church is in exile in history. This is a permanent con-
dition. It is guaranteed by a Calvinistic, predestinating, totally
sovercign God. On the other hand, he asserts that God’s ethical
randomness is manifested in history. “Things may improve,
things may get worse. Common grace ebbs and flows through-
out history.”"! He defines “exile” as an indeterminate condition in
which things may get better or may gel worse, yet on average
stay pretty much the same throughout New Covenant history.
(Would you like to construct an ethical system or social philoso-
phy in terms of this view of history? How about a theory of
business? Or tcchnology? No? Neither would anyone else.)

This assertion of indeterminacy, as I have already argued, is
a contrived illusion. If God applied His sanctions randomly,
then the institutional, covenantal outcome would hardly be ran-
dom; it would be perverse. Covenant-breakers would retain
control over culture throughout Church history, despite the
death, resurrection, and ascension of Christ to the right hand of
God the Father. But this is precisely what Calvinist amillen-
nialists say must happen. It is predestined by God this way.

Kline, Muether, and the Random Sanctions amillennialists
are all bearers of Bad News. A fiailine eschatology in a world
presently dominated by covenant-breakers is bad news. It is also
difficult to defend exegetically. No eschatological position that
1 am aware of has ever been defended exegetically which asserts

 

30. See Chapter 4, above, p. 91.
31. Mucther, p. 18.
