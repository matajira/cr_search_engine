For Further Reading 359

Dispensational Critics of Postmillennialism

The critics are a good deal more vulnerable to this criticism
than the postmillennialists are. I think it is time for dispensa-
tionalist critics to examine carefully their own dusty bookshelves
in search of a single, recent, book-long exposition of dispensa-
tional theology. Chafer's 1948 eight-volume set is gone, edited
(i.e., expurgated) down to two volumes. Ryrie’s Dispensationalism
Today has not been revised since 1965, and was a brief study at
that. No one at Dallas Theological Seminary or Grace Theologi-
cal Seminary has attempted to present a book-length summary
and hermencutical defense of dispensationalism in a generation.
Meanwhile, ‘Talbot ‘heological Seminary no longer bothers to
defend the system publicly.

We need to know what the prevailing, agreed-upon position
(if any) of dispensationalism is regarding Israel and the Church,
law and grace, the discontinuity between the Old and New
Covenants, the Lordship of Christ in the work of salvation, six-
day creationism, New Testament personal ethics, New Testa-
ment social ethics, natural law theory, the kingdom of God and
(if still believed to be separate) the kingdom of heaven, the
prophetic significance of the appearance of the state of Israel in
1948) prior to the Rapture (national Israel and the “any-mo-
ment coming”), the fulfillment of Jocl 2 in Acts 2 (an Old Cove-
nant prophecy of the “Great Parenthesis” Church), the relation-
ship between Amos 9:11-12 and Acts 15:15-17 (the nature of the
Davidic throne), the nature of the Melchizedckal pricsthood in
the Church Age (the restoration of Christ's kingship), Psalm 110
and Christ’s reigning from heaven, the number of New Cove-
nants (the Hebrews 8 problem), the restoration of animal sacri-
fices in Jerusalem during the millennium, the nature of the mil-
lennial work of the Holy Spirit with Christ present on earth,
and the millennial residence of raptured Christians. A commen-
tary on the Book of Revelation that is intellectually comparable
to David Chilton’s Days of Vengeance would also be appropriate.
Perhaps most important of all, we need a statement on the New
Testament legitimacy of abortion and the proper response of
Christians to legalized abortion. Roe v. Wade was handed down
in 1973, after all. The professorial silence is deafening.
