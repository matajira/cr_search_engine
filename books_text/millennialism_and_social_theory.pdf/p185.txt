Denying God's Predictable Sanctions in History 169

theonomy persist in this misrepresentation? I contend that it is
because their theological strategy is to call people’s attention
away from their comprehensive denial of Christianity’s social
relevance. They can readily sell their anti-theocratic views to
people raised on the humanistic theology of pluralism, but they
do not want to pursuc the logic of their position to its inescap-
able conclusion: the historical irrelevance of Christianity for both the
Church and the family. Thus, our affirmation of the relevance of
the Bible for the civil covenant becomes the focus of their at-
tempted refutations, ignoring the fact that this very affirmation
is inextricably entwined with our affirmation of the relevance of
the Bible for Church, family, and everything else. For rhetorical
purposes (offensive), these anti-covenantal theologians and
pastors attack our covenantal political stand. For equally rhetor-
ical purposes (defensive), they remain prudently silent about the
connection between our view of the covenant and all the other
areas of society. They want to deny the covenantal relevance of
Christianity for politics, while implicitly retaining faith in the
covenantal relevance of Christianity for other institutions. They
cannot do this logically or theologically, but they attempt it
anyway. It makes for good editorial copy. It also makes for
incoherent book-length studies. Hence, they refuse to write
book-length studies. They refuse to say how their view of Gad’s
sanctions in history relates to social theory. This is why they
offer no social theory.

Progressive Institutional Sanctification

The assumption of a radical historical discontinuity —- this
world vs. the next ~ is the theological foundation of the denial
of progressive institutional sanctification in history. This view is
promoted by amillennialists with respect to the entire post-
resurrection era, and by premillennialists with respect to the era
prior to Christ’s physical return to earth in order to establish a

Godfrey (eds.), Theonomy: A Reformed Critique (Grand Rapids, Michigan: Zondervan
Academie, 1990), p. 257. If there are repercussions from incensed donors, let Westmin-
ster bear the negative sanctions! The school no longer employs him. In this atea, at least,
Mr. Muether acted prudently. Westminster didn’t.
