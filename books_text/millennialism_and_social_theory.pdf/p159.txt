Time Enough 143

Lindsey quotes Chilton’s statement of 40,000 years and
Terry’s million years, but skips the explanation of the symbolic
usage of “one thousand” in the Bible. He also fails to mention
(or respond to) Chilton’s devastating use of “the cattle on a
thousand hills.”!* He then says that this kind of teaching makes
the Reconstructionists “So earthly minded that they are no
heavenly good.” He insists that Reconstructionists “often seem to
be more interested in political takeover than evangelizing and
discipling people for a spiritual kingdom.” This, despite Chil-
ton’s explicit statement in the chapter in Days of Vengeance that
Lindsey relies on to attack him." Chilton states as clearly and
emphatically as possible that politics is not primary.

Tt must be stressed, however, that the road to dominion does not lic
ptimarily through political action. While the political sphere, like
every other aspect of life, is a valid and necessary area of Christian
activity and eventual dominance, we must shun the perennial temp-
tation to grasp for political power. Dominion in civil government
cannot be obtained before we have attained maturity and wisdom —
the result of generations of Christian self-government”

According to Chilton, how long will it take for Christians
conceivably to be ready to exercise widespread political leader-
ship as a self-conscious, organized group? Generations. Christian
self-government first, he insists; then politics. He could not have
made it any plainer. This has always been the view of the Chris-
tian Reconstructionists. But neither Hal Lindsey nor the myriads
of other dispensational critics who attack Reconstructionism are
willing to take our words seriously, no matter how many times
we repeat ourselves. Our words simply do not register with
them. (They are beyond the Bible; I suppose it should not sur-
prise me that they are beyond our rhetoric.)

They see the word “dominion,” and just like the political
humanists, they automatically think “politics.” Like the Jews of
Jesus’ day, these men are judicially blinded: “. . . seeing [they] see

18, Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989}, p. 232.
19. Tbid., p. 292, footnote 15.
20. Chilton, Days of Vengeance, p. S11.
