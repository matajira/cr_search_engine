102 MILLENNIALISM AND SOCIAL THEORY

Church, and (3) the coming era of millennial blessings. In short,
God’s corporate cursings are continuous in history, but His
world-transforming blessings will be cosmic and discontinuous; beyond
history. Those who heard the prophets’ bad news were to fear
God, but when they heard the message of miraculous social
transformation, they were to pay no attention, except in a sym-
bolic sense: a world beyond history. Nebuchadnezzar was real;
tears were real; but the possibility of future cultural transforma-
tion is merely symbolic. If this is true, then this theological con-
clusion is inescapable: biblical social theory is also symbolic.

Here is my point: amillennialism cannot deal exegetically with
God’s positive corporate sanctions in New Covenant history. This is one
of my major contentions in this book. God’s positive corporate
sanctions are said to relate primarily to “sacred history,” meaning
the biblical narratives in the Old Covenant.’ They are past
events, aorist tense: completed. (This could be called “preterism
for eschatological pessimists”: the good news for society is past.)
But what about the possibility of a secondary application in New
Covenant history of the good news of future corporate cultural
transformation? For the amillennialist, there is no “secondarily”;
there is only “primarily.” This presents a major problem for the
development of amillennial social theory: explicitly biblical, no
humanism, and no “neutral” natural law. It makes it impossible.

“Why ‘Death’ Simply Cannot Mean Death”

Hoekema does not stop with his assertion that the tears of
Isaiah 65:19 will be wiped away only in the post-resurrection
world, He goes on. (There is an old slogan, “When you're in a
hole, stop digging.”) “In the light of the foregoing I conclude
that Isaiah in verse 20 of chapter 65 is picturing in figurative
terms the fact that the inhabitants of the new earth will live
incalculably long lives.” Notice what he is doing. First, he denies
that death actually refers to death. Second, he speaks of incalcu-
lably long lives in the New Heaven and New Earth, an odd
prophetic way of saying “eternal, dcath-{ree living.” Professor

9. On sacred history, see Hockema, Future, pp. 25-26,
