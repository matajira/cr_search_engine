What Is Social Theory? 51

historical sanctions of God’s law-order are implicitly or explicitly
being denied. God’s law, His predictable historical sanctions,
and millennialism are intertwined in God’s three institutional
covenants: Church, family, and State. Deny these covenantal
connections, and you thereby deny the possibility of constructing
a uniquely biblical social theory.

There is no neutrality in life. He who denies the possibility of
a uniquely biblical social theory must adopt a non-Christian
social theory. He may do so either consciously or unconsciously,
but he will choose something. There are no theoretical vacuums.
There are no social vacuums. Jesus taught: “No man can serve
two masters: for either he will hate the one, and love the other;
or else he will hold to the one, and despise the other. Ye cannot
serve God and mammon” (Matt. 6:24). “He that is not with me
is against me; and he that gathcreth not with me scattereth
abroad” (Matt. 12:30). This applies to social theory, too.
