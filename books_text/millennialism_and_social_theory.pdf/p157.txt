Time Enough 141

Then, to make his point, Chilton’s next paragraph spelled out
his position on the use of the number one thousand:

When God said that He owns the cattle on a thousand hills, He
means a vast number of cattle on a vast number of hills — but there
are more than 1,000 hills, The Bible promises that God’s people will be
kings and priests for a thousand years, meaning a vast number of
years — but Christians have been kings and priests for more than 1,000
years (almost 2,000 years now). My point is this: the term chousand is
ofien used symbolically in Scripture, to express vastness; but that
vastness is, in reality, much move than the literal thousand.

Chilton’s book is a model of rhetoric. He goes straight to the
heart of his opponents’ arguments in a few memorable words.
You will not soon forget this highly rhetorical delivery. (The
saying, “Never answer a question with a question!” is a favorite
of those who lose all their arguments. Jesus answered a loaded
question with an even more loaded question, and so devastated
were his questioners that they never again asked Him another
question: Matt. 22:41-46.) Chilton says that Psalm 50 speaks of
God’s ownership of the cattle on a thousand hills. Is this a literal
number, limiting God? No. “God owns aif the cattle on ail the
hills.”* He asks a classic rhetorical question: “Does Hill No.
1,001 belong to someone else?” The self-professed hermencuti-
cal literalist should now begin to feel the noose tightening
around his neck. Then Chilton pulls the lever on the trap door:

In the same way — particularly with regard to a highly symbolic book
~ we should see that the “1,000 years” of Revelation 20 represent a
vast, undefined period of time. It has already lasted almost 2,000
years, and will probably go on for many more. “Exactly how many
years?" someone asked me. “I'll be happy to tell you,” I cheerfully
replied, “as soon as you tell me cxactly how many hills arc in Psalm
50,"

Snap! There went dispensationalism’s forced litcralism of Revel-
ation 20. The lifeless body is twisting slowly, slowly in the wind.

14. ibid., p. 199.
15. Idem.
