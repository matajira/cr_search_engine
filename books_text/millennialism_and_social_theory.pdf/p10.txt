x MILLENNIALISM AND SOCIAL THEORY

judgment” (Heb. 9:27). This covenantal (judicial) decision will
determine where the person spends eternity, so it has to be far
more important than eternity itself, After all, something that
Adam and Eve did on earth and in history got humanity into
this frightful legal position in the first place.

Jesus was quite clear about what is most important in life and
death, and why:

Verily, verily, I say unto you, He that heareth my word, and be-
lieveth on him that sent me, hath everlasting life, and shall not come
into condemnation; but is passed from death unto life (John 5:24).

“Ts passed from death unto life”: this is the heart of the matter.
When we say to someone, “You're history,” we mean it is all
over for him in our eyes. When, at the moment of a person's
death, God says, “You're history,” He really means “You're
eternity.” It is all over for him in God’s eyes. These are the only
eyes that really count. So, the greatest problem facing the world
today is the same old problem: most people have not accepted
Jesus Christ's atoning work on the cross as their only legitimate,
acceptable payment to a God of wrath. (How about you?)
Today, the numbers of people on earth are staggering. Be-
tween five billion and six billion people are now alive. These
numbers are expected to grow, short of some unforeseen calam-
ity like a plague or world war, But if the gospel continues to be
rejected by at least 90% of these people, as is the case today,
then most people are facing a gigantic calamity that only Chris-
tians accurately foresee. They are headed for eternal wrath,
Here is the question of questions: “Are the vast majority of
these people inevitably doomed to hell?” Put another way: “Are
the vast majority of these people predestined by God to hell?” We
cannot lawfully answer this question; only God knows. “The
secret things belong unto the Lorp our God: but those things
which are revealed belong unto us and to our children for ever,
that we may do all the words of this law” (Deut. 29:29). But
there is nothing in the Bible that tells us that we should assume
that the vast majority of them are inevitably doomed. We must
work and pray on the assumption that something can be done
