134 MILLENNIALISM AND SOCIAL THEORY

political pluralism to covenantal Christian reconstruction. They
almost always have.

Because Christians do not fully understand the covenantal
implications of the faith, and also because churches drift into
apostasy, Christianity steadily gives up ground to the enemy. It
spreads westward, but as it moves forward, it surrenders its rear
flanks. The history of Christianity can be seen on a globe of the
world. It would appear on the globe as a shadow about 2,000
miles wide. As it moves north or west, it surrenders in the south
and east. Arab Muslims took North Africa and Spain on Eur-
ope's southern flank while Irish missionaries were spreading the
gospel in Northern Europe (632-732). Turkish Muslims took
Byzantium (1453) just before Western Christianity crossed the
Atlantic. Enlightenment paganism took Europe while Protestant
Christianity was spreading westward in North America. The
only major exceptions in history have been Catholicism’s re-
conquest of Spain (732-1492) and the Greek revolt (1821-22).

We do not see God’s positive historical discontinuities apart
from His negative discontinuities against those being displaced.
Nevertheless, the program of thc Church is peaceful positive
displacement, soul by soul. God wins, Satan loses: soul by soul. Who
brings the necessary negative corporate sanctions? God does, not
through the Church but through such means as pestilence,
plague, and war. The Church is supposed to pray for God’s
negative discontinuities in history against entrenched corporate
evil. This is why God gave us His imprecatory psalms to sing
and pray publicly in the Church (e.g., Psalm 83).

Here is the biblical program for cultural transformation. First,
the Church is to bring continuous positive sanctions into a cove-
nant-breaking culture: preaching, the sacraments, charity, and
the disciplining of its members (a negative sanction by the
Church, but positive for society: it keeps other Christians more
honest). Second, the Holy Spirit must also bring positive disconti-
nuities into individual lives: conversion. This is at His discretion,
not ours. Third, a sovereign God in heaven must bring His dis-
continuous, corporate, negative sanctions against covenant-
breakers in history. Notice, above all, that it is God who brings
negative corporate sanctions in society, not the Church. The
