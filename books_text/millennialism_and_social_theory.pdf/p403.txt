Puritan wing, 240
relevance (quest for), 205-6
sacrificing Christians, 276
sce also alliance

Index 387

poverty, 186

power, 44, 95, 150-51, 270-71,
284

power religion

plague, 289, 291, 293, 298, 322
platitudes, 259, 327

plowing deep, 153

pluralism

civilization and, 332
Marxism, 42

mysticism, 156
premillennialism, 150-51

Athanasian, 284
cease-fire, 45, 125-26
Christians’ preference, 134

Pratt, Richard, 77-78
predestination, x, 145, 274
Premillennialism

contractualism &, 283
equal time, 305-6
natural law, 48
tie game, 218
politics
amillennialism and, 168
assumptions about, 145
dominion and, 143-45
fourth, 168
humanistic?, 145
importance of, 145
pollution, 59
population, x, 55-56, 307-8, 310
population bomb, ix-x, 307-10
Postan, Larry, 30-31
postmillennialism

activism, 48

amillennialism &, 92-94

apocalyptic, 270-73

discontinuity, 22-23, 150,
312

ethical osmosis, 75

Great Tribulation, 193

Holy Spirit, 257

power religion, 150-51

new bodies, 152

sanctions, 152

social theory, 74-76

system, 18-19

time perspective, 147-48

trend-tending, 194

victory beyond history, 150

antinomian, 247
compound growth, 148
continuity and victory, 283
covenantal, 15
discontinuity, 273, 288-89
divided, 238

doctrine, 20-22

history, 239

inescapable concept, 116
Loraine Boettner, 243-47
nineteenth century, 242-43
perverse, 182

sanctions, 241

summary of, 20-22
utopian?, 273-74

potter, 224

wheat and tares, 296-98
present-orientation, 34
preterism, 102, 130
pride, 285, 334
printing press, 289
problems (escaping), 23
progress

acceptance of, 26-28

ambiguity, 90-91

assumptions of, 14

biblical basis, 52-53

biblical idea of, 15

coyenantal structure, 14

creedal, 27-28, 220-21

denial of, 278

economic growth, 65
