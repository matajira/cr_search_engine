Appendix: The Lawyer and the Trust 345

Smurd: Well, that depends on the profitability of the trust.

You: What has it been in the past.

Smurd: Up and down.

You: I just have to wait until I get paid.

Smurd: That's correct. But there’s another aspect of the trust
that you nccd to know about.

You: What now?

Smurd: There are other named beneficiaries of the interme-
diate payments,

You: Who are they?

Smurd: Everyone on earth.

You: You mean, my share is diluted by everyone?

Smurd: Not exactly.

You: Why, “Not exactly”?

Smurd: Because the periodic distributions are not made
randomly.

You: How are they distributed?

Smurd: The people who obey the terms of the trust instru-
ment are paid less than those who do not obey it.

You: That means that the person who does what the Trustor
wants him to do will lose.

Smurd: Only with respect to the periodic distributions.

You: This seems unfair.

Smurd: You must not call the Trustor unfair.

You: But what good does it do to obey the trust's stipula-
tions?

Smurd: Those of you who obey will be the only ones to
participate in the final distribution of the trust’s assets.

You: Then what about the periodic assessments? Are they
distributed randomly.

Smurd: I’m afraid not.

You: The people who break the terms of the trust pay more?

Smurd: I’m afraid not.

You: Are you telling me that the Trustor has sct up this trust
so that those of us who do what He says pay more to cover any
losses than the people who disobey Him?

Smurd: That is correct.

You: But what if I do not want to participate?
