Will God Disinherit Christ’s Church? 269

activist wing of the Spiritualist movement” soon became com-
munist, revolutionary, and finally polygamous. It was openly
opposed by Luther and Calvin.* Similar movements had
sprung up in Europe ever since the thirteenth century. It took
a decade for the combined military forces of Europe to crush
them. The revolt ended in the city of Miinster (1534-35). From
that time on, the bulk of the spiritualist Anabaptists have been
passivists and even pacifists; they have also tended to hold prop-
erty in common.* The Amish, Hutterites, and Mennonites are
products of this passivist Anabaptist tradition.
Who, then, are the apocalyptic millennialists today?

Pessimillennialism

The pessimillennialist denies that there is a co-partnership
between God and the Church in bringing discontinuous social
change. Personal transformation, yes: faith cometh by hearing,
and hearing by the word of God (Rom. 10:17). The Church
preaches the gospel, and the Holy Spirit then moves men to
tespond. But this is supposedly not God’s means of social trans-
formation. To achieve God-honoring social transformation, the
pessimillennialist bclicves, God must unilaterally impose a cosmic
discontinuity. “The work of Christians in history has very little
directly to do with this cosmic discontinuity. It is solely the work
of God. The continuity of the Church’s work in history has little
or nothing wo do with the positive transformation of society.”

Amillennialism. The amillennialist is an apocalyptic, but a pas-
sivist. He believes in the legitimacy traditional order, but he
expects little from it. The best he can hope for is social peace.
He sees himself as a member of a spiritual ghetto. He adopts
the mentality of the Amish. He may drive a car and have elec-
tricity in his home, but he sees no hope in the future. His only

27. On the Spiritualists, sce Henning Graf Reventlow, The Authority of the Bible ond the
Rise of the Modern World (London: SCM Press, [1980] 1984), ch. 1.

28. Martin Luther, “Against the Robbing and Murderous Hordes of Peasants” (1525),
Luther's Works (Philadelphia: Fortress Press, 1967); Willem Balke, Calvin and the Anabaptist
Redicats (Grand Rapids, Michigan: Eerdmans, 1981),

29. Leonard Verduin, The Reformers and Their Stepchildren (Grand Rapids, Michigan:
Baker, [1964] 1980), ch. 7.
