302 MILLENNIALISM AND SOCIAL THEORY

Second, biblical theology is applied theology. Third, by their
fruits are we to distinguish competing theologies.

One of the most neglected fruits of theology in the history of
the Church has been social theory. There are many reasons for
this. First, as I have attempted to show in this book, there has
been a commitment to millennial views that deny the very possi-
bility of the expansion of God’s institutional kingdom in history.
Christians have denied that kingdom means civilization, at least
with respect to God’s kingdom. God’s kingdom is not acknowl-
edged as a civilization, even though Satan’s kingdom is freely
acknowledged as a civilization. Second, there has been a denial
of God's predictable sanctions in history, either applied directly
by God or representatively by His covenant people. Third, there
has been a denial of covenant law in the New Testament era.

To remove biblical law from eschatology is to castrate the
kingdom of God. Without biblical law, the idea of God’s pre-
dictable sanctions in history inevitably disappears, The rule is
this: zo law, no sanctions. More to the point: no written law, no
predictable sanctions. Fach millennial view requires a particular
concept of God's sanctions in history. Deny that God brings
predictable positive and negative sanctions in history — sanctions
that are governed by the terms of His Bible-revealed law — and
you deliver Christians into the hands of covenant-breakers.
Either you argue, as premillennialists and amillennialists do,
that the effects of the gospel will not be culture-transforming, or
else you wind up as non-theonomic postmillennialisis have:
incapable of specifying the judicial conditions by which we can
correctly evaluate the coming of God’s millennial blessings.
Without a biblical, judicial theology, the New Age millennium!
and/or the New World Order could not be distinguished from
God’s age of millennial blessings. Neither pessimillennialism nor
pietistic postmillennialism can provide the theological founda-
tion for the establishment of God’s kingdom in history.

Then what is the alternative?

1. Alberto Villoldo and Ken Dychtwald (eds.), Millennium: Glimpses into the 21st Century
(Los Angeles: J. B. Tarcher, 1981),
