Pessimillennialism 85

The text is specific: the elders in heaven will reign upon the earth.
Because they were in a disembodied spiritual state at the time
of John’s revelation, they can be said to reign on earth in the
future in one of four ways: (1) physically (during pop-dispensa-
tionalism’'s future milennium); (2) representatively (during
postmillennialism’s progressive judicial millennium); (3) post-
historically (after the final judgment), thereby making the whole
passage irrelevant for history; or (4) symbolically.

This passage presents a major dilemma for the amillennialist
expositor. He cannot appeal to either of the first two exegetical
options and still remain amillennial, yet he does not want to
adopt the third, since this section of the Book of Revelation is
generally believed by commentators to apply to history, not to
the post-resurrection state. His only other choice is to interpret
the Revelation 5:10 symbolically. Such an appeal to symbolism
also destroys the passage’s relevance for history.

William Hendriksen, one of the premier amillennial exposi-
tors in the twentieth century, cannot gracefully avoid this prob-
lem in his commentary on the Book of Revelation. He barely
tries to escape. He begins with this presupposition: “The theme
of this book is: the Victory of Christ and of his Church over the Drag-
on (satan) and his Helpers. The Apocalypse intends to show you,
dear believer, that things are not what they seem!** Things
must be very different from what they seem to Mr. Hendriksen,

 

48. The possibility of mixing resurrected saints and fallen humanity during the
coming millennium has not been taken seriously by professionally trained dispensational
theologians (¢.g., John Walvoord, J. Dwight Pentecost), but popularizers of the dispensa-
tional position (e.g., Dave Hunt) have asserted that this will take place. See John Wal-
voord, The Rapture Question (rev. ed.; Grand Rapids, Michigan: Zondervan, 1979), p. 86;
J. Dwight Pentecost, “The Relation between Living and Resurrected Saints in The
Millennium,” Bibliotheca Sacra, vol. 117 (Oct. 1960), pp. 387, 341. Hunt offers his contrary
opinion: “After the Antichrist’s kingdom has ended in doom, Jesus will reign over this
earth at last. Which of these kingdoms we will be in depends upon the choice we make
now — for God's truth or for the Lie.” Dave Hunt, Peace Prosperity and the Coming Holo-
saust (Eugene, Oregon: Harvest House, 1983), p. 263; see also below, p. 256. In 1988,
during his debate with me and Gary DeMar in Dallas, he re-stated his view that resurrect-
ed saints will rule on earth during the millennium, A set of audiotapes or a videotape of
that debate can be ordered from the Institute for Christian Economics.

49. W. Hendriksen, More Than Conquerors: An Interpretation of the Book of Revelation
(Grand Rapids, Michigan: Baker, [1989] 1965), p. 12.
