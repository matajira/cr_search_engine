340 MILLENNIALISM AND SOCIAL THEORY

which for two hundred years had been the most dynamic force
in the politics of the West and increasingly in politics world-
wide, creates a void.”'* He insists that “we are not going to
return to a belief in salvation by faith as a major political fac-
tor,” but he is wrong, Salvation by faith is going to become the
major factor in every area of life, including politics. The present
historical trends, which Drucker probably understands better
than any other contemporary social commentator, do not tell
him what he needs to know. These trends are going to be dis-
rupted by a divinely imposed discontinuity. The question is
when, nat if.

There is only one long-term solution to modern man's crisis:
a comprehensive revival leading to the transformation of all
things and the healing of all the nations.'* This means that
Christianity needs to offer the people of this world a better
promise than “pie in the sky by and by,” yet this is all that pre-
millennialism and amillennialism can honestly offer to those
who join the Church, While premillennialists and amillennialists
may resent this statement, they need to show why I am wrong,
not merely by writing a defense of the theoretical possibility of
pessimillennial social theory (though I doubt that this can be
done), but by actually writing biblical social theory. They have
neglected to do so for over three centuries. Our enemies have
noticed this silence. They correctly conclude that the modern
Church is suffering from both a defective epistemology and a
defective ethical system, and they have dismissed the gospel as
a message fit only for children and old women of both sexes.

Christianity needs to offer a detailed, comprehensive cultural
alternative to the existing humanist social order. We cannot
beat something with nothing. The Church needs to offer cove-
nant theology and social theory based forthrightly and consis-
tently on the biblical covenant model. Until it does, the Church
will continue to suffer from pie in the face in history.

18. Drucker, The New Realities, p. 18.
19. Gary North, Healer of the Nations: Biblical Blueprints for International Relations (Ft,
Worth, Texas: Dominion Press, 1987).
