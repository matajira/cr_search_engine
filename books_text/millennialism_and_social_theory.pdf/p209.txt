Historical Sanctions: An Inescapable Concept 193

amilennialism (Louis Berkhof), and all those who think “there
will never be such a ‘Golden Age’ upon earth in history. , . .”””
This left exactly half a page for a thorough discussion of the
premillennial view of history. He never did say what this is. He
simply concluded, “The premillennial philosophy of history
makes sense. It lays a Biblical and rational basis for a truly
optimistic view of human history.”

McClain refused even to mention the key historical issue for
those living prior to the Rapture: What is the premillennial basis
for Christians’ optimism regarding the long-term effects of their earthly
efforts? Clearly, there is none. The results of all of their efforts,
pre-tribulational dispensational premillenniatists would have to
say if they had the courage to discuss such things in public, will
all be swallowed up during the seven-year Great Tribulation
after the Rapture. Even those people converted to Christ by
today’s evangelism will all be either dead or Raptured out of
history. All that will be left behind is a temporary glut of used
cars with “I brake for the Rapture” bumper stickers,

This is a self-consciously pessimistic view of the future of the
Church, and it has resulted in the triumph of humanism when-
ever it has been widely believed by Christians; therefore, the
intellectual leaders of dispensationalism refuse to discuss it
forthrightly. It is just too embarrassing. They use the language
of postmillennial optimism to disguise a thoroughgoing pessi-
mism. They keep pointing to the glorious era of the millennium
in order to defend their use of optimistic language, never both-
ering to admit out that ihe seven years that precede the millennium
will destroy the results of gospel preaching during the entire Church
Age. After all, every Christian will have been removed from the
earth at the Rapture, whether it will be pre-trib, mid-trib, or
post-trib. (This is an explicit denial of the historical continuity
predicted in Christ’s parable of the wheat and tares [Matt.
13:20, 38-43}). McClain’s essay is representative of what has
passed for world-and-life scholarship within dispensationalism
since 1830. It avoided any discussion of the premillennial view

17. Bid., p. 125.
18. Pbid., p. 116.
