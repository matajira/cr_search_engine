216 MILLENNIALISM AND SOCIAL THEORY

and poverty, (but thou art rich) and I know the blasphemy of
them which say they are Jews, and are not, but are the syna-
gogue of Satan. Fear none of those things which thou shalt
suffer: behold, the devil shall cast some of you into prison, that
ye may be tried; and ye shall have tribulation ten days: be thou
faithful unto death, and I will give thee a crown of life” (Rev.
2:9-10). Ten days of tribulation, and even the possibility of
death, but they had God’s assurance of victory. But would this
mean victory in history for those who would survive the persecu-
tion, as well as victory in heaven for those who would die? Yes.
The book of Revelation presented the Church with a promise
from God: the persecution of the Church under Isracl would
soon end, when Israel would be brought under final judgment
nationally.’ ‘That event took place within a matter of months.*
Thus, the period of prophesied persecution in John's day was
short. Then came God’s predicted, negative, culture-wide sanc-
tions against. Old Covenant Isracl: the Great Tribulation.*

There will be persecution of Christians, but the end result is
the destruction of evil-doers. They persecute us, but in doing
so, they grow progressively deceived and progressively impotent:

Now as Jannes and Jambres withstood Moses, 50 do these also
resist the truth: men of corrupt minds, reprobate concerning the
faith. But they shal} proceed no further: for their folly shall be
manifest unto all men, as theirs also was. But thou hast fully known
my doctrine, manner of life, purpose, faith, longsuffering, charity,
patience, persecutions, afflictions, which came unto me at Antioch,
at Iconium, at Lystra; what persecutions I endured: but out of them
all the Lord delivered me. Yea, and all that will live godly in Christ
Jesus shall suffer persecution. But evi! men and seducers shall wax
worse and worse, deceiving, and being deceived {II Tim. 3:8-13).

Representation
In what way did the disciples judge Israel in history? By

2. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987).

8. Kenneth L. Gentry, Jr, Before ferusalem Fell: Dating the Book of Revelation (Fyler,
Texas: Institute for Christian Economies, 1989).

4. David Chilton, The Great Tribulation (Ft. Worth, Texas: Dominion Press, 1987).
