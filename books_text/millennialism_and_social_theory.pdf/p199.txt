Denying God’s Predictable Sanctions in History 183

to put it, but it is exactly what their system implies. On the
surface, however, this sounds bad. (Frankly, it is bad.) To cover
themselves, they adopt the language of randomness and inscru-
tability in self-defense. This is the language of neutrality.

What these theologians need is biblical exegesis. They nced
to answer three sets of questions. First, how can common grace
amillennialism be defended? If common grace is being with-
drawn (Van Til’s view), how can Gad’s blessings to the lost
increase? How do the lost gain and retain power in history,
which they need in order to subdue Christian culture? If, on
the other hand, common grace is not being withdrawn, then
what connection does common grace have with biblical ethics?
What connection does it have with special (soul-saving) grace?
If common grace neither expands nor contracts in response to
the Holy Spirit’s gift of special grace in history, what docs com-
mon grace have to do with history? It had a lot to do with
history before the cross. What does it have to do with history
today? Explain, please. Use the Bible to defend your answers,
please. No more unsupported pronouncements from on high.

Second, how can amillennialism be anything but pessimistic
with respect to the future of the Church? If God’s sanctions are
random, then history is a/ best a flat line. But covenant-breakers
today control most of the world. An extension of present trends
keeps the Church in a condition of permanent historical exile.
In other words, how can there be such a thing as optimistic
amillennialism? Isn’t optimistic amillennialism a form of soft-
core postmillennialism? Explain, please. Use the Bible to defend
your answers, please.

Third, how can the amillennial version of common grace
present God’s historical sanctions as essentially random or at
least inscrutable in a world in which the cultural leaven of
covenant-keeping is supposedly being overcome progressively
by the more powerful leaven of covenant-breaking? Such a world
is clearly not ethically random, it is merely ethically perverse. History
is either moving downward into the void or upward toward the
heavenly Jerusalem. This is what millennialism teaches. So, how
can God’s covenant sanctions in this common grace world be
random? Explain, please. Use the Bible to defend your answers.
