288 MILLENNIALISM AND SOCIAL THEORY

The faster you want to achieve it, the more you will have to
pay. It is like building a retirement portfolio: the longer you
have until retirement, the less capital you need (given a fixed
rate of compound growth) to begin with. Alternatively, the
higher the rate of compound growth, the later you can wait
before beginning. But there are always trade-offs among time
remaining, the size of the capital base, and the rate of growth.

The modern Church believes that it has very little time re-
maining. It also knows that it is being swamped by its rivals:
secular humanism, occultism, Islam, cults, and all the rest. It
has a small visible capital base, in every sense: buildings, influ-
ence, money, dedicated personnel, training materials, etc. What
does the modern Church conclude? “Not very much can be
accomplished!” It has no vision of either compound growth or
a long period of growth. Its leaders say to the members, “What
we see today is all the Church will ever get in history.”

In contrast, the postmillennialist sets his sights very high: the
conquest (transformation) of the world, spiritually and therefore
institutionally, He can take two approaches: (1) continuity with
lots of time; (2) discontinuity soon, followed by lots of time.

Continuily. He can think, “slow growth, but very long term”:
little by little. If so, he must hypothesize that at some point in
the future, all covenantal rivals stop growing or shrink. They
die off. Christianity then wins by slow attrition. But what about
the six billion people already alive today?

All the other millennial viewpoints dismiss them. It’s “Sorry,
Charlie,” to about 5.5 billion of them (more, if the Second
Coming is delayed). This is at the very !cast cold-blooded, if not
actually callous. But the pure little-by-little postmillennialist has
the same problem. He thinks that there is a lot of time before
Jesus Christ comes again. What will happen in the centuries
ahead to all of these people and tens of billions more of their
biological heirs? Is the compounding process working today to
fill up hell? If things do not change, and change soon, yes: for a
long, long time. He, too, must write off today’s billions.

Discontinuity. The postmillennialist therefore prefers a mas-
sive historical discontinuity, but not one outside of the familiar
historical processes of evangelism and church-planting. He
