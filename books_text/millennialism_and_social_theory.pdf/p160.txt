144 MILLENNIALISM AND SOCIAL THEORY

not; and hearing they hear not, neither do they understand”
(Matt. 13:13b). They apparently incapable of comprehending
what they read (I am here assuming they actually do read our
books, which is probably naive), so they announce this grotesque
misrepresentation to their followers: “. .. Dominionists believe
that the Church must politically conquer the world. . . .”#"

Do they have a moment's twinge of conscience? Not that I can
detect. Lindsey allowed The Road to Holocaust to reprinted in
paperback without bothering to correct even the incorrect
names (e.g., his “John Rousas Rushdoony” instead of Rousas
John Rushdoony) and other misstatements of fact. It was as if we
had not published The Legacy of Hatred Continues just 30 days
after The Road to Holocaust appeared. Anything for the cause
{and their book royalties). Theirs is the “Sanballat Strategy.”

And they wonder why they are losing the battle to half a
dozen men with word processors!

Reconstructionists Deny Political Salvation

What do we teach? We teach that the gospel of Jesus Christ,
whenever empowered by the Holy Spirit,” will progressively conquer
the hearts and minds of men, and as a result, will conquer the
cultural world, which includes politics. This is a very different
perspective from the wholly perverse idea that the Church of
Jesus Christ must use political power in order to conquer the
world, But the critics, pessimillennialists and humanists alike,
cannot imagine that the gospel possesses this degree of authority
and power, even when the Holy Spirit imparts His irresistible
saving grace to men.

The Calvinists tell us (implicitly) that God has foreordained
the historic failure of His gospel. In contrast, the Arminians tell
us (explicitly) that the mass of autonomous mankind will never
convert to saving faith. (How they know this in a supposedly
non-predestined world is a mystery.) The humanists tell us that

21. David Allen Lewis, Prophecy 2000 (Green Forest, Arkansas: New Leaf Press, 1990),
p. 282.

22. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler, Texas:
Institute for Christian Economics, 1985), pp. 185-86.
