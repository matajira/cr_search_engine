344 MILLENNIALISM AND SOCIAL THEORY

neglected to mention.
You: What’s that?
Smurd: No death dutics!

Common Grace Amillennialism: Van Til’s Version

Smurd: Well, sir, I have good news for you. You have named
as a beneficiary of my client’s worldwide trust. All taxes have
been paid. This is a pure windfall to you.

You: This is tremendous news. I’ve been having a terrible
time making ends meet. I’m behind on some of my payments.
This money is just what I need.

Smurd: Ah, yes. The money. Well, there is a lot of it, I can
assure you. But there are certain conditions of this trust.

You: Conditions? What kind of conditions?

Smurd: Weill, you must obey the stipulations of the trust.
These are found in this book I am handing you.

You: It’s a Bible.

Smurd: Yes, it’s a Bible. You must obey it.

You: You mean all of it?

Smurd: In principle, all of ic.

You: Specifically, what parts?

Smurd: Specifically, 1 am not allowed to disclose such infor-
mation to you at this time.

You: When, then?

Smurd: Only at the time when the full capital value of the
trust is transferred to the beneficiaries.

You: When will that be?

Smurd: After you are all dead.

You: This must be a joke.

Smurd: It’s no joke. It is what the trust document requires.

You: You're telling me that I don’t get any money until
we're all dead?

Smurd; Why, not at all! You have misunderstood me. I was
speaking only of the trust's full capital value. You will be given
periodic distributions from the trust’s income.

You: When?

Smurd: Periodically.

You: How large will these distributions be?
