Will God Disinherit Christ's Church? 267

Who Gets Rich?

The socialists adopted a highly successful slogan, “The rich
get richer, and the poor get poorer.” It is a big lie. The Bible
teaches that in the long run, the covenantally faithful get richer,
and the covenantally rebellious get poorer. This is denied by the
humanists, who deny any visible manifestations of God's cove-
nant in history, and it is also denied by Christian pietists and
retreatists, who also want no public, national manifestations of
God's covenant in history. God’s covenantal system of blessings
and cursings is designed to produce long-term victory for
Christ's people in history. This long-term increase in Christians’
personal responsibility to extend God’s dominion on earth is opposed
by both humanists and Christian pietists. The humanists do not
want Christians to inherit authority in history, for they want to
retain monopoly power over history. Christian pietists also do
not want Christians to inhcrit authority in history, for with
authority necessarily comes responsibility.

Men are responsible before God, and this means that we are
responsible in terms of permanent standards. This means God’s law.
The more cultural authority that Christians inherit from God,
the harder they must strive politically to enact God’s revealed
laws in the legal codes of each nation. A Christian society’s legal
order should reflect the requirements of revealed biblical law.
So should the international legal order that is established pro-
gressively by Christian nations. The implicit covenantal division
between sheep and goats — national entities — must be made
increasingly visible as time goes by, “in earth as it is in heaven”
(Matt. 5:10b). This process of kingdom conquest through covenantal
Separation in history must include the realm of politics, although
politics is not to be regarded as primary.

Apocalypticism and Social Change

When people believe that whatever they are capable of
accomplishing in history is minimal, they will tend not to strive
to achieve very much: high expected costs coupled with very
low expected returns. This outlook is hostile to the idea of
historical progress. Societies that have no concept of historical prog-
