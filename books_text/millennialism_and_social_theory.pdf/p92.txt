76 MILLENNIALISM AND SOCIAL THEORY

tutions of higher academic certification.’ Neither group has
shown that the Bible supports its position.

Is Christian social theory relevant? Second, is it possible? The
premillennialists make social theory irrelevant by asserting the
progressive impotence and external defeat of the Church and
Christian institutions prior to the Second Coming of Jesus
Christ to set up an earthly millennial kingdom. But far more
important for the development of Christian social theory, this
perspective, being antinomian with respect to God’s covenant
law, denies the existence of God’s culture-wide sanctions in
history, at least prior to Jesus’ millennial kingdom on earth. As
I argue in this book, this anti-sanclions perspective makes impos-
sible the development of a specifically biblical social theory.

Because the modern premillennial, dispensational tradition
has not been interested in scholarship in general, its spokesmen
have not been expected by the movement’s own members to
provide a biblical social theory. All they have had to do is voice
their approval of the U.S. Constitution, free enterprise, the
nuclear family, and the American civil religion. But the amillen-
nial tradition is different. For one thing, it has been more ori-
ented toward European thought and culture, in which it has its
historical roots. For another, it has been far more interested in
scholarship. This has been especially true of the Dutch Calvinist
wing, which sometimes imitates German scholarship in its rigor
and prolixity (¢.g., Herman Ridderbos). The question arises:
What about amillennial social theory? Does such a thing exist?

Amillennialism

The amillennialists share two key viewpoints with the premil-
lennialists: entinomianism with respect to the authority of the Old
Testament case laws and historical pessimism regarding the cultur-
al efforts of Christians in history. Historically, the Lutherans
have admitted as much. They are amillennial.'’ Luther was an

16. In this sense, Carl F. H. Henry is an anomaly; he, too, is generally conservative

politically, but he does not write much about politics. Henry was educated before the

post-War expansion of higher education in America, ie., before liberalism took over.
17, The “End Times”: A Study on Eschatology and the Millennium, Report of the Commis-
