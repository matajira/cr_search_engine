18 MILLENNIALISM AND SOCIAL THEORY

fourth view.)" They are three completely irreconcilable view-
points. Nevertheless, they overlap in curious ways.

Premillennialism
The premillennial view teaches that Jesus Christ will return
to earth in history to set up a visible kingdom that will last one

thousand years. Then will come the final judgment. This is a
literal interpretation of the prophecy in Revelation 20:

And I saw an angel come down from heaven, having the key of
the bottomless pit and a great chain in his hand. And he laid hold
on the dragon, that old serpent, which is the Devil, and Satan, and
bound him a thousand years, And cast him into the bottomless pit,
and shut him up, and set a seal upon him, that he should deceive
the nations no more, till the thousand years should be fulfilled: and
after that he must be loosed a little season (Rey. 20:1-3).

Premillennialism has had a checkered history. It has been
called chiliasm, from the plural of the Greek work for thousand:
chilia. Many people in the early Church held this position, but
Augustine rejected it. So did the major Protestant reformers.
Most evangclicals today are premillennialists.

A variant of premillennialism, called dispensationalism, is
dominant in modern fundamentalism, and has been since the
late nineteenth century. This viewpoint was first developed
sometime around 1830." It focuses its attention today on a
coming Great Tribulation for the statc of Israel which will begin
seven years before Christ returns to set up His earthly millenni-
al kingdom.’ Most dispensationalists are pre-tribulational.
This pre-tribulational eschatology teaches that Christians will be

12 Robert G. Glouse (ed.), The Meaning of the Millennium: Four Views (Downers
Grove, Ilinois: InterVarsity Press, 1977); Millard J. Erickson, Contemporary Options in
Eschatology: A Study of the Millennium (Grand Rapids, Michigan: Baker, 1977).

13. Clarence B. Bass, Backgrounds to Dispensationalism: His Historical Genesis and Erelesi-
astical Implications (Grand Rapids, Michigan: Eerdmans, 1960); C. Norman Kraus, Dis-
pensationalism in America: His Rise and Development (Richmond, Virginia: John Knox Press,
1958); Dave MacPherson, The Great Rapture Hoax (Fletcher, North Carolina: New Puritan
Library; 1983).

14, Hal Lindsey, The Late Great Planet Earth (New York: Bantam, [1970]).
