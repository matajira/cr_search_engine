The Sociology of Suffering 237

If this is “realized” eschatology, I'd prefer another option. So
would a Jot of other Christians, which is why Calvinistic amil-
Jennialism cannot recruit and keep the brighter, more activist
students. Gaffin tells his disciples that they, like the Church,
have a lifetime of frustration ahead of them. This comforts the
pietists among them, but it drives the activists in the direction
of covenantal postmillennialism, which offers a consistent and
Bible-based alternative. Gaffin’s amillennialism of pre-1940
Holland cannot compete effectively against it.

Westminster Seminary and Reformed Presbyterian in general
need to return to the optimistic vision presented by J. Gresharn
Machen in 1932, in the midst of his courageous battle against
theological liberalism in the Northern Presbyterian Church. As
a postmillennialist of the Princeton Seminary variety, he be-
lieved in a coming discontinuity, a burst of new power:

We who are reckoned as “conservatives” in theology are seri-
ously misrepresented if we are regarded as men who are holding
desperately to something that is old merely because it is old and are
inhospitable to new truths. On the contrary, we welcome new dis-
coveries with all our heart; and we are looking, in the Church, not
mercly for a continuation of conditions that new exist but for a
burst of new power. My hope of that new power is greatly quick-
ened by contact with the students of Westminster Seminary. There,
it seems to me, we have an atmosphere that is truly electric. It
would not be surprising if some of these men might become the
instruments, by God’s grace, of lifting preaching out of the sad rut
into which it has fallen, and of making it powerful again for the
salvation of men.”

Sadly, he failed to articulate his eschatology, and his succes-
sors at Westminster abandoned it. The amillennialism of Dutch
Calvinism soon triumphed at Westminster. His academic and
ecclesiastical successors have had no faith in the burst of new
power that he dreamed of. In this sense, it is the Christian
Reconstruction movement that is the spiritual heir of Machen.

29. J. Gresham Machen, “Christianity in Conflict,” in Vergilius Ferm (ed.), Contempo-
rary American Theology (New York: Round Table Press, 1982), I, pp. 269-70.
