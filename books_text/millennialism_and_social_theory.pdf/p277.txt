Will God Disinherit Christ's Church? 261

capable concept. Therefore, Christians must either remain in
“exile,” or else they must seek deliverance. To seek deliverance
is necessarily to seek dominion. There is no neutrality. To seek
dominion is to seek a biblical social alternative. Today, most Chris-
tians, like their spiritual forebears in Egypt, Assyria, and Baby-
lonia, much prefer cxile. They prefer the leaks and onions of
Egypt to. the responsibility of comprehensive reconstruction.

God requires much more from His people. He required
more from them in the wilderness of Sinai, and He requires
more today. But like the Israelites in the wilderness, modern
Christians, especially the leaders, do not want to hear a message
of comprchensive responsibility, let alone preach one. Such a
message of responsibility means confronting the Canaanites who
control the Promised Land (the whole earth: Matt. 28:18-20).
Fed up with today’s manna, they are nevertheless unwilling to
risk conquering Canaan. After all, as Mr. Lewis so aptly put it:
“Unnecessary persecution could be stirred up.”

Until the amillennialists and premillennialists offer Bible-
based suggestions for Christians to pursue God’s comprehensive
redemption in New Testament times, their millennial systems
will remain fringe theologies for cultural ghettos. There is a
lesson in Western history that is dangerous to ignore: where
there are ghettos, there will eventually be pogroms of one kind
or another. Far better to win the whole society to Christ.

What about God's sanctions in history? What about the fu-
ture? With the history of God’s redemptive acts in history as
our background, one thing seems certain: we can expect a
major discontinuity. Soon. And I don’t mean the Rapture, This
discontinuity probably will not be exclusively positive.

Comprehensive Salvation

Salvation is more than just personal. It is institutional. It is
even national. We know this has to be the case because of the
nature of God’s final judgment.

20. David Allen Lewis, Prophecy 2000 (Forest Green, Arkansas: Green Leaf Press,
1990), p. 277.
