The Society of the Future 119

So the people of Nineveh believed God, and proclaimed a fast, and
put on sackcloth, from the greatest of them cven to the least of them.
For word came unto the king of Nineveh, and he arose from his
throne, and he laid his robe from him, and covered him with sack-
cloth, and sat in ashes. And he caused it to be proclaimed and pub-
lished through Nineveh by the decree of the king and his nobles,
saying, Let neither man nor beast, herd nor flock, taste any thing: let
them not feed, nor drink water: But let man and beast be covered
with sackcloth, and cry mightily unto God: yea, let them turn every
one from his evil way, and [rom the violence that is in their hands.
Who can tell if God will turn and repent, and turn away from his
fierce anger, that we perish not? And God saw their works, that they
turned from their evil way; and God repented of the evil, that he had
said that he would do unto them; and he did it not (jonah 3:5-10).

The king of Nineveh understood biblical prophecy better than
most modern Christians do. He recognized that God might be
willing to reverse His judgment and not impose negative sanc-
tions in history, even though He had said that He would. he
king recognized that God’s intent was ethical: to stop the sin-
ning. God could do this either by bringing negative sanctions or
by enabling the recipients of the message to reform their lives.
Nineveh chose the latter approach. The city was spared.

Like pre-tribulational dispensationalists who are ready (if not
willing) to see two-thirds of the Jews of Israel exterminated — see
it safely from heaven, of course, after the Rapture** — and wha
rejoice at front-page headlines filled with bad news, because this
tells us that “Jesus is coming soon,” Jonah was depressed when
the prophesied bad news turned into good news. “But it dis-
pleased Jonah exceedingly, and he was very angry” (Jonah 4:1).
He had expected God to smash His enemies in forty days. But
God had smashed them, in an ethically relevant sense. He had
made them into something bettcr: if not covenant-kcepers, then
at least covenant-observers.**

38. John F. Walvoord, Israel in Prophecy (Grand Rapids, Michigan: Zondervan Acade-
mie, [1962] 1988), p. 108,

39. A covenant-keeper is a regenerate person who obeys God's laws because of the
ethical transformation within him. As a recipient of God's special, soul-saving grace, he is
heir to God’s covenant promises, including promises to his children, The biblical covenant
offers continuity with the future (point five). Since Assyria subsequently became a ruthless
