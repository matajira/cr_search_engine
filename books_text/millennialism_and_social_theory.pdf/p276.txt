260 MILLENNIALISM AND SOCIAL THEORY

Sir Isaac Newton’s Principia? In John Locke's Second Treatise on
Government? In Jean Jacques Rousseau’s Social Contract? In Tho-
mas Jefferson’s Notes on Virginia? In Karl Marx’s Capital? In
John Rawls’ A Theory of Justice?* Or in the Bible? When we get
direct answers to this question from today's Christian intellectu-
al leaders, depending on the content of their answers, we may
begin to move away from the dominant political humanism of
our day. Not until then, however.

Without considerable pressure, they will not provide these
answers. They haven’t in the last two centuries, except when
identifying Aquinas or Locke as the proper source. There is no
independent biblical social theory. Pessimillennialism and the
myth of neutrality have done their work very well. A serics of
major catastrophes perhaps will undo it.

Biblical Blueprints

What is the alternative? Biblical blueprints.’ We must pro-
claim the fact that the Bible does provide biblical blueprints for
the reconstruction of society. If this were not true, then there
could not be an explicitly biblical social theory. There could not
be explicitly biblical social action. This is why the concept of
biblical blueprints is anathema to all sides: fundamentalists, neo-
evangelicals, Lutherans, traditional Calvinists, and of course
secular humanists. The idea of biblical blueprints, when coupled
‘with the idea of God's historical sanctions and postmillennial-
ism, threatens the existing alliance between the humanist power
religion and the pietist escape religion. This is why the idca is
opposed so consistently in recent Christian social commentary.

Those Christians who maintain that there are no biblical
blueprints have a major problem. They live in a society that is
operated in terms of non-biblical blueprints. There is no neu-
trality. There will always be blueprints. Blueprints are an ines-

18, It is easy today to dismiss Hitler's Mein Kampf. It was not so easy in Germany in
1942. The problem was, not enough Christian leaders said so, authoritatively, from 1924
to 1933. After 1933, it was too late.

19. Gary North, (ed.): Biblical Blueprint Series, 10 vols. (¥t. Worth, Texas: Dominion
Press, 1986-87),
