The Society of the Puture 99

brethren, concerning them which are asleep, that ye sorrow not,
even as others which have no hope” (I Thess. 4:13). Paul also
writes: “But this I say, brethren, the time is short: it remaineth,
that both they that have wives be as though they had none; And
they that weep, as though they wept not; and they that rejoice,
as though they rejoiced not; and they that buy, as though they
possessed not; And they that use this world, as not abusing it:
for the fashion of this world passeth away” (I Cor. 7:29-31). As
we move in history toward a tcars-free ctcrnity, our behavior
should reflect both our faith and our historical experience as
victors, There will be a progressive drying up of tears as cove-
nant history unfolds.

Hoekema raises a legitimate question: What about weeping? He
notes its absence (v. 19). But because he is defending a particu-
lar eschatology, he wipes away the tears exegetically by wiping
away history. This is strictly a tactical ploy; his view of a tears-free
future beyond the grave does not come from the text or from
normal principles of Old Testament interpretation. It comes
from his amillennial interpretive scheme. He argues in effect
that history and tears are ontologically inseparable. Any absence
of tears absolutely has to mean the absence of history, with no
exceptions, This is strict amillennial exegesis.

The Prophets’ Use of Language

The question I raise in response is this: Would those who
heard Isaiah’s message have grasped this hypothetical ontologi-
cal relationship: tears and history? Would they have had even
the slightest inkling that Isaiah was talking exclusively about a
death-free eternal state rather than history? Not if they had
heard his previous reference to a world without tears. The
context was historical and earthly:

For thou hast made of a city an heap; of a defenced city a ruin: a
palace of strangers to be no city; it shall never be built, ‘Therefore
shall the strong people glorify thee, the city of the terrible nations shall
fear thee. For thou hast been a strength to the poor, a strength to the
needy in his distress, a refuge from the storm, a shadow from the
heat, when the blast of the terrible ones is as a storm against the wall.
