346 MILLENNIALISM AND SOCIAL THEORY

Smurd: It's too late. You have already signed the trust agree-
ment.

You: When?

Smurd; When you became a Christian.

You: What about my children? They are Christians.

Smurd: Then they will participate.

You: Do you expect their preliminary distributions will be
larger.

Smurd: Certainly not larger.

You: Smaller?

Smurd: That is my expectation.

You: What about their payments into the trust? I suppose
they will be larger than mine.

Smurd: I see that you're getting the picture.

You: I have never heard of anything like this arrangement in
my life. What is the principle underlying it?

Smard: Dutch treat.

Premillennialism: Dispensational

Smurd: Well, sir, I have good news for you. You have named
as a beneficiary of my client’s worldwide trust. Ali taxes have
been paid. This is a pure windfall to you.

You: This is tremendous news. I’ve been having a terrible
time making ends meet. I’m behind on some of my payments.
This money is just what I necd.

Smurd: Ah, yes. The money. Well, there is a lot of it, I can
assure you. But there are certain conditions of this trust.

You: Conditions? What kind of conditions?

Smurd: Well, you must obey the stipulations of the trust.
These are found in this book I am handing you.

You: It’s a Bible.

Smurd: Yes, it’s a Bible. You must obey it.

You: You mean all of it?

Smurd: Hardly! Most of it has been annulled.

You: Then what part?

Smurd: Just the New Testament.

You: All of it?

Smurd: Why no, just the parts that apply to the Church.
