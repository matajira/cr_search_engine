60 MILLENNIALISM AND SOCIAL THEORY

great evil of capitalism is seen in its commitment to material
economic growth. The world is limited, socialists now insist.
What is needed now, they say, is a system of international,
centrally imposed restraints on all polluting production, yet
which also allows freedom to consumers and decentralized
“networking.” They do not show how this fusion of central
planning and local initiative is possible. In short, they have no
economic theory, a point Mises made in 1920."

Some defenders of the free market also shifted their focus:
from material output to information. Previously, they had seen
the economy as materially unlimited (open-ended) in the longer
run, even though constrained by scarcity in the short run. Alter
1980, they began to talk about man’s own mind as the primary
source of wealth. As a progressing society shifts from manufac-
turing to services and information, it progressively escapes the
fetters of material limits. There has been a near apothcosis of
autonomous man, the entrepreneur.* The power of man’s
mind is viewed as bordering on alchemy: from the self-transcen-
dence of man’s mind to a transcended environment.”

The Christian View of Progress: Personal and Social

The biblical view, being covenantal, is radically different
from both the constraints and unconstraints view, for it begins
with a different view of God, man, and history. But because
social theory has been ignored by Christians for centuries, and
because they have tended to absorb the reigning opinions from
the intellectual world around them, Christians have not articu-

15. Ludwig von Miscs, Economic Calculation in the Socialist Commonwealth (Auburn,
Alabama: Mises Institute, [1920] 1990).

16. Warren T. Brookes, The Economy in Mind (New York: Universe Books, 1982).
Universe Books became briefly famous a decade earlier for publishing The Limits ta
Growth, the doomsday book on the rapid depletion of material resources. Brookes is a
Christian Scientist, and his book reflects the view of man as not being under the con-
straints of sin or God’s curse. Julian Simon, the Uitimate Resource (Princeton, New Jersey:
Frinceton University Press, 1981); Herman Kahn, The Coming Boom: Economic, Political,
Social (New York: Simon & Schuster, 1982); George Gilder, The Spirit of Enterprise (New
‘York: Simon & Schuster, 1984).

17. George Gilder, Microcosm: Introduction to the Quantum Era of Economics and Technol-
ogy (New York: Simon & Schuster, 1989).
