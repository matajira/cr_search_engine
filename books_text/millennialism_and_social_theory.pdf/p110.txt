94 MILLENNIALISM AND SOCIAL THEORY

alternative. It is not that they are short on brains, They are,
however, woefully short on biblical presuppositions. They do
not command much respect, even within their own circles,
when they announce: “Thus sayeth The Wall Street Journal,” or
“Thus sayeth The New York Times.”

Conclusion

When Christians do not view their present social involvement
as possessing a fundamental continuity with the emergence (Le.,
development or extension) of the kingdom of God in history,
they have little incentive to develop a specifically Christian
social theory. If they also deny the fundamental continuity of
Old Testament law and God's sanctions throughout history,”
they will be sorely tempted to revert to the supposedly universal
and common logical categories of Stoic and Scholastic natural
law theory, as Geisler has done. Lacking both the temporal
incentive for dominion (God’s positive sanctions in history) and
the judicial tools of dominion (biblical law), they deny the
legitimacy of Christians’ dominion in history by means of the
biblical covenant, through the empowering of the Holy Spirit.

Both premillennialism and amillennialism deny that there
will ever be a Christian civilization prior to Christ’s Second
Coming. In saying this, both viewpoints promote an antinomian
outlook. Their defenders usually deny the continuing validity of
the Old Testament case laws, but even when the case laws are
not denied, these theologians deny the continuing presence of
God's historical sanctions — sanctions that they freely admit were
attached to His law-order in the Old Covenant era, at least in
the case of national Israel. But God’s covenant law without
God’s predictable, historic, corporate sanctions is like a nail
without a hammer. It is useless for constructing anything.

A few premillennialists and amillennialists have offered very
cogent criticisms of modern humanist culture, but these critics
have never offered a uniquely Christian alternative to the hu-
manism they reject. This exclusive negativism has the effect of

66, Pratt, He Geve Us Stories, pp. 848-44,
67, North, Zols of Dominion, ap. cit.
