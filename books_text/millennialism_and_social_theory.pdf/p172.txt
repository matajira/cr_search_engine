156 MILLENNIALISM AND SOCIAL THEORY

Pentecostalism: direct authoritative messages from God to a few
uniquely gifted leaders (spokesmen in history: point two of the
biblical covenant) — messages that replace God’s law, since God's
Jaw is no longer binding. That this (power religion) leads again
and again to ecclesiastical tyranny should surprise no one. In
either case, there is an increase of personal irresponsibility.

To classify as one of “the secret things of God” the idea of
God's predictable sanctions in history requires a leap of faith.
The question is: Is such a leap of faith biblical? Or is the Old
Testament’s message of God’s predictable sanctions in history
itself part of our covenantal legacy from God, meaning “those
things which are revealed belong unto us and to our children
for ever, that we may do all the words of this law”?

In the Old Covenant, the sins of priests and kings could
subject the society to God’s negative sanctions (Lev. 4). Christian
theologians believe that God no longer brings negative sanc-
tions against society because of the unintentional sins of His
priests, although He did do this under the Old Covenant (Lev.
4:1-3). (They might admit that certain kinds of sins by national
political leaders could bring negative sanctions, but probably
not God’s.) But their rejection of God's historical, negative,
corporate sanctions in history is more broadly conceived than
this. The vast majority of Bible-affirming theologians today
assume that there has been a radical New Covenant break from
Old Covenant citizenship.? They assume (though seldom, if
ever, aticmpt to prove exegetically) that the Old Covenant’s
close links between the social rewards of covenant-keeping and
the social cursings of covenant-breaking are no longer operative
in the New Covenant order. More than this: éhere are supposedly
no predictable covenanial sanctions in New Covenant history, meaning
no sanctions applied in terms of biblical law. Meredith G. Kline and
his disciples argue that God does not bring predictable covenan-
tal sanctions against a social order at all, i.e., that the historical
sanctions in the New Covenant era are random from covenant-
keeping man’s point of view. “God’s sanctions are mysterious.”

2 On Old Covenant citizenship, see Gary North, Political Polytheism: The Myth of
Pluralism (Tyler, Texas: Institute for Christian Economics, 1989), ch. 2.
