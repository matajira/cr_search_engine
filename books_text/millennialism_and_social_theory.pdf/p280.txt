264 MILLENNIALISM AND SOCIAL THEORY

medieval period, few people ever journeyed as far as 25 miles
from their village. As history has advanced, this restricted mo-
bility has steadily disappeared. So has people’s psychological
commitment to a home town. The growing mobility of capital
and people within nations has overcome geographical localism.
Localism will presumably not be a major factor at the resurrec-
tion. In a future millennial era characterized by high per capita
income, freedom of movement, freedom of trade, and interna-
tional peace based on one public “lip” — a public confession of
Trinitarian faith — we can expect to see nationalism go the way
of clannism (tribalism) and localism. Localism will not disap-
pear, but its hold on peoplc’s minds will decrease. A vision of
God’s international kingdom in history will replace the compet-
ing regional commitments. It will also replace the humanists’
vision of a one-world kingdom.

In the kingdom-expansion phase, however, this may not be
true. If men become committed to the four-corners strategy of
urban conquest, quadrant by quadrant,” they may develop
local sympathies and commitments that are stronger than those
that exist today, in the Church’s passive, pessimillennial phase.
A three-way commitment may replace today’s unitary national-
ism: local (the church’s parish), national (the mother tongue),
and international (the civilization of God).

If all men have one public “lip,” how will there be any anti-
Christian nations to divide on Judgment Day? Why will there
be goats? First, because there have been evil nations in the past.
Their members will be judged. Second, becausc some members
of the future covenanted communities of Christian nations will
lie about their faith and commitment. There will be a final
falling away at the last day.* The public confessions of some
groups will change. But this does not mean that a long period
in between cannot be confessionally and culturally Christian.

He Shall Overcome

We know there is only one kingdom of God, and it has many

28. See Chapter 12.
24. North, Dominion and Common Grace, Preface, pp. 189-90, 248-50.
