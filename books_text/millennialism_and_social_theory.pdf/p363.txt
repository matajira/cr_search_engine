Appendix: The Lauyer and the Trust 347

You: Which parts are those?

Smurd: Well, there’s considerable debate over that.

You: Who is winning the debate?

Smurd: Theologically or institutionally?

You: Both.

Smurd: Institutionally, those who write off only the gospels.

You: And theologically?

Smurd: Those who write off everything except Paul’s prison
epistles.

You: What do I get if I obcy only the prison epistles?

Smurd: The same that you get if you obey all the epistles.

You: Which is what?

Smurd: Assessments,

You: What assessments?

Smurd: The assessments to cover the operational costs of the
trust fund.

You: You mean the trust fund makes no profits?

Smurd: Not in the last three hundred and seventy years.

You: When did it make any profits?

Smurd: From the year 100 A.D. until about 325, Then again,
very briefly, from 1517 to 1618.

You: After that?

Smurd: After that it has been all downhill.

You: You mean that all the beneficiaries have lost money?

Smurd: No. Only those who obey the New Testament epis-
tles.

You: You mean the Christians.

Smurd: Precisely.

You: And what about the non-Christians?

Smurd: They have done quite well.

You: So, why not get them to pay operational costs?

Smurd: This is not what the Trustor wants.

You: Then it pays to be part of the non-Christian benefi-
ciaries,

Smurd: Not forever.

You: What changes things?

Smurd: The Trustor’s Son will return and collect all the due
assessments from those who haven't previously paid.
