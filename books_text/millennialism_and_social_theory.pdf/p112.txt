5

THE SOCIETY OF THE FUTURE

For, behold, f create new heavens and a new earth: and the former shall
not be remembered, nor come into mind. But be ye glad and rejoice for ever in
that which I create: for, behold, I create Jerusalem a rejoicing, and her peaple
a joy. And I will rejoice in Jerusalem, and joy in my people: and the voice of
weeping shall be no more heard in her, nor the voice of crying. There shall
be no more thence an infant of days, nor an old man that hath not filled
his days: for the child shall die an hundred years old; but the sinner
being an hundred years old shall be accursed. And they shall build houses,
and inhabit them; and they shall plant vineyards, and eat the fruit of them.
They shall not build, and another inhabit; they shall not plant, and another
eat: for as the days of a tree are the days of my people, and mine elect shall
long enjoy the work of their hands. They shall not laboter in vain, nor bring
forth for trouble; for they are the seed of the blessed of the Loxv, and their
offspring with them (Isa. 65:17-23). (emphasis added)

Isaiah introduces this prophecy by saying that it refers to the
New Heaven and the New Earth. The key question is this: Is
this coming era historical or post-resurrection? The language of
Isaiah is straightforward: an era is coming, in history, when the
person who dics at age one hundred will be considered as a
child, an indication of a major extension of men’s lifespans.
Also, sinners will be accounted accursed. This appears to be an
application of Isaiah’s prophecy of the millennial era’s improved
moral self-consciousness, when vile people will not be called
liberal,‘ and churls will not be called bountiful (Isa. 82:5). In

1, Consider the United States’ federally financed “art,” with its religious and moral
perversion. Any attempt to de-fund it by removing tax money until the National Endow-
