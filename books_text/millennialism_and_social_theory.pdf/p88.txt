72 MILLENNIALISM AND SOCIAL THEORY

millennial views are also intensely antinomian in their modern
formulations, and, as I argue in this book, also antinomian in
principle. I mean by “antinomian” that the defenders of each
pessimillennial system deny the continuing authority of the Old
‘Testament case laws in the New Testament era.*

The fundamentalist tradition has been dominant culturally in
American evangelical circles.> The Calvinist-Reformed tradition
has until recently dominated the realm of scholarship in evan-
gelical circles, with Lutheranism contributing some material in
Bible exegesis. Lutheranism has always been amillennialist,
while twentieth-century Calvinism, heavily influenced by the
Dutch, has also been amillennialist. In recent years, neo-evan-
gelicalism has combined Arminianism, mild theological libera!-
ism, mild theological conservatism, pluralism, and a narrowly
ecclesiastical Calvinism into a curious but unstable mixture of
social concern coupled with an explicit denial of the possibility
or desirability of biblical law-based solutions.

Because Christian Reconstructionism is a philosophy of social
activism,’ it has had to confront the fundamentalists, whose
dominance in twentieth-century American evangelicalism has
crippled most attempts at Christian social reform.” The only
major exception to this rule was the post-World War I passage
of the Eighteenth Amendment to the Constitution, which out-
lawed the manufacture or sale of alcoholic beverages, and which

in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990),

4. Gary North, Tools of Dominion: The Case Laws of Exodus (Tylet, Texas: Institute for
Christian Economics, 1990), ch. 2. It is conceivable that a few dispensational theologians
might admit privately that the Old ‘lestament’s case laws will be used by Jesus to judge
the nations during the future millennial age. They do not admit this possibility in public,
however. In any case, they would regard this as judicially ang culturally irrelevant for the
“Church Age.”

5. Symposium on the Failure of the American Baptist Culture, Christianity and
Civilization, 1 (1983).

6. Gary North, {5 the World Running Down? Crisis in the Christian Worldview (Tyler,
‘Texas: Institute for Christian Economics, 1988), Appendix C: “Comprehensive Redemp-
tion: A Theology for Social Action”; reprinted from The foturnal of Christian Reconstruction,
‘VIII (1981); North, Beckward Christian Soldiers? An Action Manual for Christian Reconstruc-
tion (Tyler, Texas: Institute for Christian Economics, 1984); Gentry, Greatness of the Great
Commission.

7. George M. Marsden, Fundamentalism and American Culture: The Shaping of Toentieth-
Century Evangelicalism, 1870-1925 (New York: Oxford University Press, 1980).
