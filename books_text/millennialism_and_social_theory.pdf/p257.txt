Pietistic Postmillenniatism 241

The indivisible covenantal link between biblical law and
postmillennialism, as I have argued, is the presence of God’s
sanctions in history. If there were no guaranteed historic sanc-
tions, then the two positions could be held independently, but
covenant theology does not allow this. Logically, the two may
somehow he separated; theologically, they cannot be. There is
positive corporate feedback in history for covenant-keepers and
negative corporate feedback for covenant-breakers.’°

Revivalism

The kind of Calvinistic postmillennialism preached by Jona-
than Edwards was non-theonomic. It relied entirely on the
movement of the Holy Spirit in men’s hearts. The revivalist
preachers of the Great Awakening did not discuss the possibility
of progressive cultural transformation in response to wide-
spread communal covenantal faithfulness to the stipulations of
biblical law. This was the great defect with the postmillennial
revival inaugurated by Edwards and his followers. They fully
expected to sce the blessings of God come as a result of strictly
individualistic conversions. ‘They had no social theory relating
personal salvation and social transformation. In this sense,
Jonathan Edwards was not the last New England Puritan; he
was a pietist to the core. He and his followers destroyed the
cultural remnants of Puritanism in New England."

Consider Edwards’ Treatise on the Religious Affections. There is
nothing on the specifics of the law of God for culture. Page
after page is filled with the words “sweet” and “sweetness.” A
diabetic reader is almost risking a relapse by. reading this book
in one sitting. The words sometimes appear three or four times
on a page. Consider these phrases: “sweet entertainment,”
“sweet ideas,” “swect and ravishing entertainment,” “sweet and
admirable manifestations,” “glorious doctrines in his eyes, sweet

Puritanism. It is revivalist in orientation. For a recent example, see Errol Hulse, “Recon-
struction, Restprationism or Puritanism,” Reformation Today, No. 116 (July-Aug. 1990),
10. Ray R. Sutton, That You May Prosper: Dominion By Cowenand (Tyler, Texas: Tnstivute
for Christian Economics, 1987), ch. 4.
11. Richard L. Bushman, From Puritan to Yankee: Character and the Social Order in
Connecticut, 1690-1765 (Cambridge, Massachusets: Harvard University Press, 1967).
