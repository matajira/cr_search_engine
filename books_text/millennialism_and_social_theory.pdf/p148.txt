132 MILLENNIALISM AND SOCIAL THEORY

Covenant was always violent.

In the New Testament, God’s establishment of the Church
changed the lives of those who were converted, but it did not
change the social institutions of Israel. This did not happen
until the fall of Jerusalem in A.D. 70, a generation later. The
same can be said for the centuries-long conquest of Rome’s
culture by Christians. This did not take place until after a series
of disasters had fallen on Rome: wars, confiscatory taxes, infla-
tion, and Constantine’s victory, followed by tribal invasions in
the West that continued for centuries.

The New Testament’s emphasis is on personal regeneration,
not institutional, The emphasis is on progressive sanctification over
time, not revolutionary displacement. The progressive sanctifica-
tion of individuals is to produce the progressive sanctification of
institutions. Christians are to be salt to the world. Still, this does
not deny the life-and-death nature of the struggle. Jesus warned:
“Ye are the salt of the earth: but if the salt have lost his savour,
wherewith shall it be salted? it is thenceforth good for nothing,
but to be cast out, and to be trodden under foot of men” (Matt.
5:13), It is either “them or us.” Salt is used on God’s fiery altar
as a permanent sign of destruction: “For every one shall be
salted with fire, and every sacrifice shall be salted with salt”
(Mark 9:49). Salt was used in ancient world to pollute a newly
defeated city’s land, so that it would no longer grow crops. “And
Abimelech fought against the city all that day; and he took the
city, and slew the people that was therein, and beat down the
city, and sowed it with salt” (Jud. 9:45). Salt is more than savor;
salt is a means of destruction.

Christians are to destroy the enemy's cily (civilization), though
normally through voluntary conversions and progressive, long-
term cultural displacement. Continuity, not discontinuity, is the
institutional task of New Covenant social reform, On the one
hand, it is not the task of the reformers to impose discontinuous
corporate negative sanctions, except in the case of war or legiti-
mate resistance by lower magistrates to domestic national tyran-
ny: the Calvinist doctrine of interposition.* On the other hand,

4, John Calvin, Instituies of the Christian Religion, IV:20. See also “Junius Brutus,”
