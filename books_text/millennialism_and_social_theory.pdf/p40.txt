24 MILLENNIALISM AND SOCIAL THEORY

and institutional ghettos.¥

As residents of psychological and institutional ghettos, few
American evangelical Christians have any self-conscious interest
in social theory, although they almost intuitively adopt certain
traditional views about society. They have adopted what is
sometimes called the American civil religion.”® It is based on
concepts of natural law and political pluralism. This civil reli-
gion is self-consciously neutral with respect to specific religious
confessions. No one is asked to believe in God in order to par-
ticipate in politics, or even to swear in a court of law.

This traditional social outlook is strongly reinforced by the
prevailing views of the millennium. American Christians’ very
lack of interest in social theory, like the views of society which
they almost intuitively hold, is a direct result of the exegetically
opposed yet socially similar eschatologies that they hold dear:
premillennialism and amillennialism. Both views lead to a deni-
al of the possibility, or at least the relevance, of social theory.

The Millennium: Continuity

The acceptance of the general Christian eschatological view
regarding death and resurrection has consequences for one’s
view of time: linear rather than cyclical. This has been evident
throughout Western history. But eschatology is more than
personal death and resurrection. It also raises the question of
progress. This is especially true of the doctrine of the earthly
millennium. This has become a major dividing issue among

25. Christian Reformed Church theologian and Westminster Theological Scminary
president R. B. Kuiper warned his fellow Dutch-Americans: “By this time it has become
trite to say that we must come out of our isolation, . . . Far too often, let it be said again,
we hide our light under a bushel instead of placing it high om a candlestick, We seem not
to realive fully that as the salt of the earth we can perform our functions of seasoning and
preserving only through contact.” R. B. Kuiper, 7 Be or Not to Be Reformed: Whither the
Christian Reformed Church? (Grand Rapids, Michigan, Zondervan, 1959), p. 186.

26. Russell E. Richey and Donald G, Jones (eds.), American Civil Religion (New York:
Harper & Row, 1974). See also Sidney E. Mead, The Lively Experiment: The Shaping of
Christianity in America (New York: Harper & Row, 1963); Robert N. Bellah, The Broken
Covenant: American Civil Religion in Time of Trial (New York: Seabury Crossroad, 1975).
For a warning, sce Herbert Schlossberg, Idols for Destruction: Christian Faith and Its Confron-
tation with American Society (Washington, D.C.: Regnery Gateway, [1983] 1990), pp. 250-
59.

 
