200 MILLENNIALISM AND SOCIAL THEORY

make society Christianized are futile because the Bible doesn’t teach
it. On the other hand, the Bible certainly doesn’t teach that we
should be indifferent to injustice and famine and to all sorts of
things that are wrong in our current civilization. Even though we
know our efforts aren't going to bring a utopia, we should do what
we can to have honest government and moral laws. It’s very difficult
from Scripture to advocate massive social improvement efforts,
because certainty Paul didn’t start any, and neither did Peter. They
assumed that civilization as a whole is hopeless and subject to God's
judgment.

He then went on to observe that premillennialists run most
of the rescue missions. “Premillennialists have a pretty good re-
cord in mecting the physical needs of people.” This is quite
true, but there is no doubt from his words that he does not
believe it is possible for Christians to influence the creation of
a world in which there will be freedom, righteousness, and
productivity — a world in which fewer rescue missions will be
necessary. His vision of social action is to get people out of the
gutter. This is because his view of the gospel is to take people
out of this world — first mentally and then physically, at the
Rapture. For dispensationalism, this world is one gigantic gutter, It
cannot be cleaned up during the “Church Age.” The best a Christian
can hope for is to sit peacefully on a short, clean stretch of
curbing on the sidelines of life.

In response, Professor John J. Davis of Gordon-Conwell
Theological Seminary, a postmillennialist, replied: “But gencral-
ly speaking, the premillennialist is more oriented toward help-
ing those who have been hurt by the system than by addressing
the systematic evil, while the postmillennialist believes the sys-
tem can be sanctified. That's the basic difference with regard to
our relationship to society.”** This is exactly right.

Walvoord, a consistent representative of traditional dispensa-
tionalism, assures us: “We know that our efforts to make society
Christianized are futile because the Bible doesn’t teach it.” He
deliberately ignores the Old Testament prophets. He does not

 

25. Christianity Taday (Feb. 6, 1987), pp. 5+I, 6-1.
26. Bbid., pp. 6-1, 7-1
