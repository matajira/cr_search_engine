306 MILLENNIALISM AND SOCIAL THEORY

of the political pluralists is rapidly becoming “no time for Jesus”
in every public institution. Meanwhile, every institution is stead-
ily being redefined by the messianic State either as inherently
public or else under no other jurisdiction than the State.

The Prophesied Revival

For weil over a decade, I have heard major Church and para-
church leaders predicting that there will soon be a great world-
wide revival. So, where are their recommended plans to accom-
modate this revival? Nobody has one. Leaders make these glow-
ing prophecies with all the confidence that they predict the
imminent Rapture. Yet they do not restructure their lives, debts,
and retirement investment portfolios in terms of an imminent
Rapture. Why not? Because they do not really believe in an imminent
Rapture.

If you believe that something is going to take place, you plan
for it. You take steps to finance your plan. If you do not plan
for it and begin to execute the plan, you simply do not believe
it. It is just one option among many, and not one very high on
your list of probabilities.”

Here is my premise: if God-honoring social change comes to
this nation and then to the world (or vice versa), it must come
through the institutional Church, with its sacraments and disci-
pline. What is this discipline? The threat of excommunication:
keeping people away from the communion table. To enforce its
discipline, it must close communion to all non-Christians and
Christians under Church sanctions.

The Church is primary. The Church, alone among human insti-
tutions, survives the final judgment intact. God-honoring social
change will not come primarily through Christian education,
Christian publishing, or Christian television networks. The
Church will use all of these tools; they will not be allowed by

7. The reason why J devote as much time and money as I do to writing and publish-
ing Christian Reconstruction books instead of spending time managing my investment
portfolio is that I am persuaded that the need for such materials will take an historically
discontinuous leap before I die. If Lam wrong, the books will still produce some fruits, but
it is my belief in, and hope for, a discontinuous move by the Ioly Spirit that keeps me at
my word processor eight hours a day, six days a week (then I go home and read).
