310 MILLENNIALISM AND SOCIAL THEORY

Even now this restoration is underway, or so they believe, After
nineteen hundred years of marking time or edging forward at a
snail's pace, the kingdom of God is now back on the march, heading
toward a season of spectacular evangelism and social dominion.®

Notice the tone of this remark. Anyone who believes in a
discontinuous breakthrough of the Holy Spirit in history is
supposedly naive in the ways of God. Masters points to the slow
movement of the gospel in history, but he also assumes that this
is normative throughout history. There is no question that for
the billions of people now alive ever to be saved, it will take a
monumental, historically unprecedented move of the Holy Spirit.
But so adamant are the critics of postmillennialism that nothing
like this can ever take place that they heap ridicule on those
who sincerely believe that God may not necessarily have as His
eternal decree the destruction of today’s billions of lost souls.
The more Calvinistic these critics, the more contemptuous their
dismissal of these billions. If these lost souls were converted, this
would verify postmillennialism. The critics would rather see
them perish. There is no eschatological neutrality.

Let us recognize the unstated mental assumption that must be
in the mind of anyone who calls himself a Calvinist, yet who
ridicules the idea of a discontinuous break into history by the
Holy Spirit: “These five billion people are lost, they're going to
stay lost, they therefore deserve ta stay lost, and anyone who says
anything different is a postmillennial utopian.” I am, indeed.

My concern is with evangelism. I am not willing to write off
automatically (prophetically) the souls of five-plus billion people.
God has this prerogative; I do not. Again, let me say it as plainly
as I can: my hostility to amillennialism and premillennialism is
not based on my disagreements with their interpretations of this
or that verse in Scripture. Good men have disagreed for a long
time over the proper interpretation of Bible verses. My hostility
is to the mindset that has to underlie any Calvinist who says that
God will not move large numbers of souls into His kingdom at

9. Peter Masters, “World Dominion: The High Ambition of Reconstructionism,” Sword
& Trowel (May 24, 1990), p. 13,
