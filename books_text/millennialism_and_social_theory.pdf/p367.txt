Appendix: The Lawyer and the Trust 351

You: Why not?

Smurd: It tends to lead to reduced productivity.

You: Is this bad?

Smurd: Oh, yes. It reduces the amount of wealth remaining
for the non-assessed to extract from the victims.

You: At Armageddon?

Smurd: And before.

You: You mean now. Today. Tomorrow.

Smurd: Correct.

You: Then what's the advantage of being named a benefi-
ciary?

Smurd: You get to participate in the final distribution of the
trust's assets,

You: Yes, 1 know that. I mean before then.

Smurd: You get to go to heaven.

You: All Christians go to heaven.

Smurd: Yes, but I mean you get to go to heaven right after
Armageddon. And you don’t have to die first, either! I mean, as-
suming that you don’t get killed during Armageddon,

You: How will that work?

Smurd: The ‘Trustor’s Son will return to take you there.

You: And then what happens to us?

Smurd: The trust document does not say exactly, But you
will be taken carc of, I assure you.

You: And what about those left on earth?

Smurd: They inherit the earth.

You: You mean they get all of the trust’s preliminary distri-
butions?

Smwurd: That is correct.

You: What about assessments?

Smurd: Only the evil-doers pay.

You: But that’s the way it ought to be now!

Smurd: You aren’t the first person to say that.

You: You mean others have objected?

Smaurd: They used to.

You: Why not any longer?

Smurd: They all died off during World War 1.

You: Who were they?
