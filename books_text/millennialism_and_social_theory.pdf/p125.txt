The Society of the Future 109

that the time will come when our position will be entirely lost,
but Christ shall nevertheless rule the world.”"* The process is
irreversible, he insists: “The history of Western culture is in the
main a history of a Christian culture followed by a seculariza-
tion, increasing in extent and intensity. It is moving to a final
catastrophe.”

Prosperity is said to be a spiritual oasis. It surely can be. This
is the paradox of Deuteronomy 8; wealth is both a positive and
a negative sanction in history. But for van Riesscn, wealth is
exclusively @ negative corporate sanction. It is a spiritual desert. The
Christian, in whose hands wealth can be a positive sanction,
must regard the wealth around him as a sign of God’s cultural
disinheritance of the Church’s healing work in history.

Then what of this Proverb? “The wealth of the sinner is laid
up for the just” (Prov. 13:22b). “It applies only to individuals,
not to societies,” says the amillennialist. But God delivered the
land of Canaan into the hands of the Israelites, disinheriting the
Canaanites, but providing an inheritance to His people. “Old
Testament, Old Testament!” shouts the amillennialist. This is
supposed to end the argument. It is the major offense of Chris-
tian Reconstructionism that its view of Old Testament law and
God’s historical sanctions keeps the argument alive. This offers
hope to socicty: the hope of God's inheritance to His people in
history. The amillennialist deeply resents this offer of hope.

A Society Without Legitimate Hope

Van Riessen offered no alternative, no plan of action, and no
hope. He ended with this call to... to... a stiff upper lip:
“Defeatism or passive resignation to our situation with all the
risks attached to the latter only mean the neglect of our voca-
tion. On the other hand superficial optimism based on some
favorable phenomena or on a distorted global picture of our
situation would be dangerous. In this difficult time it is essential
for us to have a correct insight into our condition, an ardent
faith in our calling irrespective of the results of our work, quiet

22. Ibid., p. 285.
28. Idem
