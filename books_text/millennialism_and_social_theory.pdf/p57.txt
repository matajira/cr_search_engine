What Is Social Theory? 4l

Communist producers of goods and services, held legally unac-
countable to enslaved consumers for over seventy years, finally
failed to deliver the goods. Communist economies all went
bankrupt economically, but far more significantly, went theoreti-
cally bankrupt. They lost their legitimacy because it had been
based on a promise of a better economic world to come. So
utterly hopeless is Communism as an economic system, The
Economist commented with characteristic English wit, that not
even Germans could make it work. While the implements of
Russian nuclear sanctions arc still growing in number, those
sanctions, if ever applied, will not be Marxist, merely Russian.
Marxism is dead. Its few remaining defenders are Western
college professors: tenured upholders of lost causes. Any social
philosophy that is dependent on college professors to keep it
alive is already suffering from rigor mortis.

This is not to say that Leninism is dead. Leninism is a theory
of power, not a theory of economics.” Its strategy, in Lenin’s
own words, is two steps forward and one step back.”* Mikhail
Gorbachev is a Leninist, and a declared one at that. What he is
throwing out is Marxian socialism, just as Lenin did with his
New Economic Policy in 1921. For the sake of the Leninist
causc, Gorbachev is abandoning Marxism. But he is not aban-
doning Leninism’s power religion. What he now appears to be
doing is substituting the strategy of the founder of the Italian
Communist Party, Antonio Gramsci, who was Lenin’s contempo-
rary. Gramsci rejected the Marxist-Leninist ideal of a workers"
revolution as a strategy for conquering the West. He designed a
propaganda campaign promoting atheism and relativism as a
means of subverting the then-marginally Christian West.”

The question is: Can Leninism as an ideology exist apart from
socialism? Can it exist apart from Marx’s goal of destroying the

24, “D-mark day dawns,” The Economist (June 30, 1990), p. 3.

25. John P Roche, The Hislory and Impact of Marxist-Leninist Organizational Theory
(Cambridge, Massachusetts: Institute for Foreign Policy Analysis, 1984).

26. VI. Lenin, One Siep Forward, To Steps Back (The Crisis of Our Party) (Moscow:
Progress Publishers, [1904] 1978).

7. Malachi Martin, The Keys of This Blood (New York: Simon & Schuster, 1990), ch,
13,
