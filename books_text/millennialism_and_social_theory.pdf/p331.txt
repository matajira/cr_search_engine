What Is to Be Done? 315

then to humanists. The First Great Awakening broke the civil
authority of the older Calvinistic holy commonwealths of New
England. Then the Civil War broke the cultural authority of
Arminian Christianity. The parallel rise of the social gospel
movement and dispensational pietism delivered the nation into
the hands of the humanists.*

‘Why did this occur? Because the revivals promoted the lowest
common denominators: theologically, ecclesiastically, judicially, and
emotionally. There was no vision of a holy commonwealth in
the preaching of the revivalists. Everything was focused on
gaining from individuals a one-time profession of faith by what-
ever means. The revival meetings were like medieval fairs:
everyone came. The unconverted masses came mostly for excite-
ment, secondarily for entertainment, and only belatedly for a
religious conversion. The revivalists gave them what they want-
ed. The pastors surely couldn't and still remain faithful to God.

There was an ecclesiastical transfer of authority after 1800.
The waves of revival spread westward, just as the population
had. The mainline denominations did not move fast enough,
except for the Cumberland Presbyterians, who were not very
Calvinistic and did not require advanced academic degrees for
their pastors. The old saw goes like this: “The Baptist evange-
lists walked into the West, the Methodists rode on horseback,
the Presbyterians went by covered wagon, and the Episcopalians
waited for regularly scheduled train service. The Congregation-
alists stayed home.” The more rigorous the academic require-
ments to serve as a pastor, the slower the comparative growth of
the denomination during the revival. Presbyterians, New Eng-
land Congregationalists, and Episcopalians, who had been the
dominant influences as late as 1790, were dwarfed by the Bap-
tists and Methodists during the next half century.

The pietism of these new churches was uniquely suited to the
humanists’ demand that Christians, as Christians, withdraw from
public life over the next century and a half. Except for one
doomed crusade — Prohibition — nothing of a social or political

13. C, Gregg Singer, A Theological Interpretation of American History (Nutley, New
Jersey: Craig Press, 1964), ch. 5.
