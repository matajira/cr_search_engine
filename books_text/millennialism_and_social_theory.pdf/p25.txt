Introduction 9

was analogous to the separation of heaven from hell or the
post-judgment perfection of the New Heaven and New Earth
from the perfection of the lake of fire (Rev. 20:14-15). This is
the judicial difference between “saved” and “lost.” Compared to
this, the physical circumstances of Christ's bodily return to earth
are minimal. Therefore, the magnitude of the judicial transition
from wrath to grace in history far overshadows the physical
transition from this world to the next.

Adam's physical death was a covenantal result (sanctions) of
his transgression in history. Jesus’ physical resurrection was a
covenantal result of His perfect atonement in history of God's
wrath. It is not physical death that stands as life's greatest dis-
continuity. The greatest discontinuity in life is the judicial transition
from wrath to grace. Jesus made this plain when He told men
what to fear most:

And fear not them which kill the body, but are not able to kill
the soul: but rather fear him which is able to destroy both soul and
body in hell (Matt. 10:28).

Each person’s discontinuity from grace to wrath is judicial
and automatic. Everyone is born under the judicial curse
against Adam. ‘Lhis is the doctrine of original sin. Thus, there
is no major transition from covenant-breaking in history to hell.
There is no judicial transition. Eternity in the lake of fire is
merely an extension of life lived apart from God’s redeeming
grace in history. In short, deliverance in eternity begins in history.

The Church has always said that it believes this. Neverthe-
jess, the Church has only rarely applied this most fundamental
of all biblical themes to its overall theological system. Specifically,
churches have refused to apply this principle to eschatology. One thing
that churches agree on today is that man’s covenantal-judicial
deliverance in history must be understood as strictly spiritual-
personal and in no sense social. The judicial transition from
wrath to grace supposedly applies only to the individual soul,
not to the physical body or the body politic.

What, then, of the Old Testament? Is it simply “God's Word,
emeritus”? For the major theme of the Old Testament is God’s

 
