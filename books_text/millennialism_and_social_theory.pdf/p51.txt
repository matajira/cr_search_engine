What Is Social Theory? 35

organism, just as the cosmos is: a growing thing that has the
characteristic features of life. The model institution of the or-
ganic society is the family, which is closely associated with physi-
cal birth, cultural and physical nurturing, and death. This or-
ganic view of society is often associated with the concept of a
hierarchical chain of being that links God, man, and the cos-
mos.* It is also associated with magic and with magic’s funda-
mental principle: “As above, so below.” Man supposedly can
manipulate any aspect of the cosmos (macrocosm) by manipulat-
ing representative features (microcosm). The crudest manifesta-
tion of this philosophy is the voodoo doll. Philosophically, this
view of socicty is associated with realism: an underlying meta-
physical unity transcendent to mere individuals. Organicism is
divided into two major historical streams: familism (medieval)
and statism (Greco-Roman).

Contractualism. This is the dominant view of the modern
world, although its philosophical roots go back to the Middle
Ages (e.g., William of Occam). Society is based either on a hypo-
thetical original contract among men in pre-historic times or on
a constitution of some kind. The primary model is the State, not
the family, although in some modern social philosophies, the
free market is the model. The familiar phrase associated with
this outlook is “the social contract.” Men in the distant past
voluntarily transferred their individually held political sover-
cignty to the State, which now maintains social order. Each
social institution is governed by the terms of an original con-
tract, whether mythical or historical. The social bond is based
exclusively on voluntary legal contracts, hypothetical or histori-
cal, among individuals. Philosophically, this view of society is
associated with nominalism: the denial of any underlying mcta-
physical reality or transcendent social unity apart from the
thoughts and decisions of individual men. Contractualism is

9. Arthur O. Lovejoy, The Great Chain of Being: A Study of the History of an Idea (Cam-
bridge, Massachusetts: Harvard University Press, |1936]}.

10. In English political thought, the archetype organic treatise is Robert Filmer's
defense of patriarchal-monarchical absolutism, Pairiarcha (1680). It was important primari-
ly because it called forth John Locke's response, First Treatise of Civil Government (1690):
contractualism.

 
