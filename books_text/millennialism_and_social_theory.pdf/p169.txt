Time Enough 153

The humanists and the pessimillennialists agree on another
point: God’s kingdom is today exclusively internal, that is to say,
culturally irrelevant. They all agree that time is not on the side of
the Church, meaning there is not “time enough” for Christians
to build a bottom-up, decentralized, biblical law-based, creedal,
international, God-blessed social order.“ This means that they
agree on this fundamental point: continuity within history favors
covenant-breakers and the kingdom of autonomous man,

Conclusion

Any millennial eschatology that proclaims the near-term re-
turn of Jesus Christ becomes a major ecclesiastical barrier to the
devclopment of Christian social theory. The supposed immi-
nence of Jesus’ physical return removes from God’s people the
crucial resource that they need to think about the future and
plan for it: time. According to premillennial theologians, the
looming eschatological discontinuity of Jesus’ Second Coming
works against the Church long-term and in favor of God's ene-
mies, near-term. Therefore, for the Church to accomplish anything of
Significance in history, it must drastically limit its vision of what it can
accomplish. It must plow shallow because there is not enough
time to plow deep. Even so, they expect the Church to fail.
Dispensationalists deeply (shallowly?) resent anyone who calls
them to plow both deeper and longer. Common grace amillen-
nialists do not resent being called to plow deeper, since they
rejoice in deeply lost causes; they scoff, however, when they are
told that Christians will finish plowing the field in history.

The biblical view of history is that God, who providentially
controls all events in terms of His decree, brings discontinuous,
negative, historical sanctions against covenant-breaking societies.
These discontinuities are the social foundation of the long-term.
victory of His kingdom in history. While these discontinuities
can and do bring great pain and consternation to covenant-
keepers, they serve as the fire that burns off the dross of sin (Isa.
1:25-28). Jeremiah, Ezekicl, and Daniel all suffered during the

41. Gary North, Healer of the Nations: Biblical Blueprints for International Relations (Ft.
Worth, Texas: Dominion Press, 1987).
