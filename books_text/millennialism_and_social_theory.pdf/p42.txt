26 MILLENNIALISM AND SOCIAL THEORY

Jesus Christ gave His Church a corporate assignment in history:
the Great Commission.

And Jesus came and spake unto them, saying, All power is given
unto me in heaven and in earth. Go ye therefore, and teach afl
nations, baptizing them in the name of the Father, and of the Son,
and of the Holy Ghost: Teaching them to observe al} things whatso-
ever 1 have commanded you: and, lo, | am with you alway, even
unto the end of the world. Amen (Matt. 28:18-20).

If this international discipling of the nations must be com-
pleted prior to the Second Coming of Christ — a denial of the
“any moment” Second Coming — then this raises an inevitable
question: “What is the specific nature of the work that has been as-
signed to the saints by God?” Second, is this task exclusively one of
soul-saving or is it also to become culture-transforming? If the
answer to the second question is “the latter,” as this book argues
that it is,” this raises a number of specific problems for philos-
ophers, strategists, and leaders in the Church, such as: “What
kind of social order is explicitly Christian?” “What personal and
institutional efforts are legitimate in achieving these ends?”
“How comprehensive is the lawful authority of the institutional
Church in pursuing its goals?”

Theologians have seldom devotcd much time or energy to
answering these questions. From time to time, however, West-
ern society has been swepi by great waves of new eschatological
speculation about the necessary earthly preparations for the
coming millennium, and these periods have been noted by the
uprooting of existing conventions and institutions.

The Idea of Progress

The question of continuity between the present and a future
earthly millennium inevitably raises the question of historical
progress. Is there meaningful progress in history? No one living
in the modern world denies some kind of progress, unless he

29. See also Kenneth L. Gentry, Jr., The Greatness of the Great Commission: The Christian.
Enterprise in a Fallen World (Tyley, Texas: Institute for Christian Economics, 1990).
