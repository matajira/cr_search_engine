Covenantal Progress 53

history, there will be both “positive feedback” and “negative
feedback.” Any attempt to renounce this passage as no longer judicially
binding in the New Covenant era is inescapably a denial of any biblical
basis for God-honoring cultural progress in history.

The passage begins with an imperative: “Therefore thou
shalt kcep the commandments of the Lorp thy God, to walk in
his ways, and to fear him.” It immediately offers a reason: “For
the Lorp thy God bringeth thee into a good land, a land of
brooks of water, of fountains and depths that spring out of
valleys and hills.” God was about to give to this people an un-
merited gift in the midst of history: control over the Promised
Land. Here is a fundamental principle of both theology and
history: God's grace precedes man’s response. The proper response
is obedicnce to God’s revealed law.

The Paradox of Deuteronomy 8

It is God who “giveth thee power to get wealth, that he may
establish his covenant which he sware unto thy fathers, as it is
this day.” It could not be any clearer: the economic success that
God was promising to His covenant people in the future was
based on the original promise given to Abraham, Isaac, and
Jacob. There is no autonomous power in man’s possession that
enables him to become productive. As James put it, “Every good
gift and cvery perfect gift is from above, and cometh down from
the Father of lights, with whom is no variableness, neither
shadow of turning” (James 1:17). The power to get wealth was
God's re-confirmation of the Sinai covenant with Israel, which was
in turn a renewal of His original covenant with the Patriarchs.

Another sign of the covenantal nature of these promises was
God's promise of future negative sanctions. “And it shall be, if
thou do at all forget the Lorn thy God, and walk after other
gods, and serve them, and worship them, I testify against you
this day that ye shall surely perish.” This is the sanction of
covenanial death and corporate disinheritance.

The biblical covenant establishes the possibility of long-term
economic growth, a promise that was unique in the ancient
world. It established the possibility of compounding. If His cove-
nant people remain faithful, He promised them, they would
