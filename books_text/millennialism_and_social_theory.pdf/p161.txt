Time Enough 145

Christianity is false, and therefore it should have no influence in
politics or culture generally. But they are all agreed: God will
never bring the world to the foot of the cross on this side of the Second
Coming of Christ. Reconstructionists say that He will, that His
kingdom’s earthly triumph in history is foreordained. This is
why Christian Reconstruction is a stumbling stone to everyone.
Here is what the debate is really all about: the God-predestined
victory of Christ in history, as manifested by a wide acceptance of
His covenant law, but not achieved through His bodily presence
on a physical throne in Jerusalem. Reconstructionists argue that
because God will accomplish this through His irresistible grace,
politics is not very important. Our critics deny that God has
foreordained His visible kingdom’s victory in history, and so
they imagine that only politics can serve as a tool sufficient to
achieve a theonomic millennium. Denying the sovereignty of
God in bringing His kingdom on earth to visible victory, they
focus on politics, which they all agree is the ultimate power in
history, given the assumption (which they all make) of the cul-
tural impotence of the kingdom of God in history. It makes
them ready to surrender to humanism in advance of defeat.

Pre-Emptive Surrender

What frightens some of the dispensational critics is their fear
of persecution. David Allen Lewis warns in his book, Prophecy
2000, that “as the secular, humanistic, demonically-dominated
world system becomes more and more aware that the Domin-
ionists and Reconstructionists are a real political threat, they will
sponsor more and more concerted efforts to destroy the Evan-
gelical church. Unnecessary persecution could be stirred up.””
In short, because politics is humanistic by nature, any attempt
by Christians to speak to political issues as people — or worse, as
@ people — who possess an explicitly biblical agenda will invite
“annecessary persecution.”

We see once again dispensational fundamentalism’s concept of
evangelism as tract-passing, a narrowly defined kingdom program

23, Lewis, Prophecy 2000, p. 277.
