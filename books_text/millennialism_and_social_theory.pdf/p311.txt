Our Blessed Earthly Hope in History 295

are the children of the desolate than the children of the married
wife, saith the Lorp. Enlarge the place of thy tent, and let them
stretch forth the curtains of thine habitations: spare not, lengthen
thy cords, and strengthen thy stakes; For thou shalt break forth on
the right hand and on the left; and thy seed shall inherit the Gen-
tiles, and make the desolate cities to be inhabited (Isa. 54:1-3).

O thou afflicted, tossed with tempest, and not comforted, behold,
1 will lay thy stones with fair colours, and lay thy foundations with
sapphires. And I will make thy windows of agates, and thy gates of
carbuncles [crystal], and all thy borders of pleasant stones. And all
thy children shall be taught of the Loxp; and great shall be the
peace of thy children. In righteousness shalt thou be established:
thou shalt be far from oppression; for thou shalt not fear: and from
terror; for it shall not come near thee. Behold, they shall surely
gather together, but not by me: whosoever shall gather together
against thee shall fall for thy sake. Behold, | have created the smith
that bloweth the coals in the fire, and that bringeth forth an instru-
ment for his work; and I have created the waster to destroy. No
weapon that is formed against thee shall prosper; and every tongue
that shall rise against thee in judgment thou shalt condemn. This is
the heritage of the servants of the Lorp, and their righteousness is
of me, saith the Lorn (Isa. 54:11-17).

Continuity and Discontinuity

If the amillennialist is correct, these passages refer to the
world beyond the final judgment. ‘This inheritance would surcly
be a peculiar form of continuity: the post-resurrection wealth
left behind by the “gentiles” will be insignificant compared to
the deliverance from sin and sin's cosmic curse. But if these
passages refer to the realm of history, as the context indicates,
then there is no escape from either premillennialism or postmil-
lennialism. Covenant-keepers will inherit the wealth and authority of
covenant-breakers in history. The law of the Proverbs will be
fulfilled corporately and individually in history: “A good man
leaveth an inheritance to his children’s children: and the wealth
of the sinner is laid up for the just” (Prov. 13:22). The debate
now shifts to the question of the continuity between today’s
Church and the coming millennial era of blessings: premillen-
nialism (discontinuity) vs. postmillcnnialism (continuity).
