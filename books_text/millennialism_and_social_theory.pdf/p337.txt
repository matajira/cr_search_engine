What Is to Be Done? 321

its quadrant. From the day any church is begun, its goal must be
to launch a minimum of three other churches in its quadrant.
Then each of these churches targets its newly designated quad-
rant, There has to be a plan.

Once the first church becomes legally independent of the
mother church’s umbrella (if any), the missionary has become a
pastor. He sits down with the heads of households in his congre-
gation and tells them that it is his goal to equip at least three
other men to become pastors of their own local congregations.
They must meet the criteria of I Timothy 3: be married men (or
widowers), be hospitable, etc. As he recruits and trains elders, he
identifies those who might be capable of becoming pastors. He
selects them and trains these few for the pastorate, but with this
proviso: each-of them must begin his own Bible study, either in
the local quadrant or across the city in a yet-unevangelized
quadrant.

This is why pastoral training must be decentralized, comput-
erized, and personalized. We must return to the original ideal of
apprenticeship. A pastor is best trained by a person in his own
city who has a vision for that city. The training must be geared
to the specific needs of that city. We need specialization.

A quadrant is a local church’s parish. Each new pastor's goal
is to repeat this proccss internally in his quadrant. This must be
agreed to well in advance. The long-term goal is to have every
person in the city worshipping weekly in a local congregation of
some Trinitarian denomination. Nothing less than this meets the
minimum requirements of the Great Commission. Any program
of church-planting that settles for less than this is a victim of
pessimillennialism. Today, they all are.

No More Megachurches

This means that no local congregation should be larger than
about 400 members. A local church that is larger than this is not
dividing in order to evangelize its quadrant. As soon as a church
hits 200 members, it should begin planning a congregational
division. Members must be approached and asked to move to
the new congregation when it is launched. Each member must
regard himself from the beginning as part of God’s spiritual
