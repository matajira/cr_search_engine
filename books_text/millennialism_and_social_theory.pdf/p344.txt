CONCLUSION

Thou sawest till that a stone was cut oul without hands, which smote the
image on his feet that were of iron and clay, and brake them to pieces. Then
was the iron, the clay, the brass [bronze], the silver, and the gold, broken to
pieces together, and became like the chaff of the summer threshing floors;
and the wind carried them away, that no place was found for them, And the
stone that smote the image became a great mowntain, and filled the whole
earth (Dan, 2:34-35).

We conclude where we began, with the fundamental theme
of the Bible: the transition from wrath to grace. This takes place
in history: definitively and progressively, It is not limited to
personal transformation. It involves every area of life in which
sin presently reigns.

The definitive transition took place at the death, resurrection
and ascension of Jesus Christ, The sending of the Holy Spirit
and the destruction of the Old Covenant’s World Order at the
fall of Jerusalem in A.D, 70 completed the definitive foundation
of Jesus Christ's New Covenant World Order.’ This New World
Order is still dominant in history. It will remain dominant. It
will smash every earthly imitation New World Order, just as it
smashed the Roman Empire.

Ask of me, and I shall give thee the heathen for thine inheri-
tance, and the uttermost parts of the earth for thy possession. Thou
shalt break them with a rod of iron; thou shalt dash them in pieces
like a potter's vessel (Psa. 9:8-9).

1. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987).
