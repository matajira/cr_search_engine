38 MILLENNIALISM AND SOCIAL THEORY

They have no strong opinions regarding the proper legal (i.c.,
covenantal) foundation of society. They do have very strong
opinions on God's historical sanctions and the millennium, and
this has deeply affected their intuitively held views of society.
Before exploring the question of sanctions and eschatology,
however, we must first consider very briefly the question of law.

A Question of Law

Prior to the Newtonian intellectual revolution, all but a hand-
ful of Christians had adopted some version of medieval natural
law theory, what we can call organic natural law theory (real-
ism). This version of natural law was based on the medieval
Roman Catholic concept of a chain of being linking God and
man, This medieval thcory’s roots were in Stoic natural law
theory, in turn derived from Greek speculative thought.* The
mind of man was seen as autonomous to some degree from
God, the Bible, and the Church. Baptism, the celebration of the
mass, and Church law were needed to make fallen man whole
again, but man’s intellectual autonomy in rational affairs (e.g.,
geomctry) was asserted. The laws of nature were seen as autono-
mous and open to accurate investigation by all mankind. Natu-
ral Jaw in society was seen as essentially metaphysical, i.e., un-
derlying all human relationships. The key philosophical assump-
tion was the idea of shared being, meaning a real, metaphysical
link in a living, organic cosmos.

The mathematical-mechanical rationality of Cartesianism and
Newtonianism destroyed this organic worldview in the Protes-
tant West. After Newton, Christians and non-Christians alike
adopted a new version of natural law theory, what we can call
mechanical natural law theory (nominalism). It rested on the hy-
pothesis of a distant creator God so far removed from the cre-
ation that He intervenes only occasionally, in order solely to

1952). On the history of Roman Catholic casuistry, see Albert J. Jonsen and Stephen
Toulmin, The Abuse of Casuistry: A History of Moral Reasoning (Berkeley: University of
California Press, 1988).
15, ‘The exceptions historically were nominalists. The Franciscans, following Francis-
can William of Occam, linked the Spiritualist tradition with philosophical nominalism.
16. On this point, sce the writings of Herman Dooyeweerd and Cornelius Van Til.

 
