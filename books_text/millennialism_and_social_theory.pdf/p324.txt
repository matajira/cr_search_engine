308 MILLENNIALISM AND SOCIAL THEORY

men merely by standing pat. People are automatically born into
his covenantal kingdom. ‘hey are lost by default because of
Adam’s rebellion. God therefore has to act positively in order to win
the souls of men. Satan doesn’t. He can win by remaining passive,
just so long as God refuses to send the Holy Spirit to bring His
irresistible grace in history. Satan wins by God’s default.

So, here is the choice of agendas: a God who wins in history
by sending grace by His Spirit, or a God who loses in history by
standing pat. If nothing changes, the mere birth rate differen-
tials between the saved and the lost will guarantee the triumph
of Satan’s kingdom in history. Add up the populations of China,
India, and the Islamic world. Toss in most of Latin America.
Toss in Europe. Don’t forget New York City, Los Angeles
County, and San Francisco. What do the numbers tell us? Chris-
tianity is losing. Continuity means historical defeat for God’s kingdom
in history.

Amillennialism teaches that this is all we can legitimately
expect. Premillennialism teaches that Christ’s coming earthly
kingdom will be marked only by the outward obedience of men.
Premillennialism does not teach that most people will be con-
verted to saving faith in Christ. In fact, given the Arminian
views of most premillennialists, they cannot possibly assert that
the coming of the earthly kingdom will automatically lead to
mass revival. Some, like Dave Hunt, say specifically that the
hearts of most men will not be changed, and that in this sense,
the millennial earthly reign of Christ should not be equated
with the kingdom of God.*

Then where are we? More to the point, where are they? Five
billion souls are here. We cannot send them back. The question
is: Are they going to perish eternally by the billions? Are we
living in the most horrible period in man’s history, when hell
starts filling up in earnest? World population keeps growing.
Will we live to see 10 billion people ready for eternal fire? Will
we not see a great harvest?

What is to be done?

8 Dave Hunt and T. A. McMahon, The Seduction of Christianity: Spiritual Discernment in
the Last Days (Eugene, Oregon: Harvest House, 1985), p. 250.
