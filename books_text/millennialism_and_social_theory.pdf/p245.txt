The Sociology of Suffering 229

your rhetorical question, changing only one word, making you
the target. He may ask: “Is it really overreacting to say that
such masochism is repugnant to biblical sensibilities?” Some read-
ers may prefer triumphalism to masochism. Not Gaffiin:

Suffering is a function of the futility/decay principle pervasively at
work in the creation since the fall; suffering is everything that
pertains to creaturely experience of this death-principle. . . . Until
then, at Christ's return, the suffering/futility/decay principle in
creation remains in force, undiminished (but sure to be overcome),
it is an enervating factor that cuts across the church's existence,
including its mission, in its entirety. The notion that this frustration
factor will be demonstrably reduced, and the church's suffering
service noticeably alleviated and even compensated, in a future era
before Christ's return is not merely foreign to this passage; it trivial-
izes as well as blurs both the present suffering and the future hope/
glory. Until his return, the church remains one step behind its
exalted Lord; his exaltation means its (privileged) humiliation, his
return (and not before), its exaltation,”

 

Christ is now resurrected; the Church will continue to be
humiliated. Christ has ascended; the Church will continue to be
crucified. Was Christ’s resurrection and ascension historical?
Yes, says orthodox Christianity. Will the Church experience a
progressive taste of either resurrection or ascension in its effect
on culture in history? No, says the amillennialist. The Great
Commission is a commission to a millennium of defeat.

Understand what this means. Gaffin says it well: the Church
of Jesus Christ in history remains one step behind the Lord. But
the Church’s experience is humiliation throughout history. So,
what does this tell us of Jesus Christ’s influence in history, just
one step ahead of the Church? Except for saving individual
souls, this influence is nil. Zip. Nada. “Satan 1,000, Christ 0.”
This is the essence of the amillennial view of history. It reduces
covenant theology to pietistic Anabaptism: save souls, not culture.
It is premillennialism without earthly hope.

20. Ibid., pp. 214-15.
