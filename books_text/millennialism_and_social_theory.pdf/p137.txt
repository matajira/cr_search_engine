The Society of the Future 121

tive sanctions in history. The presence of ethical conditionality
removes from such prophecies the category of inevitability. The
threatened sanctions are inevitable éf the target of the threat
persists in sin, but the target may repent. This is what God in
principle always prefers. “The Lord is not slack concerning his
promise, as some men count slackness; but is longsuffering to
us-ward, not willing that any should perish, but that all should
come to repentance” (II Pet. 3:9). In short, biblical prophecy does
not assume an inevitable continuation of existing ethical trends. It only
assumes a certain outcome if these existing trends continue.

Trend-Tending: The Illusion of Inevitability

Van Riessen’s vision of the future is a grim one. It partakes of
the same gloom as do modern humanism’s pessimistic utopian
novels, which he identifies as signs of the end of civilization.”
He looks around him, and he does not like what he sees. He
then extrapolates from 1957 into the future — a vision of the
future governed by his unstated amillennial presuppositions.
The question is: Are these trends inevitable (i.e., predestined)?

Robert Nisbet is a conservative sociologist and historian --
more historian, by gift and choice, than sociologist. (How else
could I have survived his graduate seminars?)* He has seen
what has happened to many prophecies in history. He has also
seen the character of modern social science prophecies today.
There is not much difference, he concludes. They seldom come
true. When they do, it is because the prophet has had a kind of
brilliant insight into the present, not the future. The successes
are based on imagination, not computer print-outs.

Commenting on a slew of “Year 2000” books published in the
mid-1960’s — a tradition going back to a communist pornogra-

42. Van Riessen, Society, ch. 2.

43, In case anyone is interested, I wrote two papers for him in 1967 and 1968, both
of which are in print: “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Cage,” in Gary North (ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspec-
tive (Vallecito, California: Ross House Books, 1976); and “Lhe Cosmology of Chaos,”
Chapter 2 of Mar’s Religion of Revolution: Regeneration Through Chaos (Tyler, ‘Texas:
Institute for Christian Economics, [1968] 1989). Never throw away an old term paper,
always say.
