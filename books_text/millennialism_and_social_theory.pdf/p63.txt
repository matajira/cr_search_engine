What Is Social Theory? 47

able implications for the devclopment of social theory. This fact
was ignored for centuries by Christian theologians generally and
by the tiny handful of Christians who over the centuries have
dealt at least peripherally with the question of social theory.**
The relationship between millennialism and social theory was
not even discussed, let alone studied in depth. The topic in
recent years has been considered sporadically and peripherally
by a few Christian theologians only because of the appearance of
the Christian Reconstruction movement, which is self-consciously
postmillennial and which has specialized in the study of social
theory.*

Ironically, secular historians have in recent decades begun to
understand that such relationships have existed in Western
history.“ Why have Christian scholars paid little or no atten-
tion to this growing body of scholarly literature? First, they
seldom keep up with academic literature outside their own
narrow theological specialtics. Second, the existence of such a
relationship between eschatology and social theory raises many
difficult personal questions regarding church membership, con-
tinued employment by Christian organizations, and relationships
within Christian organizations. If a particular eschatology is
true, and if it seems to lead to a particular theory about how
society is required by God to be governcd, then what should the
local church do when its members or officers affirm the particu-

the millennial kingdom on earth, in the absence of a millennial kingdom on earth, or
afier a millennial kingdom on earth.

44. This concern was usually a subordinate part of the discipline of casuistry: the
application of (hopefully) Christian ethical principles to historical circumstances.

45. ‘The critics of Christian Reconstruction have mislabeled the Reconstructionists’
intellectual specialization with a supposed downplaying of traditional theology. First, it is
the repeated and almost universal error of the critics to imagine that Christian Recon-
structionists betieve that society is transformed first and foremost through politics. This
view of social change is the myth of humanism, not Christian Reconstruction. Second,
society is far wider than politics, so the Christian Reconstructionists are concerned with far
more than mere political transformation. ‘Third, the Reconstructionists are convinced that
the regeneration of a majority of people by the irresistible grace of the Holy Spirit is the
sole basis of long-term social reconstruction using a biblical model. See Gary North and
Gary DeMar, Christian Reconstruction: What Jt Js, What It Isn't (tyler, Texas: Insticute for
Christian Economics, 1991). See also Gary North, Dominion and Common Grace: The Biblical
Basis of Progress (Tyler, Texas: Institute for Christian Economics, 1987), ch. 6.

46. Sce Chapter 1, footnote #11.

 
