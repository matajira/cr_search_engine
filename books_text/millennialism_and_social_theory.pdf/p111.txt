Pessimillennialism 95

discouraging their followers. This lack of a legitimate cultural
alternative has persuaded most Christians to shorten their time
horizons. They lose hope in the future: present-orieniation.

If there is no cultural alternative to humanism available in
history, then the only reasonable Christian response is to pray
for either the Rapture (dispensationalism) or the end of history
(amillcnnialism). (Historic premillennialisis and_post-tribula-
tional dispensationalists believe that the millennium will come
only after Christians have gone through Armageddon and the
Great Tribulation. I have no idea what they pray for.)

Premillennialists and amillennialists share a commitment to
a coming cosmic discontinuity as the Church’s great hope in
history: deliverance from on high (and in the case of premillen-
nial dispensationalism, deliverance to on high). Again, citing
Norman Geisler: “Hence they do not view their present social
involvement as directly related to the emergence of the future
kingdom of God. In this respect amillenarians are more like
premillenarians and have thereby often escaped some of the
extremes of postmillennialism.” This affirmation of a coming
cosmic discontinuity cuts the ground from under the Christian
who would seck to discover a uniquely biblical social theory. It
also undercuts the incentive for social action. Social action be-
comes a holding action at best and a kamikaze action at worst.

The Church is believed to be incapable of changing history's
downward move into cultural evil. Social action is therefore
adopted on an ad hoe basis: solving this or that immediate local
problem. Effective Christian social action supposedly can accom-
plish little; therefore, it requires neither a long-term strategy
nor a systematic concept of ethical cause and effect. Political
power, not ethics, is viewed as historically determinative. Power
is seen as a necessary evil today.. Christians are supposedly
never to exercise political power in the “Church Age.” Either
they cannot or should not exercise it (possibly both).

The result is predictable: the absence of Christian social
theory.
