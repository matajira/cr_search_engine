What Is Social Theory? 37

autonomous power of man to name any aspect of the cosmos
authoritatively (nominalism). Covenantalism is a separate philo-
sophical system.

Historical Sanctions and Millennial Eschatology

As I hope to show in this book, there is more to social theory
than eschatology. There are all five aspects of the social bond. A
comprehensive study of social theory would have to include all
five. I have decided, however, to limit my discussion primarily
to millennial eschatology and sanctions, meaning the idea of
sanctions in history — sanctions imposed by God and governed by
specific moral and legal standards. I could also have discussed
sovercignty, hicrarchy (authority), and law, but the great divid-
ing lines within Christianity today are associated with sanctions
and the historical result of these sanctions: millennialism. Here
is where the great debate occurs. Christians generally agree with
each other on the idea of the sovereignty of God, although the
Calvinists are the “hard-liners” on this subject: the absolute
sovereignty of God and the non-autonomy of man. Christians
also agree that there must be hierarchies in life, although they
tend to adopt the family as. their model rather than the Church,
since there is more agreement today regarding the proper struc-
ture of the family. (This structure of the Christian family has
changed many times in the past, but each era seems to think
that the present structure is the biblical model.) The churches do
verbally agree that Christians must obey God’s moral law, a
concept left judiciously and judicially undefined.

But why single out (“double out”?) historical sanctions and
millennialism? Because the debate over law would be too com-
plex. Which laws? Natural laws? Old Testament laws? Which
natural or Old Testament laws? How interpreted? How applied
today? Few Christians have seriously thought about law and
socicty in our era, and the seventeenth-century debates over law
(casuistry) have faded from most Christians’ consciousness.

14, On the history of casuistry in general, see Kenneth E. Kirk, Conscience and Its
Problems: An Introduction to Casuistry (rev. ed.; London: Longmans, Green, 1948), See also
Thomas Wood, English Casuistical Divinity in the Seventeenth Century (London: S.PCK.,
