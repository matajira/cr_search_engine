infinite, 69
irresistible, 66-67
Judge, 36
kingdom, 149, 153
laws, xiv
mercy of, 7, 39, 121, 139-40
mystery of, x, 155-56
no respecter of persons, 174-
75, 176-78
promises, 8, 121, 127
sanctions, 118, 134, 156
scales of justice, 174
secret things of, 155-56
stories, 78
trap, 55
word of (emeritus), 9
“you're history,” x
Golitsyn, A., 338
Gorbachey, M., 41-42
gospel
adult, 31
covenant lawsuit, 214
crisis and, 299
empire vs., 333
failure, 150
failure?, 144-45
Great Tribulation, 193
narrow view, 239n
offer of, 120
outcome, 177
pace of, 310
shadow, 134
world-changing, 208-4
see also Great Commission
Goudzewaard, Bob, 82
grace
common, 135, 137, 183-84
discontinuity, 135
irresistible, 60-67, 257, 292
law and, 207
obedience &, 53
precedes man’s response, 53
social reform, 68

381

see also wrath to grace
Grace Seminary, 194-95
Graham, Billy, 194-95
grammar, 77-79
Gramsci, A., 41-42
Great Awakening, 20, 242, 313-
15
Great Commission
comprehensive, 259
corporate 25-26
deflection strategy, 250
denial of, 146, 278
rival, 233-34
Great Escape, 257
Great Tribulation
dispensationalism’s focus, 18
end of persecution by Isracl,
216, 218
escaping, 257
Gaffin on, 228, 230
Greek Orthodoxy, 222, 232
Greeks, xiv, 14
“greens,” 56
growth
compound, 53-54, 284, 288
ethical, 284
evil, 284
exponential, 55-56
knowledge, 58
limits of, 55-56
guerillas, 290-91
guilt, $38
Gurnall, William, 240
gutter, 200

Halsell, Grace, 271n
Hardin, Garrett, 57

hare, 149

Hart, Hendrik, 81n
harvest, 296-97

Hayek, F. A., 174-76, 188n
heaven, 84-86

hell, ix, 9
