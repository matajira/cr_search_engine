Denying God's Predictable Sanctions in History 175

Wherefore now let the fear of the Lorp be upon you; take heed and
do it: for there is no iniquity with the Lorp our God, nor respect of
persons, nor taking of gifts (II Chron. 19:7).

These things also belong to the wise, It is not good to have respect
of persons in judgment (Prov, 24:23).

To have respect of persons is not good: for a piece of bread that
man will transgress (Prov. 28:21).

For there is no respect of persons with God (Rom. 2:11).

And, ye masters, do the same things unto them, forbearing threat-
ening: knowing that your Master also is in heaven; neither is there
respect of persons with him (Eph. 6:9).

But he that doeth wrong shall receive for the wrong which he hath
donc: and there is no respect of persons (Col. 3:25).

But if ye have respect to persons, ye commit sin, and are convinced
of the law as transgressors (James 2:9).

Then Peter opened his mouth, and said, Of a truth | perceive that
God is no respecter of persons (Acts 10:34).

In short, God applies His standards of justice impartially, and so
should the civil magistrate. Keeping this permanent, New Testa-
ment, judicial principle in mind, consider Hayek's warning:

From the fact that people are very different it follows that, if we
treat them equally, the result must be inequality in their actual
position, and that the only way to place them on an equal position
would be to treat them differently. Equality before the law and
material equality are therefore not only different but are in conflict
with each other; and we can achieve either the one or the other,
but not both at the same time. The equality before the law which
freedom requires leads to material inequality

Hayek is saying that if the legal basis of the inequality of
economic results is formal judicial equality before the law, then

35. FA. Hayek, The Constitution of Liberty (University of Chicago Press, 1960), p. 87.
