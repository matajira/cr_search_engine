PROLOGUE XV

When that day comes, Christians had better be
ready with the biblical answer: voluntary charity,
the tithe to finance the church, and all levels of civil
government combined limited by Constitutional law
to under ten percent of the people’s income. The
state is not God, and is therefore not entitled to a
tithe. Christians will pay the pipers voluntarily, and
pipers will play our tunes. Humanists can only
cough up enough money to pay pipers when they
have stolen the money with the ballot box, by means
of the politics of guilt and pity, and the politics of
envy. The gospel of Christ, when accompanied by
faith in biblical law, destroys the psychological foun-
dations of political guilt, pity, and envy. The human-
ists’ political end is in sight, and they are outraged.
Psalm 2 tells us what God thinks of their outrage,
and how much good it does them.

Conclusion

I will put it bluntly: no theologian of repute (or
even disrepute) has successfully challenged Dr.
Bahnsen’s defense of biblical law during the last
eight years. I will go farther: no theologian or Chris-
tian social thinker in our generation is capable of
successfully challenging Dr. Bahnsen’s general
thesis, because it is correct. I will take it one step far-
ther: we will not see any prominent Christian philos-
opher even attempt it, because enough of them know
what happened to Meredith Kline: he was cut off at
the knees in full view of anyone who bothered to
read Dr. Bahnsen’s response. Nobody is excited
about the prospects of going up against Dr. Bahnsen
