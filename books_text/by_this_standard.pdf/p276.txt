246 BY THIS STANDARD

the Old Testament era. God's law touched on all as-
pects of life, including criminal justice, and that law
was not presented by the Lawgiver as a racist or
tribal standard of right and wrong. It was God’s uni-
versal and eterna! standard of righteousness for
human affairs.

In a sense, we have already offered an implicit
answer to our question about the standard for a dis-
tinctive Christian outlook on political ethics. God
has spoken to issues of social justice and public pol-
icy toward crime in His law. There is a divine point
of view on politics, and it has been expressed in the
law of the Old Testament. Two things are to be said
about that law. First, it continues to be the general
standard of ethical conduct today according to the
Scripture ~as we have seen many times over in pre-
vious chapters. Second, Old Testament law did not
have a moral validity restricted to the Jewish race; it
has intended to be the standard of conduct outside
the redeemed community as well as within it. Conse-
quently, if the Old Testament law of God expresses
(among other things) God’s view on political morality,
and if that law has universal and abiding validity, we
should expect that the New Testament perspective
on law and politics would likewise affirm the stand-
ard of God's law for public policy. Differences in
time and locality, differences in dispensation and
race, differences in culture and redemptive status do
not demand or imply differences in moral standards.

We would thus expect that the distinctive Chris-
tian approach to political ethics would be defined by
the entire word of God, inclusive of the law of God re-
