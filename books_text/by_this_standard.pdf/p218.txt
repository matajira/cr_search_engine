188 BY THIS STANDARD.

grace”) can be interpreted as implying that the per-
son under grace has been released from moral obli-
gation to the law of God. Paul affirms his submission
to the law of Christ and thereby to every detail of the
Old Testament law as well (Matt. 5:17-19), Indeed,
he was not at all without the law of God (cf. Rom.
3:31; 7:22; 8:4). What then does he mean when he
says in f Corinthians 9:20 that he is not “under the
law”?

It would appear that this expression (“under
law’) is not being used in the same manner in both
Romans 6:14 and 1 Corinthians 9:20. In the former
passage it implies bondage to the power of sin, and this is
far from what Paul is saying about himself in the lat-
ter passage! Those enslaved to sin are /awiess, but
Paul unmistakeably asserts that he is not without
God’s Jaw in Christ. The phrase “under law” in
Romans 6:14 applies indiscriminately to all unbe-
lievers, but in 1 Corinthians 9:20-21 it applies to only
one category of unbelievers—while “without law’
describes the remaining category of unbelievers.

What then does Paul mean in 1 Corinthians 9:20
by asserting that he himself is not “under the law”?
Paul is showing how he became all things to all men
for the sake of the gospel (vv. 22-23). “To the Jews I
became as a Jew, that I might gain Jews” (v. 20).
When with them he acted “as though under the law,”
even though with others he acted “as though without
the law.”

Does Scripture help us understand how Paul was
not thereby acting inconsistently, immorally, and
with duplicity? Yes, it does. The unbelieving Jews
