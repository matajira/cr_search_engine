200 BY THIS STANDARD

this, that the law was not enacted for a righteous
man, but for the lawless and unruly, for the ungodly
and sinners, for the unholy and profane, for
murderers of fathers and mothers, for manslayers,
for fornicators, for homosexuals, or menstealers, for
liars, for false swearers .. .” (1 Tim. 1:9-10). This
may not be a sanctifying effect in the unbeliever’s
life, but it is nevertheless a preservative function
within society which is honored by God. It was in-
tended as one of the proper functions of the law
when God revealed it—both through the created
realm and through the medium of written legisla-
tion.
