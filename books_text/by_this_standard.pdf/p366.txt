396 GY THIS STANDARD

5. The Argument from Silence

Three last arguments may be quickly men-
tioned, all of which are guilty of notorious fallacies in
reasoning. First, there is the “argument from silence”
that the New Testament does not call for us to cam-
paign for the penal sanctions of the law, as in the
case of the incestuous fornicator (1 Cor. 5:1-5). Well,
there may not be a specific illustration available
(given the character of the society and magistrate in
those days), but the principles are indeed taught —as we
have discussed in previous studies. Paul need not say
anything further about the magistrate’s duty regard-
ing incest, for instance, since the Old Testament and
natural revelation were already adequate. What he
needed to reveal was the disciplining procedures re-
quired of the church—to whom, after all, the Cor-
inthian epistle was written (not the civil magistrate).
Given the Biblical doctrine of the law’s continuing
validity (Deut. 4:2; Matt. 5:17-19), we need more
than silence to nullify God’s commands.

6. The Argument from Abuse

Second, there is the argument from abuse — the
argument that unsaved magistrates have abused
God's law by trying to enforce it in the past, leading
to such horrors as the Inquisition. But of course God
never commanded these abuses in his law (for exam-
ple, He did not grant the magistrate the right to
judge heretics in the first place), and so this argu-
ment is actually an argument in favor of our thesis.
Since these abuses violate God’s law, God’s law
ought to be endorsed as valid in order authorita-
