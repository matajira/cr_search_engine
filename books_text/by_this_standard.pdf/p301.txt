CRIME AND PUNISHMENT 271

civil magistrate today ought to apply the penal sanc-
tions of the Old Testament law to criminals in our
society, once they have been duly tried and convicted
by adequate evidence. Thieves should be made to
offer restitution, rapists should be executed, per-
jurers should suffer the penalty they would have in-
flicted on the accused, etc.

Quite simply, ctvdl magistrates ought to mete out the
punishment which God has prescribed in His word. When
one stops to reflect on this proposition, it has an all-
too-obvious truthfulness and justice about it. “Shall
not the Judge of all the earth do right?” (Genesis
18:25). If civil magistrates are indeed “ministers of
God” who avenge His wrath against evildoers, who
better would know what kind and degree of punish-
ment is appropriate for every crime than the Lord?
And where would He make this standard of justice
known but in His word? The penal sanctions for
crime should be those revealed in the law of the
Lord. That makes perfectly good sense.

The Necessity, Equity, and Agency of Punishment

God has not only laid down certain stipulations
for how people should live in society together (for ex-
ample, forbidding stealing), He has also backed up
those stipulations—rendering them more serious
than divine recommendations — with penal sanctions
to be imposed on those who disobey His dictates (for
example, offering restitution}. A law without such
supporting penalties would not be a law at all. Now,
in the case of certain Old Testament command-
ments, there was laid down a dual sanction against the
