38 BY THIS STANDARD

change from age to age: “All His precepts are trust-
worthy. They are established forever and ever, to be
performed with faithfulness and uprightness” (Ps.
111:7-8).

Accordingly Jesus spoke with unmistakable clarity
when He said, “It is easier for heaven and earth to
pass away than for one stroke of the law to fail”
(Luke 16:17). The coming of God’s righteous Son
surely could do nothing to change the righteous
character of God’s laws, even the least of them, for
then they would be exposed as unjust and less than
eternal in their uprightness. So Christ issues this se-
vere warning: “Whoever annuls one of the least of
these commandments and so teaches others shall be
called least in the kingdom of heaven” (Matt. 5:19).
The advent of the Savior and the inauguration of the
New Age do not have the effect of abrogating the
slightest detail of God’s righteous commandments.
God has not changed His mind about good and evil
or what constitutes them.

We can be very glad that God sticks by His word
in this way. The authority of His word for human
life is as permanent as that word by which He cre-
ated and governs the world (cf. Ps. 19:1-14; 33:4-11).
Tf God’s word to us were not as stable as this, if He
were subject to moods and changed His mind from
time to time, then we could not rely on anything He
told us. If God’s law has a fluctuating validity, then
so might His promises! If we say that a command-
ment given by God in the Old Testament is no
longer a standard of righteousness and justice for to-
day, then we can equally anticipate that a promise of
