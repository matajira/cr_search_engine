236 GY THIS STANDARD

Well, no, it must be admitted, Is such a delineation of
universal and localized Jaws made in Paul’s teaching
about the general or universal revelation of God’s
moral standards? Well no, it must again be admit-
ted. In fact, the Roman epistle states quite clearly
that those who commit abominable misdeeds such as
homosexuality know from “the ordinances of God
that those who practice such things are worthy of
death” (Rom, 1:31),

There does not seem to be any obvious Biblical
support for the opinion that political laws in the Old
Testament were intended only for Israel to obey. Just
about every line of theological consideration would
incline us in the direction of the opposite conclusion:
the Creator of all men, who has an unchanging
moral character, has revealed the standards of his
law to every nation of men and will hold men ac-
countable for their behavior in all areas of life, in-
cluding politics. If His standards have been given
clear, written expression to a special group of men—
the Jews —then it would seem reasonable for all men
to pay attention to those written laws and strive to
conform to them.

When we turn from theological themes to a
specific reading of the Scripture, this is the viewpoint
which we find definitely decreed. In special blessing
God gave the Jews a written expression of His law
(for all areas of life), and that written law was in-
tended as a model for all nations—not simply
Israel—to follow. In giving Israel God’s law to be
kept in the “theocratic” land, Moses was inspired to
say: “Behold, I have taught you statutes and or-
