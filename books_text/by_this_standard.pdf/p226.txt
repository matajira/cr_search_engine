196 BY THIS STANDARD

The statement of Galatians 3:10 is blunt and ter-
rifying: “. . . cursed is every one who does not con-
tinue in all things that are written in the book of the
law to do them” (cf. Deut. 27:26). James intensifies
the threat, saying “Whosoever shall keep the whole
law and yet stumble in one point has become guilty
of all” (2:10). Every infraction of the law brings
wrath upon the sinner. All men will be judged for
their ungodliness (Jude 6), judged according to their
deeds whether good or evil (2 Cor. 5:10), and if
found guilty cast into the eternal perdition of second
death (Rev. 20:12-15). The wages of sin will be death
(Rom. 6:23). Therefore, the law works wrath (Rom.
4:15) upon those who are, by their sinful natures,
children of wrath (Eph. 2:3).

(8) The law drives us to Christ for salvation.

Thus far we have noted the unmitigated, abso-
lute, unchanging demand of the law which reflects
the holiness of God and thus sets out the evil of man
by glaring contrast. Those who would have hoped in
their own righteousness for acceptance before God
are shown the futility of this hope by looking at the
high standard of the law. The law speaks, and this
shuts every mouth by bringing all the world under
God’s judgment (Rom. 3:19). Sinners apart from
Christ have no hope in this world (Eph. 2:12). The
sinner’s only recourse must be to the free mercy of
God’s promise. Enlightened as to his guilt, he cries
out with Paul, “wretched man that I am! who shall
deliver me from the body of this death?” (Rom.
7:24). God’s gracious answer is Jesus Christ (3:25),
