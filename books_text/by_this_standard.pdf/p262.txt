232 BY THIS STANDARD

rebuke unjust judgments passed there. When those
who rule for God depart from His laws, then they
must be judged by God. The very foundation of the
civil order was undermined when judges did not dis-
cern between good and evil (cf. 1 Kings 3:9).

The Old Testament abounds with illustrations of
God’s judgment upon kings, rulers, and judges in
Israel who departed from the just standards of His
law in their governing of society. Note especially
King Ahab, who for his own selfish ends engaged in
false witness, theft, and even murder (1 Kings
21:1-22). These matters were recorded by the histor-
ian for posterity and as an example, instead of Ahab's
feats in battle which are known from secular ac-
counts of the period! It was of crucial importance in
Israel that rulers abide by the law of the Lord. Those
who, like Jeroboam and Jehu, departed from God’s
commandments and made the people sin, had evil
brought against their own houses by God, and were
swept away (1 Kings 14:8-10; 16:2-3). When princes
became unrighteous and rebellious, the whole city
was characterized as unrighteous (Isa. 1:21-28), and
God always eventually judged the injustice. When
the Jews returned from years of exile and captivity,
they confessed that their kings had not kept the law
of God (Neh. 9:34-37), and in restored Jerusalem
the magistrates determined to execute true and
peaceful judgments in the law courts (Zech. 8:16),

Law and politics in Old Testament Israel revolved
around God's law for the civil magistrate. But what
about the Gentiles? Did their governments have
different moral standards from Israel’s? To this ques-
tion we must now give attention.
