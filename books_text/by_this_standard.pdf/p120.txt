90 THE NEW TESTAMENT EXPLICITLY SUPPORTS THE LAW

ministry on earth Christ often appealed to the law of
God to bolster his teaching (John 8:17), vindicate his
behavior (Matt. 12:5), answer his questioners (Luke
10:26), indict his opponents (John 7:19), and give
concrete identity to the will of God for men (Matt.
19:17). He taught his disciples to pray that God’s will
would be done on earth (Matt 6:0), and after his
resurrection He directed them to teach all nations to
observe whatsoever He had commanded (Matt.
28:18-20). In all of these ways — without elaborate in-
troductions or explanations for departing from a
general principle or perspective—the New ‘Testa-
ment simply assumes the standing authority of every
command of the Lord found in the Old Testament.
If the Old Testament law were invalidated by the ad-
vent or work of Christ, the preceding examples
would be incredibly out of character and call for
some convincing explanation. Yet none was needed.

Jesus affirmed with solemn authority that not
even the least commandment of the entire Old Tes-
tament was to be taught as without binding validity
today (Matt. 5:19), for according to his perspective
“Scripture cannot be broken” (John 10:35). Accord-
ingly Christ reaffirmed elements of the decalogue,
for example “Thou shalt not kill” (Matt. 19:18). He
also cited as morally obligatory, aspects of the Old
Testament case law: for instance, “Do not defraud”
(Mark 10:19), and “Thou shalt not test the Lord thy
God” (Matt. 4:7). He even cited with approval the
penal code of the Old Testament with respect to in-
corrigible delinquents (Matt. 15:4).

Jesus expected the weightier matters of the law to
