332 BY THIS STANDARD

ment of the civil aspects of the law of God. Such a
“redemptive state” viewpoint is obviously so mis-
taken with respect to other interpretations (for exam-
ple, that the civil laws had a redemptive effect, or
that the state authorities were the cultic or religious
heads as well)—that it cannot be of any service as an
argument. Similarly, claims to the effect that the Old
Testament state punished “religious” crimes (for ex-
ample, blasphemy) overtook the “religious” character
of other crimes as well (for example, murder,
adultery). Such arguments are based on a false no-
tion of the secular/sacred dichotomy which is promoted
by modern humanism, and they are therefore
unhelpful in theological argumentation.

What the opponents of Biblical law need to dem-
onstrate —but do not—is that “religious” crimes like
blasphemy are of no continuing relevance or impor-
tance for social justice in the modern state. Is it con-
trary to the church’s evangelistic mission for Chris-
tians to promote the political use of God’s law, if this
means the state will punish blasphemers and open
idolaters? Such a conflict would be possible only if
we first assumed that God’s word could contradict it-
self (teaching one thing regarding civil ethics and a
contradictory thing about evangelism). Promoting
the punishment of blasphemers is no more contrary
to evangelistic concern than is the promoting of
punishment of murderers.

Arguments Relevant to the Penal Sanctions
1, Only for Israel
Against the political use of God's law today some
