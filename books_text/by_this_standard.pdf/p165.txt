‘THE CATEGORIES OF Gov’s Law 135

place to become judges of the law, for our calling is
to be doers of the law (Jas. 4:11).

Nevertheless, there do seem to be Old Testament
requirements which are not kept by New Testament
Christians, and there are some legal provisions
which seem culturally outdated or at least inapplica-
ble to our modern world. How are we to accomodate
that fact— without becoming judges of the law and
without disregarding Christ’s declaration that every
minor detail of the law has enduring validity? The
answer lies in recognizing the nature of the various
Old Testament laws, seeing the kind of categories
into which they fall. That is, it is necessary to under-
stand the laws of God according to their own charac-
ter, purpose, and function. Only in that way will the
law be “lawfully used” (cf. 1 Tim. 1:8),

Moral and Ceremonial Laws

The most fundamental distinction to be drawn
between Old Testament laws is between moral laws
and ceremonial laws. (Two subdivisions within each
category will be mentioned subsequently.) This is
not an arbitrary or ad hoc division, for it manifests
an underlying rationale or principle. Moral laws re-
flect the absolute righteousness and judgment of
God, guiding man’s life into the paths of righteous-
ness; such laws define holiness and sin, restrain evil
through punishment of infractions, and drive the
sinner to Christ for salvation. On the other hand,
ceremonial laws—or redemptive provisions — reflect
the mercy of God in saving those who have violated
His moral standards; such laws define the way of
