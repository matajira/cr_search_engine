60 BY THIS STANDARD

and deposited in the ark of the covenant which typi-
fied the throne and presence of God in the Holy of
Holies (Deut. 10:5), Moreover, this law must be ac-
knowledged to have a very special place or status be-
cause it has the exclusive qualities of God himself at-
tributed to it. According to Scripture, God alone is
holy (Rev. 15:4) and good (Mark 10:18). Yet God’s
law is likewise designated holy and good (Rom, 7:12,
16; 1 Tim. 1:8), and obedience to it is the standard of
human good (Deut. 12:28; Ps. 119:68; Micah 6:8).
God is perfect (Deut. 32:4; Ps. 18:30; Matt. 5:48),
and the law which He has laid down for us is accord-
ingly perfect (Ps. 19:7; James 1:25). Every statute re-
vealed by God authoritatively defines the holiness,
goodness, and perfections which God’s people are to
emulate in every age.

The Puritan Heritage

The Puritans were zealous to live in the moral
purity which reflects God’s own. Consequently they
upheld the honor and binding quality of every com-
mand from God. The feeling of Thomas Taylor was
typical of them: “A man may breake the Princes
Law, and not violate his Person; but not Gods: for
God and his image in the Law, are so straitly united,
as one cannot wrong the one, and not the other”
(Regula Vitae, The Rule of the Law under the Gospel,
1631). If God turned back His law, said Anthony
Burgess, He would “deny his own justice and good-
nesse” (Vindiciae Legis, 1646). Thus the Puritans did
not, like many modern believers, tamper with or an-
nul any part of God’s law. “To find fault with the
