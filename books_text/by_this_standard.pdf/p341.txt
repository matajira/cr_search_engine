ARGUMENTS AGAINST THE LAW'S GENERAL VALIDITY 311

11-12), which obviously points to the fact that the
priest spoken of in Hebrews need not come from the
particular tribe of Levi, chosen in the Mosaic law to
serve the altar (vv. 13-14). Instead the great High
Priest, Jesus Christ, came in the likeness of Meichi-
zedek —“not according to the law of a fleshly require-
ment {namely, Levitical family origin]”—so that
there has been “a setting aside of a foregoing com-
mandment,” in order that the better hope promised
in Psalm 110:4 might be realized (vv. 15-21). This
singular change in the law is, first, one which per-
tains to the ceremonial law, and thus it does not con-
tradict the general validity of the Old Testament law
as presented in this book, Second, this change is said
to be a “necessary” change, arising from its ceremonial
character and from the Scriptural teaching that the
final High Priest would come after the order of
Melchizedek. This kind of necessity does not prove
that any other law of God has been changed unless it
too is ceremonial in nature and dictated by the word
of God Himself. Consequently, Hebrews 7 does not
stand in opposition to the presumption that the Old
Testament law is binding today untif God’s word
teaches us otherwise.

Theological Considerations About
Revelation and the Covenant
If we turn now from arguments against the law’s
general validity which arise from consideration of
specific passages of Scripture, we come to a variety
of theological considerations which are meant to
militate against the perspective which has been
