week after week.

It is ICE’s contention that the only way to turn the tide in
this nation is te capture the minds of the evangelical community,
which numbers in the tens of millions. We have to con-
vince the liberal-leaning evangelicals of the biblical nature
of the free market system, And we have to convince the
conservative evangelicals of the same thing, in order to
get them into the social and intellectual battles of our day.

In other words, retreat is not biblical, any more than so-
cialism is.

By What Standard?

We have to ask ourselves this question: “By what stan-
dard?” By what standard do we evaluate the claims of the
socialists and interventionists? By what standard do we
evaluate the claims of the secular free market economists
who reject socialism? By what standard are we to con-
struct intellectual alternatives to the humanism of our
day? And by what standard do we criticize the social
institutions of our era?

if we say that the standard is “reason,” we have a
problem: Whose reason? If the economists cannot agree
with each other, how do we decide who is correct? Why
hasn’t reason produced agreement after centuries of de-
bate? We need an alternative.

It is the Bible. The ICE is dedicated to the defense of
the Bible's reliability. But don’t we face the same prob-
lem? Why don’t Christians agree about what the Bible
says concerning economics?

One of the main reasons why they do not agree is that
the question of biblical economics has not been taken
seriously. Christian scholars have ignored economic
theory for generations. This is why the ICE devotes so
much time, money, and effort to studying what the Bible
teaches about economic affairs.
