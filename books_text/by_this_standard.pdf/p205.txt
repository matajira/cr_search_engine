GOD'S COMMANDMENTS ARE A NON-LEGALISTIC RULE OF OBEDIENCE 175

thors of the Confession saw eye to eye with Bolton in
these matters. The law of God as delivered to Moses
expresses the same perfect rule of righteousness
which was binding upon man as created, even prior
to the fall (£9:1-2),

The corpus of law contained ceremonial laws
typifying the saving work of Christ and certain
moral instructions pertaining to the holy separation
of God’s people from the unbelieving world (19:3). It
also contained judicial laws particularly worded for
the ancient Jewish civil state, the general equity of
which continues to bind men (19:4). Although the
law is not a way of personal justification, it continues
to be a rule of life both for the saved and the un-
saved; Christ in the Gospel does not dissolve but
rather strengthens this obligation (19:5-7).

This is Not “Legalism”

We must agree with the Publisher’s Introduction
to the Banner of Truth reprint of Bolton’s work
against antinomianism: “The slur of legalism’ often
cast upon those who framed the Westminster Con-
fession of Faith finds no justification in this instruc-
tive and edifying work.”§ To maintain the full au-
thority of God's law today—a conclusion to which
every line of Biblical study drives us — will be unpop-
ular in some degree with many people today, and it
will be maligned as “legalism.” To that charge John
Murray could simply answer: “It is strange indeed
that this kind of antipathy to the notion of keeping

6, Mbid., p. 12.
