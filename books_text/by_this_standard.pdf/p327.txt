AUTONOMY AND ANTINGMIANIEM 297°

ever values come into one’s life must be freely chosen
and defined by him on his own. Unlike Platonism,
then, existentialism makes ethics very relevant; far
from being unattainable, the standard of right and
wrong is immediately accessible to the individual,
being completely under his voluntary control! He
can readily know what to do in particular ethical
situations, for he himself decides what is right and
wrong in each case. Of course this ethical relevance
is purchased at the extremely high price of forfeiting
an absolute authority in ethics. For Sartre every
choice made by man is absurd, but every choice
(providing it was genuinely a free choice) is
justifiable. There are not good and bad choices, only
choices. What is chosen as right by one individual in
a specific situation does not govern what should be
seen as right by another individual in a similar situa-
tion. Everyone “does what is right in his own eyes,”
and consequently there is no universal, binding
standard of conduct which can guide and correct our
living.

Plato had ethical absolutes without relevant ap-
plications. Sartre had relevant applications without
an ethical absolute. Both problems—ultimately
destructive of ethics in their own ways—stemmed
from a rejection of God’s revelation of His divine law
for human behavior. By contrast, the Christian ethic
has absolute authority, being based on the revelation
of the Lord’s will. It also has relevance, for what the
all-knowing and all-controlling God says pertains
quite specifically to our day-to-day lives and prob-
lems; God has clearly revealed unchanging stand-
