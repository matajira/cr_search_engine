‘THE COVENANTS UNIFORM STANDARD OF RIGHT AND WRONG 43

from suppressing or changing the law or moral
standard by which God's people are to live! Just the
opposite is true. Contrary to those who think that the
Mosaic law is not applicable to the New Testament
believer, Scripture teaches us: “This is the covenant
that I will make with the house of Israel after those
days, says the Lord: I will put my laws into their
minds and I will write them upon their hearts” (Heb.
8:10).

The establishment of the New Covenant does not
imply the abrogation of the Mosaic law or its
depreciation in any sense! The idea of a new law is
ruled out altogether, for it is the well known law of
God which He says He will write upon the hearts of
New Covenant believers. Unlike the Old Covenant
where God found fault with the people for breaking
His commandments (Heb. 8:8-9), the New Cove-
nant will give internal strength for keeping those
very commandments. It will write the law on believ-
ers’ hearts, for out of the heart are the issues of life
(Prov. 4:23). The Holy Spirit of God will indwell the
heart of believers, writing God’s law therein, with
the result that they will live according to the com-
mandments. “I will put My Spirit within you and
cause you to walk in My statutes, and you will be
careful to observe My ordinances” (Ezk. 36:27). As
Paul writes in Romans 8:4, those who now walk ac-
cording to the Spirit have the requirement of the law
fulfilled within them. America’s twentieth-century
orthodox Protestant leader J. Gresham Machen
said, “The gospel does not abrogate God’s law, but it
