A CONSEQUENTIAL ETHIC ENDORSES THE LAW 83

Christ has pre-eminence over all (Col, 1:18), It will be
for our good, our neighbor's good, and our society's
good if all of our actions and attitudes are governed
by an interest in the kingdom of Jesus Christ.

How do we pursue that kingdom? How do we
gain the benefits which God promises to those who
will live according to His righteousness? Obviously,
by obeying the King and manifesting His righteous-
ness in our lives, God’s word shows us how to do just
that by setting down the law of the Lord for us.
Biblical law is a pathway to divine benefits—not an
ugly, dour, painful course for believers. It is not only
a demand, it is something to desire! As John said,
“His commandments are not burdensome” (1 John
5:3). They are the delight of the righteous man who
receives God’s blessing (Ps. 1). If we wish to have a
morality which promises blessed consequences, then
our morality must be patterned after the law of God.

Consider what God’s word says about following
the commandments of God. It brings to us life and
well-being (Deut. 30:15-16), blessing and a strong
heart that does not fear (Ps. 119:1-2; 112:5-7). Obe-
dience produces peace and security (Ps. 119:28, 165,
175; Prov. 13:6; Luke 6:46-48). The Lord’s loving-
kindness is upon those who obey His precepts (Ps.
103:17-18), and they walk in liberty (Ps. 119:45; Jas.
2:25). As indicated already above, keeping God’s
word results in prosperity with respect to all of our
daily needs and interests (cf. Joshua 1:7). Moreover,
collective obedience will bring blessing upon a soci-
ety as well. “Righteousness exalts a nation” (Prov.
14:34), giving it health, food, financial well-being,
