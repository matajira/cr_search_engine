B. CARDINAL DOCTRINES
OF THE FAITH

5

THE COVENANT’S UNIFORM
STANDARD OF RIGHT AND WRONG

“My covenant I will not violate, nor will | alter
the utterance of My lips” (Psalm 89:34).

If something was sinful in the Old Testament, it
is likewise sinful in the age of the New Testament.
Moral standards, unlike the price of gasoline or the
changing artistic tastes of a culture, do not fluctuate.
In the United States, there was a time when driving
your car at 65 miles per hours was permissible; now
any speed above 55 is illegal. But God’s laws are not
like that: just today, unjust tomorrow. When the
Lord makes a moral judgment, He is not unsure of
Himself, or tentative, or fickle. Unlike human law-
makers, God does not change His mind or alter His
standards of righteousness: “My covenant I will not
violate, nor will I alter the utterance of My lips” (Ps.
89:34). When the Lord speaks, His word stands firm
forever. His standards of right and wrong do not
