Homosexuality
A Biblical View
by Greg L. Bahnsen

This book confronts the emotionally-charged issue of
homosexuality and the fundamental issues related to
it—individual rights and biblical authority. In the face of
social pressures to allow homosexual practice without
legal restraint, Bahnsen clarifies personal freedom under
both the law and scriptural directives.

Bahnsen's case is solidly evangelical amid divergent
attitudes toward homosexuality within the Christian
church. But churches are encouraged nonetheless to
accept and support homosexuals who repent and con-
fess Christ as Savior.

152 pp. indexed, paperback: $6.99; hasdback: $25.00
Institute for Christian Economics, P.O. Box 8000, Tyler, Texas 75711
