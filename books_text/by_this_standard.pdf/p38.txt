& BY THIS STANDARD

ethics is taken up--namely, the normative perspec-
tive dealing with the question of standards for con-
duct. Motivational and consequential perspectives
{touching on inner character and goal in ethics) are
not equally treated, nor is the vital area of producing
and maintaining moral behavior.

Moreover, the one aspect of ethics which is the
focus of attention in these studies, the question of
law, is presented with a view toward avoiding certain
serious errors that can be made about God’s law. Obe-
dience to God’s law is not the way a person gains jus-
tification in the eyes of God; salvation is not by meri-
torious works but rather by grace through faith. And
while the law may be a pattern of holy living for
sanctification, the law is not the dynamic power
which enables obedience on the part of God’s people;
rather, the Holy Spirit gives us new life and strength
to keep God’s commands. The externalistic inter-
pretation of God’s law which characterized the Phar-
isees is also repudiated herein; the demands made by
God extend to our hearts and attitudes so that true
obedience must stem from a heart of faith and love.
Itis not found simply in outward conformity to (part
of) His iaw.

What these studies present is a position in Chris-
tian (normative) ethics. They do not logically commit
those who agree with them to any particular school
of eschatological interpretation. Premillennialists,
amillennialists, and postmillennialists can ail har-
monize this normative perspective with their views
of history and God’s kingdom. While the author has
definite views in eschatology, they are not the subject
