THE COVENANTS UNIFORM STANDARD OF RIGHT AND WRONG 39

salvation given by God in the New Testament will in
some future day no longer be a permanent guaran-
tee of His favor toward us. But praise the Lord that
His word is stable! He never lets us down as did our
human parents and human rulers with commands
that are unfair and promises that are not kept.

Whatever God says endures and cannot be emp-
tied of validity (cf. John 10:35). God’s gracious salva-
tion and the justice of His law shalt not be abolished
but endure forever:

Hearken unto me, my people; and give ear unto
me, O my nation: for a law shall proceed from
me, and I will make my judgment to rest for a
light of the people. My righteousness is near;
my salvation is gone forth, and mine arms shall
judge the people; the isles shall wait upon me,
and on mine arm shall they trust. Lift up your
eyes to the heavens and look upon the earth be-
neath: for the heavens shall vanish away like
smoke, and the earth shall in like manner: but
my salvation shall be forever, and my righteous-
ness shall not be abolished. Hearken unto me,
ye that know righteousness, the people in whose
heart is my law; fear ye not the reproach of
men, neither be ye afraid of their revilings. For
the moth shal! eat them up like a garment, and
the worm shall eat them like wool: but my right-
eousness shall be forever, and my salvation from
generation to generation (Isa. 51:4-8).

The righteous law of God which condemns our
