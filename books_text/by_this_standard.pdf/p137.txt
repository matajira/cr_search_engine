NEW TESTAMENT ETHICAL THEMES ENDORSE THE LAW 107

make you perfect in every good thing to do His will,
working in us that which is well-pleasing in His sight,
through Jesus Christ, to whom be the glory for ever
and ever. Amen” (13:20-21).

Perhaps the most fundamental ethical concept in
either the Old or New Testament is that of the will of
God. All ethical decisions and moral attitudes of
God’s people must be in accord with the will of the
Lord by which He prescribes what is good, or well-
pleasing, or perfect in His sight. Anything conflict-
ing with that will is immoral and displeasing to God,
quite naturally. Jesus said that His “meat” was to do
the will of the Father who sent Him (John 4:34), and
that those who did the will of the heavenly Father
were His “brother and sister and mother” (Matt.
12:50); we manifest whose children we are by our
righteous behavior or lack of the same (i John 3:1).
Christ taught His disciples to pray, “Thy will be
done on earth as it is in heaven” (Matt. 6:10). Doing
God’s will is not merely a matter of words but of con-
crete acts of obedience (Matt. 21:28-31); the will of God
must be done from the heart (cf. Eph. 6:6). There-
fore, not those who cry “Lord, Lord,” but only those
who do the will of the Father in heaven will enter
into the kingdom (Matt. 7:21); those who know the
Lord’s will and fail to do it will be beaten with many
stripes (Luke 12:47). On the other hand, if a man
does the will of God, he will be able to discern the doc-
trine which comes from God (John 7:17), and his
prayers will be heard (John 9:31; cf. 1 John 5:14).
While the world and its lusts pass away, he who does
the will of God abides forever (i John 2:17). Conse-
