254 GY THIS STANDARD

13:7), and since the Old Testament law stipulated
“You shall not revile God, nor curse a ruler of your
people” (Ex. 22:28), Paul himself displayed a repen-
tant spirit when he had (unwittingly) spoken evil ofa
ruler (Acts 23:5).

Old Testament believers were told to pray for
their unbelieving, Gentile rulers (Jer. 29:7; Ezra
6:10). When captive in Babylon, they were to seek
the peace of Babylon. This would clearly contrast
any attitude of resistance. Likewise, in the New Tes-
tament, God’s people are exhorted to pray for kings
and all that are in high places (1 Tim. 2:2), and Peter
writes to Christians in “Dispersion” (1 Pet. 1:1) who
faced imminent persecution from the Roman high
command (1:6; 4:12; 5:13) that they should imitate
the godly pattern of peace-seeking as found in Psalm
34:14 (1 Pet. 3:10-14). Over and over again we find
definite continuity between the Old and New Testa-
ments regarding political ethics. Here that continu-
ity is evident in that saints under both the Old and
New Covenants were to respect civil rulers as ap-
pointed of God, praying for them, and seeking peace
within their societies. God’s people have always had
the obligation to submit to their magistrates, know-
ing that those same rulers were ordained as part of
God’s moral rule over creation. Just because the
ruler stands under the authority of God, those who
profess allegiance to God must respect the ruler. It is
not simply out of pragmatic expediency that the
Christian obeys the civil authorities—“not simply
because of the wrath” which they can express against
dissenters (Rom. 13:5a). He must obey also “for the
