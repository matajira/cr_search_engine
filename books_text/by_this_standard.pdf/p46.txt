16 BY THIS STANDARD.

ethics found in the revealed scriptures, and a stan-
dard for natural ethics found in man’s reason as it
examined the world. Of course that left some ethical
decisions or evaluations independent of the word of
God, and those religious issues which remained
under the umbrella of the Bible were ultimately de-
cided by the Pope. Thus the medieval world was ripe
for tyranny, in both a secular state and despotic
church,

Over against this, the Reformers challenged the
traditions of men and reasserted the full authority of
God's word, declaring sola Scriptura and tota Scriptura
(only Scripture and all of Scripture), The final
standard of faith and practice, the yardstick for all of
life (personal as well as social morality), was the
Bible. That is why the Puritans strove to let God's
word form their lifestyle and regulate their behavior
in every sphere of human endeavor. A holy God re-
quired them to “be holy in all your conduct” (I Peter
1:15), and the standard of holy living was found in
God's holy law (Rom. 7:12). Accordingly the Puri-
tans even took God’s law as their yardstick for civil
laws in the new land to which they eventually came,
and we have enjoyed the fruits of their godly venture
in this country for three centuries naw. The attitude
of the Reformers and Puritans is nicely summarized
in Robert Paul's painting which hangs in the
Supreme Court Building, Lausanne, Switzerland; it
is entitled “Justice Instructing the Judges” and por-
trays Justice pointing her sword to a book labeled

“The Law of God.”
