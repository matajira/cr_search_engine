200 BY THIS STANDARD.

come up with valid social alternatives to a collapsing hu-
manist civilization — a humanist order which they now
seek to embarrass and even destroy, if possible—if
they are to escape the fate of those who now live
under the self-declared sovereignty of self-
proclaimed autonomous man.

The Bus Will Crash, Unless... .

There is an old political maxim that says: “You
can't fight something with nothing.” The wisdom of
this maxim has been demonstrated for over half a
century: Christians have been impotent to stop the
drift into social disintegration. Now at last they are
feeling the cultural pressure. Their children are at
last being visibly assaulted by the perversions of this
age. Their churches are now being threatened by
some federal bureaucracy. They are now becoming
aware of the fact that they can no longer remain as
silent participants in the back of humanism’s bus,
unless they are willing to go over the cliff. They are
slowly beginning to understand that they can’t get off
this speeding bus, although a theology of “back door
escape” has been popular until quite recently. But
“Rapture fever” is steadily cooling. So there is now
only one alternative: they must persuade the other
passengers to allow them to take over at the wheel.

Christians alone possess a valid road map: the
law of God. This map is rejected by the present
driver, and if the other passengers (including confused
and psychologically defeated Christians) continue to
assent to this driver, then the bus will crash. It may
even explode.
