2 SY THIS STANDARD

attitudes (some pro-nomian, some antinomian) can
be found, with subtle variations distinguishing one
school of thought from another in many cases.
Against the background of this welter of opinions, it
would be well to specify and summarize the position
regarding God’s law which is taken in these chapters.

The Basic Thesis

Fundamental to the position taken herein is the
conviction that God’s special revelation ~ His writ-
ten word—is necessary as the objective standard of
morality for God’s people. Over against the autono-
mous ethical philosophies of men, where good and
evil are defined by sinful speculation, the Christian
ethic gains its character and direction from the re-
vealed word of God, a revelation which harmonizes
with the general revelation made of God's standards
through the created order and man’s conscience.

When we explore what the Bible teaches about
the character of God, the salvation accomplished by
Christ, the work of the Holy Spirit in making us holy
in heart and conduct, or the nature of God’s cove-
nantal dealings with men, we. see why the believer
should take a positive attitude toward the command-
ments of God, even as revealed in the Old Testa-
ment. Indeed, the Bible teaches that we should pre-
sume continuity between the ethical standards of the
New Testament and those of the Old, rather than
abbreviating the validity of God’s law according to
some preconceived and artificial limit.

Because He did not come to abrogate the Old
Testament, and because not one stroke of the law
