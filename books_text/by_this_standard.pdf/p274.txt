244 BY THIS STANDARD

taxerxes, to have God’s law enforced (even to the
point of its penal sanction of death) throughout the
region surrounding Israel (Ezra 7:11-28). Indeed,
David himself declared that he would take God’s law
for Israel and speak it before other kings (Ps. 119:46).
And he warned that the kings and judges of the earth
who would not fear Jehovah and serve Him would
perish in the way (Ps. 2:10-12).

The Old Testament evidence is quite abundant,
then, that expectations for civil rulers outside of
Israel were often the same as they were for rulers in
Israel. They were appointed by God to avenge His
wrath by enforcing the law of the Lord, The political
aspects of God’s law, therefore, were certainly not in-
tended for the exclusive use of the Jews in their
“theocratic” situation. The political justice God re-
quired in Israel was required of all nations as well. It
was not racially or geographically relative.
