Part Il: APPLICATION OF GOD'S
LAW TO POLITICAL ENDS

22

THE POLITICAL IMPLICATIONS
OF THE COMPREHENSIVE GOSPEL

“if we must glorify God even In our eating and
drinking, then surely we must also glorify Him
in the way that we vote and thereby encourage
statesmen to rule our society.”

It used to be the case that when a Bible-believing
author wanted to write on some aspect of sacial mor-
ality or political policy, he had to give an introduc-
tory apologetic and defense for entering into such an
area of discussion. Given the background of liberal
or modernistic involvement in politics, given the
threat of the social gospel, and given the evangelical
withdrawal from the world encouraged by church-
centered pietism and law-denying dispensationalism,
anyone who wrote on subjects of political or social
ethics would easily be suspected of compromise or de-
parture from the faith. So reticence characterized
evangelical and Reformed publications in these areas.
