280 GY THIS STANDARD

things by the Jews, Paul responded: “/f1 am an evif-
doer {cf. the same expression in Romans 13:4] and
have committed anything worthy of death [the law’s
designation for a capital crime}, then I refuse not to
die.” Paul did not argue that these Old Testament
penal sanctions had been abrogated, nor that they
were appropriate only for the Jews of the theocracy.
He rather insisted that they applied at the present
time, and he would not seek to avert their require-
ment. He was willing to submit to divine justice, the
justice of God’s Jaw—provided, of course, that he
had truly transgressed that law. We too endorse the
justice of God’s penal code, if the Bible is to be the
foundation for our Christian political ethic.

Invalid Attempts to Sidestep Biblical Penology

Some Christians have attempted to escape the
Biblical requirements regarding penal sanctions on
crime, Without answering the positive considera-
tions which have been laid out above, they have sug-
gested various reasons why we should not endorse
the penal sanctions of the Old Testament law. We
can quickly survey some of these reasons.

Some say that the use of the death penalty would
cut short the possibilities for evangelism. That may
be true, but we must avoid portraying God’s word as
in conflict with itself (as though the evangelistic com-
mission of the church could override the justice
demanded by the state). “The secret things [for ex-
ample, who will be converted} belong unto Jehovah
our God; but the things that are revealed [for exam-
ple, the law’s requirements} belong unto us and to
