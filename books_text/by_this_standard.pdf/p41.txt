SPECIFICATION OF PURPOSE AND POSITION 11

their actions. Nor does the Christian advocate the
punishment of criminals who have not been con-
victed under the full provisions of due process in a
court of law. Those who believe that God’s law for
society ought to be obeyed must be concerned that
all of God’s laws for society be obeyed, touching not
only the punishment of offenders but their just treat-
ment and conviction as well.

Finally we must distance ourselves from the
mistaken impression that because these studies pay
attention to a particular subsection of Christian
theology and ethics they intend to portray that area
of the truth as more important than other areas of
Biblical teaching. All discussion will of necessity nar-
rowly consider one topic instead of another, for not
everything can be discussed simultaneously. To
write about the virgin birth, for instance, is not to
offer a slight to the doctrine of Christ’s coming again;
it is merely to take up one of many important mat-
ters of Christian theology.

Likewise, to set forth a position regarding the
validity of God’s Old Testament law and to argue
that its standards of political justice bind us today (so
that civil magistrates ought to enforce the law’s penal
sanctions) is to focus attention on just one aspect of
the total picture of Christian theology and ethics. It
is not to say that the most important emphasis in our
lives and thinking should be the Old Testament law
of Moses. It is not to say that political ethics is more
vital than personal ethics or that the cultural man-
date is more crucial than the evangelistic mandate of
the church. And it most certainly is not to contend
