DISCONTINUITY BETWEEN THE COVENANTS ON THE LAW 163

the finished work of the Savior and shows faith by
clinging to Him for salvation totally apart from the
old ceremonies. From Scripture it is evident that the
New Covenant arrangement is better than the Old
Covenant pertaining to redemption, and accord-
ingly those redemptive laws have been made out-
wardly inoperative. Here is a discontinuity between the
Covenants which can be supressed only at the cost of
totally misunderstanding the teaching of the New
‘Testament.

The logic of the writer of Hebrews is that, if a
New Covenant has been given, then it must be a det-
fer covenant which as such makes the Old Covenant
outmoded. Moses himself witnessed to the provision-
ary glory of the administration of God’s grace found
in the Pentateuch by looking beyond the shadow and
promise to the realization to come (Heb. 3:5b).
Likewise, Jeremiah spoke for God of a “New” cove-
nant to come, and that very fact (according to the
author of Hebrews) indicated that already the
Mosaic administration was deemed obsolete and
passing away, ready to vanish (Heb. 8:13).

Saying this leads the author of Hebrews right
into a discussion of the first covenant’s ritual or-
dinances (9:1ff.). The work of Christ is in every way
superior to these. He is “the surety of a better cove-
nant,” “a better hope” (7:22, 19) because His
priesthood is everlasting (7:21, 24-25), and His
sacrifice of Himself is totally efficacious (7:26-28).
‘The very repetition of the Old Covenant sacrifices
demonstrated that they were temporary and im-
perfect (Heb. 10:4f.). The superiority of Christ’s
