10

A CONSEQUENTIAL ETHIC
ENDORSES THE LAW

“It wiH be for our good, our nelghbor’s good,
and our society's good, if ail our actions and at-
titudes are governed by an interest in the king-
dom of Jesus Christ.”

We have said earlier that all of life is ethical: peo-
ple are constantly making moral decisions, forming
attitudes, and setting goals. We have also noted that
there are many competing views of ethics. Let us
delineate three bastc approaches to ethical decision-
making and ethical evaluating of ourselves, our ac-
tions, and our attitudes. First, some people weigh all
moral issues and make their choices according to a
norm or standard of good and evil. Second, others
will determine how actions and attitudes are to be
morally graded on the basis of one’s character—his
traits, intentions, or motives. Third, there will be
others who see the consequences which follow from a
