‘314 BY THIS STANDARD

7:25). The very bondage from which the Spirit re-
leases us is described by Paul as precisely the sinful
nature’s inability to be subject to the law of God (Rom.
8:7). Obviously, freedom from this inability must
now mean being subject to the law of God! This free-
dom does not turn the grace of God into licentious-
ness (cf. Jude +) but inclines the heart of those once
enslaved to sin to the Spirit-given law (Rom. 7:14).

The “ordinance of the law’ is to be “fulfilled in us
who walk not after the flesh but after the Spirit”
(Rom. 8:4). Therefore the Bible makes it quite clear
that our Spiritual freedom is not liberty from God’s
law, but liberty x God’s law. James calls the com-
mandments of God “the perfect law of liberty”
(2:25), thereby combining two descriptions of the
law given by the Psalmist: “The law of the Lord is
perfect” (Ps. 19:7) and “I will walk at liberty, for I
seek Thy precepts” (Ps. 119:45). Genuine freedom is
not found in flight from God’s commands but in the
power to keep them. God’s Spirit frees us from the
condemnation and death which the law brings to sin-
ners, and the Spirit breaks the hold of sin in our
lives.

However, the freedom produced by the Spirit
never leads us away from fulfilling God’s law: “For
you, brethren, were called for freedom; only use not
your freedom for an occasion to the flesh, but
through love be servants one to another. For the
whole law is fulfilled in one word, even in this, You
shall love your neighbor as yourself” (Gal. 5:13-14).
When Paul teaches that “where the Spirit of the Lord
is, there is liberty” (2 Cor, 3:17), it is taught in the
