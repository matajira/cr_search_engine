SPECIFICATION OF PURPOSE AND POSITION 3.

will become invalid until the end of the world, Jesus
declared: “Therefore, whosoever breaks one of these
least commandments and teaches men so, shall be
called least in the kingdom of heaven” (Matt.
5:17-19). Given this instruction, our attitude must be
that all Old Testament laws are presently our obliga-
tion unless further revelation from the Lawgiver
shows that some change has been made.

The methodological point, then, is that we presume
our obligation to obey any Old Testament com-
mandment unless the New Testament indicates
otherwise. We must assume continuity with the Old
Testament rather than discontinuity. This is not to
say that there are no changes from Old to New Testa-
ment. Indeed, there are — important ones. However,
the word of God must be the standard which defines
precisely what those changes are for us; we cannot
take it upon ourselves to assume such changes or
read them into the New Testament. God’s word, His
direction to us, must be taken as continuing in its
authority until God Himself reveals otherwise. This
is, in a sense, the heart of “covenant theology” over
against a dispensational understanding of the rela-
tion between Old and New Testaments.

To this methodological point we can add the sub-
Stantive conclusion that the New Testament does not
teach any radical change in God’s law regarding the
standards of socio-political morality. God’s law as it
touches upon the duty of civil magistrates has not
been altered in any systematic or fundamental way
in the New Testament.

Consequently, instead of taking a basically an-
