House Divided
The Breakup of Dispensational Theology
by Greg L. Bahnsen and Kenneth L. Gentry

In 1988 and 1989, three books were published that
criticized the theology of Christian Reconstructionism,
and also the theology of the entire historic Christian
faith, by attacking the idea of Christian social reform.
They were Dominion Theology: Blessing or Curse?, H.
Wayne House and Thomas Ice; Whatever Happened to
Heaven?, by Dave Hunt; and The Road to Holocaust, by
Hal Lindsey. The arguments of all three books are
answered in detail by Hotse Divided, with Greg Bahnsen
taking up the question of biblical law, and Kenneth
Gentry taking up the question of biblical eschatology.

What House Divided demonstrates is that dispensational
theology has now been shattered by its own defenders.
They are no longer willing to defend the original system,
and their drastic modifications have left it a broken shell.
They are also deeply divided among themselves on the
crucial questions of biblical interpretation and social ac-
tivism. In short, today’s defenders of dispensationalism
“destroyed the system in order to save it.” No one has
attempted to put this shattered theological system back
together. No one will even outline its main points.

411 pp., indexed, hardback, $25.00
Institute for Christian Economics, P.O, Box 8000, Tyler, TX 75711
