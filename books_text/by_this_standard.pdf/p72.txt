42 BY THIS STANDARD

law (Deut. 5ff.). The failure of the Mosaic genera-
tion can be called a failure in obedience (Heb. 6:4),
but this was identical with a failure of faith (Heb.
3:9). The righteousness of the Mosaic daw was al-
ways to be sought dy faith, not works (Rom. 9:31-32).

We see illustrated here that even the Mosaic cov-
enant characterized by law is a gracious covenant.
The law which we read in the Old Testament is a
provision of God’s grace to us (Ps. 19:29, 62-64).
Every covenant carries stipulations which are to be
kept, as we have seen. But prior to that we saw that
all of the covenants of God are unified into one over-
all Covenant of Grace, fully realized with the com-
ing of Christ in the New Covenant. So if there is one
covenant enjoyed by the people of God throughout the
ages, then there is one moral code or set of stipulations
which govern those who would be covenant-keepers.
Therefore, we must answer that of course New Tes-
tament believers are bound to the Old Testament
law of God. His standards, just like His covenant,
are unchanging.

The Newness of God’s Covenant

This perspective is confirmed by the word of
God. When we inquire as to what is new about the
New Covenant under which Christians now live, we
must allow the Lord to define the proper answer. We
cannot read into the idea of a “new Covenant” just
anything we wish or can imagine. The revealed terms
of the New Covenant are given to us in both Jere-
miah 31:33-34 and Hebrews 8:8-12, and when we
Jock at them we find that the New Covenant is far
