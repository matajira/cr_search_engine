AUTONOMY AND ANTINOMIANISM = 301

through Moses and the other Old Testament writers.

Finally, we can mention fatent antinomianism as
an incipient brand of opposition to God’s law. Latent
antinomians are not explicitly antagonistic to the
law; instead they would éroadly endorse the Old
Testament commandments. But at this point they
would take a smorgasbord approach to the collection
of laws found in the Old Testament, accepting some
and rejecting others as binding today en some other
basis than specific revealed teaching. The latent anti-
nomian is opposed to some laws in the Old Testa-
ment, and he has xo Biblical warrant to offer for his re-
jection of them, This is not an outright rejection of
the category of law, nor of written law, nor of Old
Testament law. It is only incipiently antinomian
because at heart it opposes the binding authority of
certain Old Testament commandments on non-
Biblical grounds; if the principle of this practice were
carried out consistently and self-consciously, it would
amount to genuine antinomianism.

Latent antinomians usually want the Old Testa-
ment law, but not certain categories of it (e.g., civil) or
not its full details (e.g., case laws or penal sanctions).
If those who felt this way could offer some attempted
Biblical justification for setting these portions of the
law aside, then they might be theologically
mistaken, but they would not be latently anti-
nomian. It is the failure to let Godt word govern
which laws we take as binding and which laws we see
as set aside that makes this position Jatenily anti-
nomian. Jesus said that man must live by every
word that proceeds from God's mouth (Matt. 4:4).
