‘LAW AND POLITICS IN THE NEW TESTAMENT 265,

justice will be forced to substitute some other criter-
ion of good and evil for it. The civil magistrate can-
not function without some ethical guidance, without
some standard of good and evil. If that standard is
not to be the revealed law of God (which, we must
note, was addressed specifically to perennial prob-
lems in political morality), then what will it be? In
some form or expression it will have to be the law of
man (or men)—the standard of self-law or auton-
omy. And when autonomous laws come to govern a
commonwealth, the sword is certainly wielded in
vain, for it represents simply the brute force of some
men’s will against the will of other men. “Justice”
then indeed becomes a verbal cloak for whatever
serves the interests of the strongmen in society
(whether their strength be that of physical might or
of media manipulation).

Men will either choose to be governed by God or
to be ruled by tyrants. Because of the merciful, re-
straining work of the Holy Spirit in societies, we do
not see at every stage in Aistory these stark polarities
coming to expression; most societies will to some
measure strive for conformity to God’s law, even
when it is officially denounced. However, in principle
the choices are clearly between God’s law and man’s
law, between life and death for a society. If no divine
law is recognized above the law of the state, then the
law of man has become absolute in men’s eyes—
there is then no /ogical barrier to totalitarianism.

When God's law is put aside, and the politician’s
law comes to reign in its place, we have “the beast”
described for us by the Apostle John in Revelation
