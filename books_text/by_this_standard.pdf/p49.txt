GOD'S WORD IS OURNORM 19

advocate of situation ethics in our day, Joseph Fletcher,
tersely concludes that “Law ethics is still the enemy.”
And these lawless attitudes continue to filter down to
the local level. A “liberated” woman writes in The Re-
formed Journal (1975): “I thank God that as a reformed
Christian I worship a God of grace and not a God of
rules.”

The Biblical Attitude

By contrast the biblical attitude is expressed by
the apostle John when he says, “The love of God is
this, that we keep His commandments; and His
commandments are not burdensome” (1 John 5:3).
Believers in Jesus Christ do not wish to live as a law-
unto-themselves, unfettered by external divine re-
quirements. They welcome and love the biblical
standard of right and wrong ~no matter what it may
stipulate for any aspect of life. God’s holy law is not a
burden to them, and they are not constantly search-
ing for substitutes which will be more pleasing to the
autonomous attitude of their age. They do not prefer
self-law to God’s law, for they recognize that it is im-
possible to draw straight lines and make accurate
measurements in ethics without the infallible yard-
stick of God’s word.

All of life is ethical, I have said. And all ethical
judgments require a dependable standard of right
and wrong. Jesus said, having just declared that He
will eternally reject all those who practice lawless-

and Contemporary Philosophy, cd. Ian T. Ramsey (London: SCM
Press, 1966), p. 34.
