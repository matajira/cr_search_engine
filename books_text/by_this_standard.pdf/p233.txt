‘THE TRADITIONAL “THREE USES" OF THE LAW 203

cord. Luther said that apart from appealing to the
law for justification, “we cannot sufficiently praise
and magnify those works which are commanded of
God” (Commentary at Gal. 3:22). To remove the law
from the believer, thought Luther, “is a thing impos-
sible and against God.”? Accordingly Luther’s Small
Catechism begins with an exposition of the Deca-
logue. The Formula of Concord declared, “We be-
lieve, teach, and confess that the preaching of the
Law should be urged . . . also upon those who truly
believe in Christ, are truly converted to God, and re-
generated and are justified by faith” (Article VI.2).
Although the Calvinist branch of the Reformation
stresses the law as a good gift of God’s grace, and the
Lutheran branch stresses it as a constraint, they both
agree that the law is to be used to form the life of the
regenerate believer.

The Controversial “First Use”

Traditionally, Reformed thought has summarized
the proper use of the law into three specific functions,
Tt drives the convicted sinner to Christ (the second
use) and provides a pattern of sanctification for the re-
generated believer (the third use). Some debate has
surfaced in the past over the “third” or didactic use of
the law, but the Reformed faith has still persisted in
the Biblical affirmation that the law retains its binding
validity for the conduct of believers.

More recently disagreement has arisen with re-
spect to what the Reformers called the “first use” of the

2. Table Tatk, 286.
