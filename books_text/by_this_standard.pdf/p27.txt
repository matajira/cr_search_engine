FOREWORD

“But that’s what the Old Testament said! We live
in New Testament times.”

Whether spoken out loud or not, this is the reac-
tion that many Christians have to any suggestion
that we should conform to some requirement of the
law of God. A common working assumption is that
New Testament believers are not expected by God to
live according to Old Testament stipulations. It is er-
roneously thought that their ethical attitude and
standard should be limited to the New Testament,
almost as though the Old Testament is now nothing
but a historical curiosity—rather than a revelation
which is still profitable for “instruction in righteous-
ness” (2 Tim. 3:16-17). This book is written to stimu-
late Scripture-guided reflection on the question of
whether the Old Testament law is still binding as a
moral standard today. Such a question can prove
controversial, and one will find there exists a large
