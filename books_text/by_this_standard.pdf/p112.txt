82 BY THIS STANDARD

would result in blessings on the society’s children,
crops, rain, herds, cities, and fields; it would bring
peace to the people from without and prosperous
economy and health from within (Deut. 7:12-15;
11:13-15; 28:1-14; 30:15, 19; Lev. 26:3-12). In ethical
decision-making, we should properly consider the
end, aim, or consequences of our behavior. Doing
the right thing or having a proper attitude will result
in benefits. But benefits for whom? Should our aim be
to benefit ourselves, the other person, or the society as
a whole? The Bible indicates that each of these is a
subordinate, but vital, interest that we should have.
For example, when Christ commands, “Thou shalt
love thy neighbor as thyself” (Matt. 22:39), He tells
us to seek the benefit of the other just as we seek our
own benefit. Hence Paul tells husbands to love their
wives (the other) as their own bodies (the self) pre-
cisely because nobody hates himself (Eph. 5:28-29).

Egoism (note: net egotism) and altruism both
have a place in Christian ethics, as does a concern
for the wider collection of people in one’s society.
Thus, the Bible often exhorts the interest of the one
to be relinquished for the benefit of the many (for ex-
ample, 2 Cor. 8:9; Phil. 1:24). However, all of these
interests are subordinate to the one supreme goal for
all of our actions: the kingdom of God. Within that
kingdom the varying interests of one’s self, the other,
and the many are all harmonized.

Our Lord plainly declared that we were to “Seek
Just the kingdom of God and His righteousness.” The
kingdom of Christ is to have top priority when we
contemplate the consequences of our actions, for
