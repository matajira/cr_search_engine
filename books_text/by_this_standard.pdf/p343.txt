ARGUMENTS AGAINST THE LAW'S GENERAL VALIDITY 313.

tion that we should follow Jesus and not Moses is a
misleading and false antithesis. Since the New
Testament endorses the moral standards of the Old
Testament, we are not forced to choose between an
Old Testament ethic and a New Testament ethic. We
are to follow them both, for they constitute one
unified moral standard.

Is it true, as some claim, that since we live under
the New Covenant today we should formulate our
Christian ethic on the basis of the New Testament
Scriptures exclusively, seeing the standards of the
Old Covenant as obsolete? If we pay attention to the
very terms of the New Covenant, our answer must
be No. Jeremiah 31:33 stipulated that when God
made a New Covenant He would write His law on
the hearts of His people—not that He would
abrogate His law, replace His law, or give a new law.
Consequently, to live in submission to the New
Covenant is to rejoice in the law of the Old Cove-
nant, for it is written upon our hearts, out of which
are the issues of life.

Promises and Demands

Those who suggest that the establishment of the
New Covenant nullifies the general validity of the
Old Testament law appear to have confused the
sense in which the Old has become obsolete (Heb.
8:13) and the sense in which it continues the same
(Heb. 10:16). All of God’s covenants are unified.
They make the same moral demands and focus upon
the same promises. However, the promises call for
historical fulfillment —the change from anticipation
