124 BY THIS STANDARD:

Why are the law’s requirements never criticized or
explicitly repudiated in the New Testament? Why
are those who do not keep the law but claim to know
the Savior called liars? God’s inspired word says all
of these things and more. What reply can the detrac-
tors from God’s law today make in the face of such
insurmountable evidence of the law’s full validity?
The reply that is commonly, albeit fallaciously,
made is that we find details in the Old Testament
law which are somehow too strange or harsh to obey
today, or we find particular requirements in the law
which we in fact do not and should not observe in
our day. Of course, such replies as these do not face
the issues raised above. Surely God was completely
aware of the law’s details when He revealed those
truths in His word which, as observed above, con-
tradict the relaxing, ignoring, or disobeying of His
Jaw. If Scripture does not make an exception for us,
we do not have the moral prerogative to make excep-
tions for ourselves when it comes to the law’s author-
ity over us. No extra-biblical standard, reason, or
feeling can be legitimately used to depart from the
law of God, for God’s word has supreme and unchal-
lengeable authority. If the Lord says that His com-
mands are to be kept, no creature may draw His
word into question. So then, the attempt to belittle
obedience to God’s law today by pointing to allegedly
odd or harsh requirements in that law is doomed to
theological failure. It also borders on disrespect for
the Lawgiver whose holiness is transcribed for the
creature in God’s law. “O man, who are you who re-
plies against God?” (Rom. 9:20). It is never our
