NEW TESTAMENT ETHICAL THEMES ENDORSE THE LAW 117

Colossians 3:20 Paul instructs children to obey their
parents, “for this is well-pleasing in the Lord.” The
commandments of the law, therefore, can serve and
did serve as detailing what is well-pleasing to God,
even in New Testament morality.

Perfection

Perfection is another moral theme of the New
Testament which deserves our attention. Paul would
have believers “stand perfect and fully assured in all
the will of God” (Col. 4:12). John discourses against
fear because it is inconsistent with being made
perfect in love (1 John 4:18), and for John love is
tested by adherence to God’s commandments (cf.
5:2-3). James teaches that steadfastness through
trials will have “its perfect work,” so that we are lack-
ing in nothing (1:2-4), and he sees every perfect
gift —in contrast to sin—as coming from God abové
(1:17). With an insight into the special power of sins
of the tongue, James tells us that if any man does not
stumble in word he is a perfect man (3:2),

Studying perfection as a moral concept in the
New Testament, we once again are taken back to the
standard of God's law. Christ taught that our perfec-
tion must be modelled after the heavenly Father:
“Therefore you shall be perfect, as your heavenly
Father is perfect” (Matt. 5:48). Significantly, this ex-
hortation follows and summarizes a discourse on the
full measure of the Old Testament law’s demands
(vv. 21-48). When Christ was later approached by
one who presumed to be obedient to the law, Christ
taught him that to be perfect he would need to re-
