6 BY THIS STANDARD

In addition to localized imperatives and cultural
details of expression, we would note that certain ad-
ministrative details of Old Testament society are not
normative for today (for example, the type or form
of government, the method of tax collecting, the lo-
cation of the capitol). These aspects of Old Testa-
ment life were not prescribed by standing law, and
they do not bind us today.

Other discontinuities with Old Testament life
and practices would pertain to the typological fore-
shadows in the Old Testament — replaced according
to the New Testament with the realities they typi-
fied. For instance, we have the ceremonial laws of
sacrifice which served during the Old Testament as
“weak and beggarly” shadows of the perfect sacrifice
of Christ which was to come. We can also think here
of the provisions regarding the land of Palestine.
With the coming and establishment of that kingdom
typified by the “promised land,” and with the
removal of special kingdom privileges from the Jews
by Christ, the laws regulating aspects of the land of
Canaan (for example, family plots, location of cities
of refuge, the levirate institution) have been laid
aside in the New Testament as inapplicable.

Other examples could perhaps be given, but
enough has been said by now to demonstrate the
point that the position taken herein is not that every
last detail of Old Testament life must be reproduced
today as morally obligatory, but simply that our
presumption must be that of continuity with the standing
laws of the Old Testament (when properly, contex-
tually interpreted).
