NEW TESTAMENT ETHICAL THEMES ENDORSE THE LAW 99

ness of God’s kingdom or the way of righteousness,
our attention must be focused on God Himself as the
model of all righteousness. The faithful described in
Revelation 15 who have been victorious over the
Beast are portrayed as singing to the Lord, “right-
eous and true are Thy ways, Thou King of the ages”
(v. 3). Those who extol the righteousness of God
here are believers who resisted the Beast’s attempt to
replace God’s law with his own (cf. Rev. 13:16 and
Deut. 6:8), and the song which they sing is desig-
nated “the song of Moses, the servant of God”—a
phrase reflecting Joshua 22:5, “Only take diligent
heed to do the commandment and the law which
Moses the servant of Jehovah commanded you, to
love Jehovah your God, and to walk in all his ways,
and to keep His commandments, and to cleave unto
Him, and to serve Him with all your heart and with
all your soul.”

The righteousness of God is expressed in His law.
Accordingly, the kingdom righteousness demanded
by Christ and the apostles and the “way of righteous-
ness” encompassing the Christian faith both assume
and apply the law of God. Whenever these themes
appear in New Testament ethics, they are expressive
of the standard of God’s commandments as found
throughout the Old Testament. Such was the under-
standing of the New Testament writers themselves.

Holiness and Sainthood
A Biblical concept closely related to that of
righteousness is the concept of holiness. While the
former emphasizes a just and upright conformity
