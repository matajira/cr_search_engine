xxvI BY THIS STANDARD

in print. It leads to excessive humiliation.

Yet if someone from at least one modern theolog-
ical camp does not respond, and respond soon — dis-
pensationalist, neo-evangelical, Reformed, Roman
Catholic, or Eastern Orthodox~—then the intellec-
tual battle is very nearly won by the theonomists. It
does no good for defenders of an older world-and-life
view to pretend that they can safely ignore a brilliant
case presented for any new position, let alone the
biblical position. If the establishment theologians re-
main silent for another eight years, the theonomists
will have captured the minds of the next generation
of Christian activists and social thinkers. Once the
younger activists and intellectuals are won over, the
fight is in principle over. To the victors will go the
spoils: the teaching positions, the satellite T.V. net-
works, and shelf space in the Christian bookstores —
and maybe even secular bookstores, until they fi-
nally go bankrupt or go Christian.

Now, who will be the sacrificial lamb? Who
wants to attempt to prove in print that this little book
is the work of a heretic, or an incompetent? Who will
be the person to try to prove that this book's thesis
cannot be sustained by an appeal to the New Testa-
ment? Who will then go on to refute Theonomy in
Christian Ethics? A lot of very bright young men are
waiting to hear from you, and then to hear from Dr.
Bahnsen.

Stay tuned for “Bambi Meets Godzilla, Part II.”
