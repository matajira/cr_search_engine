290 BY THIS STANDARD

state is nof the same as saying that the state is sepa-
rated from obligation to God Himself and His rule.
Both church and state, as separate institutions with
separate functions (i.e., the church mercifully minis-
ters the gospel, while the state justly ministers public
law by the sword), serve under the authority of God,
the Creator, Sustainer, King, and Judge of aif
mankind in all aspects of their lives.

Different Senses of This “Separation”

When people today speak of their commitment to
the separation of church and state, we need to realize
that this commitment can be taken or interpreted in
many ways. “I believe in the separation of church
and state” may be the answer to one or more logically
distinct questions. For instance, we might ask
whether the church should dominate the state (for
example, the Pope dictating to kings) or the state
should dominate the church (for example, Congress
dictating church policy), and the answer might very
well be that we should hold to the separation of
church and state—namely, that neither institution
should dominate the other. We should have a free
church in a free state.

A second question might be whether the state
should establish one denomination over others as the
state-church (or tax the population for financial sup-
port of the ministers of one particular church or
denomination), and the answer again might very
well be that we should hold to the separation of
church and state~namely, that all churches should
be supported simply by voluntary offerings, and one
