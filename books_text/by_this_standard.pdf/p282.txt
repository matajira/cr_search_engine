252 BY THIS STANDARD

Since God has ordained the magistrates who rule in
the state, those magistrates have been put not only
in authority over others, but also under the authority
of God. Magistrates are under moral obligation to
the prescriptions of the Lord. John Murray ob-
served:

The civil magistrate is not only the means de-
creed in God's providence for the punishment of
evildoers but God’s instituted, authorized, and
prescribed instrument for the maintenance of
order and the punishing of criminals who vio-
late that order. When the civil magistrate
through his agents executes just judgment upon
crime, he is executing not simply God’s decret-
ive will but he is also fulfilling God’s prescriptive
will, and it would be sinful for him to refrain
from so doing.!

Since all civil magistrates have no power unless it
has been given to them from above—as Christ de-
clared, even while standing before Pilate (John 19:11)
—they are responsible to reverence and obey Almighty God.
When they, as with Herod, accept praise as a god,
they come under the terrible wrath of God and can
be deposed from power: “Upon a set day Herod ar-
rayed himself in royal apparel, and sat upon the
throne, and made an oration unto them. And the
people shouted, “The voice of a god, and not of a
man.’ And immediately an angel of the Lord smote

1. The Epistle to the Romans, 2 vols. (Grand Rapids, Michigan:
Eerdmans, 1965), IL, p. 149.
