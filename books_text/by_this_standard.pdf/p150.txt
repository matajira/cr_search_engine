120 BY THIS STANDARD:

plumbing yourself—if only you had some good
direction. Therefore, you conclude that you will go
down to the public library this morning and check
out a self-help book on kitchen plumbing. Add one
more feature to this scenario, namely, that you are
reasonably informed as to the operating procedures
of a public library. That is, you realize that the
library is not open all of the time and that only those
with library cards may have the privilege of checking
out books.

So then, let us go back to your decision to check
out a self-help book on plumbing this morning.
What does such a decision tell us about your current
beliefs? Among other things it tells us that you
believe (rightly or wrongly) that the public library is
open this morning, that you have a library card
there, and that the library card is still valid. If you
decided to use the library’s self-help phiumbing book
this morning but knew either that the library was
closed, that you had no card, or that your card was
expired, you would most likely be irrational or a
crook. People do not normally plan to use things
which are closed down (for example, the library),
non-existent, or expired (for example, your library
card).

Likewise when you wait in line at the Mobil Oil
gasoline station, fill your car’s tank with gas, and
then hand the attendant your credit card, you are
expecting that the card is still valid. Whether you
scrupulously check the expiration date on the credit
card before submitting it for payment to the atten-
dant or not, the very fact that you use the card
