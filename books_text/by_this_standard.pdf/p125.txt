NEW TESTAMENT ETHICAL THEMES ENDORSE THE LAW 95

ceeding that of the scribes and Pharisees in order to
enter the kingdom at all (Matt. 5:20). Just as Moses
delivered a divine pronouncement from the Mount,
asserting God’s standard of righteousness, so also
Jesus speaks from the mount with God’s require-
ment of righteousness, confirming every detail of
even the least commandment in the Old Testament
(Matt. 5:19). He proclaimed, “Seek first the kingdom
of God and His righteousness!” (Matt. 6:33). How is
such kingdom righteousness to be accomplished?
Jesus explained in the Lord’s prayer: when we ask
“Thy kingdom come,” we are praying “Thy will be
done on earth as it is in heaven” (Matt. 6:10). The
doing of God’s will, which Jesus found in the Old
Testament law, is crucial to the New Testament
theme of kingdom righteousness,

God is portrayed in the New Testament as a God
of righteousness (John 17:25}, and the fruit that He
brings forth in people is that of righteousness (Eph.
5:9). “If you know that He is righteous, you also
know that everyone who practices righteousness has
been begotten of Him” (1 John 2:29), and “whoso-
ever does not practice righteousness is not of God” (1
John 3:10). As Paul says, we are not to be deceived:
“the unrighteous shall not inherit the kingdom of
God,” and as examples of the unrighteous he lists
violators of God’s law (1 Cor. 6:9-10). Kingdom
righteousness, then, is demanded of all believers.
“Follow after righteousness” can serve for Paul as a
short summary of Timothy’s moral duty (1 Tim.
6:11).

But where is the character of this kingdom right-
