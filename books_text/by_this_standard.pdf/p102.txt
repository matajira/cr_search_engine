72 BY THIS STANDARD

Consequently, a truth that is dear to the heart of
every Christian is the summary provided by Paul in
Ephesians 2:8, “by grace have you been saved
through faith, and that not of yourselves, it is the gift
of God—not of works, lest any man should boast.”
Salvation is grounded in the grace of God, and the
instrumental means by which we gain it is saving
faith. The law does not save us but rather strikes us
dead (Rom. 7:9; 2 Cor. 3:6-7).

It is true, therefore, that the Christian life and
ethic should be characterized by the grace of God
and saving faith; the believer's behavior should be a
reflection of his faith in the mercy of God. The
Christian ethic ought not to stand in opposition to
salvation by grace through faith. As Paul said, “by
the grace of God we have had our behavior in the
world” (2 Cor, 1:12), and the Christian life can be
designated “the good fight of faith” (1 Tim. 6:12).
However, this does not mean that the Christian life
is one of antagonism to the law of God, as many peo-
ple seem to infer. It is too often thought that, since
the law condemns us and cannot save us, grace and
faith release us from any obligation to God’s law. A
gracious ethic of faith, we are told, cannot tolerate
rules, regulations, or commands from God that
would be “legalism,” it is said. But such thinking and
reasoning is not biblical. Such antinomian implica-
tions must be corrected by God’s word,

Law and Grace Are Correlative
God’s law defines my sin and thereby my need
for the Savior. Christ has saved me from the guilt
