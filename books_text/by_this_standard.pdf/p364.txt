334 GY THis STANDARD

deal with “religious” offenses today (whatever they
might be) is one that will need Biblical backing,
given the New Testament endorsement of the law of
God in general, as well as the New Testament doc-
trine that magistrates should enforce the law of God
(for Whom they are a “minister,” avenging wrath
against evildoers). Is blasphemy less heinous in
God's eyes today, or less destructive of social justice,
or less relevant to the concerns of “God’s minister” in
the state? It is perfectly true, as some point out, that
the “evil” which Paul says the magistrate should pun-
ish (Rom, 13:4) must be vestricted, since not all sins
are crimes. But the reasonable thing seems to be to
restrict it according to the law of God, not to make it
more restrictive than the law of God! The basic prob-
lem with most arguments against the position taken
in this book is that these arguments have no Biblical
warrant and authority. God’s people must then set
them aside as without force.

3. The “Severity” of the Law

To say that the penal sanctions of the Old Testa-
ment are “too severe” for a period of “common grace”
is to overlook at least two important points: (1) Israel
of old enjoyed Ged’s common grace (at least as
defined in Gen. 8:22), and was stil! required to en-
force His law, and (2) God’s political laws serve to
preserve the outward order and justice of a civilization
and thus are a sign of God’s “common grace” — rather
than detracting from common grace. If “common
grace” really conflicts with God’s law, then the critic
will need to demonstrate that what he means by “com-
