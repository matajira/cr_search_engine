172 BY THIS STANDARD.

The standard of the law remains unmitigated in
its demand on our behavior as God’s creatures.
Failure to comply with it makes us sinners. Christ
came, not to remove the standard which constitutes
us as sinners, but to atone for the sin which we com-
mit. The Spirit which He supplies to believers works
to bring obedience to the previously spurned stand-
ard of righteousness in the law. At the final judg-
ment, all men will be judged in the light of that same
unchanging standard. In whatever age, state, or cir-
cumstance man is found, his norm of godliness remains the
revealed law of God.

Accordingly, in 1774 John Newton, the theolo-
gian, hymn writer, and former slave ship owner
turned abolitionist, wrote: “It is an unlawful use of
the law, that is, an abuse of it, arr abuse of both law
and Gospel, to pretend, that its accomplishment by
Christ releases believers from any obligation to it as
a rule. Such an assertion is not only wicked, but ab-
surd and impossible in the highest degree: for the
law is founded in the relation between the Creator
and the creature, and must unavoidably remain in
force so long as that relation subsists. While he is
God, and we are creatures, in every possible or sup-
posable change of state or circumstances, he must
have an unrivalled claim to our reverence, love,
trust, service, and submission.”!

1. Letters of John Newton (London: Banner of Truth Trust,
1960, p. 46).
