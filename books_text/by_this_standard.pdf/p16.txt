Xv BY THIS STANOARD.

Bible, and only the Bible, Van Til answered—in
volume after erudite volume.

Dr. Bahnsen is certainly a spiritual and intellec-
tual heir of Van Til, as Van Til recognized early in
Dr. Bahnsen’s seminary career. Dr. Bahnsen is a
trained philosopher and a rigorous logician; indeed,
he writes more precisely than Van Til. There is a
price to pay for this precision, however, both for the
author and his readers. The author suffers from a
narrower market, and readers must think precisely
in order to follow the arguments. Not that many
readers are sufficiently self-disciplined to take up the
challenge.

Tt is not that Dr. Bahnsen’s exposition is difficult
to follow; it is that one must pay attention in order to
follow him. This requires previewing and reviewing.
It also requires readers to remember the outline of
the arguments that have been presented in earlier
sections. (Read and reread pages 345-47.) Dr.
Bahnsen requires of his readers the ability and will-
ingness to pay attention, not a high IQ. His glossary
provides definitions for technical terms. Use it.

His performance in this book is admittedly unex-
citing. He considers the standard arguments that
have been used against the idea of the continuing
validity of biblical law, and he exposes them, one by
one, as illogical, anti-biblical, and productive of
great harm. He shows not only that these arguments
are wrong logically but also that they are wrong
morally. He wraps his opponents in an exegetical
net. The more they struggle, the more ensnared they
become. He never names them, but you can.hear
