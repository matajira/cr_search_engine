‘LAW AND POLITICS IN THE NEW TESTAMENT 255

sake of conscience” (Rom. 13:5b). That is, out of regard
for the Lord Himself who stands over the civil magis-
trate, His deputy, the Christian must submit to the
tuler—and in so doing submit to the supreme Ruler.

Conscience

It should be obvious, despite the short-sighted-
ness of some commentators, that the submission
given to civil magistrates must be in the context of the
magistrate ministering for God, for this submission
is explicitly prescribed by Paul for conscience’ sake.
Paul frequently uses the word ‘conscience,’ meaning
conscience toward God (for example, Acts 23:1; 2 Cor.
4:2; 2 Tim. 1:3). “God alone is Lord of the con-
science and therefore to do anything out of con-
science or for conscience’ sake is to do it from a sense
of obligation to God” (John Murray, Epistle io the
Romans, vol. 2, p. 154). Moreover, Paul always qual-
ified the obedience that must be rendered to men as
obedience given for godly ends—obedience given in
the context of submitting first and foremost to the
moral demands of God Himself,

Charles Hodge expressed this insight:

In like manner, Paul enforces all relative and
social duties on religious grounds. Children are
to obey their parents, because it is right in the
sight of God; and servants are to be obedient to
their masters, as unto Christ, doing the will of
God from the heart, Eph. 6:1, 5, 6.2

2. A Commentary on Romans (London: Banner of Truth Trust,
[1835], 1972), p. 408.
