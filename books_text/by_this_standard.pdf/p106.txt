76 BY THIS STANDARD

man will yield obedience to the commands of Scrip-
ture (XIV:2). Genuine saving faith always is accom-
panied by heart-felt repentance from sin and turning
unto God, “purposing and endeavoring to walk with
Him in all the ways of His commandments” (XV:2).
We conclude; then, that the Christian's life of grace
and faith is not one which is indifferent or antagon-
istic to the law of God. God’s grace and saving faith
establish the validity of the law.

Christian Love and God’s Law

The same can be said for the basic Christian
ethic of love. Because God has shown His love to-
ward us, we are now to live in love to Him and our
neighbor (Eph. 5:1-2; 1 John.4:7-12, 16-21), On these
two love commandments — toward God and toward
our neighbor (as taught in the Old Testament [Deut.
6:5, Lev. 19:48]}— hang all the law and the prophets,
said Jesus (Matt. 22:37-40). Indeed, “love is the ful-
fillment of the law” (Rom. 13:10). But in the thinking
of Jesus and the apostles, does this mean that Chris-
tians can dispense with the law of God or repudiate
its details? Not at all. Moses had taught that loving
God meant keeping His commandments (Deut.
30:16), and as usual Jesus did not depart from
Moses: “If you love me, you will keep my command-
ments” (John 14:15).

The love which summarizes and epitomizes
Christian ethics is not a vague generality or feeling
that tolerates, for instance, everything from adultery
to chastity. John wrote: “Hereby we know that we
love the children of God, when we love God and do
