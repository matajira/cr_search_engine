306 BY THIS STANDARD

Instead of something being affirmed and then de-
nied, we have something denied twice in a row: “Think
not that I came to abrogate the Law or the Prophets;
T came not to abrogate.” This is not a paradoxical in-
troduction but a downright emphatic denial of some-
thing! Matthew 5:17, along with the vast majority of
instances of “not this, but that” statements in Mat-
thew’s gospel, expresses strong contrast or antithesis,
not relative negation.

Others who oppose the general validity of the law
in the New Testament might hope to come to terms
with Matthew 5:17-19 by arguing that the subor-
dinate clause “until all comes to pass” in verse 18
limits the validity of the law to the obedient ministry
of Jesus Christ on earth. To do so, they have to read
a great deal into a very colorless phrase with little
distinctive character; the phrase in Greek says little
more than “until everything happens.” The structure
of the verse seems to make this phrase parallel to one
which went before, one which specifically stated “un-
til heaven and earth pass away.” The interpretation
before us, then, would make the verse self-
contradictory by saying that the law was both valid
until the end of the world and valid undl Jesus had
kept it all (in which case it is now both set aside and
not set aside). Besides, this interpretation takes “all”
in the phrase “until all things happen” as referring to
all of the “jots and tittles” of the law mentioned in
verse 18. But this is grammatically incorrect, seeing
that “all” and “jot and tittle” do not agree in gender or
number according to the Greek text.

‘There appears to be no escape from the thrust of
