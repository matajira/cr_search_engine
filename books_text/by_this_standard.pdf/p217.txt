WHAT THE LAW CANNOT DO 187

anything perfect (Heb. 7:19). While it beautifully
foreshadowed the saving ministry of Jesus Christ in
its ceremonial enactments, the law could never by its
repeated sacrifices secure the eternal redemption
needed by God’s people (Heb, 9:11-12; 10:1-12). Only
the coming of the promised Savior, His atoning
death, and justifying resurrection could accomplish
the hoped for salvation of believers. The law could
not accomplish the remission of sins but only witness
to its coming reality. Accordingly, the ceremonial
portion of the Old Testament law was never meant
to be literally followed forever in the same manner as
it was by Old Testament saints. It was “imposed un-
til a time of reformation” (Heb. 9:10).

With the coming of the Savior, the shadows are
left behind. The ceremonial system is put out of gear
and made inoperative. To insist on keeping these or-
dinances in the same way as did Old Testament be-
lievers would be to disclose in oneself a legalistic atti-
tude toward salvation (Gal, 4:8-10; 5:1-6), It would
be retrogressive and disdainful of Christ, to whom
the Old Testament ceremonies pointed.

“Under Law”

In 1 Corinthians 9:20, Paul describes himself as
“not being myself under the law,” even though he
became to the Jews as one who was under the law in
order that he might win some Jews to Christ. In the
next verse, he continues to describe himself, now as
“not being without law to God, but under law to
Christ.” If nothing else, this verse refutes any idea
that Romans 6:14 (“you are not under law, but under
