We have no photographs of starving children, no orphan-
ages in Asia. We generate ideas. There is always a very
limited market for ideas, which is why some of them have to be
subsidized by people who iunderstand the power of ideas — a
limited group, to be sure. John Maynard Keynes, the most
influential economist of this century (which speaks poorly
of this century), spoke the truth in the final paragraph of
his General Theory of Employment, Interest, and Money (1936):

. .. the ideas of economists and political philoso-
phers, both when they are right and when they are
wrong, are more powerful than is commonly under-
stood. Indeed, the world is ruted by little else. Practi-
cal men, who believe themselves to be quite exempt
from any intellectual influences, are usuaily the
slaves of some defunct economist. Madmen in
authority, who hear voices isr the air, are distilling
their frenzy from some academic scribbier of a few
years back. I am sure that the power of vested inter-
ests is vastly exaggerated compared with the gradual
encroachment of ideas. Not, indeed, immediately,
but after a certain interval; for in the field of
economic and political philosophy there are not
many who are influenced by new theories after they
are twenty five or thirty years of age, so that the ideas
which civil servants and politicians and even agitators
apply to current events are not likely to be the
newest. But, soon or late, it is ideas, not vested
interests, which are dangerous for good or evil,

Do you believe this? If so, then the program of long-
term education which the ICE has created should be of
considerable interest to you. What we need are people
with a vested interest in ideas, a commitment to principle rather
than class position.
