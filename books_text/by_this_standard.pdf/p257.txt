LAW AND POLITICS IN OLD TESTAMENT ISRAEL 227

are called “priests.” The same Hebrew word for the
cultic office of priest was used of these political rulers
—even as it was applied in similar fashion to David’s
officer, Ira the Jairite (1 Sam. 20:26; cf. 2 Sam.
23:38). In 1 Kings 4:2-6 we find a list of Solomon’s
officers, where Zabud is called a “priest,” and the text
immediately explains this office as “the king’s friend”
(his continual adviser). The head of the political
“priests”—the priest (or prime administrator of the
kingdom)—is named as Azariah in the same
passage.

What we learn, then, is that the rulers of state in
the Old Testament were viewed as so intimately con-
cerned with the affairs of God’s word and so strictly
subject to His command, that they could be given
customary religious titles. The magistrates in Israel
were genuine ministers of God, authorized to rule
according to His just standards as his representatives
in society.

Old Testament civil rulers were ordained by
God, were not to be resisted, and bore religious titles
as the representatives of God in society. Their main
function was that of avenging God’s wrath against
violators of His law for social justice.

Over and over again the Old Testament associ-
ated the sword of judgment with God, who brought
historical punishment upon the rebellion of men.
Even Israel was threatened with the judgment of the
sword if she broke the law of the Lord (for example,
Lev. 26:25, 33, 36-37)—a threat carried out in its
climax when Jerusalem fell by the edge of the sword
according to the word of Christ (Luke 21:24). The
