LAW AND POLITICS IN THE NEW TESTAMENT 257

brute power, the Christian’s “conscience before the
Lord” cannot go along with them.

Since the Lord is the supreme Judge, the Chris-
tian must noé resist those who are appointed by God
and minister for Him. For the same reason, the sub-
mission given to rulers by the Christian is qualified by
his primary allegiance to the Lord, and by the un-
derstanding that submission to the state is for the
sake of the Lord, whose will the magistrate ought to
pursue.

2. Bearing religious titles, rulers are avengers of divine
wrath.

The supremacy of God as the preconditioning as-
sumption of Romans 13:1-7 comes to expression in
the titles assigned to civil rulers by Paul. In Old Tes-
tament Israel statesmen were sometimes designated
“priests,” and even in the Gentile nations around
Israel civil leaders were occasionally called by God
“My servant,” “My shepherd,” and “My annointed
(Christ).” This tendency to see the office bearer in
the state categorized as a religious official ~ someone
answerable to God Almighty ~carries over into the
New Testament, once again demonstrating the con-
tinuity which exists between the Old and New Testa-
ment regarding the powers that be.

‘The idea of a secular state, one which divorces its
authority and standards from religious considera-
tions about God and His will, is completely alien to
Biblical revelation. Indeed, it was alien to much of
the ancient world in general. All politics is the ex-
pression of a moral point of view, which in turn is the
