298 BY THIS STANDARD

ards for even the most specific aspects of living. Auton-
omous reasoning may reject our endorsement of the
law of God for ethics, but autonomous ethics has noth-
ing finally to offer in its place. Autonomy spells the
death of an absolute and relevant ethical standard.

Varieties of Antinomianism

The opponents of God's law in Christian ethics
are not restricted to the world of unbelieving
thought, and so we must continue our survey of an-
tagonism to the perspective advanced in these
studies. Many believing Christians would likewise re-
ject the idea that the law of God is now normative for
ethics. They would in one way or another, to one
degree or another, and for one reason or another,
repudiate the binding authority of the revealed com-
mandments of God. Those who do this are generally
known as “antinomians” because they are against
(‘anti-”) the law (“nomos”), although we must
carefully recognize that a wide variety of different at-
titudes (not all sharing the same problems) fall under
this label. We need to draw distinctions.

Licentious antinomianism ~ the most serious form
of antinomianism— maintains that since we have
been saved by grace, apart from works of the law, we
have been set free from the need to observe any
moral code whatsoever. Laws or rules have no place
in the Christian life, and thus in principle the door is
open to complete license in the way a believer lives.
Such thinking hardly squares with New Testament
teaching, Paul not only insisted that salvation was
not 4y works, he also went on to say salvation is for
