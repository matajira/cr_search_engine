36 BY THIS STANDARD

face of transgressions would be a guilty silence.
Christ directed His followers that they were to be
“the light of the world”—which is impossible if our
light is placed under a basket (Matt. 5:14-15). Conse-
quently, true Christian obedience to the law of God
will take us beyond a concern for ourselves to a con-
cern for the obedience of those around us. Churches
which preach (either intentionally or by default)
“moral individualism” are failing to proclaim the
whole counsel of God. The sins of our society cannot
be ignored or swept under the church carpet.

This short study does not by any means touch
upon every facet of obedience to God’s command-
ments, but it does point out two very important
aspects of genuine obedience. We see how far-reaching
God’s demands are when we keep in mind that obe-
dience must be from the heart, and yet that obe-
dience must not be restricted to the heart.
