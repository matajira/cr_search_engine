13

NEW TESTAMENT MORAL
JUDGMENTS ENDORSE THE LAW

“The attempt made by some Christian teachers
today to reject or circumscribe the authority of
the Old Testament law will over and over again
meet with embarrassment before the text of the
New Testament.”

The Old Testament law of God gives definitive
substance to many of the central themes of New Tes-
tament ethics—as we have illustrated before. When
we ask what it means to follow the will of God or to
be holy, as the New Testament requires, we find that
the law of God defines these ethical themes. Like-
wise, the law of God is assumed in notions like king-
dom righteousness or the golden rule. The Jaw func-
tions as a standard and a guide when we heed New
Testament exhortations to attain the stature of
Christ or demonstrate the fruit of the Spirit. New
Testament ethical themes quite often take for
