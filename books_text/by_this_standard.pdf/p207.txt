18

NEW TESTAMENT OPPOSITION TO
THE ABUSE OF GOD'S LAW

“Pauls words imply that there Is an untawful
use of God's law, a use which runs counter to
the law’s character and intent, so that the law's
good nature might be perverted into something
evil.”

The New Testament, as does the entire Bible,
surely supports the continuing validity of God’s law.
To say this is simply to submit one’s thoughts to the
Lawgiver Himself—it is not “legalism.” And yet the
New Testament contains passages which certainly
seem to be taking a decidedly negative attitude to-
ward the law of God. Paul declares that he “died unto
the law that I might live unto God” (Gal. 2:19). He
says, “you are not under the law, but under grace”
(Rom. 6:14). Again, “we have been discharged from
the law” (Rom. 7:6). For those who believe, we can
conclude apparently, “Christ is the end of the law”
