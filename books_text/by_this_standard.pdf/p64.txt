34 BY THIS STANDARD

walk not aftcr the flesh but after the Spirit” (v. 4).

Those who have hearts made right with God,
those who have been given a new heart by God,
those who wish from the heart to please God, will
seck to walk according to God’s commandments
(Jer. 31:33; Ezk. 11:19-20; 36:26-27). A proper heart
attitude should lead to proper outward conduct as
well. Obedience cannot be restricted to the heart.
Jesus not only wanted the Pharisees to realize the in-
ward values of mercy and faith; He did not want
them to leave undone the minor outward matters of
tithing garden vegetables (Matt. 23:23).

Just as obedience cannot be restricted to the
heart in the sense of forgetting the need for outward
conformity to God's stipulations, it can likewise be
said that obedience —if it is genuine Biblical obedi-
ence — cannot be restricted to a concern for our own
personal conduct. Full obedience embraces an inter-
est in the obedience of those around me to the laws of
God. The Christian must assume the responsibility
to exhort those in his home, church, society, etc. to
keep the commandments of the Lord, David wrote,
“restore unto me the joy of thy salvation, and uphold
me with thy free Spirit. Then will I teach transgressors
thy ways, and sinners will be converted unto thee”
(Ps. 51:13). The Great Commission laid upon the
church by Christ calls for us to teach the nations
whatsoever Christ has commanded (Matt. 28:18-20).
Anything less than this concern for the obedience of
those around us is disloyalty to the Lord and fails to
qualify as true obedience to his Jaw. John Murray
wrote:
