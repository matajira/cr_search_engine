A CONSEQUENTIAL ETHIC ENDORSES THE Law 79

person’s behavior as counting the most in ethical
planning and evaluating; if the effects which come
from some action (or the anticipated results) are
beneficial (or more beneficial than alternatives),
then the action is deemed morally good and accep-
table. In summary we can cail these the normative,
motivational, and consequential approaches to
ethics. (Sometimes the technical designations are
rendered as the deontological, existential, and
teleological approaches to ethics.)

Now then, the Bible has a focus on ethics from begin-
ning to end, this interest is expressed along the lines of
all three of the ethical perspectives we have just out-
lined. That is, the Bible looks to the standard which
we are to follow, encourages a certain kind of
character and motivation in us, and sets before us
goals or consequences we should pursue.

The zormative and motivational perspectives have
been somewhat explored already. We have seen that
God has lovingly and graciously set down in His in-
spired word a code of moral behavior for His crea-
tures to follow; the commandments or law of God
constitute the norm of ethics for all men, whether
they accept it or not. God’s law is found throughout
the Bible and is fully valid as a standard of morality
today. This is a uniform standard, binding all men
in all ages, for it reflects the unchanging holiness of
God. It was this law which Christ perfectly obeyed
as our Savior, thereby leaving us an example to fol-
low, and it is this law which the Holy Spirit fulfills in
us by sanctifying us daily. Thus the Bible gives us
the law of God as our normative approach to moral-
