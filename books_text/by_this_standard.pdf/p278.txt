248 BY THIS STANDARD

state. On the one hand we have Bible interpreters
who contend that Romans 13 should be read descrip-
tively, thus laying nearly exclusive stress upon Paul's
practical exhortation to Christians. That is, when
Paul says that the civil magistrate “is a minister of
God, an avenger of wrath to evildoers” (v. 4), some
interpreters take him to be giving an actual descrip-
tion of all earthly rulers in their real character and
function. All statesmen would then be described as
God’s ministers who avenge wrath on the evil ele-
ment of society — regardless of the actual quality and
conduct of the particular ruler one may have in
mind. Even Hitler and Idi Amin would be described
as genuine ministers of God. In that case, Paul’s
practical thrust in Romans 13 would simply be to in-
struct believers that they must submit obediently to
whatever magistrate God has placed over them in
society (with the proviso, of course, that they cannot
obey men when human rulers order them to disobey
God: Acts 5:29).

On the other hand we have Bible interpreters
who argue that Romans 13 should be read prescrip-
tively, thus emphasizing that Paul was giving the
moral standard for civil magistrates and thereby in-
dicating which rules were to be given submissive obe-
dience by the Christian. That is, when Paul says that
the magistrate is “a minister of God, an avenger of
wrath to evildoers” (v. 4), some interpreters see him
as laying down a moral prescription for civil rulers—
telling them what they ought to be. Magistrates are
to be ministers of God who avenge wrath on evil-
doers. Consequently, the prescriptive approach to
