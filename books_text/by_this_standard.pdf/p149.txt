NEW TESTAMENT MORAL JUDGMENTS ENDORSE THE LAW 119

granted the validity of God’s Old Testarnent com-
mandments.

The complete, continuous, and thus contemporary
validity of the Old Testament law which is assumed
without challenge in many themes of New Testament
ethics is brought out explicitly in moral judgments which
fill the pages of the New Testament. In particular
circumstances, when some kind of moral evaluation,
direction, or exhortation is called for, New Testa-
ment preachers and writers often show that they
stand firmly on the Old Testament law in making
their judgments. They treat and utilize the standing
rules of ethics as found in the Old Testament as
though these rules were meant for them to keep—
even though these rules were given a great many
years earlier, before the advent of Christ our Savior.
Particular instances of ethical decision-making in the
New Testament illustrate once again that the com-
mandments of God found in the Old Testament have
not been discarded, repudiated, or ignored as some-
how no longer authoritative and valid.

Use and Validity

Imagine that you wake up some morning to an
exasperating problem: the plumbing under the kit-
chen sink needs repair, and a pool of water sits on
the floor. After you mop up the mess, you stop and
take thought as to what should be done to solve your
plumbing problem. You think about calling a
plumber, but reject that plan as too expensive and
perhaps unnecessary. Upon reflection, you come to
believe that you might very well be able to repair the
