330 GY THis STANDARD

criminal offenders), even as New Testament magis-
trates must also deal with the criminal problem of
rape, The extraneous relation of these magistrates to
priests (or to the church) is not pertinent to their
relation to the criminal, nor does it affect what
justice demands in the case of crime; the church-
state question is really to the side.

The common claim that the religious and the
civil aspects of community life were fused in Old
Testament Israel simply will not square with a
reading of the Old Testament text, as previous
chapters have pointed out. This is not to say or
claim, as some critics have thought, that the church-
state relation in the Old Testament is identical in
every respect with the church-state relation in the
New; such a premise is not indispensable to the posi-
tion taken herein. The position does stand opposed
to the inaccurate argumentation often heard, which
says that there was no separation of church and state in
Israel. The Old Testament cult was clearly a
separate authority and function from the Old Testa-
ment civil rule. (This observation, it must be ex-
plained to some critics, does not imply that the Old
Testament cult is taken as wholly identical with the
New Testament church; there és a parallel or analogy
however, as Paul indicates in 1 Cor, 9:13-14.) Kings
could not sacrifice, and priests could not execute, in
the Old Testament situation; the state and the
church had separate functions and directions.

Nevertheless, some writers have believed that
there are significant (morally significant?)
differences between our situation today and the
