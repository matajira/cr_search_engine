‘THE NEW TESTAMENT EXPLICITLY SUPPORTS THE LAW 87

approach to ethics, he is always brought to the same
conclusion: God's law is authoritative for contemporary
ethics.

The norm which God has given to direct our
lives and to define our sin is revealed in His law, a
law from which we are to subtract nothing; since the
Lawgiver has not altered His law—indeed, the Son
of God has confirmed that law for His followers —it
must remain valid for us today.

if we turn to the motivational approach to ethics,
our concern will be to live in a way appropriate to
our gracious salvation, we wili want to be the kind of
people who are characterized by faith and love.
Scripture shows us that those who are grateful for
God’s grace will strive to live in obedience to His
commandments; rather than cancelling the com-
mandments of God in ethics, faith establishes the
law, and love is a summary of the law's re-
quirements. So then, a motivational approach to
ethics—like the normative approach—declares the
current validity of God’s law.

Finally, the consequential approach to ethics
evaluates actions and attitudes according to their
beneficial results or comparative lack thereof. Christ
teaches us in his word that the primary goal of our
moral behavior is the kingdom of God; when we
make it that, every temporal and eternal blessing
will be ours. The righteousness of this kingdom is
defined by the law of the King, and thus Scripture
promises that obedience to the law of God will even-
tuate in outstanding blessing for ourselves, our
neighbors, and our society. In short, the law of God
