368. by nes cranDARD

power, 159-62

promises, 37

righteousness, 95-99

rulers, 247

rules, 122

silence, 336

standard, 168

writers, 123-24, 133, 324
Newton, John, 192-93, 198
Noah, 140
norm, 78, 84, 87, 102, 169-70

obedience
cultural, 34
heart, 29-33
kingdom of God, 193
Old Testament era, 160-61
outward, 33-36
perfect, 47
specific, 107
whole Bible, 25, 343

Old Testament
abrogated?, 26-27, 43, 90
administration, 314
church & state, 286-90,

330-31

confidence, 157
continuity, 2
covenants, 40-41
death, 155
discontinuity, 4-6
ethics, 25-49
fellowship, 105
grace, 41-42
holiness, 102

love, 76

Jesus &, 3

Paul on, 88

sanctions, 271

shadows, 136, 162-68

standard, 36
ordinances, 316

Pannenberg, W., 18
parents, 234
passover, 288
Paul, 41, 67, 73, 81, 92, 105
180-81, 267
penal sanctions, 11-12, 92
perfection, 47, 48, 1-112
Peter, 102
Pharach, 240
Pharisees, 8, 30-32, 127, 179
305
pietism, xiii
Pilate, 252
pipers, xxiv-xxv
plate glass, 195
Plato, 295-98
plumbing, 119-20
pluralism, 14, 340
politics, 211-12
Israel, 222-32
Israe!’s administration,
323
morality, 267-68
political ethics, 1
Pope, 16
polytheism, 340
power, 68, 159-62, 185, 186
