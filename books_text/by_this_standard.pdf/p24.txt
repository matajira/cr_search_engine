yoaiv BY THIS STANDARD

submission by state-licensed, profit-seeking medical
psychopaths who tell us that abortion is a morally
valid way to control population growth and to solve
marital and financial difficulties. A renewed interest
in biblical law will “unhumble” Christians soon
enough. It already has.

People may ask: Wouldn't biblical law lead to
tyranny? I answer: Why should it? God designed it.
God mandated it. Was Israel a tyranny? Or was
Egypt the real tyranny, and Babylon? Tyranny was
what God visited upon His people when they turned their
backs on biblical law.

But to be practical about it, I cannot imagine a
successful modern tyranny that is financed by less
than ten percent of national income. J can easily im-
agine many tyrannies that are coercively financed by
five to seven times the tithe. So can you. In this
bloody humanist century, this takes very little im-
agination. A history book is all it takes. Or a
subscription to the New York Times.

Pipers and Tunes

He who pays the piper calls the tune. The hu-
manists have taxed our money away from us in
order to hire pipers to play their tunes. But they
weren't satisfied with direct taxation; they debased
the money, and the pipers are in revolt. Now they
are borrowing the money (with the “full faith and
credit” of the federal government) to keep the pipers
playing, but when those who lend the money finally
run out of patience and faith, the piper-payers will
be in big, big trouble. So will their pipers.
