CRIME AND PUNISHMENT 278

which they address. Consequently, if a magistrate
departs from the strict justice and equity of the Bibli-
cally prescribed penalties for crimes, then the magis-
trate must either require more or require /ess than the
law of God, Either way he will depart from the norm
of equity—meting out what a crime deserves— and
thus will be uajust in his judgments, being either too
hard or too easy on criminals. Hebrews 2:2 tells us,
contrary to the mistaken assumption of many, that
the Old Testament penal sanctions were not “height-
ened” or “intensified” punishments, going beyond
what strict justice for society would dictate. The
verse declares, as foundational to an a fortiori argu-
ment for the eternal justice of God toward apostates,
that according to the Mosaic law (“the word spoken
through angels,” cf. Acts 7:53) “every transgression
and offense received a just recompense of reward.”
God's penalties were not overbearing there, and thus
His judgment must be seen as fair toward apostates
as well. God never punishes in an unjust manner,
one that is too lenient or too harsh; He always
prescribes exactly what equity demands. He can be
counted on to stipulate a just recompense of reward for
every crime. Those who depart from God’s penal
sanctions, then, are the ones who are unjust.

If God says that some crime is to be punished by
the magistrate with death, then the crime in question
is indeed “worthy of death,” to use the Biblical phrase
(for example, Deut. 21:22). One of the strongest en-
dorsements of the justice of the law’s penal sanctions
is found in the words of the Apostle Paul at Acts
25:11. When he was accused of many grievious
