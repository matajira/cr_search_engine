12° BY THIS STANDARD

that capital punishment is the most significant topic
in Christian ethics or even in Christian social ethics.

By taking up a study of the Mosaic law and the
validity of its penal sanctions we are simply pointing
out that these are aspects of Biblical teaching~
indeed aspects which serve a beneficial purpose and
as such are included in God’s revealed word—and
should not be misunderstood or ignored in deciding
what the whole Bible has to say to us about our lives,
conduct, and attitudes. By paying attention to the
question of God’s law in Christian ethics we are sim-
ply being consistent with the Reformed conviction
that our Christian beliefs should be guided by sola
Scriptura and tota Scriptura—only by Scripture and by
all of Scripture.
