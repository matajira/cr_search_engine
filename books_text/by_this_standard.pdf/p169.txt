E. SUMMARY OF OLD AND NEW
TESTAMENT VIEWS OF GOD'S LAW

15

CONTINUITY BETWEEN THE
COVENANTS ON THE LAW

“God's eternal and righteous law is unalterable,
according to the joint teaching of the Old and
New Testaments.”

The purpose of the next two chapters will simply
be to compare and contrast the outlook on the law of
God which we find in the Old and New Testaments.
Granted, there are many ways to summarize the
theology of law in either testament; the present is
only one among many. However it hopefully serves a
useful purpose: that of stressing the continuity be-
tween Old and New Testaments regarding God’s
law—over against contrary misconceptions fostered
by some teachers— and of indicating salient points of
discontinuity—over against the baseless fears of
some that those who acknowledge the continuing
validity of God’s law today suppress or ignore impor-
tant differences.
