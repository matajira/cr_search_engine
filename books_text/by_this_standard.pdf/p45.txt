GOD'S WORD IS OUR NORM 15

persons, actions, or attitudes are morally good, then
we will need a standard here as well. Otherwise we
will lead crooked lives and make inaccurate evalua-
tions. What should our ethical standard be? What
yardstick should we use in making decisions, culti-
vating attitudes, or setting goals for ourselves and
the groups in which we move? How does one know
and test what is right and wrong?

“Yardsticks” for Civilization

In ancient Greece and Rome the city or state was
taken as the ultimate authority and yardstick in eth-
ics. Caesar was lord over all when moral questions
were raised. Over against the totalitarian, divinized
state the early church proclaimed the Lordship of
Jesus Christ. The ‘ruling authorities” (Rom. 13:1)
were told that “ali authority in heaven and earth” re-
sided in the resurrected Messiah (Matt. 28:18). Ac-
cordingly the apostle John portrayed the political
“beast” of Revelation 13 as requiring that his own
name be written on men’s foreheads and hands (wy.
16-17), thereby symbolizing that the state’s law had
replaced the law of God, which was to be written on
the forehead and hand (cf. 6:8). That is why those
who stand in opposition to the beast are described as
“those who keep the commandments of God and the
faith of Jesus” (Rev. 14:1, 12). God’s people insist that
the state does not have ultimate ethical authority, for
God’s law is the supreme standard of right and
wrong.

The medieval church, however, came to foster
two yardsticks of ethics: a standard for religious
