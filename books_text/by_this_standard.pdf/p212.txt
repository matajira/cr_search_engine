182 BY THIS STANDARD

were in truth the most extreme violators of the law!
“Is the law against the promises of God?” Paul asks.
Does it teach a method of justification contrary to the
gracious way of salvation found in God’s promises?
Paul’s reply is “May it never be!” (Gal. 3:21), “for if
there had been a law given which could make alive,
verily righteousness would have been of the law. But
Scripture shut up all things under sin in order that
the promise by faith in Jesus Christ might be given
to them that believe.” Far from distracting from jus-
tification by grace through faith, “the law became
our tutor to bring us unto Christ, that we might be
justified by faith” (v. 24).

So let us return to Paul’s declaration in 1 Tim.
4:8, “We know that the law is good, if a man use it
lawfully.” By implication there is an unlawful, distorting
use of the law —one which abuses it, even while pre-
tending to honor the law. Paul would surely identify
the abusive use of the law as the Pharisaical and
Judaizing attempt to make law-works the ground of
one’s own justification before God. “If righteousness
is through the law, then Christ died for nothing”
(Gal. 2:21), But “no man is justified by the law” (Gal.
3:11). The fact that God justifies the ungodly (Rom.
4:5) plainly shows that justification must be grounded
in the alien righteousness of Jesus Christ (by His shed
blood and resurrection, Rom. 4:25; 5:9); His right-
eousness is imputed to those who believe upon Him
(Rom, 4:3-5; 5:1-2; 2 Cor. 5:21). Indeed, the aim or
goal (“end”) of the law’s teaching was Christ, who
brings righteousness to all who believe (Rom. 10:4).
