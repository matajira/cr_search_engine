LAW AND POLITICS IN THE NEW TESTAMENT 269

2. Bearing religious titles, rulers are aven-
gers of divine wrath.

3. So rulers must deter evil by ruling ac-
cording to God’s law.

This provides us with a foundation for Christian
involvement in political philosophy and practice.
From this platform a distinctive contribution can be
made.
