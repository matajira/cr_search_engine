xxi BY THiS STANDARD

losophy of “anything is politically acceptable except
the Old Testament’? I believe that one reason above
all is at the root of the problem: Christians have been
afraid to exercise dominion. They have been bullied into
submission by professional humanist guilt-manipu-
Jators who have persuaded Christians that Christi-
anity, when applied to politics, has fed to tyranny
and war, As an example, they cite the 800-year-old
story of the medieval crusades, where a few thou-
sand professional soldiers went off to fight the Mus-
lims. And who is complaining loudly today about
the evil Crusades? Defenders of humanism whose
various representatives have launched twentieth-
century wars and revolutions in which as many as
150 million people died from 1901 until 1970.3
These same critics have complained repeatedly
about the Roman Catholic Church's burning of the
occult magician Brunot or Calvin's approval of the
burning of unitarian Servetus (with the enthusiastic
approval of the Catholics, who were also after him,
and whe tipped Calvin off when Servetus came into
Geneva), four centuries ago. Compare these two
events with the atrocities of Stalin, who killed 20 to
30 million Russians in his purges in the 1930's, in-
cluding a million Communist Party members,> plus

3, Gil Eliot, Tiventieth Century Book of the Dead (New York:
Scribners, 1972).

4, That Bruno was an occultist rather than a scientist is
proven conchusively in Miss Frances A. Yates’ Giordano Bruno and
the Hermetic Tradition (New York: Vintage, [1964] 1969).

5. Robert Conquest, The Great Terror: Stalin's Purges of the Thir-
ties (New York: Collier, [1968] 1973), p. 710.
