32 BY THIS STANDARD

said by those of old... , but I say unto you.” In
such sayings Jesus was not personally dissenting
from the taw of God but from the Pharisaical under-
standing and undervaluating of the law of Gad.

After alt, if the Pharisees really were living up to
the law, and Jesus added to the law’s demand, then
His ex post facto condemnation of the Pharisees for
not living up to His additions would be quite unfair!
Rather, Jesus indicted the Pharisees for not living up
to what God originally required. “You have heard it
said by those of old” refers to the rabbinic interpreta-
tions of the law passed down from one generation to
another; the scribes commonly appealed to the tradi-
tional interpretations of the ancient rabbis as a way
of teaching the law. The amazing thing to the crowds
who heard Jesus, though, was that he taught as one
having authority in Himself, and not as one of the
scribes, always appealing to others (Matt. 7:28-29).

The problem with the scribal or Pharisaical un-
derstanding of the Old Testament law was that it
was trite and externalistic. Jesus had to point out, in
accord with Old Testament teaching (for example,
Prov. 6:16-18, 25), that hatred and lust were the root
sins of murder and adultery (Matt. 5:21-30). When
God commanded that His people not kill and not
commit adultery, He did not merely require abstain-
ing from the outward acts of assault and fornication;
His requirement went to the heart, requiring that
our thoughts, plans, and attitudes be free from
violence and unchastity as well.

True obedience to the law, then, stems from a
heart that is right with God, a heart that seeks to
