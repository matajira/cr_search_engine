‘NEW TESTAMENT ETHICAL THEMES ENDORSE THE LAW 117

popular and pervasive summary of New Testament
living known as the “golden rule”—or whatever you
would have men do to you, do even so unto them —is
presented as morally authoritative by Christ just
because “this is the law and the prophets” (Matt. 7:12).
The golden rule communicates the essential demand
of the law of the Old Testament, and as such it is a
standard of ethics which we must respect. Thus we
observe that the most common summaries of New
Testament morality ~ whether love, the fruit of the
Spirit, or the golden rule—detive their importance
and binding character from the law of God which
they express. The presupposition of the New Testa-
ment authors is continually and consistently that the
Old Testament law is valid today.

Conclusion

Any attempt to speak of New Testament ethics
apart from kingdom righteousness, or the holiness of
Christ’s saints and their separation from the world,
or the good, well-pleasing, perfect will of God, or the
stature of Christ, or resurrection life, or Spiritual
freedom, or love, or the fruit of the Spirit, or the
golden rule, is bound to be inadequate. And any at-
tempt to understand these concepts apart from the
Old Testament law is bound to be inaccurate.
