116 By THIS STANDARD

and neighbor were crucial because “On these two
commands hang the whole law and prophets” (v. 40).

Love is a moral necessity for Paul precisely be-
cause it fulfills the law (Rom. 13:8-10; Gal. 5:14).
Love for your neighbor means that you do not com-
mit adultery with his wife, steal his car, or slander
him behind his back —just as the law requires. Like-
wise, James considers love the fulfillment of the
royal law (2:8), and John specifically writes, “This is
the love of Gad, that we keep His commandments” (1
John 5:3). The assumption of the New Testament
writers and the development of their thought is that
God’s law is morally authoritative; because love expresses
and follows that law, love too is a fitting standard of
moral guidance. The foundational authority of love
cannot be isolated from the law of God.

The Fruit of the Spirit and the Golden Rule

The same can be said for other New Testament
summaries of our moral duty. A prominent pattern
of godly living is set forth by Paul in the list of “the
fruit of the Spirit,” which Paul sets over against the
fruit of the sinful nature (or flesh) in Galatians
5:16-24. The attitudes or character traits mentioned
by Paul as the outcome of the Spirit’s work in a
believer's life (“love, joy, peace . . .”) are a model for
Christian morality. Yet Paul makes it clear that the
ethical authority of these traits rests on the underly-
ing authority of God’s law. Having listed the Spirit’s
fruit, Paul explains why these traits are so important
in Christian ethics: “. . , against such there is no
law” (v. 23). In the same way we can observe that the
