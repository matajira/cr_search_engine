302 BY THIS STANDARD

We cannot subtract from God’s law, then, without
His authorization (Deut. 4:2).

Over against the unbelieving attitude of auto-
nomy, these studies have promoted theonomy (God's
law). Instead of being antinomian (in either licen-
tious, Spiritual, dispensational, or latent ways), they
have taken a pronomian stand. In ethics we presume
that God’s law from the Old Testament remains nor-
mative for conduct until the Lawgiver reveals other-
wise. Self-law and opposition to God’s law are both
incompatible with genuine ethical theory and prac-
tice.
