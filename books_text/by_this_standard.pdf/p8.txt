D. Old Testament Law in the New Testament Age

it.

12.

13.

14.

The New Testament Explicitly

Supports the Law ........-....2.4.045 85
New Testament Ethical Themes
Endorse the Law ...........0 0000s eee 93
New Testament Moral Judgments
Endorse the Law.......--0.0.00ee0 ee 118
The Categories of God’s Law... ........- 132

E. Summary of Old and New Testament
Views on God's Law

15,

16.

Continuity Between the Covenants

on the Law... 06... c cece eee eee 139
Discontinuity Between the Covenants
of the Law ......... 0.262020 eee eee 154

F. The Functions of God’s Law

17.
18.
19.

20.
21.

God’s Commandments are a Non-

 

Legalistic Rule of Obedience.......... 169
New Testament Opposition to the

Abuse of God's Law ..............00- 177
What the Law Cannot Do ..... . 184
What the Law Can and Should Do....... 190
The Traditional “Three Uses” of

the Law... 20.0... eee eee eee 201

Part Il; APPLICATION OF GOD'S

22.

23.

LAW TO POLITICAL ETHICS
The Political Implications of the
Comprehensive Gospel .............- 210
Law and Politics in Old Testament
Israel... ee eee ees 222
