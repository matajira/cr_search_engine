ARGUMENTS AGAINST THE LAWS POLITICAL USE 325

Testament’s apparent practice of taking the stand-
ards of political ethics from God’s law—and asked
what the New Testament standard for political jus-
tice is, if not God’s commandments. Those who re-
strict the validity of the Old Testament law to Israel
may not realize it, but their philosophic outlook is
that of “cultural relativism,” where what counts as
justice is adjusted from culture to culture.

Those who press the argument that modern
states are not bound to the civil aspects of God’s law
since it was given in a national and redemptive cove-
nant with Israel, will find that they cannot long
maintain with consistency any of the Old Testament
commandments today. Not only were the civil
aspects of the law revealed in the same context of a
national covenant, so also were the personal and in-
terpersonal aspects of the law. If the passing away of
the national covenant means the invalidation of
those moral standards revealed within it, then we
would lose even the ten commandments! If the judi-
cial laws of the Old Testament are thought to have
expired when God’s purposes for the Jewish nation
were complete — that is, if only the “national” aspects
of the national covenant have passed away ~ then we
would be overlooking the justice of those laws and
their full purpose, which included of being a model to
other nations (Deut. 4:6-8). Besides, God’s word never
draws such a distinction between the “personal”
aspects of the law and the “political” aspects, as
though the one were any more or less a reflection of
God's unchanging holiness than the other. Who are
we to draw such a distinction on our own, with the aim
