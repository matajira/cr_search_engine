There will be few short-term, visible successes for the
ICE's program. There will be new and interesting books.
There will be a constant stream of newsletters. But the
world is not likely to beat a path to ICE’s door, as long as
today’s policies of high taxes and statism have not yet
produced a catastrophe. We are investing in the future,
for the far side of humanism’s economic failure. This is a
long-term investment in intellectual capital. Contact us at: ICE,
Box 8000, Tyler, TX 75711. You can reach our office by
phone at 903-839-8822 (hours are 9:00-5:00 CST) or by
sending us a fax at 903-839-0642. E-mail can be sent to
icetyler@juno.com
