ARGUMENTS AGAINST THE LAW'S POLITICAL USE 327

4, Multiple Moral Standards

Some who criticize the perspective taken in this
book say that magistrates (past or present) who are
outside of Israel’s “theocracy” should rule according
to the moral standards of general revelation, not
those of God’s law. The faulty assumption here, of
course, is that God has two moral standards, one re-
vealed through nature and conscience and a differ-
ent one revealed in the Bible. The Biblical perspec-
tive is that the law revealed to the Jews in spoken
form has been revealed in unspoken form to the
Gentiles, and the two moral codes are co-extensive.
Paul did not somehow restrict natural revelation to
the Decalogue (see, for example, Rom. 1:32), even if
we could see how the ten commandments might be
understood apart from their explanations and ap-
plications in the case laws.

5. Ignoring the Evidence

Others who have disagreed with the perspective
advanced herein have wanted to mitigate the force of
subordinate aspects or observations m the argu-
ments put forward (for example, disagreeing with
the claim that Old Testament Jewish and Gentile
rulers had religious titles). Even if we left such de~
tails undefended, however, the main lines of argu-
mentation in favor of the position taken on the politi-
cal use of God’s law would be unaffected by these
minor criticisms. Thus such details need not be de-
fended here, for they are not crucial to the case
made.

Others who have disagreed with the case made in
