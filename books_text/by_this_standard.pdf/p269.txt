LAW AND POLITICS IN NATIONS AROUND ISRAEL 239

reflecting parallels with the teaching about magis-
trates in Israel, begins with the truth that:

1. God's appointed rulers are not to be resisted,

The leaders of the foreign powers around Israel
were servants of God's will. Pharaoh had to learn the
lesson that God was unsurpassed in all the earth in
terms of power and authority (Ex. 19:14-16). Gentile
kings were subject to God’s reproof (Ps. 105:14). All
civil magistrates owed their authority to God’s sover-
eign disposition of history, and as such they were
subject to His rule, being set up or brought down ac-
cording to His decree (Ezk. 17:24).

God gave the earth to those unto whom it seemed
right to Him (Jer. 27:5). It was God who would
either break the yoke of the Babylonian king or
establish it as iron (Jer. 28:1-14). He was “Most
High” over the earth (Ps. 9:2; 83:18), setting the
course of nations subject to His rebuke (Ps. 9:4-8;
83:9-12). Even “beastly” rulers have been given their
authority by God (Dan. 7:6). Daniel, a Jew in exile
who would gain political honor, wrote that God
“removes kings and sets up kings” (2:21); “the most
High rules in the kingdom of men and gives it to
whomever he will” (4:25). Both Nebuchadnezzar
and Belshazzar, Gentile leaders, had to learn this
truth under the aweful hand of God’s judgment
(Dan. 4:28-34; 5:18-28). The nations round about
Israel were to know that God is the one who
sovereignly appoints and removes rulers. Indeed,
having learned this lesson, Nebuchadnezzar sent a
decree to all nations so that they might also
