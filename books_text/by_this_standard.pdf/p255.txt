LAW AND POLITICS IN OLD TESTAMENT ISRAEL 225

crime and punishment today as they were in Old
Testament Israel. Now as then, society needs to
know how to cope with attacks upon human dignity,
freedom, safety, and honor. Magistrates in all ages
need guidance for dealing with murder, kidnapping,
rape, perjury, and the like. And in this respect, the
magistrate in Old Testament Israel would be just
like any other magistrate — subject to the unchang-
ing justice and continuing validity of God’s revealed
law for socio-political affairs.

We can see this if we study the biblical teaching
about civil magistrates in Old Testament Israel,
Gentile nations surrounding Israel, and then in the
New Testament. Not only do we see, then, the con-
tinuing validity of the Old Testament law in general,
but we see the basically uniform outlook on civil rule
which is taught in God’s word. Rulers have the same
obligations and have the same standards of right and
wrong in all cultures. Having surveyed this situation
in Scripture, we can turn to the questions of church/
state separation and penology. Our survey begins by
outlining basic theses in the Biblical view of the civil
magistrate in Old Testament Israel.

1. God's appointed rulers are not to be resisted.

God was recognized in the Old Testament as the
One who ordained and removed rulers in Israel.
There was no authority in Israelite society except by
God, and those who ruled were ordained to such
leadership by God. On the one hand the people
selected and acknowledged their rulers (as in 1 Kings
12:20 or 2 Kings 9:13}, and on the other hand there
