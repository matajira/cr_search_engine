‘LAW ANO POLITICS IN THE NEW TESTAMENT 251

istrate regarding each and every decision he makes,
may be simple and easy positions to understand or
follow, but the more complex attitude of general sub-
mission for the sake of the Lord but resistance when
God's law is outrageously violated is more faithful to
Scriptural teaching and truer to political realities. It
is this balanced approach which Paul presents in
Romans 13 and which is summarized in the three
points outlined earlier.

Romans 13:1-7 states what God requires of believ-
ers regarding their civil leaders, and it states what
God requires of rulers regarding their civil function.
Submission to superiors is essential to both state-
ments of duty, The Lord expects His people to sub-
mit obediently to their rulers, for the Lord expects
those rulers to submit obediently to His law. For
conscience’ sake, then, Christians can submit to
their civil authorities, knowing that indirectly they
are submitting to the moral order of God Himself.

1. As appointed by God, rulers are not to be resisted.
Paul begins with the generalization that civil
government is a divine institution: “there is no
power (authority) but of God” (Rom. 13:1). God has
actually “ordained” the powers that be. Obviously,
then, supremacy belongs to God and not to the state.
Respect for the rulers of state ought never to reach
such proportions that the believer gives the state that
unquestioning obedience which should be reserved
for God alone. Paramount in Paul’s mind is the fact
that, even if Christians are under orders from the
state, the state itself is under orders from God above.
