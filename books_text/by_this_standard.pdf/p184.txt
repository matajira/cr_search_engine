16

DISCONTINUITY BETWEEN THE
COVENANTS ON THE LAW

“The New Covenant surpasses the Old in glory,
power, realization, and finality.”

What has been said above does not in the least
deny that there are some forms of discontinuity be-
tween the Old Testament and the New Testament —
that is, between the Old Covenant and the New
Covenant — regarding the law of God. What it does in-
dicate is that any such discontinuity must be taught
by God's word and not be brought as a categorical,
theological assumption to God’s word, We can turn
now to such Biblically grounded discontinuities be-
tween the Old and New Covenants. Because the law
of God plays a central role in His covenantal deal-
ings with His people, it is altogether appropriate that
the contrast between these two covenants should
have a bearing on our relationship to that law.
