CHURCH AND STATE 291

denomination should not be favored above others by
the state. This, as a matter of historical fact, is what
the First Amendment of the U.S. Constitution laid
down when it prohibited the “establishment” of relig-
ion. It did not prohibit the expression of religiously-
based views by politicians or their supporters; nor
did it prohibit obedience to the Bible by public
officials. It merely prohibited the establishment of
one denomination as the state-church.

Finally, in recent days, it has come to be asked
whether a distinctive religious system or revelation
should be the standard for individual lawmakers as
they determine public policy. In previous ages peo-
ple would have been wise enough to see through
such a question, for it is in fact impossible nef to have
some religious presuppositions whenever a lawmaker
takes a stand one way or another on an issue. The
only question should be wiich religious beliefs ought
to guide him, not whether religious beliefs should
guide him! However, today those who favor the
pseudo-ideal of religious neutrality when it comes to
politics tend to express their position as a commit-
ment to the “separation of church and state.” By this
they mean the separation of morality (or religiously-
based morality) from the state; they favor instead
secular or autonomous laws in society. Those who
believe that magistrates are bound to the law of God
are (mistakenly) accused of violating the separation
of church and state— which should mean the separa-
tion of two institutions and functions.
