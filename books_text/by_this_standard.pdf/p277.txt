LAW AND POLITICS IN THE NEW TESTAMENT 247

vealed through Moses and expounded by the proph-
ets in the Old Testament. When we turn to study the
New Testament writings themselves on this ques-
tion, this is precisely what we find to be the case.
There is definite continuity between the political eth-
ics of the New Testament and the political ethics of
the Old Testament, There is complete harmony be-
tween what Paul says about the state, for instance in
Romans 13, and what we found to be taught in the
Old Testament — namely:

1, As appointed by God, rulers are not to be
resisted.

2. Bearing religious titles, rulers are
avengers of divine wrath.

3. So rulers must deter evil by ruling ac-
cording to God’s law,

These very points, made by the Ojd Testament
with respect to Jewish and Gentile (redeemed and
non-redeemed) magistrates both, are clearly ex-
pressed by Paul in Romans 13:1-6, They are
premises upon which a distinctive Christian attitude
toward public justice can and ought to be for-
mulated.

Romans 13
If the three points laid out above are each taken
seriously, then perhaps we can avoid falling into the
unfortunate excesses of two conflicting interpretive
approaches to the teaching of Romans 13 about the
