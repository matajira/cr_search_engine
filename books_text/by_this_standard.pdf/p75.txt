6

THE FATHER'S UNCHANGING
HOLINESS AND LAW

“God's permanent requirement over all of life is
God-Imitating holiness. In ail ages, believers are
required to display, throughout their lives, the
holiness and perfection of their God.”

There is a sense in which the aim of every man’s
life is to be like God. All men are striving to imitate
God in one way or another. Of course not all at-
tempts to be like God are honored by the Lord and
rewarded with His favor, for there is a radical differ-
ence between submitting to the Satanic temptation
to be like God (Gen. 3:5) and responding to Christ’s
injunction that we should be like God (Matt. 5:48).
The first is an attempt to replace God’s authority
with one’s own, while the second is an attempt to
demonstrate godliness as a moral virtue.

The basic character of godly morality was made
manifest in the probation or testing placed upon
