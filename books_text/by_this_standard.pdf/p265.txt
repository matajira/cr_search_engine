LAW AND POLITICS IN NATIONS AROUND ISRAEL 235

other created people as well. The Gentiles who were
not given the law still have the work of the law writ-
ten on their hearts, thereby condemning their sinful
behavior. This is Paul's testimony in Romans
2:12-16, and it is a truth which is foundational to the
universal gospel of salvation which Paul goes on to
elaborate in Romans. All people are under obliga-
tion to the standards of God’s law — in whatever form
it has been received, written or not—and thus all
have sinned and are in need of Christ’s redemption
(Rom. 3:23). God is no respector of persons here.
He has the same standard for all men whom He has
created. And all men know those standards in virtue
of their creation as God’s image, in virtue of living in
God’s world, and in virtue of God’s clear work of
general and special revelation. Nevertheless, there
are Christians who maintain that with respect to a
special subclass of the laws revealed to the Jews in
the Old Testament, those laws were meant for only
Israel to keep. These laws were political in character.
The kings and judges of Israel were bound to obey
them, we are told, but not the rulers in other na-
tions, Alf children — Jewish or Gentile—were under
moral obligation to obey their parents, it is thought,
but only Jewish rulers (not Gentile) were under
moral obligation to punish crimes (for example, as-
saulting one’s parents violently) in the way specified
by the Old Testament law. That is, according to this
outlook, some laws from God were universal in
obligation, and other laws were localized.

Is such a delineation of universal and localized
laws made in the ¢ext of the inspired Old Testament?
