AUTONOMY AND ANTINOMIANISM = 295,

can establish self-law in its place.

Romans 1:18-32 and 2:12-26 teach that nobody
who has ever lived in God's creation has been un-
aware of the Creator’s standards of conduct. All
men, even those who have never heard of the Bible,
hinder the truth by means of their unrighteous lives.
Yet even though they may not have been privileged
to receive a written revelation of the law of God
(e.g., the “oracles of God” given to the Jews: cf. 2:17,
27; 3:1-2), “the Gentiles who have not the law. . .
show the work of the law written on their hearts”
(2:14-15), In their innermost selves all men know the
requirements of God’s law, but they seek to escape
that condemning knowledge and to construct substi-
tute theories of ethics for themselves. “The natural
man receives not the things of God's Spirit” (1 Cor,
2:14), and indeed the mind controlled by the sinful
nature “is not subject to the law of God, and neither
can it be” (Rom, 8:7). By nature the unbeliever must
oppose the concept of the law of God which this book
promotes. Like Adam their father, unbelievers seek
to “be like God,” determining for themselves what
will be good and evil—setting aside God's self-
attesting revelation in nature and Scripture, and
proceeding down the road of sinful rebellion toward
the demise of ethics.

Plato and Sartre
Plato taught that ethics is independent of
religion, for the form (or essential idea) of goodness
and piety exists apart from the thinking of the gods,
who approve of actions by looking above themselves
