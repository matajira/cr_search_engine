procoaue xvii

them screaming anyway.

His performance could also be compared to a man
who “milks” a poisonous snake: he operates methodi-
cally, without visible emotion, and precisely. Eventu-
ally, the snake is rendered harmless. Temporarily.
Until the poison is again manufactured by its system.
Then it’s another round of “milking,” with yet another
argument being squeezed dry of logical and biblical
content, until the snake is exhausted. On and on it
goes, until the snake finally dies or has its fangs ex-
tracted. To appreciate the technician's efforts, how-
ever, the observer must recognize the danger of the
poison and the. seriousness of the operation. The
observer also should not be surprised that from start
to finish, there is a lot of outraged hissing going on.

What is notable about Dr. Bahnsen’s previous
writings on biblical law has been the dearth of pub-
lished criticism. Theonomy in Christian Ethics appeared
in 1977, and it received considerable verbal criticism.
Murmurings might better describe the response. But
there was not much published criticism, and what
there was cannot be described as a serious threat to
Dr. Bahnsen’s case. A few critical essays appeared,
but only one was of any academic significance, Dr.
Meredith Kline’s, and Dr. Bahnsen’s subsequent
response ended the debate.2 Whenever I reread the

2, Meredith G, Kline, Westminster Theological Journal, vol.
XLI, No. £ (Falt 1978); Greg. L. Bahnsen, “M. G, Kline on
‘Theonomic Politics: An Evaluation of His Reply,” joumal of
Christian Reconstruction, V1, No. 2 (Winter 1979-80). The latter
volume is available from Chalcedon Foundation, P.O. Box 158,
Vallecito, California 95251.
