3

THE ENTIRE BIBLE IS
TODAY’S STANDARD

“God expects us to submit to His every word,
and not pick and choose the ones which are
agreeable to our preconceived opinions.”

All of life is ethical, and all of the Bible is perme-
ated with a concern for ethics. Unlike the organiza-
tion of an encyclopedia, our Bible was not written in
such a way that it devotes separate sections exclus-
ively to various topics of interest. Hence the Bible
does not contain one separate, self-contained book
or chapter that completely treats the subject of ethics
or moral conduct. To be sure, many chapters of the
Bible (like Exodus 20 or Romans 13) and even some
books of the Bible (like Proverbs or James) have a
great deal to say about ethical matters and contain
very specific guidance for the believer’s life. Never-
theless, there will not be found a division of the Bible
entitled something like “The Complete List of Duties
