328 BY THIS STANDARD

this book have complained that it is made “by infer-
ences” from Scripture— apparently, instead of by di-
rect and explicit statement of the political validity of
God's law. But since the same misguided complaint
could be made about major doctrines of the faith (for
example, the Trinity, the hypostatic union), it is
hardly a telling point against our position here on
political ethics.

Another argument has been that if we temporarily
set aside the major New Testament evidence that is
enlisted in support of the perspective taken in these
studies, and if we then read the New Testament
without that evidence present, then we would not get
the impression that God's law, in its political aspects,
is valid today. It is thought that the purported evi-
dence in favor of our position has been mistakenly
interpreted in a way that does not harmonize with
the rest of the New Testament.

This line of criticism shows how desperate some
can become in trying to refute the thesis that the
political use of God’s law is valid today. In the first
place, if we subtract the positive evidence for the
thesis, the rest of the New Testament is not contrary to
the thesis; it is simply silent on the subject. In the se-
cond place, it is hardly a legitimate complaint
against a position that it has no support when its
main lines of support are put to the side! A lawyer
who argued for his client by merely asking the jury
to ignore the evidence presented by the prosecutor
would not long retain his job. Until definite negative
evidence against the thesis can be adduced from the
New Testament we should acknowledge that Scrip-
