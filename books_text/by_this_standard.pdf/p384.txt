354 BY THIS STANDARD

mony; the change from one principle or regime to
another

DISPENSATION — a distinct administration of God’s
covenantal relation with man or the age character-
ized by such

EGOISM — the ethical view that one ought to act out
of regard for his own benefit or welfare

EGOTISM — the sinful, personal trait of behaving as
though one’s own interests were of supreme or sole
importance

ESCHATOLOGY —the doctrine of the “last things”
pertaining to the individual (death, afterlife) or to re-
demption (the coming, course, and consummation
of Christ’s kingdom, the millennium) or to the world
(Christ’s return, the resurrection, final judgment,
the eternal state)

EVANGELICAL MANDATE-—God’s authoritative
order for His people to preach the gospel to lost sin-
ners, seek their conversion, bring them into the sac-
ramental fellowship of the church, nurture them in
the Christian life, and thus make the nations to be
disciples of Christ; the “Great Commission”

EXEGETICAL — pertaining to the detailed analysis
and linguistic meaning of specific texts of Scripture

EX POST FACTO — applied “after the fact,” thereby
disregarding the previous circumstances, status, or
legal character of an event
