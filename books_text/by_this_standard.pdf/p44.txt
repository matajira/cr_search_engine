14 BY THIS STANDARD

But there are many moral values which are rec-
ommended to us. There are numerous implicit codes
of right and wrong. We go through every day in the
midst of a plurality of ethical viewpoints which are in
constant competition with each other. Some people
make pleasure their highest value, while others put a
premium on health. There are those who say we
should watch out for ourselves first of all, and yet
others tell us that we should live to be of service to
our neighbor. What we hear in advertisements often
conflicts with the values endorsed in our church.
Sometimes the decisions of our employers violate
laws established by the state. Our friends do not
always share the code of behavior fostered in our
family. Often we disagree with the actions of the
state. AH! of life is ethical, but making ethical deci-
sions can be confusing and difficult. Every one of us
needs a moral compass to guide us through the maze
of moral issues and disagreements that confront us
every moment of our lives.

To put it another way, making moral judgments
requires a standard of ethics. Have you ever tried to
draw a straight line without the aid of a standard to
follow, such as a ruler? As good as your line may
have seemed initially, when you placed a straight-
edge up to it, the line was obviously crooked. Or
have you ever tried to determine an exact measure-
ment of something by simple eyeball inspection? As
close as you may have come by guessing, the only
way to be sure and accurate was to use a proper
standard of measurement, such as a yardstick. And
if we are going to be able to determine what kinds of
