THE SON'S MODEL RIGHTEOUSNESS 59

was wounded for our transgressions, he was bruised
for our iniquities. . . . Jehovah has laid on him the
iniquity of us all” (Isa. 53:4-6). Sin cannot avoid the
dreadful judgment of God (Nahum 1:2-3; Habakkuk
1:13), and therefore God does not save sinners with-
out righteousness and peace kissing each other (Ps.
85:9-10); He remains just, while becoming the justi-
fier of His people (Rom. 3:26). Accordingly the law’s
demands could not be arbitrarily pushed aside.
Christ had to come and undergo the curse of the law
in the place of His chosen people; He had to satisfy
the justice of God. That is why it can be said that the
death of Christ is the outstanding evidence that
God's law cannot be ignored or abrogated. Accord-
ing to the law there is no remission of sin apart from
the shedding of blood (Heb. 9:22; Lev. 17:11), “There-
Sore it was necessary that Christ offer up himself in sac-
rifice for sin” (Heb. 9:23-26). The necessity of the
law's continuing validity is substantiated by the sav-
ing death of Christ on our behalf.

Imitating Christ

Ghristians should therefore be the last people to
think or maintain that they are free from the right-
eous requirements of God’s commandments. Those
who have been saved were in need of that salvation
precisely because God’s law could not be ignored as
they transgressed it. For them to be saved, it was
necessary for Christ to live and die by all of the law’s
stipulations, Although our own obedience to the law
is flawed and thus cannot be used as a way of
justification before God, we are saved by the im-
