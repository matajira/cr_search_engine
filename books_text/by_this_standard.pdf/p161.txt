NEW TESTAMENT MORAL JUDGMENTS ENDORSE THE LAW 131

Christians condemn private vengeance and hatred of
one’s enemies, they reaffirm the continuing authority
of God’s law (even if unwittingly),

Conclusion
One cannot escape the authoritative use of the
Old Testament law in New Testament moral judg-
ments. Upon reflection, one should recognize that
such use teaches the full validity of God’s law today.
Invalid rules might be used in fallacious moral judg-
ments —but not in inspired ones.
