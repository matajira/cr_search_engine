350 BY THIS STANDARD

the same outlawed acts in both dispensations).3 Ask
them whether it is now immoral for a woman to
marry her father. They may say yes, but they will
not find that specific case of incest dealt with in the
New Testament scriptures. Ask them whether rape
is a punishable crime. Again, no New Testament di-
rective covers it. Ask them what the equitable pun-
ishment should be for rape. No New Testament an-
swer. Ask them whether they can even show that
murder should be a capital crime today. Once more
they will find no specific New Testament answer to
that question, despite the fact that many conser-
vative believers assume that it is there,

It becomes ever so clear that it is easy to say one
holds only to “New Testament ethics,” but nearly
impossible to systematically and consistently main-
fain that position. In actual fact, Christians do not
find it a workable policy to follow, departing from
the espoused position whenever it seems convenient
or necessary to do so. But that simply opens the door
to arbitrariness.

The preceding book has attempted to provide a
principled, systematic, and consistent approach to
the question of whether and how the Old Testament
law constitutes a standard for making moral deci-
sions today.

3. Cf. treatment of this issue in “The Bahnsen-Feinberg
Debate,” a tape available from Covenant Tape Ministry (4155
San Marcos Lane, Reno, NV 89502). The debate was spon-
sored by the Evangelical Theological Society at its annual
meeting for 1981 in Toronto.
