DISCONTINUITY BETWEEN THE COVENANTS ON THE LAW 165

cused of opposing the temple and the Mosaic law's
rituals (for example, Acts 6:14; 21:28). The New
Covenant word teaches that some of God’s Old Cove-
nant ordinances were not intended to be continu-
ously observed in the same manner throughout re-
demptive history. With the coming of the Savior and
His perfect priestly work, necessarily the Levitical
priesthood has been changed (Heb. 7:12). Hence the
sacrifices, feasts, etc. of the old order are not binding
upon the believer today in their shadow forms (cf.
Col, 2:13-17). They are observed today by faith in Christ.

(B) The New Covenant Redefines the Covenant
People of God.

Under the Old Covenant order, Israel was con-
stituted as a nation and adopted as the people of
God, but under the New Covenant the people of
God is an international body comprised of those who
have faith in Christ. The kingdom has been taken
from the Jews (Matt. 8:11-12; 21:41-43; 23:37-38; 1
Cor. 14:21-22), and the church is now “the Israel of
God” (Gal. 6:16), “the commonwealth of Israel”
(Eph. 2:12), the “kingdom of priests” (1 Peter 2:9),
the “twelve tribes” of the Dispersion ( Jas. 1:1, 1 Peter
11), and the seed of Abraham (Gal. 3:7, 29). Faithful
Israel of old is included within one household of God
comprising the church (Heb. 3:1-6); Israelites and
Gentiles are separate branches, part of one olive tree
of faith (Rom. 11:17-18). Thus, the New Testament
church is the restoration of Israel (Acts 15:15-20), and the
New Covenant to be made with Israel and Judah is
actually made with the apostles who are founda-
