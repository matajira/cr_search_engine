CHURCH AND STATE 289

Scripture that the validity of the Mosaic law for soci-
ety somehow depended upon any of these extraor-
dinary features of the Old Testament social arrange-
ment. Despite the uniqueness of Israel, its law-code
was held forth as a model for other nations to imitate
(Deut. 4:6-8). What was not extraordinary or unique
was the justice embodied in the law of God; its valid-
ity was universal, applying even to nations which
did not in every respect parallel the social (or
church-state) situation in Israel. Consequently, even
if we were to point out that today our social arrange-
ment differs somewhat from that of Old Testament
Israel’s, we would not thereby be justified in con-
cluding that the law revealed to Israel is not morally
valid for our present day society. Whatever the
precise church-state relation was in Israel, the law
revealed to Israel ought to be obeyed even by soci-
eties which have a slightly different church-state re-
lation today.

A consideration of the separation of church and
state (or lack thereof) in Old Testament Israel does
not, then, invalidate the authority of the Old Testa-
ment law for current American society. Christ
taught that we should render unto Caesar the things
that are Caesar’s, and unto God the things that are
God's (Matt. 22:21). There is a difference between
Caesar and God, to be sure, and we must obey both
with that distinction in mind. And yet while we owe
obedience to the powers that be (Rom. 13:1-2), the
civil magistrate owes allegiance to God’s revealed will,
for he is the “minister of God” (Rom. 13:4).

To admit that the church is separate from the
