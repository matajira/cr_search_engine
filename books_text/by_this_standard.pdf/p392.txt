362 By THis STANDARD

TYPOLOGICAL — pertaining to a “type,” something
intended to foreshadow a later historical reality

UNREGENERATE — not born again or spiritually re-
newed by the power of the Holy Spirit; pertaining to
the “natural man” who is lost in sin, unable to do
God’s will or to understand the things of the Spirit

WESTMINSTER STANDARDS—the Westminster
Confession of Faith and Catechisms (Longer and
Shorter) which were composed 1643-1647 at the re-
quest of the English Parliament and which, since
that time, have served as subordinate doctrinal
standards in presbyterian churches; deemed a model
of “Reformed” doctrine
