GOD'S COMMANDMENTS ARE A NON-LEGALISTIC RULE OF OBEDIENCE 177

that there was a common attitude toward the law and a
presupposed continuity between the covenants as to God's
moral standards in the law —despite the fact that the
New Covenant introduced important elements of dis-
continuity regarding the believer’s relationship to the
law, In the age of the New Covenant the Old Cove-
nant law of the Lord retains its binding authority.

So then, both theological insight and specific
New Testament teaching agree in supporting the law
of God as a standard of conduct. If a person wishes
to please the Lord, then he must seek to bring his
thoughts, words, and deeds into conformity with the
norms laid down in the law of God, Christian ethics
is surely concerned with more than the law of God
(for instance, it considers issues like ethical enable-
ment, motivation, maturation, discernment, in-
sight, application), but it cannot be concerned with
less than the law of God — for the law supplies a pat-
tern and criterion of godly living.

The Law Is Natural, Universal

Because that pattern and criterion is an unchang-
ing one, the law continues to be a major concern of
Christian ethics today. The standard of holiness re-
vealed by the law is not peculiar to Old Testament
Jews, nor is it somehow uniquely for those redeemed
by God. That standard is universally binding on all
created men, being “natural” in the sense that it is
appropriate to the Creator-creature relation, and in
the sense that it is revealed as binding to all mankind
(either through the created realm and conscience, or
through special written revelation).
