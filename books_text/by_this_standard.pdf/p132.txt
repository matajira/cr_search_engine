102 ay THIS STANDARD

What is the character of this holiness which the
New Testament takes as a pervasive moral theme?
By what standard is holiness measured and where is
concrete guidance in holiness found? The fact that
Christians are to be holy is so often stated in the New
Testament that we must certainly assume that she
norm or criterion of holiness was already well known; little
needs to be said to explain to New Testament read-
ers what this holiness requires. The suggestion is un-
avoidable that the Old Testament standards of mor-
ality already sufficiently defined the holiness which
God sought in His people. Hebrews 12:10 indicates
that God chastens us so that we may become “par-
takers of His holiness,” and thus New Testament hol-
iness is nothing less than a reflection of God’s charac-
ter on a creaturely level.

How does one who is a sinner in thought, word,
and deed come to know what God’s holiness requires
of him? Peter makes it clear what is implicit in the
pervasive New Testament theme of holiness when he
writes, “even as he who called you is holy, be
yourselves also holy in all manner of living; because
it stands written, ‘You shall be holy, for I am holy’ ”
(1 Peter 1:15-16). Here Peter quotes the Old Testa-
ment law from such places as Leviticus 11:44-45;
19:2, and 20:7, where it is evident that God’s people
would be sanctified and be holy by following all the
statutes of God's revealed law. Christ was surely in-
cluding the Old Testament in His reference, when
he prayed that His people would be sanctified by His
word of truth (John 17:17). Indeed, Paul explicitly
says that the Old Testament law is our standard of
