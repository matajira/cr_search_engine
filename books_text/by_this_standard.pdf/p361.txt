ARGUMENTS AGAINST THE LAW'S POLITICAL USE 331

church-state situation in Old Testament Israel.
Israel was a priestly nation then, whereas the
church —not America —has that status today. This is
correct: the religious mission of the corporate body
(the priestly function of the community as a whole)
has now been assumed by a different kind of body,
the international community of faith, rather than a
particular nation, However, this says nothing about
the relation of church to state within the nation of
Israel, and it certainly does not belie the legitimate
separation between the two which we read of else-
where in the text.

2. The “Theocracy” Argument

It has been claimed that the Old Testament
church-state (the sense given to “theocracy”) has now
been replaced with an international church (minus
state) in the New. This flounders on the mistaken as-
sumption that the Old Testament was a church-state. As ex-
plained previously, priests and kings had separate
authorities, and the membership of the state was not
coextensive with the membership of the religious
body (for example, the sojourners in Israel).

3. The “Redemptwe Community” Argument

The claims that the Old Testament state was a
“redemptive” community and that the state existed
for a “religious purpose” are too ambiguous—being
obviously correct on some interpretations (for exam-
ple, that the state arose out of God’s redemption of
the people from Egypt and served the religious aim
of punishing social evil), yet irrelevant to the annul-
