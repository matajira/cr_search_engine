282 ey THIS STANDARD

deal with social justice within history. Christ did not
remove the penalties for social misdeeds, or else
Christians could argue that they need not pay traffic
fines! The discipline of the church does not remove
the need for the state to have just guidelines for pen-
alties in society. And far from confirming social pen-
alties, waiting for the final judgment removes social
penalties for crime altogether. Even if one could
argue (with Biblical indicators) that the penal sanc-
tions of the Old Testament foreshadowed the final
judgment, it would be something else to argue that
those penalties did nothing else but foreshadow final
judgment, After all, they also dealt with historical
matters of crime and punishment, and so they could
continue to do so today (while still foreshadowing
the coming final judgment).

May We Abrogate All but One?

If the above arguments have proven awkward in
the light of Biblical teaching and logical consistency,
one can understand how much more difficult it
would be to defend the position that the penal sanc-
tions have been abrogated today, except for one
(namely, the death penalty for murder). Such a posi-
tion fails to show that the penal sanctions have been
laid aside in general. At best it appeals to a fallacious
argument from silence, saying that such social
penalties were not mentioned, for instance, by Paul
wher he spoke to the Corinthian church about an in-
cestuous fornicator. Of course, neither did Paul
dispute those sanctions, seeing that he was speaking
to the church about ifs response to the sinner (not the
