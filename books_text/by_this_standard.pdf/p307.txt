CRIME AND PUNISHMENT §6277

variable, cultural detail.

As attractive as this suggestion may sound in
abstract (after all, it would make it much easier to
promote God’s law within a secularized culture), it is
clear that the suggestion cannot be defended in the
face of particular textual and theological realities.
For instance, the two texts rehearsed above are spe-
cifically worded so as to require more than just any
kind of punishment for those who practice witchcraft
and bestiality. What is prohibited in Exodus 22:18 is
that a witch should be allowed io live. A magistrate
who merely fines a witch (i.e., a genuine witch as
Biblically understood) would transgress this prohibi-
tion, allowing thereby what the text forbids — namely,
the allowing of a witch to live. Exodus 22:19 used an
idiomatic Hebrew expression to communicate the
certainty of the death penalty for someone committing
bestiality: “shall surely be put to death.” The whole
point here is that this crime is so heinous that only
the death penalty is its just recompense.

The arbitrariness of some commentators here is
perplexing. For example, R. A. Cole writes, “Our
attitude to perversions of God’s natural order can
hardly vary from those of the law, while our treat-
ment of offenders will be very different today.”! Yet
the Hebrew text teaches that our freaiment of this
crime must not vary: “surely” such an offender is to be
put to death, If that is not the justice which we en-

1, R. A. Cole, Exodus (Tyndale Old Testament Gommen-
taries), edited by D. J. Wiseman (Downers Grove: Inter-Varsity
Press, 1973), p. 174.
