4 Covenant Sequence in Leviticus and Deuteronomy

its fullest manifestations, God’s covenant with man, which we can
illustrate from the Mosaic covenant, entails the following steps
and aspects:

1. Announcement of God’s transcendence; His laying hold
on the situation (Ex. 2:24-25; 20:3).

2. Declaration of God’s new Name, appropriate for the new
covenant being installed (Ex. 3:13-15; 6:2-8; 20:2a).

3. Statement of how Ged brought His people from the old
covenant and world into the new one (Ex. 20:2b; Deut, 1:6-4:40).

4. Establishment of the new covenant order, especially the
governmental hierarchies thereof (Ex. 18:13-27, Deut. 1:9-18).

5. Appointment of new names for the new finished product
(Gen, 1:4-5, 6-8, 9-10; at Moses’ time, “children of Israel” is the
new name, replacing “Hebrew’}.

6. Grant or distribution of an area of dominion to the cove-
nant steward or vassal (Ex. 3:8; Deut. 1:19-12:31).

7. Stipulations concerning the management of this grant
(Ex. 20-23; Deut. 5:1-26:19).

8. Statement of the terms by which God will evaluate man’s
performance: promised blessings and threatened curses (Ex.
23:25-33; Deut. 27, 28).

9. Placement of witnesses to report to God on man’s behavior
(Ex. 23:20-23; Deut. 4:26; 30:19).

10. Arrangements for the deposition of the covenant docu-
ments (Ex. 40:20; Deut. 31:9-13).

11. Arrangements for succession of covenant vice-regents
(Deut. 31:7, 14, 23; Deut. 34).

12. Artistic poems that encapsulate the covenant, and that are
to be taught to succeeding generations (Deut. 31:14-33:29).

We could probably come up with other aspects as well, depending
on how much detail we wished to go into.

This covenant order can be helpfully and biblically grouped in
more than one way. It is possible and desirable to see the sequence
as proceeding from God's sovereign Control (1-3), to manifesta-
tions of God’s sovereign Authority (4-7), and culminating in rev-
elations of God’s sovereign Presence with His people (8-12). This
