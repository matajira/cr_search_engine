The Structure of the Book of Leviticus 17

20:1 Lord spoke to Moses
Sanctions for violations

Ila. Perfection of Priests and Sacrifices, Leviticus 21-22

21:1 Lord said te Moses
Priests must be holy to God
21:16 Lord spoke w Moses
Which priests have access
22:1 Lord spoke to Mases
Priests must not profane God’s Name
22:17 Lord spoke to Moses
Evaluating gifts and sacrifices
22:26 Lord spoke to Moses
Other qualifications of animals: 8 days old, not killed
samme day as mother, all eaten in one day

IV, Holy Times, Leviticus 23

23:1 Lord spoke again to Moses
Sabbaths and Passover
23:9 Lord spoke to Moses
Firstfruits and Pentecost
23:23 Lord spoke again to Moses
Seventh month trumpets
23:26 Lord spoke to Moses
Day of Atonement
23:33 Lord spoke to Moses again
Feast of Tabernacles

V. Historical Perspective, Leviticus 24-27

24:1 Lord spoke to Moses
Lampstand arid showbread
24:10 Historical incident
Son of Egyptian woman cursed the Name
24:13 Lord spoke to Moses
Eye for eye
25:1 Lord spoke to Moses on Mt. Sinai
Jubilee and sanctions
27:1 Lord spoke ta Moses again
Redemption of persons and gifts
