2

THE STRUCTURE OF THE
BOOK OF LEVITICUS

We can identify five major sections in Leviticus, by taking
Note of the separate speeches of the Lord, of which there are
thirty-six, and then grouping these according to fairly obvious
thematic breaks in the book.

Leviticus 7:38-39 brings the section on sacrifices to a close.
Leviticus 16 comes in the context of the deaths of Nadab and
Abihu (16:1; 10:1-2), which itself is the sad “fall” of the “new Adam”
created by the rituals of chapters 8 and 9. Thus, Leviticus 8-16 is a
section. For reasons that will emerge in the discussion later, I take
Leviticus 17-22 to be a separate section, Leviticus 23 to be a fourth
section, and Leviticus 24-27 to be a final section.

1. The Sacrifices of the Congregation, Leviticus 1-7

1:1 Lord called to Moses
Burnt Offerings, ch. 1
Cereal Offerings, ch. 2
Peace Offerings, ch. 3

4:1 Lord spoke to Moses
Purification Offerings

5:14 Lord spoke to Moses
Compensation Offerings

6:1 Lord spoke to Moses

Compensation Offerings

Ja. The Labor of the Priests, Leviticus 6:8-7:36

6:8 Lord spoke to Moses
Law of Burnt Offering, 6:8-13
Law of Cereal Offering, 6:14-18

15
