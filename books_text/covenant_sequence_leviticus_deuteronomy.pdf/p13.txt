Covenant as a Literary Structuring Device 9

Genesis is the book of beginnings, In it, we see God lay hold on
His world and announce His intentions. ‘The intentions an-
nounced to Abraham, Isaac, and Jacob do not come to pass in
Genesis, however. The names of God revealed in the Noahic and
Abrahamic covenants stress His transcendent sovereignty.
Noahic: God Most High, Possessor of heaven and earth. Abra-
hamic: God Almighty.

Exodus is the book of transitions. In it, we see God break down
His people, move them out of an old cosmos and into a new, ap-
point new names for them, reveal a new name for Himself, and
give them a new social order. God’s new name is Lord, which is
explained in Exodus 6 as meaning “the God who keeps promises
made to the fathers.” The new social order entails two hierarchies,
those of church (priests and Levites) and state (elders and
supreme judge). Also, in Exodus God builds Himself a house,
symbolizing the new social order and hierarchy.

Leviticus is the book of law. In it, we see God lay out the essential
terms of the grant He is giving His people. They are restored to a
new Garden of Eden, and thus much of the book concerns animal
laws: sacrifices and uncleanness. The distribution of this covenant
grant, this environment, brings with it rules that the stewards are
to use in governing it. Thus, almost the entire book is legislation,
both sacramental and social. It is important to note that the focus
of the laws in Leviticus is not simply obedience to God, but rather
the maintenance of the grant. The sacrifices, laws of cleansing,
sabbath observances, and payments of vows are all designed to
prevent God from taking offense and leaving. Thus these “cere-
monies” reveal the truth about the “moral” laws found in Exodus
and Deuteronomy: The Kingdom is distributed to us as a gift, but
if we are to maintain the grant, we must be faithful. Leviticus
focusses attention on the maintenance of the Kingdom by confes-
sion and cleansing.

Numbers is the book of implementation. In it, we see God apply
the sanctions of blessing and curse laid out at the end of Leviticus.
God’s people are organized as His army to execute judgment on
the Canaanites. When they refuse to do so, God executes judgment
