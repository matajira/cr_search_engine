An Outline and Analysis of Leviticus 19 49

heart that he develops more and more as a unique individual. The
commands in this first series have to do generally with matters
that would not come under the eye of a judge, and thus are self-
enforced.

The first three sections in the first series gravitate around the
first five commandments, while the last four sections comment on
the second table. Each of the first three sections ends with “I am
the Lorn your God,” while each of the last four sections ends with
“I am the Lorp.”

Section I-1 (v, 3} has two laws, linking fear of parents with sab-
bath keeping. The fourth and fifth commandments are in view
here. The sabbath affirms God’s sovereignty, and is here linked
with the image of that sovereignty in the family. True personal
integrity and uniqueness comes from submission to God and par-
ents, not from rebellion. Human autonomy is the opposite of in-
tegrity. The fourth commandment says to sanctify the sabbath
(keep it holy), while here they are told to guard it. The fifth com-
mandment says to honor parents, while here they are told to fear
them. Thus, at this point Leviticus 19 uses vocabulary different
from what is found in the ten commandments, in order to
enhance our ethical understanding. Leviticus 19 speaks of guard-
ing four times, twice of the law as a whole, and twice of the sab-
bath (vv. 3, 19, 30, 37).

Section I-2 (v. 4) has two laws dealing with the first and second
commandments. “Turning to the idols” is worshipping another
god, while making a god of cast metal violates the prohibition on
graven images. The second commandment is phrased in terms of
graven images, while the law here is in terms of molten or cast im-
ages, like Aaron’s golden calf.

Section I:3 (vv. 5-10) has two subsections embracing five laws
each, The first subsection has to do with God (first table) and the
second with man (second table). Fellowship and food are the ideas
joining the two sections. Since the other four commandments of
the first table have been covered already, it is possible that this
section is intended as an extended commentary on the third com-
mandment. The man who wears God’s Name properly will not des-
