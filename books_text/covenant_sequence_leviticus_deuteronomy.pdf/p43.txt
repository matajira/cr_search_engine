The Structure of the Book of Leviticus 39

Leviticus 24-27

The last section of Leviticus stresses succession, movement
into the future. [n an interesting way, it recapitulates the history
of the world, and then prophesies the future, following again the
covenant/re-creation sequence, which I see again as having seven
stages:

Stages I and 2, Leviticus 24:1-9 discusses the lampstand and
showbread in the Tabernacle. The bread signifies Israel, and the
lampstand the light of God. These are images of heaven and
earth, of the world as originally created by God. The lamp is a
model of the burning bush and of God’s glory cloud, Thus, this
section points to God's initiation of action as He appeared to
Moses and began to deliver Israel.

Stage 3. Leviticus 24:10-12 is not a speech by God but another
“historical interlude.” It concerns the son of an Egyptian, a new
Ishmael, who is judged for blasphemy. It recapitulates the deliver-
ance from Egypt.

Stage #. Leviticus 24:13-23 recapitulates the law given at Mount
Sinai.

Stage 5. Leviticus 25 and 26 are one speech in two sections.
Chapter 25 has to do with the Jubilee, and chapter 26 with the
blessings and curses of the covenant, Though these two concerns
are linked in one speech, they clearly correlate with two distinct
aspects of the covenant. The Jubilee has to do with the grant of
the Kingdom, and its maintenance. As we shall see below, this
chapter is set out in five sections. At any rate, with the Jubilee we
move to an anticipation of the possession of the land.

Stage 6. Leviticus 26 gives the blessings and curses of the cove-

nant, anticipating the future loss of the land.
‘Stage 7. Leviticus 27 discusses vows to God. This is often re-
garded as some kind of appendix to Leviticus, but in terms of cov-
enant sequencing it is necessary. Payment of vows relates to the
fifth commandment, as we give to our Divine parent and thereby
honor Him, and to the tenth commandment, since payment of
vows and tithes is the opposite of covetousness. Thus, this final
section of Leviticus has everything to do with continuity.
