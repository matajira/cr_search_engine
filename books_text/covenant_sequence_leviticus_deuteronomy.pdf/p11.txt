Covenant as a Literary Structuring Device 7

The Five-fold Sequence

Given the fact that we have a sequence of at least twelve items
to “fit” into five groups, a certain amount of flexibility is called for.
Each of the five categories will of necessity have a particular locus
of concern, but each will also have “fuzzy edges,” shading into the
concerns of the categories on either side. Moreover, each of the
five categories will embrace a number of elements, and it will not
necessarily be possible to come up with a one-word or short-
phrase encapsulation of each category that will do full justice to
the zone of concerns in that category. We can, however, come up
with a description of the concerns of each category if we look at
the five books of Moses and at the ten commandments.

Before doing so, however, I wish to enter a caveat. Much of
the literature generated to date in terms of the five-fold structure
has been concerned with social reform. Because of this, the focus
of presentation has been on the covenant as structure and rules,
not as gift. For instance, the second point is often simply de-
scribed as “hierarchy,” a chain of command, and the third point is
often simply considered as “law.”

While such a scheme is useful in literature focussing on social
issues, it is theologically inadequate for other concerns. God’s
Word is always promise before it is command. God summons us
to eat of the life-giving Tree of Life before we look to the authority-
bestowing Tree of the Knowledge of Good and Evil for instruc-
tion. God always bestows the Kingdom as a gift before presenting
us with our duties in it,

A scheme that simply says, “God has set up a hierarchy, has
given us His laws, and threatens curses if we disobey,” is a scheme
that can easily become abused in a legalistic fashion. I know of no
writer fo date who is guilty of such an abuse, but it is easy to imag-
ine the possibility, and history shows us that men will eventually
pervert anything they can. It is important, therefore, to affirm
clearly that grace underlies law. As presented in the Bible holisti-
cally, the covenant can help us do that.

For instance, point two is not simply the establishment of an
hierarchical chain of command. First and foremost it is God’s action
