An Outline and Analysis of Leviticus 19 51

God’s claims are eschatological. Whatever is left of a Peace
Offering must be put on God's holy fire and given to Him on the
third day. Whatever is left after harvest is designated by God for
the poor. As we shall see below in verses 23-25, God's claims are
also protological. He takes the first fruits as well as the last.

Section I: (vv, 11-12) has four laws and associates stealing
{eighth commandment) and false witness (ninth commandment).
Read in series, the idea seems to be that a man steals, then lies
about it, extends his deception further, and finally swears falsely
about it before God. This is also the sequence of events in Leviti-
cus 6:1-7,

Section I:5 (vv. 13-14) has six laws and associates stealing with
oppression of the poor. Oppression of the poor comes under the
seventh commandment, the treatment of God’s holy bride, in Ex-
odus 22:21-27.? The general idea here is fear. A man may not be
afraid to cheat his neighbor, confident that he will never be
caught. He may not be afraid to oppress his hired help, since the
hireling is sure to put up with it rather than lose his job. He may
not be afraid to curse the deaf, knowing that he will not be heard,
or trip the blind, knowing that he will not be seen. But God sees it
all; and the wise man will hold the fear of God in his mind, and
thus check his life.

Section I:6 (vv. 15-16) has six laws all concerning the ninth com-
mandment. The ninth commandment is concerned both with law-
courts and with general slander. Compare Exodus 23:1-10.

Section 1:7 (wv. 17-18) has six laws and comments on the sixth
commandment. Hatred is murder in the heart.

In Chapter 2 we saw a correlation between the sixth day of
creation, when man was created and warned, and the sixth point
in the heptamerous covenant sequence, which has to do with
warning and judgment. The sixth section of Leviticus 19 has to do
with witness bearing and lawcourts. Possibly we should associate
the fourth section of Leviticus 19, swearing falsely before author-

 

2. See my discussion of this in James B. Jordan, The Law of the Covenant: An
Exposition of Exodus 21-23 (Tyles, TX: Institute for Christian Economics, 1984),
p. 65.
