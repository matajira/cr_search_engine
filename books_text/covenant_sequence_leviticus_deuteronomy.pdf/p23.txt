The Structure of the Book of Leviticus 19

God’s estate. Accordingly, man as God’s princely steward is not to
wear the mantle of His Name in vanity, and is to respect the bound-
aries of His property as He has distributed it (third and eighth
commandments). This is the general concern of Leviticus 17-22.

God will not hold the man gu:liless who wears His name emptily.
As we have seen in Chapter 1, this is an overall concern of Leviticus
as a whole, but it is focussed in chapters 17-22. Here we find re-
peatedly the statement “I am the Lorp,” as God repeats His name
as the reason for obedience. Here also we find repeatedly the
threat of being “cut off,” which means that the sinner is no longer
covered by the sacrifices, no longer counted as guiltless. A study
of the incidence of these phrases shows that they dramatically pre-
dominate in this section of Leviticus, though they are also found
in other parts of the book since the book as a whole is concerned
with the Name.

Lev. Lev. Lev.
1-16 17-22 23-26 Numbers
Tam the Lorp 9 18 2 3
Tam the Lorp your God 0 10 3 2
I am the Lorp who sanctifies you 9 2 0 0
For I am the Lorp your God 1 1 3 0
I am the Lorp your God, who
has separated you 0 t 0 0
TOTAL 1 32 8 5
cut off (Heb. karath) 4 12 i 5
GRAND TOTAL 5 ft 9 10
Lam the Lorn:
Lev. 18:5, 6, 21
Lev. 19:12, 14, 16, 18, 28, 30, 32, 37
Lev. 21:12
Lev, 22:2, 3, 8, 30, 31, 33
Lev. 26:2, 45

Num. 3:13, 44, 45

fam the Lorp your God:
Lev. 18:2, 4, 30
Lev. 19:3, 4, 10, 25, 31, 34, 36
