18 Covenant Sequence in Leviticus and Deuteronomy

The Covenant/Re-creation Sequence

This outline falls fairly well within the parameters of the cove-
nant/re-creation sequence discussed in Chapter 1. The first aspect
of the covenant is the affirmation of God’s transcendence and His
relationship with man. This correlates with the first and sixth
commandments, but in Leviticus the first is preeminent. The first
commandment, which prohibits having any other gods besides
the Lord, deals with covenanial idolatry, idolatry in the whole of life,
disloyalty to God. They must be loyal to the God who initiated cove-
nani with them, and any sin must be covered by sacrifice. This is
the overall theme of Leviticus 1-7.

The second aspect of the covenant is its transition from an old
to a new order. When covenants are made, generally this second
part is taken up with a description of how the original order broke
down, and how God worked to restructure the world and society
and bring about a new order. The order itself is a system of medi-
ation, and the process of bringing it into being is a process of
mediation. Thus, mediation, the concern of the second com-
mandment, is highlighted here. It deals with liturgical idolatry
{iconolatry), access to God, man’s relationship to God, and the
image of that relationship in marriage (hence, also the seventh
commandment). This is the overall concern of Leviticus 8-16, for
men must be “clean” to draw near.

Exodus 25-40 describes the establishment of a new Garden of
Eden, the Tabernacle. Leviticus 1-7 discusses the animals in this
Garden, the sacrifices. Leviticus 8 shows the creation of anew Adam
for this Garden, the High Priest and his family. Leviticus 9 brings
the new Adam together with his new Eve, the people of Israel.
Leviticus 10 shows the fall of this new Adam, as his sons Nadab
and Abihu bring upon themselves the wrath of God. The re-
mainder of Leviticus 10 through chapter 16 discusses the subject of
mediation, how men might approach God and not be destroyed.

The third aspect of the covenant is its privileges and rules.
God gives or otherwise distributes things to man, and with that
distribution come laws, stipulations by which man is to administer
