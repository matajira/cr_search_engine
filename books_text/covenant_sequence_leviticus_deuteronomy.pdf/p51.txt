Section Verse Numbered
Law

IL:1b.

TT:2.

1:3.

21,

22.

23,

24.

25.

26.

27.

28.

29.

An Outline and Analysis of Leviticus 19 47

It is not the case that they must die, because
there was not freedom.

And he must bring his Compensation Offer-
ing to the Lorn, to the entrance of the Tent of
Meeting, a ram of Compensation Offering.

And the priest shall cover him [atone for him]
with the ram of the Compensation Offering
before the Lorp for his sin that he sinned, and

he will be forgiven for his sin.

44, And when you enter into the land and you
plant any fruit tree, you shal] regard as uncir-
cumcised its fruit.

45. For three years it shall be to you as uncircum-
cised; it must not be eaten.

46. And in the fourth year all of its fruit shall be
holy, praise offerings to the Lorn.

47, And in the fifth year you may eat its fruit.
This is to increase to you its harvest.

Zam the Lor your God.
48. You shall not eat with blood.
*49, You shall not practise divination.

50. And you shall not practise sorcery.

51. You shail not cut the hair on the side (edge,
corner] of your head.

52, And you shall not clip off the side [edge, cor-
ner] of your beard.

53. And cutting for the dead: you shall not cut
into your body.

54. And the mark of a tattoo you shall not put

- upon you.
Tam the Lorn.

55. You shall not degrade your daughter to make
her a prostitute.

56. And the land shall not become a prostitute

and the land be filled with wickedness.
