38 Covenant Sequence in Leviticus and Deuteronomy

Sabbath
Passover
First-sheaf
Pentecost
Trumpets
Atonement
Tabernacles

As we have seen, three-fold and fwe-fold covenant sequences have
predominated in Leviticus heretofore. Given that chapter 23 has
to do with time and sabbaths, however, we are not surprised to see a
heptamerous presentation, pointing perhaps back to the originally
heptamerous creation of the covenant world in Genesis One:

Sabbath Transcendence Day 1: Light
Passover Passage Day 2: Separation
First-sheaf Grant Day 3: Plants
Pentecost Law Day 4: Astral rulers
Trumpets Convocation Day 5: Fish & Birds!”
Atonement Sin Day 6: Fail of man
Tabernacles Relaxation Day 7: Sabbath'#

It remains to note an “eighth day” motif in Leviticus 23. After
the initial presentation of Tabernacles in 23:33-36, we have a
statement summarizing the whole chapter in 23:37-38. We then
return to Tabernacles in 23:39-43, with an emphasis on eschato-
logical blessing. In this way the passage seems to press beyond the
first creation into a new one.

17. This step is much more obscure than the rest. I believe it fits, but we must
think in terms of biblical symbolism to see it (and I may be wrong in my hypo-
thesis, of course). According to Numbers 10, the trumpets were for calling the
people together, especially for war. Thus, the trumpets had to do both with con-
vocation and with sanction. On Day 5, the waters and air are said to “swarm”
with fish and birds, the convocation idea. In terms of biblical symbolism, Israel
is the land and the gentiles are the sea. Battling the sea and sea creatures can thus
be associated with the use of trumpets for war.

18. Notice that if this scheme is correct, the cycle both begins and ends in sab-
bath, so that both first and last days are sabbaths.
