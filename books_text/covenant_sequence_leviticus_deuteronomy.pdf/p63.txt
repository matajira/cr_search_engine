The Structure of the Book of Deuteronomy 59

In outlining the covenant rehearsals in Deuteronomy I have
found it most convenient to use the five-fold sequencing model. In
Moses’ various sub-sermons, the transition from one aspect of the
sequence to the next is not always abrupt, and it might be possible
to outline these passages differently. What is very clear, however,
is the covenant sequence itself, whether analyzed into three, four,
five, six, or more steps.

Covenant Breakdown and Renewal in 1:6-4:43
A. Covenant Breakdown, 1:6-46
1. God initiated covenant, 1:6-8
2. New socio-political order, 1:9-18
3. Disobedience to stipulations, rejection of distributed grant,
1:19-33
4, Judgment: the people to be restructured, 1:34-40
5. Loss of inheritance, 1:41-46

B. Covenant Renewal, 2:1-4:40
1. God initiates all actions in 2:1-3:11
2. Historical prelude to the distribution of the land:
Esau, 2:1-8
Moab, 2:9-13 (defeat of giant is condition for inheritance)
Ammon, 2:14-23
Sihon, 2:24-37
Og, 3:1-11 (giant finally defeated)
3. Distribution. of land and accompanying rules, 3:12-4:24
4, Sanctions, 4:25-31
5. Continuity: think back and pass it on, 4:32-40

After his sermon, Moses set up the essential geographical/
hierarchical order for the land (4:41-43), which thus climaxes this
section of Deuteronomy.

II. Moses’ Sermon on the Ten Commandments

The third aspect of a covenant renewal is a grant with accom-
panying rules. It is with a view to Israel’s coming into the land
that Moses preaches his sermon on the ten commandments (Dt.
