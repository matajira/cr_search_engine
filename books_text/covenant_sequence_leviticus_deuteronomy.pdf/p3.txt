TABLE OF CONTENTS

 

Introduction, 0... -..0- 06. eect tect tet eee 1
1, Covenant as a Literary Structuring Device ............ 3
2. The Structure of the Book of Leviticus............... 15
3. An Outline and Analysis of Leviticus 19.

4, The Structure of the Book of Deuteronomy ........... 57
Scripture Index... 0.0... cece eect eee ene 7
Index of Covenant Sequences ......... 66.6.0 e eee eee 74

General Index 0.00.0... cece ccc ence ene e nee 75
