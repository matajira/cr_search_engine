48

Section

Ts.

I6.

Close:

Covenant Sequence in Leviticus and Deuteronomy

Verse Numbered
Law

30.

31.

32.

33.

34.

35.

36.

37.

57. My sabbaths you shall guard.
58. And My sanctuary you shall fear,
Lam the Lorn.

59, You shall not turn to the mediums and to the
spiritists.
60. You shall not seek to become defiled by them.
f am the Lorp your God.

61. _ In the presence of the aged you shall rise.
62. And you shall respect the presence of the elderly.
63. And you shall fear your God.
fam the Lorn.
64. And when an alien dwells with you in your
fand you shall not mistreat him.
65.  Asanative from your midst must be the alien

to you, the one living with you.
66. And you shall love him as yourself, for you
were aliens in the land of Egypt.
Lam the Loxp your God.

67. You shall not use dishonesty in measurement,
for the length, for the weight, or for the quantity.

68. Scales of honesty, weights of honesty, an
ephah of honesty, and a hin of honesty must
be yours,

2 am the Lorn your God, who brought you from the land of

Egypt.

69. And you shall guard all of My decrees and all
of My laws.

70. And you shall do them.
£ am the Loxp,

Extended Comments

The first series opens with the command to be holy as God is
holy. It has to do with personal integrity, the human reflection of
Divine autonomy. It is as a man becomes closer to God in his
