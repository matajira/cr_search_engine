24 Covenant Sequence in Leviticus and Deuteronomy

Sancta trespass, 5:15-16
Unknown sin, 5:17-19
High-handed perjury, 6:1-7

The fifth section, Leviticus 6:8-7:36, concerns the priests. It
also follows the covenant/re-creation sequence, as follows:

Speech 1:
Law of Burnt Offering, 6:8-13
God’s total fiery judgment
Law of Cereal Offering, 6:14-18
God's claim of fealty

Speech 2:
Priests’ Cereal Offering, 6:19-23
Priests cannot mediate for themselves

Speech 3:
Distribution to Priests and People:
Law of Purification Offering, 6:24-30
Law of Compensation Offering, 7:1-10
Law of Peace Offerings, 7:11-21

Speech 4:
Sanctions, 7:22-26 (counterfeit sacraments)
Cut off for eating fat
Cut off for drinking blood

Speech 5:
Honor God; honor His priests, 7:28-36

These five speeches overlap five “laws.” The expression “This
is the law of the --- offering” groups this section differently. Since
God is three and one, and the ultimate artist, we should not be
surprised at this literary polyphony:

Speech 1: I. Law of the Burnt Offering, 6:8-13

II. Law of the Cereal Offering, 6:14-23
A, Layman’s Cereal Offering, 6:14-18
Speech 2: B. Priests’ Cereal Offering, 6:19-23
