28 Covenant Sequence in Leviticus and Deuteronomy

Leviticus 9 shows Aaron entering into his duties and leading the
congregation in sacrificial worship. Then, at the beginning of
Leviticus 10, we read of the presumptuous sin of Nadab and Abihu.
The fire that came from the Lord and destroyed them answers to
the “that you may not die” of Leviticus 8:35, for by offering fire of
their own making the two men had failed to guard the holiness of
God.’ Accordingly, they were destroyed by the flaming sword of
the cherubim (Gen. 3:24; Ex. 25:18-22).

We have seen creation and fall. In Genesis, what follows is the
passing of judgment and the offer of hope of restoration through
sacrifice. This will be the concern of Leviticus 11-16.

The Consecration of the Priests, Leviticus 8-10
A. Chapter 8: The Consecration

8:1-5 Congregation assembled
8:6 Washing— removal of cursed soil
8:7-9 Clothing of Aaron—new Adam
8:10-11 Anointing of Tabernacle—new Garden
8:12 Anointing of Aaron— Spirit breathes new life
8:13 Clothing of Aaron’s sons— helpers fitted to him
8:14ff, Animals brought to Aaron/Adam
8:14-17 Purification Offering, Bronze Altar
8:18-21 Burnt Offering
8:22-29 Ordination Offering:
8:23 Aaron anointed with blood — Passover
—note three places:
right ear (head)
right thumb (hand)
right big toe (foot)
8:24 Helpers anointed with blood— Passover
—note three places again
8:25-28 Gifts for Ged
8:29 Moses’ portion

7. James B. Jordan, Sabbath Breaking and the Death Penalty: A Theological Investi-
gation (Tyler, TX: Geneva Ministries, 1986), p. 52.

8, Compare the blood on Aaron’s extremities with the blood on the horns of
the altar, which itself symbolized the nation (1 Kings 18:31). The crucifixion of our
Lord put blood on His ears (from the crown of thorns), His hands, and His feet.
