58 Covenant Sequence in Leviticus and Deuteronomy

I. The Steward as Vice-Gerent

As a covenant renewal document, Deuteronomy opens with a
declaration of the sovereign who is granting the covenant (1:1-5).
In this case, the “sovereign” is Moses. God had set up the cove-
nant at Sinai, on the basis of His defeat of Egypt and His deliver-
ance of Israel. Moses now renews the covenant, acting as God's
viceroy, on the basis of his (Moses’) defeat of Sihon and Og.
Moses speaks only for God (1:3), but in contrast to what we find in
Exodus, Leviticus, and Numbers, Moses does not simply report
what God told him, Rather, Moses generates his own words, his
own self-conscious, receptively reconstructive “amen” to God. He
repeats God’s thoughts in his own words. It is the Lorp’s covenant
that Moses renews, but it is Moses who does the renewing.

This is not unexpected. Part of the pattern of covenant mak-
ing is that the sovereign defeats his enemies and delivers his peo-
ple. He then builds his house out of the spoil of his enemies. Once
the house is built, he lights the fire on his hearth, “kicks off his
shoes” and relaxes in sabbath, leaving the servants to run the
house.? This is what God has done in setting up the Sinaitic cove-
nant. Moses is the chief servant of the house (Heb. 3:4), and he
now administers the house for God. As chief servant (or prophet,
Num, 12), Moses takes charge and renews the covenant.

II. The New Cosmos

The second aspect of a covenant renewal document is a de-
scription of how the original order or hierarchy of the covenant
broke down, and how a new order has been established. The
Abrahamic order, for instance, broke down in the furnace of
Egyptian affliction, but God created a new order by defeating
Pharaoh and constituting Israel a people at Sinai. The breakdown
and renewal of this covenant is the concern of Moses’ first sermon,
Deuteronomy 1:6-4:40.

3. See Kline, ibid., pp. 76ff.; and Ktine, Images of the Spirit (Grand Rapids:
Baker, 1980), p. 38 ef passim. And see James B. Jordan, Sabbath Breaking and the
Death Penalty: A Theological Investigation (Tyler, TX: Geneva Ministries, 1986),
chap. 3.
