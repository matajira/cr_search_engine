INDEX OF COVENANT SEQUENCES

Three-fold

discussed 3-4
Lev, 1-3 23
Five-fold

discussed 5, 6-14
Pentateuch 9-10

Ten Commandments 10-13

 

Leviticus 15-22
1-7 29-28
6:8-7:36 24-25
9 29

11-16 30-31, 33

17-22 33-36

2% al

Deuteronomy 57
1:6-46 59

: 59
61
61

29:1-30:20 68

32 68

32:48-34:12 69

Seven-fold

discussed 5-6

Gen. i 38, 40, 51-52

Lev. 19 43, 51-52, 56

Lev. 23 37-38

Lev, 24-27 39-40

Rev. 1-22 a

74
