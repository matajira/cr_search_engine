154 The Law of the Covenant

beast, clean or unclean. Sexual adultery, being so close to
spiritual adultery, merits the death penalty in Scripture, in all its
forms (Lev. 20; Dt, 22).!! The specific form of fornication com-
mitted by Eve was bestiality —hearkening to the voice of a beast.
Accordingly, spiritual adultery is likened to bestiality, as the
heathen are likened to beasts (Lev, 20:24f.; Ezk. 23:20).

Finally, anyone publicly sacrificing to any god except the Lord
is to be put to death. Literally this reads, “put under the ban
(herem).” According to Deuteronomy 13:12-18, when a whole city
committed this sin, the city was to be destroyed and burned “as a
whole burnt sacrifice.” In other words, fire was taken from God’s
altar, signifying his fiery justice, and applied to the city and its
corpses. Since they refused God’s sacrifice, they themselves
became the sacrifices. In Leviticus 21:9, the daughter of a priest is
to be put to death, if she plays the harlot, and her body burned,
the implication again being with God’s fire from His altar. Thus, I
take it that the intended application here is that an open idolator
should be put to death, and then his body burned with fire from
the altar.

Idolatry is specifically likened to spiritual harlotry in Exodus
34:

13. But you are to tear down their altars and smash their sacred
pillars and cut down their Asherah,

14. For you shall not worship any other gad, for the Lorp,
whose name is Jealous, is a jealous God,

15. Lest you make a covenant with the inhabitants of the land
and they play the harlot with their gods, and sacrifice to their gods,
and someone invite you and you eat of his sacrifice,

16. And you take some of his daughters for your sons, and his

1. From the Diary of Samuel Sewell: “April 6 [1674]. Benjamin Gourd of Rox-
bury (being about 17 ycars of age) was executed for committing Bestiality with a
Mare, which was first knocked in the head under the Gallows in his sight, N.B.
He committed that filthiness at noon day in an open yard. He after confessed
that he had lived in that sin a year. . . .” Since the crime was committed openly,
there was the testimony of two or more witnesses, Citation from the edition of
M. Halsey Thomas (New York: Farrar, Straus and Giroux, 1973), p. 4.
