Subject Index 305

influence on Egypt, 43, 45
savior of the world, 35n.
seed, 78n.

Joseph (husband of Mary), 148

Joshua, 240, 255

Jubilee, 70, 70n., 135f,, 156n.

Judah, 78n., 190f,

Judaizers, 276

Judges, 53ff., 1718.

Judicial procedure, 176.

Jury system, 232

Justice, 1678.

Justification, 24f., 133
circumcision and, 80n,
resurrection and, 80n.

Keturah, 15, 32, 249

Kid (young goat), 190ff., 2721.
Kidnapping, 67, 89, i04f.
King’s Friend, 82n.

Kinsman redecmer, 364,
Korah, 126n.

Laban, 32ff., 34n., 258n.
Land, sabbath af, 183
Language, Biblical view of, 94, 167
dominion and, 132f,
Law
apparent incompleteness, 72.
applies today?, 48f.
before Sinai, 50f.
binding nature of, 28f.
case laws, 2267,
ceremonial, 16, 197
changed in New Covenant, 12ff.
changing and unchanging, 9n.,
1Uf., Un., 73n., 74n,
characteristics of, 68f.
civil, 117
civil use, 26
criminal, 117
covenant and, 6
coming of, 50ff., 50n.

common, 45
courts, 167ff.
death and resurrection of, 49
divine origin af, 70f.
equity of, 16f,, 18ff.
Equivalence, Law of, 115tf,, 177#f.,
192
esoteric character of pagan law, 71
glorification and, 24ff.
grace and, 6ff.
“harshness” of, 27f.
hierarchy of, J9ff.
judicial, 68H, 240, 242
judicial implications, 26ff.
justification and, 24f,
kinds of, 688.
motive clauses in, 71f.
nat 10 be altered by man, 3
penal sanctions of, 73
public character of, 74f,
recorded at the exodus, 30
retaliation, 113i.
revelation, 24
sanctification and, 24f.
spheres of, 69
state and, 26ff.
theology of, If.
three-fold division of, 68ff.
to be read every seven years, 71
transcript of God’s character, Gif.
two tables of, 22
uses of, 247.
within the Trinity?, 7n,
written by Moses, 9f.
written constitution, 52.
Lawyers, and tithe, 217
Leah, 33f., 78n., 147
Leash laws, 125, 130
Leaven, 186ff., 192
Legalism, 8
Legs, and doorposts/pillars, 82n.
Leprosy, 216
Levi, 51
