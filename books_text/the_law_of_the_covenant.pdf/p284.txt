Appendix G—Four and Five-Fold Restitution 265

mation, five squads of ten men.) Thus, the number five is
associated with might and power.

We also have to look at Genesis 14, the war of the five kings
against the four. The Canaanite kings numbered five, and their
opponents numbered four. At first glance, the symbolism seems
reversed, so perhaps our hypotheses are in error. I believe not,
however. The Canaanites manifested the sin of Ham, in attempt-
ing to seize dominion and preeminence (Gen. 9:18-27). They
were overcome by the four-fold dominion of am alliance of
Shemites (Chedorlaomer), Japhethites (Tidal), and non-
Canaanite Hamites (Amraphel and Arioch). Since Chedorlaomer
is preeminent (Gen. 14:4, 5, 17), the others are to be regarded as
dwelling in the tents of Shem (Gen. 9:27). The Canaanites rebelled,
seeking to assert preeminence, but were defeated. Possibly we
should see Abram as a fifth to Chedorlaomer’s four, especially
since Abram defeated Chedorlaomer (note the language of 14:17)
and thus has final preeminence in the situation. At any rate, in
Genesis 14 we find some corroboration for our hypotheses.® All of
this is background for Exodus 22:1.

 

ard military formation. Also, the supposed second root Amst having to do with
armies only. occurs in one form, the form used in Ex. 13:18; Num. 32:17; Josh.
3M; 4:12; and Jud. 7:11. The more obvious and simpler lexical explanation is
that the number five is here used to refer to military organization.

5. In Exodus 18:21, we have elders over 10s, 50s, 100s, and 1,000s, Why not
over 500s and 5,000s? The reason is that these elders would also have been com-
manders in the Israelite militia. Israel did not have a professional army until the
lime of the kings, and so the ordinary elders would have doubled as military
leaders in times of distress. This system gives us squads of 10 men, arranged in
platoons of 50. Two platoons give us a company of 100. Ten companics give us a
battalion of 1,000, and ten battalions give us a brigade of 10,000. I have dealt with
this more fully in an essay entitled “How Biblical is Protestant Worship?” The
Gentua Papers 25 (February, 1984). ‘This is available in exchange for a contribu-
tion from The Geneva Papers, P.O, Box 8376, Tyler, TX 75711.

6, The notion of four as the number of dominion is not strange. For further
uses of the number five, indicating some type of preeminence, the reader should
consider Joshua chapter 10 in its entirety, and the fact that there were five lords
of the Philistines (Josh. 13:3 and many other passages), and the symbolic usages
in Isaiah 19:18 and 30:17. When David confronted the giant Goliath, he selected
five stones (1 Sam. 17:40}, because there were in fact five giants, one for each of
the five cities of the Philistine pentapolis (2 Sam. 21:15-22). The numbers four

 

 

  

   
