38 The Law of the Covenant

12:13). Similarly, when Moses was journeying toward Egypt, God
tried to kill his son because Moses had not yet circumcised him
(Ex, 4:22-26). God had just told Moses that all the firstborn sons
of the world deserved to die, but Moses still had not placed his son
“under the blood.” Moses’ wife circumcised the young man and
smeared the blood on his thigh, displaying it before God. Now the
lad was shielded by the blood of the Substitute, and God let the
lad alone. !*

Vengeance is covenantal in nature. The rebellion of man was
the breaking of covenant life. Man came. under the curse of the
covenant. The curse of the covenant is, specifically, to be ripped
into two halves and devoured by birds and other beasts (Gen.
15:10f., Jer. 34:19f.). All men are under the vengeance-curse of the
covenant, but Christ takes the place of His people. In this sense,
redemption, which is the foundation of our reentry into the cove-
nant, itself is a covenantal act. In Exodus 11:7, we read that
“against any of the sons of Israel a dog shall not sharpen his
tongue.” This Hebrew expression “sharpen his tongue” means in
English “lick his chops.”§ Gad’s people will not be devoured by
the beast, but God’s enemies will be (1 Kings 21:19-24; Matt.

14. This passage is discussed in detail in Appendix F.

15. This is what the Hebrew text literally says. The English translations are
most unhelpful. The New International Version, which is a. sophisticated
paraphrase rather than a translation, says that no dog shall bark. The New
Berkeley Version says the same, as does the New American Standard Version.
The Hebrew term for bark (navah), seen in Is. 56:10, does not appear here. The
King James Version has “move his tongue,” which is also a mistranslation; The
yerb is Aarats, which the Brown, Driver, Briggs edition of Gesenius's Hebrew Lex-
icon translates as “cut, sharpen, decide,” The NASV margin gives “sharpen his
tongue” as the literal translation of Ex. 11:7, ‘The reference might be to the sound
of sharpening, analogous to growling, but such is not a clear analogy; also, the
Hebrew for “grow?” is hamah. Far more likely is an analogy between the sharpen-
ing of a knife and a dog’s licking his chops in preparation for a meal. The tongue,
then, is a sword or knife (cf. Rev, 1:16; 19:15, 21). The only other time this expres-
sion is used is in Joshua 10:21, Translators have universally taken it io refer to
men, so that the NASY, for instance, has “no one uttered a word [lit. sharpened
his tongue] against any of the sons of Israel.” There is, however, no reference to
men here, and by analogy to the earlier occurrence of the phrase, we should un-
derstand it of dogs. That is to say, not one Israelite was killed in the battle.
