28 The Law of the Covenant

should keep them today, surely they reflect His goodness. Doesn't
this harshness serve to show us that we have too lax a view of sin?
Also, have our modern loose laws done us any good? Modern hu-
manistic law is soft on criminals and harsh on the innocent,
Biblical law is harsh on criminals, and thus protects the innocent,
the widow, the orphan, the poor, and the law abiding.

We have said enough to indicate that our position in this study
is that the state is not exempt from the law of God. The reader is
invited to consult Dr. Greg L. Bahnsen’s Theonomy in Christian
Ethics for an extended discussion of this matter.39

The Binding Nature of the Law

God is the Creator of all men, not only of Christians, and
therefore His law is binding on all men, not only on Christians.
As we have seen, people are bound to the law accarding ta their
circumstances. Not all of the laws apply to every person. Not all
are bound to the laws for fathers, for not all are fathers. All men
are, however, bound io the laws for individuals. Moreover, since
all men are in the state, all men have some degree of responsibility
for the laws of God for the state. The civil magistrate is directly
responsible to obey God’s laws for the state, but every citizen must
work to see that God’s will, not that of sinful man, is done.

By creation, the purpose of the Church was worship. After the
rebellion of man, worship had to be reestablished on the basis of
redemption. All men by nature worship something, but only the
redeemed worship the true God, and thus only the redeemed are
in the Church. The sphere of Church power is limited to the realm of the
redeemed.*° Ultimately, of course, all men will be held accountable
for having or not having accepted the offer of salvation, but as a
sphere of earthly power, the Church is restricted to its membership.

By creation, the purpose of the state was order. After the rebel-
lion of man, order had to be established on vengeance.*! Since all

39. See note 7 above, p. 6.

40. The power of the Church governmentally is sacramental; i.e., the power
to bestow or withhold communion.

41, This is discussed at greater length in Chapter 2 of this study.
