The Book of the Covenant: Its Context 59

94:1-8. After the people had ceremonially passed through death unto
resurrection, they, in the persons of their elders, were privileged to sit
at the Lord’s Table and share a communion meal with Him (Ex.
94:9-11).15 Then God told Moses to come up to Him, to receive in-
structions on building the ‘Tabernacle, the house of prayer.

We cannot go into the details here, but just as the land of
Canaan was a new garden of Eden to the re-created children of
God, so the Tabernacle was a more concentrated form of a new
garden of Eden, even including the Tree of Life (for the golden
candlestick was tree-shaped, and signified the Holy Spirit, the
Lord and Giver of life, Who stands before the throne of God).16
Just as Adam was to start from the throne of God and cuitivate
the garden, and then move out to cultivate the land of Eden, and
then out to the whole world; so Israel would start from the throne
of God in their midst, proceed out to cultivate Canaan, and then
hopefully spread the Word of Life to all the world,

Predictably, the re-created Adam (Israel) did not delay io rebel against
God once again, While Moses was on the mountain, the people left
the Lord and turned to the worship of the golden calf (Ex. 32),
once again replacing God. with an animal (Gen. 3:1-5).!7 As a
result of this act of rebellion, together with several others, Gud
cursed the renewed Adams and cast them out of His presence, to wander and
die in the unlderness (Num. 14:20-25). Indeed, because they had
been cast out of the kingdom of God, they did not practise circum-
cision while they were in the wilderness, because circumcision
was the sign of entrance into the sanctuary, and they had forfeited
the right to go in (Josh. 5:5-7).'8

15. Sharing a communion meal is analogous ta the consummation of a mar-
riage; thus, the Lord’s Supper is termed thc “Marriage Supper of the Lamb” in
the book of Revelation. See Appendix F.

16. On this see M. G. Kline, smuges of the Spirit (Grand Rapids: Baker, 1980).

{9. This is specifically likened to adnitery in Exodus 34:12-17 (and note that v.
18 takes up a discussion of the proper covenant meals with God), See the discus-
sion of this in Chapter 8.

18. Possiily they would not have practiscd it anyway, as long as they were out-
side of a land set apart by Gad for them (as Moses had not circumcised his sons
while in the wilderness, away from Goshen). This is discussed more fully in Ap-
pendix F.
