Appendix D~—State Financing in the Bible 237

Nehemiah’s Head Tax

When Israel was restored from captivity there was no king, for
the nation was a vassal state to other powers. The house of David
continued to have preeminence in the society (cf. Haggai 1:1;
Zerubbabel was a descendent of David), but there is no recorded
statement of any taxes being paid to them.

Nehemiah led the people to place themselves under a cove-
nant to support the rebuilt Temple and its sacrifices. This is
recorded in Nehemiah 10:32-33, “We also placed ourselves under
obligation [literally, imposed commandments on ourselves} to
contribute yearly one third of a shekel for the service of the house
of our God: for the showbread, for the continual grain offering,
for the continual burnt offering, the sabbaths [ie., sabbath
offerings], the new moon [i.e., new moon offerings], for the ap-
pointed times [i.e., the seasonal festival offerings], for the holy
things, and for the sin offerings to make atonement for Israel, and
all the work of the house of our God.”

This rule has not infrequently been connected to the head tax
of Exodus 30, under the erroneous impression that the Mosaic
head tax was an annual assessment. Notice, however, first of all
that it is not a half-shekel but a third of a shekel that is collected.
Second, note that the passage does not say, as Nehemiah 10:29
and 34 do, that this was done in accordance with the law of God
given through Moses. Rather, it expressly says “we imposed com-
mandments on ourselves.” What was done here was surely right,
and in accordance with the general rules God had given, but it is
not purely and simply an application of the Mosaic head tax.

Money was needed to rebuild and maintain the Temple, and
there would be no more mustering taxes collected from holy war.
Nehemiah’s provision took up the slack. Also, there were no
longer any kings to provide the sacrifices, and peaple were not in
a position to reinstitute the entire Levitical order overnight, so
again Nehemiah’s provision was a needed accommodation to the
situation.

Again, this was not a civil tax at all. Its purpose was wholly
cultic.
