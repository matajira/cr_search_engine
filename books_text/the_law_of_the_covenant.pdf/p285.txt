266 The Law of the Covenant

Sheep and Oxen

The numbers four and five are associated with sheep and oxen
respectively. (Both terms used to refer to sheep here can also be
used for “member of the flock,” including goats. Thus, goats are
not excluded from view.) Do we find corroboration for the sug-
gested meanings of the numbers four and five in their correlation
with sheep and oxen? I believe so. These are the animals which
particularly syrnbolize humanity in the sacrificial system. They
are, thus, repeatedly set forth as preeminent analogies for men
(cf. e.g., Lev. 22:27, with Lev. 12).

We should note here that the verb used in Exodus 22:1,
“slaughter,” is used almost always with reference to men. Ralph
H. Alexander comments, “The central meaning of the root occurs
only three times (Gen. 43:16; Ex. 22:1; 1 Sam. 25:11), The root is
predominantly used metaphorically, portraying the Lord’s judg-
ment upon Israel and upon Babylon as a slaughter.”? This again
points to a basic symbolic meaning of this law.

A distinction between the two is set forth in Leviticus 4. In
verse 3, if the high priest sins and brings guilt on the whole peo-
ple, he must bring a bull as a purification sacrifice. Similarly, in
verse 13, if the whole congregation brings guilt upon itself, it must
also sacrifice a bull, On the other hand, in verse 22, ifa leader sins
and brings guilt only upon himself, he must sacrifice a male goat;
and in verse 27, if any one of the common people sins, he is to
sacrifice a female goat or lamb.® What this indicates is that the

 

and five figure preeminently in the dimensions of the Tabernacle and Temple,
the number four generally concerning dimensions (four sides, four corners) and
directions, and the number five generally having to do with thicknesses, lengths,
breadths, and heights.

7. Theological Wordbook of the Old Testament (Chicago: Moody Press, 1980), p.
341,

8. Males were worth more than females, since the first male goat was always
sacrificed (Ex. 13:12), so that there were fewer males than females, and because
one male can perform stud service for an entire flock of females. Also, as men-
tioned in Chapter 10, male kids were frequently eaten; because they were not
needed, they made a good meat dish, Thus, there were relatively few adult male
goats,
