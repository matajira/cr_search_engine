Violence (Exodus 21-12-36) 15

approach that the killing or maiming is accidental, then this law
indicates that a man is responsible for his actions when he deliber-
ately places himself in a situation where he loses control of him-
self. The drunk driver, by analogy, is guilty of murder if he runs
over someone, because he is responsible for getting drunk and
driving under those conditions. There is no excuse for street
fighting (except immediate self defense, and flight is preferable),
and there is no excuse for drunk driving; anyone harmed as a
result is considered to have been deliberately harmed.

Second, in either situation, the unborn child is considered a
person, and is avenged. The Biblical penalty for abortion is man-
datory death. The “physician” responsible for performing the
abortion is a murderer and should be put to death, Since at least
two people are always involved in it, abortion ts conspiracy to commit
murder, and the “mother,” the “physician,” the anesthetist, the
nurses, and the father or boy friend or husband who pay for it, all
are involved in the conspiracy, and all should be put to death for
conspiracy to commit murder. Until the anti-abortion movement
in America is willing to return to God’s law and advocate the
death penalty for abortion, God will not bless the movement. God
does not bless those who despise His law, just because pictures of
salted infants make thern sick.”

In summary, it is difficult to determine whether this case law
deals with an accidental or a deliberate assault. The vagueness of
the wording indicates that we should allow it to speak to either
situation.

IV. Retaliation and Recompense (The Law of Equivalence)

The placement of the so-called lex talionis (law of retaliation)
here indicates that the section on murders and assaults is at an
end. This is the summary of all the previous Jaws in this section

27, tis interesting, though, that the injection of a salt solution into the uterus,
which burns the unborn child to death, is very similar to the eternal punishment
described by God as a permanent salted sacrifice that burns forever: “salted with
fre” and “salted with salt” (Mk. 9:49). The punishment for abortionists meets the
crime.
