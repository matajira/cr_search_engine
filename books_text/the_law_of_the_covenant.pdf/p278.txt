Appendix F—Proleptic Passover 259

We are now in a position to say why God dealt with Zipporah
rather than with Moses in this situation. It is because Zipporab
could more fully signify Israel as God’s bride, in need of a token of
virginity. Thus, Moses is simply absent from this narrative.
Where he was and what he was doing, it is not important for us to
know.

This passage also explains the reference in Revelation 19:7,9 to
the “marriage supper of the Lamb.” The blood of the lamb was the
sign of Israel’s marriage to the Lorp, at Passover. The Passover
feast, thus, was a marriage feast. Passover was the marriage feast
of the lamb. In the New Covenant, the Lord’s Supper fulfills
Passover (and all the other feasts and meals of the Old Covenant
as well). Thus, the Holy Eucharist of the Church is the marriage
supper of the ‘True Lamb of God. Since the Book of Revelation is
arranged in the order of a worship service,** we expect the Lord’s
Supper to come at its climax, as here it does. In a very precise
way, then, the phrase “marriage supper of the Lamb” refers to the
fulfillment of Passover.

In paganism, the marriage relation between a man and his
god is seen in sexual terms. Thus, sexual relations are sacramen-
tal in pagan religions, and repeatedly in Scripture this “fertility
cult” form of religion is warned against (for an example, see 1
Sam. 2:22), Because of the Creator/creature distinction, there is
no sexual relationship between God and man. The sexual rela-
tionship between man and woman symbolizes the Spiritual mar-
riage between God and His bride. The act of this Spiritual mar-
riage is not ritual fornication in a temple, but the communion

 

sandwiched in between twa notices of the unfitness of Esau’s wives (27:46 - 28:9).
Thus, it is because each woman in succession signifies the Bride, and each man
in succession signifies the Secd/Lord, that cross-generational symbolic structures
are appropriate. In one very real sense, the woman gives birth to her seed, who
will grow up to become her deliverer, lord, and husband. The Bible strictly for-
bids any actual physical (sexual) acting out of this theology, thus reserving it
wholly for the realm of symbol. In common life, the seed is the “bloody
bridegroom” of his mother only in a symbolic sense, a sense fulfilled in Christ's
redemption of the Church.

45. This can be seen from Revelation 1:10, but the worship pattern prevails
throughout the entire book.
