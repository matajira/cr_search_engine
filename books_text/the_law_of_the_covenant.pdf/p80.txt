4

THE ORDINANCES: STRUCTURE
AND CHARACTERISTICS

The actual covenant God made with Israel consisted of two
parts, the Zén Words and the Ordinances or Judgments (Ex. 24:3). We
have argued that, although God wrote the Ten Commandments
into stone later on (Ex, 24:12), Moses wrote them down for the
covenant-making ceremony, so that the Book of the Covenant
contains both the Ten Words and the Ordinances. It ig not exactly
clear what parts of Ex, 20-23 were included and which might not
have been. The passage breaks down like this:

1. The Ten Commandments, Ex, 20:2-17,

2. The People’s Response, Ex. 20:18-21. This was probably
not-included in the Book of the Covenant, though it might have
been.

3. A Brief Warning against False Worship, Ex, 20:22-26, This
section is neither Word nor Ordinance, but was probably part of
the Book of the Covenant,

4, The Ordinances, Ex. 21:1-23:19 (or 23:33).

5. (Possibly) Warnings, Ex. 23:20-33. This section does not
consist of actual laws, but again probably was part of the Book of
the Covenant.!

This study is actually concerned with the Ordinances, and for
our purposes we shall consider only Exodus 21-23, contenting our-
selves with only a couple of remarks on Exodus 20:22-26:

1. Curses and blessings seem to be part of the norma! covenant structure. See
Meredith G. Kline, The Structure of Biblical Authority (Grand Rapids: Eerdmans,
revised edition 1975).

61
