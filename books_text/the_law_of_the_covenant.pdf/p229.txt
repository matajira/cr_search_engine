210 The Law of the Covenant

because of its typological nature, the Levitical tithe is always
spoken of in terms of the agricultural year. The tithe is seen as col-
lected annually, and given to a centralized Church order. In the
New Covenant, the tithe is given to local churches, and the em-
phasis is on weekly rather than annual contributions (1 Cor. 16:2),

9. Of course, agriculturalists and self-employed persons may
and probably should continue to tithe on annual increases. Wage
earners, however, should tithe on their paychecks as they arrive,
weekly if possible.*

10. The stress in 1 Corinthians 16:2 on laying aside the tithe on
the first day of the week gives a New Covenant focus to the
first fruit offerings of the Old Covenant. The time-honored
custom of paying the Church before paying anything else is based
on this.

li. How many tithes were there? Deuteronomy 12:17 and 14:23
speak of a tithe on “grain, new wine, and oil.” Leviticus 27:30, 32
speaks of a tithe on seed, fruit, and animals. If we take these as
two different tithes, we should notice that they do not overlap.
Together they do not constitute 20% of the whole [.10(a +b) + .10
(c+d)=.10(a+b+c+d)]. Thus, the total tithe remains at 10%.

12. More likely, however, these specifications should not be
taken to mean different tithes, but different aspects of one tithe.
We must beware of an overly nominalistic hermeneutic, which
assumes that because different terms are used for the same thing,
different things are in fact meant. Leviticus 27 is concerned with
vows and their redemption, and the tithe is here seen as a form of
vow (cf. Gen. 28:20, where Jacob’s tithe is seen as a vow). The
fact that Numbers 18 speaks of the tithe as going to the Levites
does not contradict Deuteronomy 14:22-29, which tells us that the
tithe was used to finance participation in the feast before being
turned over to the Levites. The expression “grain, new wine, and
oil’ is used in Deuteronomy 7:13 as significant of all the blessings
of the land.

4. Wages must be seen as income, since the power to earn wages comes from
God, just as the yield of a field or vineyard comes from Him. Thus, clearly a tithe
is owed on wages.
