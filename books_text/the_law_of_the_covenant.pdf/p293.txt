274 The Law of the Covenant

milk-stage of infancy. The destruction of Jerusalem is likened to
the casting out of Ishmael (Gal. 4:21). What all of this cor-
roborates and fills out is that there is a peculiar tie between a child
and its mother during the nursing years.

Then we look at the concept of boiling, We find twice that
when cities were under siege, women boiled and ate their own
nursing children (Lam. 4:10; 2 Ki. 6:28f.). This definitely tends to
corroborate our initial hypothesis, that the primary purpose of
this law is to regulate human conduct.

To summarize: What we have found thus far is nothing that
contradicts our initial hypothesis, and much that seems to cor-
roborate it and fill it out.

We can now expand on the hypothesis and breaden our con-
ception, Apparently, a child stayed with its mother until it was
weaned. The feast Abraham threw al the weaning of Isaac in-
dicates that the mother presents the child to the father on that oc-
casion. She has done her initial work. Now the child is ready for
“solid food” (Heb. 5:13-14). That a mother should devour her nur-
sing child is, thus, not simply a horrible act of cannibalism, it is
also a consuming of her sacred trust. The child is not hers to
possess; rather, the child is in her stewardship. She is supposed to
rear him to a certain point, and then present him to her husband.

What our broadened conception points to is an analogy be-
tween weaning the child and paying our tithes and offerings to
God. In terms of the general teaching that we are the Bride of
Christ, we can say that all our works are like children. We are
supposed to present them to our Lord and Husband, once we
have developed them to a certain point. To consume the works of
our hands, without first tithing on them, is equivalent to eating
our own nursing children. That is our hypothesis at this point.

Now, we turn to the context of these three laws. What do we
find? We find that each time the law occurs in a context connected
to the Feast of Ingathering, when the tithe is presented (note con-
texts of Ex. 23:19; 34:26; Dt. 14:21). Again, this fits with the
overall hypothesis as it has developed. The positive injunction is:
Pay your tithe at the time of the harvest. The negative injunction
