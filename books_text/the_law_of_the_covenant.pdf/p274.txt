Appendix F—Proleptic Passover 255

the royal household and 40 years of age, and his action in judging
and executing the Egyptian was not an act of lawless violence, but
an execution of lawful justice. The Bible never criticizes Moses for
this, but presents his action as righteous and faithful (Acts 7:24ff.;
Heb. 11:24ff.). The execution of criminals is never said to defile
the land, or to require atonement; such execution is itself the
atonement required. Thus, in my considered opinion, I think it
unlikely that it was Moses’ killing of the Egyptian which called
forth the Avenger.

The other possibility is that God acted here ta teach Moses the
basic principle that He was about to use in the Passover. Thus,
Moses came to be in a better position to explain God’s ways to the
people; The land was defiled, and all men were living on borrowed
time, for the Avenger was coming soon. Substantiation for this
opinion comes from the context in which this incident took place.
The Lorp referred to the “wonders” He was going to perform in
Egypt (v. 21). The “miraculous signs” are referred to in verse 28.
These two paragraphs bracket the incident at the lodging place.
Thus, this incident was one more sign to Israel of how God was
going to deal with them. The things that happened to Moses were
types or shadows of what would happen to those in union with
Moses, The experience of the head precedes that of the body.
Moses’ flight from Egypt, his forty years in the wilderness, his
rearing sons there, and their circumcision at the time they draw
near to God, are recapitulated in the experience of the Israelites,
though with some differences. (The reader should remember that
the Hebrews did not practice circumcision in the wilderness, but
their sons were circumcised by Joshua when they entered the
Land of Promise.)’”

37. The Bible indicates, by this, a connection between the rite of circumcision
and Israel's position as a priest to the nations. There is no indication that true
believers outside Israel were supposed to practice circumcision, It was tied to
Israel as the seed-throne-sancluary people. When Israel was out of the sanctuary
land, or away from God’s special presence, they did not practice circumcision, it
seems, Thus, Moses was not necessarily at fault for not having circumcised his
sons. But, once he came into God's special presence, circumcision was again nec-
essary. In the New Covenant, there is no longer a division of labor between seed-
