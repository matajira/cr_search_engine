252 The Law of the Covenant

defiled with blood which has called up the Divine Blood Avenger,
Moreover, he is drawing near to God’s Holy Mountain, resting
place of the Divine Blood Avenger. God, the Avenger, is going to
visit Egypt, accompanying Moses. God (perhaps) joins Moses’
party here at the lodging place, and when He does so, He finds that
Moses has not heeded His warning regarding the firstborn sons.
God is going ta go with Moses to bring blood vengeance on Egypt,
but before doing so, He is going to make Moses an object lesson to
the Egyptians, by giving Moses a proleptic experience of Passover.

God attacks “him.” The logical reference for the “him” in verse
24 is the word “firstborn” in verse 23. If we assure that God
attacked the firstborn son, and Zipporah circumcised him, then
we simply assume that Moses was not present at the time (which
is entirely possible, since he might have been watering the
animals, obtaining food, paying the bill, etc.). In this case, we see
that God is attacking Moses’ family by attacking his firstborn.
Possibly, God was attacking Moses, and only the salvation of
Moses’ firstborn would save his family, and therefore Moses him-
self. The text is deliberately vague; God could have made it clear
for us if it were important. Theologically, Moses and his son are
in the same position, under the threat of death, and the simplest
way to take the text is to hold that God was attacking Gershom.

In order to avert destruction, Zipporah circumcised her first-
born son, Then she smeared the blood on “his” legs (not “threw it
at his feet,” which is an indefensible translation), On whose legs?
Most likely, on her son’s legs. This made the atoning blood visible
to God, and He stopped His attack. (If God were attacking
Moses, it would still have been more practical for Zipporah to
smear the blood on her son’s legs, than to get near a possibly
struggling Moses.)

Then, Zipporah addressed “him” and called him a “bride-
groom of blood” (not a “bloody husband,” a mistranslation).%?

32. The Hebrew term, hathan, means either “son-in-law” or “bridegroom,” as
in Psalm 19:5; Isaiah 61:10; 62:5; Jeremiah 7:34; 16:9; 25:10; 33:11; and Joel 2:16,
It never means simply “husband.” The two Hebrew words for “husband” are ‘ish
{man) and éa‘al (lord).
