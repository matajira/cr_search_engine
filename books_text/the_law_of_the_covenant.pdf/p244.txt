Appendix D

STATE FINANCING IN THE BIBLE?

The Mosaic Head Tax

The head tax in Exodus 10:11-16 is sometimes thought to have
been a civil tax in ancient Israel. Since each man paid the same
amount, it is argued that modern Christian states should employ
the same poll tax principle? There are problems with this view,
and in the first part of this essay we need to explore the Mosaic
head tax to discover its actual purpose.

Exodus 30:11-16 reads as follows:

ii, The Lorn also spoke to Moses, saying,

12. When you take a census [literally, sum] of the sons of Israel
to number them [lit., for their being mustered], then each one of
them shall give a ransom for himself [lit., his soul or life] to the
Lorp, when you number [muster] them, that there may be no
plague among them when you nuniber [muster] them.

13. This is what everyone who is numbered [who passes over to
the group which is mustered] shall give: half a shekel according to
the shekel of the sanctuary (the shekel is twenty geras), half a
shekel as a contribution [heave offering] to the Lorn.

4, Everyone who is numbered [who passes over to the group

1. An earlier and less complete form of this essay was published in newsletter
form by the Institute for Christian Economies in 1981. 1 have revised my opinion
on a couple of points since that time, so this version should be regarded as my
latest thinking on the subject,

2. This is the position of R. J. Rushdoony, Jnstitutes of Biblical Law
(Phillipsburg, NJ: The Craig Press, 1973), pp. 281f., 492, 510, 719. Rushdoony is
not alone in this opinion, however.

225
