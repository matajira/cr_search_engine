xii

Vi

The Law of the Covenant

TH. Laws Concerning Property and Stewardship 65
TV. Laws Concerning Marriage and Faithfulness 63
V. Laws Concerning Witness-Bearing 66
VIL Laws Regulating Time and Rest 66
VIL Epilogue: Exhortations to Obey God
and Conquer Canaan 66
Characteristics of the Case Laws 68

 

 

Slavery 0. teen eee 75
Male Slav 6

The Circumcision of the Ear 78

Female Slaves 84

Slavery in the Bible 87

Practical Observations 90

Violence... ect e een tn eee 93

The Nature of Violence 93
Capiral Offenses 96
1. Premectitated Murcer 97
HU. Accidental Manslaughter 97
TH. Striking Parents 103
IV. Kidnapping and Slave Trading 104
¥. Repudiating Parents 103
Assault 109
I. Fighting or Dueling 100
IL. Slave Beating 112
UY. Bystanders 113
TV. Retaliation and Recompense
(The Law of Equivalence) 115
V. Equivalence for Slaves 121
The Goring Ox 121
1. The Rebellious Beast 122
If. The Incorrigible Beast 124
TH. Ransem 125
IV. Appropriate Punishment 125
V. The Price of a Slave 127
Hazardous Property 128
I. The Open Pit 129
H. Ox Gores Ox 130

  
