Conclusion (Exodus 23:20-33) 195

Indeed, right away in Exodus 32 they fell away, and received the
implied curse of this passage, as the God Who had promised to
protect thern if they were faithful turned against them because of
their faithlessness.

Practically, we see from this a program of Christian conquest.
We are advised that we should not expect to take over society as a
Christian minority, but to work at influencing and changing soci-
ety little by little, We have no mandate to exterminate whole races
of people today, except as we “exterminate” them by means of con-
verting them into the Christian race. The roots of the tares are in-
tertwined with those of the wheat (Matt. 13: 24f.), and the former
cannot be uprooted without also destroying the latter. Gradual-
ism, therefore, is the order of the day.

We may expand on this to point out that Christians frequently
do not know how to run society. Suppose all non-Christians
resigned from all government posts tomorrow, and invited Chris-
tians to take over. Do we have Christians in our local community
who know the ins and outs of running the water department, or
the sanitation department, or how to use firearms against
criminals? Probably not. Christian infiltration and influence
must, therefore, be gradual.

The promise not to drive out the Canaanites all at once, then,
is most gracious of God. He will not leave His children in dire
straits. He will use the Canaanites to protect us from the “wild
beasts,” until we are mature enough to take over on our own.
Here, then, is the method by which Christians may work to im-
pose these righteous civil laws upon societies in the world today.
