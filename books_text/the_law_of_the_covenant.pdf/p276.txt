Appendix F—Proleptic Passover 257

blood. In polite society, we do not usually discuss the blood of the
wedding night,*° but what other possible meaning is there for the
phrase “bridegroom of blood”? Somehow, the blood of circumci-
sion is equivalent to the blood of the wedding night. How shall we
understand this?

The answer is that God demands His bride be a virgin.*! This
is clear from 2 Corinthians 11:2,3: “For ] am jealous for you with a
godly jealousy; for I betrothed you to one husband, that to Christ
I might present you a pure virgin. But I am afraid, lest as the ser-
pent deceived Eve by his crattiness, your minds should be led
astray from the simplicity and purity (of devotion) to Christ.” We
see from this that Eve lost her Spiritual virginity when she
hearkened to the voice of the serpent, and as a result she began to
bear his seed alongside her own (Gen. 3:15).42 The bride has
played the harlot.

The bride, thus, can provide no blood on the wedding night.
She is condemned to death, by the law of Deuteronomy 22:13-21,
which states that a woman must have a token of virginity, a blood-
stained wedding sheet, to prove that she is not a whore. What will
be done for such a woman, if her husband truly loves her? He will
provide his own blood to stain the sheet, to provide her with
tokens of virginity. Just as it was blood from her “private parts”
which would have been her token, so it must be blood from his,
for it is at this part of their bodies that they become “one flesh.”
The blood of the wedding night is the visible token of their
oneness, blood which flows from the very place at which they
become one flesh. Since the woman cannot provide it, the circum-

 

 

40, Possibly Ezekiel 16:9, taken in context, makes direct reference to the blood
of the wedding night. At the very least, the blood washed away here conflates
that of birth and marriage

41, ‘To symbolize this, the wife of the high priest had 1o be a virgin, Leviticus
21:13. See also the parallel between the destruction of Jerusalem, God’s wife, and
the death of the wife of acting high priest, Ezekiel, in Ezekiel 24:16ff,

42. After all, angels do not marry or give in marriage. The seed of the serpent
must come, thus, through the woman. We see the fulfillment of this in Genesis 4,
where the two gons are the two seeds.
