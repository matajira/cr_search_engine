Property (Exodus 22:1-15) 133

notion that using the spirit’s name gives the magician power over
him.

Finally, language is the first stage of creativity. God created
yia the Word. All cultures and societics are shaped by verbal con-
cepts. This is why proclamation of the Gospel is the foundation
stone of Christian society. Scientific advance usually comes about
when a new understanding or new verbal distinction arises. The
Reformation was based on a verbalized distinction between
justification and sanctification; Rome had confused the two.

If language is the first stage and prerequisite of dominion,
property is the second. Adam was given the garden to beautify
and protect (Gen. 2:15), He was to name it, get power over ir, and
creatively remold it. The eighth commandment protects private
property, as do other provisions in the law of God (cf. esp. Lev.
95:13; and see 1 Ki. 21). Every man is to have his own garden. His
marriage and his garden (work) are the major axes around which
the ellipse of his temporal life is drawn. In pagan aristocratic
societies, few men have gardens, and many men are slaves, More-
over, such aristocrats often exercise only minimal dominion,
preferring to war or entertain themselves.

Under the influence of Christian concepts of familistic prop-
erty, the free market has acted to break up such large aristocratic
holdings. The industrious poor eventually buy out the lazy rich,
and anyone with thrift can eventually obtain his own garden,
Dominion is multiplied.

There are two basic sections in the Ordinances concerning
property. These two sections can each be further divided:

A. Seven cases concerning the unauthorized invasion of
another's property:

1. Five cases concerning the punishment of a thief who

breaks into his neighbor's property (vv. 1-4).

2. Two cases concerning pollution (vv. 5-6).

B. Eight cases concerning the abuse of the authorized use of
another’s property:

3. Five cases concerning bailment or safekeeping (vv.

7-13).
