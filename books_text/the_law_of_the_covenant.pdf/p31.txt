12 The Law of the Covenant

(Eph, 5:25). This law is applicable in all times and in all places.
Applicable —that is, ad/e to be applied. If, however, a man is not
married, then this law in fact does not apply to him. It is ap-
plicable, but because of circumstances it is not applied.

The law of God commands that we are not to have an open pit
in our yard (Ex. 21:33f.), and that we are to have a rail around
our roof (Dt. 22:8). If a man lives in an apartment, however, he
will never have the opportunity to leave an open pit in his yard,
As regards the rail around the roof, the houses in ancient Israel
had flat roofs, and people frequently spent time on their roofs. In
colder climates, snow threatens to cave in a flat roof, so raofs are
sloped. This law would then not apply. It would apply, however,
to a raised porch or balcony. Again, circumstances make the
difference.

Thus far, the circumstances we have been discussing have
been legal (marriage) or geographical (location). There are also,
however, historical or temporal circumstances. As regards our
topic, the major change in historical circumstances occurred dur-
ing the first century 4.p. There were four interlocked important
events during that century which changed the way in which God’s
unchanging law is applied. They were: 1) the sacrificial death of
Christ, 2) the outpouring of the Holy Spirit in power, 3) the
removal of the defilement of the world, and 4) the destruction of
Jerusalem. These four events altered four aspects of Old Testa-
ment law, as regards the way it is applied.

First, the sacrificial death of Christ replaced all the bloody rites of the
Old Testament. Such matters as circumcision, Passover, the Day of
Atonement, the sprinkling with water mixed with ashes of a red
heifer, the redemption price of the firstborn~all these had to do
with blood or with death. They pointed to the coming death of
Christ. Christians still keep these laws today, but because of
changed circumstances, they keep them in a different manner.
The Old Testament believer, when he performed these acts, put

16. Other theologians might list more or fewer than these. | have tried to cover
all the necessary bases with these four.
