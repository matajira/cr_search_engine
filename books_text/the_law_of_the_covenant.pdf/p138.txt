Violence (Exodus 21:12-36) Lis

the same, equivalent compensation is not ruled out.

The idea that monetary compensation would favor the rich is
a logical error, The same standard of punishment commensurate
compensation— would apply to all; thus there is no favoring of
any class in society. Both rich and poor resent having their money
taken away. The fact that the poor assailant might have to endure
slavery in order to pay his fine, which the rich would not, is a
reflection of their previous socio-economic condition. A man’s secio-
economic background or condition is irrelevant in considerations of justice,
which views all men equally, Tf we do not favor the rich, neither may
we favor the poor. It is a logical error to assume, then, that such a
systema of justice would favor one group or another. The perceived
advantage of the wealthy is due to his previous circumstances, not
to the system of justice.

All the same, there is no reason to believe that society main-
tained then, or should maintain now, a schedule of monetary
equivalences for bodily parts, so that a hand is worth so much,
and an eye so much, etc. The payment is “as the judges deter-
mine,” and it is quite likely that the rich man would be made to
pay a larger amount than would a poor man. I argue in Appendix
G that the attack of a rich man upon a poor one requires four-fold
restitution as punishment.

This raises the question of whether a man might demand a
physica! equivalence in lieu of monetary payment. In light of the
fact that physical equivalence was justly effected in Judges 1:6, 7,
we cannot say that there is anything wrong with it. Doubtless a
blinded man might demand that his assailant be blinded, and re-
ject a monetary composition; though ordinarily his friends would
probably prevail on him to take the money.32

‘Thus, in the actual case described in Exodus 21:22-25, a man
might take a monetary composition if his infant son’s foot was
damaged. The monctary composition would doubtless be far
greater than the fine for inconvenience assessed in the case where

32. An elaborate discussion of che ins and outs of this law is found in Gary
North, The Dominion Covenant: Exodus (Tyler, TX: Institute for Christian Eco-
nomics, 1985).
