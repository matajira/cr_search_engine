14 The Law of the Covenant

until evening.'8 If he touched a human corpse, and then touched
anyone else, that person too would become unclean.!* Thus, he
had to avoid all human contact for a set period. In this way he was
cut off from the people.

These laws of cleanness, as we shall see in this book, signified
life and death. Man’s fall had defiled the world. Thus, clean per-
sons, clean animals, clean land, etc., were the exception, only
made clean through blood. In the New Covenant, all has been
definitively cleansed (Acts 10, 11).2¢ The death of an animal or ofa
person still reminds us of the curse on sin, but it no longer defiles us.
The only place that remains defiled is the heart of man. The world
is no longer defiled, but redeemed, Those men who refuse to par-
ticipate in redemption, and remain defiled, will be removed from
the world and sent to the lake of fire. :

Fourth, the destruction of Jerusalem, and thus of the Old Testament’s
culturally-tied Church system, changed the way certain laws are kept. The
episile to the Ephesians speaks about this alteration in the way we
adhere to Biblical legal principles in 2:11-22, referring to a
category of laws which constituted a wall of division between Jew
and Gentile. We may call these “boundary laws,” for they were de-
signed, in part, to set a visible boundary between God's people
and the people of the world, in order to show that Israel was a
priest to the nations. With the coming of the Spirit and the
destruction of Jerusalem, these boundary laws were no longer to
be observed in the same way. There are still, of course, moral and
religious boundaries between God’s people and Satan’s, but there

18. “But all other winged insects which are four-footed are detestable to you.
By these, moreover, you will be mads unclean: whoever touches their carcasses
becomes unclean until evening’ ” (Lev. 11:23, 24).

19. “‘Furthermore, anything that the unclean person touches shall be
unclean; and the person who touches it shall be unclean until evening’ * (Num.
19:22).

20. This is why consistent Protestants do not “consecrate” church buildings
and other material objects. There is no need to do so, Indeed; to do so implies
that Pentecost has not really and definitively cleansed the world, and thus
obscures the Gospel. Setting aside buildings for a special use is a different matter,
since the bipolarity of special/general is not the same as the bipolarity of
deansed/defiled,
