The Bible and the Laws of God 27

practices among the world’s religions. We already are somewhat
intolerant of other religions. (See No. 5 below.)

4, “But if we do not tolerate them, they will not tolerate us.” Well,
according to the Bible we are in a war to the death anyway, and it
certainly seems that in most places of the world Christians are not
being tolerated. Even in America, Christian schools are under
constant attack. Besides, who is our protector? Are we to trust
God to protect us, or are we to enter into détente with non-
Christians?

5. “How can we evangelize non-Christians if we execute them?” This is
not a problem, since Biblical law does not require the execution of
those who hold to other religions. Only if they actively promote
their anti-Christian views and thus try to destroy the Christian
Republic, does the Bible teach that action should be taken against
them (Dt. 13:2, 6, 13).

6. “Weren't these civil laws for the Jews only?” No. As we shall see in
our study, the “civil laws” were given long before Moses, and were
known among the nations, even though the nations had departed
from the faith.

7. “Weren't Church and state tied together in a special way in the Old
Testament?” Yes. Well, doesn’t that imply that these civil laws were
designed for that peculiar situation? Haven’t our circumstances
changed? Yes, our circumstances have changed, but it is not
primarily the state which has undergone the change; it is the
Church which has been changed in structure. The Old Testament
Church was for many centuries tied to one particular civil order
(Israel) for protection, as we have seen. The Church is no longer
tied to any particular place or culture, It is the Church that has
been changed. God’s laws show what a good social order will be
like, whether or not the Church is tied to it in any special way. We
should study the relationship between Church and state in the
Bible, and see what relevant wisdom we can draw for a Christian
society,

8. “But some of these laws are so harsh... .” Perhaps to our
modern ears they seem harsh, but we must be careful not to accuse
God of sin. He gave these laws, and regardless of whether or not we
