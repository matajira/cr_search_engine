106 The Law of the Covenant

their parents, and the verb used is the verb “to make heavy, to
glorify.” Thus, to make light of, to despise, is the opposite. An ex-
ample of this is clearly set out in Deuteronomy 21:18-21: “If a man
has a stubborn and rebellious son who will not obey his father or
his mother, and when they chastise him he will not even listen to
them, then the father and mother shall take hold of him and bring
him out to the elders of his city at the gate of his place, And they
shall say to the elders of his city, ‘This son of ours is stubborn and
rebellious; he will not obey us; he is a glutton and a drunkard,’
Then all the men of his city shall stone him to death; so you shall
remove the evil from your midst, and all Israel shall hear of it and
fear.”

Notice that it is an older child who is in view, not a little boy; he
is old enough to be a drunkard. Second, notice that the sin is a set-
tled disposition to rebel, not a one time act of disobedience.
Third, notice that the young man has given public witness to his
rebellious heart; the parents can remind the judges that they all
know he is a drunkard and a glutton. Note, fourth, that the
parents do not have the power to deal with this rebel on their own;
they have to bring evidence and testimony to the judges. This
shows us how the law was carried out, and what is involved in
making light of one’s parents, ridiculing them, and repudiating
them.

In 1 Timothy 5:3, 17, to “honor® someone means to give them
money, to care for them financially.'# [n line with this understand-
ing, Jesus applies the death penalty for dishonoring parents
directly to those who refuse to care for them in their old age. Mark
7:9-13, “He was also saying to them, ‘You nicely set aside the com-
mandment of God in order to keep your tradition. For Moses
said, “Honor your father and your mother,” and “He who reviles
father or mother, let him die the death,”*® But you say, “If a man

18. Obviously, 10 honor someone entails more than just money, but in a prac-
tical, down ta earth, legal sense, giving money is a strong evidence of honor. Proof
that money is partially in view in 1 Timothy 5:3, 17 is in vv. 16 and 18.

19, “Die the death” is a Greek rendering of the Hebrew doubling spoken of in

footnote 5 above. Jesus very definitely establishes the death penalty for this
offense.
