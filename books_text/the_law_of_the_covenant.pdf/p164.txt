8
FAITHFULNESS (EXODUS 22:16-31)

The laws in this section are generally regarded as randomly
thrown together, or arranged for mnemonic purposes. Originally
Thad placed these under the “Property” heading, because so many
seem to have to do with the interplay between rich and poor. Fur-
ther reflection, however, has caused me to believe that these are
best seen as a section elaboration on the seventh commandment,
“Thou shalt not commit adultery.” Partly this is because we should
expect a section on the seventh commandment, when the other
commandments having readily applicable civil and  sacial
ramifications are dealt with (commandments five through nine).
Also, the laws of vv. 16-17 indicate a bridge from property laws
into marriage laws. Finally, and most convincing to me, all the
laws in this section can easily be fit into the general Biblical view
of the marriage between Goi and his people (on this, see Chapter
4, and Appendix F).

When God made covenant with Israel at Mt. Sinai, He mar-
ried Israel. Israel was expected to be faithful to the Lord, as He
was to Israel. Immediately, however, Israel played the harlot, and
aroused the Divine jealousy (Ex. 34:14). The Divine Husband
caused His faithless Bride to undergo the ordeal of jealousy
(Num. 5; Ex. 32:20). In later years, God frequently was to refer
back to His marriage to Israel—Ezekiel 16, Hosea 2, Malachi
2:11-16 being three prominent passages, The New Testament con-
tinues this imagery, explaining that Christ is the Husband of the
Church (Eph. 5:22-33).

All the laws in this section have to do with covenant faithful-

145
