32 The Law of the Cavenant

to become the peculiar custodians of the truth, and told Abram
that the Savior would spring from his line.t Throughout the Old
Testament period we see that God had His covenant people among
many families and nations (e.g., Melchizedek, Jethro, Jonah’s
Ninevites); there is no reason to think that God was parsimonious
with His saving grace during the Old Testament period. God did,
however, have special dealings with ihe seed people, Abraham's
descendents through Jacob.

When God established His covenant community-oflife with
Abraham, this meant that Abraham was restored, in essence, to
the garden of Eden environment. Thus, we are not surprised to
see that God guaranteed him /and, Even though Abraham did not
formally own the land, he did exercise dominion over it, as
Genesis chapter 14 shows, and he did acquire a plot of it as a token
of his inheritance (Genesis 23). Abraham sent his other sons to the
East, to Havilah, thus establishing the bipolarity of sccd-
sanctuary priests (Isaac) and the nations (Ishmael and the sons of
Keturah), .

The history of the seed-throne people is a sermon to humanity.
Israel was to be a light to the nations, teaching them God's ways.
Thus, the history that God charted for Israel illustrates His prin-
ciples (laws) and repeatedly portrays in various dimensions the
principles involved in man’s fall and redemption.

Whenever Gad’s covenant with men is reestablished, blessings
follow, This brings on the envy of the wicked, and renewed enmity.
We see this principic repeatedly exemplified in the history. recorded
in Genesis. Thus we are told that God blessed Isaac, that Isaac
became rich, then richer, and finally very wealthy, and that as a
result the Philistines envied him and began to persecute him,
finally driving him out (Gen, 26:12-16), Similarly God blessed
Jacob despite Laban’s attempt to cheat him, so that Jacob became
very wealthy, and Laban began to hate him, finally forcing Jacob

4, Between the Mood and the call of Abram were 427 years. Sce Martin
Anstay, Chronology af the Old Testament (Grand Rapids, Kregel, [1913] 1973).
