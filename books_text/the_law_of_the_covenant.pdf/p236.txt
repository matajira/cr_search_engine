Appendix C—Tithing: Financing Christian Reconstruction 217

professional lawyer class that existed in Israel, for they were ex-
perts in the law of God.

43. Thus, the tithe should be used to maintain a corps of pro-
fessional theologians and legal experts, as well as educators. The
Church must ever advise the state regarding its duties before
God, Rightly do the confessions of the Reformation state that the
civil magistrate has the power to call Church synods to advise
him. In light of this, it would be proper for a church to use part of
its tithe to assist men in law school, so that they will “know the
enemy” better,

Worship

44. The Levites were professional musicians within the
Church, Worship in the Bible centers around teaching, the
sacraments, and the response in singing and dancing. The Bible
shows us that it is God’s will for his people to be trained in proper
worship, and to be led by skilled professionals (1 Chron, 15:16-24;
25:1-7; Ps. 149:3; 150:4). This use of the tithe is almost completely
overlooked by the conservative and platonic Reformed and fun-
damentalist churches. The result has been the secularization of
music, the reduction of dance to an exclusively erotic function,
and the fragmentation of life; not to mention the fact that people
do not know what to do with themselves on the sabbath, The rein-
troduction of wholistic worship to our dying churches will take
time, but it is part of the work of the tithe to pay for it.

The Foundation of Society

45, The purpose of the tithe, in sum, is to provide the financial
underpinning for the foundational work of society. As such, it
finances Christian reconstruction. Society is founded and recon-
structed only on the basis of the forthsetting and implementation
of the Word. The capitalization of all of life is made possible when
the tithe is properly paid and directed.

46. The tithe finances the reconstruction of society indirectly,
through the proclamation of the Word. This is the meaning of
