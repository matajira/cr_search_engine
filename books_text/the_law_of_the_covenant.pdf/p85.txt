66 The Law of the Covenant

94). Itis the presumption of vv, 25-27 that God, the Husband, ha
given the cloak to the poor man (as Boaz spread his cloak ove
Ruth, Ruth 3:9). The final section uses marital language in v. 31
where the phrase “holy men t Me” occurs (cf. Ezk. 16:8, “Mine
“to Me”; Lev. 20:26, also a marital passage; etc.).

V. Laws Concerning Witness-Bearing (23:1-9)
A. False Witness (23:1-3)
B. Personal Adversaries (23:4,5)
C. Courtroom Justice (23:6-8)
D, The Sojourner (23:9)

Each of these laws concerns the ninth commandment. Th
command not to bear false witness is a command to be impartia
in dealing with people. Thus, not only do we find laws agains
false witness both in court and in the general round of life, but wi
also find laws enjoining impartiality in the treatment of peopl
who are our personal adversaries (for instance, helping thi
donkey of a personal enemy, 23:5).

VI. Laws Regulating Time and Rest (23-10-19)

A, Sabbath years and days (23:10-13)
B. Annual festivals (23:14-19}

These laws pertain to the fourth commandment, which has tc
do with the time of rest, festivity, and worship.

VIL. Epilogue: Exhortations to Obey God and Conquer Canaan (23:20-33,

This section corresponds to the curses and blessings so ofter
added to the law codes in Scripture (Lev. 26; Dt. 28). Here wha’
is set before the people is the blessing that will come from obeying
God and following His Angel. This corresponds to the first com
mandment: God brought them out and they were to worship hin

only. Now He is bringing them in, and they are to worship Hirr
only.
