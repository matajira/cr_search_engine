204 The Law of the Covenant

21:1-9 Laws for unsolved acts of violence.

21:10-14 Laws protecting the warbride from violence.

21:15-17 Laws for protecting the firsiborn from violence.

21:18-21 Laws removing incorrigible criminals before they
turn to violence.

21:22-23 Laws against degradation in death; violence to God’s
image.

22:1-8 Laws to promote life:

vy. 1-4 Return lost property,

v. 5 Distinguish male and female, promoting sexual life.

vy. 6-7 Preserve environment for future generations.

v. 8 Protect against accidental violence and harm.

The Seventh Commandment

The seventh commandment has to do with chastity and purity,
in the bride of Christ and in the earthly marriage. In Numbers:

5:1-4 Chastity and Covenant membership.

5:5-10 Chastity and faithfulness to God.

5:11-31 The “ordeal” of jealousy.

6:1-21 The vow of the Nazirite, especially chaste and
separated to God.

In Deuteronomy:

22:9-11 Covenant chastity, signified by not mixing things that
differ symbolically. The ox is clean, and the ass unclean. Wool
causes sweat, while linen does not (Ezk. 44:18; Gen. 3:19).

22:12 Reminder of covenant chastity (Num. 15:37-41).

22:13-21 Laws to protect the integrity of marriage.

22:22-29 Laws against adultery and rape.

22:30 Law against incest.

23:1-8 Ghastity and the bride of the Lord.

23:9-14 Chastity and the warcamp, seen as the house of the
heavenly Bridegroom.

 

3:

The Eighth Commandment

The eighth commandment has to do with the preservation of
propriety, the preservation of what is proper to one sphere or
