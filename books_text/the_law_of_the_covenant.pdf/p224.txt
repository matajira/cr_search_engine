Appendix B—Gase Laws Correlated with Commandments 205

another. It is difficult to establish the boundary in Deuteronomy
between the seventh and eighth commandmenis, because cove-
nant chastity and covenant propriety are so close conceptually as
io overlap. Moses may be deliberately blending the two com-
mandments here:5

23:15-16 Foreign slaves who covenant with God become God’s
property (or, become part of His chaste bride).

23:17-18 It is improper to contribute a harlot’s wages to God
(or, a harlot cannot be part of God’s bride).

23:19-20 Interest.

23:21-23 Money vowed to God.

23:24-25 Respect of the neighbor’s property.

24:1-4 Propriety in divorce.

24:5 Newlyweds may not be “stolen” from one another.

24:6 Pledges.

24:7 Man-stealing.

The Ninth Commandment

The ninth commandment has to do with justice, impartiality,
and false witness. In Deuteronomy:

24:8-9 Libel and leprosy.

24:10-13 Justice for the debtor.

24:14-15 Justice for the hired man.

24:16 Impartiality in punishment.

24:17-22 Justice for the stranger, orphan, and widow.

25:1-3 Justice in punishments.

25:4 Justice for all creatures.

The Tenth Commandment
The tenth commandment prohibits coveting of the neighbar’s
wife and house. It refers to any attempt to obtain something to
which one has no right, even if one avoids violating the letter of
the law. In Deuteronomy;®

5. Kaiser puts 23:15-18 with the seventh commandment,
6. Kaiser stops the tenth commandment with 25:16.
