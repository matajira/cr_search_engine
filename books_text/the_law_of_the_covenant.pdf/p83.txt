64 The Law of the Covenant

J, Laws Concerning Slavery (212-1)
A. Five laws concerning the freeing of male slaves (21:2-6)
B. Five laws concerning the freeing of female slaves (21:7-11)

These laws are positioned first because of the deliverance o’
Israel from slavery in Egypt, and to correspond to the firsi
sentence of the Decalogue: “I am the Lorn, your God, whe
brought you out of the land of Egypt, out of the house of slavery.’
They relate to the fourth commandment, rest from bondage, fo
these laws focus on release of slaves (vv. 2, 3, 4, 5, 6, 7, 8, 10.

Il. Laws Concerning Violence (21:12-36)
A. Assault to death (21;12-14)
B. Willful equivalents of assault to death (21:15-17)
C. Assault to wound (21:18-27)5
D. Violence: animal property to man (21;28-32)
E. Violence: dangerous inanimate property (21,33-34)§
F. Violence: animal property to animal (21:35-36)

Each of these laws seems clearly to be associated with the sixt
commandment, “Thou shalt not kill.” (I discuss below a possibl

 

admits “there is no easy outline for dividing up the topics and sections of tt
covenant code” (p. 97), His own suggestion does not attempt to tie-the sections

the Ten Words, though he dacs follow Kaufman’s outline of Deuteronomy (s
Appendix B in this book). Kaiser’s outline of the Book of the Covenant breaks
down into cases involving slaves (21:2-11), homicides (21-12-47), bodily injuri
(2118-32), property damages (21:33-22:15), society (22:16-31), justice ar
neighborliness (23:1-9), and laws on sacred seasons (23:10-19). I shalt call atte
ion in the fyotnotes below to my reasons for not following the same division

he sets out.

5. Kaiser separates this into a new section on injuries, continuing throu;
verse 32. [ believe it still comes under the sixth commandment, and is but a su
section of the laws regulating violence. Kaiser, idem.

6. Kaiser begins his section on cases involving property damages here. The
is clearly some shading from the sixth to the eighth commandment at this poir
Grammatically, however, 21:33 and 35 begin, as does verse 28, with vii, “andi
In contrast, 22:1 (in Hebrew, 21:37) simply begins with i, “if,” and I take it tt
this begins a new section. Similarly, 21:2 does net begin with “and,” indicati
the start of a new section, All the cases within the section begin with “ani
Kaiser, idem,
