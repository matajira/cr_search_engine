104 The Law of the Covenant

special provision. According to Shalom Paul, this also is unique to
the Bible: “Only in Biblical legislation does the parents’ authority
receive a divine sanction (cf, Ex. 20:12), and this helps to explain
the severity of the punishment in Exodus.”!® Mesopotamian cul-
tures were statist, and treason against the state was a capital
offense, but not treason against the family. We might add that the
Bible is not family-centered (familistic), but does grant a high value
to the family and to parental authority when compared with
statist cultures.

As Appendix B indicates, the Bible puts submission to any
earthly authority under the fifth commandment. Thus, by exten-
sion, the death penalty for attempted murder can be applied to a
man who attacks Church elders, civil magistrates, or policemen
(cf. Dt. 17:12); and to a slave who attacks his master. As we shall
see below, the Bible never condones violence, and always enjoins
arbitration.

IV. Kidnapping and Slave Trading

The other law codes of the Ancient Near East also punished
kidnapping with death, but only if the stolen person were an
aristocrat.6 The Bible punishes all man-stealing with a man-
datory death penalty. In Deuteronomy 24:7, the kidnapping of
covenant members is particularly forbidden, but in Exodus 21:16,
all man-stealing is prohibited. It might be maintained that if we
read v. 16 in context of v. 2, it is only Hebrews who are protected
and avenged by this law. The text simply says “man,” however,
and there is no indication in the immediate context (vv. 12, 14)
that “man” is restricted to covenant members.

The death penalty is appropriate because kidnapping is an
assault on the very person of the image of God, and as such is a
radical manifestation of man’s desire to murder God. Like rape, it
is a deep violation of personhood and manifests a deep-rooted
contempt for God and his image.

15, Paul, p. 64.
16. [id., p. 65. Those who were not full members of the city were always re-
garded as outsiders or barbarians, and were not given full protection under law.
