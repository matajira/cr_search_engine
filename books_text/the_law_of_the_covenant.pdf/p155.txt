136 The Law of the Covenant

There can be no question but that the laws of the Bible here
are much superior to the penalties for theft today. If the thief is
penalized at all, the payment goes to the state. The man robbed
gets nothing, but has to pay taxes year after year to put the thief
up in prison (there are no prisons in Scripture),

If the thief cannot make the required restitution, he is sold into
slavery to raise the necessary money. The provisions of the sab-
bath year and Jubilee, which freed Hebrew slaves, would not ap-
ply to the thief, for obvious reasons: This would be arbitrary
justice, and would give the thief an incentive to steal as the years
of release approached. Such a man forfeits his right to the con-
sideration given other Hebrews who wind up in slavery. Jesus
says that the man sold to pay for his theft will not come out of the
prisonhouse of slavery until he has paid it all (Matt. 5:26; and cf.
Matt. 18:23ff.), The slave contract for the thief would be for as
many years as were needed to pay his debt.

Il. Seif defense

Sandwiched in among these laws of restitution is the provision
that if a thief is caught breaking in at night, the owner of the
house is not guilty if he kills him. He does not know the intent of
the thief, so he rightly suspects the worst, and kills him to protect
himself and his family. On the other hand, if he comes home dur-
ing the day and finds a thief, he may not kill him. In a small com-
munity, he will recognize the man, and can set the authorities on
him. Obviously, if the thief tried to kill the man, the owner would
have the right to defend himself.

Cassuto has noted that “the Bible presents the case in usual
circumstances.” What about our circumstances? Are we bound
to stand by and let a thief steal from us if we come home and find
him in our house? [ think not, for two reasons. First, the law is
written in terms of the thief’s “breaking in.” We take this as in-
dicating a breaking into a house, but in the context of this verse it
is a breaking into a yard which is in view. The immediate context

6. Cassuto, Exodus, p. 283.
