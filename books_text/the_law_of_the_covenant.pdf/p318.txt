SUBJECT INDEX

Aaron, 67, 99n., 157f., 251
Abel, 94
Abijah, 270
Abimelech, 34n.
Abiram, 126n.
Abortion, 113M, 276
Abram/Abraham, 31f., 40n., 147,
258n., 263
Chedorlaomer defeated by, 265
circumcision of, 78, 78n., 79, 79n.,
260
household numbered around 3000,
S4f., 92n.
Midianites descendents of, 40, 52,
244, 256n.
oppressian/deliverance pattern,
34n,
sanctuary for Lot, 5in.
saved by faith in the New
Covenant, 198
sons of, 15, 78
tithed to Melchizedek, 209
weaning of Isaac, 198n,, 274
Absalom, 268
Abuse, as grounds for divorce, B6f,
Achan, 126n.
Achsah, 147
Adam, 47, 50, 5. B et passim, 127,
15B, 164, 182, 188, 202,258n., 260n.
and Eve, 5, 63n., 79
Adonijah, 268
Adoption
slavery and, 77, 8in., 82, 85

 

 

theological, 24ff., 82f., 256
Adultery
death penalty for, 148f,
spiritual, 152A, 158
witchcraft as spiritual adultery, 18
Adversaries, dealing with, 66, 1699.
Advertising, and calculating one’s
tlhe, 222
Agape (see Love Feast)
Alliances, sinful, 19¢
Altar
as hearth, 153n.
as refuge, 98
Amalek, 206
American Life Lobby, 276
Aminon, 268
Amraphel, 265
Angels, 257n.
Angel of Death, 37, 82n., 83, 10In.,
126n,
Angel of the Lord, 194
Animals
clean and unclean, 56, 122ff., 164
psychology of, 123n,
social regulation of, 124f., 130
Antiochene exegesis, 244n., 245n.
Antiochus Ephiphanes, 183n.
Arioch, 265
Army
mustering of, 227ff.
organized by fives, 265n,, 267
versus militia, 228n
Assaults, 1094.

299
