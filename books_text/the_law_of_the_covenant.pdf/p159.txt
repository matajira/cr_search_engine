140 The Law of the Covenant

difficult it is to assess costs and benefits. Only when political
boundaries are involved—county vs. county, state vs. state~
should higher levels of civil government be brought in to redress
grievances. Local conditions, local standards of cleanliness,
silence, or whatever, involve local conflicts; these are best settled
by local governmental units.

“The question of pollution, therefore, is a question of responsi-
bility. The Bible affirms that each man is responsible for his ac-
tions. No man is to pass along the costs of any activity to his
neighbor, apart from the latter's consent. Where there is owner-
ship (legitimate sovereignty), there must also be respansibility.®

Safekeeping

7, When a man gives to his fellow money or articles to keep,
and it is stolen from the man’s house, then, if the thief should be
found, he must repay [make it whole] double.

8-9, If the thief should not be found, then the owner of the
house shall be brought to The God (or, the judges), [to determine]
whether or not he has put his hand on the property of his fellow.
(9.) For every matter of transgression with reference to an ox, or
an ass, or a sheep, or a garment, or any lost thing concerning
which one says, “Indeed, this is it”; to The God (or, the judges) the
matter of both of them must be brought. The one whom God con-
demns (or, the judges condemn) must repay [make whole] double
to his fellow.

10-11. When a man gives his fellow an ass, or an ox, or a sheep,
or any beast to keep, and it dies, or it is injured, or it is driven
away, no one seeing it, (I1.) the oath of the Lorp must be between
the two of them. If he did not lay his hand on the property of his
fellow, then its owner shall accept [the oath], and he will not make
it whale,

12. But if it is certainly stolen from him, he will make it whole
to its owner,

8. Gary North, “An Economic Commentary on the Bible, No. 36: Polhation,
Ownership, and Responsibility,” Chalcedon Report No. 130, June 1976. A much ex-
panded treatment of this whole matter can be found in North’s The Dominion
Covenant: Exodus (Tyler, TX: Institute for Christian Economics, 1985)
