Sabbaths and Festivals (Exodus 23-10-19) 183

particularly to one’s slaves,

In a sense, land is a living organism, composed of countless
living organisms. The life of the land moves at a much slower
pace than the life of men or animais, but even so, the land needs
rest in order to be refreshed, just as men and animals do. To re-
store the land and keep it healthy, it is to be left fallow one year in
seven. Any produce it may yield is free to any man or beast. The
poor may freely take such produce from anyorie’s properly, Here
is one more practical social welfare law, of the gleaning variety (as
opposed to the humanistic “hand-out” variety).

Even though the land was conquered in stages (Ex. 23:29), all
the land was to rest simultaneously (Lev. 25). This required the
Israelites to trust in God, and they failed to do so (Lev. 26:34, 43;
2 Chron. 36:21). God’s promise was, however, that there would be
a triple harvest in the sixth year (Lev. 25:20-22), which would
make up any financial loss.

Even though we do not have such a guarantee of a miracle to-
day, the principle of crop rotation, of letting the land lie fallow one
year in seven, remains sound, The use of fertilizers to keep soil in
continual production eventually destroys the earth.

The emphasis in v. 12 is on the giving of rest to one’s subor-
dinates, even down to the children of slaves and one’s animals.
The purpose is so that they might be refreshed. This word for
“refreshed” implies catching one’s breath, as well as reviving one’s
life (2 Sam. 16:14).

According to Leviticus 23:3, the sabbath day was also the time
for worship in the local synagogue. The prophetic, teaching form
of worship was decentralized in Israel, with local Levites teaching
in local synagogues. The priestly, sacrarnental aspect of worship
was centralized at the Temple, and highlighted by the three major
festivals. Here we are dealing with sabbath day worship, and God
enjoins them to pay close attention to His words as they are
taught sabbath by sabbath, and to be sure not to give worship to
any other gods. The plural in verse 13 implies corporate worship,
and the return to the singular in the last phrase covers private
devotion as well.
