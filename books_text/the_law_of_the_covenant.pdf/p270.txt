Appendix F—Proleptic Passover 251

was done in order that it might be seen: ‘When I see the blood, I
will pass over you . . . and not smite you’ (Ex. 12:13, 23)."34

Concerning the phrase “bloody bridegroom,” Zipporah is
clearly addressing her son, not Moses, Moses is net in view in this
story. Moreover, Moses had been a bloody bridegroom years be-
fore, on their wedding night, It would make no sense to address
him thus on this occasion, Kosmala, however, has no explanation
for Zipporah’s use of this phrase, except to suggest that it was part
of “Midianite theology.” This, however, is wholly speculative on
his part.

A Suggested Interpretation

What actually is happening here? God has just stated that His
wrath against sin is going to take the form of killing the firstborn
of men. This is because the firstborn male inherits preeminence,
blessing, and a double portion from the father (Gen. 25:23,31;
Q7:4ff; 48:14ff.). Also, the firstborn acts as priest in the home,
under the father’s oversight (and thus, the Levitical order came
into being by the Levites’ being substituted for the firstborn of all
Israel, Numbers 3). Thus, the jirstborn son signified the center and
future of the family. To spare the firstborn was to spare the family; to
kill the firstborn was to kill the family.

Where was the “lodging place on the way?” We are nol told.
One might be inclined to think it is the border of Egypt, but im-
mediately after this incident (if the text is in chronological order
here), Aaron meets Moses at Mount Sinai. Thus, the lodging
place would have to be near the mountain of God. On the other
band, possibly the lodging place incident is set here, out of
chronological order, because of the theological context set up by
verse 23, God’s threat against the firstborn sons. In that case, it
could easily be the border of Egypt.

I think the best way to resolve this difficulty is to take note of
the vagueness of the text. Moses is drawing near to Egypt, a land

3. fézd., p. 24, Of course, far Kosmala there is not the idea of substitution in
this blood-rite. Rather, its purpose is superstitious: to avert evil.
