The Bible and the Laws af God 25

always puts pressure on man. This drives men either to hate God
more (Rom, 7:9ff.), or to flee to God for justification (Rom.
3:498F.).

The Lord Jesus Christ earned justification for His people by
taking upon Himself the penalty for their sin, as their substitute.
When a man places his faith in the sacrificial death of Christ, and
jn the God Who ordained this death, he experiences justification.
The experience of justification has three stages. First, the law
drives a man to Christ for initia justification, Second, day by day
the law drives the Christian to his knees and to the cross, so that
his confidence in God’s justifying act is renewed and matures.
‘This is a progressive aspect of justification, for an older Christian
should understand more of God's saving work, and have greater
faith and confidence. Third, on the last day, the Day of Judg-
ment, the law once again condemns all men, but God declares his
faithful sheep to be justified. This is final justification.

Second, fallen man is also morally sinful in the eyes of God. The
law of God shows man what a holy lite is like, so the law is the rule
of holiness or sanctification. The Lord Jesus Christ earned
sanctification or holiness for His people by living a perfect life for
them, This perfect life is applied to the saints by the Holy Spirit.
When a man places his faith in God, he is renewed and given new
life; he is set apart to live a holy and righteous life in the midst of a
sinful and corrupt world. This is the initial experience of
sanctification: God counts him as morally holy in spite of his sin.
The law is the moral rule of that sanctification from its initia] in-
ception, through its progressive development, until that final day
when the saints are made perfect in righteousness. This is the se-
cond use of the law.

Third, fallen man has also forfeited dominion and sonship. Salva-
tion restores men to dominion and to sonship by adoption. The
Lord Jesus Christ, King of the Universe, lived a life in one sense
devoid of dominion, with no place to lay His head and having no
possessions, so that His people might be given the honors and
privileges forfeited by-sin. This is part of glorification, the
bestowal of honors and privileges on the saints. Glorification also
