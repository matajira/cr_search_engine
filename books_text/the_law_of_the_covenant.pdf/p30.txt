The Bible and the Laws of God it

God’s arguments with Pharaoh in Chapter 2 of this study. The
fact that the Bible is centrally concerned with such things, how-
ever, does not negate the fact that it speaks to all areas of life, in
varying degrees of specificity. The chronology of Scripture, for in-
stance, is always connected to the Seed-line, but this does not
make it any less true, and any less valid as a statement of the age
of the world; and as a statement of the age of the world, Biblical
chronology is relevant to a host of disciplines.

The Law of God is Unchanging

Since God’s law is a transcript of His personal character, it
cannot change, any more than God can change. Jesus makes this
point in Matthew 5:17-19: “Do not think that I came to abolish the
law or the prophets; I did not come to abolish, but to fulfill (or,
put into force). For truly I say to you, until heaven and earth pass
away, not the smallest letter or stroke shall pass away from the
law, until all is accomplished. Whoever then annuls one of the
least of these commandments, and so teaches men, shall be called
least in the kingdom of heaven; but whoever does and teaches
them, he shall be called great in the kingdom of heaven,”

Yet we know that there has been some change. We no longer
sacrifice bulls and sheep, as the epistle to the Hebrews makes
clear. If the law has not changed, then what has? It is the ci-
cumstances which have changed.'4 Let us consider some examples.

The law of God commands, “Husbands, love your wives”

 

throne-worship of God. Despite my appreciation for the work done by some of
these writers, I cannot go along with the down-playing of sacramental worship
which is found in some of them. See James B. Jordan, ed., The Reconstruction of the
Church. Christianity and Civilization, No. 4 (Tyler, TX: Geneva Ministries,
forthcoming).

15. Again, if we are using the word daw to refer to the unchanging moral
character of God, as we are here, then of course the law in its essence cannot
change. When Scripture sometimes refers to a change in law, asin Hebrews 7:12,
the reference is to particular laws or to the system of the law, which system
undergoes a death and resurrection in Christ, becoming the New Covenant. Our
point here is rather simple and non-technical: The unchanging essence of the law
is a reflex of its source in God’s own character; the changing manifestations af the
law are a reflex of changes in circumstances relative to lhe creature.

  
