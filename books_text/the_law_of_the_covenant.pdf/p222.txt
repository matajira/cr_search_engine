Appendix B—Case Laws Correlated with Commandments 203

23:14-19 Festivals.

Leviticus 25 also connects with the fourth commandment, fol-
lowing as it does immediately after the stoning of the man who
cursed the Name of the Lord,

In Deuteronomy:*

15:1-11 Release of debts.

15:12-18 Release of Slaves.

15:19-23 Consecration to God; rest in God.

16:1-17 Festivals.

The Fifth Commandment

The fifth cormmandment has to do with submission to God in
the form of submission to His earthly authorities. In Numbers 1-4
the leaders are set up for battle, and the L.evitical authorities are
established. In Deuteronomy; :

16:18-20 Judges.

16:21-17:1 Submission before God.

17:2-13 The judgments of the judges.

17:14-20 Kings.

18:1-8 Levites.

18:9-22 Prophets.

The Sixth Commandment

The sixth commandment has to do with preserving life from
violence. This is different from the third commandment, which
has to do with avoiding death in the presence of God. In
Deuteronomy:

19:1-13 Cities of Refuge and the regulation of the Avenger of
Blood.

19:14 Violence against covenant life and property.

19:15-21 Murder as attempted by the tongue.

20:1-20 Laws of war, regulating violence.

4, Kaiser starts this section with 14:28, because of the mention of the festival
and tithing. I think that the emphasis in 14:22-29 is on the presence (Name) of the
Lorp,
