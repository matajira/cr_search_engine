Appendix C— Tithing: Financing Christian Reconstruction 211

13, The term ‘poor tithe’ to refer to the command in Deuteron-
omy 14:28, 29 is a misnomer. The money was given to the elders
of the gate, which today are the elders of the local churches (1 Cor.
6:1-5). They determined its use. Part of it went for the poor, but
part also for the salary of the local Levite.

14. Contrary to popular ideas, Levites were found in the towns
of Israel as teachers in proto-synagogues. Worship was conducted
every sabbath and new moon (Lev. 23:3, Dt. 18:6; Jad. 17:7;
18:30; 19:1; Neh. 10:37f.). The third-year tithe was, then, not a
poor tithe, but a local as opposed to a national tithe.

15. In the New Covenant, since there is no longer any central
sanctuary, all tithes go to the “elders of the gate.” We are in a
perpetual third-year tithe situation, until God’s great seventh year
comes at the Last Judgment. A study of the third-day and third-
year concept in the Bible reveals that just as Christ arose on the
third day, we are living in the third day until the seventh day ar-
rives (Gen. 22:4; 42:18; Ex. 19; Num. 19; Hos, 6:2; Jonah 1:17).

16. Under the Old Covenant, in the first and second years the
people took their tithes to the sanctuary to celebrate the Feast of
Booths (cf. Dt. 14:22-27 with 16:13-14). They used the tithe to
finance their participation in the feast. What was left over, the
larger portion by far, was given to the national Levites.

17. In the third year, the people took part of their tithes to the
sanctuary to celebrate the Feast of Booths (cf. Dt. 26:14), and then
returned to their locales, depositing the remainder of the tithe
with the elders of the gate.

18. During the year, as various crops came in, and as various
animals gave birth to their firstborn, the tithe and firstborn offer-
ings would be laid up. These were apparently delivered in the fes-
tival of the seventh month, the Feast of Booths. (Cf. 2 Chron.
31:7.)

19. The tax of the firstborn was also used first to help finance
participation in the Feast of Booths, and then the remainder given
to the Levites (Dt. 14:23 + 15:19-20).

20. The Lord's Supper and the Love-Feast of the New Cove-
nant corresponds to participation in the Feast of Booths (1 Cor.
