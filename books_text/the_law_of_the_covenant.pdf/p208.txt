Sabbaths and Festivais (Exodus 23:10-19) 189

the Church at Pentecost (Lev. 23:17). Secondarily, the leaven is
the redeemed humanity, the Church, now inserted into the three
measures of meal (the world). This symbolism is most ap-
propriate, since leavened meal is what is normally used as starter
in leavening more meal. A baker reserves a small part of his
leavened dough, and uses it to start more. Thus, the insertion of
leaven (the Holy Spirit), is done once and for all, and then
spreads from dough to dough day by day.

Thus, the Feast of Unleavened Bread required a week of
unleaven before the leaven is reinserted into the bread. This can
more clearly now be seen as a sign of the re-creation of the world
in seven days, followed by the new leavening of the new humanity
beginning on the eighth.

Il. The Feast of the Harvest

Also known as Weeks or Pentecost, the one-day Feast of the
Harvest was the time at which the first-fruits of the field were
presented, baked in a leavened loaf (Lev. 23:17), to the Lord, in
recognition that it was He who gave the harvest.

The Feast of the Harvest also fell on the day after a sabbath,
seven weeks after the waving of the first sheaf (Lev, 23:15-21). The
seven days of re-creation have issued in séven weeks of true
righteous maturation and growth, for it was a leavened loaf which
was waved to God. Jesus arose on the day of the first sheaf, and
imparted the Holy Spirit to his disciples on that day (John 20:22),
though the full outpouring of the leaven of the Spirit did not come
until seven weeks later (Acts. 2).

From this we see the progressive growth of the kingdom of
God. Immediately after the Sacrifice, the first-fruit sheaves
become available for eating (after they have been waved before
God). Seven weeks later, there is enough to bake into a loaf to
wave before God. But there is still more growth to come, which
will be made manifest at the Feast of Ingathering.

The liturgy of the first-fruits is found in Deuteronomy 26:1-11.
