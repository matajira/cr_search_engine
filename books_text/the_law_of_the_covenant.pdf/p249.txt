230 The Law of the Covenant

of the host of the Lorn are not seen as censuses, and no head tax
is taken, There simply is no evidence of an annual head tax.

There seems to have been one peacetime occasion for collec-
ting the half shekel tax, and that is when the house of God needed
repairs. When Joash repaired the Temple, he called for the
Levites to collect money from three sources (2 Kings 12:4). These
were (1) “money that passes”; (2) assessment money from trespass
offerings and vows; and (3) goodwill gifts. The first category
seems to be a reference to Exodus 30:13 and 14, money from those
who “pass over to the ranks of the mustered.” Keil and Delitzsch
argue for this understanding in their Old Testament Commentary on 2
Kings 12:4. More evidence for this interpretation is provided by
the parallel account in 2 Chronicles 24:6, where the tax is called
the “levy of Moses.” Just as a shepherd Hines up his sheep and
counts thern as they pass under the rod (Lev. 27:32), so the Lord
counts his people (Ezk. 20:37). As each man passed the counter,
he deposited his half shekel in a receptacle, and joined the ranks of
the mustered.

Joash was not going to war, so this peacetime muster was solely
for the purpose of raising money to repair the house of God. We
have argued from the use of the term pagad (muster) that the tax
was collected before battle. These are the only hints in the Old
Testament that the half shekel tax was ever collected in the history
of Israel after the events recorded in Exodus 30 and Numbers 1.’

Use of the Tax

Since the money was collected just before battle, it might be
supposed that the money was used to finance holy war. While this

7. In 2 Ghronicles 24 we read about the apostasy of Joash, how he murdered
Zechariah the son of Jehoiada, and how God judged him by bringing in the
Syrians against him. In 2 Kings i2, however, Joash’s apostasy is not mentioned.
Rather, the theology of 2 Kings 12 turns on an ironic point in connection with the
atonement money, When the Syrians invaded, Joash bought them off by giving
them the gold and silver of the Temple (2 Ki. 12:18). Thus, instead of defeating
God’s enemy, and using the atonement money and the war spoils to build up the
Temple, Joash uses God's Ternple money to buy off the enemy! This is all 2
Kings 12 needs to say in order for us to realize that Joash has aposratized.
