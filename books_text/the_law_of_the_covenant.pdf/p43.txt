24 The Law of the Covenant

Example: If we are not to muzzle the ox which treads out the
corn, surely we are not to muzzle the preacher who preaches the
Word (1 Tim. 5:17-18). If we are to execute openly rebellious and
incorrigible teenagers, surely we are to execute incorrigible adult
criminals (Dt. 21:18ff.).

The Uses of the Law

In its essence, the law of God is revelation. We have already
discussed this at length: The law is a transcript of God!’s perfect
personal and social character. As such, it is also a transcript of the
perfect personal and social character of man, the image of God. It
shows us what God is like, what we were created to be like, and
what we shali be like through the redeeming blood of Christ the
Savior.

Traditionally, the law is seen to work in three ways. These are
called the three uses of the law. They are justification (which
reveals the justice and judgment of God), sanctification (which
reveals the character of God), and dominion (which reveals the
purposes of God).

When man was created he was legally right with God, he was
morally upright, and he had certain honors and privileges, the
chiefs of which were sonship (Luke 3:38) and dominion (Gen.
1:26). Man was created just, holy, and glorious (Ps. 8:5). The re-
bellion of man made him legally guilty in the sight of God
(unjust), morally sinful in his actions (unholy), and lost him his
honors and privileges: He was cast out of Eden (dominion) and,
while he is still a son of Gad by creation (Acts 17:29), he is no
longer a covenantal son of God, and so must be adopied to be saved.
This was his loss of glory. Salvation restores all three aspects of
man’s life—legal standing, moral standing, and dominical
standing and the law has a role in each aspect.

First, fallen. man is legally guilty before the judgment bar of
God’s court. The sentence of condemnation passed upon him is
the wrath of God. It is the law of God which man has broken, and
which makes him guilty. The first use of the law, then, is to show
man his sin. When God uses the law to show man his sin, He
