142 The Law of the Covenant

breaks into Bob’s house and steals John’s flatware, but nothing
else. “Tough luck, John,” thinks Bob, and forgets about it. Bob is
not liable, after all. John will just have to accept Bob’s oath (vv. If),
Why should Bob even lift a finger to get John’s stuff back?

Ah, but this law puts a different complexion on matters. If
Bob manages to track down the thief, working with the police,
etc., then Bob will get to keep half of the restitution. 50, Bob calls
the cops, and cooperates fully in getting John’s flatware back. The
thief is (wonder of wonders) caught. He is forced to make double
restitution, returning the silver and also its equivalent in money
to Bob. A few days later, John returns. Bob gives him back his
silver flatware, and Bob gets to keep the restitution.

The ffth case specifies that no compensation is due if it can be
proven that the animal was slain by a beast. The third case had
already stated that an oath was to suffice in such a case, but in the
interest of removing all suspicion, evidence should be presented
when such is available.

This is a valuable principle in all of life. Since men want to
play God, they like to insist that people should simply take them
at their word. We are not God, however, and we should back up
our statements with evidence wherever possible (without weary-
ing other people, of course, and without disclosing confidences).
Church rulers, to take an example, can have a tendency to main-
tain that people should believe what they say simply because they,
the officers, say it. This is unwise, and creates suspicion,

Borrowing, Neighborliness, and Rent

14, When a man borrows something from his fellow, and it is
injured or it dies, its owner not being with it, he will surely make it
whole.

ida. If its owner is with it, he will not make it whole.

15b. Lf it is hired, it comes for its hire,

Full compensation is due for any damaged borrowed article.
This ensures that the borrower will take care of his neighbor's
property. If the owner is present, it is assumed that he will watch
