Violence (Exodus 21:12-36) 107

says to father or mother, anything of mine you might have been
helped by is ‘corban’ (that is to say, an offering [to God]),” you no
longer permit him to do anything for father or mother; in-
validating the word of God by your tradition which you have
handed down; and you do many such things like that. ” Notice
that Jesus sets Exodus 21:17 right next to the fifth commandment
in binding force. Notice also that “cursing” father and mother is
definitely said to include verbally reviling them.2° Principally,
however, this passage shows us that in the practical legal sense, refus-
ing to care for parenis in their old age is a capital offense.

Practically, then, “repudiating” parents could mean a settled,
publicly manifest disposition to reject Godly household rules. It
could mean a refusal to care for them in their old age. [t could
mean reviling and cursing them. For the death penalty to be ap-
plied, however, there would have to be evidence that would stand
up in court. The small boy who wants to appear tough to the
fellows may call his parents “the old man” and the “old lady.” He is
not guilty of a capital crime, though he is in sin nonetheless. The
teenager who is upset with household rules may get mad and yell
at his parents, “You don’t love me; you’ve never loved me. I hate
you.” He has not committed a crime worthy of death either,
though he has sinned in the passion of frustration. Those who
hate the law of God, and that includes many in the so-called evan-
gelical churches today, like to twist the Old Testament Scriptures
to make it appear that small children are commanded to be put to
death for minor offenses. That is not, however, what is in view.

A few comments may be helpful. When a son marries, he sets
up a new household, according to Genesis 2:24. If parents come
to live with their married children, they must adjust to the rules of
their son’s or son-in-law’s house. Honoring parents does not mean
permitting them to destroy one’s home. If grandparents under-
mine the discipline of the children, or if the mother-in-law con-
stantly badgers and harasses her daughter-in-law, something will

20. Some translations give “speak evil” for “revile.” The Greek word indicates
something stronger than the English “speak evil,” which could mean “say bad
things about.”
