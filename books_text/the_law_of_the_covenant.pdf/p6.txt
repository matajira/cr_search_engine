PREFACE AND ACKNOWLEDGMENTS

The style of this work is different from that of some other
modern Bible commentaries. This is because I have written it as a
Christian addressing other Christians, rather than as a scholar
addressing other scholars. There is no true conflict between
scholarship and faith, but my primary purpose is to cdify the
Church of Jesus Christ, and thus my scholarship is subordinate to
that end.

The passage with which I am concerned has been the object of
much higher critical analysis, and could become the subject of a
lengthy scholarly discussion.! This is not my purpose. While I do
not despise the place of sound conservative scholarship, my pur-
pose in this book is to give a theological and useable exposition of
the first extended law code of the Bible. To this end I have
brought to bear in my discussion as much as seemed useful from
the work of modern scholars, while leaving much of the apparatus
of scholarship behind.

There is no extensive commentary on this passage of the
Bible, written from an orthodox, evangelical standpoint. Thus, at

1, The interested reacler might procure Edward J. Young, An Introduction to the
Old Testament (Grand Rapids: William B. Eerdmans, 1960 [revised edition]); also,
in ascending order of scholarly detail and precision, Roland K. Harrison, Jn-
troduction to the Old Testament (Grand Rapids; Eerdmans, 1969); Oswald T. Allis,
The Old Testament: Its Claims and its Critics (Phillipsburg, NJ: The Presbyterian
and Reformed Pub. Co., 1972); G, Gh. Aalders, A Short Introduction to the Pen-
tatench (London: The Tyndale Press, 1949); and Oswald T. Allis, The Five Books of
Moses (Philipsburg, NJ: The Presbyterian and Reformed Pub. Co., 1949 [2nd
edition]).

vii
