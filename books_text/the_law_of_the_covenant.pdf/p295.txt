276 The Law of the Covenant

men in bondage (Pharisees), to corrupt the early Church
(Judaizers), and to crucify Her Son (John 19:7, “we have a law,
and by that law, He must die”), These teachings are familiar to
all, and need no substantiation. Our hypothesis, we note, fits
beautifully in with these common teachings, and adds a new
dimension to them. That is what we should expect, if the teaching
is true.

Finally, we want to know if the hypothesis enables us to preach
to our modern situation. Times and seasons vary, and perhaps it
would be difficult to find close analogies to child cannibalism in,
say, certain more Christian periods of history. Today, however, we
find no problem. According to “All About Issues,” January, 1984,
put out by the American Life Lobby, in an article by Olga Fairfax
entitled “101 Uses for a Dead (or Alive) Baby,” modern “collagen
enriched” lotions, hand creams, shampoos, and the like are fre-
quently made from the boiled-down substances of aborted chil-
dren, Aborted babies are sold by the pound or by the bag. Dr.
Fairfax mentions some drug/cosmetic companies which do not use
human collagen, and some human collagen comes from placentas
rather than from babies, but others apparently are using fetuses
as a source. (Copies of a 40-page documented study are available
for a donation from Dr. Olga Fairfax, 12105 Livingston St.,
Wheaton, MD 20902. The short article can be had for a donation
from American Life Lobby, Box 490, Stafford, VA 22554. Similar
data is found in William Brennan, Medical Holocausts: Exterminative
Medicine in Nazi Germany and Contemporary America [Nordland Pub-
lishing International, 1980], and Brennan, The Abortion Holocaust:
Today's Final Solution [St. Louis: Landmark Press, 1983], available
from Geneva Christian Bookservice, P.O. Box 8376, Tyler, TX
75711.)

We have not yet seen widespread eating of fetal material by
the women of America. But, the women of America are, as a
group, boiling down their murdered children, and using the
residue as cosmetics.

Tt seems that our hypothesis does indeed give us preaching
material.
