48 The Law of the Covenant

and the like. Once Jesus came, however, the covenant was defini-
tively established, once and for all, and God’s people were not
simply revitalized for a short time, but definitively resurrected
from Spiritual death.

As yet, the New Covenant has definitively but not fully arrived,
since we are not yet living in resurrection bodies, and this world
has not been transfigured into the New Earth. Thus, we are living
in the old Adamic world, but in the power of the New Resurrec-
tion Covenant. We still have the original Adamic tasks and
responsibilities. We still need to set aside one day in seven for
worship and rest, even though “in Christ” we are in a continual
sabbath.?

All this helps us understand the covenant God made at Sinai.
It was a provisional administration of the covenant, based on
animal sacrifices, which was never intended to last for all time.
Because the exodus is éhe preeminent redemptive event in the Old
Testament, it is proper that the covenant be fully set forth and
described right after this redemption. This is why so much of the
covenant legislation in Exodus through Deuteronomy refers back
to the exodus as its rationale and foundation. The covenant is
only reestablished and republished on the basis of redemption, so
the facts of redemption are adduced as arguments for keeping the
covenant. To apply these laws to today’s world, we should think of
them as based not on the exodus, but based on the finished work
of Jesus Christ.

This of course raises again the question we have taken up be-
fore, of whether we ought to regard these laws as our own. The

2. Ihave discussed this ai greater length in a series of essays in “The Geneva
Papers,” starting with No. 1, published by Geneva Ministries, P.O. Box 8376,
Tyler, TX 75711; available upon request for a contribution. The Bible presents us
with a bipolarity of the “special” or particular, and the “general.” This is a reflex of
the one and three in God. There are general officers in the Church (lay people”),
and special office bearers. Concerning the sabbath, there is a general rest in
Christ, and there is a special form of rest which is made manifest one day in
seven. Rest is tied to the activity of faith, and there is a general faith-trust we are
to have in Christ, and there is also a special act of faith which is connected to
fellowshipping with His special presence at the Lord’s Supper in special worship.
