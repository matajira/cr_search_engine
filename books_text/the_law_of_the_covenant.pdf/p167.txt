148 The Law of the Covenant

Perhaps just one interesting note here. Apparently the free
Israelite woman, with her mohar, had quite a bit of “independence”
from her husband. This money would be her insurance policy, in
case her husband died or divorced her (and so it is not the same ag
an insurance policy today). Also, however, it was money she
could use and invest. It seems most likely that the money the
woman in Proverbs 31:26ff. is using is her own money, not that of
her husband (though perhaps it is money he has entrusted to her),
Thus, if the husband were something of a fool when it came to
money, the wife had her own which she could invest wisely to care
for herself and her children,

Modern American Christian women are often more in the
position of slave wives than of free ones, according to the Bible.
They seldom have their own separate money. In the proper
“Christian” home of today, the husband has aif financial control.
He has not provided his wite with money at marriage, nor later on
(though he may adorn her with jewelry, which is hers to keep). If
she takes a job, it is assumed that he will have ultimate say-so over
the spending of her money. The Biblical marriage, however, while
it may appear to entail more tension and negotiation between
husband and wife, also produces people who are much more
mature,

Turning directly to the law itself, the punishment for the
seducer is that he must marry the girl, unless her father objects,
and that he may never divorce her (according to Dt. 22:29). Ac-
cording to Deuteronomy 22:25-27, if the girl were engaged to be
married, this would count as a case of adultery, and both would
be put to death, unless it were a case of rape. There seems to be
some latitude here, hawever, since we read in Matthew 1:19 that
“Joseph, being a just man... was minded to put her [Mary]
away privately.” Here again we see a circumstantial application of
the unchanging law of God; Joseph apparently regarded Mary as
basically a good woman, who must have fallen into sin on one oc-
casion, and so he determined that death was too severe a punish-
ment for her. That this was perfectly just, the text itself tells us.
This proves, by the way, that the death penalty is not mandatory
