Justice (Exodus 23:1-9) 177

My purpose in taking up this passage is that it sheds light on
Exodus 23:1-9. In verse 15, we are instructed that na man is to be
convicted save at the “mouth” of two or three witnesses, This
might seem to exclude the non-verbal testirnony of fingerprints
and the like, but in fact the Bible speaks of the testimony of non-
human “voices,” as in Genesis 4:10-11, “And He said, ‘What have
you done? The voice of your brother’s blood is crying to Me from
the ground. And now you are cursed from the ground, which has
opened its mouth to receive your brother’s blood from your hands. ”
At least two lines of evidence are, however, required.

In a recent Church trial, questions were raised concerning
verses 16-19. The precise matter at dispute was whether this
passage means that a false witness is automatically to be counted
as a malicious witness, or whether a witness who proves false
must also be proven malicious before the Law of Equivalence can
be applied to him. Mr, Jones (not the real name) brought charges
against Mr. Smith. He charged that Smith had stolen $1,000 from
him. Thus, Smith owed him double restitution, claimed Jones.
The Church court informed Jones that if he lost the case, and
Smith were declared innocent, then he would have to pay Smith
$2,000. Jones countered, however, by asserting that the court
would have to prove that he was malicious before he would be
lable under Deuteronomy 19:19. If Jones were right, then a false
witness would only be liable for his false charges if it could be
proven that he was malicious in bringing them (according to
modern American notions of what “malice” is).

Verse 16 states that if a “malicious” (Aamas) witness rises up to
accuse a man, then the matter is to be brought before the priests
or the judges, according to whether the matter pertains to the
Lorp or to the civil realm (Dt. 17:9; 2 Chron. 19:11). In verse 18
we are told that if the witness is a “false” (sheger) witness, one who
has falsely (sheger) accused his brother, then the Law of Equiva-
lence applies.

As it stands, verses 18 and 19 state that an accuser need only be
proven false (sheger) in order for the Law of Equivalence to apply.
In larger context, the sheger witness has also been described as
