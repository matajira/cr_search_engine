Property (Exodus 22:1-15) 137

Jeals with a thief who steals animals. This is a slow process, A
man has to drive the animal away. Hf we see him during the day,
we can raise an alarm, and neighbors will help us track him
down. At night, however, we cannot see—no street lights in an-
sient Israel, and none in rural areas today. We don’t know the
thief’s intention, Maybe he is invading our yard with a view to
killing us. Thus, we may go into the yard and kill him. Thus, the
law is not directly addressing the situation of a man’s breaking
into the house, the sleeping area, of a family.

Secondly, the concepts “day” and “night” in Scripture have an
extensive metaphorical meaning. In Genesis 1:5, God called the
light day, and the darkness night. Light and darkness are fre-
quently used in Scripture ta describe Spiritual or social conditions.
Moreover, this passage does not actually use the terms day/night or
light/darkness, but says “if the sun is risen upon him.” The rising of
the sun sheds light upon the situation. Moreover, the rising of the
sun is a token of power (Jud. 5:31; Gen. 32:31; Ps. 19:4-6). Thus,
we may understand the verse as meaning that the owner is able to
identify the thief (the sunrise of understanding), or is able.to over-
power him without killing him (the sunrise of strength). I suggest
that the meaning of the law is this: If you don’t recognize him, and
cannot deal with him in any other way, you may kill him; but if you
know who he is, or have the strength to deal with him, you may not
kill him. This will vary from situation to situation, and local or~
dinances specify what a shopkeeper or houseowner may do to a
person who breaks in. It is up to the elders of any given local com-
munity to determine under what circumstances the sun may be
said to have risen upon a situation,

Pollution

5. When a man causes a field or vineyard to be grazed over, or
he lets loose his beast and it grazes in the field of another, from the
best in his field or vineyard he must make it whole.

6. When fire breaks out and catches in thorns, and shocks of
grain, or standing grain, or the field is consumed, the one causing
the burning must most certainly make the situation whole.
