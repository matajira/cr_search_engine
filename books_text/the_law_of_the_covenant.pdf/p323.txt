304

symbolizes a person’s livelihood,
160, 262
Hanukkah, 1851.
Harlotry, spiritual, 145, 157f,
Hatred, 93f., 95
of authority, 95
Havilah, 15, 32
Head tax, 225¢f.
Hearthfires, 153n.
Heaven, 196
Hell, 102
covenantal character of, 4, 5
Helpless persons, 156ff,
High priest
death of, 98iF,
slave of, Bin.
symbolized Groom to the Bride,
163n.
Holiness, 165f.
Holy war, 230, 231, 237
Homeborn slave, 8lf.
House, and human body, 82n.
Husband, bloody, 2438.

Idolatry, 154
harlotry and, 52f.
Image of God, 3ff., 11, 124n.
personal and social, 4ff.
Impartiality, 66, 167.
Tncarnation, and slavery, 810.
Inflation, 159
Inquisition, 153
Instinct, animal, 123n.
Insurance, 85, 148
Interest, on loans, 158f.
International relations, 171
Investiture, 82, 82n., 264n. (sce also
Clothing)
Investments, 158f.
Tsaac, 44, 190
conceived after Abraham's
circumcision, 78
fall of, 35n.

The Law of the Covenant

oppression/deliverance pattern,
34n.
seed, 15, 32, 260
weaning of, 192n., 273, 274
Ishmael, 15, 32, 78, 192n., 256n.,
273, 274

Jacob, 51, 258n., 263
descent into Egypt, 42, 55
enslavement of, 32if., 50
oppression/deliverance pattern,

34n.
Rebekah and, 190f., 273
sons of, 35n.

tithe and, 210
Japhethites, 265
Jealousy
defined, 33n,
ordeal of, 445
wrath and, 157f.
Jephthah, 82n,
Jeremiah, 191
Jeroboam I, 270
Jerusalem
bride af the Lord, 257n.
destruction of, 14ff., 36f., 81, 191f.,
273, 274
heavenly, 102
kills sons, 275f,
Jethro
advised Moses, 52f.
believer, 15, 32, 40, 52f., 244ff.
descendent of Abraham, 15, 40,
528, 244A
Meichizedekal office, 40, 40n.
taught Moses, 40
Jewelry, 146, 149
Jezebel, 153
Joab, 228n.
Joash, 230f., 230n,, 231
Jonah, 32
Jordan River, 83n.
Joseph (son of Jacab), 35, 157, 264
