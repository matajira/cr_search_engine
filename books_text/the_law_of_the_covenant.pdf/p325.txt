306

Levirate, 51

Levites, 230, 232
ecclesiastical specialists, 212
health and, 216
replaced firstborn, family priests,

55, 164, 242, 251, 253n.

tithes, paid, 212
tithes, received, 212, 220, 236
work of, 218

Levitieal cities, 10in., 253n.

Levitical tithe, 209.

Lex tationis, W5ft.

Liability, limited and unlimited, 129f,,
130n.

Life, and death, 190

Loans, I58f.
six year limit, 21

Lord's Day, 182

Lord's Supper, 259f,, 260n.
common grace and, 83
consecration of elements, 187
festival, 192, 2itf.
financing of, 2Uf., 235
marriage analogy, 59n., 259f.,

260n.

presence of Christ, 48n.
resurrection and, 59, 197
sacrament, 188, 197
sacramental theory, 218f.
tithing and, 209, 21if,

Lat, 35n,, 5in.

Love, 21

Love Feast, 21if.

Luke, 216

Lying, 20

Magic, 132f., 1526,
Magistrate, 69, 1672.
Manasseh 153
Manna, 86
Manslaughter, accidental, 93, 97H.
Marriage
contract, 150

The Law of the Covenant

covenant of, 6f.
kinds of, 146i.
laws of, 65f.
Marriage Supper of the Lamb, 39,
59n., 259, 260n.
one flesh concept, 19
Mary, 148
Masochism, 95
Mediator, 200
Medical expenses, owed, 110f.
Medicine, tithe and, 2i6
Melchizedek, 32, 40, 40n., 208f., 236
Mercy, works of, 17in.
Metaphors, mixed, 258
Michael, 162
Midian, 40, 253
Midianites, 15, 52, 52n., 243ff., 256n,
Milk, 190f.
Millstone
not to be used as collateral, 160
symbolizes a person’s livelihood,
160, 262
Miriarn, 1576.
Miscarriage, 194
Missiona, financing of, 213
Mixed multitude, 176
Moabites, 52n.
Mob rule, 169
Morning (sunrise), significance of,
187f.
Moses, 35, 50, 51, 67, 157f,, 162
head tax, 2439.
“levy of Moses,” 230
organized Israel, 52ff.
Multitude, mixed, 176
Murder, 93, 97, 110, 114
attempted, 112
Muster, 22617.

 

Naboth, 162

Nakeclness, 62, 63n., 81, 202, 210f.,
258n.
circumcision and, 79
