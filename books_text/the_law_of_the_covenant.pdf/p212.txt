Sabbaths and Festivals (Exodus 23:10-19} 193

however, and where the Church refuses to set up a festival calen-
dar, men simply use whatever pagan calendar surrounds them.
Churches which attack Christmas and Easter as “pagan” holidays
(because pagan cultures also celebrate feasts at these times of the
year) generally wind up making a big to-do about Thanksgiving,
New Year’s Day, and the Fourth of July, festivals which tend to
partake of the genuinely pagan idolatry of nationalism.
