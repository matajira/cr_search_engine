108 The Law of the Couenani

have to give. Parents do not have an absolute claim to honor,
Their claim is only goad if they live righteously according to God's
law. Lazy parents who leech from their young married children,
who refuse to go to church with them, and who undermine their
homes, should be shown the door, if the elders agree. They have
forfeited the right to honor by their actions. We must ever honor
God first, and parents second.*! Only in this way can we truly
honor parents.

At the same time, this is not to say that we should not put up
with senility in aged parents. It is simply to say that not all
parents have an equal claim to honor. The law is deliberately
phrased in a somewhat vague manner. The poor son will not be
able to honor parenis in the same way as the wealthy man. Bitter,
nasty, pagan parents do not have the right to the same kind of
privileges as saintly, Godly parents do. Any case that comes be-
fore the judges, therefore, will have to be assessed in terms of its
situation, The mother who claims that her son has “pushed me off
into an old folks’ home” will not necessarily get to see him die if he
can prove that she was intolerable to live with, and that in order
properly to cleave to his wife he had to put his mother at a
distance,

There is no sourid reason for rejecting this law. God wrote it
for His commonwealth, and man can hardly hope to improve on
it. Jesus repeated it, as we have seen, and applied it-to the people
of His day. Unless we believe we are smarter than God, we should
do well to take it seriously.

One closing remark on the order of these last three laws. At
first glance, it looks as if the two laws against attacking parents
should be together, instead of having the law against kidnapping
sandwiched between. The logic of the overall passage, however, is
to move from the greater to the lesser, in three interlaced ways:

1. Criminals and victims (people to people; animals to people;

21. “If any man come to me, and hate not his father, and mother, and wife,
and children, and brethren, and sisters, yea, and his own life also, he cannot be
my disciple” (Luke 14:26), Our covenant with Christ must take precedent over all
human covenants, if there is a conflict.
