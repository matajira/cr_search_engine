216 The Law of the Covenant

smaller than Messianic schools. There is much in the Messianic
curriculum which need not be in that of the Christian schools,

38. To take one example, experimental science. Science in the
Christian school should take the form of naturalism, the study of
the “ways” of animals, building on the proverbs and observations
of Solomon. Experimentation and dissection are specialized and
technical studies, and have more to do with education for a calling
than education for the whole of life.

Medicine

39. Because of the involvement of the Levites in the cleansing
rituals of the leper (Lev. 13, 14), it has sometimes been maintained
that medicine is a proper use of the tithe. In the Bible, however,
there is a difference between sickness as such, which is “healed,”
and leprosy, which is “cleansed.” A woman on her period is un-
clean, but not sick. A child with measles is sick, but not unclean.
A leper is both sick and unclean. Uncleanness is “ceremonial” in
nature, not medicinal. Also, the Bible clearly distinguishes be-
tween the ritual healing work of Church elders (James 5:14) and
the Jabors of a physician such as Luke.

40, Most “medicine” in Scripture is preventalive, a side benefit
of the “ceremonial” law, and still instructive for us today. Child-
birth and general care of the sick was accomplished by midwives
and ather semi-professionals within the community. There is
really no reason to see the Levites as a class (in part) of profes-
sional healers. Medical care should be under free enterprise, and
its expenses covered by insurance policies, as we have it today.

41. Care for the poor, in the area of medicine and health in
general is, of course, a proper use of the tithe.

Advisors to the State

42, The Levites in Israel served as advisors to the state (Dt.
17:9, 18) and they sat in on court cases to help render judgments
by giving professional advice concerning the law of God (1 Chron.
23:4; 26:29-32; 2 Chron. 19:8-11), They were the closest thing toa
