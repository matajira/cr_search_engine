50 The Law of the Covenant

The Law Before Sinai

Paul tells us that the law was in operation before Sinai, when
he says “for until the law sin was in the world; but sin is not im-
puted when there is no law. Nevertheless death reigned from
Adam to Moses” (Rom. 5:13, i¢a). Before the law “came,” the law
was already in operation, for it was already dealing death to sin-
ners. (Similarly, before the New Covenant “came,” it was already
in operation, for it was already granting resurrection life to repen-
tent men.) At Sinai, the law was given a definitive publication,
but it was already operating.in the world, and was already known
to men.*

Indeed, Paul says “just as through one man sin entered into
the world, and death through sin, and so death spread to all men,
because all sinned” (Rom. 5:12). In other words, the same law
which came at Sinai was operating in the Garden. This is the con-
nection between the Old (Adamic) Covenant and the Old
(Sinaitic) Covenant.5

It is often thought that at Sinai God set up something new, a
new administration of law, which had not been in force previously.
We have seen from Paul that this was not the case, for the law was
in operation in the Garden, and in the period between the Fall
and Sinai. We can also turn to passages in Genesis and in Exodus
before Sinai and see that people knew the law before it was writ-
ten down by Moses.

First of all, we have demonstrated that the laws of slavery were
known and functioned in the life of Jacob and in the interaction
between Moses and Pharaoh. Second, the law of evidence concern-

4. In other words, in one sense the pro-Sinaitic period was one of “no law,’ for
law had not yet “come.” In another sense, however, the law clearly was in the
world, because sin is not imputed apart from law, and sin was clearly being im-
puted, as the fact of death demonstrates. Before Sinai, the law had already but
not yet come. This is parallel to the gospel, which had already but not yet come
during the Old Covenant; and parallel to the consummation, which has already
but not yet come in the New Covenant era.

5. On how I am using the terms ‘Old Covenant’ and ‘New Covenant,’ see Ap-
pendix A,
