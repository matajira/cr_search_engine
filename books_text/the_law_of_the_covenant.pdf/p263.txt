244 The Law of the Covenant

death. His wife appeased the angel by the blood of the child’s cir-
cumcision,”? Gregory comments further, “since his son had not
been circumcised so as to cut off completely everything hurtful
and impure, the angel who met them brought the fear of death.
His wife appeased the angel when she presented her offspring as
pure by completely removing that mark by which the foreigner
was known.”3 From all this Gregory learns that the Christian phi-
losophy may marry pagan philosophy (the wife) provided that the
offspring (a synthesis) has the corruption of pagan thought removed
(circumcision), so that only the good remains. Clever as this is, it
has nothing to do with the theclogy of Exodus.

Turning to the Reformation, we find that Calvin’s comments
are also speculative and do not derive from the text. God was
angry, says Calvin, because Moses had not circumcised his son,
and Moses had not circumcised him because Jethro and Zipporah
opposed it. The text nowhere indicates this latter notion; indeed, as
a descendant of Abraham, Jethro doubtless did know about cir-
cumcision, whether we understand it as a rite applying only to the
seed people, Isracl, or to all believers. Calvin goes on to assume
that Zipporah was angry, threw the foreskin at Moses’ feet, and
“fiercely reproaches him with being ‘a bloody husband, ”* Nothing
in the text indicates any anger; the phrase “bridegroom of blood” is
not the same as “husband of blood”; the Hebrew word for “touch”
does not mean “throw.” Finally, we have to say that Calvin, like
Gregory of Nyssa, ignores the context, which is God’s announced
threat against the firstborn sons of all persons living in Egypt.5

2. Book I, para. 22. Trans. by Abraham Malherbe and Everett Ferguson
(New York: Paulist Press, 1978), p. 35.

3. Book II, para. 38, p. 63.

4, Commentaries on the Last Four Books of Moses, trans. by Charles W. Bingham
(Grand Rapids: Baker, reprint 1979), comments ad loc. See also Calvin, Znstituies,
Book 4, Chapter 15, Section 22, for further comments on Zipporah.

5. Calvin’s approach illustrates a danger in the Antiochene or “grammatical-
historical” approach to exegesis. The Antiochene school, under the influence of
Theodore of Mopsuestia, a quasi-Nestorian, can fairly be said to have erred in
the direction of naturalism, at the expense of the literary structures of Scripture.
The problem ties not in the “grammatical” side of the method, but in the
“historical” side. Modern evangelical Bible commentaries, using this method,
