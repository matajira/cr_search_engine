The Book of the Covenant: [ts Context 53

two million people to deal with. People were lined up all day long
for judgments.

The wise Jethro right away saw that a corrective was needed,
and asked Moses what his time was taken up with. Moses ex-
plained to Jethro that he had two problems (Ex. 18:16): First, he
had to judge every case, and second, he needed to teach the peo-
ple the laws and statutes of God. Taking the problems in reverse
order, Jethro told Moses to get the law from Cod and to teach it to
the people (v, 20). As regarded rendering judgments, Moses was
to divide the nation into groups of households, with judges over
groups of tens, of fifties, of hundreds, and of thousands (vv.
21-22). If a judgment in a specific case became too difficult, or if it
was appealed, it would be taken up, through the ascending series
of courts, so that Moses would only have to hear the toughest
cases (v. 26). Examples of this procedure can be seen in Leviticus
24:10-23, Numbers 27:1-11, and 1 Kings 3:16-28.

In this way, Exodus 18 sets the stage for the giving of the law.
There needed to be a written book of law for use by the many
judges now present in Israel.

A few comments on these judges are in order. According to
Deuteronomy 1:15, Moses “took the heads of your tribes, wise and
experienced men, and gave them as heads over you, leaders of
thousands, and leaders of hundreds, leaders of fifties and leaders
of tens, and officers for your tribes.” The “officers” were a kind of
scribe, whose principal duty seems to have been the recording of
court decisions and the maintenance of genealogies.? Notice,
however, that Israel already had elders or leaders in each tribe,

9. In Hebrew, shoterim. The New Covenant deacon, as an assistant to the
elder, is probably the New Testament equivalent. “The meaning of the root, by
analogy with its Semitic cognates, is ‘wrile’; Assyr, Satara, Arab. satara. In Exod.
§:6-19, the role of the shoferim (RSV ‘foremen’) was to keep tally of the building
supplics.” P. G, Craigie, The Book of Deuteronomy (Grand Rapids: Eerdmans,
1976), p. 98. In military situations, the officer would be the equivalent of a
quartermaster. On their duties to record genealogies, see E. C. Wines, The
Hebrew Republic (The American Presbyterian Press, Box 46, Uxbridge, MA
01569 [1980], p. 102). The frequency with which the officers are said to be present
with the elders or judges indicates their role as court recorders.

 
