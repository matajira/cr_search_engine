Tommy Ice: A Response — Part I 109

however, has been a simplistic confusion in the minds of many
that historical succession means necessary and logical connection
and succession. Hence, it is held, because postmillennialism was
the original kingdom of God idea in America, the social gospel
idea of the kingdom of God is a logical and necessary product of
postmillennialism. This “proves” too much, Niebuhr gives us
three stages in the theological motive forces of American history:
a) the kingdom of God, God as sovereign, a Calvinistic, postmil-
lennial faith, b) the kingdom of Christ, Arminian, revivalistic,
and concerned with soul-saving, amillennial and premillennial in
eschatology; c) the social gospel kingdom of God, modernisti
humanistic, and socialistic (or statist). It is highly illogical and
rational to jump from “a” to “c”; if Calvinistic postmillennialism is
the cause, then Arminianism and revivalism are also its logical
products! Then too we must hold that Arminian revivalism led to
the social gospel and to modernism! But history is not merely log-
ical development; it involves rival faiths, and their rise and fall.

 

Christian Reconstructionists have no ties with liberalism, and
postmillennialism no more leads to liberalism than dispensational
premillennialism leads to cultism.® All Reconstructionists believe

59. R. J. Rushdoony, “Postmillennialism Versus Impotent Religion,” The four-
nal of Christian Reconstruction: Symposium on the Millennium, p. 122.

60. Some of the major cults are premillennial; Jehovah's Witnesses, Mor-
monism, Children of God, and The Worldwide Church of God (Armstrongism).
Moreover, you can find elements of premillennialisra in the Manifest Sons of
God. In fact, most cults are premillennial in their eschatology. Here’s an example
of premillennial thinking from the Worldwide Church of God:

And Christ will rule over and judge nations, UNTIL they beat their
swords into plowshares, their weapons of destruction into implements of
peaceful PRODUCTION.

When, at last God takes over puts down Satan, and the WAYS of
Satan’s world... then the NEW World—The WORLD TOMOR-
ROW shall reap what it then sows— PEACE, HAPPINESS, HEALTH,
ABUNDANT, INTERESTING LIVING, OVERFLOWING JOY! It
will be a Utopia beyond man's fondest or wildest dreams. Herbert W.
Armstrong, 1975 in Prophecy (Pasadena, CA: Ambassador College Press,
1952), p. 31. Quoted in Paul N, Benware, Ambassadors of Armstrongism: An
Analysis of the History and Teachings of the Worldwide Church of God (Nutley,
NJ: Presbyterian and Reformed, 1975), p. 72.
