14

TOMMY ICE: A RESPONSE
Part II

Tommy Ice continued his critique of Christian Reconstruction
with a brief exposition of Matthew 24:1-34. For Ice, this seemed to
be the central issue in the debate. He maintained that Matthew
24:1-34 describes the great tribulation, the rapture, and the return
of Christ —all future events. Ice does believe that some of Mat-
thew 24:1-34 has already been fulfilled. This is a convenient way
to look at the Olivet Discourse, but it leads to interpretive sub-
jectivity. How can we distinguish between those verses that per-
tain to an a.p, 70 fulfillment and those verses that address a yet
future fulfillment? With this approach, the interpreter is left with
the pick and choose method. But if the interpreter allows the time
texts of Matthew 23:36 and 24:34 to be his guide and the Bible to
define terms and phrases, there is no problem fitting the entire
section within an a.p. 70 fulfillment.

So What?

But there is a more basic issue to consider. Why was a single
interpretation of Matthew 24:1-34 made the keystone in a debate
over Christian Reconstruction? There is nothing novel about a
preterist interpretation of this passage. As was noted in the previ-
ous chapter, the fourth-century historian Eusebius interpreted the
Olivet Discourse as having its fulfillment in a.v. 70.4 So then,

1. Eusebius, in recounting the writings of Josephus and his description of the

destruction of Jerusalem by the Romans in 4.p. 70, writes that

it is fitting to add to these accounts [of the secular historian Josephus) the
true prediction of our Saviour in which he foretold these very events. His

1
