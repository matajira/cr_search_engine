186 The Debate aver Christian Reconstruction

mean? For Tommy Ice, dispensationalists must go elsewhere to
formulate their “ethical positions” as they relate to sucial issues.
This is quite an admission, What “contemporaries” is he talking
about? Are these Christian contemporaries or non-Christian con-
temporaries? If they are Christian contemporaries, then what
theological system are they using to develop their social ethic?
According to Tommy Ice, it can’t be dispensationalism. Recon-
structionists insist that the Bible gives a comprehensive system of
ethics for all aspects of society. Christians do not have to “pick up”
on any system other than the one the Bible outlines.

Tommy Ice tells us that “premillennialists have always been
involved in the present world.” There is no doubt that this is true.
But the dispensationalist, according to Ice, must look to his con-
temporaries for assistance. So then, the basis of involvement for
the dispensationalist is borrowed from systems that Ice criticizes.
Why doesn’t Ice say that Christians should not be involved in the
present world? Since he believes that his position is the biblical
position, and a theory of social involvement cannot be developed
within dispensationalism, then why is he still for involvement in
the present world?

He then goes on to insist that the motivation for ethical living is
the imminent return of Jesus Christ. The issue in this debate,
however, is not motivation. There are a number of things that can
motivate Christians to live holy lives: fear, love, the kindness of
God, gratitude, and the final judgment, Rather, the issue is what
standard has God outlined for us to follow once we are motivated to
live a holy life? Motivation is not enough. Many evil things have
been done in the name of “good motivation.” What standard will
Jesus use to judge us when He does return? This is the substance
of the debate.

Reconstructionists insist that God has given us His law as a
guide for proper behavior. There will then be no surprises on
judgment day. In fact, God has saved us so that we can walk in
terms of God's law.

When Paul says that we are saved by grace through faith, he
immediately adds that as God’s workmanship we are expected to
