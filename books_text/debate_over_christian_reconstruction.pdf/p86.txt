68 The Debate over Christian Reconstruction

And now, Israel, what does the Loxp require from you, but to
fear the Lorn your God, to walk in all His ways and love Him,
and to serve the Lor your God with all your heart and with ail
your soul, and to keep the Lor’s commandments and His stat-
utes which I am commanding you today for your good (Deuter-
onomy 10:12-13),!5

So keep and do them, for that is your wisdom and your
understanding in the sight of the peoples who will hear all these
statutes and say, “Surely this great nation is a wise and under-
standing people.” For what great nation is there that has a god so
near to it as is the Lorp our God whenever we call on Him? Or
what great nation is there that has statutea and judgments as
righteous as this whole law which I am setting before you today?
(Deuteronomy 4:6-8),

Righteousness exalts a nation, but sin is a disgrace to any
people (Proverbs 14:34).

But we know that the Law is good, if one uses it lawfully,
realizing the fact that law is not made for a righteous man, but
for those who are lawless and rebellious, for the ungodly and sin-
ners, for the unholy and profane, for those who kill their fathers
or mothers, for murderers and immoral men and homosexuals
and kidnappers and liars and perjurers, and whatever else is con-
trary to sound teaching (1 Timothy 1;8-10; cf. 4:8; Matthew 6:33;
Romans 13:1-4).

Postmillennialism

The third Reconstructionist distinctive is postmillennialism,
the victorious advance of God's established kingdom throughout
history. Dave Hunt wants us to believe that the Christian’s imme-
diate hope is the rapture, an event in dispensationalism that is sep-
arated by 1007 years from the judgment seat of Christ. It is only

15. Some would counter that these verses apply strictly to Israel as an ethnic

people. But the Bible tells us that the church, made up of Jews and Gentiles, is
now “a chosen race, a royal priesthood, a holy nation, a people for God’s own
possession” (1 Peier 2:9). Jesus described it this way: “The kingdom of God will
be taken away from you [unbelieving Jews], and be given to a nation producing
the fruit of it [believing Jews and gentiles? (Matthew 21:43; cf, Mark 12:1-12;

Luke 20:9-19),
