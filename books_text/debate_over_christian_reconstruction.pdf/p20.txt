2 The Debate over Christian Reconstruction

The debate question was: Is Christian Reconstruction a de-
viant theology? As The Debate Over Christian Reconstruction will
show, the basic tenets of Christian Reconstruction are thoroughly
orthodox. The April 14th debate did nothing to change the long-
standing and authoritative opinions of other studies on Christian
Reconstruction.

Let's look at four independent studies on the orthodoxy of
Christian Reconstruction and the narrower topic of biblical law,
which is one of Christian Reconstruction’s major distinctives.

The Presbyterian Church in America

The General Assembly of the Presbyterian Church in Amer-
ica made the following recommendations on the issue of theon-
omy (biblical law), a basic tenet of Christian Reconstruction:

1, That since the term “theonomy” in its simplest definition
means “God’s Law,” the General Assembly affirms the Westminster
Confession of Faith, Chapter 19, and the Larger Catechism, Questions
93-150, as a broad but adequate definition of theonomy.

2. That no further study of the subject of theonomy be un-
dertaken at the General Assembly level at this time, but that indi-
vidual Christians, sessions, and presbyteries having particular
interest be encouraged to study the subject in a spirit of love,
kindness, and patience.

3, That the General Assembly affirm that no particular view
of the application of the judicial law for today should be made a
basis for orthodoxy or excluded as heresy.

4. That the General Assembly encourage pastors and ses-
sions to instruct their people in the Law of God and its applica-
tion in a manner consistent with our confessional standards.*

3. The Presbyterian Church in American (PCA) was created in 1973 as a con-
servative Presbyterian alternative to the liberal mainline Presbyterian churches.
In the past 15 years, the PCA has grown from 240 congregations and 40,000
members to 1046 churches and 160,000 members. In short, the PCA is not a radi-
cal fringe denomination. These statistics ate drawn from Frank Trexler’s June
13, 1988, Religious News Service report on the 1988 PCA General Assembly.

4. “Report on Theonomy,” Minutes of the Seventh General Assembly of the
Presbyterian Church in America, 1979, p. 195.
