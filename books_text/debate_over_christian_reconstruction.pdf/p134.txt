116 The Debate over Christian Reconstruction

would occur within the contemporary generation (within a forty
year period), then we must take Him at His word. Dispensation-
alists insist on literalism. Why not in this instance?

Third, the use of “this generation” throughout the Gospels
makes it clear that it means the generation to whom Jesus was
speaking. [t never means “race,” as many dispensationalists and
non-dispensationalists assert, or some future generation, The ad-
jective “this” points to the contemporary nature of the generation.
If some future generation were in view, Jesus could have chosen
the adjective “that”: “That [future] generation which begins with
the budding of the fig tree [Israel regathered to the land of their
fathers] will not pass away until all these things take place.”

Here is a list of every occurrence of “generation” or “this gener-
ation” in the Gospels: Matthew 1:17; 11:16; 12:39, 41, 42, 45; 16:4;
17:17; 23:36; 24:34; Mark 8:12, 38; 9:19; 13:30; Luke 1:48, 50;
7:31; 9:41; 11:29, 30, 31, 32, 50, 51; 16:8; 17:25; 21:32. In each and
every case, these verses describe events occurring within a current
time frame. David Chilton summarizes the argument succinctly:

Not one of these references is speaking of the entire Jewish race
over thousands of years; aif use the word in its normal sense of the
sum total of those living at the same time, It always refers to contem-
poraries. (In fact, those who say it means “race” tend to acknowl-
edge this fact, but explain that the word suddenly changes its
meaning when Jesus uses it in Matthew 24! We can smile at such
a transparent error, but we should also remember that this is very
serious. We are dealing with the Word of the living God.)®

Fourth, notice how many times Jesus uses the word “you” in the
parallel passage in Luke 21: “They will lay their hands on_you and
will persecute you, delivering you to the synagogues and prisons,
bringing you before kings and governors for My name's sake” (v.
12; see verses 13, 14, 15, 16, 17, 18, 19, 20, 28, 30). Matthew 24 and
Mark 13 use the same contemporary address. Now, if you heard

4. David Chilton, The Great Tribulation (Ft. Worth, TX: Dominion Press,
1987), p. 3.
