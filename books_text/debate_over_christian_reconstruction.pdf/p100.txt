82 The Debate over Christian Reconstruction

stuck with such a conclusion since it follows from his basic prem-
ises: His view of interpreting the Bible (a “literal” hermeneutic), "
the distinction between Israel and the church, an imminent pre-
tribulational rapture followed by a seven-year tribulation, and a
thousand year reign of Christ on the earth. Anyone who does not
hold these fundamentals is not “rightly dividing the word of truth.”
How can anyone be considered “orthodox” if he does not interpret
the Bible “literally”? How can anyone possibly be orthodox if he
mixes the promises made to God’s earthly people (physical Israel)
with His heavenly people (the church)? How can anyone be or-
thodox if he denies the next great eschatological event: the secret
pre-tribulational rapture of the church?

In fact, as this book will show, Dave Hunt has made the timing
of the rapture the litmus test of orthodoxy. A denial of his timing of
the rapture is the denial of orthodoxy. But even pre-tribulational

18. Actually, even the dispensationalist is not consistent in his “literal” her-
meneutic, For example, Charles Ryrie, a noted dispensational scholar, has writ-
ten of Revelation:

How do we make scnsc out of all those beasts and thrones and horse-
men and huge numbers like 200 million? Answer: Take it at face value,
(Ryrie, The Living End, p. 37).

Later he gives an example of the usefulness of his “face value” hermeneutic in
seeking the correct interpretation of Revelation 9:1-12 (the locusts from the abyss):

John’s description sounds very much like some kind of war machine
or UFO. Demons have the ability to take different shapes, so it is quite
possible that John is picturing a coming invasion of warlike UFOs. Until
someone comes up with a satisfactory answer to the UFO questiod, this
possibility should not be ruled out. (Zéid., p. 45)

A literal interpretation would mandate that locusts would be locusts. Hal
Lindsey makes the locusts “cobra helicopters.”

Ihave a Christian friend who was a Green Beret in Viet Nam. When
he first read this chapter he said, “I know what these are, P've seen hun-
dreds of them in Viet Nam. They're cobra helicopters!”

That may just be conjecture, but it does give you something to think
about! A Cobra helicopter dees fit the composite description very well.
They also make the sound of “many chariots,” My friend believes that the
means of torment will be a kind of nerve gas sprayed from its tail.

Lindsey, There's 2 New World Coming (New York: Bantam Books,
1984), p. 124.
