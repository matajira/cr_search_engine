196 The Debate over Christian Reconstruction

Dave Hunt says that he has not abdicated his “responsibility to
mankind.” He goes on to say:

We have a responsibility not just to feed people, but also to
preach the Gospel to them. And there are people out there who
put the emphasis in the wrong place. Who, for example, will say
that Jesus never preached to any multitude without first of all
feeding them. Well, you recall the case where there were 5,000
people and Jesus said to the disciples “give them something to
eat.” The disciples said, “Well, we don’t have anything. Why
don’t you just send them home and they can buy something on
the way.” Jesus said, “They have been with me three days and
three nights and they haven’t had a thing to eat.” He preached to
them on empty stomachs, He felt there was something more im-
portant. He said, “If we send them away without any food now,
they will perish in the way.” So, I kind of resent being told that we
don’t have any concern for the social and ethical issues in our
world. We certainly do. But, there is something more important
and that is their eternal salvation.

Reconstructionists agree with Dave Hunt that the most important
thing for the Christian is the salvation of the lost. But, again, this
is not the issue in the debate. What does the new creature in
Christ do for the next forty or fifty years of his life? Certainly he
should lead others to Christ. But what then should ¢hese new con-
verts do? How does the Christian live now? Does the Bible have
anything to say about the family, economics, education, law, car-
ing for the poor, politics, journalism, and so forth? This is what
Christian Reconstruction is all about.

With the imminent rapture doctrine, preaching the gospel is
the only Christian activity because there is no time to pursue
long-term reconstruction, But if Jesus does not return for another
thousand years, then the world will be worse off than it is today
because Christians would have done very little to reconstruct their
world. The world will degenerate into hopeless misery. Our salt
will have become “tasteless.” It will be “thrown out and trampled
under foot by men” (Matthew 5:13). If previous generations of
Christians had had the same short-term, anti-dominion perspec-
tive as Tommy Ice and Dave Hunt, then the freedoms that we now
