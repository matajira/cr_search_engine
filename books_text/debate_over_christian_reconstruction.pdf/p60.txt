42 The Debate over Christian Reconstruction

God's law: “Thy Word is a lamp unto my feet and a light unto my
path” (Psalm 119:105).

17, Franklin’s appeal to Congress during the drafting of the
Constitution reads in part: “In the beginning of the contest with
Britain, when we were sensible of danger, we had daily prayers in
this room for Divine protection. Our prayers, Sir, were heard ~
and they were graciously answered. All of us who were engaged in
the struggle must have observed frequent instances of a superin-
tending Providence in our favor... . And have we now forgot-
ten that powerful Friend? Or do we imagine we no longer need its
assistance? I have lived, Sir, a long time, and the longer I live the
more convincing proofs I see of this truth, that God governs in the
affairs of men. And if a sparrow can not fall to the ground without
His notice, is it probable that an empire can rise without His aid?

“We have been assured, Sir, in the sacred writings, that ‘except
the Lord build the house, they labor in vain that build it’ [Psalm
127:1]. I firmly believe this and I also believe that, without His
concurring aid, we shall succeed in this political building no bet-
ter than the builders of Babel. . . .”6

18. The Supreme Court narrowly defined the legal protec-
tions of the First Amendment to exclude polygamy on the grounds
that the practice was out of accord with the basic tenets of Christi-
anity: “It is contrary to the spirit of Christianity and the civiliza-
tion which Christianity has produced in the Western world.”? A
year earlier the Court declared that “Bigamy and polygamy are
crimes by the laws of all civilized and Christian countries. . . . To
call their advocacy a tenet of religion is to offend the common
sense of mankind.”®

19, The highest office in our land demands the greatest wis-
dom. King Solomon learned this early in his political career.

6. Benjamin Franklin, “Motion for Prayers in the Convention,” The Works of
Benjamin Franklin, Federal edition, ed. John Bigelow (New York and London:
The Knickerbocker Press, 1904), 11:337-338.

7. Late Corporation of the Church of Jesus Christ of Latter Day Saints v. United States,
136 U.S. 1 (1890).

8. Davis » Beason, 133 U.S. 333, 341-342 (1890). Cited in John Eidsmoe, The
Christian Legal Advisor (Milford, MI: Mott Media, 1984), p. 150.
