Questions and Answers 201

eousness. But ’m not sure how many dispensationalists would
agree with Tommy Ice on this point.

But if keeping the law is simply a wise thing to do, with no ob-
ligation on our part, then how can God hold anyone accountable
for breaking the law? Is abortion wrong? Why? Well, we would
say the Bible opposes abortion. Are civil governments obligated to
follow the laws that apply to the criminalization of abortion? If
they are not, then Christians have no real standard by which to
influence legislation. We could only go with the wisdom ap-
proach: “It would be wise for you to outlaw abortion, but you
have no obligation to do so,” Ice seems to support this idea with
the following:

‘The book of Proverbs, in my opinion, is a whole development
of meditating on Old Testament law and coming up with wisdom
or advice that are general principles. There’s not Jaw in Proverbs.
It says that a wise person will do this. Therefore, since we are re-
generate in the New Testament, we love the law because it re-
flects God's character, but we're not under the law. Therefore, it
was totally done away with as a legal contract and ethical system.

This is an impossible situation. According to Ice, we no longer
have law, but only “wisdom or advice.” Yes, the Proverbs do rest
on the Old Testament law. That’s just the point. You can’t under-
stand and apply the Proverbs unless you understand the law.
Murder is wrong. The Bible says that murder is wrong. A mur-
derer cannot claim to be innocent because he doesn’t have enough
wisdom to tell him that it’s wrong. Certainly, more difficult laws
take a great amount of wisdom to interpret and apply. But wis-
dom is not a substitute for the law. Wisdom is something we pray
for: “But if any man lacks wisdom, let him ask of God, who gives
to all men generously and without reproach, and it will be given
to him” (James 1:5).

This next quotation by Tommy Ice gives us some insight on
how this wisdom approach is to be applied:

We're like Daniel when we're in a foreign country. We Jook to
the law to be our counselor. Daniel was a counselor who whis-
