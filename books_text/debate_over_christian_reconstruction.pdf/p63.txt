9
TRUE AND FALSE SPIRITUALITY

Failure to develop a comprehensive world view often is related
to a false view of spirituality. To be “spiritual” means to be gov-
erned by the Holy Spirit. For many, spirituality means to be pre-
occupied with non-physical reality. Therefore, to be spiritual
means not to be involved with the material things of this world.

The unbiblical idea of “spirituality” is that the truly “spiritual”
man is the person who is sort of “non-physical,” who doesn’t get
involved in “earthly” things, who doesn’t work very much or
think very hard, and who spends most of his time meditating
about how he'd rather be in heaven. As long as he’s on earth,
though, he has one main duty in life: Get stepped on for Jesus.
The “spiritual” man, in this view, is a wimp. A Loser, But at least
he’s a Good Loser.t

The devil and his demons are spiritual (non-physical) and
evil: “And I saw coming out of the mouth of the dragon and out of
the mouth of the beast and out of the mouth of the false prophet,
three unclean spirits like frogs; for they are spirits of demons, perform-
ing signs, which go out to the kings of the whole world, to gather
them together for the war of the great day of God Almighty” (Rev-
elation 16:13-14). There are “deceitful spirits” (1 Timothy 4:1),
“unclean spirits” (Revelation 18:2), and spirits of “error” (1 John
4:6). There is even “spiritual wickedness” (Ephesians 6:12),

1. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Tyler, TX:
Dominion Press, 1985), pp. 3-4.

45
