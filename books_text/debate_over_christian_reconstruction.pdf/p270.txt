252 The Debate over Christian Reconstruction

kingdoms of the world are become his, and his name called upon
from the rising of the sun to its going down. (1) That the old off-
casten Israel for unbelief would never be forgotten, especially in
these meetings, that the promised day of their ingrafting again by
faith may be hastened; and that the dead weight of blood re-
moved off them, that their fathers took upon them and upon their
children, that have sunk them down to hell upwards of seventeen
hundred years.2!

Puritan Independent Thomas Goodwin, in his book, The Return of
Prayers, encouraged people to pray even when they failed to see
their desires realized. Among the things for which the Church
should pray were “the calling of the Jews, the utter downfall of
God's enemies, the flourishing of the gospel.” Goodwin assured his
readers that all these prayers “will have answers.”2?

Eighteenth-Century America

Jonathan Edwards, a postmillennialist’s postmillennialist,
outlined the future of the Christian Church in his 1774 History of
Redemption. Edwards believed that the overthrow of Satan’s king-
dom involved several elements: the abolition of heresies and
infidelity, the overthrow of the kingdom of the Antichrist (the
Pope), the overthrow of the Muslim nations, and the overthrow of
“Jewish infidelity”:

However obstinate [the Jews] have been now for above seventeen
hundred years in their rejection of Christ, and however rare have
been the instances of individual conversions, ever since the de-
struction of Jerusalem .. . yet, when this day comes, the thick
vail that blinds their eyes shall be removed, 2 Cor. iii.16. and
divine grace shall melt and renew their hard hearts... And
then shall the house of Israel be saved: the Jews in all their disper-
sions shall cast away their old infidelity, and shall have their
hearts wonderfully changed, and abhor themselves for their past
unbelief and obstinacy.

21. Quoted in sid:, pp. 101-102.
22. Quated in id. p. 102.
