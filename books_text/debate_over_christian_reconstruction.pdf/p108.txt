90 The Debate over Christian Reconstruction

1, Postmillennialism and Scripture

Tommy Ice opened the debate by making this charge against
postmillennialism: “The postmillennial position, as it is advocated
by the reconstructionist movement, really doesn’t have any pas-
sages that teach it.” This assertion hinges on where in eschatologi-
cal time you put the thousand years of Revelation 20. Some amil-
lennialists (many prefer to be called “realized millennialists”)}3 be-
lieve that the great millennial blessings embrace the entire church
age. Those prophecies that are not fulfilled in the church age are
fulfilled during the establishment of the “new heavens and new
earth.”!* So then, Tommy Ice’s charge is equally applicable to the
amillennialist since the “millennium” of Revelation 20 is a present
reality for him.

The “Thousand Years” in Postmillennialism

Postmillennialists assert that the millennial blessings refer pri-
marily to a future era, although many are progressively manifested
during the gospel or church age. Some believe that the thousand
years of Revelation 20 and the millennial blessings set forth in
Scripture are yet future, to be fulfilled in some type of “golden
age.” This seems to be the view of the Westminster Confession of
Faith and other confessions of the seventeenth century.'5

13. Jay E, Adams, The Time is at Hand (Nutley, NJ: Presbyterian and Re-
formed, 1970), pp. 7-!; Anthony A, Hoekema, “Amillennialism,” in Robert G.
Clouse, ed., The Meaning of the Millennium; Four Views (Downers Grove, IL: Inter-
Varsity Press, 1977) pp. 155-56; and Hockema, The Bible and the Future (Grand
Rapids, MI: Eerdrnans, 1979), pp. 173-74

14. For a rehearsal of the major tenets of amillenniulism, see the following
books: Adams, The Time is at Hand; Everett I. Carver, When Jesus Comes Again
(Phillipsburg, NJ: Presbyterian and Reformed, 1979); Hockema, The Bible and
the Future; William E, Cox, Amillennialism Today (Nutley, NJ: Presbyterian and
Reformed, 1966); William Hendriksen, The Bible on the Life Hereafter (Grand Rap-
ids, MI: Baker Book House, 1959); Hendriksen, More than Conquerors: An Inter-
pretation of the Book of Revelation (Grand Rapids, MI: Baker Book House, [1940]
1982).

15, “As the Lord is in care and love towards his Church, hath in his infinite
wise providence exercised it with great variety in all ages, for the good of them
that Jove him, and his own glory; so, according to his promise, we expect that in
