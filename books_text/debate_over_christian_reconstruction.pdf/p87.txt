Understanding Christian Reconstruction 69

after this lengthy period (plus two thousand years of history since
Jesus’ birth) that Jesus will be victorious. He understands the rap-
ture as the “rescue of the church” rather than as the more biblical
doctrine of the ascension of the saints. The rapture is the prelude
to the return of Christ to deliver up the kingdom that was given to
Jesus at His first coming (1 Corinthians 15:20-28). The rapture is
not separated from the final return of Christ by a seven year tribula-
tion period or a thousand year millennial reign of Jesus on the earth.
To support his view that the rapture is simply a “rescue,” Mr.
Hunt refers us to Lot and his wife and daughters being “raptured”
out of Sodom, and to Israel’s being “raptured” out of Egypt.'® This
is strained biblical interpretation at best, and hardly in keeping
with dispensational literalism. Weren't the Jews also “raptured” out
of Israel into pagan Babylon? Must we sce the rapture in every
geographical movement of God’s people? In fact, Israel was led
out of Egypt and brought to the éand of Canaan to take dominion
over it. In the New Testament, Jesus tells us that we are the “salt
of the earth” and the “light of the world” (Matthew 5:13-14), Jesus
prays to His Father on behalf of the disciples not “to take them aut
of the world” ( John 17:15),"7 a clear contradiction of the rapturc as
rescue. If there is any given priority of order in Scripture, it is that
the lost will be taken first in judgment, since the tares are gathered
and burned before the wheat is gathered (Matthew 13:30).18

16. Post-tribulational futurists point out that Israel was protected from the
plagues upon Egypt while still in the land. This was especially true of the tenth
plague. And the emphasis in Revelation is upon sealing the tribulation saints. If
the tribulation saints are sealed and thus protected from God’s wrath, then
church saints do not have to be removed from planct earth to avoid God's wrath.
The context of 1 Thessalonians 5:9 does not logically require even a futurist to be
pretribulational.

17. The Greek in John 17:15 is almost identical to Revelation 3:10, one of the
few pretribulational rapture proof texts: “keep them from evil one” and “keep from
the hour of testing.”

18. In the first editian of the Scofteld Reference Bible, Scofield reverses the order:
“The gathering of the tares into bundles for burning does not imply immediate
judgment. At the end of this age (v. 40) the tares are set apart for burning, but
first the wheat is gathered into the barn (John 14.3; 1 Thes. 4.14-17)’ (p. 1016,
note 1}. Matthew 13:30 reads: “First gather up the tares

 
