Tommy Ice: A Response — Part IT 115

whom Jesus was speaking. Rather, it refers to the generation alive
at the time when these events will take place, There are a number
of difficulties with this position.

First, Jesus says that “this generation will not pass away until
all these things take place.” “All these things” take place within the
“this generation” time frame. There cannot be a partial fulfillment
in a.D. 70 and a partial but final fulfillment at the end of the twen-
tieth century. Nor can there be a gap between some of the a.p. 70
events and the events that lead up to the last days, nearly 2000
years from the time when Jesus first made the prophecy. This
would make nonsense of the passage. “This generation” and “all
these things” are tied together. The “this generation” of Matthew
24:34 is either the generation to whom Jesus was speaking or it’s a
future generation that experiences the prophetic fulfillment. It
cannot be some of both, Neither is there anything in the passage
to lead us to believe in some type of “double fulfillment,” where
these events repeat themselves in a future tribulational period
with a rebuilt temple.

Second, “this generation” means the generation to whom Jesus
was speaking. It is the contemporary generation. How do we know
this? Scripture is our interpreting guide. We do not have to specu-
late as to its meaning. Those who deny that “this generation”
refers to the generation to whom Jesus was speaking in the Mat-
thew 24 context must maintain that “this generation” means some-
thing different from the way it’s used in other places in Matthew
and the rest of the New Testament! Matthew 23:36 clearly has
reference to the Pharisees and their contemporary generation.
Why should we interpret “this generation” in Matthew 24:34
different from 23:36, since Jesus is answering His disciples’ ques-
tions regarding His statement to the Pharisees about their house ~
the temple —being left to them desolate in Matthew 23:36? The
usual rejoinder is, “Well, some of these events could not have been
fulfilled during the life of the apostles. There must be a future ful-
fillment even though ‘this generation’ seems to refer to those who
heard Jesus’ words.” This is not the way we should interpret
Scripture. If Jesus said that all the events prior to Matthew 24:34
