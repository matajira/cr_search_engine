208 The Debate over Christian Reconstruction

Grace is still grace. God will save those who turn to Jesus,
those who repent of their evil deeds. “Such were some of you,”
Paul says of the Corinthians (1 Corinthians 6:9). This does not
mean, however, that they will necessarily be freed from the tem-
porary punishment that accompanies these crimes. Thieves must
still pay restitution (Ephesians 4:28), and those who commit capi-
tal crimes may be put to death (Acts 25:11).

Ice also admits that “the law was given to a regenerate . . .
people . . . called Israel. It was part of their covenantal agree-
ment after they were delivered from Egypt, and it was given to
them to show them how to live.” Why can’t the law be given to a
regenerate people under the New Covenant to show them how to
live? The Reconstructionists insist that this is one of the main pur-
poses of the law.

Ice discusses the operation of the Spirit as our guide. The mature
Christian is “motivated and directed by the Spirit.” The “Word of
God is his guide.” Exactly. The Spirit uses His Word. The fulfill-
ing of the law is not antithetical to keeping the law. They are the
same thing. Being led by the Spirit does not free us from keeping
the law. The Spirit is not an independent source of law.

What about the issue of the law as a “schoolmaster”? Yes, the
law was Israel's “schoolmaster” to lead them to Christ, that they
could be justified by faith (Galatians 3:24), In this sense, the law
is done away with. This is simply a function of the law. As a reve-
lation of God’s moral standards, the law cannot change and still
binds us, although with significant modification as delineated
under the New Covenant.

The law, after we come to Christ, doesn’t cease to exist for the
Christian. Since sin is lawlessness, we need the law to know when
we are sinning (Romans 7:7). While it’s no longer a schoolmaster,
it remains a standard of righteousness for Christians.

Ice comments: “Let us fulfill the law. If we walk in the Spirit,
we will not fulfill the lust of the flesh.” As Christians, we are now
empowered so we can keep the law, In fact, Scripture tells us that
we were created for this very purpose: “For we are His workman-
ship, created in Christ Jesus for good works, which God prepared
