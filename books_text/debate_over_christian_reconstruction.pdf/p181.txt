Dave Hunt: A Response 163

wrath in this world before Jesus returns in judgment. This is
spelled out very clearly in Romans 13:

For rulers are not a cause of fear for good behavior, but for
evil, Do you want to have no fear of authority? Do what is good,
and you will have praise from the same; for itis a minister of God to
you for good. But if you do what is evil, be afraid; for it does not
bear the sword for nothing; for it is a minister of God, an avenger who
brings wrath upon the ane who practices evil (vv. 3-4).

You see, dominion isn’t an option. When Christians fail to ex-
ercise dominion, humanists take up the banner, and a nation,
Christians included, must live in terms of their dominion. The
kind of dominion that Dave Hunt fears is a dominion that domi-
nates unrighteously, whether by Christians or by humanists. We
certainly agree. But this question still remains: What standard
will be used to determine whether righteous dominion is taking
place? The Christian Reconstructionist says it’s the Bible. Dave
Hunt denies that there is a mandate to take dominion over cul-
tures, We disagree, and we believe that we have the Bible and his-
tory on our side.

The “Cultural Mandate”

Again, we find that Hunt fails to give any definitions. It seems
that Hunt wants to reduce “culture” to “environment.” Culture is
broader than the environment. Henry Van Til states that the term
“culture” is “that activity of man, the image-bearer of God, by
which he fulfills the creation mandate to cultivate the earth, to
have dominion over it and to subdue it.” He goes on to explain:

The term is also applied to the result of such activity, namely,
the secondary environment which has been superimposed upon
nature by man’s creative effort. Culture, then, is not a peripheral
concern, but of the very essence of life. It is an expression of
man’s essential being as created in the image of God, and since
man is essentially a religious being, it is expressive of his relation-
ship to God, that is, of his religion.
