138 The Debate over Christan Reconstruction

With this background, we can conclude that immediately after the
destruction of Jerusalem there would be a great shake-up of the
world powers. The church would be freed from the womb of Juda-
ism, and would go forth to “turn the world upside down.”

6. “And then the sign of the Son of Man will appear in the
sky, and then all the tribes of the earth will mourn, and they
will see the SON OF MAN COMING ON THE CLOUDS OF
THE SKY with power and great glory” (v. 30).

This single verse is one of the most difficult to interpret in light
of an a.p. 70 fulfillment, especially as translated in the New
American Standard Version. Is Jesus “coming down” to earth to
set up His thousand year millennial reign? Or, does His “coming”
refer to something else? Let’s begin by seeing what the Bible
means by God “coming on the clouds.” “Behold, the Lorn is
riding on a swift cloud, and is about to come to Egypt” (Isaiah
19:1). God riding on a swift cloud is an expression of His sover-
eignty over the nations as their judge.

He makes His clouds His chariots; He walks upon the wings
of the wind; He makes the winds His messengers; flaming fire
His ministers (Psalm 104:3).

God didn’t literally appear on a cloud, using them as a “chariot” as
He is depicted in Psalm 104:3. This is the language of judgment
and retribution. No one takes this literally.

This same language is used by Isaiah to describe God’s com-
ing in judgment upon Egypt. Who judged Egypt? God did. Did
the Egyptians “see” Him? Yes and no. They saw Him in the judg-
ment that He meted out. They understood and realized that He was
the true God by what they saw coming upon them. John Calvin
writes that Isaiah “speaks of the defeat of the Egyptians by the
Assyrians, and shews that it ought to be ascribed to God, and not,

31, Edward J. Young, The Book of Isatah, 3 vols. (Grand Rapids, MI: Rerd-
mans, 1969), vol. 2, p. 14.
