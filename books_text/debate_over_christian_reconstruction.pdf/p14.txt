xiv The Debate over Christian Reconstruction

could go on and on.!!

Over the last decade I have witnessed more slurs and misrep-
resentations of Reconstructionist thought than I have the heart or
ability to count, and I am thinking here only of the remarks made
by Christians in positions of leadership: elders, pastors, instruc-
tors, writers—those who bear the “greater accountability” since
they lead Christ’s sheep as teachers. This has forced me as an edu-
cated believer to stand back and look more generally at what is
transpiring in the Christian community as a whole with respect to
its scholarly integrity. And I am heart broken. It is difficult
enough for us to gain a hearing in the unbelieving world because
of its hostility to the Lord Jesus Christ and its preconception of the
lowly intelligence of His followers. The difficulty is magnified
many times over when believers offer public, obvious evidence of
their inability to treat each other’s opinions with careful accuracy.
Our “scholarship” is justly ridiculed by those who have been edu-
cated in institutions which have no commitment to Christ or His
Word, but who have the ethical integrity to demand as a prere-
quisite to acceptable scholarship that a student represent his op-
ponent fairly before proceeding to criticize or refute him. To use a
Pauline expression, “even the Gentiles” know better than to permit
imprecision and erroneous portrayals in a serious intellectual dis-
cussion. Yet Christians (1 include all of us) often seem to care little
for that minimal standard of scholarly respectability. How, then,
can we be taken seriously? How can we take ourselves seriously?

That holy and inspired Word of God, to which all of us swear
allegiance as followers of Christ (whether Presbyterians or Bap-
tists or charismatics or dispensationalists or Reconstructionists or
whatever), is profitable to us “for correction, for instruction in
righteousness” (2 Timothy 3:16). From it we should Jearn not to
speak carelessly: “See a man who is hasty in his words? There is
more hope for a fool than for him” (Proverbs 29:20). We should
learn to speak cautiously about others (e.g., Matthew 5:22; Psalm

i. See the Preface to the expanded edition of my Theonomy in Christian Ethics
(Phillipsburg, NJ: Presbyterian and Reformed, [1977] 1984).
