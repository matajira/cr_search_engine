Dave Hunt: A Response 175

ists teach.5+ The promises made to David have been fulfilled in
Christ at His resurrection and exaltation (Romans 8:34; Hebrews
1:3, 13; 10:12-13). Interestingly, the most quoted Old Testament
passage appearing in the New Testament is Psalm 110:1— which
speaks of Jesus present rule!

Similarly, there is no need for a rebuilt tabernacle or temple,
since Jesus has entered into the true tabernacle and cleansed the
heavenly things. In order to protect dispensationalism’s futuristic
interpretation of prophecy, however, such writers as Hal Lindsey
predict a future rebuilding of the tabernacle. In a discussion of
Acts 15:12-21, Lindsey places a gap of (at least) 2000 years be-
tween verses 14 and 15. Lindsey admits that verse 14, which
describes the admission of the Gentiles into God’s favor, was being
fulfilled in the first century. But when verses 15-21. talk about a
rebuilt “tabernacle of David,” Lindsey arbitrarily assumes that
this prophecy is still to be fulfilled. James's words, however, can-
not bear this misinterpretation. The whole point of the quotation
about David's tabernacle is that God is gathering together a new
Israel, in which the distinction of Jew and Greek is erased.

The Manifest Sons of God

How many of you know who the Manifest Sons of God are?
Do you know what they believe? No? Well, you're not alone. I
looked in a number of books on the cults, and I couldn’t find one
that dealt with this theological movement. (Of course, this doesn’t
mean that there are no books that deal with the movement. They
don’t seem, however, to be readily available for research purposes.)

Dave Hunt accuses Christian Reconstructionists of having a
theology similar to the Manifest Sons of God. But he never tells us

54. According to dispensationalism, Jesus “must reign on David’s throne on
the earth over David's people forever” J. Dwight Pentecost, Things to Come
(Grand Rapids, MI: Zondervan, °[1958] 1987), p. 112. But the Bible talks about
a heavenly throne (Revelation 12:5). The dispensationalist has an answer for
this. “This heavenly city will be brought into a relation to the earth at the begin-
ning of the millennium, and perhaps will be made visible above the earth” (Ibid.,
p. 546),
