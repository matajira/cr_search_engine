240 The Debate aver Christian Reconstruction

The idea of abomination is thoroughly Levitical. Unclean
food was called abominable, or literally detestable, because you
were to spit it out. If they ate detestable food, they would become
detestable, and God would spit them out. This is clearly set out in
Leviticus 11:43, 18:28, and 20:23; sce alsa Revelation 3:16. This
was all symbolic of sin, of course. It meant that God would spit
out the people if they corrupted themselves with idolatry, since the
unclean animals were associated with idols and with the idola-
trous nations, (Compare Paul’s “table of demons.”)

False worship is idolatrous worship. When the Jews rejected
Jesus and kept offering sacrifices, they were engaged in idolatry.
The fringes of their garments became “wings of abominations.”
This was the “wing of abominations” that took place in the Tem-
ple. It is why the ‘Temple was destroyed.

A full picture of this is provided in Ezckicl 8-11. I shall not ex-
pound the passage, but simply direct you to it. There you will see
that when the apostate Jews of Ezckiel’s day performed the sacri-
fices, God viewed them as an abomination. He called the holy
shrine an “idot of jealousy, that provokes to jealousy” (8:3). The
Jews had treated the Temple and the Ark as idols, and so God
would destroy ther, as He had the golden calf, Ezekiel sees God
pack up and move out of the Temple, leaving it empty or
“desolate.” The abominations have caused the Temple to become
desolate. Once God had left, the armies of Nebuchadnezzar swept
in and destroyed the empty Temple. (When we remember that
Ezekiel and Daniel prophesicd at the same time, the correlation
becomes even more credible.)

‘This is what happened in Matthew 24, Jesus had twice in-
spected the Temple for signs of leprosy (Lev. 14:33-47; the two so-
called cleansings of the Temple in John 2 and Matthew 21). Jesus
had found that the Temple was indeed leprous, and as the True
Priest He condemned it to be torn down, in accordance with the
Levitical law. “And Jesus came out from the Temple [leaving it

  

 

desolate; God departing] and was going away {compare Ezekiel],
when His disciples came up to point out the Temple buildings to
Him. And He answered and said to them, ‘Do you not see all
