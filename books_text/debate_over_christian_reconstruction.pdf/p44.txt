26 The Debate over Christian Reconstruction

the Spirit (Romans 6-7). God’s blessings should humble us,
reminding us again and again of how utterly undeserving we are.

All of this is well summarized in Mark 10;29-31. Jesus prom-
ises several things to His faithful disciples, those who have left
everything to follow Him.

First, He promises them earthly prosperity and enjoyment of
blessing. That these blessings are not merely future and heavenly
is indicated by several facts: Not only does Jesus say that His dis-
ciples will receive a hundred-fold reward “in the present age,” but
the blessings He lists are very concrete and earthly: houses, wives,
children, farms. In short, Jesus is here promising earthly rewards.

Second, Jesus promises that persecution will accompany these
blessings. His faithful disciples should therefore expect both,

Finally, Jesus promises that our reward will be fully realized in
the future age, in heaven. We should never let God’s blessings in
this world blind us to our ultimate hope and our final rest, which
is in heaven. Abraham became a wealthy man, but he continued
to look for a heavenly city, made without hands,

Jesus saw no contradiction in laying these three themes side-
by-side: earthly reward, persecution, heavenly reward. He prom-
ises all three, and we need to emphasize all three equally strongly.
