Questions and Answers 181

for 1800 years. There’s not a trace of it anywhere in church history
before 1830. This is not development. On the other hand, postmil-
Jennialism and amillennialism are truly developmental, There are
traces and a development of these positions throughout church his-
tory: From Athanasius (c. 296-373) and Augustine (354-430), from
Calvin (1509-1564) and the Puritans, and from Charles Hodge, B. B.
Warfield, and J. A. Alexander to present-day Reconstructionists.

Ice wants us to believe that premillennialism was held by
everyone —he says, “to a man”—~in the early church. But then he
modifies this assertion by stating, “as far as early church records
show.” We really don’t know if premillennialism was the one and only
view. Justin Martyr (c. 100-165) certainly recognized other ortho-
dox positions, and the creeds do not outline a specific millennial
position.* He also says that the early church’s views on eschatol-
ogy were “futuristic,” But all millennial positions are futuristic to
some degree.

Ice then makes an unsubstantiated accusation regarding the
influence of “Greek philosophy” and the development of “amillen-
nialism.” This is almost impossible to prove. A postmillennialist
could just as easily say that premiilennialism was influenced by
Judaism and its emphasis on an earthly millennial kingdom.
Arnold Ehlert traces dispensationalism back to “Jewish and pre-
Jewish thought.”* But we know that Jesus rejected the apostate
Jews’ understanding of the kingdom, and Paul had his greatest
battles with these Jews. We also know that Greek influence had an
early impact on the church during the apostolic age (1 Corinthians
1:18-25; Colossians 2:8) as well as the compromised worldview of
the Nicolaitans (Revelation 2:6, 15). Using Tommy Ice’s logic,
couldn’t we just as easily assert that premillennialism grew out of
apostate Judaism or Greek philosophy since these are closer to the
time of Justin Martyr than they are to Athanasius and Augustine?
This would be just as impossible to prove as is Ice’s inference
about Greek philosophy and Augustine.

3. See the discussion of this in chapter 12.
4, Bass, Backgrounds to Dispensationalism, pp. 15-16. See also Leon Morris, The
Revelation of St. John (Grand Rapids, MI; Eerdmans, 1969), p. 234.
