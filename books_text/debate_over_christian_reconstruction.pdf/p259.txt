The Abomination of Desolation: An Alternate Hypothesis 241

these things? Truly I say to you, not one stone here shall be left
upon another which will not be torn down’” (Matt. 24:1-2).

(Note that the counterfeit Ark is removed from Israel right
after a description of house-leprosy in Zechariah 5:4. The
message in Zechariah was that when Gad’s Temple was rebuilt,
wickedness would be removed. This is a type of the New Cove-
nant: When the Church was established at Pentecost, God sent
leprosy into the Temple, and it became a seat of wickedness.)

With this background we can interpret Daniel 9:27b much
more clearly: “And on the wing of abominations [apostate Jewish
clothing of the High Priest] will come one who makes desolate
[the apostate High Priest], even until a complete destruction, one
that is decreed, is poured out on the one who makes desolate fat
the destruction of Jerusalem in a.p. 70].” Thus, verse 27 is simply
an explanation of verse 26. Verse 26 says that the Messiah will be
sacrificed; verse 27 explains that this ends the sacrificial system.
Verse 26 says that the Roman invasion will desolate the Temple
and that it is determined. Verse 27 says that wrath will be poured
out on the apostate Jews and their High Priest, whose actions
desolated the Temple, and that this is decreed.

This correlates magnificently with 2 Thessalonians 2, as follows:

3. Let no man in any way deceive you, for it [the Day of the
Lord] will not come unless the apostasy [of the Jews] comes first,
and the man of lawlessness is revealed, the son of destruction {the
apostate High Priest, prince of the Temple, no longer a man of
God’s law but a man of lawlessness, no longer a son of Abraham
but a son of destruction].

4, Who opposes and exalts himself against all that is called
God and every object of worship, so that he takes his seat in the
Temple of God, displaying himself as being God. [The High
Priest had opposed Christ and God, and thus had opposed the
truc meaning of all the worship objects in the Temple. “The
scribes and Pharisees have seated themselves in the seat af Moses,”
Matthew 23:2. Those who reject God make themselves God,
Genesis 3.]
