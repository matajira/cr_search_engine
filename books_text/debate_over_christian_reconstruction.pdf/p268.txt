250 The Debate over Christian Reconstruction

“The end of this world shall not be till the Jews are called, and
how long after that none yet can tell.”"3

Speaking before the House of Commons in 1649, during the
Puritan Revolution, John Owen, a postmillennial theologian,
spoke about “the bringing home of {[God’s] ancient people to be
one fold with the fulness of the Gentiles . . . in answer to millions
of prayers put up at the throne of grace, for this very glory, in all
generations.”'* Owen even believed, as he explained in his popu-
lar 1677 book, Israel Redux, that the Jews would someday return to
the land of Palestine. 5

Creeds and Confessions

Councils of the English and Scottish churches also addressed
the question of Israel. The Westminster Larger Catechism, Ques-
tion 191, displayed the same hope for a future conversion of the
Jews. Part of what we pray for in the second petition, “Thy king-
dom come,” is that “the gospel [be] propagated throughout the
world, the Jews called, the fullness of the Gentiles brought in.”
Similarly, the Westminster Directory for Public Worship directed
ministers to pray “for the Propagation of the Gospel and Kingdom
of Christ to all nations, for the conversion of the Jews, the fullness
of the Gentiles, the fall of Antichrist, and the hastening of the sec-
ond coming of the Lord.”!6 In 1652, a group of eighteen Puritan
ministers and theologians, including both Presbyterians and In-
dependents, affirmed that “the Scripture speaks of a double conver-
ston of the Gentiles, the first before the conversion of the jews, they
being Branches wild by nature grafted into the True Olive Tree instead
of the natural Branches which are broken off. . . . The second, after
the conversion of the Jews.”!7 In the American colonies, the Savoy

13. All quotations from DeJong, As the Waters Cover the Sea, pp. 27-28.

14. Quoted in Murray, Puritan Hope, p. 100.

45. Peter Toon, God's Statesman: The Life and Work of John Owen (Grand Rapids,
MI: Zondervan, 1971), p. 152.

16. Quoted in DeJong, As the Waters Cover the Sea, pp. 37-38.

17, Quoted in Murray, Puritan Hope, p. 72. Interestingly, some of this same
language — the phrase “double conversion of the Gentiles” in particular—was used
by Johann Heinrich Alsted, whose premillennial The Beloved City or, The Saints
Reign on Earth a Thousand Yeares (1627; English edition 1643) exercised great influ-
ence in England. See De Jong, As the Waters Cover the Sea, p. 12.
