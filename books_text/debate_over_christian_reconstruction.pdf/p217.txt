Questions and Answers i99

prays to His Father, not to take us out of the world, but to keep us
from the evil one (John 17:15). Our separation from the world is
not geographical, but ethical. By staying in the world we act as
salt and light to a decaying and lost humanity. The world has
already been taken over by Christ. We do not take over anything.
Because we, through adoption, are children of God, “fellow-heirs
with Christ” (Romans 8:17). We inherit what He already pos-
sesses. As the meek of God, we “inherit the earth” (Matthew 5:5).

In chapter one, I emphasized that the whole Bible is the Chris-
tian’s standard. The Apostle Paul makes this very clear in 2 Timo-
thy 3:16-17 where he declares that “all Scripture is God-breathed.”
The man of God who follows Scripture will be “equipped for every
good work” (v. 17). Paul had the Old Testament in mind when he
wrote these words. The Old Testament has laws that touch on
every facet of life (e.g., Exodus 21-23). These laws are the Chris-
tian’s guide along with the principles set forth under the New
Covenant. Yes, there have been some changes, and modifications
have to be made so these laws can be adapted and applied under
the New Covenant; but they are there for “our instruction.”

Question 4; How would both sides (reconstructionists and dis-
pensationalists) apply Old Covenant or Old Testament teaching
to the New Covenant or New Testament teaching?

Numerous books would need to be written before this question
could be answered to anyone’s satisfaction. There are a number of
books that begin to address this very complicated issue.

1. R. J. Rushdoony, The Institutes of Biblical Law (Phillipsburg,
NJ: Presbyterian and Reformed, 1973).

2. Greg L. Bahnsen, Theonomy in Christian Ethics (rev. ed.; Phil-
lipsburg, NJ: Presbyterian and Reformed, [1977] 1984).

3. Greg L, Bahnsen, By This Standard: The Authority of God's Law
Today (Tyler, TX: Institute for Christian Economics, 1985).

4. James B. Jordan, The Law of the Covenant: An Exposition of Ex-
odus 21-23 (Tyler, TX: Institute for Christian Economics, 1984).
