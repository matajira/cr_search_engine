The Debate over Christian Reconstruction 83

American Presidents have had a high regard for the Bible because
they knew that its wisdom was greater than what any man could
offer.

John Quincy Adams: The first and almost the only Book de-
serving of universal attention is the Bible.

Abraham Lincoln: All the good from the Saviour of the
world is communicated through this Book; but for the Book we
could not know right from wrong. Alt the things desirable to man
are contained in it.

Andrew Jackson: Go to the Scriptures . . . the joyful prom-
ises it contains will be a balsam to all your troubles.

Calvin Coolidge: The foundations our society and our gov-
ernment rest so much on the teachings of the Bible that it would
be difficult to support them if faith in these teachings would cease
to be practically universal in our country.

Woodrow Wilson: The Bible . . . is the one supreme source
of revelation of the meaning of life, the nature of God and spiri-
tual nature and need of men. It is the only guide of life which
really leads the spirit in the way of peace and salvation,

America was born a Christian nation, America was born to
exemplify that devotion to the elements of righteousness which
are derived from the revelations of Holy Scripture.

Harry Truman: The fundamental basis of this nation’s law
‘was given to Moses on the Mount [Sinai]. The fundamental basis
of our Bill of Rights comes from the teachings we get from Ex-
odus and St. Matthew, from Isaiah and St. Paul. I don’t think we
emphasize that enough these days. If we don’t have the proper
fundamental moral background, we will finally wind up with a
totalitarian government which does not believe in rights for any-
bedy but the state.

Those who deny that the world can be changed have ignored
centuries of history. These skeptics are preoccupied with their
own generation as if it is normative for all of history. This is a
