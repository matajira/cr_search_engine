24 The Debate over Christian Reconstruction

Christian peoples and nations will, in general, be more economic:
ally successful than non-Christian peoples and nations.

Of course, God may raise up non-Christian nations, such as
the Islamic nations in the Middle Ages and today, to act as a
scourge against His church. In the short run, moreover, Christian
cultures may not enjoy earthly blessing. But in the long run and
on a general cultural scale, Christians will receive God’s blessing.

Even on an individual level, faithful obedience to the com-
mands of Christ tends to lead to relative earthly prosperity. The
Bible tells us to avoid debt; the Bible teaches us not to live beyond
our financial means. As Christian financial advisor Ron Blue
says, if you spend less than you earn, and do it for a long period of
time, you will be financially successful. At least, a faithful Chris-
tian will be more successful than someone with a similar income
who has no self-restraint and borrows heavily.

Second, the fact that God promises earthly rewards does not
mean that we are motivated solely or even primarily by our hope
for material rewards. God made us in such a way that the expec-
tation of reward motivates us to action. There is nothing necessar-
ily sinful about that; this seems to be the situation cven before the
fall: Adam was promised the reward of life if he would be faithful,
and threatened with death if he was unfaithful. God has gra-
ciously promised to make us sons and “fellow-heirs with Christ”
(Romans 8:16-17).

But this is not our most important motivation. Above all, we
should seek God’s glory and good pleasure. A Christian should be
satisfied with pleasing God, with being a doorkeeper in the house
of the Lord, a mere servant in our Father’s house, with hearing
the Master say, “Well done, thou good and faithful servant.” If
need be, we should be ready to give up our lives, and all our re-
sources for the good of His Kingdom. This is what it means to
“seek first the Kingdom of God” (Matthew 6:33).

So, while our primary motivation for being faithful to God is
to please Him, God has given many promises that provide a sec-
ondary motivation for obedience and faith.

Finally, this does not mean that the Christian life is not a life
under the cross. Nor does it mean that Christians who are suffer-
