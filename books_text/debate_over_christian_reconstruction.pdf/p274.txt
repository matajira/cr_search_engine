Appendix C

TO THOSE WHO WONDER IF
RECONSTRUCTIONISM IS ANTI-SEMITIC
by Steve M. Schlissel

Greetings in our Messiah. I must say that when I was told that
reconstructionists are being accused of being anti-semitic, I was
somewhat taken aback. Why would anyone, aware of the hopes,
let alone the principles, that guide and motivate reconstructionists
regard them as anti-semitic?

Perhaps it is because they have encountered certain recon-
structionists who are, in fact, anti-semitic. Indeed, there are some
who have written things about the Jewish people, especially their
history, which ought to be regarded as stupid (at least), but even
then not necessarily anti-Jewish. In any event, it would be wrong
to extrapolate from the one to the many. That would be, of
course, an example of prejudice and bigotry of which, Iam sure,
most dispensationalists would not like to be guilty.

After all, having at one time been a missionary with one of the
largest and oldest dispensational Jewish mission organizations in
the world, I have met more than one dispensational anti-semite.
But [ do not, therefore, conclude that all dispensationalists and
their system are anti-semitic, That is clearly not the case. It is the
custom of Christian gentlemen to judge by the best of a class, not
the worst; to focus on principles in controversy, not persons.

But that we may present a more positive case in the hope that
we may put to death the notion that reconstructionism is anti-
semitic, consider me, if you please, I was born and raised as a Jew
in a city of 2,000,000 Jews. I was circumcised the eighth day, at-

256
