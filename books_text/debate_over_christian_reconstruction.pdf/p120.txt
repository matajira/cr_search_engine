102 The Debate over Christian Reconstruction

Dispensationalism had its beginnings around 1830.4! This in
itself does not mean that dispensationalism is an unorthodox theo-
logical position. But Tommy Ice used the historical argument to
support premillennialism over against postmillennialism. His ar-
gument may be summarized this way: Premillennialism has been
around since the second century, and postmillennialism had its
origins as a system in the sixteenth century, therefore premillen-
nialism is the orthodox view. But this assumes too much. As we
have already noted, there is little if any support in the early church
for modern dispensational premillennialism. Even the type of pre-
millennialism that operated in the early church fell out of favor
with many after a time. It gained support only from fringe sects.

In the course of time millenarian belief (or ‘chiliasm’ to give it
its Greek name) was repudiated and relegated to sectarian versions
of Christianity. [I]t is only fair to point out that in the second cen-
tury, it did provide a kind of this-worldly hope for Christians in
addition to their ultimate hope of heaven. . . . [T]he experience
of persecution and the hostility of the all-powerful Roman Em-
pire, left little room for hope for the historical future of human
society... . As with late Jewish apocalyptic, Christian millen-
nialism flourished in times of persecution, when there seemed no
hope for human society without drastic divine intervention.4?

Millennialism flourishes in times of trouble, persecution, and a
general decline in the health of the church, It becomes an escapist
eschatology as society seems to be disintegrating.

41, For a discussion of the origins of dispensationalism see the following:
Murray, Puritan Hope, pp. 185-206; Timothy P. Weber, Living in the Shadow of the
Second Coming: American Premillenniatiom, 1875-1982 (en. ed.; Grand Rapids, MT:
Zondervan/Academie Books, 1983); Dave MacPherson, The Incredible Cover-Up
(Medford, OR: Omega Publications, [1975] 1980); Charles Caldwell Ryrie, Dis-
pensationalism Today (Chicago, [L: Moody Press, 1965); William E. Cox, An Ex-
amination of Dispensationatism (Nutley, NJ: Presbyterian and Reformed, 1963);
Joseph M. Canfield, The Incredible Scofield and His Book (Asheville, NC: n.p.,
1984); John Zens, Dispensationalism: A Reformed Inquiry into its Leading Figures and
Features, reprinted from Baptist Reformation Review (Autumn 1973), Vol. 2, No. 3;
Clarence B. Bass, Backgrounds to Dispensationalism: Its Historical Genesis and Ecclesi-
astical Implications (Grand Rapids, MI: Baker, {1960] 1977).

42. Brian Hebblethwaite, The Christian Hope (Grand Rapids, MI: Eerdmans,
[1984] 1985), pp. 47-48.
