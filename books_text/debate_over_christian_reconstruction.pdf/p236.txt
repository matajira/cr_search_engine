218 The Debate over Christian Reconstruction

it would happen. Ladd writes it is “John’s custom to proclaim re-
demptive events which he nowhere actually describes. . . . In the
present pericope John heralds the marriage supper of the Lamb,
but he does not actually describe the event; he merely announces
it.”6 Chilton says that the marriage supper of the Lamb has com-
menced (cf. 2 Corinthians 11:2-3; Jude 3, 24), and he offers an in-
terpretation, something which few commentators even attempt.”

But Chilton’s interpretation of this passage is not the result of
either a postmillennial eschatology or a Reconstructionist theol-
ogy. There are probably a number of postmillennialists (both
Reconstructionist and non-Reconstructionist) who would dis-
agree with Chilton’s interpretation. Therefore, Dave Hunt’s com-
ments are irrelevant to the debate since Christian Reconstruction
is not dependent upon Chilton’s interpretation.

The Thousand Years

Hunt continues to engage in extraneous and irrelevant argu-
mentation when he tells us that “we are asked to take comfort by
the fact that Jesus may come in another 100,000 years.” Who says
this? He doesn’t tell us. Hunt concocts this misrepresentation
from a series of arguments that David Chilton uses to prove that
the length of the thousand year period in Revelation 20 is a very
long period of time, Chilton writes: “My point is this: the term
thousand is often used symbolically in Scripture, to express
vastness; but that vastness is, in reality, much more than the literal
thousand.”® The following quotation will show that Chilton does
not specify the length of the symbolic “thousand years,” whether
it’s 100,000 or 360,000 years:

We should see that the “1,000 years” of Revelation 20 repre-
sent a vast, undefined period of time. It has already lasted almost
2,000 years, and will probably go on for many more. “Exactly

6. Ladd, Commentary on the Revelation of John, pp. 245-46.

7, Chilton’s “Eucharistic” interpretation is not unusual. See Geoffrey Wain-
wright, Eucharist and Eschatology (New York; Oxford University Press, 1981).

8. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
TX: Dominion Press, [1985] 1987), p. 221
