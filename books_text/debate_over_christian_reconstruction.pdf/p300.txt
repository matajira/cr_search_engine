282 The Debate over Christan Reconstruction

Tarbyites, 172
Dartmouth University, 38
Davis v, Beason (1890), 42n
Dispensationalism, 1, 8, 70-71
Biblical Law, 148-53
Distincrives, 82, 107n, 108
Divisions, 9-10
Dorninion, 164-65
Historic Premillennialismn, 107-8
History, 101-6, 179-85
Israel, 246-47, 256-61
Law and Grace, 206-10
Literal Hermeneutic, 82n, 16,
136-37, 175, 179, 182-83
Millennium, 76-81, 113
Orthodoxy, 6
Progress, 188-95, 202-3
Rapture, 183-84
Sacrificial System, 72-73
Social Action, 195-99, 203
Social Ethics, 185-87
Dominion, 74, 160-68
Givil Government, 161-63
Great Commission, 165, 167-68

 

Early Church, 13, 86, 118
Millennium, 180-81
Persecution, 223-24

Education, 37-39, 30

Erastianismn, 3n

chatology, 75-86

Social Action, 195-99, 203

Sovereignty of God, 84

Evangelism, 64, 168

 

Fig ‘Tree, 143, 171

General Revelation, 66 (see also
Natural Law}
Politics, 27-29, 65, 67

Geneva Ministries, 237

Gentiles, 141-42, 191

Gettysburg Address, 40, 1610

 

Great Tribulation, 130-34

Harvard University, 37-39

High Priest, 237-45

Holland, 190

Holy Spirit, 13, 45, 139, 204, 208-9
Homosexuality, 149

Iran, 30, 160

Islam, 24

Israel
Apostasy, 223-24, 237-43
Babylonian Captivity, 52, 201-2
Conquest of Land, 50
Prophecy, 183, 244-55
Rebellion, 49-50
Rebirth as Nation, 171, 258-59
Symbols, 144

Japan, 187-90

Jehovah's Witnesses, 80, 109n, 176

Jerusalem, Destruction, 79, 83, 98,
UL-45, 215-16, 223-27, 237-43

Jesuits, 105-6

fournal of Christian Reconstruction, 203

Jubilee, 142

Justification, 209-10

Kingdom of God, 65, 109-162
Millennium, 73
Present Reality, 213-15
Progress, 52-55, 70, 74, 187-95
Timing, 101

Kingdom Theology, 4

Last Days, 223

Late Corporation of the Church of Latter
Day Sainis v. United States 1890), 42n

Late Great Planet Earth, 171, 226

Latter Rain Movement, 176-78

Law and Grace, 205-10

Leaven, 192

Liberalism, 108-10
