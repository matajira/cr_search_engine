Tommy Ice: A Response — Part I 97

fested during the “church age.” Boyd continues by challenging his
fellow-dispensationalists “to be more familiar with, and com-
petent in, patristics,2 so as to avoid having to rely on second-
hand evidence in patristic interpretation.” He suggests that “it
would seem wise for the modern system [of dispensational premil-
lennialism] to abandon the claim that it is the historic faith of the
church (for at least the period considered). . . .”2

Another graduate of Dallas Theological Seminary comes to a
similar conclusion relating to a pretribulation rapture, a major
pillar of dispensationalism: “An intensive examination of the writ-
ings of pretribulational scholars reveals only one passage from the
early fathers which is put forth as a possible example of explicit pre-
tribulationalism.”%°

A “Hint” of Evidence

Where, then, is the historical evidence for premillennialism?
What was once considered insurmountable evidence, has now
turned out to be scant evidence. This conclusion is made even by
scholars from within the dispensational camp.

Tommy Ice says that “there’s absolutely no one in the early church
that even gives a hint that they believe that things were fulfilled in
70 a.p.” But according to Justin, there were people who did hold a
non-premillennial position. This is at least a “hint” of something
else, perhaps even the possibility of an a.p, 70 fulfillment.

28. Relating to the church fathers and/or their writings.

29. Ibid., p. 92. In a footnote on this same page, Boyd questions the historical
accuracy of the research done on the patristic fathers by George N. H. Peters in
his much cansulted three-volume work, The Theacratic Kingdom (Grand Rapids,
MI; Kregel, [1884] 1988). Boyd sides with the evaluation of the amillennialist
Louis Berkhof when he writes that “it is not correct to say, as Premillenarians do,
that it (millennialism) was generally accepted in the first three centuries. The truth
of the matter is that the adherents of this doctrine were a rather limited number.”
Berkhof, The History of Christian Doctrines (London: The Banner of Truth Trust,
[1937] 1969), p. 262. Boyd goes on to disagree with the conclusion of the dispen-
sational author John F. Walvoord that “The early church was far from settled on
details of eschatology though definitely premillennial.” Walvoord, The Rapture Ques-
tion (Grand Rapids, MI: Zondervan, 1957), p. 137.

30. William Everett Bell, A Critical Evaluation of the Pretribulation Rapture Doctrine
in Christian Eschatology (School of Education of New York University, unpublished
doctoral dissertation, 1967), p. 27. Emphasis added.
