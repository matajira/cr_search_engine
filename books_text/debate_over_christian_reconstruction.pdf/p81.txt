Understanding Christian Reconstruction 63

The attacks against Christian Reconstruction center on two
emphasized doctrines — biblical law and postmillennialism— while
ignoring the comprehensive biblical system that includes all the
doctrines of the faith. Such a procedure would make any theologi-
cal system look out of focus. Reconstructionists do not cast aside
prayer, evangelism, and worship. Instead, we emphasize long-
neglected doctrines like the application of biblical law in the New
Covenant order and the advance of God’s kingdom in the world.
Reconstructionists subscribe to the basics of the Christian faith:
from the inerrancy of Scripture to a literal heaven and hell and
everything in between.

While there are many “pillars of Christian Reconstruction,” I
have chosen three of the most prominent ones. There is an in-
timate and necessary relationship between regeneration, biblical
law, and postmillennialism, There are other Reconstructionist
distinctives. For a more complete analysis of Christian Recon-
struction and its historical and theological setting, I encourage
you to read The Reduction of Christianity: A Biblical Response to Dave
Hunt.

Regeneration

Regeneration is the starting point for Reconstructionists.
Society cannot change unless people change, and the only way
people can change is through the regenerating work of the Holy
Spirit. Those “dead in trespasses and sins” (Ephesians 2:1) must
have a “new heart” and a “new spirit.” The “heart of stone” must be
removed and a “heart of flesh” substituted. This is God’s work.
God's Spirit must be in us defore we can walk in His statutes (Romans
8:3-4, 7), The result will be that we “will be careful to observe”
His “ordinances” (Ezekiel 36:26-27). The New Testament sum-
marizes it this way: “If any man is in Christ, he is a new creature,
the old things passed away; behold, new things have come” (2 Corin-
thians 5:17).

There is no way to change our world unless people are given
the will to change (1 Corinthians 2:14). The instrument of that
change is the preaching of the gospel, not political involvement,
