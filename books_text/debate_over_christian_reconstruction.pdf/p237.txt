Concluding Remarks by Dave Hunt and Tommy Ice 219

how many years?” someone asked me. “I'll be happy to tell you,” I
cheerfully replied, “as soon as you tell me exactly how many hills
are in Psalm 50."

No one knows when Jesus is coming back. As Garry Friesen,
a graduate of Dallas Theological Seminary writes, most premil-
lennial teachers hold the opinion that “Christ may return tomor-
row or a thousand years from now.”!° Of course, we could say
with equal validity that Jesus may return tomorrow or two (three,
five, ten) thousand years from now. This view is not unique to the
Reconstructionists. Chilton, in a footnote, comments: “Consider
the fact that the compilers of The Book of Common Prayer provided
‘Tables for Finding Holy Days’ all the way to a.p. 8400! Clearly,
they were digging in for the ‘long haul,’ and did not expect an im-
minent ‘rapture’ of the Church.” How many premillennialists of
the second century would have dreamed that the return of Christ
would be 2000 years off? One older Bible commentary (not Re-
constructionist) proposes that if a day is taken for a year,'® the
thousand years

represent 360,000 years, a period which some deem so long as to

appear inadmissable. . . . Then, let it be recollected, that Jeho-

 

9. Ibid., p. 199.

10. Garry Friesen, ‘A Return Visit,” Moody (May 1988), 1988, p. 31.

11. Chilton, Days of Vengeance, p. 497, note 6.

12. Dispensationalisis such as Dave Hunt and Tommy Ice should uot be sur-
prised at such an interpretation since the hermencutical methodology that one
uses to get this large number is inherent in dispensationalism. For example, a
non-existent gap that dispensationalism places between the 69th and 70th week
of Daniel 9:24-27 is nearly 2000 years long. “Shortly” is not that the event may
occur soon, but when it does, it will be sudden, So then, if I tell you I'll be there
shortly, don’t hold your breath. Then there are the many “double fulfillments.”
‘Joel's prophecy, for example, wasn’t really fulfilled on the day of Pentecost. There
must be a future fulfillment. Peter didn’t say it all when he declared that “this is
what was spoken of through the prophet Joel” (Acts 2:16). The “this generation”
of Matthew 24:34 does not mean the generation to whom Jesus spoke, but a gen-
eration nearly 2000 years in the future, Of course, there is the infamous “gap” of
innumerable years that CG. L. Scofield placed between the first two verses of Gen-
esis 1: “The first creative act refers to the daleless past, and gives scope for all the
geologic ages.”
