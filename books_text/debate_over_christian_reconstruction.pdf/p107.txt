Tommy Ice: A Response ~ Part I 89

of Reconstructionist distinctives: Biblical law, the sovereignty of
God in salvation, presuppositional apologetics, the dominion
mandate, covenant theology, and postmillennialism.! Ice and
Hunt decided to debate a single pillar of Christian Reconstruc-
tion: Postmillennialism.'! Since many people listening to the
debate will get the impression that postmillennialism is éhe issue,
T've decided to answer Tommy Ice and Dave Hunt point by point
on the issue of postmillennialism.

This chapter will examine the eschatological view presented
by Tommy Ice, specifically his contentions that postmillennialism
cannot be supported by Scripture and that there is no hint of a
preterist!? view of prophecy among the early church fathers. More-
over, I shall explicate several topics related to eschatology, the
foremost being the identification of historical premillennialism
with modern-day dispenstionalism. In Part If of my response to
Tommy Ice, I shall address the prophetic time frame of Matthew
24:1-34, the identification of the abomination of desolation, the
man of sin, and a number of related eschatological issues.

10, For a definition of a number of these terms, see DeMar and Leithart,
Reduction of Christianity, pp. 19-43; and Robert M. Bowman, Jr., “The New Puri-
tanism: A Preliminary Assessment of Reconstructionism,” Christian Research Jour-
nal (Winter/Spring 1988), pp. 23-27.

11. Dave Hunt and Tommy Ice knew the parameters that defined Christian
Reconstruction. The Reduction of Christianity spelled them out, This is why it was
inexcusable to weight the debate so heavily on the side of eschatology,

12. The preterist view of prophecy can be defined in at least two ways: First, an
interpretation of prophecy, and particularly the book of Revelation, which holds
that the events depicted had already taken place. This definition of preterist would
deny predictive prophecy. Postmillennialists are not preterists in this sense. Second,
an interpretation of prophecy, and particularly the book of Revelation, that
describes the present and near-future struggles of the Christian church and the
victory of Ghrist over His enemies. “This approach has its strength in the fact
that when the Revelation is thus understood, it becomes immediately and thor-
oughly relevant co the life and struggle of the early church.” Everett F. Harrison,
Introduction to the New Testament (Grand Rapids, MI: Eerdmans, 1982), p. 463
The second view of pretertst, held by many postmillennialists, maintains the pre-
dictive element in prophecy, The use of preterist in this book simply means an
A.D, 70 fulfillment of much of New Testament prophecy.
