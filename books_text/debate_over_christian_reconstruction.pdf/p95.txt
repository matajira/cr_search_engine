Putting Eschatology into Perspective 77

doctrines of verbal inspiration, the deity of Christ, substitutionary
atonement, and bodily resurrection.”3

Of course, all biblical doctrines are important. In fact, as I've
already noted, all biblical doctrines can be studied in the light of
eschatology, but can we say that a particular view of the millennium
is comparable to the study of verbal inspiration, the deity of Christ,
the substitutionary atonement, and the bodily resurrection —espe-
cially since there are so many divergent views among solidly or-
thodox Christians? While we will study this more fully in the next
chapter, it’s curious that none of the church’s earliest creeds made
the millennial issue a test of orthodoxy. Louis Berkhof writes that
up to the present time “the doctrine of the millennium has never
yet been embodied in a single confession, and therefore cannot be
regarded as a dogma of the Church.” A number of dispensational
Bible schools and seminaries, however, have made adherence to a
particular millennial position a requirement of graduation.

A Word of Caution

Revelation 20 is the only chapter in the Bible that speaks of a
thousand year reign of Christ or “millennium.” This thousand
year reign is mentioned in this single chapter of this one book of
Scripture, and its meaning is not crystal clear.’ Moreover, the

3. John F, Walvoord, The Millennial Kingdom (Findlay, OH: Dunham Publish-
ing Go., 1959), p. 16.

4. Louis Berkhof, The History of Christian Doctrines (London: The Banner of
Truth Trust, [1937] 1969), p. 264.

5. C. H. Spurgeon, in his very worthwhile and informative book Commenting
and Commentaries, writes the following introductory section on Revelation:

‘The works upon REVELATION are so extremely numerous (Dar-
ling’s list contains 52 columns), and the views entertained are so many,
so different, and so speculative, that after completing our List we resolved
not to occupy our space with it, but merely to mention a few works of
repute, (London: The Banner of Truth Trust, [1876] 1969), p. 198

This was written over one hundred years ago. Consider how large that list would
be today. Spurgeon goes on to enumerate “a fourfold manner of apprehending
the Apocalyptic Prophecy”: Preterists, Continuists, Sirnple Futurists, and Ex-
treme Futurists. No single position is designated orthodox or heretical.
