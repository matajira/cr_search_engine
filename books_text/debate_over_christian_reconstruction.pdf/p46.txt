28 The Debate over Christian Reconstruction

all these statutes and say, ‘Surely this great nation is a wise and
understanding people.’ For what great nation is there that has a
god so near to it as is the Lorp our God whenever we call on
Him? Or what great nation is there that has statutes and judg-
ments as righteous as this whole law which I am setting before you
today?” (Deuteronomy 4:6-8).

Some Christians would tell us to put the Bible away! Why?
The writer quoted above, who maintains that “Government is not
based on special revelation,” rejects the Bible as a standard for
civil legislation because “the unregenerate cannot live out the de-
mands of God’s law.” I can just hear my children telling me that
my standards for their behavior should not be obeyed because
they are unable to obey them. A criminal could use this defense
when accused of a crime listed in the Bible. “Pm not a Christian,
so I am unable to live out the demands of God's law.” The purpose
of civil government, as a minister of justice, is to punish external
acts of disobedience that the Bible describes as criminal. If any of
us could keep the Jaw perfectly, then there would be no need for
the civil magistrate. Moreover, punishment is designed to restrain
evil in all people. The law and its attendant punishment are de-
signed to protect law-abiding citizens from anarchy.

T’m sure that this same author believes that murder is wrong
and that common sense, the laws of nature, natural rights, natu-
ral conscience, reason, principles of reason, and the light of rea-
son would also lead a judge to conclude that murder, for example,
is wrong. Adherents to a natural law ethic want to maintain that if
it’s in the Bible then we cannot use it.

There are many non-Christians who never murder, commit
adultery, or practice sodomy. Much of their restraint comes from
the fear of punishment. Let us keep in mind that the civil magis-
trate can only punish pudlic acts of disobedience. It does not deal
with sins of the heart, nor does it compel the unregenerate to be-
come Christians. If the above writer wants to say that the unre-
generate cannot keep the law in its demands on the heart, then I
will agree. But the civil magistrate does not have jurisdiction over
the heart.
