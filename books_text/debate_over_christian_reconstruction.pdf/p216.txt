198 The Debate over Christian Reconstruction

tivation, once again there for present operation, is that the master
may return at any moment. And, therefore, because he may
return at any moment he is supposed to be found doing what God
has willed.” Again, the debate is not over motivation. Rather, it’s
over what ethical standard the Christian should follow in anticipa-
tion of his Master’s return, and should he expect success in his
efforts. He must be “found doing what God has willed.” Well,
what has God willed? How do the slaves know what to do? The
one slave who buried his mina in the ground had the best of
motives. He also had the master’s return in mind (vv. 20-27).1?
Ice continues by stating that he has not seen “the reconstruc-
tionists come up with Scripture from the New Testament or even
[to] show where the Old Testament would apply in the New Tes-
tament in the way that they teach.” I’m not sure what he means by
this. He seems to be saying that Reconstructionists must prove
that the Christian's task is more than to preach the gospel. His fol-
lowing comments seem to indicate that this is what he means:

The New Testament teaches that we’re supposed to preach
the Gospel. The current purpose of this age is for God to call out
from among the nations a people for His name. The very word
church, ecclesia in the Greek, means “called out ones.” [The Bible]
nowhere says we're to take over the world for Christ. Surely,
wherever Christianity goes through history, it produces a much
superior culture, but that is not the purpose, according to Scrip-
ture, during this particular age. When Christ returns and sets up
His kingdom, He will cataclysmically bring in a culture and a so-
ciety that will conform to the will of God.

Yes, our job is to preach the gospel to those who are in bond-
age to sin, But Jesus nowhere tells us to leave the world. Jesus

17. Pm not sure that Jesus is teaching His Second Coming in this parable. The
nobleman returns to the same people to whom He gave the minas. His return
seems to occur within a generation, not two thousand years later, in which case
the descendants would be required to give an accounting of their ancestors’ in-
vestments. The citizens are said to hate the nobleman, saying, “We do not want
this man to reign over us” (Luke 19:14). This is reminiscent of John 18:15, when
the Jewish leaders cried out, “We have no king but Caesar.”
