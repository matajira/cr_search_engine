Questions and Answers 197

enjoy would not exist. Our Christian forefathers understood the
concept of dominion and Christian Reconstruction, They worked
hard to ensure that the State would not interfere in the affairs of
religion. The United States of America, because of its early Puri-
tan influence, created a decentralized constitutional republic that
became the standard of freedom around the world. Christians
were not silent in this enterprise.

Dave Hunt uses an incident in Jesus’ preaching ministry to try
to prove his point that the gospel takes precedence over all other
activities (Matthew 15:32-39), Again, Reconstructionists agree that
leading people to Christ is the church’s primary mission. But the
people who came to hear Jesus were not the poor and the disen-
franchised. This is hardly an example of “gospel-first” preaching.
The people did not come to Jesus for foad. They did not expect to
stay as long as they did. They had been without food for “three
days” (v. 32). But Jesus did eventually feed them. Those who criticize
Christian Reconstructionists because we insist that there is duty
beyond proclaiming the gospel should heed the words of James:

If a brother or sister is without clothing and in need of daily
food, and one of you says ta them, “Go in peace, be warmed and
be filled,” and yet you do not give them what is necessary for their
body, what use is that? Even so faith, if it has no works, is dead,
being by itself. But someone may well say, “You have faith, and I
have works; show me your faith without the works, and I will
show you my faith by my works” (James 2:15-18).

So then, reconstruction and dominion follow necessarily from
gospel proclamation and conversion. They cannot be separated.
Preaching the gospel must be emphasized along with giving the
cup of cold water in Jesus’ name. The world is drawn to fruits,
whether good or bad. The bad fruit repels, while good fruit attracts.

Tommy Ice, with his comments, points again to the motiva-
tion that Christ’s return stirs up the Christian for ethical duty
(Luke 19:11-27), “The picture is of the good steward who is put in
charge of the kingdom because, interestingly enough, the master
has gone away to a far country to receive a kingdom, and the mo-
