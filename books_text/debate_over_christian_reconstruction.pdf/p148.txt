130 The Debate over Christian Reconstruction

God then deserted his Temple, because it was only founded for a
time, and was but a shadow, until the Jews so completely violated
the whole covenant that no sanctity remained in either the Tem-
ple, the nation, or the land itself.”*!

As in all fulfilled prophecy, there are still future applications.
Jesus died for our sins two centuries ago, but His finished work is
just as applicable to the lost today as it was to those who witnessed
the events. “The destruction of the Temple and of its Jerusalem-
culture, as portrayed in the remainder of revelation, was thus a
warning to the Seven Churches: If you do the same, God will do
this to you. Thus, the principles are still in force, and serve to
warn us: If our churches depart from Christ, he will destroy both
them and our society, which grew up around them.”22

4, “There will be a great tribulation, such as has not oc-
curred since the beginning of the world until now, nor ever
shall” (v. 21).

This verse follows the verses that: present a localized judg-
ment. The people are from Judea. They are still living in houses
with flat roofs. It’s mainly an agricultural economy. The Sabbath
is still in force. The tribulation has reference to the Jews, the peo-
ple of Judea (Luke 21:20-24); it is not a world-wide tribulation.
The evaluation of the tribulation, however, is universal. In com-
parison to all the tribulations the world has experienced or ever
will experience, this one is the most dreadful and horrifying.

Why? Matthew 24:21 must be seen in the light of a number of
verses that show why the language used by Jesus is so incomparable:

The blood of all the prophets, shed since the foundation of the world,
may be charged against this generation, from the blood of Abel to the
blood of Zechariah, who perished between the altar and the
house of God; yes, I tell you, it shall be charged against this generation
(Luke 11:50-52).

21, John Calvin, Commentaries on the Book of the Prophet Daniel, 2. vols. (Grand
Rapids, ME: Baker Book House, 1979), vol. 2, p. 390.
22. Jordan, ‘Abomination of Desolation.” Appendix A, p. 243.
