52 The Debate over Christian Reconstruction

show himself a good and faithful steward of his God-given gifts.
God requires an accounting,

The kingdom of God has purpose because God directs its
every movement. History is not bound by a never-ending series of
cycles, with God powerless to intervene and govern. The future,
as Nebuchadnezzar came to realize, is governed by God. Earthly
sovereigns who fail to recognize God’s absolute sovereignty will be
destroyed: “You [Nebuchadnezzar] continued looking [at the
statue] until a stone was cut out without hands, and it struck the
statue on its feet of iron and clay, and crushed them. Then the
iron, the clay, the bronze, the silver and the gold were crushed all
at the same time, and became like chaff from the summer thresh-
ing floors; and the wind carried them away so that not a trace of
them was found. But the stone that struck the statue became a
great mountain and filled the whole earth” (Daniel 2:34-35). The
pagan idea of the future is a myth. The future belongs to God’s
people and Christians are not trapped in futile historical cycles.

The Christian’s view of the future determines how he lives,
plans, and works in the present for the future. Even during Israel’s
captivity under Babylonian rule, the nation’s darkest hour, the
people were told to plan and build for the future: “Build houses
and live in them; and plant gardens, and eat their produce. Take
wives and become the fathers of sons and daughters, and take wives
for your sons and give your daughters to husbands, that they may
bear sons and daughters; and multiply there and do not decrease.
. .. For I know the plans that I have for you,’ declares the Lorn,
‘plans for welfare and not for calamity to give you a future and a
hope’” (Jeremiah 29:5-6, 11).

God’s words seemed contrary to what people saw all around
them. Destruction and captivity awaited the nation, yet God com-
manded them to prepare for the future. In spite of every pessimistic
view, God wanted the people’s desires and hopes to be future-
directed. Build for what will be. The psychological benefit of such
amind set does much to spur the church of Jesus Christ to greater
kingdom activity. A preoccupation with defeat brings defeat by
default. Why would anyone wish to build for the future when
