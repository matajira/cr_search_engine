188 The Debate over Christian Reconstruction

law, politics, education and every other good thing created by God.
These blessings are realized through faithfulness to God. If a per-
son is faithful in carrying out God’s commandments, he will pros-
per (Joshua 1:8). Failure to acknowledge that God is the one who
gives these good gifts will mean judgment (Deuteronomy 8), Does
this mean that God’s people will not encounter tribulation? Not at
all. But even in tribulation God's people eventually prosper.

And not only this, but we also exult in our tribulations, know-
ing that tribulation brings about perseverance; and persever-
ance, proven character; and proven character, hope; and hope
does not disappoint, because the love of Ged has been poured out
within our hearts through the Holy Spirit who was given to us
(Romans 5:3-5).

God uses all things, even tribulation, to make us what He wants
us to be, So then, it’s wrong to pit tribulation over against pros-
perity of whatever kind. In terms of the sovereignty of God, we
always prosper. Even in death there is victory. Every attempt to
thwart the advance of God’s kingdom is overthrown by God. Ter-
tullian (c. 160-240) captures the spirit of this truth with these
words; “The more ye mow us down, the more quickly we grow;
the blood of Christians is seed.”

Finally, Dave Hunt seems to say that Christianity as a world-
view really has nothing over Shintoism and Buddhism when it
comes to economic development. He tells us that Japan has done
very well economically without an operating Christian worldview.
This is not the place to rehearse Japan’s post-World War II prog-
ress, but nearly all historians agree that Japan’s development is
based on the infusion of western capital, education, and technol-
ogy, which, in turn, is largely based on the western world’s his-
toric commitment to Christianity. Thomas Sowell writes:

Mciji Japan introduced the study of English in its secondary
schools in 1876, permitted the establishment of Christian
churches and schools, and its leaders and intellectuals publicly
expressed strong admiration for the United States and the Amer-
