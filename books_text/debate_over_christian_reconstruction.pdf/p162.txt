144 The Debate over Christian Reconstruction

Jesus used a similar analogy in Matthew 16:1-4, The Pharisees
and Sadducees were asking Jesus to produce a sign from heaven
to prove who He was. The Pharisees and Sadducees were able to
discern the signs regarding approaching weather, but they could
not discern the signs that Jesus had already produced proving His
Messiahship. Jesus often used natural phenomena to make His
points with the stiff-necked religious leaders of His day.

One final point should be made about the fig tree representing
Israel. The New Testament is very clear that the preferred sym-
bols for Israel are the vine (John 15:1-I1), the olive tree (Romans
11:16-24), the lump of dough (Romans 11:16), and the flock (Isaiah
40:11; Jeremiah 23:2; Matthew 26:31; Luke 12:32; John 10:16;
1 Peter 5:2). In the case of the flock and the olive tree, both Jews
and Gentiles make up these representative groups of God’s peo-
ple. Jesus said that He has “other sheep, which are not of this fold;
I must bring them also, and they shall hear My voice; and they shall be-
come one flock with one shepherd” ( John 10:16). There is one olive tree
with Gentiles grafted in to make one tree, consisting of believing
Jews and Gentiles.

All these events are a prelude to Jesus’ coming in judgment
upon apostate Judaism. This destruction will be the manifestation
of Jesus’ enthronement in heaven, the passing away of the old cov-
enant order, and the inauguration of the age to come. Jesus will
consummate His work in a yet future day:

Then comes the end, when He delivers up the kingdom to the
God and Father, when He has abolished all rule and all authority
and power. For He must reign until He has put all His enemies
under His feet. The last enemy that will be abolished is death.
For He has put all things in subjection under His feet. But when
He says, “All things are put in subjection,” it is evident that He is
excepted who put all things in subjection to Him. And when all
things are subjected to Him, then the Son Himself also will be
subjected to the One who subjected all things to Him, that God
may be all in all (t Corinthians 15:24-28).
