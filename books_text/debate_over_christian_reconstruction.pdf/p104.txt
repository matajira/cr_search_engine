86 The Debate over Christian Reconstruction

way it should be. Christian Reconstructionists say otherwise.
Those opposed to Christ are usurpers who must hear the gospel
message that comes from their Sovereign Lord and King. Is this
deviant? The early church turned the world “upside down’ (Acts
17:6). Should we do any less?
