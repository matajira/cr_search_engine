158 The Debate over Christan Reconstruction

tory. He also maintains that victory is “the martyrs going to their
death, singing of their love for the Lord, and trusting in Him.”™
Is this the only way to be victorious? T don’t think so. If we are in
Christ, we are always victorious, no matter what the circum-
stances. But what if we're not suffering? Are we to conclude that
we're out of God’s will? If we are doing something God calls us to
do, should we expect failure or success? Should the Apostle Paul
have chosen to drown rather than be rescued as he was shipwrecked
off the coast of Malta? (Acts 28:1). Who gave Paul “victory” over
the “viper” that came out of the fire? (vv. 2-6).

To support his claim that suffering is normative for the Chris-
tian, Dave Hunt quotes a few verses out of Hebrews 11; “By faith
Moses, when he had grown up, refused to be called the son of
Pharaoh’s daughter; choosing rather to endure ill-treatment with
the people of God, than to enjoy the passing pleasures of sin; con-
sidering the reproach of Christ greater riches than the treasures of
Egypt” (Hebrews 11:24-26). He then goes on to quote how some of
God's people were victorious. It was not Moses and the Israelites
who were drowned in the Red Sea (v. 29); it was not Rahab and
her family who were destroyed as the walls of Jericho came tumb!-
ing down (vv. 30-31).

Dave Hunt continues by telling us how some of these men, “by
faith conquered kingdoms, performed acts of righteousness, ob-
tained promises, shut the mouth of lions, quenched the power of
fire, escaped the edge of the sword, from weakness were made
strong, became mighty in war, put foreign armies to flight” (vv.
33-34). Isn’t this victory? Why, of course it is!

Reconstructionists do not deny that the people of God are im-
prisoned, stoned, tempted, and put to death with the sword (vv.
36-37). But this too is victory! Christian Reconstructionists do not
teach that a positive confession will dismiss these persecutions.

25, Dominion: A Dangerous New Theology, Tape #1 of Dominion; The Ward and
New World Order, distributed by the Omega-Letter, Ontario, Canada, 1987. Quoted
in DeMar and Leithart, Reduction of Christianity, p. 138. See pages 136-143 for a
full refutation of Dave Hunts views on “victory.”
