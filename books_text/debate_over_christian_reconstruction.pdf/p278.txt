260 The Debate over Christian Reconstruction

state emphatically that he has no sympathy whatever with any
Millenarian (i-e., Premillennial) theory, and that he considers all
such ideas, and especially such as involve the personal reign of our
Savior (from earthly Jerusalem), as merely carnal and Judaizing.”

As early as 1847 the great Dr. David Brown (of Jamieson,
Faussett & Brown fame) wrote of his conviction that the Jews
would one day again possess the Land of Israel. But he labored
carefully to emphasize the point that whatever occupation of the
land they may enjoy outside of Christ, that would not be the fulfill-
ment of the promised restoration. Dr. Brown, in his mature years
wrote a most stimulating, and characteristically irenic book on the
subject. Both dispensationalists and reconstructionists would
profit from reading The Restoration of the Jews. The History, Prin-
ciples, and Bearings of the Question (Edinburgh: Alexander, Strahan
& Co., 1861).

Now, whatever any individual Christian reconstructionist
might say, either from ignorance or honest disagreement, it can
hardly be maintained that reconstructionism itself is anti-semitic.
Calvin’s position (as excerpted above) is mine, and I am a “recon-
structionist.” ] can testify that while not every reconstructionist
would agree with my position, my views on this issue are not only
accepted within the reconstructionist world as being perfectly con-
sistent with the system, but sought out.

This being the case, I think it would be best to bury the charge
of anti-semitism in the sea of disproved contentions, If you should
meet or read a reconstructionist who is, in fact, anti-semitic,
please put him in touch with me. And as for me, if l should meet a
dispensationalist who really believes that the church’s efforts to
reach the Jews with the Gospel will be successful, I'll be sure to
send him to you so that you can convince him of the futility of his
optimism!

It seems to me that this is what has occurred: Some dispensa-
tionalists have accepted the unbelieving Jewish expectations ofthe
Messianic Kingdom as correct. They have, thereby, taken sides
with Rabbinical Judaism against Christ’s “Judaism,” or Kingdom.
They then cite the existence of the State of Israel as proof of their
