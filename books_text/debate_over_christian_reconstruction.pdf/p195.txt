Dave Hunt: A Response 177

Spiritualistic trends were evident in the doctrine of the “mani-
festation of the Sons of God.” The doctrine originated from the
predictive prophecyings [sic] of Rev. E. Crane of the Northwest
Bible Institute in Edmonton [Alberta, Canada]. He believed that
he had seen a vision of the victorious saints as a mighty
army. .. . His entire class was affected by his belief and soon
there grew the doctrine that the “elect” would receive “redemptive
bodies” here and now and that any person who died had not been
able to “appropriate the redemption body” and was therefore not
one of the “overcomers.”57

The doctrines of the Manifest Sons of God are a mixed bag. You
can find some very orthodox statements in their theology and some
heretical ones as well. In my research on them, however, I learned
that there is no real systematic theology articulated by the leaders
in the group. In fact, it’s hard to find out who the leaders are.

The most unorthodox doctrines are:

1. Some teach that an elite group in the church, namely them-
selves, have been perfected prior to glorification. These are the
manifested sons of God. “They believe that they have fully attained
the state of spiritual and moral perfection that the redeemed will
possess in heaven. Thus, they see themselves in a class above the
average Christian.” Do Reconstructionists believe this? No!
Does Genitry’s article even suggest this idea? No!

2. “A few even go to the extreme of saying that Christ and the
Church are meant to become one in essence or nature. Thus,
some of them teach that ultimately there will be no distinction be-
tween Christ and His Church.”? Do Reconstructionists believe
this? No! Does Gentry’s article even suggest this idea? No!

57, Cornelius J. Jaenen, “The Pentecostal Movement” (M.A, Thesis, Uni-
versity of Manitoba, 1950), p. 89. Quoted in Richard Michael Riss, “The Latter
Rain Movement of 1948 and the Mid-Twentieth Century Evangelical Awaken-
ing” (B.A. Thesis, University of Rochester, 1974), p. 134.

58. Elliot Miller, The Manifestation of the Sons of God, Christian Research Fact
Sheet, 1979, Christian Research Institute, Box 500, San Juan Capistrano, Cali-
fornia 92693.

59. Idem.
