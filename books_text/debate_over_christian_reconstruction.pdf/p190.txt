172 The Debate over Christian Reconstruction

and still believes that the rapture and Tribulation are near.”* But
this next point is important to Hunt’s belief that “Jesus is going to
return very soon.” Lindsey’s and Hunt’s opinion about the near-
ness of Jesus’ return is “at odds with most premillennial teachers,
who say that Christ may return tomorrow or a thousand years
from now.”® Dave Hunt's theology is defeatism if the church has
another 1000 years to go before Jesus returns, and the church is
still putting its hope in the rapture with no task of dominion or
any hope that the world can be made better in Christ. Christians
can only achieve victory in martyrdom,

Dave Hunt has put his hope in the soon-rapture of the church,
a doctrine that the church never held until it was manufactured by
Darbyites in the nineteenth century. Hunt then implies that any-
one who does not believe in the soon-rapture doctrine subscribes
to a “deviant theology.” Apparently this includes historical premil-
lennialists, since for them, the rapture could be a thousand years
away! The Reconstructionists, on the other hand, put their hope
in the reality of Jesus’ return and not in its nearness.!

But what of those passages that tell us to “watch” for Jesus’
return? Don’t they teach an imminent, any-moment return? They
do not. Rather, they teach us always to be ready to be held ac-
countable because we do not know when Jesus will return.

It should be stressed that none of the exhortations to watch-
fulness are undermined because of the factor of delay. If they did
not threaten or dilute the early church’s resolve to remain watch-
ful, then they will not weaken ours either, Delay does not lessen
our anticipation or expectancy. In spite of pretribulational argu-
ments that the church cannot be genuinely motivated to watch-
fulness unless they believe the Lord’s coming could happen any
moment, reality does not support this premise. The true motiva-
tion for watching is not based on imminency, but on our ultimate

49. Idem.

80. Idem.

51. Herbert Bowsher, “Will Christ Return ‘At Any Moment?,” The Journal of
Christian Reconstruction, Symposium on Evangelism, ed. Gary North, Vol. VII,
No. 2 (Winter, 1981), p. 51. The entire article is worthy of study.
