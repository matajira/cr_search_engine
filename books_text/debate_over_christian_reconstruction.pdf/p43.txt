Heavenly and Earthly Rewards 25

ing sickness, financial difficulties, vocational frustrations, or other
problems are necessarily suffering because they lack faith. That
may be the case in some instances, but it is not the case in every
instance,

Many of the biblical characters who exercised the strongest
faith were also the most sorely pressed with manifold difficulties:
Joseph, Moses, Job, David, Nehemiah, Paul, and Jesus Christ
Himself, io name but a few. The faith praised in Scripture is not
one that is able to avoid suffering, but one that perseveres through
suffering. Jesus is said to have learned obedience through what
He suffered (Hebrews 5:8), and the same pattern applies to us (cf.
Psalm 119:71).

Yet even this truth must also be seen from the ultimate perspec-
tive of victory. This can be understood in several different ways.
First, God delivers us out of affliction—be it sickness, persecu-
tion, or something else—bringing greater glory to Himself.

But obviously not every Christian is delivered out of his or her
affliction. Still, suffering is not defeat, but victory because our afftic-
tions strengthen our faith; our faith overcomes our afflictions. As
Paul said in one of the most rhapsodic passages in the Bible:

Who shall separate us from the love of Christ? Shall tribulation,
or distress, or persecution, or famine, or nakedness, or peril, or
sword? Just as it is written, “For Thy sake we are being put to
death all day long; we were considered as sheep to be slaugh-
tered.” But in all these things we overwhelmingly conquer
through Him who loved us. For I am convinced that neither
death, nor life, nor angels, nor principalities, nor things present,
nor things to come, nor powers, nor height, nor depth, nor any
other creature shall be able to separate us from the love of God,
which is in Christ Jesus (Romans 8:35-39).

In other words, there is a sense in which we are victorious even in
the midst of suffering.

Also, we need to recognize the continuing power of sin in the
life of a believer. We already possess the new life of the Kingdom,
but we still are engaged im an internal war between our flesh and
