6
ONE STANDARD FOR ALL

It’s time that Christians begin to understand what's at stake. A
battle is raging. In many cases, the fire is coming from within the
camp. Millions of Christians say they believe the Bible is the
Word of God, that it’s inerrant and infallible. But when it comes
to using the Bible as a blueprint for living, they begin to take out
their scissors. You've heard the objections:

* The Old Testament doesn’t apply in the church age.
* You can’t put a non-Christian under Biblical law.

© We're under grace, not law.

These objections are myths, or at best, dangerous half-truths.
Just try to understand the New Testament without the Old Testa-
ment. Paul says that pastors are to be paid, and he supports this
from a seemingly obscure verse from the Old Testament: “For the
Scripture says, ‘You shall not muzzle the ox while he is threshing,’
and ‘The laborer is worthy of his wages’” (1 Timothy 5:18; see
Deuteronomy 25:4; Leviticus 19:13).

Read what the Bible says about the alien in Israel. The alien
was required to keep the law just like the covenant-bound Israel-
ite: “There shal] be one standard for you; it shall be for the stranger
as well as the native, for I am the Lorp your God” (Leviticus
24:22; cf. Exodus 12:49). The alien was given “equal protection
under the law.” Aliens could acquire property and accumulate
wealth (Leviticus 25:47), They were protected from wrong-doing
and treated like the “native” Israelite (Leviticus 19:33-34). A

31
