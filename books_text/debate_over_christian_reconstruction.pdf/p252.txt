234 The Debate over Christian Reconstruction

a situation when a person is accused of heresy for his published
statements. Dr. Anthony Gampolo, a Christian professor, had
been accused of making heretical statements in one of his books.
Others were drawn into the controversy to defend and to
Campolo. As Dr. Fenton describes it:

 

 

Use

The situalion deteriorated, threatening to become a knock-down,
drag-out fight. Opinions may still differ as to whether such a
tragedy was completely averted, Nevertheless, the debate was
tempered by those who were concerned with both the doctrinal
issues and the threat of disruption in the body of Christ. A pancl
of three highly respected Christian leaders was asked to meet
with Campolo and with his critics to help all concerned under-
stand the issues, and to minister constructively both to the ac-
cused and the accusers.!

But what if the person accused of heresy or some other sin will
not listen to a rebuke, even from a group of prominent Christian
brothers? How do conservative churches, divided as they are, en-
force a sentence of excommunication? The only answer at this
stage of church history is for everyone to recognize the right of
other churches to decide who is orthodox and who is a heretic.
Thus, since several orthodox denominations have determined
that the distinctives of Christian Reconstruction—theonomy and
postmillennialism—are orthodox (though not necessary) teach-
ings, critics should give Reconstructionists the benefit of the
doubt. Again, this doesn’t mean that Reconstructionists cannot be
criticized; it means that critics should exercise care. This is a very
imperfect way to deal with heresy. Who decides, after all, whether
the church itself is orthodox? But as long as the churches remain
in the present state of division, there is really no other responsible
and God-honoring solution,

 

  

Finally, there is the issue of emphasis. In the final analysis,
Dave Hunt’s disagreement with Reconstructionists comes down

1. Horace L. Fenton, Jr., When Christians Clash: How to Prevent and Resolve the
Pain of Conflict (Downers Grove, IL: InterVarsity Press, 1987), p. 73.

 
