The Abomination of Desolation: An Alternate Hypothesis 243

of your father, the Devil,” John 8:44], with all power and signs
and false wonders [see Chilton and Josephus on the situation in
Jerusalem just before the end].

10. And with all the deception of wickedness for those who
perish because they did not receive the love of the truth so as to be
saved. [This clearly describes the situation of apostate Judaism.]

11. And for this reason God will send upon them a deluding
influence so that they might believe what is false. [Compare
Romans 1:21-32 and 1 Kings 22:19-23.]

12. In order that they all may be judged who did not believe
the truth but took pleasure in wickedness.

Now, just because these events were fulfilled in a.p. 70 does
not mean that they are irrelevant to us. Churches can also apos-
tatize, and Christ warned the Seven Churches that they too could
be destroyed if Christ departed from them. They would be “deso-
late” and their worship would be “abominable” (Rev. 2-3). The de-
struction of the Temple and of its Jerusalem-culture, as portrayed
in the remainder of Revelation, was thus a warning to the Seven
Churches: If you do the same thing, God will do this to you.
Thus, the principles are still in force, and serve to warn us today:
If our churches depart from Christ, He will destroy both them
and our society, which grew up around them.
