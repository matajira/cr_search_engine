Tommy Ice: A Response ~ Part IT 137

darkened. How can this be? Physiologically or scientifically it
may be difficult to comprehend exactly how this takes place. Does
it mean thal the light is reduced, that heat is reduced, clouds
block the vision of man, or what? I believe that Rev. 8:10, 11 helps
clarify part of this, The waters of the earth are polluted by the
falling of a great star. This is clearly physiological, but spoken in
alanguage of appearance. Just as we know that it is the earth that
revolves around the sun, yet still speak of sunrise and sunset, so
we, speaking of comets as “shooting stars,” when we know they
are not stars. Even though we may not understand exactly how
this takes place, that something dramatic will take place in the
heavenly bodies cannot be doubted.”

This dispensational author tells us that the language “isn’t poetic
as some of the OT passages.” Let’s test this assertion: The stars of
the sky fell to the earth, “as a fig tree casts its unripe figs” (Revela-
tion 6:13); the “sky was split apart like a scroll when it is rolled up”
(v. 14); “the Lamb broke one of the seven seals” (v. 1); John “heard
one of the four living creatures say as with a voice of thunder, ‘Come.’”

The entire Book of Revelation reads like this. It’s absurd to
claim that this chapter is not poetic. Dean tells us that hiding in
caves proves that the chapter cannot be symbolic or poetic. If the
point of the chapter is to describe economic and political upheaval
in descriptive terms reminiscent of the plagues on Egypt, then
hiding in a cave is a natural thing (Judges 6:1-6). But hiding in a
cave seems to indicate a localized judgment as well as a first-
century judgment. David Chilton’s comments are to the point:

The name of this fallen star is Wormwood, a term used in the Law
and the Prophets to warn Israel of its destruction as a punishment
for apostasy (Deut. 29:18; Jer. 9:15; 23:15; Layn. 3:15, 19; Amos
5:7). Again, by combining these Old Testament allusions, St. John
makes his point: Israel is apostate, and has become an Egypt;
Jerusalem has become a Babylon; and the covenant breakers will
be destroyed, as surely as Egypt and Babylon were destroyed.

29. Robert L. Dean, “Essentials of Dispensational Theology,” Biblical Perspec-
tives (Jan/Feb, 1988), p. 4. This newsletter is published by Tornmy Ice.

30. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation
(Ft. Worth, TX: Dominion Press, 1987), p. 240.
