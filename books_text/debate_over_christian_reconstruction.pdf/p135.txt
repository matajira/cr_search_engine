Tommy Ice: A Response — Part IT u7

Jesus say that all these things would happen to “this generation,”
and you also heard Him say that when “you” see these things,
what would you conclude? The most natural (literal) interpreta-
tion is that it would happen to your generation and maybe even to
you personally (cf. Matthew 16:27-28). Again, if it were a future
generation, we would expect Jesus to have said, “when they see
. . . they will bring them . . . they will persecute them.”

Fifth, Jesus warned His followers that they should flee Judea
(Matthew 24:16) when they saw the events described by Him.
Jesus assured them that these judgmental events would be cut
short for the sake of the “elect” (24:22). Those who endured to the
end would be saved, that is, they would not die in the conflagra-
tion if they heeded Jesus’ words and left the city before the descent
of the Roman army (24:13). These events make up “the great trib-
ulation” (24;21)—the same tribulation in which John had a part as
he wrote about what was shortly to come to pass (Revelation 1:1,
3, 9; compare 6:10-1t; 7:14).

Early Warning Signs

The earliest chapters of the Gospels—especially Matthew’s
Gospel — point forward to the impending destruction of Jerusalem
later prophesied by Jesus and the apostles. John the Baptist leads
the way with his indictment of the self-righteous religious leaders,
the very ones who engineered the crucifixion of the Lord of glory.
In Matthew 3:7-12 we read:

But when he saw many of the Pharisees and Sadducees coming
for baptism, he said to them, “You brood of vipers, wha warned you
to flee from the wrath to come? Therefore bring forth fruit in keeping
with your repentance; and do not suppose that you can say to
yourselves, ‘We have Abraham for our father’; for I say to you,
that Ged is able from these stones to raise up children to Abra-
ham. “And the axe is already laid at the root of the trees... .
And His winnowing fork is in His hand, and He will thoroughly
clean His threshing floor; and He will gather His wheat into the
barn, but He will burn up the chaff with unquenchable fire.”
