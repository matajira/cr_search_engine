126 The Debate over Christian Reconstruction

interpretations of Scripture found anywhere within the dispensa-
tional system. In order for Ice’s future fulfillment of Matthew 24
to work, the 70th week of Daniel mus¢ still be future. It is not.'5
Daniel tells us that “seventy weeks are decreed” (v. 24), not 69
weeks, a gap, and a yet future week. The weeks form a unit with-
out separation.

Dispensationalists should be challenged to produce a single
Bible verse that considers such a gap. Why is this so important?
The dispensational system stands or falls on this doctrine. It’s a
point of orthodoxy among dispensationalists. Why is there no
mention of this “great parenthesis” in the Bible and nearly 1900
years of church history?!

Second, Ice has not proven that Matthew 24, Luke 21, and
Mark 13 address two separate events: the destruction of Jerusalem
in a.p. 70 and some future fulfillment. He understands Matthew
and Luke as describing two separate events. No, they are describ-
ing the same event with a different language. Luke’s description is
historical, while Matthew’s is more theological.

Third, Ice’s view. mandates that a future temple must be re-
built. There is not one verse in the New Testament that mentions

15. The separation of the 70th week from the 69th week is unique to dispensa-
tionalism, Any non-dispensational commentary on Daniel will give the orthodox
view. Here’s a list of readily available books on the subject: R. Bradley Jones,
The Great Tribulation (Grand Rapids, MI: Baker Book House, 1980), pp. £3-61;
E. J. Young, The Prophecy of Daniel (Grand Rapids, MI: Eerdmans, 1949); pp.
191-221; Owen, Exposition of the Epistle to the Hebrews, vol. 1, pp. 305-31; Philip
Maura, The Seventy Weeks (Swengel, PA: Reiner Publications, [1923] n.d.). Mauro,
as a former Dispensationalist, wrote:

 

That systern of interpretation I had accepted whole-heartedly and
without the least misgivings, for the reason that it was commended by
teachers deservedly honored and trusted because of their unswerving
loyalty to the Word of God. But I had eventually to learn with sorrow,
and to acknowledge with deep mortification, that the modern system of
“dispensationalism”. . , to which I had thoroughly committed myself,
not only was without scriptural foundation, but involved doctrinal errors
of a serious character, Philip Mauro, How Long to the End? (Boston MA:
Hamilton Brothers, 1927), pp. 4, 15. Quoted in William R. Kimball, The
Rapture: A Question of Timing (Grand Rapids, MI: Baker Book House,
1985), p. 178.
