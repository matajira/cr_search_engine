168 The Debate over Christian Reconstruction

what does Chilton really say? He says that “the Great Commis-
sion to the Church does not end with simply witnessing.” The impli-
cation is that the Great Commission begins with witnessing and
ends with discipleship, Ice continues his evaluation of Chilton’s
quotation by maintaining that “Chilton is reading into the passage
his theology, and then citing it as proof for his theology.” Of
course, this is the essence of the debate between Christian Recon-
struction and the brand of dispensational premillennialism
espoused by Hunt and Ice.

Chilton tells us that the Great Commission’s emphasis on dis-
cipleship means that every “sphere of human activity” is part of
this discipleship process. Didn't Jesus tell us to teach the nations
“to observe all that I commanded you”? (Matthew 28:20). The
Gospels themselves are filled with instruction beyond evangelism.
Reconstructionists are only importing what Jesus tells us to im-
port: adf that He commanded.

The dominion mandate of Genesis 1 was given prior to the
fall. Evangelism was not part of it. The Great Commission of
Matthew 28 deals primarily with evangelism. Nothing can be
changed until man is changed. Evangelism sets fallen man back
on track. But the Great Commission does not end with evangel-
ism; it moves on to teaching the nations aé/ that Jesus taught. Our
task is not accomplished until the ations are discipled. Evangelism
is the church’s primary mission.*! But a priority does not exclude
the second, third, and fourth steps in the process. What are we to
do with all the spiritual babies that we bring into the world
through the preaching of the gospel? What if Jesus does not return
for a thousand years? We'll have millions of baby Christians walk-
ing around in a world dominated by humanists, What are they to
do if Jesus does not return for another thousand years?

40. Iden.

41. See DeMar and Leithart, Reduction of Christianity, pp. 178-83, The relation-
ship between the dominion mandate of Genesis | and the Great Commission of
Matthew 28 has been discussed in DeMar, God and Government: Issues in Biblical
Perspective, pp. 67-69. Again, it makes me wonder if Dave Hunt has really read
the material that has bcen produced on these topics. Also, see R. J. Rushdoony,
The Philosophy of the Christian School Curriculum (Vallecito, CA: Ross House Books,
1981), p. 148.
