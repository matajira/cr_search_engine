The Place of Israel in Historic Postmillennialism 255

Conclusion

Many more examples of the postmillennial concern for the
conversion of Israel could be cited, but we have sufficient evi-
dence before us to permit us to come to several conclusions. First,
the preterist interpretation of prophecy and postmillennial escha-
tology does not imply that the Jews have no place in God’s plan in
history. Owen and Edwards, both of whom emphasized the im-
portance of the destruction of the Jerusalem Temple in a.p. 70,
also believed in a future conversion of national Israel. Historic-
ally, it has been the amillennial position that denies a future large
scale conversion of Jews.”

Second, postmillennial writers were concerned with the issue
of Israel for centuries before dispensationalism made its appear-
ance, It is patently false to imply that dispensationalism was the
first view of prophecy to deal with the question.

Third, the concern for the Jews among postmiliennialist writ-
ers makes anti-Semitism unthinkable.

The reader interested in further evidence for these conclusions
is directed to J. A. DeJong’s As the Waters Cover the Sea and Tain
Murray’s The Puritan Hope.

30. E.g., William Hendriksen, Exposition of Paul’s Epistle to the Romans (Grand
Rapids, MI: 1981), pp. 377-82; and Anthony A. Hoekema, The Bible and the Future
(Grand Rapids, MI: Eerdmans, 1979), pp. 143-47,
