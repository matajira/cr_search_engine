276

DeJong, J. A., 248, 250, 251, 255
DeMar, Gary, 1, 4, 60, 61, 73, 75, 80,
B4, 87, B9, 101, 107, 118, 147, 149,
153, 154, 158, 160, 168, 169, 176

Eareckson-Tada, Joni, 156-57
Eagar, William, 9

Edwards, Jonathan, 252-53, 255
Ehlert, Arnold, 181

Eidsmoe, John, 42, 87

Erickson, Millard J., 1

Estes, Steve, 156

Eusebius, 98-99, 100, 111-112, 145

Faussett, A. R., 113
Feinberg, Paul D., 81, 83, 246
Fenton, Horace L., 233-34
Ferme, Charles, 249

Flinn, P. Richard, 203

Ford, Desmond, 127

Fowler, Richard, 205

France, R. T., 88, 121, 140
Franklin, Benjamin, 18, 42, 189
Friesen, Garry, H2, 171, 219

Gaffin, Richard B., Jr., 118

Gasque, W. Ward, 171

Geisler, Norman L., 27, 65, 147, 148,
150, 152

Geldenhuys, Norval, 145

Gentry, Kenneth, 10, 78, 118, 176-78,
232, 233

Gerstner, John H., 1, 108

Gilley, John A., 100

Gilstrap, Michael, 108

Goodwin, Thomas, 252

Grant, George, 35

Guiness, H. Grattan, 106

Gundry, Robert H., 183, 184

Gunn, Grover E., Hl, 1, 9, 10, 207

Gutenberg, Johann, 193-94

Hagin, Kenneth, 156-57

The Debate over Christian Reconstruction

Haldane, Robert, 253

Hall, Franklin, 178

Harrison, Everett F., 89

Hawkins, Craig S., 4

Hawtin, George, 178

Hebblethwaite, Brian, 102

Hebdriksen, William, 90, 127, 143,

255

Hengstenberg, E. W., 127

Henry, Matthew, 127

Hitchcock, James, 37

Hitler, Adolf, 162

Hodge, A. A., 93, 113

Hodge, Charles, 93, 113, 181, 253

Hoekema, Anthony A., 73, 90, 255

House, H. Wayne, 64, 205

Hoyt, Herman A., 81, 165-66

Hughes, Don, 157

Hummel, Charles, 35

Hunt, Dave, 1, 4, 6, 7, 8, 9, 10, 36, 48,
61, 75, 76, 81, 85, 89, 105, 139,
146-178 passim, 202, 203, 226,
229, 230

Biblical Law, 204-5

Defeatism, 169-70

Dispensational Ethics, 187-88
Distortions by, 23-235
Dominion, 74, 84, 160-69, 212-13
Kingdom of God, 212-15

Law and Grace, 209-10

Manifest Sons of God, 175
Marriage Supper of the Lamb, 216-18
Millennium, 218-21

Natural Law, 147
Postmillenaialism, 88

Progress, 188-195

Rapture, 68, 69, 82, 184-85
Second Coming, 215-16

Social Action, 196-97

Suffering, 153-60

Traditional Dispensationalism, 73
Victory, 157-59
