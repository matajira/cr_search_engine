Tommy Ice: A Response — Part I 91

Other postmillennialists hold a view of the thousand years
similar to the interpretation advocated by amillennialists. The
“thousand years” is thought to be figurative of a “very long period
of time,” since “thousand” is often used in Scripture to mean more
than a thousand (Exodus 20:6; Deuteronomy 1: 5 32:30;
Joshua 23:10; Judges 15:15; Psalm 50:10; 90:4; 91:7; 105:8; Eccle-
siastes 6:6; Isaiah 30:17; 60:22; 2 Peter 3:8). This view is not
inconsistent with the way numbers are used in the Book of Reve-
lation or the Bible in general.!§

We know that the thousand years began with the “binding of
Satan.” Jesus said, “How can anyone enter the strong man’s house
and carry off his property, unless he first d:nds the strong man?
And then he will plunder his house” (Matthew 12:29). Jesus bound
the strong man, Satan, through His sinless life, propitiatory
death, resurrection, ascension, and exaltation. He overpowered
Satan, taking from him all his armor on which he had relied, and
distributed his plunder (Luke 11:22). This is why the demons were
“subject to” the disciples and why Jesus could say, “And I was
watching Satan fall from heaven like lightning” (Luke 10:18). Paul
gives a very descriptive picture of the present status of Satan in
the world: “God will soon crush Satan under your feet” (Romans
16:20). The assumption is that Satan has already been crushed
under Jesus’ feet, Notice that this crushing is to happen “soon,”

 

 

the latter days, Antichrist being destroyed, the Jews called, and the adversaries
of the Kingdom of his dear Son broken, the Churches of Christ being enlarged
and edified through a free and plentiful communication of light and grace, shall
enjoy in this world a more quiet, peaceable, and glorious condition than they
have enjoyed.” The Savoy Declaration of Faith and Order (1658), “Of the Church,”
Chapter XXVI, paragraph V. Philip Schaff, The Creeds of Christendom: With His-
tory and Gritical Notes, 3 vols. (6th rev, ed.; Grand Raids, MI: Baker Book House,
[1931] 1983), vol. 3, p. 723.

16. “As we have found the number ten to symbolize the general idea of fudiness,
totality, completeness, 80 not improbably the number one thousand may stand as the
symbolic number of manifold fullness, the rounded acon of Messianic triumph
.., during which he shall abolish all rule and all authority and power, and put
all his enemies under his feet (t Cor. xv, 24, 25), and bring in the fullness . . . of
both Jews and Gentiles (Rom. xi, 12, 25).” Milton S. Terry, Biblical Hermeneutics:
A Treatise on the Interpretation of the Old and New Testaments (Grand Rapids, MI:
Zondervan, [1883] 1909), p. 390.
