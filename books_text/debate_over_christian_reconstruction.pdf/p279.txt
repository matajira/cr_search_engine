To Those Who Wonder if Reconstructionism Is Anti-Semitic 261

assertions, define themselves as the true protectors of the Jews,
and, with the arrogance that so often accompanies such pragmatic
paternalism, declare that all those who don’t agree with their the-
ology are, in principle, anti-semitic. Hogwash (i.e., non-kosher
argumentation).

T trust this letter has served to provoke more careful thinking
about this most important subject. To be sure, the last word has
not been said. It is my judgment that the interpretation of proph-
ecy requires more patience and care than most other areas of the-
ology. This being the case, we are more faithful servants of Christ
and the church when we allow latitude in this area, all other areas
being orthodox. In this way, it may be that our efforts may turn
toward more productive cooperation in achieving what we both
desire: glory to God through the conversion of sinners, both Jew-
ish and Gentile.

Steve M. Schlisse!
Messiah’s Christian Reformed Church
Brooklyn, New York

“Magnify the Lord with me; let us exalt His name together”
(Psalm 34:3).
