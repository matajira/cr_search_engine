Thinking God’s Thoughts after Him 35

the same as God's worldview, the creature thinking the thoughts
of the Creator.?

Is God’s view of the world comprehensive? Is He concerned
about every nook and cranny of creation? Did He give His life for
the “world”? Is He Lord of “all things”? To all of these questions
we would answer “Yes!” Then, why should Christians limit their
scope of the world? Why should Christians have a lower view of
the world than God does? Why should humanists have a higher
view of the world than we do? George Grant writes:

One of the basic demands of Christian discipleship, of follow-
ing Jesus Christ, is to change our way of thinking, We are to “take
captive every thought to make it obedient to Christ” (2 Corinthi-
ans 10:5). We are “not to be conformed to this world but [are to]
be transformed by the renewing of our minds” (Romans 12:2). In
other words, we are commanded to have a Biblical worldview. All
our thinking, our perspective on life, and our understanding
of the world around us, is to be comprehensively informed by
Scripture.

God’s condemnation of Israel came because “their ways were
not His ways and their thoughts were not His thoughts” (Isaiah
55:8). They did not have a Biblical worldview. When we begin ta
think about the law, or bio-medical ethics, or art, or business, or
love, or history, or welfare, or anything else apart fromm God’s
revelation, we toa have made ourselves vulnerable to condemna-
tion. A Biblical worldview is not optional. It is mandatory.?

How does the Christian begin to develop a Biblical world-
view? Of course, the first place to start is with the Bible. The Bible
is the blueprint for life. Just like a builder turns to his blueprints
to build a house, the Christian turns to the Bible to build a civili-
zation that includes every area of life.

2. Johannes Kepler (1571-1630) wrote: “O God, I am thinking thy thoughts
after thee.” Cited by Charles Hummel, The Galileo Connection: Resolving Conflicts
between Science and the Bible (Downers Grove, [L: InterVarsity Press, 1986), p. 57.

3. George Grant, Bringing in the Sheaves: Transforming Poverty into Productivity
(Atlanta, GA: American Vision Press, 1985), p. 93.
