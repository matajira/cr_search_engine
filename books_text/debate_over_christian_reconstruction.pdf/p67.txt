10
THE FUTURE IS OURS

When Israel was taken to the borders of the promised land,
twelve spies were sent to survey the land and report to the nation
(Numbers 13). Before choosing twelve representatives for the task,
God promised the land would be theirs: “Send out for yourself men
so that they may spy out the land of Canaan, which J am going to
give to the sons of Israel; you shall send a man from each of their
fathers’ tribes, every one a leader among them’ (v. 2). No matter
what the spies encountered, the promise of God should have had
priority and overruled any desire to retreat.

When the spies returned, ten brought back pessimistic (unbe-
lieving) reports (vv. 28-29, 31-33). Two spies, Joshua and Caleb,
returned with optimistic (faithful) reports because they believed
God and not the circumstances they encountered (v. 30). It is im-
portant to note that Caleb never denied that there were “giants in
the land”; he believed God was stronger than any army of giants.
Why is this so? “You are from God, little children, and have over-
come them; because greater is He that is in you than he who is in
the world” (1 John 4:4).

The nation responded to the report without faith. In effect,
they called God a liar: “Then all the congregation lifted up their
voices and cried, and the people wept that night” (Numbers 14:1).
Their refusal to believe the promise of God brought judgment
upon the entire nation.

Israel did not enter the promised land until forty years passed
and the unbelieving generation died (14:26-38). Their pessimistic
perspective of the future affected their plans for the future. The

49
