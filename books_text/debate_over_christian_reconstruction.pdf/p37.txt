3
BY WHAT STANDARD?

What's the real issue in the debate concerning Christian re-
construction? The first issue, and the most basic one, is the ques-
tion of ethics. How are Christians to determine what is right and
wrong? What is our standard? How do we make ethical decisions
in our personal lives? How do we know the right way to treat our
spouse and children? How do we know how to deal with a sinful
brother in the church? How do we know how to treat our employ-
ers or our employees? How do we know what laws are just?

Most Christians would agree that the Bible is our standard for
personal morality, for family morality, for the church, and per-
haps for business ethics. But many would stop short of saying that
the Bible is the standard for civil justice. We need to emphasize
this, not because we believe that politics is the most important
thing, but because this is an area where many Christians become
confused and inconsistent.

If we don’t use the Bible as our standard of civil justice, what
shall we use? We cannot make our final appeal to reason, because
our minds are tainted by the effects of sin. We cannot make our final
appeal to the majority, because the majority often enacts laws that
perpetuate self-interest. Nor can we make our final appeal to some
elite, because they too are prone to error and sin. If we want to
please God in our political action, we need to be obedient to the
Word of God.

This doesn’t mean that it’s always easy to decide what the
Bible teaches about a certain issue. We need to do our homework,
studying both the Bible and our situation, if we want to find God-
honoring policies and laws,

19
