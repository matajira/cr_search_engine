Tommy Ice: A Response — Part IT 139

as irreligious men commonly do, to fortune. He shows it to be a
judgment of God, by whose hand all things are governed.”22

At first reading, Matthew 24:30 seems to indicate that we
should expect to see Jesus “appear in the sky. . . coming on
clouds.” But this is not what it says, and as we’ve seen, coming on
the clouds of heaven is a general description of judgment. Unfor-
tunately, we’re working with a poor translation, The older Ameri-
can Standard Version (1901) reads, “And then shall appear the sign of
the Son of Man in heaven.” But let’s go further and translate from
the Greek:

Then the sign of the Son of Man will appear in heaven, and
then all the tribes of the Jand® will mourn, and they will see the
Son of Man coming on the clouds of heaven with power and great

glory.

This is a word-for-word translation from the Greek, The “sign” is
that the Son of Man is in heaven at His Father's right hand. Isn't
this what Peter preached at Pentecost?: “Therefore having been ex-
alted to the right hand of God, and having received from the Father
the promise of the Holy Spirit, He has poured forth this which
you both see and hear” (Acts 2:33). How were the gathered Jews
to know that Jesus had been “exalted to the right hand of God”?
According to Peter, it was the outpouring of the Holy Spirit at
Pentecost. By “seeing” and “hearing” they knew that Jesus was in
heaven. This was God’s sign to Israel. Isn’t this also what Stephen
saw? “Behold, I see the heavens opened up and the Son of Man
standing at the right hand of God” (Acts 7:56).

The Book of Daniel is the reference point for Jesus’ words in
Matthew 24:30, not our twentieth-century imaginations. When

32. John Calvin, Commentary on the Book of the Prophet Isaiah, 4 vols. (Grand
Rapids, MI: Baker Book House, 1979), vol. 1, p. 49. Dave Hunt has a problem
with this concept of coming. According to Hunt, Jesus just could not have “come”
in a.p. 70 to destroy Jerusalem through the agency of the Roman armies. Sce his
May 1988 CB Bulletin, p. 2. But the Bible expresses God’s coming in just this way.

33. Compare with Luke 21:23.

34. Compare with Matthew 26:64.
