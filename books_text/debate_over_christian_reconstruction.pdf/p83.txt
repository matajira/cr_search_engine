Understanding Christian Reconstruction 65

This is a grossly unfair misrepresentation, especially in the light of
the publication of The Reduction of Christianity wherein the goals of
Christian Reconstructionists are plainly set forth, First, Christian
Reconstructionists believe the kingdom was inaugurated by Jesus
Christ. We do not “bring in the kingdom.” Second, the kingdom is
not “man-made.” The Bible tells us that it’s the “kingdom of God.”
Third, entering the kingdom comes through regeneration. Jesus
tells Nicodemus that he cannot even “see the kingdom” until he is
“born from above,” that is, “born again” (John 3:3).

Biblical Law

The second Reconstructionist distinctive is biblical law. I've
made it abundantly clear in my three-volume God and Government
series and in Ruler of the Nations that civil government is just one gov-
ernment among many governments, The State is just as responsible
to keep God’s law as the individual. Many dispensational premil-
lennialists deny that God’s law is applicable for today’s world,
especially in the area of civil government. The dispensational sys-
tem, if it is consistent with its own interpreting methodology, will
have nothing to do with the Old Testament law in spite of the New
Testament’s own validation of it. Paul deseribes it as “God-breathed

. and profitable for teaching, for reproof, for correction, for
training in righteousness; that the man of God may be equipped
for every good work” (2 Timothy 3:16-17). Paul declares that we
confirm the Law by faith (Romans 3:31).

But what standard can the dispensational premillennialist ap-
peal to when he denies the validity of biblical law? General revela-
tion? Natural law? Look at nature, we’re told, and we can then
determine an ethical system for civil government. This is what Norm
Geisler says, who is a dispensationalist and an ardent opponent of
Christian Reconstruction. He contends, “Government is not based
on special revelation, such as the Bible. It is based on God’s general
revelation to all men. . . . Thus, civil law, based as it is in the nat-
ural moral law, lays no specifically religious obligation on man.”

11. ‘A Premillennial View of Law and Government,” The Best in Theology, gen.
ed, J. 1, Packer (Carol Stream, IL: Christianity Today/Word, 1986), vol. 1, p. 259.
