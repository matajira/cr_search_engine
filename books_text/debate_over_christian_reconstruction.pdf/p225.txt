Questions and Answers 207

Testament. According to the Old Testament men were just because they
were true and faithful in keeping the Mosaic Law.”*| He continues with
these comments: “Men were therefore just because of their own
works for God whereas New Testament justification is God’s work
for man in answer to faith (Rom. 5:1).”2 This is not an isolated
text in Chafer’s Systematic Theology.

Tommy Ice admits that some within dispensationalism used
language that seemed to have taught two ways of salvation, But
Charles C. Ryrie, in his very popular defense of dispensational-
ism, denies the charge of two types of salvation ever being taught.
He maintains that “neither the older nor the newer dispensation-
alists teach two ways of salvation, and it is not fair to attempt to
make them so teach.”** Now, in order to give dispensationalists
the benefit of the doubt, we should only say that some of their
writings seem to express two ways of salvation. The fact that the
New Scofield Reference Bible corrected the notes that seemed to teach
two ways of salvation is evidence that there was confusion.

Tommy Ice continues by stating that “in the New Testament
there is an emphasis on the graciousness of the gospel because it's
going to all the nations. And in that sense, I would say that the New
Testament is more gracious than the Old Testament.” If he means by
this that the gospel message includes the nations beyond the bound-
aries of Israel, whereas under the Old Covenant it did not, then I
will agree that there is an emphasis on the graciousness of the Gos-
pel under the new Covenant. But that grace is not more gracious,
while the extent of the grace is, God’s demands have not changed.
Sin is still sin. Homosexuality, abortion, theft, perjury, and politi-
cal tyranny are still with us. The law still applies in condemning
these acts. The Apostle Paul points this out in 1 Timothy 1:9-10.

21. Lewis Sperry Chafer, Systematic Theology, 8 vols. (Dallas, TX: Dallas Sem-
inary Press, 1948), vol. 7, p. 219. Quoted in Curtis 1. Crenshaw and Grover E.
Gunn, III, Dispensationalism Today, Yesterday, and Tomorrow (Memphis, TN: Foot
stool Publications, 1985), p. 345.

22. Idem.

23. Ibid., pp. 346-397.

24. Charles C. Ryrie, Dispensationalism Today (Chicago, IL: Moody Press,
1965), p. 207.
