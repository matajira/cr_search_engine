134 The Debate over Christian Reconstruction

those “days of vengeance” (Luke 21:22) fits the magnitude of the
offense. The translator of Josephus’s works makes the following
comment in a footnote to Josephus’s eyewitness account of what
Jesus predicts in Matthew 24:1-34:

That these calamities of the Jews, who were our Saviour’s
murderers, were to be the greatest that had ever been since the
beginning of the world, our Saviour had directly foretold, (Matt.
xxiv. 21; Mark xiii. 19; Luke xxi. 23, 24:) and that they proved to
be such accordingly, Josephus is here a most authentic witness.

5. “But immediately after the tribulation of those days
THE SUN WILE BE DARKENED, AND THE MOON WILL
NOT GIVE ITS LIGHT, AND THE STARS WILL FALL
from the sky, and the powers of the heavens will be shaken”
{v. 29).

The tribulation described above took place just prior to the de-
struction of Jerusalem in s.p. 70 upon the city that had rejected
the Messiah, the same city that Jesus wept over. It was this Jeru-
salem, “who kills the prophets and stones those who are sent to
her” (Matthew 23:37), that experienced this “great tribulation.”
With this in mind, it’s important to notice that verse 29 begins
with, “But immediately after the tribulation of those days. . . .” What-
ever verse 29 means, it follows “immediately after” the tribulation
described in verses 15-28. “‘Immediately’ does not usually make
room for much of a time gap—certainly not a gap of over 2000
years.”25

Should we expect the sun Hterally to be darkened and the
moon to cease reflecting the light from the sun? Will literal stars

24, William Whiston, “Preface” to “The Wars of the Jews” in Josephus: Complete
Works (Grand Rapids, MI: Kregel, [1867] 1982), p. 428. The works of Josephus
describe the events predicted by Jesus in Matthew 24. For a shortened version of
the account, see David Chilton, Paradise Restored: A Biblical Theology of Dominion
(Tyler, TX: Dominion Press, [1985] 1987), pp. 237-90.

25. Paul ‘Y. Butler, The Gospel of Luke (Joplin, MO: College Press, 1981), p.
485, Quoted in William R. Kimball, What the Bible Says About the Great Tribulation
(Grand Rapids, MT; Baker Book House, 1988), p. 155.
