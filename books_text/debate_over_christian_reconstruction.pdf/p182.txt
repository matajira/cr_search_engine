164 The Debate over Christian Reconstruction

That man as a covenantal creature is called to culture cannot
be stressed too much. For the Lord God, who called him into
being, also gave him the cultural mandate to replenish the earth
and to have dominion over it. David was so filled with ecstasy at
this glory-filled vocation that he exclaimed in awe and wonder;
“What is man, that thou art mindful of him? . . . For thou hast
made him a little lower than God, And crownest him with glory
and honor... , Thou hath put all things under his feet.”

To say thar culture is man’s calling in the covenant is only
another way of saying that culture is religiously determined.

My thesis, then, is that Calvinism furnishes us with the only
theology of culture that is truly relevant for the world in which we
live, because it is the true theology of the Word.*

Hunt tells us that everything flows from this “false view of do-
minion,” a view of dominion that he never defines for the au-
dience. The above definition of “dominion” was not written by a
Christian Reconstructionist. Notice that it grows out of a Calvin-
istic world view that millions of Christians hold.*? A Reconstruc-
tionist’s view of dominion flows, as Van Til writes, from the belief
that “Calvinism furnishes us with the only theology of culture that
is truly relevant for the world in which we live, because it is the
true theology of the Word.” It is also interesting that Henry Mor-
ris, a dispensational premillennialist, teaches a very similar view
of dominion in his book, The Biblical Basis for Modern Science.

31, Henry Van Til, The Calvinistic Concept of Culture (Grand Rapids, MT:
Baker Book House, [1959] 1971), pp. 7-8.

Culture is often conceived too narrowly, The resulting definition
lacks both scope and insight, breadth and depth. There are people, for
instance, who identify culture with refinement of manners, social
courtesy and urbanity, with the veneer of polite society. For others it is
synonymous with good taste in interior decorating, paintings, music and
literature.

However, the idea that development of the artistic, scientific or social
aspect of man’s nature constitutes culture is altogether too narrow. The
whole man must be involved, and all the aspects of human life have a
bearing on the issue (p. 25).

32. Again, this is shown in Reduction of Christianity, pp. 30-37.
