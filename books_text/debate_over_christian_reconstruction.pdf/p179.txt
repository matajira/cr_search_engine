Dave Hunt: A Response 161

decades, the writings of Reconstructionists have been noted for
espousing a decentralized social order where more freedom will
operate in the world if biblical law were chosen over autonomous
law. The State has usurped control from individuals, families,
schools, businesses, and local governments. What's even more
tragic, we've sat back and let it happen.*

Dave Hunt does not give us an example of what he means by
what Reconstructionists mean by “dominion over other people.”
His fabricated “dominion over people” definition was dealt with at
length in The Reduction of Christiantty.®

But evil men must be restrained, In this sense, there must be
dominion over some people because of their actions to harm others.
God ordained civil government for this very purpose (Romans
13:1-7), Is it wrong to “dominate” other men through the God-
ordained channels of civil government to remove law-makers
from public office (e.g., voting, swaying public opinion, ete.}
when they continue to enact and enforce laws that allow women,
for example, to abort their unborn babies? Should Christians re-
main silent when laws are passed that restrict parents from send-
ing their children to Christian schools or from educating their
children at home? Education is a tool to exercise dominion over a
culture, for good or evil. Is Dave Hunt telling us that sodomites

 

Gettysburg speech of 1863 of ‘government of the people,’ he gave the world a neat
definition of democracy which has sinced been widely and enthusiastically adopted.
But what he enunciated as a form of government was not in itself especially
Christian, for nowhere in the Bible is the word democracy mentioned. Ideally,
when Christians meet, as Christians, to take counse] together, their purpose is
not (or should not be) to ascertain what is the mind of the majority but what is the
mind of the Holy Spirit—something which may be quite different,” “Thatcher:
Sow, and ye Shall Reap for All,” World (June 20, 1988}, p. 5.

28. “Only 50 percent of Christians eligible to vote are registered. Of those reg
istered, only 25 percent actually vote on election day. This means just 12.5 per-
cent of eligible Christians actually take part in the political process.” California
Voter's Guide. Quoted in Intercessors For America (July/August 1988), p. 4.

29. See pages 24-29 of Reduction of Christianity, Throughout the debate, I got
the distinct feeling that Dave Hunt had not read The Reduction of Christianity. Why
was he raising issues that were already dealt with in the book? A more helpful ap-
proach would have been to interact with the definitions and explanations out-
lined in Reduction.
