92 The Debate over Christian Reconstruction

that is, soon after Paul wrote these prophetic words. If Satan is yet
to be crushed under our feet, then “soon” ceases to mean anything.
Where, then, is the literalism of dispensationalism when “soon”
means later, much later?

Millennial Blessings

If the millennial blessings are still future, as in premillennial-
ism, then the postmillennial position is wrong. Then there would
be no verses to support it. But consider this argument: The pre-
millennialist places certain “millennial” blessings in a future earthly
thousand year period. If it can be demonstrated that these so-called
premillennial blessings are actually present now, in the “church
age,” then postmillennialism does have verses to support it, The
same verses, in fact, that the premillennialist uses, the postmillen-
nialist uses, It’s a question of timing!

In 1652 John Owen preached before the English House of
Commons describing “The Advantage of the Kingdom of Christ
in the Shaking of the Kingdoms of the World.” “Therein he explained
the kingdom of God as spiritual control of Christians resulting in
obedient conformity to the word of Christ. The antichristian king-
doms being shaken will, according to Owen, be replaced with the
triumph of Christ’s reign, signalized by the conversion of the
Jews. Certain things will characterize this time.”!”

That God in his appointed time will bring forth the kingdom
of the Lord Christ unto more glory and power than in former
days, I presume you are persuaded. Whatever will be more,
these six things are clearly promised:

1. Fullness of peace unto the gospel and the professors thereof,
Isa. 11.6,7, 54.13, 33.20,21; Rev. 21.15

2. Purity and beauty of ordinances and gospel worship, Rev. 11.2,
21.3. The tabernacle was wholly made by appointment, Mal.
3.3,4; Zech. 14.16; Rev. 21.27; Zech. 14.20; Isa. 35.

17, Greg L. Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
The Journal of Christian Reconstruction: Symposium on the Millennium, ed. Gary North,
Vol. III, No. 2, (Winter 1976-77), p, 84,
