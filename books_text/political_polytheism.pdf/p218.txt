194 POLITICAL POLYTHEISM

very least, we at the Institute for Christian Economics were told by
one of his associates that he did not personally do the all of the basic
research for it. Like his popular early books, which were edited by
James Sire from tapes of Schaeffer's lectures, A Christian Manifesto
may have been merely edited in its final stages by Schaeffer. If he did
research it, then he was even more dishonest in hiding footnotes
than T have previously indicated.

 

Chilion’s Discovery

In 1981, David Chilton spotted a phrase on page 97 of A Christian
Manifesto which had been lifted virtually word for word from page
200 of Chilton’s essay on John Knox, published in early 1979. Here
is what he found:

Chilton: Within a few years, tens of thousands of Huguenots were
offering armed resistance to the French government; and the year Knox
dicd saw the beginning of the successful Calvinist revolt and takeover of
Holland and Zceland. Knox had shocked the world with his Admonition to
England, but he had also convinced it. As Ridley states it, “The theory of the
justification of revolution is Knox’s special contribution to theological and
political thought.”7

Schaeffer: Within a few years, tens of thousands of Huguenots were
offering armed resistance to the French government; and che year Knox
died saw the beginning of the successful revolt and saving of Holland. Knox
had shocked the world with his Admonition to England, but he had also been
convincing. Jasper Ridley in Joke Knox writes, “Che theory of the justifica-
tion of revolution is Knox’s special contribution to theological and political
thought.”

Then Chilton spotted another direct lifting, in this case from
Richard Flinn’s essay on Samuel Rutherford, which appeared in the

75. Philip Yancey, “Francis Schaeffer: A Prophet for Our Time?” Christianity
Today (March 23, 1979), p. 16. Yanccy says that only What Ever Happened to the Human
Race (1979) and How Should We Then Live (1976) were books actually written by
Schaeffer, as distinguished from edited tapes (p. 17)

Tremendous pressure was brought against the editor of IVP, through Franky
Schaeffer’s mobilizing of public protests to the IVP Board of Trustecs when IVP’s
published the pro-abortion book, Brave New People (1984) by D. Gareth Jones. As
Franky said in an interview on Pat Robertson's “700 Club,” IVP made its money
with his father’s books and then wound up publishing a pro-abortion book. IVP soon
dropped the book, Eerdmans then picked it up.

76. David Chilton, “John Knox,” Journal of Christian Reconstruction, V (Winter,
1978-79), p. 200. ‘This was the Symposium on Puritanism and Law, which T edited,

TT. Schaeffer, Complete Works, vol. V, p. 472
