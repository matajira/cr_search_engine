The Strategy of Deception 439

tion; French Masonry’s hostility to absolutism led to open revolu-
tion. Subversion by stealth is no less a threat than subversion by rev-
olution. Stealth in fact calls less attention to itself.'7°

Conclusion

Colonial Masonry was one of the major components of the
American Revolution, and especially of the Constitutional settle-
ment. On this point, Rushdoony has remained silent, almost as if he
has been afraid to raise the question.!?! Were he to pursue it, he
would find his thesis regarding the Christian roots of the Constitu-
tion seriously threatened.

I have called the Convention the first stage of a coup. I have
argued that Masonic influence was important both in terms of the
philosophy of the delegates and their membership in the lodges. If
the entire nation had been Masonic, then this would not have been a
coup, But very few colonists were Masons. Prior to the Revolution-
ary War, there were about two hundred lodges in the thirteen col-
onies.!?? Their combined membership was somewhere between 1,500
and 5,000. Yet the total population of the nation was about 2.5 mil-
lion. By 1800, there were perhaps 16,000 members. !?> Thus, to argue
that the Constitution was essentially Masonic is necessarily to argue
for a conspiracy.

Christians ratified it. They must have been ignorant about the
long-term effects of their actions, They must have been unaware of
the covenantal implications of their decision. The defenders of the
document were able to appeal to a common body of opinion regarding
religious freedom and the supposed tyranny of Christian creeds. !?*

120, Cf. Margaret Patricia McCarran, Fabianism in the Political Life of Britain,
1919-1933 (2nd ed.; Chicago: Heritage Foundation, 1954); Rose L. Martin, Fabian
Freeway: High Road to Socialism in the U.S.A., 1884-1966 (Belmont, Massachusetts:
Western Islands, 1966). This was a popular condensation of McCarran’s suppressed
manuscript, “The Fabian Transmission Belt.”

121, [have often wondered what is in Rushdoony’s 1965 manuscript, “The Strategy
of Subversion,” which he never completed, or at least never submitted for publication.

122, Morse, Freemasonry in the American Revolution, p. 28.

123, William Preston Vaughn, The Arti-Masonic Parly in the United. States,
1826-1843 (Lexington: University Press of Kentucky, 1983), p. 11. Vaughn estimates
that there were only a hundred lodges at the outbreak of the Revolution.

124. This anti-creedaiism was a heritage of the pietism and revivalism of the mid-
dle third of the eighteenth century, See Mead, “American Protestantism,” John M.
Mulder and John F. Wilson (eds.), Religion in American History (Englewood Cliffs,
New Jerscy: Prentice-Hall, 1978), pp. 164-65, 173-74.
