210 POLITICAL POLYTHEISM

cases, a presbytery will simply allow a known adulterous pastor to
take a call from a church in a different presbytery, if only he will
leave without forcing the presbytery to conduct an embarrassing for-
mal trial. In short, “Keep him away from eur wives; other husbands
must take their chances!”1?¢

Should we be surprised when we find that this denial of covenant
sanctions in history eventually undermines pastors’ own leadership?
It destroys their own institutional continuity. This is why both amil-
lennialism and premillennialism are doomed. Their theologians in-
sist that Christianity (pre-second coming) is doomed to failure and
progressive impotence, and their followers act accordingly. Their
view of ethics inevitably affects their view of historical progress.
Denying Deuteronomy’s covenantal cause-and-effect relationship
between ethics and God's judgments in history, pessimillennialists
thereby commit institutional suicide. Ideas do have consequences.

Why This Chapter?

Until Francis Schaeffer arrived on the scene, there was literally
not a single nationally known Bible-believing intellectual leader in
American Protestantism after the death of J. Gresham Machen on
New Year’s Day in 1937. There had been a gap in America’s Prot-
estant leadership that lasted almost a full generation. There were
conservative theological books defending this or that circumspectly
limited theological doctrine— written primarily by Lutherans or
faculty members of Machen’s Presbyterian Westminster Theological
Seminary —as well as Bible commentaries, but there was barely a
six-day creationist movement and no visible conservative Christian
activism, political or otherwise. Protestant Christianity disappeared
as an intellectual force in the United States carly in the second term
of President Franklin Roosevelt, and did not reappear until several
years after the assassination of President John Kennedy in 1963. It
did not emerge as a major political force nationally until the 1976
Presidential election (Garter vs. Ford), when fundamentalist Chris-

 

ers, when cach of them refused to submil to the denomination. Bakker never showed
up to the formal hearing, and Swaggart refused to obey the denomination’s one-year
ban from preaching. The only sanction was the erasure of their names from the de-
nomination’s ministerial rolls.

120. Tam not exaggerating. I purchased part of the library of such a man when he
decided not to take his presbytery’s offer. He voluntarily resigned from the pas-
torate; he was not removed.
