Rushdoony on the Constitution 683

Rushdoony never cites Blackstone directly; and the one quotation he
cites from secondary sources is his defense of the absolute sover-
eignty of Parliament. Had he read Blackstone, he would have had
great difficulty in defending his chapter on sovereignty. Consider
Blackstone’s general statement that “Sovereignty and legislature are
indeed convertible terms; one cannot subsist without the other.”
He went on to speak of “the natural, inherent right that belongs to
the sovereignty of a state, wherever that sovereignty is lodged, of
making and enforcing laws,”*> This is surely the language of political
sovereignty.

Rushdoony’s chapter on sovereignty I regard as the weakest in
This Independent Republic. He makes it look as though the Constitu-
tion possessed a judicial continuity with Christianity. It did not. It
represented a fundamental break from Christianity, a break that the
Lockean concept of humanistic sovereignty and civil compact had
been preparing for almost a century. Rushdoony still believes that a
restoration of Constitutional order is the best strategy for Christian
Reconstruction in the United States. Not only is this impossible es-
chatologically — time does not move backward —but it is naive judi-
cially. In his desire to make the case for Christian America, he closed
his eyes to the judicial break from Christian America: the ratification
of the Constitution, The Christian cultural continuity of America
was not able to be sustained by subsequent generations; the judicial
break with Christianity had been definitive.

Rushdoony’s Rewriting of Constitutional History

It is this covenantal fact which Rushdoony, in his 30-year defense
of the Constitution as an implicitly Christian document, has refused
to face. Indeed, he has created a whole mythology regarding the
oath in order to buttress his case. To an audience of Australian
Christians, who could not be expected to be familiar with the U.S.
Constitution, he said in 1983: “In every country where an oath of
office is required, as is required in the United States by the Constitu-
tion, the oath has reference to swearing to almighty God to abide by
His covenant, invoking the cursings and blessings of God for obedi-

38, Ibid., pp. 18 (from a book by Clarence Manion), 29 (from Tocqueville).

34. William Blackstone, Commentaries on the Laws of England, 4 vols. (Chicago:
University of Chicago Press, [1765] 1979), 1, The Rights of Persons, p, 46.

35. Ibid., 1, p. 47.
