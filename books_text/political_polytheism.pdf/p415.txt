Renewed Covenant or Broken Covenant? 391

and representatives might all be pagans.”*° A prophetic voice, in-
deed! It was not heeded. But this objection was more distinctively
political and practical. The more important issue was covenantal,
but the opponents of the Constitution did not fully understand this.
(Surely today’s textbook commentators do not.) The officers of the
U.S. government are not to be subjected to a religious test for hold-
ing office.

We must understand what this means. It means that civil officers
are not under an oath to the God of the Bible. It means that in the exercise
of their various offices, civil magistrates are bound by an oath to a
different god. That god is the American People, considered as an au-
tonomous sovereign who possesses original and final earthly jurisdic-
tion. This view of the sovereign People is radically different from
anything that had been formally stated or publicly assumed by pre-
vious Christian political philosophers. The People were no longer
acting as God’s delegated judicial agents but as their own agent.
This same view of political sovereignty undergirded Rousseau’s
political theory, and also the various constitutions of the French
Revolution. The ratification of the U.S. Constitution was therefore a
formal covenantal step toward the left-wing Enlightenment and
away from the halfway covenant political philosophy of Christianity
combined with right-wing Scottish Enlightenment rationalism. It
would take the rise of Darwinism and the victory of the North in the
Civil War to make clear the judicial nature of this definitive step to-
ward Rousseau’s unholy commonwealth.*”

The Fourteenth Amendment (1868) brought the federal govern-
ment’s religious toleration to the states, a procedure originally
denied to the federal government by the First Amendment, which

45. Henry Abbot, North Carolina ratifying convention: Elliot's Debates, TV, p
192.

46. This is not to say that Americans steadily abandoned Scottish conunon sense
ralionalism afier 1787. They did not. It remained the dominant intellectual tradition
in the U.S, until Darwinism broke its hold on men’s thinking. Bul the major fune-
tion of this school of thought was to preserve Newtonian rationalism and eighteenth-
century natural law philosophy in the thinking of evangelicals. See George M.
Marsden, The Evangelical Mind and the New School Presbyterian Experience: A Case Study of
Thought and Theology in Nineteenth-Century America (New Haven, Connecticut: Yale
University Press, 1970), pp. 231-33.

47. Lam not arguing that this was a self-conscious step toward Rousseau. Rousscau’s
influence in colonial America was minimal, limited mainly to his educational theories
in Emile, See Paul M. Spyrlin, Rousseau in America, 1760-1809 (University, Alabama:
University of Alabama Press, 1969).

 

  
