524 POLITICAL POLYTHEISM

views of their civil judicial status. There will be screening. The ques-
tion is: By whose covenant?

The problem in understanding this judicial screening process is
easy to state but hard to comprehend, namely, covenants are judicially
binding under God. He takes them seriously —as seriously as He takes
Church covenants and family covenants. The civil and military
alliance of the Revolutionary period, from July 4, 1776, until the ra-
tification of the Articles by the state legislatures in 1781, was more
than an alliance; it was a covenant. The Declaration of Independ-
ence was not heralded as'a covenantal document, but it was one. It
had to be; it formally dissolved the previous civil covenantal ties with
Great Britain: “When in the Course of human events, it becomes
necessary for one people to dissolve the political bands which have
connected them with another, and to assume among the Powers of
earth, the separate and equal station to which the Laws of Nature
and of Nature’s God entitle them. . . .” The sovereign of this new
civil covenant was Newton’s Unitarian god of nature. Thus, the next
step — establishing the by-laws of a formal covenant — was far easier
to take.*

 

A Unitarian Rebellion

In their act of Unitarian political rebellion, the colonies committed
treason, not just against Great Britain, but against God. This is
what the heirs of the American Revolution never admit, even in pri-
vate, Neither the revolutionaries nor their heirs have taken biblical
covenant theology seriously, so the covenantal character of that civil
rebellion has simply been ignored for over two centuries.

The revolutionary leaders did not clearly and formally appeal to
the Trinitarian God of the Bible in defending their rebellion; in-
stead, they appealed forthrightly again and again to Newton’s Uni-
tarian god. The Congress asked Jefferson to write the covenantal
document that formally broke the existing covenant with the King.
Jefferson became their covenantal representative, and therefore the
new nation’s representative (point two of the biblical covenant).

4, The Declaration is sometimes referred to as having established the nation’s
“organic” law. This is the language of philosophical realism, religious pantheismn,
secular conservatism, and Roman Catholicism. The Declaration was a covenant
treaty under a god that bound the formerly subordinate British states into a new ju-
dicial union. Covenants are judicial, not organic. We must abandon both nominal-
ism (contractualism) and realism (organicism) in our thinking.
