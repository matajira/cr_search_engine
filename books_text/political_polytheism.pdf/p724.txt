700 POLITICAL POLYTHEISM

under the treaty of the great collective king: the Constitution. This
was why the Framers insisted on requiring an oath of allegiance to
the Constitution that made illegal any judicial allegiance to God by
federal officers. The oath made the federal government sovereign.
This is exactly what Hamilton announced in Federalist 27.5° Yet
Rushdoony is still using this bit of mytho-history regarding the idea
of sovereignty in the early American period in order to justify his
defense of the Constitution, “The Constitution is unique in world
history in that there is no mention of sovereignty, because sovereignty
was recognized as being an attribute of God.”® Indeed, it truly was
seen as an attribute of God, and the Framers identified this god: the
People.

The transformation. of Rushdoony’s biblical judicial theology of
the early 1970's into a theological defense of judicial neutrality in the
late 1980's was accurately predicted . . . by Rushdoony; “If a doc-
trine of authority embodies contradictions within itself, then it is
eventually bound to fall apart as the diverse strains war against one
another. This has been a continuing part of the various crises of
Western civilization. Because the Biblical doctrine of authority has
been compromised by Greco-Roman humanism, the tensions of au-
thority have been sharp and bitter.”6* No sharper and no more bitter
than in the remarkable case of Rushdoony v Rushdoony.

86. Rushdoony has pointed to an incident late in Hamilton's career that indicates
Christian faith, Hamilton’s call to create a Christian political party just before he
was killed, He relates this in his taped lecture on Leviticus 8:1-13.

What Rushdoony is referring to is Hamilton’s 1802 call for a “Christian Constitu-
tional Society.” This society was not to be a separate political party, but a means of
challenging atheism in politics generally, especially the Jeffersonians. It was to be a
network of political clubs. He also proposed the creation of charitable societies, a
Christian welfare program. Hamilton's biographer Jacob Cooke points out that this
concern for Christianity came only after he had lost all political influence nationally.
“Perhaps never in all American political history has there been a fall from power so
rapid, so complete, so final as Hamilton’s in the period from October, 1799, to
November, 1800.” Cooke, Alexander Hamilton: A Profile (New York: Hill & Wang,
1967}, p. 246. While Cooke believes that Hamilton was actually transfarmed inter-
nally, he ties this to this to his loss of political influence. In short, when he had
power, Hamilton was not a Christian, and he helped to destroy the remaining
Christian civil foundations of the national government.

87. This was his reply to Otto Scott's comment about the U.S. being the first
nation to establish itself without reference to God. Q & A, Leviticus sermon, Jan.
30, 1987.

88. Rushdoony, /nstitutes, p. 213.
