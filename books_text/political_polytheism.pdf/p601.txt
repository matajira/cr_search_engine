The Restoration of Biblical Pluralism 577

The kingdom of God includes every human institution, and every aspect of life,

for all of life is under God and is governed by His unchanging principles. All of
life is under God and God’s law because God intends to judge all of
life in terms of His law.*

In this structure of plural governments, the institutional churches
serve as advisors to the other institutions (the Levitical-function), but
the churches can only pressure individual leaders through the threat
of excommunication, As a restraining factor on unwarranted
Charch authority, an excommunication by one local church or de-
nomination is always subject to review by another, if and when the
excommunicated person seeks membership elsewhere. Thus, each of
the three covenantal institutions is to be run under God, as inter-
preted by its lawfully elected or ordained leaders, with the advice of
the churches, not their compulsion.

All Christians are in principle theocrats. All Christians say that
God rules the universe. God (dheos} rules (krates). Theocracy means
simply that God rules. He rules in every area of life: Church, State,
family, business, science, education, etc. There is no zone of neu-
trality. There is no “King’s X” from God. Men are responsible for
everything they think, say, and do. God exercises total jurisdiction.
Jurisdiction means law (juris) and speaking (diction). God speaks His
Word. It is a comprehensive Word. Anyone who says that God's law
does not apply to some area of life is thereby saying that God does
not have jurisdiction in that area. “No law —no jurisdiction.”

Revolution and Law

T am convinced that both the West and the Far East are about to
experience a major transformation. It has already begun. The pace
of social change is already rapid and will get faster. The technologi-
cal possibility of a successful Soviet nuclear strike against the United
States grows daily;5 so does the possibility of chemical and biological
warfare;® so does the threat of an AIDS epidemic, None of these
threats to civilization may prove in retrospect to be devastating, but

4. Ibid., ch. 4.

5. Angelo Codevilla, While Others Build: The Commonsense Approach to the Strategic
Defense Initiative (New York: Free Press, 1988); Quentin Crommelin, Jr., and David
S. Sullivan, Soviel Military Supremacy (Washington, D.C,: Citizens Foundation,
1985).

6. Joseph D. Douglas and Neil G. Livingstone, America the Vidnerable: The Threat of
Chemical and Biological Warfare (Lexington, Massachusetts: Lexington Books, 1987).
