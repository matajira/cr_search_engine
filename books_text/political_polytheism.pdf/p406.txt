382 POLITICAL POLYTHEISM

John Adams, Architect

The major intellectual influence in the actual structuring of the
U.S. Constitution was probably John Adams rather than Madison.
In December of 1787, the final volume appeared of Adams’ famous
three-volume study of the state constitutions, A Defense of the Constitu-
tions of the Government of the United States. The first volume had ap-
peared while the Convention was assembling. This study was a
defense of the idea of the separation of powers, a theme that he had
written about earlier. Adams had been the primary architect of the
1780 Massachusetts constitution. Thus, his blunt speaking was both
representative of the new worldview and authoritative nationally,
He viewed their earlier constitution-writing actions as unique in his-
tory: the creation of a republic founded on the sovereignty of the
people, with only a brief peripheral mention of Christianity. Notice
carefully his reference to Vitruvius, the Roman architect; this
fascination with Vitruvius had been basic to European humanism
since the Renaissance.”*

It was the general opinion of ancient nations that the Divinity alone was
adequate to the important office of giving laws to men. . . . The United
States of America have exhibited, perhaps, the first example of govern-
ments erected on the simple principles of nature; and if men are now suffi-
ciently enlightened to disabuse themselves of artifice, imposture, hypocrisy,
and superstition, they will consider this event as an era in their history. . . .
It will never be pretended that any persons employcd in that service had in-
terviews with the gods or were in any degree under the inspiration of
Heaven, more than those at work upon ships or houses, or laboring in mer-
chandise or agriculture; it will forever be acknowledged that these govern-
ments were contrived merely by the use of reason and the senses, .. .
Neither the people nor their conventions, committees, or subcommittees
considered legislation in any other light than as ordinary arts and sciences,
only more important. Called without expectation and compelled without
previous inclination, though undoubtedly at the best period of time, both
for England and America, suddenly to erect new systems of laws for their
future government, they adopted the method of a wise architect in erecting
a new palace for the residence of his sovereign. They determined to consult
Vitruvius, Palladio, and all other writers of reputation in the art; to ex-
amine the most celebrated buildings, whether they remain entire or in
ruins; to compare these with the principles of writers; and to enquire how

24. Frances A. Yates, The Rosicrucian Enlightenment (London: Routledge & Kegan
Paul, 1972), p. ll, See above, pp. 342-43.
