572 POLITICAL POLYTHEISM

tal flaws of the U.S. Constitution: 1) the prohibition of a Trinitarian
oath for all U.S. officials; and 2) the removal of the affirmation of the
Bible as the revealed, sovereign, exclusive, and authoritative Word
of God. Not all of the state governments had made these two cove-
nantal mistakes in 1787.

The Constitution will eventually be amended, so that the reli-
gion of the nation is at last consistently manifested in its incor-
porating covenant document. Either the society will be remade by
the politicians, bureaucrats, and hidden hierarchies into a secular
society, top to bottom, or else it will become a Christian society, bot-
tom to top. A halfway covenant Christianity cannot survive this
clash of irreconcilable worldviews. Neither can a halfway covenant
secular humanism, One or the other will triumph, or else lose to a
third unificd worldview.

This suggestion will be attacked as immoral, unamerican, and
tyrannical. It will be atcacked as being undemocratic— the greatest
political sin of this dying era. But the critics will not admit the truth,
namely, that the U.S. Constitution is only the judicially official
aspect of the story of political rule in America. The system of elitist
control over national affairs in America which Georgetown Univer-
sity’s Caroll Quigley wrote about favorably in Tragedy and Hope
(1966),2! and which George Washington University’s Arthur Selwyn
Miller wrote about favorably just before he dicd in 1988,77 is never
mentioned in polite academic circles. This system of hidden hierar-
chies is nonetheless the way our world works today.

The inescapable political fact is this: there must always be repre-
sentation. This representation can be open or hidden, or more likely,
hidden with the illusion of being open, It is time for Christians to
cease deluding themselves about the hidden hierarchies of the mod-
ern democratic world. There will always be hierarchies; the question
Will they be open.or hidden? In modern democracy, where the
political hierarchy is formally open, it is in fact secretly closed, It was
planned that way, beginning no later than 1787,

 

 

21. Caroll Quigley, Tragedy and Hope: A History of the World in Our Time (New York:
Manmillan, 1966), especially Chapter 17. This textbook is extremely rare, available
(if at all) only in a “pirate” edition.

22. Arthur Selwyn Miller, The Secred Constitution and the Need for Constitutional
Change (Westport, Connecticut: Greenwood Press, 1987).
