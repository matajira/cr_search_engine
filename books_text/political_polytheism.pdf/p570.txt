546 POLITICAL POLYTHEISM

church government had arisen in 1785 because of the supposed need
to escape the annual meetings in Philadelphia.

The new plan also entitled the General Assembly to issue stand-
ing rules, which a majority of the presbyteries would have to ratify.
Any student of bureaucracy can see what the results would be. The
General Assembly would normally be attended by the activists in the
presbyteries. Thus, any organized resistance by over half of the pres-
byteries would be unlikely. To change this new system, it would take
a two-thirds vote of the presbyteries.

Church and State

The restructured form of government included a revision of the
Westminster Confession of Faith: specifically, Chapter XX (closing
paragraph), XXIH:3, and XXXI:1, 2. These were the sections deal-
ing with the relationship of Church and State, in which the civil mag-
istrate was charged with certain tasks, such as defending the Church
and calling assemblies. The main figure on the committee was New
Side leader John Rogers, who had served on all of them since 1785,
He became an ecclesiastical leader in the late 1760's during the colonial
battle against the sending of an Anglican bishop to the colonies. He
believed so greatly in the separation of Church and State that he
thought ministers should not vote in civil elections.*? The Synod was
adjourned. In 1788, it reconvened, and the recommended changes in
the Confession were approved. Church historian Philip Schaff de-
scribes these alterations:

The changes consist in the omission of those sentences which imply the
union of Church and State, or the principle of ecclesiastical establishments,
making it the duty of the civil magistrate not only to protect, but also to
support religion, and giving to the magistrate power to call and ratify eccle-
siastical synods and councils, and to punish heretics. Instead of this, the
American revision confines the duty of the civil magistrate to the legal pro-
tection of religion in its public exercise, without distinction of Christian
creeds or organizations. It thus professes the principle of religious liberty
and equality of all denominations before the law. This principle has been
faithfully and consistently adhered to by the large body of the Presbyterian
Church in America, and has become the common law of the Jand,%°

48. Carl Bridenbaugh, Mitre and Sceptre: Transatlantic Faiths, Ideas, Personalities, and
Politics, 1689-1775 (New York: Oxford University Press, 1962), p. 281.

49. Trinterad, Forming, p, 260.

50. Philip Schaff, The Creds of Christendom, 3 vols. (Grand Rapids, Michigan:
Baker Book House, [1877] 1977), I, p. 807.
