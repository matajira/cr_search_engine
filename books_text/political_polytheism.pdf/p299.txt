Halfway Covenant Historiography 275

1787? They are few and far between. But this dearth of documentary
evidence is neither here nor there for Dr. Marsden. He resorts in-
stead to rhetoric, which is a safe tactic only if one’s intended au-
dience is utterly ignorant of even the most cursory facts of one’s own
academic discipline. This is grossly unfair to the victimized reader,
who thinks he is reading a history book rather than a political tract.

We can honestly trace the history of American pluralism “From
Morton to Marsden,” That Dr, Marsden would select Thomas Mor-
ton, a highly unrepresentative figure in colonial American history, as
“probably” more representative of most colonists than the “strict”
Puritans, is clear testimony to Dr. Marsden’s personal war against
any civil manifestation of Christian civilization. Morton was a de-
viant both sexually and religiously, and those few humanistic text-
book writers who have bothered to mention this otherwise peripheral
figure have done so only because he was éhe representative example
of what New England was noi. But not Dr. Marsden; he singles out
Morton as a representative of what most colonists “probably” were in
their anti-prayer closets.

To present Thomas Morton as a more representative figure than
the “strict” Puritans in American colonial history is the equivalent of
selecting Benedict Arnold as a representative figure in colonial mili-
tary history, or Aaron Burr as a representative American political
figure. It would make as much sense to select Shimei as a representa-
tive political igure in Davidic Israel (II Sam, 16:5-14; I Ki. 2:8-10,
36-46). In fact, Dr. Marsden can be accurately described as the dean
of the Shimei school of historiography.

What Marsden’s statement regarding Morton indicates is that
he, in his academic life, has reverted back to something reminisceni
of the secular humanist worldview of Vernon L. Parrington and the
Progressive historians, but without doing any new primary source
research, It was they who regarded New England's theocratic cxperi-
ment as an aberration. Having moved to post-Civil War American
religious history as his preferred specialty, Marsden briefly returned
to colonial religious history in 1983 as if Perry Miller and Edmund
Morgan had never appeared on the academic scene. It was Miller
who understood the Puritan doctrine of the covenant as foundational
to the development of American political theory, with the doctrine of
natural rights a secularized derivative;!” it was Parrington who

172, Perry Miller, “From the Covenant to the Revival,” in The Shaping of American
Religion, 4 vols., edited hy James Ward Smith and A. Leland Jamison (Princeton,
New Jersey: Princeton University Press, 1961), I, pp. 322-68,
