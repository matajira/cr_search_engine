370 POLITICAL POLYTHEISM

McDonald is representative of the best of the humanist historians
of the origins of the American Revolution and the Constitution. His
mastery of the facts of the 1780's is impressive; for example, he has
read every colonial newspaper of the era. His mistake is in asking
subordinate questions of subordinate figures. He ignores the source
of the modern West's paradigm shift—Isaac Newton— and concen-
trates instead on its diligent developers in the limited field of political
theory. He does not discuss the origin of the politics of the 1780's in
the laboratories of the 1680's. The story of the American Revolution
does not begin in 1688 with the Glorious Revolution; it begins in
1687 with the Principia, or in 1660 with the restoration of Charles II
and his incorporation in 1661 of the Royal Society. When the Invisi-
ble College became visible, the revolution was.?2?

Conclusion

McDonald’s neglect of Newton is matched by his far less well-
informed equivalents in the Christian academic community. For
well over a century, a handful of Christian conservatives have at-
tempted to place the American Revolution within the context of
Christian thought and culture, despite the steady expiration of both
explicitly Christian thought (moral casuistry) and culture in the early
eighteenth century. This approach can be somewhat successful with
respect to certain intellectual defenses of the American Revolution
itself, especially by an appeal to patriotic sermons, though not with-
out considerable qualification and a clear recognition of the crippling
effects of Newtonian natural law philosophy on the defenses of the
best Christian political apologists.?23 On the other hand, such an at-
tempt is utterly fruitless with respect to the ideological origins of the
U.S. Constitution. Noll, Hatch, and Marsden recognize this, and
they have successfully defeated their traditional conservative Chris-
tian rivals in the field of intellectual battle. Their political ideal is
Newtonian, and they can demonstrate that their preference is incar-
nated in the Constitutional settlement, (Being Newtonian, however,
such an ideal is without epistemological foundation today, thereby
making it possible for neo-evangelicals to “go with the flow” of evolu-

222. Yates, The Rosicrucian Enlightenment.

223. Archie P. Jones, “The Christian Roots of the War for Independence,” Journal
of Christian Reconstruction, IL (1976), pp. 6-51,; M. E. Bradford, “And God Defend
the Right: The American Revolution and the Limits of Christian Obedience,” Chris-
Hanity and Civilization, 2 (1983), pp. 233-41.
