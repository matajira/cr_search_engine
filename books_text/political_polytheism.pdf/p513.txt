10

“WE THE PEOPLE”: FROM
VASSAL TO SUZERAIN TO SERF

We the people of the United Siates, in Order to form a more perfect
Union, establish Justice, insure domestic Tranquility, provide for the
common Defence, promote the general Welfare, and secure the Blessings
of Liberty to ourselves and our Posterity, do ordain and establish this
Constitution for the United States of America.

Preamble, U.S. Constitution

How paradoxical that the first nation to base its political philosophy
on the principle that all political authority derives from the people, and
that the people express their will through elected representatives, should
also be the first to embrace the principle that the ultimate interpretation of
the validity of the popular will should be lodged not in the people them-
selves, or in their representatives, but in one non-elected and, therefore,
non-democratic branch of the government.

Henry Steele Commager (1977)!

In Chapter 7, I noted that Warren Burger, who served as Chief
Justice of the U.S. Supreme Court in the 1970’s and half of the
1980's, says that “We the people” are the Constitution’s most impor-
tant words.? He sent me a one-sentence reply when I questioned him
about the meaning of his statement, “They are the key words con-
cepiually.”® This gets right to the point.

At the time that I read his reply, I did not fully understand the
reason why his statement is correct. I had not yet recognized the ex-
traordinary construction of the Preamble: it precisely follows the

1. Henry Steele Commager, The Empire of Reason: How Europe Tmagined and America
Realized the Enlightenment (New York: Oxford University Press, 1977), p. 229,

2. Orlando Sentinel (Sept. 8, 1988), p. A-2.

3. Letter to author: Sept, 26, 1988. Emphasis his.

489
