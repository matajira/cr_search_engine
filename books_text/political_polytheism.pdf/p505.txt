From Coup to Revolution 481

The biblical view of the covenant oath is that only three institu-
tions can lawfully compel them: Church, State, and family, God has
authorized only these three monopolies as His covenantal organiza-
tions. By requiring self-maiedictory oaths for membership, Masonry
has set itself up as a rival Church and, in eighteenth-century France
and in late nineteenth-century Mexico, as a rival State. In the words
of Count Savioli (“Brutus”), a member of Weishaupt’s Illuminati in
the late eighteenth century: “The Order must possess the power of
life and death in consequence of our Oath; and with propriety, for
the same reason, and by the same right, that any government in the
world possesses it: For the Order comes in their place, making them
unnecessary.”!#?

5. Succession/Inheritance

Finally, we come to point five of the covenant: continuity or in-
heritance. Here is where politics enters the picture. Those inside the
organization are promised power outside the organization. Initiation
and continued membership are the basis of this inheritance. Those
who refuse to examine this “conspiratorial” side of secret societies
miss the whole point. Those who see Masonry as “clubbery” miss the
point. !23 Clubs are leisure-oriented. They are established for revelry
and companionship; secret societies are established to gain power.
The goal of the secret society is analogous to the goal stated by Psalm
37:9: “For evildoers shall be cut off. but those that wait upon the
Lorp, they shall inherit the earth.”

Who will exercise political power in a democracy or a republic?
Those who gain the support of those who can communicate with and
mobilize the party mechanisms, the media, and then the voters. It is
this aspect of Masonry that can be of crucial importance. Those who
have been sanctioned by the continuing brotherhood have a great
advantage in the transfer of political power.!?* The continuity of the
Masonic order provides a means of access to political continuity,
even though Masonry is officially nonpolitical. It was not nonpoliti-
cal in 1776 or 1789 in the colonies, and not nonpolitical in 1789 in
France.

122. Cited in John Robison, Prooft of a Conspiracy (4th cd.; New York: George
Forman, 1798), p. 170.

123. See, for example, Robert Micklus, “The Secret Fall of Masonry in Dr. Alex-
ander Hamilton's The History of the Tuesday Club,” in Deism, Masonry, and the Entighten-
ment, pp. 127-36.

124. Knight, The Brotherhood.
