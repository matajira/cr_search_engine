88 POLITICAL POLYTHEISM

The Three Views

Actually, there are three common views today regarding Chris-
tians and political participation: withdrawal, preservation, and re-
placement (separation, accommodation, and transformation). Pre-
millennial theology, especially dispensationalism, tended in practice
toward the first view until the late 1970's. Its proponents were con-
tent to suffer the older, milder forms of persecution and ridicule by
American political pluralism. Amillennialism and academic (his-
toric) premillennialism have both favored the second view: salt and
light in an age of permanent darkness. Its proponents have been self-
consciously favorable to pluralism, even when the political system is
authoritarian. They see no alternative except anarchy or outright
tyranny. Theocratic postmillennialism favors the third view. Its pro-
ponents are ready to use political pluralism until the day that Chris-
tians have sufficient votes and voluntary support from the voting
public to change the political rules. This is how the conspirators stole
the republic from colonial American Christians; the stolen goods can
be repossessed in exactly the same way.

The question remains; Why should Christians get involved in
politics? For many years in American history, especially after the
repeal of the eighteenth amendment (Prohibition), which had pro-
hibited the legal sale of alcoholic beverages, in 1933,°? fundamen-
talist Christians could not think of a good reason. They might vote
occasionally, but they insisted that “politics is a dirty business,” and
that it is a waste of time to seek political solutions to the problems
of this world. They did not vote as a self-conscious political inter-
est group, as they had in 1918, when the eighteenth amendment was
passed.

Defeat by Default

The problem with this attitude is that it leaves the political arena
under the domination of those who do believe that political action is
the way to solve the major problems of this world. Christians for dec-
ades did not understand that successful politics must be as much
negative as positive: getting the State out of the affairs of men. (Few
of them understand this today.) They did not devote themselves to
the study of politics in general or to the biblical laws governing civil

57. John Kobler, Ardent Spirits: The Rise and Fall of Prohibition (New York: Putnam's,
1973).
