166 POLITICAL POLYTHEISM

Egypt, but he could not stomach the judicial leaven of Moses. He
had to put up with manna.

A Closet Presbyterian Calvinist

Francis Schacffer (1912-1984) was a Calvinist. He was not a Puri-
tan Calvinist, but he was a Presbyterian Calvinist within the tradi-
tion of the old (pre-1929) Princeton Seminary.’ He was also a premil-
lennialist, although not a dispensationalist, which placed him closer
to the camp of post-Civil War American fundamentalism$ than to
traditional American Calvinism, which in the nineteenth century
was more commonly postmillennial* and in the twentieth century
has generally been amillennial.5 He was a pastor in Carl McIntire’s
Bible Presbyterian Church for two decades, which was explicitly a
premillennial church.

As a seminary student, he spent two years at J. Gresham Machen’s®
Westminster Theological Seminary in Philadelphia, where he studied
apologetics under Cornelius Van Til. He began his ministerial ca-
reer shortly after he joined fundamentalist-premillennialist-Calvinist
Carl McIntire’ in the 1937 split of the one-year-old Orthodox Pres-
byterian Church. He appears in the photograph of the departing
Bible Presbyterian Church leaders in A Brief History of the Bible Preshy-

2. Princeton’s faculty had been mostly postmillennial until the 1920’s, and those
who were nol posUmil were amillennialists, Also, the anti-alcohol (abstinence) posi-
tion of American fundamentalism, embraced by the young Schaeffer, was not prom-
inent in the old Princeton tradition. But Schaeffer’s commitment to biblical infalli-
bility and to an intelligent, well-informed evangelism was surely Princetonian.

3, Timothy P, Weber, Living in the Shadow of the Second Coming: American Premillen-
nialism, 1875-1925 (New York: Oxford University Press, 1979).

4. In the North, the postmillennialism of the Hodges and Princeton Seminary
dominated; in the South, the postmillennialism of Dabney and Thornwell: James B.
Jordan, “A Survey of Southern Presbyterian Millennial Views Before 1930,” Journal
of Christian Reconstruction, HY (Winter 1976-77), pp. 106-21.

5. This was the influence of Dutch Calvinism after the demise of orthodoxy at
Princeton after 1929. See Gary North, Dominion and Common Grace: The Biblical Basis
of Progress (Tyler, Texas: Tnstitute for Christian Economics, 1987), Appendix.

6. Pronounced GRESSum MAYchen.

7. Pronounced PERsonal conFROL.

8. George P, Hutchinson, The History Behind the Reformed Presbyterian Church, Evan-
gelical Synod (Cherry Hill, New Jerscy; Mack Publishing, 1974), p. 229n. The OPC
was at that time called the Presbyterian Church of America, but it was taken to civil
court and successfully sued by the mainline Presbyterian Church in the U.S.A. for
infringement on that church’s name. The new church was forced by the courts to
change its name in 1939. Charles G. Dennison (ed.), Orthodux Presbyterian Church 50,
1936-1986 (Philadelphia: Orthodox Presbyterian Church, 1986), p. 7.
