758

ideological synthesis, xxiii, xvi,
2O-xXi

ideology vs., 263

immigration, 186

judicial deferral, 267

legitimate?, 105

Masonic creed, 469

moral confusion, 266-67

moral crisis, 268

myth, 92

nationalism, 529

natural law &, 16n, 17, 660

Neuhaus, 100-01

neutrality, 179, 292, 651

Niebuhr, 234

noble lies, 540

Novak, 100

pacifism, 265

pay as you go philosophy, 29

permanent?, 224

pessimillennialism &, 158-60

philosophy of life, 158

polytheistic, 158 (see also
political pluralism)

problems, 84-85

procedural, 292

Protestant, 280

public schools, 660 (see also Mann)

public relations, $2

Puritans vs., 244

reigning myth, 85

Rhode Island, 315

sanctions, 121

sanctuary, 189

schizophrenia, 91

screeninig, 85, 190 -

sell-out, 15

Schaeffer on, 170, 174, 185-86

silent god, 102-3

sting, 658

Smith on, xvii

status quo, 124-25

stupidity &, 297

suicide of, 224, 293, 293, 568-69,
651, 657-58

Supreme Court vs., 651

taskmasters’ politics, 246

POLITICAL POLYTHEISM

temporary, 227
Tower of Babel, 85
vampire bat, 654
vague, xvii
virtue, 540
zombies, xxiii
political polytheism
2+2=4, 353
autonomy, 82
Bahnsen on, xii
Christian, 22
civil war, 91, 116-17
Constitution, 701
defined, 75-76
idols, 82
pluralism, 158
principled dizziness, 253
problems of (5), 64-85
Rushdoony on, 82-83
statism, 537
status quo?, 100
iaskmasters’ political theory, 537
theology of, 158
political philosophy, 78, 80-83
political rights, 75 (see also
civil rights)
political science, 366-67
politics
Articles era, 455-56
Christian, 583
consent, 249
covenantal ideal, 659
“dirty business,” 87
“fourth,” 559
god of, 528
ideological, 267
inheritance, 481
international, 650
irrational, 268
kingdom &, 254-55
moral struggle, 266
new theory needed, xxii
organization, 461
participation, 87
relative, 254
relativism, 454
religion &, 447
