Halfway Covenant Social Criticism 179

df Not Biblical Law, Then What?*\

It does not matter how many times a person assures us that he is
in favor of Christian civilization and opposed to the humanistic myth
of neutrality. If he does not affirm the continuing validity of the bibli-
cal case laws, his affirmation in favor of Christian civilization is in
vain, intellectually speaking.*? At some point, his denial of the con-
tinuing moral and judicial authority of God’s revealed law will logic-
ally force him to affirm some form of natural law theory or common-
ground reasoning, i.e., the myth of neutrality. He may prefer for
matters of personal taste to deny the myth of neutrality when writing
about what goes on inside the abortionist’s office, but if he enters the
classroom or the pulpit and publicly affirms democratic pluralism as
Christianity’s only temporal goal for society rather than as merely
a temporary and transitional historical phenomenon, then he is in
principle an ethical and epistemological humanist. He speaks with a
forked tongue. If he denies the continuing validity of the revealed
law of God, he is implicitly and operationally a humanist. Democratic
pluralism is his true religion, not biblical Christianity.

Franky Schaeffer denies the continuing law of God for society.
“Christians, of all people, do not want a theocracy. The idea of theocracy
denotes the lack of checks and balances.”*3 This statement, if taken
at face value, is a direct attack on God, for it was God who established
theocratic civil government in ancient Israel, as well as theocratic fam-
ily government and theocratic Church government. Was God’s law for
Israel inherently tyrannical because it was inherently “unbalanced”? **
Yet if Schaeffer’s statement is not taken at face value, it makes no sense
at all. Does he think that the concept of institutional checks and bal-
ances came from somewhere other than the Old Testament? Does he
think that the Greeks or Romans invented the idea of citizenship
which is protected by political checks and balances? If so, he ought
to read Fustel de Goulanges’ masterpiece, Zhe Ancient City (1864).

41, For the origin of this rhetorical phrase, see Franky Schaeffer, Bad News for
Modern Man: An Agenda for Christian Activism (Westchester, Illinois: Crossway, 1984),
pp. 54-55.

42. Gary North, Tools of Dominion: The Case Laws of Exodus (Vyler, Texas: Institute
for Christian Economics, 1989).

43. Schaeffer, Bad Naws, p. 104. He means “conaotes,” not “denotes.”

44. God is a Teinity, three yet one. Thus, biblical theocracy always has a division
of governmental powers: Church, State, and family, In each, the executive possesses
more authority than his advisors, but not exclusive authority. This same kind of bal-
ance cxisted in the theocracies of the Old Testament,
