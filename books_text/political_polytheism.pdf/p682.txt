658 POLITICAL POLYTHEISM

tate political covenants. In fact, the pluralist cannot successfully de-
fend the legitimacy of the pluralist order once the anti-pluralists have
persuaded the voters of the impossibility of maintaining a halfway
covenant political order. And pluralism, because it encourages open
debate and lots of “dialoguing,” opens the political door to its deadly
enemies. Pluralists have in principle left wide open the political door
to future Constitutional amendments, as they must do if they are to
remain true to their pluralist principles. Those of us who are theo-
nomic postmillennialists can then respond: “We will live peacefully
with this open-door policy for the present, but we do not intend to
live with it forever. Someday, we or our spiritual heirs will close that
door. We will vote it shut by popular acclamation.”

In short, we covenantal Christians can presently rejoice: O plu-
ralism, where is thy long-run sting?

Christian pluralists are hypnotized by their apostate enemies be-
cause their enemies have learned to play the game by mouthing plu-
ralist rhetoric even while they are substituting political oligarchy and
bureaucracy for pluralism. The theonomists refuse to play this rhe-
torical game—or in cases where they do play it, they play it
abominably —so they draw the wrath of the pluralists. “How dare
you people break with our rhetoric? Better to suffer tyranny at the
hands of bureaucratic humanists who say what we like to hear while
they are sending us into the cultural gulag rather than live under the
ideology of theocracy!” And so,-as always, substance progressively
triumphs over procedure in the name of procedure, The pluralists
never know what is happening to them. Even when they do, they
prefer it to living under God’s civil law. “And they met Moses and
Aaron, who stood in the way, as they came forth from Pharach: And
they said unto them, The Lorn look upon you, and judge; because
ye have made our savour to be abhorred in the eyes of Pharaoh, and
in the eyes of his servants, to put a sword in their hand to slay us”
(Ex. 5:20-21).

I realize that very few Christians believe this today. I am writing
for the handful who do. I am not writing a manifesto to be used in
today’s elections. I am writing a manifesto for the more distant
future. I realize that a Christian politician or activist who is living on
this side of the looming crises, and on this side of the great work of
the Holy Spirit, will probably prefer to disassociate himself from
these sentiments. I was in a meeting of Christian political activists in
the fall of 1987, During that meeting, a number of Arminian charis-
