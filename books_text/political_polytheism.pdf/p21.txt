Preface xxiii

Christianity. Without this international revival, men and nations will
go to war in the name of their respective creeds and non-negotiable
judicial demands. In either case, pluralism will at last be given a
public burial.

This book is a public autopsy of a dead system. Several opti-
mistic physicians are still hovering over the body, trying to breathe
new life into the corpse. They will not appreciate my report on their
patient's condition, for it points to their own monumental failure in
both diagnosis and treatment.

Ido not believe in zombies, but whenever I examine the philoso-
phy of political pluralism, I wonder if my prejudice can be sup-
ported, “It never sleeps, it walks, it crashes into things, yet it’s brain-
dead. Nothing has stopped it in eighteen hundred years. Unchained,
it has now gone berserk!” There are only two ways to stop a zombie
in the movies: smash its skull or decapitate it.

What Christian political pluralists steadfastly refuse to discuss is
what the new foundation of political pluralism can possibly be, now
that natural rights and natural law are intellectually dead concepts,
and very nearly dead culturally. First, Christian scholars baptized
Plato; then they baptized Aristotle; then they baptized Newton. Will
they also baptize Heisenberg, Sartre, and Camus? If so, we can then
expect the next public baptism: Nietzsche. What is the basis of the
“grand ideological synthesis” between Christianity and anti-Christi-
anity now that the very foundations of that ancient synthesis have
collapsed from the erosion produced by the acids of modernity?
They refuse to say, and their silence testifies to the intellectual bank-
ruptcy of their position.

Conclusion

In this book, I survey the writings of Christian scholars who have
been unwilling to face the implications of the death of natural law
theory, I use them as representative examples. What I argue is that
they have been intellectually schizophrenic, a fact revealed by their
own diagnosis of the modern religious crisis, They have remained
unwilling to choose between their commitment to the Bible and their
commitment to political pluralism. They refuse to cross over into
biblical covenant theology because of that great, immovable barrier
that separates the political religion of man from the civil covenantal-
ism of the Bible: biblical law. They prefer to dwell in the theocracy of
