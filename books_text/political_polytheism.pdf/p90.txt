66 POLITICAL POLYTHEISM

has no one ever told me about this before?” I have an answer: a con-
spiracy. Not simply a conspiracy of silence, which today unquestion-
ably exists, but a self-conscious, deeply religious, highly intellectual,
well-organized political conspiracy which gained a major victory
over Christian civilization almost exactly two centuries ago. This
conspiracy had previously been working for about a century to
achieve this major victory. We do not call it a.conspiracy because it
won. A successful conspiracy is like successful treason, as described
by John Harrington in 1613:

Treason doth never prosper; what’s the reason?
Why if it prosper, none dare call it treason.

Political Polytheism is an attempt to overcome this contemporary
conspiracy of silence. It is also a preliminary step in overcoming the
political results of the three-centuries-old original conspiracy. My
hope is that the spiritual and political heirs of this conspiracy will
take little notice of this fat book until it is way too late for them to do
much about it. This is one reason why I made it so fat. I hope those
outside the Reconstructionists’ paradigm shift will decide not to read
it until after the shift.

Time is rapidly running out, for one side or the other.

I want to make it clear from the beginning that this is a politically
nonpartisan book. It is a politically panpartisan book. All the politi-
cal parties on earth should proclaim the crown rights of King Jesus.
Every political platform should be governed by God's revealed law.
Jesus Christ is the Lord of politics, for He is Lord of the universe.
There is no “King’s X” anywhere in the universe from the absolute
authority of the King of Kings. “Whither shall I go from thy spirit?
or whither shall I flee from thy presence? If I ascend up into heaven,
thou art there: if I make my bed in hell, behold, thou art there” (Ps.
139:7-8). This pretty well covers al] the potential loopholes.

Sanctuary for Strangers

The ancient world was in spiritual darkness and bondage. There
was only one exception, one light on earth: the nation of Israel. In
Israel, there was civil liberty. Why? Because the Israelites were
under God’s liberating covenant.? They were also under the terms

2. Gary North, Liberating Planet Earth: An introduction to Biblical Blueprints (Ft, Worth,
Texas: Dominion Press, 1987).
