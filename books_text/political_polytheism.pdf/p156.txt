132 POLITICAL POLYTHEISM

Blowing Up the Cultural Dike

He was a Dutchman. He should have understood what he was
doing. He was like a demolitions expert who places explosives at the
base of a dike and triggers an explosion. This is what he did at the
base of the incomparably leaky dike of natural law theory. But Van
Til never publicly asked himself this crucial question: “After the dike
of natural law is shattered, what will take its place?” Even more to
the point, “Should I build a back-up dike before I blow up the exist-
ing one?” Van Til never offered any answers to these crucial ques-
tions. He had no recommendations regarding Church or civil legal
standards. He apparently believed that it was not his job to provide
such answers, as if natural law philosophy had not stood for almost
two millennia as virtually the sole foundation of Christian political
theory. All he ever wanted to do academically was blow up theologi-
cally inconsistent leaky dikes. He built no restraining wall. The only
reliable materials available for building such a wall are the biblical
case laws, found mostly in the Old Testament, and Van Til refused *
to use them. But the pagan sea of ethical nihilism always threatens to
rush in with full force to carry away the last traces of Christian civili-
zation. (This was also my criticism of Free University of Amsterdam
professor A. Troost back in 1967.)1#

Van Til taught that there is a radical distinction between cove-
nant-keeping man’s concept of culture and covenant-breaking man’s
concept. He insisted that there must be a specifically Christian goal,
standard, and motivation for culture. The goal is the glory of God.
The standard is the way of Christ. The motivation is the discovery. of
God's grace in culture.!* But he offered no explicitly biblical defini-
tion of this standard, which would also make it impossible for cove-
nant-keepers to assess when they have found God's grace in culture.
As I shall argue throughout this essay, Van Til’s antinomianism (no
explicitly biblical standard for culture) was the fraternal twin brother
of his pessimism regarding Christianity’s earthly influence in the
fature (amillennialisrm).

I think his theological supporters instinctively recognize the risk
in such a demolition operation, yet they are also hostile to biblical
law, so he has never recruited many disciples. They know that he is

18. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler, Texas:
Institute for Christian Economics, 1986), Appendix C: “Social Antinomianism.”
19. Van Til, Essays on Christian Education, pp. 35-38.
