598 POLITIGAL POLYTHEISM

not written this book for their benefit; they will not read a book as
long as this book is. I am writing for those who take seriously this
book’s covenantal theology, but who may be confused about where
to begin a program of applied covenantalism.

The way to gain a Trinitarian national covenant is first to suc-
ceed visibly and undeniably in the other areas of life. Men must see
the positive fruits of Christianity before they commit themselves to
its negative sanctions. They must see, Deuteronomy 4:5-8 says, that
biblical justice—and not just biblical civil justice—is the best avail-
able alternative to tyranny and misery.

There are three covenantal institutions: Church, family, and
State. Therefore, I propose preliminary reforms in these three insti-
tutions based on the major area of theological confusion and rebel-
lion during the Protestant era: the question of sanctions, I begin
with the Church because the Church is God’s model for the other
two institutions. It extends into eternity; the other two do not. God
begins bringing His historical sanctions, positive and negative, to
those who are formally covenanted with Him ecclesiastically. “For
the time is come that judgment must begin at the house of God: and
if it first begin at us, what shall the end be of them that obey not the
gospel of God?” (I Pet. 4:17).

Church Sanctions

This is the area of the sacraments. Church discipline must be im-
posed in terms of the sacraments of baptism and the Lord’s Supper.
Because the churches have not taken seriously baptism, the Lord’s
Supper, membership rolls, and éxcommunication for well over two
centuries, the civil government has had no clear model. When the
churches do start taking these things seriously, persecution and
lawsuits against Church leaders will begin. Our enemies who now
control civil government will instinctively perceive the threat to their
power, and they will take steps to stop it, They cannot safely allow
the Church to begin imposing sanctions against the sins of this age.

This is not the place to debate baptism. Baptism is the rite by
which the dual judicial sanctions of the covenant—blessings and
cursings— are imposed on people: either directly on adults or repre-
sentatively on children through a baptized parent. This representa-
tion principle is valid. Just as the centurion’s servant was healed
through the centurion’s faith in Jesus Christ and his willingness to
submit to Christ publicly, even though the servant was absent and
