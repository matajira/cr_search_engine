464 POLITICAL POLYTHRISM

outcast of Puritan New England in the 1640's and 1650’s.)** The peo-
ple of the colonial era recognized that an oath to God and an affirma-
tion of the authority of the Bible were basic to the preservation of
Ghristian social order, political freedom, and economic prosperity.
What the colonists did not fully understand is that the God-given
function of civil government is inherently negative: to impose sanc-
tions against public evil. It is not the function of civil government to
use coercively obtained tax money in order to promote supposedly
positive causes. By using tax revenues to finance specific denomina-
tions, the state governments created ecclesiastical monopolies. This
was a catastrophic error—one shared by the whole Western world
from the beginning of the West. This error could have been solved
by the Constitution’s refusal to subsidize churches with direct eco-
nomic grants of any kind; instead, the Constitution created a secular
humanist, anti-Christian republic in the name of religious freedom.
It was the legitimate hostile reaction of the various non-established
churches to this misuse of tax revenues that created the alliance be-
tween the Deists-Masons and the dissenting churches. Church histo-
rian Sidney Mead has described this situation well: “. . . the struggles
for religious freedom during the Jast quarter of the eighteenth cen-
tury provided the kind of practical issue on which rationalists and
sectarian-pietists could and did unite, in spite of underlying theolog-
ical differences, in opposition to ‘right wing’ traditionalists.”°> The
creation of “positive” economic support of specified ecclesiastical
groups led politically to the Constitutional destruction of the ex-
plicitly Trinitarian judicial foundations of the United States. The
federal example reminded men that national leaders were not
bound by any Trinitarian oath. Why should state officers be simi-
larly bound? The symbol of the oath was real; the covenantal exam-
ple could not be ignored. The Deists who wrote this provision into
the Constitution fully understood this; their opponents were not
equally alert. A century of Newtonian rationalism and an ancient

64. Irwin H, Polishook, Rhode Island and the Union, 1774-1795 (Evanston, Illinois:
Northwestern University Press, 1969). Cf, Patrick T, Conley, “First in War, Last in
Peace: Rhode Island and the Constitution, 1786-1790,” in Patrick T. Conley and
John P. Kaminski (eds.), The Constitution and the States: The Rote of the Original Thirteen
in the Framing of the Federal Constitution (Madison, Wisconsin: Madison House, 1988,
ch. 13.

65. Sidney Mead, “American Protestantism During the Revolutionary Epoch,” in
Religion in American History, edited by John M. Mulder and John F. Wilson (Engle-
wood Cliffs, New Jersey: Prentice-Hall, 1978), pp. 165-66.
