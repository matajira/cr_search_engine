448 POLITICAL POLYTHEISM

speaking, in order to reduce the economic dependence of the Church
on the State— Madison wanted to remove from civil government all
sources of political dependence on Christianity. In his Memorial and
Remonstrance of 1785, written against the move of Governor Patrick
Henry and the legislature to provide limited state aid to churches
{not to any one church), he wrote: “During almost fifteen cen-
turies, has the legal éstablishment of Christianity been on trial.
What have been its fruits? More or less in all places, pride and in-
dolence in the Clergy; ignorance and servility in the laity; in both,
superstition, bigotry and persecution,”!* He continued in this vein:

What influence in fact have ecclesiastical establishments had on Civil
Society? In some instances they have been scen to erect a spiritual tyranny on
the ruins of Civil authority; in many instances they have been seen upholding
the thrones of political tyranny; in no instance have they been seen the guard-
ians of the liberties of the people. Rulers who wished to subvert the public lib-
erty, may have found an established clergy convenient auxiliaries. A just gov-
ernment, instituted to secure & perpetuate it, needs them nat,”

What is interesting is his appeal to the biblical principle of sanc-
tuary or asylum, but dressed in new secular garb: “Because the pro-
posed establishment is a departure from that generous policy, which,
offering an asylum to the persecuted and oppressed of every Nation
and Religion, promised a lustre to our country, and an accession to
the number of its citizens.”1® He equated asylum with a religiously
neutral State, ignoring the truth of the Old Testament’s example: it
is only when a civil government is explicitly God-honoring, and
when it screens those from public office who refuse to place them-
selves under God's covenant oath as His servants, that the sanctuary
can be maintained.

Nature's God or Nature Is God?
Madison called all state-established religion an Inquisition in

15, Robert Douthat Meade, Patrick Henry: Practical Revolutionary (New York: Lip-
pincott, 1969), p. 280. Henry had opposed the pre-Revolutionary Anglican
Church’s position which prohibited the free exercise of worship by other Christian
faiths. As a lawyer, he had opposed Edmund Pendleton’s active civil persecution of
non-conforming churches. Norine Dickson Campbell, Patrick Henry: Patriot and
Statesman (Old Greenwich, Connecticut: Devini-Adair, 1969), pp. 100-1.

16, Mind of the Founder, p, 12.

17, fbid., p. 13.

18. Zdem.
