4 POLITICAL POLYTHEISM

day. Each side is required by its respective coyenantal head to seek a
victory over the other side in every area of life. There is no neutrality.

Christians, however, for the most part no longer believe that con-
tinual conflict between mutually exclusive covenantal standards is in
fact inevitable in the civil realm, so they have signed a temporary
cease-fire which they hope will last until the final judgment. They
hope and even occasionally pray that this will be a permanent cease-
fire in history. They have voluntarily surrendered the fundamental
principle of the sovereignty of God, as revealed by His covenantal
law, as this law and its specified sanctions apply in the civil sphere.
(Incredibly, this is frequently done in the name of the sovereignty of
God.’ These people are masters of deception.) Having surrendered
the doctrine of the sovereignty of God in civil government, they are
now in the process of surrendering it in the first two areas, family
and Church, piece by piece. The rise of “Christian” counselling
courses in our Bible-believing seminaries—courses based on pagan
psychology texts and principles — is one example, and the practice of
registering churches with the government in order to receive tax-
subsidized mailing permits is another. “Peace, peace” in fact means
“surrender, Christians, piece by piece.”

The Abandonment of Casuistry

Political Polytheism deals with the process of piecemeal intellectual
surrender. It will not win me academic friends or influence tenured
people. Books by watchmen never do. It presents a case-by-case
study of what I regard as representative intellectual compromises
with humanism by contemporary Christian scholars. These intellec-
tual compromises within the English-speaking Calvinist community
have been going on for over three centuries, and within the rest of
Christendom for at least eighteen centuries. From time to time in re-
cent years, it has looked as though this systematic program of intellec-
tual compromise was about to cease in some key Christian commando
units, but each time, senior officers have put down their battle flags
and have signed cease-fire agreements with the enemy. This is dis-
couraging for those of us who remain on the battlefield.

7. Cf. Gary Scott Smith, Introduction, in Smith (ed.), God and Politics: Four Views
on the Reformation of Civil Government (Phillipsburg, New Jersey: Presbyterian & Reformed,
1989), pp. 2, 12.
