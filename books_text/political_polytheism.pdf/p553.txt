Conclusion, Part 3 529

ratify it: the American People. This is not a universal god; it is a na-
tional god. This national god can neither offer nor defend any uni-
versal rights of man, It can only offer power to the national State,
with derivative power in the states. The national State becomes the
sole definer and guarantor of American rights, which today means
five people on the U.S, Supreme Court.

Farewell to Christendom

Washington's Farewell Address of 1796 (a newspaper article, not
an actual verbal address) reflected a major change in the thinking of
Americans. A new nationalism had already appeared. Washington’s
address merely ratified this shift in popular thinking. There must be
no covenants with other nations, Washington said. He did not use
the words, “no entangling alliances,” but this is what he meant. He
thereby announced the end of the older Puritan vision of Trinitarian
universalism, the kingdom of God on earth. There can be no cove-
nanted community of nations in a world marked by nation-states
created by exclusively national democratic gods. The new demo-
cratic nationalism destroyed the covenantal foundation of Chris-
tendom when it removed the covenantal foundation of Trinitarian
national covenants.

There is no neutrality. There are two kingdoms in history. Both
kingdoms seek to establish covenantal connections. Satan’s kingdom
is an empire: a top-down, centralized, bureaucratic system. In-
itiative is at the top. God’s kingdom is a bottom-up, decentralized,
appeals court system. Initiative is at the bottom. In God’s kingdom,
Christian localism is supposed to lead also to Christian regionalism,
to Christian nationalism, and finally to Christian internationalism,
just as it was supposed to do in Old Covenant Israel."* Israel failed
in internationalizing God’s kingdom, so God gave the kingdom to a
new nation, the Church International (Matt. 21:42-43). Christian
civil governments are supposed to imitate the churches, and the
churches are not to remain the tiny, fragmented, isolated institutions
that Madisonian political nominalism and extreme denominational
confessionalism have made them. Like the Trinity who created it,
the international Church of Jesus Christ is to be both one—a unity
based on Athanasian confessionalism—and many: traditional de-
nominational practices and confessions, The problem is, the

14. North, Healer of the Nations.
