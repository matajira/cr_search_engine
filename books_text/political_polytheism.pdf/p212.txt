188 POLITICAL POLYTHEISM

tant disease: Anglo-Saxon anti-Catholicism. This is appalling. . . .”°”

What Schaeffer may have meant is conjectural. What we do have
here is the case of a professional historian with a clouded memory for
dates and facts. The great migration to the United States did indeed
begin in 1848, and it accelerated with the development of the steam-
ship in the 1870's. It was not just the Irish famine that swelled the
seaboard cities of the East. What does not seem to have occurred to
Professor Wells is that 1848 was also the year of the socialist revolu-
tions of Continental Europe, the year of Marx’s Communist Manifesto.
Marx and Engels celebrated these revolutions, and were saddened
that they did not triumph and did not go far enough in their radical-
ism.° In the wake of the defeat of those revolutions by conservative
forces, several thousand humanist, socialist, and anti-Christian
political refugees streamed into the United States, accompanied by
three quarters of a million peasants, not all Irish, who had also de-
cided that it was time for a change.5® Few of the immigrants were
revolutionaries; most were Northern Europeans. But 1848 is the
obvious year to date the beginning of the change in the religious
make-up of the American voter.® The year 1854 was the peak year
for immigration until 1873, with over 400,000 immigrants entering
the U.S.* Historian Marcus Lee Hansen calls the 1850’s “The Great
Migration.”®? Schaeffer was right on target historically. Professor
Wells ignores all this, preferring instead to tar and feather Schaeffer
posthumously with the accusation of his possible anti-Irish Know-
Nothingism.** My response to him is what he said of Schaeffer:
“This is appalling.” Judiciousness with regard to historical facts is not

57. Wells, “Schaeffer on America,” Reflections, p. 236.

58. Karl Marx, The Class Struggles in France, 1848-1850 (1850), in Kaxl Marx and
Frederick Engels, Collected Works (New York: International Publishers, 1978), vol.
10, pp. 48-145; Engels, Tivo Years of a Revolution; 1848 and 1849 (1850), in tbid., pp.
353-69. Cf. Joan Sigmann, 1848: ‘The Komantic and Democratic Revolutions in Europe
(New York: Harper & Row, [1970] 1973); Frank Eyck (ed.), The Revolutions of
1846-49 (New York: Barnes & Noble, 1972).

59. Marcus Lee Hansen, The Atlantic Migration, 1607-1860 (New York: Harper
Torchbooks, [1940] 1961), p. 274.

60. Zbid., ch. 12,

61. Ibid., p. 303.

62. Tbid., ch. 33.

63. The American Party, a minor, anti-immigration political party of the 1850's,
was known as the Know-Nothing Party, for whenever questioned about it, its mem-
bers insisted that they knew nothing, Its lack of importance is teslified to by the lack
of modern monographs on it written by tenure-hungry historians,
