558 POLITICAL POLYTHEISM

the beginning. The third point of the covenant still would have to be
dealt with: What about God’s revealed civil law? Will the whole
Bible be established as the final law of the land, the common law of
the nation? Point four: What about a national oath required of all
citizens and all civil magistrates? Will people who are not under eccle-
siastical covenant sanctions be allowed to serve as civil magistrates or
agents who sanction civil magistrates? Fifth point: What about a sys-
tem of adoption: the “naturalization” (meaning de-naturalization)! of
future citizens? Will all immigrants be allowed to take the national
and ecclesiastical Trinitarian oaths, thereby becoming full citizens?
Will the nation have open geographical borders, but closed political
borders?

In short, while it is certainly important to preach the necessity of
re-establishing the abandoned Trinitarian state confessions of the
colonial American period, this is not enough. We need to renew the
original civil covenant, this time on a national basis. God’s covenant
is comprehensive. We have already seen what happens to halfway
national covenants. We do not need a replay of the last two cen-
turies. We need a full covenant, not a halfway covenant. Newton’s
god was buried by the Framers in 1787; there is no need to seek to
resurrect him.

This does not mean that the civil magistrate is required by God
to launch a wave of religious persecutions. What it means is that the
long-term goal of Christians should be the preaching of the compre-
hensive gospel of salvation, including the supernatural healing of all
institutions. Christians should also pray for and expect a huge re-
vival, so that a vast majority of Americans will convert to saving
faith in Jesus Christ. If this future postmillennial revival does net

 

take place, then any attempt to establish a national covenant will
fail, long-term. It is not our job as Christians to ram religion down
everyone’s throat. We must recognize that if postmillennialism is
wrong, then the pursuit of the national covenant really is utopian.
Worse; it would require massive coercion or deception. We dare not
imitate the deceptive strategy of James Madison and his national
covenant-breaking accomplices, We also dare not be premature, as
Cromwell was.

1. “But the natural man receiveth not the things of the Spirit of God: for they are
foolishness unto him: neither can he know them, because they are spiritually dis-
cerned” (I Cor. 2:14).
