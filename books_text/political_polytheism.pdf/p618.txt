594 POLITICAL POLYTHEISM

Athanasian Pluralism

What I am talking about in this book is a republic in which legal
access to the franchise and seats of elected and appointed political
authority are open only to those who take a Trinitarian civil oath of
allegiance and who are also communicant members in good stand-
ing in churches that profess a Trinitarian creed. I call this Athanasian
pluralism. The basic covenant issue is the Trinity. The earliest
Church creeds will do quite well as judicial screening devices, at
least for the first few centuries of the millennium. It is not my opin-
ion that in a system of Athanasian pluralism, in the words of House
and Ice, “heresy is apparently transformed from a punishable crime
into ‘treason.’”3” Apparently? Apparent to whom? What they are do-
ing here is superimposing their view of the coming premillennial bu-
reaucratic kingdom on top of Reconstruction’s blueprint for a theo-
cratic republic. This sort of swift justice is how they expect Jesus to
run things from his throne in Jerusalem. It is our view that uttering
heretical opinions—yes, even theological opinions as utterly wrong-
headed as those that House and Ice proclaim**— would not be civil
crimes at all. The civil government would have nothing to say about
it. Those who say that Christian Reconstructionists would “kill any-
one who is not a Reconstructionist” are blowing smoke. They are
bearing false witness against their neighbors. Where are their foot-
notes proving such an accusation? There are none.

What Christian Reconstructionists are saying is that the mark of
judicial sovereignty in a Christian civilization will be membership in
a Trinitarian church. The issue here is civil sanctions. Those not for-
mally under God's eternal sanctions—the ecclesiastical marks of
baptism and regular holy communion—in a Trinitarian society
would not be biblically authorized to impose God’s negative civil
sanctions. Everyone else would have the same civil liberties guaran-
teed to strangers within the gates in Old Covenant Israel, beginning
with equality before the civil law (Ex. 12:49), Those Christians who
believe that Old Covenant Israel was a tyranny whenever biblical
law was being honored will have to argue about this with God on
judgment day.

37. House and Ice, Dominion Theology, p. 79.
38. See Kenneth L. Gentry and Greg L. Bahnsen, House Divided: The Break-Up of
Dispensational Theology (Tyler, Texas: Institute for Christian Economics, 1989).
