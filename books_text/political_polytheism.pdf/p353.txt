The Theological Origins of the U.S. Constitution 329

nomic freedom. Virtue was therefore instrumental for them —a means
of achieving social stability and progress, liberty and security.*”

I contend in this book that this was also their view of religion. In
this, they were not fundamentally different in principle from
Robespierre, who established a formal civic religion of nature and
reason in the midst of the Terror in 1794. De-Christianization was
morally debilitating, Robespierre conchided; it had to be followed
by the establishment of a new civic religion. He knew that men
needed to believe in God’s sanctions in order to keep them obedient.
‘Talmon identifies this impulse as “cosmic pragmatism.”*9 The major
figures among the Framers were wiser men than Robespierre, more
influenced by traditional Christianity, but they were Enlightenment
men to the core; their veneer and their constituencies were different
from those of the French Revolutionaries, but not their theology.
Their religion was civic religion. The difference is, they saw civic re-
ligion as a decentralized, individual matter rather than as a State
affair; it was to aid the national government but not be part of the
national government. Unitarian John Adams wrote in his autobio-
graphy, presumably for himself and not the electorate:

One great advantage of the Christian religion is that it brings the great
principle of the law of nature and nations, Love your neighbour as yourself,
and do to others as you would that others should do to you, to the know!-
edge, belief and veneration of the whole people. Children, servants, women
and men are all professors in the science of public as well as private moral-
ity, No other institution for education, no kind of political discipline, could
diffuse this kind of necessary information, so universally among all ranks
and descriptions of citizens. The duties and rights of the man and the citi-
zen are thus taught, from early infancy to every creature. The sanctions of
a future life are thus added to the observance of civil and political as well as
domestic and private duties. Prudence, justice, temperance and fortitude,
are thus taught to be the means and conditions of future as well as present
happiness.

Not a word about the atonement; not a word about the sacraments.
The whole passage is geared to the needs of public morality. The

47, Ibid., pp. 72-73.

48. R.R. Palmer, Tiwelve Who Ruled: The Year of the Terror in the French Revolution
(Princeton, New Jersey: Princeton University Press, 1941), pp. 323-26.

49. J.L. Talmon, The Origins of Totalitarian Democracy (New York: Praeger, 1960),
p. 148.

50. L. H, Butterfield, ef al., (eds.), The Diary and Autobiography of John Adams, 4
vols. (Gambridge, Massachusetts: Harvard University Press, 1962), IL, pp. 240-41.
