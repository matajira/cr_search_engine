3
WINNERS AND LOSERS IN HISTORY

A Psalm of David. The Lord said unto my Lord, Sit thou at my
right hand, until I make thine eriemies thy footstool. The Lorp shall
send the rod of thy strength out of Zion: rule thou in the midst of thine
enemies. Thy people shail be willing in the day of thy power, in the
beauties of holiness from the womb of the morning: thou hast the dew of
thy youth (Ps. 10:1-3).

There shall be no more thence an infant of days, nor an old man that
hath not filled his days: for the child shall die an hundred pears old; but
the sinner being an hundred years old shall be accursed (Isa. 65:20).

For he must reign, till he hath put all enemies under his feet. The
last enemy that shall be destroyed is death. For he hath put all things
under his feet. But when he saith all things are put under him, tt is man-
ifest that he is excepted, which did put all things under him, And when
all things shall be subdued unto him, then shall the Son also himself be
subject unto him that pui all things under him, that God may be all in
all (I Cor. 15:25-29),

The Bible is clear: Jesus Christ sits at God’s right hand until the

final judgment, at which time He will leave the throne, return in
glory, and end the curse of bodily death. Premillennialism denies
this; Jesus is supposed to leave the throne in heaven to reign on earth
for a thousand years—a discontinuity not taught in Psalm 110,
Second, before the final judgment takes place, the world will experi-
ence increased life spans far beyond what is common today. This ex-
tension of life has to take place before the final judgment, since there
are still sinners operating in history (Isa. 65:20). Thus, there has to
be a literal era of earthly blessings ahead of us before Jesus returns to
earth at the final judgment: a continuity marked by God’s visible

605
