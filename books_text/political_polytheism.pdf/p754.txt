730 POLITICAL POLYTHEISM

Berger, Raoul, 395
Berman, Harold, 485, 578, 624
bestiality, xvii
Beveridge, Albert, 412
Bible
ambiguous?, 229-232, 537
blueprints, 615
civilization, 230
demon’s work?, 580-81
justice &, 537
law of land, 558
Masonic, 423, 687
Paine on, 580
pluralism vs., 93
self-authenticating, 92
see also Word of God
Bible Presbyterian Church, 166-67,
616
biblical law
abandoned (18th c.), 356
barrier to theocracy, xxiii
boundaries, 40
center of Israel, 103-4
center of society, 630-31
Church ignores, 538, 542, 579,
591, 662
confidence &, 458
coyenantal, 43
dominion &, 576-77
evangelism tool, xiii, 31, 164,
244, 631-34
fear of, 615
gospel &, 90
heart, 521
Holy Spirit, 151
impotent?, 52
international, 89-90
irrelevant (civil)?, 515
last choice, 632
liberty, 66-67
mark of victory, xiii
maturity &, 649
merciful, 581
moral law, 48
necessity of, 666
New Testament, 582-83
plural legal authorities, 576

progress, 230, 575
representation, 542, 649
resident aliens, 69, 72, 75
responsibility, 38, 662
restitution, 647
Roman Church, 603
tool of dominion, 76, 158-59, 636

(see also dominion)
tyrannical?, 537
Witherspoon vs., 403
work of, 31
see also case laws

biblical principles, 223-24

Bicentennial Commission, 571

Bill of Rights, 381, 496

Bishop of London, 525-26

black regiment, 527

Blackstone, William, 321-23, 377-78,
429, 469, 525, 682-83

blank check, 224

blasphemy, 269, 272

blueprints
Constitution, 452
covenantal, 104, 641
culture &, 640-41
denial of, 47-48, 54, 234, 289, 291,

297, 615, 640, 667, 673
“final,” 667, 673
historical guide, 232, 297
Marsden vs., 297, 667, 673
politics &, 291
Puritans, 234
reconstruction with, 651
responsibility &, 612
revival &, 611-12
tools of dominion, 615
utopian?, 54
worldwide adoption of, 612

Body of Liberties, 237, 242, 287, 314,
643

Bork, Robert, 562

bondage, 63, 75-76

Boston tea party, 434-35

boundaries
borders, 71
covenant, 71
ethical, 40
