176 POLITICAL POLYTHEISM

as Forrest Baird concludes, that “Schaeffer was heavily influenced by
Van Til.”*5 When, toward the end of his career, his son Franky quite
properly radicalized him regarding the ethical issue of legalized
abortion, he began to move from his strictly intellectual critique of
humanist civilization into social activism. He wrote The Great Evan-
gelical Disaster (Crossway, 1983), an attack on the social and political
apathy of American evangelical churches. This alienated many of
his followers. A year later, he died of cancer.**

Neither he nor his son Franky ever resolved the immense contra-
diction in their attempt to create an intellectual defense of Christian
activism while simultaneously denying biblical law and postmillen-
nialism, and also while staying discreetly quiet about the institu-
tional Church and the sacraments. ‘The father died in the summer of
1984 without suggesting any resolution to the theological dilemma
that his writings had created. Eventually, this contradiction over-
whelmed the son. In the fall of 1984, Franky cancelled all his future
Christian audience speaking engagements, and he disbanded his
own Christian Activist tabloid newspapcr a few months later. He sold
the tabloid’s mailing list and then disappeared from the evangelical
scene in order to produce an atrocious R-rated, teenage violence
motion picture, Wired to Kill, that was released in 1987 and flopped
financially, fortunately, because. almost nobody went to see it. The
film received scathing reviews by Christian film critics for its need-
less bloodshed and its lack of any Christian theme. By the grace of
God, it was a total economic failure in theaters, and the investors
lost their money.?” Christian film critic Ted Baehr wrote: “So, in the
end, this is a humanistic film about humanistic despair, showing no

35. Baird, “Schaeffer's Intellectual Roots,” Reflections, p. 64.

36. I met Schaeffer twice, the first time in late 1963 or early 1964 in the den of
Rev. Richard Gray in Pennsylvania, where he discussed his ministry with a few
seminarians, and again in late 1982 at a private meeting at Pat Robertson’s Virginia
Beach headquarters. Rev. Robertson had invited me, Schaeffer, Franky, and lawyer
John Whitehead to advise him regarding programs that CBN University could
launch. I wrote a paper for him which was later published as “Levers, Fulcrums,
and Hornets,” Christianity and Civilization, 3 (1983), pp. 401-31. As far as I know, Rev.
Robertson never adopted any of our suggestions. Rev. Falwell did; his Liberty Uni-
versity’s videotape-based home college education program was developed alter Fal-
well's assistant, Ron Godwin, read the essay and adopted its educational strategy.

37. T knaw. I was one of them. In retrospect, it is understandable why Franky
asked us to invest without reading the screenplay in advance. It is not fully under-
standable why we were all so foolish as to invest, screenplay unread. In any case, far
better that we lost our money than to have had lots of people see this disastrous movie.

 

 
