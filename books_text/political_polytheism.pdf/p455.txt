The Strategy of Deception 431

Masters.83 Edmund Randolph was also a major Masonic figure in
Virginia and a major figure at the convention, but he did not sign
the document because of doubts, although he later supported its rati-
fication at the Virginia ratifying convention. He had been a former
military Aide-de-Camp for Washington, and he had been the official
who signed the charter documents that created Alexandria Lodge
No. 39, later No. 22, when Washington, as its first or Charter
Master, served as Grand Master.**

Does lodge membership of several prominent nationalists prove
my thesis regarding the Constitutional Convention as a Masonic
coup? No, because men on both sides of the Constitutional debate
were found in the lodges, just as evangelical Christians today are in
the lodges, despite two centuries of protest from the historic Reformed
churches and traditional dispensational leaders.*5 Daniel Shays
seems to have been a Mason, yet it was his rebellion in Massachu-
setts that so frightencd the nationalists.°° What has to be considered
in assessing the accuracy of my thesis regarding the Convention is
the theological character of the Constitution itself. Was the Constitu-
tion a civil covenant modeled along the lines of Masonic theology?
Was it closer to the Masonic ideal than the existing state constitu-
tions were? In other words, were the terms of judicial and political dis-
course shaped by the Masonic worldview? It is my contention that
Masonry did shape the terms of discourse, translating the near-
impersonal mathematical providentialism of Newton’s Creator into
the language of the average man. The Mason’s Grand Architect of
the Universe was in fact the Newtonian Deity.

83. Bedford (Delaware), Blair (Virginia), Brearley (New Jersey, but in 1806),
Franklin (Pennsylvania), Washington (Virginia, but in 1788).

84. Heaton, Masonic Membership, pp. 56, 74.

85. Christian Reformed Church, “Report 37: Lodge and Church Membership,”
Acts of Synod 1974 (Grand Rapids, Michigan: Board of Publications of the Christian
Reformed Church, 1974), pp. 504-67; Lutheran Church, Missouri Synod, Masonry
in the Light of the Bible (St. Louis, Missouri: Concordia, 1964); Alva J. McLain,
“Freemasonry and Christianity,” reprinted in E. M. Storms, Should a Christian Be a
Mason? (Fletcher, North Garolina: New Puritan Library, n.d.). From a Roman
Gatholic perspective, sec Paul A. Fisher, Behind the Lodge Door: Church, State and Free-
masonty (Washington, D.G.: Shield, 1988). A very easy to read yet well documented
introduction to this lopic is John Ankerberg and John Weldon, Christianity and the
Secret Teachings of the Masonic Lodge: What Goes on Behind Closed Doors (Chattanooga,
Tennessee: John Ankerberg Evangelistic Association, 1989)

86. Carter, Masonry in Texas, p. 58.

 
