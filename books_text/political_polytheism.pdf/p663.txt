Conclusion 639

Even in the coming millennium, they insist, this Old Testament,
“Jewish kingdom” law-order will not in and of itself produce appro-
priate sanctions; only Jesus’ physical presence in Jerusalem will pro-
duce them. Sanctions then, unlike today, will be swift.”7

Kingdom and Case Laws

Rarely in the history of the Church have leaders or laymen taken
the Old Testament case laws seriously. Christians have assumed that
Jesus’ ministry, or at least Paul’s, did away entirely with the case laws.
Nevertheless, when pressed to defend some traditional practice of any
particular denomination, the group’s in-house theologians turn to the
Old Testament in search of a legal precedent. This is an aspect of what
Rushdoony has called smorgasbord religion; selectively picking what you
like out of a large selection of rules and doctrines. The best example
of such New Testament selective shopping is the strict sabbatarian’s
appeal to all but one verse in the Old Testament requiring God’s
people to keep holy the sabbath. The exception is Exodus 35:2, the
verse to which the capital sanction is attached: “Six days shall work
be done, but on the seventh day there shall be to you an holy day, a
sabbath of rest to the Lorp: whosoever doeth work therein shall be
put to death.” When it comes to the imposition of this most rigorous
of Old Testament civil sanctions, capital punishment, the Church
flees in holy terror. “Keep the sabbath holy, but not that holy!”

A required sanction clearly identifies God’s attitude toward a
particular infraction. The sanction tells us just how important the
infraction is in the overall operation of the kingdom of God. Without
sanctions, there can be no civil law, and without civil law there can
be no civilization, meaning no identifiable kingdom. But there is
always some form of civilization. There are no historical vacuums.
There are therefore no political or judicial vacuums. Thus, we ought
to conclude that God has required His people to declare His re-
quired civil sanctions, while self-proclaimed autonomous man has a
different set of civil laws and sanctions. God has revealed His re-
quired sanctions in His Bible-revealed law, self-proclaimed autono-
mous man has revealed his required sanctions in his voluminous
legislation. For as long as there are infractions of a judicial standard,
there will be sanctions, The question is: Whose? Whose standards
and whose sanctions?

27. Sce footnote 29, Chapter 12, p. 586.
