Sanctuary and Suffrage 89

justice. (Few of them do today.) They did not learn that the judicial
goal. of the civil government is overwhelmingly negative: prohibiting morally
evil public acts rather than coercing individuals or organizations into
doing acts of positive good. Thus, they were out of office when those
who believe in political salvation salvation by law—came into
power and reshaped the American republic. They could not halt or
even slow down the engine of secular humanism, Christians have
become third-class citizens precisely because they have for too long
believed that politics is a questionable activity for Christians.

We are still residents in the world, of course, living in the midst
of covenant-breakers. We cannot escape history. But Christians are
inevitably at war throughout history with a rival kingdom which
secks to overcome us, as representatives of God’s kingdom. Christ
has already definitively overcome this rival kingdom. “These things I
have spoken unto you, that in me ye might have peace. In the world
ye shall have tribulation: but be of good cheer; I have overcome the
world” (John 16:33). We shail progressively overcome it, too, as the
representatives of Jesus Christ in history.5* Only then will history
end: “Then cometh the end, when he shall have delivered up the
kingdom to God, even the Father; when he shall have put down all
rule and all authority and power. For he must reign, till he hath put
all enemies under his feet. The last enemy that shall be destroyed is
death” (I Cor. 15:24-26).

If we represent God in history, how can we represent Jesus
Christ in any capacity other than as definitive conquerors who are
progressively working out the implications of this conquest in his-
tory? To do less, we inescapably deny representationaily His defini-
tive victory over sin at Calvary, and His elevation to the right hand
of God at the ascension. This is what the early Church believed. It
should be the modern Church’s vision, too.

God’s Law and God’s Kingdom

The visible manifestation of God’s kingdom in history involves
the progressive extension of His law and gospel over all the earth,
century by century. Whai ancient Israel was supposed to reveal for
foreigners to see within its national boundaries, New Covenant civi-

58. Roderick Campbell, israel and the New Covenant (Phillipsburg, New Jersey:
Presbyterian & Reformed, [1954] 1981).

59, David M. Hay, Glory at the Right Hand: Psalm 110 in Early Christianity (Nash-
ville, Tennessee: Abingdon, 1973).
