Conclusion 629

not God the Judge of perversion in history.” When these people
speak of God the Creator, they do not mean the God who created the
world in six literal days; they mean a hypothetical god of their imagi-
nation who does not bring full-scale covenant judgments in history.
“God the Creator” in the theology of ethical dualism and natural law
means.“God the Stand-Patter.” This is the God of the rescue mission,
with free meals, hot showers, and clean sheets for burned-out Chris-
tian scholars holding advanced academic degrees. They would, to a
man, rewrite Genesis 25, for they have already done so in their own
tenured lives:

And Esau sod pottage: and Jacob came from the field, and he was faint:
And Jacob said to Esau, Feed me, I pray thee, with that same wheat and
barley pottage; for I am faint: therefore was his name called Wheaton. And
Esau said, Sell me this day thy birthright. And Jacob said, Behold, I am at
the point to die: and what profit shall this birthright do to me? And Esau
said, Swear to me this day; and he sware unto him: and he sold his birth-
right unto Esau. Then Esau gave Jacob bread and pottage of lentiles; and
he did eat and drink, and rose up, and went his way: thus Jacob despised
his birthright (Gen. 25:29-34; King Saul Edition).

The pietist declares, “God saves souls, not cultures. God judges
souls in eternity, not cultures in history. And even if He may occa-
sionally judge a culture, we Christians must attribute this to the
forces of history,” i.e., the latest fad theory of historical change that
has captured the historical guild. Any theory of historical causation
is open for discussion by Christians, except one: Gad’s providence.
Any theory of civil government is open for discussion by Christians,
except one: God’s theocracy. These two opinions are linked together,
and the common thread is a rejection of Eusebius and Constantine,
for Constantine, the head of civil government in Rome, publicly
placed an entire civilization under the sanctions of God's civil cove-
nant, as did his successors (except Julian the apostate, a two-year
retrogression who failed). This was a bad, bad precedent in the eyes
of pietists, whether they are fundamentalist political conservatives or
tenured neo-evangelical political liberals. “No creed but the New
Testament pastoral epistles and no law but love!”

This same discount kingdom syndrome existed in David's day,
too. Its promoters infuriated David. He cried out to God: “I hate the
double-minded, but I love your law” (Ps. 119:113, New King James
Version).
