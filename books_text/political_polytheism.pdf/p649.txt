CONCLUSION

There is no neutrality.
You can't beat something with nothing.

We see a conflict going on today in every area of life. It is a war
over first principles. It is ultimately a war over the covenant. Whose
covenant will individuals afirm, God’s or Satan’s? Being covenantal,
this conflict also necessarily involves the allegiance of three corporate
coyenantal institutions: Church, State, and family. Which covenant
will these God-authorized corporate monopolies affirm, God’s or
Satan’s? Which covenant will be the source of law for them?

The humanist has made his choice against God’s covenant. He
has made this choice personally, and he sees to it that all those cove-
nantal institutions in which he has authority have also made the
same choice. The problem today is that Christians refuse to make
this choice. They pretend that such a choice is unnecessary to their
salvation, even uncalled for biblically. They refuse to think in terms
of the five points of the covenant. They want Jesus as Savior but not
as Lord, meaning covenantal Lord.! They think they can take part of
Jesus’ office and leave the rest. They think they are not under the full
covenantal obligations of God when they accept Jesus Christ as their
savior. They do not really believe that God imposes His covenantal
standards on professed Christians to test the validity of their public
confession. They say that they believe in the Book of Galatians, the
book that frees Christians from the law of God. “Galatians, Gala-
tians,” they cry, conveniently forgetting Paul's warning in Galatians:

Now the works of the flesh are manifest, which are these; adultery,
fornication, uncleanness, lasciviousness, idolatry, witchcraft, hatred,
variance, emulations, wrath, strife, seditions, heresies, envyings, murders,

1, For a defense of the Lordship of Christ, see John MacArthur, Jr., The Gospel
According to Jesus (Grand Rapids, Michigan: Zondervan Academie, 1988).

625
