94 POLITICAL POLYTHEISM

the State, the bureaucratization of the churches, freedom of con-
science, etc. But then another problem raises its head: What about
State-authorized personal income tax deductions for gifts to
churches? The economies of the two examples seem to be the same:
the allocation of personal funds for State-approved religious pur-
poses. In the first case, the State compels the transfer; in the second
case, the State in some economic sense subsidizes the transfer by
refraining from taxing the money so transferred. The transfer is en-
couraged by means of either a negative sanction (taxation) or a
positive sanction (tax-deductibility).

Are the cases economically the same? Yes. The individual has less
money after the transfer, and the State does not collect this money for
its treasury. Are they judicially the same? This is a question which very
few Christians are prepared to answer by means of a well-developed
political theory.

Tf we do not distinguish between the rival concepts of the State as
an agent of positive compulsory good and the State as an agent of positive
voluntary good, we will find it difficult to distinguish the two cases
politically. If the State can lawfully, morally compel a specific right-
eous action, then the State becomes in most people’s eyes the domi-
nant agent of social reform in society.*” No other agency has the per-
manent power of physical compulsion. (A parent exercises some
minimal physical sanctions over minors, but this power ends at some
legal point in the child’s life.) The State can force people to do what
it wants.

If the State is limited to prohibiting evil acts (which would also
include public health measures against non-human organic in-
vaders), then the most it can do to promote a particular social good
is to restrain itself by refusing to confiscate an individual’s assets. It
does not tell the individual what to do with his money. It only agrees
to take less of thé person’s assets if he does certain State-approved
things with the money. In other words, rather than being a restraint
on the individual, as is the case with State-enforced compulsory tith-
ing, tax deductions on charitable gifts are in fact a politically imposed

67. In fact, the Church is the dominant agency of social reform, for it presents
the heavenly model of the eternal covenant between God and man. It alone provides
the sacrament of the Lord’s Supper, which is God’s common meal with redeemed
mankind. But this fact of the Church’s predominant position must be accepted on
faith. It is much more difficult to prove this by an appeal to historical records, once
the view of the State as the positive reform agent becomes widespread in a society,
and the bureaucrats start collecting money and issuing orders.
