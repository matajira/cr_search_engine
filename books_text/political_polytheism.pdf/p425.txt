Renewed Covenant or Broken Covenant? 401

will of the people. But could this be considered both necessary and
sufficient in late eighteenth-century American life? Would there not
also have to be an appeal to God?

There was no escape, There had to be an appeal to God. This
was what Hume sensed, and he forthrightly rejected all traces of
theism in his political theory. Locke had known better, At the end of
his Second Treatise, he invoked the name of God. He did so when he
raised the question of sanctions. We can see here his attempted fu-
sion between Christianity and natural law theory. It was an attemp-
ted fusion that has dominated Christian political theory down to our
own era. He raised the question of the right of political rebellion, the
dissolution of the compact.

Here, it is like, the common question will be made: Who is to judge
whether the prince or legislative act contrary to their trust? This, perhaps,
ill-affected and factious men may spread among the people, when the
prince only makes use of his due prerogative. To this I reply: the people
shall be judge. . . . But further, this question, Who shall be the judge? can-
not mean that there is no judge at all; for where there is no judicature on
earth to decide controversies among men, God in heaven is Judge. He
alone, itis true, is Judge of the right. But every man is judge for himself, as
in all other cases, so in this, whether another has put himself into a state of
war with him, and whether he should appeal to the Supreme Judge, as
Jephthah did.7*

So, there was some degree of transcendence in Locke's system.
But he invoked the name of an undefined God rather than an earthly
hierarchy in formal covenant with a specific God. He placed man as
a sovereign agent acting directly under God. There is no hierar-
chical chain of command, no hierarchy of temporal appeal, no doc-
trine of defined representation, in Locke’s concept —a limiting concept or
convenient theoretical backdrop— ofa theocratic covenant. How is
God to enforce His transcendent covenant in the midst of history?
Directly or mediatorially through specific judicial institutions? That
was the question Locke needed to answer. He did not cven attempt
to do so.

Rousseau and Darwin
Rousseau’s concept of political legitimacy was strictly immanent,
In his system, there is no transcendent Sovereign who enforces the

78. Second ‘Treatise, paragraphs 240, 241. Here I am using Sherman's cdition.
