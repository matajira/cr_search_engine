542 POLITICAL POLYTHFISM

theocratic Christians. The Whig liberals, in reaction to the Puritan
Revolution of 1640-60, successfully ridiculed the churches and sects
on this basis: surely they could not all have represented God. But the
same accusation can be made against the critics today: surely not all
the claimants to the office of Official Spokesman are accurately rep-
resenting the sovereign People. When it comes to numbers of
claimants, in fact, the humanists today are far more numerous than
theocratic claimants who say they are the voice of the sovereign God
of the Bible. In this day and age, Christians are almost completely
politically humbled; they are terrified of the thought they might in
fact really be God’s lawful designated authorities in speaking for God
in the realm of civil law. They do not even want to think about the
possibility that God’s revealed laws in the Bible are God’s required
standards for modern jurisprudence. They do not want to bring
God’s covenant lawsuit against any nation. They have been steadily
browbeaten on this point since at least 1660.

The Religion of Procedure

Contractualism is evolutionary when honored and revolutionary
when transcended. It is an empty ethical shell. Lenin once remarked
about making omelettes, that you have to break a lot of eggs. If there
are no ethical standards inside the contractual shells, then we should
expect to see a lot of broken shells as time goes by, as people continue
their search for righteous civil government.

There is no sovereign God in contractualism who will judge the
righteousness of men’s contracts, in time or eternity. Man is officially
on his own. Thus, there is only procedure. In cases of civil dispute,
the only question is: Which ofthe parties best honored the formal
terms of the contract, meaning the letter of the contract? This means
the triumph of fine print and the lawyers who alone can interpret it. To
the extent that questions of ethics enter into the judge’s decision —
substantive questions —the result is judicial arbitrariness. Such judi-
cial arbitrariness erodes the very foundation and justification of con-
tractualism: procedural predictability. This creates an intellectual at-
mosphere favorable to revolution. Every would-be spokesman for
the People wants to be sure that his version of god’s word is enforced.
The inherent, inevitable dualisrn or dialecticism between formal
procedure and ethics,?” between the letter of humanist law and the

37. Gary North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Gage,” in Gary North (ed.), Foundations of Christian Scholarship: Essays in the Van ‘Til
Perspective (Vallecito, California: Ross House Books, 1976), ch. 8.
