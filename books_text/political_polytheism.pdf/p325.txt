CONCLUSION, PART 2

The Christian is to resist the sptrit of the world. But when we say this,
we must understand that the world-spirit does not always take the same
form, So the Christian must resist the spirit of the world in the form it
takes in his own generation. Jf he does not do this, he is not resisting
the spirit of the world at all. This is especially so for our generation, as
the forces ai work against us are of such a total nature.

Francis Schaeffer (1968)!

The spirit of this world has for almost two millennia clouded
Christian philosophy by means of the doctrine of natural law. The
Church has adopted variations of an intensely pagan philosophy
based on neutrality, universal categories of reason, and the Greek
idea of salvation through knowledge: “know thyself.” It has overlaid
these doctrines on top of Paul’s discussion of universal categories of
ethics in the human heart:

For there is no respect of persons with God. For as many as have sinned
without law shall also perish without law: and as many as have sinned in the
law shall be judged by the law; (For not the hearers of the law are just be-
fore God, but the doers of the law shall be justified. For when the Gentiles,
which have not the law, do by nature the things contained in the law, these,
having not the law, are a law unto themselves: Which shew the work of the
law written in their hearts, their conscience also bearing witness, and their
thoughts the mean while accusing or else excusing one another;) In the day
when God shall judge the secrets of men by Jesus Christ according to my
gospel (Rom. 2:11-16).

What Paul taught was this: all men have been given sufficient in-
ternal revelation of God—the image of God in man—to condemn

1. The God Who Is There (1968); in The Complete Works of Francis A. Schaeffer, 5 vols.
(Westchester, Illinois: Westchester, 1982), I, p. 11.

301
