family, 71
judicial, 71
Rome's, 80
Schaeffer on, 183
Bradford, M. E., 416, 420, 427
Bradford, William, 447, 565
brainwashing by humanists, xx
Breen, T. H., 255
Brinton, Crane, 432, 437
Brown, Harold O. J., 405-6, 654,
655
Brown, Judy, 204
Bruno, Giordano, 349
“Brutus,” 481, 508-9, 518-19
Bryan, William Jennings, 239
Buel, Richard, 419
bureaucracy, 366, 585, 587, 658
Burger, Warren
Bicentennial Commission, 570-71
code of silence, 417
judicial review, 503, 513
Philadelphia, 311
“We the people,” 375, 489
Burgess, John W., 520
Burke, Edmund, 678-79, 696
burned feet, xix
Burr, Aaron, 275, 318n, 320
Bush, George, 634, 687
buzz saw, 135

Calhoun, John C., 101
Caligula, 79
Calvin, John, 50, 285-86, 599, 693n
Calvin Gollege, 5n, 190, 191, 238
Calvinism
affirmed sanctions, 642
American Revolution, 36
capital punishment, 642-43
colonial, xv-xvi
compromise, 4-5
Dutch, 19, 42, 59, 136, 153-55, 616
eighteenth century, 515
errors, 108
fundamentalist mentality, 616
Framers’ rejection of, 406
ghetto mentality, 616

Index 731

Great Awakening, 360
guilt, 515
halfway covenant, 107-9
holy commonwealth ideal, 108
moralism, 515
opponents, 626
pietistic, 19
post-1660, 19
rejection of, 633n-634n (see also
Arminianism)
Schaeffer, 218
Unitarianism replaces, 515
U.S., 161
see also Puritans
Campbell, Roderick, 618
campus riots, 211-12
Canaan, 75n, 115
Canada, 661
Cantwell vs. Connecticut, 392
capitalism, 177-78
Carolinas, 271-72
Carter, James, 423, 429-30, 466
Carter, Jimmy (President), 210
case laws
“anti-Christian,” 657
blueprints, 667
chief offense: sanctions, 673
Christian civ ition, 179
conservative voting, 282
Harvard Law School, 563
ignored, 21
kingdom &, 639-40
natural law vs., 179
Puritans adopt, 255, 657
radical, 228
rejection of, 229, 581, 667, 673
restitution, 647
revolution &, 578
see also biblical law
casuistry
decline of, 5-7, 109
revival of, 575-76
Supreme Court's, 503, 510
catch-22, 244
Cato's Letters, 325, 500
cause & effect
Bible & civilization, 230

 

 
