The Strategy of Deception 425

Christian responsibilities. He did possess a personal prayer book,
written in his own hand, which he called Daily Sacrifice. It contained
familiar formal set prayers, such as this one: “I beseech Thee, my
sins, remove them from Thy presence, as far as the east is from the
west, and accept of me for the merits of Thy Son Jesus Christ.”9°
Similar Trinitarian prayers are published in the AAiman Rezon, the
constitutional handbook for Ancient Masons. He perhaps was a
“closet Trinitarian” in the way that John Locke was. Publicly, he was
a Masonic Unitarian. Of him it can legitimately be said, as Mark
Noll in fact says: “In short, the political figures who read the Bible in
private rarely, if ever, betrayed that acquaintance to the public.”°”
In contrast to Washington’s public silence stands thé example of
Patrick Henry. A member of the same Protestant Episcopal Church,
he took regular communion. While he was governor of Virginia, he
had printed at his own expense Soame Jenyns’ View of the Internal Evi-
dence of Christianity and an edition of Butler’s Analogy. These books he
gave to skeptics he would meet.5* He never joined the Masonic fra-
ternity. He wrote to his daughter in 1796: “Amongst other strange
things said of me, I hear it said by the deists that I am one of their
number; and, indeed, that some good people think I am no Christian.
This thought gives me more pain than the appelation of Tory; . . .”59

Benjamin Franklin

In order to modify the argument that Franklin was a Deist, Rush-
doony cites Franklin’s June 28 plea at the Constitutional Convention
that they pray to God in order to resolve their differences. Then,
speaking of Jefferson and Franklin, he writes: “That both these men

55. Cited in Benjamin Hart, Faith & Freedom: The Christian Roots of American Liberty
(Dallas, Texas: Lewis & Stanley, 1988), p. 274. Seven of these set prayers are reprinted
in Tim LaHaye, Faith of Our Founding Fathers (Brentwood, Tennessee: Wolgemuth &
Hyait, 1987), pp. 111-13. LaHaye cites W. Herbert Burk, Washington's Prayers (Norris-
town, Pennsylvania: Published for the Benefit of the Washington Memorial Chapel,
1907).

56, Ahiman Recon Abridged and Digested (Philadelphia: Hall & Sellers, 1783), pp.
111-12.

87. Mark A. Noll, “The Bible in Revolutionary America,’ in James Turner
Johnson (ed.), The Bible in American Lave, Politics, and Political Rhetoric (Philadelphia:
Fortress Press, 1985), p, 43.

58. Moses Coit ‘Lyler, Patrick Henry (New Rochelle, New York: Arlington House,
[1887] 1975), pp. 392-95. Henry adopted this practice toward the end of his life. Henry
Mayer, A Son of Thunder: Patrick Henry and the American Republic (New York: Franklin
Waits, 1986), pp. 467-68.

59. Ibid., p. 392.
