152 POLITICAL: POLYTHEISM

those segments will eventually lose influence, money, and power.
Their place will be taken by those Christian churches that obey
God’s laws, and that will therefore experience the covenant’s exter-
nal blessings. These churches will spread the gospel more effectively
as a result. This is the positive feedback aspect of biblical law.

Again, let me stress that I am not saying that the external success
of God’s civilization automatically or necessarily converts sinners zn-
wardly to faith in the saving work of Jesus Christ. Men are not saved
by law or by the historical sanctions of the law. What I am saying is
that the defiant sinner in the millennial future will reject the gospel
in the face of even more visible testimony to the historical benefits of
saving faith. He will suffer even greater quantities of eternal coal on
his resurrected head (Rom. 12:20).

Kline attacked both of Bahnsen’s recommended doctrines — bib-
lical law and postmillennialism — in his critique of Theonomy.® Kline
rejects the idea of a New Testament covenantal law-order, and he
also rejects postmillennialism. Kline and his fellow amillennialists are
consistent in their rejection of both biblical law and postmillennialism.

Postmillennialists should be equally consistent in linking the two
positions. We must argue covenantally, and this necessarily involves
the question of the positive feedback of external covenantal blessings
and the Church’s empowering by the Holy Spirit. If we accept the
possibility of a defense of God’s law that rejects the historic
inevitability of the long-term cultural expansion of Christian domin-
ion through the covenant’s positive feedback, then we face a major
problem, the one Bahnsen’s theory of the empowering by the Spirit
has raised: how to explain the difference between the New Testament Church
and Old Testament Israel. If the Christian Church fails to build the visi-
ble kingdom (Christian civilization) by means of biblical law and the
power of the gospel, despite the resurrection of Christ and the pres-
ence of the Holy Spirit, then what kind of religion are we preaching?
Why is the Church a significant improvement culturally and socially
over Old Testament Israel?

What does such a theology say about the gospel? What kind of
power does the gospel offer men for the overcoming of the effects of
sin in history? Is Satan’s one-time success in tempting Adam never
going to be overcome in history? Will Satan attempt to comfort him-
self throughout eternity with the thought that by defeating Adam, he

89. Kline, op. cit.
