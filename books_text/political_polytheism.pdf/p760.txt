736 POLITICAL POLYTITEISM

discontinuity with Christianity, 683

discontinuity with past, 516

dualism, 692-93

evolutionary, 50L

“Father of” 428

five points, 374

fundamental law, 506, 510-11

God's name absent, 495

golden calf, 566

Great Architect, 367

hijacking, 570-71

historiography, 312

humanist, 410

idol worship, 701

“immanentized,” 402

implicit oath, 440

Joseph Smith on, 334-35

judicial review, 394-97, 512-13

keys (3), 386

“Jag.” 397

legitimacy, 386, 400-401, 506

legitimacy (loss of), 506-7

Masonic worldview &, 431, 704

myth of, 311

national citizen, 388-89

nationalism, 496

natural rights, 528-29, 562

neutral?, 681

new document planned, 569-71

few covenant, 383, 407, 491,
654-55, 701

new god, 383, 407, 528-29, 531,
654-55, 701

new theory, 497-98

Newtonianisin &, 368

not exportable, 324

oath, 385-92, 402, 507, 692, 687
(see also test oath)

officers, 386-91

open-ended, 568-69

People (see People)

polytheism, 701

Preamble, 374-75, 386, 489-90,
501, 517

“procedural manual,” 675

protection?, 396, 561-62, 660

ratification (see ratification)

Rhode Island, 380, 386, 535

rights, 529

roots of, 535

Rousseau &, 388, 402, 451

Rushdoony on, Appendix B

safeguards, 499

sanctions, 402, 687-88

Schaff on, 551

Scott on, 675-76

secular, 382, 464, 686

separation of powers, 361-82

“sloganized history,” 674

sovereign, 402

structure, 535

test oath, 311, 379, 383, 410, 692,
694 (see also test oath)

text prohibits Christianity, 692

theological character, 431

timeless?, 363-64

toleration, 465

treaty of king (People), 700

tyranny, 675

unique, 676

Unitarian, 630

unpopular (1787), 412

“wall of separation,” 410

worship of, 655

contingency (see chance)
contract theory

Anglo-Saxon, 293n

covenant &, 493-95

Enlightenment’s two views, 398-99,
539-40

evolutionary, 542

God's name, 493

historical content (Locke), 398-99

mythical, 540-41

no ethics, 542

revolutionary, 542

see also compact theory

contractualism, 341, 540, 542 Gee also

compact theory)

Convention

attendees, 412, 415
authority, 412, 492, 499, 520
authorization, 413-14, 492
broken covenant, 492
