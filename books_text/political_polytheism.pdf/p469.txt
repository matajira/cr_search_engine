From Coup te Revolution 445

replacements of Parliament in order to award new kings their lawful
executive authority: Charles If in 1660 and William III in 1689.2
Writing of these two English precedents, he observes:

But the idea of an elected convention that would express enduring popular
will in fundamental constitutions superior to government was a viable way
of making popular creation and limitation of government believable. It was
fictional, for it ascribed to one set of elected representatives meeting in con-
vention a more popular character, and consequently a greater authority,
than every subsequent set of representatives meeting as a legislature. But it
was not too fictional to be believed and not so literal as to endanger the
effectiveness of government. It never came into use in England, but it was
reinvented in the American Revolution,

The term “convention” was also used by the revolutionaries in
France in September of 1792 to launch the radical phase of the Revo-
lution. R. R. Palmer writes: “It was called a convention from the
precedent of constitutional conventions in the United States.”*
Under this Convention four months later, Louis XVI was beheaded.
This was surely a transfer of executive power. It led to the rise of a
new executive: Robespicrre. The Convention then wrote a new con-
stitution, later called the stillborn constitution of 1793.5 The centrali-
zation of power in Paris escalated under this new constitution. To
accomplish this, the Jacobins imitated Madison’s tactic: they had the
constitution ratified by plebiscite.*

Madison planned an initial coup—the convention’s immediate
scrapping of the Articles—to be followed by a plebiscite. The
plebiscite, as the voice of the People, would consotidate and sanction
the coup. Thus, a bloodless revolution could be achieved—a revolu-
tion in national sovereignty, testified 1o by a change in judicial oaths.
Had there been no alteration of the oath structure, there would have
been no revolution.

2. Edmund 8. Morgan, Inventing the People: The Rise of Popular Sovereignty in England
and America (New York: Norton, 1988), pp. 94-95, 107-21

3. iid., p. 91.

4. R.R. Palmer, Ticeloe Who Ruled: The Year of the Terror in the French Revolution
(Princeton, New Jerscy: Princeton University Press, 1941), pp. 20-21.

5. Leo Gershoy, The Hench Revolution and Napoleon (New York: Appleton-
Century-Grofts, 1933), p. 258.

6. Ihid., p. 258.
