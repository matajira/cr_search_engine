396 POLITICAL POLYTHEISM

liberalism won out over Lockean liberalism. In 1973, the Supreme
Court determined that lives in the womb are not under this protec-
tion because of a Court-invented Constitutional guarantee of privacy:
woman and physician. State civil sanctions could no longer be brought
against this class of murderers who had successfully conspired to
deprive another person of life. Post-Darwinian liberalism won
again, Human life can now be legally sacrificed on the altar of con-
venience. The hope of the Framers—to place judicial limits on the
worst decisions of the legislature — did not succeed, although this fact
took a century and a half to become clear to everyone, If anything,
the Court, insulated from direct public opinion, proved in 1973 that
it was the worse offender.

A Political Judiciary

The procedural limits of the Constitution proved to be no safe-
guard from the substantive apostasy of the humanists who have
dominated politics in the twentieth century. The Lockean liberals of
1787 designed a system that was neither substantively nor pro-
cedurally immune to the Darwinian liberals of the twentieth century.
Whig liberalism won in 1788, and its spiritual heir is still winning to-
day. Constitutional procedure has revealed itself to be as morally
“neutral” as humanism’s ethics is, ie., not at all.°9 It sometimes takes
longer for procedure to respond to the shifting moral and political
winds, although in the case of the Warren Court, procedure shifted
more rapidly than politics did. It was not, after all, the U.S. Con-
gress that forced integration of the public schools of Topeka, Kansas,
and therefore the nation, in 1954.”

Darwinian jurist Oliver Wendell Holmes, Jr., who later served
on the U.S. Supreme Court, began his 1881 lectures on the common
Jaw with this observation: “The life of the law has not been logic: it
has been experience. The felt necessities of the time, the prevalent

 

by President Reagan was rejected in 1988, by a vote along party lines, 8 to 6, by the
Senate Judiciary Committee. Of 340 previous nominations by President Reagan,
only one had been successfully blocked by the Judiciary Committee; two others-—
both conservatives~had also been opposed by the committee, but the final vote
went to the whole Senate, where one was defeated, Robert Bork. “Panel Rejects
Reagan Gourt Nominee,” New York Times (July 15, 1988).

58. In the United States, the death penalty is exclusively a state sanction, except
in the case of treason within the military.

59. See Appendix B: “Rushdoony on the Constitution.”

60, Brown v. Board of Education of Topeka, Kansas (1954).
