364 POLITICAL POLYTHEISM

tion was intended to be a perfect system, “as infallible as any mathematical
calculations,”!9* Secure in their scientific faith, Federalists waxed cuphoric
on the superiority of the new Constitution; it was, as one said, the “best
form of government that has ever been offered to the world.”!95 Whereas
other schemes had fallen into corruption and decline, a perpetually bal-
anced federal Constitytion seemed capable of continuing forever. With it,
predicted an admiring Robert Davidson, the American states “shall resem-
ble, the Solar System, where every obedient planet moves on its proper
path, — never seeking to fly from, nor even approaching the great attractive
orb, than the wise author of nature intended.”!96 The federal Constitution
was created to apply equally to every age, never running down, wearing
out, or falling into disrepair. As far as these Federalist writers were con-
cerned, the new republic could continue in this perfect state forever —“a
system,” Barlow rhapsodized, “which will stand the test of ages.”!97
Throughout the debates, Federalists would continue to argue that the
Constitution was a theoretically perfect instrument. As the state conven-
tions drew on, however, they came to admit the cold hard truth so often
propounded by the Antifederalists— that the Constitution, however excel-
lent in theory, might well be flawed in practice, Equally important, they re-
alized that the case for ratification could be strengthened by embracing the
Antifederalist demand for an amendment procedure. Thus, in Federalist
rhetoric, “experience” began to undergo one final change, from experience

 

as scientific truth to experience as scientific experimentation.%

This appeal to experience was no deviation from Newtonianism.
Newton had admitted that God must occasionally reimpose His will
on a declining, friction-bound cosmic order. The universe is not a
perfect autonomous cosmic clock. Thus, the revised view of those
who defended this “modern” view of the Constitution was really con-
sistent with Newtonianism. Lienesch does not make this clear in his

194. The Friends of Union and Just Government, An Address to the Citizens of Penn-
syloania (Philadelphia: n.p., [1789]; Elizur Goodrich, D.D., The Principles of Civil
Union and Happiness. considered and Recommended. A Sermon... (Harlford, Con:
Hudson and Goodwin, 1787), p. 8; Hamilton, address to the New York Conven-
tion, 28 June 1788, in [Elliot's] Debates, II, p. 366.

195. Simeon Baldwin, An Oration Pronounced Before the Citizens of New-Haven, July 4,
1788... (New Haven, Conn.: J. Meigs, 1788), p. 13.

196. Robert Davidson, D.D., An Oration on the Independence of the United States of
America. . . (Carlisle, Pa, Kline and Reynolds (1787)), p. 15.

197. Joel Barlow, An Ovation, Delivered in the North Church in Hartford... July 4,
1787... (Hartford, Gonn.; Hudson aad Goodwin, [1787)), p. 19.

198. Michael Liencach, New Order of the Ages: Time, the Constitution, and the Making
of Modern American Political Thought (Princeton, New Jersey: Princeton University
Press, 1988), pp. 134-35.

 

 

 
