528 POLITICAL POLYTHEISM

covenant was sealed. Rush’s confidence in the wisdom of this act
began to waver within a year;!! Witherspoon’s never did. After July
4, 1776, it was then just a matter of extending the apostate principles
of the original halfway covenant into a full-scale apostate covenant.

The New Nationalism

The language of the laws of nature and nature’s god disappeared
from the Constitution. So did the twin doctrines of natural law and
natural rights. Historian Carl Becker wrote in 1922: “In the Declara-
tion the foundation of the United States is indissolubly associated
with a theory of politics, a philosophy of human rights which is valid,
if at all, not for Americans only, but for all men. This association
gives the Declaration its perennial interest.”!? Yet a few pages later he
noted, almost as an aside, that these ideas disappeared in nineteenth-
century constitutions. Natural rights are absent, he said, “even
where we should perhaps most expect it, in the Constitution of the
United States... .”'8

On the contrary, if my theory of apostate covenantalism is correct,
this is exactly where we should not expect it. When the American na-
tion moved from biblical covenantalism to halfway covenantalism, it
remained open to a universal god, though Newtonian-Unitarian.
Article VI, Clause 3 of the Constitution closed the door judicially to
any transcendent god beyond the political order itself. The Constitu-
tion is therefore an apostate covenant; a wholly new god is ordained
in it, a god acknowledged by the Framers in order to ordain it and

11, Writes Rush’s biographer, David Freeman Hawke: Rush “had banked on the
Declaration to bring ahout a real revolution in America—a purified people march-
ing as one in a glorious crusade while the world looked on. A year with the reality of
independence had darkened the dream. Rush still hoped for a revolution in the
hearts of the people, still dreamed the war would introduce ‘among us the same
temperance in pleasure, the same modesty ini dress, the same justice in business,
and the same veneration for the name of the Deity which distinguished our
ancestors.’ But by the summer of 1777 hopes were tarnished with doubts, and he saw
‘a gloomy cloud hanging over our states.’ He once feared Tories would subvert the
cause; now he saw the corrosiveness of internal danger. ‘If we are undone at all,’ he
said in early August, ‘it must be by the aristocratic, the mercenary, the persecuting,
and the arbitrary spirit of our own people—I mean the people who are called
Whigs.” Hawke, Benjamin Rush: Revolutionary Gadfy (Indianapolis, Indiana: Bobbs-
Merrill, 1971), p. 203.

12, Carl L. Becker, The Declaration of Independence: A Study in the Histary of Political
Ideas (New York: Vintage, [1922] 1942), p. 225

13. Tbid., p. 234.
