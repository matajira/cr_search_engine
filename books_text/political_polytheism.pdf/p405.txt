Renewed Covenani or Broken Covenant? 381

The Structure of National Sovereignty

The Constitution officially divides national judicial spokes-
manship into three branches: legislative, executive, and judicial.
Each of these is a separate juridical sphere. Each has its own section
in the document itself. For a law (piece of legislation) to be binding,
all three branches must agree.

Originally, this was not clear to the Framers. They believed that
the agreement of the executive and the legislature would be suffi-
cient. They divided the legislative branch into two sections, House
of Representatives and Senate. Very little was said of the judicial
branch. It was assumed that it would be by far the weakest of the
three. Alexander Hamilton went so far as to say that “the judiciary is
beyond comparison the weakest of the three departments of power,”
and assured his readers that “it can never attack with success either
of the other two; and that all possible care is requisite to enable it to
defend itself against their attacks.”** The Framers did not recognize
that he who interprets the law authoritatively is in fact the true voice of sover-
eign majesty. They also did not fully understand that the implicitly
vast powers of political centralization that the Constitution created
on a national level would lead to the creation of a new hierarchy.
The federal (national) government would steadily swallow up subor-
dinate jurisdictions. Why? Because in any covenant, there must be a
hierarchy, and the pinnacle of that hierarchy is the agent who pos-
sesses the authority to announce the law and therefore sanctify the
law’s sanctions.

So, there was initial confusion over hierarchy and representa-
tion, point two of the biblical covenant model. This had been the
great political debate immediately prior to the Revolution: Which
body had legitimate legislative sovereignty in the colonies, the Eng-
lish Parliament or the colonial legislatures? This was also the heart
of the political debate over the Bill of Rights, the first ten amend-
ments to the Constitution. The voters, as represented by state ratify-
ing conventions in 1788, had insisted on retaining numerous powers
in the states. Any power not expressly transferred to the central gov-
ernment automatically resides in the states (Amendment 10). Thus,
the debate became one of states’ rights vs. national power.

23, Hamilton, Federalist No. 78: The Federalist, edited by Jacob E. Cooke (Mid-
dletown, Connecticut: Wesleyan University Press, 1961), p. 523.
