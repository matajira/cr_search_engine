A New National Covenant 571

Perhaps it is not so confusing after all. Following a tip given to
me, I had a year earlier predicted Burger's decision to resign in ex-
change for control of the Bicentennial Commission.” The Bicenten-
nial of the Constitution is almost over as I write this. We shall see
what time brings, What I expect is that sooner or later there will be a
series of major national crises. These could then be orchestrated by
the media to create public pressure to have a Constitutional Conven-
tion. The Constitution could easily be scrapped, just as the Articles
of Confederation were scrapped, It does no legal good to tell the del-
egates that they are not allowed to offer a new Constitution. That
strategy was taken by Congress in 1786. It failed.

This substitution could very easily destroy the one source of le-
gitimacy remaining to the U.S. national civil government. A new
covenantal experiment, I predict, will not gain widespread public
support once the headlines fade. There would be a growing con-
frontation between Christians and humanists who run the various
civil governments. Christians will see the openly humanist nature of
the new Constitution; they are presently blinded by tradition and
wishful thinking regarding the present Constitution. Thus, the hu-
manist powers that be will lose the one thing that they desperately
need: the legitimacy imparted by the national sovereign, “We the
People.” Couple this with a Holy Spirit-directed revival, and you
could get a total transformation of the American political order. But
this will take time. It will also take a recognition of the nature of the
Masonic coup détat of 1787-88.

Conclusion

We cannot expect to go back to the Articles of Confederation, nor
do I believe that the Articles were capable in 1781 of solving the cove-
nantal problem of the one and the many, unity and diversity. This
document was a halfway covenant. The Articles needed major revi-
sions, as all men of the day knew. It may well be that the U.S. Con-
gress in 1787 would not have agreed to the necessary revisions: the
strengthening of the executive, the abolition of the unanimous state
agreement rule, the abolition of all internal tariffs, and the abolition
of state government fiat (unbacked) paper money. What I object to
as a Christian is the continuing silence regarding the two fundamen-

20. “Hijacking the Constitution, Phase One,” Remnant Review (May 24, 1985), pp.
2-3. The man who first predicted this is Jeffrey St. John.
