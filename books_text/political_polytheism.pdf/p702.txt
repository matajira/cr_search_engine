678 POLITICAL POLY THEISM

This is only half of the story of the Enlightenment. That in 1798 we
find an anti-Jeffersonian, anti-French Revolution outlook among many
Americans!°-- those who agreed with Edmund Burke regarding the
horrors of the French Revolution— should be no more surprising than
the fact that we also find pro-French, pro-Jefferson sympathizers."
The mere presence of an anti-French Revolutionary outlook in the late
eighteenth century was no guarantee of Enlightenment-free wisdom.

Edmund Burke had been the most eloquent opponent of the
French Revolution from the very beginning, and nineteenth-century
European conservative intellectual thought was overwhelmingly
Burkean. Yet Burke was surely a representative thinker of the right
wing of the Enlightenment. He was a correspondent with Adam
Smith, David Hume, and other Scottish Enlightenment figures. His
conservative philosophy of pluralism and social traditionalism
agreed with their classical liberal doctrine of social evolutionism.
This outlook is reflected in Burke’s statement that “The science of
constructing a commonwealth, or renovating it, or reforming it, is,
like every other experimental science, not to be taught a priori. Nor is
it a short experience that can instruct us in that practical science, be-
cause the real effects of moral causes are not always immediate; . . .”!2
Burke had been a supporter of the American Revolution, actually
serving as the paid London agent-lobbyist of the New York Legisla-
ture right up until the War broke out.'? His defense was that the
British Parliament should “leave the Americans as they anciently
stood.”4* Was this opinion inherently conservative, liberal, or

10. Vernon Stauffer, New England and the Bavarian Illuminati (New York: Russell &
Russell, [1918] 1967); Zoltan Haraszti, John Adams and the Prophets of Progress (New
York: Grosset & Dunlap, [1952] 1964); David Hackett Fischer, The Revolution of
American Conservatism: The Federalist Party in the Era of Jeffersonian Democracy (New York:
Harper & Row, 1965).

11, On the conflict, see John C. Miller, The Federalist Era, 1789-1801 (New York:
Harper Torchbooks, [1960] 1963); Daniel Sisson, The American Revolution of 1800
(New York: Knopf, 1974); Richard Buel, Jr., Securing the Revolution: Ideology in Ameri-
can Politics, 1789-1815 (Ithaca, New York: Cornell University Press, 1972). For an
interesting monograph on one such Jeffersonian, Nathaniel Ames, see Charles
Warren, Jacobin and Junto (New York: Blom, [1931] 1968). Ames was Fisher Ames’
brother.

12. Edmund Burke, Refections on the Revolution in France (Indianapolis, Indiana:
Bobbs-Merrill, [1790] 1955), p. 69.

13. Robert B. Dishman, “Prelude to the Great Debate,” Burke and Paine: On Revo-
lution and the Rights of Man (New York: Scribner's, 1971), pp. 27-28.

14. Isaac Kramnick (ed.), Edmund Burke (Englewood Cliffs, New Jersey: Prentice-
Hall, 1974), ch. 2.
