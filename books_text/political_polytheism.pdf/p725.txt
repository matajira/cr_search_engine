Rushdoony on the Constitution 701

Conclusion

Rushdoony begins The Nature of the American System with this ob-
servation: “The concept of a secular state. was virtually non-existent
in 1776 as well as in 1787, when the Constitution was written, and no
less so when the Bill of Rights was adopted. To read the Constitution
as the charter for a secular state is to misread history, and to misread
it radically, The Constitution was designed to perpetuate a Christian
order.”8° He has never retreated from this position; indeed, he has
escalated his commitment to it—so much so, that he has now under-
cut the covenantal foundation of The Institutes of Biblical Law,

The problem with the U.S. Constitution was and is polytheism.
Rushdoony has described the problem of political polytheism:
“Modern political orders are polytheistic imperial states, but the
churches are not much better. To hold, as the churches do, Roman
Catholic, Greek Orthodox, Lutheran, Calvinist, and all others vir-
tually, that the law was good for Israel, but that Christians and the
church are under grace and without law, or under some higher,
newer law, is implicit polytheism.”®° But he has always refused to
identify the obvious polytheism of the Constitution. Thus, he has
had to explain modern political pluralism as a deviation from the
Constitution rather than its inevitable product.

The ratification of the U.S. Constitution in June of 1788 created
a new nation based on a new covenant. It placed the new nation
under a “higher, newer law.” The nation had broken with its Chris-
tian judicial roots by covenanting with a new god, the sovereign Peo-
ple. There would be no other God tolerated in the political order.
There would be no appeal beyond this sovereign god. That collective
god, speaking through the federal government, began its inevitable
expansion, predicted by the Antifederalists, most notably Patrick
Henry. The secularization of the republic began in earnest. This
process has not yet ceased.

Nevertheless, the surrender to secular humanism was not an
overnight process. The rise of abolitionism, the coming of the Civil
War, the advent of Darwinism, the growth of immigration, the
spread of the franchise, the development of the public school system,
and a host of other social and political influences have all worked to
transform the interdenominational American civil religion into a re-

89, Rushdoony, Natwe, p. 2.
90. Bbid., p. 18.
