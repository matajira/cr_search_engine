Preface xxi

There are always firing squads in life, in peace and war. Hell it-
self is a kind of cosmic firing squad. The question is: Who will do the
firing? More to the point: Whose law will govern the civil court that
issues the sentence? Will the standard of justice be God’s law or
man’s? !6 Tt is this question which the political pluralists dare not ask
in public. To ask this question publicly is to encourage Christians to
seek exclusively biblical answers. Pluralists do not want Christians
to seek exclusively biblical answers. Biblical exclusiveness is a denial
of the religion of pluralism.

The Dilemma of Christian Pluralism

The synthesis of political pluralism is not breaking down only be-
cause of the inherent contradictions between the religion of man and
the religion of the Bible. It is also breaking down because the reli-
gion of secular humanism is itself collapsing, not only theoretically
but institutionally.

For about eighteen centuries, the foundation of the West's ideo-
logical synthesis was men’s naive faith in the existence of natural
rights and natural law principles, discoverable by unaided human
reason, or at least by “right reason.” This intellectual construct was
the invention of later Hellenistic philosophers who were trapped by
the collapse of the Greek polis and its'religious and philosophical
underpinnings. They saw that the collapse of the polis and the simul-
taneous rise of empire made necessary a new philosophical outlook.
Natural rights theory was their suggested solution.!?

This intellectual construct served Christian apologists as the
epistemological foundation of a synthesis, a common-ground philoso-
phy of justice. It was on the basis of supposedly shared philosophical
first principles that the discovery of a shared moral universe was
thought to be possible, From Justin Martyr to Gary Scott Smith,
Christians have had faith in this shared universe of discourse. What
we forget is that Justin was indeed a martyr, and a martyr under the
rule of that most Stoicly philosophical of emperors, Marcus
Aurelius. The hoped for common moral ground with Christianity did
not exist in the eyes of the Emperors. This fact should have sent
warnings to Justin’s successors down through the centuries. Unfor-

16. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
Texas: Institute for Christian Economics, 1985).

17. Sheldon S. Wolin, Politics and Vision: Continuity and Innovation in Western Political
‘Thought (Boston: Little, Brown, 1960), ch. 3.
