Halfway Covenant Social Criticism 209

ment times of positive feedback between covenantal faithfulness to
God’s revealed law and God’s external blessings in history (Deut.
28:1-14), Implicitly or explicitly, this is exactly what they do deny.
They necessarily remove the institutional covenantal sanctions and
promises from the New Testament era. The Old Testament then be-
comes God’s discarded first draft.

This hostile attitude regarding the Jegitimacy of specified Old
Testament covenant sanctions in civil government eventually spills
over into Church government and family government. “We're under
grace, not law” is a misleading slogan that cannot be bottled up in
the realm of civil government; it reflects an attitude corrosive of all
government: self-government, family government, and Church gov-
ernment. Thus, we find that biblical antinomianism-— hostility to
God’s revealed law—becomes either libertinism or legalism, and
sometimes both, The best example of this dual process is fundamen-
talism’s hostility to tobacco and liquor— “Never, ever, for any reason!”
—and its institutional tolerance of adultery, especially by pastors.
They may click their tongues, but they often do not permanently de-
frock their adulterous leaders. (Since fundamentalist pastors rarely
wear robes in the pulpit, the word “defrock” has no visual reference-
point to them.) At most, the denomination or local church suspends
them for a period.!'* And when the adulterous pastors refuse to sub-
mit even to this, which is most of the time, they are not publicly ex-
communicated as rebellious church members, but are only removed
quietly from the group’s ministerial responsibilities./? In some

  

 

 

118. Internationally known television evangelist Jimmy Swaggart visited a prosti-
tute for over a year in Louisiana. This was publicly exposed in carly 1988 by another
pastor whase sexual life had also been corrupt, and whose large ministry had disin-
tegrated when he had been exposed publicly by Swaggart. (It is not wise to consort
with a prostitute who operates within a mile of the man whose career you destroyed with
a similar accusation.) The Assemblies of God denomination suspended Swaggart
for a year, This prepostcrously mild tap on the wrist was too hard for Swaggart, who
left his denomination in protest and continued to run his disintegrating worldwide
ministry. This is what virtually all of them do. The modern Church pays no atten-
tion to Gods sanctions (blessings and cursings) that accompany the Lord’s Supper,
so excommunication means little to cither church governments or the excommani-
cated members. ‘The idea that God brings His sanctions in history appeais to them
only when dealing with their rivals, Nevertheless, their ministries never fully
recover. Incredibly, Charles R. Fontaine and Lynda K. Fontaine have written a
defense of Swaggart against the Assemblies of Gad, Jimmy Swaggart: Ta Obey God
Rather Than Men (Crockett, Texas: Kerruso, 1989).

119, The Assembly of God imposed no public excommunication on adulterous
Jim Bakker in 1987 or on Swaggart in 1988, their most well-known television preach-

 

   
