The Authority of H. Richard Niebuhr 671

men, and, above all, the activity of Christ. For faith in the Absolute, as
known in and through Christ, makes evident that nothing I do or can do in
my relative ignorance and knowledge, faithlessness and faith, time, place,
and calling is right with the rightness of completed, finished action, right
without the completion, correction, and forgiveness of an activity of grace
working in all creation and in the redemption.”

Got that? He went on like this for another two pages, but you
probably cannot stand much more of it. I can think of no more
fitting evaluation of the literary style, not to mention theological con-
tent, of H. Richard Niebuhr, theologian of the Absolutely Relative,
than H, L. Mencken’s description of the oratorical style of President
Warren G. Harding: “He writes the worst English that I have ever
encountered. It reminds me of a string of wet sponges; it reminds me
of tattered washing on the line; it reminds me of stale bean soup, of
college yells, of dogs barking idiotically through endless nights. It is
so bad that a sort of grandeur creeps into it. It drags itself out of the
dark abysm (I was about to write abscess!) of pish, and crawls in-
sanely up to the topmost pinnacle of posh. It is ramble and bumble.
It is flap and doodle. It is balder and dash.”

What was Niebuhr getting at? Simple. He did not want to face
the thought of a personal God who judges all men in terms of their
personal commitment to His living, bodily resurrected, physically
Incarnate Son. He did not want to accept the fact that God’s Son
came into history in order to fulfill perfectly the specific terms of
God's covenant law, and that anyone who refuses to accept the work
of Jesus Christ as his personal sacrificial substitute will spend eter-
nity in screaming agony.

 

Escape to the Noumenal

To avoid thinking of these unpleasant things, Niebuhr the Apos-
tate also abandoned along the way other unpleasant thoughts, such
as covenant law, an infallible Bible, cognitive revelation from God to
man, prayer that works, exercism of real demons, creeds that ac-
curately reflect the theological and ethical standards of God, and the

10, Ibid. pp. 239-40.

11. Mencken, Baltimore Evening Sun (March 7, 1921), Cited by Mirian Ringo,
Nobody Said It Beiter! (Chicago: Rand McNally, 1980), p. 201. William Allen White
almost matched Mencken, and did so in one sentence; “If ever there was a he-harlot,
it was this same Warren G. Harding.” Idem. He-harlotry is the very core of
Niebuhr's theology.
