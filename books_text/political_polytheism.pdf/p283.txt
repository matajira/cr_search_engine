Halfway Covenant Historiography 259

Plymouth, protesting that their title to the land was faulty. The King
may have granted them this land, but this title was illegitimate; only
by purchase of land from the Indians could their claim be defended.
Obviously, this was as much an attack on the Massachusetts Bay as
it was on Plymouth.

The rulers of the Massachusetts Bay then ordered him to court
for a proposed censure for asserting that the King had lied, ‘Their
second charge against him rings down through the ages to rest at the
door of our trio: the court also threatened to censure him for his hav-
ing accused the King and others of blasphemy “for calling Europe
Christendom, or the Christian world; . . .”'°' Then he backed
down, apologizing to everyone. This became his familiar, lifelong
tactic: launching a series of full-scale frontal assaults on the very
legal foundations of New England, and then, when he faced public
censure, offering abject apologies. He did this repeatedly until they
were about to banish him, whereupon he fled to the wilderness of
Rhode Island, where he was joined the following year by Mrs.
Hutchinson and the antinomians, who had also challenged the foun-
dations of New England.!!° And then, once safely outside the bor-
ders of Massachusetts, his language against the Massachusetts’ con-
cept of a biblical civil covenant grew ever-more outrageous: “My end
is to discover and proclaim the drying and horrible guilt of the
bloody doctrine, one of the most seditious, destructive, blasphe-
mous, and bloodiest in any or in all the nations of the world. . . .”41
Tt was safe to say this in Rhode Island.

The New Rhode Istand

The modern Christian academic world still lives metaphorically
and judicially in Rhode Island, surrounded by antinomians. A dis-
tinct school of historical interpretation has grown up in recent years,
which I like to refer to as the Rhode Island wilderness school of his-

109. Winthrop’s Journal: “History of New England, 1630-1629, edited by James K.
Hosmer, 2 vols. (New York: Barnes & Noble, [1908] 1966), I, p. 117

110. Emery Battis, Saints and Seclaries: Anne Hutchinson and the Antinomian Controversy
in the Massachusetts Bay Colony (Chapel Hill, North Carolina: University of North
Carolina Press, for the Institute of Early American History and Culture at
Williamsburg, Virginia, 1962).

111. Williams, The Bloody Tenant Yet More Bloody (1614), extract in Theodore P.
Greene (ed.), Roger Williams and the Massachusetts Magistrates (Boston: D. C. Heath,
1964), p. 14.
