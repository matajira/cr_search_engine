subversion, 70, 465, 471, 680-81
suffrage, Chapter 2, 116, 661
suicide, 421
Supreme Court (U.S.)
abortion, 226, 292, 396, 602
American rights, 529
anti-pluralism, 651
appeal beyond, 505, 509, 509-13,
37
Bork, 562
checks & balances (none), 509
Cherokee Nation 2 Georgia, 510
Civil War &, 512
Congress controls, 511-13
continuity, 395
declaration, 503-4
Dred Scoté, 511
election returns, 397
Ex parte McCardle, 511-12
final appeal, 504, 510, 517
fundamental law, 506
interpretation, 381, 381, 502-4,
509, 517
judicial review, 450, 503-4, 509-11,
517
jurisdiction of, 511-13
legislation by, 503-4, 510
legitimacy by default, 510
legitimates legislation, 502
plebiscites, 506, Stt
pre-Civil War, 506
private property, 395-96
public opinion, 503
sacrosanct, 503
self-restraint, 512
sovereign, 440, 517
unitary interpreter, 509
voice of People, 504, 517
warnings, 508-9
web of subjectivity, 504
Worchester v. Georgia, 510
surrender, 3-4, 91
suttee, 97
Swaggart, Jimmy, 209n
syllogism and law (Holmes), 397
syphilis, 10

Tabernacle, 103

Index 767

taskmasters, 295
tax deductions, 94-96
taxation, 294, 584-85, 595-97
Taylor, A. J. P., 13
tea party, 434-35
Temple, 103-4, 342, 344
Temple of Virtue, 434
Tennent, Gilbert, 357-59
tenure, 248, 257, 295, 629
Terror, 32, 339, 348
Tertultian, 497
test oath
Articles of Confederation, 379, 384
Constitution, 379, 386-87, 389-92,
410, 507
central feature, 410, 699
churches & 421, 643
courts, 383
Deism, 30
disestablishment, 643
eschatological sanctions denied,
403
false, 389-90
federal sovereignty &, 700
final prohibition (1961), 387, 392
Fourteenth Amendment, 643
implicit, 440
judicial atheism vs., 568
Madison on, 389-90, 428, 449
Massachusetts, 643, 719
natural law &, 393, 410
Pennsylvania, 463
precedent, 643
reversal, 385-86
sacrosanct, 410
screening, 387
secularization, 367, 463,
state constitutions, 379, 383-85,
387, 389, 450, 459, 462-63, 643
Senate, 387
testament, 383
Torcaso v. Watkins (1961), 387n, 392
Virginia, 449
witness, 383
see aiso Article V1, sanctions
theocracy
bottom-up, 39, 157, 224, 529, 585,
590, 612, 647, 649-50
