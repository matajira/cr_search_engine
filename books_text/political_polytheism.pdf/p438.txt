414 POLITICAL POLYTHEISM

Madison cites these provisions in Federalist 40, claiming that the
Convention honored the first provision~- suggesting alterations —
while it legitimately violated the second: not reporting back to Con-
gress. This was what Clinton Rossiter called the four-part “short-
range bet” of the Framers: that they could get away with a four-step
transgression of the rules under which the Convention had been au-
thorized.* Therefore, men such as Rufus King and Sam Adams be-
lieved the Convention to be unconstitutional and dangerous.>

Virginia delegate George Mason had written a letter in late May
stating that the “most prevalent idea 1 think at present is a total
change in the federal system and. instituting a great national
council.” From the opening of the Convention, no consideration
was given to a mere revising of the Articles of Confederation, Gover-
nor Edmund Randolph of Virginia opened the main business of the
Convention on May 29 by giving a speech on why a totally new gov-
ernment ought to be created, and he then submitted the fifteen-point
“Virginia Plan” or large-states’ plan to restructure the national gov-
ernment. This had been drawn up by James Madison.” According
to New York’s Chief Justice Yates, who became an opponent of the
Constitution, and who made notes for his personal use {but not for
publication): “He candidly confessed, that they were not intended
for a federal government; he meant a strong, consolidated union, in
which the idea of States should be nearly annihilated.”®

The Articles were completely scrapped by the delegates. There is
little doubt that this had been the original intention of the small group
of men who first promoted the idea of the Convention, beginning
with the meeting held in the spring of 1785 at Washington’s home at

4. The steps were the decisions of the delegates: 1) to become Framers of a new
government; 2) to go beyond their instructions; 3) to designate special conventions
to ratify the new document; and 4) to determine that the new government would
come into existence when only nine of the stale conventions ratified it. Clinton
Rossiter, 1787: The Grand Convention (New York: Norton, [1966] 1987), p. 262.

5. Steven R. Boyd, The Politics of Opposition: Antifederatists and the Acceptance of the
Constitution (Milwood, New York: Kraus-Thomson Organization, 1979), p. 4.

6. Mason to Arthur Lee, May 21, 1787: The Papers of George Mason, edited by
Robert Rutland, 3 vols. (Chapel Hill: University of North Carolina Press, 1970),
IIL, p. 882; cited in ibid., p. 6.

7, Irving Brandt, The Bill of Righis: Its Origin and Meaning (Indianapolis, Indiana:
Bobbs-Merrill, 1965), p. 16.

&. Secret Proceedings and Debates of the Convention (1838), p. 101; reprinted by Omni
Publications. Hawthorne, California, 1986. Reprinted also in Documents Illustrative of
the Formation of the Union, p. 747.
