Renewed Covenant or Broken Covenant? 4il

T argued at the beginning of this chapter that “the oath has con-
tinuity over generations. So do its stipulations. Only the sovereign who
establishes the oath can change the stipulations or the oath. The
ability to change the stipulations or the oath is therefore a mark of ul-
timate sovereignty.” The U.S. Constitution can legally be amended.
Doesn’t this indicate that the nation’s sovereign is the electoraie
rather than God? This is exactly what the amending procéss indi-
cates under the present Constitution. This is why the Constitution is
a broken covenant,

To preserve its judicial continuity, a national covenant must es-
tablish the Bible as the law of the land. The Bible is an permanent
covenant document. Its stipulations do not change. A nation’s civil
courts must therefore enforce the Bible's civil laws.. Any statute not
in conformity to the Bible must be declared unconstitutional. An
oath of allegiance to the national government is a promise to uphold
the national constitution; this must automatically be an oath to up-
hold and enforce the Bible.

A national constitution is required by God to serve as the by-laws
of the ultimate source of legitimate civil law, the Bible. A constitu-
tion’s Preamble is the appropriate place to declare this publicly. The
Preamble should be a nation’s Declaration of Absolute Dependence
on the Trinitarian God of the Bible. The Preamble should therefore
declare the Bible as the unchanging law of the land. It should declare
this law as being immune to any subsequent alteration. Thus, any
public rejection of this judicial standard would be identifiable as a
breaking of the national covenant.
