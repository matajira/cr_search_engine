550 POLITICAL POLYTHEISM

acknowledges the government of Divine Providence.” The address
then identified the role of the Presbyterian Church in the American
political religion: “We shall consider ourselves as doing an acceptable
service to God in our profession when we contribute to render men
sober, honest, and industrious citizens, and the obedient subjects of
a lawful government.”

The Grand Master from Virginia politely responded in kind.®

T have argued elsewhere that the Church sets the pattern for
what the State does.® That pair of constitutional assemblies held on
May 28, 1787 — one civil, the other ecclesiastical; one beginning, the
other ending —are the best representative examples in American his-
tory of how a change in the thinking of Christians parallels a change
in the thinking of politicians. As the Presbyterians closed their meet-
ing and the Framers opened theirs, the nation was turned down a
path that would have been covenantally unthinkable anywhere on
earth a generation earlier (except, of course, in Rhode Island). In
this case, the change in men’s thinking transformed the constitutional
(covenantal) foundations of both Church and State in America. What
had been called the Presbyterian Rebellion by its enemies in Eng-
land became a Presbyterian revolution judicially. The Presbyterians
and the Framers ended the holy commonwealth ideal in America.
The Presbyterians of Philadelphia, like the lawyers of Philadelphia,
removed the covenantal foundations of the American Christian ex-
periment in Christian self-government. Without these covenantal
cornerstones to support it, the American Trinitarian edifice col-
lapsed. We live today in its ruins.

Conclusion.

By 1800, the myth of the national covenant was just about over.
The churches, in the words of Perry Miller, “were forced to recog-
nize that in fact they now dealt with the Deity only as particular indi-
viduals gathered for historical, capricious reasons into this or that
communion. They had to realize, at first painfully, that as a united
people they had no contractual relationship with the Creator, and
that consequently a national controversy with Him could no longer

64. Cited in Patton, Popular History, p. 209.
65, Idem. ’
66. Noxth, Healer of the Nations, especially pp. 4-5.
