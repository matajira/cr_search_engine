Introduction 15

ogy, his pamphlets, his vitriol, and his incendiary language. He
humiliated his opponents in public and in print. He ridiculed the ri-
diculous. I am at best a pale imitation of Luther. But at least I know
this much: I cannot hope to lead anyone to victory in my assigned
sector of this battle for civilization by imitating Erasmus’ reputed
strengths. I have at least a possibility of contributing to the victory of
Christ’s kingdom in history by adopting what many of his heirs re-
gard as Luther’s embarrassing weaknesses.

There is, however, one important difference between my style
and Luther’s. The confrontation between Erasmus and Luther has
been described as “a duel in which the two participants got up at the
crack of dawn, one armed with a rapier, the other with a blunder-
buss. . . .”*1 Erasmus wielded the rapier, Luther the blunderbuss. I
do much better with a rapier.

Creationism, Neo-Evangelical Style

This book is designed to clear the deck ideologically. 1 show what
cannot work biblically and has not worked for over three centuries.
By attacking Christian political pluralism, I am attacking a sacred
cow which is in fact a long-dead mule. It is an intellectual system
which cannot be successfully defended by an appeal to the Bible, and
which is regarded by humanists as intellectually peripheral to the
concerns of the non-Christian world. Christian political pluralism is
to modern political theory what theistic evolution is to modern bio-
logical science: a sell-out of Christianity to the humanists who run
the academic world. The irony is that the humanists regard the
whole charade of theistic evolution as either a crude intellectual joke
or else a self-serving fraud deserving of contempt. Christian politi-
cal pluralism is not much better respected in the world of humanist
scholarship. We should not be surprised to learn that the same
undergraduate Christian academic institutions and publishing houses
support both forms of this ideological sell-out. I call this the Wheaton-
Westchester-Downers Grove-Grand Rapids-Wenham-Toronto com-
plex, Wheaton College is the model.*

41. Rupp, op. cit, p. 2.

42. See, for example, George Gaylord Simpson, This View of Life: The World of an
Evolutionist (New York: Harcourt, Brace & World, 1964), ch. 11: “Evolutionary
Theology: The New Mysticism.”

43. If you can locate a copy, I encourage you to read Wilhelm E, Schmitr’s self-
published book, Steps Toward Apostasy at Wheaton College (1966). Schmitt was a
Wheaton student, class of 1954,
