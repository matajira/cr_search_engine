136 POLITICAL POLYTHEISM

to discussing why all other Christian apologists had in effect “set
their buzz saws at the wrong angle,” but he refused to discuss the
God-revealed “straight edge” against which we measure deviations
from perfection: covenant law.

The true heart of his apologetic system is covenantal ethics, but
the system is cloaked in the language of logic and epistemology. The
heart of his system is the covenant, but his language is misleadingly
philosophical. This misleading language is reflected in his famous
phrase, epistemological self-consctousness, By this phrase, he meant a
person’s ability (and willingness) to see what he is in relation to God,
and then think in terms of it. Yet he always knew that thought is tied
to action, that it is not just what man thinks but also how he acts. We
always know God covenantally and oly covenantally. There is no
other God to know but the God of the covenant. The heart and soul
of Van Til’s presuppositional methodology is therefore not epistemo-
logical self-conscious but rather ethical seif-consciousness. Yet I do not
recall ever seeing this phrase in his writings.

Ethics and Eschatology

Part of this self-imposed confusion in Van Til’s apologetics is the
fault of his amillennial eschatology: it leads to a debilitating pessi-
mism concerning the earthly future of the kingdom of God.*° He fol-
lowed what he thought had been the eschatology of Geerhardus Vos:
“Dr. Vos makes plain that there is a two-fold aspect to Jesus’ teaching
of the kingdom, Righteousness and conversion have to do with the
present aspect of the kingdom, and blessedness primarily with the
future aspect of the kingdom,”*! (In fact, Vos may have been a mild
postmillennialist, but since he was Dutch, no one could quite believe
it.)22 In other words, Van Til and other Dutch-background amillen-

30. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987), ch. 4: “Van Til’s Version of Com-
mon Grace.”

31. Van il, Christian Theistic Ethics, vol. 3 of In Defense of Biblical Christianity (Phil-
lipsburg, New Jersey: Presbyterian & Reformed, [1958] 1980), p. 121,

32. His interpretation of Matthew 24 was quite similar to that of J. Marcellus
Kik, An Eschatalogy of Victory (Nutley, New Jersey: Presbyterian & Reformed, 1971),
Section II. He saw the “immediately after” of verse 29 us referring to the fall of Jeru-
salem in a.p. 70. Vos, “Eschatology of the New Testament,’ Jnfemational Standard
Bible Encyclopedia, § vols. (Grand Rapids, Kerdmans, 1929), I, pp. 982-83. He saw a
fature conversion of the Jews with some kind of gospel increase (Rom. 11): #bid,, p.
983. See especially the subsection, “Events Preceding the Parousia”
