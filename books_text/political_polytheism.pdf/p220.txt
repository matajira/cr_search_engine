196 POLITICAL POLYTHELSM

general, though he did not admit to his prior verbatim liftings.®?
Chilton let bygones be bygones and stopped complaining. He and I
did not mention this incident in our 1983 essay on “Apologetics and
Strategy,” although we did mention the nearly verbatim lifting of cer-
tain material from Rushdoony’s The One and the Many (1971). We had
not noticed that Schaeffer's Complete Works (1982) reproduced the first
edition of the Christian Manifesto, so the footnotes acknowledging
Chilton and Flinn were again missing. Had we spotted this, we
might not have been so conciliatory. Printing and typesetting sched-
ules were presumably responsible for the omission, but when you or
your research assistant literally steal other men’s works, and the vic-
tims catch you at it, then you should go out of your way to rectify
things, cven if it means some extra typesetting fees or a delay in pub-
lishing your Complete Works. Make the set complete: add the missing
footnotes (not to mention the missing essay on infant baptism).

Rutherford: Yes and No

Why all of this space devoted to an unsavory incident? Because
of the highly revealing nature of the material that Schaeffer refused
to plagiarize or even mention: Flinn’s lengthy section on Samuel
Rutherford’s use of the Old Testament’s case laws. What is missing
points clearly to the theological schizophrenia of Francis Schaeffer's
social criticism: an attempted rejection of humanism, yct also a re-
jection of biblical law.

Here is what Flinn concluded regarding Rutherford’s teachings
on the case laws. First, Rutherford used the Old Testament magis-
trate as the model for today’s civil magistrate. Second, he insisted
that God’s law alone properly defines crime. Flinn observes: “We
have seen that the magistrate cannot arbitrarily suspend punishment
from or pardon those crimes which God’s law stipulates as capital
crimes, and requiring the death penalty. To do so is to deify the
state.”®3 Rutherford’s third principle regarding the civil magistrate
and the case law is that the magistrate has a duty to enforce all ten
commandments, not just the second five.**

Finally, wrote Flinn, “The most conclusive evidence for Ruther-
ford’s position on the case law being one of continuing validity for

82. Christian Manifesto, p. M41, notes 2 and 7.
83. Flinn, of. cil., p, 71.
84. Tbid., pp. 71-72.

 
