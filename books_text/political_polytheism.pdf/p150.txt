Professor Roscoe Pound and other modern jurists have criticized
the natural-law basis of sovereignty on the same ground as the critics
of the pre-Christian era: namely, that it does not furnish a specific
consensus of ethical judgment. It boils down, says Pound, to what
the individual conscience dictates, and consciences differ. “An eigh-
teenth-century jurist laying down natural law and Bentham’s man
who claimed to be one of the elect are in the same position. Each is
giving us his personal views and is assuming that those views must
be binding upon everyone else.” The point is well taken as regards
eighteenth and much of nineteenth-century thought, under which
there is no longer a common ethos. For lack of it, as Pound points
out, jurists have fallen back on interest as the basis of law, and have
conceived the problem of jurisprudence as the evaluation or har-
monization of interests— as indeed, on the practical plane, it largely
(though never wholly) is. But the problem is insoluble without cri-
teria. The evaluation of anything is impossible without a standard.
The assumption that out of the clash of group interests as such an
harmonious synthesis can be devised or discovered is simply a return
to natural law by the back window because the key of the door has
been lost. As Woodrow Wilson said in 1918, “interest does not bind
men together: interest separates men . . . There is only one thing
that can bind people together, and that is common devotion to right.”
The emphasis is on the word common. Only where there exists a
fundamental agreement, not perhaps of explicit belief, but of out-
look, feeling, and value, is true toleration possible. It was this that
both Burke and Acton had in mind; it was to this that Catholic schol-
ars referred —as they still do when they made the secular state subject
to natural law; it was this that made political freedem possible.
Destroy that foundation and everything else falls to pieces.

William Aylott Orton (1945)*

*Orton, The Literal Tradition (New Haven, Connecticut: Yale University Press,
1945), pp. 95-96.
