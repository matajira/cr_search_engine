Halfway Covenant Ethics 145

blessedness belong together. On this point he was right, but the difficulty
was that he could not see that they could be temporarily separated from
each other. In order for Job to sce the absolute ideal at all, he had to see it in
a form that came very close to him, Then, when his blessedness was taken
away from him, he only slowly began to see that there was a future in which
matters would be rectified.

Where did Job go wrong? “He was not able to see things at long
range.” He did not see that covenant-keeping and external cove-
nantal blessings “could be temporarily separated from each other.”
This is admitted by all orthodox Christians; it is indeed a basic
teaching of the Book of Job. (The book’s primary message is the ab-
solute sovereignty of God: point one of the biblical covenant model.)
So, what is Van Til’s real point? Though Van Til writes “temporar-
ily,” he really means temporally, When Van Til said “temporarily sepa-
rated,” he really meant separated throughout pre-second-coming history, as
we have seen. He said that when it comes to the cause-and-effect re-
lationship between righteousness and visible blessedness, “the one
may be far ahead of the other.”*! By “far ahead,” he means inside history,
and his implicit meaning of “far behind” is beyond history (after the final
judgment). He simply ignored James 5:11 on Job’s earthly career.

Van Til refused to use words that would have revealed more
plainly what he really believed about covenantal history. As to why
he refused, I can only speculate. I suspect that the main reason was
that he did not want his readers to think that running through his en-
tire ethical system, and therefore his entire philosophical system,
was his eschatology, even though it really did. He always insisted
that covenant-keeping and covenant-breaking ultimately govern
philosophy, but what he could not bring himself to admit was that
they also govern history. Amillennialism reverses the cause-and-
effect relationship described in Deuteronomy 28. Van Til always
avoided mentioning his eschatological preference for amillennialism.
He seldom mentioned the dread word “eschatology,” and never, ever
used words like “postmillennialism” or “amillennialism.” It was as if
he believed that he could be eschatologically neutral when writing
about philosophy and ethics. This was a strange tactic for a man who
denied neutrality in any area of thought and life. The fact is, his
mind-set was established by his Dutch amillennial heritage, and he

50. Van Til, Ethics, pp. 120-21.
51. Iid., p. 122,
