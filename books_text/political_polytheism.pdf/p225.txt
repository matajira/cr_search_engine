Halfway Covenant Social Criticism 201

By the spring of 1987, Koop was self-consciously in retreat from
his earlier Christian position, With respect to the abortion issue, he
commented: “I’ve written all that I have to write on that issue. There
are other, bigger things that I should turn my attention to as surgeon
general; Where this country is and where it’s going in health
care.”400 He had openly adopted ethical neutrality as his theology. In
an interview with the liberal Washington Post, he announced: “I am
the surgeon general of the heterosexuals and the homosexuals, of the
young and the old, of the moral and the immoral, the married and
the unmarried. I don’t have the luxury of deciding which side I want
to be on.”?°1 This is the essence of political pluralism: a man seeks to
represent the entire community, meaning every individual self-pro-
claimed autonomous god. It invariably results in the abandonment
of God’s law and the triumph of God’s enemies,

In this crisis, when God raised him up so that he could make visi-
ble the nation’s highest official medical office, in the very year that
AIDS was identified, 1981, Dr. Koop returned to the common-
ground philosophy of natural law and neutral science. In 1987, he
announced: “I am not afforded the luxury of bringing ideology or
morals into my job, especially with the sort of threat we have with
AIDS. When you walk into a lab to do a sterile technique, you do a
sterile technique. When you walk into a health job, you make pro-
nouncements about health based on facts.”10? Facts? Neutrad facts? In
the fall of 1986, his office released an AIDS report under his name
that was misleading medically through its understatement of the
risks of transmission, and which did not call for drastic measures in
the face of what he himself says is a plague.'©? There is a reason for
this, AIDS is a very special plague, a politically protected plague.

The Economics of Sex Education

What is the solution? Dr. Koop said it was sex education, One
professional economist knows better. She has concluded that what is
needed is more church attendance. It is a remarkable irony of

100, “The Still-Crusading Koop Keeps the Moralizing Quiet,” Jnsight (March 16,
1987). This is published by the conservative Washington Times.

101. Washington Post (March 24, 1987), “Health Focus.” This is what he had said
publicly from the beginning: Washington Post (Oct. 2, 1981).

102. “Dr, Koop Defends His Crusades,” New York Times (April 6, 1987),

103. Gene Antonio, A Gritical Evatuation of the Surgeon Grneral’s Report on AIDS
(Ft. Worth, Texas: Dominion Press, 1987); Richard Bishirjian, “AIDS Education
and the Tale of Two Cities,” World & I (Sept. 1989), pp. 555-67.
