98 POLITICAL POLYTHEISM

the highways on Saturdays. It is illegal on Saturdays to serve a meal
in a restaurant until the sun goes down.) Some faiths will always be
excluded from any society. There will always be certain practices
and beliefs that are considered too perverse for any society to allow
in public. There is no neutrality,

Still, if many “churches” are allowed to preach their rival
messages, then this raises an unavoidable general question: What is
the basis of social cohesion? It also raises a specific question: What
should be the basis of civil law?

Natural Law

Traditionally, the answer of Protestants since the Protestant Ref-
ormation has been an appeal to universal principles of natural law.
The Roman Catholic Church also accepted this principle of law,
which the Church fathers had reworked from Stoic natural rights
theory, But there are several problems with this natural-law ap-
proach, The first problem is the religious basis of modern natural
law theory: Aumanism. Historian Paul Hazard discusses this in his
chapter on natural law: “Natural law was the offspring of a philoso-
phy which rejected the supernatural, the divine, and substituted, for
the acts and purposes of a personal God, an immanent form of
nature.””? How can Christians safely rely on a legal theory that is
either atheistic or pantheistic?

Second, Darwinism blew natural law theory to Kingdom Never
Come. Darwin’s universe is a universe without purpose, where
everything evolves in terms of presenily sovereign but evolving environ-
mental constraints. The iron law of nature changes. It is altered by
random responses to environmental constraints. “Varieties then arise
we know not why,” said Thomas Huxley, Darwin's popularizer.”!
Darwinism persuaded men that the world is under the control of
continually evolving forces in a purposeless environment. These
laws can be discovered by scientists, thereby enabling “man” (mean-
ing the scientists and technicians) to take control of his environment.
Socialists then persuaded men that there are also evolving laws of so-
ciety, that these laws can be discovered by highly trained experts,
and these experts can then show politicians how to achieve their so-

72. Paul Hazard, The European Mind, 1680-1715 (New York: Meridian, [1935]
1963), pp. 269-70.

73. Thomas Huxley, “The Origin of Species” (1860), in Huxley, Essays, edited by
Frederick Barry (New York: Macmillan 1929), p. 91.
