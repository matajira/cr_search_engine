But is this contemporary Western culture likely to continue for
long? The answer, it seems to me, must be in the negative —if we
take any stock in the lessons of the human past. One cannot be cer-
tain, of course; there is no sure way of catapulting ourselves into the
future; no way of being confident that even the hardiest or most
promising of current trends will continue indefinitely. But we can
take some reasonable guidance, I believe, first from the fact that
never in history have periods of culture such as our own lasted for
very long, They are destroyed by all the forces which constitute their
essence. How can any society or age last very long if it lacks or is
steadily losing the minimal requirements for a society~ such re-
quirements being the very opposite of the egocentric and hedonistic
elements which dominate Western culture today?

Second, it is impossible to overlook at the present time a phe-
nomenon that as recently as the 1940s we thought so improbable as
to be unworthy of serious thought or discussion. I refer to the faint,
possibly illusory, signs of the beginning of a religious renewal in
Western civilization, notably in America. Whatever their future, the
signs are present— visible in the currents of fundamentalism, pente-
costalism, even millennialism found in. certain sectors of Judaism
and Christianity. Even the spread of the occult and the cult in the
West could well be one of the signs of a religious renascence, for, as is
well known, the birth of Christianity or rather its genesis as a world
religion in Rome during and after the preaching of Paul was sur-
rounded by a myriad of bizarre faiths and devotions,

Robert Nisbet (1980)*

“Nisbet, History of the Idea of Progress (New York: Basic Books, 1980), p. 256.
