From Coup to Revolution 475

wealth began to undermine the original egalitarian goal of Masonry.
The fact that a single negative vote by a member could keep a pro-
posed member out also indicates that the lodge system was not all
that egalitarian.%

This Masonic hierarchical structure was gnostic. The Masonic
degrees were —or rapidly became — official manifestations of a series
of initiations into secret wisdom. This gnosticism was inherent in its
commitment to secrecy. In the AAiman Rezon, the constitutional doc-
ument of the Ancients, we are told regarding secrecy: “The last qual-
ity and virtue I shall mention, as absolutely requisite in those who
would be Masons, is that of SECRECY. . . . So great stress is laid
upon this particular quality or virtue, that it is enforced among
Masons under the strongest penalties and obligations. . . .”°? What
was seemingly a vertical hierarchy was in fact concentric. It was this
profound impetus to be elevated into a hierarchy by means of access
to concentric degrees of illumination that is the key to understanding
Masonry and all other illuminist secret societies. Every covenant re-
quires a priesthood, whoever the elected Grand Master may be. The
priests were those with higher knowledge who could select which of
the brethren would be allowed to advance upward, i.e., inward.
Masonry became an ideal recruiting ground for future revolutionaries.

Masonry cloaks its operations by means of parties and con-
viviality. Most of its own members do not suspect that it has ulterior
motives, the main one being the substitution of a different covenant
from that proclaimed by the Church, But the gnostic organization of
its hierarchy—initiation into the “inner circles”®*—is what distin-
guishes Masonry from clubs. Masonry can easily become a recruit-
ing ground for those who are willing to submit unconditionally to
others on the basis of hidden hierarchies. Secret societies, despite
possible rhetoric to the contrary, inherently tend to promote institu-
tional centralization and rigorous hierarchical obedience.°9

96. “By-Laws or Regulations” (1733), First Lodge, Boston: reprinted in Johnson,
Beginnings of Freemasonry, p. 14.

97, Ahiman Rezon, pp. 19-20.

98. Wrote the sociologist Georg Simmel in 1908 regarding secret societies: “The
contrast between exoteric and esoteric members, such as is attributed to the Pythag-
orean order, is the most poignant form of this protective measure. The circle composed
of those only partially initiated farmed a sort of buffer region against the non-initiates.”
Simmel, “The Secret Society,” in The Sociology of Georg Simmel, edited by Kurt H.
Wolff (New York: Free Press, [1950] 1964), p. 367,

99. Ibid., pp. 370-72.
