Halfway Covenant Historiography 257

necessarily raises the question of biblical law and Gods sanctions in history.
They reject the idea of God’s covenant judgments against any nation
in the New Testament era. (If this statement is exaggerated, which I
do not think it is, then at the very least they assume that we cannot
determine whether any event or group of events is part of God’s
positive or negative sanctions in history, Dr. Marsden states this em-
phatically, as we shall see.)

They have adopted this attitude from the anti-Trinitarian pro-
fessors who taught them, certified them, and tenured them. Their
former professors have a much deeper reason for adopting such a
view of God’s sanctions: God's covenantal judgments in history, if
accepted as a fact of history, point directly to God’s final judgment
beyond history. Presumably, Noll, Hatch, and Marsden are not
worried too much about final judgment. But they are quite ready to
remove the civil-judicial aspect of the gospel from serious con-
sideration. Our three scholars announce openly regarding their
previous academic publications (which, I might add as a trained his-
torian, are technically and intellectually way above average): “We
have written for our academic peers. . . .”!°' They are still writing
“for” them —as their agents—in The Search for Christian America. They
are also writing to them, at least in part.

Again, they reject the claim that New England’s civil order was
truly Christian. This is a very strange argument, given their asser-
tion that only church members were allowed to vote in Puritan New
England.'°? (This assertion, by the way, is questionable; town resi-
dents were allowed to vote in town elections if they were property
holders but not church members, but let us pass over this historical
problem.) Would they also try to argue that Iran’s civil order is not
Muslim because only Muslims are allowed to vote? I doubt it.

Pitying the Almost Noble Savages

Furthermore, there is that other great, intolerable evil of the
New England Puritans: the Puritans took land away from the “na-
tive Americans,”!®3 You know, the Indians, (Liberals have adopted
the phrase “native Americans” in recent years. They never, ever say
“American natives,” since this is only one step away from “American
savages,” which is precisely what most of those demon-worshipping,

101, [bid., p. 16,
102, {bid., p. 36.
103. Ibid,, p. 36.
