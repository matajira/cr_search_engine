Appendix D

TESTIMONY OF THE REFORMED PRESBYTERIAN
CHURCH OF NORTH AMERICA (1980)

Chapter 23; Of the Civil Magistrate

1. We reject the belief that civil government is unnecessary or
essentially evil.

2. God has given the exercise of all authority to the Lord Jesus
Christ. Christ is the Divine Lawgiver, Governor and Judge. His will
concerning the purpose of civil government and the principles re-
garding its functions and operation are revealed in the written Word
of God. The Holy Spirit enables even unregenerate rulers to fulfill
their proper functions. A true recognition of the authority and law of
Christ in national life can only be the fruit of the Spirit’s regenerat-
ing power in the lives of individuals.

Deut. 4:39; Dan. 4:25, 32, 35; Matt. 28:18; Phil. 2:10; Eph. 1:22; Isa.
33:22; Deut. 17:18-19; Isa, 45:1-7; Ezek, 36:27,

3. God has assigned to people, both individually and collectively,
the responsibility for establishing and maintaining civil government,
and the people are accountable to Jesus Christ for the proper exer-
cise of this responsibility.

Deut. 1:13-14; Deut. 17:15; 1 Sam. 8:22; 2 Sam. 5:3; Hos. 8:1, 4; Eccl.
10:16-17.

4. Every nation ought to recognize the Divine institution of civil
government, the sovereignty of God exercised by Jesus Christ, and
its duty to rule the civil affairs of men in accordance with the will of
God. It should enter into covenant with Christ and service to ad-
vance His Kingdom on earth. The negligence of civil government in
any of these particulars is sinful, makes the nation liable to the wrath

Tit
