Sanctuary and Suffrage 65

So, we will not choose today between Baal or God, since we have left
that matter te the individual conscience. In civil affairs, we are com-
pletely neutral between Baal and God, Also Moloch.”

A modern-day Elijah might answer: “Then whose law is enforced
in the civil courts of this land, Baal’s or God’s?” The predictable an-
swer from the assembled representatives is: “Neither.” That is to say:
“Not God’s.” But the God of the Bible is not content with this answer,
for it always means: “Man’s.”

Were Elijah to persist in this continual political agitation, and if
it looked as though he was getting a following, the major news media
would run a 42-week exposé on his 42-month stay in the home of the
widow of Zarephath. Her neighbors would be interviewed in depth,
and the widow would be offered $250,000 by Penthouse magazine for
an exclusive interview, assuming her story was sensational. Those in
charge of the national media—and they surely are not Christians —
would correctly perceive that if he and his followers were to become
politically successful, then their own days of power would be num-
bered, not just for political reasons, but because of the religious trans-
formation that would make such a political transformation possible.

In our day, Christians are afraid to hear such a message. It
frightens them. It points to a vast increase of personal and institu-
tional responsibility for them — responsibility in fields they have pre-
viously chosen to ignore. They do not want any added responsibility.
They much prefer their chains. (And maybe even an occasional
browsing through Penthouse—to read the latest political exposé, of
course.)

  

A Message of Liberation

In the next few pages, I am going to discuss the judicial and
political implications of these verses dealing with strangers and the
law of God. In the following subsection, “Sanctuary for Strangers,” I
will introduce some very simple biblical ideas — ideas that have been
suppressed, forgotten, or at least systematically neglected for at least
two centuries. These ideas are no longer preached from the pulpits
of the world. They are not discussed in public school textbooks. If
the principles they reveal were widely honored, the world would be
transformed. Radically transformed.

When you are finished with the subsection, I want you to sit
there and say to yourself, “Do I really believe this?” If you say either
“yes” or “maybe,” I want you to ask yourself another question: “Why
