Conclusion, Part 3 545

A poorly attended Synod in 1786 resolved to create 16 new pres-
byteries. Action on the creation of four synods was postponed. The
report of the committee on discipline was discussed, but no action
was taken, A new committee was set up to continue the study. A
meeting in September of 1786 led to a draft of a whole new constitu-
tion, to which the presbyteries generally paid litile attention.
(These events were paralleled by Madison’s and Hamilton's in-
conclusive Annapolis convention in September, which in turn led to
the call for the Convention in Philadelphia.)

Then came the Synod of 1787. From May 16 to May 28, the
Synod met in Philadelphia to discuss the formation of a new church
structure. On the last day of the Synod, May 28, the Synod voted to
create yet another committee to print a thousand copies of the draft
of the proposed form of government to be sent to the presbyteries for
consideration. The presbyteries would have to confirm the plan.
(This was paralleled by the Constitutional Convention’s decision to
have state ratifying conventions vote on the proposed new plan of
government. One difference: the presbyteries were the legal equiva-
lent of the established state legislatures, rather than conventions.)

The changes recommended by the committee were approved by
the joint Synod meeting exactly one year later in Philadelphia: May
28, 1788. This judicial act established a new constitution, 46 pages
long, for the Presbyterian Church in America. The form of govern-
ment radically centralized power in the General Assembly. From
that time on, it would take a two-thirds vote of the presbyteries plus
the assent of the General Assembly to make further changes. The
Synod did this on its own authority, after consultation with ihe pres-
byteries; the presbyteries did not vote.#° (On June 21, New Hamp-
shire became the ninth state convention to ratify the Constitution;
the new nation came into being.)

Trinterud tries to make this sound as if it was not a monumental
centralization of power. After all, he says, the General Assembly
could not initiate any further changes; only the presbyteries could.”
This is hardly persuasive. Try to organize presbyteries that are scat-
tered across a growing country. Get them to initiate and then organ-
ize fundamental change.. The whole discussion of the change in

44, Trinterud, Forming, p. 285.
45. Tbid., p. 288.
46. Ibid,, p. 295.
47. Ibid., p. 296.
