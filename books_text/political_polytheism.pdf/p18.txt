XX POLITICAL POLYTHEISM

supported public schools. This aided the cause of the more consist-
ent Christians, who could then plausibly begin to call for the banish-
ment of all public schools. For those in the middle—defenders of
public education and defenders of government subsidies to private
education— the skies grew dark, With the government subsidy comes
the iron fist. There are no free lunches, not even school lunches.
There is no neuitrality.

It is one of those historical ironies that Gary Scott Smith should
be a professor at the most famous non-cooperating college in Amer-
ica, Grove City. College, whose refusal to take federal money led to
its resistance to the imposition of federal educational regulations.
The government then sued the college. The case went all the way to
the U.S. Supreme Court, which ruled that unless Congress passed a
law that specifically brought Grove City College under its regula-
tions, the school was free to resist such controls. Congress immedi-
ately passed such a law over the President’s veto, announcing pub-
licly that even the indirect acceptance of federal funds through loans
granted to students constitutes the “open door policy” to all govern-
ment regulations over education. Nevertheless, Professor Scott con-
tinues to defend the legitimacy of the eighteenth-century “ideological
synthesis.” So does his Grove City colleague, historian L. John. Van
TTil.5 So deeply entrenched is the ideology of political pluralism that
most of its Christian victims cannot perceive what is happening to
them. The brainwashing by the humanists of the intellectual leaders
of conservative Protestantism has been remarkably successful.

A Warning Shot

This book is a warning shot across the bow of the aging battleship,
Ideological Synthesis. It argues that Christian defenders of political
pluralism are now trapped by the necessary and inescapable impli-
cations of their own compromise, They have bet their futures (and
yours) on the preservation of the political cease-fire between Christi-
anity and anti-Christianity. But as Christians steadily retreated from
this covenantal conflict, 1673 to 1973, turning in their weapons (e.g.,
Christian education) to a supposedly “neutral” police force, their
covenant-breaking enemies have systematically taken over that po-
lice force. This cease-fire is beginning to resemble the cease-fire of
the firing squad. It can end with one word: “Fire!”

15. He is the great-nephew of Cornelius Van Til.
