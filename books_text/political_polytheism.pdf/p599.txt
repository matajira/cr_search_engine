2
THE RESTORATION OF BIBLICAL PLURALISM

T have more understanding than all my teachers: for thy testimonies
are my meditation. I understand more than the ancients, because I keep
thy precepts (Psalm 119:99-100).

We need to take David’s words seriously. He defined personal
progress in history in terms of a better understanding of God’s re-
vealed laws. He could measure his progress beyond anything achieved
by those who had preceded him, not in terms of better study tech-
niques or improved means of communication or greater per capita
wealth, but in terms of his mastery of God’s precepts. Put another
way: without God’s precepts, we cannot measure our progress in personal sanc-
tification, Without a measure, we cannot make personal comparisons
over time. This is equally true of institutions.

Covenant-breaking man regards as preposterous any such view
of biblical law as the only valid standard of moral progress in history.
Sad to say, so does the modern Christian. Modern men worship at
their own shrines in the hope of achieving unbroken compound eco-
nomic growth per capita, but without covenantal conformity to God.
“We the people” are the self-identified gods of this age. We propose
and we dispose: Morally, autonomous man refuses to change; eco-
nomically, he expects and demands progress. He is like the drunk-
ards described by Isaiah: “Come ye, say they, I will fetch wine, and
we will fill ourselves with strong drink; and to morrow shall be as
this day, and much more abundant” (Isa. 56:12). This is why modern
society is headed for either an enormous series of disasters or an
enormous and culturally comprehensive revival—or perhaps the
crises, followed by the revival. God will not be mocked.

What Christianity needs today is a revival of casuistry: the appli-
cation of conscience to moral decisions, The conscience needs a reli-
able guide: biblical law. Casuistry has not been a popular.academic

575
