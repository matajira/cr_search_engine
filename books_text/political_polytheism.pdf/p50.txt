And when the queen of Sheba heard of the fame of Solomon con-
cerning the name of the Lorn, she came to prove him with hard
questions, And she came to Jerusalem with a very great train, with
camels that bare spices, and very much gold, and precious stones:
and when she was come to. Solomon, she communed with him of all
that was in her heart. And Solomon told her all her questions: there
was not any thing hid from the king, which he told her not. And
when the queen of Sheba had seen all Solomon's wisdom, and the
house that he had built, And the meat of his table, and the sitting of
his servants, and the attendance of his ministers,.and their apparel,
and his cupbearers, and his ascent by which he went up unto the
house of the Lorn; there was no more spirit.in her. And she said to
the king, It was a true report that I heard in mine own land of thy
acts and of thy wisdom. Howbeit I believed not the words, until I
came, and mine eyes had seen it: and, behold, the half was not told
me: thy wisdom and prosperity exceedeth the fame which I heard.
Happy are thy men, happy are these thy servants, which stand con-
tinually before thee, and that hear thy wisdom. Blessed be the Lorp
thy God, which delighted in thee, to set thee on the throne of Israel:
because the Lorn loved Israel for ever, therefore made he thee king,
to do judgment and justice. And she gave the king an hundred and
twenty talents of gold, and of spices very great store, and precious
stones: there came no more such abundance of spices as these which
the queen of Sheba gave to king Solomon. And the navy also of
Hiram, that brought gold from Ophir, brought in from Ophir great
plenty of almug trees, and precious stones. And the king made of the
almug trees pillars for the house of the Lorp, and for the king’s
house, harps also and psalteries for singers: there came no such
almug trees, nor were seen unto this day. And king Solomon gave
unto the queen of Sheba all her-desire, whatsoever she asked, beside
that which Solomon gave her of his royal bounty. So she turned and
went to her own country, she and her servants (I Kings 10:1-13).
