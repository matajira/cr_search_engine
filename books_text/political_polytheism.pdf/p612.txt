588 POLITICAL POLYTHEISM

In opposition to this view, amillennialists deny the premillennial
doctrine that Jesus will ever physically return in history. They insist
(as postmillennialism also insists) that Jesus will physically appear
only at the end of history at the final judgment. They therefore deny
(in contrast to postmillennialism) the possibility of an earthly mani-
festation of God’s comprehensive kingdom in history.

Because of their denial of the widespread acceptance of the gos-
pel at any point in history, premillennialists and amillennialists alike
invariably associate the word “theocracy” with some sort of top-
down, power-imposed, widely resisted rule that is imposed by an
elite. Premillennialists accept this as a valid system of civil rule, but
only if Christ personally and physically runs it from the top of the
bureaucratic pyramid, Amillennialists deny that Christ will ever do
this in history, so they deny bureaucratic theocracy’s legitimacy at
any point in the pre-final judgment future.

The Work of the Holy Spirit

First, we Calvinistic postmillennialists disagree with both groups
concerning the supposed impotence of the gospel in history in changing
whole societies, person by person, covenant institution by covenant
institution, We believe that the Holy Spirit will pose His will on the
recalcitrant hearts of huge numbers of people, just as He has always
imposed His will on each recalcitrant heart every time He has saved
anyone from his sins. God is utterly sovereign in election and salva-
tion, He changes people's hearts, transforming them so that they can
respond in faith to the free offer of the gospel. “The king’s heart is in
the hand of the Lorn, as the rivers of water: he turneth it whither-
soever he will” (Prov, 21:1). This is the only way anyone has ever
been saved, for the natural man does not receive the things of the
Spirit, for they are foolishness to him (I Cor. 2:14). The natural man
does not partially receive the things of the Spirit in his unsaved state;
he rejects the very idea that such a wrathful God exists or that his
own condition warrants such negative sanctions, Thus, the unsaved
person needs to be transformed before he or she can accept the gospel.
First comes God’s irresistible saving grace; then comes man’s
response: internal faith in Jesus Christ. In short, man does not have
free (autonomous) will. Paul was correct.?* Augustine was therefore

33. “(For the children being’not yet born, neither having done any goad or evil,
that the purpose of God according to election might stand, not of works, but of him
that calleth;) It was said unto her, The elder shall serve the younger, As it is written,
