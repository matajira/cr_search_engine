530 POLITICAL POLYTHEISM

churches for over three centuries have imitated the national State, a
disastrous. legacy of Erastianism: the national Church-nation State
alliance. It was this that the American colonies should have revolted
against in 1776; instead, they allowed the merchants, the lawyers,
the Unitarians, and the Freemasons to set the agenda for covenant-
breaking revolution. The result is today’s apostate national cove-
nantalism and denominational impotence, just as Madison planned.

In contrast to God’s kingdom, Satan’s empire leads to the reduc-
tion of localism through the investiture of total political power at the
top: the central international state facing the lone, atomized individ-
ual. This is Rousseau’s nationalism writ large: the political elimina-
tion of all intermediary institutions. It is also Madison’s and Hamil-
ton’s: the political trivialization of all intermediary institutions. This
new nationalism also created the need for a new humanist interna-
tional pantheon, i.e., the revival of Imperial Rome: an international
one-world order which must be a one-State order, a world in which
the national gods remain forever silent except as relics of the new
world order.

What I am arguing is that nationalism is an intermediary historical
step in the progress of the two kingdoms. It is not the final resting place of
either Christian covenantalism or humanistic covenantalism. We are
inevitably headed toward world government, both civil and ecclesi-
astical, World government is an inescapable concept, given the uni-
versalistic claims of both God and Satan. Neither God nor Satan is
about to “back off” in his claims for total allegiance. The question
therefore is: By whose covenant will this world government be created?

I offer this litmus test or early warning system regarding the
coming of humanism’s one-world order; Christian missions. The day
the churches stop sending out missionaries is the day we can mark
the next step of the extension of Satan’s one-world kingdom. The
missionary is living proof of the continuing commitment of Chris-
tians to world government. He is the representative of Christian inter-
nationalism. The missionary delivers a universal message: the king-
dom of God. He delivers God’s announcement of His sovereignty.
The missionary delivers Gods covenant lawsuit to foreign nations. He
also brings a denominational slant to the message. Unfortunately,
Christian social thought has ignored the missionary for so long—as
if the work of world missions were some sort of peripheral Church
activity —that Christians have for two centuries or more been en-
chanted by spurious nationalism and spurious denominationalism,
