642 POLITICAL POLYTHEISM

sory sanctions, a consistent but seldom articulated viewpoint.) Zn
order to assert his autonomy from God, the covenant-breaker places himself
under the authority of a self-proclaimed autonomous State. He prefers to be-
lieve that the State’s sanctions are final. The State’s sanctions must
be seen.as alternatives to God's final judgment, not evidence for it.
He must assert this if God’s final sanctions are to be denied effec-
tively. In order to make such an assumption believable, the State
must be given power to impose sanctions far worse than those au-
thorized by the Old Testament.

Leaky Christians

It is my view that there are three subordinate tests for orthodoxy,
once a person has affirmed the obvious: the virgin birth of Jesus, the
Trinity, and the bodily resurrection of Christ. These three tests are:
affirming the six-day creation, affirming the worldwide Noachic
flood, and affirming the doctrine of hell (lake of fire), There are other
tests, but anyone who wafiles on one of these should be examined far
more carefully; he leaks. He is going to leak a lot more. A Christian
who is unwilling to affirm publicly the inescapability of God’s eternal
negative covenant sanctions is also unlikely to be willing to insist on
the temporal, negative, and covenantal sanctions, for such temporal
sanctions are an earnest—down payment—on His final sanctions.

Such sanctions-denying Christians eventually find themselves
under the civil (and also intellectual) authority of covenant-breakers
who also deny the continuing validity of biblical law, meaning Old
Testament sanctions. Those who assert their defiance of covenant
law the most insistently are covenant-breakers who affirm the auton-
omy of man, or who at least deny the existence of the God of the
Bible. Thus, in their quest to avoid thinking about God’s eternal tor-
ture chamber beyond the grave, Christians have willingly submitted
in principle to temporal rule by those covenant-breakers who deny the
lake of fire with the greatest self-confidence. You cannot beat some-
thing with something less.

On the other hand, those Christians who in history were most
willing to affirm God’s predestinated, inescapable, eternal sanctions
were also the only ones ready to insist on the covenantal necessity of
legislating the most feared of God’s negative sanctions, public execu-

30. Murray N. Rothbard, For ¢ New Liberty: The Libertarian Manifesto (rev. ed.;
New York: Collier, 1978); Rothbard, The Ethics of Liberty (Auantic Highlands, New
Jersey: Humanities Press, 1983).
