xiv POLITICAL POLYTHEISM

One reason why the Israelites failed to pursue a systematic pro-
gram of world evangelism is because God’s program required cove-
nantal faithfulness, manifested in public justice and personal dealings
—relationships governed by God’s law and enforced covenantally:
family, Church, and State. This program of world evangelism re-
quired the Israelites to obey God’s civil laws both at home and
abroad. Their failure to obey at home led to their failure to bring the
message of spiritual deliverance to the world. Instead, they were
repeatedly delivered by God into the world, as foreign invaders cap-
tured the reigns of power inside the nation, or else captured the peo-
ple and sent them into slavery abroad.

It is my contention that the failure of worldwide Christian evan-
gelism today is to a large degree the result of a similar failure of
God’s people to proclaim and pursue covenantal civil standards for
their own societies. There are at least seven hundred different plans
for converting the world to Christ now in operation.? Not one of
them seems to be working well enough to offer much hope that the
whole world will be converted in time to save the souls of some five
billion non-Christian people who are already alive.

What is the problem? Here is one major problem: if the whole
world were to convert tomorrow to faith in Jesus Christ, American
Christians (where the major centers of evangelism are located in the
twentieth century) would be unable to answer the inevitable ques-
tion: “What should we do, now that we believe in Jesus Christ as
Savior and Lord?” Americans have offered no explicitly biblical cov-
enantal civil model to the world. So, God withholds His Spirit. He
has suffered enough embarrassment on this score already, He waits.
He waits for American Christians to abandon their present mixed
theology: the theology of religious pluralism.

Intellectual Schizophrenia

It is clear to most people that in these final days of the twentieth
century, anti-Christianity is triumphant institutionally and interna-
tionally. In the West and the Far East, humanism controls the
media, the educational systems (especially higher education), civil

2. David B. Barrett and James W. Reapsome, Seven Hundred Plans to Evangelize the
World: The rise of a globat evangelization movement (Birmingham, Alabama: New Hope,
1988). This is a publication of the Foreign Missions Board of the Southern Baptist
Convention.
