This book is dedicated to the
members, living and dead, of the

Reformed Presbyterian Church
of North America (“Covenanters”)

who for 190 years have
smelled a rat in Philadelphia
