Halfway Covenant Social Criticism 181

which exclusively offers the sacraments? If so, there is never a time
when Church and State are supposed to be fused (if this is what he
means by “linked”) in God's kingdom (civilization), not even in the
coming premillennial kingdom that Schaeffer believed in. Church
and State are separate covenantal institutions. The State is supposed
to enforce the specified negative sanctions of biblical civil law by
means of the sword; the Church offers the positive sanctions of the
sacraments and enforces its discipline in terms of the negative sanc-
tion of withholding the sacraments. Schaeffer never got this clear in
his own mind. If he had, he would have had to affirm the New Gove-
nant legitimacy of a theocratic republic.

On the other hand, if he meant Church as ekklesta—“called-out”
Christians—then his argument officially turns the civil government
over to Satan. If Christians as citizens are not required by God to
bring their Bible-based views to bear on politics, and to pass legisla-
tion that conforms to God’s revealed case laws, then anti-Christians
inherit all civil governments by default. Sometimes Schaeffer re-
jected such an idea, and he called for Christian political participa-
tion, but he always rejected the idea that biblical law is to be the
model for Christian legislation. At other times (such as in this pas-
sage), he affirmed the separation of Christianity and State by rejec-
ting the idea of a national covenant, for such a covenant clearly
necessitates public obedience to biblical law.

Second, what did he mean by the word “linked” when he wrote:
*There is no New Testament basis for a linking of church and state
until Christ, the King returns”? He never said what he meant. Did
he mean a legal tie, with the institutional Church giving orders to
civil magistrates? Did he mean that church membership is required
for voting, meaning that only Christians are allowed to give orders
to civil magistrates? He simply did not tell us what he meant. This
necessarily confuses the reader.

Loyalty to Jesus Christ

Third, he correctly observed that the fusion of Christianity (not
the institutional Church) and State “has caused great confusion be-
tween loyalty to the state and loyalty to Christ, between patriotism
and being a Christian.” So what? So has the fusion of family and
Christianity, as Jesus predicted, quoting Micah 7:6.

Think not that I am come to send peace on earth: I came not to send
peace, but a sword. For I am come to set a man at variance against his
