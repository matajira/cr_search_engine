The Strategy of Deception 427

Franklin’s work in Paris, Code Number 72: Ben Franklin — Patriot or Spy?
(1972),° the historical guild was generally hostile, as he related in an
essay I asked him to write in 1976. (An exception was Forrest
McDonald.)*” He says that he received a letter from Thomas Flem-
ing, Washington’s biographer, who had attacked the book in print,
and then wrote to the author: “I will state bluntly here that I have not
read your book because the title speaks for itself. The question it
raises is preposterous.”6 Code Number 72 went out of print and has
remained out of print. The dark side of Franklin’s Paris career is ig-
nored by tenured professional historians.

I deal with Franklin’s theology in Chapter 9, “Franklin’s Theol-
ogy of Union.” I also trace his early Masonic connections. He was
the most prominent Freemason in the colonies from 1734 until Wash-
ington rose to fame.

James Madison

Historian Robert Rutland is correct regarding James Madison’s
view of religion. The former student of Rev. John Witherspoon at
the College of New Jersey had a dream. That dream was the creation
of a secular republic.®® He had spent an extra year in post-graduate
study with Witherspoon studying Hebrew, ethics, and theology,” so
he knew what Christianity is. He wanted no part of an explicitly
Christian republic. (Neither did Witherspoon.) He worked hard to
see to it that such a republic, which existed at the state level under
the Articles of Confederation, would not survive. “He was a guiding
force behind the Mount Vernon Conference (1785) and the subse-
quent Annapolis Convention (1786), where with other ‘choice spirits’
he planned out the set of maneuvers which finally led to the Great
Convention in Philadelphia the following May.””' (Bradford’s refer-
ence to the Mount Vernon conference is one of the few I have come
across. This meeting, where the coup first began to be planned, is
simply ignored in most histories of the Convention.)

66. Englewood Cliffs, New Jersey: Prentice-Hall.

67. McDonald, Review of Code Number 72, William and Mary Quarterly, XXXL
(Jan. 1974).

68. Cited by Currey, “Franklin Legend," p, 145.

69. Robert A. Rutland, “James Madison’s Dream: A Secular Republic,” Free In-
quiry (Sept. 1983), pp. 8-11.

70. Bradford, A Worthy Company, p. 142

71. Dbid., p. 144,
