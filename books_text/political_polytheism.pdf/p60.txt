36 POLITIGAL POLYTHEISM

4. Transcendence/Immanence

We must begin where the Bible does: the creation of all things by
God (Gen. 1:1). We must maintain an absolute distinction between
the Creator and the creature, God is the absolutely sovereign Master
of all that comes to pass in history. Nothing takes place outside His
sovereign decree. “I form the light, and create darkness: I make
peace, and create evil: I the Lorn do all these things” (Isa. 45:7). “I
have made the earth, and created man upon it: I, even my hands,
have stretched out the heavens, and all their host have I com-
manded?” (Isa. 45:12), “For thus saith the Lorn that created the heav-
ens; God himself that formed the earth and made it; he hath estab-
lished it, he created it not in vain, he formed it to be inhabited: I am
the Lord; and there is none else” (Isa. 45:18).

Isaiah uses the familiar (but extremely unpopular) biblical imag-
ery of the potter and his clay: “Woe unto him that striveth with his
Maker! Let the potsherd [strive] with the potsherds of the earth.
Shall the clay say to him that fashioned it, What makest thou? Or
thy work, He hath no hands? Woe unto him that saith unto his
father, What begettest thou? Or to the woman, What hast thou
brought forth?” (Isa. 45:9-10).!8 These words became the basis of
Paul’s argument regarding the absolute sovereignty of God in choos-
ing to save one person and not another. It is the classic argument in
the Bible for the doctrine of election. Paul says of Pharaoh; “For the
scripture saith unto Pharaoh, Even for this same purpose have I raised
thee up, that I might shew my power in thee, and that my name might
be declared throughout ail the earth” (Rom. 9:17). This explains the
words in Exodus: “And he hardened Pharaoh’s heart, that he heark-
ened not unto them; as the Lorp had said” (Ex. 7:13). But this
means that God keeps some men from responding positively to the
universal offer of salvation. This keeps them from obeying His word,

The believer in free will (a degree of human autonomy outside of
God’s eternal decree) then asks: “How can any sinner therefore be
personally responsible for his sin?” Paul well understood this line of
reasoning, to which he replied:

14. This does not mean that God is the author of sin. This verse speaks covenant-
ally: God brings evil times to those who defy Him.

15. Ihave used brackets to indicate the italicized inserted word of the King James
translators. Normally, I do not do this, preferring instead not to disrupt the flow of
biblical language. But my arguments here are sufficiently controversial that I do not
want critics saying that I relied on the translators to make my points,
