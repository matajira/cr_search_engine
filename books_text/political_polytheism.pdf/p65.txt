What Is Covenant Law? At

bow, every tongue shall swear” (Isa. 45:23). His Word is sufficient.
He will not go back on His Word. He has sworn by His own name.
God has therefore taken a covenantal oath that in the future, every
human knee shall bow, and every human tongue shall swear. There
is no escape from God's authority; and therefore all mouths shall
swear — they shall acknowledge His sovereignty, either on earth or in
the afterlife. Even in the lake of fire, they must eternally swear that
God is who He says He is.

God's law is our standard, both individually and corporately.
There are covenantal institutions that are bound by the revealed law
of God: Church, State, and family. These are the three covenantal
institutions that God has established to declare and enforce His law.
All institutions must obey, but these are those that are exclusively
governed by formal oaths before God.

What is an oath? It is the calling down on one’s head the negative
sanctions of God. If a person or covenanted institution disobeys the
law of God, then God comes in wrath to punish the rebels. He comes
in history. This was the warning of the Old Testament prophets. On
the other hand, if men repent and obey, God is merciful and will
bless them. “Your iniquities have turned away these things,” Jeremiah
warned Judah regarding the rain and the harvest, “and your sins have
withholden good things from you” ( Jer. 5:25). The prophets came in
the name of God as covenantal representatives, calling individuals,
as well as representative kings and priests, to repent, to turn back to
God’s law and thereby avoid God’s negative sanctions in history.

The passage above all others in the Bible that describes the his-
torical sanctions of God is Deuteronomy 28. Verses 1-14 describe the
blessings (positive sanctions), and verses 15-68 describe the cursings
(negative sanctions), Understand, these are historical sanctions. They
are not appropriate sanctions for the final judgment. In this sense,
they are representative sanctions of eternity’s sanctions, what Paul called
the “earnest” or down payment of God in history on what must in-
evitably come in eternity (Eph. 1:14).

5. Succession/Continuity/Inheritance

“In the Lorp shall all the seed of Israel be justified, and shall
glory” (Isa. 45:25). Because God is the Creator, His people will in-
herit the earth: “The earth is the Lorp’s, and the fulness thereof; the
world, and they that dwell therein” (Ps. 24:1). (This is point one of
the covenant.) Psalm 25:12-13 provides the covenantal promise:
