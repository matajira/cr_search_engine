Halfway Covenant Historiography 997

point in its history, you always find that there was a time before that
point when there was more elbow room and contrasts weren't quite
so sharp; and that there’s going to be a time after that point when
there is even less room for indecision and choices are even more
momentous. Good is always getting better and bad is always getting
worse: the possibilities of even apparent neutrality are always dimin-
ishing. The whole thing is sorting itself out all the time, coming to a
point, getting sharper and harder.”® This was also Van Til’s position
regarding history, as Dr. Marsden knows. What becomes of political plu-
ratism in the middle of an escalating religious war? And what can prevent
such a war if Lewis is correct regarding the historical process of
“sharpening and hardening” in both the past and the future???

Let me put it bluntly: as covenant-keepers and covenant-breakers become
more consistent in thought and life, pluralism will be shot to pieces in an ideo-
logical (and perhaps even literal) crossfire. Pluralism is a political arrange-
ment based on a temporary religious and ideological cease-fire.
When this cease-fire ends, pluralism also ends. So will the appeal of
its ideology.

Each time a Christian presses the claims of the gospel on a fallen
world, he makes plain the irreconcilable differences. This is true no
matter what he does to “soften the punch,” Thus, we can expect
escalating confrontations as time goes on if Christians preach and
live the gospel, until the final assault on the Church at the end of
time (Rev. 20:9-10). But in the meantime, who becomes dominant:
the covenant-breaker or the covenant-keeper? Our trio of historians
believes that covenant-breakers will; I believe that Christians will.
This may sound like a peripheral debate over eschatology, but it is at
the very center of our conflicting views of American history, as we
shall see. Because our authors do not want to provoke unbelievers,
they prefer to avoid pressing God’s claims on them in the field of civil
government. They have reinterpreted colonial American history in
order to justify their call for Christians to withdraw from the political
arena as people with an explicitly Christian political agenda.

9, C. S, Lewis, That Hideous Strength (New York: Macmillan, 1946), p. 283.

10, That Lewis refused to consider this question publicly is obvious from his
muddled defense of political pluralism in God in the Dock (Grand Rapids, Michigan:
Eerdmans, 1970), pp. 198-99. I have dealt with this in my book, Dominion and Com-
mon Grace: The Biblical Basis of Piogress (Tyler, Texas: Institute for Christian Eco-
nomics, 1987), pp. 148-53.
