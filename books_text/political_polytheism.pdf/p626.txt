602 POLITICAL POLYTHEISM

really do not fully understand this relationship between humanism
and murder, so only a handful of them are willing to challenge abor-
tion publicly. But they sense that abortion is wrong.

Pastors stay away, from this topic in droves. They are afraid to
bring sanctions, such as picketing abortion clinics. ‘hey are more
afraid of the employment sanctions or declining membership eanc-
tions that pro-death or anti-involvement congregations can bring
against them. It is like pastors in the American South in 1859: they
refused to preach against slavery, So, God brought negative military
sanctions from the north. He had done this before. “Then the Lorp
said unto me, Out of the north an evil shall break forth upon all the
inhabitants of the land” (Jer. 1:14).

When the U.S. Supreme Court in 1973 made illegal all state and
local civil sanctions against the practice of abortion, it thereby sanc-
tioned murder. There is no neutrality, This ethical fact is clearer on the
abortionists’ table than anywhere else in America. This is why a
growing number of Christians have been mobilized on this point.
But this is still a minority of Christians in America.

We need prayer in the churches that God will withhold His judg-
ments in history against the United States so that Christians can
gain more time to fight this intolerable evil. If Christians do this,
they will step by step be led to the philosophy of Trinitarian politics
and away from political pluralism. This is what the abortionists failed.
to recognize in time. The tide has begun to turn. It is time for each
Christian regularly to picket a local abortion clinic or cven stand to-
gether with others (under publiely stated church sanctions) in the door-
ways of abortion clinics to close them for a day or more (if possibic).*9

Reform must begin with self-government under God’s law.

Conclusion

We must not come to the Old Testament with a sense of fear and
loathing. The Old Testament provides us with a vision of victory and
the tools of dominion, namely, God’s laws. These laws are not a
threat to us as Christians; they are the foundation of our efforts to
reconstruct society.

Christians have not wanted to think about God’s law. It reminds
them of their sins of commission. It also reminds them of their sins of

49. Gary North, When Justice Is Aborted: Biblical Standards for Non-Violent Resistance
(Ft. Worth, Texas: Dominion Press, 1989).
