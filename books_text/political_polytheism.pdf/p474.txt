450 POLITICAL POLYTHEISM

Political Unitarianism: Rousseau With Factions

By centralizing judicial power under a national government that
prohibited the use of religious oaths as a test for holding national
office, Madison correctly believed that this would break up the abil-
ity of any single denomination to influence local policy permanently
in any question under the national government's ultimate jurisdic-
tion. The doctrine of judicial review — first consistently promoted in
the Federalist?*—coupled with the abolition of religious test oaths,
guaranteed the long-term eradication of the pre-Revolutionary
War's concept of oath-created civil covenants under God. One judi-
cial body —the Supreme Court — could override-the oath-bound “fac-
tionalism” of the various state courts. As it has turned out, the
Supreme Court can also overturn the decisions of state legislatures
and the federal legislature, although this was not fully understood
by the authors of the Federalist.

Understand what Madison assumed throughout: that religious
factions — indeed, all factions—are an essentially surface phenome-
non; they disturb an underlying national unity. In other words, there
ts an inherent unity in man’s political affairs apart from factions. All that is
needed to allow this underlying political unity to flourish is to ex-
pand the geographical boundaries of government in order to absorb
(and therefore offset) more and more factions. Implicitly, this is a
one-world impulse, a view not shared by the nationalistic Framers.

Madison and Rousseau

Such an outlook regarding factions makes Madison an implicit
follower of Rousseau. It is this assumption of a unitary reality
behind factions that undergirds Rousseau’s theory of the General
Will.2° I am not arguing that Madison was a strict follower of
Rousseau. Rousseau thought of all of life as political. Intermediary
institutions are to have no influence in society at all because all of life
is political. Man is a citizen and only a citizen. Madison was not
politicized to this extent. But the two men were agreed in those
cases where the actual exercise of political power was concerned.

25. Writes political theorist Gottfried Dietze: “The Féderalist’s creation of the doc-
trine of judicial review cannot be evaluated too highly.” Dietze, The Federalist: A
Classic on Federalism and Free Government (Baltimore, Maryland; Johns Hopkins Uni-
versity Press, 1960), p. 331.

26. Robert A. Nisbet, Thadition and Revolt: Historical and Sociological Essays (New
York: Random House, 1968), ch. 1: “Rousseau and the Politica! Community.”
