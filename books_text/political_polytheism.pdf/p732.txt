708

POLITICAL,.POLYTHEISM

John Blair

Edmund Burke (British)
Richard Caswell

George Clinton

Gen. James Clinton

Gen. Johann DeKalb

Gen. William Davie

Gen. Mordecai Gist
Benjamin Franklin

Nathaniel Greene

Richard Gridley

Nathan Hale

Alexander Hamilton (probably)
John Hancock

Gen. Edward Hand

Nicholas Herkimer

Gen. James Jackson

John Paul Jones

Jean Paul Lafayette

Henry (Light Horse Harry) Lee
Richard Henry Lee

Morgan Lewis

Gen. Benjamin Lincoln
Robert Livingston

John Marshall

Gen. Hugh Mercer

Jacob Morton

Rev, John Peter Muehlenberg
James Otis

Gen Sam Parsons

William Pitt (British)

Gen. Thomas Proctor

Israel Putnam

Rufus Putnam

Edmund Randolph

Peyton Randolph

Paul Revere

Maj. Gen. Philip Schuyler
Roger Sherman (probably)
Gen. John Stark Baron von Steuben
