648 POLITICAL POLYTHEISM

The greatest, most permanent, and most fundamental of all the diffi-
culties of Democracy, lies deep in the constitution of human nature.
Democracy is a form of government, and in all governments acts of State
are determined by an exertion of will. But in what sense can a multitude ex-
ercise volition? The student of politics can put to himself no more pertinent
question than this. No doubt the vulgar opinion is, that the multitude
makes up its mind as the individual makes up his mind. A host of popular
phrases testify to this belief. The “will of the People,” “public opinion,” the
“sovereign pleasure of the nation,” “Vox Populi, Vox Dei,” belong to this
class, which indeed constitutes a great part of the common stock of the plat-
form and the press. But what do such expressions mean? They must mean
that a great number of people, on a great number of questions, can come to
an identical conclusion, and found an identical determination upon it, But
this is manifestly true only of the simplest questions."

Vox Populi may be Vox Dei, but very little attention shows that there never
has been any agreement as to what Vox means or as to what Populus means,
... In reality, the devotee of Democracy is much in the same position as
the Greeks with their oracles, All agreed that the voice of an oracle was the
voice of a god; but everybody allowed that when he spoke he was not as in-
telligible as might be desired, and nobody was quite sure whether it was
safer to go to Delphi or Donona.*?

The discrepancy between the hypothetical sovereign People and
the representatives of the People is why we have constitutions: to
hold in check those who claim to speak for people who have not yet
had time to bring sanctions or the threat of sanctions against their
spokesmen. Of course, even this is too limited; constitutions exist in
order to place restrictions on a temporary majority, even a huge ma-
jority, at any point in time. As Nisbet says, “Of all the heresies afloat
in modern democracy, none is greater, more steeped in intellectual
confusion, and potentially more destructive of proper governmental
function than that which declares the legitimacy of government to be
directly proportional to its roots in public opinion—or, more ac-
curately, in what the daily polls and surveys assure us is public opin-
ion. It is this heresy that accounts for the constantly augmenting
propaganda that issues forth from all government agencies today —
the inevitable effort to shape the very opinion that is being so
assiduously courted— and for the frequent craven abdication of the

41, Sir Henry Sumner Maine, Popular Government (Indianapolis, Indiana: Liberty
Classics, [1885] 1976), p. 104.
42. Ibid., p. 187.
