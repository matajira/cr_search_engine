Iniroduction 5

Thave taken Calvinist scholars as the representative cases. Why?
For one thing, I am more familiar with modern Calvinist philosophy
and social theory. For another, those closest to you are always the
greatest threat. You have to answer their criticisms and expose their
deviations before you get on with the war for civilization.? Far more
important, however, is the fact that the other Protestant traditions
have been generally less devoted to the academic areas of philosophy,
social theory, and historical studies. Calvinists believe in the absolute
sovereignty of the Creator God over every aspect of the universe,
and they are therefore the only biblical theological movement that
can sustain serious scholarship, generation after generation. What
the Calvinists have written (and I include here Augustine)® has es-
tablished both the intellectual standards and the targets. When the
Calvinists get philosophically “squishy,” we can be reasonably confi-
dent that their imitators and critics in the other Protestant traditions
are even more squishy, and surely no tougher.

Casuisiry, 1673-1973

The problem facing the West today is that for three centuries,
1673 to 1973, Protestant Christians abandoned the intellectual disci-
pline of casuisiry— applied biblical morality— and thereby abandoned
the crucial task of developing an explicitly and exclusively biblical
social theory. *° In the seventeenth century, English casuists had writ-
ten their works in English, aiming their message at the general pop-
ulation. !! They provided elaborate discussions of the general biblical

8. This is why cults, revolutionary organizations, and other hard-core ideclogi-
cal groups tend to excommunicate or purge heretics and apostates more readily than.
attack the “heathen” outside the camp. One must consolidate the center before
spiraling outward. The same screening strategy is used in Christian academia ta
remove all those who are not committed to the program of epistemological com-
promise. For instance, why are there so few six-day creationists teaching in the
science departments of places like Wheaton College and Calvin College, not to men-
tion in 90 percent of the Bible-believing seminaries?

9. Benjamin Breckenridge Warfield, Calvin and Augustine (Philadelphia: Presby-
terian & Reformed, 1956). This is a collection of his journal articles published be-
tween 1905 and 1909.

i0. Some might date the end of casuistry with Samuel Willard’s massive com-
mentary on the Larger Catechism of the Westminster Confession of Faith, A Com-
pleat Body of Divinity (1726).

fi, Thomas Wood, Hnglish Casuistical Divinity in the Seventeenth Century (London:
S.P.C.K., 1952), p. 47.
