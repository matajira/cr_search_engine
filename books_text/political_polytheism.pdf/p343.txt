The Theological Origins of the U.S. Constitution 319

fore the representative of the Church in that era. He did not merely
sign the Declaration of Independence; he symbolically signed his
brightest student’s 200-year (or more) jail sentence for the American
Church.

Witherspoon, in the name of Calvin’s God, substituted Locke's
compact theory of civil government for biblical covenantalism:
society as contractual, not covenantal. He did not distinguish society
from the State. “Society I would define to be an association or com-
pact of any number of persons, to deliver up or abridge some part of
their natural rights, in order to have the strength of the united body,
to protect the remaining, and to bestow others.”® Sovereign men
agree with each other to set up a hierarchy, to pass and enforce laws,
and to bestow rights on others in the future. Here is the Lockean
covenant in all its autonomous grandeur. Society is a “voluntary
compact” among equals.? Most important, his discussion of oaths
was limited strictly to contracts (person to person) and vows: per-
sonal promises between God and an individual. Oaths, he says, “are
appendages to all lawful contracts; . . .”'° He did not discuss cove-
nants as oath-bound contracts among men in-which God is the en-
forcing party. Had he done so, he would have had to abandon Locke
and the whole Whig tradition.

Witherspoon made the assumption that there is a common sense
logical realism that links the logical processes of all men, Christians
and non-Christians. He appealed to this common sense. realism in
his defense of the Christian faith. This was the heritage of eighteenth-
century Scottish rationalism, the birthplace of the right wing of the
Enlightenment.

Because he believed that there is such a realm of neutral human
reason, it was easy for Witherspoon to fall into the trap of believing
in common principles of political philosophy. After all, this was the
common error of a generation of level-headed Scots who were in the

8. John Witherspoon, An Annotated Edition of Lectures on Moral Philosophy, edited
by Jack Scatt (Newark: University of Delaware Press, 1982), Lecture 10, p. 123.

9. [id., p. 124. Slavery was a problem for him, and he took the view that origi-
nal slavery is only valid for those captured in war or lawfully punished as criminals
(pp. 125-26). Here we sec the Old Testament’s influence, not Locke’s. But we are not
obligated to release them, once we find them in slavery. Here we see everyone else’s
influence in the history of man except the Quakers after 1770, not the New Testa-
ment’s. See Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas:
Institute for Christian Economics, 1989), ch. 4: “A Biblical Theology of Slavery.”

10. Zbid., Lecture 16: “Of Oaths and Vows,” p. 177.

 
