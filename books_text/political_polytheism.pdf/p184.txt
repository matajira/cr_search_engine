160 POLITICAL POLYTHEISM

maintain their existing control of this world’s institutions by default.
This means that Satan will not be effectively challenged, worldwide
and in every area of authority, in his doctrine of “squatter'’s rights.”
Jesus won back the rights to the whole world (Matt. 28:18-20), and
transferred the kingdom to the Church (Matt. 21:43), yet the sup-
posed historical impotence of His Church will never allow His peo-
ple to reclaim title in His name covenantally, meaning representatively
(point two of the biblical covenant). :

In a world in which people are born judicially as covenant-
breakers, Christianity necessarily loses institutionally and culturally
by-default if there is no positive feedback for covenant-keeping and
no negative feedback for covenant-breaking. Deny God's covenant sane-
tions in history, and you thereby proclaim the inevitable institutional and cultural
defeat of Christianity in history.7?

Conclusion

No better case study exists of this denial of God’s covenant sanc-
tions than the amillennial theology of Cornelius Van Til. His amil-
lennialism undermined his ethical system, and this inevitably called
into question his life’s work: the destruction of halfway covenant phi-
losophy. He expected his followers to enter the historical battle
needlessly ill-equipped: to fight as God’s earthly representatives
against Satan’s earthly representatives, but without God’s covenant
sanctions in history. He called them to fight something (Satan’s exist-
ing cultural strongholds) with nothing (a worldview that denies
God’s covenant sanctions in history). That very few people have
heeded Van Til’s call to join him in his intellectual battle should not
be surprising; they implicitly recognize that this battle is far more
than merely intellectual. It is at bottom ethical, and therefore it in-
volves a personal confrontation in-every area of life. To enter such a
battle without faith in God’s covenant sanctions in history is the
theological equivalent of joining a squad of Japanese kamikaze pilots

72. While it is true that God might convert the whole world to Christianity, with-
out a uniquely Christian law-order, and without covenant sanctions, there would be
no fundamental change in the nature of world culture. The world’s institutions
would be operated just as they are now: by standards that are not biblical. This is
why the older postmillennialism of Princeton Theological Seminary, like the post-
millennialism of Jonathan Edwards, was uncompelling intellectually and theologi-
cally, It was not tied to biblical law. It was ethically neutral, meaning antinomian, It
was pictistic rather than covenantal.
