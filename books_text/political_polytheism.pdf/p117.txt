Sanctuary and Suffrage 93

Word for the answers to these questions. Either we go to God’s Word
in search of the answers or to man’s word. The two words are
mutually irreconcilable. There is no neutrality.

The political pluralist necessarily refuses to acknowledge that the
Bible is authoritative in civil government. Were he to affirm such a
thing, he would automatically abandon political pluralism. His official
reason for not going to the Bible is that he fears a tyrannical mixture
of Church and State. (I am including Christian defenders of political
pluralism here.) He thinks of Old Covenant Israel as inherently tyran-
nical. Then what of Egypt and Babylon? What of Greece and Rome?
Were they bastions of civic freedom? And if they were, why did they
continually and systematically oppress God’s covenant people? ®

There will always be a Church, in time and eternity. There will
also be civil governments in history. There is no escape from the
questions of the proper judicial relationships between Church and
State. The six key questions are: 1) Who is the sovereign agent (or
agents) who announces each covenant? 2) Who is authorized to de-
fend each covenant? 3) Whose covenant standards will govern each
covenant? 4) By what sanctions? 5) For how long? 6) What is the
proper relation between these two covenantal institutions — the ques-
tion of biblically appropriate mutual sanctions? American Christian-
ity has deferred offering a clear answer to these six questions for over
two and a half centuries.

The Tithe

Consider an example that was never called into question in the
West five centuries ago, and rarely called into question two centuries
ago: State-enforced tithing. Does the State have the God-authorized
authority (note: authority and author are linked) to compel all residents
of a community to pay money to support a specific church, meaning,
inevitably, a politically approved church? Today, few Christians
would regard such State power as morally or legally valid. (I surely
would not.) Why not?

There are many reasons that could be offered: too much power
for the State, too close an association between specific churches and

65, Ethelbert Stauffer, Christ and the Caesars (Philadelphia: Westminster Press,
1955),

66. One reason why only the Church can in history lawfully announce God’s
eternal sanctions is because she alone survives intact as a covenant agency through-
out eternity. She speaks for eternity today because she alone will survive in eternity.
