What Is Covenant Law? 43

ecclesiastical ~ never civil. The fourth—spiritual ease—is historical
but exclusively internal. Why these restrictions on the first four
points? Because the fifth—inheriting the earth—is seen as exclu-
sively post-historical.

Summary

The definition of pro-nomianism must begin and end with the
biblical concept of the covenant. All five points of the biblical cove-
nant must be included in any valid definition of biblical law. We
should not expect to be able to define biblical law without first con-
sidering the Bible’s primary revelation of God’s law: the structure of
the various covenants God has made with men.

Thus, I define “pro-nomianism’ in terms of God’s covenant model:

The belief that God, the sovereign, predestinating Creator, has dele-
gated to mankind the responsibility of obeying His Bible-revealed law-or-
der, Old and New Testaments, and promises to bless or curse men in his-
tory, both individually and corporately, in terms of this law-order, This law-
order and its historically applied sanctions are the basis of the progressive
sanctification of covenant-keeping individuals and covenantal institutions
—family, Church, and State— over time, and they arc also the basis of the
progressive disinheritance of covenant-breakers.

This leads us to the question of the biblical definition of anti-
nomianism, the antithesis of this definition.

“Antinomianism” Defined

We have seen that the biblical definition of God’s law is governed
by the structure of God’s covenant. Thus, the biblical definition of
antinomianism must also be governed by the structure of God’s cov-
enant. If being an antinomian means that you are against the law,
then it must also mean that it is God’s Jaw that you are against, and
God’s law is always covenantal.

To understand what antinomianism is, we can do no better than
to consider the first.revelation in the Bible of the original anti-
nomian: Satan. Satan came to Eve with a proposition: eat of the for-
bidden fruit, and you will become as God (Gen. 3:5). “Run an ex-
periment, and see if this isn’t the case,” he tempted Eve. “See whose
word is authoritative, mine or God's.” He offered her a covenantal
argument, a perverse imitation of the biblical covenant:
