Halfway Covenant Ethics 141

gressively weaker and less influential culturally over time, while the
unrighteous will supposedly become progressively stronger and
more culturally influential. This was Van Til’s explicit argument in
Common Grace (1954). God supposedly says to His eternal enemies, “I
hate you so much, and My haired of you is increasing so rapidly,
that I will let you beat the stuffing out of My people, whom I love
with increasing fervor as they increase in righteous self-knowledge.”
The ways of God are strange . . . if you are an amillennialist or a
premillennialist.#

The Doctrine of Ethical “Unevenness”

Van Til spent his career preaching against natural law philoso-
phy, yet he steadfastly refused to recommend the adoption of biblical
law as the Christian alternative to natural law. He adopted a doc-
trine of ethical cause and effect which, temporally speaking, is inher-
ently pagan. This is strong language, but Van Til’s own arguments
bear this out. In attacking modern apostate ethical theory, he wrote:
“In the first place, it is said that the idea of ethics having anything to
do with externals has been done away with in the New Testament.
We are no longer considered morally impure when we are physically
impure. Then, too, it is not a part of the New Testament teaching,
as it was of the Old Testament teaching, that redemption has any-
thing to do with the external world.”* Yet he himself adopted a simi-
lar view of ethical cause and effect in history—a view which in fact
denied moral cause and effect in history. This is his doctrine of the his-
torical unevenness of ethical causes and external effects:

. according to all non-Christian ethics there is no relation at all between
moral and physical evil, There is thought to be a physical evil that is independ-
ent of man which befalls man irrespective of his moral life. There is a sense
in which this is true. We too believe that those on whom the tower of Siloam
fell were -no greater sinners than others. But on the other hand we do be-
lieve that the fall of man has brought physical evil in the world. And be-
cause we believe this we can also believe that a good moral man, who
suffers physical evil, is not therefore necessarily at a final disadvantage in
comparison with him who, though he suffers no physical evil, is morally
corrupt, In other words, we have, as Christians, a longer range, the range

43. North, Dominion and Common Grace, p. 82.
44. Van Til, Ethics, p. 116.
