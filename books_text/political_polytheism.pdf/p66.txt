42 POLITICAL POLYTHEISM

What man is he that feareth the Loxp? Him shall he teach in the way
that he shall choose (v. 12).
His soul shall dwell at ease; and his seed shall inherit the earth (v. 13).

God is to be feared (point one), God teaches man (subordina-
tion: point two) the required way (point three). The man’s soul shall
dwell in ease (point four), and his heirs shall inherit (point five).
These two brief verses set forth God’s covenant model, and in these
verses we can see the outline of God’s plan of history for covenant-
keepers. This is so simple that a child can grasp it. Unfortunately, as
we shall see, very few theologians have.

My point is that these verses refer to history. The fear of God is
historical. God’s instruction to man is historical. The law applies in
history. The man is spiritually blessed in history: his soul is at ease.
His heirs shail inherit.

Some commentators might agree regarding the historical refer-
ence of points one through three, but object to my view of point four,
Perhaps the focus of the verse is exclusively internal. After all, the
covenant-keeper’s soul is what is spoken of. Perhaps the blessings are
not visible in history. My response is to ask a question: Why.should
point four—spiritual ease —be confined to only the inner person? If
the inheritance is historical, then the spirit’s ease must refer to con-
tentment regarding the past, present, and future. Only if the inherit-
ance will be post-historical could the ease of the soul be legitimately
confined to the internal realm, The covenant-keeper is at ease in his-
tory because he is confident about the future success of those who
share his faith. It is his seed that will inherit.

If the inheritance of the whole earth is merely symbolic of the in-
heritance of God’s resurrected people, then why refer to the inherit-
ance delivered to a man’s seed? In eternity, this inheritance will be
his, too. In short, the primary focus of the passage is on history, not
eternity. Fear God zow. Learn from God now, Obey God’s law now.
Experience spiritual contentment now, Why? Because your spiritual
heirs will inherit in the fudure: in time and on earth.

Yet there are theologians, especially Calvinists in the Continen-
tal (Dutch) tradition and all Lutherans, who insist that this prom-
ised inheritance is strictly limited to the post-final judgment world of
eternity. The first point— the fear of God—is historical, but personal
rather than corporate. The second ~ being taught by God —is histor-
ical, but personal rather than corporate. The third—obeying the law
of God—applies in history, but is exclusively personal, familial, and
