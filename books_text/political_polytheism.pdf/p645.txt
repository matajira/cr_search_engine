Conclusion, Part 4 621

one full church citizenship, which is covenantally representative of
heavenly citizenship (Matt. 18:18).

God wants every nation to have its political citizenship match its
people’s heavenly citizenship. This goal can be achieved positively:
by widespread conversions of political citizens to saving covenantal
faith in Jesus Christ. This new political order can subsequently be
maintained — though not without continuing widespread conversions
—on a judicially negative basis: by removing legal access to the fran-
chise and civil offices from those who refuse to become communicant
members of Trinitarian churches. Very few Christians believe that
the former strategy is possible, and even fewer of them believe that
the latter strategy is moral.

Christians therefore continue to live under apostate civil cove-
nants. They continue to affirm implicitly and even explicitly that
there is one area of life which is immune to the gospel, and required
by God to remain immune from the gospel: civil government. They
really believe that if every voter on earth were a Christian, all civil
constitutions should remain religiously neutral on principle.

Meanwhile, they insist: there is no neutrality. They ignore James’
warning: “A double minded man is unstable in all his ways” (James
1:8).
