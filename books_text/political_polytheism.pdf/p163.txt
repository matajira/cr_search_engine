Halfway Covenant Ethics 139

that those fruits will suddenly appear in their fulness and beauty at the time
of the judgment day. It sometimes happens that the spring is cold and wet
and that in addition to all this a hailstorm sets back the crops. Yet to the
great surprise of all the fall will bring an abundance of fruits. Now this is
true in a much greater degree in the realm of spiritual things. In this world
there is opposition from without and opposition from within while we build
our program. Hailstorms descend upon us and cut all things level with the
ground. There is very little in the way of fruitage that can be seen. Yet we
know that when all the opposition of sin will be removed and the sunshine
of the Son of Righteousness will shine upon it all the time, then there will be
such fruitage as has never been seen in this world.29

Van Til offered the Church this cosmic weather report: “We in-
terrupt our regular programming with this emergency weather
bulletin. This afternoon, Christians can expect wet, cold weather,
followed by hailstones this evening. Tomorrow we will have more of
the same, only worse, Sunny weather is predicted for next week, im-
mediately following the end of the world. Details at eleven.”

Let us understand the nature of amillennialism. Insofar as
eschatology refers to human history, amillennialism ts postmillennialism
Jor covenant-breakers, Covenant-breakers take dominion progressively
in history. (Dispensational premillennialism is also postmillen-
nialism for covenant-breakers, insofar as eschatology refers to the
Christians who live and labor prior to Jesus’ physical second coming,
the so-called Church Age. All their good works will be swallowed up
during the great tribulation period, either immediately before Jesus
returns —the post-tribulation position—or in the seven-year period
which follows the “secret Rapture”: pre-tribulationism.) Postmillen-
nialism is an inescapable concept. It is never a question of cultural
triumph vs. no cultural triumph prior to Jesus’ second coming; it is a
question of which kingdom's cultural triamph.

Ethical Neutrality in History?

Van Til, as with premillennialists and amillennialists generally,
argued that there is no reliable, predictable, statistically relevant
ethical cause-and-effect relationship between covenantal faithfulness
and external, visible covenantal blessings, or between covenantal
unfaithfulness and external, visible covenantal cursings. Van Til ac-

39, Ibid, p. 163.
