The Theological Origins of the U.S. Constitution 345

argue that this god was not the biblical God was doomed to failure.
Before Darwin, this false connection left men under the social and
political dominion of those who had rejected the Bible as the final
voice of earthly authority; after Darwin, it left men under the do-
minion of men who were not even willing to acknowledge the exist-
ence of the stripped-down god of Newtonianism.

The Newtonian system, being Unitarian-Socinian'!® theologi-
cally and mathematical epistemologically, left mankind without a
personal, covenantal God who intervenes in history in order to meet
the needs of mankind. At-best, He intervenes to meet the needs of a
disjointed universe. This Newtonian god really was the distant,
transcendent. god of older high school textbook accounts of Deism.
There was insufficient presence of this Newtonian god with his peo-
ple. He was all system and no sanctions. The quest for an immanent
god led a segment of the Newtonian movement back into pantheism’s
mystical paths. Any segment of Newtonianism that did not go down
these paths eventually headed out to the far shore of atheism.
Newton’s god of gravity — influence at a distance but without physi-
cal connection — was too little for the pantheists and too much for the
atheists. 119

This god of gravity became even too much for Newton to bear as
time went on. Like a dog returning to its vomit, in the second edition
of Opticks (1717), he once again returned to his experimentally unten-
able theory of the “ether” that fills all intermediary spaces. He had to
find a physical means of explaining attraction at a distance.!?° He
had offered this theory in an early paper to the Royal Society (1675),
a paper which had been cogently attacked by Robert Hooke.1?!
Newton had defended this ethereal theory in Book IV of the 1693
manuscript Opticks, but his experiments later concluded that the ex-

118, Socinianism is named after Fausto Paolo Sozzini, who lived in the late six-
teenth century. He proclaimed the doctrine of an omnipotent God, He denied the
atoning nature of the death and resurrection of Jesus Clhrist. Jesus’ influence is pri-
marily moral. It was essentially a Unitarian faith. See “Socinianism,? James
Hastings (ed.), Encyclopaedia of Religion and Ethics, 12 vols. (Edinburgh: T. & T.
Clark, 1920), XI, pp. 650-54.

119, For a study of Newton's religious perspective on the origin of gravity, see
J. E. McGuire, “Force, Active Principles, and Newton's Invisible Realm,” ‘A mbix,
XV (1968), pp. 154-208. Ambix is a scholarly journal devoted to the history of
alchemy.

120, Edwin Arthur Burtt, The Metaphysical Foundations of Modern Physical Science
(Garden City, New York: Doubleday Anchor, [1925] 1954), p. 266.

121, Christiansen, Presence of the Creator, pp. 189, 447.
