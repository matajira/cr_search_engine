388 POLITICAL POLYTHEISM

Judiciary of the States, notwithstanding their national independence
on the State Legislatures are in fact, so dependent on them, that un-
less they be brought under some tie to the Natl. system, they will
always lean too much to the State systems, whenever a contest arises
between the two.”3* He added this comment as debate progressed:
“We are erecting a supreme national government; ought it not be
supported, and can we give it too many sinews?”*6

Hamilion and Rousseau3”

It is to Hamilton's explanation on the need for this loyalty oath
that we must turn in order to see what was really involved. He was
the most eloquent defender of the strongest possible national govern-
ment. In Federalist 27, he stated plainly what was being done by
means of this required oath. A new judicial relationship was being
created by the Constitution: @ direct covenant between the new national
cluil government with the individual citizen, without any intermediary civil
government, (This alteration is generally regarded by legal theorists as
the most important single innovation that the Constitution imposed.
They are wrong; the prohibition of religious test oaths was its most
innovative breakthrough: one nation, under the god of the People,
indivisible, with a civil war to prove it.)

The lack of intermediate governments, social and civil, between
the individual and the national civil government, was the heart of
Rousseau’s concept of the General Will, meaning the heart of Rousseau’s
totalitarianism, as Robert Nisbet and many other scholars have
argued. Because the colonial political and social traditions were
Christian, and therefore decentralist and iastitutéonally pluralist
(though not ethically and confessionally pluralist), the Constitution
would not have been ratified by the existing Congress. The Phila-
delphia conspirators fully understood this. They were ready to aban-
don the colonial Christian tradition of decentralized power.
Hamilton made it clear that the Constitution, when ratified, would
take a major step forward in the direction of Rousseau’s General
Will ideal of weakening intermediary civil governments. He wrote:

35. Max Farrand (ed.), Records of the Federal Convention, I, p. 203; extract in
Founders’ Constitution, TV, p. 637.

36. Records, I, p. 207; idem.

37. See Chapter 9, subsection on “Madison and Rousseau,” pp. 450-52.

38. Robert A. Nisbet, Tadilion and Revolt: Historical and Sociological Essays (New
York: Random House, 1968), ch. 1: “Rousseau and the Political Community.” J. L.
‘Yalmon, The Origins of Totalitarian Democracy (New York: Praeger, 1960).
