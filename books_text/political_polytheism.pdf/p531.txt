“We the People”: From Vassal to Suzerain to Serf 507

troyed in theory by Darwinism and in fact by the twentieth century’s
political wars to control appointments to the Court. The idea of the
legitimate earthly sovereignty of the Court cannot be maintained
once the public loses faith in the heavenly origin of the law.

In short, the incorporation of éegitimate earthly sovereignty was
destroyed by the voters in 1788 when they ratified the Constitution,
with its denial of the legitimacy of a covenantal oath to the cove-
nantal God who alone is the source of all law. Here is what is most
significant covenantally about the Constitution, and therefore most
significant overall. It abandoned the source of legitimacy, the
Creator. The state constitutions on the whole were explicitly Chris-
tian, The Constitution was explicitly non-Christian: Article VI,
Clause III on official federal oaths. The language of natural law in
the Declaration, the absence of any religious test oath in the Articles,
and the concept of the religiously neutral civil compact in the Consti-
tution, began the formal judicial break nationally with Christianity.
The Fourteenth Amendment completed it.

Then came Darwinism. We can accurately date the coming of
widespread unbelief in the United States: 1865-90.*7 With the rapid
philosophical erosion of the traditional eighteenth-century world-
view, the long-term covenantal basis of U.S. Constitutional law was
undermined —I believe permanently.

The Antifederalists’ Warning

Patrick Henry was one of the few critics who sensed the danger.*
As mentioned earlier, Hamilton went so far as to say that “the judici-
ary is beyond comparison the weakest of the three departments of
power,” and he assured his readers that “it can never attack with suc-

47, James Turner, Without God, Without Creed: The Origins of Unbelief in America
(Baltimore, Maryland: Johns Hopkins University Press, 1985), Pt. II.

48.. He warned that the implicit doctrine of judicial review would eventually lead
to a conflict with the common law principle of trial by jury: Elliot, Debaies, III, pp.
539-42. It is one of the strangest ironies of American history that Chief Justice John
Marshall, the man who first declared openly the doctrine of judicial review in Mar-
bury 2. Madison (1803), was appointed Chief Justice by Adams in the fall of 1800. His
predecessor, Oliver Ellsworth, had as a U.S. Senator written the Judiciary Act of
1789, which permitted the Court to overturn acts of state courts and legislatures. He
later became Supreme Court Chief Justice. He resigned in time for outgoing Presi-
dent John Adams to appoint John Marshall to the position. Had Ellsworth waited
just a few weeks before resigning, the likely nominee by Jefferson would have been
Spencer Roane, Henry’s son-in-law, who was a defender of the state's rights posi-
tion. Campbell, Patrick Henry, pp. 367-68.
