650 POLITICAL POLYTHEISM

believe that it is possible for large numbers of people voluntarily to
become theonomists; after all, they haven't!

When that theocratic consensus appears, you can bet your life
(perhaps literally) that the humanists will then try to subvert it by an
elitist conspiracy. We read about such a revolt against Moses and
Aaron in Numbers 16. It was done in the name of the People: “And
they gathered themselves together against Moses and against Aaron,
and said unto them, Ye take too much upon you, seeing all the con-
gregation are holy, every one of them, and the Lorn is among them:
wherefore then lift ye up yourselves above the congregation of the
Lorn?” (v. 3). We read about the final such attempt in Revelation
20:8-9, at the very end of history. These voices of the People are in
favor of democracy for only so long as they can control a majority of
voters by means of a hierarchical elite that pretends to listen to the
People—an elite far more subtle than the Communists’ one-party
dictatorship in the name of the people.

A sovereign agent always acts through spokesmen in a hierarchy.
There will always be an elite: intellectual, educational, military, and
so forth. The question is never elite or no elite. It is always a ques-
tion of which elite, It is a question of the judicial terms of authority,
internal and civil, governing the elite. It is therefore a question of
which sovereign agent authorizes the elite, The Bible is clear: God is com-
pletely, absolutely sovereign over the creation, and men are subordi-
nately, inescapably responsible for their actions. Thus, the goal of
covenant-keepers is to work toward a social order in which every in-
stitution reflects this dual sovereignty, absolute and delegated. It is
the creation of an entire world order that prays, “Thy kingdom
come. Thy will be done in earth, as it is in heaven” (Matt. 6:10).

As a subset of this broad social goal is politics. Politically, the
only legitimate long-term biblical goal is the creation of a worldwide
theocratic republic.“ It is the creation of a bottom-up political order
whose civil courts enforce the law of God, and whose people rejoice,
not because such a law-order is natural but because it is supernatural,

A Christian National Covenant
This raises a valid question: Should Christians who are cove-
nanted to God personally and ecclesiastically seek to persuade their

44. Gary North, Healer of the Nations: Biblical Blueprints for International Relations (Ft,
Worth, Texas: Dominion Press, 1987), ch. 2: “All Nations Under God,”
