Preface XV

governments, and the economies of this world. It controls almost
every visible institution.?

Wherever humanism’s monopoly increases, it becomes ever-
more vicious. The slaughter of a few thousand unarmed Chinese
students by the military in June of 1989 was only a comparatively
mild-mannered manifestation of Communism’s long-term tactics of
terrorizing civilian populations. What was different about this mass
execution was the presence of satellite television, which made visible
to hundreds of millions of Westerners something that had been going
on quietly and systematically in Communist societies since 1917.
What the world saw take place in Tiananmen square is humanism
unchained from the restraint of Christianity.

Nevertheless, for about eighteen centuries, Christian philosophers
and social theorists have tried to smooth over the inherent religious
differences between covenant-breakers and covenant-keepers. Chris-
tian intellectual leaders have repeatedly adopted the philosophical
systems of their mortal enemies in the name of a “higher synthesis,” a
supposedly creative common ground between light and darkness.
Even those scholars who recognize clearly that Christianity and human-
ism are rival creeds based on radically different views of God, man,
law, and time, still cling to the temporarily convenient fiction that
there can be a permanent political cease-fire between the two systems.
Following Rushdoony’s lead, I call this “intellectual schizophrenia.”

Gary Scott Smith

We can see this operational intellectual schizophrenia quite
clearly in the writings of Christian historian Gary Scott Smith, who
calls himself a principled pluralist. He has pointed to the earlier in-
tellectual schizophrenia of American colonial social theorists. First,
he admits that most of the colonists were Christians, and Reformed,
Calvinistic Christians at that:

Reformed theology was carried to the shores of Amcrica by English
Puritans in the 1620s. From the founding of Plymouth to the American
Revolution, about 80 percent of the colonists were adherents of Reformed

3. Only the Islamic Middle East is exempted, Here, a much older enemy of
Christianity rules, and in much of the Middle East, humanism has made major in-
roads. The absence of veils on Muslim women is the most visible manifestation of
this change. Not even the Ayatollah Khomeini could get young Iranian women to
put on veils. There are limits to every tyranny. A change in women’s fashions, once
established, will always mark the limits of any tyranny.
