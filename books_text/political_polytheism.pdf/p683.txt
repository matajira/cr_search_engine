Conclusion 659

matic Christians wanted a definition of Christian Reconstruction
that would enable them to “join the ranks” without giving up their
distinctives. Several objected to the fact that “Christian Recon-
struction” had been defined too narrowly — meaning, I think, escha-
tologically ~ and suggested that the definition should be broadened.
One astute charismatic participant objected to this objection, how-
ever, a former state-wide political organizer. He said this: “The defi-
nition of what Christian Reconstruction is should be offered by those
who developed the position, We ought to do our best not to be
brought under the definition. We want to be able to distinguish our
work from theirs. Each of us needs to be able to say to the public,
‘No, I am not a Christian Reconstructionist.’” He was a wise short-
term Arminian political strategist. He will lose the war, of course,
but he will not audomatically lose the next battle, I commend his sug-
gestion to others who are also short-term strategists. Stay away from
long-term theocracy. You will not like it until you switch your cove-
nantal position. (And eventually you will switch, or your spiritual
heirs will. Isn’t postmillennialism great?)

What I am doing here is to lay down the foundation for a future
comprehensive revolution. I want men to look back at my books and
say, “He laid down the theological, moral, and political principles of
a decentralized, international theocracy. No Protestant before him
ever did.” Somebody has to set forth the ideal — the covenantal ideal —
for politics. Somebody has to tell Christian activists where they are
inevitably headed if they take an implicitly pro-covenantal position
regarding such issues as abortion and private education, and then
try to defend themselves theologically. I might as well be the one to
do it. They will etther become theocrats or quietly drop out of the fight. I have
been saying this throughout the 1980’s,° and leader by fundamen-
talist leader, my prediction is coming true. They are dropping out.
They no longer even attempt to write a theological rationale for their
political views. They know that if they do, they will wind up sound-
ing too much like the Reconstructionists (dangerous) or like Noll,
Hatch, and Marsden (wimpy liberal). So they stay silent.

This leaves the Christian intellectual battlefield to a tiny handful
of theonomists and a large, theologically incoherent, “mentally soft
through disuse” group of Christian humanists. Meanwhile, self-

56, Gary North, “The Intellectual Schizophrenia of the New Christian Right,”
Christianity and Civilization, 1 (4982), pp. 1-40.
