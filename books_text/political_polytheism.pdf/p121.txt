Sanctuary and Suffrage 97

always be prohibited from receiving the subsidy. The question then
is: By what standard should the State decide?

These issues are now becoming clearer to the humanists who
hate the Church, as well as to a much smaller number of Christians,
but the recommended Christian solutions are not at all clear.
Radical humanists would gladly repeal all laws that grant either tax-
exemption or tax-deduction privileges for donors to churches and
Christian schools. There are even a few humanist-influenced Chris-
tians who would agree. But most Christians see where such an argu-
ment easily leads: to the control of the churches by the State. The
power to tax is the power to destroy, announced Supreme Court
Chief Justice John Marshail in 18197 — the case revolved around the
taxing of a federally chartered bank by a state government— and few
Christians would disagree.”) But how can the consistent Christian
defend the lawful sovereignty of a particular church against State
control without also raising the embarrassing question of theocracy?
Who is to say what constitutes a church? By whai standard? -

Ifthe Ghristian says that anyone can set up a tax-immune or tax-
exempt institution designated as a church, whatever the theolagy of
the church happens to be, he has thereby admitted that other reli-
gious faiths are judicially equal to Christianity. Such a latitudinarian
view regarding the establishing of churches allows many “strangers
in the gates” to establish rival institutions that compete on an equal
basis with the true and only covenant Church. This clearly violates
Old Testament law. Everyone recognizes this, so Christians today
have publicly abandoned this aspect of Old Testament law.

But where does this principle of equality end? What about sut-
tee, the Hindu practice of forcing widows onto the funeral pyres of
their husbands? What about polygamy, the Mormon practice which
has never been condemned theologically by the Church of Jesus
Christ of Latter Day Saints, but only suppressed provisionally for
the present? What about some “native Americans’ ” (Indians’) prac-
tice of using hallucinogenic drugs in their worship services? What of
orthodox Judaism’s practice of enforcing laws against work on Satur-
day? (In the state of Israel, radical Orthodox Jews sometimes block

70, McCulloch v. Maryland.

71, Christians need to argue that churches are legally and inherently tax-immune,
Churches cannot legally be taxed, for they are God's lawful sovereigns that adminis-
ter and enforce God’s eternal oath, But this is a different issue from the question of
income tax-deductibility for those giving assets to a church.
