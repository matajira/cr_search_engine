“We the People”: From Vassal to Suzerain to Serf 513

A similar self-restraint has been shown by Congress in removing
the Court's appellate jurisdiction. In fact, the decrease of such asser-
tions of authority has paralleled the increase of the Court's willing-
ness to declare laws unconstitutional. It is clear why: Congress has de-
Served authority to the Supreme Court, A power never announced by the
Constitution (judicial review) has triumphed, and a power clearly
announced by it— Congress’ control over the Court’s appellate juris-
diction—has dropped from the memory of Congress and the vast
majority of citizens.

The source of the Court’s power is the implied doctrine of judicial
review, the idea that in law, as in politics, there must be this sign on
someone’s desk: “The buck stops here.” Again, citing former Chief
Justice Burger, who has set forth this position clearly: “The corner-
stone of our constitutional history and system remains the firm ad-
herence of the Supreme Court to the Marbury principle of judicial
review that ‘someone must decide’ what the Constitution means.”

The Break With the Colonial Past

Sociologist Robert Bellah, in his provocatively titled book, The
Broken Covenant, begins with a chapter titled, “America’s Myth of
Origin.” He speaks of the era of the Revolution, from the Declara-
tion to Washington’s inauguration in 1789, in religious terms: “We
will want to consider the act of conscious meaning-creation, of con-
scious taking responsibility for oneself and one’s society, as a central
aspect of America’s myth of origin, an act that, by the very radical-
ness of its beginning, a beginning ex nzhilo as it were, is redolent of
the sacred.”® He refers to these datable acts as “mythic gestures” that
stirred up images and symbols of earlier myths. The newness of
America is one such myth. So is the wilderness theme. So is reform
and rebirth. So is the promised land and the city on a hill. These are
all biblical images, he says.® (The book is a collection of lectures de-
livered at Hebrew Union College and the Jewish Institute of Religion.)
He recognizes the Augustinian-Calvinist-Puritan roots of the Ameri-
can experiment in freedom.® The Revolution appropriated these
biblical themes by reworking them in a secular mold.

61. Burger, Introduction, William E, Swindler, The Constitution and Chief Justice
Marshall, p. xiii.

62. Robert N. Bellah, The Broken Govenant; American Cioil Religion in Time of Trial
(New York; Crossroad Book, Seabury Press, 1975), p. 4.

63. Ibid., pp. 5-16.

64. Ibid., pp. 17-18.
