134 POLITICAL POLYTHEISM

intellectual heir—he stood alone, by choice, all of his life, battling
bravely but seemingly with little thought about who would succeed.
him —he could hardly specify those who would not be allowed to in-
herit. Like Queen Elizabeth I, he never publicly named a successor.”
This was consistent with his basic operating presupposition: “Every-
one else is wrong.” (Rushdoony subsequently has suffered from this
same dilemma over inheritance.)

The Key Contradiction

No one has said this in print, but it is time to say it: Van Til’s
original system has a glaring contradiction in it, one which doomed
it from the beginning. All his life, he proclaimed the sole and exclu-
sive validity of presupposttional apologetics. What did he mean by the
term? He meant that there are two (and only two) ways of viewing
the world: as a covenant-keeper or as a covenant-breaker. At the
heart of his apologetic method was the fundamental concept of the
biblical covenant. He spoke of man as “a covenant personality.”2
He insisted that “The rational creature of God must naturally live by
authority in all the activities of his personality. All these activities are
inherently covenantal activities either of obedience or disobedi-
ence.”* But Van Til never explained in detail what the biblical covenant is.
(I suspect that this was because he deferred to the writings on the
covenant of his teacher Geerhardus Vos, whose influence over him
was great,?6 and his colleague at Westminster, the Presbyterian eth-
icist John Murray.”’) From time to time, and in many, many places
—just try to locate some key phrase—he did describe in piecemeal
fashion certain of the covenant’s features. The covenant is a legal bond

22. I suspect that he would have preferred Greg Bahnsen, but Bahnsen upheld
the dreaded doctrine of theonomy, even going go far as to name it. To have had
Bahnsen as his successor would have produced the same bothersome result: students
might conclude that theonomy and presuppositionalism are inherently linked, the
idea Van Til was at pains to deny privately and ignore publicly.

23. “Thus Scripture may be said to be the written expression of God’s covenant
with man.” Van Til, “Nature and Scripture,” in The Infullible Word: A Symposium, by
the faculty of Westminster Theological Seminary (Grand Rapids, Michigan: Berd-
mans, 1947), p. 256.

24. Ibid., p. 265.

25. Idem.

26. William White, Jr. Van Til: Defender of the Faith (Nashville, Tennessee:
Thomas Nelson, 1979), ch. 6.

27. Jobn Murray, Principles of Conduct (Grand Rapids, Michigan: Eerdmans,
1957).
