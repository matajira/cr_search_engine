What Is Covenant Law? 37

‘Therefore hath he mercy on whom he will have mercy, and whom he
will he hardeneth. Thou wilt say then unto me, Why doth he yet find fault?
For who hath resisted his will? Nay but, O man, who art thou that repliest
against God? Shall the thing formed say to him that formed it, why hast thou
made me thus? Hath not the potter power over the clay, of the same lump to
make one vessel unto honor, and another unto dishonor? (Rom. 9:19-21).

Paul appealed directly to the biblical doctrine of creation —the
imagery of the potter and the clay—in order to cut short every ver-
sion of the free will (man’s autonomy) argument. There is no area of
chance or contingency in history. None. It is unlawful even to appeal
to this line of reasoning, Paul said: “Who art thou that thou repliest
against God?” The doctrine of the moral and legal responsibility of
man before God must always be understood in terms of the absolute
decree of God; it must never be defended in terms of the idea that
man has a zone of uncontrolled decision-making at his disposal.
Man’s responsibility must be understood therefore in terms of the
biblical doctrine of creation. God decrees, yet men are responsible.

The biblical doctrine of creation teaches the sovereignty of God
in electing some people to salvation. This is why so few Christians
accept the biblical doctrine of the six-day creation, and why they are
ready to compromise with this or that version of evolution. They
want to affirm the partial sovereignty (partial autonomy) of man.
They do so in terms of the pagan idea of chance: a realm of decision-
making, of cause and effect, outside of God’s absolute: providential
control and absolute predestination. They refuse to accept the words
of Paul in Ephesians: “According as he hath chosen us in him before
the foundation of the world, that we should be holy and without
blame before him in love: Having predestinated us unto the adop-
tion of children. by Jesus Christ to himself, according to the good
pleasure of his will? (Eph. 1:4-5).

The biblical doctrine of creation leads directly and inescapably to
the biblical doctrine of the absolute providence of God: God creates
and sustains all things in history. Speaking of Christ, Paul writes:
“For by him were all things created, that are in heaven, and that are
on earth, visible and invisible, whether they be thrones, or domin-
ions, or principalities, or powers: all things were created by him,
and for him: And he is before all things, and by him.all things con-
sist” (Col. 1:16-17). Nothing lies outside the sovereign providence of
God. There is no area of contingency. There is no area of neutrality.
There is no area that is outside the eternal decree of God or the law
