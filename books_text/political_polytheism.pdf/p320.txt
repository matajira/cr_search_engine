296 POLITICAL POLYTHEISM

themselves with hymn-singing on Sunday. In our day, only the
humanists are officially authorized to sing “We shall overcome.”
Postmillennialism is supposedly a valid earthly eschatology only for
covenant-breakers.

Conclusion

Christian political pluralist Gordon Spykman rests his case for
pluralism not on the Bible but on history, since “confessional diver-
sity or religious heterogeneity is a historical reality.” He insists that
we must recognize “the dialectic that exists in history because of sin
and the counter-acting effect of God’s grace; a conflict is occurring
between the city of God and the city of the world, between the king-
dom of light and the kingdom of darkness.”??* He is quite correct,
and this same tension and war existed in Old Testament Israel, in
which political pluralism was forbidden by God’s revealed law.
Strangers in the gate did not serve as civil judges. T. M. Moore,
commenting on Spykman’s presentation, remarks that “his belief
that history provides ultimate norms—creational norms— which
should direct our interpretation of the Bible. Yet such an approach
exalts the analytical powers of man’s mind above the plain words of
the text of Scripture and ignores the supremacy of God’s revelation
over mere human interpretations (cf. Isa. 55:8-9; 2 Pet. 1:20-21).7229
The truth of this accusation applies equally well to The Search for
Christian America.

I argue in this chapter that The Search for Christian America is not a
serious piece of historical scholarship. Yet I also admit that our trio
as individuals had previously written academically reputable books.
In those earlier books and essays, the authors presented evidence
that America was from the beginning a Christian nation, and that
any attempt to deny these origins is comparable to Marsden’s charge
against Perry Miller: an attempt to de-Christianize a self-consciously
Christian people. By the time these men (like myself) reached grad-
uate school, secular historians had done too much research showing
the Christian roots of the United States. It was no longer academically
respectable to promote a pure, unvarnished Progressivist version of

228. Gordon Spykman, “The Principled Pluralist Major Response,” in Gary
Scott Smith (ed.), God and Politics, p. 248,

229, T. M. Moore, “The Christian America Response to Principled Pluralism,”
ibid., p. 110.
