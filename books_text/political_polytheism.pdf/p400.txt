376 POLITICAL POLYTHEISM

People, King, and Parliament

“We the People” can also be interpreted in a more Protestant
fashion. The anti-monarchical Vindictae Contra Tyrannis, by “Lucius
Junius Brutus,” published in 1579, offered a biblical and covenantal
justification for political rebellion. It was translated into English and
published in the year following the Glorious Revolution of 1688.
This book became a familiar reference during the American Revolu-
tion, It asserted the sovereignty of the people above the sovereignty
of kings. One of the sections of “The Third Question” announces:
“The whole body of the people is above the king.” So common were
these ideas among Protestants in the late sixteenth century that even
Richard Hooker appealed to the Vindiciae in his Laws of Ecclesiastical
Polity (1594) in his defense of the divine right of the kings of
England.°® He said that the representatives of the “people’s majesty”
crown the king.!° The king rules by God through the people. He
tules by law, meaning natural law, which is the same as God's re-
vealed law in the Bible. Hooker began his study with a discussion of
natural law, which remained the hypothetical law structure that sup-
posedly serves autonomous man 4s a legitimate substitute for bibli-
cal law.

The Stuart Kings

Within half a decade after the death of Hooker, James I came to
the throne. A. pagan Renaissance monarch to the core,!! James I
asserted the divine right of kings far more forcefully than Hooker
had. He viewed kingship as directly under God, without any refer-
ence to the sovereignty of the people. “It is atheisme and blasphemic
to dispute what God can doe, so it is presumption and high contempt
in a subject, to dispute what a King can doe. . . .”!? This arrogance
did not go without a challenge. In a document published by the
House of Commons in 1604, An Apology, the argument appears that

8. A Defence of Liberty Against Tyrants, Introduction by Harold J. Laski (Glouces-
ter, Massachusetts: Peter Smith, 1963), p. 124.

9. Hooker, Laws of Ecclesiastical Polity, Book VILL, Gh. 2, Sect. 7; in The Works of
Mr, Richard Hooker, arranged by Rev. John Keble, 3 vols. (Oxford: At the Clarendon
Press, 1865), ILL, pp. 347n, 348n.

10. Zbid., IIT, p. 348.

1, Otto Scott, James (New York; Mason/Charter, 1976). 'I'his is now distributed
by Ross House Books, Vallecito, California 95251.

42. Cited by C. Northcote Parkinson, The Evolution of Political Thought (Boston:
Houghton Mifflin, 1958), p. 80.
