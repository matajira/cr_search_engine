690 POLITICAL POLYTHEISM

A fourth aspect of man under law is that Jaw means true order as justice,
The law is justice, and it is order, godly order, and there can be neither true
order nor true law apart from justice, and justice is defined in terms of
Scripture and its revelation of God’s law and righteousness. The law cannot
be made more than justice. It cannot be made into an instrument of salva-
tion without destruction to justice. Salvation is not by law but by the grace
of God through Jesus Christ.**

The issue is justice, not salvation. So, why does he now raise the
spurious issue that the Constitution “can make no man nor nation
good; it is not a moral code”? This is utter nonsense; every law-order is
amoral code. Until he got on national television, this had been Rush-
doony’s refrain for 30 years! As he wrote in the Jnstituées, there is “an
absolute moral order to which man must conform.” He insisted
therefore that “there can be no tolerance in a law-system for another
religion. Toleration is a device used to introduce a new law-system
as a prelude to a new intolerance.”® In this sentence, he laid the
theological foundation for a biblical critique of the U.S. Constitution
as a gigantic religious fraud, a rival covenant, “a device used to in-
troduce a new law-system as a prelude to a new intolerance,” which
it surely was and has become. But he has been blinded for 30 years
by his love of the Constitution. In a showdown between his theocratic
theology and the U.S. Constitution, he chose the Constitution.

Prohibiting Judicial Evil

He says that it will do no good for Christians to appeal to the
Constitution. “The Constitution can restore nothing, nor can it
make the courts or the people just.”* The courts are the enforcing
arm of the Constitution, yet it supposedly cannot make the courts
good. Of course it cannot; but a constitution can and must prohibit
evil, lawless decisions by lower courts. It must reverse all lower court de-
cisions that are not in conformity to the fundamental law of the land.
This is the doctrine of judicial review. This is the whole idea of
American Constitutional law. Rushdoony knows this. In 1973, he
appealed to that crucial covenantal and legal concept: sanctions. He
warned Christians that the concept of treason is inescapably religious:

58. Ibid., p. 144.

59. Rushdoony, Institutes, p. 18.

60. Thid., p. 5.

61. Rushdoony, “U.S. Constitution” p. 39.
