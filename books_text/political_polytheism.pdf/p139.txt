CONCLUSION, PART 1

Thou shalt make no covenant with them, nor with their gods (Ex.
23:32),

This commandment was given to the Israelites regarding their
future conquest of the land of Canaan. They were to exterminate the
Canaanites because of the threat that Canaan’s local, land-based
idols of Canaan would pose to them. The gods of the land in the an-
cient world were always the most threatening to the conquerors,
which is why conquerors either exterminated their enemies or made
slaves of them, sending them back to the home city. There could be
no peace treaty without shared gods.

The Israelites did not at first knowingly make national covenants
with the gods of Canaan, but over time, they adopted the ethical and
ritual practices of these gods. Step by step, they broke God’s cove-
nants; in family, Church, and finally State. From time to time, God
delivered them into the hands of invaders who would reign in the
land. Finally, He allowed invaders to enter the land, capture the
people, and send them into foreign bondage. But He had promised
them that they would not remain in bondage there permanently, for
He had made His covenant with them: “And yet for all that, when
they be in the land of their enemies, I will not cast them away,
neither will I abhor them; to destroy them utterly, and to break my
covenant with them: for I am the Lorn their God” (Lev. 26:44). He
was God over Babylon, too,

The problem in the New Testament era is culturally different.
Christians understand that to worship another god is to break cove-
nant with Christ. So, Satan has invented a remarkably successful
strategy: to persuade Christians that the God of the Bible is the same
god as the one who is discovered by the light of autonomous reason.
If this. god of mankind’s common reason can be foisted off on Chris-
tians as the God of the Bible, then they can be persuaded to establish

15
