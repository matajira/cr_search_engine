Halfway Covendnt Historiography 279

all eighteenth-century colonial Christians had adopted a Newtonian
vision of cause and effect, fusing this view with the remnants of
medieval natural law theory. They did not recognize the enormous
threat to their worldview that any such reliance on Newtonian theory
presented '**— nor do most Christians recognize this today. Apolo-
getics is still conducted as if Kant’s revolution and Darwin’s revolu-
tion had not taken place.1*

By 1983, Noll had toned down his language considerably. Thus,
we read in the book co-edited by Noll, Hatch, Marsden, David
Wells, and John Woodbridge that “It is occasionally said that the
United States was founded on ‘Christian principles.’ While that is
not true in any specific political sense, it is true that certain themes
from the Puritan past contributed to Revolutionary thought. In par-
ticular the Puritan idea of the covenant lent force to some Real Whig
ideas. Puritans had believed that settlers in the New World sustained
a special relationship with God. This conviction gave a moral over-
tone to all of life.”!86 Notice the shift since 1977. Here he speaks only
of the “moral overtone” of Christian ideas; “Real Whiggery” is the
main issue.

Listen to him on the subject of colonial eschatology; he inserts the
historian’s weasel phrase, “to the extent that.”!87 “To the extent that
colonists in 1776 still believed in the divine mission of British North
America, they were ready to interpret Parliament’s administrative
errors as assaults upon God and his people.”!88 In 1977, he had made
it clear that few ideas were more prominent in 1776—and long after
—than the idea of the divine mission of British North America.
Then, over thirty pages later, he admits as much: “Judged by the
number of sermons and books addressing prophetic themes, the first
generation of United States citizens may have lived in the shadow of
Ghrist’s second coming more intensely than any generation since.” !49
Both premillennialism and postmillennialism were preached.1*°

184. James Ward Smith, “Religion and Science in American Philosophy,” in
Smith and Jamison (eds.), Shaping of American Religion, 1, pp. 402-42.

185, Van Til devoted his entire career to exposing and refuting this traditional
apologetic approach.

186. Noll, in Noll, at al, (eds.), Eerdman’s Handbook, p. 134.

187. ‘This is the historian’s version of the economist’s weasel phrase “other things
remaining equal.’

188. Idem.

189. Ibid., p. 167.

190, Jbid., pp. 168-70.
