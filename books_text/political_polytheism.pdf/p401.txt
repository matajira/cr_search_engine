Renewed Covenant or Broken Covenant? 377

the rights of Englishmen are as old as the monarchy, especially prop-
erty rights. “The voice of the peaple is said to be as the voice of
God.”#3 In response, James suspended Commons. The theoretical
and institutional battle between Stuart kings and Parliament began.
Tt ended only with the Revolution of 1688.

In the Puritan Revolution of the 1640's, Parliament conducted its
revolt against James P’s son Charles I in the name of both God and
the people. Obviously, the Jacobite concept of the divine right of
kings had to be jettisoned. But jettisoned in the name of what earthly
agent? The divine-right doctrine always meant that the named agent
would be the final earthly court of appeal. The person of the king
had been that sole agent, Charles I’s father had maintained. Not so,
said Parliament. They reasserted the older Protestant view of the
sovereignty of God as delegated to all civil governments through the
people.'* Nevertheless, during the Restoration period, 1660-1688,
the views of James I resurfaced. In an 1681 address to Charles II by
the University of Cambridge, we read:

We will still believe and maintain that our kings derive not their title from
the people but from God; that to him only. they are accountable; that it
belongs not to subjects, either to create or censure but to honour and obey
their sovereign, who comes to be so by a fundamental hereditary right
of succession, which no religion, no law, no fault of forfeiture can alter or
diminish.

The Trumph of Parliament

These sentiments did not last long. Parliament overthrew Charles
II’s son James II in 1688. Nevertheless, the problem of sovereignty
still remained: someone must speak for the People-Deity in the Peo-
ple’s corporate political capacity. The Parliament asserted that
Parliament’s sovereignty is unbounded. In this political theorists
were following Sir Edward Coke [“Cook”], who had drawn James I's
ire for his defense of absolute Parliamentary sovereignty.

This view of Parliamentary sovereignty was carried down in
William Blackstone’s Commentaries on the Laws of England (1765) to the
era immediately preceding the American Revolution. As we have

13, Gited by Scott, James 1, p. 285.

14. Edmund S. Morgan, inventing the People: The Rise of Popular Sovereignty in Eng-
land and America (New York: Norton, 1988), p. 56.

15, Quoted by John N, Figgis, The Divine Right of Kings (2nd ed.; 1914; reprinted
by Peter Smith, Gloucester, Massachusetts), p. 6.
