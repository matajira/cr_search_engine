22 POLITICAL POLYTHEISM

begin. Christians must regain faith in the Bible as a guide to social
transformation. They must regain confidence in the Holy Spirit as
the agency of social change in New Covenant history. They must
also regain confidence in weekly communion, the place where God
meets with His people judicially. All of this may seem unlikely today.
What is likely, however, is that the ship really is sinking. We have to
do something.

Christians must regain confidence that God exercises power in
history, and that our Bible has the answers. Our God is not a loser,
in time or eternity, nor are His representatives, regenerate Chris-
tans. Triumphalism is a legitimate biblical attitude, but it must first
be tested in the trenches of life. Christians must adopt what I call the
toilet bowl strategy. Until we are willing to scrub toilets, and do it
better than the competition, we are not fit to be civil engineers.“ We
must build high quality, cost-effective tree houses before launching
skyscrapers. We must do a lot of good before we can legitimately ex-
pect to do very well.®

Conclusion.

This book presents the case against any compromise with politi-
cal pluralism, With respect to Christian political pluralism itself,
there is no Bible-based case. From Milton’s Areopagitica (1644) to
Richard John Neuhaus’ The Naked Public Square (1984), there has
been no case. There has not been a single, well-developed, self-
consciously biblical exposition of the case. We have waited patiently
for well over three centuries. Christian political pluralists still have
yet to produce the equivalent of Rushdoony’s Institutes of Biblical Law.
Yet they write as though the dogma of democratic pluralism had been
etched on the back side of the original tablets of the law, as though
Moses presided over an incipient Rhode Island. They write as though
they were sitting upon a mountain of supporting literature. This
reminds me of the gambler’s wad: a roll of thirty one-dollar bills with
a fifty dollar bill at each end, It looks impressive when you first see it,
but watch carefully when he peels off that $50.

61. According to European scholar and former U.N. official Ernst Winter, Chris-
tian women in the Soviet Union for many decades have been assigned this unplea-
sant but necessary task. They are the toilet bow] ladies-of Russia.

62. Golonel V. Doner, The Samaritan Strategy: A New Agenda for Christian Activism
(Brentwood, Tennessee: Wolgemuth & Hyatt, 1988).
