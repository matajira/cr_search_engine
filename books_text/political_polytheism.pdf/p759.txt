communion, 22, 71, 359, 599-600,
661
Communism, xv, xviii, 159.
Communist cell, 405n
compact theory
apostasy, 494
autonomy, 82
broken covenant, 492-93, 539-43
evolutionary, 542
imitation covenantalism, 493-95
judicial apostasy, 494
‘Locke & Witherspoon, 319, 493n
Mayflower Compact vs., $16
Roger Williams, 248
confession
Babel, 83
citizenship &, 595
movement, 57, 559
state governments, 522
without content, 234-35
see also oath
confidence, 458, 612-13, 619
conflict
Civil War, 264-65
consistency &, 226-27
covenantal, 86, 89, 625
escalating, xviii-nix, 4, 228,
232, 250
inescapable, 232
irreconcilable, 266
kingdom, 636
Confucianisin, 637
Congress
boxed in by plebiscite, 520
bypassing, 416, 418-19
First Continental, 635
defers to Court, 513
Framers vs., 491-92
Jefferson &, 524-25
national status, 492
no abdication, 429
overthrown, 444
rabbi's prayer, 686-87
Supreme Court vs,, 511-13
weak leadership, 458
conscience, 126, 253, 292,
449, 462-63

Index 735

consent (political), 249, 378
consistency, 226
conspiracy
citizenship, 74
Constitution &, 31, 416
Convention, 416, 428-29
God's sanctions, 437
historians deny, 436-37
humanists, 650
Jacob on, 432
new Constitution, 569-71
new oaths, 74
personalism, 437
repossession, 87
revolution &, 436-37
silence, 66, 74
successful, 66, 551-52, 657
theocratic consensus, 650
Constantine, 80-81, 536-37, 629-30
Constitution
amending, 101, 224, 293, 364, 411,
560, 568-69, 651
amendments to (Christian), 617,
653
anti-Christian, 681, 692
apostate covenant, 492, 528-29,
561, 681, 703
atheistic, 390-91, 393, 403-10, 493,
676
balance?, 366, 380, 452
“better than Christianity,” 281-82
blueprint, 452
boundaries, 444
Christian?, 370-7t
Christian view, 324
citizenship, 388-89, 393
convention (see Convention)
content of, 535
continuity with past, 408
coup, 485, 564
covenant, 310, 510-11
covenant model, 491, 505
crises, 562
Darwin, 562
Declaration, 311
Deistic, 353
direct legal contact, 388-89
