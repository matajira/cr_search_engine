644 POLITICAL POLYTHEISM

they all to some degree accepted the principle of taxpayer-financed
education, Rushdoony first sounded the alarm among Christian
scholars: that 1) all state-supported, taxpayer-financed education is
inherently immoral and anti-Christian, since it removes the author-
ity over education from parents, and 2) the Unitarians had invented
the public school system to further their own messianic and salva-
tional agenda.*? Sidney Mead also admitted it in 1963: “. . . the
public-school system of the United States is its established church.”

Mann built his case on the doctrine of natural law, natural
rights, and Providence, and he stated this forthrightly, in the lan-
guage of George Washington and the Framers, whose theology he
shared. “I believe in the existence of a great, immortal, immutable
principle of natural law, or natural ethics—a principle antecedent to
all human institutions, and incapable of being abrogated by any or-
dinance of man, —a principle of divine origin, clearly legible in the
ways of Providence as those ways are clearly manifested in the order
of Nature and in the history of the race, which proves the absolute
right to an education of every human being that comes into the world;
and which, of course, proves the correlative duty of every government
to see that the means of education are provided for all.”*

Mann’s program of education was a consistent development of
Madison’s system of factionalizing Christianity. The goal is to
remove faction — meaning sectarianism — from every public institution
and from all public policy. Mann’s program relied on the Madiso-
nian removal of Trinitarian oaths and creeds from public institu-
tions, a policy that had been copied by Massachusetts law in 1833,
His strategy involved the substitution of creedless morality for Trini-
tarian morality. It therefore involved the substitution of common-
ground religion for Trinitarian religion. His was the morality of the
procedurally empty but substantively Unitarian, fully Constitu-
tional, Madisonian pantheon. He articulated this Madisonian strat-
egy in 1848, at the end of his career,

 

I believed then (837), as now, that sectarian books and sectarian in-
struction, if their encroachment were not resisted, would prove the over-
throw of the schools,

32. R. J. Rushdoony, The Messianic Character of American Education: Studies in the
History and Philosophy of Education (Nutley, New Jersey: Craig Press, 1963). Reprinted
by Presbyterian & Reformed, Phillipsburg, New Jersey.

33. Sidney BE. Mead, The Lively Experiment: The Shaping of Christianity in America
(New York: Harper & Row, 1963), p. 68.

34, Mann, “The Tenth Annual Report” (1846), cited by Rushdoony, Messianic
Character, p. 21.
