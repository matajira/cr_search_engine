Introduction 21

to repair the ship or at least to get the passengers into the lifeboat.
But the ship’s officers will blame all their troubles on those who
shout, “Abandon ship!” To-do otherwise would be to blame the leak-
ing ship and their own incompetence. Better to blame the alarmists
on board.

What I am proposing is a social transformation that will take
centuries to develop. The. problem is, the storm clouds are looming
now, We may not have much time to begin the process. By “we,” I
mean Christians in general, What we need to do is get experience in
the little things of social life: establishing private Christian schools;
beginning local social welfare programs, funded by voluntary dona-
tions; launching local political campaigns; starting Bible studies. We
must prove that the gospel works before we can expect non-Christians
or pietistic-Christians to trust the Bible for the larger things.

The problem is, we—a minority, Christian activist “we”—may
find ourselves under the spotlight before we are ready. The interna-
tional financial system could collapse overnight, and the response of
frightened politicians could create an international economic col-
lapse to match the financial collapse. In such a scenario, will
churches be ready to exercise leadership locally? If not, they will get
a lot of “on the job training” in the middle of a massive international
crisis. (If this crisis does turn out to be a debt-created crisis, it will be
fitting and proper. The rise of the Enlightenment was closely con-
nected to the rise of central banking, with the Bank of England as the
model. That experiment in private inflation, bureaucratic expan-
sion, and political control—so popular with economists and his-
torians—began in 1694 and remains with us today.)

Case Laws and Communion

If society in general can begin to see, case by case, that the case
laws of the Old Testament really do bring positive visible results,
people will be far more ready to replace a dying pagan civilization
with a comprehensively, covenantally Christian civilization. Unfor-
tunately, very few Christians have ever heard of the case laws of Ex-
odus and Deuteronomy. Thus, a great educational program must

59, P.G.M. Dickson, The Financial Revolution in England: A Study in the Development
of Public Credit, 1668-1756 (London: Macmillan, 1967).

60. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
‘Texas: Institute for Christian Economics, 1984); Gary North, Tools of Dominion: The
Case Laws of Exodus (Tyler, Texas: Institute for Christian Economics, 1989).
