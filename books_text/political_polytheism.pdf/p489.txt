From Coup to Revolution 465

heritage of Stoic natural law theory had blinded them to the impor-
tance and inescapable nature of covenantal civil oaths as surely as
they have blinded Professors Noll, Hatch, and Marsden.°

The Framers who were the dominant voices at the Constitutional
Convention had a definite goal: to make illegal at the national level
the imposition of Rushdoony’s thesis that theocracy is judicially
mandatory and there must not be toleration: of non-Christian reli-
gions: “The modern concept of total toleration is not a valid Jegal
principle but an advocacy of anarchism. Shall all religions be toler-
ated? But, as we have seen, every religion is a concept of law-order.
Total toleration means total permissiveness for every kind of prac-
tice: idolatry, adultery, cannibalism, human sacrifice, perversion,
and all things else. Such total toleration is neither possible nor desir-
able... . And for a law-order to forsake its self-protection is both
wicked and suicidal. To tolerate subversion is itself a subverstve activtty.”6?
Tolerating Christianity’s subversion: it would be difficult to produce
amore accurate yet succinct description from a biblical point of view
of the result of the Constitutional Convention.

It was the explicitly Christian character of state constitutions that
became the target of the delegates in Philadelphia.

Franklin’s Theology of Union

Benjamin Franklin has been regarded as a conservative Deist.
He was not. When he died, a printed document was found in his
pocket. He had carried it around with him for many years: “Articles
of Belief.” It declared his faith in the plurality of worlds, a widely
held Renaissance doctrine.® The universe is filled with many suns
like ours, and many worlds like ours, the document said. It also an-
nounced his idea that the “INFINITE has created many beings or
Gods, vastly superior to Man... . It may be that these created
Gods are immortal; . . . Howbeit, I conceive that each of these is
exceeding wise and good, and very powerful; and that Each has
made for himself one glorious Sun, attended with a beautiful and ad-
mirable System of Planets. It is that particular Wise and good God,
who is the author and owner of our System, that I propose for the

66. See Chapter 5, above.

67. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 89.

68. Arthur O, Lovejoy, The Great Chain of Being: A Study of the History of an Idea
(New York: Harper & Row, [1936] 1965), ch. 4.
