Conclusion 663

sational response is to assure everyone that Jesus is coming soon
with his life preservers, but that promise is wearing thin. Mean-
while, the humanists are coming to us, one by one, with an anchor
to tie around our right legs. And every time they attach an anchor to
some faculty member from either Wheaton College or Calvin Col-
lege, the victim cries out: “Look, everyone: it floats!” Two minutes
later, all we can see is bubbles.

We have less and less breathing room. ‘To switch metaphors, the
lines between Christianity and humanism are being drawn more
sharply, We have less room to maneuver. C. S, Lewis’ fictional cal-
lege professor said it well: “If you dip into any college, or school, or
parish, or family — anything you like—at a given point in its history,
you always find that there was a time before that point when there
was more elbow room and contrasts weren’t quite so sharp; and that
there’s going to be a time after that point when there is even less
room for indecision and choices are even more momentous. Good is
always getting better and bad is always getting worse: the possibili-
ties of even apparent neutrality are always diminishing. The whole
thing is sorting itself out all the time, coming to a point, getting
sharper and harder.”*!

Sooner or later, the reality of this Vantillian-style prophecy will
dawn on both sides of the two-edged sword of God’s covenant: there is
no neutrality. And when it does, both sides will give political pluralism
the burial it deserves, before it stinks up the commonwealth even
more. And after political pluralism has been in the grave for a cen-
tury or so, people will look back in amazement at the period that
began in Rhode Island in 1636, expanded nationally in 1789, and in-
ternationally in the twentieth century, They will ask: “How could
serious people have believed such naive drivel — that it is possible to
operate a political order apart from a public acknowledgment of God?”

The intermediate strategy question facing Christians today is
this: Which God will they have in mind?

61. C. S. Lewis, That Hideous Strength: A Modern Fairy-Tale for Grown-Ups (New
York: Macmillan, 1946), p. 283.

62. Edward Mortimer, Faith and Power: The Politics of Islam (New York: Vintage,
1982).
