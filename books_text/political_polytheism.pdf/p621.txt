The Restoration of Biblical Pluralism 597

in allocating the revenues from taxation. The biblical model for a
theocratic republic would restrict voting to those who are voting
(i.e., tithe-paying) members of local churches, and who are also tax-
paying citizens. The payment of taxes is not sufficient to gain a per-
son access to the ballot box; resident aliens pay taxes today, but they
do not legally vote. There must be something more: a voluntary
acceptance of the national covenant. So it is with theocratic republic-
anism, but with this added provision: there must also be a voluntary
acceptance of the Church covenant. Strict voluntarism means judi-
cial independence, i.e., adult status. In Old Testament Israel, this
occurred at age 20, when a man became eligible for military num-
bering (Ex. 30:14), There seems to be no New Testament reason to
change this age standard.

Modern humanist political liberals and their Christian academic
accomplices hate such a conclusion of covenantal politics with a pas-
sion because they hate covenant theology with a passion. They hate the
idea of covenant sanctions,

The Theocratic Strategy Is Not Primarily Political

Biblical theocracy is not primarily political, so the biblical theo-
cratic strategy shouldn’t be, either. Theocracy means “God rules.”
God rules theocratically over everything, In God’s kingdom, politics
is supposed to be only a small part of life. Taxes are to be low; the
State’s influence is to be minimal. It should only impose negative
sanctions; it is not to remain a modern welfare State. Family life,
business, education, the arts, leisure, and just about everything else
should be removed from State financing and therefore from direct
State control. It is the humanist who believes in salvation by law, not
the Christian. The humanist believes in the messianic State, not the
Christian. The only reason that theocracy appears today to be pri-
marily a political issue is that the biblical theocrat wants to shrink
the State drastically. This is an affront to the modern humanist, who
equates politics with religion.

The critics of Christian Reconstructionism seldom read any of
the books written by Christian Reconstructionists. Only a few have
been willing to read ten or fifteen, which is the minimum required,
since we have about a hundred available, if you count the journals.
Reading what we have actually written would be too difficult and
time-consuming a task, as honoring the Ninth Commandment so
often is. Slander and misrepresentation are easy. Therefore, I have
