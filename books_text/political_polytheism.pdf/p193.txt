Halfway Covenant Social Criticism 169

ciation with Rev, Carl McIntire was concealed entirely in his wife’s
two (or possibly three)?* biographies, which were expressly written
for a broadly Arminian (“free will”) evangelical audience. Few peo-
ple know that McIntire’s Independent Board for Foreign Presbyter-
ian Missions sent Rev. Schaeffer to Switzerland in 1947. I know of
nothing in print which clearly and without Mrs. Schaeffer's “pre-1950
fog” discusses his life, church connections, theology, and intellectual
development. When asked in 1968, “Where did your husband get all
this?” Mrs. Schaeffer offered a long, rambling disquisition about his
discussions with “existentialists, logical positivists, Hindus, Buddhists,
liberal Protestants, liberal Roman Catholics, Reformed Jews and
atheistic Jews, Muslims, members of occult cults, and people of a
wide variety of religions and philosophies, as well as atheists of a va-
riety of types.” This went on for two pages." What she and he both
refused to mention throughout their mutual career were the sources
of his theology rather than his intellectual targets. Reading Mrs.
Schaeffer's books gives the reader the impression that Francis Schaeffer
the philosopher-critic appeared mysteriously one afternoon after a
walk in the Swiss Alps. That this remarkable transformation ap-
peared after a long struggle with (and rejection of) Cornelius Van
Tif’s rigorously presuppositional apologetics and Carl McIntire’s rig-
orously personal ecclesiastics is nowhere discussed.

Why the Criticism?

My essay on Rev. Schaeffer is mostly critical. I believe that he
gave away far too much ground to the humanists and liberals who
were the targets of his critiques. I believe that his apologetic ap-
proach, like Cornelius Van Til’s, was deeply compromised by anti-
nomianism and by eschatological pessimism. To prove my case, I
have had to take a critical stand against him. This is a one-sided,
specialized essay, not a well-rounded assessment of his personal min-
istry overall. I believe that on the whole, he (like Van Til) fought the
good evangelical fight, given his self-imposed theological handicaps,
his lack of advanced formal academic training beyond seminary, and
his geographical isolation in Switzerland. (To some extent, all three
were advantages: they kept him out of the intellectually debilitating
clutches of the academic compromisers who control the humanities

20. Her book, L’Abri, is a partial biography.
21. Edith Schaeffer, L’46ri (London: Norfolk), pp. 226-27.
