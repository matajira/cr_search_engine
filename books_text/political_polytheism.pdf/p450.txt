426 POLITICAL POLVTHEISM

were influenced by Deism, among other things, is certainly to be
granted, but, unless one charges these statements off as the most ar-
rant kind of hypocrisy, it becomes equally clear that even stronger
colonial influences were at work. Here, in clear and forthright lan-
guage from these men, is Calvinism’s predestination and total provi-
dence, and, at the same time, the near Unitarian exclusion of Christ
from the Godhead. God is not seen as an absentee landlord, and not
only reason but more than reason is appealed to. It becomes clear
that, in view of the mixed linguistic, religious and philosophical
premises, no facile classification can be ventured.”®°

On the contrary, a very accurate “facile” classification can be ven-
tured, the one which Rushdoony appeals to over and over in his dis-
cussion of the French Revolution: the providentialism of the Masonic
theological system. Franklin became the Grand Master of the most in-
fluential Masonic lodge in France, the “Nine Sisters” (Nous Sours),
in 1779.5! He had been there when the lodge initiated Voltaire in
1778, four months before Voltaire died.

First, Rushdoony and all those Christian authors who cite Frank-
lin’s famous prayer request should inform their readers that only three
or four of the delegates voted to sustain it.°} These prayers were
never offered. This indicates the degree of the orthodox theological
commitment of the Framers. Second, we do indeed need to ask: Was
Franklin capable of hypocrisy? He surely was capable of looking the
other way while treason was being committed under his nose.
Throughout his tenure in France as the senior American representa-
tive in Paris, he hired known British spies to serve on his ‘staff,
despite repeated warnings that they were spies. Furthermore, in his
work in Paris to negotiate the peace treaty between England and the
colonies, he steadfastly objected to the insistence of his superiors that
the colonies be granted formal independence by the treaty.®+

When Professor Cecil Currey, who had devoted most of his aca-
demic career in studying Franklin, and who had previously been
considered a well-informed Franklin scholar, wrote his exposé of

60. R. J. Rushdoony, ‘Ihis Independent Republic: Studies in the Nature and Meaning of
American History (Fairfax, Virginia: Thoburn Press, [1964] 1978), p. 6. Emphasis his.

61. Carl Van Doren, Benjamin Franklin (New York: Viking, 1938), p. 655.

62. Ibid., p. 606.

63. Max Farrand (ed.), The Records of the Federal Convention of 1787, 4 vols. (New
Haven, Connecticut: Yale University Press, [1913] 1966), 1, p. 452n.

64, Cecil B, Currey, “The Franklin Legend,” Journal of Christian Reconstruction, TIE
(Summer 1976), p. 136.

65. Cecil B, Currey, Road to Revolution: Benjamin Franklin in England, 1765-1775
(Garden City, New York: Doubleday Anchor, 1968).
