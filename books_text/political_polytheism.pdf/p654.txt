630 POLITICAL POLYTHEISM

Permanent Cease-Fires Are Concealed Surrenders

At the time of the American Revolution, there were three million
people living in the U.S.; of these, 20,000 were Roman Catholics
and about 3,000 were Jews. Who inherited the country? The
Unitarians and their spiritual heirs. They used the myth of neutral-
ity to capture the national government covenantally — which Rush-
doony has always denied*—and therefore politically, which Rush-
doony has always affirmed.’ “So deeply is the myth of neutralism im-
bedded,” he writes, “that to deal realistically and honestly with it is
tantamount to political suicide. Politicians must assure every last
plundering faction of its sanctimonious neutralism while insisting on
its own. Each particular faction, of course, insists on its own impar-
tial, neutral and objective stance while deploring the partisan and
subjective position of its adversaries. All are equally committed to
the great modern myth that such neutrality is possible. This myth is
basic to classical liberalism and to most schools of thought, conserva-
tive and radical, which are derived from it.”* This myth of neutrality
steadily leads to the concept of humanist world government; local
loyalties are not sufficiently broad; true neutrality can only be
achieved through international politics and international morality.?

The Consitutional capture of America by the Unitarians in 1788
was based on the myth of neutrality. They destroyed the biblical
State covenant at the national level, and then quietly, unobtrusively
substituted apostate state and local covenants. This was also done in
the name of neutrality, as Rushdoony has said so well. In short, new
covenant, new god. This new god is revealed by his law-order in Amer-
ican society.

Rushdoony writes that “in any culture the source of the law is the god
of that society.”!" The source of biblical law is the God of the Bible. His
moral character is revealed in His laws— all His laws, not just the
Ten Commandments. Without biblical law at the center of a

5. R. J. Rushdoony, The Nature of the American Sysiem (Fairfax, Virginia: Tho-
burn Press, [1965] 1978), p. 67.

6. See Appendix B.

7, Ibid; ch. 5: “Neutrality.”

8. Ibid., p. 68.

9, Ibid., p. 69.

10, Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: In-
stitute for Christian Economics, 1987), ch. 12: “The Biblical State Covenant.”

1. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 4.
