Conclusion 641

which the national covenant could be judged by God and other na-
tions, Only one idea is more repugnant to modern Christian intellec-
tuals than the idea of judicially binding biblical civil blueprints.”
‘That idea is the doctrine of each person’s predestined eternity. The
two ideas are linked judicially: sanctions. Men do not like to be
reminded by Paul that “the scripture saith unto Pharaoh, Even for
this same purpose have I raised.thee up, that I might shew my power
in thee, and that my name might be declared throughout all the
earth. Therefore hath he mercy on whom he hath mercy, and whom
he will he hardeneth” (Rom. 9:17-18). If God did this with Pharaoh,
He can do it to anyone. This means sanctions.

The mild and gentle negative civil sanctions of the Old Testa-
ment—whipping, restitution, slavery, banishment, and public exe-
cution— are light taps on the wrist compared to eternal screaming
agony in the lake of fire. Covenant-breaking men easily understand
this distinction in intensity. They know what is coming in eternity.
They resent it. Thus, in order to banish from their consciousness the
thought of eternal torture at the hand of an outraged, implacable
God, they feel compelled to banish also the idea that God has estab-
lished civil covenants in history which authorize and require His
lawful representatives to apply the Old Testament’s minimal negative
sanctions. Instead, they have implicitly adopted two other doctrines,
the doctrine of autonomous man and the concomitant doctrine, the
autonomous State.

The State becomes the sole agency authorized by autonomous
man to impose compulsory sanctions. (The only alternative to this
view is the doctrine of zero civil government, meaning zero compul-

29. That no such blueprints exist in the field of economics was the assertion of all
three of the other authors in the book, Wealth and Poverty: Four Christian Views, edited
by Robert G, Clouse (Downers Grove, Ilinois: InterVarsity Press, 1984). The
fourth view—the explicitly, self-consciously, blueprint-insistent Christian one ~ was
mine. I, of course, challenged all three of the others, calling attention to their self-
conscious rejection of any explicitly biblical standards in economic analysis. Not
surprisingly, in less than a year, with the book selling well and our royalties ade-
quate, the neo-evangelical liberals who run InterVarsity pulled the book aff the mar-
ket and sold my company the remaining 6,000 copies at 25 cents per copy, just to
wash their hands of the whole project. That was when I knew who had won the de-
bate. Liberals would never be sv crass as to burn conservative books; they simply
refuse to publish them or, once the mistake has been made, dump them. But their
name is still on the books, and I am making an astounding profit margin on each
sale. And I was able to add this choice footnote, too. Liberals are really suicidal
(Prov. 8:36).
