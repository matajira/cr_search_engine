Halfway Covenant Historiography 225

insist that “At the base of every human culture is a shared set of ‘reli-
gious’ values that help hold the society together.”* (Why do they
place quotation marks around the word religious? They do the same
thing repeatedly with the word Christian. Why such hesitation adopt-
ing these crucial words with the words’ full force? Why do they try to
soften the power of these words by placing them inside quotation
marks?) On the other hand, the authors promise that “for the pur-
poses of civic debate and legislation we will not appeal simply to reli-
gious authority.” A most intriguing goal! But their promise raises a
crucial question: What authority, pray tell, is not religious? (Dr. Marsden
studied apologetics under Cornelius Van Til at Westminster
Seminary. He is aware of Van Til’s arguments regarding the impos-
sibility of religious neutrality. At the heart of Van Til’s apologetic
system is this presupposition: Ad/ authority is at bottom religious, There
is no religious neutrality. I wonder if he has rejected Van Til’s con-
clusions. I believe that he must have. How else could he have signed
his name to such a statement?)

Now, if our three professors of history are, as they insist, also
professors of Christ, why do they believe that those who deny Christ
have a leg (or an epistemology) to stand on? Every knee should bow
at the name of Jesus: “That at the name of Jesus every knee should
bow, of things in heaven, and things in earth, and things under the
earth” (Phil. 2:10). The verse does not say that every knee shall bow
at judgment day, but rather that every knee should bow today, mor-
ally speaking. It is an ethical imperative, not an eschatological
prophecy. So is the requirement that every tongue should confess
that Jesus Christ is Lord (Phil. 2:11).

If our three professors are saying only that we do not need to cite
the Bible every time we speak, they are saying nothing special. I can
legitimately order a sandwich from a waitress without reminding her
that man does not livé by bread alone. (By “man” I of course mean
mankind, as in “the family of man.” I include women in my use of
the word here. No need to offend liberal academic sensibilities at this
early stage!) But our three professors mean something a bit more
substantial. What they are talking about is common grace: the ability
of non-Christians to understand logical arguments even though they
reject the Bible, God, and Christianity.®

4, [bid., p. 44.
5. Ibid., pp. 135-37.
