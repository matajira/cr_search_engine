506 POLITICAL POLYTHBISM

“py the numbers.” The People speak by way of five votes out of a
maximum of nine.

The Court had reversed itself in 143 cases by 1972.#) Of this total,
all but seven instances came after the Civil War.® All but 28 came
after 1913,*° Over half have come since 1941,*4 This process has been
accelerating. The element of judicial discontinuity has now begun to
undermine the concept of the Constitution as fundamental law, as
covenant, Legal scholars have all but abandoned such a view of the
Constitution. Respect for the intentions of the Framers, respect for
the idea that the document’s language is perpetually binding, and
respect for the idea of binding judicial precedent are now all but gone.
This loss of faith has undermined the very concept of Constitutional
legitimacy. But without faith in legitimacy to undergird a legal sys-
tem, self-government becomes anarchy, and the State asserts its will
in the name of power alone. Like the Persian kings of old, whose
word was law, but only for as long as their power could enforce their
word, so is the modern State when the public’s confidence in its judi-
cial legitimacy wanes in response to the assertions of what Glazer
has called the imperial judiciary.**

The doctrine of judicial review was the only available alternative
to the idea of continuing plebiscites. Until the Civil War, the
Supreme Court reigned but did not rule. It only asserted its author-
ity to declare a law unconstitutional twice. But as its arrogance has
increased, and it has attempted to rule, it has become the ever-
changing plebiscite that the Framers feared. But it is a plebiscite of a
majority of nine rather than a majority of the voting public. The
Constitutionally unavoidable doctrine of the Court's legitimate rep-
resentation cannot survive the public’s loss of faith in the existence of
a stable, permanent, fundamental law which is being represented by
the Court. There must be continuity between the voice of the funda-
mental law and the law itself over time. This continuity has been des-

41. Congressional Research Service, Library of Congress, The Constitution of the
United States of America: Analysis and Interpretation (Washington, D.G.: Government
Printing Office, 1972), p. 1797.

42. Ibid., p. 1790.

43. Ibid., p. 1791.

44, Ibid., p. 1793.

45. Robert Bork, Foreword, Gary L. McDowell, The Constitution and Contemporary
Constitutional Theory (Cumberland, Virginia: Center for Judicial Studies, 1987),
pp. vili-ix.

46. Nathan Glazer, “Towards an Imperial Judiciary?” Public Interest (Fall 1975).
