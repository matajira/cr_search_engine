Sanctuary and Suffrage 73

The Stranger as Oppressor

The stranger that is within thee shall get up above thee very high; and
thou shalt come down very low..He shall lend to thee, and thou shalt not
lend to him: he shall be the head, and thou shalt be the tail. Moreover all
these curses shall come upon thee, and shall pursue thee, and overtake thee,
till thou be destroyed; because thou hearkenedst not unto the voice of the
Lorn thy God, to keep his commandments and his statutes which he com-
manded thee (Deut. 28:43-45).

The stranger was allowed to worship foreign gods in the privacy
of his household in Old Covenant Israel. The State did not invade
his home, although he could not proselytize publicly for his god or
gods,”°. These household idols were false gods. They represented
demons who were sources of spiritual bondage and therefore poten-
tial oppression, This is why the stranger had no political rights at all,
meaning rights associated with the exercise of the franchise — voting
-~ because voting is inescapably an aspect of civil rulership or judge-
ship. The voter brings sanctions against the rulers. To grant the
stranger political rights apart from his public rejection of his foreign
gods was inevitably to grant those gods a degree of civil authority in
the land. There is no neutrality, The stranger would seek to impose
different laws because he worshipped a different god.

This political principle is so obvious that only Christians who
have been educated by “strangers” or the strangers’ certified task-
masters within the camp of the faithful will have trouble understand-
ing it. Unfortunately, this means millions of them.

The Politics of Polytheism

Christians today are in bondage. They do not readily understand
this fundamental theological principle: many gods, many moralities,
many laws, This is the political principle of polytheism. It is also the
fundamental principle— seldom or never stated publicly — of politi-

20. This was no great restriction on his religious liberty. Gods in the ancient
world were almost always family gods or gods of a particular city. They might also
be gods of a region. They were not universal gods. This is why the gods of Canaan
wete a serious threat to the Israelites; these gods were gods associated with the land.
On the non-universal nature of pagan gods, consider the military results of the bad
theology given to Ben-hadad: “And there came a man of God, and spake unto the
king of Israel, and said, Thus saith the Loxv, Because the Syrians have said, The
Lorp is God of the hills, but he is not God of the valleys, therefore will T deliver all
this great multitude into thine hand, and ye shall know that I am the Lorn” (I Ki. 20:28).
