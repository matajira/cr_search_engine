266 POLITICAL POLYTHEISM

erarchy. There must be representation in history; the question is: In
what way are historical sanctions brought against the representatives?

Mills correctly identified the nature of today’s illusion: political
pluralism’s philosophy of balanced moral and economic interests,
Political life is far more ruthless than this. He also. identified the
source of this belief: cighteenth-century Scottish Enlightenment phi-
losophy, which undergirded free market economics and the Framers’
pluralist political philosophy.**° (Scottish Enlightenment philosophy
was a secularized version of Scottish common sense realism, which
was the basic philosophy of American Protestant academic theology
until at least the end of the nineteenth century, and really until after
World War I. Mark Noll is a specialist in this field,\*! and one
wonders if he has ever abandoned its overall outlook, Van Til’s
demolition of it notwithstanding.)

Mills argued forcefully that this view of American political insti-
tutions is mythical, that power is not parcelled out in the way that
the pluralists say it is. Ultimately, Mills insisted, the political struggle is
in fact a conflict over moral values. The failure of Americans to under-
stand this, he said, is the heart of the modern political crisis:

The moral uneasiness of our time results from the fact that older values
and codes of uprightness no longer grip the men and women of the corpor-
ate era, nor have they been replaced by new values and codes which would
lend moral meaning and sanction to the corporate routines they must fol-
low, It is not that the mass public has explicitly rejected received codes; it is
rather that to many of the members these codecs have become hollow. No
moral terms of acceptance are available, but neither are any moral terms of
rejection, As individuals, they are morally defenseless; as groups, they are
politically indifferent. It is this generalized lack of commitment that is
meant when it is said that ‘the public’ is morally confused. 13?

Such moral confusion regarding public issues is the inevitable
product of a philosophy which teaches that irreconcilable moral con-
flicts are not inherently religious, and therefore perhaps not inher-
ently irreconcilable. In such a pluralist political order, there must be
no public political appeal to ultimate, God-given moral standards.

130. Zbid., p. 242.

131, Mark A, Noll (ed.}, The Princeton Theology. 1812-1921. Scripture, Science, and
Theological Method from Archibald Alexander to Benjamin Breckinridge Warfield (Grand
Rapids, Michigan; Baker, 1983). His footnotes in the introductions to each chapter
lead the student into the background of Scottish realist philosophy.

132. Mills, Power Elite, p. 344.

 
