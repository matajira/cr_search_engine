From Coup to Revolution 463

verbatim the statement from Massachusetts’ constitution: “And
every denomination of christians. . . .”5” In short, these state com-
monwealths were explicitly designated as Christian.

The Virginia constitution of 1776 was less specific. It affirmed
freedom of conscience, and it recommended “Christian forbearance,
love, and charity towards each other.”5* Virginia had a state-supported
church. Pennsylvania’s 1776 constitution specified that a man’s civil
rights could not be abridged if he “acknowledges the being of a
God.”5° The test oath had been removed through the influence of
Franklin.

Delaware in 1776 was more theologically explicit. “That all persons
professing the Christian religion ought forever to enjoy equal rights
and privileges in this state, unless, under color of religion, any man
disturb the peace, the happiness or safety of socicty.”* Maryland’s
1776 constitution was similar to Delaware's: “. . . all persons, pro-
fessing the Christian religion, are equally entitled to protection in their
religious liberty. . . .” Furthermore, “the Legislature may, in their
discretion, lay a general and equal tax, for the support of the Christian
religion. . . .”6 North Carolina simply affirmed liberty of conscience.
Most states had special confessional oaths for state officials,

The Short-Circuiting of the State Constitutions

The state governments of most of the colonies — always excluding
Rhode Island —combined legitimate Christian oaths and illegitimate
state-financed churches. (It is one of the great ironies of American
history that Rhode Island served as the religious mode! of the Con-
stitutional settlement, yet it was this state’s intransigence after 1783
in the area of commercial policy and its wave of paper money infla-
tion in the mid-1780’s that persuaded the Framers to replace the Arti-
cles. Rhode Island refused to ratify the Constitution until 1790. It
was the outcast of America in the 1780's as surely as it had been the

57. Tbid., p. 383.

58. Fbid., p. 312.

59, Ibid,, p. 329.

60. Philip Schaff, Church and State in the United States (New York: Arno Press,
{1888] 1972), p. 22.

61. Perry and Cooper, of. cii., p. 338.

62. Ibid., p. 349.

63. Ibid., p. 356.
