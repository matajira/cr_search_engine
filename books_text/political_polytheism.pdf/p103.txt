Sanctuary and Suffrage 79

come the political polytheism of the polis. That experiment, too, had
suffered the fate of. political religion: intermittent tyrants.°° This
local polytheism could not be sustained. When each polis was con-
quered, one by one, by Alexander the Great, the Pharaoh returned
once again to his throne. The Roman Emperor was, theologically
speaking, like Alexander: the Pharaoh reincarnate.

The culmination of classical politics came with the Emperor
Gaius Caligula (37-41), less than a decade after the crucifixion of
Jesus. He had probably been the instigator of the assassination of his
predecessor Tiberius by the prefect of the praetorian guards. After
eight months in office, he became very ill; upon recovering, he re-
vealed traces of madness. He proclaimed himself a literal god and
demanded ritual sacrifice, the first Emperor to do so, though not the
last. He bestowed both the priesthood and a consulship on his horse,
presumably to show his contempt for representative institutions. He
raised taxes and executed wealthy people who resisted, confiscating
their property. He announced that he wished that the whole Roman
populace had one head, so that he could cut it off with one stroke.?!
Yet he also tried to promote popular elections, without success.” He
could not be removed from office politically. He too, of course, could
be assassinated. He began the Imperial tradition of having all visi-
tors searched for weapons when they came into his presence.** But
how could he protect himself against his own guardians? He
couldn’t; the praetorians assassinated him in a.p. 41.

Immediately after his assassination, the consuls declared an end
to the emperorship; the now-sovereign praetorian guard ended this
final experiment in political liberty 48 hours later.** This was the last
gasp of meaningful political representation in pagan Rome's history.
Caligula’s symbol of the horse among the dignitaries had been ap-
propriate; political representatives in Rome were by then clearly fit
only for saddles and bridles. Rome’s open pantheon had produced

30. A. Andrewes, The Greek Dyvants (New York: Harper Torchbooks, [1956] 1963).

31. “Gaius Caesar,” Encyclopaedia Britannica, 32 vols. (llth ed.; New York: Britan-
nica, 1920), XI, p. 391.

32, A. H. M Jones, Studies in Roman Government and Lew (New York: Barnes &
Noble, 1968), p. 49

33, Charles Norris Cochrane, Christianity and Classical Culture: A Study in Thought
and Action from Augustus to Augustine (New York: Oxford University Press, [1944]
1957}, p. 132.

34. Edward Gibbon, The History of the Decline and Fall of the Roman Empire (1776),
Milman edition, 5 vols, (Philadelphia: Porter & Coates, n.d.), I, p. 123.
