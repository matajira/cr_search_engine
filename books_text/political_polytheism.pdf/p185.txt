Halfway Covenani Ethics 161

in early 1945. No matter how enthusiastically you shout “Banzai!”
you know where things are headed.

Van Til affirmed forthrightly only the first of the covenant’s five
points: the transcendence of God. He keyed his philosophy to this
crucial doctrine: the Creator-creature distinction. This is why he
revolutionized Christian apologetics. But he adopted a false doctrine
of temporal succession: the progressive triumph of Satan’s represen-
tatives in history. Thus, he had the second point of the covenant in-
correct; representation/hierarchy. While he believed in a hierar-
chical approach to epistemology—mian’s thinking God’s thoughts
after Him—he fervently believed that Satan’s representatives can
successfully run the world throughout history, i.e., successfully
maintain political and cultural power. But how can they do this,
since they deny God’s revealed law (point three), thereby placing
themselves under God’s negative sanctions in. history (point four)?
Van Til did not believe in God’s covenant sanctions in history, and
therefore he could not affirm the power and historical authority of
biblical law, a law-order that cannot be separated from God’s histori-
cal sanctions.

Thus, Van Til laid the intellectual foundations for the acceptance
of political pluralism: the doctrine that God is represented politically
throughout history by those who refuse to affirm His absolute sover-
eignty, His comprehensive (multi-institutional) theocratic rule, His
revealed law, His sanctions in history, and the progressive triumph
of His kingdom in history. This means that he refused to abandon
ethical pluralism in the field of social ethics. Nevertheless, such a
view of political and ethical pluralism is inextricably tied to the doc-
trine of natural law or evolutionary law, i-e., tied to the autonomy of
man. Yet he denied this doctrine throughout his career. Van Til was
a classic victim of intellectual schizophrenia, a schizophrenia pro-
duced by his amillennialism, He got point five of the covenant
wrong, and points two through four toppled, too.

He always said that there is no neutrality in thought or life, yet
he constructed his apologetic system as though there could be escha-
tological neutrality. His denomination affirms that there must be es-
chatological neutrality organizationally. But in Van Til’s case, this
cloak of eschatological neutrality was a cover for a radical pessimism
regarding the earthly future of the gospel, the Church, and Christian
culture. And so we must say with respect to this cloak of eschatologi-
cal neutrality, “The emperor wears stolen clothes!”
