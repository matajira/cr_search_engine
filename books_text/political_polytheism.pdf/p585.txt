ory
A NEW NATIONAL COVENANT

Uf I shall be in the minority, I shall have those painful sensations
which arise from a conviction of being overpowered in a good
cause, Yet I will be a peaceable citizen. My head, my hand, and my
heart, shall be at liberty to retrieve the loss of liberty, and remove the
defects of that system in a constitutional way. I wish not to go to violence,
but will wait with hopes that the spirit which predominated in the revo-
lution is not_yet gone, nor the cause of those who are attached to the revo-
lution yet lost. I shall therefore patiently wait in expectation of seeing that
government changed, so as to be compatible with the safety, liberty, and
happiness of the people.

Patrick Henry (1788)!

Christians lost the battle in 1788. The lawyers in Philadelphia
won it, Christians accepted the ratification of the Constitution, not
just as good losers, but as enthusiastic cooperators. They have yet to
identify their problem, as decade by decade, the American republic
has grown ever-more consistent with the apostate foundation of the
Constitution. Christians find themselves besieged today, and they
vainly expect to get rid of their problems by a return to the “original
intent” of the Framers. On the contrary, what we have today is the
political outcome of that original intent, as Henry warned so long
ago. Darwinism, socialism, and several major wars speeded up the
process of moral disintegration, but the judicial foundation of this
disintegration had been established in 1787-88.

The political question facing American Christians today is this:
How much longer will the Constitution serve as the protector of our

1, Jonathan Elliot (ed.), The Debates in the Several State Conventions on the Adoption of
the Federal Constitution as Recommended by the General Convention at Philadelphia in 1787, 5
vols. (Philadelphia: Lippincott, [1836] 1907), HI, p. 652. Henry remained true to
his word, He remained loyal to the United States, even when he opposed the Federal-
ist Party’s Alien and Sedition Acts at the end of his life. He was opposed to anarchy.
Norine Dickson Camphell, Patrick Henry: Patriot and Statesman (Old Greenwich, Con-
necticut: Devin-Adair, 1969), pp. 426-28.

561
