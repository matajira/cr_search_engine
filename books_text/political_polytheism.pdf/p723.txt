Rushdoony on the Constitution 699

Henry said emphatically of the delegates to the Philadelphia
Convention, “The people gave them no power to use their name.
That they exceeded their power is perfectly clear.”** Rushdoony, for
all his praise of Henry’s Christianity, has steadfastly refused to dis-
cuss the religious and judicial foundation of Henry’s opposition to
ratification. This is not an oversight on Rushdoony’s part. He knows
exactly why Henry objected. Henry recognized that the Constitu-
tion rested on a revolutionary break from American law. It was a
broken covenant.

The Constitution was ratified under the presumption of the sov-
ereignty of the people. But this was more than mere presumption: it
is right there at the beginning of the document. Here is why there is
no Trinitarian oath in the Constitution: the Framers were operating
under the legal fiction that the sovereign People, not the God of the
Bible, had authorized the new national covenant.** “We the People”
were not the vassals of the Great King in this treaty; “We the People”
were the great king, and there shall be no other gods beside “We the
People.” Thus, the Framers outlawed religious oaths. Outlawed! Yet
this crucial Constitutional provision is rarely mentioned today. The
humanist defenders of the Constitution automatically assume it, and
the Christian defenders either do not recognize its importance or else
do not want to face its obvious implications. Instead, the debate has
focused on Congress and the freedom of religion. This provision is
not the heart of the Constitutional covenant; it is merely an applica-
tion of it. The oath is central in any covenant.

 

Only Earthly Sovereignty

It was hardly the case that the Framers had no concept of earthly
legal sovereignty. It was that they had only a concept of earthly legal
sovereignty. They announced a divine right—not of kings, not of leg-
islatures, but of the People. The divine right of kings doctrine meant
that no one and no institution could appeal any decision of the king;
he was exclusively sovereign under God. This was exactly what the
oath of Article VI, Clause 3 was intended to convey: ne appeal. The
national government was the final voice of the people, for it operated

84. Iam using the version in Norine Dickson Campbell, Patrick Henry: Patriot and
Statesman (Old Greenwich, Connecticut: Devin-Adair, 1969), p. 338. This statement
appears in The Debates in the Several Stale Conventions on the Adaption of the Federal Consti-
tution as Recommended by the General Convention at Philadelphia in 1787, edited by
Jonathan Elliot, 5 vols. (Philadelphia: Lippencott, [1836] 1907), UL, p. 22.

85. Edmund §. Morgan, Inventing the People: The Rise of Popular Sovereignty in Eng-
land and America (New York: Norton, 1988).
