From Coup to Revolution 447

may be presumed to depend on the extent of country and number of
people comprehended under the same government.”? Epstein is cor-
rect: “It is clear from Madison’s previous versions of Federalist 10's ar-
guments that religious factions were his primary concern among
opinionated parties.”!° Epstein unfortunately did not follow through
on this cogent observation.

Madison’s Fear of Trinitarian Society
Madison expressed his concern over consolidated churches in a
letter to William Bradford of Philadelphia in 1774:

If the Church of England had been the established and general religion
in all the northern colonies as it has been among us here, and uninterrupted
tranquility had prevailed throughout the continem, it is clear to me that
slavery and subjection might and would have been gradually insinuated
among us. Union of religious sentiments begets a surprising confidence,
and ecclesiastical establishments tend to great ignorance and corruption; all
of which facilitate the execution of mischievous projects,

But away with politics! #

Away with politics? It is clear that politics was the context of his
discussion of churches. Madison was judicially unconcerned about
religion as such; he was very concerned about politics. In this sense,
he was a consistent secular humanist, and has been correctly iden-
tified as such.!* He railed against the “pride, ignorance, and knavery
among the priesthood, and vice and wickedness among the laity.” He
then said, “I want again to breathe your free air.”!* In these sen-
timents, he revealed himself as a true independent Whig dissenter.

Several states had created established churches. Pennsylvania
was an exception in 1774—“free air.” Within any one state, a single
denomination could gain special powers or favors. Rather than
merely oppose compulsory state financing of churches, as he did in
1779 and 1785'*—a worthy and legitimate politival goal, biblically

9, The Federalist, edited by Jacob E, Cooke (Middletown, Connecticut: Wesleyan
University Press, 1961), pp. 351-52.

10. David F. Epstein, he Political Theory of The Federalist (Chicago: University of
Chicago Press, 1984), p. 76.

11, Madison to Bradford (Jan. 24, 1774), Mind of the Founder, p. 3.

12, Robert A. Rutland, “James Madison's Dream: A Secular Republic,” Free fn-
quiry (Spring 1983).

13, Mind of the Founder, p. 4.

If. Dbid., p. 8
