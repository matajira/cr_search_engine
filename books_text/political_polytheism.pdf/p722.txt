698 POLITICAL POLYTHEISM

vinist.” He offers no evidence for this statement. The thesis is suffi-
ciently peculiar that some reference to primary source documenta-
tion is mandatory, but none is offered. He refuses to define what he
means by “legal sovereignty,” which makes things even more difh-
cult, He cites some historians on Americans’ opposition to the sover-
eign State, but it is clear from the context that their hostility was to a
centralized, monopolistic sovereignty, which is not the point Rush-
doony is trying to make.

The question Rushdoony has been attempting for three decades
to avoid answering from the historical record is this one: Why did
the Framers refuse to include a Trinitarian oath? If the states had
such oaths—and they did—and the Patriot party regarded the col-
onies as legal, sovereign civil governments under the King, which is
the thesis of This Independent Republic, then why not impose the oath
requirement nationally? The presence of an oath is basic to any cov-
enant, as Rushdoony knows. The question is: Who is the iden-
tifiable sovereign in the federal covenant? And the answer of the
Framers was clear, “We the People.” Not we the states, but “We the
people.” It is right there in the Preamble.

We the People

Patrick Henry had been invited to attend the Philadelphia con-
vention, but he had refused. He recognized what was implicitly
being asserted in the Preamble. In the Virginia debate over ratifica-
tion in 1788, he spoke out against ratification. He warned against the
implications of “We the People”:

Give me leave to demand, what right had they to say, “We the People,” in-
stead of “We the States”? States are the characteristics, and the soul of a
confederation, If the States be not the agents of this compact, it must be onc
great consolidated national government of the people of all the States. . . .
Had the delegates, who were sent to Philadelphia a power to propose a con-
solidated government instead of a confederacy? Were they not deputed by
States, and not by the people? The assent of the people, in their collective
capacity, is not necessary to the formation of a federal government. The
people have no right to enter into leagues, alliances, or confederations: they
are not the proper agents for this purpose: States and sovereign powers are
the only proper agents for this kind of government. Show me an instance
where the people have exercised this business: has it not always gone
through the legislatures?. . . . This, therefore, ought to depend on the con-,
sent of the legislatures.
