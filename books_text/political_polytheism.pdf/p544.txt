The natural leaders of the American people were at last assembled for
the purpose of deliberating upon the whole question of the American
state. They closed the doors upon the idle curiosity and the crude crit-
icism of the multitude, adopted the rule of the majority in their acts,
and proceeded to reorganize the American state and frame for it an
entirely new central government. . . . This was the transcendent re-
sult of their labors. It certainly was not understood by the Confederate
Congress, or by the legislatures of the commonwealths, or by the public
generally, that they were to undertake any such problem, It was gen-
erally supposed that they were there for the purpose simply of improv-
ing the machinery of the Confederate government and increasing
somewhat its powers. There was, also, but one legal way for them to
proceed in reorganizing the American state as the original basis of the
constitution which they were about to propose, viz; they must send the
plan therefore, as a preliminary proposition, to the Confederate Congress,
procure its adoption by that body and its recommendation by that body
to the legislatures of the commonwealths, and finally secure its approval
by the legislature of every commonwealth. The new sovereignty, thus
legally established, might then be legally and constitutionally appealed
to for the adoption of any plan of government which the convention
might choose to propose. The convention did not, however, proceed
in any such manner. What they actually did, stripped of all fiction and
verbiage, was to assume constituent powers, ordain a constitution of
government and of liberty, and demand the plebiscite thereon, over the
heads of all existing legally organized powers. Had Julius or Napoleon
committed these acts they would have been pronounced coup d'état
[sic]. Looked at from the side of the people exercising the plebiscite, we
term the movement revolution. The convention clothed its acts and as-
sumptions in more moderate language than I have used, and professed
to follow a more legal course than I have indicated. . . . Of course the
mass of the people were not at all able to analyze the real character of
this procedure. It is probable that many of the members of the conven-
tion itself did not fully comprehend just what they were doing. .. .
Really, however, it deprived the Congress and the legislatures of all
freedom of action by invoking the plebiscite. It thus placed those bodies
under the necessity of affronting the source of their own existence un-
less they yielded unconditionally to the demands of the convention.

John W. Burgess (1893)*

"Burgess, Political Science and Comparative Constitutional Law, vol. 1, Sovereignty and
Liberty (Boston: Ginn & Co., 1893), pp. 104-6.
