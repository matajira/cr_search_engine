oath, 379, 459
perpetual union, 407, 16
representatives, 384
sanctions, 380
scrapped, 379, 414-15
sovereignty, 378
unanimity, 458
arrogance, 612
assassination of Emperors, 79-80
Assyria, 635
Athanasian pluralism, 332, 339, 529,
594-97
atheism, 334-35, 348, 369, 376, 392,
394, 400, 403-10, 566
Athens, 62, 83
Augustine, 5, 12, 14, 513, 588-89
authority
doctrine of, 91
evolving, 505
final, 504
humanism, 666
interpretation, 381
religious, 225
Rushdoony of, 700
Williams vs., 245
see also hierarchy
autonomous man
antinomian institutions, 40
autonomous State, 641-42
chaos or tyranny, 302
democracy, 102
enthronement of, 102
humanism's god, 208
judge of history, 33-34
morality of, 575
natural law, 131, 376
political pluralism, 85, 105-6
polytheism of, 82
priests of, 2
procedural religion, 542
rationalism vs, irrationalism, 334
sanctions, 639
shrine of, 575
subordination of, 46
Tower of Babel, 106
autonomy
chance, 45

Index 729

contract theory, 82
creationism vs, 37
Enlightenment, 677
ethics, 352

free will, 36

frustration, 674

God or man?, 110

God's sanctions, 640
humanism, 158

King’s X, 298

law, 131

neutrality, 640
Newtonianism, 362
partial, 37

pluralism, 102

political pluralism, 85, 292
political polytheism, 82, 158
relativism, 158, 674

State, 158, 641-42
theocracy, 208

Baal, 107, 421, 634-35

Baalism, 565, 653

Babel (see Tower of Babel)

Babylonians, 635

Backus, Charles, 494

Baehr, Ted, 176-77

Bahnsen, Greg L.
Holy Spirit, 150-51
political polytheism, xii
postmillennialism, 50n, 56, 59
Van Til, 134n

Bailyn, Bernard, 331-32, 378

Bank of England, 21

banking, 552-53

Bakker, Jim, 209n

baptism, 598-99

Baptists, 526, 539

Barlow, Joel, 364

Barth, Karl, 53, 128n, 233-34, 680-81

bat, 654

Baxter, Richard 107, 576

Beard, Charles A., 455

Becker, Carl, 369, 528

Belcher, Jonathan, 547-48

Bell, Daniel, 263

Bellah, Robert, 513-14
