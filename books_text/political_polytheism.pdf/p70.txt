46 POLITICAL POLYTHEISM

the name of a cosmic experiment. What would happen if she dis-
obeyed? Good things, he promised.

“Trust me,” Satan said. “Take my word for it.” In other words, “I
lay down the true law.” Man thinks that he is disobeying God on his
own account, in his own authority, but in fact, man must serve only
one master. Ethically, he subordinates himself to Satan when he refuses
to obey God. He comes under the hierarchical rule of another mas-
ter. Man may think he.is acting autonomously, but he in fact is sim-
ply shifting masters. God or Baal? This was Elijah’s question (I Ki.
18:21). God or mammon? This was Jesus’ question (Matt. 6:24).

But neither God nor Satan normally appears to an individual.
Each sends human representatives, Men represent God in positions of
corporate responsibility. God has established three monopolistic insti-
tutions: Church, State, and family. The head of each can serve God or
Satan, and those under him are sanctified (set apart) institutionally.

Soldiers live or die in terms of decisions made by their superiors.
Nations rise and fall in terms of the decisions of their national lead-
ers. An individual’s success or failure in history cannot be discussed.
without reference to the institutional hierarchies above and below
him, and their success or failure. Thus, to deny that God's law ap-
plies to your covenantal superior is another way of saying that it
really does not apply to you. “I was just following orders!” says the
subordinate who has sinned. In other words, “I was under someone
else’s authority — someone other than God,”

Uriah the Hittite was a righteous man. He died because he was
so righteous. Unrighteous King David told unrighteous General
Joab to be sure that Uriah died in battle, and Joab carried out the
order (II Sam. 12). In short, covenantal hierarchy is important.

David later decided to number the people. This was against God’s
law. Joab warned him about this, but David insisted, so Joab carried
out the order. God’s prophet then came to David and announced one
of three judgments: seven years of famine, three months of David’s
fleeing before his enemies, or a three-day pestilence. Take your pick,
the prophet said. David was too proud to accept the mild but person-
ally humiliating second sanction, so he gave God the choice. God
sent the worst one, nationally speaking: a plague that killed 70,000
people (II Sam. 24). (Anyone who teaches that God does not send
sickness to His people has a real problem in explaining this passage.)
In short, covenantal representation is important.
