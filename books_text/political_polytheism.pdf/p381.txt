The Theological Origins of the U.S. Constitution 357

men’s church comniunion in the thinking of the Calvinist revivalists.
Their Arminian colleagues agreed. This opened the door to Armin-
ianism and then, when the fires cooled, to Deism and rationalism. It
established “hot gospelling” as the basis of evangelism. The least-
common-denominator principle took hold, until people fell to their
knees and barked like dogs for Jesus. In the next century, “Old
School” Calvinist Charles Hodge referred to this as “the leaven of en-
thusiasm.” As he said, such outbursts were opposed by Jonathan
Edwards, the Boston clergy, by Gilbert Tennent, and others (though
initially, not by George Whitefield).!”! Hodge defended the Presby-
terian Church’s disciplinary structure and its essentially judicial,
covenantal theology in opposing such antinomian outbursts of re-
vivalism. Hodge spoke for the orthodox, hierarchical Church of all
ages against antinomian lawlessness when he wrote:

Those under its influence pretended to a power of discerning spirits, of
deciding at once who was and who was not converted; they professed a per-
fect assurance of the favour of God, founded not upon scriptural evidence,
but inward suggestion. It is plain that when men thus give themselves up to
the guidance of secret impressions, and attribute divine authority to sug-
gestions, impulses, and casual occurrences, there is no extreme of error or
folly to which they may not be led. They are beyond the control of reason or
the word of God.172

He clearly had in mind Presbyterian revivalist Gilbert Tennent,
a founder of the Log College, which became the College of New
Jersey, and finally became Princeton College in the late nineteenth
century, who wrote The Danger of an Unconverted Ministry (1741). He
accused his creed-proclaiming, jurisdiction-protecting fellow Pres-
byterians of being reprobates and “Old Pharisee-Teachers.”!’* They
had “exerted the Craft of Foxes,” and had displayed “the Cruelty of
Wolves.”!7* “The old Pharisees, for all their long Prayers and other

171, Charles Hodge, The Constitutional History of the Presbyterian Church in the United
States of America, 2 vols. (Philadeiphia: Presbyterian Board of Publication, 1851), II,
p. 82.

172. Ibid., IL, p. 83.

173. Gilbert Tennent, “The Danger of an Unconverted Ministry” (1741), in
Heimert & Miller, Great Awakening, p. 73.

174, Ibid., p. 74.
