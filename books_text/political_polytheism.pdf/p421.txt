Renewed Covenant or Broken Covenant? 397

moral and political theories, intuitions of public policy, avowed or
unconscious, even the prejudices which judges share with their fellow-
men, have had a good deal more to do than the syllogism in deter-
mining the rules by which men should be governed.”*' This was put
less academically and more memorably by the fictional Mr. Dooley
(humorist Finley Peter Dunne) in the early years of the twentieth
century: “The Supreme Court follows the election returns.”

The ambivalence of eighteenth-century Scottish moral philoso-
phy regarding the judiciary as a field independent from politics now
has been answered: it is not independent from politics; it is an arm of
politics. Witherspoon had warned Madison about this, but Madison
and his colleagues did not take the brief warning seriously enough,

This failure of procedural structure to match the speed of social
change has become a familiar theme of liberalism. As an heir of both
Madison and Holmes, Clinton Rossiter, known (incorrectly) as a
conservative scholar, dismisses the Articles of Confederation:

Although handicapped in many ways in the battles of rhetoric and political
maneuver with the fearful republicans, the nationalists had one advantage
that, in the long run and therefore in the end, would prove’ decisive: they
knew, as did many of their opponents, that the prescriptive course of
nation-building in America had run beyond the Articles of Confederation
to serve national needs, By 1787 . . . the constitutional lag had become too
exaggerated for men like Washington and Madison to bear patiently,

‘This is the same criticism that we hear today regarding the Con-
stitution, which is an ominous political indication of a constitutional
crisis in the making.

Locke’s Legacy: Life, Liberty, and Property

Locke's “covenant formula” — life, liberty, and property —echoes
down through the centuries in the Fourteenth Amendment. Jeffer-
son’s insertion into the Declaration of Independence the phrase of

61. Oliver Wendell Holmes, jr., The Common Law (Boston: Little, Brown, [1881],
1923), p. 1. My aging copy (undated) was listed as the 47th printing.

62. Witherspoon wrote: “Moral philosophy is divided into two great branches,
Ethics and Politics, to this some add Jurisprudence, though this may be considered
as a part of politics.” John Witherspoon, An Annotated Edition of Lectures on Moral Phi-
losophy, edited by Jack Scott (Newark: University of Delaware Press, 1982), Lecture
1, p. 65.

63. Clinton Rossiter, 1787: The Grand Convention (New York: Norton, [1966]
1987}, p. 38.

  
