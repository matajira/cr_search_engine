526 POLITICAL POLYTHRISM

sea journey drastically reduced the supply of Anglican pastors in the
colonies. (The Presbyterians faced a similar constraint: either ad-
vanced education in Scotland or at the tiny College of New Jersey.
This restriction was later to cripple their missions program in the
West, from which the church never recovered. The Baptists and
Methodists captured the West and thereby the nation for Roger
Williams’ commonwealth vision.)® The colonists suspected that this
move by the Anglican Church was an attempt to strengthen Anglican-
ism and therefore the English crown, for the King was the head of
the Church.” Thus, the original Erastian error of Reformation Eng-
land—a national church with the civil sovereign as its head—had at
last led to a major political crisis. This, too, was an aspect of God’s
historic sanctions. When Saul had offered a sacrifice instead of Sam-
uel, he violated God’s law (I Sam. 13:9-15), The same was true of
Uzziah (II Chr. 22:19). Saul lost his kingdom, Uzziah was stricken
with leprosy. George III lost a war and claims to most of a continent.

Christians all over the world in the mid-eighteenth century still
believed that it was necessary for the State to finance the Church.
This placed the Church economically under the State to some
degree. Christians, then as now, did not understand that the State is
a ministry of God for the suppression of evil—a covenant institution
which is supposed to impose exclusively negative sanctions. By mak-
ing the State into an organization like the family or Church —an in-
stitution imposing positive sanctions — Christians created a perverse
institution that could masquerade as a blessing. It was a curse in
disguise, a wolf in sheep’s clothing. It still is. (Fortunately, it is a
nearly bankrupt wolf.)®

 

  

The Defection of the Pastors

A majority of colonial patriot pastors became Whig Common-
wealthmen rather than Holy Gommonwealthmen during the years
of the Revolution. They became dissenters in the sense of the Whig
radical dissenters. They saw the need to escape an Anglican bishop

6. Leonard J. Trinterud, The Forming of an American Tradition: A Re-examination of
Colonial Presbyterianism (Philadelphia: Westminster Press, 1959), pp. 269-71.

7. Carl Bridenbaugh, Mitre and Sceptre: Transatlantic Faith, Ideas, Personalities, and
Politics, 1689-1775 (New York: Oxford University Press, 1982); Trinterud, Forming,
ch. 13.

8. Peter G. Peterson and Neal Howe, On Borruwsd Time: How the Growth in Entitle-
ment Spending Threatens America’s Future (San ¥rancisco: Institute for Contemporary
Studies, 1988).
