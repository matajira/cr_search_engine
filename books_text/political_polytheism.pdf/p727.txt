Rushdoony on the Constitution 703

church, and to take the sacrament of the Lord’s Supper on a regular
basis. Those who refuse are outside the Church covenant. Therefore,
in a theocratic republic, they would not be entitled to impose civil
sanctions.

This raises the other question that he has always avoided: the
State must identify which churches are Trinitarian and therefore
whose members are authorized to vote. A Christian republic in-
evitably must face the question analogous to the one today disturb-
ing the state of Israel: Who is a Jew?

On this dual point— the question of civil sanctions and ecclesias-
tical sanctions—Rushdoony has remained conspicuously silent
throughout his career, but his actions in recent years indicate that he
sides with the Baptists and Anabaptists in American history: Ghurch
membership has nothing to do with voting or holding civil office.
This conclusion leads him straight into the pluralistic arms of Roger
Williams. There is no permanent halfway house between John Win-
throp and Roger Williams, There is no permanent halfway cove-
nant. There is no neutrality.

Instead, there are Church sacraments. These are the foundation
of Christian civilization—not the franchise, not the gold standard,
not the patriarchal family, not the tithe to parachurch ministries,
and not independent Christian education. The sacraments. Deny
this, and you necessarily deny the biblical Church covenant as well
as the biblical civil covenant. Rushdoony has, at the very least, im-
plicitly denied both. The sign of this denial is his life-long designa-
tion of the U.S. Constitution as an implicitly Christian covenant,
meaning a halfway national covenant. That was what the Articles of
Confederation constituted; the Constitution is apostate.

91, Understand, T mean the sacraments as covenant sealing (baptism) and covenant-
renewing (Lord's Supper), see the sacraments as judicial, in opposition to both
Protestant nominalism (memorials) and Roman Catholic realism (infusions of
grace).
