344 POLITICAL POLYTHEISM

It could be transmitted safely only within a secret fraternity. The
stonemasons were one such fraternity. Here were the seeds of the
later “speculative” Freemasonry, ''®

This Newtonian impulse is basic to understanding the close asso-
ciation of Newton’s followers in the Royal Society and the spread of
reconstituted Freemasonry after 1717. Freemasonry worshipped
geometry, even as the Principia had relied on geometry to prove its
case.

There was another aspect of this theology of geometry: the search
for God in history. God’s transcendence was manifested by geometry,
but this was not sufficient; God had to make Himself manifest to
man. Again, geometry was the key. This was the reason for the
fascination with Solomon’s Temple. Write Baigent and Leigh:

Within this ‘esoteric’ tradition of ‘initiated’ masters, sacred geometry
was of paramount importance—a manifestation, as we have seen, of the
divine. For such masters, a cathedral was more than a ‘house of God’. It
was something akin to a musical instrument, an instrument tuned to a par-
ticular and exalted spiritual pitch, like a harp. If the instrument were tuned
correctly, God Himself would resonate through it, and His immanence
would be felt by all who entered. But how did one tune it correctly? How
and where did God specify His design requirements? Sacred geometry pro-
vided the general principles, the underlying laws."!7

Geometry was not enough. Music was not enough. There must
be intellectual content to this immanence, There must be ethical
content, including the assurance of personal salvation, itself defined
as presence with God in eternity. This is what scientific Newtonian-
ism could not provide. The creation of speculative Freemasonry —a
guild open to men without any connection to stonemasonry — was a
major theological and institutional attempt to provide this assur-
ance, but within the geometrical worldview of Newtonian science.

A Distant God

The god of Newton was not the God of the Bible; it was the god
of the Deists. It was the cosmic clockmaker rather then the Sovereign
Judge of all men, in history and in eternity. It was this concept of
God which swept Europe in the eighteenth century. Any attempt to

116. Temple and Lodge, p. 134.
117. Idem. Cf. J. E. McGuire and P. M. Rattansi, “Newton and the Pipes of Pan,”
Notes and Records of the Royal Society of London, XX1 (1966), pp. 108-43.
