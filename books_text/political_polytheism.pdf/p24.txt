These things need to be pondered by Protestants to-day. With
what right may we call ourselves children of the Reformation? Much
modern Protestantism would be neither owned nor even recognised
by the pioneer Reformers. The Bondage of the Will fairly sets before us
what they believed about the salvation of lost mankind. In the light
of it, we are forced to ask whether Protestant Christendom has not
tragically sold its birthright between Luther’s day and our own. Has
not Protestantism to-day become more Erasmian than Lutheran?
Do we not too often try to minimise and gloss over doctrinal differ-
ences for the sake of inter-party peace? Are we innocent of the doc-
trinal indifferentism with which Luther charged Erasmus? Do we
still believe that doctrine matters? Or do we now, with Erasmus, rate
a deceptive appearance of unity as of more importance than truth?
Have we not grown used to an Erasmian brand of teaching from our
pulpits—a message that rests on the same shallow synergistic con-
ceptions which Luther refuted, picturing God and man approaching
each other almost on equal terms, each having his own contribution
to make to man’s salvation and each depending on the dutiful co-
operation of the other for the attainment of that end? —as if God ex-
ists for man’s convenience, rather than man for God’s glory?

J. 1. Packer and O. R. Johnson (1957)*

“Packer and Johnson, “Historical and Theological Introduction,” Martin Luther,
Bondage of the Will (London: James Clarke, 1957), p. 59-60.
