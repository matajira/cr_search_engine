Introduction, Part 2 123

cocting notions of perfect government,” of “transnationalism,”§ and
of ignoring “essential Christian truths.”? What I want is a list of con-
crete, Bible-supported policies that are morally mandated today,
What I want is casuistry, What I want is guidance based on what the
Bible tells all mankind to do in history, on threat of punishments in
history. What I get is verbiage:

To adapt the principles of love and public justice to circumstances today
we must understand how they were implemented in the Mosaic civil legisla-
tion. Today’s civil magistrates are not “exempted” from God's law in its per-
manent form, but are still under the comprehensive love commandment as
it applies to public justice. They are only exempt from the specific Mosaic
statutes that were adaptations to Israel’s unique and temporary position as
the Old Testament people of God after He constituted them a theocracy.®

Mr, Schrotenboer is an amateur in the verbiage game, however
hard he tries. He has never been able to advance to professional
status. Nevertheless, he has faithfully promoted the professionals.
Consider this statement by A. Troost, a professor at the Free Uni-
versity of Amsterdam, which Mr. Schrotenboer published while he
was editor of the International Reformed Bulletin, In reply to my
criticisms of his vague call for a return to something resembling
medieval guild socialism in the name of Christ,? Professor Troost
wrote a classic “clarification” of Christian social ethics apart from
Old Testament law. He began his clarification with this statement: “I
hope that this further elucidation clears me sufficiently of the charge
of antinomianism and vague pietism.” Judge for yourself:

As for so-called social ethics, let me explain it in the following way: The
question of what justice is in the concrete case and of what love to my neigh-

5
6.
2.

 

8. Paul G, Schrotenboer, “Ihe Principled Pluralist Response to Theonomy,’ in
Gury Scott Smith (ed.), God and Politics: Four Views on the Reformation of Civil Govern-
ment (Phillipsburg, New Jersey: Presbyterian & Reformed, 1989), p. 60. This book
is a compilation of papers given at Geneva College in Beaver Falls, Pennsylvania in
1987: “Consultation on the Biblical Role of Civil Government.”

9. Gary North, “Social Antinomianism,” International Reformed Bulletin (Oct. 1967),
pp. 46-52, Itwas also published in the Netherlands in the Gereformeerd Gezenblad (Oct.
28, 1967), much to the consternation of Dr. Schrotenboer, who wrote to me regard-
ing his disapproval. Dr. Troost had not expected to have anyone blow the whistle in
public “back home.”
