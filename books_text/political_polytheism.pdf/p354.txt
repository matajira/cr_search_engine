330 POLITICAL POLYTHEISM

churches are viewed as effective educational institutions; no other in-
stitution could accomplish this task more effectively. Hence, Christi-
anity is a good thing socially. ‘The whole perspective is civic: what is
good for the churches is good for the nation. Adams’ view is with us
still; Robespierre’s did not survive 1794,

The Right Wing of the Enlightenment

In my view, it is Rushdoony’s greatest single historiographical er-
ror that he has always downplayed the Enlightenment influence on
eighteenth-century American history.*! At the heart of the Enlight-
enment's right-wing branch philosophically (the Scottish Enlighten-
ment)®? and also its left-wing branch (the French philosophes, but
above all Rousseau) were the doctrines of natural law and natural
rights. This commitment to natural law, in fact, was what made both
branches part of the same movement. Their main differences were
historical: different enemies. It would not be far from wrong to sum-
marize the origins of the two wings as follows:

The Scottish Enlightenment philosophy was developed by Presby-
terians who had abandoned Christian orthodoxy, but who maintained
certain outward forms of belief by substituting a new humanistic
theory of contracts for the Calvinistic theory of covenants.*3 Con-
tinental Enlightenment philosophy was developed by graduates of
Roman Catholic institutions who had abandoned Christianity alto-
gether, and who substituted the State for the Church as the agency of
social salvation.*+

51. Rushdoony, This Independent Republic, p.4. See Appendix B: “Rushdoony on
the Constitution,” below.

52, I would call the early seventeenth-century Anglican-Whig movement conser-
yative, but it did not develop a systematic moral philosophy and political philosophy
comparable in interpretive power to that produced by the Scottish Enlightenment.

53. Adam Ferguson (1723-1816), a founder of the Scottish Enlightenment tradi-
tion, and a close friend of skeptic David Hume, was also an ordained minister in the
Ghurch of Scotland and had been chaplain to the Black Watch regiment. He taught
natural philosophy (science) at the University of Edinburgh, and later moral phi-
losophy, a chair be resigned in 1785, to be replaced by Dugald Stewart. His last
words were: “There is another world.” Dictionary of National Biography (Oxford Uni-
versity Press, 1921-22), vol. 6, pp. 1200-4, F. A. Hayek regards his work as crucial to
his own economic worldview.

54. The best example is Adam Weishaupt, founder of the Bavarian Tluminati,
who was far a time professor of canon law at the University of Ingoldstadt. Another
example is Robespierre, who had been a prize-winning student at Louis-le-Grand
college of the University of Paris. Otto Scott, Robespierre: The Voice of Virtue (New
York: Mason/Charter, 1974), pp. 38-19. Distributed by Ross House Books,
Vallecito, California.
