626 POLITICAL POLYTHEISM

drunkenness, revellings, and such like: of the which I tell you before, as I
have also told you in time past, that they which do such things shall not in-
herit the kingdom of God (Gal. 5:19-21).

The adultery, fornication, uncleanness, and lasciviousness of
major Arminian, antinomian, dispensational leaders have been
spread all over the front pages (and back pages) of the newspapers in
the last few years. Why? Because they have lived consistent with their theol-
ogy! They have publicly despised Calvinist covenant theology. They
have said this theology is of the devil. They have done whatever pos-
sible to discredit it. And then they unzipped their trousers to prove
their personal consistency to their publicly stated faith. They have
testified under bedcovers that they are under antinomian grace, not
biblical law.

The sanctions have come. “Extra, extra, read all about it!”
“Details at eleven!” But these sanctions have not been imposed by
their denominations until long after the sanctions have been imposed
publicly by the secular humanist media. And so the curse of Nathan
burdens the Church today just as it did in David's day: “. . . by this
deed thou hast given great occasion to the enemies of the Loxp to
blaspheme . . .” (IJ Sam. 12:14). These pastors, living faithfully and
consistently in terms of their publicly stated theology, have given
great occasion to the enemies of the Lord to blaspheme.

For this they may get suspended by their denominations for two
whole years. Or transferred to a different presbytery. When they
refuse to submit even to this, their denominations quietly erase their
names from the list of official pastors. Nothing more. The three-ring
circus will therefore go on. The scandals will continue; the enemies
of God will be continue to blaspheme. And laugh.

God will therefore escalate His sanctions against the churches.
Count on it. Prepare for it. God will not be mocked.

Modern Christians are like the Israelites of Elijah’s day: they
stand between two covenants and two covenant sacrifices, and they
wait. “And Elijah came unto all the people, and said, How long hait
ye between two opinions? if the Lorp be God, follow him: but if
Baal, then follow him. And the people answered him not a word”
(I Ki. 18:21). They waited then, and they wait today. And wait.
They want to see which way the fire falls. After visible judgment
comes, they think to themselves, then they will decide. Until it
comes, they will hedge their bets.
