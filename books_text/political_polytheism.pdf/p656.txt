632 POLITICAL POLYTHEISM

The fact that God has allowed this judicial evangelical testimony
to fade, time after time, when His people have failed to maintain His
laws whenever they have gained political influence, does not mean
that His permanent norm for all societies is a similar abandonment
of Old Testament law. It only means that so far in history, He has re-
peatedly allowed His people to depart from His law, just as Israel
did, only to find themselves as slaves to their God-hating enemies.
God does not annul His sanctions in history; He continues to en-
force them.

The question of questions for Christian applied theology, ethics,
and social theory is this one: Why should Christians accept as their
long-term earthly goal the establishment of any system of civil law
other than the one set forth in the Bible? In other words, why should
Christians affirm in principle the acceptability of any law-order other
than biblical law? Why should they enthusiastically choose second-
best, third-best, or even totalitarian civil order in preference to bibli-
cal law? Why is their last choice always biblical law? We could
search for answers in psychology, sociology, education, and in any
other academic specialties. I prefer to begin looking for the answer in
the area of ethics: Christians prefer irresponsibility.

The Suppression of God's Eternal Sanctions

It is not just biblical law that modern evangelicalism rejects.
Where is any clearly stated doctrine of hell and eternal torment in
most of today’s evangelism programs? The doctrine of hell has stead-
ily disappeared from mass evangelism ever since D. L. Moody an-
nounced: “Terror never brought a man in yet.”"* Campus Crusade
for Christ is today’s model: not a word about eternal punishment in
their primary evangelism materials. The question of evangelical
questions, “Are you saved?” does not have a forthright, open answer
to the obvious response, “Saved from what?”!*

The doctrine of covenantal sanctions has been the weakest point
of the theology of the Protestant Reformation. Protestantism has
never been clear on exactly what the sacraments are: judicial sanc-

, 33, Cited by Stanley N. Gundry, Love Them in: The Proclamation Theology of Dwight
L. Moody (Chicago: Moody Press, 1976), p. 99.

14. The Evangelism Explosion program is an exception to this rule. Its founder,

D. James Kennedy, has publicly preached that modern preaching seldom mentions

hell. Kenneth Kantzer of Christianity Today and Wheaton College remarked in the late

1980's that he had not heard a sermon on hell in a quarter of a century. I believe him.
