Thou shalt break them with a rod of iron; thou shalt dash them in
pieces like a potter’s vessel. Be wise now therefore, O ye kings: be in-
structed, ye judges of the earth. Serve the Lorp with fear, and rejoice
with trembling. Kiss the Son, lest he be angry, and ye perish from
the way, when his wrath is kindled but a little. Blessed are all they
that put their trust in him (Ps. 2:9-12).

We cannot help but see, then, how far the infallible moral in-
struction of this psalm is removed from the pluralist political theories
of our day. By contending that civil policy should not be based upon
or favor any one distinctive religion or philosophy of life (but rather
balance the alleged rights of all conflicting viewpoints), pluralism ul-
timately takes its political stand with secularism in refusing to “kiss
the Son” and “serve Jehovah with fear.” The pluralist approach tran-
gresses the first commandment by countenancing and deferring to
different ultimate authorities (gods) in the area of public policy. In-
stead of exclusively submitting to Jehovah's law with fear and openly
following God's enthroned Son, the pluralist attempts the impossible
task of honoring more than one master in civil legislation (Matt.
6:24)—a kind of “political polytheism.”

Greg L. Bahnsen (1989)*

*Bahnsen, “The Theonomic Position,” in Gary Scott Smith (ed.), God and Politics:
Four Views on the Reformation of Civil Government (Phillipsburg, New Jersey: Presbyter-
ian & Reformed, 1989), p. 30.
