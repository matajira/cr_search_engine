Preface xix

sharper and harder.”!*

Pluralists want to defer until the final judgment the appearance
of that dangerously sharp point of history. They want it to end his-
tory. They do not want to face the inescapable historical decision of
all confrontational politics: stick or be stuck. It is God’s law or chaos,
light or darkness, God or Satan - manifested progressively as time
passes. They do whatever they can to deny this. Pluralists spend
their Lives trying to file down that inescapably sharp historical point.
Their files are made of high quality stainless steel, but the point is
made of diamond.

A Parting of the Ways

What we find in the final decades of the second millennium after
the birth of Jesus Christ is a growing realization on both sides of the
political cease-fire line that the traditional ideological synthesis of
political pluralism is collapsing. What we are witnessing is a slow
but sure breakdown of the political cease-fire between humanism
and Christianity. On each side, the defenders of the compromise sys-
tem can no longer hold their own troops in line. Guerilla skirmishes
are breaking out continually. The humanists are beginning to act
like humanists, and a tiny handful of Christians are beginning to act
like Christians.

The confrontation over the life-and-death issue of abortion is one
obvious example of this irrepressible conflict. On the abortionist’s
table, there is no neutral position between life and death. This is why
the inescapably political debate over abortion is so frustrating for
those who want to steer a middle course. There is no middle course.
There is no neutrality, The politician’s left foot is being held to the fire
by the pro-death forces, and his right foot is being shoved in the coals
by the pro-life forces. He has only one choice: accept the political
fact of either one burned foot or two. He, like the political pluralist,
deeply resents being forced to make this choice. He wants no burned
feet. He longs for the simpler, cooler world of yesterday, when the
common morality was implicitly Christian and officially neutral. He
is not going to get that world; it is gone forever. So are at least 25
million dead babies, all executed legally in the United States.

Another example is Christian education. The humanists on the
United States Supreme Court in 1963 banished prayer in the tax-

4. C. 8. Lewis, That Hideous Strength (New York: Macmillan, 1946), p. 283.
