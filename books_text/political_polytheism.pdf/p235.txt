Halfway Covenant Social Criticism 2

tians who did not understand how committed to humanism Carter
was decided to vote for him because of his public profession of a
vague biblical faith, !?*

Rushdoony’s study of Cornelius Van Til’s philosophy, By What
Standard? (1959), was a full-scale attack on humanist thought, but it
was disguised as a narrowly theological book on apologetics, which
was hardly a popular topic. His books that followed began to reveal
the broad range of his thought: Jnéellectual Schizophrenia (1961), The
Messiantc Character of American Education (1963), This Independent Repub-
lic (1964), The Nature of the American System (1965), Freud (1965), The
Mythology of Science (1967), and so on. He wrote in the 1960’s mainly
on the topics of history and education, The Institutes of Biblical Law
appeared in 1973, after Schaeffer’s books had begun to soften the re-
sistance to intellectual Christianity in evangelical circles. Rush-
doony’s books were published by tiny Craig Press (Presbyterian &
Reformed), and they did not sell well. Most of them were allowed to
go out of print.1??

The major intellectual transformation in the evangelical and fun-
damentalist communities came with the publication of Schaeffer's
The God Who Is There. This book appeared in 1968, when the counter-
culture had become a worldwide phenomenon, This was thirty-one
years after the death of Machen. The period 1965-71 was the crucial
period for fundamentalists to re-enlist in the battle for the minds of
men in the West—their best opportunity since the Scopes “Monkey
Trial’ of 1925, which the fundamentalists had lost to the evolutionists
in the court of public opinion. Now, in the aftermath of Kennedy's
assassination and the Vietnam War, conventional liberalism was
coming visibly unglued. The self-confidence of pragmatic political
liberals was shattered by the campus riots and then student and race
riots inthe cities. The free speech movement that began in the fall of

124, Bob Slosser, who co-authored The Secret Kingdom with Pat Robertson (Nash
ville, Tennessee: Thomas Nelson Sens, 1983), also co-authored a paperback book
that can best be described as a campaign tract: The Miracle of Jimmy Carter (Logos
International, June, 1976). I bought my copy in a British used book store in 1986. It
was published in Britain by Logos sometime in 1976.

122, Half a dozen titles were picked up by young David Thoburn, who used his
own money to reprint them as paperbacks in 1978. They have not sold well since, ex-
cept for one large “fire sale” that I promoted in my newsletter, Remnant Review, in
1982. I financed the republication of By What Standard? in 1983, and was reimbursed
by Thobum from early sales. These books will probably not be reprinted when they
go out of print, unless in a large, expensive, and very difficult to sell “collected
works” version.
