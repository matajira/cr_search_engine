Conclusion, Part 3 549

tion to ratify the U.S. Constitution, which immediately went into
force as the new covenant of the nation. Thus, the Whigs political
and the Whigs ecclesiastical had at last overturned the covenantal
foundations that had been established by their seventeenth-century
Puritan enemies, and had done so in a period of slightly less than
thirteen months.

Governor William. Livingston of New Jersey was correct when
he observed in 1790 that the clergy of America were “almost all uni-
versally good Whigs.”6! He himself had been “the American Whig”
in 1768, when he wrote or at least organized a series of New York
Gazeite and Pennsylvania Journal articles against sending an Anglican
bishop to the colonies, a step regarded by many colonists as being
the first step in Parliamentary control over colonial religion.™ Yet it
was “the American Whig” himself who had asked rhetorically the
most important question in American history: “. . . why might not
Christianity have been allowed the honor of being called the Na-
tional Religion?”®* The answer should be clear by now: because the
Unitarians did not want it that way, and the Whigs ecclesiastical did
not really think that the implicit Christianity of the nation was
threatened by the idolatry of the new national covenant, i.e., the
People as the new national god.

A year after the 1788 Synod, in May of 1789, the General Assem-
bly had John Witherspoon again chair a committee, this time to pre-
pare an address to the newly elected President of the United States.
The alternate chairman was John Rogers. The committee drafted a
lengthy report in which it expressed those sentiments that have been
passed down from textbook to textbook. Echoing Washington’s
Masonic rhetoric, the address announced: “Public virtue is the most
certain means of public felicity, and religion is the surest basis of vir-
tue. We therefore esteem it a peculiar happiness to behold in our
Chief Magistrate a steady, uniform, avowed friend of the Christian
religion, and who on the most public and solemn occasions devoutly

61. William Livingston, “Observations on the Support of the Clergy,” American
Museum, VTIL (1790), p. 254; cited in Alan Heimert, Religion and the American Mind:
From the Great Awakening to the Revolution (Cambridge, Massachusetts: Harvard Uni-
versity Press, 1966), p. 1.

62, Bridenbaugh, Mitre and Sceptre, pp. 297-98.

63. “Colonial Criticism of the Appeal (1768),” in John F. Wilson and Donald L.
Drakeman (eds.), Church and Stale in American History: The Burden of Religious Pluralism
(Qnd ed.; Boston: Beacon Press, 1987), p. 57.
