Conclusion, Part 3 551

exist.”*? He wrote contractual, but he clearly meant covenantal.®8 Miller
saw what the key issue was: sanctions, There would be no more national
controversies with God. He would no longer threaten the nation
with His negative sanctions.

If it were not for the continuing presence of the remnant Church
(Chapter 11), Miller would be correct. God would have washed His
hands of this nation long ago. He would have imposed His negative
sanctions.

Despite the historical facts— both theological and Constitutional
—that I have surveyed in this study, from the beginning of the Con-
stitutional era, Christian historians have promoted the myth of the
Christian origin of the Constitution. Philip Schaff, the most promi-
nent American evangelical Church historian of the late nineteenth
century, summarized this view, and the language of his imitators has
not deviated in any significant respect:

We may go further and say that the Constitution not only contains
nothing which is irreligious or unchristian, but is Christian in substance,
though not in form. It is pervaded by the spirit of justice and humanity,
which are Christian, . . . The Constitution, moreover, in recognizing and
requiring an official oath from the President and all legislative, executive,
and judicial officers, both of the United States and of the several States,
recognises the Supreme Being, to whom the oath is a solemn appeal. .
And, finally, the framers of the Constitution were, without exception, be-
lievers in God and in future rewards and punishments, from the presiding
officer, General Washington, who was a communicant member of the Epis-
copal Church, down to the least orthodox, Dr, Benjamin Franklin. . , .7°

 

 

There are minor variations, of course. Rushdoony argues that
the Constitution is neutral both in substance and in procedure, (See
Appendix B.) But on the whole, Schaff’s statement is representative
of two centuries of incomparable historical misrepresentation—a
myth that is taken seriously by virtually all conservative American
Ghristians. The conspirators were successful beyond their wildest

67. Perry Miller, Nature's Nation (Cambridge, Massachusetts: Belknap Press of
Harvard University Press, 1967), p. 113.

68. His chapter is titled, “From the Covenant to the Revival.”

69. What is needed today is a detailed study of how the American churches could
continue to proclaim God’s positive historical sanctions for the nation. Did they re-
interpret the covenant’s sanctions? Did they really teach such views?

70. Philip Schaff, Church and State in the United States or the American Idea of Religious
Liberty and Hts Practical Effects (New York: Arno Press, [1888] 1972), pp. 40-41.
