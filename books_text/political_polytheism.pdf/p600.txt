576 POLPTICAL POLYTHEISM

endeavor within Bible-believing Protestantism since the late seven-
teenth century. I am thinking of Richard Baxter’s enormous study, A
Christian Directory, written in 1664-65 and first published in 1673, and
Samuel Willard’s equally massive commentary on the Westminster
Larger Catechism, published posthumously, 4 Compleat Body of
Divinity (1726). T am also thinking of the work of the Anglican theo-
logian Jeremy Taylor, Richard Baxter’s goal was basically the same
as mine: “I do especially desire you to observe, that the resolving of
practical Cases of Conscience, and the reducing of Theological knowl-
edge into serious Christian Practice, and promoting a skilful facility in the
faithful exercise of universal obedience and Holiness of heart and
life, is the great work of this Treatise; .. .”!

 

Biblical Pluralism

Dominion Christianity teaches that there are four covenants
under God, meaning four kinds of vows under God: personal {indi-
vidual), and the three institutional covenants: ecclesiastical, civil,
and familial.? All other human institutions (business, educational,
charitable, etc.) are to one degree or other under the jurisdiction of
one or more of these four covenants. No single human covenant is
absolute; therefore, no single human institution is all-powerful.
Thus, Christian liberty is /berty under God and God's law, administered
by plural legal authorities.

‘There is no doubt that Christianity teaches pluralism, but a very
special kind of pluralism: plural institutions under God’s single com-
prehensive law system. [t does not teach a pluralism of law struc-
tures, or a pluralism of moralities, for this sort of hypothetical legal
pluralism (as distinguished from institutional pluralism) is always
either polytheistic or humanistic.’ Christian people are required to
take dominion over the earth by means of all three God-ordained in-
stitutions, not just the Church, or just the State, or just the family.

1. Richard'Baxter, A Christian Directory: Ox, A Surmm of Practical Theologie, and Cases
of Conscience (London: Robert White for Nevil Simmons, [1673] 1678),. unnumbered
page, but the sccond page of Advertisements.

2. Ray. R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: In-
stitute for Christian Economies, 1987), ch. 4. It was the failure to acknowledge the
unique presence of God in these covenantal institutions that undermined the Chris-
tian case for state constitutional covenantalism in the late eighteenth century, Sce
John Witherspoon's Lectures on Moral Philosophy, Lecture 16, “On Oaths and Vows.”

3. Gary DeMar, Ruler of the Nations: The Biblical Blueprints for Government (Et,
Worth, Texas: Dominion Press, 1987), ch. 3.
