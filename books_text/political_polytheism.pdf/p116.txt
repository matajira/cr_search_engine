92 POLITICAL POLYTHEISM

Because, even because they have seduced my people, saying, Peace;
and there was no peace; and one built up a wall, and, lo, others daubed it
with untempered mortar: Say unto them which daub it with untempered
mortar, that it shall fall; there shall be an overflowing shower; and ye, O
great hailstones, shall fall; and a stormy wind shall rend it. Lo, when the
wall is fallen, shall it not be said unto you, Where is the daubing wherewith
ye have daubed it? Therefore thus saith the Lord Goo; I will even rend it
with a stormy wind in my fury; and there shall be an overflowing shower in
mine anger, and great hailstones in my fury to consume it. So will I break
down the wall that ye have daubed with untempered mortar, and bring it down
to the ground, so that the foundation thereof shall be discovered, and it
shall fall, and ye shall be consumed in the midst thereof: and ye shall know
that I am the Lorp, Thus will I accomplish my wrath upon the wall, and
upon them that have daubed it with untempered mortar, and will say unto
you, The wall is no more, neither they that daubed it; To wit, the prophets
of Israel which prophesy: concerning Jerusalem, and which see visions of
peace for her, and there is no peace, saith the Lord Gon (Ezek. 13:10-16).

Political pluralism is a myth, the official philosophy of a perpet-
ual cease-fire. Like défente with the Nazis or the Communists, it is a
myth useful only to some, a gigantic public relations ploy of an
implacable enemy, who uses a time of peace to consolidate his posi-
tion for the next assault. Christians are naive. They believe that they
should always think the best of people, giving their covenant-breaking
opponents the benefit of the doubt. They should be ready rather to
doubt the benefits,

What Defines What the Stipulations of a Covenant Are?

Something must define the religion of the covenant in every cov-
enantal institution. Each covenantal institution needs answers: Who
is God, who represents God, what are God's laws, what are His
sanctions, and who will inherit? There is no escape from this set of
five questions. Christians have refused to deal with these questions
for over two centuries, as testified to by the civil covenants under
which they live contentedly. They refuse to go to the Bible in search
of the answers.

The Bible is the only self-authenticating visible Word today. It
tells us what the laws of God are that govern each covenant. All men
are therefore to seek their answers in the Bible. This moral require-
ment is also a legal requirement: God will hold all men and all soci-
eties responsible on judgment day, and in history, for searching His
