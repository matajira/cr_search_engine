274 - POLITICAL POLYTHEISM

Thomas Morton

Marsden does not mention any of this. Instead, he points to
Thomas Morton (1620's) as a more representative colonial American.
This is an outrageous assertion, the product of Marsden’s ideology
rather than any detailed research on his part (or anyone else’s).

You probably have never heard of Thomas Morton. He is known
only to specialists in colonial history. That Dr. Marsden refers to him
in a brief essay in a one-volume general encyclopedia is simply
astounding, What is even more astounding is that he thinks Morton
is representative of colonial life. In the first decade of New England,
Morton immigrated, set up a little community outside the normal
jurisdiction of Plymouth colony, sold guns to the Indians, denounced
Christianity, and erected (the correct verb) a maypole.'® This prac-
tice had been condemned as a survival of paganism by Puritan
Philip Stubbes haif a century earlier. '®* Miles Standish was immedi-
ately sent by the authorities at Plymouth to arrest Morton, which he
did, and the authorities deported Morton back to England.'® His-
torian Charles M. Andrews described him thusly: “Morton was a
bohemian, a humorist, a scoffer, and a libertine, with no moral stan-
dards of thought or conduct.”!” Morton later returned to Massachu-
setts, where he remained in intermittent conflict with the authorities.
Marsden neglects to mention any of this, and then concludes: “In the
colonies as a whole, the Thomas Mortons probably were always
more numerous than the strict Calvinists. They are just as legiti-
mately part of the American heritage.”!71

Interesting. Yet it raises this question: Which historical records
indicate that those holding to Mr. Morton’s theology, let alone im-
itators of his maypole activities, were more widespread in the col-
onies than Christians or even “strict Puritans,” from 1624 until, say,

167. Dancing around a maypole, a phallic symbol, is commonly practiced in
May. “Wherever this custom is found,” writes Eliade, “the ‘Maypole’ gives an occa-
sion for gencral jollity ending with a dance round the pole. ‘The chief part is usually
played by young people or children. It is a feast of spring but, like all such manifes-
tations; can turn into something of an orgy.” Mircea Eliade, Patterns in Comparative
Religion (New York: Sheed & Ward, 1958), p. 310.

168. idem. The work was Anatomie of Abuses (1583).

169. Charles M. Andrews, The Colonial Period of American History, 4 vols. (New
Haven, Connecticut: Yale University Press, [1934] 1964), I, p. 362.

170, Ibid., 1, p. 333.

171, idem,
