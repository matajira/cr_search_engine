sanctions &, 50, 540-41
Satan?, 49-50
initiation, 338
insurance premiums (common
grace), 148
intellectual schizophrenia, xv-xviii
interests, 126, 266, 446-47, 451
interpretation (see hermeneutics,
judiciary, Supreme Court)
InterVarsity Press, 641n
Invisible College, 370, 476
irrelevance (Erasmus), 12
Islam, xv, 342-43
Israel
apostasy, 76
broken covenants, 115
Church, 68
Church &, 152
civil rights, 72
civil war, 71
evangelism, xiii-xiv
failure of, 529
imagery in sermons, 243-45, 513,
758
international kingdom, 529
midlife crisis, 213n
open door policy, 68
pluralism denied, 296
political discrimination (Q.T.), 72
represented God, 39
sanctuary, 66-70

Jackson, Andrew, 510
Jacob (Esau &), 629
Jacob, Margaret
anti-conspiracy, 432
church members (cover), 420
English radicals, 471-72
Fay, 432
historians on Masonry, 432-433
internationalism, 471
Knights of Jubilation, 420
mechanism, 350-51
new religion, 340
Newtonianism’s dualism, 339-40
religious deception, 420
James I, 376-77

Index 747

James II, 377, 429
‘Japan, 636-37
Jay, John, 428-29
Jefferson Committee, 516
Jefferson, Thomas
Baptists &, 410, 539
blood of patriots, 497
broken covenant, 492n
Congress’ agent, 492n, 524-25
god of, 294, 492n
Kentucky Resolves, 498
linguistic subversion, 681
Locke &, 397-98
Madison &, 449
Masonry downplays, 704
nature’s god, 492n, 521, 524, 528,
531
New Testament, 326-27
plebiscites, 496
prophet?, 261-63
pursuit of happiness, 398-99
Unitarian, 406-7
“wall of separation,” 410
Jensen, Merrill, 455
Jeroboam, 565-66, 634-35
Jerusalem, 58
Jesuits, 438
Jesus Christ
ascension, 149-50, 620
betrayal of, 38
bureaucracy?, 587
bowing to, 225
Constitution &, 228, 617
covenantal obligations, 625
crown rights, 185, 379, 616, 660
family of man, 83
Japan &, 637
life preservers, 662, 663
Lord, 625
Lord’s prayer, 590, 638-39
loser?, 153
loyalty to, 182
Messiah, 99
Niebuhr on, 668
owner, 160
reigns, 605-6
return of, 586-88, 662-63
