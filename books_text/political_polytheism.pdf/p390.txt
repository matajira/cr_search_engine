366 POLITICAL POLYTHEISM

Thus, the fundamental problems of the political philosophy of
the “ancients” reappear in the political philosophy of the “moderns.”
Both of these humanist viewpoints are anti-Trinitarian and anti-bib-
lical covenant. There was no Constitutional solution to the problems
of political philosophy in either Federalist Whig Newtonian re-
publicanism or Antifederalist Whig Newtonian republicanism. The
sought-for Constitutional balance of the one and the many, apart
from the Bible and the Old Testament case laws, is unattainable.
Like Newton’s universe apart from God's constant, active provi-
dence, the “balanced Constitution” will inevitably move toward cen-
tralized tyranny (the fear of the Antifederalists) or toward dissolu-
tion (the fear of the Federalists). Both movements took place in
1861-65. The centralists won the intellectual battle of political phi-
losophy on the military battlefields of the U.S. Civil War. (So did the
bankers.)2°? The federal bureaucracy began to expand as never be-
fore after 1860, although it appears small in retrospect in today’s
bureaucratic world. Contrary to Madison’s vision, but consistent
with Madison’s system after the Fourteenth Amendment had made
legally possible the increasing centralization of the nation, these
new bureaucracies were geared to special interests in a diversifying
economy,?°*

What the Framers needed was a model: a fixed governmental
system that would deal with man as he is, yet encourage him to act
in ways that are best for him. The Framers were almost messianic;
they believed that such a constitution had never before been devised.
The republics of Greece and Italy had failed, Hamilton said, for they
had oscillated between tyranny and anarchy?°5 — the perpetual prob-
Jem of the one and the many,?°° But there is hope: “The science of
politics, however, like most other sciences has received great im-
provement. The efficacy of various principles is now well under-
stood, which were either not known at all, or imperfectly known to
the ancients.”*°? Were this not the case, pessimism alone would be

203. Gary North, “Greenback Dollars and Federal Sovereignty, 1861-1865,” in
Hans ¥. Sennholz (ed.), Gold is Money (Westport, Connecticut: Greenwood, 1975),
pp. 122-56.

204. James Q. Wilson, “The Rise of the Bureaucratic State,” Public Interest (Fall
1975), p. 88.

205. Hamilton, Federalist 9, Federalist, p. 50.

206, R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ulttimacy (Fairfax, Virginia: Thoburn Press, [1971] 1978).

207, Hamilton, Federalist 9, Federalist, p. 51.
