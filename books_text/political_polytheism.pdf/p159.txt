Halfway Covenant Ethics 135

between man and God, one which Adam broke, and which he fur-
thermore broke as mankind’s judicial representative. Adam disobeyed
God. He broke God’s law, which is how one breaks a covenant. In
short, at the heart of Van Til’s epistemology is ehics. Ethics is funda-
mental, he repeated throughout his career, not intelligence. He used
to say in the classroom that Christians do not get a new set of brains
when they get converted. Obedience to God is fundamental, not
precise thinking. In one of his marvelous analogies, he said that it
does not matter how much you sharpen a buzz saw if it is set at the
wrong angle: if it is crooked, it will not cut straight. The “angle” ob-
viously has to refer to God’s covenant law, but he never discussed
any details.

Despite the centrality in his thinking of the biblical covenant,”
Van Til refused to discuss covenant law. He refused to discuss which
laws are still part of God’s covenant law-order. He always discussed
epistemology —“What can man know, and how can he know it?”—in
terms of ethics: “What does man do, and why does he do it?”

Now if anything is obvious from Scripture it is that man is not regarded
as properly a judge of God’s revelation to him, Man is said or assumed from
the first page to the last-to be a creature of God. God’s consciousness is
therefore taken to be naturally original as man’s is naturally derivative.
Man’s natural attitude in all self-conscious activities was therefore meant to
be that of obedience. It is to this deeper depth, deeper than the sinner’s con-
sciousness can ever reach by itself, that Scripture appeals when it says:
“Come let us reason together,” It appeals to covenant-breakers and argues
with them about the unreasonableness of covenant-breaking.®

Nevertheless, despite his heavy reliance on the concept of cove-
nantal obedience as the basis of man’s correct knowledge about the
universe, Van Til steadfastly refused to discuss biblical law. He
attacked natural law but put nothing in its place. He devoted his life

28. Along with other doctrines: the sovereignty (providence) of God, the
Creator-creature distinction, the image of God in man as the sole point of contact
between covenant-keepers and covenant-breakers, rationalism vs. irrationalism,
unity vs. diversity, change vs. stability, continuity vs. discontinuity, univocal vs.
equivocal reasoning, biblical analogical reasoning, and the impossibility of a
uniquely “central” doctrine in theology. On Van Til’s denial of any single central
doctrine, see Frame, “Theological Paradox,’ of: cit, p. 305. I think the Trinity, crea~
tion, the fall of man, redemption, and God’s providence are the fundamental doc-
trines of his overall theology, while the covenant is the heart of his epistemology, i.e.,
his discussion of what and how men can know anything correctly.

29. Van Til, “Nature and Scripture,” Infallible Word, p. 273.
