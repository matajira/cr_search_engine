770 POLITICAL POLYTHEISM

Wallace, Henry, 405
Ward, Lester F., 99n
Warren, Mercy, 567
Warren, Joseph, 474
Washington, George
communion, 420-21
cornerstone, 422, 550
Farewell Address, 529
Founder?, 311
general officers, 433
godfather, 428
Grand Master, 422
iMegal act, 428-29
initiated, 423
letters to Masonic lodges, 424
Masonic Bible, 687
Masonry, 422-24
milestones, 422
nationalism, 529
notes, 418
painting of, 422
prayer book, 425
stare of, 459
Temple of Virtue, 434
Trinity, 421-22
Witherspoon &, 549-50
Watts riots, 212
Webcr, Max, 692
Webster, Nesta, 432, 437-39
Weishaupt, Adam, 428, 488, 481
Weils, Ronald, 187-91, 584
Westminster Confession, 168, 170,
172, 231, 332, 347, 546-47
Westminster Theological Seminary,
285, 288n
Wheaton College, 5n, 15, 238, 277
Wheaton (pottage), 629
Whigs
anticlerical, 446, 542
colonial pastors, 526
Commonwealthmen, 331-33
Constitutional theory, 497-98
court, 498
eyclical theory, 497-98
Darwin &, 494
ecclesiastical, 409, 526-27, 548-50
Federalists & Antifederalists, 366

 

heretical, 332-33
historiography, 516
Madison vs., 497
Marsden on, 240
Masonry, 468
neutrality, 361n
Noll on, 278-79
pastors, 526-27, 548-50
political theory, 325-26
radical, 340
ridiculed churches, 542
view of history, 312
White, Morton, 369
white uniforms, 17-19
Whitcfield, George, 357
Whitehead, John, 176n, 197
Wigner, Eugene, 348
William TIL, 429, 445
Williams, Roger
ant-separatist, 252
authority, 245, 251
Baptists, 526
Calvinist?, 238
compact theory, 248
dizzy, 253
emigrant, 250-53
Founding Father, 311
Hatch on, 283
hicrarchy, 245, 251
humanists &, 245
ignored (pre-1776), 283
jurisdiction, 245, 251
Locke &, 248
Marsden’s appeal to, 238-40
Methodists, 526
modern world, 313
natural law, 250, 260
neutral State, 114
nutty, 252
oath, 251-52
Parrington’s view, 246-49
public attacks by, 245
separatist, 251
Tower of Babel, 247
Western states, 526
worldview, 538-39
Wilson, Woodrow, 239, 679
