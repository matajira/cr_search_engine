124 POLITICAL POLYTHEISM

bor means, cannot any longer be viewed as a metaphysical ‘given’ —as all
forms of idealistic ethics suggest. However, the content of justice and love
in the concrete case hic et nunc is not found literally in the Bible as a recipe
for all time. But here the biblical-a[p]riori of faith in the divine creation or-
der must function in the philosophical and social investigation. In so far as
this has in broad lines and outline form led to preliminary results in the phi-
losophy of the cosmonomic idea, this philosophy has shown that in the con-
crete giving of form to justice and love cultural-historical basic possibilities
and the regulating function of faith always play roles in a normative way,”

Try reading this to your spouse as you both sit in bed this eve-
ning, It is a non-prescription cure for insomnia.

“Gee, Grandma,” said Little Red Riding Hood, “what big words
you have.” “The better to keep the Bible-thumpers at arm’s length,
my dear, while cashing their checks for an entire career.”

A Denial of Judicial Specifics

Christian pluralist James Skillen promises, but he never delivers:
“This response does not, of course, answer the question of what does
constitute a God-honoring, limited, and legitimate political order to-
day. That we must work out in accord with the hermeneutical and
historical suggestions outlined above.”'! I want biblical judicial spe-
cifics, he wants the pluralistic status quo. Specific recommendations
based on a hermeneutic consistent with the Bible and John Locke
are what we never, ever get from Christian political pluralists. But
still they keep making promises of comprehensive guidelines to
come: “The Scriptures reveal God’s plan for reordering social rela-
tionships and institutions. Despite sin’s intrusion in the world, God’s
creation ordinances remain in force.”!? This sounds good, but after
350 years of such unfulfilled promises, I grow skeptical.

Their problem runs deeper than a mere absence of a hermeneu-
tic. If they really believed that specific biblical answers existed, they
would then have to accept the reality of explicitly Christian alternatives to
the status quo of the present pluralistic world order. This would be a denial
of their theology of political pluralism. They would have to become
Trinitarian social thinkers rather than polytheistic social thinkers. So

10. A. Troost, “A Plea for a Creation Ethic,” Fnternational Reformed Bulletin (Oct.
1967), p. 54:

11. James W. Skillen, “The Principled Pluralist Response to Christian America,”
God and Politics, p. 164.

12. Gary Scott Smith, “The Principled Pluralism Response to National Confes-
sion,” iid., p. 215.
