What Is Covenant Law? 33

Christians are required to affirm the existence of a normative, cove-
nantal relationship of positive feedback in history, God intends His cove-
nant to work this way: “But thou shalt remember the Lorp thy God:
for it is he that giveth thee power to get wealth, that he may establish his
covenant which he sware unto thy fathers, as it is this day” (Deut.
8:18). In short: more obedience, more blessings; more blessings,
more confirmation; more confirmation, greater obedience. This is
covenantal positive feedback in history. This is Christianity’s stan-
dard of ethical performance, both personally and corporately.? God
brings His sanctions in history, positive and negative, in terms of
men’s public conformity to His revealed law.

We have read that the power to get wealth is one of God’s positive
covenant sanctions in history. This is a New Testament teaching,
too: “Every good gift and every perfect gift is from above, and com-
eth down from the Father of lights, with whom is no variableness,
neither shadow of turning” ( James 1:17). How is this steadfastness of
God revealed in history? By the predictability of His historical sanc-
tions in response to men’s responses to the unchanging principles of
His covenant law. Conversely — much to the outrage of political lib-
erals and most academic neo-evangelicals—long-run poverty is one
of God’s negative sanctions in history.5 Such a view of history is un-
acceptable to the Christian world generally, and especially to univer-
sity-trained Christian intellectuals. Why? Because such a view is ut-
terly hostile to the God-denying worldview of Darwinism, which
contemporary Christians have adopted far more than they are aware
of. Darwinism teaches that there is no supernatural force in history.
Until the advent of man, there was no direction to history, no moral-
ity, and no purpose. Only with the appearance of man in history
does cosmic personalism appear. Man proposes, and man disposes.
Man extends dominion in the name of the human species. Man, and
only man, brings meaningful sanctions in history. Autonomous man

3. These sanctions apply more clearly to corporate bodies than to individuals,
rather than the other way around, contrary to what pietism teaches. We know that
righteous individual covenant-keepers can suffer cursings in history, as the Book of
Job teaches. What the Bible teaches is that in the aggregate (corporately), and in the long
run, God’s covenant sanctions are reliable and predictable.

4, Gary North, “Free Market Capitalism,” in Robert G. Clouse (ed.), Wealth and
Poverty: Four Christian Views on Economics (Downers Grove, Mlinois: InterVarsity
Press, 1984), pp. 27-65.

5. Gary North, Unholy Spirits: Occultism and New Age Humanism (Ft. Worth, Texas:
Dominion Press, 1986), ch. 8; “Magic, Envy, and Foreign Aid.”
