360 POLITICAL POLYTHEISM

If anything other than verbal profession of faith and outward walk
according to God’s Bible-revealed law is suggested as a substitute re-
quirement for Church membership, the result is the creation of a dis-
tinction in membership based on this added requirement. If the added
requirement is experience, then someone in the church will not meet
this inherently undefinable standard. If experience becomes in any
way a formal basis of membership, detailed creeds will then be seen
as inherently divisive within the church, and the defenders of such
creeds will be seen as narrow bigots. The supplemental standard will
become the primary screening device in the eyes of those who believe
that it is more than supplemental. This is what happened during the
Great Awakening and its aftermath in the 1760's. The Great Awak-
ening restructured Church government as surely as it restructured
civil government. !8?

Samuel Davies, a leader in Virginia Presbyterian circles and
who succeeded Jonathan Edwards as president of the College of New
Jersey, began in the late 1750's to urge a “unity of affection and design”
among all of Virginia’s dissenters, Baptists and Presbyterians. He
argued that this unity would not be based on doctrine or logic, but
on “experimental and prectical Religion.” 183 In the revival of 1763, this
was the basis of another call to Christian union; Christians were to
be “one in heart, one in affection” in attending to “the same great con-
cern,” which was the Work of Redemption,** Contrary to Heimert’s
assertion that “the essentials of Calvinism” were “the New Birth and
experimental religion,”*®> there was nothing explicitly or even im-
plicitly Calvinistic about these concerns. There was clearly nothing
Puritan. The Great Awakening was creating a new basis of Christian
unity: experientialism and a least-common-denominator creedalism.

This unity could not be maintained ecclesiastically. Baptists were
Baptists; Presbyterians were Presbyterians (and separated from their
brethren until 1758). Where, then, was this hoped-for unity to be
manifested? In civic religion. This would require a common view of
civil law to match the ever-leaner creedal confessions and the ever-
less covenantal conception of Christian society. This was reflected in
the Presbyterians’ steady acceptance of a practice they had never
been comfortable with, public fast days. These days wore a celebra-
tion of God’s common moral law among nations:

182. See Bushman, From Puritan io Yankee.
183. Cited in Heimert, Religion, p. 142.
184, Idem,

185, Idem.
