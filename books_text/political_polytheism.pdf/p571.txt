Conclusion, Part 3 547

The synod of 1788, in its last official act as a Synod, appointed
John Witherspoon to address the new General Assembly before it
elected a moderator, which was John Rogers. This seemed appropri-
ate, for it was Witherspoon who. almost certainly had written the
Preface to the proposed new form of government back in 1786, The
Preface stated:

“God alone is Lord of the conscience; and hath left it free from the doctrine
and commandments of men, which are in any thing contrary to his word,
or beside it in matters of faith or worship;” Therefore they [Presbyterians]
consider the rights of private judgment, in all matters that respect religion,
as universal and inalienable: they do not even wish to see any religious con-
stitution aided by the civil power, further than may be necessary for protec-
tion and security, and, at the samc time, be equal and common to others.*!

The End of the Holy Commonwealth Ideal

Thus ended the ideal of the theocratic republic in mainstream
Presbyterianism and American Protestantism in general. That this
official position had been articulated by the president of the College
of New Jersey was fitting. Its predecessor, the Log College, had been
the leading light in the battle against what Trinterud calls “the narrow
spirit of denominationalism.”5? Founded in 1746, its trustees had in-
vited newly appointed Governor Jonathan Belcher onto the Board of
Trustees in 1748, They immediately voted him president of the Board.
Governor Belcher saw to it that the college was granted a new charter,
and he worked hard to create a new board filled (with three excep-
tions) with graduates of Harvard and Yale. This is understandable; he
had been Governor of Massachusetts from 1730-41. The college was
moved to Newark; in 1755, it was moved to Princeton.5

That Jonathan Belcher became the driving force of the develop-
ment of the College of New Jersey is representative of what was tak-
ing place throughout the colonies. Belcher was not a Presbyterian.
Nevertheless, he found it easy to cooperate with Presbyterians. His
theology was expressly geared to cooperation. Jonathan Belcher was

51. Cited in James H. Smylie, A Cloud of Witnesses: A History of the Presbyterian
Church in the United States (Richmond, Virginia: Covenant Life Curriculum, 1968),
p. 26. The Presbyterian Church in the U.S. was the Southern Presbyterian Church
until it merged with the P.C.U,S.A, (Northern) in 1983.

52. Trintcrud, Forming, p. 131.

53. Jacob Harris Patton, A Popular History of the Presbyterian Church in the United
States of America (New York: Mighill, 1900), pp. 118-19.
