suzerain, Chapter 10, 375
under God, 656
“vacation,” 495-96
vassals, Chapter 10, 656, 702
very far away, 496
Vindiciae contra Tyrannis, 376
voice of, 504, 541-42
voters &, 458, 499-500
see also plebiscite
Pepys, Samuel, 476
perfection, 54
perjury, 390, 484-85
persuasion, 101
pessimillennialism
anti-leadership, 210
Christian Reconstruction, 155
dualism, 662
eschatology &, 158-60
Lee defines, 50
no future harvest, 153
pluralism &, 207-10
progressive sanctification vs., 208
suicide of, 210
theocratic, 156-57
treading water, 662
Pharaoh, 536-37
pietism
abortion, 246
alliance with humanism, 245
anti-Constantine, 629
anti-creedal, 629
denies judgment in history, 629
neutrality, 246
pluralism, 246
politics, BG
salvation of souls only, 629
Van Til vs., 141-42
piety (classical politics), 81
piper and tunes 96
plague, 46
Plato, 343, 345-46, 365
Playboy, 125
plebiscite
Burgess on, 520
covenantal, 5il
judicial review, 506
Madison’s plan, 445

Index 757

multiple, 496
People &, 499, 510-11
plural governments (biblical), 576-77
Pocock, J. G. A., 365n
polis, 77,.29, 81, 647
political philosophy, 78, 81
political pluralism
abortion’s screening process, 190
adultery’s sanctions &, 298
agreement, 126
ambiguity, 240-41
amendment process, 224, 292-93,
658
anti-Christian, 660
antinomianism &, 158
assumptions, 106
autonomy, 292
Baalism, 421
bankrupt, 106-7
“beyond the Pale,” 86
Bible &, 93
broken wall, 91
bureaucracy, 658
burial, 663
cease-fire, Ll, 227, 250, 265
chattel slavery, 264-65
“Christian” (see Christian political
pluralism)
civil war, 91, 116-17, 264-65
collapsing, xix
Communism &, xvii
coniract (see compact theary,
contract theory)
covenant forgetfulness, 294
crossfire, 227
deception, 267
demonic quest, 371
“dialoguing with enemies,” 658
dying, 660
Enlighteament myth, 540
formalism, 292
fiux of history, 540
God vs., 652
good society, 540
halfway house, 111
higher power, 293-94

humanism, 191
