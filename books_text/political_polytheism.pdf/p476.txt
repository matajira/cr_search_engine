452 POLITICAL POLYTHEISM

Rousseau wanted to overcome Roman Catholicism by making the
world socially Unitarian and politics salvational.

Ancient Rome sought Madison’s political goal by inviting all
conquered cities of the Empire to send their local gods into the pan-
theon; Madison told the conquered cities of the republic to keep
their gods home and multiply them. He then emptied the pantheon.
This confidence in what should be described as a Unitarian political
settlement was based on some version of Newtonian or Ciceronian
natural law. It was also the worldview of Freemasonry. Masons be-
lieved that the religious “factions” or traditions ~creeds, liturgies,
and unique institutional histories— are peripheral to the true spiri-
tual unity of the Brotherhood under the Supreme Architect.

Factions for Stability’s Sake

The Constitution had not yet been ratified when the Antifederal-
ists began organizing to capture Congress under the new Constitu-
tion,”9 Political factions and parties had already sprung up during
the Revolutionary War era.5? They developed even further during
the Confederation period.*! They became entrenched after 1788.3?
Madison’s dream was shattered before sunrise. There is universal
agreement among historians: this Madisonian faith in a world of off-
setting political factions was utopian in 1788, just as it would be uto-
pian today. What few scholars are willing to say forthrightly is that
the very presence of such a faith marks Madison as the most ration-
alistic of political philosophers. He paid no attention to the realities
of politics in constructing the rationale for the Constitutional blue-
print. He believed that the Constitution would actually balance real-
world politics into oblivion. Patrick Henry’s assessment of the man
was on target: “a man of great acquirements, but too theoretical as a
politician.” Madison and his peers were totally naive on this point,

29, Merrill Jensen and Robert A. Becker (eds.), The Documentary History of the First
Federal Elections, 1788-1790, 2 vols. (Madison: University of Wisconsin Press, 1976).

30. Stephen E, Patterson, Political Parties in Revolutionary Massachusetts (Madison:
University of Wisconsin Press, 1973). I find it ironic that the publisher is located in a
city called Madison.

31. Jackson Turner Main, Political Parties before the Constitution (Williamsburg,
Virginia: Institute for Early American History and Culture, published by the Uni-
versity of North Carolina Press, 1973).

32. Richard Hofstadter, The Idea of a Party System: The Rise of Legitimate Opposition
in the United States, 1780-1840 (Berkeley: University of California Press, 1969).

33. Quoted by Meade, Patrick Henry, p. 435.

 
