Masons in the American Revolution 707

Edmund Randolph
Peyton Randolph
Daniel Roberdeau
Arthur St. Clair
Jonathan Bayard Smith
John Stark Baron von Steuben
Richard Stockton

John Sullivan

Jethro Sumner

William Thompson
James Mitchell Varnum
John Walker

George Walton

George Washington
George Weedon
William Whipple

Otho Holland Williams
William Woodford
David Wooster

To this should be added Joseph Warren and Paul Revere of Bos-
ton, whose lodge was closely associated with the Boston Tea Party.
James Otis is missing. So is Robert Livingston of New York and John
Paul Jones. Above all, so is John Marshall, who in the mid-1790’s was
Grand Master of Virginia (Beveridge, Marshall, II, 176-77).

Philip Roth

Philip A. Roth self-published Masonry in the Formation of our Gov-
ernment in 1927. He was the Past Master of Henry L. Palmer Lodge
No. 301 and was at the time manager of the Masonic Service Bureau
in Washington, D.C. The book provides biographies of key figures
in the American Revolution, including English figures, and also in-
cludes some brief summaries of key events, such as the inauguration
of President Washington. Roth was judicious; he did not claim that
anyone was a Mason unless he could document the actual Lodge in
which he was a member or was inititiated. This list.is probably less
reliable than Heaton’s, who had access to more complete records.

His list of Masons includes the following men:

Gen. Benedict Armold
Col. William Barton
