Sanctuary and Suffrage 73

Immigration Barriers

Today, not a single sanctuary nation remains on earth. They all
have restrictive immigration barriers. This is a recent development, a
political product of World War I and the decade following (1914-24).
Prior to 1914, there were-no passports in the modern sense. Physically
healthy people could cross national borders permanently without
significant legal interference. The ilth edition of the Encyclopaedia
Britannica, published in 1911, begins its brief section on “Passport” as
follows: “PASSPORT, or safe conduct in time of war, a document
granted by a belligerent power to protect persons and property from
the operation of hostilities.” The idea of a passport was the protection
of individuals of onc nation from bureaucrats of another. “In its more
familiar sense a passport is a document authorizing a person to pass
out of or into a country, or a licence or safe-conduct to the person
specified therein and authenticating his right to aid and protection.
Although most foreign countries may now be entered without pass-
ports, the English foreign office recommends travellers to furnish
themselves with them, as affording a ready means of identification in
case of need.” Most people today can barely imagine such a world.
‘Today, passports are universally required as a means of control over
travellers from abroad. They are used to track the movements of
travellers, foreign and domestic. (They are now computerized in the
United States.) Passports exist for the sake of the bureaucrats, not
citizens. We need to ask ourselves: What happened to the Old Testa-
ment concept of sanctuary, which Western nations, especially Eng-
land, Holland, the United States, and Switzerland," scrupulously
honored before World War I?

What happened was that mass democracy had a head-on colli-
sion with cheap mass transportation. The almost destitute and the
homeless of the world can now afford to seek sanctuary in a geo-
graphically distant land of plenty. And so the gates were closed by
legislation, nation by nation.'® Citizenship today is by birth in some
nations; if you are a resident alien, your children will automatically
become citizens. Thus, to keep from getting politically “swamped”
by the children of strangers, these nations have closed their gates. In
other nations, citizenship comes with property ownership. So, the

15, All were originally Calvinist nations, it should be recalled.
16, In the United States, the 1924 immigration law was the major event. See Roy
L. Garis, /mmigration Restriction (New York: Macmillan, 1928).
