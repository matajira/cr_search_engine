A New National Covenant 563

The humanists have abandoned natural law; so have the theono-
mists. The Marxists never did accept the theory. Thus, whether the
case law approach of the Harvard Law School is adopted or the case
law approach of the Bible, natural law or natural rights philosophy
no longer provides either covenantal legitimacy or judicial restraint;
the philosophical-moral foundation of the original Constitutional
settlement—but not the actual document—has disappeared. It is
therefore just a matter of time and escalating crises for the U.S. Con-
stitution to go the way of the Articles of Confederation. It can be re-
defined into something new by the courts, as has been done for over
a century, or else it can be replaced by a series of amendments over
many years or overnight by a Constitutional convention. If the final
option is selected by those who make long-term political plans, it is
not the Christians who are the likely candidates to achieve a victory.

Strangers in Their Own Land

The second set of restraints on the Framers was political: Chris-
fan voters. They still controlled or heavily influenced state politics.
They had lost only the battle in Philadelphia. For a time, they re-
mained a threat to the humanists who ran the country, but it was a
downhill battle after 1788. Liberal theologian and University of
Chicago professor of Church history Martin Marty waxes eloquent
regarding Franklin and his Deist peers. “Fortunately for later Amer-
icans, the Founding Fathers, following the example of Franklin, put
their public religion to good use. While church leaders usually
forayed only briefly into the public arena and then scurried back to
mind their own shops, men of the Enlightenment worked to form a
social fabric that assured freedom to the several churches, yet stressed
common concerns of society.”* Today, however, the “common con-
cerns of society”—legalized pornography and prostitution, for in-
stance —have come into conflict with local church freedom.

What Marty and virtually all contemporary historians fail to dis-
close is that virtually all of these leaders of the American Enlighten-
ment had a working model for this common “social fabric”: the
Masonic lodges of America (and in Franklin’s case, of France).
Some were actual members, bound by its oaths; others were simply
literate men of their time, and Masonry was #ke religion of the New-

4. Martin E. Marty, Pilgrims in Their Own Land: 500 Years of Religion in America
@oston: Little, Brown, 1984), p. 158.
