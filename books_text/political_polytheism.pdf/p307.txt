Halfway Covenant Historiography 283

Period and the Moral Roots of New England Federalism.” I could go
on, but why bother? You get the point.

In 1979, a book by Hatch, Noll, and a third pluralist historian,
John D. Woodbridge, was published by Zondervan, The Gospel in
America, In this book, we learn that “The watershed of the American
sense of mission and destiny can be traced to Puritan England. Re-
cent studies have emphasized the intense sense of religious
patriotism that dominated seventeenth-century England —and par-
ticularly those of Puritan persuasion. . . . The Puritans who came
to establish New England were even more intense in their conviction
that God had chosen them. The theme that crossing the Atlantic was
the Exodus for God’s ‘New English Israel’ runs throughout the early
sermons of Massachusetts,”?0!

‘They also say that “During the colonial period the strongest
attack on the idea that America was a city on a hill came from the
lips of Roger Williams, founder of Rhode Island and spiritual father
of American Baptists.”*? Do they then go on to demonstrate that
Williams’ opinion became dominant during the eighteenth century?
On the contrary:

In the years prior to American independence, the dissenting voice of
Roger Williams gave way to an ever stronger chorus that America was
unique—and providentially so. In New England ministers continued to ap-
ply ta their own colonies Old Testament texts addressed to Israel; they
began also to address the Puritan founders by such names as Moses,
Aaron, and Joshua. If other Americans considered Yankees a bit provincial
because of this, they were soon relieved to find that in the aftermath of the
Great Awakening (a revival ignited throughout the colonies) that Jonathan
Edwards announced that not just New England but ail of America would be
the center of Christ’s millennial kingdom. And as the Americans took up
arms against the French in the Seven Years’ War (1756-1763), many agreed
with the evangelical Presbyterian Samuel Davies from Virginia that de-
feating Catholic France would wound Antichrist sufficiently to bring on
that long-awaited reign.?°*

There was one important shift in perspective, however: the reason
offered for this millennial triumph. It was America’s civil Itberty
which was the basis of God’s favor.?°+

201, John D. Woodbridge, Mark A, Noll, and Nathan ©. Hatch, The Gospel In
America: Themes in the Story of America’s Evangelicals (Grand Rapids, Michigan: Zon-
dervan, 1979), p. 212.

202. Ibid., p. 213.

203. Thid,, p. 214.

204. Idem.

 
