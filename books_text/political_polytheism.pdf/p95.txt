Sanctuary and Suffrage ai

open borders and sanctuary status internationally. 7 maintain its
open borders, Israel had to close its political borders.

There can be no covenant without boundaries. These boundaries
are judicial, God’s Church covenant places a judicial boundary
around the communion table: Access to it is limited to those who
have been baptized. God’s family covenant places a judicial bound-
ary around the home. To violate this boundary sexually is to commit
adultery and thereby break the covenant.!! God’s civil covenant
places judicial boundaries around the voting booth, the jury room,
and the civil magistrate’s office. To violate these boundaries is to
commit revolution or treason against God. To invade these bound-
aries physically is to disobey the biblical covenant. '? To deny the ex-
istence of these boundaries is to deny the biblical covenant.

Had strangers in ancient Israel been given access to the office of
civil magistrate, they would cventually have governed openly in
terms of the laws of their household gods. Israel could not possibly
have remained a civil sanctuary. There would have been endless
political battles and-even civil wars -~ wars between rival gods. In the
ancient world, wars were understood as wars between gods or be~
tween rival alliances of gods, as Homer’s I/iad makes clear, and as
the Bible does, too.1?

9. Did., ch. 3.

10. Ibid., Appendix 9.

11. Ray R. Sutton, Second Chance: Biblical Blueprints for Divorce and Remarriage (Ft.
Worth: Dorninion Press, 1987).

12. While it is legal for officers of one office to invade the geographical boundary
of another in order lo enforce a law against an individual, it is unlawful for the judi-
cial boundary itself to be broken unless those inside have broken a covenant, For ex-
ample, a police officer can lawfully break down the door of a house if he has reason
to believe that a husband inside is strangling his wife, but this is still a protection of
the home. The husband has broken the covenant judicially by strangling his wife.
‘The policeman is acting in his capacity as the legal agent of the wife, who as the vic-
tim is the covenantal representative of the family covenant. The husband has placed
himself outside the covenant, and therefore outside its protection.

13. “They fought from heaven; the stars in their courscs fought against Sisera”
(Judges 5:20). The “stars” here were clearly angels, not physical stars. The Bible
does not promate astrology. A similar use of “stars” is found in the Book of Revela-
tion: “And I beheld when he had opened the sixth seal, and, lo, there was a great
earthquake; and the sun became black as sackcloth of hair, and the moon became
as blood; And the stars of heaven fell unto the earth, even as a fig tree casteth her
untimely figs, when she is shaken of a mighty wind” (Rev. 6:12-13), These fallen
stars are angels. Obviously, the flaming stars in the heavens do not literally come
crashing down on earth. T am a biblical literalist, but let’s agree to be sane about
literalism.

 
