732 POLITICAL POLYTHEISM

chance, 45

Christian Reconstruction, 32
covenantal, 52

denied, 31-32

ethics, 141-47, 159

gaps in, 45

positive feedback, 32-35
Van Ti, 137-46

CBN University, 176n.

cease-fire (pluralism)
breakdown, xix
humanism’s, 2
permanent?, xv
pluralism, 250
surrender, 630-31
temporary, 4, 87n, 91,

227-28, 265, 294

central planning, 99

Chalcedon Report, 3610

chain of being, 337

chance, 37-38, 45

chaos, 45, 80-81

chaos theory, x, 45, 337, 353

chaplains, 527

Charles I, 313, 377

Charles Il, 312-14, 445, 460-61,
476, 647

check-kiting, 224, 294-95

chemistry, 338-39

Cherokee Nation vs, Georgia, 510

Chilton, David, 194-96

China, xiv, 10

Christian nation, 241, 564-66, 672
(see also Marsden)

Christian political pluralism
anti-blueprints, 234
anti-Conslantine, 536
Calvinist spokesmen, 110-11,

121-25, 250
“cognitive dysfunction,” 297
defenseless intellectually, 22,
122-25, 250
“Egyptian,” 537
hypnotized by apostates, 658
Myers’ defense, 121-22
natural law (atheism of), 247
Novak on, 100, 102-3

Neuhaus on, 100-1

pantheon, 536-37

rhetoric of, 121-25, 254-55, 658

Schrotenboer on, 123

Skillen on, 124-25

Spykman on, 16n, 17,

task of defenders, 106-7

taskmasters’ theology, 537

Troost on, 123-24

Christian Reconstruction

alternatives to, 214

Arminians &, 659

Athanasian pluralism, 594-97

bottom-up, 39, 157, 224, 529, 585,
590, 612, 647, 649-50

Calvinistic, 612

causality, 32

creeds &, 57-58

critics of, 597-98

decentralization, 590

defined, 659

Holy Spirit, 589

insufferable, 32

majority rule, 585-86, 649

new foundations, 610-611

postmillennial, 155-57 (see also
Holy Spirit, revival)

replacement strategy, 610

revival (see Holy Spirit, revival)

strategy, 20-21

salvation described, 611

sanctions in history, 147, 149

social theory, 32

watershed period (1980's), 214

Christianity

alliance with humanism, 245-46,
400, 464, 527-28, 591

America’s failure, xiv

bondage, 63, 75-76

capitalism &, 177-78

Christendom, 528-31, 579-80

citizenship, 81

compromise (see common ground)

Constitution &, 324, 675-76, 683
(see alse test oath)

culture &, xi, 160, 243-44, 579,
588-89, 668

 
