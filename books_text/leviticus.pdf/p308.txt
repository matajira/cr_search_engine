252 LEVITICUS: AN ECONOMIC COMMENTARY

done them all in. So Moses had to separate some to save oth-
ers.”* Wildavsky could also have offercd the example of the
tribe of Benjamin, whose rebellion led to the military destruc-
tion of almost the entire tribe by the other tribes (Jud. 20). Sin
was contained. Israel’s tribal boundaries served as restraints
against the spread of covenantal rebellion. In this sense, tribal
boundaries had a primary defensive judicial aspect: preserving the
authority of local jurisdictions and outlooks.

These boundaries also had a secondary expansive judicial as-
pect. A local jurisdiction could begin to apply God’s law in a
new way, and this new application might prove beneficial to the
local community. Localism leads to experimentation. A tribal
unit could become a kind of judicial laboratory. The rest of the
nation could see if God blessed this experiment. (This presumes
that God did bring predictable, visible, positive corporate sanc-
tions in history in response to corporate covenantal faithful-
ness.) At the discretion of the local community, the new judicial
practices of another tribe could be imported. But the importing
initiative was local, unless the nation’s supreme civil authorities
mandated the change in the name of God’s law. If the nation’s
appeals court used the local guideline as a judicia) standard, it
would become a national standard.

Localism in Mosaic Israel was offset in part by the presence
of Levites: local advisors who rarely had an inheritance in rural
land. Instead, they had income from the tithe (Num. 18:20-
21) and urban property (Lev. 25:32-34). They served as special-
ized judicial agents of God. The tribe of Levi was the only
cross-boundary national tribe: the tribe that publicly spoke

4, Aaron Wildavsky, The Nursing Father: Moses as a Political Leader (University,
Alabama: University of Alabama, 1984), p. 112,

5. There were two exceptions: 1) land that had been vowed for use by a priest
but then was leased by the vow-taker to someone else; 2) land that had been vowed
for a priest which was then voluntarily forfeited by the heirs at the time of the jubilee
(Lev, 27:20-21), See Chapter 37, below.

 
