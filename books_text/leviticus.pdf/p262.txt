206 LEVITICUS: AN KCONOMIG COMMENTARY

Himself as the owncr of the Promised Land. He did not verbal-
ly claim an equally special ownership of businesses. The land,
not business, was identificd as God’s covenant agent that
brought God’s covenant lawsuits in Old Covenant Israel.'* Any
attempt to derive a modern system of charity, public or private,
from the gleaning law faces this crucial limitation. It was not
intended to apply outside a farm.

The modern welfare State is a perverse mirror image of the
gleaning law. It disregards the moral boundaries of charity and
substitutes unconditional charity. This has greatly expanded
both the political boundaries of charity and the extent of pover-
ty. People get paid by the State for being poor; the market
responds: more poor people. The welfare State now faces bank-
ruptcy: the destruction of those dependent on its support.”

‘There are few modern applications of the gleaning law,
which was a land law. Modern society is not agricultural.’
Nevertheless, there is a theological principle that undergirds
gleaning: fallen man is always a gleaner. But redeemed men will
progressively escape their dependence on other men’s charity
as society advances through God’s grace.

Conclusion

‘The gleaning law was part of an overall system of political
economy. Many of the details of this political economy were
tied to the Promised Land and the sacrificial system of that
land. ‘The economic laws of Leviticus were more closely at-
tached to the Promised Land and the sacrifices than the laws of
Exodus and Deuteronomy were. The Levitical lund laws were part
of a temporary system of landed familism, tribalism, and localism.

16. See Chapter 10, above.

17. North, Boundaries and Dominion, ch. 11, scction on “Unconditional Charity:
Political Boundaries.”

18. fbid., ch. 11, section on “Modern Applications of the Gleaning Law Are Few.”
