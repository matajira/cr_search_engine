21

INHERITANCE THROUGH SEPARATION

% shall therefore keep all my statutes, and all my judgments, and do
them: that the land, whither I bring you to dwell therein, sprue you not
out. And ye shall not walk in the manners of the nation, which I cast out
before you: far they committed all these things, and therefore I abharved
them. Bui I have said unto you, Ye shall inherit their land, and I will
give it unto you to possess it, a land that floweth with milk and honey. 1
am the Lorn your God, which have separated you from other people. Ye
shall therefore put difference between clean beasts and unclean, and
between unclean fowls and clean: and ye shall not make your souls
abgminable by beast, or by fowl, or by any manner of living thing that
creepeth on the ground, which I have separated from you as unclean.
And ye shall be holy unto me: for I the Lorn am holy, and have severed
you from other people, that ye should be mine (Lev. 26:22-26).

The theocentric foundation of this law was God’s act of
covenantal separation: “I am the Lorp your God, which. have
separated you from other people” (v. 24b). The Creator God
has separated His people from all other people. This separation
is not only historical; it is eternal. It is above all covenantal. It
has ethical and judicial implications. The fundamental issue is
holiness: the set-apartness of God and also of His people. This
jaw was one of these implications of holiness. Some of these
implications are still in force judicially; others are not. It is the
