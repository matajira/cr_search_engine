Sacred, Profane, and Common 123

the Mosaic Covenant, circumcision and Passover; in the New
Covenant, baptism and the Lord’s Supper. The word sacrament
comes from the Latin word sacramenium, a military oath of
enlistment.”

Anything that violates these holy things of the Lord is con-
sidered profane. In contrast, anything that violates a non-holy
thing is not considered profane. Such a violation is illegal, but
it is not profane. This is the heart of my thesis in this chapter:
the association of the biblical concept of profane with unique acts
of violation, namely, violations of a boundary surrounding a
judicially holy place or holy object. Profanity in the broadest
sense is a breach of a judicial wall of separation between the
holy and the common.

Leviticus 5:14-19 offers evidence of a judicial distinction
between the sacred and the common, but this difference is
minimal in the case of unintentional transgressions: a 20 per-
cent penalty for violating either a sacred object or sacred space
(vv. 15-16). What kind of boundary had been transgressed?
Was it geographical? This scems unlikely. We know that the
common Israelite was not permitted to enter the inner core of
the temple, on threat of death (Ex. 28:43). He would never
have been in a position to commit a tabernacle or temple tres-
pass in ignorance. Furthermore, no common priest in his right
mind would have tried to enter the holy of holies. He could not
have committed such a transgression ignorantly. So, the ele-
ment of the sacred here must refer to something broader in
scope than the performance of temple rituals.

If we are properly to understand the nature of each type of
transgression in Leviticus 5:14-19 — cach type of boundary
violation — we must first understand what the idea of the sacred
meant under the Old Covenant. ‘Then, and only then, can we

7. “Sacrament,” in Cyclopacdia of Biblical, Theological, and Ecclesiastical Literature,
edited by John M’Clintock and James Strong, 12 vols. (New York: Harper & Bros.,
1894), LX, p. 212.
