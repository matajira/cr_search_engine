The State’s Monopoly of Vengeance 269

should be held.” The person who faces a life-threatening as-
sault must decide which risk is greatest: 1) death from the
assailant if no action is taken; 2) death from the assailant in a
failed self-defense; 3) death from the State for murder of the
assailant. There is a slogan used by American defenders of their
Constitutional right to own and use guns.” “I would rather be
tried by twelve than carried by six.” When a person is faced
with a life-threatening attack, a jury in his future is preferable
to pallbearers.

The plea of self-defense is in fact a plea of the right to de-
fend oneself as an authorized agent of the State. Self-defense is
not an autonomous act of violence. It is not an act of ven-
geance. It is a boundary delensc.

What is clearly prohibited is vengeance by the victim after
the suspect has fied from the scene of the crime. In such a case,
there can be no claim of self-defense if the suspect dies as a
result of the attack. The victim [aces no life-threatening attack.
His response is therefore limited to bringing a lawsuit. He may
lawfully seek out the civil magistrate as a public avenger, but he
is not allowed to impose vengeance unilaterally.”

10. In cornmon law, this authority to decide to hold a trial belongs to the grand
jery, which hands down an indictment. Then the tial is held.

11. This is the Second Amendment of the Constitution: part of the original Bill
of Rights. More than any other Constitutional guarantee, this one is under assault by
the State in late-twentieth-century America. It was imposed on the Federal govern-
ment in the 1790's because citizens had achieved parity of weaponry with the State.
They were determined to keep this parity, which they recognized as the means of
enforcing boundaries on the State. Parity in weaponry was the technical basis of the
advent of modern democracy. Carroll Quigley, Tragedy and Hope: A History of the
World in Our Time (New York: Macmillan, 1966), pp. 34-35. Quigley was an expert in
the history of warfare and weaponry and their relation to politics.

   
 

 

12. Some legal codes authorize people to pursue a criminal who is fleeing from
the scene of a crime. ‘his is the doctrine of citizen’s arrest. Civil government may
lawfally authorize such a practice. ‘his law in effect makes the citizen a deputy of the
State. If the suspect is injured by the citizen-arrester under such circumstances, the
citizen would be at legal risk if the suspect is not subsequently convicted for the crime
in question.
