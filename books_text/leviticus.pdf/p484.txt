428 LEVITICUS: AN ECONOMIC COMMENTARY

passport is one of humanism’s important covenantal marks: a
progressive contraction of international sanctuarics. Political
liberals as well as political conservatives have affirmed the legiti-
macy of these immigration barriers.°° When nations are no
longer covenantally Christian, ie, when they adopt religious
pluralism and other marks of citizenship besides church mem-
bership, and when they replace voluntary charity with welfare
State entitlements, the Christian evangelist’s call to the lost in
the name of Christ steadily fades. “Come unto me, all ye that
labor and are heavy laden, and | will give you-rest” is replaced
by “Keep out those welfare-seeking bums!” Finally, when man-
datory identification cards are issued by the State to every
resident in order to “reduce welfare fraud,” all remaining sanc-
tuaries tend to disappear: in churches, regions, and families.

Land Ownership by Foreigners: Then and Now

Under the initial distribution of the land under Joshua, no
non-citizen could own rural land. Not every citizen had to own
rural Jand — most notably, a circumcised immigrant or his heir
who was eligible to serve in the army — but every rural land
owner had to be a citizen.

The primary legal issue for rural land ownership in Mosaic
Isracl was adoption, not confession. Both the confessing resi-
dent alien [geyr] and the non-confessing resident alien [nokree}
could buy inheritable residential real estate inside walled cities.
Confession had nothing to do with urban residential ownership.
On the other hand, covenant-keeping converts to the faith had
no access to rural land ownership apart from their adoption
into a family of the conquest generation. The resident alicn’s
orthodox confession had nothing to do with inalienable rural
ownership except insofar as such confession was necessary for
legal adoption into an Israclite family.

36. The ultimate immigration barrier is abortion,
