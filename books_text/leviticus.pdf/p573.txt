Slaves and Freemen 517

A little over a century later, slavery had been abolished in the
West. This was one of the most remarkable theological, moral,
and judicial transformations in history.*

In Tools of Dominion, I devoted over one hundred pages to a
discussion of the biblical theology of slavery.’ It would be un-
wise for me to reproduce that chapter here. It was appropriate
to include such a discussion in a book dealing with the case
laws of Exodus because the case laws begin with a consideration
of the purchase of a slave (Ex, 21:2-6). Slaves on their way oul
ofa generation of servitude and into freedom would have been
interested in a law governing slavery. The economics of slavery
was governed by the jubilee’s laws of inheritance: the preserva-
tion of freemanship.’ Without the. jubilee law, slavery lost its
legitimacy. ‘Uhis line of argument was never raised by abolition-
ists, whether Christians or Jews, nor is it acknowledged today
by those few theologians who refuse to break with the principle
of abolitionism.®

The Jubilee Context

It is my contention that the laws governing permanent hea-
then slaves were an unbreakable part of the jubilee laws. If I
am correct, this means that the exegetical case in favor of the
annulment of the heathen slave laws rests on the New Testa-
ment’s annulment of all of the jubilce laws. It is also my conten-

3. Ibid., p. 108.

4, Gary North, ‘ols of Dominion: The Gase Laws of Exodus (Tyler, Texas: Tnstitute
for Christian Economics, 1990), ch. 4.

5. Ibid., pp. 140-44, 166-68, 144-47, Reprinted with modifications in Gary North,
Boundaries and Dominion: The Economics of Leviticus (computer edition; ‘Iyler, ‘Yexas:
Institute for Christian Economics, 1994), ch. 31, sections on “Che Economics of
{sraelite Slavery,” “Slavery and Hell,” and ‘Jesus’ Annulment of the Jubilee Land
Laws.”

6. Most notably in the Calvinist world, Professor John Murray of Westminster
Seminary. Murray, Principles of Conduct: Aspecis of Biblical Ethics (Grand Rapids,
Michigan: Ferdmans, [1957] 1964), pp. 100-1, For a critiqne, see North, Boundaries
and Dominion, ch. 31, section on “The Ethics of Slavery.”
