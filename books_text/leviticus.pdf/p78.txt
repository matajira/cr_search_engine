22 LEVITICUS: AN ECONOMIC COMMENTARY

weather God promised to bring on the land when the nation
obeyed Him. The very high ecclesiastical costs of living in Israel
would have placed the Israelites at a competitive disadvantage
in relation to those foreign farms located close to Isracl’s bor-
der. If anything, Israclites living on the borders of the nation
would have had to become manufacturers, trading their goods
for imported food and other manufactured goods. Conclusion:
inside Israel, journeying Israelites would have been forced to
exchange manufactured goods or services for other manufac-
tured goods. ‘his would have made Jerusalem a center of tradc
and information: goods and information brought from the
edges of the nation’s borders.

The required feasts would have created economic incentives
for residents located close to foreign borders to import goods
from abroad in exchange for goods produced in Isracl, and
then use these imports to pay for their mandatory journeys.
But they would not have exported any crop that was not
unique to Israel. What is called the law of comparative advan-
tage operated in agriculture. Isracl must have imported food
from abroad in cases where transportation costs were low, espe-
cially in citics close to the Mediterranean or close 1o foreign
borders. Why? Because there is no question that foreigners who
did not bear the high agricultural production costs borne by
Israelites could serve as exporters of food to Israel. Foreign
farmers who lived close to roads into Israel or the sea had a
decisive economic advantage in those years in which famine did
not strike their land — a curse God promised to keep away from
Israel if His people obeyed Him. It should be clear that God’s
law was designed to move His people from the farms to cities.
From the citics, they were to move out across the entire globe.

Those living near highways probably did not farm crops that
were immediately consumable, such as corn, fruit, olives, etc.
The law allowed neighbors to pick a handful of the crop free of
charge (Deut. 23:25). This would have included Jews on a
