412 LEVITICUS: AN ECONOMIC COMMENTARY

meck before covenant-breaking men.’ To be compelled to be
meek before covenant-breaking men is evidence of God’s tem-
porary chastisement of His covenant people. It is to be forced
to adhere to an illegitimate civil oath.*

On the day above all other days in which each Israelite
publicly manifested his subordinate position before God, the day
of atonement also served, twice per century, as the day of inheri-
tance, the day on which a man’s inheritance was returned to
him. Ile would henceforth regain legal authority over a piece of
land. He would then discover if he had the skills and foresight
as an cntreprencur — a future-predicting planner and executor
of plans® — to retain economic authority over it as an economic
representative of God, his family, and the consumers in the
marketplace. The hierarchy of consumcr’s sovereignty would
henceforth operate though the land’s owner, not through a
leascholder.° This would be a subordination based on the
principle of sabbatical rest.1!

The Spoils of War

“In the year of this jubile ye shall return every man unto his
possession” (Lev. 25:13). This provision applied to rural land.
It did not apply to property in walled cities (Lev. 25:29-30). It
did not apply to non-agricultural property.

What was the historical origin of this law? Judicially, it was
an application of the Mosaic sabbath (Ex. 23:10-12). But histori-

 

7. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), pp. 436-39.

8. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989).

9. Ludwig von Mises, “Profit and Loss” (1951), in Planning For Freedom (4th ed.
South Holland, Ilinois: Libertarian Press, 1980), ch. 9; Frank H. Knight, Risk,
Uncertainty and Profit (New York: Harper Torchbooks, [1921] 1965). Cf. North,
Dominion Covenant, ch. 23.

10. North, Boundaries and Dominion, ch. 26, section on “A Question of Subordina-
tion.”

IL. Ibid., ch. 25, subsection on “Rest and Subordination.”
