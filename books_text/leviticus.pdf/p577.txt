Slaves and Freemen 521

Slavery as a Model of Sin

Heathen residents of Isracl could be permanently enslaved
to repay their debts. ‘lhe presence of permanent slaves in Isra-
elite households was a visible testimony of what il means to be
outside the inheritance of God. Slave status was like a perma-
nent sign in front of a person’s eyes: “No Exit.” This was the
representative mark of eternal punishment. There is no exit for
Adam’s heirs apart from. adoption into the family. of God
through Jesus Christ, the firstborn Son. The Seed — the culmi-
nation of the Abrahamic promise — lawfully inherited the land.
Elect gentiles are heirs of this promise. But the focus of this
promise is liberation from sin. Those who trust in the law for
their inheritance are disinherited, replaced by those adopted by
grace. This is why Jesus’ message outraged the Jews. Paul
spelled out the message in its judicial context: promise, inheri-
tance, and seed. He began his discussion with the redeemed
person’s escape from the imputation of Adam’s sin (Rom. 4:8-
18).

It was Jesus Christ who sacrificed His lawful inheritance in
the Promised Land in order to bring His brethren through
adoption into the family of God. The son of David abandoned
His lawful inheritance for the sake of His elect. In doing this ~
delivering to them the promised inheritance — He gave them
their irrevocable judicial status as freemen.

It is worth noting that the judicial precedent for this act was
Joseph’s decision to forfcit his status as the namesake of a tribe
of Israel for the sake of his Egyptian sons, Ephraim and Manas-
seh (Gen. 48). [is father Jacob acquiesced to this transfer of
inheritance: the name. Jacob thereby adopted into his house-
hold the foreign-born sons of an Egyptian mother: gentiles.
Thus, even prior to the announcement regarding the promised
Seed, Shiloh (Gen. 49:10), there had been an adoption by the
patriarch which disinherited his son for the sake of this beloved
son’s gentile sons. Joseph, the kinsman-redeemer of Israel/Jac-
ob, was the primary redemptive model in the Old Covenant for
