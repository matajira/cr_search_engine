414 LEVITICUS: AN ECONOMIC COMMENTARY

tance laws are so frequently misinterpreted, including their
various applications to areas completely outside of the jubilee
land law’s agricultural frame of reference.

Genocide and Burnt Offerings

For the Israelites to inherit the land, they were required to
kill everyone who had previously occupied the land. “And thou
shalt consume all the people which the Lorp thy God shall
deliver thee; thine eye shall have no pity upon them: neither
shalt thou serve their gods; for that will be a snare unto thee”
(Deut. 7:16a). Note that the key issue was theology: the gods of
the land’s previous owners. ‘The people of Israel were to be
kept away from these alien gods.

God required a bloody burnt sacrifice as the covenantal
foundation of the national inheritance: the genocide of the
residents of Jericho and the city’s subsequent burning. ‘This
mandatory ritual sacrifice was to be followed by the total
annihilation of all other residents of the Promised Land. To the
degree that the Israelites in any way pitied the existing inhabit-
ants, they would thereby compromise their inheritance. They
would have to share the land with others.

Recent commentators have attempted to apply the jubilec
laws to the modern world as if these laws had not been ground-
ed in genocide. ‘The original promise had been given to Abra-
ham, but it was conditional on the heirs’ continuation of the
ritual of circumcision: a bloody rite symbolizing the cutting off
of a man’s biological heirs. ‘This is why the generation of the
conquest had to be circumcised before the conquest could begin
(Josh. 5). Commentators who do not trace the origin of the

of the Biblical Jubilee (Rome: Pontifical Biblical Institute, 1954), is a good example of
modern scholarship. Based on higher critical assumptions and methodology, it never
mentions the jubilee in relation 10 the conquest of the land.

13. Achan and his entire family, including their animals, were executed for his
having thwarted this required burnt offering. Sec North, Boundaries and Dominion,
Appendix A; “Sacriloge aud Sanctions.”
