166 LEVITEGUS: AN ECONOMIG GOMMENTARY

The house law, which was given in the wilderness period, speci-
fied that when the people came into the land of Canaan, and
built or inherited houses, those houses would sometimes be
subjected to the curse of plague. It began: “When ye be come
into the land of Canaan, which T give to you for a possession,
and [ put the plague of leprosy in a house of the land of your
possession. . .” (Lev. 14:34). This law was restricted to Canaan,
as we shall sec.

God said that He would put the plague of leprosy on a
house. When the owner of the house discovered an outbreak of
mold in the house’s walls, he was required to go to the priest
and inform him of the fact (Lev. 14:35-38).

The Sanctity of the Priest

Before entering the house, the priest saw to it that every-
thing inside the house was first removed. It is specifically said
that this would keep everything inside the house from becom-
ing unclean. “Then the priest shail command that they empty
the house, before the priest go into it to see the plague, that all
that is in the house be not made unclean: and afterward the
pricst shail go in to sec the house” (Lev. 14:35). After the house
was cmptied, the priest would go into the house. ‘This indicates
very clearly that the problem was not the spread of disease
inside the house, but rather the judicial sanctity of the priest. If this
sanctified agent were to enter the house when the housc was
under suspicion, this would make all of the implements and
furniture of the house unclean if the house was found to be
unclean. The boundary here was primarily judicial rather than biologi-
cal. The house was not judicially unclean until the priest
crossed its doorway boundary. He himself would not become
unclean. When he did cross it, if he then corroborated the
symptoms, everything inside the house at the time of his en-
trance would become unclean.

Note carefully that the text docs not say that the things
inside the house would become unclean after the priest entered
