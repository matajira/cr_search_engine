The Redemption Price System 605

redeem his field only in the. [inal sabbatical year before the
jubilee, when the unseeded output of the field would be mini-
mal, or in the jubilee year itself.’* If he or his surviving heirs
decided not to redeem it, his family lost the field forever. The
translators’ interpretation of the 50 shekels — applying to a
homer of barley — would lead us to the conclusion that the
details of the prorated redemption payment structure were
merely symbolic, for almost no one could have afforded to
redeem his field much before the jubilee year.

If the conventional translation is correct, we are led inexora-
bly to this unpalatable conclusion: once the owner dedicated
the field to the priesthood, he could not expect to redeem it
until the jubilee year. The price would have been far too high.
‘This seems to be too radical a requirement: a redemption price
totally disconnected from the market price. Conclusion: the
reference to 50 shekels of silver refers to the fixed judicial price
of a field that would produce one homer of barley per season
through the entire jubilee cycle. The closer to the jubilee year,
the lower the field’s remaining redemption price. In short, the
redemption price of a licld capable of producing one homer of
barley per year was 50 shekcls of silver at the beginning of the
jubilce cycle, plus 20 percent.

My conclusion is that the commentators’ conventional inter-
pretation, not the translators’ conventional translation, is cor-
rect: the prorated redemption price was one shekel of silver per
year remaining until the jubilee year per homer-producing unit
of land. This means that translators should abandon the famil-
iar translation: “[a] homer [ol] barley seed [shall be priced at]
fifty shekels [of] silver.” It should be translated as follows: “[A
field producing a} homer [of] barley sced [per ycar shall be
priced at] fifty shckels [of] silver [at the beginning of the jubilee

15. Legally, the crop could not be harvested. Probably this would have been
interpreted as a crop of zero output. If the estimation was made in terms of harley
seed used for planting, the price had to be zero, since it was illegal for anyone to
plant in a sabbatical year or a jubilee year.
