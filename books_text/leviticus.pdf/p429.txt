Blasphemy and Civil Rights 373

Blasphemy is cursing: a verbal attack on God, comparable to
assault. Blasphemy calls down on God some sort of implicit
negalive sanction for having misrepresented Himself. Blasphe-
my is the opposite of the positive sanction: “Bless God.” The
classic example of blasphemy in the Bible is the statement by
Job’s wife: “Curse God, and die” (Job 2:9b). The two events are
associated: cursing God and death.

It is almost universally assumed today that God’s name
needs no civil protection. God is not regarded as a person in
the legal order. He therefore is entitled to no judicial protec-
tion. This means that men are not to be threatened by the State
for cursing God, i.e., publicly saying that the God of the Bible
is a liar, or a cheat, or some other kind of reprobate being, and
therefore deserving of contempt.

Treason against God is considered judicially irrelevant. Trea-
son against the State is regarded as serious, but. concern over
even this form of treason has faded in recent decades. Treason-
ous acts that would have led to execution in 1950 today result
in life imprisonment with no possibility of parolc, which means
parole will be possible a decade later. There is a judicial reason
for this: wars fought by the United States after World War II
have not been formally declared wars. The U.S. Congress has
not formally declared war, which Constitutionally is the only
legal basis of war. With no formally declared enemy, it has been
difficult to prosecute treason.'' With the demise of the Soviet
Union. in 1991, it is likely that another round of reduced sanc-
tions for treason will result: the West’s primary enemy has

11. The obvious example was the popular actress Jane Fonda during the Viet-
nam “war.” Legally, she did not commit treason. Conservatives ridiculed her as
“Hanoi Jane,” but they could not say that she was treasonous, despite her public
endorsement of Communism. Her career soared during and after the conflict.
Similarly, American pilots captured by the North Vietnamese were not entitled to
Geneva Convention protection because the United States had not declared war
against North Vietwam. The North Vietnamese treated them as spics who have no
tights. They were tortured without legal appeal. See Jeremiah A. Denton, Jr, When
Hell Was in Session (Clover, South Carolina: Riverhilis Plantation, 1976).
