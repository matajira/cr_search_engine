96 LEVITICUS: AN ECONOMIC COMMENTARY

will become progressively tyrannical, no matter which authority
structure the State adopts: oligarchic, democratic, republican,
bureaucratic, or monarchical (today a defunct ideal).

The Door of the Tabernacle

The priest had to sacrifice a young bullock in order to turn
back the negative sanctions of God against those who were
under the priest’s authority. These sanctions threatened not
only the priest; they threatened that segment of the covenanted
community under his lawful jurisdiction. The atoning sacrifice
had to take place ai the door of the tabernacle of the congrega-
tion. “And he shall bring the bullock unto the door of the ta-
bernacle of the congregation before the Lorp; and shall lay his
hand upon the bullock’s head, and kill the bullock before the
Lorn” (Lev. 4:4). The very place of sacrifice is designated by
God as the congregation’s tabernacle, i.c., a dwelling place. This
was the place where God met the congregation. “This shall be
a continual burnt offering throughout your generations at the
door of the tabernacle of the congregation before the Lorn:
where I will meet you, to speak there unto thee” (Ex. 29:42).
This was the dwelling place of God, but it was also the dwelling
place of the congregation. Although the people were not al-
lowed bodily into the presence of God, the furniture of the
tabernacle symbolically represented them. The tabernacle was the
place where the dual citizenship — heaven and earth”® — of both man
and God was publicly revealed. Covenant-keepers in history are
not citizens merely of earth (Phil. 3:20), and Cod in history is
King not mercly in heaven. The whole creation is His kingdom,
and to prove this, He brings His sanctions in history, both
directly and representatively.

26. This was fulfilled in Christ: “That in the dispensation of the fulness of times
he might gather together in one all things in Christ, both which are in heaven, and
which are on earth; even in him” (Eph. 1:10).
