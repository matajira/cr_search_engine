28

THE RIGHT OF REDEMPTION

The land shall not be sold for ever: for the land is mine; for ye ave
strangers and sojourners with me, And in all the land of your possession
ye shail evant a redemption for the land (Lev. 25:23-24).

We begin with a theocentric analysis of this passage. The
prohibition against the permanent sale of rural land was con-
nected to the nation’s judicial status as strangers and sojourners
with God. What did this mean? God began to dwell in the land
of Israel when the conquest began, i.e., after the nation had
crossed Canaan’s border. This means that He lived among
them judicially. He did not take up residence with them physi-
cally. [Tis unique judicial presence in the land was marked
physically by the prescnce of the two tablets of the law inside
the Ark of the Covenant. Even this testimony had to be taken
on faith; no one was allowed to look mside the Ark. When this
law was violatcd by the men of Bethshemesh, God killed 50,070
of them (I Sam. 6:19). Negative corporate sanctions came im-
mediately after God allowed the corporate infraction to take
place.!

1. Had the first three or four people who fooked inside the Ark immediately
been stricken with leprosy, as Miriam was stricken in the wilderness (Num. 12:10),
the infraction would have ceased.
