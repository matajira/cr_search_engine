648 LEVITICUS: AN ECONOMIC COMMENTARY

principle of the leaven - the best a man can ofler God from his
“field” - has not been removed from the New Covenant’s vol-
untary offerings. Neither has the cultural principle of leaven:
expansion over time.

The related principles of corporate responsibility and corpo-
rate representation are no longer manifested in the sin offering,
ie, purification offering. Nevertheless, they are clearly revealed
in the Adamic covenant and the New Covenant. The biblical
principle of the delegation of earthly authority — from God to
the people to their representative — was illustrated in the purifi-
cation offering, but it was not inaugurated by this offering. It
therefore did not perish with this offering. Also, we no longer
bring an animal to serve as our trespass or reparation offering
for a sin of omission, but the principle of the sacrifice propor-
tionate to one’s wealth still applies, in church and State.

A thief’s reparation offering is no longer made by presenting
a ram without blemish. But there is no indication that an offer-
ing comparable in value to a ram in the Mosaic economy
should not still be presented to a church by the self-confessed
thief, nor should his victim be denicd the return of the thing
stolen plus a reparation payment of 20 percent. The judicial
boundary between sacred and common still exists. A violation of
such a boundary still constitutes a profane act.

The annulment of the Levitical sacrifices has not annulled
the principles that underlay these sacrifices, any more than the
annulment of the priesthood has somehow annulled the princi-
ple of sacrifice. The High Priest’s office still exists, but only one
man holds it: the resurrected Jesus Christ. The mediatorial role
of the Old Covenant priest in offering a bloody sacrifice has
been annulled by Christ’s perfect, one-time sacrifice (Heb. 9).
This does not mean that the ministerial, judicial, and educa-
tional role of the Levites has becn annulied, The diaconate has
replaced the Levites’ social role. Melchizedck, the priestly king
of Salem, offered Abraham bread and wine, and Abraham paid
