512 LEVITICUS: AN ECONOMIC COMMENTARY

law, this deliverance had to await the blowing of the trumpet at
the next jubilee year: on the tenth day of the seventh month,
the day of atonement (Lev. 25:9), yom kippur. L agree with James
Jordan that this final jubilee year came three years afier the
crucifixion, in the same year as the inauguration of Paul’s min-
istry to the gentiles.'? On that historic yom kippur, God released
from judicial bondage every gentile slave in Israel who had
publicly professed faith in, and subordination to, the New Cov-
enant’s head of household. Because Old Covenant Israe} re-
fused to honor this adoption, having killed the adopter instead,
God destroyed Old Covenant Israel.’

As I said, there was one exception to manumission through
outside adoption: the criminal who had been sold into slavery
to repay his viclim. The adopter would have had to pay the
owner’s purchase price plus anything still owed to the victim.
In the case of Jesus Christ, He made this supreme payment to
the victim, God the Father, who had placed all of mankind into
servitude because of man’s rebellion in the garden.

This shouid end the debate over whether a man needs to
profess the Lordship of Christ in order to be saved. A regencr-
ate person has no choice but to profess Christ’s comprehensive
lordship. He cannot lawfully partake in the jubilee inheritance
without this profession, But because of God's mercy, this oath
can be taken for him representatively, cither by his parents
when they offer him for baptism as an infant or when he volun-
tarily consents to baptism after infancy. Whether the oath is
verbally professed or not, it is an inescapable aspect of God's
covenant. There is no lawful inheritance apart from this subor-
dination to the head of the church. There is therefore no

testator. For a testamentis of force after men are dead; otherwise it is of no strength
at all while the testator liveth” (Heb. 9:16-17),
12. James B. Jordan, ‘Jubilee, Part 3,” Biblical Chronology, V (April 1993), p. 2.
13. David Chilton, The Great Tribulation (Fi. Worth, Texas: Dorninion Press,
1987).

14, Some fundamentalists who have no doctrine of covenantal representation
