76 LEVITICUS: AN ECONGMIC COMMENTARY

This sacrifice is designated by the Hebrew word transliterat-
ed zehbakh. Milgrom says that this word always means “slain
offering whose meat is eaten by the worshipper.” Ie cites as
particularly revealing Jeremiah 7:21: “Thus saith the Lorp of
hosts, the God of Israel; Put your burnt offerings unto your
sacrifices [zehbakh], and eat flesh.”? I conclude that the common
person could eat part of this ollering because of the laws gov-
erning uncleanness. The law stated that an unclean person
could not lawfully eat this sacrifice (Lev. 7:20-21). But this law
of uncleanness always applied to priests. If this law applied only
to priests, there was no need to mention this requirement. By
singling out the possibility that a clean person could enter the
sacrifice (priestly) area, this law identified this sacrifice as a
shared meal in which the common offerer could participate.
This sacrifice was unique among the five in that it allowed a
common Israelite or circumcised resident alien to eat a ritual
meal in the presence of God. The pricst collected part of this
offering for his own use (Lev. 7:14). This indicates, though does
not prove, that the pricst ate the meal with the sacrificer and
his family and friends.

Whcere was il caten? Milgrom argues that it was eaten inside
the sanctuary’s boundaries. He refers to the sacrifice of the
Shilonite sanctuary (IL Sam. 2:13-14): a zehbakh, a shared meal.
‘The offerer’s sacrifice was boiled on the sanctuary premises.
There were probably special halls for eating the sacrificial meal,
he concludes (I Sam. 9:22; Jer. 35:2). This is why there were
rules governing the offerer’s uncleanness, he says.*

The Israelite was not allowed to cat fat or blood when mak-
ing this sacrifice. Normally, fat was regarded as a blessing;
blood was always prohibited. So, in this case, God reserved to
Himself both the fat and the blood.*

 

2. Jacob Milgrom, Leviticus 1-16, vol. 8 of The Anchor Bible (New York: Double-
day, 1991), p. 218.

3. Tbid., p. 228.

4. Sec Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
