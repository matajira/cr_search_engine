688 LEVITICUS: AN ECONOMIG COMMENTARY

finitude, 559, 561, 562
fire
Adam, 48
alchemy, 324
altar, 1, 32, 48, 67
everlasting, 48, 67, 516
God &, 1, 516
inheritance by, 324
leaven, 66-67, 79
Moloch, 323-36
Mt. Sinai, 184
prostitution, 65
strange, 330
fire-walking, 328n
Firstfruits, 15, 109, 302 (see
Pentecost}
Flood, 557, 631
Fonda, Jane, 373n
food
Babylon's, 342-43
blessing, 448-50, 459, 554,
558
covenantal obedience &,
451-52
drought’s sanctions, 574
miracles, ch. 27
population growth, 559
promisc, 556
sabbatical ycar, 397, 400,
402
vegetarianism, 342
also see diet
foreign aid, 559
foreskins, 340
forgiveness as sacrifice, 56
Frame, John, 649
fraud, 209, 212, 222, 643
free lunch, 205, 255

free market, 321, 400-1, 445,
553, 559
freedom, 52, 309, 425-26, 506,
555 {also see liberty)
freemanship, 389, 495, 522-23,
525, 587-88, (also see army:
citizenship; citizenship)
freewill offering, 80-81
French Revolution, 380
fruit
no trespassing, 294, 307
Passover, 302
time boundary, 304
unholy, 298-99
fruitfulness, 558
full-time Christian service,
xxxix, 121-22, 133
fundamentalism
alcohol prohibited, 156-57,
159-60
antinomian, 161
atomism, 100-1
embarrassment to God, 161
Gomes vs., xxvi
history, 458
humanism &, 159
kingdom, 159
mysticism, 457-58
power rejected, 160
wine, 156
wineskins, 160
furniture (plague), 167
future-orientation, 304-6, 396-
97

garden, 35, 153, 220, 294, 420
Genesis, xlii-xliii
Geneva, xxx
