80 LEVITICUS: AN ECONOMIC COMMENTARY

tion. of which (whosc) leaven. It is not a question of “dominion
vs. no dominion”; it is a question of whose dominion. The
dough (history) is here. Whose leaven will complete it, God’s or
Satan's? This is why Jesus described Ilis kingdom as one of
righteous leaven."”

Leaven takes time to produce its positive effect. Leaven re-
quires historical continuity. Men can wait lor their leavened bread,
for God gives them time sufficient for the working of His spiri-
tual leaven. They may not understand how it works, how its
spiritual effects spread through their culture and make it a
delight, any more than they understand how yeast works to
produce leavened bread, but they can see the bread rising, and
they can see the progressive effects of the leaven of the king-
dom. They can look into the oven and sec risen bread.

Free-Will Offering and Covenant Renewal

‘Lhe peace offering in Leviticus 7 was what in modern Eng-
lish phraseology would be called a free-will offering.’ This
language is found in Psalm 119:108: “Accept, I beseech thee,
the freewill offerings of my mouth, O Lorn, and teach me thy
judgments.” ‘he peace offering was brought by the individual
of his own free will; that is, he was not required by law to do
this because of a particular sin. It was not a legal payment for
sin. Ji was a token of his appreciation for the grace that God had
shown to him. I. was this Mosaic Covenant sacrifice that Paul had
in mind when he wrote this injunction to Christians: “I beseech
you therefore, brethren, by the mercics of God, that ye present
your bodies a living sacrifice, holy, acceptable unto God, which
is your reasonable service. And be not conformed to this world:

10, For a more detailed exegesis, see Gury North, Moses and Pharaoh: Dominion
Religion vs. Power Religion (Tyler, Texas: Institute for Christian Economics, 1985), pp-
158-72.

11. Andrew A. Bonar, A Commentary on Leviticus (London: Banner of ‘Iruth
‘Leust, [1846] 1966), p. 181.
