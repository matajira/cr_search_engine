Local Justice us. Centralized Government 259

civil judicial system function. The clders in the gates imposed
the negative sanctions of God’s civil law. The priests were advi-
sors to the elders (Deut. 17:8-13). The point is, civil rulership was
plural because priestly rulership was plural. This has not changed.
Who are the civil priests - citizens who exercise lawful civil
sanctions ~ in New Testament times? Biblically speaking, in a
formally covenanted nation — which al] nations arc required by
God to become” — only those adults who are church members
and are therefore under church authority.

The fundamental agency of the local court — both civil and
ecclesiastical ~ is the jury. It is the jury that announces guilt or
innocence after having heard the arguments of conflicting
patties in the courtroom. Its members evaluate the cogency of
the arguments and the “fit” between the law and the evidence.
The jury places limits on the judge’s authority to decide the
case. This is especially true in the United States."

The jury system is a necessary outworking of a biblical legal
order. It did not appear overnight in the carly church, even as
slavery was not condemned overnight. But it had to develop in
a Christian legal order, even as slavery had to be abolished.”
The jury’s legal basis is the priesthood of all believers.” The jury is
a Christian institution. ‘This is not to say that it is exclusively a
Christian institution. Ancient Athens and Rome both had trial
by jury.”

17. Gary DeMay, Ruder of the Nations: Biblical Blueprinis for Government (Ft. Worth,
Texas: Dominion Press, 1987), ch. 4.

18, Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 2.

19. Lawrence M. Friediman, A Histary of American Law (New York: Simon &
Schuster, 1973), p. 135.

20. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Tnsti-
tute for Christian Econemics, 1990), ch. 4.

21. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23
Ciyler, Texas: Institute for Christian Economics, 1984), p. 232.

22, North, Boundaries and Dominion, ch. 15, subsections of “The Jury” and
“Popular Sovereignty in Athenian Democracy”
