 

574 LEVITICUS: AN ECONOMIC COMMENTARY

Softening Their Resistance

‘The first negative sanction is both psychological and physi-
cal: terror and consumption. This will produce sorrow. ‘This
defensive mentality is the mentality of the slave and the prison-
er. The second threatened negative sanction is military defeat.
If this threat fails to persuade them to repent, the sanctions will
escalate further. “And if ye will not yet for all this hearken unto
me, then 1 will punish you seven times more for your sins”
(Lev, 26:18). ‘he stated punishment is drought. God’s wrath is
manifested by Tis destruction of a nation’s food supply. This
was a major threat to a pre-modern agricultural society. “And
I will break the pride of your power; and I will make your
heaven as iron, and your earth as brass: And your strength
shall be spent in vain: for your land shall not yield her increase,
neither shall the trees of the land yield thcir fruits” (Lev. 26:19-
20). Drought was God's means of softening up the resistance of
King Ahab against Elijah’s message (I Ki. 17:1).

As in the case of Egypt, the next sanction involved the chil-
dren: “And if ye walk contrary unto me, and will not hearken
unto me; I will bring seven times more plagucs upon you ac-
cording to your sins. I will also send wild beasts among you,
which shall rob you of your children, and destroy your cattle,
and make you few in number; and your high ways shall be
desolate” (Lev. 26:2 1-22). God sent beasts against those children
who mocked the prophet Flisha: “And he went up from thence
unto Bethel: and as he was going up by the way, there came
forth little children out of the city, and mocked him, and said
unto him, Go up, thou bald head; go up, thou bald head. And
he turned back, and looked on them, and cursed them in the
name of the Lorn. And there came forth two she bears out of
the wood, and tare [tore] forty and two children of them” (II
Ki, 2:23-24).

‘The judgments are again military: “And if ye will not be
reformed by me by these things, but will walk contrary unto
me; Then will I also walk contrary unto you, and will punish
