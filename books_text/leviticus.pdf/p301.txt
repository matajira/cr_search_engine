Impartial Justice vs. Socialist Economics 245

tortured equally (Luke 12:47-48). (Those Christians who are
squeamish about the word “torture” may prefcr to substitute
the word “torment,” as in Matthew 18:34 and Luke 16:24.)

Inequality of results is an inescapable outcome of the im-
equality of men’s productivity, given the existence of impartial
justice. Put another way, impartial justice — justice that does not
bring sanctions or evaluate public actions in terms of a person’s
economic status or legal status — inevitably produces inequality
of economic results. When the judge imposes double restitution
on the criminal, he inescapably creates inequality of economic
results, This is exactly what God does in history. When God
brings His judgment into history, there will be unequal eco-
nomic results.

It is part and parcel of the socialist perspective of all libera-
tion theologians to deny this principle. They seek equality of
results, and therefore they inescapably recommend policies that
are a flat denial of the biblical principle of impartiality of jus-
tice. Liberation theology is a self-conscious rebellion against Leviticus
19:15. Its defenders seek to confuse their followers and their
readers on this point. Impartial justice that is applied in a
world made up of people with differing capacities and differing
degrees of rightcousness will mevitably produce inequality of
economic results, It is this outcome of biblical law which enrag-
es and outrages almost all modern Christian theologians, espe-
cially those who are either neo-evangelical college professors
(outside of the natural sciences) or liberation theologians. ‘hey
call for the Statc to usc the threat of violence to steal the wealth
of the successful and transfer it to the unsuccessful. ‘I‘hey call
for socialism: the State’s control over resources through burcau-
cracy. They prefer the political sanctions of bureaucrats to the
economic sanctions of consumers.

The law of God testifies against the legitimacy of any society
that seeks the equality of results. The law of God testifies
against any society that would use the power of the civil govern-
ment to redistribute wealth on any basis except one: the pro-
