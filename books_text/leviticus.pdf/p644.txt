588 LEVITICUS: AN ECONOMIC COMMENTARY

someone’s yow of adoption into the tribe. But if the vow was
accepted by a pricst in authority, the vow-taker and any other
members of his family covered by his vow were then adopted
into the tribe of Levi if they could pay the entry fee. There was
no way back into non-Levitical freemanship in Israel; the adop-
ted family’s original inheritance had been forfeited to the kins-
man-redeemer, the closest relative in their original tribe (Num.
27:9-11). They could retain their status as freemen only as
members of the tribe of Levi.

The Restrictive Function of Price

These prices were not market prices. They had nothing to
do with comparative rates of economic productivity.* They
were instead barriers to entry into the tribe of the priests. Pri-
mary judicial authority in Israel was supposed to be inside the
tribe of Levi, for the Levites had unique access to the written
law of God. They were the spiritual and therefore the judicial
counsellors in Israel.” It was not easy to’ gain access to this
position of honor and authority. Adoption into the tribe of Levi
was legal, but it was not cheap.

The entry price for an adult male was set at 50 shekels of
silver. The price for an adult female was 30 shekels.?? The
male child’s price was 20 shekels; the female child’s was 10

14, I have summarized a possible explanation of these prices as redemption
prices. See North, Boundaries and Dominion, ch. 86, subsection on “Explanation:
Economic Productivity.”

15. ‘Chis is why Paul speaks of the double honor of those who labor in the word:
“Let the elders that rule well be counted worthy of double honous, especially they
who labour in the word and doctrine” (I Tim. 5:17).

16. ‘This was the same as another judicial price: the formal bride price owed by
a seducer of a virgin to her fathen North, Tools of Dominion, pp. 649-57. Tt rests on
an interpretation of the false accuser’s penalty of Deuteronomy 22:19: “And they
shall amerce him in an hundred shekels of silver, and give them unto the father of
the damsel, because he hath brought up an evil name upon a virgin of Israel: and
she shall be his wife; he may not put her away all his days,” One hundred shckels
was double restitution.

 

17. ‘The same price that was owed to the owner of a gored slave (Ex. 21:32).
