The Redemption Price System 613

jubile: and he shall give thine estimation in that day, as a holy
thing unto the Lorn” (Lev. 27:22-23). This law specified that
the ficld would return to the original owner in the jubilee year
(Lev. 27:24). The law protected the original land owner from
the consequences of vow-breaking by the lessee.

To Protect the Priests

‘The lessee also escaped the penalty of disinhcritance. A
lessee who broke his vow of dedication and reclaimed the land
was nol threatened by the loss of the land in the jubilee. In fact,
this law specifies no penalty at all. It docs not state that the
lessee must forfcit an equivalent quantity of his own land. This
means that there was far greater likelihood that he would break
his vow of dedication, compared to an original owner. The
question arises: Why was the lessee exempt from the 20 percent
penalty? If he was. not subject to the threat of losing the dedi-
cated land — it was not his land - then why wasn’t the redemp-
tion penalty even greater than 20 percent? Why were no penal-
ties imposed? The text does not say. We can only guess. Let us
guess intelligently.

The lessee owed the original owner regular payments unless
he had already paid the owner in advance. This placed him in
a weaker economic position, other things being equal, than the
original land owner. Either he bore greater contractual risk
than an original owner would have borne or, if he had already
paid the owner in advance, he had less cash available to redeem
the land from the priest. Since the goal of a land-dedication
vow was to reward the priests, excessive economic barriers to
redemption would have been a disincentive for such vows.
Thus, the priest bore greater risk of having his plans disrupted
by a lessee than by an original owner. The lessee was more
hkely to reclaim the dedicated property than an original owner
was.

If he paid no 20 percent penalty for breaking his vow to the
priest, what would have protected the priests? They were protected
