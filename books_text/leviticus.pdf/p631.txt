God's Escalating Wrath 575

you yct seven times for your sins. And J will brig a sword
upon you, that shall avenge the quarrel of my covenant: and
when ye are gathered together within your cities, I will send
the pestilence among you; and ye shall be delivered into the
hand of the enemy. And when I have broken the staff of your
bread, ten’ women shall bake your bread in one oven, and they
shall deliver you your bread again by weight: and ye shall eat,
and not be satisfied” (Lev. 26:23-26). Enemies laying siege
outside the gates, pestilence and hunger inside the gates: so
shall covenant-breakers be reminded of the importance of
God’s law.

But even this may prove futile. “And if ye will not for all this
hearken unto me, but walk contrary unto me; Then I will walk
contrary unto you also in fury; and I, even I, will chastise you
seven times for your sins. And ye shall eat the flesh of your
sons, and the flesh of your daughters shall ye eat” (Lev. 26:27-
29). This was fulfilled in the days of Elisha, during Ben-hadad’s
siege of Samaria:

And it came to pass after this, that Ben-hadad king of Syria gathered all
his host, and went up, and besicged Samaria. And there was a great
famine in Samaria: and, behold, they besieged it, until an ass’s head
was sold for fourscore pieces of silver, and the fourth part of a cab of
dove’s dung for five pieces of silver And as the king of Isracl was
passing by upon the wall, there cried a woman unto him, saying, Help,
my lord, O king. And he said, Lf the Lorp do not help thee, whence
shall I help thee? out of the barnfloor, or out of the winepress? And the
king said unto her, What ailcth thee? And she answered, This woman
said unto me, Give thy son, that we may eat him to day, and we will eat
my son to morrow. So we boiled my son, and did eat him: and I said
unto her on the next day, Give thy son, that we may eat him: and she
hath hid her son. And it came to pass, when the king heard the words
of the woman, that he rent his clothes; and he passed by upon the wall,
and the people looked, and, behold, he had sackcloth within upon his
flesh (11 Ki. 6:24-30).
