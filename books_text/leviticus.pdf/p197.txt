Guardian of the Civil Oath 141

senis God subordinately. The civil court acis on behalf of’ the
victim, but only in its judicial capacity as the minister of kingly
justice (Rom. 13:4), as the institution that lawfully bears the
monopolistic sword of vengeance. But God requires more than
civil sanctions to placate His wrath against the criminal. He sits
on His throne as both High Priest and King; on carth, these
offices are divided except in two unique cases: Melchizcdck
(Gen. 14:18) and Jesus Christ. God must be placated in both of His
offices. This is why no single earthly court can lawfully offer two-
fold atonement to a criminal. God therefore requires a priestly
sacrifice.

New Testament Sacrifice

In the New Testament, this priestly sacrifice was made by
Jesus Christ at Calvary. ‘The various animal sacrifices in the Old
Testament representationally prefigured this ultimate sacrifice
(Heb. 9}. A question legitimately can be raised: Is any post-
Calvary public mark of contrition lawfully imposed by the
church on the perjurer? If so, on what legal basis?

if the perjurer is a church member, he has partaken of the
Lord’s Supper throughout the period following his false testi-
mony to the court. This placed him in jeopardy of God’s nega-
tive sanctions (I Cor. 11:30). He ignored this threat, thereby
implicitly adopting the same false theclogy of God’s minimal
sanctions, previously described. The church's officers deserve to
know of the transgression, and can lawfully assign a penalty.
This penalty should not exceed the value of a ram in the Mosa-
ic economy.

if the perjurer is not a church member, he is still dependent
on continuing judgments by the church to preserve God’s
common grace in history. The State can lawfully function in
non-Christian environments, but only because of the common
grace of God mediated through His called-out church to the
world. In the Mosaic covenant, the church served as a mediat-
ing agency for the entire world. This is why the priests had to
