60 LEVITICUS: AN ECONOMIC COMMENTARY

struction: the progressive removal in history of the cffects of
God’s curse.”

Conclusion

By sacrificing to something sovercign over him (point 1),
man acknowledges his debt to this highcr authority (point 2).
He secks to draw a boundary of safety or immunity around
himself, his works, and his property (point 3). He believes that
his sacrifice will enable him to avert the wrath and/or gain the
blessings of this higher authority (point 4), cnabling him to
leave a valuable legacy to his heirs (point 5), Offering sacrifice
is a ritual acknowledgment of someone else’s sovercignty and
one’s own economic subordination: stewardship.

Covenant-keeping man in the Mosaic Covenant era was told
by God to sacrifice animals from his flock. The animal had to
be the best of his flock: blemish-free. This pointed symbolically
(representatively), as had Abraham’s sacrifice of the ram in
place of Isaac, to the ultimate sacrifice: God’s ethically blemish-
free Son. At the same time, God did not require total sacrifice
from His holy people. That which would constitute total sacri-
fice from fallen man is insufficient to pay the required bill to
God. Thus, the person who presented the sacrifice to the priest
was proclaiming ritually and publicly that he in principle owed
everything to God (ie., the best of his flock), but at the same
time, all that he owned would not suffice to repay God (Le., one
animal only). ‘he individual sacrifice was to be of high value
but not total.

God placed specific limits — boundaries — on the required
sacrifices.** These sacrificial boundarics put man in his proper
place. They also allowed him to retain the majority portion of
the wealth under his jurisdiction. In order to keep what he

25. Gary North, is the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for ChrisGan Economics, 1988), chaps. 8, 9.

26. North, ‘fools of Dominion, ch. 30: “God's Limits on Sacrifice.”
