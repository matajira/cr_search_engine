Measuring Out Justice 309

cially by Gad for those who seek liberty under God.’ The establish-
ment of a boundary is an aspect of point three, In this case, the
boundary was geographical. It was to serve as a judicial model
for the whole world (Deut, 4:4-8). Strangers in the land were
expected to tell “the folks back home” of the benefits of dwell-
ing in God’s sanctuary. God prepared a place for strangers to
live in peace through justice. This system of justice did not give
strangers political authority, for they were outside the ecclesias-
tical covenant. But the system provided liberty. Conclusion:
political pluralism is not biblically necessary for civil liberty.

There is no valid biblical reason to believe that God’s ideal of
sanctuary for strangers in a holy commonwealth has been an-
nulled by the New Covenant. On the contrary, the sanctuary
principle has been extended across the globe through Christ’s
universal gospel of deliverance (Matt. 28:18-20).? Nation by
nation, the whole world is to become such a sanctuary. But a biblical
sanctuary is a theocratic commonwealth. That is to say, the
extension of God’s theocratic commonwealth means the exten-
sion of God’s civil sanctuary: the transition from civil wrath to civil
grace. The judicial evidence of this biblical civil grace is equality
before the civil law. To maintain the blessings of liberty, all
residents of a holy commonwealth are required to obey the
Bible-revealed law of God. God made it quite clear: without
corporate obedience to God’s Bible-revealed law, no nation can
maintain the blessings of civil liberty.*

1. Gary North, Political Polytheisra: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989), ch. 2: “Sanctuary and Suffrage.”

2. Kenneth L. Gentry, Ju, The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Tyler, Texas: Institute for Christian Economics, 1990).

3, Gary North, Healer of the Nations: Biblical Blueprints for International Relations
(Ft. Worth, Texas: Dominion Press, 1987).

4. Those who seek to defend the ideal of civil Hberty — sometimes called “civil
Kberties” - apart trom an appeal to God's Bible-revealed law-order are indulging
their preference for humanism: Stoic natural law theology or Newtonian natural law
theology, In either case, they have abandoned the Bible’s explicit method of retaining
the blessings of iberty: Trinitarian, covenantal, oath-bound constitutionalism,
