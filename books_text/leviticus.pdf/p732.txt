676 LEVITICUS: AN ECONOMIC COMMENTARY

God’s name, 11, 136, 179,
209-10, 324

grace, 122, 132

guardians, 155

hell’s gates, 132

hermeneutic, 645-50, 654-
55

holiness, 1

holy of holies, 123

home, 268, 330

house, 150, 268

household servant, 498

houses with plague, 166

immigration, 427

immunities (rights), 264

inclusion/exclusion, 8

information, 367

invented, 163

Tsracl’s (sec Israel’s bound-
aries)

judges, 238

judicial (geographical bless-
ings), 541, 554

king, 257

kingdom, 158

land, 423

law, 271

leprosy, 170

Levites, 252-53

Leviticus, 1, 7

life &, 7

limits, 559, 561-62

list of, xiv, 180

Lord’s Supper, 346

modern man denies, 163

mysticism, 3

“No Trespassing,” 8, 120

ownership, 129

peace offering, 46, 75,
81

plague of houses, 166-67

pluralism, 330-32

policing the land’s, 423

political authority, 258

pollution of sin, 100

poor person, 204

post-Calvary, 83-85

priest, 154-55, 166-67

priestly, 150

priest’s household, 495

principle of, 646-47

profane acts &, 123, 126

property, 11, 144, 209, 486

property rights, 43

purification offering, 88-
89

rain, 541

rights, 264, 376

ritual, xlvi, 158

sacraments, 129, 158

sacrifices, 60-61

sacrosanct, 627

sanctification, xlvii

sanctions, 542-43

sanctuary, 76, 88, 348

sin, 100

sobriety, 153-55

State, 53, 62, 204, 271,
367

State/individual, 5

temple, 89, 152

textiles, 278

thefi, 11, 136

time, 304
