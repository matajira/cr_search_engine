Economic Oppression by Means of the State 447

interest payments. The decision to make a cash payment in
order to acquire legal ownership of a stream of resource-gener-
aled income over a fixed pcriod of time is identical economical-
ly to making a cash payment to buy an interest-paying bond
with the same expiration date as the lease. This means that a
prohibition against all interest payments must also be a prohibi-
tion against all rent payments. Yet this law establishes thc legali-
ty of rent. I therefore conclude that the Bible does not prohibit
interest in non-charity loans.

There is no evidence that the jubilee laws were ever en-
forced in Isracl. This may indicate that the jubilec laws some-
times were not enforced. Probably they were not enforced prior
to the exile, for the seventh year of release was not honored,
which is why God sent Israel into exile (II Chron. 36:21). Thus,
every Israelite could safely assume that other laws beside the
jubilee land law would govern leasehold contracts. Other civil
laws would provide differing authority to magistrates to decide
which leases would be honored and which would not. The
magistrates of Israc! arrogated authority to themselves to dis-
obey God regarding the sabbatical year. How [ar could they
safely be trusted to honor the terms of other laws? The oppor-
tunities for economic oppression must have increased, com-
pared to rule by the sabbatical law and the jubilee land law, for
there would have been less certainty about the enforcement of
the civil law, The greater the degree of judicial uncertainty, the
greatcr the amount of resources necessary to protect oneself:
better lawyers, larger bribes, and higher expenditures on
searching out information regarding the integrity of one’s trad-
ing partners and also the moral integrity of their legal heirs.
These were long-term lease contracts.
