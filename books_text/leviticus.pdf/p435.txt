Blasphemy and Civil Rights 379

adulterers — to a man, the public defenders of civil rights and
opponents of anachronisms like the blasphemy laws - far more
than it has damaged conservatives.'®

Conclusion

What kind of crime is blasphemy? ‘he Bible’s discussion of
the appropriate sanction against blasphemy appears in a pas-
sage that raises the question of restitution: killmg animals,
injuring men. This indicates that blasphemy is judicially the
equivalent of an assault. ‘he resolution of the incident rests on
the idea that the State must intervene and impose the ultimate
form of restitution: the death penalty. he criminal is thercby
delivered into God’s heavenly court for final judgment.

Blasphemy is a public attack on the character, integrity, and
final authority of the God of the Bible. The attack may call
down a formal curse on God. It may describe God in obscene
or perverse language or imagery. It constitutes a public assault
against a biblically covenanted Christian social order. Treason
against God is treason against a biblical covenantal order, for it

16. The reputation of President John F Kennedy (1961-63), literally a daily
adulterers, has been tarnished retroactively. In his day, however, the press still
covered up for him. No longer. U.S. Senator Gary Hart, the leading candidate for
the Democratic Party’s nomination for President in 1988, went down to defeat when
his suspected adultery with Donna Rice became public. A photograph of Miss Rice
sitting on his lap on the pier next to the rented boat “Monkey Business” ended his
campaign. Miss Rice had the good grace to disappear very quietly into religious
service work aficr the scandal died down,

A quarter century earlier, so had England’s Member of Parliament and Scerciary
of War, John Profumo, whose scandal regarding a prostitute brought down the
Conservative government in 1968. It was not that he had committed adultery that
destroyed him; it was that he had lied on the floor of Parliament about the affair
Lying publicly to one’s colleagues is taken very scriously by Parliament; adulecry isn’t.
If the prostitute had not also been closc toa known Russian agent, Profumo probably
would have had no problem. He worked nearly anonymously for the next two dec-
ades, helping juvenile offenders, for which he was eventually granted nalional honors
by the Queen, What is uot remenubered is that the conservative journalist who first
broke the story in a small-circulation newsletter went to jail for violating the govern-
ment’s secrecy laws by exposing Profumo. English common law is more concerned
about protecting (lic civil government than protecting God’s name.
