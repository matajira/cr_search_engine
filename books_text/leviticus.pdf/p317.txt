Local Justice vs. Centralized Government 261

individual citizens bring formal political sanctions against their
rulers in a democracy, they are to he left free from subsequent
retaliation by politicians. The secret jury and the secret ballot
are both basic to the preservation of the institutional indepen-
dence of the sanctioning agents, and therefore to the preserva-
tion of the impartiality of the decision.

The other biblical principle of civil law is the prohibition
against double jeopardy. If “not guilty” decisions could be
overturned on appeal by a higher court, mnocent men could be
bankrupted by the State by endless trials for the same accusa-
tion.”

Men must judge as God’s representatives in history. “Judge
not!” (Matt. 7:1) is misapplicd when it is said to apply to gov-
ernment. This would make all government impossible. This
would turn over the office of judge to covenant-breakers.*
When the judge renders judgment lawfully, he does so as God’s
agent.*

ment voted secretly (in disguised hand) on the Billeting Act. This act was repudiated
by Charles IL. The secret ballot was not used again by the Scottish Parliament until
1705, In the United States, the use of the secret ballot was introduced in the New
England colonies, and in Pennsylvania, Delaware, and the two Carolinas at the time
of the American Revolution, beginning in 1775. See “Ballot,” Encyclopedia Britannica,
Lluh edition (New York: Encyclopedia Britannica, Inc., 1910), IT, pp. 279-81.

24, North, Boundaries and Dominion, ch. 45, section on “Double Jeopardy.” See
also Greg L. Bahnsen, “Double Jeopardy; A Case Study in the Influence of Christian
Legislation” journal of Christian Reconstruction, II (Winter 1975-76). This protection
against double jeopardy docs not apply to church courts. First, church membership
ig voluntary. Second, court costs arc minimal. Third, and most important, unlike
American civit government, local church government is not divided into judicial,
legislative, and executive branches. A church court js unitary. There must be a way
to overturn the decisions of such a unitary local power, A local congregation’s declar-
ation of “not guilty” can be overturned by a higher court. If this were not true, no
fiberal clergyman could be removed from office when declared innocent by his liberal
congregation, presbytery, or synod. The protection of biblical preaching and the
sacraments is morc important than the preservation of double jeopardy protection,

25. Mid., ch. 18, s

26. Ibid., ch. 15, section on “Rendering Judgment: A Voice of Authority.”

  

tion on “Judge Not!”
