Limits lo Growth 569

argument in later editions, but its outlook still dominates the
ZPG advocates, His earlier outlook is hostile to the biblical idea
of man’s dominion over nature. Post-Adamic nature is neither
autonomous nor permanent. This idea upsets modern Malthus-
ians. They see nature as autonomous and man under nature
rather than both man and nature under a Creator God."

Conclusion

The fundamental economic issue is not population growth.
It is not the increase of food per capita, It is not capital invest-
ed per person. The fundamental economic issue is ethical: Ged’s
covenant. Nevertheless, the language of Leviticus 26:9-10 is
agricultural. Why? Because in an agricultural society, the mark
of God’s blessing is food. God promised to provide bread for
all. He also promised to inereasc their numbers.

This does not mean that He promised nothing else to them.
He promised an agricultural people access in history to the city
of God, the New Jerusalem (Isa. 65:17-20). The city of God is
the image of a regenerate society. The city is therefore not
inherently evil. Urban life is not inherently depersonalizing.
Covenant-breaking is evil and depersonalizing. Covenant-break-
ing is made less expensive in cities because of the higher costs
of gathering information about individual actions, as well as the
higher costs of imposing informal social sanctions. But cove-
nant-breaking is not inherent to cities. It can be overcome
through God’s grace.

If this were not the casc, then the promise of population
growth would be a threat to the covenant. A covenantal blessing
would inevitably become a covenantal curse. The grace of God
would necessarily produce the wrath of God. This is the opera-
tional viewpoint of both premillennialism and amillennialism
regarding church history, but it is a false view of history.’*

 

18. [id., ch. 84, section on “The Malthusians.”
19. Gary North, Millenntalism and Social Theory (Tyler, Texas: Institute for
