Sacred, Profane, and Common 129

as a priest or as a layman, was considered profane. If done in
ignorance, there was an added penalty of one-fifth.

‘There was an ownership principle involved. God had estab-
lished legal boundaries around the sacraments: spatial bound-
aries and liturgical boundaries. These were ultimately ownership
boundaries, analogous to the boundary He placed around the
tree of the knowledge of good and evil. That which belongs exclu-
sively to God is specially protected by law. Jesus’ distinction between
God and Caesar would apply here: render to each what is
jawfully claimed by cach (Matt. 22:21).’° God is sacred; Cacsar
is common. (It was this confession that later became the legal
basis of the Roman Empire's persecution of Christians.) Jesus’
distinction between God and Mammon. would not apply here:
no one should ever serve Mammon. No one should ever be
profane (Mammon: falsc worship). What was established in.
Leviticus 5:14-19 was a legal distinction between sacred and
common, not between sacred and profane. That which is common
cannot be profaned.

What did the church in the Mosaic Covenant require? The
sacrifice of unblemished animals, for one thing. What if a man
had ignorantly offered an animal with a defect — a discase, for
example? He had mistakenly brought the wrong animal to the
altar. He owed another animal, plus a penalty payment of one-
fifth. Since he could not kill one-fifth of an animal, a monetary
equivalent according to the shekcls of the tempie was allowed.
To offer a blemished animal was the equivalent of stealing from
God — profaning His table-altar (Mal. 1:8-12). God’s warning
was clear: “But ye have profaned it, in that ye say, The table of
the Lorp is polluted; and the fruit thereof, cven his meat, is
contemptible” (Mal. 1:12).

19. Because God brings Caesars to the throne who unlawfully claim far more
than a tithe, we are usually to obey even the unlawful daims, God brings such men
to power in order to judge us. However, God allowed Jeroboan to revolt against
Rehoboam in protest against Rehoboam’s taxes (1 Ki. 12).
