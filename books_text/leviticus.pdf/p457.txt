The Sabbatical Year 401

the newly resealed Israelite had to be liberally provided with
food (Deut. 15:13-14): to get him through the sabbatical ycar.
The fruit of his own field would belong to non-owning harvest-
ers and beasts,

The sabbatical year was a system for forcing men to become
self-consciously dependent on God’s grace. Dependent on Flim,
they were to become dominion-minded. Subordinate io Gad, they
were to become active toward the creation. (This is the mandated
pattern for the dominion covenant.) The year of debt release
was to be the year of open access to the fields for non-owners.
It was a year of hard work for harvesters, for they harvested on
their own and for their own. A new master told them to do
this: the market.

Because independentharvesters were given free access to the
land’s unassisted production one year in seven, they had an
incentive to recommend land management practices that would
maximize output in the seventh year: crop rotation, [crtiliza-
tion, irrigation, etc. This does not mean that landowners were
required. to follow the suggestions of the full-time harvesters,
but to the extent that owners deferred to harvesters in gather-
ing information and assessing ils value, the sabbatical year law
encouraged agricultural practices that did not strip the land of
its long-run productivity. This law, when enforced, created a
class of preferred workers who had an incentive to act as eco-
nomic agents of the land, and therclore as economic agents of the

future.

We do not know for certain whether the gleaners would
have received morc income’ as secondary harvesters in a year
following an investment of capital or as primary harvesters in a
year following an investment of zero. As I hope to show, it is
quite likely that the total output of the fields was greater in a
normal harvest year than in a sabbatical ycar.* What we do
know is that when this law. was enforced, the land received its

8. See below, “The Defection of the Levites.”
