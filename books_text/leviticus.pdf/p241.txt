The Land as a Covenantal Agent 185

written. This prophecy of looming covenantal judgment was
fulfilled in A.D. 70: the fall of Jerusalem.*

Military Sanctions

The Mosaic Covenant’s symbolic use of the land as God’s
agent of negative sanctions represented military conquest: Isracl
vs. the Canaanites, Moab vs. Israel (Jud. 3), Canaan vs. Isracl
(Jud. 4}, Midian vs. Israel (Jud. 6), Phoenicia and Ammon vs.
Israel (Jud. 10; 13), Syria vs. Israel (II Ki. 5:2). In the cases of
Assyria vs. Israel and Babylon vs. Judah, the Israelites were
actually removed from the land. If someone should argue that
the New Covenant has transferred to the earth in general the
symbolic authority to serve as an agent bringing negative sanc-
tions, meaning that God still raises up nations to bring military
sanctions against His people, he must also insist that genocide
is sill authorized by God as the mandatory strategy of covenan-
tal conquest by His people. But genocide is not the way of the
gospel; persuasion, not military conquest, is iis means of evan-
gelism. Conclusion: the land no longer serves as a covenantal
agent under the New Covenant except in the general Adamic
sense (Gen. 3:17-19). That is, the symbolism of the land as God’s
covenantal agent is no longer valid; the arena of covenanial con-
flict is no longer the military battlefield.

With the abolition: of the unique covenantal status of Old
Covenant Israel, God ceased to speak of the Promised Land as
His covenantal agent. Remember, it did not act as a covenantal
agent until the Israclites crossed into the land from the wilder-
ness. Egypt had not spewed out God’s enemies. The idea that
the land is ir some way the bringer of God's military sanctions
against covenant-breakers was. valid only under the Mosaic
Covenant, and only within the boundarics of national Israel.

 

2. David Chilton, The Days of Vengeance: An Exposition of the Baok of Revelation (Ft.
Worth, Texas: Dominion Press, 1987), pp. 196-97.
