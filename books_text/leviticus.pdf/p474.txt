418 LEVITICUS: AN ECONOMIC COMMENTARY

rates. Second, it established the possibility of longer life spans:
“Honour thy father and thy mother: that thy days may be long
upon the land which the Lorp thy God giveth thee" (Ex.
20:12). Third, the law allowed the adoption by Israclites of
circumcised foreigners, a practice that had taken place widely in
Egypt before the persecutions began.’? This was a covenantal
formula for blessings that would produce “explosively” high
population growth.” The rapid population growth they had
experienced in Egypt, which had so terrificd the Pharaoh of
the oppression (Ex. 1:7-10), was the model.

When a high population growth rate is combined with a
fixed supply of land, societies become progressively urbanized
and progressively engaged in foreign trade. ‘The model in the
early modern period of Furope is the tiny nation of the Nether-
lands, The twentieth-century model is the even tinier nation of
Hong Kong.” If residents of a small, formerly rural nation are
unwilling to become urbanized, they must emigrate to less
densely populated nations. The land at home fills up.

Small Farms and Large Families

Tn ancient Israel, the land was to be transferred back to the
original families. The geographically bounded nation was small
when they invaded, yet they came in with at least two million
people. There were 601,730 adult males at the time of the
conquest (Num. 26:51), plus 23,000 Levites (Num. 26:62). Since
this was approximately the same number that had come out of
Egypt (Ex. 12:37), there had been no population growth for 40
years. his meant that they were reproducing at the replace-
ment rate level: 2.1 children per family. (Some children do not

49. Gary North, Moses and Pharaoh: Dominion Religion vs, Power Religion (lyler,
‘Lexas: Institute for Christian Economics, 1985), pp. 23-25.

20. Populations do not explode except when bombed. The language of modern
growth theory has attached the metaphor of explosives to the metaphor of growth.

21. Alvin Rabushka, Hong Kong: A Study in Economic Freedom (Universily of
Chicago Press, 1979).
