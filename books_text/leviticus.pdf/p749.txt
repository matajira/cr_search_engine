point two, li, 46
precedes law, 9-10
priestly law, 72
priest’s anointing, 65
prior to law, 10
sacraments &, 127
Sodom, 545
transformation, 127
tribute, 64
victimless crime, 72
works of, 86

grant, 9-10, 396, 426

grape juice, 156, 157, 158, 161

Great Commission, 83, 347

Great Plains, 553

Great Tribulation, 628

Greek philosophy, xix

gross payment, 109

growth
compound, 561
covenantal limits, 563
economic, 113
enemies of, 568-69
exponential, 561
limits to, 559-60, 562-65
Malthus, ‘{. Robert, 568-69
moral imperative, 560, 570
population, 417-20, 560
rapid, 422
social limit, 564-65.
threat to Satan, 565-66
zero, 560, 562

grudges, 265

guardianship, 128, 139, 144,
155

guilt, 120-21, 189

guilt offering (see reparation
offering; trespass offering)

693

gun control, 336n

Hammurabi Code, 304
Hansen's Disease, 165
harmony of interests, 220
Harrison, R. K., xlvii, 82n,
156n
Hart, Gary, 379n
Hartley, John, |liii, 44n.
Ilarvard University, xxiii, 26
harvest, 303, 558
harvesters, 198-99, 398-401,
430
Hayek, Ir. A., 241-43
headquarters, $47
healing, 267n
health, 349-43
heart, 263
heat death, 568
heathen enslavement, 391, 497,
509, 515
hell, xxix, 132, 526-27, 636
Hellenism, 469
herbs, 79
herd, (tithe on), ch. 38
heresy, 371, 381
hermeneutic
applications, 653-55
Bahnsen, xxxiv, 645-46
boundary, 645-46, 654-
55
case laws, XXxviii
continuity/discontinuity,
279-80, 331, 646-47,
651-52
disagreement, 645-46
dispensational, xxxix-xxxviii
goals, 279, 653-54
