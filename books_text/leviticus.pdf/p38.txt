XXxviil LEVITICUS: AN ECONOMIC COMMENTARY

Why spend so much time, space, and money to prove my
thesis in a period of history in which hardly any Christian
assumes that any of these laws are still in force? Answer: be-
cause Christians need a principle of biblical interpretation to sift
through the case laws of the Bible. Without such a sifting prin-
ciple — a hermeneutic — Christians risk falling mto one of two
disastrous crrors: legalism-Phariseeism or antinomianism. A
carelul study of no other biblical book is better calculated to
force Christian interpreters to discover and then apply a princi-
ple of biblical judicial interpretation. Leviticus is the hard case,
judicially speaking. Get through Levilicus intact, and the other
65 books of the Bible become comparatively smooth sailing.

There is another issue to consider. Some of the laws of Levit-
icus are still binding. Which ones? This is a diflicult question to
answer, but Christians need to find the correct answer. This,
too, requires a hermeneutic: a consistent, coherent principle of
biblical judicial interpretation that enables us to study other
books of the Bible and their case laws. A serious Bible commen-
tary on the Mosaic law should instruct the reader on how to do
this work of interpretation. Very few commentaries on the Old
Testament do this.

There is an old saying: “Give a man a lish, and you have fed
him for a day. Teach him to fish, and you have fed him for a
lifetime.” This principle of feeding always holds true, at least
until the fish give out. In biblical interpretation, the fish will
never give out. Finite minds will never. succeed in exhausting
the potential of infinite projects. The work of interpretation
and application must go on. It is therefore not sufficient for me
to present a scries of conclusions. The reader deserves to know
how a commentator reached his or her conclusions.

Let me state the obvious: this is a Bible commentary. It is
not a treatise on economics. It was written one chapter at a
time; it should be read the same way. A commentary is sup-
posed to throw light on specific verses or passages. Because the
Book of Levilicus is structured in terms of a unifying concept -
