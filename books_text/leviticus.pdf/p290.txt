234 LEVITICUS: AN FCONOMIG COMMENTARY

end of the work day. The employer is required to do this or
else pay them in advance for a longer term of service.

‘This law proves that Mosaic Israel was not a debt-free soci-
ety. There were creditors and debtors. A legitimate biblical goal
is to reduce long-term debt, but God’s civil law does not man-
date absolutely debt-free living. Debt is basic to society, for
society implies a division of labor. Debt will exist in a division of
labor economy until such time as an economically efficient
means of making moment-by-moment wage payments becomes
universal.

The employer who delays payment to his workers is de-
frauding them. Verse 13 says this. But to do this, he is inescap-
ably providing an opportunity for some workers to oppress
their competitors. The worker who can afford to work without
pay for a period is given an opportunity by the employer to
steal a job away from a worker so poverty-stricken that he
cannot survive without payment at the end of the day. ‘This
form of competition is illegitimate, this passage says (“fraud,
robbery”). It is unfair competition. God’s civil law makes it
illegal for an employer to act as the economic agent of any
employee against a destitute competitor There are very few
cases of unfair competition specified in the Bible, but this is one
of them.

Verse 14 prohibits the active assault on the deaf and blind.
We are not to attack defenseless people. The text specifies this.
‘This case law also implicitly condemns all those who sit idly by
when others publicly assault these defenseless pcople. We are
required by God to become covenantal agents of those
victimized people in our presence who arc inherently incapable
of defending themselves judicially: the deaf and the blind. We
are to act as the ears of the deaf and the eyes of the blind
whenever we hear or see others assault them. In short, we are
to accept our role as covenantal witnesses. God reminds us of
who He is: “I am the Lorn” (14b).
