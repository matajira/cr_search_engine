xlvi LEVITICUS: AN ECONOMIC COMMENTARY

oaths and incantations (magical power), thereby affirming a
boundary surrounding God’s name and implying dominion
through cthics rather than magical invocation,” and the
eighth commandment parallels the third, for it is law three in
the second list of five.'! “Thou shalt not steal” is a command
regarding legal boundaries."* The eighth commandment indi-
cates that the concept of boundaries is basic to economic ethics,
the third point of the covenant.”*

Gordon Wenham comments on Leviticus’ place in the Old
‘lestament’s covenant-treaty structure: “(3) The centerpiece of
every treaty was the stipulations section. In collections of law,
such as Hammurabi’s, the laws formed the central section. ‘he
same holds for the biblical collections of law. In the treaties a
basic stipulation of total fidelity to the suzerain may be distin-
guished from the more detailed stipulations covering specific
problems. In this terminology ‘Bc holy’ could be described as
the basic stipulation of Leviticus. The other laws cxplain what
this means in different situations.”’* Leviticus is literally the
center of the Pentateuch: two books precede it; two books fol-
low it.

God seis apart His people and their worship. He makcs them
holy — set apart. He places ritual boundaries around them.
“Leviticus centers around the concept of the holiness of God,
and how an unholy people can acceptably approach Him and
then remain in continued fellowship. The way to God is only
through blood sacrifice, and the walk with God is only through

10. Ibid., ch. 3.

11. I conclude that the Lutherans’ structuring of the Ten Commandments is
incorrect. The fifth commandment is “Honour thy father and thy mother: that thy
says may be long upon the land which the Lorn thy God giveth thee.” This is
of inheritance: point five.

12. Ibid., ch. 8.

13. Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth,
Texas: Dominion Press, 1987), ch. 3.

14, Gordon J. Wenham, The Book of Leviticus (Grand Rapids, Michigan: Eerd-
mans, 1979), p. 30.

  

a law
