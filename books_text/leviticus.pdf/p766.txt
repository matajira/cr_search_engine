710 LEVITICUS: AN ECONOMIC COMMENTARY

numbering, 468, 585
Numbers, 1, 6, 42
nutrition, 342

oath

better than sacrifices, 9-10

growth &, 556

limits on, 90 (also see inter-
position)

prosperity &, 450, 454-55,

Adam’s, 633

atheist’s, 142, 635

baptism, 512

broken, 88-91, 633

churches, 142-44, 631

civil, 95, 102, 142-44, 354,
457, 634

court, 142

covenant, 102, 211, 220,
630-31

false, liii, liv, 64, 135, 136-
37, 142, 145, 209, 210

family, 631

government, 631

guardian, 142-44

guilt removed, 142-44

implicit, 631

Lord’s Supper, 82, 457

peace offering, 75

pluralism’s theory, 457

political, 630

purification offering, 88

removal, 336n

Rushdoony on, 143

sacramental, 97, 457,
631

sanctions, 142, 457, 634-35

self-maledictory, 142, 336n,
457

theocratic, 101-2

trespass offering, ch. 7

‘Trinitarian, 102, 634

obedience

544 (also see sanctions)
sanctions &, 548, 555
trust & obey, 340
also see cthics

objectivity, 319-20

offerings, 9, 82-83

officers, 99

ogam, 30, 31

“old money,” 472

Olmecs, 27

Ontario, 30

oppression,
information &, 441-45
jubilee non-enforcement,

444, 446
price, 442
representative illegal act,

313
State, 442-43, 445
time, 435
two-way street, 441
untrustworthy, 240
weights & measures, 313-14

orchard, 299, 304-7
orphans, 10, 313, 328
Othello, 376-77
ownership
Adam’s fall, 83-84
Adam’s stewardship, 139,

394
capitalization process, 440
costs of, 113-14
disownership &, 466
