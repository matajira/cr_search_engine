Covenantal Fruit 299

fruit? Biologically, nothing at all; symbolically, everything. In
Israel, not to be circumcised was to be judicially unholy, ie.,
common or “gentile.” Those pcople who were holy had been
set apart judicially by God: incorporated into the covenant
people. The new fruit was identified by God as judicially unholy
— not ritually unclean, but judicially unholy, meaning common.
‘The unholy or gentile judicial status of the fruit was not pro-
duced by the land, which was itself holy; it therefore had to be
produced by the Israclites who did the planting. Conclusion: the
fruit’s judicial status of being uncircumcised came from men who were
circumcised. Why was this the case?

Pointing Back to the Wilderness

Obviously, there was nothing unholy about the judicial status
of the circumcised Israelite at the time that he planted an or-
chard. What was it about judicially holy men that produced an
opposite judicial status in the fruit of young trees? Here is the
dilemma: the Israelite’s present judicial status at the time of
planting was holy; the land’s present judicial status was also
holy; yet the fruit would be judicially unholy for three years.
‘The judicial question has to be turned away from the Israelites’
present judicial status in Mosaic Israel to their past, their fu-
ture, or both.

‘The frame of reference surely was not eschatological in the
way that the seed laws of Leviticus 19:19 were. The orchard
statute had nothing to do with tribal separation, the way Leviti-
cus 19:19 did. The law of uncircumcised fruit did not refer
Jacob's promise to a specific tribe of Isracl, nor did it mandate
the permanent separation of tribal inheritance until the Prom-
ised Seed appeared. I therefore conclude that this statute’s
primary frame of reference was historical. The anomaly of two
holy things — land and circumcised planter - producing some-
thing temporarily unholy points back to the gencration of the
conquest of the land: the fourth generation after Abraham’s
covenant (Gen. 15:16). Why do I conclude this? Virst, because
