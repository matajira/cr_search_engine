Preface xlix

can’t beat something with nothing.” It is not enough to mumble
that “Sutton’s book tries to prove too much,” or “There are lots
of different models in the Bible.” There are indeed lots of
proposed biblical models, among them the Trinity, the seven-
day week, and the biblical covenant model.” But when we
come to the question of God's formal judicial relationships with men,
we always come to the covenant. It is a five-point structure.
Accept no substitutes!

The Five Levitical Sacrifices

Most Christians have trouble remembering the required
sacrifices of Leviticus. When people have difficulty remember-
ing something, it is usually because they have no handle, no
model by which to classify what appear to be unconnected
facts.” This has been the problem with the five Levitical sacri-
fices.

Five sacrifices, “Oh, no,” moan the critics. “Here it comes.
He's going to argue that they conform to Sutton’s five-point
covenant model.” Exactly!

22. In my Publisher's Preface to Sutton's 1987 first. cdition, I wrote: “. . . the
author has discovered the key above all other keys to interpreting the Bible, from
Genesis to Revelation. . .” (xi). But what about the doctrine of God? It is included in
the Grst point of the biblical covenant model. The covenaut mode} is more compre-
hensive than the doctrine of God. It includes hierarchy - God> man> creation —
law, sanctions, and eschatology. What about the doctrine of the Trinity? What about
creation? The Trinity and the doctrine of the Creator-creature distinction (creation)
are guiding presuppositions of orthodoxy, as reflected in the creeds. Nevertheless,
the Trinitarian doctrine of God, like the doctrine of creation, appears in very few
texts in the Bible. The Trinity is a doctrine derived from a comparative handful of
texts in the New Testament, Jn contrast, the covenant suucture is found in hundreds
of texts and even whole books of the Bible, including Leviticus. If you are trying to
interpret a large number of texts in the Bible, the texts that are explicitly structured
by the covenant vastly outnumber the texts explicitly structured by the Trinity or the
creation. The traditional Christian exegetical exercise called “find the implied but
camouflaged Trinity in the Old Tesatament,” is far more difficult and far less persua-
sive than “find the implied or explicit covenant model in the Old “Testament.”

 

28. “his is why military history is so demanding, and why so few academic his-
torians work in the field.
