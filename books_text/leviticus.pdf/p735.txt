Index

Joseph &, 521-22
jubilee, 454, 534 (also
see Luke 4:18-21)
King, 145
Kinsman-Redcemer, 56,
391, 540, 592-95
law of, xxxi-xxxii, xxxiii
liberation refused, 523
liberator, 33, 169
liberty, 427
lordship, 512
ininistry of, 7
mouth, 183, 193
offices, 141
ownership of world, 84
Passover lamb, 44
prophet, 391
redeemed nature, 128
representative, 549
represented the nation, 546
resurrection, 1040
sacrifice, 54-55, 61
Second Coming, xv
Seed, 514, 557 (also see
Shiloh)
soldiers &, 119
substitute sacrifice, 50, 54-
55
sword, 85
threatened Jews, 523
vomiting, 189
whip, 155
Christendom, xv, 102, 256,
274-75
Christianity, 2, 85, 490
church
accountant, 395
antinomianism &, 104

679

centrality, 97, 103-4, 458

civil creed &, 465

civil oath &, 142-44

continuity with world, 458

courts, 277

defection, 104

discontinuity with world,
458

division of labor, 255

Eastern Europe, 105n

excommunication, 104
(also see excommun-
ication)

family alliance, 106

heirs, 611

kingdom &, 193, 458

knowledge of Bible, xii-

model, 104, 277

most important, 98

nation, 103

ownership of world, 84

perjury &, 141-42

pre-eminence, 104

progress of, xii

protects State, 143

purifying, 193

sanctions, 103, 142, 360
{also see excommun-
ication)

sealed-off, xiii

sets world’s standards, 104,
277

social order, 103

State &, 93, 97, 104,
111-12, 142-44, 165

State’s model, 104, 275

tithe, 98, 109 (also see tithe)
