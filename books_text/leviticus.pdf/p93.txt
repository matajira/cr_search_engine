Introduction 37

Note: As in previous volumes of this commentary, I capital-
ize the word State when I refer to the civil government in gen-
eral. I do not capitalize it when I refer to the intermediate
American legal jurisdiction known as the state (e.g., California,
Texas, Kansas).
