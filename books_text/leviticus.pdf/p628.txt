572 LEVITICUS: AN EGONOMIG GOMMENTARY

This section on sanctions appears in the fifth section of the
Book of Leviticus, The fifth point of the covenant deals with
succession. Why does a section on sanctions appear here? Be-
cause sanctions are linked covenantally to succession. This is
why eschatology cannot be separated covenantally from theon-
omy, i.c., God's law and its biblically mandated sanctions. Sanc-
tions determine who will inherit what: inheritance and disinher-
itance. God identifies Himself as the God of the covenant:
deliverer, law-giver, and sanctions-bringer. God’s threat of
temporal wrath is to redirect the attention of citizens of a holy
commonwealth to the possibility of disinheritance in history:
wrath as the prelude to corporate disinheritance.

The Fear of God

he God who threatens
these historical sanctions is the God of corporate grace in histo-
ry. Ile led them out of bondage in Egypt. They had been bent
under the yoke of slavery, but He had broken their yoke and
made them walk upright. This upright physical walk was analo-
gous to an upright ethical walk. The language of walking before
God is the language of covenantal obedience, both individual
and corporate.’ The morally crooked walk is mirrored by the
bent walk of the slave who is under a yoke.

The passage begins with a reminder:

  

1. “And when Abram was ninety years old and nine, the Lord appeared to
Abram, and said unto him, 1 am the Almighty God; walk before me, and be thou
perfect” (Gen. 17:1). “And ye shall not walk in the manners of the nation, which I
cast out before you: for they committed all these things, and therefore I abhorred
them” (Lev. 20:23). “In the ninth year of Hoshea the king of Assyria took Samaria,
and carried Isracl away into Assyria, and placed them in Halah and in Habor by the
river of Gozan, and in the cities of the Medes. For so it was, that the children of
Isracl had sinned against the Lorn their God, which had brought them up out of the
land of Egypt, from under the hand of Pharaoh king of Egypt, and had feared other
gods, And walked in the statutes of the heathen, whom the LoRp cast out from before
the children of Israel, and of the kings of Israel, which they had made” (II Ki. 17:6-
8).
