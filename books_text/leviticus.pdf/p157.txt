Corporate Responsibility 101

servatives and libertarians. The liberal adopts the statism of left-
wing Enlightenment thought.” He allies himself with Keynes-
ian economists and secular liberals. Meanwhile, the secular
conservatives, libertarians, and liberals disregard the corporate
implications of adultery in high placcs.** The secularist affirms
the necessity of exclusively earth-bound “transmission belts” for
historical cause and cffect; God’s omniscience and sanctions are
dismissed as historically irrelevant.” The vast majority of hu-
manist-trained and university-certified Christian social theorists
agree with this view. They adopt humanism’s pluralism, so they
are unable to identify the primary locus of civil responsibility in
society: the church and the sacraments.*°

Priesthood and People

The required sacrifices of Leviticus 4 reveal a tighter judicial
link between priest and people than between king and people.
‘The priest sacrificed a bullock for his sin. A bullock also atoned
for the sin of the congregation (vv. 14-15). Civil rulers and
private citizens brought lesser sacrifices. The civil ruler brought
a male goat (vv, 22-26). The individual brought a female goat
or lamb (vv. 27-35). This indicates that the congregation was
sacrificially closer to the priesthood than it was to the civil ruler.
The congregation possessed primary authority in civil government
because the threat against them was great; hence, the more holy the
required sacrificial animal, The king operated by the authority
delegated to him by the congregation (J Sam. 8). His required
sacrificial animal was less holy — less associated with priestly
sacrifice.

 

Christian Individualisin” and “The Moral Atomism of the Enlightenment.”

34. Ibid., ch. 4, subsections on “The Enlightenment’s ‘lio Wings” and “Enlight-
enment Thought and Corporate Responsibility.”

38. Ibid., ch, 4, section on “Adultery in High Places.” I study the effects of the
behavior of Martin Luther King, John F Kennedy, and King Edward VII.

39. Id., ch. 4, section on “Transmission Belts.”
40. Ibid., ch. 4, scction on “Sacramental Pricsthood and Civil Gongregation.”
