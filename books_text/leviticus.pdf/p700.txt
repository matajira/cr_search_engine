644 LEVITICUS: AN ECONOMIC COMMENTARY

The law prohibiting judicial discrimination against strangers in the
land (non-citizens) (Lev. 19:33-36). his law an aspect of the just
weights law. Laws governing justice were not land-based or
seed-based.”!

The law against offering a child to Molech (Lev. 20:2-5). This
was a law governed by the principle of false worship, although
it appears to be a seed law (inheritance) or perhaps a land law
(agricultural blessings). It had to do with identifying the source
of positive sanctions in history: either God or a false god. God’s
name is holy: sanctified.” This will never change.

The jubilee law prohibiting laking interest from poor fellow believers
or yesident aliens (Lev. 25:35-38). This law was an cxtcnsion of
Exodus 22:25. It was included in the jubilee code, but it was
not derived from that code. In non-covenanted, non-Irinitari-

 

 

an nations, however, Christians are the resident aliens. Thus,
the resident alien aspect of the law is annulled until such time
as nations formally covenant under God.®

The law promising fruitfulness and multiplication of seed (Lev.
26:9-10). his law was covenantal, not tied to the holy land or
the tribal structure of inheritance. It was a confessional law, but
because of its universal promise, it was a common grace law.**

Negative corporate sanctions (Lev. 26:13-17). These were prom-
ised to Israel, but they were not tied Lo either the holy land or
the promised sced. The governing issue was the fear of God,
which is still in force.”

The law of the tithe that applied to animals passing under a rod
(Lev. 27:30-37). This law still applies, though it is no longer
very important in a non-agricultural sctting. Ged still prohibits

51. Chapter 19.
52, Chapter 20,
53. Chapter 29,
54. Chapter 34.
55. Chapter 35.
