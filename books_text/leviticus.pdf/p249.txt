The Land as a Covenantal Agent 193

sense that it is said to vomit the nations out of the land, just as
God is also said to be the one who drives the nations out of the
jand.

In the New Testament, we no longer legitimately speak of
the land’s vomiting out its inhabitants. Instead, we read of the
kingdom and its worldwide expansion. Because the self-con-
sciousness and consistency of the individual is supposed to be
greater under the New Covenant than under the Old, the New
‘Testament does not speak of the land as analogous to both God
and man. We read instead of the sword of the Lord: the word
of God that proceeds out of the mouth of God. “And out of his
mouth goeth a sharp sword, that with it he should smite the
nations: and he shall rule them with a rod of iron: and he
treadcth the winepress of the ficrceness and wrath of Almighty
God” (Rev. 19:15).

One reason why the land is no longer spoken of as vomiting
out its inhabitants is that the progress of the gospel is no longer
conducted by means of military conquest. There is no longer a
God-sanctioned system for covenant-keeping people to replace
covenant-breaking people by means of expulsion. Today, they
replace covenant-breakers through performance and productiv-
ity. They are to replace them in positions of cultural and politi-
cal leadership — not by force but by performance.

Covenant-keepers are also to conquer covenant-breakers by
means of preaching. Men are to brought into the “Promised
Land” today by bringing them into the church, and then by
bringing the whole nation under the biblical civil covenant
through a democratic vote. This does not equate the visible
church with the Promised Land, but it acknowledges that the
kingdom of God is primarily manifested in history by the
church, and all those who profess to be Christians are supposed
to be members of the church. Thus, the land is not the primary
agent of enforcement; Jesus Christ is. By purifying the church,
Ie enables Ilis people to purify themselves and to begin the
conquest of the earth by means of the preaching of the gospel.
