XXXVi LEVITICUS: AN ECONOMIC COMMENTARY

political activism.* This raised a major theological problem,
one which none of them is ever forthright enough to admit in
public. By acknowledging that Exodus 21:22-25 is the only
biblical law prohibiting abortion, they would have had to admit
that Rushdoony had already pre-empted this judicial position
because of his view of the continuity of the Mosaic law. They
understood that to uppeal to Fxodus 21:22-25 is to invoke the theo-
nomic hermeneutic. Vhey have generally feared to do this. The
Feinbergs are an exception, and the results are intellectually
embarrassing: “We are not theonomists, but. . . .”

Today, many Christian anti-abortionists blithely assert that
“abortion is murder” We theonomists ask them a question:
What is the biblically mandatory civil penalty for murder? They see a
terrifying chasm opening up before them. They refuse to com-
bine their pro-capital punishment stance with respect to mur-
der — a biblically correct connection — with their rhetoric about
abortion’s being murder. If they did, they would have to call
for legislation demanding the future execution of physicians,
nurses, and former mothers who have been lawfully convicted
of having participated in an abortion. So, they either remain
silent or judicially schizophrenic on this issuc. They do not take
the Bible’s mandated sanctions seriously. They do not even take
their own rhetoric seriously. Not surprisingly, the politicians see
no need to take them seriously. Abortion continues to be legal.

The self-contradictory hermeneutic of the two Feinbergs is a
visible result of the fundamentalists’ long-term judicial dilem-
ma. They want judicial continuity when convenient (e.g., anti-
abortion), while rejecting judicial continuity when inconvenient
(e.g., anti-Bahnscn). They cannot have it both ways. Step by

 

38, Social activism in fundamentalist circles has always meant opposing some-
thing, especially the “big four” temptations of life: alcohol, tobacco, social dancing,
and (in the twenticth century} the movies. Occasionally, fandamentalists band togeth-
er to call on the State t do the repression: political activism. ‘Chis is why the anti-
abortion movement does contain some fundamentalists. But on the whole, except for
publicly supporting anti-alcohol laws, fandamentalisichurches have avoided politics.
