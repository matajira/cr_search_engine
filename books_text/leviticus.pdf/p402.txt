346 LEVITICUS: AN ECONOMIC COMMENTARY

14), There would soon be a rooting up of the nation of Israel.’
The old nation of priests (Ex. 19:6) was about to be replaced by
a new nation of priests (I Pet. 2:5, 9). A change in covenantal
law is accompanied by a change in the priesthood (Heb. 7:12).
This is why Peter was told repeatedly by God in a vision to eat
unclean foods (Acts 10:15). The covenantal separation between
Jews and gentiles had ended forever (Eph. 2). A new covenan-
lal separation had arrived: Christian vs. non-Christian. A new
dietary law accompanicd this new form of covenantal separa-
tion: the Lord’s Supper - a new dictary boundary.

Covenant-keeping man's defilement by unclean or abomina-
ble meats ceased as soon as the Lord’s Supper replaced Pass-
over. Gentiles outside the land were never under its restric-
tions. There was nothing intrinsically evil or unclean in any
food; there was only lemporary uncleanness — as temporary as the
covenantal status of the boundarics of the Promised Land.
When Jesus announced that there has never been anything
intrinsically unclean or defiling about any food, Ile was also
announcing that there was nothing intrinsically sacrosanct
about the boundaries of geographic Israel.

The Jews of Jesus’ day thought that Israel’s dictary laws, like
Isracl’s geographical boundaries, would last forever. Today,
Jews and Anglo-Israelites suppose that the Mosaic dietary laws
are sull binding. But the covenantal significance of Israel's
geographical boundaries and the dietary laws ended together:
the demise in A.D. 70 of national Israc] and the temple sacri-
fices. As Paul wrote to a gentile church, “Wherefore if ye be
dead with Christ from the rudiments of the world, why, as
though living in the world, are ye subject to ordinances, (Touch
not; taste not; handle not; which all are to perish with the
using;) aller the commandments and doctrines of men? Which
things have indecd a shew of wisdom in will worship, and hu-

7. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Tt.
Worth, Texas: Dominion Press, 1987).
