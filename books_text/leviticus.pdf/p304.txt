248 LEVITICUS: AN ECONOMIC GOMMENTARY

nomic order would have to develop from Christianity. Thus, in
order to leave the social order biblically open-ended, the Chris-
lian defenders of the welfare State are forced to deny that the
Bible offers any blueprints at all. Then they tell us what kind of
economic order they would like to see established in God’s
name (by way of Keynes, Marx, or no economist at all).™

The issue of wealth redistribution through taxation is never
discussed by Christian defenders of the welfare State in terms
of Samuel’s warning in J Samuel 8: a tyrannical king is marked
by his willingness to extract as much as 10 percent of his sub-
jects’ net income. To return to such a “tyrannical” tax rate,
every modern industrial nation would have to cut its average
level of taxation by 75 percent. Yet Christian defenders of the
welfare State insist that far too much money is left in the hands
of today’s citizens. We need more “economic justice” in the
name of Jesus, they say. We need grealcr taxation of the
wealthy — and the not-so-wealthy. We need a “graduated tithe.”

The biblical solution is to restrict total personal and corpo-
rate taxation — national, regional, and local taxation combined —
to less than 10 percent of net income, just as the tithe lawfully
collected by the combined levels of a national church's hierar-
chy is limited to 10 percent. But this Old Covenant limit on
taxation is too confining for welfare statists.

‘The State today asserts an implicit claim to be the primary
judicial agent of God in history. The mark of this presumed
primary sovereignty is the lack of biblically revealed limits
(boundaries) on the wealth that it is authorized by Cod to ex-
tract from those under its jurisdiction. This is the political
doctrine of the divine right of the people — an assertion of the
voters’ God-granted moral authority to steal from each other by
means of the ballot box. “hou shalt not steal, except by major-
ity vote.”

  

24. See the footnotes in the essays other than mine in Clouse (ed.), Wealth and
Poverty: the absence of economists.
