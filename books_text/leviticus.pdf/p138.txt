82 LEVITIGUS: AN ECONOMIC COMMENTARY

also by formal acts of restitution for crimes involving a vic-
tim.’ The emphasis is on self-examination (I Cor. 11:28-29).

The peace offering was a meal caten by the donor, the only
shared meal in the Levitical system’s five sacrifices. The
Lord’s Supper is also a shared meal. But is the Lord’s Supper
an act of covenant renewal analogous to the freewill offering of
Leviticus 7? I think not. The Lord’s Supper is judicial. It is an
aspect of the covenant oath (point four).'* Regular participa-
tion in the Lord’s Supper is required from God’s covenant
vassals, just as the Passover feast was. It is not optional. It is a
regularly scheduled public event. Any church member who
refuses to take this sacrament, or who has been excluded from
the table by the church, receives a formal declaration from
God: “Guilty!” This public declaration takes place every time
the Lord’s Supper is served by the church. This is one reason
why it should be offered weckly: to bring under God’s judicial
condemnation all those who are not participating, whether
inside the church or outside. Calvin belicved that the Lord’s
Supper should be ollered at feast weekly."®

In contrast to the sacrament of the Lord’s Supper, which is
analogous to the Mosaic Covenant’s sacrament of Passover, the
peace offering was optional. It was a self-conscious additional
act of sacrifice, “beyond the call of duty.” One manifestation of
our personal quest for peace with God is the presentation of
gifts and offerings above the mandatory tithe. ‘hese constitute
the New Covenant’s version of the animal sacrifice of the peace
offering. When the pastor calls publicly for “tithes and offer-
ings,” meaning money for the church, he is calling for the

12. Gary North, Zols of Dominion: The Case Laws of Exodus (Alen, ‘Yexas: Insti-
tute for Christian Economics, 1990), ch. 7; see also Gary North, Vietim’s Rights: The
Biblical View of Civil Justice (Tyler, Texas: Institute for Christian Economics, 1990).

13. R. K, Harrison, Leviticus: An Inuroduction and Commentary (Downers Grove,
Illinois: Inter-Varsity Press, 1980), p. 79.

14, Sutton, That You May Prosper, ch. 4.

1. John Calvin, Institutes of the Christian Religion (1559), [V:XV11:43.
