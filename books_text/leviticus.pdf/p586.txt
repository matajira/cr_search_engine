530 LEVITICUS: AN ECONOMIC COMMENTARY

tion of Israclite freemen out of bondage was to be automatic in
the fiftieth year, the jubilee year. On the day of atonement in
the jubilee year, the day on which the ram’s horn sounded, no
Israclite heir of the original conquest could lawfully be kept in
bondage except for criminals and those who, through renuncia-
tion of the covenant or by excommunication, had lost their
judicial status as freemen.

This law added another way of escape for the Israelite bond-
servant: redemption by his kinsman-redeemer. The first form of
redemption - the jubilee — required no payment to the slave
owner; the second did. ‘The first was based on judicial inheri-
tance; the second was based on personal grace by the nearest of
kin.

Why would anyone have sold himself to a resident alicn?
Because he had finally run out of income. This raises another
question: Had he already leased his land to another? I think he
had, The sabbatical year system of morally mandatory intercst-
free charitable loans would have protected a person with a farm
to return to. Defaulting on this kind of loan, he would have
sold himself to another Israelite to repay it. His temporary
owner then had to care for him and his family, although with-
out paying him wages, and then was required to give him food
and animals in the sabbatical year (Deut. 15:14-15). This im-
plies that the man in year seven owned his own land to return
to with his new flock. But the man in Leviticus 25 was in such
desperate straits that he had to sell himself and his family into
bondage until the next jubilee year. He would not be entitled
to asscts out of his master’s capital at the end of his term of
service. He had become a stranger in the land. This was only
permitted by God until a kinsman-redcemer bought him back,
or until he could buy his way out of bondage, or until the
jubilec’s trumpet sounded. But the foreigner was under no
obligation to pay him a wage. This made the Israelite slave
especially helpless.
