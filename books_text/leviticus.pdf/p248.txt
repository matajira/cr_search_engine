192 LEVITICUS: AN ECONOMIC COMMENTARY

other words, pcople who build homes in flood plains or on top
of major seismic faults will not sec their property predictably
protected from the effects of flooding or carthquakes merely
because they pray a lot or give money to the poor. The best
they can legitimately expect from God is better information
about controlling floods or better construction methods that
resist Richter-7 quakes. I scriously doubt that increased per-
centages: of adultery will produce increased percentages of
Richter-9 quakes.”

Conclusion

Under the Mosaic Covenant, God dwelt in Israel in a unique
way. As men approached God’s carthly throne room, they
approached holy ground. The extreme edges of this series of
concentric holy boundaries were the nation’s geographical
boundaries. The land of Isracl therefore acted as God's coven-
antal agent. In the New Covenant era, there is no holy ground
separate from common ground. We do not take our shoes olf
when we enter a church, as God required of Moses when he
stood on holy ground (Ex. 3:5), and as some Eastern religions
and Islam require. We do not have ritual foot washings. The
land: of the New Covenant no longer serves as a covenantal
agent. It no longer brings predictable sanctions in history. It is
no longer tied covenantally to military affairs.

Prior to the exile period, the land was spoken of in terms of
its covenantal position as-God’s representing and represented
agent. The land was represented by man, but it also represent-
ed God when it came time for God to bring His negative sanc-
tions against covenantally rebellious residents inside the land’s
boundaries. The Promised Land was analogous to God in the

12. If Lam wrong, Westminster Seminary West would be wise to move out of
Escondido, California to, say, Lynden, Washington. Combine the morality of South-
ern California and the San Andreas fault, and you have a prescription for disaster if
the land is still a covenantal agent.
