civil liberties, 309
civil religion, 635-36
civil rights, 376, 380
civil year, 356
clan, 17
class analysis, 116
class warfare, 472
classical economics, 436
clean/unclean
dictary laws (prophetic),
345, 346
national separation, 340-41
priest and house (leprosy),
166-69, 639
priestly declaration, 149-50,
154
cleanliness, 149-50
cleansing, 45, 89, 150, 167-68
climate, 349
Clinton, Bill, xxviii
clothing, 164, 281, 288-91, 292
coals of fire, 491
Coke, Edward, 143
collateral, 493-95.
Columbus, 25
commentaries, xii, xvi, xx, 388
Committee, 256
common, 122-25, 129
common grace
Assyria, 188n, 543
church’s judgments &, 141
climate, 191-92, 349
fallen men as gleaners, 195
justice, 550-51
outside Mosaic Israel, 557
rain (NT), 191-92, 550-53
restraint on rebellion, 632-
33

681

special grace & 127-28, 543
competition, 234
confession
churches, 144
circumcision, 282, 557
common, 277
dialects of faith, xii
early, 145-46
Isracl’s, 282
pluralism, 636
rural land, 428
separation, 282, 557
subsidy to, 135
congregation, 91-92, 96, 101-2
conquest
aller the exile, 469-70
circumcised fruit &, 294,
303-4
leaven &, 79
preaching (NT), 193-94
progressive sanctification,
430
spoils of war, 412-15
walled cities, 466
conscience, 548
consent, 94
conservation of the soil, 396
consumer sovercignty, 436-39,
532-33, 539, 555 (also see
sovereignty: consumer)
continuity
Christianity, 85
church & world, 458
covenantal, 279-80
hermeneutics, 279-80
leaven, 66-67, 80
reparation offering, 120
sacrifices, 647-50
