INTRODUCTION TO PART 3

Therefore shall ye keep my commandments, and do them: I am the Loxp.
Neither shall ye profane my holy name; but I will be hallowed among the
children of israel: I am the Lorp which hallow you, That brought you
out of the land of Egypt, to be your God: I am the Lorp (Lev. 22:31-
33),

Separation: this is the heart of the Book of Leviticus, the
third book of the Pentateuch. The biblical meaning of holiness is
lo be set apart by God, i.e., hallowed. Separation and holiness are
inescapably linked; or, we might say, inescapably bound. Leviti-
cus 17-22 presents the laws of separation.

Leviticus 22:31 speaks of profaning God’s name in relation
to obeying the commandments. ‘This points back to the third
commandment, which prohibits the taking of God’s name in
vain (Ex. 20:7). God places a boundary around His name; to
violate this boundary is to profane it. ‘hat this law is recapitu-
lated in a passage mandating obedience to God’s command-
ments should not be surprising. Point three of the biblical cove-
nant model, ethics, is related to the third commandment. It is
also related to the eight commandment, “thou shalt not steal”
(Ex. 20:15), the Bible’s supreme affirmation of the rights of
private property, i.e., the right of individuals to own, use, and
sell (disown) property.’

1. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tylex,
