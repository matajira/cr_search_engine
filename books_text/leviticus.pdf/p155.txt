Corporate Responsibility 99

lions; nevertheless, it is the people who will reccive the brunt of
God's judgment, for it is they who possess greater authority
under God.

If my thesis on the primary connection between priest and
people is correct, then the fundamental political thrust of Old
Testament covenant theology was toward theocratic republicanism:
the political authority of formally covenanted citizens. In both
church and State, the locus of institutional authority in Old Covenant
Israel flowed upward: from the people to their legal representa-
tives. The moral integrity of the ecclesiastical hicrarchy was of
greater importance for the survival of a biblically covenanted
society than the political hierarchy’s integrity was.

In church and State, those people who possess initiatory
earthly authority — church members and citizens — are those
who are under the formal jurisdiction of superiors who possess
derivative authority: officers. The officers’ authority is derived
from above ~ God — but also from below, i-e., those who are
under their oath-bound authority. Those who are under the
visible sanctions of these two covenantal institutions are those
who are required by God to exercise institutional sanctions:
positive and negative. Formal acts of covenant renewal periodi-
cally manifest this God-derived sanctioning authority of the
people. This is why there are no acts of covenant renewal for
the family: there are no formal sanctioning powers held by
those who are under the authority of the head of the housc-
hold.** Authority is delegated downward by God to the head
of the household, not upward from his wife or children.

33. The negative sanction may he imposed by leaving the jurisdiction of the
particular institutional authority. ‘his is called “voting with your fect.”

34. Minor children are not legally allowed to flee the ju
the houschold. Civil governments are required to return runaway children to their
parents upless the civil authorities can prove in civil court that the parents have
broken the family covenant by child abuse, either moral or physical. On the other
hand, adult children cannot legally be compelled 1o return to their parents’ housc-
hold. This is why the parent-authorized, forcible “de-programming” of adult cult
members is biblically legal; itis a form of kidnapping.

diction of the head of

 
