31

SLAVES AND FREEMEN

Both thy bondmen, and thy bondmaids, which thou shalt have, shall
be of the heathen that are round about you; of them shail ye buy bond-
men and bondmaids. Moreover of the children of the strangers that do
Sojourn among you, of them shall ye buy, and of their families that are
with you, which they begat in your land: and they shall be your posses-
sion. And ye shall take them as an inheritance for your children after
you, fo inherit them for a possession; they shall be your bondmen for ever:
but over your brethren the children of Israel, ye shall nol rule one over
another with rigour (Lev. 25:44-46).

The text must be taken literally. First, Israclites could buy
slaves from other nations. These people were already slaves
according to the laws of their own nations. The Israclites did
not make them slaves; they mercly changed the slaves’ resi-
dence: new boundaries. Second, the Israclites could buy slaves
from among strangers residing in the land. But there was no
authorization to buy slaves from other Israelites. This means
that slaves in one'Israclite family could not be sold to another
family. The laws of inheritance forbade such sales (v. 46). They
became part of a family’s permanent inheritance.

There is no question about it: Mosaic law legalized inter-
generational slavery. If Leviticus 25:44-46 is still binding, then
the enslavement of those who not part of the covenant by those
