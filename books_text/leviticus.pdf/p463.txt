The Sabbatical Year 407

Finally, this indicates that there was less net agricultural output
in the seventh year than in the other six.

This law was good for the land and all the creatures great
and small that inhabited i1. Owners were restrained in their use
of God’s land. Agricultural practices that overworked the land
were restrained by this law. The land, as God’s judicial agent,
deserved its rest. ‘his law mandated it. If this land-protecting
aspect of the law was enforced by the State, as I believe it was,
it rested on the legal status of the land as God’s judicial agent,
not on the State as an agency of wealth redistribution to the
gleaners. This law is no longer in force in the New Testament
era because the land ceased to be a covenantal agent in A.D. 71.

The sabbatical year law was enforced alter the Babylonian
exile (I Macc. 6:49, 53). ‘he fear of God is a great incentive.
During the exile, Ged had substituted Ilis negative sanctions in
history for the failure of the priesthood and the State to enforce
the sabbatical year law. Exile was God’s partial disinheritance of
Israel. It warned Israel of comprehensive disinheritance, should the
nation continuc to rebel. The exile altered land tenure: a new
distribution replaced the original distribution under Joshua.
The exile had severed the judicial link between each family’s
plot and Joshua’s distribution. The jubilee land laws had been
established by genocide, but genocide was neither authorized by
God nor possible afier the exile. ‘The jubilee’s heathen slave
laws remained in force, but the residents who participated in
any post-exilic distribution were to become immune to the
threat of permanent servitude by Israclites.

‘Lhe sabbatical land law was an extension of the law of the
sabbath. It was not a subset of the jubilee land laws; on the
contrary, the jubilee land laws were temporary applications of
the sabbath law’s principle of rest. If there are any New Testa-
ment applications of the sabbatical year of rest for the land,
they are based on ecology or the general authority of sabbath
rules, not on the jubilee’s military conquest. ‘This transfers the
locus of authority to the landowner: individual, not corporate.
