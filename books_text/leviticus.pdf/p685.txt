Conclusion 629

because the Mosaic Covenant had ceased to have any authority.
God's predictable, covenantal, negative corporate sanctions
were thoroughly applied to that nation which had broken His
covenant. Divine protection for the boundaries of the land
would never again defend Israel’s residents.

Government and Sanctions

This raises a major question of biblical interpretation: What
about those aspects of the Mosaic law that applied to Isracl’s
civil government? Were they all annulled with the annulment
of Isracl’s geographical boundaries? Were any of those laws
cross-boundary phenomena? That is, did any of them serve as
binding judicial standards for foreign nations? Deuteronomy
4:4-8 indicates that at least some of them did. Does this mean
that these have been extended by God inte the New Covenant
era? Are they still covenantally binding and therefore judicial
ideals toward which all nations should strive, and in terms of
which all nations are judged in history?

My answer is the answer which is sometimes said to be the
ultimate summary of all sociological theory: some are, some aren't.
This answer in turn requires an additional principle of inter-
pretation, a theological means of separating: 1) the cross-boun-
dary Mosaic Covenant civil standards that are still judicially
binding on men and nations from 2) the temporally and geo-
graphically bounded Mosaic standards. In short, the correct
answer requires a hermeneutic: a principle of interpretation.
‘Lhe Book of Leviticus forces serious Christians to search for
this biblical hermeneutic. Without this hermeneutic, Leviticus
becomes a snare that traps antinomians in their total dismissal
of all of its laws, and iraps legalists in their total acceptance.

By Oath Consigned

In order to apply the Bible judicially to the governmental
realm — personal, church, State, and family - we require two
