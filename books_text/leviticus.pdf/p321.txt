The State’s Monopoly of Vengeance 265

any more than “thou shalt not kill” applies to the State. What
Leviticus 19:18 does is to establish the State as the lawful mo-
nopolist of covenantal vengeance in history.” ‘Vhe Bible is nei-
ther pacifistic nor anarchistic; it affirms the legitimacy of the
State in seeking public law and order. But both the law and the
order must be God’s — a covenantal, oath-bound law-order:’
The State exercises a monopoly over public sanctions."

Establishing the Judicial Conditions of Wealth-Creation

The State makes wealth-creation possible for individuals by
protecting private property, i.c., by protecting individuals who
own property. The State is required by God to enforce the
decisions of property owners to exclude others from using their
property. The State is therefore to enforce legal boundaries
that are established by private contract. Property owners are
given legal immunities — rights - by God in history, and these
immunities are to be defended by.the State whenever the victim
of an unauthorized invasion appeals to the civil magistrate. The
State is to defend the rights of stewards over the property that
God has assigned to them by covenant (lawful inheritance) or
by contract. As Rushdoony says, “All property is held in trust
under and in stewardship to God the King. No institution can
exercise any prerogative of God untess specifically delegated to
do so, within the specified arca of God’s law. The state thus is
the ministry of justice, not the original property owner or the
sovercign lord over the land.”*

2. ‘The family is not an agency of vengeance. It is an agency of justice only within
the boundaries of a covenanted houschold.

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Tastitute for Christian Economics, 1987), ch. 12. Second edition: 1992.

4. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), ch. 16, section on
“Monopoly Control Over the Sword.”

5. R.J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), p. 504.
