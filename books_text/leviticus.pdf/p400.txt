344 LEVITICUS: AN ECONOMIC COMMENTARY

animals. ‘The manna ceased when the entered the land. After
they conquered the land, they would have no eating habits to
overcome, and therefore no gastronomical temptation to mix
with any of the remaining tribes of Canaan.

These laws marked off the Israelites gastronomically, just as
circumcision marked them off physiologically. The Levitical
dietary laws were no more permanent than the Passover law —
and no less permanent. In captivity, they could not journey to
Jerusalem to celebrate the mandatory feasts. Abraham had been
instructed to circumcise those males under his household au-
thority, but he received no instruction regarding his diet. Why
not? Because he did not dwell in the land of Canaan as a per-
manent owner. He was still a stranger in a strange land, He was
a pilgrim..A pilgrim has no geographical headquarters, no
geographical home. Abraham’s earthly home was eschatological.
God told him that his family’s inheritance of the land would not
take place until the fourth generation after him (Gen. 15:16).
So, he did have to honor the jaw of circumcision, for circumci-
sion identified who his heirs were: a law of covenantal separa-
tion. The Israelites in Joshua’s day crossed the Jordan, camped
and Gilgal, were circumcised, and celebrated the Passover in
the land (Josh. 5:2-10). Then they ate the corn of the land: the
spoils of conquest (v. 11). They thereby also claimed their in-
heritance. They thereby claimed their national headquarters.
“And the manna ceased on the morrow after they had eaten of
the old corn of the land; neither had the children of Israel
manna any more; but they did eat of the fruit of the land of
Canaan that year” (v. 12). At that point, they had the option of
violating the dietary laws that Moses had announced four de-
cades earlier. Their testing began at Gilgal.

The laws governing Passover had been given in Egypt before
they crossed the boundary out of Egypt to enter the wilderness
(Ex. 12), Passover’s laws were primary in Mosaic Israel. ‘hey
established the rite that would henceforth celebrate their deliv-
erance from Egypt. Passover was celebrated inside Egypt. Pass-
