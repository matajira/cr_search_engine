Wine as a Boundary Marker 161

convict drunk drivers, harsh economic sanctions would be
applied to. those driving while intoxicated. Other people are at
risk; thus, the person under the influence of alcohol or drugs
is a threat to society.

But what about after work? Why should alcohol be prohibit-
ed, if the person does not subsequently drive? What about
relaxation? ‘Vhere is no biblical prohibition. ‘The enjoyment of
conviviality is sometimes enhanced by the loosening of inhibi-
tions that alcohol produces. This is the “merry heart” phenom-
enon: the reduction of worldly cares that mtcrfere with inter-
personal relationships. The merry heart is a legitimate goal
when one’s work is completed, “Go thy way, cat thy bread with
jey, and drink thy wine with a merry heart; for God now accep-
teth thy works” (Eccl. 9:7). Anyone who would wanslatc the
Hebrew word for wine as “grape juice” in this passage is per-
sonally unfamiliar with the merrying effects of wine — and
proud of it!

Modern fundamentalism views the God of the Oid Testa-
ment as horribly harsh. For example, God’s law requires wit-
nesses to stone those convicted of a capital crime. “The hands
of the witnesses shall be first upon him to put him to death,
and afterward the hands of all the people. So thou shalt put the
evil away from among you” (Deut. 17:7). Such judicial barba-
rism is not required today, they tcll us. “We're under grace, not
jaw.” (In fact, Christians are today universally under covenant-
breakers and their laws.) Yet at the same time, they view the
God of Israel as far too morally lax, allowing people to drink
alcohol. In both cases, Mosaic law is a great embarrassment to
them. They do not consider an alternative viewpoint, namely,
that pictistic fundamentalism is a great embarrassment to
God.”

12. Reformed pietism is only halfembarrassed by the Old Testament. its defend-
ers are repulsed by the thought of the capital sanction of stoning, bul some of them
do enjoy drinking. On the fundamentalists’ hostility to the cultural dominion that is
symbolized by wine, see North, Boundaries and Dominion, ch, 8, section on “Breaking

  
