402 LEVITICUS: AN ECONOMIC COMMENTARY

rest, and the poor had access to the fields. God therefore
placed self-interested monitors in the midst of the community
The question was: Would these monitors possess sufficient
power or influence over landowners through the priesthood?
The answer for 490 years: no.

The Threat of Debt

Because this law pressured landowners to save for six years
in preparation for the sabbatical year, it subsidized those who
possessed an attitude favorable to thrift, i.e., future orientation.
Simultaneously, this law threatened improvident landowners
with debt servitude, beginning in the year of rclease.° In the
seventh year, all charitable, zero-interest loans to poor Israelites
became null and void (Deut. 15:1). Creditors could not legally
collect from impoverished debtors. Mcanwhile, the economy
grew tight: reduced food production. Improvident landowners
went looking for loans to get them through the year. There
would have been greater-than-normal demand for interest-
bearing loans, i.e., higher interest rates. This would have tend-
ed to squeeze the weakest borrowers out of the loan market.
Lenders prefer to lend to those who are likely to repay. On the
other hand, if an evil man wanted to trap a weak debtor in
order to gain control over his labor if he defaulted, the ycar of
national gleaning would have been an ideal time. ‘he recently
liberated debtors would have known this. Their memory of
their previous bondage was to keep them from succumbing to
this temptation. The poor had access to the untilled fields of
the landowncrs. They were to take advantage of this unique
situation and stay out of debt. They were not to “return to
Egypt” by going into debt and risking another round of bond-

age.

9. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), ch. 24, section on
“Lhe Pressure on Landowners to Save.”
