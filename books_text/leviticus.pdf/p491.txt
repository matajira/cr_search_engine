Economic Oppression by Means of the State 435

The context indicates that rural land was the thing being
bought and sold. But the legal restriction on the leasing of land
would also have applied to the leasing of men. If, for example,
an Israclilc was sold into bondage because of his failure to
repay a business debt, his term of servitude could not extend
beyond the jubilee year.’ ‘Vhe law required that “ye shall -re-
turn every man to his possession, and ye shall return every man
unto his family” (Lev. 25:10b). Business debt could not be
collateralized by land or servitude beyond the jubilee.

The first question is more difficult to answer. What is oppres-
sion in this context? Has it anything to do with pricing? The
text indicates that it has everything to do with the periad of time
in which the terms of the contract will apply. Time has some-
thing to do with pricing, but what? “According to the multitude
of years thou shalt increase the price thereof.” The question
arises: Increase the price from what? What were the price floor
and price ceiling that governed the pricing of additional years?
How were they established? ‘fo answer these qucstions in the
absence of historical records, we need to understand something
about modern capital theory.

Pricing a Factor of Production

The text speaks of the years of the fruits. “According to the
number of years after the jubile thou shalt buy of thy ncigh-
bour, and according unto the number of years of the fruits he
shall sell unto thee” (v. 15). This is a very important cconomic
concept. Capital theory is dependent on it. Land and labor
produce fruit over time. This is what makes land and labor
valuable. Modern economic theory, beginning with the margin-
alist revolution of the early 1870’s,? attempts to explain the

 

L. If he was being sold to repay a zero-interest charitable joan, his term of
servitude could not extend beyond the sabbatical year (Deut. 15:12).

2. The simultaneons and independent work of William Stanley Jevons, Lcon
Walras, and Carl Menger. See The Marginalist Revolution in Keonomics: Interpretation
