Promise, Adoption, and Liberty 493

with zero-interest charitable loans to poor people, cither Israel-
ites or resident aliens (vv. 35-38). The succeeding section deals
with inter-generational slavery: a legal condition exclusively of
non-Israelites (vv. 44-46). In between is this section: how to
treat a poor Israelite.

Two Forms of Bondservice

In the previous chapter, I argued that the identifying mark
of a person who was morally entitled to consideration for a
zero-interest charitable loan was his willingness to become a
bondservant if he did not repay the debt on schedule. In the
sabbatical year, charitable debts were cancelled (Deut. 15:1-7).
So was bondservice (Deut. 15:12). The two obligations were
linked judicially. When the legal obligation to repay a charitable
loan ceased, so did the obligation to serve as a bondservant for
having defaulted on a charitable loan.

Leviticus 25:39 states that an Israclite could be sold into
bondservice. He would not automatically go free until the jubi-
lee year. The sabbath-year release did not apply to him. I call
this jubilee bondservice, in contrast to sabbatical. I argue in this
chapter that both forms of bondservice were likely to have been
legal penalties for personal bankruptcy. There was always the
threat of debt bondage in Mosaic Israel. The differences be-
tween the two forms of bondservice were the results of two
different types of loans: charitable vs, non-charilable. There was
a much greater threat of long-term bondage for having default-
ed on a non-charitable loan than a charitable loan. A person
entered a business debt contract with open eyes. A poor man
who sought a charitable loan was under greater external con-
straints. God imposed reduced risks of servitude on him.

Bondservice as Collateral

A man’s unwillingness to bear the risk of up to six years of
bondservice for his failure to repay a loan established the loan
