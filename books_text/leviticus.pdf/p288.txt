232 LEVITICUS: AN ECONOMIC COMMENTARY

ly prosecuted by victims in our day. The Bible regards a verbal
curse as a judicial act with consequences in history, just as it
regards a verbal blessing.

This outlook is foreign to modern man, both humanist and
pietist. Modern man does not believe that God’s blessings or
curses are called down on others in history because a represen-
tative covenantal agent pronounces blessings or curses. The
third commandment is clear: God’s name must not be taken in
vain. The frame of reference is the misuse of God’s holy name
- a boundary violation - by someone who is not authorized to
invoke that name judicially.

Cursing a deaf man is a double violation: calling down God’s
curses illegitimately, and cursing someone who cannot respond
judicially. The deaf man is unaware of the boundary violation.
Because God’s name has been misused, or at least the violator
has judicially misused language, society is at risk. The agent
who has been authorized by God to press charges in God’s
name in an earthly court — the victim — is unawarc of the viola-
tion. This transfers responsibility for invoking a lawsuit from
the victim to the witness.

Pressing Charges

The deaf victim must be informed of the infraction, and the
blind person must be informed of the identity of the person
who tripped him. The blind person cannot press charges. He
did not see who tripped him. Similarly, a deaf person cannot
respond to a curse against him, since he did not hear it.
Through no fault of their own, these victims cannot bring a
lawsuit against the evil-doers who have broken Cod’s law.

But God can. So can His lawfully appointed courts if a rep-
resentative of the victim either informs the victim or, if the
victim cannot press charges himself (e.g., a mental defective),
the representative presses charges in the name of the victim.
Victims in these cases need spokesmen to act in their behalf,
