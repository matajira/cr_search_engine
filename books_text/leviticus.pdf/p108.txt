52 LEVITICUS: AN ECONOMIC COMMENTARY

As we shall see in this chapter, any attempt to offer a blem-
ished sacrifice is a judicially representative assertion of man’s
own partial autonomy: a denial of man’s total depravity and
also God’s absolute sovereignty. It asserts that man’s sin is really
not so bad. On the other hand, any attempt to offer more than
what is required is also an assertion of man’s partial autonomy:
a declaration thal men are capable of paying God everything
they owe Tim out of their own assets. To argue either way —
less than an unblemished animal or more than one unblem-
ished animal — is to argue for the autonomy of man: man’s
ability to buy his own salvation. Ged does not allow man to
substitute sacrifices.

Stewardship and Ownership

The whole burnt offering symbolized God’s primary owner-
ship (point one of the biblical covenant model) and man’s stew-
ardship under God (point two). Whatever man owns has been
granted to him by God. Both wings of the Enlightenment have
denied this fact emphatically. The Enlightenment’s right wing
has attempted to substitute the market for the State as the
primary sovereign. This is the basis of the doctrine known as
consumer’s sovereignty. In reaction, the Enlightenment’s left
wing has transferred it to the State, which ts an easier concept
for most men to grasp: sovereignty as power.’ Whenever the
doctrine of sovereignty is transferred from God to the State, so is the
concept of primary ownership. Vhe State is then regarded as the
absolute owner. Individuals become stewards of the State. They
own only what the State allows them to retam. A grant ol tax
excmption by the State is regarded as revocable at any time.
This is why a successful defense of freedom must begin with the doctrine
of God’s sovereignty and permanent restraints on those covenantal
agencies that represent God in history. The permanent economic

6. Sce North, Boundaries and Dominion, ch. 1, section on “Substitute Sacrifices.”
7. Ihid., ch. 1, section on “Public Sacrifices and Implicit Sovereignty.”
