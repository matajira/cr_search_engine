The Priesthood: Barriers to Entry 589

shekels. Very young children’s prices were lower: 5 shekels
(male) and 3 shekels (female). For the elderly, the prices were
15 shekels (male) and 10 shekels (female).

The formal prices of the sexes differed. Males were priced
higher than females in every age group. Similarly, old people
were priced higher than very young children, but less than
children age 5 to 20. Why? Did this have something to do with
market pricing? These were not cases of pure market pricing,
but can the differences in formal prices be explained in terms
of expected productivity, just as market prices can he ex-
plained? Yes, but such an explanation is misleading.'*

Prices always serve as barriers. The question is: Were prices
in this instance barriers to entry or barriers to escape; that is,
were they entry prices or redemption prices? Were they based
on the value of services to be redeemed or were they tests of
authority to be honored?

Submission to Authority

Let us begin with an assumption: these prices were dowrics,
not redemption prices. Why was the highest entry price re-
quired of an adult male? Because the adult head of a house-
hold was a man who was used to exercising [amily authority
and perhaps other kinds of civil authority. By placing a high
entry price on his adoption into the tribe of Levi, God protect-
ed His priestly servants from invasion by two groups: 1) power-
seckers seeking to extend their authority into the church; 2)
poor pcople seeking a guaranteed income as members of the
tithe-receiving tribe, The power-seckers first had to abandon all
legal claim to their original inheritance and also had to provide
a considerable entry fec. Married men also had to pay for their
wives’ and minor children’s entry into the tribe of Levi. This
further restricted entry into the priestly class.

18, North, Houndaries and Dominion, ch. 36, subsection on “Explanation: Econom-
ic Productivity.”
