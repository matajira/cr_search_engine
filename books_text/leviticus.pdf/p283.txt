Protecting the Weakest Party 227

division-o[-labor economy. (It surely does not mean that we are
required to become household slaves.)

The restraining factor against the extension of too much
credit by the stronger party is the employer's fear that the
worker wil] cither quit before his term of service ends or else
not produce competent work. It is too expensive for the em-
ployer to sue the average worker for damages; court expenscs
plus his own time in court exceed the money owed.® The eco-
nomic judgment of the employer is the restraining factor. He
suspects that he will not be repaid if he extends too much
credit.

What this text specifies is that the worker must not be asked
to work for a week or two in order to receive his wage. There
is. always a risk of default on the part of the debtor, whether he
is the employer or the worker. This law specifies that the risk of
default for this form of debt —- wages beyond one work day —
must be born by the employer, not by the worker. This law
prohibits a form of robbery: by the employer and also by the
employer's accomplice, i.e., tne worker who can afford to accept
a delaycd-payment contract, thereby excluding the poorest
workers from the labor market.”

The employer must not become a thief by withholding any-
one’s wages.* By forcing the employer to make restitution to
his employed workers who had seen their wages withheld, the
law reduces the amount of such robbery of those unseen by the
judges: future workers who are too weak even to compete for
the delayed-payment job.

6. God does sue workers who default on His advance payments. Some are sucd
in history; all are sued on the day of judgment. Court costs are irrelevant to God

1. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, 'Lexas: Institute for Christian Economics, 1994), ch. 13, sections on “A
Case of Economic Oppression” and “Bargainers: Strong, Weak, and Weakest.”

8. Ibid., ch. 13, section on “What Did the Employcr Steal?”

9. Ibid., ch. 18, section on “The Limits of Judicial Knowledge.”
