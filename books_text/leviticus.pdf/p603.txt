Nature as a Sanctioning Agent 547

God’s promises to a corporate entity do not mandate that
there be a representative political agency to serve as His primary
economic agent. ‘This means that a central agricultural planning
bureau should not be created by the State, nor should such an
agency make the decisions about what to plant, where, and
when. There must be no civil “Department of Bread and
Wine.” Neither it nor any another political agency should de-
cide which crops to sell, at what price, and to whom, except
during wartime, and then only because the State takes on a
priestly function, when its corporate decisions are literally life-
and-death representative decisions. Nevertheless, the ques-
tion remains: If God makes men responsible collectively, as [lis
covenantal promises indicate that He does, then what kind of
representative human authority should be established in order
to monitor the arena — the boundaries — in which the sanctions
are applied, both positive and negative?

Stipulations and Representation

God’s covenantal promises in the Mosaic law were ethical,
not magical or technical. They were governed by God’s stipula-
tions: the boundaries of legitimate behavior. Were these stipula-
tions exclusively civil? No. Were they predominantly civil? No.
The Mosaic laws matched the four covenants, ie., the four
biblically legitimate sclf-maledictory oaths: individual, familial,
ecclesiastical, and civil. The problem in any covenanted society
is to discover which agency has primary jurisdiction in any
specific instance. No human agency has final, total authority.
Only God possesses absolute authority, an authority that He

12. Even during wartime, politicians should strive to Iet the market allocate
resources in most instances. Fiscal policy - taxing and spending ~ not monetary
inflation coupled with a system of compulsory rationing, should be the primary
control device. This enables producers to make rational decisions about. what to
produce. The prolit system motivates producers to create the most efficient weapons.
Ludwig von Mises, Human Action: A Treatise on Economics (New Haven, Connecticut:
Yale University Press, 1949), ch. $4, sect. 2.
