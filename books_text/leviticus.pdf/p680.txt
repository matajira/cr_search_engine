624 LEVITICUS: AN ECONOMIC COMMENTARY

owner can pay its market price plus 20 percent. The presump-
tion is, however, that prize animals of breeding age will be
segregated in advance. ‘Lhe tithe on the net increase in prize
animals must come from the segregated herd of prize animals.
Such segregation was not lawful in Mosaic Israel (Lev: 19:19).°

If, after counting everything owed, there are up to nine
beasts left over, no tithe is imposed. God still gives herd owners
the benefit of the doubt.

What about the ban? Today, we do not sacrifice animals to
God. Thus, to place an animal under the ban is to misinterpret
this law. The owner can buy back the beast al a market price,
but probably at public auction. Then he pays an additional 20
percent to the church. No cheating is allowed; whatever he
pays for the animal, and however he obtains it, he pays 20
percent of what the purchase price had becn at the time of the
auction or its initial sale by the church.

3. See Chapter 17: “The Preservation of the Seed.”
