700 LEVITICUS: AN ECONOMIC COMMENTARY

hidden agendas (mod-
ern), 385-86

holiness, 389, 430

inequality, 433

inheritance &, 506

judicial, 599

kinsman-redeemer, 479

land Jaws annulled, 407

legal rights, 549

Levites, $87, 390-91, 432,
477, 598

Levitical cities, 598

loans (commercial), 494-95

manumission of slaves, 525

meaning of, 387-89, 408,
524

miracle year, 450-52

national Israel, 640

original title to land, 416

political today, 385-87

post-exilic, 529

private property, 387

purpose of the law, 518-20,
524

records (none), 409

resi, 393-94

sabbath, 393, 412

sanctuary, 426

separation, 423

slavery &, 427, 469, 517-20,
525

spoils of war, 412-15

subordinate alicns, 424

timing & calendars, 409

triple crop, 450-54

trumpets, 409, 411

trust, 453

urbanization, 421, 432

walk, 410
walled cities, 388, 412, 430,
432
Judah, 12, 638
judge
alicns, 425, 477
authority, 258
bribery, 316, 363
citizen, 471-72
corrupt, 317
income, 363
intuition, 321
judgment of, 316
local, 250
not omniscient, 263
payment of, 363
politics, 250
self-interest, 361
Judges, 471-72, 503, 508
judgment
alcohol &, 153-56, 160-61
analogical, 317-21
disinterested, 236
division of labor, 253-56
final, 321
lack of precision, 319
local court, 257
next-to-last, 267
objective/subjective, 319-20
priesthood, 155-56
total, 169
wine &, 160
juggler, 566
jurisdiction, 251
jury
American, 259-60
authority, 259
biblical, 260-61
