Priestly Representation 73

The meal offering was priestly. It was associated with Isracl’s
status as priest of the nations. The common Israelite was held
responsible by God for honoring the priestly laws of separation
from the nations. This sacrifice probably atoned for violations
of the laws of separation.
