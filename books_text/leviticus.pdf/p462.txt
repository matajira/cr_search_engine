406 LEVITICUS: AN ECONOMIC COMMENTARY

Conclusion

The judicial foundations of the sabbatical year of rest were
two-fold: 1) the sabbath rest principle; 2) God’s original own-
ership of the land. At the time of the conquest, God transferred
control over the land to families that held legal title on a share-
cropping basis, operating under specific terms of the original
leasehold agreement. The lease provided a payment to God
(the tithe), ic., a high percentage return to God’s authorized
sharecroppcr-owncrs (90 percent before taxes), and a provision
for the maintenance of the long-term capital value of the land
(the sabbatical year). Those residents in Isracl who did not own
the land had legal title to the output of the land: unrestricted
harvest in sabbatical years. This legal title was to be enforced by
the Levites and priests, not the civil government.

The judicial issue of the sabbatical year was rest: rest for the
Jand, hired workers, and animals. This also included release
from the requirement to repay charitable debts (Deut. 15). By
“rest,” the law meant a respite for the landless from the requirement
to work for the landed. This law governed agricultural land and
those who worked it. There is nothing in the Bible to indicate
that it governed any nonagricultural occupation.

This law pressured landowners to plan and save for the
sabbatical year. They had to store up both food corn and seed
corn, When this law was enforced, it forced them to develop
the habit of thrift, i.c., future-crientation. The law also required
landowners to forfeit the automatic (though not “natural”)
productivity of the land in the seventh year. The poor, the
stranger, the field animals, and the regular harvesters all had a
legal claim on this production, if they were willing to do the
work to glean it.

The Levites were the enforcers of this law as it applied to the
gleaners’ lawful acccss to the fruits of the land. The Levites
refused. This indicates they had litle or no short-term econom-
ic incentive to enforce it. This in turn indicates that their tithe
income was greatcr when the land was planted and harvesicd.
