Sacrifice, Stewardship, and Debt 61

owned, he had to acknowledge ritually that it was all a gift from
God. He acknowledged that his property was a residual: things
left over for his use after God had taken His fair share. ‘This
same theology of residual ownership undergirds the tithe.

Mosaic sacrifices were representative. They represented the
death of man and the death of nature. Boih man and nature
are under the curse of death because of Adam’s rebellion.
When God extended grace to man through Jesus Christ, He
also extended grace to nature. No longer does God require
animal sacrifices. Men may lawfully keep their blemish-free
animals, and the blemish-free animals now keep their lives.
Because God the Father has definitively extended grace to man
and nature in history through the perfect sacrifice of Jesus
Christ, there is no further need for man to shed blood in order
to placate God.

Economicaily, bankruptcy laws acknowledge the Bible’s view
of debt forgiveness. When a man declares bankruptcy, he hands
over alj his assets to his creditors, including all debts owed to
him. He can no longer demand payment of debts owed to him,
for they are no longer owed to him. Whatever had been owed
is now paid to his creditors. This acknowledges the hierarchical
nature of stewardship and forgiveness.

The Bible’s definitive limitation on blood sacrifice has placed
a definitive limit on mandatory economic sacrifice. Neither the
State nor the church possesses an unlimited claim to our
wealth. The tithe seis the maximum limits of both institutions in
New Covenant times. This is a great blessing from God; under
the Mosaic law, the combined burden was far greater — at least
twice as great.” But when men refuse to sacrifice to God, they
eventually wind up sacrificing far more to the State. God gra-
ciously limits His economic demands on us. The State, repre-
senting the collective god, autonomous man, is not equally gra-
cious. ‘This is why widespread moral rebellion always brings

27. Sce the Introduction, above, pp. 18-19.
