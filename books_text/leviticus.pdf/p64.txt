8 LEVELICUS: AN ECONOMIC COMMENTARY

Israel's boundaries were established in terms of God’s unique
presence among His people: “And I will set my tabernacle
among you: and my soul shall not abhor you. And I will walk
among you, and will be your God, and ye shall be my people”
(Lev. 26:11-12). The Book of Leviticus rests on the assumption
that God’s unique covenantal presence among His set-apart
people had geographical implications. The Mosaic Covenant was
@ geographical covenant. God’s covenant with Abram (renamed
Abraham: “father of nations”) involved land because it involved
seed: “In the same day the Lorp made a covenant with Abram,
saying, Unto thy seed have I given this land, from the river of
Egypt unto the great river, the river Euphrates” (Gen..15:18).

God’s goal in all of His laws is to place men under certain
moral and judicial boundaries. Men are to acknowledge God’s
absolute sovereignty over them by accepting the authority of
His covenant’s hicrarchy. The stipulations enforced by His
hierarchical institutions scrve as the legal boundaries of cove-
nant-keeping man's existence. Men are to learn to live within
these boundaries. Theve.is both inclusion and exclusion in establish-
ing and enforcing all boundaries. God in effect puts a “No Tres-
passing” sign around something, and man is required to honor
the stipulations of that sign. If he refuses, God threatens to
impose negative sanctions on him in history and perhaps cven
cternity. God is not mocked at zcro cost.

The Book of the Kingdom

Leviticus is also the book of the kingdom. God delivered
His people from bondage in Egypt, a false kingdom. In doing
so, He gave them an opportunity to gain land for a new king-
dom. The next generation did inherit this land. The generation
of the exodus did not. They died in the wilderness. Because of
their rebellion and lack of faith, their boundary was the wilder-
ness. They could not return to Egypt, nor could they enter the
Promised Land. ‘The kingdom grant of land could be claimed
