The Redemption Price System 611

Old Covenant Isracl’s [ailure to redeem this dedicated land
was God’s means of disinhcriting all of His rebellious Israelite
sons. They could be legally disinherited only as.a family unit;
selective disinheritance by a father was not possible. So long as
any of the family’s land remained in the father’s possession, all
of his sons would have a piece of the inheritance. Disinheri-
tance would not remove them from their tribe. Tribal member-
ship secured their legal status as freemen. Thus, disinheritance
was in this case economic, not judicial. The sons would have no
lawful claim on any portion of the land. In A.D. 70, the self-
disinherited sons of God were evicted by Rome from the tem-
ple. After Bar Kochba’s rebellion of A.D. 133-35, they were
evicted by Rome from the land. The diaspora began.

The idea so prevalent in modern fundamentalism that the
modern State of Israel is in some way biblically entitled to
God's original grant of land to Abraham, which was sccured by
Joshua during the conquest, is inescapably a denial of the au-
thority and binding character of God’s revealed law. The Old
Covenant sons of God forfeited forever their legal title to the
Promised Land and their guaranteed citizenship in the king-
dom of God by their persecution of the New Covenant priests,
the heirs of the dedication: the church. The covenantal heirs of
these disinherited sons can reclaim their citizenship in the
kingdom only as adopted sons, i.e., as members of God’s New
Covenant church. There can never be a repatriation of cither
the Promised Land or the kingdom of Ged to the Jews. Once a
dedicated piece of land passed into the possession of a priest at
the jubilce, there was only one way for it ever to be transferred
back to the original owner. ‘The original owner had to become
a priest, and not merely a priest: the nearest of kin to the priest
who had been given the land. He had to be adopted by that priest.
Only through the death of this adopting kinsman-priest could
the original owner legally regain possession of his former inher-
itance.
