Inheritance Through Separation 349

tion: sanctions that were primarily military in nature. The
Israclites would drive out the Canaanites; if they subsequently
rebelled, other nations would drive them out.

After A.D. 70, the land of Israel lost its special covenantal
status, The Mosaic sacrificial system was cut off.’° In no sensc
~- militarily or environmentally — is land to be regarded today as
a covenantal agent. Under the New Covenant, common grace
and common curses have completely replaced special grace and
special curses with respect to the climate: sunshine and rain,
drought and flooding. God the Father “makcth his sun to rise
on the evil and on the good, and sendcth rain on the just and
on the unjust” (Matt. 5:45). Only to the extent that climate is
directly influenced by man’s science and practices does it mani-
fest covenantally predictable sanctions: blessings and cursings.

10. One result of this was the appearance of a new religion, Judaism.
