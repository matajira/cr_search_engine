446 LEVITICUS: AN ECONOMIC COMMENTARY

fic: knowledge of fuiure decisions by the civil magistrates not to enforce
the terms of the jubilee land law.

Fither party to the transaction could become an oppressor
under this definition. The land owner might persuade the
lessee to agree to a contract in which the lessce promised to
make regular payments until the jubilee year was declared. If
it was not declared, and the magistrates refused to allow him to
escape from the terms of the contract, the lessee would find
himself locked into the contract. Under some economic condi-
tions (e.g., a long-term fail in the money price for agricultural
products), this would defraud: the lessee. On the other hand,
the lessee might be able to get the land owner to accept a cash
payment in advance for legal control over the land’s produc-
tion. If the jubilee land law was not enforced, the lessec would
be able to extend his control over the land indefinitely. This
would defraud the land owner. Conclusion: the State was the
source of the opportunity for oppression.

Both parties were warned to honor the terms of the jubilee
land law whether the civil magistrates did or not. God placed
the primary responsibility for law enforcement on the contract-
ing parties. He warned them both: “Honor the terms of the
leasehold that I have made with Israel for control over My
land.”

‘The issue of economic oppression in this law was not the
actual pricing of the factor of production: land. ‘This decision
was left to the contracting parties. Each looked at the expected
future stream of income from the land. Each would apply the
prevailing market discount of the price of future goods in rela-
tion to present goods to this stream of income: interest. Then
they would decide what to offer each other. The agreed-upon
price, however, had to take into consideration the irrevocable
date for the termination of the contract: the jubilec year.

The existence of a law governing land leases in Israel testi-
fies to the error of interpreting the Bible’s prohibition against
usury in charitable loans as a prohibition against all forms of
