Poverty and Usury 483

ly automatic “entitlement,” to use the terminology of the mod-
ern welfare Staic: a compulsory redistribution of wealth from
the successful to the unsuccessful (minus approximately 50% for
“handling” by government bureaucrats’). It is this element of
covenantal conditionality which distinguishes biblical charity
from humanist compulsion.®

The modern welfare State does not distinguish judicially
between faith and unbelief, or between righteousness and moral
rebellion, as primary factors underlying both wealth and pover-
ty. The Bible’s ethics-based correlation is an implicit denial of
the very foundation of humanism’s welfare State. The welfare
State rests on two rival theories of the origin of wealth and
poverty, held together dialectically in most humanist explana-
tions of economic inequality: 1) the chance distribution of eco-
nomic assets and personal skills; 2) the exploitation of the poor
by the economically and politically successful. The State is seen
as the most powerful agency that possesses a moral and legal
obligation to offset the effects of either chance or exploitation.
‘The welfare State therefore in theory looks only at the num-
bers, not at the moral condition of the recipients of State mon-
ey: their reported income in relation to statute law. Being bu-
reaucratic, the West’s welfare State must by law ignore moral
criteria and respond strictly in terms of formal criteria: so much
income; so many children in the household under age 18,
irrespective of the mother’s marital status; and so forth. The
welfare State is to biblical charity what fornication is to biblical mar-
riage. It literally subsidizes fornication by subsidizing the bas-
tards who are produced by fornication, thereby swelling the
ranks of the government-dependent children of the morally
corrupt.’ This creates lifetime employment for the next gener-

8, James L. Payne, The Culture of Spending: Why Congress Lives Beyond Our Means
(San Francisco: ICS Press, 1991), p. 51.

6. Marvin Olasky, The Tragedy of American Compassion (Westchester, Ilinois:
Crossway, 1992).

7. Charles Murray, “The Coming White Underclass,” Wall Street Journal (Oct. 29,
