11

GLEANING: CHARITY
WITHOUT ENTITLEMENT

And when ye reap the harvest of your land, thou shalt not wholly
veapi the corners of thy field, neither shalt thou gather the gleanings of
thy harvest. And thou shalt not glean thy vineyard, neither shalt thow
gather every grape of thy vineyard; thou shalt leave them fer the poor
and stranger: I am the Lonp your God (Lev. 19:9-10).

I have already covered certain aspects of gleaning in Tools of
Dominion.' The thcocentric principle that undergirds this law is
this: God shows grace to man in history by allowing mankind
access to the fruit of God’s field, His creation. Put another way,
God allows mankind inside the boundaries of His field. Fallen
man is in the position of the poverty-stricken, landless Israelite
or stranger. God does not exclude externally cursed mankind
from access to the means of life in history. Neither were land-
owners in post-conquest Mosaic Israel to exclude the economi-
cally poor and judicially excluded residents of the land. Fallen
man is always a gleaner?

1. Gary North, ‘tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1990), pp. 819-22.

2. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; ‘Iyler, ‘loxas: Institute for Christian Economics, 1994), ch. 11, section on
