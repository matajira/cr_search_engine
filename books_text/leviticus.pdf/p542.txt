486 LEVITICUS: AN ECONOMIC COMMENTARY

— the general welfare — is the social result of the State’s exclu-
sively negative sanctions.

The State is required by God to defend the legal boundaries
that establish private property, not invade these boundaries in
an illegitimate messianic quest to bring positive sanctions to the
poor. The civil magistrate is figuratively to stand inside the
boundaries beside of the property owner to defend him against
any threat of invasion by a non-owner. He is not to stand out-
side the boundaries by the side of the non-owner, threatening
to invade. Defenders of the welfare State reject this view of the
civil magistrate. Becausc so many of these defenders are ortho-
dox theologians and church leaders, Christian social theory
today is either non-existent (baptized humanism) or under-
mined by humanism.

Strangers and Neighbors

This text says that we are to relieve the stranger and the
sojourner, The text in Deuteronomy 23:20 says that we may
lawfully charge strangers interest. How can this apparent con-
tradiction be resolved? Answer: by going to the Hcbrew text. I
have dealt with this issue in Tools of Dominion."| There were
two kinds of foreigners in Israel: 1) permanent resident aliens
who had placed themselves under God’s law (Hebrew words
transliterated toshawb and geyr); 2) traders and other temporary
residents (nokvee). ‘lhe former were to be treated as if they were
Israelites; the latter were not.

The resident alien was to be treated as a neighbor. The
neighbor was a local resident. There is proximity in life. The
neighbor is the person we meet in our daily walk, like the

Ll. North, Tools of Dominion, pp. 708-16. Reprinted with a few modifications in
Gary North, Boundaries and Dominion: The Economics of Leviticus (computer edition;
‘Tyler, Texas: Institute for Christian Economics, 1994), ch. 29, section on “Strangers
and Neighbors.”
