Foreword xiii

ties will increase. With greater knowledge comes greater re-
sponsibility (Luke 12:47-48).

‘his means that the church’s knowledge of the Bible cannot
remain static. Only by sealing off culture from biblical ethics
could the church’s knowledge of the Bible become static. This
is an impossible goal, for the ethics of the world surrounding
an cthically isolated, culturally defensive church eventually
makes inroads into the thinking of its members. It is therefore
an illegitimate goal. Nevertheless, a sealed-off church and a
scaled-olf external culture are the twin cultural goals of pict-
ism.” Pietists seek to place an exegetical boundary around the arena of
Christian responsibility. The smaller this boundary is, the pietist
believes, the better.

What this commentary is designed to show is that the church
as an institution and Christians as individuals have far more
responsibility than Protestant pietistic churches have taught for
over four centuries. More to the point, these responsibilities will
grow over time. But so will God’s grace in history. This is the
meaning of progressive sanctification, both personally and
corporately.

There are a lot of laws in Leviticus. As in the case of my
previous commentaries, I ask two questions of each law that I
consider: 1) How was this law applied in ancient Israel? 2) How
should it be applied today, if at all? A few commentators ask the
first question about a few laws in their selected biblical books.
Hardly anyone since the year 1700 has bothered to ask the
second, let alone answer it clearly.

This Book Called Leviticus

In a humorous book about psychologically afflicted people
who cannot resist buying books, especially used books — I am
one of these people — the author provides a bricf history of

2. J. Gresham Machen, “Christianity and Culture,” Princeton Theological Review,
XI (1913), pp. 4-5.
