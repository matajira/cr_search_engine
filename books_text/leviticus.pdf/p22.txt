Xxil LEVITICUS: AN EGONOMIC GOMMENTARY

behavior. This was an utterly radical break with all religion,
and it alone changed human history. The gods of virtually all
civilizations engaged in sexual activity. The gods of Babylon,
Canaan, Egypt, Greece, and Rome were, in fact, extremely
promiscuous, both with other gods and with mortals.” In the
case of Egypt, he says, homosexuality with a god was the mark
of a man’s lack of fear of that god. One Egyptian coffin text
reads: “Atum has no power over me, for I copulate between his
buttocks.”"* This attitude of defiance — the professed lack of
any fear of God — is basic to all homosexuality.

Gomes us. Moses

‘The New York Times is the most prestigious newspaper in the
United States. It is sometimes referred to as America’s newspa-
per of record. This identification is accurate. It is a thick, politi-
cally liberal,'® rhetorically bland,” well-indexed newspaper.
Its published index may be its primary strength, even more
important than its widespread availability on microfilm. Histori-
ans become dependent on indexes, and the Times has always
provided the best index of any American newspaper. There-
fore, historians quote the Times. Therefore, it has become the
nation’s newspaper of record.

On the page opposite the editorial page — the famous Op-Ed
page — appear essays by famous and not so famous people,
usually liberals. On August 17, 1992, an article by Peter J.

14, id., p.

15. David Greenberg, The Construction of Homosexuality (Cniversity of Chicago
Press, 1988), cited in ibid., p. 64.

16. Herman Dinsmore, All the News That Fits (New Rochelle, New York: Arling-
ton House, 1969). Dinsmore was the editor of the international edition of the Times,
1951-60. This book’s title comes from the words on the masthead of the Times: “All
the News That’s Fic to Print.” The word “liberal” in the United States has meant
“statist” since the late nineteenth century. It meant the opposite prior to the 1890's.
The transition can be dated: the 1896 Presidential carnpaign of Presbyterian funda-
mentalist and populist William Jennings Bryan.

 

17. “The good, gray lady.”
