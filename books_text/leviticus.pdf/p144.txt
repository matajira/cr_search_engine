88 LEVITICUS: AN ECONOMIC COMMEN'TARY

mandatory means of placating Him publicly through formal
repentance.

In the Mosaic Covenant, the sin offerings were the mandato-
ry means.’ They are also known as the purification offerings.
These offerings, more than any other offering in Leviticus —
and perhaps more than any other passage in Scripture — estab-
lished the judicial principle of corporate responsibility. They
raised the issue of hierarchical representation (point two), but
in the context of corporate sanctions (point four). The judicial
issue is oath-breaking.

The purpose of the purification offerings (“sin offerings”:
KJV), the fourth sacrifice, was the restoration of sinful people
to the presence of God after a covenantal oath had been broken
through sin. Without these offerings, the Israclites could not
lawfully cross the boundaries associatcd with God's sanctuary:
local (tabernacle) and regional (nation). The people needed
double protection: from their own sins and from the sins of
their covenantal representatives, the priests and princes. Rulers
had to offer sacrifices for their own sins in order purify the
boundaries in which God resided: the temple-tabernacle and
the nation.

The sins in question were unintentional. “And if the whole
congregation of Isracl sin through ignorance, and the thing be
hid from the eyes of the assembly, and they have done some-
what against any of the commandments of the Lorp concerning
things which should not be done, and are guilty” (Lev. 4:13).
‘The context of these verses is the legal relationship between the
people and a ruler. To speak of going astray within a context of
judicial hierarchy has the implication that someonc in authority
has taken the lead: the biblical shepherd and sheep relation-
ship. It is never said anywhere in the text precisely what these
sins were. Presumably, they were not major, self-conscious sins

1. In the case of Nineveh, fasting and sackcloth were the required means (Jonah

 
