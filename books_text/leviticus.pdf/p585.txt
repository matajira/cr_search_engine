$2

MANDATORY REDEMPTION
UPON PAYMENT

And if @ sojourner or stranger wax rich by thee, and thy brother that
dwelleth by him wax poor, and sell himself unto the stranger or sojourner
by thee, or to the stock of the stranger's family: After that he is sold he
may be redeemed again; one of his brethren may redeem him: Either his
uncle, or his uncle's son, may redeem him, or any that is nigh of hin
unto him of his family may redeem him; or if he be able, he may redeem
himself. And he shall reckon with him that bought him from the year that
he was sold to him unto the year of jubile: and the price of his sale shalt
be according unto the number of years, according to the time of an hired
servant shall it be with him. If there be yet many years behind, according
unto them he shall give again the price of his redemption out of the
money that he was bought for. And if there remain but few years unto
the year of jubile, then he shall count with him, and according unto.his
years shall he give him again the price of his redemption. And as a
yearly hived servant shail he be with him: and the other shall not rule
with rigour over him in thy sight. And if he be not redeemed in these
years, then he shall go out in the year of jubile, both he, and his children
with him. For unio me the children of Israel ave servants; they are my
servants whom [ brought forth out of the land of Egypt: I am the Loxp
your God (Lev. 25:47-55).

‘Lhe theocentric meaning of this passage is that deliverance
out of bondage is an act of God’s grace, The universal redemp-
