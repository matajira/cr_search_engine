598 LEVITICUS: AN EGONOMIG COMMENTARY

giver proved his dedication to God by dedicating the beast to a
priest and then paying a 20 percent transaction fee in order to
redeem it.

Priests and Fields

The jubilee law applied to houses in the 48 cities of the
Levites and to the common land surrounding them (Lev. 25:32-
33; Num. 35:7). These homes could not be permanently alien-
ated from the families of the Levites. “Notwithstanding the
cities of the Levites, and the houses of the cities of their posses-
sion, may the Levites redeem at any time. And if a man pur-
chase of the Levites, then the house that was sold, and the city
of his possession, shall go out in the year of jubile: for the
houscs of the cities of the Levites are their possession among
the children of Israel” (Lev. 25:32-33). The jubilee law of inher-
itance applied to the Levitcs’ homes in Levitical cities and to
rural land in Israel. The Levites could not lawfully be excluded
from their inheritance, but they were excluded from the other
tribes’ inheritance. To maintain their own inheritance, they had
to defend the inheritance of the other tribal families. They had
to preach the jubilee law. God gave them an inheritance in
their cities; this served as an economic incentive for them to
declare the jubilee year.

Priests could not normally own rural land; it was not part of
their inheritance at the time of the conquest of Canaan. When
enforced, the jubilee law madc it impossible for the pricsthood
to extend its political influence into the other tribes apart from
the exposition and application of the Mosaic law. The jubilee law
was designed to keep a centralized ecclesiocracy from being formed. The
jubilee land law was primarily a law of citizenship. It was de-
signed to provide a permanent judicial veto for the tribes. The
tribal system, when reinforced by the jubilee law, decentralized
political power in Israel.

Levites could lease rural properties, however. They could
also reccive rural properties as gifts until the next jubilee year.
