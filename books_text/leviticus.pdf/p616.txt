560 LEVITICUS: AN EGONOMIG COMMENTARY

God calls for population growth because He calls for coven-
antal obedience. He wants to seé positive growth in covenant-
keeping societies. Long-term compound growth is a moral imperative
in God’s covenantal universe. Long-term stagnation is a sign of
God’s curse. Yet there are unquestionably limits to growth. ‘This
is why God's call for population growth points to God’s final
judgment at the end of history and the transformation of man-
kind into a host like the angels: fixed numbers, either in the
lake of fire (Rev. 20:14-15) or in the resurrected New Ileaven
and New Earth (Rev. 21:1-2).

Covenant-breakers who do not wish to think about the final
judgment have become advocates of zero population growth: an
exchange, cither compulsory or voluntary, cither natural or
political, of.a compounding human population for extra eons of
time. The growing acceptance by intelicctuals in the West of the
zcro-population growth movement® and the zero economic
growth movement,’ which became a unificd cause and intellec-
tual fad almost overnight in the mid-1960’s, testifies to the
presence of widespread covenant-breaking and philosophics to

1970); reprinted in North, An Intreduction to Christian Economics (Nutley, New Jersey:
Graig Press, 1973), ch. 8.

6. Lincoln H. and Alice Taylor Day, tho Many Americans (New York: Delta,
[1963] 1965); William and Paul Paddock, Famine — 1975! America's Decision: Who Will
Survive? (Boston: Little, Brown, 1967); Paul Ehrlich, The Population Bomb (New York:
Ballantine, 1968); Gordon Rattray Taylor, The Biological Time Bomb (New York:
World, 1968}; Population and the American Puture, the Report of the Commission on
Population Growth and the American Future (New York: New American Library,
197), Yor an economist’s critique, see Jacqueline Kasun, The War Against Populatio
The Economics and Ideology of World Population Control (San Francisco: Ignatius, 1988).

7, Ezra J. Mishan, The Costs of Economic Growth (New York: Praeger, 1967);
Mishan, The Economic Growth Debate: An Assessment (London: George Allen & Unwin,
1977); Donella Meadows, et al., The Limits to Growth (New York: Universe Books,
1972); E. K Schumacher, Small Is Beautiful: Economics As if People Mattered (New
York: Harper & Row, 1973); Mancur Olson and Hans H. Landsberg (eds.), The No-
Growth Society (New York: Norton, 197%); Leopold Kohr, The Overdeveloped Nations:
The Diseconomies of Scale (New York: Schocken, 1977). For a critique, see F. Calvin
Beisner, Prospects for Growth: A Biblical View of Population, Resources, and the Future
(Westchester, Illinois: Crossway, 1990).

 

 

 
