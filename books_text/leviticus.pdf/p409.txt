INTRODUCTION TO PART 4

And the Lorp spake unto Moses, saying, Speak unte the children of
Israel, and say unto them, Concerning the feasts of the Lorp, which ye
shall proclaim to be holy convocations, even these ave my feasts (Lev.
23-1-2),

And thou shalt speak unto the children of Israel, saying, Whosoever
curseth his God shall bear his sin. And he that blasphemeth the name of
the Lor, he shall surely be put to death, and all the congregation shall
certainly stone him: as well the stranger, as he that is born in the land,
when he blasphemeth the name of the Lorn, shall be put io death (Lev.
24:15-16).

Leviticus 23 and the first section of Levilicus 24 are con-
cerned with corporate religious feasts. The second half of Levit-
icus 24 deals with blasphemy. The judicial link between these
passages is point four of the biblical covenant: sanctions. The
first section deals with covenant renewal through participation
in corporate covenant-renewal celebrations. The second section
deals with individual covenant-breaking by oath.

Leviticus 23 lists the three national covenant-renewal cele-
brations: Passover (vv. 4-8); Firstfruits (vv. 10-21); and Taberna-
cles or booths (vv. 23-44). Verse 22 is seemingly a textual ano-
maly: “And when ye reap the harvest of your land, thou shalt
not make clean riddance of the corners of thy field when thou
reapest, neither shalt thou gather any gleaning of thy harvest:
