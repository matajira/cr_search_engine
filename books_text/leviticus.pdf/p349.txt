18

COVENANTAL FRUIT

And when ye shall come into the land, and shall have planted all
manner of trees for food, then ye shell count the fruit thereof as uncir-
cumeised: three years shall it be as uncircumcised unto you: it shall not
be eaten of. But in the fourth year all the fruit thereof shall be holy to
praise the Lorp withal. And in the fifth year shall ye eat of the frait
thereof, that it may yield unto you the increase thereof: I am the Lorn
your God (Lev, 19:23-25).

When we consider a biblical case law, it is best to begin theo-
centrically. God established this prohibition, so it must have had
something to do with His relation to the land through His
agents, men. The problem that the commentator faces is to
specify three things: 1) what this relationship involved; 2) which
men it applied to, men in general or the Israelites of the Mosaic
covenant; 3) its proper application today. Was it a universal
prohibition, or did it apply only to the Promised Land under
the Mosaic economy?

This is another seed law. The seed laws weve laws of separation.
That is, they placed judicial boundaries around living organ-
isms. We need to determine what this Jaw meant. Because this
statute invokes the language of circumcision, it has to refer
symbolically (i.e., representatively) to the covenantal separation
between circumcised and uncircumcised people. Tribal or fami-
