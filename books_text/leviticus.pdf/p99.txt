Introduction to Part 1 43

guardians of the sacramental boundaries (Num, 18). The family
of Aaron within the tribe of Levi served as the priests, i.e., those
who actually performed the sacrifices. They had legal access to
the inner area of the temple that was closed even to the Levites.
The high priest once a year had access to the holy of holics (Ex.
30:10). ‘hus, the ultimate boundaries in Mosaic Israel were
judicial-spatial.' The temple, the place where the Ark of the
Covenant resided — the royal residence of the God on earth —
was supremely holy, geographically speaking.” Inside the Ark
were the two tables of the law: the Ten Commandments (Deut.
31:9, 26). The holiest place on carth was where the original
records of God’s covenantal law rested. The judicial links among
God’s revealed law, Isracl’s national and cultural boundaries,
holiness, sacrifices, and the priestly family of Levi constitute the
central message of the Book of Leviticus.

What about the economics of Leviticus? We begin with this
observation: based on God’s ownership both of the land (Lev.
25:23) and the Israelites (Lev. 20:26), He established a unique
sct of property rights over Israel and inside Israel. As is true in
all cases of property rights, these rights were marked by a series
of legal boundaries. Leviticus, the third book in the Pentateuch,
is most closely associated with these boundaries.*

The Five Year-Round Sacrifices

There are five year-round sacrifices in Leviticus. As with the
three mandated festival-feasts (Lev. 23), all five sacrifices had to
be offered to God at a central location. To get to this central
location, most of the Israelites had to walk.

1. These ended forever with the fall of Jerusalem in A.D. 70.

2. Meredith G. Kline, Images of the Spirit (Grand Rapids, Michigan: Baker, 1980),
pp. 39-42.

3. ‘Vhe third commandmentestablishes a boundary around God’s name: “Thou
shalt not take the name of the Lorp thy God in vain” (Ex. 20:7a). The cighth com-
mandiment (the third in the second table of the law) establishes property rights:
“Thou shalt not steal” (Ex, 20:15).
