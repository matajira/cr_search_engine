496 LEVITICUS: AN ECONOMIC COMMENTARY

idual’s circumcision, not his right of mobility. In contrast, the
definition of “sojourner” and “hired servant” applicable to
Leviticus 25:40 is based on the existence of a household bound-
ary. The sojourncr and the hired servant could legally leave the
jurisdiction of the household at the end of their voluntary,
contractual service. The bondservant could not. The jubilee law
did not require the Israclite to treat his impoverished brother
as an uncircumcised person; it therefore must have required
the owner to treat his fellow Israelite as well.as he would treat
a geographically mobile person. The poor Israelite was to be
protccted. He was defined judicially as a man who had tempo-
rarily lost possession of his landed inheritance. Economically, he
was poor, but this was not a judicially binding definition.’

To Buy a Brother

This passage governs the treatment of an Israelite who had
been sold to another Israelite. [le had to serve the purchaser
until the jubilee unless his kinsman-redeemer bought him out
of bondage. This means that he was not under the protection of
the sabbatical year of release (Deut. 15), What is the judicial
distinction between the two conditions of household servitude?
‘The Bible is not explicit, but the difference appears to relate to
lawful immediate access to rural land. The poor man in Deuter-
onomy 15 was to be scnt away with sheep, grain, and wine
(Deut. 15:13-14). This indicates that he had a home to return
to. The poor man in Leviticus 25 was.to be sent back to his
Jand only with his family. Nothing is said of his buyer’s respon-
sibility to provide him with any economic resources (Lev.
25:41). His judicial status as a free man was his primary re-
source; his landed inheritance was his economic resource; and
his family went free with him. This distinguished him from
both the poor man who had defaulted on a zero-interest, mor-

2. North, Boundaries and Dominion, ch. 30, section on ‘Who Were the Poor in
Israel?”
