Inheritance by Fire 327

tion service comparable to circumcision? Second, why did the
ritual offering of.a child defile the sanctuary of God? Why was
this a uniquely profane act? Third, why was this forbidden to
resident aliens? The specified negative sanction, death by ston-
ing, if ignored by the judges, would be followed by God’s inter-
vention against that family and all those who joined with that
family. ‘The law here says nothing about a threat to the Israelite
community at large. Why, then, should resident aliens be pro-
hibited from performing such a rite? Fourth, was this law a law
governing false worship in general, or was it confined to Mol-
ech worship only? Fifth, does it still apply in New Covenant
limes? Was it a cross-boundary law? Let us consider cach of
these questions in greater detail, one by one?

1. Rites of Dedication

There is no question that some sori of cultic rite was in-
volved in this crime. It was a formal, covenantal transgression
of the first commandment: “Thou shalt have no other gods
before me” (Ex. 20:3). The legal question is: Did this act be-
come a crime only when committed outside of a household?
No; it was a crime for an Israelite no matter where it took
place. False worship within an Israelite household was a capital
crime in Mosaic Israel (Deut. 13:6-11).

Molech’s required ritual was a perverse imitation of Jcho-
vah’s require rite of circumcision. Instead of physically marring
the organ of generation as a symbol of physical death but also
covenantal life, the child was actually passed through a literal
fire. The child who survived this ordeal was therefore assumed
to be blessed covenantally by Molech. Ie had passed the deadly
initiation rite by means of supernatural intervention. ‘This ritual
was covenantal, but rather than being ethical in its focus, it was

 

3. A far more detailed discussion appears in Gary North, Boundaries and Domin-
ion: The Economics of Leviticus (computer edition; ‘Iyler, Texas: Institute for Christian
Economics, 1994), ch, 20, sections 1-V.
