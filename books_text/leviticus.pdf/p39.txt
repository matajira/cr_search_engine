Foreword XXXiX

boundaries — this commentary can be read cover to cover, but
most readers will probably confine themselves to specific chap-
ters.

This commentary is aimed at economists, who in my day are
unlikely to pay any attention. It is aimed at pastors, who rarely
read long books, especially on economics. Most of all, it is
aimed at intellectually serious Bible students who have not yet
decided what their callings in life should be.*” I hope this
book will give them a larger picture of what full-time Christian
service really is. They, too, can devote their lives to discovering
what God requires from His people, and then try to persuade
Christians to believe a word of it — a seemingly foolish task,
indecd, if there were not covenantal sanctions in history, But there
are: positive and negative. The positive sanctions are wonder-
ful, but seeking to avoid the negative sanctions is imperative.

I know, I know: that’s just my opinion.

40. They may have occupations. These jobs are rarely their callings. I define a
person's calling as follows: “he most important lifetime service that he can render
to Ged in which he would be most difficult to replace.”
