532 LEVITICUS: AN ECONOMIC COMMENTARY

covenant-keepers in the techniques of wealth accumulation.
This education was a positive sanction of bondage.

Consumer Sovereignty

Nothing is said in this passage that would have prohibited
another Israelite from buying the poor man. What is affirmed
is that the resident alien could also enter the market. He was
authorized by God’s law to become a competitive bidder in the
market’s auction for the poor Israelite’s labor services. This
raised the market price of these services. Why did God allow
this? First, in order to allocate scarce labor services according to
the demand of consumers. Second, in order to enable the poor
Israelite to become a more efficient economic agent of consum-
ers. He had to become the subordinate agent of a covenant-
keeper — a rich one. He would have to hew wood and draw
water in a covenant-breaker’s household until the day of his
redemption. He would learn from the most aggressive bidder
in the local market.

The covenant-breaker, acting as the economic agent of con-
sumers, was allowed to purchase the capitalized labor services
of covenant-keepers in order to meet the demands of consum-
ers. The scarce economic resource of labor would then be chan-
neled into goods and services that were demanded by consum-
ers. What this means is that preserving consumer sovereignty in
israel was more fundamental in God’s law than preserving
freeman legal status of bankrupt Israelites, at least until re-
demption took place or the jubilce’s trumpet sounded. In this
case; that which served consumers most efficiently was autho-
rized by God’s law. A bankrupt Israclite’s legal status as a frec-
man was not to defended, [ree of charge, al the expense of the
consumer.

The kinsman-redeemer could lawfully buy back the servant's
legal status as a freeman, but this involved a risk on his part.
He would probably have had to take over the care of the man
and his family, for they had no land to return to. Freemanship
