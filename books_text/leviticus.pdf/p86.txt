30 LEVILIGUS: AN ECONOMIG COMMENTARY

Tberian-based banks all across North America in the time of
Jesus. These contacts continued, and they left traces. “In 1933,
an astonished Mexican archcologist excavated a terra-cotla head
of a Roman figurine of the third century A.D. from an undis-
turbed ancient grave sealed under the Calixtlahuaca pyramid,
thirty-five miles southwest of Mexico City.”**

The Carthaginians and Romans were late-comers. The Scan-
dinavians were trading in North America during the Bronze
Age, possibly as early as 1700 B.C.” — the era of Joseph in
Egypt. A visiting Norwegian sailor-king left an account of one
of these visits in what is now called Petroglyph Park in Peterbo-
rough, Ontario, in Canada. He had an inscription chiseled into
rock, written in a nearly universal alphabet of the ancient
world, ogam consaine,* and another alphabet, equally univer-
sal, Tifinag, an alphabet still employed by the ‘Tuaregs, a Berber
tribe in North Africa. The Norse inscription was accompanied
by a comment written by an Algonquin Indian scribe in a script
common among the pre-Roman Basques, but using a form of
the Algonquin language still understood.”® The inscription was
discovered in 1954,

‘This same Basque script was also employed by the Cree
Indians well into the nineteenth century. It was not known to
be related to Basque until Fell transliterated into Latin conso-
nants a document written in this “Indian” script. The document
had been sent to him by a Basque etymologist who had been
unable to decipher it. When it was transliterated, the Basque
scholar recognized it as a pre-Roman dialect of the Basque

56, Huyghe, Columbus Was Last, p. 98.

57. Kell, Bronze Age America, ch. 1. The dating is calculated by the zodiac data in
the inscription: ch. 5, especially pp. 127, 130.

58. A gift to man from the Gaulish god Ogimos, god of the occult sciences. Ibid.,
p. 165.

59, Ibid., p. 36. For additional information, see Huyghe, Columbus Was Last, ch.
5.

60. Fbid., p. 39.
