204 LEYITICUS: AN EGONOMIG COMMENTARY

The land was God’s covenantal agent. This law was agricul-
tural only. It did not apply to urban businesses.”*

Conditional Charity: Moral Boundaries

The owner of the farm had to acknowledge the sovereignty
of God by obeying the gleaning laws. These laws were a re-
minder to him that éblical authority always has costs attached to it.
‘The owner of the land had been given capital that other people
lack. He therefore had an obligation to the local poor as God’s
agent, for the land itself was pictured as God’s agent. His obli-
gation was to supply the land’s leftovers to the poor.

In making this demand, the gleaning law placed decisive
limits (boundaries) on both the poor rural resident and the
State. It limited the moral demands that the poor could make
on cconomically successful people in the community. The poor
had no comparable moral claim against the successful non-
agricultural businessman. This law also limited the demands
that the State could make on the community in the name of the
poor. Biblical law specified that the man with landed wealth
should share his wealth with the deserving poor, but not the
poor in general. ‘The deserving poor were those who were
willing to work hard, but who could not find work in the nor-
mal labor markets. In short, the gleaning law had conditions attached
to it. he idea of morally compulsory, non-conditional charity
was foreign to the laws of the Mosaic Covenant.”

12. North, Boundaries and Dominion, ch. 11, section on “A Law of the Land, Not
the Workshop.”

13. It is equally foreign to the law of the New Covenant, This assertion appalls
Timothy Keller. See Keller, “Thconomy and the Poor: Some Reflections,” in William
S. Barker and W, Robert Godfrey (cds.), fheonamy: A Reformed Critique (Grand
Rapids, Michigan: Zondervan, 1990), pp. 273-79, He calls for initially unconditional
charity to all poor people. He argues that anyone in need anywhere on earth is my
neighbor, thereby universalizing the moral claims of ail poor people on the wealth of
anyone who is slightly less poor. He writes: “Anyone in need is my neighbor — that is
the teaching of the Good Samaritan parable.” /bid., p. 275. He rejects the traditional
Christian concept of the deserving poor (pp. 276-77). He concludes: “T am proposing

 

 
