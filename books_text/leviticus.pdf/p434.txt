 

378 LEVITICUS: AN ECONOMIC COMMENTARY

but they cannot say precisely which sanctions will be imposed
and when. But to deny that God’s sanctions are in any way
predictable in history in terms of Eis law is to deny the pro-
phetic function altogether. If this theology were true, the gospel
would lose its status as prophetic. Its proclamation would be-
come a matter of personal opinion in history regarding sanc-
tions in eternity. The covenantal link between history and eter-
nity would be broken because the covenantal link between
historical and eternal sanctions is broken, To a great extent,
this is the gospel of modern evangelicalism. Denying God’s law
and especially its mandated civil sanctions, and denying also
God's predictable corporate sanctions in history, modern evan-
gelicalism has stripped the gospel of its prophetic character.
Thus, a society that has disallowed the Bible’s blasphemy law
is not in a position to defend itself against the extension of the
society of Satan: the triumph, judicially and culturally, of Sat-
an’s representatives in history. If God’s name is not worth
defending through civil law, neither is anyone clse’s. It should
come as no surprise that we have seen the virtual annulment of
civil Jaws in the United States that once protected public figures
from libel.” It is not enough for a victim to prove that some-
one has lied about him; he must also prove malice on the part
of the liar.”® his erosion of protection has escalated in the
final third of the twentieth century, paralleling the systematic
removal of God’s name from all public institutions and space in
the U.S. We have also scen the rise of gossip as a major political
force. This has tarnished the reputations of politically liberal

 

14, New York Times Co, v, Sullivan (1964). Ci. The Constitution of the United States of
America: Analysis and Interpretation, prepared by the Congressional Research Service,
Library of Gongress (Washington, D.C.: Government Pyinting Office, 1972), pp.
1003-7. The case was handed down by the U.S. Supreme Court two years after Lngel
», Vitale, in which prayer in public schools was outlawed by the Gourt, In 1968 came
Abington School District u. Schempp, in which Bible reading in public schools was out-
lawed. Cf. Constitution, pp. 920-22.

15. Avery fine movie that examines the crisis produced by the anaulmentof the
Vibel laws is Absence of Matice (1981).
