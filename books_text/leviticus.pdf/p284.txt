228 LEVITICUS: AN RGONOMIC COMMENTARY

Worker vs. Worker

In a poor nation, which the whole world was until the nine-
teenth century, an offer by a worker to accept delayed payment
would have given this capital-owning worker a competitive
advantage over destitute workers who needed payment immedi-
ately. This law establishes that competition among workers must
not involve the employer's acceptance of such an offer by any
worker. The biblical standard of payment is specified: payment
at the end of the day. There may lawfully be payment in ad-
vance by the employer but not delayed payment, unless there
is a legally separate interest-paying savings plan involved, as
mentioned earlier.

Where this law is enforced, destitute workers in the commu-
nity are not replaced in the labor force by less destitute workers
who can afford to forego immediate payment. All workers are
to be allowed to compete for jobs, irrespective of any worker’s
possession of reserves sullicient to tide him over until the next
payday. So, one idea behind this law is to make job opportuni-
ties available to the destitute workers in the community. Every-
one who is physically able to work is to be allowed to compete
for a job on a basis independent of his asset reserves. The desti-
tate man’s poverty is nol to become the basis of his exclusion from the
labor market. His competitors are not allowed to use their ability
lo extend credit to an employer as a way to offset his only
assets: his willingness and ability to work.

Weaker Parties

The worker needs protection. An employer might hire him
for a period and then dismiss him without pay. Jacob’s com-
plaint against Laban was that Laban had changed his wages
repeatedly, meaning retroactively (Gen. 31:7). To protect the
worker from this sort of robbery, the Bible requires the em-
ployer to bear the risk of longer-term default on the part of a
worker. The employer bears the risk that the worker may turn
