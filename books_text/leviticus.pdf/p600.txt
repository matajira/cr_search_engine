544 LEVITICUS: AN EGONOMIG COMMENTARY

Sanctions and Representation

The blessings listed here are agricultural and social: bread,
wine, and peace. These are positive sanctions.’ Ten righteous
representatives of Sodom would have kept God from bringing
total negative sanctions against that city, but only because of
Abraham’s negotiation with God (Gen. 18:24-32). But what
about positive sanctions in Isracl? What had to be done ‘in
Israel in order to gain bread, wine, and peace? ‘lhe people as
a covenantal unit were told to obey God. The Bible never men-
tions a specific percentage of the population that must obey
God in order for God's positive, visible sanctions to become
predictable in history. This is why the absolute predictability of
God's sanctions in history is an unobtainable ideal. But absolute
anything in history is unobtainable by men, ‘so this should not
deter us in our quest to gain His positive sanctions. What the
Bible teaches is that the number of active covenant-keepers
must be large enough to represent the nation judicially. The
society must bé marked by widespread obedience to the civil
jaws set forth by God. Blessings apart from faithfulness are a
prelude to negative sanctions on a comparable scale.

Covenantal Representation

God promised covenantal blessings to the residents of. the
nation of Israel in response to individuals’ covenantal obedi-
ence. Obedience is always in part individual, for individuals are
always held responsible by God for their actions. This responsi-
bility is inescapable in history and at the day of final judg-
ment.> Nevertheless, there is no doubt that God’s promised
historical responses to individual obedience were corporate

4. Peace might be considered the absence of war, but given the condition of
mankind after Adam’s rebellion, it takes God’s active grace to bring peace io man.
Peace is not normal even though it is normative. Peace is not passive. War and sin
are the passive condition of covenant-breaking man (James 4:1).

5. The law’s visible sanctions are more predictable at the final judgment.
