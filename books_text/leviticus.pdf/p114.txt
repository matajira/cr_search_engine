 

58 LEVITIGUS: AN ECONOMIC COMMENTARY

the work of his hands will placate God. He exhibits this faith in
two ways. First, he seeks to offer a public sacrifices of reduced
value, Gain’s offer of agricultural produce rather than a slain
animal is representative of man’s search for an alternate sacri-
fice. [le proclaimed ritually that he believed that his blood
(life) was not on the line. Second, man repeatedly places
himself under the covenantal jurisdiction of false divinities that
time and again claim total sacrifice. ‘This is why the quest for
autonomy from the God of the Bible has led politically to the diviniza-
tion of the State, no matter how strong the technical case against
the omnicompetent State may be. Autonomous man returns to
the theology of the messianic State like a dog to its vomit. That
which can command unlimited sacrifice is seen as the savior of
man and society.

Covenant-breaking man is schizophrenic. He seeks a divinity
in history powerful enough to bless the works of his hands, yet
not so powerful as to constituic a threat to his autonomy. ‘This
is why, whenever and wherever God’s required public sacri-
fices*! are either ignored or denied by society, we can expect
to sce increasingly successful attacks on the legitimacy of private
property. Put differently, whenever and wherever the limits
(boundaries) placed by God on man’s required sacrifices are
ignored, we can expect to see the State substitute itself as a new
god which in principle requires unlimited sacrifice. Whatever
property that the State allows men to retain under their per-
sonal contro] will be understood as duc to the present grace of
the State, or due to the State’s present political inability to
confiscate everything, or due to the State’s present perception

20. Blood and life arc linked biblically: “For the life of the Hesh is in the blood:
and I have given it to you upon the altar to make an atonement for your souls: for
it is the blood that maketh an atonement for the sou!” (Lev. 17:11). “For it is the fe
of all flesh; the blood of it is for the life thereof: therefore 1 said unto the children of
Israel, Ye shall eat the blood of no manner of flesh: for the life of all flesh is the
blood thereof: whosoever eateth it shall be cut off” (Lev. 17:14).

Q1. The Lord's Supper is public. It is not mandated by the State; it is mandated
by God.
