590 LEVITICUS: AN ECONOMIC COMMENTARY

God cstablished an entry fee higher for aged people — age
60 and over (v. 7) — than for very young children: under age 5
(v. 6). Why? Because old people tend to be more set in their
ways, more used to deference from younger people, even
priests. They would be more trouble to govern than very young
children. The very young child would grow up in the presence
of the Levites and the priests. He would learn to respect au-
thority. He would not be a major threat to the ecclesiastical
hierarchy. ‘Where was Icss need for a monetary barrier to his
entry into the houschold of the church.

God established lower prices for old men than for male
children ages 5-19 (v. 5). The prices for females, young and
old, were the same: 10 shekels. Why? The issuc was authority:
males had more authority than females did. Children of this
age group reflected their parents’ attitudes. The boys would
have been more difficult to control than aged men. Young girls
and old women were judged of equal difficulty.

So, the discrepancies in these dowry prices can be explained
in terms of expected resistance to ecclesiastical authority. But what
about the lower price for females in each age group? This is
also consistent with the hypothesis that this law was imposed by
God in order to reduce the Levite adoptces’ resistance to eccle-
siastical authority. Israelite women were accustomed to obey
male heads of houschold. They were more likely to respect
hicrarchical authority. Thus, they were less of a threat to the
established ecclesiastical order. The payment could be smaller
because the need to establish a barrier to entry was less.

Sonship Is Judicial

It was an honor to be a member of the tribe of Levi. This
tribe guarded the law of the covenant, a guardianship symbol-
ized by the two tablets of the law inside the Ark of the Cove-
nant (Deut. 31:26). The priests were in charge of guarding the
Ark. That is, the pricsts policed the boundaries between the
Ark and the world outside.
