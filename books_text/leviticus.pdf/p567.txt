Promise, Adoption, and Liberty 511

versal adoption would have made the liberator very unpopular,
as we can easily imagine, but it was always a legal option under
the Mosaic covenant. The most likely candidate to do this was
a man with abolitionist sentiments and without biological heirs.

Would he have owed the slave owners anything? Only for
the time remaining until the jubilee. This prorated payment
would have become progressively smaller as the jubilee year
approached. In the year of jubilee, he would have owed them
nothing. There was only one exception to this rule: the criminal
who had been sold into slavery to pay his victim. In this case,
his owner had to be repaid fully before the slave could be re-
leased. The buyer had paid a price based on the amount of
restitution the criminal owed to the victim, not the prorated
value of his services until the jubilee. The criminal was not
protected by the jubilee. God’s law does not subsidize crime. So,
in order for the redemption to be secured through adoption,
the adopting redeemer would have had to pay to the owner
whatever the owner had paid to the criminal’s victim.

It is understandable why Israel may never have invoked the
jubilee, Lad it been honored, almost every slave owner’s investment
would have been at risk. All it would have taken to free all the
gentile slaves in Israel was for one lawful heir to decide that the
per capita economic value of his children’s landed inheritance
was worth forfeiting for the sake of a single mass adoption: the
ultimate abolitionist.

The Ultimate Adoption

There was such a man. His name was Jesus. He publicly
declared the judicial intent of His ministry by announcing the
availability of liberation through adoption into His family (Luke
4:18-21). The result was predictable: the slave-owners and their
accomplices killed Ilim. With the death of the ‘Testator came
the inheritance: judicial liberation." But because of the jubilee

lL. “For where a testament is, there must also of necessity be the death of the
