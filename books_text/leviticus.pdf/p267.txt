Verbal Bonds and Economic Coordination 211

In the Bible, a covenantal bond establishes a formal legal rela-
tionship under God: personal, ecclesiastical, familial, or civil.
While a promise does not possess the judicial authority of a
covenantal bond, since it lacks a lawfully imposed self-maledic-
tory oath, a promise is nevertheless analogous to a covenant
bond. It is a verbal contract.

In modern finance, a bond is a promisc to pay. A person
gives up money in the present in exchange for a specified
stream of money in the future, with the return of the principal
at a stated date, which will complete the transaction, thus end-
ing the legal relationship.

Promiscs are a form of inventory. In this sense, they are a
form of capital. They serve as substitutes for physically stored
assets. Instead of accumulating physical assets for future use, a
producer relies on another person to deliver the goods, literally
or figuratively.® A broken promise in such circumstances is the
cconomic equivalent of an empty storage facility that was
thought to be full. Worse; someone had guaranteed that it
would be full. The missing good or service creates a kind of
falling domino effect: delayed production all down the line.
‘The person who Jails to deliver on time produces losses for the
person who became dependent on him.

The person who promises to deliver goods or services puts
his reputation on the linc. The better his reputation, the more
business he will generate, other things (such as price) being
equal. It pays a person to gain a reputation as one who keeps
his word. He tells the truth, and when other people plan their
actions in terms of what he says, they are not disappointed (Ps.
15:4). This person lowers the cost of doing business with him.
By lowering the price of anything, the sclicr increases the quan-

6, In the 1980's, the advent of inexpensive computers and the spread of over-
night ait cargo delivery companies made possible the development of “just in time”
manufacturing. Manufacturers can time the delivery of raw materials and parts so
that they do not have to invest in large inventories. This has made manufaciuring far
more efficient.
