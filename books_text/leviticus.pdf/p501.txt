Economic Oppression by Means of the State 445

God warned everyone to abide by the jubilee law even if the
civil rulers did not enforce it.

If 1 am correct in my analysis of this passage, then we have
additional evidence that economic oppression in a free market is
usually the result of civil magistrates who refuse to enforce God’s re-
vealed law. It is rarely the process of voluntary pricing in a free
market that constitutes economic oppression; it is rather pricing
in a society in which civil magistrates favor a particular individ-
ual or class by means of economically discriminatory legislation
or economically discriminatory court decisions. State subsidies of
all kinds enable people to oppress each other economically. The incen-
live to oppress others in this way is universal; the ability to do
so is very limited when the civil magistrates restrict their actions
to enforcing the laws of God by imposing the sanctions speci-
fied by His law. The State initiates economic oppression by
creating the legal conditions in which such oppression is
profitable. In short, the Siate subsidizes economic oppression. As in
the case of any State subsidy, this increases the supply of the
item being subsidized: economic oppression.

Contrary to utopian Christian non-economists, oppression
has nothing to do with rental income or interest income, which
tend toward an equal rate of return in a free market economy.
The jubilec law authorized long-term lease contracts, which are
a form of rent; it therefore by implication also authorized inter-

est paymenis.”!

Conclusion

The jubilee land law prohibited oppression in the writing of
land lease contracts, Oppression resulted when one of the two
parties to the transaction used specialized knowledge to take
advantage of the other. ‘he kind of knowledge was quite speci-

21. North, Bourdaries and Dominion, ch. 26, section on “The Legitimacy of Reat
and Interest.” Cf. North, Thols of Dominion, Appendix. C: “Lots of Free Time: The
Existentialist Utopia of 8. C. Mooney.”
