Foreword xvii

by a handful of New England scholars in the United States in
the first half of the 1800's. But interest in higher criticism faded
in New England during the Civil War.’ ‘Then, independent of
the moribund New England critical tradition, it revived in the
mid-1870’s and spread rapidly among German-educated Ameri-
can theologians.® ‘Yoday, most of the very few Bible scholars
who pay attention to Leviticus adhere to the interpretive princi-
ples of higher criticism. They assume that the Old ‘Testament is
the product of several centuries of highly successful forgers.® I
do not.

But the question remains: How should we interpret this
difficull book? Are there principles of interpretation — a herme-
neutic — that enable us to understand it correctly and apply it
to our social problems?

Five Examples

There is more to the texts of Leviticus than meets the eye on
first reading, or even second reading. ‘Ihe size of this book
indicates just how much more. Let me offer the five Levitical
sacrifices as examples. [ere are a few one-sentence conclusions
stemming from the five Levitical sacrifices.

Whole Burnt Offering. There are limits on man’s sacrifice, yet a
perfect sacrifice is required. There is no autonomy of possessions. God
imposed an economic loss: a sacrifice. God’s mercy requires sacrifice on
the part of the recipient. ‘There is a hicrarchy of debt in life: I owe

7. Jerry Wayne Brown, The Rise of Biblical Criticism in America, 1800-1870: The
New Fngland Scholars (Middletown, Connecticut: Wesleyan University Press, 1969), p.
8.

8. Thomas II. Olbricht, “Rhetoric in the Higher Criticism Controversy,” in Paul
H. Boase (ed.), The Rhetoric of Protest and Reform, 1878-1898 (Athens, Ohio; Ohio
University Press, 1980}, p. 285; Walter F, Petcrsen, “American Protestantism and the
Higher Criticism,” Wisconsin Academy of Sciences, Arts and Letters, L (1961), p. 321.

9. See Cary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), Appendix J: “Gon-
spiracy, Forgery, and Higher Criticism.”

 
