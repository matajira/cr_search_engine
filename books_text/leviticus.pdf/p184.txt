128 LEVITIGUS: AN ECONOMIC COMMENTARY

enantally speaking. History is then seen as linear but not pro-
gressive.

The biblically valid distinction between the sacred and the
non-sacramental reminds us ‘that all of nature is under grace,
either special or common. Without the unearned gifts (grace) of
life, law, time, and knowledge, and power, there could be no
history.’ The processes of nature have been definitively re-
deemed by Jesus Christ by His death, resurrection, and ascen-
sion.” This is equally true of culture. The Bible is clear: na-
ture is sustained by God’s common grace and is progressively
sanctified in history in response to Ilis extension of special
grace to the church, Grace progressively redeems nature in history
because Jesus Christ definitively redeemed nature at Calvary. Nature
is thercfore sanctificd: definitively, progressively, and finally.

What Constituted “Ignorant Profanity”?

A profane act involves the misuse of God’s name. It is pri-
marily a priestly misrepresentation of God. Profanity is worse
when committed by an ordained special priest (guardian of the
sacraments) than by a non-ordained special priest (guardian of
the kingdom), i.e., a redeemed person. It can also be commit-
ted by a general priest, i.e., an unredeemed person under
Adam's original priestly status.’* Thesc profane acts are public
acts.

Leviticus 5:14-19 referred to a profane act committed in
ignorance. A profane act under the Old Covenant necessarily
involved the church, for it involved some aspect of the sacra-
ments, i.e., the priesthood. To violate the office of priest, cither

15. This is Meredith G. Kline’s view of New Covenant history: Kline, “Comments
on an Old-New Exror,” Hesiminster Theological Journal, XLJ (Fall 978), p. 184.

16, North, Dominion and Common Grace.

17. North, is the World Running Bown?

18, North, Boundaries and Dominion, ch. 6, sections on “Profanity, Priesthoods,
and Pagans” and “New Covenant Sanctions.”
