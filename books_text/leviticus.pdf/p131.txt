Leaven and Progressive Sanctification 75

surrender.’ The question is: Must man surrender uncondition-
ally to God, or must God surrender unconditionally to man?
The irreconcilable conflicting answers to this question constitute
the essence of the war between Christianity and humanism.

The Mosaic sacrificial system testified to the possibility of
peace. The peace offering was the third of the five Levitical
sacrifices. It corresponded to point three of the biblical cove-
nant model because it dealt with boundaries: the boundary
separating God from man. In Mosaic Israel, this boundary
principle applicd above all to the temple-tabernacle.

The goal of this sacrifice was peace with God: a goal for all
seasons, When an Israelite sought to establish special peace with
God, he brought a sacrificial animal to the priest. This offering
had to be blemish-free, as was the case in the other offerings.
The blemish-free animal was the mark of the best a man could
offer God. As we shall see, this is also why leaven had to accom-
pany the peace offering. But the offering had to include un-
leavened bread as well. ‘This mixture of leaven and unleaven
creates a problem for the commentator. What did each of these
offerings symbolize? ‘They seem contradictory, yet both were
required in the same offering. Why? To answer, we need to
understand the special nature of this sacrifice.

The Peace Offering

The peace offering was not tied to a vow or an oath. We
know this because the Israelite was not allowed to eat this sacri-
fice over a period of two days (Lev. 7:15), unlike a votive (vow)
offering, which could lawfully be eaten the second day (Lev.
7:16). An unclean person who ate the peace ollering had to be
excommunicated: cut off from the people (Lev. 7:20). This
sacrifice, more than the other four, involved boundaries (point
three): lawful and unlawful crossing into God’s presence.

1. Gary North, Unconditional Surrender: God's Program for Victory 3rd ed.; Tyler,
Texas: Institute for Christian Economics, 1988).
