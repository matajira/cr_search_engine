454 LEVITICUS: AN ECONOMIC COMMENTARY

hired harvesters, strangers, and gleaners. It is clear from the
message of Jeremiah that the nation did not honor the sabbati-
cal years for 70 sabbatical cycles, or 490 years. This is why they
were sent into captivity. “To fulfil the word of the Lorp by the
mouth of Jeremiah, until the land had enjoyed her sabbaths:
for as long as she lay desolate she kept sabbath, to fulfil three-
score and ten years” (II Chron. 36:21).

‘There is no unambiguous biblical record that the jubilee law
was ever honored in Israel. We know that the sabbath year of
release was not honored for 490 years prior to the exile. Since
they did not honor the sabbath year of release, it is highly
doubtful that God ever gave them the promised triple crop in
the seventh sabbatical cycle. Without the triple crop, perhaps
they chose to ignore the jubilee law. ‘he Bible does not say.

Miracles, Sanctions, and Mysticism

When Jesus announced His fulfillment of the jubilee year
(Luke 4:18-21), He was announcing the end of the miraculous
jubilee year. Under the New Covenant, there is no triple crop
in the sixth year of the seventh “weck of ycars.” The faith of
New Covenant-kcepers has been stripped of a national miracle
that demonstrated the reliability of God’s providential and law-
bounded covenantal order, just as a similar faith during the
wilderness era was stripped of a daily miracle when the manna
ceased upon the nation’s entry into Canaan. As the spiritual
maturity of covenant-keepers advances, miracles steadily cease.*

The question arises: What about the covenantal cause-and-
effect connection between corporate external obedience and
corporate blessings? Arc covenant-bound socicties still promised
peace and agricultural prosperity if they adhere to the externa!

5. This has been the case in the history of Christianity. In the early church, the
miracles of healing and exorcism were importantin evangelism. Today, both of these
gifts are far less evident in advanced industrial nations, although both still are used
by some fundamentalist missionaries working in primitive societies or in societies
deeply in bondage to a rival supernatural religion.
