112 LEVITIC

 

AN ECONOMIC GOMMENTARY

collect 10 ounces, leaving the tithe-payer with 90 ounces of gold
afier taxes, He then pays 9 ounces to the church. In both ex-
amples, he retains 81 ounces. In the first example, the church
collects 10 ounces and the State collects 9; in the second exam-
ple, it is the reverse. ‘The first example is closer to God’s stan-
dards than the second.

Sharecropping

We can understand this better if we think of the pre-twenti-
eth-century agricultural practice of sharecropping. Land owners
owned land and capital. (Capital is the product of land and
labor over timc).° After slavery was abolished, they no longer
owned pcople. Instead, they hired people.”* Rather than pay-
ing them wages, land owners leased to sharecroppers tools and
land. Owners concluded that it was less expensive to monitor
economic results — a local crop ~ than it was to monitor the
productivity of their employees’ labor inputs to the production
process, requiring them to specify a wage for these labor in-
puts. What maticred to-land owners was results, not labor
inputs. (“Activity is no substitute for production.”)

Hourly wages are based on the average productivity of a
particular class of workers. An above-average producer in any
given class is usually much better off to become a sharccropper,
a piece-rate worker, or a commissioned salesman. Ile is paid in
terms of his measurable net productivity, not in terms of his

9. Murray N. Rothbard, Man, Economy, and State: A Treatise on Economic Principles
(Auburn, Alabama: Mises Institute, [1962] 1993), pp. 285-88.

10. Tn the American South, 1865-80, sharecropping became a way of life for ex
slaves and ex-slave owners. It was a cost-effective system for a defeated post-war
society with minimal financial capital. Roger Ransom and Richard Sutch, One Kind of
Freedom: The economic consequences of emancipation (New York: Cambridge University
Press, 1977)

10, ‘The cost of monitoring people’s behavior is fundamentalin the evolution of
economic and political institutions. Thomas Sowell, Knowledge and Decisions (New
York: Basic Books, 1980), pp. 55-56, 65-66, 111-12, 215-26. See Sowell’s index for
more entries: “Monitoring.”
