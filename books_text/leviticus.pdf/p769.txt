divine rule, xliv, 95-96
judicial order, 250-51,
262
local, 250, 262
pluralism, xliv (also see
pluralism)
stoning &, 336n
polygamy, 280
polytheism, 102, 274-75
poor
bureaucracy &, 208
court, 140
deserving, 114, 196, 204
entitlements, 208
gleancrs, 196, 199-200,
203-5
God &, 240-41, 243-44
grace to, 198
legal definition, 489-90
limits on, 204
local, 201-3
oppression, 229
rural subsidy, 202
theology, 240-41
population
bomb, 561
control, 559
covenant fruitfulness,
558
density, 561
expansion &, 417-18
family plots, 19-20, 387-
88, 419, 509, 524
geography, 567
growth, 418-20, 522-23, 557
Himit, 561
sanctions, 557
Satan &, 565-67

713

stagnant, 15
zero growth, 568-66
Populists, 538
pornography, 190n
positional goods, 564
positive confession, 542n
postmillennialism, 132
poverty
bureaucrats, 485
curse, 242
ethics &, 483
legal definition, 487, 489-
90, 496
loans, 494
local, 201-3
socialist’s explanation, 242-
43
welfare state’s explanation,
483
work &, 228
power, 160, 258, 328, 460
power religion, 328, 528
Prager, Dennis, xxi
prayer, 457
preaching, 193
predestination, 460
present, 64
pricing
judicial, 614-17
oppression, 435, 442
plans &, 215
pricstly, 229-30, 594, 597-98
producers & consumers,
436-39
rural land, 606-9
sacrificial animals, 597-98
priest
Adam, 97, 128, 139
