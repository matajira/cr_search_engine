Mandatory Redemption Upon Payment 539

Conclusion

The jubilee was the year of redemption in Israel. It reunited
judicially the dispossessed Israelite and his landed inheritance.
The maximum time limit placed by God’s law on Israelite
bondservice was therefore the same as the limit on the leasing
of rural property: the next jubilce year.

The possibility of immediate redemption was available in
both cases: land and labor. The kinsman-redeemer could buy
his relative out of bondage by making a prorated payment to
the buyer based on the original purchase price. This payment
was based on the years remaining until the jubilee: the original
purchase price divided by the number of years until the jubilce
multiplied by the number of years remaining.

The presence of this law in the Mosaic law indicates how
important the ideal of consumer sovereignty is in God’s eyes.
An Israelite who found himself in dire straits cconomically
could lawfully sell himself to a resident alien. The economic
success of the resident alien was legitimate. le had met the
demands of consumers. The Israelite had failed to meet the
demands of consumers. The resident alien was authorized to
buy the Israelite until the next jubilee year. So important were
the twin ideals of efficiency and profit that God was willing to
sce some of His people in temporary bondage to covenant-
breakers within the boundaries of the Promised Land. Perhaps
these less efficient Israelites would learn to become more cffic-
ient producers, thereby improving the options available to
consumers.

Because the resident alien did not have to pay a wage to an
Israelite bondservant, while Israelites were required to pay him
a wage, this law gave a competitive advantage to the resident
alien in the market for Israelite bondservants. It made it clear

For a critique of Mr. Mooney, see Gary North, Tools of Dominion: The Case Laws of
Exodus (Tyler, Texas: Institute for Christian Economics, 1990), Appendix G: “Lots of
Free Time: The Existentialist Utopia of S. C. Mooney.”
