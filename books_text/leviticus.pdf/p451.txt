The Sabbatical Year 395
The Terms of the Lease

Leasing land is a very difficult proposition for a landlord.
For an absentcc landlord, it is even more difficult. The problem
is to establish leasing terms that preserve economic incentives
that achieve three goals: 1) keeping a competent lessee on the
property by allowing him to maximize his income; 2) maintain-
ing or increasing the capitalized value of the land; 3) maximiz-
ing the landlord’s lease income. The absentee landlord must
discover a way to achieve all three goals without a great expen-
diture on local monitors. Inexpensive monitors are valuable.

God established the laws governing the Promised Land
because He delivered it into their hands. As its owner, He had
the authority to establish the terms of the leasehold. If the
people did not like the terms of the lease, they could live else-
where. So, one foundation of this law is God’s ownership. (The
other foundation is the principle of sabbath rest.)

The terms of God's lease are generous to the lessee, who
keeps nine-tenths of the net income of the operation. This is
the principle of the tithe. The tithe is paid to God’s designated
agency of collection, the church.? The church acts as God’s
accountant and crop-collector. The payment of the tithe is a
public acknowledgment by the lessees of God’s ultimate owner-
ship of the original capital: land (rent) plus labor (wages) over
lime (interest). his original grant of capital is also accurately
described by John Locke’s three-fold classification: life, liberty,
and property.

2. Gary North, Tuhing and the Church (Tyler, Texas: Institute for Christian
Economies, 1994).

3. He never used this phrase exactly as quoted. Ie wrote of property in general
as “life, liberty, and cstate.” John Locke, Second Treatise on Government (1690), para-
graph 87. He spoke of “life, liberty, or possession” in paragraph 137. Exactly one
century later, Edmund Burke wrote of “property, liberty, and life.” Reflections on the
Revolution in France (1790), paragraph 324. The U.S. Constitution adopted “life,
liberty, or property” in Article V of the Bill of Rights (1791), and also in Artide
XIV:1 (1868).
