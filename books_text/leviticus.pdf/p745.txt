Index

genocide
annulled, 185
inheritance, 414-16, 463
jubilee, 386, 414-15, 423-24
local gods, 333, 423-24
negative sanctions, 183
reasons for, 333, 416
sanctification, 430-31
theology &, 423-24
gentiles
adopted by God, 520-21,
523, 534
covenanial separation an-
nulled, 346
final jubilee &, 610, 640
food laws &, 346
grafting in, 288
Jonah’s ministry to, 6
liberation from sin, 521
modern Israel, 405
slavery, 510 (also see bond-
servant: heathen; slav-
ery)
work of the law in, 682
also see aliens
geography, 180, 182
ghetto cultures, 2, 132, 334,
461
ghetto eschatologies, 461
Gibeonites, 298n, 582-83
Gilchrist, J., 481n
Gilder, George, 563n
Gilgal, 9, 297, 300, 344
gleaning law
agricultural, 196, 198, 204,
206
annulled, 207
anti-compulsive effect, 200

689

conditional, 204

economics of, 197-200

employers &, 201

enforcement, ch. 10, 360,
363-67, 641

excommunication, 359-
60

fallen man, 195

feasts & Levites, 358, 367

God’s field is open, 195

gross payment, 109

hard work, 200-1, 205,
641

harvesters, 198-99

inheritance &, 359

jubilee, 205, 397-402

Jandowners, 198-200, 397-
99

leftovers, 198, 360, 399

lessons, 207-8

Levites &, ch. 92

localism, 201-4, 207

mandated, 198

model for charity, 196

monitors, 364-65

poverty, 359

Priesthood &, ch. 22

restitution, 197

rural subsidy, 201-2

sabbatical year, 399, 402,
406-7

sanctions, 197, 198

self-interest of judges, 362-
65

skills, 205

strangers, 203

summary, 641]

tithe, 70, 364-65
