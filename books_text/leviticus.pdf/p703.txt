Conclusion 647

matic. ‘The geographical and tribal promises that went lo Abra-
ham’s seeds (plural) were fulfilled with the coming of the pro-
phesied Seed (singular: Gal. 3:16) - the Messiah, Shiloh, Jesus
Christ, the incarnate Son of God - who announced His minis-
try’s fulfillment of the judicial terms of the jubilee year (Luke
4:16-21). This fulfilment was confirmed through His death and
resurrection — the ultimate physical liberation. Israel’s perma-
nent disinheritance was prophesied by Jesus: “Therefore say I
unto you, The kingdom of God shall be taken from you, and
given to a nation bringing forth the fruits thereof” (Matt.
21:43). This transfer of the kingdom’s inheritance to this new
nation took place at Pentecost (Acts 2). Vhe visible manifestation
of the permanent revocation of the Abrahamic inheritance to
his biological heirs was the fall of Jerusalem in A.D. 70. Israel
had failcd to keep the terms of the covenant. ‘lhe predictable
negative corporate sanctions came in history.

Discontinuity and Continuity in the Levitical Sacrifices

The whole burnt offering was annulled by the New Cove-
nant, There is no evidence that its underlying principle of
sacrifice was annulled: unblemished animal, the best of the
flock, but only one. This was a high-cost sacrifice, but it was
nevertheless limited. Conclusion: man cannot pay God all that he
owes. ‘Vhis judicial principle was illustrated by the whole burnt
offering, but it was not limited to it.

The meal offering was annulled, but not its underlying prin-
ciple of the hierarchical authority of the priesthood. he salt of
this earthly sacrifice is no longer lawfully administered by any
pricst; the cternal salt of the covenant (Mark 9:47-49) is admin-
istered by the High Priest, Jesus Christ. ‘The judicial principle
of the meal offering still is in force: if you do not bring a satis-
factory offering to be salted and consumed by the fire, then you
will become that offering.

‘The peace offering is no longer caten by the offerer at a
meal held inside the boundaries of the temple. But the economic
