288 LEVITICUS: AN ECONOMIC COMMENTARY

fundamental. The practice of seed-mixing was illegal, not hy-
brids as such.

This law did not apply to the familiar practice of grafting the
branches of one species of fruit tree into the trunk of anoth-
er.” Leviticus 19:19 was specific: it dealt with seeds planted in
a field, not with branches grafted into an adult tree. The trce’s
trunk is the primary agent, symbolic of the covenant itself. The
branch would become part of the older tree. It was not a com-
peling seed. The removed branch was “adopted” by the older
tree. This was always a legal option in Israel, as the marriages
of Rahab and Ruth indicate. The technique of grafting was
symbolic of conversion, which was why Paul used this imagery
as the archetype in discussing the fate of the old branch of
Israel and the grafting in of the gentiles (Rom. 11:17-21). So,
tree grafting symbolized covenantal inclusion — adoption by
conversion and confession — not tribal mixing.

Some crops do better when mixed, such as fodder. In the
modern-day State of Israel, Jewish farmers deal with this prob-
lem in a Rabbinically approved way. One man makes a pile of
seeds in a public place and covers it with a board. A second
person piles up a second seed crop on top of the board. ‘Then
a third person comes along and announces in front of witness-
es, “I need this board.” He removes it. Finally, a fourth man
comes along and is instructed to sow the field with the now-
mixed crop.”

 

Clothing

Mixed clothing made of linen and woo! was under a dilfer-
ent kind of prohibition. It was illegal to wear clothing produced

12. Rabbinic opinion on this verse forbade grafting. Sce Nachmanides (Rabbi
Moshe ben Nachman, the Ramban), Commentary on the Torah: Leviticus (New York:
Shiloh, [12677] 1974), p. 205. He cites the Talmud: Kiddushin 80a.

13. Israel Shahak, jewish History Jewish Religion: The Weight of Three Thousand
Years (Boulder, Colorado: Pluto Press, 1994), p. 45.

 
