Gleaning: Charity Without Eentidement 205

The gleaner had to work very hard, for he reaped only the
lefiovers. This means his income was lower than would have
been the case if he had been a professional harvester. Gleaning
provides a lesson to the poor: there are no free lunches in life.
Someone always has to pay. ‘I'he economic terms of the glean-
ing system established that only the destitutc members of the
community would have become gleaners. If there had becn any
other source of income, other than begging, they would have
taken it. ‘The hard work and low pay of gleaning was an incen-
tive for the individual to get out of poverty.

We must always remember that the gleaning laws operated
within the framework of the jubilee land laws. The poorest
Israelite in the community at some point would inherit from his
father or grandfather a portion of the original family inheri-
tance. ‘he size of that portion of land depended on the num-
ber of male heirs. Its value depended on the economic produc-
tivity of local residents who could legally bid to lease it.!* The
more productive the heir, the more likely that he would be able
to retain control over it.'* Cleaning gave the poor Israelite an
opportunity to gain management and other skills as a landown-
er prior to the time that he or his children would be given back
the original family land grant through the jubilec land law. The
gleaning law provided training that could in the future be
converted into family capital. The gleaning law was designed to
keep poor people in the local agricultural community.

The gleaning law did not apply to non-agricultural business-
es or professions. It originated from the fact that God declared

that the reconstructionist approach to biblical charily is too conditional and restric-
tive.” Ibid., p. 278, For my response, sce North, Westminster's Confession: The Abandon-
ment of Van Til’s Legacy (lyler, Texas: Institute for Christian Economics, 1991), pp-
271-73. See also Sutton, “Whose Conditions for Charity?” in North (od.), Theonamy,
ch. 9.

14. The economist looks for a price to establish value. ‘The highest market value
is determined by the highest market bid by a potential buyer or long-term leaser.

15. This legal right to inherit the family’s land did not extend to the stranger
untif after the exile (Ezek. 47:22-23).
