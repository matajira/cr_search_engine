Impartial Justice vs. Socialist Economics 237

‘These binding civil laws have been revealed by God directly to
mankind in the Bible, and only in the Bible.

Almost every legal theorist in Western society accepts the
principle of equality before the law. This ideal is one of the
bedrock foundations of Western civilization. It comes from the
Bible, not from Greek and Roman law, both of which explicitly
denied the concept of equality before the civil law. Classical law
protected only citizens: males who had lawful access to the
religious rites of the city. Women (half the adult population),
slaves (one-third of all males), and foreign-born residents were
excluded.* The ultimate manilestation of the biblical principle
of equality before the law in history was God the Father’s will-
ingness to place His incarnate son, Jesus Christ, under the
negative sanction that had threatened Adam. Paul writes: “He
that spared not his own Son, but delivered him up for us all,
how shall he not with him also freely give us all things?” (Rom.
8:32). Among these things that God gives is liberty. Liberty is a
product, along with other judicial factors, of the ideal of equali-
ty before God’s law. But Stoic: natural law theory is not the
source of this ideal; biblical law is. Natural law theory invariably
falls into ethical dualism: one law-order for pluralistic society,
another for Christians.’ The natural law thcorist prefers Pon-
tius Pilate to Moses as a civil judge. Those Christians who de-
fend natural law theory ignore this biblical judicial principle:
any failure to impose God's specified civil sanctions in society
necessarily requires the imposition of anti-biblical civil sanctions.

Sanctions: Evaluation and Imposition

Biblical civil justice must seek to apply written laws to public
acts. Neither the social status nor the economic class of either

2. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), Appendix E: “Greck
Mythology: The Myth of Classical Politics.”

3. Tid., ch. 14, section on “Natural Law Theory: Ethical Dualism.”
