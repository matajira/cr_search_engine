dominion covenant, 394
God’s, 43, 52, 113, 129,
207, 289, 307, 394-97
God’s leasehold, 417
God's presence, 423
homes, 472-73, 479
Israelites, 422-23
jubilee &, 408, 416-17, 430
leftovers to gleaners, 198
original title, 416-17
owners bear costs, 113-14
profits, 113
property rights, 266
rational calculation, 113-14
representative under God,
139
responsibility & 466
sabbath belongs to God, 394
sacramental boundaries, 129
Satan’s claim, 83-84
sharecropping, 113, 430
(also see sharecroppers)
sovercignty &, 219, 438
special (God’s presence),
422-23
stewardship, 52-53, 139,
219

pain, 116-18
pantheism, 3
paper production, 28
parental authority, 328
Parliament, 3790
Passover
annulment of, 638
bread, 79
covenantal fruit (feast),
302

Index

711

crowds, 14-15
delayed, 23-24
dietary laws, 344-45, 348
discontinuity, 78
doorway, 100
final, 628
Holy Communion &, 82
household slaves attended,
505
Israel (modern), 405
lamb, 44, 50
leprosy, 170
strangers, 495
passport, 428
patriarch, 521
payment of wages, 223, 225-26
peace, 10, 74-75, 415, 449-50,
455, 544
peace offering
freewill offering, 80-81
holy communion, 81-83
leaven, 74, 78
no oath, 75
point three, li-lii, 46
service, 81
shared meal, 76
peddlers, 23
Pella, 470n.
penalty payment (20%), 597
Pentateuch, xlii-xlix
Pentecost
calendar, 356
costs of, 357
Firstfruits, 15-16
harvest, 355, 356
leaven, 355-56
male-oriented, 71
meal offering, 69
