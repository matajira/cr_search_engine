Verbal Bonds and Economic Coordination 215

carry inventories of raw materials and spare parts. These inven-
tories compensate for men’s imperfcct knowledge of the future.
An inventory of raw materials and spare parts enables the busi-
ness to expand production without a great deal of warning. An
inventory of finished products enables the business to meet the
demand without increasing the price of the product. It allows
the business to continue operating if there is an interruption of
the delivery of materials. In other words, inventories creatc a
production system in which there are fewer bottlenccks. Bottle-
necks create “ripple effects,” both in the company and in some
cases in the economy as a whole. Inventories smooth produc-
tion. But they must be paid for. They must be “carried.” They
tie up resources.

Second, the producer seeks predictability in the pricing of his
resource inputs as a means of gaining predictability of produc-
tion. If an individual agrees to sell you an item, and you then
muke plans in terms of the price of that item, you have become
dependent on him. Similarly, you have agreed to pay him a
money price at a particular time. He has therefore become
dependent on you. The free market economy produces a sys-
tem of independence legally (individualism) and mutual depen-
dence economically (coordination of plans). We are legally free
to make our voluntary decisions; therefore, we are judicially
independent. At the same time, because of these voluntary
agreements, we become mutually interdependent in our cco-
nomic activities. This is why pricing is so important. It enables
us to make decisions rationally in terms of existing conditions of
supply and demand.

‘Third, predictability of quadity is also important. This one is
more difficult to police. What level of quality is good enough at
a particular price? ‘Vhis is difficult for the buyer or the seller to
specify in a written contract. We seek ways of gaining this infor-
mation inexpensively, both as buyers and sellers. The existence
of brand names is very important in lowering the costs of peo-
ple’s estimates of quality. Pricing also plays an important role.
