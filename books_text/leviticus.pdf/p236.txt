180 LEVITIGUS: AN ECONOMIC COMMENTARY

The separation described in Leviticus is multifaceted. Sepa-
ration was judicial: sacred, common, and profane. It was geo-
graphical: the holy of holies in relation to the temple; the tem-
ple area in relation to the rest of the nation; each tribe of Israel
in relation to the other tribes; walled cilies in relation to the
countryside; the very land of Israel in relation to the land out-
side the boundaries. Tribal separation was in turn prophetic,
relating to the promised Seed (Gen. 3:15; 49:10). Separation
was priestly: Aaron and Levi; Levi and the other tribes; Israel
and the nations. Separation was chronological: the three man-
datory yearly feasts, the sabbatical year, and the jubilee year. It
was biological: breed vs. breed. It was dietary: clean and un-
clean. It was physical: clean and unclean. It was ritual: clean.
and unclean. It was economic: rich and poor. It was political:
citizen and non-citizen. It was above all ethical: good and evil.

It is in these chapters that the hermeneutical problem with
Leviticus - and with the Mosaic covenant generally — presses
the commentator. Which of these laws were cross-boundary
laws? Which applied both inside and outside the nation of
Israel? ‘The geographically cross-boundary laws were universal
moral laws, and as such, their binding character has crossed
over into the New Covenant. To use a New Govenant meta-
phor, these laws were resurrected with Jesus.

Texas: Institute for Christian Economics, 1986), ch. 8
