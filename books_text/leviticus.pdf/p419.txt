Mutual Self-Interest: Priests and Gleaners 363

spected by the social theorist. We must apply this insight to the
behavior of those people who have been invested by God with
covenantal authority. We therefore need to pursue the question
of law-enforcement in Old Covenant Israel. If the Levites and
priests were in fact the covenantal agents assigned by God to
enforce the gleaning laws, then we should not expect God’s law
to rest on the assumption that the Levites and priests would
normally carry out this assignment against their personal self-
interest. We should expect rather to find judicial safeguards
that protected their interests as they went about their judicial
assignments. This is exactly what we find in the case of the
gleaning laws.

How to Pay Judges

Judges should not take bribes (Ex. 23:8; Deut. 16:19). Judg-
es should declare God's law and apply it to specific cases that
come before them. This is a basic operating premise of biblical
jurisprudence. ‘he availability of personal gain is not to influ-
ence the judges’ decisions. Having said this, we should also
acknowledge the bribery law’s economic corollary: judges
should not suffer losses because of their decisions. ‘Their deci-
sions should not make them poorer. Thus, we conclude, judges’
income should not be affected positively or adversely by their decisions.
This is why they should be paid agreed-upon salaries by the
sanctioning institution irrespective of their decisions for as long
as they are employed by that institution. This rule governs both
church and State. This is also why they should not be allowed
to judge cases in which they are uniquely in a position to gain
or lose because of their decision.

The question then arises: Were the Levites and priests
threatened economically by their honest enforcing of the glean-
ing law? If they did enforce it, did they or the priesthood in
general risk a loss of income? Even morc to the point, would
their income automatically have been reduced? Specifically, did
the enforcement of “gleaner’s rights” reduce the priesthood’s portion of
