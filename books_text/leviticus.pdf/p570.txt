514 LEVITICUS: AN ECONOMIC COMMENTARY

as few as three (Deut. 23:7-8). Once they became citizens, they
could not be permanently enslaved (Lev. 25:44-46). The hea-
then slave law served as a magna carta of liberty for the natur-
alized immigrant. He could achieve full legal status as a citizen
despite the fact that he had no inheritance in the land. Citizen-
ship was by confession, circumcision, and numbering in the
holy army. But it was not granted overnight by a tribe, except
during wartime.

Jesus Christ was the ultimate Heir, the promised Seed (Gal.
3:16), the One for whom the Mosaic system of tribal inheritance
had been created. It was He who announced the jubilee year
(Luke 4:18-21). It was He who offered men adoption into His
family (John 1:12). It was Ile who paid the debts of the crimin-
als We adopts into His family, Instead of a hole in the ear
drilled by an aw] at the doorway of an Israclite’s houschold (Ex.
21:6), baptism is the new mark of adoption. The New Coven-
ant’s jubilee year of release was the final jubilee for Old Goven-
ant Israel.
