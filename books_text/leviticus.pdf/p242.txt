186 LEVITICUS: AN ECONOMIC COMMENTARY

Ethics and Geography

The issue was ethics, point three of the biblical covenant
model. The focus was geography: the Promised Land (Lev.
18:2-5). The laws that God’s people must follow in the land
should not be the laws of either Egypt or Canaan. While the
text does not specifically mention it, it is clear that God’s histor-
ical sanctions were involved. The Israelites had already seen the
sanctions that God had brought against Egypt. First, there were
plagues inside the land. Second, the Egyptians had given pre-
cious gems and precious metals to the fleeing Israelites. Third,
the Israelites had been expelled from Egypt as God’s means for
providing deliverance and liberation. The Egyptians lost their
slave labor force. Similarly, God tells them in this chapter that
there will be comprehensive negative sanctions imposed against
those who presently dwell in the Iand of Canaan. The Canaan-
ites will someday be vomited out by the land, i.e., by the invad-
ing Israelites. The imagery of vomiting out symbolized a military
phenomenon ~ invasion of the land — and the cultural phenom-
enon of replacement -by a new nation.

‘The operational factor here was ethics. God promised them
that when they entered the land and established residence, the
plagues of Egypt would be removed from the land, if they
remained covenantally faithful (Deut. 7:15), The God of libera-
tion they understood as the God who brings positive and negative
sanctions in history. What is unique about this chapter is that the
land itself is described as imposing negative sanctions against
law-breakers. The Promised Land would become God’s coven-
antal agent after they invaded Canaan. The land was a surro-
gate for man.’ Because of this, living inside the land’s bound-
aries meant that all residents were under a theocratic order.’

3. For a detailed discussion, sce Gary North, Boundaries and Dominion: The
Economics of Leviticus (computer edition; Tyler, Texas: Institute for Christian Econom-
ics, 1994), ch. 10, section on “The Land as a Surrogate for Man.”

4, Ibid., ch. 10, section on “heocratic Order in the Promised Land”
