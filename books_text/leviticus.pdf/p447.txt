Introduction to Part 5 391

Levites, Because the homes of the Levites in Levitical cilies were
governed by the jubilee (Lev, 25:32-33), the Levites had an
economic incentive to declare the jubilee on schedule twice per
century — far stronger than the incentive for them to declare a
sabbatical year. Did they nevertheless defect? If so, why?

Conclusion

‘The jubilee year was a year of liberty for all the inhabitants
of Israel (v. 10). But there was an exclusionary clause in the
jubilee law: the enslavement of heathens (vv. 44-46). The best
way to avoid slavery was to become a citizen of the holy com-
monwealth. Unlike the other ancient nations, citizenship in
Israel was open to any resident alien, or at least to his heirs
(Deut. 23:2-8). The blessings of liberty could be secured
through confession of faith in God, circumcision, and eligibility
to serve in God’s holy army. This was the Mosaic law's incom-
parable promise to all resident aliens. But to attain citizenship,
a family would have to remain economically productive until
the heirs of the promise could secure their claim. This promise
was conditional: remaining productive and avoiding being sold
into servitude.

The jubilee law pointed to the conditional nature of Israel’s
very existence as a nation: God’s threat of disinheritance, which
was a threat of servitude to forcigners. There were conditions
attached to citizenship: covenantal stipulations. The jubilee
law’s stipulations (Lev. 25) — point three of the biblical covenant
~ were immediately followed by a list of promised sanctions
(Lev. 26): point four.

Every true prophet of Israel came before the nation to bring
a covenant lawsuit. This reminded them of the ethical basis of
liberty. Israel’s final prophet would bring Israel's final covenant
lawsuit. He would declare liberty for the enslaved and slavery
for the rebellious slavemasters. He would serve as the final go’el:
the kinsman-redeemer and the blood avenger. Ile would adopt
many and disinherit others. Ile would bring sanctions. He
