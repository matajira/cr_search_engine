Corporate Responsibility 93

offering (Ley. 4:23). The common man who sinned uninten-
tionally had to bring a female goat without defect (Lev. 4:28),
He could also bring a female lamb without defect (Lev. 4:32).
The symbolism is obvious: masculinity under the Mosaic Covenant
was associated in the civil covenant with rule, femininity with subordi-
nation.*" In neither case — civil ruler or citizen - was a bullock
an appropriate sacrificial animal, for the bullock was associated
with priestly authority.

We have seen that the sin of the priest and the sin of the
whole congregation were of similar consequence in God's eyes.
Likewise, the sins of the ruler and the lone individual were
comparable. The sacrificial link between priest and people
indicates that the pricst had sufficient representative authority
for his unintentional sin to bring the people under God’s nega-
tive sanctions. The civil ruler did not possess comparable repre-
sentative authority.

What is indicated in Leviticus 4:1-3 is that there was a much
closer judicial link between the priesthood and the covenanted society
than there was between the civil ruler and the covenanted society. This
is why we must conclude that the church was covenantally more
important in Isracl than the State was. The unintentional sin of
the priest was treated by God as comparable to the unintention-
al sin of the whole congregation, while the unintentional sin of
the ruler was treated on a par with the unintentional sin of the
average citizen."! Conclusion: the laxity of the priesthood re-
garding their personal sins threatened greater direct negative

 

20. ‘The abolition of all required ritual sacrifices in the New Covenant (Heb. 9)
has removed the male-female distinction in the civil covenant. Without civil sacrifices,
there is no legitimate judicial restriction on women participating in civil rulership.
The male-female distinction is maintained in matters of the church's ordained clders
only because a male must represent a male God in the administration of the sacra-
ments and the covenantally authoritative declaration of God’s word (I Gor, 14:34-35).
Prohibiting female elders has nothing to do with sacrifices.

 

21. Economically speaking, the king’s sacrifice was fess burdensome than the
commoner’s, for a female goat can produce offspring and milk. The male animal was
symbolically more important in the ancieni world, bul. not. cconomically.
