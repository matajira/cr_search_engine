Introduction 9

only by their children, and only after their covenant renewal at
Gilgal (Josh. 5).

Leviticus presents the rules governing this kingdom grant
from God. This land grant preceded the giving of these rules.
Grace precedes law in God’s dealings with His subordinates. We are in
debt to God even before He speaks to us. The land grant was
based on the original promise given to Abraham. ‘That promise
came prior to the giving of the Mosaic law.’ This is why James
Jordan says that the laws of Leviticus are more than. legislation;
the focus of the laws is not simply ebedience to God, but rather
on mainiaining the grant.° The basis of maintaining the grant
was ethics, not the sacrifices. Man cannot maintain the kingdom
in sin.’' ‘he fundamental issue was sin, not sacrifice; ethics,
not ritual, God told them this repeatedly through His prophets:

For | spake not unto your fathers, nor commanded them in the day
that I brought them out of the land of Egypt, concerning burnt offer-
ings or sacrifices: But this thing commanded I them, saying, Obey my
voice, and I will be your God, and ye shall be my people: and walk ye
in all the ways that I have commanded you, that it-may be well unto
you. But they hearkened not, nor inclined their ear, but walked in the
counsels and in the imagination of their evil heart,.and went backward,
and not forward (Jer. 7:22-24).

To what purpose is the multitude of your sacrifices unto me? saith
the Lorp: I am full of the burnt offerings of rams, and the fat of fed
beasts; and I delight not in the blood of bullocks, or of lambs, or of he
goats. When ye come to appear before me, who hath required this at
your hand, to tread my courts? Bring no more vain oblations; incense
is an abomination unto me; the new moons and sabbaths, the calling of
assemblies, 1 cannot away with; it is iniquity, even the solemn meeting.
Your new moons and your appointed feasts my soul hateth: they are a

9. James B. Jordan, Gavenunt Sequence in Leviticus and Deutermomy (Tyler, Texas:
Institute for Christian Economics, 1989), p. 8.

10. Did., p. 9.
WM. bid, p. LL.
