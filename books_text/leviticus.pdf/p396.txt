340 LEVITICUS: AN ECONOMIC COMMENTARY

God’s statutes. Public obedience to the Mosaic law was to re-
main the mandatory manifestation of their service to Him and
fear of Him.

The familiar Christian hymn, “Trust and Obey,” expresses
the ethical nature of covenantal inheritance: “for there’s no
other way” to maintain this inheritance. (This hymn is sung
enthusiastically by Protestants whose churches officially deny its
theology of sanctification.) Abraham was told to trust God. This
meant trusting God’s promises. [lis heirs were also to trust
these promises. The outward manifestation of this trust was
circumcision. Without this outward act. of obedience, the Isracl-
ite ceased to be an Israelite, and therefore he removed himself
and his heirs from the promised inheritance. So, the inheri-
lance of the land was a pure gilt [rom God, but to remain the
beneficiary of this unmerited legacy, the recipients of the prom-
ise had to obcy the terms of the covenant. It was not that their
obedience was the legal foundation of the promise. The prom-
ise of God was its own legal foundation. But obedience was the
legal basis of their remaining in the will of God, in both senses:
the moral will and the testamentary will. A refusal to place the
mark of the covenant ~ a symbolic boundary — on the flesh of
all one’s male heirs was an act of self-disinheritance. Excommuni-
cation became mandatory: a cutting off from the people, i.c., a
kind of judicial circumcision of the nation. Covenant-keepers who
broke this commandment were to be treated as foreskins.°

The Dietary Laws

The prohibition against eating certain foods was part of the
land laws of Israel. This passage makes it clear that the reason
why God imposed the food laws was to preserve the nation’s
separation. “Ye shall therefore put difference between clean
beasts and unclean, and between unclean fowls and clean: and

5, Saul’s demand that David provide a bride price for Michal of a hundred
Philistines’ foreskins points to this judicial meaning of the foreskin (T Sam. 18:28).
