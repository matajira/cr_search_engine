214 LEVITICUS; AN ECONOMIG COMMENTARY

doing or not doing? This is the problem of economic sanc-
tions.”

Promise and Dependence

The importance of the division of labor has been emphasized
in modern economics ever since Adam Smith wrote his famous
first chapter on the production of pins.” A highly skilled indi-
vidual craftsman cannot produce a great number of pins in one
day, Smith observed. On the other hand, a small group of
relatively unskilled workers can produce thousands of pins if
they are given the proper capital equipment. He pointed to the
division of labor as the explanation — a fundamental biblical
concept (Rom. 12; I Cor. 12). The division of labor allows the
increase of output per unit of scarce resource input. Goopera-
tion produces greater wealth than economic autarky can. It is
the division of labor which enables us to pool our talents in
order to gain much greater output together than we could
possibly have achieved as individuals acting in isolation. Be-
cause increased cooperation increases individual productivity,
it also increases per capita wealth. This increase - a positive
sanction — is the incentive for men to cooperate economically
with each other. It is a very important aspect of the preserva-
tion of society. It allows the pooling of individual talents, and it
allows the pooling of capital. This capital can be of three kinds:
economic, inteHectual, and moral.

Cooperation requires a degree of predictability. First, it
requires the predictability of timing. Let us consider a business.
A business manufactures a particular product. To do so, it
requires resource inputs. Because people's knowledge of mar-
kets is limited, and because it is expensive to go out and buy
exactly what you want exactly when you want it, businesses

11. Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth,
Texas: Dominion Press, 1987), ch. 9.

12. Adam Sinith, The Wealth of Nations (1776).
