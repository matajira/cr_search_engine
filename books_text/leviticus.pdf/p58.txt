2 LEVITIGUS: AN ECONOMIC COMMENTARY

cus is generally acceptable to most Christians, but the specified
civil sanctions are a terrible mental burden for them. They will
do almost anything, including dismissing the continuing validity
of almost all the laws in Leviticus, i order to escape any per-
sonal or corporate responsibility for pressuring civil rulers to
enforce the Levitical civil sanctions. Christians are tempted to
deny all of the Levitical separations rather than affirm any of
the Levitical civil sanctions. In short, they would rather deny
the ethical terms of the Levitical system of holiness than affirm
the judicial terms of Levitical civil justice.

Escaping Cultural Relevance

Here is a major dilemma for the modern church. Christians
confidently affirm that “the Bible has answers for all questions.”
But one question is this: What relevance should Christianity
have in culture? Modern antinomian Christians emphatically
deny the judicial foundation of Christianity’s cultural relevance
in history: biblical law and its biblically mandated sanctions.
Most Christians prefer pietism to cultural relevance, since civil
responsibility accompanies cultural relevance. They seek holi-
ness through withdrawal from the prevailing general culture.

This withdrawal has forced them to create alternative cul-
tures — ghetto cultures — since there can be no existence for
man without culture of some kind. Mennonites have achieved
a remarkable separation from the general culture, though not
so radical as tourists in Amish country like to imagine, by aban-
doning such modern benefits as electricity in their homes and
the automobile. But they travel in their buggies on paved high-
ways, and they use electricity in their barns. They are always
dependent on the peace-keeping forces of the nation. Pietistic
Christians have longed for a similar separation, but without the
degree of commitment shown by the Amish. Thcy send their
children into the public schools, and they still watch television.
The result has been catastrophic: the widespread erosion of
pictism’s intellectual standards by the surrounding humanist
