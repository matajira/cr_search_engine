xii LEVITICUS: AN ECONOMIG COMMENTARY

I had not grasped its importance for my work. The five points
of the biblical covenant are crucial for understanding Leviticus.

The Pentateuch’s Five-Point Covenant Structure

As far as I am aware, what no onc had scen — or at least no
one had published - when I began this commentary project is
this: the Pentateuch is structured in terms of the Bible’s five-
point covenant model. I recognized this structure of the five
books of Moses only alter I had finished reading (as I recall)
the third draft of Sutton’s manuscript. My discovery forced me
to think through my strategy for the entire commentary. I
wrote a Preface at the last minute for The Sinai Strategy (1986),
introducing the five-point model. Then I wrote a General In-
troduction to the entire economic commentary series in the
second edition of The Dominion Covenant: Genesis (1987).

So far, I have completed commentarics on only three of the
five books of the Pentateuch. I can say with considerable confi-
dence that the first three books of the Pentatcuch conform to
the model. I can see that the last two also conform, although I
have not worked through them in detail yet. Honest critics who
reject Sutton’s thesis will eventually have to take into account
my commentaries and the support volumes I have published.
(Dishonest critics will, as usual, murmur in private to their
students that nothing has been proven, that this model is all
smoke and mirrors. But I am confident that they will not go
into print on this, also as usual.)* Here is the five-point outline
of the Pentateuch.

Genesis

Genesis clearly is a book dealing with God’s transcendence.
‘Transcendence is point one of the biblical covenant model, The

4. See Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; ‘Iylex, Texas: Institute for Christian Economics, 1994), Appendix K: “Critics
of the Five-Point Covenant Model.”
