Covenantal Fruit 295

had to be a discontinuity between the trees and seeds of the old
Canaan and the trees and seeds of the new Canaan. Like the
leaven of Egypt that had to be purged out during the first
Passover, so were the firstfruits of Canaan, ‘he Jeaven (ycast) of
Egypt could not be used as “starter” for the leaven of the con-
quered Canaan. It was different in the case of Canaan’s trees.
‘They had to be used as “starter” for Isracl’s new orchards.
Thus, God prohibited access to their fruit for a period, thereby
emphasizing the covenantal discontinuity between the old Ca-
naan and the new Canaan.

Second, God called “uncircumcised” the forbidden fruit of the
first three scasons. ‘This is a peculiar way to speak of fruit.
Circumcision was the visibic mark of the Abrahamic Covenant:
the visible legal boundary separating the heirs of the promise
from non-heirs. ‘hat is, circumcision determined inheritance
(point five of the biblical covenant model: succession/inheri-
tance). In Mosaic Isracl, circumcision separated those who had
lawful access to the Passover meal from those who did not
(point four:
inclusion vs. exclusion inside the formally covenanted people of
God (point three: boundarics). Incorporation into the covenant-
ed nation was by covenantal oath-sign (point four). The uncir-
cumcised individual was institutionally outside God’s covenantal
boundary. He was therefore judicially unholy, ie., not set apart
legally. He would profane a ritually holy place by crossing its
legal boundary. But who was this uncircumcised person? Was
he a resident alien? If so, what did the mandatory three years
of separation have to do with him?

A judicial separation of this kind implied a threat - negative
sanctions ~ to the violator of the boundary. Whom did the
forbidden fruit threaten? Not the birds or other beasts of the
land. They had lawful access to the fruit during the first three
years. The [ruit was not poisonous, obviously. ‘Then why was it
prohibited to an Israclitc? Why was there a legal boundary
placed around it? What did this boundary symbolize?

 

oath/sanctions). ‘The legal basis of separation was
