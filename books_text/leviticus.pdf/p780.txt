724 LEVITICUS: AN EGONOMIG COMMENTARY

national, 100
oath-breaking, 88
omission, 107-8
pollution, 100
priest vs., ruler, 93-94
purification offerings, 88
representative, 88, 90,
325
rich man’s, poor man’s,
114-15, 116-18
ruler’s, 87-88, 91-94
slavery &, 526
tribal jurisdiction &, 252
unintentional, 90, 100, 105
sin offering (sec purification
offcring)
skills, 531
skin, 168-69
skins, 47
Skousen, Mark, 436n
slandcr, 377-380
slavery
abolition (late), 259, 427,
516-17
adoption vs., 510-13, 520-23
aliens, 417, 426
annulled, 427, 513; 516-
17
Augustine's view (sin), 526
Bible, xxvi-xxvii
biblical theology, 517
citizenship, 389, 426
costs of (jubilee), 525
Enlightenment vicw, 516
escape from, 390, 510-13,
520-23
fall of Jerusalem &, 513

heathen, 390-91, 426,
460-69, 509, 515,
521-23

humanisin’s version, 527-28

immunity, 520-23

inescapable concept, 526

Israclites, 642 (also see
bondservant)

jubilee &, 427, 513, 517-
20

liberation (3), 510, 523

military service, 504-5

models (2), 526

modern man &, 527

negative sanction, 526

No Exit, 521

post-exilic, 407, 469

price, 601

purpose of, 518-20

Quakers, 516

redemption, 520-23

sharecropping, 112n

sin &, 526

temple, 582-83

trade, 515

sloth, 163, 243, 482

Smith, Adam, 214, 255, 436

smoke as judgment, 1, 67, 69,
184

smorgasbord Christianity, 518

sobriety, 153-55

social chaos, 4

Social Security, 111

social theory

adultery &, 101

baptized humanism, 486

centrality of church &, 103,
458
