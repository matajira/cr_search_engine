Preface xliti

opening words of Genesis affirm God as Creator, testifying to
God’s absolute transcendence, the foundation of the Creator-
creature distinction: “In the beginning God created the heaven
and the carth” (Gen. 1:1). God established a hierarchy through
His covenant: mankind over nature (Gen. 1:26-28), each man
ruling over his wife (Gen. 2:18). Ile gave them a /aw: no eating
from the prohibited tree (Gen. 2:17a). Ile promised to bring
judgment against them if they disobeyed (Gen. 2:17b). They
violated His law, but out of His grace, God promised them an
heir (Gen. 3:15). Here arc the five points of the biblical cove-
nant model.

What is the story of Abraham all about? It is the story of a
promise that was sealed by a covenant act and sign (circumci-
sion). Tribal Israel’s story is one of covenant-breaking, God’s
negative sanctions, and the renewal of Abraham’s covenant.
Genesis ends with Jacob’s verbal blessings and cursings on his
sons. Jacob transferred the inheritance, tribe by tribe. Then he
died. But above all, Genesis is the story of God the absolutely
sovereign Creator and providential Sustainer of history, the
transcendent God who has revealed Ilimself to Ilis people.

Exodus

Exodus is clearly the book of the covenant itself. “And he
took the book of the covenant, and read in the audience of the
people: and they said, All that the Lorp hath said will we do,
and be obedient” (Ex. 24:7). “And the king commanded all the
people, saying, Keep the passover unto the Lorp your God, as
it is written in the book of this covenant” (II Ki. 23:21). God
established His authority over the Israelites by delivering them
out of Egypt. This is what Kline calls historical prologue: point
two of the covenant.’ But what did the historical prologue of

5. Meredith G. Kline, Treaty of the Great King: The Covenant Structure of Deuterono-
my: Studies and Commentary (Grand Rapids, Michigan: Eerdmans, 1963), pp. 52-61:
“Historical Prologue: Covenant History, 1:6-4:49.”
