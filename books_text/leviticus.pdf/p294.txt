238 LEVITICUS: AN ECONOMIC COMMENTARY

the victim or the accused is to be considered in judicial pro-
ceedings. The pronouncement by the judge or the jury regard-
ing the fit between the law and the public act of the accused is
to be based solely on the law and the evidence. Justice is never
impersonal; it is wholly personal: the law, the act, the evidence,
and the court’s judgment.

Judgments should involve the imposition of sanctions: bless-
ings (on the victim) and cursings (on the criminal). There is no
neutrality. Any failure to impose biblical sanctions, apart from the
permission of the victim, is necessarily the imposition of unbiblical
sanctions. Biblical sanctions are limited. There must be the ap-
plication of sanctions, but the victim always has the right 1o
reduce the sanctions. Biblical sanctions are always based on the
principle of restitution: to God and the victim.* The victim is to
gain back what he lost plus a penalty payment. But biblical
sanctions must not exceed what is legally appropriate to the
crime. This places limits on the judges. ‘The judges are not to
declare greater sanctions than God’s law allows. The judges
therefore are under a legal boundary.

The imposition of the sanctions restores the judicial status quo
ante, Judicially, at the end of the trial and after the sanctions
have becn imposed, both the victim and the criminal are re-
stored to their original judicial status. Their economic status has
changed. This is because of the restitution payment. The victim
is richer than before the commission of the crime. The convict-
ed criminal is poorer than before the commission of the crime.
‘This fact categorically denies the ideal of cconomic equality.
The economic positions of the two individuals are not equal
aficr the sanctions have been enforced. On the other hand, the
judicial positions of the individuals are equal after the sanctions
have been imposed. Therefore, judicial equality before the law has
to mean economic inequality afler the sanctions have been imposed.

4. Gary North, Thols of Dominion: The Case Laws of Bxodus (1ylex, ‘Lexas: Lnstituce
for Christian Economics, 1990), ch. 7.
