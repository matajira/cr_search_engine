654 LEVITICUS: AN ECONOMIC COMMENTARY

I begin any investigation of any. suspected judicial disconti-
nuity with the following questions. First, is the case law related
to the priesthood, which has changed (Heb. 7:11-12)? Second,
is it related to the sacraments, which have changed? Third, is it
related to the jubilee land laws (e.g., inheritance), which Christ
fulfilled (Luke 4:18-21)? Fourth, is it related to the tribes (e.g.,
the seed laws), which Christ fulfilled m Ilis office as Shiloh, the
promised Seed? Fifth, is it related to the “middle wall of parti-
tion” between Jew and gentile, which Jesus Christ's gospel has
broken down (Gal. 3:28; Eph. 2:14-20)? These five principles
prove fruitful in analyzing Leviticus 19:19.

Let us ask another question: Is a change in the priesthood
also accompanied by a change in the laws governing the family
covenant? Jesus tightened the laws of divorce (Matt. 5:31-32).
The church has denied the legality of polygamy. Did other
changes in the family accompany this change in the pricsthood?
Specifically, have changes in inheritance taken place? Have
these changes resulted in the annulment of the jubilee land
laws of the Mosaic economy? Finally, has an annulment of the
jubilce land laws annulled the laws of tribal administration?

: ARR RO RE

I hope the reader recognizes by now that there are princi-
ples of interpretation that are applicable to the laws of the

67. ‘This application is especially important in dealing with Rushdoony’s theory
of “hybridization.” See Gary North, Boundaries and Daminion: The Economics of Levilicus
(computer edition; ‘Iyler, Texas: Institute for Christian Economics, 1994), Appendix
H.

68, There are several other hermencutical questions that we can ask that relate
to covenantal discontinuity. Sixth, is it an aspect of the weakness of the Israclites,
which Christ's ministry has overcome, thereby intensifying the rigors of an Old Gove-
nant law (Matt. -48)? Seventh, is it an aspect of the Old Covenant’s cursed six
day-one day work weck rather than the one day-six day pattern of the New Coven-
ant’s now-redeemed week (Heb. 4:1-11)? Eighth, is it part of legal order of the once
ritually polluted earth, which has now been cleansed by Christ (Acts 10; T Gor. 8)?

 
