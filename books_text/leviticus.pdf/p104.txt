48 LEVITIGUS: AN ECONOMIC GOMMENTARY

the book of boundaries. It begins with the sacrifices, for man in
Adam has crossed a holy boundary and has become profane.
He is fit for the everlasting fire. To avoid that fire, he would
have to place various substitutes on the flaming altar. These five
sacrifices forced covenant-keeping Israelites to confront the
consequences of sin.
