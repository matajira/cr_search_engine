Foreword XXxvii

step, fundamentalists and cvangelicals are being forced to
choose between Bahnsen’s hermeneutic and Gomes’ hermeneu-
tic. They are willing to do almost anything, such as write theo-
logically befuddled books, to defer this decision.

Conclusion

In 1993, Simon & Schuster, a major publishing firm, re-
Icased an updated version of Ernest Sutherland Bates’ 1936
expurgated version of the King James Bible. The text is 1,248
pages long. Approximately two and a quarter pages comprise
Leviticus. It is the shortest book in Bates’ text until he reaches
the minor prophets.

Boundaries and Dominion is longer than Bates’ text for the
entire Bible. Why should anyone struggle through a book as
large as this one, let alone Boundaries and Dominion? Neither
book is easy reading. hey were surely not easy writing. What
possible benefits are likely to offset the large investment cost of
forfeited time: mine (past) and the reader’s (future)?

For most readers, the costs are far higher than the prospec-
tive gains. Such readers will not even begin. Few people are
sufficiently interested in the Bible to read it cover to cover. Of
those who are this interested in the Bible, few are interested in
the Old ‘Testament. Of those interested in the Old Testament,
few (including pastors) are interested in theology.*® Of those
who are interested in theology, few are interested in biblical
law. Of those who are interested in biblical law, few are interest-
ed in Mosaic laws that are no longer in force. In this commen-
tary, I show why most of the economic laws in Leviticus are no
longer in force. Professional advertisers would call mine a prod-
uct-driven campaign, in contrast to a market-driven campaign.
Product-driven campaigns almost always lose money.

39. David F Wells, No Place for Truth; or Whatever Happened to Evangelical Theol-
agy? (Grand Rapids, Michigan: Eerdmans, 1993).
