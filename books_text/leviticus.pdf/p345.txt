The Preservation of the Seed 289

by mixing these two fibers. There was no law against producing
mixed cloth for export, however. Why was wearing it wrong
but exporting it allowed?*

No other form of mixed-fiber clothing was prohibited by the
Mosaic law. Did this case law by implication or extension pro-
hibit all mixed fibers? This seems doubtful. It would have bcen
easy to specify the more general prohibition rather than single
out these two fibers. Deuteronomy’s parallel passage also speci-
fies this type of mixed fabric (Deut. 22:11) Then what was the
nature of the offense? Answer: to wear clothing of this mixture
was to proclaim symbolically the equality of Israel with all other
nations. This could not be done lawfully inside Israel. But it
could be donc by non-Israclites outside Israel.

Linen was the priestly cloth. The priests were required to
wear linen on the day of atonement (Lev. 16:30-34). Linen was
to be worn by the priest in the sacrifice of the burnt offering
(Lev. 6:10). During and after the Babylonian captivity, because
of their rebellion in Israel, the Levites and priests were placed
under a new requirement that kept them separate [rom the
people: they had to wear linen whenever they served before the
table of the Lord. ‘hey had to put on linen garments when
they entered God's presence in the inner court, and remove
them when they returned to the outer court. No wool was to
come upon them (Ezek. 44:15-19). The text says, “they shall not
sanctify the people with their garments” (Ezek. 44:19). Priestly
holiness was associated with linen.

Inside a priestly nation, such a mixture was a threat to thé
holiness of the priests when they brought sacrifices before God.
‘As between a priestly nation and a non-pricstly nation, this
section of Leviticus 19:19 symbolized the national separation of
believers from unbelievers. Deuteronomy 22:11 is the parallel
passage: “Thou shalt not wear a garment of divers sorts: [as] of
woo] and linen together.”

14. In biblical law, if something is not prohibited, it is allowed.
