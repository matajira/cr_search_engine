Sacred, Profane, and Common 121

gression of God’s law, later recognizes this infraction, and then
offers sacrifice. lo pay for his transgression. He recognizes his
own guilt, and he then offers a sacrifice as his acknowledgment.
Nevertheless, the King James Version comes closer to the theo-
logical meaning of the type of transgression involved: a trespass
— a boundary violation ~ in the same sense that Adam’s sin
involved a transgression of the judicial boundary which God
had placed around the forbidden fruit. Adam and Eve were
indeed guilty, but their guilt was based on a literal trespass.

Holy Things and Holy Commandments

This passage rests on a distinction between holy things of the
Lord and holy commandments. A transgression of holy things
in ignorance required a 20 percent penalty plus the offering of
a ram (vv. 15-16). In contrast, a wransgression of God’s com-
mandment in ignorance required only the sacrifice of the ani-
mal (v. 18). This secmingly minor distinction becomes the basis
of the analysis of the present chapter — specifically, acknowledg-
ing the biblical distinction between the sacred and the common,
but denying the legitimacy of a far more widely accepted dis-
tinction: sacred us. profane. The latter distinction undergirdcd
ancient and modern religion.* This false distinction has be-
come an important aspect of modern sociology and anthro-
pology, especially as a result of the work of Emile Durkheim®
Onc of the most serious errors that has resulted from a misun-
derstanding of the biblical categories of sacred, common, and
profane is the false distinction between what is sometimes called
full-time Christian service and secular employment. Full-time
Christian service is regarded as sacred; secular employment is

 

4, Gary North, Boundaries and Dominion: The Economies of Leviticus (computer
edition; ‘Fyler, Texas: Institute for Christian Feonomics, 1994), ch. 6, section on
“false Distinctions Within Ancient and Modern Religion”

5. See my detailed treatment of Durkheim’s error in this regard: ibid., ch. 6,
section on “False Distinctions Within Modern Academia.”

 
