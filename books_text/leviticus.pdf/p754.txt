698 LEVITICUS: AN ECONOMIC COMMENTARY

legal claim on land, 183-
84

limits were temporary,
567-68

loss of kingdom, 609-12

marching, 14

modern, 405

nation of priests, 297

Passover 348, 405

population, 418-20

post-exilic, 469-70

primitive?, xxxiii

profaned by invasion,
297, 300, 504, 627

profaning the land, 300

represented the nations,
141-42, 546

responsible corporately, 98

sanctuary, 187, 309

separation, 282

seventy bullocks, 141-42

sonship, 323

State of, 288, 405, 611-
12

theocratic republic, 99, 95,
102

unholy for a generation,
301

unholy status: tree planters,
298

urbanization, 20, 421,
476

walk with God, 8

walking, 11-18

welfare, 196

also sce land

Israel’s boundaries
concentric circles, 180

constraints of, 159

garden of Eden, 6, 294

inheritance transcends, 568

Israelites’ faith in, 346

Jinks to God’s law, 43

Mosaic Covenant &, 628-29

presence of God, 8, 89, 423

province of God, 5-6

sacred, 348

temporary, 5, 159, 346

Israelites

adoption, 519-20

bastardy law, 507

bondservice, 642

common, 123, 151-52

evangelism, 32, 35

fruit’s legal status, 298-99

owned by God, 43, 506

profaned the land, 298

unholy until Gilgal, 297,
300, 301, 307

wilderness era, 299-302, 307

Jacob, 158, 228, 521, 557, 638
jade, 28
Japan, 24, 27
jealousy offering, 64
Jephthah, 584
Jephthah’s daughter, 585-86
Jericho, 301, 302, 414, 584
Jeroboam, 12
Jerusalem

center, 22, 253

fall of, 187, 207, 513, 628,

638

rent, 18

servitude ends, 513
Jesus, 523 (also see Christ)
