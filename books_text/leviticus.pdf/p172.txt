116 LEVITICUS: AN ECONOMIC COMMEN'TARY

nature of the sin determined the nature of the sacrifice. Then
how could God maintain the principle of proportional pain? Had
the sacrifice been a specified amount of money, either the rich
man would have paid too little proportional to his economic
benefits in life or the poor man would have paid too much.
The penalty would not have been proportional.

There is no way to sacrifice one-third of an animal without
killing it. This is ithe problem of sacrificial discontinuity. Thus,
proportional restitution to Ged is not possible in a world that
requires a single type of animal sacrifice. If killing a lamb or
goat is the only legitimate way to placate God, then both the
rich man and the poor man have to pay it. But this would
violate the biblical principle of greater responsibility on the part
of those possessing greater wealth.

The problem of sacrificial discontinuity is reflected in the
specified sacrificial animals in Leviticus 5: lambs or goats, a pair
of birds,” or fine flour and oil. The payment for sin to God
(as distinguished from an earthly victim) was not to be made in
terms of money, except by someone who was willing to pay an
extra 20 percent to buy back (redecm) the animal. The wealth
(capital) of the sinner was to determine which animal he was to
sacrifice, or even if he was to sacrifice an animal. The poor man
could legitimately sacrifice fine flour and still meet the judicial
requirement, but the sacritice had to impose pain on the sin-
ner. The sacrifice was to reflect or represent the intensity of the
negative sanction he was avoiding, on earth and in eternity.

Rich Man, Average Man, Poor Man

The tripartite division that we commonly make in class anal-
ysis — upper, middle, and lower — is reflected in this passage.
The idea that each wealth group was bound by differing ritual
obligations pointed to the biblical principle of present obligations

18. One for a purification offering and the other for a burnt offering: Wenham,
Leviticus, p. 100.
