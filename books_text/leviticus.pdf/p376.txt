320 LEVITICUS: AN ECONOMIC COMMENTARY

unsystematic approaches." There is no way to test the accura-
cy of this presumption except by observing God’s sanctions in
history on those groups that are under the authority of specific-
ally covenanted judges.”

God is the Supreme Judge. Ile makes objective judgments,
yet He makes them subjectively. We must think THis thoughts
after Ilim. We must render subordinate judgment. We must
not cheat. The free market’s competition establishes limits to
cheating by sellers of goods and services, who are more capable
of cheating, given their specialized knowledge of what is being
sold."° Sellers will compete against sellers, thereby improving
the public’s access to honest standards.” In a similar way, a
form of competition — immigration — brings “buyers of justice”
into the courtrooms of honest judges.?' Honest courts that
predictably enforce law reduce the costs of justice and thereby
create conditions of prosperity’ But when the rules are in
flux, the quest for power through politics increases.” The
rights of victims are no longer upheld.

Eternally objective standards of justice are found in the
Bible, not in natural law. All other standards are subject to the
flux of history and man’s subjectivity,” God declares the truth
objectively.

17. Iid., ch. 19, section on “Intuition and Measurement,” subsection on “Intu-
ition and Creation.”

18. If God's sanctions in history are random in the New Covenant era, as
Meredith G. Kline insists that they are, then there is no way to test this presumption.
Intuition-based decisions would become as random in their effects as God’s historical
sanctions supposedly are. See Meredith G. Kline, “Comments on an Old-New Error,”
Westminster Theological Journal, XLI (Fall 1978), p. 184.

19. North, Boundaries and Dominion, ch. 19, section on “Objective Standards.”
20. Zbid., ch. 19, section on “Competition and the Margins of Cheating.”

21. Iid., ch. 19, subsection on “The Scales of Justice.”

29. Ibid., ch. 19, section on “A Final Sovereign.”

23. Ibid., ch, 19, subsection on “Justice in Flax.”

24. Ibid., ch. 19, section on “Victiin’s Rights and Restitution.”

25, Tbid., ch. 19, section on “Evangelical Antinomianism and Humvanism’s Myth,
of Neutrality”
