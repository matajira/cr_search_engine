604 LEVITICUS: AN ECONOMIG COMMENTARY

this output would have required a quarter of an acre. For a
small farm - say, 10 acres - this scems like a reasonably sized
plot to dedicate to the priesthood.

If we are discussing seed inputs, a modern farmer can get
almost a 20-to-one increase from seeds planted. This ratio of
output to input would have been far less in ancient Isracl, but
still the amount of acreage necessary to seed (input) one homer
of barley would have been quite small. It therefore secms more
likely that the text refers to output rather than input: the land
required to produce one homer of barley.

Lhe Economics of the ‘Translators’ Version

Were the King James and other versions’ translators correct?
Does the reference to 50 shekels mean “50 shekels per homer”
rather than “50 homers of barley per jubilee cycle,” i.e., one
shckcl of silver times 50? If the translation is correct, this re-
demption price was astronomical: 50 times the average market
price of a homer of barlcy, plus 20 percent. But this would
have been only the beginning of the redemption burden. The
field’s potential output of barley per year was then multiplied
by 44: the years of production remaining until the next jubilee
year. So, the total number of homers of barley that a lield could
produce was multiplicd by 44 ycars, and this gross output
figure was then multiplied by 50 shekels. There was a prorated
reduction in price in terms of the number of years remaining
until the jubilce, but with these huge payments, such prorating
would have been economically irrelevant to most Israelites.

What was the redemption payment all about? It covered the
case of a person who had vowed to transfer a field or a field's
output to a priest. At some point before the jubilee, the original
owner decided to reclaim the field for himself. To do this law-
fully, he had to pay a cash redemption price to the priest at the
time of the reclaiming. If the formal redemption price was
established at 50 shekels per homer of barley, as the familiar
translations suggest, then the typical owner could afford to
