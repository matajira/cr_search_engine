 

410 LEVITICUS: AN RCONOMIC GOMMENTARY

The Day of Atonement

In the jubilee year, trumpets were to be blown throughout
the land on the tenth day of the seventh month. This marked
the great year of releasc. It was also a day of rest because it was
the day of atonement (Lev. 23:28). The next day, men dwelling
near the borders of Israel had to begin their walk to Jerusalem
to celebrate ‘labernacles (booths): the feast of ingathering. In
no more than four days, they had to complete their journey.
This time requirement restrained any major extension of the
geographical boundaries of Israel. On the day above all other
days in Israel's life that was Ged to geographical boundaries —
jubilee’s day of landed inheritance - the timing of the jubilec
and Tabernacles established tight limits on the size of the na-
tion. Israel could never become an expansionist territorial empire and
still honor the day of atonement (vest), the jubilee year (inheritance),
and the feast of Tabernacles (celebration). When the Israelites
walked to Jerusalem in the jubilec year, all but the Levites went
as rural land owners and citizens, even urban dwellers who had
leased out their land to others. But they could never lawfully
walk from an inheritance located very far from Jerusalem. If
Israel ever became an empire, Israelites living near the outer
boundarics would forfcit their inhcritance in the original land.*

The year of jubilee was to begin on the day of atonement
(yom kippur). The theological significance of this is readily appar-
ent: the day of atonement was the day on which the people of
Israc] made a formal public acknowledgment of their depen-
dence on the grace of God in escaping from God’s required
punishment for sin. There had to be an animal sacrifice as part
of this formal worship ceremony. It was a day of affliction:
death for an animal and public humility for the participants.

6, ‘The reader may think: “This is obvions. Whatis the big deal?” ‘Sry to find any
commentary on Leviticus that. discusses the relationship between the timing of the
day of atonement-jubilcc and the growth of empire, The silence of the commentators
is testimony to their unwillingness to take the Bible's literal texts seriously (theological
liberalism) ot’ to take political theory seriously (theological conservatism).

 
