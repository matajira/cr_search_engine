142 LEVITICUS: AN EGONOMIC COMMENTARY

offer 70 bullocks annually (Num. 29:12-32) as sacrifices for the
symbolic 70 pagan nations of the world (Jud. 1:7), plus a single
bullock for Israel on the eighth day (Num. 29:36).

The Church: Guardian of the Civil Oath

What this means is that the church is the guardian of the
covenantal civil oath. This is an inescapable conclusion from the
fact that only the church has the authority to aceept the perjur-
er’s sacrifice in atonement for the false oath. The Statc cannot
offer this release from guilt. The oath involves the formal call-
ing down of God’s negative covenant sanctions on the oath-
taker. He who uses God’s name in vain in a formal judicial
conflict must then seek judicial cleansing from the church. The
reason why the oath is guarded by the church is that the
church alone can lawfully invoke the ctcrnal negative sanctions
of God against an individual.“ Thus, by invoking the oath in a
civil court, the criminal necessarily brings himself under the judicial
authority of the church.

The modern practice of allowing atheists to “affirm” to tell
the truth in court, but not to swear on the Bible or in God’s
name, is a direct affront against God and against the church as
the guardian of the oath. It is also inevitably an act of divinizing
the State by default. The State becomes the sole enforcer of the
public affirmation. In such a worldview, there is no appeal in
history beyond the State and its sanctions. The atheist’s affirmation
is therefore a judicial act demanding the removal of God from the
courtroom. Thus, it requires the creation of a new oath system,
with the State as the sole guardian of the oath. The State acts
not in God’s name but in its own, Rushdoony’s comments are

13. When Israel fell in A.D. 70, she had become like all the other pagan nations.
She could no longer offer efficacious sacrifices for them or for herself. From that
point on, only the sacrifice of Jesus Christ at Calvary could serve as any nation’s
alonement — covering or ransom — before God.

14, Gary North, The Sinai Strategy: Economics and the Ten Commandments (Clyler,
“Texas: Institute for Christian Kconomics, 1986), pp. 52-56.

 
