God's Escalating Wrath 573

The temptation is always disobedience to God’s standards
(point three of the biblical covenant model). “But if ye will not
hearken unto me, and will not do-all these commandments;
And if ye shall despise my statutes, or if your sou! abhor my
judgments, so that ye will not do all my commandments, but
that ye break my covenant.” ‘This necessarily involves the threat
of negative sanctions (point four). “I will cven appoint over you
terror, consumption, and the burning ague, that shall consume
the eyes, and cause sorrow of heart.” ‘he essence of this sanc-
tion is disinheritance (point five). “And ye shall sow your seed
in vain, for your enemies shall cat it.” Invaders will inherit:
“And I will set my face against you, and ye shall be slain before
your enemies: they that hate you shall reign over you; and ye
shall flee when none pursueth you.” So fearful will God’s peo-
ple become that they will flee when none pursue.

‘The covenantal issue is the fear of God. When men refuse to
fear God, He raises up others who will terrify them. Covenant-
breakers will thereby learn to fear God’s human agents of
wrath, so that they might better learn to fear God. The poimt is
this: God is worth fearing even more than military invaders. If
the stipulations of the Creator arc widely ignored, then military
invaders will become increasingly difficult to ignore. In this
regard, the covenant-breaking adult is as foolish as a child. A
father spanks a child when the child runs into a busy street.
The rea! threat to the child is the street’s traffic, but the child
is fearless before this external threat. [le must learn to fear his
father in order to learn the greater fearfulness of the street. He
fears the lesser threat morc than the greater threat. Similarly,
the covenant-breaker loses his [ear of the Father — the far great-
er threat - and must be reminded to fear God by a lesser exter-
nal threat. The magnitude of God’s wrath is manifested by the
magnitude of the threat of military sanctions: God’s wrath is
more of a threat than a military defeat. The lesser threat is
imposed by God in order to remind men of the greater threat.
