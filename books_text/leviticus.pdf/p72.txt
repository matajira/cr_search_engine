16 LEVITICUS: AN ECONOMIC COMMENTARY

Pentecost ended. The costs of making the journey were high;
the time in Jerusalem was brief; perhaps many people stayed
behind to offer these special sacrifices. But Pentecost was a
summer festival, when agricultural time is most valuable. For as
long as Israel remained predominately agricultural, there
would have been economic pressure to return home immedi-
ately after Pentecost. Also, the pressure of so many visitors at
any festival would have raised food and housing costs. less
busy periods were less expensive, but to take advantage of this,
the sacrificer would have been required to make another jour-
ney to the temple. In short, the costs of sacrifice were very
high. Conclusion: faithful people would have been very careful
to obey the details of God's law, just to avoid an extra journey
to the tabernacle-temple to make a sacrifice for having violated
some detail. In the phrase of modern political theory regarding
men’s exodus from tyranny, Israclites voted with their feet. The
marched for liberty. In their case, however, they voted for
God’s covenant order with their feet, not against it.

The Challenge to Tribalism

‘There is another aspect of the three marches, but especially
Passover, that must be considered: the mitigating eflects on
tribalism. The three feasts were national celebrations. Clans and
tribes from across the nation were required to mect together in
one city: the carthly dwelling place of God where the sacrifices
had to be conducted. Loyalty is ultimately to God and His law.
This cross-tribal loyalty was to be demonstrated at the national
feasts.”

When all the familics of Isracl journeyed to Jerusalem,
young adults of marriageable age could meet each other: those
of the opposite sex who were members of different tribes.’*

17. The feast still promotes unity across judicial boundaries, acknowledged or
not: at the communion table.

18, “he denominational college or Bible college has long served a similar
