SACRIFICE, STEWARDSHIP, AND DEBT

And the Lorp called unio Moses, and spake unto him out of the
tabernacle of the congregation, saying, Speak unto the children of Israel,
and say unto them, If any man of you bring an offering unio the Lorp,
ye shall bring your offering of the catile, even of the herd, and of the
Flock. If his offering be a burnt sacrifice of the herd, let him offer a male
without blemish. he shall offer it of his own voluntary will at the door of
the tabernacle of the congregation before the Lorp. And he shall put his
hand upon the head of the burnt offering; and it shall be accepted for
him to make atonement for him (Lev. 1:1-4).

To understand any biblical law, we should first seek to dis-
cover its theocentric meaning. What does a law have to do with
God and His relation to man? James Jordan argues that the
whole burnt sacrifice symbolized the death of the sacrificer. This
death was imputed judicially to the animal. The animal became
covenantally dead. The sinner’s liability to death was trans-
ferred to the animal. Death must be dealt with before sin is.
Jordan writes: “What the sacrifice removes is not sin but death,
the judgment for sin. Death having been removed, it is now
possible to live a righteous life.”! The effect of this transfer of

1. James Jordan, “The Whole Burnt Sacrifice: Its Liturgy and Meaning,” Biblical
Horizons Occasional Paper, No. 11, p. 4. For a more extensive extract from Jordan's
