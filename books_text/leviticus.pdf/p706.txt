650 LEVITICUS: AN EGONOMIC COMMENTARY

fact left us judicially defenseless. If we cannot appeal to God’s
justice, as manifested in His Bible-revealed law, to what should
Christians appeal? The dispensationalist answers, “the Rap-
ture.” The amillennialist answers, “the end of history.” But
what happens to us if either event is delayed?

I answer: if we cling to a hermeneutic of personal judicial
discontinuity, we should prepare for negative corporate sanctions.
“Salt is good: but if the salt have lost his savour, wherewith
shall it be seasoned? It is neither fit for the land, nor yet for the
dunghill; but men cast it out. He that hath ears to hear, let him
hear” (Luke 14:34-35).

Conclusion

For two decades, the critics of theonomy have issued this
challenge: “Prove your case exegetically.” Rushdoony’s first
volume of The Institules of Biblical Law (1973) was theonomy’s
frontal assault. He suggested hundreds of ways in which Mosaic
case laws still apply. He used the ‘Ten Commandments as his
integrating principle. Bahnsen’s Theonomy in Christian Ethies
(1976) provided a technical apologetic defense of theonomy,
written in the arcane language called “theologian.” It has re-
ceived more attention — most of it negative — from the theolo-
gians than Rushdoony’s /ustitutes because Bahnsen writes fluent-
ly in their adopted tonguc, which the rest of us have difficulty
following without a dictionary and a grammar handbook. I
showed in Tools of Dominion (1990) how the case laws of Exodus
still apply to economics and civil justice. These books all em-
phasized continuity.

Our critics have not been satisfied. They have continued to
complain: “You say that you have a hermencutical principle of
discontinuity. Let us see it in action.” They have implied that

(Portland, Oregon: Multnomah Press, 1988), pp. 127, 180. For a reply, see Greg L.
Bahnsen and Kenneth L. Gentry, Jr., House Divided: The Break-Up of Dispensational
Theology (Tyler, Texas: Institute for Christian Economics, 1989), pp. 93-96.
