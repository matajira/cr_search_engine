Poreword xv

tional authors do not expect their readers to be around long
enough to wear out paperback books, let alone hardback books.
Neither do their readers. That they will not wear out their
paperback books is a safe assumption, but it has nothing to do
with the timing of Christ’s Second Coming. It has everything to
do with the reading habits and attention spans of most dispen-
sational readers. A movement based on such reading habits and
short attention spans is not going to be dominant indefinitely.’
It will be replaced.

Applying the Bible’s Texts Today

I am targeting an audience that is not yet in existence. ‘This
nonexistent audience is the future leadership of Christianity. At
some point, there will be an unprecedented Christian revival.
The Holy Spirit will make His worldwide move.* Many will be
called, and many will be chosen.® One of the results of this
worldwide revival will be the revival of the ideal of Christen-
dom: the civilization (kingdom) of God in history. Christianity will
eventually possess sufficient judicial authority, by means of
Christian candidates’ popular election to political office and
their clection or appointment to judicial office, to begin to
apply God’s Bible-revealed laws to civil government. That victo-
rious generation and the generations that will follow it will need
a great deal more than a 200-page commentary. Those future
generations will nced many commentaries like this one: com-
prehensive within a specialized field of study. I want Boundaries
and Dominion to become a model for those future commentaries
in such fields as education, social theory, and political theory.
Until such studies exist, and exist in profusion, Christianity will

4, Gary North, Rapture Fever: Why Dispensationalism Is Paralyzed (Tyler, Texas:
Institute for Christian Economics, 1993).

5. Kenneth L. Gentry, Jn, He Shall Have Dominion: A Pastmillennial Eschatology
(lyler, Texas: Institute for Christian Economics, 1992).

8. Few havc bcen chosen so far over the history of mankind’s time on earth; this
does not prove that few will be chosen in every generation.
