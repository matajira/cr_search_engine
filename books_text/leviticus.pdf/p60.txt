4 LEVITICUS: AN ECONOMIC COMMENTARY

image (Gen. 1:26), he is not God, nor does he participate in
God’s being. Man is commanded to be holy, for God is holy
(Lev. 11:44-45; 19:2), but man is also warned not to seek divini-
ty for himself (Gen. 11:6; Deut. 29:29; Job 38-41). Man is com-
manded to seek ethical unity with the perfect humanity of Jesus
Christ, God incarnate, but man cannot attain ontological unity
with God. A permanent boundary is placed between God’s
being and man’s being. The unity between God and man is to be
ethical, never ontological or metaphysical.

The doctrine of the Creator/creature distinction has enor-
mous consequences for social theory and practice. A contempo-
rary Jewish political scientist has correctly obscrved: “The boun-
dary between God and man is His supreme safeguard against
social chaos. For what would men not do to one another if they
were to claim ultimate authority?*" When covenant-breaking
men have sought to erase this divine-human boundary, they
have reaped their appropriate reward: social chaos followed by
tyranny. Twentieth-century Europe is a monument to this
reality: World War I, Communism, Nazism, Italian Fascism,
World War II, the Cold War, and the break-up of Yugoslavia
in civil war after 1990. In addition to the politics of despair
have come existentialism, nihilism, the self-conscious meaning-
less of modern art; pornography, the drug culture, and the
mindlessness of hard rock music. The laws of Leviticus were
designed to remind men not to erase the divine-human bound-
ary. The Mosaic law was designed to avoid social chaos and
tyranny. It established laws - boundaries — governing the rela-
tionships between men in order to remind men of the ultimate
boundary between God and man.

This leads me to a very important point: any attempt to
define Christian “relationships” apart from God's Bible-revealed

4, Aaron Wildavsky, The Nursing Father: Moses as a Political Leader (University,
Alabama: University of Alabama, 1984), p. 97. Professor Wildavsky died before 1
completed this manuscript. I had hoped to send him a copy of the book. He was one
of the great conservative academic scholars in this century.
