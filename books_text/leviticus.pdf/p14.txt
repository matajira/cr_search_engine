xiv LEVITICUS: AN FCONOMIGC COMMENTARY

what book reviewing might have been like before ihe invention
of the printing press. Here is how he imagines an early book
industry report on the sales of Leviticus:

Highly publicized diet book published under the title Leviticus. Sales
flop. “Too many rules, too depressing, not enough variety, not enough
attention to cholesterol,” cry the critics. “And for crying out loud, give
it a decent title.”*

This parody is not too far from the opinion of the average
reader who has started but not finished Leviticus. He sees it as
a kind of “healthful living” diet book. It isn’t.

Then what is Leviticus all about? It is a book about limits:
boundaries. There are a lot of boundaries laid down in the Book
of Leviticus. Some of these limits are liturgical, Others are
familial. Some are tribal. Some are dietary. ‘There are also limits
that have to do with the status of the Promised Land as God’s
holy place of residence. Finally, a lot of them establish econom-
ic limits. | have discussed these applications at considerable
length, especially the economic ones. This is why Boundaries and
Dominion is longer than Tools of Dominion.

I offered several pages of reasons to justify the length of
Tools of Dominion in its Introduction. I have adopted what I call
a “fat book” strategy. A movement that seeks to change the
world cannot make its claims believable with only short books.
The world is much too large and much too complex to be
capable of being restructured in terms of large-print, thin pa-
perback books ~ the only kind of books that most Christians
read these days. The best that any movement can expect to
achieve if it publishes only short books is to persuade readers
that the world cannot be changed. ‘This is why contemporary
dispensationalism is limited to short paperback books. Dispensa-

3. Tom Raabe, Biblisholism: The Literary Addiction (Golden, Colorado: Kulerum,
1991}, p. 39.
