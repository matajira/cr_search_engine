Guardian of the Civil Oath 143

on target: “If a witness is asked to swear to tell the whole truth
and nothing but the truth without any reference to God, truth
then can be and is commonly redefined in terms of himself.
The oath in God’s name is the ‘legal recognition of God’® as
the source of all things and the only true ground of all being.
It establishes the state under God and under His law. The
removal of God from oaths, and the light and dishonest use of
oaths, is a declaration of independence from Him, and it is
warfare against God in the name of the new gods, apostate man
and his totalitarian state.”"*

The biblical State can lawfully impose negative sanctions
against a perjurer, but only on behalf of the victim. The State
cannot lawfully pronounce the permanent negative sanctions of
the oath against anyone. The State can lawfully require an oath,
but it is not the institutional enforcer of this oath. The presence
of the oath to God is a public acknowledgment of the non-
autonomy of the State. God is above the State, and the church
stands next to it as the guardian of the oath.”

‘his means that theocracy is required by God’s civil law.
Without the God-given authority to require an oath, the State
would lose its covenantal status as a lawful monopolistic institu-
tion with the authority to enforce physical sanctions against evil-
docrs. It would lose its status as a covenantal institution. Yet by
imposing an oath, it inevitably places itself under the protection
of the church, for the church is the defender of the oath. As the
great seventeenth-century jurist Sir Edward Coke put it, “pro-
tection draws allegiance, and allegiance draws protection.”"*

15. ‘L Robert Ingram, The World Under God's Law (Houston, ‘lexas: St. Thomas
Press, 1962), p. 46.

16. .R. J. Rushdoony The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 115.

17. The State, in turn, is responsible for the preservation of the legal environ-
ment that protects the church. The church is not institutionally autonomous, either.

18. Cited by Rebecca West, The New Meaning of Treason (New York: Viking Press,
1964), p. 12; in Rushdoony, /nstituées, p. 118.
