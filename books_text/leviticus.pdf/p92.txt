36 LEVITICUS: AN ECONOMIC COMMENTARY

biblical case law as a Bible-revealed statute that reveals a gener-
al principle of biblical law in a specific case. Rushdoony writes
that “the law, first, lays down broad and basic principles,” but
there is also “a second characteristic of Biblical law, namely, that
the major portion of the law is case law, i.c., the illustration of
the basic principle in terms of specific cascs. These specific cases
are often illustrations of the extent of the application of the law;
that is, by citing a minimal type of case, the necessary jurisdic-
tions of the law are revealed.” God has provided us with case
laws in advance in the form of legally binding statutes. A casc law
illustrates a general legal principle, making this principle clear-
er by making it specific. God, as the sovereign Legislator, is also
the sovereign Judge. Thus, biblical laws. are simultaneously
statute laws and case laws.

This usage does not conform to legal terminology in the
United States. Lhe modern humanist legal theorist defines a
case law as a judge-made law that serves as a legal precedent.
He regards case laws as the products of specific legal disputes,
in contrast to statute laws cnactcd by legislatures. The modern
dichotomy between case laws and statute law reflects the dichot-
omy between humanistic English common law, which floats on
legal precedents announced by self-proclaimed autonomous
judges, and Continental Europe's humanistic Napoleonic code,
which floats on legal enactments announced by self-proclaimed
autonomous legislatures.” Ultimately, this dichotomy reflects
the autonomy in all humanist thought between historical flux
and fixed principles of logic: Heraclitus (“all is in flux”) vs.
Parmenides (“logic is constant”). Neither approach solves the
problem of discovering binding fixed principles of law thal can
be applied to a changing world. ‘he Bible provides this; hu-
manistic law schools do not.

73. R. J. Rushdoony The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. LL.

4. CFA. V. Dicey, Introduction to the Study of the Law of the Constitution (8th eds
Indianapolis, Indiana: Liberty/Classies, [1915] 1982), esp. Chapter 12.
