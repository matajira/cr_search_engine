citizenship &, 465
civil, 258-59
estimation, 582
excommunication, 359
gleaning &, ch, 22
houschold, 301, 302
inheritance, 643
judicial prices, ch, 36, 614-
17
nation of, 6
ritual washing of, 149
rural land owners, 606-7
sanctification agents, 596
also see high priest; priest-
hood
prison, 527
producers, 437-39
productivity
agricultural, 431
aliens’, 531-32
condition of citizenship, 391
division of labor, 214 , 220
inequality of, 245
monitoring, 112-13
profane
Adam, 139
blasphemy, 369-70
common &, 122-26, 129
false definition, 121-22
family meal?, 125
God’s name, 128, 179, 210,
212, 325
holy things violated, 121-26
land of Israel, 297, 300
Mammon, 129
pierced, 125
slain, 125
tithes, 130

uncircumcision, 295

violation, 123, 125
profit, 219, 535, 537, 539
Profumo, John, 379n
progress, xii, 127-28
progressive sanctification, 78-

79, 83, 86, 132, 321, 430
Promised Land’s boundary, §
promises

bonds &, 210-11, 219

cooperation &, ch. 12

God’s, 10, 340

peace & food, 449-50

also sce oath; bond
property

book of, 10-11

boundaries, 43, 486

boundary, 209, 276

crime, 136

eighth commandment,

179, 213

legitimate, 213

military peace, 415

residual, 61

rights, 43, 179, 265-66,

276

trust, 265

wealth &, 265

wine, 153
prophet, 377-78, 391
proportionality, 107-18
prosperity, 10, 450, 459, 555
prostitution, 65
Protestantism, 106
public choice theory, 361-63
public health, 173
purification offering

cleansing, 89
