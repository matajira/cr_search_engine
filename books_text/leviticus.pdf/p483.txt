Boundaries of the Jubilee Land Laws 427

the New Covenant’s place of sanctuary — not merely the institu-
tional church, but the civilization of God. ‘Vhe whole world of
paganism is required by God to seek sanctuary in Christ’s
church. This substitution of a new sanctuary annulled the jubi-
lee land laws, and thereby also annulled the jubilee’s perma-
nent slave law.

The alternative to this interpretation of the New Covenant is
the long-held defense of slavery made by Christian commenta-
tors. Their interpretation — never explicit but necessarily implic-
it — is that the annulment of the jubilee land laws did not also
annul the slave law. This leads to the conclusion that Gad’s law
no longer makes provision for those sccking geographical sanc-
tuary. In other words, when national Isracl ceased to offer
sanctuary to the lost or the righteous foreigner, geographical
sanctuary ccased in history. The argument runs as follows:
“The Israclites no longer possessed a guarantee of jubilee liber-
ty; therefore, the liberty announced by Christ must have consti-
tuted the annulment of Mosaic liberty. God has annulled the
land-sanctuary-liberly connection, but nothing has taken its
place. Thus, slavery is validated as a universal institution.”

The only New ‘Iestament-based alternative to this unpleasant
interpretation is to conclude that liberty has been validated by
the work of Jesus Ghrist, and the mark of this validation is the
abolition of slavery in Christian nations. he church has never
publicly acknowledged the abolitionist implications of Jesus’
fulfillment of the jubilee law. His announcement was not, to my
knowledge, ever cited by any abolitionist of the latc cighteenth
and nincteenth centuries. But alicr 1780, pressure to abolish
slavery increased within many Anglo-Saxon Protestant churches
located outside of the slave-owning regions. By the end of the
1880's, slavery had been abolished in the West.

Meanwhile, national sanctuaries for the oppressed and poor
were opened: free emigration and immigration. But after
World War I, this open access was stcadily closed by legislation.
Immigration barriers were erected everywhere. The modern
