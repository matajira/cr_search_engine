430 LEVITICUS: AN ECONOMIC COMMENTARY

train landless Israelites and poor strangers how to produce for
a market. The Mosaic law identified harvesters as landless or
impoverished people who worked as harvesters or gleaners in
six years out of seven. In sabbatical years, they became depen-
dent on whatever it was that God would allow the fields to
produce apart from cultivation. In those years, harvesters
learned to make decisions without a land owner or his supervi-
sor ruling over them.

The jubilee inheritance law applied to rural jand inside the
boundaries of Israel. It did not apply to houses within the
walled cities of the nation except Levitical cities (Lev. 25:32-33).
It also did not apply to property outside the Promised Land.
‘This law had been given to the people by God because He was
the owncr of the land (Lev. 25:23). It was part of the terms of
God’s lease under which they held rights of administration as
sharecropping tenants, with 10 percent of any increase owed to
God through the Levites and priests. It was also part of the
spoils of war.

A Question of Sanctification

God is owner of all the earth, not just the Promised Land.
Why did the jubilee laws not apply to all other nations? Be-
causc these laws applied only to His special dwelling place.
‘They were an aspect of God’s holiness, which is why the jubilee
laws appear in Leviticus, the book of holiness. The Promised
Land was to be kept holy: set apart judicially from all other
nations. How? Initially, this separation began with God’s prom-
ise Lo Abraham: definitive holiness, i.c., definitive sanctification.

‘The second phase of the process of separation began with
the conquest: progressive holiness, i.c., progressive sanctifica-
tion. God cleansed the land of His enemies by means of total
war: the annihilation of His cnemics. He required the extermi-
nation of the gods of Canaan by means of an original program
of genocide. He promised to dwell in the land that contained
the tabernacle and temple; He would not permit any other god
