Economic Oppression by Means of the State 441

someone clsc. A second person agrees to make a cash payment
or else a periodic payment to the person who leased the equip-
ment first. ‘I'he person who leased the asset first has now be-
come a recipient of money income. It is now the same as if he
had purchased a bond in the first place instead of leasing a
piece of equipment from someone clsc. He now owns a piece of
paper issued by a third party who promises to pay him in the
future. So, there is no economic difference between buying a
stream of net future income in the form of a piece of capital
equipment or a written promise to pay (LOU).

Economic Oppression

The text warns against becoming an economic oppressor.
What must be recognized from the beginning is that in the case
of buying and selling rural land in Israel, economic oppression was
@ two-way street. Whether a person was a seller of land (buyer of
moncy) or a buyer of land (seller of money) — ie., whether a
lessor or lessee — he could become an oppressor, according to
this passage. “And if thou sell ought unto thy neighbour, or
buyest ought of thy neighbour's hand, ye shall not oppress one
another” (Lev. 25:14). This should warn us against any thought
that the potential oppressor is always a buyer of some asset, or
that a seller is always the potential oppressor.

This is especially relevant with respect to buyers of labor
services (sellers of money) and sellers of labor services (buyers
of money). It has been assumed by those who favor civil legisla~
tion that “protects labor” that employers are almost always the
oppressors. Similarly, it has been assumed by those who oppose
trade unions that the unions are normally the oppressors. Nei-
ther assumption is valid. What is valid is the conclusion that
when the civil government interferes in the competitive market
process of making voluntary contracts, the group favored by the
legislation becomes the economic oppressor. This oppression is estab-
lished by positive sanctions (subsidies) and negative sanctions
(restraints against trade). The element of civil compulsion is the
