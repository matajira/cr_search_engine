502 LEVITICUS: AN ECONOMIC COMMENTARY

slave? If his kinsman-redecmer wanted to buy him out of bond-
age, how much did he owe the buyer? The prorated price of
the jubilee year did not apply if he was not entitled to go out in
the jubilee.

Let us consider modern business practice. If a man buys an
intcrest-paying instrument at face valuc in order to receive a
guarantecd income, and the company issuing the bond posscss-
es the right of redemption, the company must repay the face
value of the bond in order to cancel the debt. The buyer has
received guaranteed income from the asset in the meantime.

‘The economic difference between a bond and a bondservant
is that the buyer is not sure how much net income the bond-
servant will produce. The bond pays a guaranteed rate of re-
turn. It is purchased at a discount from its face value. The
discount is based on the prevailing rate of wterest. The face
value - redemption price — of the bond and today’s rate of
interest are known in advance. The price and the rate of return
can be calculated.

There is no guaranteed rate of return for a bondservant.
The buyer must estimate the future net income from a bond-
servant. Then he must discount this by the prevailing rate of
interest. The higher the estimated net income, the higher the
market price. But how long will he retain control over the
bondservant? Unlike a bond, there is no fixed time period.
Unlike a bondservant under the protection of the jubilee, there
is no fixed time period. There must be a way to reduce the
number of variabies, so that the victim gets paid. But how?

The higher the estimated value of the criminal’s productivity
as a servant, the higher the price he will brmg. This means that
a criminal with a good work ethic is less likely to be able to
escape servitude; his redemption price will be too high. This is
contrary to biblical law: a subsidy for evil. There must be a way
around this anomaly. But what?

The solution solves both problems: 1) too many variables
and 2) the subsidy for evil. His legal redemption price must be
