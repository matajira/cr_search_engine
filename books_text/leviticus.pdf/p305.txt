15

LOCAL JUSTICE VS.
CENTRALIZED GOVERNMENT

36 shall do no unrighteousness in judgment: thou shalt not respect the
person of the poor, nor honour the person of the mighty: bul in righteous-
ness shalt thou judge thy neighbour (Lev, 19:15).

Leviticus 19:15 deals with more than just the principle of
impartial civil justice; it also deals with the locus of civil judicial
sovercignty: “Ye shall do no unrighteousness in judgment: thou
shalt not respect the person of the poor, nor honour the person
of the mighty: but in righteousness shalt thou judge thy neigh-
bour.” This law established the requirement that the citizens of
Israel from timc 1o time be required to serve as civil judges in
their communitics (Ex. 18:21-22). The focus of Leviticus 19:15
is on civil courts within the focal community, although the
principle of equality before the law also applies to ecclesiastical
courts. The verse specilically says, “in righteousness shalt thou
judge thy neighbor.” There is a very strong emphasis on ethics:
righteousness. There is also a very strong emphasis on localism
in this verse: judging a neighbor.

‘Two issues are fundamental in this verse: equality before the
law and local judicial participation. First, equality before the law:
this points back to Exodus 12:49, where the law of God is iden-
