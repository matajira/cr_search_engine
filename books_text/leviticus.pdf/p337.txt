The Preservation of the Seed 281

Case Laws and Underlying Principles

A law governing agriculture, animal husbandry, and textile
production had to be taken very seriously under the Mosaic
Covenant. The expositor’s initial presumption should be that
these three laws constitute a judicial unit. If they are a unit,
there has to be some underlying judicial principle common to
all three. All three prohibitions deal with mixing. The first
question we need to ask is the crucial one: What was the coven-
antal meaning of these laws? The second question is: What was
their economic effect?

I argue here that the fundamental judicial principle under-
girding the passage is the requirement of separation. Two kinds
of separation were involved: tribal and covenantal. The first two
clauses were agricultural applications of the mandatory segrega-
tion of the tribes inside Israel until a unique prophesied Seed
would appear in history: the Messiah. We know who the Seed
is: Jesus Christ. Paul wrote: “Now unto Abraham and his seed
were the promises made. He saith not, And to secds, as of
many; but as of one, and to thy seed, which is Christ” (Gal.
3:16). The context of Paul’s discussion is inheritance. Inheritance
is by promise, he said (Gal. 3:18). The Mosaic law was given,
Paul said, “till the seed should come to whom the promise was
made” (Gal. 3:18). Two-thirds of Leviticus 19:19 relates to the
inheritance laws of national Israel, as we shall see. When the
Levitical land inheritance laws (Lev. 25) ended with the estab-
lishment of a new priesthood, so did the authority of Leviticus
19:19,

The final clause of Leviticus 19:19 deals with prohibited
clothing. This prohibition related not to separation among the
tribes of Israel — separation within a covenant — but rather the
separation of national Israel from other nations. The principle
undergirding second form of separation — clothing - is more
familiar to us: covenantal separation.
