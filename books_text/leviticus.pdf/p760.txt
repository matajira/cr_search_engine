704 LEVITICUS: AN ECONOMIG COMMENTARY

casuistry, 260, 649

Christ, xxxi-xxxii, xxiii

civil, 235, ch. 14 (also see
State)

cleanliness, 150

coherent system, 524

comparative advantage, 22

contempt for, 389

content: OT & NT, xxxii

continuity, xxx, xxxiv, xxxvi

cross-boundary (see cross-
boundary laws)

death sentence, 7

declaration, 258

defensive shield, 270

dietary, 183, 341-47 (also
see diet; food)

diminishing returns, 562

distrust of, xix

division of labor, 253-56,
260

dominion &, 77

enforcement, 104

equality before (see equality:

legal)
family, 549
follows grace, 9-10
freedom and, 555
general covenant (Adam),
631-33
hermeneutic, 279
ignored by Israelites, 20
immunities (rights), 265
impartial, 236, 551
inheritance, 280
interpreters, 153-54
jurisdiction, 251-52, 256-57
jury Gee jury)

king &, 156

king reads, 258

labor, 441-42

land, 638-41, 653

land grant, 9

landed inheritance, 359

language of, 319

Levitical, 6

libel, 378

liberty, 309 (also see
freedom)

life &, 7

localism, 250-53

love &, 263-64, 311-13, 550

magic, 64

Moore’s, 563n

murder, xxxvi

natural, 97, 237, 276, 579,
633, 636-37, 649-50

“of 73,” 419

opinions regarding, 246

predictable, 253

priesthood, 279, 346

priestly, 72, 549, 642-43

quantification, 315, 318-19

read to all, 271

rejection, 33

resurrection, 180, 524

revolution &, 416

rule of, 311

sacraments &, 280

sanctification &, xix

sanctions, xii, 310, 572,
577-78 (also sec State:
sanctions)

seamless, 578

seed & land, 335

seed law (see laws)
