608 LEVITICUS: AN ECONOMIC COMMENTARY

output of the dedicated field. But if, by the time of the jubilee,
he had refused to redeem the land by the payment of one
shekel of silver for every year of the eviction, plus 20 percent,
he lost ownership of the land.

The priests had the possibility of inheriting rural land if the
vow-designated land was not redeemed by the vow-taker. In
such cases, the potential beneficiaries obviously had an econom-
ic incentive to oppose the debascment of the shekel (Isa. 1:22).
A shekel of falling value would have made it less expensive for
those who faced the permanent loss of their land to redeem it
prior to the jubilee.

Would the owner of rural land ever have dedicated all of its
outpul to a pricst? Not unless he was willing to risk disinherit-
ing his sons. If he was subsequently forced by economic pres-
sures to reclaim the land’s output, and then he or his sons
failed to redeem the land at the mandatory price, plus 20 per-
cent, all of his land would go to the priest in the jubilee year.
Thus, there was an economic restraint on the over-commitment
of land to the priesthood. The heirs of the conquest were to
this degree protected. The only person who would have com-
mitted most or all of his land’s output to a priest would have
been a very rich absentee landlord who madc his moncy in
commerce. But to dedicate all of one’s land in a grand display
of wealth was risky. This person might subsequently fail into
economic distress and be compelled to lease his property to
another. The heirs of this individual would then have lost own-
ership of all the dedicated land. If their father had pledged all
of their land, they would have lost their guaranteed status as
freemen. Thus, the high risks of default would have tended to
reduce the number of such large-scale pledges to priests.

Nevertheless, the possibility of disinheritance did exist. Ifa
father was so distressed by the ethical rebellion of all of his
sons, he had the ability to disinherit them. He could not disin-
herit one son among many in this way, but he could disinherit
all of them. He could do this by dedicating all of his landed
