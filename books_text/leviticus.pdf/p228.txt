172 LEVITIGUS: AN ECONOMIC COMMENTARY

the curse of God had something to do with the presence of the
city. We may not be able to understand all of these ram-
ifications. The point is, the individual was not quarantined
inside the city; he was quarantined by removing him from the
city. The one exception to this was King Uzziah (II Chron.
26:21). He was forced to dwell in a separate house, and he was
cut off for the rest of his life from the house of God. This judg-
ment had come upon him immediately after his presumptuous
sin of offering sacrifice in the temple. It was clear from this
incident that the judgment was regarded as judicial — coming
directly from the hand of God — and not biological. As the king,
he was granted immunity from exclusion from David's city, but
only by means of a boundary separating him from the city.
There is no question that quarantine was legal for those
dwelling inside the cities of Israel. Men were cut off from their
homes, their families, their livelihood, and especially from the
household of faith. They could not participate in the covenant
rituals and feasts of Israel. This was the ultimate civil quaran-
tine in ancient Israel, other than execution.’ It meant excom-
munication from Passover and the loss of citizenship.

To Protect the Public

‘The justification for quarantine in Leviticus 13 was the need
to protect the public. ‘he spread of the disease, or other forms
of God’s judgment, was to be halted by removing the afflicted
individual from the city. he concern was public health, but it
was not a concern about biological contagion. It was concern
about the willingness of God to afflict other individuals with the
disease or other afflictions because of the rulers’ unwillingness
to enforce God’s law. Thus, the quarantining process of Leviti-
cus 13 was primarily judicial. In fact, it would probably be safe
lo say that it was entirely judicial. Only by the extension of the

7. Being confined to a city of refuge at least had a temporal limit: the death of
the high priest (Num. 85:25).
