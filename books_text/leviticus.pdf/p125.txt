Priestly Representation 69

The conclusion: “Keep therefore the words of this covenant,
and do them, that ye may prosper in all that ye do” (Deut.
29:9).

‘The threat of God’s covenant sanctions was not limited to
the nation; it also included the individual. God warned what
would happen to the covenant-breaking individual. Notice the
language of smoke, which accompanies burning. “And it come
to pass, when he heareth the words of this curse, that he bless
himself in his heart, saying, I shall have pcacc, though I walk in
the imagination of mine heart, to add drunkenness to thirst:
The Lorp will not spare him, but then the anger of the Lorp
and his jealousy shall smoke against that man, and all the curs-
es that are written in this book shall lic upon him, and the
Lorp shall blot out his name from under heaven. And the Lorp
shall separate him unto evil out of all the tribes of Israel, ac-
cording to all the curses of the covenant that are written in this
book of the law” (Deut. 29:19-21).

Firstfruits (Pentecost)

The meal offering is associated in the text with the firsUruits
offering, another meal offering (Lev. 2:12, 14). Firsufruits was a
mandatory annual offering (Ex. 23:16, 19). This offering was a
man’s public acknowledgment that God must be paid “off the
top.” That is, the farmer owed God the best of his ficld and the
first portion of his crop. He was not to pay God last; he was
required to pay God first. Firstfruits was one rare case in the
Bible where God taxed capital rather than the net increase. ‘he
farmer did not deduct the replacement seed before offering the
firstfruits; whatever came up was God's, But it was a small
offering — a token offering.

The firstfruits payment was mandatory. This was his public
acknowledgment of his subordination to God through the Aa-
ronic priesthood. When the blessings of God’s bounty appeared
in his field, the owner was required to acknowledge the source
of this bounty by bringing a meal offering to God.
