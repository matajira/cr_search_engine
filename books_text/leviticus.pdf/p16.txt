xvi LEVITICUS: AN ECONOMIC COMMENTARY

not be taken seriously as a religion with answers to the world’s
problems. Christianity will continue to be dismissed as simply
one more expcriment in mystical personal escape and well-
organized fund-raising.

A short commentary that offers only conclusions is not going
to be taken seriously as a book for restructuring economic
theory and practice. ‘Iwo hundred or so pages of brief conclu-
sions can be dismissed as a list of unsubstantiated speculations.
I wrote this large book in order to substantiate my opinions. A
commentator should include reasons for his exclusions: why he
rejected other possible interpretations and applications. He
must show what he believes to be truc, but he must also show
clearly what he is not saying, so as Lo avoid confusion after he is
dead and gone. This requires large commentaries. ‘Those who
are serious about understanding the Bible and applying it to
this world require books as large as this one. Those who are not
equally scrious arc not my targeted audience. Besides, such
people will not read a book like this anyway.

This commentary, like Boundaries and Dominion, does things
that normal Bible commentaries seldom do. First, it applies
biblical texts to the modern world - specifically, to economics
and law. Second, it citcs the opinions of non-Christians who
have reached either similar or rival conclusions regarding the
judicial issues that Leviticus deals with. Third, it offers cxam-
ples from history about how societics have enforced or failed to
enforce these laws, and what the results were. Fourth, it offers
the logic and evidence that led to the conclusions. The reader
can evaluate for himsclf my reasoning process, case law by case
law.

The Book of Leviticus is not understood by Christians, not
obeyed by Jews, and not taken scriously by anyone clsc. For
example, Ghristians do not understand the five Levitical sacri-
fices, Jews do not offer them, and everyone else thinks of them
as archaic, barbaric, or both. Then there is the factor of the
higher criticism of the Bible, which first began getting a hearing
