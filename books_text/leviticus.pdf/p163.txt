5

PROPORTIONAL PAYMENTS TO GOD

And he shall bring his trespass offering unto the Lorp for his sin
which he hath sinned, a female from the flock, a lamb or a hid of the
goats, for a sin offering, and the priest shall make an atonement for him
concerning his sin, And if he be not able to bring a lamb, then he shall
bring for his trespass, which he hath committed, tuo turlledoves, or two
young pigeons, unto the Lorn; one for a sin offering, and the other for
a burnt offering (Lev. 5-6-7).

But if he be not able to bring two turtledoves, or two young pigeons,
then he that sinned shall bring for his offering the tenth part of an ephah
of fine flour for a sin offering; he shall pul no oil upon it, neither shall
he put any frankincense thereon: for it is a sin offering. Then shall he
bring it to the priest, and the priest shall take his handful of it, even a
memorial thereof, and burn it on the altar, according to the offerings
made by fire unto the Lorn: it is a sin offering (Lev. 5:11-12).

This passage extends the law of purification offerings: point
four. This was a special form of purification offering that ap-
plied to a specific kind of sin: a sin of omission (vv. 2-4).' A pur-
ification offering was rcquired to purify the tabernacle or the

1. The sins were hidden sins. Gordon J. Wenham, The Book of Leviticus (Grand
Rapids, Michigan: Eerdmans, 1979), p. 100.
