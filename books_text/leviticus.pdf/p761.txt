self-defense, 267-69
self-interest, 360-63
shield, 270
source, 368
stoning, 161
strangers, 375, 506
summary, 637-45
tablets of, 462
taxes &, 33
temple &, 154-55
theft &, 11
theocentric, 375, 394
universalism, 17
urbanization &, 20, 22, 33-
34, 35, 70
vomiting metaphor, 182
walk, 14
weights & measures, 311,
313
wine &, 153
work of, 632
world government, 277
written, 271
lawsuit
church's immunity, 104
churches’ reluctance, 377
covenant, 317, 325, 377,
391
deaf & blind, 223, 232-33
God’s court, 144
victim as God’s agent, 136
victim’s brings, 269
lawyers, 104, 216
laziness, 242-43
leasehold
Adam's, 84
economics of, 395
jubilee, 416-17, 474

Index

705

priests &, 606-7
rural, 430-31, 538
Satan’s, 84
violation of, 397, 447
also see jubilee; land; own-
ership; walled cities
leaven
altar, 66, 79
best, 77, 86
continuity, 79-80, 355-56
death, 78
dominion, 77, 79-80
Egypt, fi, 71, 78, 295,
355
exodus, 71
expansion, 79, 648
evil, 6
God’s, Satan’s, 77, 86
grain offering, 66
growth, 79, 85
holiness, 79
Iloly Communion, 79
Israel (modern), 405
not evil, 66, 77-80
Passover, 355
peace offering, 74, 78
progressive sanctification,
78-79, 86
rival, 6, 78
symbol, 86
symbolism, 66, 355
taboo?, 79
unleaven, 75, 77, 86
yeast, 80
Leithart, Peter, xi
leprosy
army, 170
blood avenger, 171
