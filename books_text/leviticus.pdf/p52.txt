li LEVITICUS: AN ECONOMIC COMMENTARY

ing invited gucsts, to cat the meat returned to them in a festive
meal.”?®

‘The significant judicial fact of this oflering was its openness.
The. offerer joimed in a meal with God and his family. This
indicates that the judicial barriers that always exist between God
and sinful man were reduced. ‘he participants’ sins had al-
ready been dealt with judicially by another sacrifice. The sacri-
fice of well-being was a communion meal. The meal’s partici-
pants were visibly identificd as holy before God, set apart to
praise Him and rejoice in His grace. The boundaries separating
the offerer and this sacrifice were minimal compared to the
boundarics around the other offerings. The offerer received
back most of the offering. This points to point three of the
covenant: ethics/boundaries.

4. The Purification Offering (Lev. 4-35:13)

‘This is called the sin offering in the King Jamcs Version. This
was the sacrifice governing unintentional sins committed by the
high priest, the civil ruler, the congregation as a whole, or
individuals. “It describes behavior that violates the community’s
standards.”*° Without the purification offering, the whole
community was endangered. These sacrifices were required to
avoid God's negative sanctions in history. They were offered to
escape “a religious judgment on deviant behavior.”* Hartley
cites a 1989 article by A. Marx, who argued that this sacrifice
was required on threc formal occasions: the investiture of Le-
viles (Num. 8:1-36); the ordination of Aaron (Lev, 8:1-36; Ex.
29:1-37), and the consecration of the altar (Lev. 8:11, 15; Ex.
29:36-37), The Nazarite had to make a purification offering at
the tcrmination of his vow (Num. 6:13-20).*

29. Hartley, Leviticus, p. 38.
30. Ihid., p. 85.

84. Idem.

32. Ibid., p. BB.
