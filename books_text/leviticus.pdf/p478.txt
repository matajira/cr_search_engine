422 LEVITICUS: AN ECONOMIC COMMENTARY

the West has experienced since the late eighteenth century is
what God had in mind for Israel from the time of the conquest,
namely, rapid growth — of population, cities, specialization, man-
ufacturing, trade, emigration, and per capita wealth. To the
extent that they did not experience this, they would know that
they were under God’s national curse.

‘The jubilee inheritance law was designed to promote emigra-
tion out of Israel and urban occupations inside the land that
relied on foreign trade. The rural land inheritance law promot-
ed contact with forcigners. This was an aspect of the dominion
covenant. It was to serve as a means of evangelism. The story of
Israel, her laws, and her God was to spread abroad (Deut. 4:5-
8). The jubilee law was in no sense a law mandating the State-
enforced equalization of wealth, contrary to that peculiar late-
twentieth-century theology known as liberation theology (social-
ism for evangelical Christians).

Aliens and Inalienable Land

We can discover the fundamental jubilee principle by begin-
ning with God’s own statement regarding the reason for the
jubilee law: “The land shall not be sold for ever: for the land is
mine; for ye are strangers and sojourners with me” (Lev.
25:23). Problem: God owns all the earth, then and now. “For
every beast of the forest is mine, and the cattle upon a thou-
sand hills” (Ps. 50:10). Yet this very ownership of the world is
what led to the special position of the land of Canaan and its
conquerors: “Now therclore, if ye will obey my voice indecd,
and keep my covenant, then ye shall be a peculiar treasure
unto me above all people: for all the earth is mine: And ye shall
be unto me a kingdom of priests, and an holy nation. These are
the words which thou shalt speak unto the children of Israel”
(Ex. 19:5-6). It was the Israclitcs, and only the Israelites, who

28. North, Boundaries and Dominion, ch. 25, section on “The Myth of Jubilee
Egalitarianism.”
