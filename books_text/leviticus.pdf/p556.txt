500 LEVITICUS: AN ECONOMIC COMMENTARY

An Exception to the Law: Criminal Trespass

The criminal did not go free in the jubilec year. Had he
received such a releasc, a criminal, seeing the approach of the
jubilee year, might think to himself: “If I get away with this
crime, I will benefit. If 1 do not get away with it, I will not have
to remain in another man’s service for very long. The larger
the value of what I steal, the bettcr the risk-reward ratio is.”
The closer to the jubilce year, the better the risk-reward ratio
for crimes against property. The criminal’s victim ‘could not
expect anything like double restitution from the sale of a crimi-
nal if the jubilee year was near. The stream of expected labor
services would be cut off by the jubilee. Thus, the sale price of
the criminal would be low. If the criminal was to be liberated at
the jubilee, this legal arrangement would not only subsidize
theft, it would subsidize high-value thefts. The victims would be
penalized because of the liberation aspect of the jubilce year.

My conclusion is that the year of jubilee did not apply to
convicted criminals. Neither did the law mandating owncrs to
treat Israelite bondservants as hired workers. Criminals were
sold into slavery in order to repay their victims and mect God’s
judicial requirements. The most important issuc was not the
liberation of the criminal; rather, il was the maximization of the
criminal's selling price, so that the victim would receive double
restitution. The law of God does not discriminate against vic
tims of crime in the name of liberation. The principle of vic-
uim’s rights lies at the heart of the Bible’s criminal justice sys-
tem.* The criminal must have remained outside the proicction
of the jubilee, and therefore outside the judicial status of citizen,
until he repaid his debt to his victim. Ile could regain his citi-
zenship only when his debt was paid. ITis adult sons, however,

4, Gary North, Jools of Dominion: The Case Laws of Fxodus (Tyler, Texas: Institute
for Christian Economics, 1990), chaps. 7, 8, 11-14. See also Gary North, Victim's
Rights: The Riblical View of Civil Justice (Tyler, Texas: Institute for Christian Fconom-
ics, 1990).
