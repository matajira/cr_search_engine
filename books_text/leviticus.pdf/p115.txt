Sacrifice, Stewardship, and Debt 59

that individuals acting as taxpaying stewards can morc efficient-
ly expand the State’s capital base, What God has delegated to
the family in history, the Moloch State will eventually attempt
to confiscate.” We must defend the free market from the Mol-
och State, but we must appeal to the Bible; it is the moral high
ground. Technical, “value-free” economics is not.”

What must become central to economic analysis is the under-
lying theology of the five-point covenant that preceded Cod’s
imposition of negative sanctions against the creation:

1. ‘fhe integrated doctrines of the special creation, the sovereignty
of God as Creator, and therefore His absolute ownership of the cre-
ation (Gen. 1:1);

2. The doctrine of God’s delegation of secondary ownership of the
24

creation to man (Gen. 1:26-28
3. The doctrine of the law of God, which appears in the form of an
exclusive (and therefore exclusionary) property boundary (Gen. 2:17a);

4. The doctrine of God’s negative sanctions against the person who
violaies [is law and His property (Gen. 2:17b);

5. The doctrine of the promised negative historical sanctions against
Satan through God’s promised Seed (Gen. 3:15).

The acknowledgment of the reality of God-cursed economic
scarcity is necessary but not sufficient for the reconstruction of
economic analysis. We must also discover in God’s word and
apply covenanially the judicial foundations of economic recon-

22. Gary North, The Sinai Strategy: Economics and the Ten Commandments (lyler,
Texas: Institute for Christian Economics, 1986), ch. 5: “Familistic Capital.”

28. North, Boundaries and Dominion, ch. 1, subsection on “Che Moral High
Ground.”

24, That is, the dominion covenant.
