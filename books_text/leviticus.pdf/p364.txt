19

MEASURING OUT JUSTICE

And if a stranger sojourn with thee in your land, ye shall not vex
him, But the stranger that dwelleth with you shall be unio you as one
born among you, and thou shalt love him as thyself; for ye were strangers
in the land of Egypt: I am the Lorp your God. ¥% shall do no unrigh-
teousness in judgment, in meteyard, in weight, or in measure. Just bal-
ances, just weights, a just ephah, and a just hin, shall ye have: I am the
Lorp your God, which brought you out of the land of Egypt. Therefore
shall ye observe all my statutes, and all my judgments, and do them: Iam
the Lorp (Lev. 19:33-37).

The theocentric meaning of this law is equality before God's
law. This includes strangers. The general principle is the famil-
iar guideline known as the golden rule: do unto others as you
would have them do unto you (Matt. 7:12).

God reminded the Israelites in this passage that He had
delivered them from Egyptian bondage, where they had been
strangers. This deliverance had been an application of the
fundamental theme of the Bible: the transition from wrath to grace.
The God who delivered His people in history (point two of the
biblical covenant model: historical prologue) is also the God
who lays down the law (point three).

Onc judicial application of God’s historical deliverance of His
people is the creation of a civil sanctuary: a place set apart judi-
