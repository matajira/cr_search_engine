Gleaning: Charity Without Entitlement 207

Localism and tribalism were both basic to the application of
the gleaning law in Mosaic Israel. The authority of the local
landowner to chose who would glean and who would not from
among various candidates — the boundary principle of inclusion
and exclusion — transferred great responsibility and authority
into his hand. This kind of personalized charity is no longer
taken seriously by those who legislate politically grounded
welfare State policies in the modern world. Such a view of
charity transfers too much authority to property owners, in the
eyes of the politicians, and not enough to the State and its
functionaries. But it is not the principle of localism that changes
in the New Testament era; it is only the landed tribalism that
changes. When the kingdom of God was transferred to a new
nation (Matt. 21:43), meaning the church, the Levitical land
laws were abolished.

Gleaning no longer applies in the New Covenant era. The
jubilee land law was annulled by Jesus through: 1) His minis-
try’s fulfillment of the law (Luke 4:16-27); 2) the transfer of the
kingdom to the church at Pentecost (Matt. 21:43; Acts 2); and
3) the destruction of Jerusalem in A.D. 70. Can we learn any-
thing from the gleaning law? I think we can, but these lessons
are essentially negative. They show us what should not be done,
not what must be done, to avoid God’s negative sanctions.

The lessons from gleaning are these: 1) all charity is based
legally on the fundamental principle that God owns the earth
(Ps. 24:1); 2) a third party has no legal civil claim on any assct
that he does not own;’ 3) charity should not create a perma-
nent dependence on the part of the recipient; 4) charity should
not subsidize evil; 5) it should involve hard work except in cases

19, In Old Covenant Israel, a person had the right to pluck grapes and corn
from a neighbor's fields, though only what he could carry in his hands (Deut. 23:24-
25; Luke 6:1). This law increased the likelihood of travel and communications, since
visitors would not have to return home to eat or carry food with them everywhere.
The economic benefit of being located close to a road — cheap Wransportation of farm
commodities — was partially offset by this open access law.

 
