442 LEVITICUS: AN ECONOMIC COMMENTARY

most important aspect m identifying the Bible’s concept of
economic oprpression.'*

The State and Economic Oppression

‘The person who leases a picce of land from an owner can
become an oppressor, but so can the owner who leases it. The
ethical and judicial question is this: What is economic oppres-
sion? This is not so easy to answer as Christian social commen-
tators and humanistic legislators have sometimes imagined. In
Tools of Dominion, I argued that neither the Bible nor economic
theory provides a legally enforceable definition of cconomic
oppression that is based on price. I argued rather that the State
creales the conditions for economic oppression: injustice.’
This is affirmed by Psalm 82, which refers to rulers of the con-
gregation, which was the nation as a whole.” “God standeth in
the congregation of the mighty; he judgeth among the gods.
llow long will ye judge unjustly, and accept the persons of the
wicked? Selah. Defend the poor and fatherless: do justice to the
afflicted and needy. Deliver the poor and needy: rid them out
of the hand of the wicked” (Ps. 82:1-4).

Oppression and the Jubilee Land Law

‘We now return to the text of the jubilee land law. Who is
likely to become the victim of oppression? Answer: the person
with less rcliable information about alternative offers and future
economic and legal conditions. This can be either party. In an
overwhelmingly agricultural community, both parties probably

18, For a discussion of State-enforced compulsory trade unionism as an aspect
of oppression, sce North, Boundaries and Dominion, ch. 26, subsection on “Things Seen
and Unseen.”

19, See Gary North, Tools of Dominion: The Case Laws of Exodus. (ler, Texas:
Institute for Christian Economics, 1990), pp. 683-84

20. James B. Jordan, The Sociology of the Church Clyler, ‘Yexas: Geneva Ministries,
1986), Appendix A: “Biblical Terminology for the Church.” See Chapter 4, above:
section on “Ritual Gleanings,” pp. 91-92.
