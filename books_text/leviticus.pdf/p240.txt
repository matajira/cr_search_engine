184 LEVITICUS: AN ECONOMIC COMMENTARY

will be a snare unto thee” (Deut. 7:16). Why no pity? Because
of the abomination of their gods. God’s warning: similar wor-
ship inside the land wiil bring similar military sanctions (Deut.
8:19-20). God subsequently raised up Assyria and Babylon to
perform an analogous service for Him, which is why this pas-
sage warned of a future spewing forth.

The land specified as God’s agent was the Promised Land,
not Egypt or any other plot of ground. Only the land inside
God's covenantal boundary of separation scrved as His agent of
negative military sanctions. This lcads us to a conclusion: be-
cause the Promised Land could serve as a prosecuting witness
against Israel, it was unique. The witness for the prosecution is
required to cast the first stone (Deut. 17:7). The carthquake is
the obvious example of stone-casting by the land (Isa, 29:6;
Zech. 14:5; Joel 2:10; Nahum 1:5), This quaking is the lan-
guage of covenantal judgment. Israel’s covenantal agent, Moses,
had already expcrienced this. “And mount Sinai was altogether
on a smoke, because the Lorp descended upon it in fire: and
the smoke thereof ascended as the smoke of a furnace, and the
whole mount quaked greatly” (Ex. 19:18). It took place again at
the crucifixion of Jesus Christ (Matt. 27:51). Conclusion: If the
land’s office as witness for the prosecution still exists, then its office
as slone-caster sull exists. Because the resurrected Christ appears
as the vomiter in New Covenant imagery, I conclude that He is
the witness who brings judgment against societies.

What about this prophecy in the Book of Revelation? “And
the. kings of the earth, and the great men, and the rich men,
and the chief captains, and the mighty men, and every bond-
man, and every {rce man, hid themselves in the dens and in
the rocks of the mountains; And said to the mountains and
rocks, Fall on us, and hide us from the face of him that sitteth
on the throne, and from the wrath of the Lamb” (Rev. 6:15-16).
The reason such language applied to that event is because the
prophecy was intended to be fulfilled a few ycars after it was
