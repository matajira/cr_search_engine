620 LEVITICUS: AN ECONOMIC COMMENTARY

sometimes retained by their producers to be used or enjoyed
for themselves, not sold into the market for money.

Why would someone pay a commission to redeem an object?
Only if that object has special meaning or importance for him.
If the quality of grain in a Uithed sack is identical to the grain in
the other ninc sacks, the tithe-payer is nol going to pay a com-
mission to buy back the tithed sack unless economic conditions
have changed in the meantime. The assumption behind this law
is that the impersonal collecting of the tithe may produce a
personally significant loss [or the tithe-payer. In order to enable
him to minimize this loss, the law allows him to pay a 20 per-
cent commission to buy back the special item.

There is no indication that this law has been annulled by
subsequent biblical revelation. It applies only to agriculture, as
the text indicates - primarily to herds of animals.

A Tithe on the Net Increase

The text reads: “And concerning the uthe of the herd, or of
the flock, even of whatsocver passeth under the rod, the tenth
shall be holy unto the Lorp. He shall not search whether it be
good or bad, neither shall he change it: and if he change it at
all, then both it and the change thercof shall be holy; it shall
not be redeemed.” The tithe was collected from the increase of
the herd. ‘This increase was a net increase. If one animal of the
herd had died since the time of the most recent payment of the
tithe, the herd owner was allowed to set aside a replacement
from the animais born since the last payment.’ Had this not
been the case, then losses from a disease that killed half a man’s
herd could not be deducted when assessing the net annual
increase. This would constitute a tax on capital.

L. This is the cconomic equivalent of allowing a farmer to set aside from this
year’s crop an amount equai to last year’s sccd. A person pays the tithe on nct output
only once. He does not keep paying on capital, ic., replaced producer goods.
