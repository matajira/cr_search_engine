Leaven and Progressive Sanctification 83

peace of God. Tithes are obligatory payments to the owner of
the universe as our acknowledgment of our position as share-
croppers in His field, the world (Matt. 13:38).!° Offerings in
this context are peace offerings that are analogous to the sacri-
ficial peace offering of Leviticus 7.”

The Lord’s Supper is both individual and corporate. So is
progressive sanctification. The kingdom of God on earth is
revealed progressively through redeemed mankind’s renewal
and re-structuring of all of man’s institutions, including the
State. It is this vision of the kingdom’s corporate expansion
which modern pictistic fundamentalism and almost equally
pietistic amillennialism reject. ‘they proclaim “souls-only re-
demption.”" The New Testament offers a more profound
concept of the Great Commission.’® There can and must be
social progress in history, not as a product of humanism but of
Christianity.° Modern Christian social theory, pluralistic .to
the core, rejects this possibility.’ It rejects the idea that there
will be an cultural expansion of Christianity in history, as with
rising leaven, culminating in the cosmic fire of the Second
Coming.®

Boundaries After Calvary

Who owns this world? God does (Ps. 24:1). But because of
Adam’s fall, Adam subordinated himself covenantally to Satan,
who claimed Adam’s legacy for himself. But Adam could main-

16. Gary North, Tithing und the Charch (flex, ‘Yexas: Institute for Christian
Economics, 1994), Part 1.

17. See North, Boundaries and Dominion, ch. 3, section on “Living Sacrifices.”

18, ttid., ch. 3, section on “Progressive Corporate Sanctification.”

19, Renneth L. Gentry, Jn, The Greatness of the Great Commission: The Christian
Enterprise in a Fallen World (Fyler, Texas: Tastitute for Christian Economics, 1990).

20. North, Boundaries and Dominion, ch. 3, section on “Covenant Sanctions and
Social Progress.”

21. Jbid., ch. 3, subsection on “Pluralism.”

22, Idid., ch. 3, section on “Total Victory: Final Judgment.”
