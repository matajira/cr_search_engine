58 THE COASE THEOREM

Yet it must be challenged. More than this: it must be scrapped.
The search for a timeless, rational mental construct as the basis
of a science of human action is fruitless. Even the great Mises
was pattially sidetracked by this quest. What confidence can we
legitimately have in an explanation of market processes which
argues that as entrepreneurship becomes successful, it “tends
toward” the creation of a world in which human action and
human choice is impossible, a world of automatons rather than
people? Yet this is the explanatory model used by Mises (and
almost all other economists). As he says in Hznan Action con-
cerning his theoretical construct, the Evenly Rotating Economy:
“Action is change, and change is the temporal sequence. But in
the evenly rotating economy change and succession of events
are eliminated. Action is to make choices and to cope with an
uncertain future. But in the evenly rotating economy there is
no choosing and the future is not uncertain as it does not differ
from the present known state. Such a rigid system is not peo-
pled with living men making choices and liable to error; it is a
world of soulless unthinking automatons; it is not a human
society, it is an ant hill.”*° Nevertheless, he states flatly: “The
theorems implied in the notion of the plain state of rest are
valid with regard to all transactions without exception.””! For
the modern economist, all human action tends toward a final
state in which human beings become omniscient and therefore
take on one of the attributes of God.” The problem is, their
view of God is that He could not possibly act if He existed. He
would be a “rule-following automaton,”* because “A perfect
being would not act.”**

20. Mises, Human Action, p. 248.
21. Did., p. 245,
22. Mises writes: “No matter whether this thirsting after ommniscience can ever be

fally gratified or not, man will not cease to strive after it passionately.” Mises, Ultimate
Foundation, p. 120.

28. Buchanan, Cast and Choice, p. 96.
24, Mises, Epistemological Problems of Economics (Princeton, New Jersey: Van
