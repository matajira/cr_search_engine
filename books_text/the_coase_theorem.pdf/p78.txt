60 THE COASE THEOREM

really cannot even be mentally conceived’? —- which is used to
explain the world inhabited by men.

Nevertheless, with absolute confidence (even “apodictic
certainty,” one of Mises’ favorite terms), Mises proclaims that
“These insoluble contradictions, however, do not affect the
service which this imaginary construction renders... .”"° Or
even more forcefully: “Even imaginary constructions which are
inconceivable, self-contradictory, or unrealizable can render
useful, even indispensable services in the comprehension of
reality, provided the economist knows how to use them proper-
ly." That word, “provided,” covers a multitude of epistemol-
ogical sins. So does the word “properly.”

Anyone who has ever tried to read an article in such journals
as Econometrica and The Review of Economics and Statistics knows
how rarified economic logic can become.” It reminds me of
what little I know about the formal academic debates carried on
by the late medieval scholastics. The number of angels dancing
on the point of a needle is a down-to-earth problem compared
to stochastic analysis applied to a world of perfect foreknowl-
edge. The sophistication of modern econometric analysis is

29. How can we imagine a world in which every actor has perfect foreknow-
ledge? Try to explain the meaning of human choice in a world in which everyone
knows in advance precisely what the others will inevitably do in the future. We may
take such a world on faith; we cannot explain

 

 

30, fbid., p. 248. He writes: “The method of imaginary constructions is indispens-
able for praxeology (the science of human action -- G.NJ} it is the only method of
praxeological and economic inquiry. It is, to be sure, a method difficult to handle
because it can easily result in fallacious syllogisms. It leads along a sharp edge; on
both sides yawns the chasm of absurdity and nonsense. Only merciless self-criticism
can prevent a man from falling headlong into these abysmal depths.” /bid., p, 237.
Question: Self-criticism in terms of what truth, or by what standard? For a critique
of this position, see North, Dominion Covenant: Genesis, pp. 352-53.

31. Mises, ibid., p. 236.

32. Ido not have in mind merely the writings of Nobel Prize-winning economist
Gerard Debreu, which da not pretend to deal with the real world, I have in mind
investigations into the operation of real-world institutions, such as William M. Lan-
des, “An Economic Analysis of the Courts,” Journal of Law & Economics, XIV (April
1971), pp. 61-107.

  
