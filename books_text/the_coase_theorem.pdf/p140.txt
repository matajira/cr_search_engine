122 THE COASE THEOREM

. Economics: Principles and Policy from a Christian
Perspective. Milford, Michigan: Mott Media, 1977.

Christian Epistemology

North, Gary, editor. Foundations of Christian Scholarship: Essays in
the Van Til Perspective. Vallecito, California: Ross House
Books, 1976.

Rushdoony, Rousas John. By What Standard? An Analysis of the
Philosophy of Cornelius Van Til, Tyler, Texas: Thoburn Press,
(1959) 1983.

Van Til, Cornelius. A Christian Theory of Knowledge. Phillips-
burgh, New Jersey: Presbyterian & Reformed, 1969.

. The Defense of the Faith, Revised edition. Phillips-
burgh, New Jersey: Presbyterian & Reformed, 1963.

. A Survey of Christian Epistemology. Den Dulk
Foundation, (1932) 1969. Distributed by Presbyterian & Re-
formed, Phillipsburg, New Jersey.
