Preface xv

through court decisions, Prof. Coase went on to show that the
‘problem’ of externalities would be resolved, without govern-
ment regulation, in ways that maximize social value if transac-
tion costs are low, and the outcome does not depend on which
party receives the initial property right.” This is a misleading
final sentence. There should have been a semicolon after the
word fow. It is the heart of the Coase theorem that the econom-
ic outcome does not depend on which party receives the initial
property right. Mr. Lehn went on:

“The Problem of Social Cost” spawned a large body of litera-
ture that debated the equilibrium tendencies of the imaginary
world of zero transaction costs, a development that Prof. Coase
found unfortunate. For the major insight of this paper was not
to suggest that we live in this imaginary world, but rather to
show conditions under which legal decisions concerning property
rights do affect resource allocation.

I disagree. The article’s major conclusion was that the initial
distribution of property rights is economically irrelevant in
establishing the social (aggregate) economic costs of settling dis-
putes over externalities. If this thesis regarding costs of settling
disputes over externalities is true, then R. H. Coase’s theorem
constitutes one of the most subtle yet profound attacks ever
written on the concept of private property rights.

It is my perception of the subdiscipline of law and economics
that it is dominated by scholars who have cither accepted the
truth of Coase’s theorem or who have at least accepted its terms
of discourse. To the extent that the field’s developers have
accepted the Coase theorem, this relatively recent academic
subdiscipline is grounded on a concept of law which is at odds
with the moral and legal foundations of liberty.

As I hope to show in this monograph, the Coase theorem is
thoroughly consistent with the free market economic method-
ology associated with the Chicago School of economics. The
Coase theorem on social cost is in this sense an example of the
