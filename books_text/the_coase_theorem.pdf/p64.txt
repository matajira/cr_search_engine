46 THE COASE THEOREM

Micro-Efficiency and Macro-Revolution

It is not possible to discover an economically efficient solu-
tion to just one transaction. We cannot be efficient in just one
thing. The question of efficiency is not simply a microeconomic
issue; it is also macroeconomic. We cannot discover an efficient
solution to any economic problem that does not in some way
affect the whole social order. In short, we cannot do just one thing
efficiently. We need to heed the warning of biologist Garrett
Hardin: “The dream of the philosopher's stone is old and well
known, and has its counterparts in the ideas of skeleton keys
and panaceas. . . . We now look askance at any one who sets
out to find a philosopher’s stone. The mythology of our time is
built more around the reciprocal dream — the dream of a high-
ly specific agent which will do only one thing. ... The moral of
the myth can be put in various ways. One: Wishing won’t make
it so, Two: Every change has its price. Three (and this one I
like best): We can never do merely one thing.”*

The system of justice that governs any social order is itself a
net producer or reducer of both macro-efficiency and micro-
efficiency. Equity can never be segregated from efficiency. If our
judges’ supposedly economically efficient decisions at the micro
level call into question the moral integrity of the prevailing
legal order, we have not yet reached an efficient solution to our
microeconomic problem. This is why it is astonishing to find
economist and Talmudist Aaron Levine siding with Coase:
“While the principle of equity is promoted by the selection of
appropriate liability rules, economic efficiency is realized when
the negative externality is eliminated by the least-cost method.
Hence, should it be less costly to avoid crop damage by growing
smoke-resistant wheat than by installing a smoke-control device,
the former method should be adopted. Whether the farmer or

18. Garrett Hardin, “The Cybernetics of Competition: A Biologist’s View of
Society,” in Helmut Schoeck and James W. Wiggins (eds.), Central Planning and
Mercantilism (Princeton, New Jersey: Van Nostrand, 1964), p. 84.
