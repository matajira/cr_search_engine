Copyright by Gary North, 1992

 

Library of Congress Cataloging-in-Publication Data

North, Gary.

The Coase theorem : a study in economic epistemology / Gary
North

Pp. cm.

Includes bibliographical references and index

ISBN 0-930464-61-3 (hardback : acid-free paper) $25.00
1, Economics — Moral and ethical aspects.

2. Pollution — Economic aspects.

3. Property.

4. Law — Economic aspects.
5. Coase, Ronald Harry _I. Tide

HB72.N67 1992 91-40767
330.1-dc26 ciP

 

Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711
