The Crisis: Living With Dialectical Schizophrenia 91

mal rationality), Weber argued, capitalism’s socialist critics very
often take offense: “All of these [substantively rational, ethical -
G.N.] approaches may consider the ‘purely formal’ rationality
of calculation in monetary terms as of quite secondary impor-
tance or even as fundamentaily inimical to their respective
ultimate ends, even before anything has been said about the
consequences of the specifically modern calculating attitude.”*
In short, Weber concluded, “Formal and substantive rationality,
no matter by what standard the latter is measured, are always
in principle separate things, no matter that in many (and under
certain very artificial assumptions even in all) cases they may
coincide empirically.”° This assertion of permanent dialectical
tension in economic thought was basic to Weber's sociological
analysis.’°

Professional Blindness to Moral Issues

Economists who defend the free market seldom acknowledge
the nature of this fundamental debate between the free mar-
ket’s intellectual defenders and the free market's critics. Their
“value-free” methodology and their methodological individual-
ism blind them to the realities of the debate - a debate over
morality, values, and the effects of voluntary economic transac-
tions on social aggregates. Free market economists cannot seem
to understand those scholars and critics who raise the question
of individual morality, let alone social consequences and social
values, and who then ignore questions of economic efficiency

York: Bedminster Press, 1968), pp. 85-86. This is a translation of Weber's posthu-
mous Wirtschaft und Gesellschaft, 4th German edition, 1956.

8. Iid., p. 86. See a slightly different translation of this passage and the one in
the preceding footnote in Weber, The Theory of Social and Economic Organization,
edited by Talcott Parsons (New York: The Free Press, [1947] 1964), pp. 185-86.

9. Ibid., p. 108. (Theory, p. 212.]

10. Gary North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Cage,” in North (ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspec-
tive (Vallecito, California: Ross House, 1976), pp. 141-46.
