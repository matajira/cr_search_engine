“Weighing Up the Gains and Losses” 77

faster than normal, thereby throwing off more sparks. (The
example applies to railroads before the era of diesel engines,
but it is still valid as an example.) Suppose also that the sparks
set a fire that burns a farmer’s crop. Pigou said that the rail-
road company should reimburse the farmer for the loss of his
crops by paying him the crop’s market value. This, it should be
pointed out, is also what Exodus 22:6 says. Coase denies Pigou’s
conclusion. “The conclusion that it is desirable that the railway
should be made liable for the damage it causes is wrong.”®
Why? Because the economic gains to the total economy, as revealed
by the value of the crops lost vs. the cost of installing spark-
arresters on the engine, or the losses to the railroad company
if the train was not run at all, might be greater by allowing the train
to emit sparks. (Might be, might be, might be: How can anyone
know, given the intellectual tools of modern, subjectivist eco-
nomics?) The judge should consider the monetary value of the
burned crops in relation to the cost of installing a spark-arrest-
er or the monetary losses to the company of running the train
more slowly, and then make a decision as to what each party
owes the other. In other words, he must consider the value of
total production. “This question can be resolved by considering
what would happen to the value of total production if it were
decided to exempt the railway from liability from fire-dam-
age... .”8 Coase argues that it might be better for society in
general if the farmer’s property rights are ignored, leaving him
free to pay the railroad company sufficient money to install the
spark-arrester. After all, the value of the crop may be greater
than the cost of the spark-arrester.*

42, Coase, “Social Cost,” p. 32.

43. Tbid., p. 83.

44, Clearly, the damage inflicted on the crops planted close to the tracks by
numerous farmers could be high. The costs would be high to organize the farmers
together in order to contribute money to finance the installation of the spark arrest-
er. Each farmer would tend to wait for the others to put up the money. Each would
prefer to become a “free rider” in the transaction: paying nothing, but benefitting
from the spark arrester. The payment to the railroad firm probably would not be
