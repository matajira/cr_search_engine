PREFACE

Today in the sciences, books are usually either texts or retrospective
reflections upon one aspect or another of the scientific life. The scientist
who writes one is more likely to find his professional refrutation impaired
rather than enhanced.

Thomas Kuhn‘

Science is a sacred cow, as Anthony Standen observed in
1950,? and modern economists are faithful academic hindus.
They have done their best to hitch economics’ wagon to the star
of physical science. They have adopted physical science’s use of
mathematics, even when this methodological tool is totally
inapplicable to the topic under discussion, which is most of the
time in the study of individual human action. They have also
imitated physical science’s system of professional advancement
by means of publishing scholarly articles in academic journals,
meaning the top dozen or so professional journals.’

1. Thomas Kuhn, The Structure of Scientific Revolutions (Chicago: University of
Chicago Press, 1962), p. 20.

2. Anthony Standen, Science Is a Sacred Cow (New York: Dutton, 1950).

3. John J. Siegfried, “The Publishing of Economic Papers and Its Impact on
Graduate Faculty Ratings, 1960-1969,” Journal of Economic Literature, X (1972), pp.
31-49. A. W. Coats writes: “In the process of acquiring his professional qualifications,
every fledgling economist is initiated into the prevailing occupational folklore, part
of which consists of opinions about the aims, characteristics, and comparative prestige
ratings of the various periodical publications in the field. . . . [These opinions’]

 
