3

COASE VS. PROPERTY RIGHTS

Anomaly appears only against the background provided by the paradigm.
The more precise and far-reaching that paradigm is, the more sensitive
an indicator it provides of anomaly and hence of an occasion for para-
digm change... . By ensuring that the paradigm will not be too easily
surrendered, resistance guarantees that scientists will not be lightly
distracted and that the anomalies that lead to paradigm change will
penetrate existing knowledge to the core.

Thomas Kuhn’

We come now to the issue of property rights. The meaning
of “property rights” is this: individuals or associations repre-
sented by individuals possess a legal right to prevent others
from stealing, invading, destroying, or otherwise interfering
with their property. Owners therefore possess a legal right fo
exclude others from the use of specified property. This is analo-
gous to covenantal forms of exclusion: the State’s right to ex-
clude non-citizens from voting; the married person’s right to
exclude others from sexual access to the partner; and the
church’s right to exclude non-members or non-Christians from

L. Thomas Kubn, The Structure of Scientific Revolutions (Chicago: University of
Chicago Press, 1962), p. 65.
