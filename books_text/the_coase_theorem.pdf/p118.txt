100 THE COASE THEOREM

mists — as a landmark essay. What it is, on the contrary, is clev-
er sophistry: a land mine essay.

The Myth of Value-Free Social Science

In a brilliant yet almost despairing essay, Arthur Allen Leff
has described the development of modern legal theory: a war
between legal formalism (the “logic of the law”) and legal em-
piricism or positivism (“man announces the law”). The fact is,
this debate goes back at least to the Socratic revolution in Greek
political thought: the debate over physis (nature) and nomos
(convention).> Writes Leff: “While all this was ‘going on, most
likely conditioning it in fact, the knowledge of good and evil, as
an intellectual subject, was being systematically and effectively
destroyed.” What he calls the swamp of historical legal studies
was replaced by the desert of legal positivism: the “normative
thought crawled out of the swamp and died in the desert.” He
continues:

There arose a great number of schools of ethics — axiological,
materialistic, evolutionary, intuitionist, situational, existentialist,
and so on - but they all suffered the same fate: either they were
scen to be ultimately premised on some intuition (buttressed or
not by nosecounts of those seemingly having the same intuitions)
or they were even more arbitrary than that, based solely on some
“for the sake of argument” premises. I will put the current situa-
tion as sharply and nastily as possible: there is today no way of
“proving” that napalming babies is bad except by asserting it (in
a louder and louder voice) or by defining it as so, early in one’s
game, and then later slipping it through, in a whisper, as a conclusion.®

5. On the rival conceptions of Jaw, see Sheldon Wolin, Politics and Vision: Continue
ity and Innovation in Western Political Thought (Boston: Little, Brown, 1960), pp. 29-34.
On plysis, see Robert A. Nisbet, Social Change and History: Aspects of the Western Theory
of Development (New York: Oxford University Press, 1969}, pp. 21-29.

6. Arthur Allen Leff, “Economic Analysis of Law: Some Realism About Nomin-
alism,” Virginia Law Review, LX (1974), p. 454.
