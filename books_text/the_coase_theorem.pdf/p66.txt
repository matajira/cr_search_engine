4

ROTHBARD’S CHALLENGE TO COASE

Often a new paradigm emerges, at least in embryo, before a crisis has
developed far or has been explicitly recognized.

Thomas Kuhn!’

One economist who has seen at least some of the implica-
tions of Coase’s position is Murray Rothbard. Rothbard very
early recognized the truth of Robbins’ refutation of Pigou,
namely, that there can be no scientifically valid interpersonal
comparisons of subjective utility.? He has written a critique of
the Coase theorem which underscores some of the points I
raised in the original draft of this study before I discovered
Rothbard’s 1982 essay. But he goes to the full logical conclusion
of the subjectivist school, namely, that there can be no such thing
as social cost - not simply that economists cannot measure it, but
that it does not exist as a category of economics.’ He discusses

1, Thomas Kuhn, The Structure of Scientific Revolutions (Chicago: University of
Chicago Press, 1962), p. 86.

2. Murray N. Rothbard, “Toward a Reconstruction of Utility and Welfare
Economics,” in Mary Sennholz (ed.), On Freedom and Free Enterprise: Essays in Honor
of Ludwig von Mises (Princeton, New Jersey: Van Nostrand, 1986). This has heen
reprinted by Liberty Press, Indianapolis, Indiana.

 

3. The Christian economist must reject this thesis. There are indeed social costs
