Rothbard’s Challenge to Coase 61

matched (“correlation of at least .9”) only by the irrelevance of its
conclusions.

Conclusion

The dilemma faced by modern economists is to explain the
time-bound, uncertainty-bound processes of the market in
terms of timeless logical categories (“models”). They try to ex-
plain change in terms of fixed laws, time in terms of timeless-
ness, uncertainty in terms of omniscience (i.e., equilibrium
conditions), They need to show how the concept of efficiency
applies to (1) objective, real-world decisions of men and (2)
subjectivist economic theory. They need to show how policy-
making can be abstracted from both ethics and the concept of
objective value. They need to show how social cost is a legiti-
mate extension of the idea of subjective cost. They need to
show how expected costs can be linked to retrospective costs,
both individual and social.

Economists have long assumed not only that these epistem-
ological problems can somehow be solved some day in terms of
modern economic theory, but more to the point, that these
problems can safely be ignored until that later day. Economists
have quietly dropped any public discussion of these problems
until that day arrives. But that later day is the equivalent of the
advent of equilibrium: a theoretical limiting concept, not a
target date. Meanwhile, the blessed ones deposit their objective
Nobel Prize checks in the loca! objective bank accounts. Nice
work if you can get it.

Rothbard has faced this crucial epistemological limit on the
science of economics: “I contend that no advocacy of public
policy, however seemingly ‘scientific,’ can be value free; none
can escape taking an ethical position. Far better, then, to frame
one’s ethics clearly and consciously, instead of smuggling them
in, ad hoc and unanalyzed, as implicit assumptions of one’s,
