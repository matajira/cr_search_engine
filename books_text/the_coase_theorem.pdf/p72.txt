54 THE COASE THEOREM

Genuine choice is confronted only in a world of uncertainty,
and, of course, all economic choices are made in this con-
text.” Take away equilibrium - from men’s thinking, that is;
it never has existed in the real world - and you thereby climi-
nate the economist’s concept of objective cost. Eliminate the
concept of objective cost, and you eliminate the possibility of
scientifically valid policy-making by economists. Eliminate the
concept of objective cost, and you also eliminate that trusty
ideological weapon of all free market economists: the idea of
the objective efficiency of the free market.

Efficiency for Whose Ends?

Here is the problem Rothbard is struggling with: How can
we discuss the question of efficiency — the coherence of plan-
ning and action - in a context of change, both with respect to a
man’s plans and the environment which he attempts to change
and yet also must respond to. Rothbard wants to believe that he
can appeal to what he calls “proficiency” in learning, but his
critique of efficiency applies equally well to proficiency. Why is
human action a learning process? Why does anything we
learned a decade, a year, or a moment ago still apply in the
now-changed world of the present? Humanists have no answer
to this fundamental question, at least none which is consistent
with their epistemology of autonomous man.

Rothbard argues correctly that “efficiency only makes sense
in regard to people’s ends, and individuals’ ends differ, clash,
and conflict. The central question of politics then be-
comes: whose ends shall rule?””” He attacks modern economics
because it is based on utilitarianism - “the greatest good for the
greatest number” — a system of ethics which assumes that it is
possible to make interpersonal comparisons of subjective utility.
Utilitarianism ultimately asserts that there is a universal common

11. Buchanan, Cost and Choice, p. 98; cf. pp. 49-50.
12. Rothbard, “Comment,” Time, Uncertainty, p. 91.
