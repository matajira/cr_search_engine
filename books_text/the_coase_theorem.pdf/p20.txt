2 THE COASE THEOREM

the cynic Diogenes’ search for an honest man - a man whose
support could not be purchased — as a wasteful expenditure of
scarce economic resources. Economists know before they begin
— begin anything ~ that “every person has his price.” There are
therefore no truly honest men. I have in mind rather the defin-
ition of the cynic that was offered by Oscar Wildc in Lady Wind-
ermere’s Fan: “A man who knows the price of everything and the
value of nothing” ~ the economist as cynic.

The economist’s dilemma — the dilemma of value vs. price —
is in fact the central dilemma of the academic discipline known
as economics, Economists search for an answer to one question
above all other questions: “What is the verifiable relationship
between value and price?” For over two centuries, generations
of economists have attempted to discover the answer, and it
eludes them today as much as it did in the days of Adam Smith.
The difference is, today the lack of any internally consistent
answer is covered by far more layers of dead ends that were
and are described as successful solutions to the problem.

Value and Price

Let us begin the search. Assume that you are interrogating
a modern economist. You ask: If all value is objective, then why
do prices keep changing? What is it that makes them change?
Answer: Supply and demand change. Why does supply change? In
response to changes in demand. Why does demand change? Because
people change their minds. Why? Because prices change. Why do
prices change? Changing conditions of supply and demand.

Wait a minute. We are going in circles. We had better talk
about demand apart from price. Sorry, you are not allowed to talk
about demand apart from price, or price apart from demand. All right,
let me ask this: If people’s changing minds are the source of
the changes in demand, then isn’t the price of anything really
based on subjective value? Ys, that is correct. Personal subjective
value? Yes, that is correct. But how is personal subjective value
translated into objective value? It isn’t; there is no objective value.
