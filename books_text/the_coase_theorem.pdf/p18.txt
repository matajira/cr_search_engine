Ifa man shall cause a field or vineyard to be eaten, and shall
put in his beast, and shall feed in another man’s field; of the
best of his own field, and of the best of his own vineyard, shall
he make restitution. If fire break out, and catch in thorns, so
that the stacks of corn, or the standing corn, or the field, be
consumed therewith; he that kindled the fire shall surely make
restitution.

Exodus 22:5-6

The traditional approach has tended to obscure the nature
of the choice that has to be made. The question is commonly
thought of as one in which A inflicts harm on B and what has
to be decided is: how should we restrain A? But this is wrong.
We are dealing with a problem of a reciprocal nature. To avoid
the harm to B would inflict harm on A. The real question that
has to be decided is: should A be allowed to harm B or should
B be allowed to harm A? The problem is to avoid the more
serious harm.

R. H. Coase*

BR. H. Coase, “The Problem of Social Cost,” Journal of Law and Economics, Til
(Oct. 1960), p. 2.
