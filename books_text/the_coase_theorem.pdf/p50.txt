32 THE COASE THEOREM

of the now-restricted (or previously executed) genocidal techno-
crats. Yet we live in a society in which the right to life has been
successfully challenged in the courts (including church courts)
in the name of personal and social costs. Should we be sur-
prised that R. H. Coase’s essay won him the Nobel Prize?

Coase offered the following example of reciprocal harm.
What about cattle that stray onto another man’s property and
destroy crops? This, it should be noted, is precisely the issue
dealt with by Exodus 22:5: “If a man shall cause a field or vine-
yard to be eaten, and shall put in his beast, and shall feed in
another man’s field; of the best of his own field, and of the best
of his own vineyard, shall he make restitution.” Coase writes:
“Tf it is inevitable that some cattle will stray, an increase in the
supply of meat can only be obtained at the expense of a de-
crease in the supply of crops. The nature of the choice is
clear: meat or crops?”**

This appears to be correct cconomic analysis, as far as it
goes. It forces us to think about the problem in terms of what
members of the society must give up, meat vs. crops. But his
next sentence is the very heart of the problem, and he never
shows how economists ~ or anyone else, for that matter — can,
as scientists, make an economically rational (i.e., value-neutral)
choice in the name of society: crops vs. meat. Indeed, humanis-
tic economics cannot possibly answer this question because of
the inability of economists, as scientists, to make interpersonal
comparisons of subjective utility.> But the economics profes-
sion refuses ta acknowledge the existence of this dilemma.

24. Idem.

25. In other words, we cannot make scientific comparisons of the utility gained
by one person vs. the utility thereby forfeited by another man. There is no unit of
“utility measurement” which is common to both men. We cannot as neutral scientists
legitimately say that one man has gained greater utility (a subjective evaluation on his
part) than another man has lost (another subjective evaluation). 1 discuss this prob-
tem in The Dominion Covenant: Genesis (2nd ed; Tyler, Texas: Institute for Christian
Economics, 1987), ch. 4.
