Rothbard’s Challenge to Coase 49

the case of the farmer whose orchard is burned by sparks emit-
ted by a passing train. His analysis focuses on the farmer’s
subjective costs that are imposed by the railroad’s aggression.
Should the State solve this dispute by forcing the railroad to
pay the farmer the market value of the lost trees?

There are many problems with this [Coase’s] theory. First,
income and wealth are important to the parties involved, although
they might not be to uninvolved economists. It makes a great
deal of difference to both of them who has to pay whom. Second,
this thesis works only if we deliberately ignore psychological
factors. Costs are not only monetary. The farmer might well have
an altachment to the orchard far beyond the monetary damage.
Therefore, the orchard might be worth far more to him than the
$100,000 in damages. . . .

The love of the farmer for his orchard is part of a larger
difficulty for the Coase-Demsetz doctrine: Costs are purcly sub-
jective and not measurable in monetary terms. Coase and Dem-
setz have a proviso in their indifference thesis that all “transac-
tion costs” be zero. If they are not, then they advocate allocating
the property rights to whichever route entails minimum social
transaction costs. But once we understand that costs are subjec-
tive to each individual and therefore unmeasurable, we see that
costs cannot be added up. But if all costs, including transaction
costs, cannot be added, then there is no such thing as “social
transaction costs,” and they cannot be compared. . . .

Another serious problem with the Coase-Demsetz approach is
that pretending to be value-free, they in reality import the ethi-
cal norm of “efficiency,” and assert that property rights should
be assigned on the basis of such efficiency. But even if the con-

and social benefits. This is one reason why the Bible can and does specify certain
social policies. They are beneficial for the covenanted community. But Rothbard's
logic is correct: in terms of the presuppositions of modern, subjectivist economics,
there is no way to add up subjective costs or benefits. In fact, consistent reasoning
leads us to conclude further that this conclusion applies to any attempt by economists
scientifically to measure intrapersonal subjective utilities. Since actions and evaluations
take place over time, economists would have to construct an “index number of per-
sonal satisfaction” - an impossibility, given the premises of subjective utility,

 
