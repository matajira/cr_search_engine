Coase vs. Property Rights 45

guided tours into (but never out of) this conceptual hall of
mirrors.

The allocation problem of welfare economics cannot be
solved by humanist economics, for the economists are overcome
by a series of antinomies: the subjective-objective dualism, the
individual-society dualism, the problem of fixed law and the
endless flux of circumstances, and the overwhelming and unan-
swered problem of interpersonal comparisons of subjective
utility. It is all premised on this formula: dialectics plus intuition
equals cost-effective justice. This formula does not produce any-
thing except additional scholarly articles for professors’ vitae —
in short, negative social returns.

Third, and far more important for social analysis, there
would be a sense of outrage among the victims of the polluting
factory if there were no State-enforced liability rules. The initial
reaction of any one of the victims, if he knows that the civil law
does not protect his ownership rights automatically, may be to
blow up the factory or murder its owner. The multiplication of
acts of violence would be assured under such a non-liability
legal order. The issue of economic efficiency therefore cannot be separ-
aled from the issue of judicial equity. This is what Chicago School
economists and legal theorists never show any signs of having
understood, When rightcous men are thwarted in their cause
by seckers of local “efficiency” who care nothing about the
ethics of the solution, there will be serious social consequences.
To discuss the efficiency of any given transaction without also
discussing the equity of it is to begin to deliver the society into
the hands of socialist revolutionaries. Or, to put it in language
more familiar to Chicago School economists, penalizing righteous-
ness in the name of economic efficiency is not a zero-cost decision. Any
approach to economics that ignores righteousness and justice as
valid economic factors is a trip into the hall of mirrors. Yet this
is almost universally the assumption of all schools of modern
economics.
