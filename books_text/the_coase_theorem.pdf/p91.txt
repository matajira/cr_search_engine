“Weighing Up the Gains and Losses” 73

not conclude in print that there is no theoretically valid eco-
nomic difference between profit-seeking activities and criminal
acts; there are only differences in net social utility? But he does
not go this far. It is almost as if some last remaining trace of
common sense and moral values has kept Dr. Becker from
pursuing the logic of his position,

His followers have not been so reticent: “An individual deci-
sion to commit a crime (or not to commit a crime) is simply an
application of the economist’s theory of choice. If the benefits of
the illegal action exceed the costs, the crime is committed, and
it is not if costs exceed benefits. Offenders are not pictured as
‘sick’ or ‘irrational,’ but merely as engaging in activities that
yield the most satisfaction, given their available alternatives.”
Then what of the warning of God in Proverbs? “All they that
hate me love death” (8:36b). Of course: just redefine suicidal
criminal behavior in terms of the criminal’s subjective prefer-
ence for death, assume the existence of subjective ordinal (or
even cardinal) utility in his subjective value preference scale,
and economic analysis still holds! Common sense disappears,
but economic analysis, like the smile of the cheshire cat, re-
mains. (In all honesty, this kind of economic analysis goes back
to the laie eighteenth century. Jeremy Bentham used a very
similar approach based on net pleasure or pain. Mercifully, the
academic world had not yet discovered either econometrics or
multivariate regression analysis, so his essays were literate and
coherent.)

Becker was too timid to pursue his remarkable thesis very
far. Let me show you where it leads. What about the net social
cost or net social benefit of murder? He writes that “the cost of
murder is measured by the loss of earnings of victims and
excludes, among other things, the value placed by society on

83. Morgan O. Reynolds, “The Economics of Criminal Activity” (1973), reprinted
in ibid., p. 34.
