76 THE COASE THEOREM

him would be a victimless crime, and therefore necessarily
outside conventional economic policy analysis.)

The biblical view of man rests on the presupposition that
there are two kinds of people: covenant-breakers and covenant-
keepers. There is also such a thing as common grace.*' When
God removes it, people become more consistent with their own
ethical presuppositions. Increasing numbers of covenant-break-
ers turn to crime as an expression of their ethical rebellion
against God. The economics of crime and punishment no doubt
can be discussed in part in terms of criminals’ expected costs
and benefits, but equally important, if not more important, is
the psychological link between crime and certain forms of ad-
diction, especially the addiction to illicit thrills and danger.
People’s tastes are not stable, contrary to Chicago School econo-
mists; people can and do develop an addiction to criminal
behavior. They need ever-increasing doses of crime to satisfy
their habit. Thus, to analyze all economic actors in terms of the
pure logic of expected profit and loss is a fundamental error of
modern economic analysis.

Becker disagrees. He wants to consider only people’s per-
ceived costs and benefits, risks and rewards, net. The logic of
Becker’s position seems to infer the right of a criminal to inflict
damage as heavy as murder so long as he can demonstrate in
court through cost-benefit analysis that the particular murder
produced net social utility. Coase, writing eight years earlier,
was more judicious in his conclusions. He wanted only to assert
the right at some price of an individual to inflict on other people
less permanent forms of damage than murder.

The “Right to Inflict Damage”

Coase considers an example taken from Pigou’s Economics of
Welfare. Suppose that it would pay a railroad firm to run a train

41. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987).
