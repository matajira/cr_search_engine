There’s No (Autonomous) Accounting for Taste ig

our lifetime performance objectively, moment by moment, and
also at the day of final judgment. There is objective continuity
over time and across interpersonal barriers because man is
made in God's image, and God is objectively sovereign.

The God of the Bible is the basis of a theoretical resolution
of the subjective-objective dualism of all humanist thought. He
is therefore the fundamental presupposition of all valid eco-
nomics. Without God's comprehensive planning and God's
comprehensive judgments (evaluations) through history and in
eternity, there is no way theoretically for the economist to
“bridge the gap” between subjective value theory and the objec-
tive reality of the objective numbers in the capitalist’s account
books. God is the subjective Author of objective accounting,
including index numbers, and man, who is made in God’s
image, can use accounting techniques to the glory of God and
the benefit of society.

Conclusion

The modern economist does not want to deal forthrightly
with the fundamental dualism of accounting theory because this
problem is a manifestation of the epistemological crisis - the
crisis of dialecticism — in humanism’s various economic theories.
The economist shrugs off such philosophical criticism as periph-
eral to his task. These epistmological problems have no solu-
tions that are consistent with the economist’s presuppositions
concerning God, man, law, and time. Therefore, the modern
economist concludes that they are irrelevant. He chooses to
deal only with problems that may have solutions, and price-
competitive solutions at that. So, when his ideological colleagues
reach conclusions that sound irrational, immoral, or irrelevant
— especially irrelevant — he pays no attention. Irrelevance is par
for the academic course. In fact, it is a way of life in the profes-
sional journals. Yet economics was announced by its developers
to be the most relevant of all academic inquiries: inquiries into
the wealth of nations. God is not mocked at zero price.
