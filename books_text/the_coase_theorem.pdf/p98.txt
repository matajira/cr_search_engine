80 THE COASE THEOREM

Even more important, there must also be compensation for
the loss of security that is necessarily involved in every willful
violation of another man’s property rights. Exodus 22:5 re-
quires that restitution be paid with “the best” of the violator’s
field, “and of the best of his own vineyard.” To argue, as Coase
does, that as far as society is concerned, it is economically irrele-
vant to the total economic value accruing to society whether the
farmer (victim) builds the fence at his expense or the cattleman
(violator) does so at his expense is to place zero price on the
rights of ownership. When free market economists place zero econom-
ic value on the rights of ownership, they have given away the case for
the free market. This is precisely what Coase and the many aca-
demic “economics of law” specialists have done. They have
preferred the illusion of value-free economics to the ideal of
private property — our legal right to exclude others from using
our property.

Theft as a Factor of Production

Coase explicitly argues that the ability to cause economic injury
is a factor of production. Therefore, the State’s decision to deny a
person the right to exercise this ability involves a social cost: the
loss of a factor of production. “If factors of production are
thought of as rights, it becomes easicr to understand that the
right to do something which has a harmful effect (such as the
creation of smoke, noise, smells, ctc.) is also a factor of produc-
tion. Just as we may use a piece of land in such a way as to
prevent someone else from crossing it, or parking his car, or
building his house upon it, so we may use it in such a way as to
deny him a view or quiet or unpolluted air. The cost of exercis-
ing a right (of using a factor of production) is always the loss
which is suffered elsewhere in consequence of the exercise of
that right - the inability to cross land, to park a car, to build a

TI Spring 1982), p. 58.
