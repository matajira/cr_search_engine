 

“Weighing Up the Gains and Losses” 67

What the reader should be aware of is that the practitioners
of economics are unhappy with the public’s perception of their
trade. On the one hand, the economist as rigorous scientist
cannot do without the concept of equilibrium to build his theo-
ries, and this concept begins with the presupposition of perfect,
zero-cost knowledge. Then the economist attempts to impose
this equilibrizm model onto the error-filled real world, “making
appropriate modifications,” of course. Problem: the moment
you make any modification, the model disintegrates. At best,
the equilibrium model is useful as a platform for making intu-
itive leaps of faith. Intuitive leaps of faith are inescapable as-
pects of all economic thought, but they are something that
economists prefer not to discuss, even in private.’

Becker’s Breakthrough

Gary Becker insists that his approach to crime and punish-
ment does not “assume perfect knowledge, lightning-fast calcu-
lation, or any of the other caricatures of economic theory.””
Dr. Becker is self-deceived; this is exactly what all discussions of
socially optimum decision-making must assume. This so-called
caricature is in fact the heart, mind, and soul of modern eco-
nomics as an academic discipline. Without it, there could be no
Mathematics or equations in economic analysis, and without
mathematics, one rarely gets into print in the prestigious schol-
arly economics journals.” Certainly, Dr. Becker’s essay is
made nearly unreadable by page after page of pseudo-scientific
equations, as are most of his other essays. (I have decided to

Interest (Fall 1965); reprinted in Nisbet, Tradition and Revolt: Historical and Sociological
Essays (New York: Random House, 1968), ch. 12.

11. North, “Economics: From Reason to Intuition,” in North (ed.), Foundations
of Christian Scholarship (Vallecito, California: Ross House, 1976), ch. 5.

12. Gary §, Becker, “Crime and Punishment: an Economic Approach” (1968), in
Essays in the Economics of Crime and Punishment, p. 9.

13, John Kenneth Galbraith, Economics Peace and Laughter (Boston: Houghton
Mifflin, 1971), ch. 2.
