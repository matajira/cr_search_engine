INTRODUCTION

Costs and benefits cannot be compared across individuals, even when
monetary sums ave involved, because of the impossibility of interpersonal
utility comparison. This insight is a straighiforward application of the
defining principle of the Austrian school: radical subjectivism.'

Since ail costs and benefits are subjective, no government can accurately
identify, much less establish, the optimum quantity of anything. But even
the tort [private law suit over wrongs - C.N.] approach runs up against
the immeasurability of costs and benefits: how are damages to be deter-
mined?

Another problem is the lack of a method for calculating the effect of a
decision or policy on the total happiness of the relevant population. Even
within just the human population, there is no reliable technique for
measuring a change in the level of satisfaction of one individual relative
to a change in the levet of satisfaction of another$

Economists are a cynical bunch. What is a cynic? I do not
mean the Greek definition. A modern economist would regard

1. John B. Egger, “Comment: Efficiency Is Not a Substitute for Ethics,” in Mario
Rizzo (ed.), Time, Uncertainty, and Disequilibrium (Lexington, Massachusetis: Lexington

Books, 1979), p. 121. Italics not in original.

2. Charles W. Baird, “The Philosophy and Ideology of Pollution Regulation,”

Cato fournat, 1i (Spring 1982), p. 303. Italics not in original.

3. Richard A. Posner, The Economics of Justice (Cambridge, Massachusetts: Har-

vard University Press, 1983), p. 54. Italics not in original.
