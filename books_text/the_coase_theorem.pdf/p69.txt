Rothbard’s Challenge to Coase 51

no one’s action can be called “efficient.” We live in a world of
uncertainty. Efficiency is therefore a chimera.

Put another way, action is a learning process. As the individ-
ual acts to achieve his ends, he learns and becomes more profi-
cient about how to pursue them. But in that case, of course, his
actions cannot have been efficient from the start — or even from
the end — of his actions, since perfect knowledge is never
achieved, and there is always more to learn.

Moreover, the individual's ends are not seally given, for there
is no reason to assume that they are set in concrete for all time.
As the individual learns more about the world, about nature and
about other people, his values and goals are bound to change.
The individual's ends will change as he learns from other peo-
ple; they may also change out of sheer caprice. But if ends
change in the course of an action, the concept of efficiency -
which can only be defined as the best combination of means in
pursuit of given ends - again becomes meaningless.*

Two comments are in order. First, we can perceive the whole
corpus of economics steadily slipping through our fingers. If
the question of efficiency is meaningless, what have economists
been arguing about over the last three centuries? An illusion?
The answer must be yes, if we hold to a rigorously subjectivist
epistemology. “Not only is ‘efficiency’ a myth, then, but so too
is any concept of social or additive cost, or even an objectively
determinable cost for each individual. But if cost is individual,
ephemeral, and purely subjective, then it follows that no policy
conclusions, including conclusions about law, can be derived
from or even make use of such a concept. There can be no
valid or meaningful cost-benefit analysis of political or legal
decisions or institutions.”® Rothbard has shown the intellectual
courage to affirm the validity of the implications that Roy Har-

5. Murray N. Rothbard, “Comment: The Myth of Efficiency,” in Mario J. Rizzo
(ed.), Time, Uncertainty, and Disequilibrium (Lexington, Massachusetts: Lexington
Books, 1979), p. 90.

6. Ibid., p. 94.
