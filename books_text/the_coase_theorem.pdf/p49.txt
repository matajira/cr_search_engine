The Coase Theorem 31

inflict harm on A. The real question that has to be decided
is: should A be allowed to harm B or should B be allowed to
harm A? The problem is to avoid the more serious harm.”

Such reasoning is ethically perverse, if accepted as a method-
ological standard governing economic analysis in all instances
involving economic action. It would be just as easy to say of
kidnapping that any restrictions on kidnapping by the State
harm the kidnapper, and that a lack of restrictions harms the
victims. If we are going to build an economic system in terms of
the supposedly “reciprocal nature of harm” — that each eco-
nomic actor suffers harm when he is restricted from acting
according to his immediate whim — then economics becomes
positively wicked, not value-free, in its attempt to sort out just
how much harm the courts will allow each party to impose on
the other.

There are some areas of life - areas governed by biblical
morality - in which such “cost-benefit analyses” must not even
be contemplated. For example, any attempt to impose cost-
benefit analyses on competing techniques of mass genocide,
including abortion, is demonic, not scientifically neutral.
Whether a genocidal society should adopt either gas chambers
or lethal injections for adults, or either saline solutions or suc-
tion devices for unborn infants, cannot be solved in terms of
comparative rates of cost-efficiency, for the economist always
ignores a major “exogenous variable”: the wrath of God. God
will efficiently judge those individuals who promote all such
cost-efficient systems, as well as societies that adopt them. If
legal restrictions against mass genocide harm the potential mass
murderers and the purchaser of their services, this is all to the
good. Society faces no “reduction in social benefits” whatsoever.
Justice does cost something, but the net economic cffect is
positive, whether the economist sees this or not. There is no
reduction in net social benefits as a result of the thwarted goals

23. Coase, “Social Cost,” p. 2.
