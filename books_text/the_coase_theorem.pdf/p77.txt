Rothbard’s Challenge to Coase 59
Timeless Metaphysical Models

Mises relies on this limiting concept of a hypothctical econo-
my filled with soulless people in order to explain the operations
of real-world market forces. “This final state of rest is an imagi-
nary construction, not a description of reality. For the final
state of rest will never be attained. New disturbing factors will
emerge before it will be realized. What makes it necessary to
take recourse to this imaginary construction is the fact that the
market at every instant is moving toward a final state of
rest.” He calls this movement toward (or “tendency toward”)
a final state of rest a fact. But this “fact” is precisely what must
be demonstrated.” It is the economists’ version of the familiar
pre-Socratic contradiction between Parmenides’ changeless and
limeless logic and Heraclitus’ ceaseless historical flow. These
two worlds cannot be shown to be connected; nevertheless, they
are correlative in the thinking of humanistic scholars.

To explain this intellectual dilemma, Van Til uses the de-
lightful analogy of someone who is trying to put together a
string of beads, but the string is infinitely long, and the beads
have no holes. The imaginary world of timeless logic (Van Til’s
“string”) which cannot possibly exist serves as the limiting concept
(to use Kant’s terminology for the “noumenal”),”’ or limiting
notion (to use Mises’ term)** for our understanding of the
world which does exist - the world of ceaseless flux (Van Til’s
“beads”). This world of timeless logic is, in short, a logical back-
drop which cannot ever exist in the real world - and which

Nostrand, 1960), p. 24. Cf. Mises, Ultimate Foundation, p. 3.

25. Idem.

26. The problem Mises is dealing with is the problem of analyzing change in
terms of fixed mental categories. “There is no means of studying the complex
phenomena of action other than first to abstract from change altogether, then to
introduce an isolated factor provoking change, and ultimately to analyze its effects
under the assumption that other things remain equal.” Mises, Human Action, p. 248.

27. Immanuel Kant, Critique of Pure Reason, translated by Norman Kemp Smith
(New York: St. Martin’s, [1929] 1965), B311, p. 272.

28. Mises, Human Action, p. 249.

 

 
