68 THE COASE THEOREM

com a new adjective that describes this pseudo-scientific ap-
proach to economic reasoning: psientific.)

Becker insists that “This essay concentrates almost entirely
on determining optimal policies to combat illegal behavior and
pays little attention to actual policies.” In this regard, the
essay is representative of virtually the whole field of law and
economics. Becker prefers equations and equilibrium to person-
al responsibility when it comes to suggesting what should be
done about crime. He and his colleagues refuse to honor
Baird’s warning: “Since all costs and benefits are subjective, no
government can accurately identify, much less establish, the
optimum quantity of anything.”’* Admit this, and 90 percent
of what gets published in the professional academic journals
would have to be rejected by the editors. Where, under such
academically sub-optimal circumstances, would a career econo-
mist publish an essay such as Isaac Ehrlich’s representative
example, “Optimal Participation in Illegitimate Market Activi-
ties: A One-Period Uncertainty Model”?'®

Biblical law is the foundation of optimal social and economic
policies — the only foundation that honors God and can there-
fore produce long-term benefits: covenantal blessings. This is
why we need to adhere to the Bible’s system of penalties to be
imposed by the civil government; without this, we are flying
blind. We are flying as blind as Gary Becker is when he writes:
“A wise use of fines requires knowledge of marginal gains and
harm and of marginal apprehension and conviction costs; ad-
mittedly, such knowledge is not easily attained.” Not easily
attained! In terms of the logic of subjective economics, such

14. Essays in the Economics of Grime and Punishment, p. 44.

18. Charles W. Baird, “The Philosophy and Ideology of Pollution Regulation,”
Cato Journal, If (Spring 1982), p. 303.

16. Actually, this was only a subsection in his influential and equation-filled
article, “Participation in legitimate Activities: An Economic Analysis,” in Essays in the
Economics of Crime and Punishment.

17. Becker, in ibid., p. 28.
