 

There’s No (Autonomous) Accounting for Taste 113

inflation, but in fact are a result of inflation. Inflation is an
increase in the money supply. But what does he mean, “rising
prices”? Which prices? Gerald P. O'Driscoll, who is presently
employed by the Federal Reserve Bank of Dallas, and Sudha R.
Shenoy, both of whom are ardent defenders of subjectivist
economics, felt compelled to discuss such statistical relation-
ships. They wrote in 1976: “However, after 1945, the problem
turned around completely and became that of gently (and later,
more rapidly) rising prices. In eleven major developed coun-
tries, prices declined hardly at all, and when they did, it was
only for a couple of years during the early fifties.”

The question arises: How can they know what “prices” -
prices in general — did? Because they have statistical evidence.
But how did these statistical data come into their possession?
Because other economists constructed national index numbers
of prices in terms of various implicitly objective theories of eco-
nomics. We are back to the problem of intuition, the unstated
but crucial basis of modern economics. Kant’s dualism remains.

Changing Tastes (Discontinuity)

We also have another problem: personal tastes that change over
time. What an economic actor thought was a great idea when he
began planning may have changed. He may be like the man
who stripped naked and leaped onto a cactus plant. When
asked later on why he did it, he replied: “It seemed like a good
idea at the time.” The economic actor originally wanted to
achieve one set of goals, but now he has changed his mind. His
tastes have changed, and there is no accounting for changing
tastes. I mean literally no accounting. In fact, most modern econ-
omists, but especially “Chicago School” economists, ignore the
relevance of changing tastes precisely because changing tastes

13. Gerald P. O'Driscoll, Jr, and Sudha R. Shenoy, “Inflation, Recession, and
Stagfladon,” in Edwin G. Dolan (ed.), The Foundations of Modem Austrian Economics
(Kansas City, Kansas: Sheed & Ward, 1976), p. 186,
