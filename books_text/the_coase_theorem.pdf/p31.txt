THE PERSISTENT PROBLEM OF VALUE

In both political and scientific development the sense of maifunction that
can lead to crisis is prerequisite to revolution.

Thomas Kuhn'

Economists, as self-consciously humanistic social scientists,
claim to be defenders of a rational academic discipline. Most of
them defend their methodology in terms of the assertion that it
allows them to make accurate predictions of human actions
under limited, specified conditions.’ These predictions are
supposed to enable people to make economic decisions that are
more profitable than decisions made by flipping a coin, consult-
ing a fortune teller, or throwing darts at a wall covered with
slips of paper, with each slip containing a different suggested
course of action.

‘To make their claim believable, economists have to make a
myriad of assumptions about reason, the human mind, the
powers of observation, the external world, and the interrela-

1. Thomas Kuhn, The Structure of Scientific Revolutions (Chicago: University of
Chicago Press, 1962), p. 91.

2. Milton Friedman, Essays in Positive Economics (Chicago: University of Chicago
Press, 1953), ch. 1: “The Methodology of Positive Economics.”
