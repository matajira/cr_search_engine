140 The Judeo-Christian Tradition

range of 3,000 volumes of these records, with at least 300,000
judgments, have been compiled.?* While these records until re-
cently were unindexed (they arc now being put on computer in
Israel),2* and therefore were usable only by highly trained special-
ists who possessed astounding memories, the basic conclusions are
known. Thus, Horowitz’s statement is probably representative of
the history of Jewish decisions regarding kidnapping: not one
conviction.

Michael Guttman made a similar assessment: “The general
principle upon which the Mishnab has te be valued juridically is
the endeavor to restrict death punishment to a minimum. The
Talmud could not flatly annul the death penalty since a Pen-
tateuchal law could not be abrogated; therefore the requirements
pertaining to the giving of evidence and the proof of premeditation
were made so severe that a death verdict was almost impossible.”2

Covenantal Sanctions

One reason for this reticence to impose the penalties estab-
lished in the Old Testament was that the Jews believed that every
Jewish court had to have at least one judge who had been ap-
pointed by the laying on of hands (semikak) by a preceding judge.
Like the rabbi who supposedly could trace his teachers back to
Moses, so was the judge. But there was a problem. This laying
on of hands could take place only in the Holy Land. “A court not
thus qualified,” writes Horowitz, “had no jurisdiction to impose
the punishments prescribed in the Torah.”26

After the Bar Kochba revolt ended in 135, the Romans scat-
tered the Jews throughout the Empire; the Diaspora began in
earnest. This loss of residence was used as an excuse by the rabbis

23. Menachem Elon, “Introduction,” in Elon (ed.), Principles of favish Law (Jerusa-
lem: Keter, [19752]), col. 13.

24, “Computer Digests the Talmud to Help Rabbis,” New York Times (Nov. 24,
1984).

25. Michael Guttman, “The Term ‘Foreigner’ Historically Considered,” Hebrew
Union College Aromat, WMA (1926), p. 17.

26. Horowitz, Spirit of Jewish Law, p. 93.
