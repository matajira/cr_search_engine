2
POLITICAL IDOLATRY

Preach the word; be instant in season, out of season; reprove,
rebuke, exhort with all longsuffering and doctrine. For the time will
come when they will not endure sound doctrine; but after their own lusts
shall they heap to themselves teachers, having itching ears; And they
Shall turn away their ears from the truth, and shail be tumed unto
Sables (HT Timothy 4:2-4).

This book grew out of an appendix in my book, Tools of
Dominion.' I realized after | wrote it that few people would ever
read an obscure appendix in a large Bible commentary on biblical
economics. Yet what I had discovered in writing that book and the
appendix deserves a wider audiencc. So, here is a slim book that
gets right to the point.

What is the point? Simple: “There are fundamental and irrec-
oncilable differences between Orthodox Judaism and orthodox
Christianity.” This may sound obvious to all concerned: Jews are
Jews, and Christians are Christians. But what I present here is
evidence of the extent to which the two religious systems do not
and cannot agree, especially in the area of civil law and its appro-
priate sanctions.

But if they do not agree, then what about the much-heralded
Judeo-Christian tradition? This was the question that I finally

1. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1990), Appendix B: “Maimonides? Code: Is It Biblical?”

20
