48 The Judeo-Christian Tradition

sovereignty in history? It is the question of representation.

Every system of thought needs a system of interpretation, This
applies to political systems, cconomic systems, and all othcr kinds
of systems. Men seck information about the way the world warks.
To discover this, they must somehow penetrate the “noise” of this
world to the coherence — if any — beneath the observable surfacc.
(And if there is no underlying coherence, then the interpreter must
then interpret the facts accordingly — in terms of some coherent
principle, of course.)

This means that men must seck the source of sovereignty, the
voice of authority. Something makes the world go round (my apolo-
gies to flat-earthers and geocentrists). If we can begin to under-
stand even the barest essentials of this something, we can gain
wisdom, power, and a great deal of responsibility. Or, on the other
hand, we can run for our lives. But at least we will be running from
something we recognize and partially understand,

The point is, in order to become knowledgeable people, we
must seck the truth. Whatever is the source of this truth is the
sovereign of the system, or at least eur system. While I do not wish
to belabor the next point, T will nevertheless offer it for your careful
consideration: “, . . in any culture the source of law is the god of that
society."' Put another way: “Similarly, only the power who is
ultimate has the right to be the source of law.”? And when I say
“law,” I mean covenant law: the law of God for man.

Both Jews and Christians say that the ultimate Sovereign,
God, has spoken to man authoritatively in the Old Testament.
But then the debate begins. Neither group believes that the Old
Testament “speaks for itself.” Each believes that there is an even
more authoritative commentary on the Old Testament. Christians
say that this is the New Testament. Orthodox Jews say that this
is the Talmud, In this conflict over the proper source of interpreta-

1. R. J. Rushdoony, The Jnstitutes of Biblical Law (Nutley, New Jers
1973), p. 4.

2. Bid, p. 34.

Graig Press,

 
