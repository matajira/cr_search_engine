62 The Judeo-Christian Tradition

covenant for non-Israelite civil law has been offered by Calvinist
theologian John Murray and also by neo-dispensational thcologi-
ans H. Wayne House and Thomas I. Ice. In fact, all three of them
conclude that there is only one biblically required sanction im
Noah’s covenant, capital punishment for murder. This, they be-
licve, is the only biblical law that God has required all men to
obey throughout mankind’s post-flood history.’ The Talmud at
least adds an additional six laws that God specifically established
through Adam and Noah that gentiles are supposed to honor
throughout history.

How Little Most People
Know About Judaism

Maimonides’ opinion regarding the immorality of non-Jews
who read the Old Testament would probably come as a shock to
most Christians, assuming they had ever heard of Maimonides
and his Mishnek Torah. It might even come as a shock to most
contemporary Jews. The average Bible-believing Christian in the
United States knows very little about post-New ‘Testament Juda-
ism, He may be vaguely aware that American Judaism is divided
into three theological wings: Reform (liberal}, Conservative, and
Orthodox. He may also be aware that European Judaism has two
great ethnic branches: the Sephardim® (those whose ancestors
once lived in Spain, Portugal, or the Eastern Mediterranean) and
the Ashkenazic Jews'® (those who came west from Russia and
Poland), who were the Yiddish-speaking Jews in the late 1800's
and early 1900's, prior to their linguistic assimilation into Ameri-

 

8. John Murray, Principles of Conduct: Aspects of Biblical kthics (Grand Rapids,
Michigan: Eerdmans, 1957), pp. 118-19; House and Ice, Dominion Theology: Curse ar
Blessing? (Portland, Oregon: Multnomah, 1988), p. 130.

9. Heinrich Graetz, History of the Jos, 6 vols. (Philadelphia: Jewish Publication
Society of America, 1894), IV, chaps 10-14. ‘The problem with Graetz’s history is
that it is so heavily biographical. The gentile reader recognizes no one. For the
Sephardim in America, see Stephen Birmingham, The Grandees: America’s Sephardic
Elite (New York: Harper & Row, 1971).

10. Bernard D. Weinryb, The Jaws of Poland: A Social and Economic History of the
Jewish Community in Poland fiom 1100 to 1980 (Philadelphia: Jewish Publication Society
