“You Have Heard It Said” 105

“For had ye believed Moses, ye would have believed me: for he
wrote of me. But if ye believe not his writings, how shall ye believe
my words?” (John 5:46-47). The Mishnah and the Talmud are
not what we Christians might have hoped for, and what some
Christians have mistakenly believed that they are: commentaries
on the Old Testament, but with no mention of the Trinity.

It is also my contention that the unprecedented cconomic,
intellectual, and cultural strides made by Jews in the West could
begin, and did begin, only when their young men at last were
allowed to become rabbis and leaders within the community with-
out being required to go though this initiatory process. But a price
has been extracted by Western society for this advancement. The
price has been the steady secularization of the vast majority of
Jews, just as Orthodox rabbis have warned their upwardly mobile
brethren from the early decades of the nineteenth century until
today. Most Western Jews today have become little more than
Karaites without the Pentateuch, or cven like Unitarians, though
with better business conncctions.

‘To understand the extent of this later transformation in Jewish
life, it is necessary to look at what preceded it. The best way to
discover the lost worldview of Orthodox Judaism is to survey the
works of the greatest of all medieval Jewish scholars, Moses Maimo-
nides.

 
