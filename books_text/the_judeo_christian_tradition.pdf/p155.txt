Undermining Justice 141
to abandon the required sanctions of the Old Covenant:

The Rabbis were compelled, therefore, in order to preserve the
Torah and to maintain law and order, to enlarge the authority of
Rabbinical tribunals. This they accomplished by emphasizing the
distinction between Biblical penalties and Rabbinical penalties.
Rabbinical courts after the second century had no authority to
impose Biblical punishments since they lacked semikak; but as
regards penalties created by Rabbinical legislation, the Rabbis had
of necessity, the widest powers of enforcement. They instituted,
accordingly, a whole series of sanctions and penalties: excommuni-
cation, fines, physical punishment, use of the “secular arm” in
imitation of the Church, etc.”??

Thus ended, formally, the Old Govenant. It had ended judi-
cially in God’s eyes in A.D. 70, but now there could be no
mistaking what had happened. Judaism officially became rabbinic
rather than Mosaic. To “preserve the Torah,” the rabbis decided to
abandon it. That Rabbi Akiba, one of the early compilers of the
oral law, had joined with Bar Kochba and died in this revolt,?*
was fitting; the defeat of Bar Kochba was to make possible the
triumph of the Talmud over the Old Testament and its required
sanctions,

Without sanctions, there can be no covenant.” Without God’s
specified sanctions, there can be no covenant under Him, except
as a broken covenant. ‘This is the dilemma of Judaism. The
specified sanctions in the Old Testament are no longer applicable,
Orthodox Jews believe, because they are outside the land. The

27. Idem, So serious was being outside the land that one rabbi cited in the Talmud
taught that those Jews buried outside the land will not be resurrected. “R. Eleazar
stated: The dead outside the Land will not be resurrected; for it is said in Scripture,
And I will set glory in the land of the living, [implying] dhe dead of the land in which I
have my desire wil be resurrected, but the dead [of the land] in which I have no desire
will nol be resurrected.” Kethuboth V1a.

28. Supposedly he died on the very day of the birth of Judah haNasi, che compiler
of the Mishnah: J. H. Hertz, Foreword, Babylonian Talmud, Baba Kamma (London:
Soncino Press, 1935), p. xv.

29. Ray R. Sutton, That You May Prasper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4.

 
