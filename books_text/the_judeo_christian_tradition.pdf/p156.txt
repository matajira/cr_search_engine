142 The Judeo-Christian Tradition

specified sanctions of animal sacrifices are also gone. The Temple
was destroyed in A.D. 70. Yet without these sanctions — against
criminals and against animal representatives — there cannot be
Old Covenant religion, ‘There can only be a broken covenant.

Jews do not admit that they have lived under a broken cove-
nant since the fall of Jerusalem. To do so would be to abandon
Judaism. And yet, in terms of their civic religion, they have already
abandoned Judaism. They have made their covenantal pact, not
under Jesus Christ as Lord of the universe, but under self
proclaimed autonomous man as lord of the evolutionary process.
Even Orthodox Jews, as the self-proclaimed heirs of the Pharisees,
have sold their birthright for a mess of pottage, or, given the nature
of humanism’s promises, a pot of message. The result has been the
twin pressures of the steady erosion of Orthodox Judaism or else
the steady isolation of Orthodox Judaism.
