Rabbi Moshe Ben Maimon, “Maimonides” 125

tected them from excessive contact with gentiles.? (It is also
interesting that in the twelfth century, the walled-in Jewish com-
munity of Constantinople also had its own wall that separated the
2,000 Talmudic Jews from the 500 anti-Talmudic, “Torah-only”
Karaites,)©

Forced social division is inevitably the curse of a double legal
standard in a single society. Neither group trusts the other; both
groups seek to exploit the other, or at least tolerate those within
their midst who do. This is why the Bible says, “One law shall be
to him that is homeborn, and unto the stranger that sojourneth
among you” (Exodus 12:49). This case law appears in the section
on the laws regarding strangers and the Passover; it was given to
Israel immediately after the exodus itself. This indicates how
emphatically God demands that men observe it: even their oppres-
sors, the Egyptians, are entitled to equal treatment before the law.

“For the Sake of the Peace”

The rabbis were not fools, of course. They modified this judi-
cial double standard for practical purposes, namely, “for the sake
of the peace.” Horowitz explains: “Halakot [law] and customs
which discriminated. against Gentiles and which might, therefore,
appear unjust in the eyes of the world, were not to be enforced or
practiced though perhaps ‘legally’ valid, because it might reflect
unfavorably on the Jewish people, its morals and its religion. “For
the Sake of Peace’ was in effect an equitable principle which
modified the strict law, with regard to treatment of Gentiles.”*
This was a belated recognition of the need for a unified legal

59, In 1306, Phitip IV of France evicted the Jews, repudiated his debts to them,
and confiscated their property. England drove them out in 1290, after having taxed
them heavily and soaked up their capital with forced loans that were then repudiated.
Tn 1370, they were driven from the low countries. Herbert Heaton, Economis History
of Eusope (New York: Harper & Row, 1948), p. 184.

60. This was recorded by Benjamin of Tudela in his Book of Travels (1168}; cited
in Johnson, History of the Jaws, p. 169.

61. Horowitz, Spirit of Javish Law, p. 100.
