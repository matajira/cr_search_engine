204 The Judeo-Christian Tradition

Talmud
bestiality, 83, 87
books on, 27
casuistry, 103, 126-27
censored, 75
Christians’ ignorance, 73
circumcision, 95-96
closed book, 73, 128
commentary?, 87
concealed, 101-3
curse of Ham, 163
daughters, 95
death penalty, 139-40
dialecticism, 57-58, 74, 81-83, 90-92
diet, 93
double standard, 117
cating dates, 92-93
epilepsy, 93
fingernails, 93
Gemara, 80, 89-90
gentiles prohibited, 60
getting even, 94
history of, 75-76
hostility to, 166
ignorance about, 73, 101
initiation, 103, 105
interprets Old Testament, 48
Jesus as sorcerer, 4]
Judaisms’ bond, 77
labyrinth, 75
living text, 88-89
medicine, 93
memorization, 77, 103
Mishnah, 76, 79-80, 89-90
Molech, 92
Old Testament &, 52
Orthodox Judaism, 26
pedophilia, 87, 100
publishing, 97-99
reasoning, 139

sanctions, 141
secrecy, 84-85
sin, 9+
translation, 75, 85
whitewashing, 102-3
women, 94-95
Tamari, Meir, 168
Temple, 13, 15, 38
theft, 113-14, 116-17, 133-34, 135-36, 136,
137
THEOS (covenant model), 3
time, xii, 161, 161-62
toleration, 148-49
Torah
evolving, 145-46
Maimonides, 115
man's maneuverability, 89
obedience to, 18

 

 
 

rabbinic, 80-81
Tratiner, F. R., 101-2
treason, 18, 161
truth, 48

‘Twersky, Isadore, 106, 109
universities, 150

‘Van Til, Cornelius, 7, 50, 51, 90
victim, 136-37, 138
victims, 135
victim’s rights, 13-14
visions, 12

Weiss, David, 89, 103-4
Westminster Confession, 7
Willowbank Declaration, 179
women, 94-95

Zionism, 63, 66, 154, 169
Zoroastrianism, 9
