32 The Judeo-Christian Tradition

would point to the primary fact in history that humanists also dare
not admit, namely, that the conflict of the ages is essentially and
inescapably covenantal: the broken Adamic covenant, the broken
Old Covenant, and the New Covenant. So they invent constructs
that hide the covenantal nature of this conflict. They pretend that
the West is more a fusion of two warring worldvicws rather than
a ceaseless conflict among three; this is a fundamental intellectual
strategy in their conduct of this war against both Jews and gentiles.
Tt has been remarkably successful,

‘The Renaissance was a self-conscious battle against Christian-
ity and the Church in the name of classical antiquity. But this war
had to be conducted in stealth: Christian “myths” were the neces-
sary public camouflage for a war against Christian civilization.
Robert Nisbet’s comments on Burckhardt’s view of Renaissance
humanists are correct: “He pictures them as shallow, opinionated,
rootless, alicnated, hostile to all aspects of the establishment,
especially the Church, and always willing to hire out for a term of
service to the highest bidder, businessman or prince. Burckhardt’s
resistance to all entreaties by publishers to write yet another book
on the Italian Renaissance is easily explainable: he detested the
period and its dramatis personae just as much as Burke and Tocqueville
detested the Revolution and its politiques, its Marats and Robe-
spierres.”2?

The Blindness of the Covenantal Heirs

The standard humanist textbook account of the Renaissance
and the Judeo-Christian tradition is what is taught in Christian
colleges. Seminaries say nothing; they avoid such “secular” topics
as intellectual history. Church history is taught as a separate
discipline, with endless books about long-forgotten denominational
figures who started this or that temporary movement. Church
history is never discussed as the account of a war for Christian

29. Robert Nisbet, Conservatiom: Dream and Reality (Minneapolis: University of
Mirmesota Press, 1986), p. 82.
