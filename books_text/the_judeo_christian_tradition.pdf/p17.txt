Introduction 3

Then he looked at other books of the Bible that are known to
be divided into five parts: Psalms and Matthew. He believes that
he found the same structure. Then he went to other books, includ-
ing some Pauline epistles, He found it there, too. When he dis-
cussed his findings in a Wednesday evening Bible study, David
Chilton instantly recognized the same structure in the Book of
Revelation. He had been working on a manuscript on this New
Testament book for well over a year, and he had it divided into
four parts. Immediately he went back to his computer and shifted
around the manuscript’s sections clectronically. The results of his
restructuring can be read in his marvelous commentary on the
Book of Revelation, The Days of Vengeance.’ Here, then, is the
five-point structure of the biblical covenant, as developed by Sut-
ton in his path-breaking book, That You May Prosper: Dominion By
Covenant?

1. Transcendence of God

2. Hierarchy/authority/representation

3. Ethics/law/dominion

4, Oath/judgment/sanctions: blessings and cursings
5. Succession: inheritance and disinheritance

THEOS. Simple, isn’t it? Yet it has implications beyond your
wildest imagination. Here is the key that unlocks the structure of
all human governments. Here is the structure that Christians can
use to analyze Church, State, family, and numerous other non-
covenantal but contractual institutions.

Here is the structure that allows us to analyze the American
civil religion, and its familiar religious concept, the Judeo-
Christian tradition.

Conclusion

So, let me end this introduction with another strong statement:
if you read this introductory book on the Judeo-Christian tradition

1. Dominion Press, 1987.
2. Institute for Christian Economics, 1987.
