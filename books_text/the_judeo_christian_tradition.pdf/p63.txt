Interpretation and Representation 49

tion of the sovercign Old Testament is a hidden conflict over
carthly authority.

The Principle of Interpretation

Commenting on anything requires a principle of interpreta-
tion, Principles of interpretation differ, and sometimes very sharply.
This means that rival hermeneutical principles can and do become
divisive. This is the price of open inquiry. It is a price that must
be paid on both sides.

If we believe in the God of the Bible, then we must affirm that
in His revealed Word, we have the universal principle of interpre-
tation that God requires mankind to adopt. The Bible is self-
attesting. There is no higher authority to which we can appeal. If
there were, then this would be the new source of interpretation,
the new voice of authority.

We must think God's thoughts after Him. But how? How
should we interpret the Bible, itself the basis of all interpretation?
The answer: é7btically/ Therein lies the problem. People do disagree
about how the Bible is to be interpreted. This is cspecially truc of
Christians and Jews. The division between them is above all a
disagreement over the proper interpretation of the Bible, including
a disagreement over what constitutes the Bible.

There is no way to reconcile the following rival principles of
biblical interpretation in Judaism and Christianity: 1) Jesus as the
sole fulfillment of Old Testament messianic prophecies vs. Jesus
as a false prophet and blasphemer, for which He was lawfully
executed; 2) the New ‘Testament as the sole authoritative commen-
tary on the Old Testament vs. the New Testament as falsc proph-
ecy; 3) Christians as the only true covenantal heirs of Abraham
vs. Jews as the only true covenantal heirs of Abraham. It is the
ancient debate, recently revived politically in the state of Israel,
over the question, “Who is a Jew?” It is a debate over the truth
of Paul’s assertion: “For we are the circumcision, which worship

3. In Judaism, this question is reaily, “Who is the authentic rabbi?” The rabbi
sanctions marriages and therefore the legitimacy of the children.
