Two Branches, One Bride 7

‘This was also the teaching of the great Presbyterian theologian
Charles Hodge in 1864. “The conversion of the Jews,” he wrote,.
“will be attended with the most glorious consequences for the
whole world.”? This is what I was taught at Westminster Theo-
logical Seminary in John Murray’s class on Romans 9-14.35 While
Murray’s postmillennialism was different from Cornelius Van Til’s
amillennialism, Van Til also believed that the Jews arc still impor-
tant in the overall historical plan of God, as he wrote in 1968: “And
with the rejection of Christ by the Jew, his [the Jew’s] mission in
history dissipates as the waters of a river in a desert. But Christ
will not allow the Jew thus to defeat himself in rejecting him.
Through his Spirit Christ can and will create a new heart within
him and give him true repentance toward him. Then, together
with all Gentiles who truly repent, all Israel shall be saved.”*

This is not a new view in Protestant Christianity regarding the
futurc of the Jews.> The Larger Catechism of the Westminster
Confession of Faith (1647), the classic Puritan statement of ortho-
dox faith, specified that Christians are to pray cxplicitly for the
conversion of the Jews; no other group is so singled out. The reason
given is eschatological:

In the second petition, (which is, Thy kingdom come,) acknowl
edging ourselves and all mankind to be by nature under the
dominion of sin and Satan, we pray, that the kingdom of sin and
Satan may be destroyed, the gospel propagated throughout the
world, the Jews called, the fulness of the Gentiles brought in. . .
(Answer 191).

This perspective on the future of Israel was basic to what Iain

2. Charles Hodge, Commentary on the Epistle to the Romans (Grand Rapids, Michi-
gan: Eerdmans, [1864] 1950), p. 365.

3, See John Murray, The Epistle to the Romans, 2 vols. (Grand Rapids, Michigan:
Eerdmans, 1965), Il, pp. 65-103.

4. Cornelius Van Til, Christ and the Jews (Philadelphia: Presbyterian & Refortned,
1968), p. 2.

5. Gary DeMar, The Debate Ouer Christian Reconstruction (Vt. Worth, Texas: Domin-
ion Press, 1988), Appendix B: “The Place of Israel in Historic Postmillennialism.”

 
