152 The Judeo-Christian Tradition

fore the whole religion had to change. Reform Judaism launched
a successful intellectual attack on Orthodox Judaism in the early
decades of the nineteenth century, leading to the steady isolation
of the defenders of old Pharisee tradition, and in the twentieth
century, secular Judaism and Conservative Judaism have become
the dominant traditions. Orthodox Judaism today retains very
little influence outside of the state of Isracl, Reform Judaism and
conservative Judaism are overwhelmingly dominant in the West.
Secular Jews seem to be the norm today, as far as gentiles can
discern. (The most memorable description I have ever read re-
garding the outlook of sccular Jews regarding Judaism is Lis
Harris’ description of her family, “fans whose home team was the
Jews.”)*) Anti-credalism giveth, and anti-credalism taketh away.

The Crisis of Modern Judaism

The crisis of modern Judaism is today the crisis of humanism.
Rushdoony identified the underlying problem a generation ago:
“Judaism grew out of the rejection of Jesus Christ and steadily
became humanism, and the Talmud is essentially the exposition
of humanism under the facade of Scripture. There is thus actually
no true theism, or worship of the absolute God, apart from ortho-
dox Christianity.”2? Thus, when humanism offered Jews the vis-
ible blessings of increasing their participation in secular culture,
very few Jews resisted. They already shared too many of the
presuppositions of the humanists: dialecticism, evolutionary ethics,
the open-endedness of scholarly debate, and the need for an edu-
cated elite for social advancement.

We all need to remember that the first full-scale move to
emancipate the Jews politically came during the French Revolu-
tion. This was an aspect of the French religion of democracy; it
was also part of the revolutionaries’ war against the church, It was
an aspect of French political messianism, to use J. L, Taimon’s

21, Lis Harcis, Holy Days: The World of a Hasidic Family (New York: Summit Books,
1985), p. 17.
22. Newsletter #18 (March 1, 1967).
