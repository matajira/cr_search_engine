58 The Judeo-Christian Tradition

stressed the systematization of legal principles.”?°

We need to examine some of the legal sources of the Jewish
legal tradition in order to determine to what extent there has been
or can be a Judeo-Christian tradition. Christian scholars have
seldom done this in the past, and the result has been a major
intellectual gap and therefore major blind spot in the thinking of
modern Bible-believing Christians, But blind spots are not per-
ceived by those who suffer from them unless they are shown to the
victims. This book, I trust, will make this blind spot visible.2!

20. fdem.

21. The physical blind spot in cach cye exists because of the structure of the eye.
Discover it for yourself, Get a piece of blank paper, and put an X in the middle of the
paper and a dot about two inches to the lefl, Close your right eye. Keeping your left
cye focused on the X, move the paper slowly oward your eye. At some point, the dot
will disappear from view. Your brain will continue to “cover” for your cye’s failure
by filling the visual gap with the color of the paper. The dot is too specific, so it
disappears. In short, we are all partially blind, bue we do not see this.
