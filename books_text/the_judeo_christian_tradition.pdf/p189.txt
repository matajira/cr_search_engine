Conclusion 175

man’s lost covenantal unity. This promise is to all mankind:
“There is neither Jew nor Greek, there is neither bond nor free,
there is neither male nor female: for ye are all one in Christ Jesus”
(Galatians 3:28). “Where there is neither Greek nor Jew, circumci-
sion nor uncircumcision, Barbarian, Scythian, bond nor free: but
Christ is all, and in all” (Colossians 3:11).

Al) it will take is for you to break the terms of your present
covenant. You must become a traitor to your faith, as Rahab did
so long ago: “Now therefore, I pray you, swear unto me by the
Lorn, since IT have shewed you kindness, that ye will also shew
kindness unto my father’s house, and give me a truc token: And
that ye will save alive my father, and my mother, and my brethren,
and my sisters, and all that they have, and deliver our lives from
death” (Joshua 2:12-13). There is no other way. God takes His
covenant seriously. The cost is high, as Jesus Christ declared:

Whosoever therefore shall confess me before men, him will I
confess also before my Father which is in heaven. But whosoever
shall deny me before men, him will I also deny before my Father
which is in heaven, Think not that I am come to send peace on
earth: 1 came nat to send peace, but a sword. For I am come to set
a man at variance against his father, and the daughter against her
mother, and the daughter in law against her mother in faw. And
a man’s foes shall be they of his own houschold. He that loveth
father or mother more than me is not worthy of me: and he that
loveth son or daughter more than me is not worthy of me. And he
that taketh not his cross, and followeth after me, is not worthy of
me. He that findeth his life shall lose it: and he that loseth his life
for my sake shall find it (Matthew 10:34-39).
