Rabbi Moshe Ben Maimon, “Maimonides” HS

for the same work, or for other work, or for anything in the
world . . . it is a case of borrowing with the owner and the
commodatary is quit. Tf, however, he borrowed the animal first,
and then the owner gave him water to drink, it is not a case of
borrowing with the owner. And so it is in all similar cases.”?° This
sort of reasoning places barriers of extreme legalism in between
neighbors. Legal technicalities can overwhelm personal relation-
ships.

Talmud vs. Torah

Maimonides’ Code docs rcpresent both the letter and spirit of
the Talmud, This is not simply my opinion. Orthodox Jews have
long believed that the Code is faithful to the Talmud. The translator
of his introduction to the Talmud, which Maimonides wrote at the
age of 23, is adamant on this point: “Although he utilized the fruits
of his time’s researches, every statement of Maimonides ix securely grounded
and borne from the Torah literature. It is extremely important to bear
this in mind. The Torah is the means by which the Rambam saw
and explained everything.”2”

Horowitz begins his detailed, readable, and nearly indispensa-
ble study of Jewish law with this assertion: “Though there are in
the laws of Moses not a few specific and literal commands which
give emphatic expression to the spirit of that lcgislation, it is the
gradual changes against the letter of Scripture which came about
in the course of centuries, that offer the most striking manifestation
of the true, the humane spirit of Jewish law.”?8 But is this really
true? Was the “humaneness” of the Jewish legal order truly in-

 

26. fbid., 11:1, p. 55.

27. Zvi L. Lampel, Maimonides’ Introduction fo the Talmud (New York: Judaica Press,
1975), p. 9.

28. Horowitz, Spirit of Jewish Law, pp. 1-2, This reflects a view quite similar to
that expressed by Lauterbach in his criticism of Sadduceeism because of its having
become “blind slaves of the law without regard for its spirit, It divorced the law from
life, in that it made the two absolutely independent of cach other.” Jacob 2, Lauter-
bach, “The Sadducees and Pharisees” (1913); reprinted in Lauterbach, Rabbinic
Essays {Cincinmati, Ohio: Hebrew Union College Press, 1951), p. 38.
