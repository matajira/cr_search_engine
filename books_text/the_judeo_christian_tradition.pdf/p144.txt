130 The Judeo-Christian Tradition

over semantics or semitics. /t is a fundamental debate over biblical
hermeneutics, and both Orthodox Judaism and orthodox Christi-
anity teach that this ultimate division cannot be overcome in
principle. It divides Christians from Jews, and has from the first
century, whether A.D. or C.E. Arthur Cohen is correct: “I suggest
in part, thercfore, that the Judeo-Christian tradition is a construct,
an artificial gloss of reason over the swarm of fedeist passion. . . .
What is omitted is the sinew and bone of actuality, for where Jews
and Christians divide, divide irreparably, divide finally . . . is
that for Jews the Messiah is to come and for Christians he has
already come. That is irreparable.”

From the day that the English-language translation of Maimo-
nides’ Cude was completed, the terms of this division came to the
surface of the academic waters, and have drifted along ever since.
That this debate has not previously broken out stems mainly from
the fact that the two sides that presumably care one way or the
othcr about the underlying religious issues and therefore the her-
meneutical questions — Orthodox Jews and orthodox Chris-
tians — have not debated publicly, primarily because the Chris-
tians have never heard of the Afishneh Torah. Very few have read.
any of the Talmud, cither. Maimonides’ Code is an unknown book
that comments on a closed book.

 

hardback collectors’ edition by Revisionist Press, 1982. It appears as volume 47 of
Luther's Works (Philadelphia: Fortress, 1971), pp. 137-306. Tauther was not alone in his
hostility to Judaism. Two years prior to the publication of his book, his arch-rival,
Catholic theologian Jol Eck, published Refitation of a Jew-Book, and two years before
this, Calvinist Martin Bucer published On the jews. Luther, however, was typically
extreme, He recommended seven steps to be taken by the civil government: 1) burn
dawn every synagogue until not a cinder remains; 2) raze the homes of all Jews; 3)
confiscate and destroy their books and the Talmud; 4) forbid rabbis to teach on the
threat of execution; 5) revoke all safe-conduct passes on the highways; 6) forbid them
to loan money at interest; and 7) require them to work at manual labor. Luther’s
Works, vol. 47, pp. 268-72. For a study of European life for Jews in. the sixteenth
century, sce Selma Stern, Jose of Rosheim (Philadelphia: Jewish Publication Society,
1965).

72, Arthur A. Cohen, The Myth of the Judeo-Christian Tradition (New York: Schocken,
1971), p. xii
