1
TWO BRANCHES, ONE BRIDE

1 say then, Hath God cast away his people? God forbid. For I
also am an Israelite, of the seed of Abraham, of the tribe of Benjamin.
God hath not cast away his people which he foreknew. Wot ye not
what the scripture saith of Elias? how he maketh intercession to God
against Israel, saying, Lord, they have killed thy prophets, and digged
down thine altars; and I am left atone, and they seek my life. But
what saith the answer of God unto him? I have reserved to myself seven
thousand men, who have not bowed the knee to the image of Baal. Even
so then al this present time also there is a remnant according to the
election of grace (Romans 11:1-5).

These words of Panl, the former Pharisee (Philippians 3:5),
warn Christians not to imagine that God has ceased dealing with
the Jews as a separate people. Paul made it clear that the eschato-
logical hope of the church of Jesus Christ is tied closely to the
conversion of the Jews as a people at some future point in history.
The Israelite “branches” that were removed by God in order to
make possible the “grafting in” of the gentiles will eventually be
re-grafted in, thereby bringing the church of Jesus Christ as close
to perfection as it will experience this sidc of the resurrection and
final judgment. Paul makes this emphatic:

For if thou wert cut out of the olive tree which is wild by
nature, and wert graffed contrary to nature into a good olive tree:
how much more shall these, which be the natural branches, be
grafied into their own olive tree? For I would not, brethren, that

5
