102 The Judeo-Christian Tradition

nantly Christian books) when he offers these three reasons why
Christian magistrates have been so hostile to the Talmud in the
past:

1, Since it forms the main teaching of the Jewish religion, it has
been regarded as the supreme obstacle in preventing Jews from
being converted to Christianity.

2. Since the Talmud interprets the Old Testament by reshaping
ancient Biblical laws to mect the needs of post-Biblical times,
it has been charged with the falsification of Scripture.

3, Since the Tafmud is a non-Christian production, it has been
accused of harboring an evil and irreverent attitude towards
Christ and the Church.*?

Would he say that teaching that Jesus Christ and His followers
will be boiled in hot semen and hot excrement for eternity consti-
tutes a reverent attitude? Are Christians supposed to believe that
this is a reverent “attitude toward Christ and the Church”?

He goes on: “For many centuries the Talmud was regarded as
mysterious and a source of blasphemous statements against Chris-
tianity. This suspicion was not only grossly untrue but it was
magnificd and distorted by ignorance of the Talmud. The inability
of Christian scholars to read the Talmud made matters worse.””
An uncensored (as far as we gentiles know} version of the Talmud
is now in English. Those few of us who bother to consult it still
do not find that these ancient suspicions have been calmed. They
have in fact been confirmed.

T do not think that Michacl Rodkinson was being any more
honest that Rabbi Trattner when he wrote thcse words in the
Preface to his expurgated version of the Talmud: “The Talmud is
free from the narrowness and bigotry with which it is usually
charged, and if phrases used out of their context, and in a sense
the very reverse from that which their author tended, are quoted
against it, we may be sure that those phrases never existed in the
original Talmud, but are the later additions of its enemies and

39. f0id., p. 198.
40, Idem.
