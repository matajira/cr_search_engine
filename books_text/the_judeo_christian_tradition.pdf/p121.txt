Rabbi Moshe Ben Maimon, “Maimonides” 107

lian philosophy into the Jewish community in Europe, as well as
into the Christian community.

Maimonides was a medieval man. He did not escape the
worldview of his era, Consider his views on slavery. He insisted
that slaves should not be taught the Bible. “It is forbidden for a
man to teach his slave the Scriptures. If he does teach him,
however, the slave does not become free thereby.” He correctly
recognized that the slave might conclude that if he converted to
biblical faith, the owner could no longer morally keep him in
permanent bondage. This same fear occurred to Christian com-
mentators, too. The post-Temple biblical requirement of the aboli-
tion of all forms of permanent slavery was an idea long ignored or
unrecognized by all Bible commentators.*

The standard view of Bible commentators from the fall of
Jerusalem until the carly nineteenth century was that slavery is
biblically justified because of the curse placed on Canaan by Noah,
and this curse was essentially racial in nature, the so-called “curse
of Ham.” There had indeed been a curse: Noah did curse Canaan,
the son of Ham, but this curse was covenantal, not racial, and it
was generally fulfilled by the conquest of the land of Canaan by
the Israelites, and the subjection of the remnant as slaves. Winthrop
Jordan has identified the source of the idea of “Ham’s curse” as
black skin: the idea first appeared in the Talmud and the Midrash.>
The myth that the “curse of the children of Ham” refers exclu-
sively to blacks was universally adopted by Jews, Christians, and

3. Moses Maimonides, The Book of Acquisition, Book 12 of The Code of Maimonides
(New Haven, Connecticut: Yale University Press, [1180] 1951), “Laws Concerning
Slaves,” Chapter VIII, Section 18, p. 278. Hereafter, I identify chapters by Roman
numerals and sections by Arabic numerals.

4. Gary North, Toals of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1990), pp. 138-47.

5, Winthrop D. Jordan, White Over Black: American Attitudes Toward the Negro,
1550-1812 (Chapel Hil: University of North Carolina Press, 1968), p. 18. He cites the
Babylonian Talmud (Soncino Press edition), tractate Sanhedrin, vol. II, p. 745; Midrash
Rabbah (Soncino Press edition), vol. I, p. 293. Reprinted by Bloch Pub. Co., New
York.
