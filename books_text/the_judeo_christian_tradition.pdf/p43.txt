Political Idolatry 29

 

philosophy of the intellectuals who pioneered the Renaissance?
‘The answer, so well described by the nineteenth-century scholar
Jacob Burckhardt, is simple: classical paganism. He wrote (from the
perspective of his subjects, not his own views): “But culture, as
soon as it freed itself from the fantastic bonds of the Middle Ages,
could not at once and without help find its way to the understand-
ing of the physical and intellectual world. It necded a guide, and
found one in the ancient civilization, with its wealth of ruth and
knowledge in every spiritual interest. Both the form and the sub-
stance of this civilization were adopted with admiring gratitude;
it became the chief part of the culture of the age.”

Heller admits as much with respect to the field of politics,

 

which is always the “queen of the sciences” for humanist civiliza-
tions: “It was political writing which was most consistently con-
cerned with antiquity; here the prestige of Plutarch and Ciccro
was unshakable.”!§ Had she been better informed or more honest,
she would also have mentioned Renaissance magic, which was also
a self-conscious revival of ancient paganism, at least among gen-
tiles. ‘Th 's who wanted to dabble in magic had the tradi-
tional Kabbalah, which is why gentile Renaissance pagans became

 

18. Jacob Burckhardt, The Civilization of the Renaissance in Haly, 2 vals, (New York:
Harper Colophon, [1860] 1958), IT, p. 182.

19. Heller, Renaissance Man, p. 59,

20, Frances A. Yates, Giordano Bruno and the Hermetic Tradition (New York: Vintage,
[1964] 1969). I do not capitalize “gentile,” although the King James translators did,
and it is still common for writers to do so, T do not view the gentiles as a separate
people in the ethnic or national way that Americans, Mexicans, Chinese, and Jews
are. To capitalize the word would imply that gentiles are a separale people, meaning
a separate people as contrasted to Jews, who alone are “not gentiles.” Such ethnic
separation no longer exists in principle: “That at that time ye were without Christ,
being aliens from the commonwealth of Israel, and strangers trom the covenants of
promise, having na hope, and without God in the world: But now in Christ Jesus ve
who sometimes were far off'are made nigh by the blood of Christ. For he is our peace,
whe hath made both one, and hath broken down the middle wall of partition between.
us” (Ephesians 2:12-13), Jews equate gentiles with heathen, yet they do not capitalize
“heathen,” for they correctly understand “heathenism” as a spiritual condition rather
than an ethnic or national condition. I use “gentiles” in the sense of “not Jews,” but
not in the sense af a separate ethnic or national group.

 

 

 
