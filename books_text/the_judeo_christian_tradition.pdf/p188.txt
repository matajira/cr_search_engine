174 The Judeo-Christian Tradition

by him, and for him: And he is before all things, and by him all
things consist. And he is the head of the bedy, the church: who is
the beginning, the firstborn from the dead; that in all things he
might have the preeminence. For it pleased the Father that in him
should all fulness dwell; And, having made peace through the
blood of his cross, by him to reconcile all things unto himself, by
him, I say, whether they be things in earth, or things in heaven.
And you, that were sometime alienated and cnemies in your mind
by wicked works, yet now hath he reconciled In the body of his
flesh through death, to present you holy and unblameable and
unreproveable in his sight (Colossians 1:15-23).

The Messiah has come. Do not pray for His return if you deny
that He has come. When He returns again, it will be to enforce the
cternal sanctions of His covenant. ‘here will be no escape then,
“For there is no respect of persons with God. For as many as have
sinned without law shall also perish without law: and as many as
have sinned in the law shall be judged by the law; (For not the
hearers of the law are just before God, but the doers of the law
shall be justified” (Romans 2:11-13), The law is comprehensive;
you are allowed not one mistake. “For whosoever shall keep the
whole law, and yet offend in one point, he is guilty of all” (James
2:10). But there is hope: “For the wages of sin is death; but the gift
of God is eternal life through Jesus Christ our Lord” (Romans
6:23). And again: “But God commendeth his love toward us, in
that, while we were yet sinners, Christ died for us. Much more
then, being now justified by his blood, we shall be saved from
wrath through him. For if, when we were enemies, we were
reconciled to God by the death of his Son, much more, being
reconciled, we shall be saved by his life” (Romans 5:8-10).

Covenant: Make One, Break One

This is God’s promise of covenantal hope to both Jew and
gentile, You can appropriate this promise today. “For he saith, I
have heard thee in a time accepted, and in the day of salvation
have I succoured thee: behold, now is the accepted time; behold,
now is the day of salvation” (II Corinthians 6:2). We can restore
