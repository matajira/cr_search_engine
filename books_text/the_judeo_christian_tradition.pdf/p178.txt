164 The Judeo-Christian Tradition

Talmud rather than dealing carefully with the actual biblical texts.
To prove that such expositions of the text can and have been be
done by Jews, I recommend the efforts of U. Cassuto and Samson
R. Hirsch. If, however, Christians spend their lives in headlong
antinomian flight from the law of God, vainly misquoting Paul —
“We're under grace, not law”® — and Jews run to the Talmud every
time they feel the pressure of Exodus 12:49 ~ “One law shall be to
him that is homeborn, and unto the stranger that sojourneth among
you” — then the Judeo-Christian tradition will remain a myth.
Honesty is truly the best policy, for the sake of the peace. We
must search out the Pentateuch to discover the God-given, Gud-
required standards of honesty. Honesty produces its appropriate
rewards, irrespective of race, color, or creed. There is a cause-and-
effect relationship between honesty and success. ‘his is an aspect
of God’s common-grace order — an order shared by all people.
Jesus said: “But I say unto you, Love your enemies, bless them
that curse you, do good to them that hate you, and pray for them
which despitefully use you, and persecute you; That ye may be the
children of your Father which is in heaven: for he maketh his sun
to rise on the evil and on the good, and sendeth rain on the just
and on the unjust” (Matthew 5:44-45). Deny this in word and
deed, and you subject society to never-ending internal conflicts,
Judaism and Christianity use rival principles of biblical inter-
pretation, but there is nevertheless a common-grace order which
does lead men in the direction of God’s truth, assuming they do
not actively resist His testimony. While there can never be a fusing
of these two religions, they can live in social peace if their adher-
ents choose to. For the sake of the peace, they can hammer out a
cultural cease-fire based on shared judicial standards of the Penta-
teuch. But if they abandon God’s law, for whatever seemingly
convenient reason, they will either be continually at each other’s
throats, or else they will have the humanists’ boots on their throats.

6. For a positive antidote to such antinomian thinking, written by a dispensa-
tional fundamentalist, see John MacArthur, Jr., The Gospel According to Jesus (Grand
Rapids, Michigan: Zondervan Academie, 1988).
