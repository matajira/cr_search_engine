7
“YOU HAVE HEARD IT SAID”

Whosoever therefore shall break one of these least commandments,
and shall teach men so, he shall be called the least in the kingdom of
heaven: but whosoever shall do and teach them, the same shall be called
great in the kingdom of heaven. For I say unto you, That except your
righteousness shall exceed the righteousness of the scribes and Pharisees,
we shall in no case enter into the kingdom of heaven (Matthew 5:19-20).

Orthodox Judaism is not simply “Old Testament theology
without Jesus.” It is the religion of “You have heard it said.” This
was Jesus’ repeated response to the erroneous oral teachings of the
Pharisees. We can use the same technique today as we examine
the Talmud.

What you are about to read on the next few pages will forever
change your opinion about Orthodox Judaism, unless you are an
Orthodox Jew. I want you to read the footnotes, although I cannot
imagine that you might skip them. What is revealed here is today
publicly available information. For over 1,400 years —in the final
version, from 500 A.D. until 1952 — this information was jealously
guarded by the rabbis; no unconverted gentile was allowed to gain
access to it. When you read just a few picces of it, you will
understand why. But since 1952, the complete Talmud has been
available in an unexpurgated English version.! It sits on university

1. That it had formerly been expurgated is revealed by the editor's admission:
“All the censored passages reappear in the Text or in the Notes.” J. H. Hertz,

84
