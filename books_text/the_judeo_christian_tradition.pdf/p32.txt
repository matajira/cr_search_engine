18 The Judeo-Christian Tradition

century.2! While there may be only 250,000 Hasidic Jews alive
today, with 200,000 living in the U.S., and half of these in Brooklyn,”
prior to World War TI, they were the dominant force in European
Judaism.”

‘This book is also designed to promote the conversion of Jews
to soul-saving faith in Jesus Christ. This presentation of the gospel
cannot be effective if the Christian in any way de-emphasizes the
total break with Judaism that Christian faith requires. I am in
complete agreement with Robert L. Reymond when he writes:

A righteousness bore out of good works and the keeping of the law
is futile (Galatians 2:16). Even the highest and best of Jewish
extra-Biblical tradition only makes void the true Word of God
(Mark 7:13). Paul was convinced that by their rejection of Jesus
the Christ, “his kinsmen according to the flesh” had called down
upon themselves the wrath of God eis telos (I Thessalonians 2:14-
16)2# And he was equally convinced that the Jew must give up
that very distinctive which separates him from other men, namely,
his exalted idea of his own acceptability before God because of his
racial relation to the Patriarchs and his obedience tv the Torah, if

 

he is ever to know genuine conversion to God through repentance
and faith in Jesus Christ. Tt is indeed a strange twist of thinking, if
not outright treason, for the Christian man in any way to aid or tu
abet the Jew in his retention of that distinctive, the holding on to
which only solidifies him in his unbelief. And yet, in order that the
blessing of Genesis 12:3 might be his, and in order that he might

21. Elijah Judah Schochet, Animals in Javish Tradition: Attitudes and Relationships
(New York: Kiav, 1984), p. 237. He discusses the development of the idea of
reincarnation in Hasidic literature: pp. 251-54.

22. Harris, Holy Days, pp. 11-12.

23, Fdward Norden, “Behind ‘Who Is a Jew’: A Letter from Jerusalem,” Commen-
tary (April 1989), p. 22. There were perhaps a million and a quarter of them alive in
1900: Harris, Holy Days, p. 12.

24. “For ye, brethren, became followers of the churches of God which in Judaca
are in Christ Jesus: for ye also have suffered like things of your own countrymen, even
as they have of the Jews: Who both killed the Lord Jesus, and their own prophets,
and have persecuted us; and they please not God, and are contrary to all men:
Forbidding us to speak to the Gentiles that they might be saved, to fill up their sins
alway: for the wrath is come upon them to the uttermost” (I Thessalonians 2:14-16)
