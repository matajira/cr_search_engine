82 The Judeo-Christian Tradition

A good example of this Talmudic dialccticism is the debate
over whether gentiles should be allowed to read the ‘Torah {the
five books of Moses). Consider the saying of Rabbi Johanan, on
which Maimonides’ assertion cited at the beginning of this appen~
dix is based: “R. [Rabbi] Johanan said: A heathen who studies the
Torah descrves death, for it is written, Moses commanded us a law for
an inheritance; it is our inheritance, not theirs.” Johanan was one of
the most prestigious af the rabbis, a disciple of Hillel (late first
century B.C.).3? Yet in the same paragraph is recorded the saying
of Rabbi Meir, an cqually prestigious authority, both jurist and
preacher, from the second century A.D.: . even a heathen
who studies the Torah is as a High Priest!” So, which is it?
Maimonides sided with Johanan, but he could as easily have sided
with Mcir, This is the main problem in assessing the ethical
pronouncements of the Talmud. There is seldom any effective
resolution of conflicting viewpoints. This is the characteristic fea-
ture of the Talmud: a mountain of brief, sometimes outlandish
statements, without any coherent resolution. Paul, a former Phari-
see (Philippians 3:5), warned Titus regarding such speculation:
“But avoid foolish questions, and genealogies, and contentions,
and strivings about the law; for they are unprofitable and vain”
(Titus 3:9). Thirty-four fat volumes of this material is wearying
to the soul.

The rabbis were often incredibly obscure, in stark contrast to
the clear statements of the biblical texts. This was a major point
of conflict between Sadducees and Pharisees before the destruction
of Jerusalem: the Sadducees believed that the texts of the ‘forah
are clear.“ Writes Lauterbach of the Sadducees: “They would not
devise ingenious methods to explain away a written law or give it
a new meaning not warranted by the plain sense of the words.”*

 

 

 

that a is 6 and that a is not 4, he cannot be said to declare anything.” Leo Strauss,
“How to Read The Guide of the Perplexed,” in Moses Maimonides, The Guide of the
Perplexed, 2 vols., trans, Shlomo Pines (University of Chicago Press, 1963), I, p. xv.

32. Sanhedrin 59a.

33. Lauterbach, “Sadducees and Pharisees,” Rabbinical Misays, p. 31.

34. Ibid., p. 32. The Sadducees were not “proto-Christians,” however. They did

 
