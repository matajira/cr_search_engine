Rabbi Moshe Ben Maimon, “Maimonides” 129

he was, and there have been few traditional Jewish detractors of
Maimonides who have been visible to gentiles, from his day to the
present.

T interacted repeatedly with Maimonides’ Code in the footnotes
of the text of my economic commentary on the case laws of
Exodus, Tools of Dominion. Sometimes he got things correctly, and
many times he did not. It was my task in this chapter to deal with
the ways that he got things wrong rather than right, as well as the
reasons why. I suppose I would have a much more difficult task
in writing a chapter analyzing Rabbi S. R. Hirsch’s commentary
on Exodus. I find so often that he got things right.” How was this
possible, when he, like most Orthodox Jews of his day and earlier,
must have relied heavily on Maimonides — not Maimonides the
Aristotelian philosopher, who was regarded with suspicion by
Jewish scholars from the beginning, but Maimonides the ‘Ial-
mudist?

So, T find that I am er
and judicial opinions, and through him, of the Talmud. But how
does a gentile scholar say this politely yet effectively, and also
avoid the counter-charge of anti-Semitism? I suppose he does this
in the same way that a Jewish scholar would discuss Martin
Luther's notoriously anti-Semitic book on the Jews,’! yet remain
free of “anti-gentilism.” All [ can say is this: what we have here is

 

‘al of many of Maimonides’ economic

more than a failure to communicate. It is more than a difference

70. Again and again as I wrote the commentary, I found myself turning to Hirsch
and citing him. James Jordan has been working on his study of the dietary laws
during the period that have been working on the casc laws. He also has noticed this
phenomenon: Hirsch frequently makes sense, while the observations in Maimonides?
Code often seem archaic, superstitious, and irrational. Hirsch sticks to the biblical text
far more closely than Maimonides does, Yet he also cites the Talmud, and the
conclusions he draws from these citations seem sensible, whereas Maimonides, if he
is in fact being faithful to the Talmud (and I find that he seems to be faithful in the
cases that I have studied), frequently makes the Talmud seem unreliable. I leave it
to Orthodox Jewish scholars to sort out the discrepancies between these two giants
of Jewish thought. T have run out of time to pursue the matter.

71. On the Jews and Their Lies (1543), published over the years in cheap, poorly
printed paperback cditions for the anti-Semitic masses, as well as in an expensive
