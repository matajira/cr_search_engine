8 The Judeo-Christian Tradition

Murray has called the Puritan hope.®

Jews may ask themselves about Christians: “Why do they
single us out for their cvangclism?” The answer is — or should
be — “Because when you people as @ people at last come to faith in
Jesus Christ as your promised Messiah and dcliverer, it will culmi-
nate in the transformation of this world, bringing in the fullness
of the bicssings of the kingdom of God in history.” Paul writes:
“Now if the fall of them be the riches of the world, and the
diminishing of them the riches of the Gentiles; how much more
their fulness?” (Romans 11:12). God never said this about any
other covenanted ethnic group. I1 is the stated goal of orthodox
Christianity to preach the gospel of salvation in Christ to the Jews,
until not a trace of the traditional practices of Judaism remains,

 

Final Solutions

Non-Christian movements (e.g. the Nazis), as well as mis-
guided Christian movements (c.g, Russian Orthodoxy), ba
the past also sought the eradication of Judaism in history. ‘They
have attempted to destroy the religion by destroying ils adherents.

 

      

They have used terrorism, murder, and expropriation against
Jews. This has becn a dark blot on Western history. There is no
biblical warrant for such persecution, The Jews have scldom if
ever actively sought gentile prosclytes. They have conducted them-
selves as “strangers in the gale,” lo use an analogy based on the

 

Old Testament. They have sought peace, which is an appropriate
social goal for a self-conscious minority group that mtends ta avoid
assimilation by ihe surrounding culture.

Nevertheless, Paul says clearly that God’s goal in history is the
final assimilation of the Jews into the Church, This is the only “final
solution” to Judaism spccified in the New Testament: the soul-
saving movement of the Holy Spirit which will work in a unique
way in the hearts of an entire people. “For if the casting away of
them be the reconciling of the world, what shall the receiving of

6. Tain H. Murray, The Puritan Hope: A Study in Revival and the Interpretation of
Prophecy (London: Banner of Truth Trust, 1971}, pp. 41-53, 39-76, 91-92, 98-99.
