160 The Judeo-Christian Tradition

tion would backfire. If it was not of God, persecution would be
umnecessary. Besides, what good does it do to create martyrs (the
Greck word for witnesses}?

For the Sake of the Peace

I have done my best to honor Orthodox Judaism. When
Orthodox Jews tell me that they honor the Talmud, I accept this
statement as true. I do not attempt to argue that they really don’t
acccpt it as true, that they really and truly take it only metaphori-
cally, that “no rational person could believe such things in today’s
world.” In short, 1 do not treat them as theological liberals treat
me and those like me. If a man says that he believes something,
and if he is a member of a group that has repeatedly been perse-
cuted for adhering to certain ideas, then I assume that he is telling
me the truth. He really does believe what he says he belicves.

What the Orthodox Jews says that he believes is the Talmud.
He also says that he believes in the Torah, what I call the Old
‘Testament. I think that the ‘Talmud is unfaithful to the Old
‘Testament. The Orthodox Jew—or any Jew, for that mat-
ter—thinks that the New Testament is unfaithful to the Old
Testament. What we have here is not a failure to communicate.
This is not a debate over semantics. This is a debate over biblical
hermeneutics, as formidable a disagreement as men can have in
life, for its consequences extend to eternity.

Orthodox Jews and orthodox Christians disagree about many
things, especially the theological integrity of their respective sys-
tems. The Talmud has some graphic things to say about Jesus and
His followers. The New Testament has some graphic things to say
about the Jews of that day: whited sepulchers, blind guides, gnat-
strainers, hypocrites, thicves, and dogs. Paul wrote: “Beware of
dogs, beware of evil workers, beware of the concision” (Philippians
3:2). The dog in those days was not a domesticated beast or
“man’s best friend.” Dogs roamed in packs and devoured the
weak.

What good does it do to cover this up? None. What good docs
