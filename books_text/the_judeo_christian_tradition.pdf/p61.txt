4

INTERPRETATION
AND REPRESENTATION

And when they [the Jews] had appointed him [Paul] a day, there
came many to him into his lodging; to whom he expounded and testified
the kingdom of God, persuading them concerning Jesus, both out of the
law of Moses, and out of the prophets, from morning till evening. And
some believed the things which were spoken, and some believed not. And
when they agreed not among themselves, they departed, after that Paul
had spoken one word, Well spake the Holy Ghost by Esaias the prophet
unto our fathers, Saying, Go unto this people, and say, Hearing ye
shall hear, and shall not understand; and seeing ye shall see, and not
perceive: For the heart of this people is waxed gross, and their ears are
dull of hearing, and their eyes have they closed; lest they should see with
their eyes, and hear with their ears, and understand with their heart,
and should be converted, and I should heal them. Be it known therefore
unto you, that the salvation of God is sent unto the Gentiles, and that
they will hear it (Acts 28:23-28).

Who speaks for God in history? This is the question, above all
other questions, that divides religions.

Who speaks for the state in history? This is the question, above
all other questions, that divides political systems.

They are in fact the same question, but in two different forms.
It is the question of ultimate sovereignty: Who is God? But more
to the point for practical purposes, it is the question of the locus

of earthly sovereignty: Who most faithfully represents this ultimate

47
