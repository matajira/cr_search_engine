On Evangelizing Jews 183
fellowship and glory.

WE DENY THAT those who think of Jesus Christ in lesser terms
than these have faith in him in any adequate sense.

ARTICLE 1.4.

‘WE AFFIRM THAT all human beings are sinful by nature and
practice, and stand condemned, helpicss and hopeless, before God,
until the grace of Christ touches thcir lives and brings them to
God's pardon and peacc.

WE DENY THAT any Jew or Gentile finds true peace with God
through performing works of law.

ARTICLE 15.

WE AFFIRM THAT God’s forgiveness of the penitent rests on
the satisfaction rendered to his justice by the substitutionary sacri-
fice of Jesus Christ on the cross.

WE DENY THAT any person can enjoy God’s favor apart from
the mediation of Jesus Christ, the sin-bearer.

ARTICLE 1.6.
WE AFFIRM THAT those who turn to Jesus Christ find him to
be a sufficient Saviour and Deliverer from all the evil of sin: from
its guilt, shame, power, and perversity; from blind defiance of
God, debasement of moral character, and the dchumanizing and
destructive sclf-assertion that sin breeds.

WE DENY THAT the salvation found in Christ may be supple-
mented in any way.

ARTICLE 1.7.
WE AFFIRM THAT faith in Jesus Christ is humanity’s only way

to come to know the Greator as Father, according to Christ’s own
Word: “I am the Way and the Truth and the Life; no one comes
to the Father except through me” (John 14:6).
