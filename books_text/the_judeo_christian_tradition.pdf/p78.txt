64 The Judeo-Christian Tradition

It is not just Christians who are ignorant of Jewish history,
however. American historians generally have ignored the Jews and
know little about their history. The entry under “Jews” in the
one-volume index to the other 12 volumes of the Cambridge Modern
History is miniscule: just a handful of references. The same is true
of the standard textbooks in European history. Except for Hitler’s
Germany in the 1930’s, there are very few references. ‘This is not
because modern historians are involved in a self-conscious conspir-
acy to black out all references to Jews. It is in part because Jews
were physically isolated for so long in European history that they
had no impact on events, except as occasional victims of oppres-
sion. Also, because they existed almost exclusively as a religious
people, Jews are assumed by modern historians to have been
incapable of effecting events positively until they began te be
assimilated into Western humanist culture. There scems to be no
Treason to study them prior to the 1880’s, when they arrived in
America and started working in the cities, especially New York
City, and even more important, started voting. (For the humanist,
voting is the closest equivalent he knows to taking Holy Gommun-
ion.)!” This, however, raises the question: Where is the documen-
tary evidence of a Judeo-Christian tradition?

To the extent that the Bible-believing Christian thinks about
Reform Jews generally, he assumes that they are something like
Unitarians; politically liberal, skeptical about the Bible, and essen-
tially humanistic. (Orthodox Jews also view Reform Jews in much
the same way.) Christians, however, tend to think of almost all
Jews in this way, which turns out to be a statistically correct
political assumption; American Jews are consistently liberal in

 

 

York; Barnes & Noble, 1974); Rabbi Elmer Berger, The Jewish Dilemma: The Case
‘Against Zionist Nationalism (New York: Devin-Adair, 1945). The major published
English-speaking critic of Zionism is Alfted M. Lilienthal: What Price Israel? (Chicago:
Regnery, 1953); There Goes the Middle Fast (New York: Devin-Adair, 1957); The Zionist
Connection: What Price Peace? (New York: Dodd, Mead, 1978)

17. I am only half joking. Political philosopher Sheldin Wolin speaks of political
participation in Language that borders on the religious: Politics and Vision: Continuity
and Iunovation in Westem Political Thought (Boston: Little, Brown, 1960).
