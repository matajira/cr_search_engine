Two Branches, One Bride I

The Bible compares adultery and whoredom with apostasy.
This is a universal theme in the Bible, but it is seen most clearly
in the Book of Hosea. Hosea the prophet was told by God to marry
a prostitute, who was the symbol of rebellious Israel. Again and
again, Israel committed spiritual whoredom with gentile gods and
gentile nations. Again and again, Ged “divorced” Israel by deliv-
ering the nation into the hands of foreign conquerors. He even led.
Judah into captivity under Babylon and then Medo-Persia. Each
time, He would remarry Israel out of grace. But still Israel would
depart again from the terms of God’s covenant.

Jesus came as the final prophet, the very Son of God, to call
Israel to final repentance. He delivered God’s fina! covenant law-
suit against Israel, a final threat of divorcee, The Jows as a cove-
nanted nation refused to listen. Instead, they killed Jesus — divorce
through execution. They submitted their own final divorce papers
to God by killing the Bridegroom. To their shock and horror, the
Bridegroom returned from the dead to issue the final divorce
decree to Israel.

Jesus issued this divorce decree in private, after His resurrec-
tion, but prior to His ascension. He did this by announcing the
annulment of the covenantal requirement of the mark of circumci-
sion, and the substitution of a new mark, baptism. “Then the
eleven disciples went away into Galilee, into a mountain where
Jesus had appointed them, And when they saw him, they wor-
shipped him: but some doubted. And Jesus came and spake unto
them, saying, All power is given unto me in heaven and in earth.
Go yc therefore, and teach all nations, baptizing them in the name
of the Father, and of the Son, and of the Holy Ghost: Teaching
them to observe all things whatsoever I have commanded you:
and, lo, I am with you alway, even unto the end of the world.
Amen” (Matthew 28:16-20). From that point on, Israel could not
return as a nation to be God's Bride.

Judgment Announced Publicly
Next, God divorced Isracl publicly at the day of Pentecost,
