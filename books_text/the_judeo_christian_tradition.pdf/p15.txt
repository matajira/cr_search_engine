INTRODUCTION

Here is another book on the Judeo-Christian tradition. “So
what?” you may ask. An accurate answer to this initially skeptical
question can only be obtained by reading the book, or at Icast
reading a review of it, I do not expect many reviews. (After two
dozen books, I am beginning to get the picture.)

This book presents a covenantal evaluation. Why covenantal?
Why not a historical evaluation, or theological, or even racial (if
this book were hot off some backyard anti-Semitic press)? Why
covenantal? What has covenant got to do with anything?

My answer is straightforward: everything!

To get the right answers in life, we need first to ask the right
questions. For a long, long time, Christians and Jews have had the
right questions right under their noses, but no one paid any
attention. The questions concerning lawful government are organ-
ized in the Bible around a single theme: the covenant.

Most Christians and Jews have heard the word “covenant.”
‘They regard themselves (and occasionally even each other) as
covenant people. They are taught from their youth about God’s
covenant with Israel, and how this covenant extends (or doesn’t)
to the Christian Church. After all, Paul called Christians “the
Israel of God” (Galatians 6:16). Everyone talks about the cove-
nant, but until late 1985, nobody did very much about it.

Is this too strong a statement? Maybe, but I have a weakness
for strong statements. They catch people’s attention. Furthermore,
sometimes they are accurate. The fact remains, if you go to a
Christian or a Jew and ask him to outline the basic features of the

1
