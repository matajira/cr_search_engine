Political Idolatry 25

the United States? Mead is quite open about it: the public school
system. “If we ask, Why the rise of compulsory free public educa-
tion? must it not be said that prominent among the reasons was a
desire to make possible and to guarantee the dissemination and
inculcation among the embryo citizens of the beliefs essential to
the existence and well-being of the democratic society? And who
can deny that these beliefs are religious? . . . Here are the roots
of the dilemma posed by the acceptance of the practice of separa-
tion of church and state on the one hand, and the general accep-
tance of compulsory public education sponsored by the state on
the other.”!? He then says what should be obvious to any serious
Protestant, Catholic, or Jew: “In this sense the public-school sys-
tem of the United States is its established church.”

To put it bhintly, as is my habit: the Judeo-Christian tradition is
a myth that is “of the humanists, by the humanists, and for the humanists.”
Tt will be dropped by the humanisis as a legitimizing ideology
whenever Christians and Jews decide to seek political and judicial
power in terms of their respective ethical systems. Tt will then serve
no purpose for any of the three groups: Christians, Jews, and
humanists.

Let us not be naive about the obvious preliminary step in the
process of demythologizing the myth of the Judeo-Christian tradi-
tion: the creation of privately fundcd, religiously oriented schools.
The worse the public schools get — and they will get much, much
worse — the more likely parents will be willing to pull their chil-
dren out. To do this is unquestionably an act of religious disestab-
lishment. This is also the first step in the creation of a new voting
bloc that will oppose the further use of taxpayers’ funds to finance
the humanists’ number-one institution, In short, the war for pri-
vate education is a war against the Judeo-Christian tradition. Few
Christians realize this today, but it will become increasingly appar-

 

ent to them over time.
‘That the Judeo-Christian tradition is mythical dues not mean

12, Mead, izely Experiment, p. 67.
13. Dbid., p. 68.
