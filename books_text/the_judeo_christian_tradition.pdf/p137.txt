Rabbi Moshe Ben Maimon, “Maimonides” 123

required to pay damages, whether or not he was forewarned about
his beast, if his ox gores an ox belonging to a heathen. He added
reasons for the Mishnah’s discriminatory law. The “heathen do
not hold one responsible for damage caused by one’s animals, and
their own law is applied to them.” This is truly preposterous, and
he offers no evidence. On the other hand, the heathen is fully
liable, whether or not he was forewarned, if his ox gores the ox of
an Israelite. Why? Because “should they not be held liable for
damage caused by their animals, they would not take care of them
and thus would inflict loss on other people’s property.”>! ‘This is
a classic example of different laws for different residents, in open
violation of Exodus 12:49: “Onc law shall be to him that is
homeborn, and unto the stranger that sojourneth among you.”

Maimonides argued that if the ox was unowned at the time of
the goring, and is subsequently appropriated by somcone else,
before the plaintiff can seize it, the new owner is not liable for
previous damages.5? This would leave the victim without recourse,
and it would leave the animal immune from judgment, for it would
not serve as payment —ox for ox—for the damages it caused.
(Rabbi Judah had carly argued that “A wild ox, or an ox belonging
to the Temple, or an ox belonging to a proselyte who dicd arc
exempt from death, since they have no owner.”)*3

Even morc incredibly, Maimonides argued that if the existing
owner renounces ownership after the goring takes place, but before
the trial, “he is exempt, for there is no liability unless the ox has
an owner both at the time it causes the damage and at the time
the case is tried in court.”** This would destroy personal legal
liability in the most serious cases. The owner would be allowed to
separate himself retroactively from the social responsibilities of

51. Maimonides, Torts, “Laws Concerning Damage by Chattels,” VII:5, p. 29.

52. Idem.

58. Baba Kamma 1V:7, Mishnak, p. 337. The Talmud also specifies that the ox had
to have gored on three previous occasions for the owner to become personally liable:
Shalom Albeck, “TORTS, The Principal Categories of Torts,” in The Principles of
Jewish Lae, edited by Menachem Elon (Jerusalem: Keter, 19752), col. 322.

54. Maimonides, Torts, “Laws Concerning Damage by Ghattels,” VIIL4, p. 29.
