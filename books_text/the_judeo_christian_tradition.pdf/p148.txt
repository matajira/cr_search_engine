134 The Judeo-Christian Tradition

the time of the trial? If he has profited from the transaction,
Maimonides said, he must pay restitution based on the stolen
object’s value at the time of the trial. But what if the thief acciden-
tally lost the animal or accidentally broke the vessel? Maimonides
stated, though without presenting any justifying argument, that
the negligent thief owes restitution only on the value of the object
at the time of the theft.”

Such a Icgal principle would undermine biblical civil justice.
First, how is the court to determine whether the loss was acciden-
tal? The thief obviously has a financial incentive to lie, since the
burden of his repayment will be lighter. Second, what of the
victim’s added economic loss? Who protects the victim's interests?
Why should his loss as a result of the time delay between the theft
and the trial not be fully compensated by the thief, irrespective of
the latter’s quality of stewardship over the stolen goods? What
Maimonides should have concluded was tbat the thief must pro-
vide multiple restitution a victim based on the replacement cost at the
lime of his conviction for the crime. If the animal were still alive, he
would be required to return that animal, and the animal would
obviously be worth today’s market value. Thus, the replacement
value for a slaughtered animal is also to be worth today’s market
value, and so is the equivalent proportional restitution payment.
This is obvious, this is fair, and Maimonides ignored it. He de-
parted from both the letter of biblical law and its spirit.

He concluded all this by stating that two-fold restitution is not
required from any thief who is convicted of stealing bonds, land,
or slaves, “because Scripture has imposed the liability for double
payment only on movable things that have an intrinsic value, for
it says, On an ox or an ass or @ sheep or a garment (Exod. 22:8).” But
aren’t slaves movable? Physically, yes, but not legally, he said.
“Now slaves are legally regarded the same as land, for Scripture

2.“ . . if, however, the animal dies or the vessel is lost, he need pay only double
its value at the time of the theft.” Moses Maimonides, The Book of Torts, Book U1 of
The Code of Maimonides (New Haven, Connecticut: Yale University Press, 1954),
“Laws Concerning Theft,” [14, p. 63.
