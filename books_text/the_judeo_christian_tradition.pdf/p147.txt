9
UNDERMINING JUSTICE

Woe unto you, scribes and Pharisees, hypocrites! for ye pay tithe
of mint and anise and cummin, and have omitted the weightier matters
of the law, judgment, mercy, and faith: these ought ye to have done,
and not to leave the other undone. Ye blind guides, which strain at a
gnat, and swallow a camel. Woe unto you, scribes and Pharisees,
hypocrites! for ye make clean the outside of the cup and of the platter,
but within they are full of extortion and excess (Matthew 23:23-25).

George Horowitz asserts that the spirit of Jewish law has been
humane because the rabbis have departed from the letter of Mo-
saic law.’ (Implicitly or explicitly, this is the same defense offered
by Christian theologians when they also depart from the letter of
the Mosaic law without specific New Testament authorizations.)
One problem with Horowitz’s argument is that Maimonides’ in-
terpretations are frequently opposed to the spirit of biblical justice
precisely because he ignored the letter of biblical law.

For example, Maimonides discussed the case of a thief who
stole an animal or a vessel, and who then immediately slaughtered
the animal or deliberately broke the vessel ~ perhaps to conceal
the evidence of the crime? — and later is convicted of the theft.
What if, in the meantime, the market value of the stolen object has
doubled? Does the thief pay double restitution based on the value
of the item at the time of the theft or based on its market value at

1. George Horowitz, The Spirit of Jauisk Law (New York: Central Book Co.,
[1953} 1963), pp. 1-2.

133
