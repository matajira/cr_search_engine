8

RABBI MOSHE
BEN MAIMON, “MAIMONIDES”

The Mishnch Torah also becomes an Archimedean fulcrum in
the sense that he [Maimonides] regularly mentions it and refers corre-
Sspondents and inquirers to it. The repeated references convey the impres-
sion thal he wanted to establish it as a standard manual, a ready,
steady, and uniform reference book for practically all issues.

Isadore Twersky (1980)!

Few gentile scholars have ever heard of the Mishneh Torah, but
all medieval historians and spccialists in the history of Western
philosophy know of Maimonides. Moshe, the son of Maimon,
better known as Maimonides {1134-1204), is by far the most
famous Jew in medieval history. He was the Rambam (Rabbi
Moshe ben Maimon: RMBM). He lived in Spain and later in
Cairo, where he served as the Sultan’s physician. He became
world-famous as a physician. Copies of at least ten of his medical
treatises still survive.? He is best known for his theological-
philosophical treatise, The Guide of the Perplexed (a better translation
than “guide “o the perplexed”), completed in 1190. His native
tonguc was Arabic. He was familiar with the Arabic translations
of Aristotle, and he became a major conduit of the flow of Aristote-

1, Isadore Twersky, Introduction to the Code of Maimonides (Mishneh Torah) (New
Haven, Connecticut: Yale University Press, 1980), p. 18.

2. Paul Johason, A History of the Jaus (New York: Flarper & Row, 1987), p. 186.
106
