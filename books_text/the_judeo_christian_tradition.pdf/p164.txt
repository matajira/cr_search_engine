150 The Judeo-Christian Tradition

theless, the impact of pagan philosophy in Judaism was less direct
in the Middle Ages, probably due to the isolation of Jews from the
surrounding gentile Christian culture, It is not surprising that the
path of Greek philosophy into late medieval Judaism, and then
into Christianity, was by way of Islam, especially through Maimo-
nides. Aristotle came to Paris through Cairo and Spain.

For centuries, Talmudic Judaism resisted the rational catego-
ries of pagan wisdom, despite The Guide of the Perplexed. But with
S. R. Hirsch in the mid-nineteenth century, the epistemological
barriers began to break down.'® This process of cultural and
intellectual assimilation accelerated rapidly in twentieth-century
America, especially after the Second World War. The most pres-
tigious American universities opened their doors to all those who
could compete academically, and Jews surely could compete. They
at last gained equal access to the professional schools — law, medi-
cine, architecture — as well as to the Ph.D-granting graduate schools.
‘The price they were asked to pay, however, was very high. Too
high, The universities offered a Faustian bargain to Jews (and also
to Bible-believing Christians): “You may go as high as your brains
can carry you, just so long as you leave your religion off campus.”
Most academically oriented Jews could not resist this offer.”
Intermarriage with the gentiles whom they met on campus was
also nearly inevitable. Cohen’s remarks are on target: “The Jew,
in joining the West, no longer joined a Christian West, for he did
not jom a church wedded to a society... . The Jew joined an
alrcady de-Christianizing West, and as part of the bargain he
agreed — foolishly ~ to de-Judaize.”!*? What Nazi Germany’s poli-

 

16. T. Grunfeld, “Samson Raphael Hirsch—The Man and His Mission,” in
Judaism Eternal.

17. A very effective presentation of this post-1940 transformation of Judaism is
found in the Chaim Potok’s novel and the movie based on it, The Chasen. In the early
1960's, Potok served as editor of the Jewish Publication Society of America’ transla-
lion of the Hebrew Bible. Potok, “The Bible's Inspired Art,” New York Times Magazine
(Oct. 3, 1982), p. 63.

18, Arthur A. Cohen, The Myth of the Judeo-Christian Tradition (New York: Schocken,
1971), p. 186.
