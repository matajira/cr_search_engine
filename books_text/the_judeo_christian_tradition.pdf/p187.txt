Conelusion 173

to beat something with nothing. If there is no uniquely Christian
body of law, then there is no effective way for Christians either to
criticize or to reform the present world order. They have been
steadily absorbed culturally by the surrounding pagan humanist
culture.

Covenant-brcaking Orthodox Jews have dealt with the prob-
lem is a different way: to create a separate legal tradition that
officially claims continuity with the Old Covenant, but which in
fact is a monumental counterfeit. Since the late eighteenth century,
more and more Jews have abandoned this counterfeit covenant
law-order, as they have become steadily assimilated into the surround-
ing pagan humanist culture. They have abandoned the creeds of
Orthodoxy for other creeds — creeds far closer to humanism, but
which still go by the label Judaism. This phenomenon has been
called non-Jewish Judaism; it is adhered to by non-Jewish Jews.”

It is my prayer that Christians will become faithful once again
to the full-orbed New Covenant, as witnessed by their adherence
to His revealed law. I also pray that pagan humanists of all
varieties will also affirm the legitimacy of this covenant and aban-
don their own. But, finally, I pray that the Jews of all varieties
may abandon their false covenants — Orthodox Jews the counter-
feit Old Covenant of the Talmud; Conservative and Reform Jews
the counterfeit covenant of the Rights of Autonomous Man. May
the latter cease to mouth such babble as this: “he main division
theologically between people today is between those who have
adapted to the secular age and those who have rejected it.”?? The
answer is Jesus Christ,

Who is the image of the invisible God, the firstborn of every
creature: For by him were all things created, that are in heaven,
and that are in earth, visible and invisible, whether they be thrones,
or dominions, or principalities, or powers: all things were created

22. Isaac Deutscher, The Non-Jewish Jew and other essays (New York: Hill & Wang,
1968).

23. “Giving Judaism a Humanist Face,” Insight (May 4, 1987). The article features
the ministry of Rabbi Sherwin 'T. Wine.
