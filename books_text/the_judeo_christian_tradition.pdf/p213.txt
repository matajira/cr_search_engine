Subject Index

students of law, 59-61, 82
ghetto, 56, 57, 124-25, 148
gnosticism, 56, 57
God
bigamy, 16-17
divorces Israel, 15
Israel of, ix, 1, 178
Kingdom of, xi
marries Church, 16
non-kingdom, 33
people of, 5
remarriage?, 16
sovereignty, 48
spokesmen, 47, 59
stealing from, 116
wrath of, xi-xii
Graewz, Heinrich, 75, 7
Greeks, 27-28
Grounds, Vernen, 179
Guttman, Michael, 110, 140
Guttman, Joseph, 66-67

Haldane, Robert, 6

Ham, 107, 163

Hanina, 91

Harris, Lis, 152

Hegelian, 90

Hegelianism, 31

hell, 88

Helinism, 149-50

Heller, Agnes, 28, 29, 30

Herberg, Will, 22-28

Herford, R. Travers, 75

hermeneutic, 49-50, 51, 53-54, 130, 160
higher criticism, 51, 54, 55-56, 65, 151
Hinduism, 17

Hirsch, 8. R., 129, 149

Hodge, Charles, 7

holocaust, 181

Holy Spirit, 8, 11, 57

homosexuality, 87

Horowitz, George, 109-10, 115, 125-26,

199

127, 133, 139, 140-41
Hosea, 1]
honesty, 164
House, H. Wayne, 62
humanism
Babylonian captivity, 165
common enemy, xiii, 163, 166
conspiracy, 27
death of, 9
dominant, 21
idolatry, 23-24, 154
Judaism, 152
Judeo-Christian Tradition, 24-25, 28
legal revolution, 54-55
myth of, 155
mythology, 28
politics, 24
religion ag myth, 28
sanctions, 152
sovereignty of man, 42
voting, 6+

Ice, Thomas, 62
idolatry, 24, 51, 154
initiation, 103, 105
injury, 111-13
intellectuals, 30
interpretation, 48, 49-50, 51, 163, 164
Iraq, 170
Islam, 52, 69, 80, 107-8, 150
Israel
adoption, 16
blindness, 6
divorced, 15
harlot, 11, 15
of God, ix, 1, 178
nation of, x
state of, xiii, 50, 51-82, 63, 166,
169-72, 171

jealousy, 167
‘Jerusalem, 30, 77-80
