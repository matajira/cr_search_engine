124 The Judeo-Christian Tradition

ownership, as if ownership of a physical object were all that is
involved in ownership, and not also the legal immunities and legal
responsibilities that are inescapably bound up with possession of
the object. Maimonides did not say that the victim could not
demand that the beast be destroyed or sold in order to compensate
him. He did say that if the owner sells the animal, the victim can
collect compensation from the animal, and the buyer must reclaim
damages from the defendant.5>

Maimonides also added that the testimony of certain witnesscs
is invalid: slaves, shepherds, children, and women. “One must not
think that because only slaves, shepherds, or similar persons are
generally found in horse stables, cattle stalls, or sheep pens, these
should be heard if they testify that one animal has caused damage
to another, or that children or women should be relied on if they
testify that one person has wounded another or if they testify about
other types of damage.”

The Origin of the Ghetio

In response to such judicial standards, gentiles in the late
medieval period over-reacted by forcing Jews into urban ghettos
that were surrounded by high walls and locked at night. They did
not want to live as geographical neighbors to people who held such
a double standard.” They chose instead to allow Jews to be
governed by their own courts in most matters that involved dis-
putes between Jews.> Of course, when it came to Christian rulers
(and presumably also private citizens) who defaulted on loans, the
Jews may also have occasionally appreciated the walls that pro-

55. fbid., VITE6, p. 29.

56. f6id., VIII:I3, p. $l.

57. The social and political results of this policy were evil: foreed urbanization, the
creation of a permanently alienated political element within the towns, and the
eventual subsidizing of nineteenth-century Jewish radicalism, which was far more
common in urban settings than in rural ones.

58. Louis Finkelstein, Jewish Self-Government in the Middle Ages (Westport, Connecti-
cut: Greenwood Press, [1924] 1972)
