Conclusion 161

it do to de-emphasize it? A great deal. Why? For the sake of the
peace.

Both sides should be aware of the unbridgeable barrier be-
tween them. Both sides should also be aware of the equally unbr-
idgeable barrier between them and the Caesars of this world. It
has been the Caesars of this world, not the Christians, who have
been the great enemy of the Jews. It is the Caesars who have been
the great threat to the Christians, not the Jews.

Orthodox Jews and orthodox Christians are the traditional
enemics of the Cacsars of this world, because the Caesars are tied
to time rather than eternity. Their efforts have meaning only in
terms of time. But Jews and Christians are tied to eternity, and
live or dic in terms of this commitment. They are therefore the
ultimate traitors to the time-bound systems of this world. This is
why persecution always comes, cspccially after some crisis has
called into question the survival of a particular world system. In
this sense, both Jews and Christians are “a scparate people among
us” in the eyes of the humanists. What Rosenstock-Huessy wrote
of this world’s leaders is equally true in every era: “The ruler who
gives his name to an hour of history must be absorbed completely
in that hour. He must dive into its waves and be lost in it more
than any other man. For it is the ruler’s business to mark the
epoch, to appear on the stamps or coins of his country. Rulership,
because it personifies an epoch, always finds itself in a polarity to
the workings of Eternity.”! What he wrote of the Jews applies
equally well to orthodox Christians in history:

The pagan leader is the servant of time. ‘The Jew can never
“believe” in time. Since every Jewish leader or prophet thinks of
Eternity or of innumerable generations, the star of Judah always
shines most brilliantly in times when there are no pagan heroes.
‘When a nation is despoiled of its governing class, when a national
failure has brought a darkness without comfort or illumination, the
nation is struck by the fact that the Jews are not leaderless in the

1, Fugen Rosenstock-Huessy, Out of Revolution: Autobiography of Western Man (Nor-
wich, Connecticut: Argo, [1938] 1969), p. 222
