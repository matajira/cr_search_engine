16 The Judeo-Christian Tradition

The Bigamy Question

God is not a bigamist. Once the divorce from Old Covenant
Isracl was legalized, and God marricd His Bride, the Church — both
events taking place publicly at Pentecost — Old Govenant Israel
could never again be God’s wife, God’s law of divorce and remar-
riage is emphatic on this point:

When a man hath taken a wife, and married her, and it come
to pass that she find no favour in his cycs, because he hath found
some uncleanness in her: then tet him write her a bill of divorce-
ment, and give it in her hand, and send her out of his house. And
when she is departed out of his house, she may go and he another
man’s wife. And if the latter husband hate her, and write her a bill
of divorcement, and giveth it in her hand, and sendeth her out of
his house; or if the latter husband die, which took her to be his wite;
Her former husband, which sent her away, may not take her again
to be his wife, after that she is defiled; for that is abomination before
the T.orp: and thou shalt not cause the land to sin, which the Lorp
thy God giveth thee for an inheritance (Deuteronomy 24:1-4)

Now and forevermore, it is ilcegal for God to take Israel back as His
wife. Even if the entire Church were in heaven, this would be true.
Then how can there still be a promise for Tsrael to become
God's bride? Only through the promisc of adoption. “But as many
as received him, to them gave he power to become the sons of
God, cven to them that believe on his name” (John 1:12). Adop-
tion was the basis of marriage in the Old Covenant: a woman was
adopted into her husband’s family.'5 God adopted Israel — the
abandoned illegitimate child— before He married her (Ezekiel
16), The Jews can become God’s Bride today and in the future in
the same way that gentiles can: by joining the Church, the one and
only Bride of God. This is the meaning of “re-grafting.” There is
no other way of salvation. There is no other marital arrangement.
There is only one Bride; God is not a bigamist. He took no
gentile wife under the Old Covenant, and He will not accept a
pale imitation of Old Covenant Israel— modern Judaism — as

15. Sutton, Second Chance, ch, 10.
