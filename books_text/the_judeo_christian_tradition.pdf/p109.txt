“You Have Heard It Said” 95

matters.”” This view is consistent with the Talmud’s general view

of women: “The world cannot do without either males or females.
Yet happy is he whose children are males, and alas for him whose
children are females.””’ At least one section of the Talmud ques-
tions the wisdom of instructing women in the law: “How then do
we know that others are not commanded to teach her? — Because
it is written, ‘And ye shall teach them your sons’— but not your
daughters.”*

The Question of Circumcision
Most important of all is circumcision, the Talmud says.

It was taught: Rabbi said, Great is circumcision, for none so
ardently busied himself with [God’s] precepts as our Father Abra-
ham, yet he was called perfect only in virtue of circumcision, as it
is written, Walk before me and he thou perfect, and it is written, And J
will make my covenant between me and thee. Another version [of Rabbi’s
teaching] is this: Great is circumcision, for it counter-balances all
the [other] precepts of the Torah, as it is written, For ufler the tenor
of these words T have made a covenant with thee and with Israel. Another
version is: Great is circumcision, since but for it heaven and carth
would not endure, as it is written, {Thus saith the Lord,] But for my
covenant by day and night, I would not have appointed the ordinances of
Heaven and earth.”

Contrast these words with Paul’s: “But as God hath distrib-
uted to every man, as the Lord hath called every one, so let him
walk, And so ordain I in all churches. Ts any man called being
circumcised? let him not become uncircumcised. Is any called in
uncircumcision? let him not be circumcised. Circumcision is noth-
ing, and uncircumcision is nothing, but the keeping of the com-
mandments of God” (I Corinthians 7:17-19). He warned all men

26, Cited by Judah Goldin, The Living Talmud (University of Chicago Press, 1957),
p35.

27. Baba Bathra \6b.
8, Kiddushin 29b.
29. Nedarim 32a.

i
