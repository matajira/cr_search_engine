180 The Judeo-Christian Tradition

THE WILLOWBANK DECLARATION ON THE
CHRISTIAN GOSPEL AND THE JEWISH PEOPLE

“The Gospel is the power of God for salvation, to everyone who believes, to
the Jew first and also to the Greek.” (Romans 1:16)

“Brethren, my heart’s desire and prayer to God for Israel is that they may
be saved.” (Romans 10:1)

PREAMBLE

Every Christian must acknowledge an immense debt of gratitude
to the Jewish people. The Gospel is the good news that Jesus is the
Christ, the long-promised Jewish Messiah, who by his life, death
and resurrection saves from sin and all its consequences. Those
who worship Jesus as their Divine Lord and Saviour have thus
received God’s most precious gift through the Jewish people.
Therefore they have compelling reason to show love to that people
in every possible way.

Concerned about humanity everywhere, we are resolved to uphold
the right of Jewish people to a just and peaceful existence cvery-
where, both in the land of Israel and in their communities through-
out the world. We repudiate past persecutions of Jews by those
identified as Christians, and we pledge ourselves to resist every
form of anti-Semitism. As the supreme way of dcmonstrating love,
we seek to encourage the Jewish people, along with all other
peoples, to receive God’s gift of life through Jesus the Messiah, and
accordingly the growing number of Jewish Christians brings us
great joy.

In making this Declaration we stand in a long and revered Chris-
tian tradition, which in 1980 was highlighted by a landmark
statement, “Christian Witness to the Jewish People,” issued by the
Lausanne Committee for World Evangelization. Now, at this Wil-
Jowbank Consultation on the Gospel and the Jewish People, spon-
sored by the World Evangelical Fellowship and supported by the
Lausanne Committee, we reaffirm our commitment to the Jewish
people and our desire to share the Gospel with them.
