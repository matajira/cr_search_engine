80 The Judeo-Christian Tradition

tradition, while the rabbis’ comments on it are called Gemara.
‘The Talmud contains both Mishnah and Gemara. The rabbinical
comments comprise the bulk of the Talmud. Danby’s standard
translation of the Mishnah is one long volume. The Soncino Press
edition of the Talmud is 34 volumes, plus the index.

The Torah

When Jews speak of “Torah,””” they do not always mean the
Old Testament or even the Pentateuch. Sometimes they mean
something much broader. Christians are generally unaware aware
of this broader usage, which leads them to believe that Orthodox
Jews are somehow Christians without Christ, or Unitarians who
believe in miracles and angels, i.e., people who believe in the Old
Testament by itself. They think of Orthodox Jews as undeveloped
Christians, theological first cousins who were publicly disinherited
in A.D. 70. They have missed the point of Jesus’ absolute challenge
to the Pharisees.

Orthodox Judaism constitutes a rival religion that developed
alongside the early Church, The Pharisees insisted that the oral
law is equal to the written law, as surely as Christians insist that
the New Testament is as authoritative as the Old Testament, the
Muslims insist that the Koran is as authoritative as the Old
Testament, and the Mormons insist that the Book of Mormon is
as authoritative as the Old Testament. Each group really means
that its unique post-Old Testament document is more authoritative
now than the Old Testament is. No major religion since the fall
of Jerusalem has taken the Old Testament as its sole or even
primary authoritative document. Only the Karaite sect of Judaism
has pretended to.”

27. “Direction, instruction, doctrine, law": Oxford English Dictionary.

28. The tiny Karaite sect, begun in the mid-eighth century, openly opposed the
oral law until the nineteenth century, when Reform Judaism began to take hold of
Judaism. The Karaites never became influential. For this entire period, Rabbi Chajes’
mid-ninetcenth-century assessment is representative of the preceding seventeen cen-
turies of Judaism: “Allegiance to the authority of the said rabbinic tradition is binding
upon all sons of Israel, since these explanations and interpretations have come down
