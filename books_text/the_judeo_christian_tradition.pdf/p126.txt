12 The Judeo-Christian Tradition

another person’s servant. He insisted that there is no penalty. But
there is a penalty for injuring another man’s animal. “One’s slave
is regarded as his own person, but his animal is regarded as his
inanimate property. Thus, if one places a burning coal on the
breast of another’s slave so that he dies, or if one pushes a slave
into the sea or into a fire from which he can escape but he does
not escape and dies, the injurer is exempt from paying compensa-
tion. If, however, one does the same to another’s animal, it is
regarded as if he had placed a burning coal on another’s clothing
and burned it, in which case he is liable for payment, The same
rule applics in all similar cases.”'9 God’s law supposedly is more
concerned with the protection of animals than the protection of
men. T cannot imagine what other gencral principle of jurispru-
dence could be drawn from Maimonides’ exposition of this case
law.

He added strange qualifications to the Old Testament texts.
Consider the case of a man who injures another. Biblical law and
Talmudic law both impose economic penalties on the injury-
inflicting victors of such private conflicts, This is basic Old Testa-
ment Jaw: “And if men strive together, and one smite another with
a stone, or with his fist, and he die not, but keepeth his bed: If he
rise again, and walk abroad upon his staff, then shall he that smote
him be quit: only he shall pay for the loss of his time, and shall
cause him to be thoroughly healed” (Exodus 21:18-19). Maimo-
nides wrote, following Jewish legal tradition: “If one wounds an-
other, he must pay compensation to him for five effects of the
injury, namely, damages, pain, medical treatment, enforced idle-
ness, and humiliation. These five effects are all payable from the
injurcr’s best property, as is the law for all who do wrongful
damage.”™ There is nothing judicially objectionable here. As Maimo-
nides put it, “The Sages have penalized strong-armed fools by

19. Maimonides, The Book of Torts, Book 11 of The Code of Maimonides, 14 vols.
(New Haven, Connecticut: Vale University Press, 1954), “Laws Concerning Wound-
ing and Damaging,” 111:22, p. 176,

20. Itid., “Laws Concerning Wounding and Damaging,” I:1, p. 160.
