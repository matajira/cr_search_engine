182 The Judeo-Christian Tradition

Christians celebrate and maximize their Jewish identity, have
emerged as active evangelists to the Jewish community. Jewish
leaders often accused them of deception on the grounds that one
cannot be beth a Jew and a Christian. While these criticisms may
reflect Judaism’s current effort to define itself as a distinct religion
in opposition to Christianity, they have led to much bewilderment
and some misunderstanding and mistrust.

The Declaration responds to this complex situation and seeks to
set directions for the future according to the Scriptures.

THE DECLARATION
I. THE DEMAND OF THE GOSPEL

ARTICLE 1.1.
WE AFFIRM THAT the redeeming love of God has been fully
and finally revealed in Jesus Christ.

WE DENY THAT those without faith in Christ know the full
reality of God’s love and of the gift that he gives.

ARTICLE 1.2.

WE AFFIRM THAT the God-given types, prophecies and vi-
sions of salvation and shalom in the Hebrew Scriptures find their
present and future fulfillment in anid through Jesus Christ, the Son
of God, who by incarnation became a Jew and was shown to be
the Son of God and Messiah by his resurrection.

WE DENY THAT it is right to look for a Messiah who has not
yet appeared in world history.

ARTICLE 1.3.

WE AFFIRM THAT Jesus Christ is the second person of the one
God, who became a man, lived a perfect life, shed his blood on the
cross as an atoning sacrifice for human sins, rose bodily from the
dead, now reigns as Lord, and will return visibly to this earth, all
to fulfill the purpose of bringing sinners to share eternally in his
