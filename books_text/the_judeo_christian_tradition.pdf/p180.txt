166 The Judeo-Christian Tradition
An Alliance, not a Covenant

By the time that Jews entered the mainstream of Western
culture, Christians had long since abandoned faith in the continu-
ing validity of the specifics of God’s Old Covenant law. Thus, it
was the pagans who invented the myth of the Judeo-Christian
tradition, as Cohen has argued.*

How can this myth become a reality? There are reasons why
there has been great resentment on both sides. If Christians were
more familiar with the Talmud, they might take even greater
offense at the Jews, especially if both sides fail to understand the
cnormous threat of the common humanist enemy. Anti-Semites
from time ta time remind Christians of what the Talmud teaches.
If this public exposure of Talmudic texts is used only to fan the
flames of hatred, then it is an exercise in futility. On the other
hand, if this exposurc is designed to lay the cards on the table in
order to establish the basis of a cease-fire, then it should do no
harm. An ad hoc co-operation based on misinformation will not
survive the test of time. Our common enemies are too well organ-
ized.

An effective ad hoc alliance is not a covenant. There can be
no God-authorized covenants among those who do not accept the
same God.° But there can be legitimate alliances against a com-
mon enemy. “And there came one that had cscaped, and told
Abram the Hebrew; for he dwelt in the plain of Mamrc the
Amorite, brother of Eshcol, and brother of Aner: and these were
confederate [allied] with Abram” (Genesis 14:13). What has passed
in the past as a Judco-Christian alliance has in fact been a clever
cover for the triumph of political and cultural humanism. What
we need is an alliance based on whatever is mutually shared from
the Old Testament, and perhaps cven parts of thc New Testament

8 Arthur A. Cohen, The Myth of the, Judeo-Christian Tradition (New York: Schocken,
1971).

9. Gary North, Healer of the Nations: Biblical Blueprints for Intemational Relations (Vt.
Worth, Texas: Dominion Press, 1987), ch. 9.
