Sumptuary Legislation 45

not by capricious men, Formal law is to restrain the activities of
the state itself, limiting its arbitrariness.

A Hopeless Conflict of Interests and Lack of Harmony

 

In retrospect, it is not difficult for us to understand why the
New:England Puritans, no less than their English cousins, would
find it difficult to assign limits to a familistic state. It is rather
like children sctting limits on fathers, especially when fathers
confront their children not mercly with the threat of violence,
but also with the moral obligation of submission. Yet from the
1630’s through the 1670's, this is precisely what Puritan leaders
attempted to do, They wanted to permit godly men sufficient
freedom to exercise their personal callings, for they well under-
stood that if a’ man is personally responsible before God for his
acts, he must be given wide latitude in cxercising his personal
talents without interference from other men, including leaders.
Nevertheless, they also wanted to insure that the “family of
God’s people” would preserve its inherited status distinctions
and also insure that peace and harmony would prevail as a
testimony to the whole world. As the seventeenth century pro-
gressed, they were to. find that the two goals were very frequently
in opposition, and harmony was not maintained.

Modern commentators must be extremely careful not to read
our contemporary views about status back into the seventeenth
century — or at least not back into the first three quarters. There
was no public outcry from “oppressed” inferiors, no colony-wide
movement to redress grievances. There is little, if any, evidence
that the “inferior sort” and their elected representatives, the
deputies, were in fundamental opposition to the medieval view
of status obligations. Puritan society was in reality a society
made up of people who in England would have been regarded
as the “middling sort” — sons of the lesser gentry, yeoman farm-
ers, craftsmen, and others who had sufficient capital to make the
journey. There were servants, however, and these could wind
up as members of a truly lower class, but masters were expected
(and even compelled. legally) to provide some capital, usually in
