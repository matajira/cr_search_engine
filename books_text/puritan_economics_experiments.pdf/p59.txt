Sumpluary Legislation 5]

fact of life: time. Mcn do not discuss their personal futures if their
goal is to conceal their aspirations, fortunes, and plans. Yet they
must conceal such matters in a society motivated by feelings of
envy. Second, under such restraints, innovations arc unlikely,
since no one wants to let his neighbors see how much better off
a person is as a result-of some advance.” Cut off discussion of
the future, compromise men’s orientation toward the future,
penalize advancement technologically and personally, and the
society in question will show few signs of economic growth.”

Personal and Social Growth

Puritan preaching, therefore, ‘served as a stimulus to both
personal wealth in one’s calling and. economic development for
the community. Men were to be moderate in all things, and they
were not to pursue wealth for its own sake. This was made clear
by a century. of preaching, from John Cotton to Cotton Mather
to Benjamin Franklin. Nevertheless, ‘there is nothing innately
wrong with wealth in the Puritan view, however much a spiritual
snare and delusion great wealth might become. So when men
began to follow the tenets of the Puritan faith, they found them-
selves stcadily increasing in wealth, both personally and cultur-
ally. This was to raisc an absolutely baffling dilemma: how was
the fact of social mobility to be reconciled with medieval catego-
ries of fixed status, implying defined place and function?

The Puritans were. hardly the first people to face this. di-
lemma. The millennium of institutional struggles over monastic
reform in the Roman Catholic Church testifies to the traditional
nature of the problem. Irom the day that St. Benedict set forth
his eminently practical’ monastic rules~ humility, hard: work,
thrift, patience, self-help, discipline— the monasteries that fol-
lowed his guidc faced the problem of economic growth. The
monasteries had a tendency to’get richer and richer. ‘hen the

19. Schoeck, Enzy, pp. 46-50,

20. On the importance of future-orientaiion to economic and cultural lite, see
Edward C. Bantield, The Unheavenly City: The Nature and Future of Our Urban Crisis
(Boston: Litue, Brown & Co., 1969),
