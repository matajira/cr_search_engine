18 Puritan Economic Experiments

cels soon emerged, a market in which most farmers sought to. sell
distant lands and buy lands closer to their main holdings. The new
result was the coalescence of private farms. From here, it would be but
two short steps for farmers whose holdings were centered in outlying
areas to move their barns and then their houses from the village out
to their lands. As of 1686 few seem to have taken these steps, but the
way had been prepared and the days of a society totally enclosed by
the village were numbered. In any event the common-field system was
gone, taking with it the common decisions and the frequent encounters
of every farmer with his fellows which it entailed.”

The closer to Boston, the faster these changes occurred, for
with transport cheap enough — within 10 miles or so along a
well-traveled road — the effects of the free market could be felt
far more alluringly. It paid to become more efficient.

Cambridge lay across the Charles River from Boston. The
demise of the commons in Cambridge seems typical. The first
division took place in 1662. A second followed in 1665. Two
small divisions were made in 1707 and 1724. Various methods
were used to determine who got what parcels of land: lots were
drawn, or acres were distributed in terms of the number of cows
a family was allowed to graze on the common meadow, or a
committee was formed to consider other methods. In some towns
there was considerable strife; in others, the distributions were
relatively peaceful. The effects on Cambridge were significant,
and in retrospect they seem quite predictable. After 1691, it was
no longer necessary to pass new laws against the cutting of
timber from the commons. Men owned their own land, and they
cut or refused to cut as they saw fit. It was no longer necessary
to pass laws against selling timber to men from other towns, a
common feature of mid-seventeenth century legislation in the
towns. A thoroughly individualistic system of land tenure evolved.

Opposition to the Andros Regime
The final impetus to private ownership came in the 1680's.

25. Kenneth Lockridge, A New England Town: The First Hundred Years — Dedham,
Massachusetts, 1636-1736 (New York: Norton, 1970), p. 82.
