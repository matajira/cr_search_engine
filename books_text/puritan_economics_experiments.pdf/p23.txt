Common Ownership 15

up scarce time. A similar law for goats was passed two years
later, in 1639.” People naturally attempted to evade the Jaw, and
by 1648 the revenues supporting the town’s herdsman were not
meeting his salary. Conscquently, in typical interventionist fash-
ton, the selectmen decided to assess all men a certain amount,
whether-or not they ran cattle on the commons.”! A similar rule
was established in Watertown in 1665, and the massive evasions
encouraged the sclectmen to.pass an even stiffer law in 1670.7

Corrected Over Time

The confusion reigned for decades. As the Watertown re-
cords report so cloquently, “there being many complaints made
concerning the disorderliness of cattle and swinc and the multi-
tudes of sheep in the town, it was voted that the matter above
mentioned is left with the selectmen to consider something that
may tend to reformation and to present what they shall do to the
town to be confirmed.” Necdicss to say, the sclectmen could
not do anything about it, any more than half a century of Puritan
town governments before them. The only solution was the distri-
bution of the commons to local inhabitants — the demisc of the
commons.

‘Traditional patterns of life do not die out overnight. Men are
usually unwilling to change their way of life unless forced to do
so, either by economic circumstances or by direct political pres-
sure. The little town of Sudbury was a case in question. Its
inhabitants clung to the old English system of communal prop-
erty management. The access to the commons was restricted in
1655, and at least thirty younger men received no meadow
grants for their animals. They went out of the sclectmen’s mect-
ing ready to fight. Fight they did, until the town was split. They

20. The Records of the Town of Cambridge, Massachusetts, 1630-1708 (1901), pp. 28, 39.
21, Ibid., p. 72.

22. Watertown Records (1894), 1, pp. 92, 94-95.

23. Ibid, p. 142.
