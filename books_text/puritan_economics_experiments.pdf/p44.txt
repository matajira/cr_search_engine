36 Puritan Economic Experiments

by far the worst. The Indians, led by their prince who was known
as King Philip by the whites, struck without warning up and
down the edge of the New England fronticr. For almost a year
it looked as though they would triumph, or at least scriously
restrict European settlement in the area. Fistimates of the loss of
life have ranged above ten percent of the total population of the
whites, unmatched by any other U.S. war. Thousands lost their
homes and fled to the urban areas of more populated towns.
Understandably, such population shifts put terrible burdens on
the New England economy. Outfitting the militia and paying the
salaries intensified the disruption, as men Icft their farms and
shops to join the armed forces.

Pastoral Pessimism

For years, Puritan preaching had become increasingly pessi-
mistic in approach. After 1660, the old theological optimism had
begun to fade, and a new sermon style appeared. Known today
as the “jeremiad,” thesc sermons warned the people against the
consequences of sin and the failure of the young generation to
join the churches as members (membership had always been a
minority affair in New England, but after 1660 it was cven more
of a minority affair), Michael Wigglesworth’s famous poem, the
Day of Doom,®* and his less famous Ged’s Controversy with New
England, became best-sellers in their day (the early 1660's).
Pastors warncd of God’s impending judgment; the old faith in
New England as a triumphant “city on a hill” appeared less and
less in their sermons. The Indian uprising secmed to confirm all
the dire prophecies of the ministers.

Rev. Increase Mather (who along with his son Cotton be-
came the most prolific writing team in American history) pressed
the General Court to pass a list of “Provoking Evils” that had
brought on the curse of the war. The “democratic” deputies

 

34. Reprinted in Harrison T. Mescrole (ed.), Seoenteenth-Contury American Poetry
{Garden City, New York: Doubleday, 1968), pp. 42-54.
35. Ibid., pp. 55-113.
