Common Ownership Il

requirement—in fact, a new emphasis on an old requirement
~ appeared: restrictions on immigrants who might. become a
burden on the welfare rolls. The towns had stcadily become
more pluralistic theologically, but the fear of an increase in tax
rates was a truly ccumenical device. By offering econamic sup-
port to local indigents, the. townspeople were afraid that outsid-
ers might.take advantage of this legal charity. Barriers to entry
followed in the wake of “free” goods, however modest — and
they were very modest—the size of the public welfare allot-
ments.!©

Pressure on the Commons

The fear of increased welfare burdens was not the only
economic issue confronting established communities every time
a stranger sought admission as a resident of some town. In the
early years of settlement, each town had considerable land — six
to eight miles square, meaning anywhere from 30,000 to 40,000
acres—and relatively few inhabitants. Each resident had legal
access to the common pasturage and to any future divisions of
tand from the huge blocs owned by the town. But as the number
of inhabitants increased, and as more and more distributions of
town land reduced the available source of unowned land, the per
capita supply of land began to shrink. Those inhabitants who
had a share in the.common pasture and the common lands
sought to protect their control over further use and distributions
of such property. In town after town, a new rule was imposed:
outsiders had to purchase access to rights in the common prop-
erty from local inhabitants, The result was a new appreciation
of private ownership and private control of property, even among
men who had grown up in English communities that had used
the open field system of farming. The land hunger of New
England after 1650 created new incentives to gain and exercise

16. On the size of local town charities, see Stephert Foster, Their Solitary Way:
The Puritan Suctal Ethic in the First Century of Settlement in Nav England (New Haven,
Connecticut: Yale University. Press, 1971), p. 137,
