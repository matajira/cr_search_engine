28 Puritan Economic Experiments

ity to regulate prices and wages to the various towns.'* Neverthe-
less, the magistrates could not resist the cry of “oppression,” and
in March of 1638, a committee was set up to investigate com-
plaints against exorbitant prices and wages. Such ruthless pric-
ing, the authorities state, is “to the great dishonor of God, the
scandal of the gospel, and the grief of diverse of God’s people,
both here in this land and in. the land of our maturity... .7
The city on a hill was not setting a godly example te the heathen
and the people back home in England. The central government
continued to maintain its right to step in and regulate prices
when such action scemed warranted by the situation, but in
general the towns did most of the regulating work after 1636.
Only with the great Indian war of 1675-76 did the central
government stcp in to take vigorous action against high prices.
As Prof. Richard B. Morris summarizes the history of the con-
trols: “The codes of 1648 and 1660, and the supplement of 1672,
continued substantially the basic law of 1636 against oppression
in wages and prices, leaving to the freemen of each town the
authority to settle the rates of pay.”!®

Connecticut’s Wage Code

The other Puritan colonies were no better, with the exception
of the Pilgrim group in Plymouth. Connecticut’s General Court
insisted that men had not proved reliable when. left as’ “a law
unto themselves,” and therefore it passed an incredibly detailed
wage code. At first, the officials apparently had no insight into
the consequences of the regulatory nightmare they were con-
structing. Skilled crafismen were not to accept more than 20
pence a day (12 pence to a shilling) from March 10 through
October 11, nor above: 18 pence for work on any other day during
the year. This included carpenters, masons, coopers, smiths, and

14. Mass. Col. Recs., I, p. 183.

15, Ibid, L, p. 223.

16. Richard B. Morris, Government and Labar in Early America (New York: Colum-
bia University Press, 1941}, p. 62.
