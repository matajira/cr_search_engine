18 Puritan Economic Experiments

Tobacco consumption, which was regarded by Puritan lead-
ers as another unnecessary excess, had been under fire (sorry, T
couldn’t resist] from some of the directors of the Massachusetts
Bay Company right from its inception.® All four of the Puritan
commonwealths - Massachusetts, New Haven, Connecticut, and
Plymouth — passed numerous provisions placing restrictions on
the sale and consumption of the “noxious weed.” These prohibi-
tions were not really status oricnted; they were motivated by a
number of fears. One, understandably, was fire. Boston was
forever burning down in the seventeenth century, as Carl Briden-
baugh’s Cities in the Wilderness reports in some detail. At one
stage, Massachusetts prohibited the buying and selling of to-
bacco entirely, although it was legal to import it for re-export
later.'° They apparently thought it was all right to burn down
other cities, if local merchants were 1o gain some profit in the
transaction. Plymouth tried to ban its importation in 1641, but
repealed the law six months later.!’ Connecticut’s ban is the
most amusing in retrospect. It was directly tied to the issue of
personal health, but in the exact opposite of today’s concern: no
one under the age of twenty who had not already addicted
himself to tobacco was allowed to buy it, unless he had a physi-
cian’s certificate “that it is useful to him,” and he had to present
the certificate to the Court in ordcr to obtain a license to pur-
chase the weed.”

Time-Wasting
Taverns, brewers, and liquor retailers werc under restric-
tions throughout the century. Indeed, some of these controls are
as common today as they were in the New England colonies.
Mcn were not to waste precious time in taverns, the magistrates

9. 1bid., I, pp. 387-89, 403.

10. [bid., 1 (1635), p. 136; (1635), p. 180.

HM. Nathaniel B. Shurtleff (ed.), Records af the Colony of New Plymouth (New York:
AMS Press, [1855] 1968), XI, p. 38. (Cited hereafter as Plym. Col. Recs.]

12. Gona. Col. Recs., T (1647), p. 153.
