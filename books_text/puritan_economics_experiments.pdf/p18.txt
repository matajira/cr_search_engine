10 Puritan Economic Experiments

as possible to a system of individual farm management,

A third system was a sort of combination, the closed field
system of East Anglia. “There was one common pasture, but
each farmer was expected to provide a balance of arable pasture
and hay meadow for himself. He succeeded, or failed on his own
farming ability.”'? One of the problems in a Massachusetts town
like Sudbury was the diversity of backgrounds of its inhabitants.
There was’ no agreement as to where the locus of economic
sovercignty should be. Should it be the individual farmer? Should.
it be the town’s selectmen who controlled the resources of the
town commons?

Tightly Knit Communities

The towns and colonial governments of seventeenth-century
New England were not strictly theocracies; ordaincd ministers
could not be elected to- political office. But they were important
as advisers. Furthermore, the laymen of that cra were very often
more theologically motivated than ministers of this century.
Most of the towns were regarded as tightly knit Christian com-
monwealths by their inhabitants, and during the first fifty ycars
of their existence, they imposed restrictions on immigration into
the local community. They were concerned that newcomers
might not mect the religious and moral standards of the present
inhabitants. As late as 1678, the records of Plymouth Colony
offered the hope that “the Court will be careful, that whom they
accept are persons orthodox im their judgments.” The Puritan
towns of Boston, Cambridge, Dedham, and probably many oth-
ers all included the requirement that outsiders be cleared by
town officials before they were allowed to buy land. locally.
Braintree even included a restriction on land sales (though not
explicitly religious in intent) that local residents would have the
right to bid first on all property offered for sale to outsiders.

It is significant that in the final quarter of the century, these -
religious restrictions were generally dropped. Instead, a new

15, Ibid., p. 72
