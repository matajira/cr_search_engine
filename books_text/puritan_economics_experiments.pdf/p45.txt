Price Controls 37

instantly followed his advice; the “aristocratic” magistrates held
out for a week, until the news of another Indian victory forced
them into action. The list included the usual social failings: the
ignoring of God’s warnings, the many uncatechized children in
the commonwealth, the decline of church membership, the wear-
ing of wigs, long hair among nien, luxurious wardrobes, and the
existence of Quakers (this issue had faded in the early 1650's).
{Topless fashions among women—far more common in 1675
than today—drew a fine of 90 percent less than the fine for
being a Quaker, which may indicate something about Puritan
priorities in 1675.) But the list of “Provoking Evils” could not
be rounded out without calling attention to various economic
eppressions. Double réstitution for price gouging was imposcd
(for the amount of the overcharge); fines could also be imposcd
by. the court. (Today, we also hear suggestions to impose a
system of restitution for crime. victims, but with this grim vari-
ation: the victims are to be reimbursed by the taxpayers, not the
criminals.) Complaints against artisans and merchants could
be lodged by “ “victims,” ° but there was no mention of inflated
agricultural priccs.’

The key concern of the magistrates was slown.in the follow-
ing year, 1676. Inhabitants of different counties were charging
varying prices for the same goods sold to the militia. (You can
be virtually assured that the higher prices appeared in those
areas where the militia was actively engaged at any point in
time.) The General Court asserted that goods and services are
the same in value, wherever found, that is, a rifle is a rifle in any
county of Massachusetts. The answer to this problem, said the
Court, was the imposition of full-scale price controls, and this
became law on May 3, 1676. This was the last fling; not for
another century, when a ncw war broke out, would any New

 

 

 

England legislature pass such a comprehensive scheme of price
controls. So they created a central counci.

 

36. Gf, Trial (May/June, 1972}, the publication of the American ‘Lrial Lawyers

 
