58 Puritan Economic Experiments

older generation said. It is always becoming degenerate.”*? By
the 1680’s, the civil magistrates had abandoned the attempt to
maintain medieval concepts of social status in an increasingly
modern culture.

The older Puritan standards of social propriety had become
the victims, not of Enlightenment rationalism or philosophical
skepticism, but of operational Puritanism. Like the medieval
monasteries, the Puritan commonwealth had prospered as a
direct result of Puritan teachings. But unlike the monasteries, the
society of late-seventeenth-century New England did not heed
the call to reform itself. Indeed, the cries for reform were so
vague, especially after the defeat of the Indians in King Philip’s
War (1675-76), that had any magistrate wanted to listen, he
would have had nothing to hear in the way of specific reforms.
The saints in the churches were as unwilling to abide by the older
standards of dress and social status as those outside the churches
who had neglected to “own the covenant” of church member-
ship. Puritan sermons had warned of God’s wrath in the face of
hardheartedness, but when judgment came~—in the shape of
an Indian uprising—the Puritan military forces were victori-
ous. Success was the one thing that the pessimistic jeremiad
sermons of the second generation simply could not deal with
successfully.

33. Worthington C. Ford, “Sewall and Noyes on Wigs,” Publications of the Colonial
Society of Massachusetts, XX. (1917-19), p. 112.
