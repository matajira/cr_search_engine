Common Ownership 13

and assign plots, thus creating opportunities for local political
dissension, Animals that wandered around the fenced land often
broke down unrepaired fencing between plots, getting into some-
one else’s crops. Tension here was continual.

Fencing inspectors were important officials in every town.
Conflicts over responsibility were endless. Without private plots
privately repaired, such conflicts were inevitable. In the early
decades of Massachusetts, no single public policy prevailed long.
First, the colony’s General Court — the chief legislative agency
~ placed the responsibility for fencing on the local town; then it
placed the responsibility on the local individual citizen; next it
switched back to its original position of town control. ‘The stat-
utes did not function well in practice. Different communities had
different problems, and the central government had difficulty in
dealing with all of them through the use of any single administra-
tive policy.!”

The Tragedy of the Commons

The problem facing cvery selectman in every New England
village was the tragedy of the commons, as the biologist Garrett
Hardin has called it. Each person who has access to the benefits
of public property for use in his own personal business has a
positive incentive to drain additional resources from the com-
mons, and he has a very low or even negative incentive to
restrain him. he cost of his actions are borne by all the “own-
ers,” while the benefits are strictly individual. One more cow or
sheep or goat grazing on the town commons will register no
noticeable increase in the communally assessed economic burden
which rests on any single individual. Yet such grazing is imme-
diately beneficial to the owner of the animal, High benefits, low
costs: “Each man is locked into a system that compels him to

17. William B. Weeden, Economic and Social History of New England, 1620-1789, 2
vols, (New York: Hillary House, [1890] 1963), I, pp. 59-60.
