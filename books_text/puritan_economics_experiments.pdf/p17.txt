Common Ownership. 9

uted to the families in each town, were these problems overcome.

Varying Concepts of Ownership

In order to understand the thinking of the first half century
of New England’s settlers, we have to realize that these immi-
grants did not bring over from England some universally ac-
cepted concept of land ownership. There was an obvious ten-
dency for groups of settlers from one region in England to estab-
lish homogeneous townships in Massachusetts. English towns
had developed at least three major systems of land tenure: the
open field system, the closed field system, and the incorporated
borough. All three appeared in New England in the early years.

The open field system stressed the community administra-
tion of land. It is this system which we generally associate with
the word “medieval,” although the Middle Ages saw many
systems of land tenure. Sumner Chilton Powell has described
these systems in some detail in his Pulitzer Prize-winning study,
Puritan Village. The open field system “regarded the advantages
of the area as communal property, to be shared by all. No one
was. to exclude a neighbor from such a necessity as good meadow,
or the down, or the wuods. And if anyone practiced such exclu-
sion, or attempted to increase the amount of his holding at the
expense of his neighbors, all villagers reacted instantly to restore
their ‘rights.’” 8 Needless to say, this approdch did not survive
long in the setting of New England.

Quite different was an English borough like Berkhamsted.
In the early seventeenth century, over onc thousand acres “were
opened up, bought, or traded, in countess individual transac-
tions. If the men of Berkhamsted were doing nothing else, they
were trading land.”'* The legend of the Yankee trader was
rooted in this sort of English inheritance. There were some
enclosed lands, but most of the farmers were shifting as rapidly

13, Sumner Chilton Powell, Puritan Village: The Formation af a Naw Kngland Town
(Garden City, New York: Doubleday Anchor, [1963] 1966), p. 11.

M4, fbid., p. 26.
