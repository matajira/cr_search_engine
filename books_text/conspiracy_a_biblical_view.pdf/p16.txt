xvi CONSPIRACY: A BIBLICAL VIEW

insiders have taken control of the key resource, whether mon-
ey, the media, or whatever. This places the key to history inside
history. It divinizes the relative.

There are limits to the imagination of man, but not many.
The farthest reach of the limits of conspiracy historiography
that I have ever come across is a 115-page book by one James
Wood Monk: Karl and Taffy (1958), published by something
called the Eldorado Pilgrim Press, Mountains of the Moon. On
page 9, he announces his unique thesis. “This book is dedicated
to the thesis that the most powerful nation in the world today
is Wales, whose rate of multiplication and climb to power in at
least five continents since 1485 has been little short of phenom-
enal.” The year 1485 marked the defeat of Richard III by
Henry Tudor of Wales.” If you think that the secret powers
behind Western governments have been obscure, consider this
one-sentence assessment: “This book seeks to stretch in outline
the process whereby a combination of the faithless South Welsh
who abandoned and the loyal North Welsh who supported the
last Llywelyn of Wales in 1282 has [sic] now gained mastery of
the island, and through it of the Empire, and through the
Empire seeks to ally with British and Israelite forces in the
United States to dominate the world in collusion with represen-
tatives of the original Marxist revolutionaries who assisted
World Communism in its march to power” (p. 10). There are
many hidden trails on the road to historiographical nuttiness,
and Mr. Monk has done yeoman service in blazing one of
them. A word to the wise is sufficient.

11. [ was at Bosworth Field, where the battle took place, on its 500th anniversa-
ty. A few hundred visitors were there for the preliminaries to the weekend’s
reenactment of the battle. The followers of Richard HE seemed to outnumber
Henry's advocates. One of them cried out: “Death to the Welsh pretender!” Too late
by 500 years, according to Mz. Monk.
