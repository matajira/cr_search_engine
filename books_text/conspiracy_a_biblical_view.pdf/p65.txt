Two Kinds of Conspiracy 49

lation” always said that these were just verbal excesses. “They
really don’t mean it! So let’s make a deal.”

But aren't all “conspiracies through manipulation” always
secret all the time? No, they aren't. They are secret about sume
things. They were secret about the real intentions of the Feder-
al Reserve System before it was voted into law in 1913. They
were secret about the real intentions of the Federal income tax
before it was voted into law (or more accurately, before voters
were told that it had been voted into law) in 1913. But some of
their program has always public. Their “helpful guys” image is
carefully maintained. Nevertheless, prior to Dan Smoot’s Invisi-
ble Government (1962), the C.F.R. kept an incredibly low profile.

Books and Covers

Books, however, were always a high priority item. The
CER. is always bringing out books. So are its members. Bland,
boring books. One important C.F.R. outlet is Praeger Publish-
ers. How many variations of titles and books have appeared
that are along the lines of Richard N. Gardner's (Harvard U.,
Harvard Law School, Rhodes scholar, Oxford Ph.D., Deputy
Assistant Secretary of State, and, of course, C.F.R.) Jn Pursuit of
World Order: U.S. Foreign Policy and International Organizations
(Praeger, 1965), with a foreword by Harlan Cleveland (C.F.R.)?
Hundreds? Thousands? Who cares? Too many, at the very
least. (For more on Gardner, see my Preface, page xiii.)

Here is this book, written by a man with impeccable academ-
ic credentials, who writes a book with a very “peccable” the-
sis: that the United Nations is an institution which offers the
world hope. I happen to have bought a used copy, probably for
under a dollar, at some long-forgotten used book sale. It is an
autographed copy. It is dedicated to someone as follows: “With
gratitude for his contributions to the pursuit of world order
through ‘UN We Believe,’ Best Wishes, Richard N, Gardner.”
Maybe it’s a forgery. Anyway, forgery or not, it still sold for
under a dollar.
