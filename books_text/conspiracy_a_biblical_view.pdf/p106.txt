90 CONSPIRACY: A BIBLICAL VIEW

peace. The civil government is not to save mankind; it is to
protect residents from fraud and violence (domestic and inter-
national). But the State, in attempting to do more than this, has
done less. We are no longer safe on the streets, precisely be-
cause the resources of the State have been misdirected into
salyationary projects. Lyndon Johnson’s use of Graham Wallas’
ghastly phrase, “the Great Society,” illustrates the lure of politi-
cal messianism. From Teddy Roosevelt's Square Deal, to Wood-
row Wilson’s New Freedom, to Franklin Roosevelt's New Deal,
to Harry Truman’s Fair Deal, it has all been one basic move-
ment: the Raw Deal. Raw for taxpayers, entrepreneurs, and
freedom-lovers; beneficial for the manipulators.

There is only one way to deal with all forms of “conspiracy
by manipulation”: cut off their funds. Cut off their grants of State
privilege. Exposure is not cnough. They can live with exposure,
though not so easily as without it. But they cannot live without
the grants of State power that secure them from the competitive
market economy—from the nipping at their heels by brighter,
more innovative, and leaner competitors.

It is a wasted effort if we cut off the head of any conspiracy,
but leave available to their replacements the raw power of the
State, especially the centralized State. Like the hydra-headed
monster of Greek mythology, for every head cut off, two more
will spring up from the stump. The searing sword of economic
liberty must be used to cauterize the monster’s open wound
and seal it. No more government-guaranteed loans, no more
taritis, no more import quotas, no more racial hiring quotas, no
more price supports, no more minimum wage Jaws, no more
compulsory union membership, no more graduated income
taxes, and no more fractional reserve banking.

Above all, no more fractional reserve banking.

in short, the primary public institutional counter-offensive to
all conspiracies is a civil government which is governed by this
fundamental truch: there is no such thing as a free lunch. Or to put
it bluntly, “You get your hand out of my wallet, and lll get my
