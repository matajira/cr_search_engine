78 CONSPIRACY: A BIBLICAL VIEW

with Gary Allen and Cleon Skousen, who concluded the same
thing.)

These mistakes do happen. For example, Otto Scott, a
profound but unfortunately little-known conservative journalist-
author (the man who coined the phrase, “the silent majority”),
had his sensational book on John Brown published by Times
Books, a subsidiary of the New York Times. The Secret Six reveals
the details of the conspiracy of Unitarian ministers behind the
murderous John Brown in the 1850's.” The Secret Six hit the
book stores in 1980. Then, according to Scott, the company lost
any interest in promoting it. (This is putting Scott’s version as
mildly as I can.) He bought back the publishing rights and all
of the remaining copies later that year.

A similar case, according to legal scholar Henry Manne
(MANee], happened to him when a pro-free-market book of his
got into print, and it subsequently outraged a senior official in
the publishing company, who told Manne face to face that he
intended to kill it. That book, too, created a minor sensation,
but in the economics profession and scholarly legal circles. This
was not the intention of the publisher, although it had been
Manne’s intention.

Tragedy and Hope was published two years before the conser-
vatives began getting excited about it. It initially set no sales
records. Don Bell (of Don Bell Reperts) stumbled upon that
single copy in 1966 and featured it in one of his newsletters,
but not many people paid any attention. Word began to get out
by 1968. It began to be quoted by Gary Allen in American Opin-
ion, the John Birch Society magazine, beginning in early 1969.
Then Cleon Skousen published The Naked Capitalist in 1970.
This book was basically a compilation of excerpts of Quigley’s

3. R. J. Rushdoony had written about the Secret Six in 1963, but few people
have ever read his chapter on “The Religion of Humanity” in his low-selling litle
classic, The Natwe of the American System, which was published by a small religious
publishing firm, the Craig Press.

 
