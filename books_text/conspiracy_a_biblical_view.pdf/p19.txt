Introduction 3

until we can agree on what, exactly, is the Bible's approach to
a proper understanding of history, we remain be confused.
People are not agreed about the nature of God, man, law,
sanctions, and time.' Therefore, people are not agreed about
the nature of history.

We need to be clear about this: the war I spoke of is also a
war over the proper interpretation of historical facts. The facts
don’t just “speak for themselves.” Men speak in the name of the
facts they have chosen to speak about. We are creatures. We are
not omniscient. Therefore, we all pick and choose the facts that
we believe are most relevant. Relevant to whose purposes and rele-
vant to what goals? Therein lies the problem of historical inter-
pretation.

The Public’s Skepticism

Over the last 30 years, and especially since 1971, there has
been an increasing interest in the United States concerning the
existence, influence, and relevance of hidden, clandestine con-
spiracies. All ideological groups have participated. We have
been presented with numerous “conspiracy theories” from the
conservative right,” the libertarian right,* the old left (which
generally prefers “power groupings” or “class influence” to
more highly personalized conspiracy theories),* and the new

}. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed.; Tyler,
‘fexas: Institute for Christian Economics, 1992).

2. The classic example is the book by Gary Allen and Larry Abraham, None Dave
Call It Conspiracy (1972) and the sequel by Abraham, Call Hi Conspiracy (1985).

3, The works of Antony Sutton are the best examples: see the Bibliography.
Murray Rothbard’s book, America’s Great Depression (Princeton: Van Nostrand, 1963),
is a conspiracy theory of the origins of the great depression in the pro-British infla-
tionary monetary policies of the Coolidge era and the statist responses of Herbert
Hoover. See also Rothbard, Wall Street, Banks, and American Foreign Policy (Bur-
tingame, California: RRR, 1995); The Case Against the Fed (Auburn, Alabama: Ludwig
von Mises Institute, 1994).

4, A best-seller of the late 1960's was Ferdinand Lundberg, The Aich and the
Super-Rich: A Study in the Power of Money (New York: Lyle Stuart, 1968). For a mare
scholarly sociological analysis, see G. William Donhoff, Who Rules America Now? A View
