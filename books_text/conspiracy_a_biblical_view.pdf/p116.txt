100 CONSPIRACY: A BIBLICAL VIEW

There are many books on the conspiracies of this world.
What there has not been is a serious, long-term, multiple level,
yet deceniralized counter-offensive against the forces of evil.
Conservative organizations have come and gone, but still the
power-brokers remain entrenched in the seats of power. They
still make deals with our mortal enemies. All the paperback
exposés since 1961 have done nothing to roll back the illegiti-
mate profits of the deal-docrs. We are not saved by knowledge,
nor are they particularly threatened by it. Paperback books will
not dislodge them. A principled, long-term, ethics-based
counter-offensive will dislodge them. Shrink the State!

(Written, remember, in 1985:] I propose a program. Some
variant of this program must be adopted if we are to have any
meaningful hope in recapturing the machinery of civil govern-
ment, the media, and the educational institutions. It will be
done. It has already begun. How long it will take is problemati-
cal; 1 think we will begin to see major victories before the year
2005. In two decades, our enemics will begin to feel the pres-
sure; possibly sooner. They act as though they are on the offen-
sive, but in fact they are already on the detensive. We need to
be prepared to replace them in every area of life when the
spiritual revival comes. This means that we need years of pa-
tient hard work in order to gain the experience we need to
lead. There is no quick fix. We need the same kind of experi-
ence that David gained in the years that King Saul pursued
him.

What I outline here is only an outline. Many steps are left
out, and almost all the details. But something iike this must
develop if we are to preserve our declining freedoms.

Localism
You must start where you are. You must begin to take seri-
ously the responsibilities God has assigned to you locally.
“Think globally, work locally” is a biblical principle.
