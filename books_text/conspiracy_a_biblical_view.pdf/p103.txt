The Shifi in the Climate of Opinion 87

Why? Because they are opportunists by conviction and wind-
testers by training. They understand that the pragmatist’s ques-
tion, “Does it work?” is being answered in the negative, in case
after welfare case. The conservatives, especially the neo-conserv-
atives, may not be offering consistent, free market, Constitu-
tional solutions in many instances, but that is nor the issue. The
issue is, why have intellectuals begun to question the “tried and
true” New Deal-Great Society answers? Why are these intellec-
tuals critics at last announcing: “tried and false” solutions?

The answer sends chills down the spines of the manipula-
tors: because the statist solutions thai they recommended, and they have
long used to create their insulated, government-protected world, ave
visibly failing. As the public, through new intellectual leadership,
catches on, the power base of the conspirators will be threat-
ened.

These people are really not that smart. After all, these are
the people who, after seventy years, finally got their very own
hand-picked man to be President of the United States, and it
turned out to be grinning Jimmy, with his beer-loving brother
(a paid Libyan agent), his evangelist sister (a pornographer's
spiritual counsellor)—the President who produced simultan-
eously the worst foreign policy record in decades (including
Salt 11, which even the liberal, Democrat-conwrolled Senate
refused to sign), and the most inflationary economic policy in
peacetime U.S. history. In short, Jimmy (“Why not the
Best?”) Carter was visibly the least competent President in
U.S. history.

It doesn't build up a conspirator’s self-confidence, does it?
This, in my view, is the heart of the matter. The conspirators are
losing self-confidence. That is always the beginning of the end: in
business, in politics, and in conspiracy. They are now on the
run. The emperor has no clothes. Do you a hear a grinding,
rasping sound? That’s the Trilateral Commission scraping the
bottom of the barrel. I smell fear. What should be our re-
sponse? Attack!
