The People’s Will 23

let alone summarize. But in this century—indeed, since Rous-
seau’s late 18th-century writings—one view seems to prevail, at
least in the history textbooks: the will of the People.

The concept of “the People” has, for over a century, served
the “toreadors” of professional historiography as a red cape: to
focus the attention of the victims away from the sword. The
People’s will cannot be thwarted, we all know. Watch the Peo-
pie march forward! The People will control the evil special-
interest groups. Federal legislation will protect the People, for
the People have so willed it. Forget about actual individuals;
keep your eyes on the People.

Does all this sound vaguely familiar? We can almost see
Dorothy in front of the screen, with the Wizard’s glaring face
looking out at her. “Don’t mess with the People! The People’s
will is sovereign!” she is warned. Meanwhile, Toto is behind the
curtain, pulling at an old man’s pants leg. “Pay no attention to
the old man behind the curtain,” she is sternly warned by the
image.

There are some people behind that curtain who don’t want
you to pay any attention, and they are not “the People.”
They’ve even gone to the expense of training and employing
three generations of professional historians to explain to you
why nothing important is going on back there.

Naive Conservatives

The problem is, too many conspiracy theorists naively be-
lieve in a conservative version of the will of the People. They
understand that the People do not, in fact, understand what is
going on, and that the People do not control the drift of events.
Nevertheless, these theorists believe that if the People really did
understand what is going on and has gone on, they would
“take matters into their own hands.” They would rise up and
throw off their chains. In other words, the world should be
governed by the People. The world could be governed by the
