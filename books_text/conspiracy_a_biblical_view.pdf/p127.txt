Conclusion 111

This decision must be made before getting involved in any
long-term program of self-education; if it isn’t, selfeducation
can easily become an exercise in self-paralysis and eventual
psychological escape. The more a partially committed person
Jearns about the influence of the enemy, the more he grows
convinced of the enemy’s omnipotence. Too much insight into
the workings of the opposition paralyzes weak investigators.
This is why I seldom recommend that someone begin a detailed
investigation of occultism; there ought to be a legitimate “need
to know.” One or two books on the topic should be sufficient,
except for a prayerful specialist who needs more information
for a particular task, not just mformation for its own sake. The
“burned out” conservative who served as a discussion group
leader for 15 years is a familiar casualty in the war.

When you get involved in anything, count the long-term
costs. “For which of you, intending to build a tower, sitteth not
down first, and counteth the cost, whether he hath sufficient to
finish it? Lest haply [it happen], after he hath laid the founda-
tion, and is not able to finish it, all that behold it begin to mock
him, saying, This man began to build, and was not able to
finish” (Luke 14:28-30).

The will to self-education is the third stage. The enemy has
captured most of the tax-financed and non-profit educational
institutions of our cra. The enemy has also captured the major
communications media: newspapers, book publishers, radio and
television. They screen out facts that are uncongenial to their
jong-term goal of the political unification of mankind. Thus,
truth-seekers are forced to find better information by means of
“alternative media.” Bible-preaching churches are the place to
begin the search, but only a start. They, too, have been com-
promised. Their ministers and leaders have been educated in
the enemy’s schools and propagandized by the enemy’s media.
They, too, need to get involved in a self-education process.
Only a minority are willing to do this. But at least they operate
