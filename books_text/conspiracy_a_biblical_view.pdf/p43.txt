The People’s Will 27

logical liberalism (what later came to be called the social gospel)
and the spread of Darwinism and other forms of evolutionistic
thinking.* This moral shift led to the rise of the carly twentieth-
century American movement which historians call “Progressiv-
ism.” The point is, it was part of a major shift in the climate of
moral opinion, and no conspiratorial group created it. They
did, however, use it and profit from it. They still do.

Take the case of the sixteenth amendment, the Federal
income tax. (As an aside, Red Beckman and former Tllinois
revenue agent Bill Benson have discovered that it was never
ratified properly in 1913. Technically, it is an illegal amend-
ment.° Is this really significant, except as a curiosity of history?
The 14th amendment wasn’t ratified legally, either, since cer-
tain states were not allowed to vote. The point is, the public in
1913 was willing to ratify it, even though certain technicalities
were missing. There was no hue and cry of outrage when the
amendment was announced as having passed. Why not? There-
in lies a tale.)

How was that amendment sold to American voters? By an
excecdingly evil appeal: “Soak the rich!” It was an appeal based
on covetousness, pure and simple. And, like all forms of evil, it
backfired. It led to the capture of the middle-class voters by the
rich who were supposedly the targets of the law.

Rockefeller, Harriman, Morgan, Carnegie, and all the other
“masters of 1913” knew how to to recruit and control profes-
sional politicians, who in turn knew how appeal to the voters.
The 16th amendment was a classic Brer Rabbit ploy: “Don’t
toss us into that briar patch! Anything but that.” And poor,
dumb middle-class voters acted just like Brer Fox. They tossed
the elite into the briar patch—the briar patch of tax-exempt

4. G, Gregg Singer, A Theological Interpretation of American History (Nutley, New
Jersey: Craig Press, 1964).

5. Bill Benson and M. J. ‘Red’ Beckman, The Law That Never Was—The Fraud of
the 16th Amendment and Personal Income Tax (Box 350, South Holland, Illinois: Constit-
utional Research Associates, 1985).
