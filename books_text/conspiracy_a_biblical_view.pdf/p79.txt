Court Historians 63

of opinion matches up with economic self-interest could a con-
spiracy rule that long—and then only if the conspiracy really
did represent the people’s best interests. But then it would not
be a conspiracy any longer.

Relativism

But wait a minute. Those of us who hold a conspiracy view
of history are also interested in discussing the climate of opin-
ion, or economic forces that create the historical setting for a
shift in the climate of opinion. What is the difference between
our interpretation and the interpretations of conventional class-
room historians? The difference is chis: the conspiratorialists
know that there is a continuing ideological and theological war
going on. Different players, same issues. New faces, same con-
flict. Those who favor a conspiracy view argue that there are
fundamental issues—moral, religious, and political issues—that
divide good from evil, good guys from bad guys. In short,
conspiracy buffs usually are opposed to ethical relativism. They
tend to be moral absolutists. They view history as a continuing
personal struggle between the forces of good and the forces of
evil,

This is what outrages professional historians, including most
economic historians. (An exception was Murray Rothbard, who
believes in natural law and permanent ethical standards, and
who was ready to consider almost any conspiracy theory.) They
are weaned on a diet of ethical relativism; this perspective is
basic to all the social sciences and humanities. They will admit
today that Stalin was evil (by feday’s standards). “But we need to
understand that in his time, and confronting the situation of an
economically backward nation, Stalin faced tremendous diflicul-
ties in modernizing Russia, so measures that we now regard as
extreme had to be imposed . . . blah, blah, blah.”

On the other hand, Hitler was absolutely evil. (Goodbye
relativism—or so it initially appears.) Don’t push them on why
it is more evil to slaughter Jews than kulak peasants. They grow

 
