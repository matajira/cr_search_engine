Maverick “Insider” Historians 81

piece. It is one of those exceedingly rare books which is simul-
taneously seminal and seemingly definitive—not just a path-
breaker, but a four-lane highway. It is a standing testimony to
the failure of all previous, certified, Establishment scholars to
take seriously the role of conspiracies in European history.
Furthermore, while Quigley almost never provided footnotes
(though there are a lot of them in Anglo-American Establishment),
Billington buries the reader in footnotes, in more languages
than any of us cares to learn, and from more obscure books
and scholarly journals than any of us cares to know about. The
silence from the historical profession has been deafening. (The
same silence also greeted volumes two and three of Antony
Sutton’s previously mentioned three-volume bombshell, Western
Technology and Soviet Economic Development.) What they cannot
answer, professional historians prefer to ignore.

Billington focuses on the revolutionary underground: secret
societies, pornographers, occultists, and revolutionary journal-
ists, who established the basic philosophy and organizational
structure of the twentieth century’s bloody revolutionary
groups. What he shows is that the “rational” socialists and revo-
lutionaries of the “left” were from the beginning deeply mixed
up in such things as occultism, irrationalism, and pornography.
He exposes the dark side of “progressive” revolutionary forces.

He does not discuss events after 1917, nor does he provide
much material that would link today’s Establishment manipula-
tors (or their spiritual forebears) to revolutionary movements.
What he does do, however, is to demonstrate that twentieth-
century Marxism and socialism were born in dubious circum-
stances, Without support from the occult underground and
what income and influence the founders derived from service
as popular journalists, there would have been no “inevitable
victory of the proletariat,” no “vanguard of the revolution.”

Billington had previously been a Harvard and Princeton
history professor, and is a C.F.R. member. Today, he is the
Librarian of Congress. By ail standards, he is one of the high-
