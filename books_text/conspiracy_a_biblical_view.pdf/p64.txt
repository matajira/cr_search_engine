48 CONSPIRACY: A BIBLICAL VIEW

premise of the existing underlying ethical unity of mankind,
meaning “mankind properly understood.” They, of course, are
just the people to “properly understand” mankind, whether
mankind agrees with them or not.

Secrecy

What these manipulating conspirators need is secrecy. They
realize that at this stage of history, men publicly disagree about
the fundamentals. If they try to “hold mankind together” by
serving as intermediaries between (or among) various warring
societies, they need to do so invisibly. Voters in the United
States never wanted to join together in a one-world government
with the Soviet Union. They knew full well who would have
been the policeman in such a society. If “conspirators through
manipulation” had tried to try persuade the average American
voter to allow them to move forward in the creation of such a
New World Order of ethically unified humanity, they would
not have gained co-operation. The collapse of the Soviet Union
in 1991 has now made this project irrelevant. The question now
is: How soon will tormer Communist states be brought into
NATO and the other internationalist organizations?

The conspirators always kept their program of long-term
convergence relatively quiet. [ say “relatively.” Since 1973, the
Trilateral Commission has published its intention repeatedly to
create a New World Order. Doesn't this refute my contention
that they are a conspiracy? Aren't all conspiracies always com-
pletely secret?

No, they aren't. Adam Weishaupt’s Illuminati were almost
entirely secret. But, as time goes on, the conspirators have be-
come more open, especially the “conspirators by execution.”
Hitler published Mein Kampf. Lenin published his intentions
repeatedly. True, they did not announce their intention to
liquidate specific numbers of specific groups, but they an-
nounced their general intentions. But hardly anyone in power
believed them. Why not? Because the “conspirators by manipu-
