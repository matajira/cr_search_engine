40 CONSPIRACY: A BIBLICAL VIEW

the process more clearly than anyone ever has in his classic
little booklet, What Has Government Done to Qur Money?, which
you can (and should) buy from the Mises Institute, Auburn,
Alabama. I’ve never read anything better on money. Let me
simply say that the monopoly of fractional reserve banking is
inherently corrupt, inherently a process of legalized theft, and
inherently power-seeking.

When you deposit, say, a check for $100 into your bank, the
bank takes about $10 of that money and sends it to the Federal
Reserve System, our nation’s partially private and partially
governmental central bank. The FED pays no interest on the
money. This $10 serves as a legal reserve for the money. Now,
your banker makes money by lending money. He takes the $90
and loans it out. The fellow who borrows it then spends it. The
recipient takes the $90 and deposits it in Ais bank. His banker
takes $9 and sends it to the FED. Then he loans $81 to some
borrower, who spends it, and the recipient takes the $81 to his
bank.

You get the picture. In theory, $900 can come into circula-
tion on the basis of your original deposit of $100, if the reserve
Tatio is set at 10%. This is the “genius” of fractional reserve
banking. If you wonder why we have inflation in the modern
world, here is a good place to begin looking.

Who, then, controls the Federal Reserve System? And why
was it established? Why was it, in the words of Thibaut de Saint
Phalle, an intentional mystery?"

Again and again, the story of Establishment conspiracy re-
turns to the big New York banking firms. For centuries, the
plans of the conspirators have originated in conference rooms
of the great banks, or in conjunction with the banking establish-
ment.'! Why? Because money is the central institution in a

10. Thibaut de Saint Phalle, The Federal Reserne System: An Intentional Mystery
(New York: Praeger, 1985).

BL, John Brewer, The Sinews of Power: War, Mones and the English State, 1688-
1783 (New York: Knopf, 1988); P.G. M, Dickson, The Financial Revolution in England.
