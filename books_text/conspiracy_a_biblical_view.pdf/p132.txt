116 CONSPIRACY: A BIBLICAL VIEW

It must begin with knowledge. This is why I have written a
full-length study of the forces arrayed against us: Unholy Spir-
its: Occultism and New Age Humanism (1986).° Until Christians
understand the nature of the confrontation, they will continue
to tilt at windmills.

The kind of information Christians need is not usually avail-
able in bookstores. Not in Christian bookstores, because Chris-
tian bookstores rarely have carried books like Quigley’s Tragedy
and Hope, Allen’s None Dare Call It Conspiracy, and Billington’s
Fire in the Minds of Men. (Some now carry Pat Robertson's New
World Order. Part 2 is such a good introduction that the book
was widely criticized by the liberal press, unlike his other books,
which have received little media attention.) Humanist book-
stores seldom carry books like my Unholy Spirits. The books 1
list in my bibliography are usually out of print and are hard to
locate, even in large university libraries. If it were not for copy-
right problems, I would put a lot of them on a CD-ROM.

Nevertheless, you need to read. He who refuses to read will
be in a poor position to lead. If we think our opponents have
done a terrible job, then we need to prepare ourselves to do a
better job. They've done their homework. Are we doing ours?

It is time to sign up for a religious war that will last for the
rest of your life. If we do our work faithfully, maybe we will
start seeing some major victories before we get very far into the
next millennium. I think we will. I smell victory. I think the
enemy, for the first time in a century, has begun to smell de-
feat. They are trying to speed up their timetable because their
dream of a one-world order is unravelling. Technology is now
against them: decentralization. Newsletters are multiplying:
alternative information sources. The World Wide Web is be-
yond anyone’s control. The public schools are disintegrating.
And millions of people are catching on. This is good news for
God’s people and bad news for His enemies.

6. Today published by the Institute for Christian Economics.
