8 CONSPIRACY: A BIBLICAL VIEW

Cosmic Personalism

You need to understand something about serious Christians
and serious Jews: they have a view of history which is personad.
They recognize that God created the world. The world is not
now, nor has it ever been, an impersonal product of random
material forces. Biblical religion is inescapably a religion of
cosmic personalism.' Men are responsible personal agents—re-
sponsible primarily to God and secondarily to each other
through institutional arrangements: church, State, family, econ-
omy. (1 capitalize “State” when I refer to civil government in
general; I don’t capitalize it when I refer to the regional iegal
jurisdiction known as a “state.”) Furthermore, biblical religion
sees history as the product of a giant cosmic struggle: between
good and evil, between God and Satan, between redeemed men
and rebellious men. This struggle is innately and inescapably
personal.

This struggle is not simply personal, however; it is inherently
conspiratorial. The Bible tells about a great conspiracy against
God. It is a conspiracy which affects every area of life, including
politics. David describes it in Psalm 2. In David's day, there was
a conspiracy among the kings of the earth against God. It was
in existence long before David’s day. That same conspiracy is
still raging, even though there are no more kings of any impor-
tance left on earth.’ It is an age-old struggle.

For as long as there are history and sin, members of this
conspiracy will be enraged at righteousness. The conspirators
“breathe together” (con = with; spire = breathe). They “breathe
together” against God and God's law, and also against all those
who are faithful to God, or who may not even believe in God,

1. Gary North, The Dominion Covenant; Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), ch, 1: “Cosmic Personalism.”

2, King Farouk, the deposed puppet monarch of Egypt, put it very well when he
announced: “There are but five kings left in the world: the king of England, and the
kings of clubs, diamonds, spades, and hearts.”

   
