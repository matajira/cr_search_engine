Court, Historians 69

as it is in a free, competitive market. People who work for univ-
ersities or foundations must please their peers and their imme-
diate superiors. A person cannot prove his or her importance to
the organization by pointing to the corporate profit-and-loss
statement, There is no profit-and-loss statement in a non-profit
organization. Thus, scholars are figuratively held captive by
their peers. Ideological deviation in such an environment can
be fatal to a career.

These people are usually petrified of the free market. They
have spent all their lives in academic bureaucracies. The uncer-
tainties of market competition scare them, which is why most of
them work for comparatively little money (although they don’t
have to work very hard, either). They are psychologically tied
to their jobs in a way that people in the business world are not.
The tighter the academic job market—and it grew incredibly
tight in 1969 and has remained so—the more fearful they are.
They can imagine few employment alternatives outside their
non-profit universe. Thus, a raised cyebrow from a superior,
especially if an assistant professor has not received tenure—a
legal guarantee of lifetime employment—can work wonders.

Something else must be understood. Advancement—either
upward or outward (a better place of employment)—is essen-
tially medieval. It is not what you know but whom you know.
Especially in your first few jobs, you need the support and
recommendations of senior professors. Personal recommenda-
tions are major screening devices for hiring. This is why people
try to get into prestigious departments in graduate school:
better-known senior professors have a wider group of contacts.
Contacts are vital.

To make it into the academic “big time” in social science,
you have to be an absolute genius, as well as someone who has
a knack at writing articles for professional journals, or you have
to have the right contacts. But few people are geniuses. Thus,
contacts are probably 80% of the game for 80% of the players in
the big time competition. (Yes, the same rule applies to the
