108 CONSPIRACY: A BIBLICAL VIEW

You need a person who reads, votes, donates his time and
money to charities, takes his kids on vacation each year, and
generally is a credit to God’s kingdom. You should not be
trying to recruit the walking wounded, the spiritual basket cases
who wind up in the church. You need to be highly selective in
recruiting people to the cause.

You need self-evaluation, too. Are you perceived as a “red
how’? Are you perccived as a loose loaded cannon rattling
around on the deck? Are you noted for your temporary com-
mitment to this or that cause to “save the world” — causes that
usually fail, and which have yet to save the world? If so, you are
a liability in the battle against conspiracies. Stay on the sidelines
in this fight. Get involved in some other cause, such as bake
sales or visiting the old folks home. You have to demonstrate
that you are a worthy follower before anyone is going to believe
that you are a potential leader. You need to “wait tables” for a
few years, the way deacons should before becoming elders (Acts
6:2).

Conclusion

As anyone can see, there is a great deal to be done. There is
therefore a great deal that anyone can do at several levels.
Anyone who asks rhetorically, “But what can 7 do?” now has
some very specific answers. This rhetorical question is not to
become an excuse for inaction. If you do nothing now, you have no
excuse.

Do I expect a flood of thousands of letters from dedicated
people who have decided to get involved in one or more of the
projects I have listed in this chapter? Of course not. Not one
person in a hundred, if that, will respond by letter. But per-
haps five or six per hundred who read this book will begin to
do something, even though they never identify themselves by
letter. Maybe a few hundred churches will at least begin to get
involved in stage two, the stage beyond the Christian school
fight and the abortion fight. [f there is no response to the con-
