The Shift in the Climate of Opinion 85

The rise of the neo-conservative movement since 1965 is one
indication of this shift. A lot of articulate New York Jews are
not 1930's Trotskyites any longer; they are defenders of at least
a modified free market, and they were vociferous critics of
Communism of all varieties.

Let me give you a choice example. Midge Decter is the wife
of Norman Podhoretz, the former editor of the highly literate
neo-conservative magazine, Commentary, published by the Amer-
ican Jewish Committee. In one 12-month period, 1980-81,
Decter, as an editor of Basic Books, oversaw the publication of
four crucially important books: black ex-Marxist economist
Thomas Sowell’s Knowledge and Decisions, James Billington’s Fire
in the Minds of Men, conservative sociologist-historian Robert
Nisbet's History of the Idea of Progress, and George Gilder’s devas-
tating criticism of the government welfare system, Wealth and
Poverty. (Gilder was helped as a young man by David Rocketel-
ler, who financed Gilder’s Harvard education. This investment
is now paying off for the conservatives, especially with Gilder'’s
1984 book, The Spirit of Enterprise [Simon & Schuster], an anti-
bureaucracy, pro-entrepreneurship study.) 1 know of no other
comparably stupendous year of editing in conservative book-
publishing history. The cliraate of opinion is changing.

Singing a New Tune

All over the landscape these days, intellectuals are singing
the praises of decentralization. New Age mystics, former Marx-
ists, best-selling books like Alvin Toffler’s The Third Wave and
John Naisbett’s Megatrends have all joimed the chorus: a new
day is a-comin’. Out with central planning, in with local deci-
sion-making.

The manipulators are trying to join in the song fest, but it’s
like dressing a Russian dancing bear in ballet shoes and calling
it the Bolshoi’s prima ballerina. They just can’t pull it off. “De-
centralization” is our program, and has always been our side of
the debate. I think they are faking it; they still know that they
