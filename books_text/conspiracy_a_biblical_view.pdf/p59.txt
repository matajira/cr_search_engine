The Conspiracy’s Theology 43

“Intelligence and adaptability” are code words for evolution,
meaning man-directed secial, political, and economic evolution.
“Sectarian and national rivalries” are code words for religious
differences and nationalism. But Sagan is optimistic. He sees a
new world a-comin’. Some people might even call it the New
World Order.

Iv’s clear that sometime relatively soon in terms of the lifetime
of the human species people will identify with the entire planet
and the species, provided we don't destroy ourselves first. That's
the race I see us engaged in—a contest between unifying the
planet and destroying ourselves.”

  

Back in the 1950's, the slogan was: “Peace in the world, or
the world in pieces.” It is the same religious pitch: the unifica-
tion of mankind ethically and politically—the one-world or-
der—is necessary if mankind is to survive as a species. Men
must have the very similar moral, political, and economic goals.
Divisive creeds and opinions need to be educated out of people,
preferably by means of compulsory, tax-financed schools. Diver-
sity of opinion concerning these “humanistic” goals must not be
tolerated, meaning “sectarian and national rivalries.” Mankind
must not be allowed to reveal differences of opinion on funda-
mentals. Mankind’s godhead is at stake.

Now, there are three ways to achieve this unity: persuasion
(“conversion”), manipulation, and execution. The first approach
takes forever, or at least it seems to take forever. It also eats up
lots of resources. It takes teams of “missionaries.” People just
never seem to agree on these humanistic first principles. They
bicker. They battle. They refuse to be persuaded. Mankind
reveals its lack of agreement on religion and ethics. This, you
understand, must not be tolerated.

 

ews and World Rept (Oct. 21, 1985), p. 66.
