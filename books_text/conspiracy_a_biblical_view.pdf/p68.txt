52 CONSPIRACY: A BIBLICAL VIEW

a colonel when he joined Henry Kissinger’s staff in 1969. Four
years later, he was a four-star general, skipping the third star
(lieutenant general) completely. He catapulted over 240 other
general officers.’ He later served as Secretary of State under
President Reagan. Not bad for a “verbally challenged” guy!

Second, publishing. Every secret agent needs a “cover.”
Every conspiracy also needs a cover, as Weishaupt said. Aca-
demic books have for two centuries been part of the manipula-
tors’ cover. But remember North’s law of “harmless” conspira-
cies: you shouldn't judge a cover by its books. The books may be
bland and boring, but their authors are not harmless.

The Trilateral Commission

Things have changed since 1970 that have forced the manip-
ulators to become more open about part of their activities. Mil-
lions of Americans now know who they are and what they are.
It is now impossible for them to hide completely. So they are
attempting to deflect the charge of “conspiracy” by going par-
tially public. This tactic can be seen in David Rockefeller’s
defense of the Trilateral Commission in a letter to the editor in
the New York Times (August 25, 1980):

Is the commission secretive? Not at all. For $10 a year, any-
one can subscribe to its quarterly magazine, “Trialogue,” and
also receive periodic mailings of task force reports. Furthermore,
we publish a list of the source of all U.S. contributions in excess
of $5,000. The only part of our proceedings that is “off the
record” are discussions at commission meetings, and we keep
these private to encourage uninhibited criticism and debate.

 

His letter was clearly pulled out of a computerized word
processor, for this canned response contains whole paragraphs

 

7, Gary Allen, Kissinger: The Secret Side of the Secretary of State (Seal Beach, Califor-
nia; "76 Press, 1976), p. 119.
