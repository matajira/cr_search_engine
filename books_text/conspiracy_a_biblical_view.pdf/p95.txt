Maverick “Insider” Historians 79

book. By 1985, it had sold over a million copies; the first half
million came by 1973. None Dare came out in 1972.

Sales of tragedy and Hope began to take off in 1968, but
supplies of the book ran out, and Macmillan declined to reprint
it. They also destroyed the plates, according to author Quigley.
I know one man who paid $150 for a used copy, so ught was
supply, before a “pirate” edition appeared around 1975. (The
publishers agreed to pay a royalty to Quigley, so it is more of
an “unsuppressed book” than a true pirate edition.) Here is
Prof. Quigley’s account of what he alleged was the suppression
of Tragedy and Hope:

The original edition published by Macmillan in 1966 sold about
8800 copies and sales were picking up in 1968 when they “ran
out of stock," as they told me (but in 1974, when | went after
them with a lawyer, they told me that they had destroyed the
plates in 1968). They lied to me for six years, telling me that
they would re-print when they got 2000 orders, which could
never happen because they told anyone who asked that it was
out of print and would not be reprinted. They denied this until
T sent them xerox copies of such replies to libraries, at which
they told me it was a clerk's error. In other words they lied to
me but prevented me from regaining the publication rights by
doing so (on OP [out of print] rights revert to holder of copy-
right, but on OS [out of stock] they do not.) . . . Powerful influ-
ences in this country want me, or at jeast my work, suppressed."

Several years before Quigley wrote this letter, Larry Abra-
ham and Gary Allen appeared on a radio talk show where the
interviewer had scheduled Quigley to debate with them over
the phone. Quigley immediately denied that he had written the
sensational material that Abraham and Allen had attributed to
him. As soon as Abraham read one of the denied passages over

4. Letter to Peter Sutherland, December 9, 1975, reprinted in Conspiracy Digest
(Summer 1976), and reprinted again in American Opinion (April 1983), p. 29.
