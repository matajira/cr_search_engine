Appendix

MONITORING THE
ELITE’S PUBLIC FACES

If this little book has made any impression on you whatsoev-
er, it has at least raised your level of curiosity. Is the story in
this book true or not? If you have asked yourself this. question,
jet alone answered it for yourself, you should also have con-
cluded: “I need more information.”

Go right to the primary sources. Contact the several of these
organizations and begin to examine what they say publicly
about themselves. Then, if you become seriously interested, you
should take time to monitor the public activities and published
plans of one or more of them. Since they have gone to the
expense of creating a public image, we should at least pay
attention to this public side of their activities. All it takes is a
letter to them (preferably typed, single spaced, return address
in upper right-hand side) asking to receive a list of their various
materials.

Order a copy of the Annual Report of the Council on Foreign
Relations. Address your request to:

Council on Foreign Relations
The Harold Pratt House

58 East 68 Street

New York, NY 10021
