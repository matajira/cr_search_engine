THE REALITY OF CONSPIRACIES

Why de the heathen rage, and the people imagine a vain thing? The
kings of the earth set themselves, and the rulers take counsel together,
against the Lorn, and against his anointed, saying, “Let us break their
bands asunder, and cast away their cords from us.” He that sitteth in the
heavens shall laugh: the Loxn shall hold them in derision. Then shall he
speak unto them in wrath, and vex them in his sore displeasure (Psalm
2:1-5).

People from time to time ask me what I think of the conspir-
acy view of history. My answer is usually not what they expect,
whether they are pro-conspiracy theorists or anti-conspiracy
theorists.

Why do they bother to ask me? Probably because they have
spotted my weak point: a near-maniacal addiction to history
books. By training, I’m an historian. I even have a Ph.D. in the
field. This means that I had three choices when I got out of
school: go into teaching and starve, stay out of teaching and
starve, or go into business and not starve (maybe). I did the
latter, and I haven't starved (so far). But I still read a lot of
history books. I just can’t stop.

I have a theory of history. It begins with Genesis 1:1: God’s
creation of the universe out of nothing.
