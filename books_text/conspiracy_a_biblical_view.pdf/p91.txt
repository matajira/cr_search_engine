6

MAVERICK “INSIDER” HISTORIANS

And they came to Balaam, and said to him, Thus saith Balak the son
of Zippor, Let nothing, I pray thee, hinder thee from coming unto me.
For {will promote thee unio very great honour, and [ will do whatsoever
thou sayest unto me: come therefore, I pray thee, curse me this people.
And Balaam answered and said unto the servants of Balak, If Balak
would give me his house full of silver and gold, f cannot go beyond the
word of the Lord my God, to do less or more (Numbers 22:16-18).

Balaam was a prophet. He was nota man of God, but he was
a spokesman for God. He later was executed at Moses’ com-
mand (Numbers 31:7-8) for the treachery which he had shown
to Israel (Numbers 31:16). But in this earlier incident, he re-
fused to prophecy falsely against the Israelites. Balak the king
sought his counsel and his curse against Israel, but Balaam
refused to ca-operate.'

What was true of this “court” prophet is sometimes (though
rarely) true of modern court historians. Despite the overwhelm-
ing unity of professional opinion against conspiracy theories
(and theorists), from time to time a certified scholar breaks

1. Gary North, Sanctions and Dominion: An Economic Commentary on Numbers Clyler,
Texas: Institute for Christian Economics, 1996), ch. 18: “The Office of Court Proph-
et”
