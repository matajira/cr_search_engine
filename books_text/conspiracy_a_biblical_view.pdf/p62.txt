46 CONSPIRACY: A BIBLICAL VIEW

conspiracy against the people caused a civil war and had split
the kingdom permanently.

Rehoboam learned slowly. First he had conspired against the
people of Israel. Next, he rebelled against God. He took his
much-reduced kingdom into ethical war against God—the old
error of conspirators throughout history, both human and
angelic. Then Shishak of Egypt prepared to invade. The origi-
nal slave-holding tyranny threatened again. This time, Rehobo-
am repented, along with the princes (II Chronicles 12:6). God
then repented in part, but He brought Shishak in for a raid.
God gave them “some deliverance” (II Chronicles 12:7).

This was Israel’s recurring lesson, which the people never
really learned. Conspire against the law of God, and you there-
by conspire against God. God then brings in full judg-
ment: invasion, Again and again, He did this with Israel. When
the kings conspired against God, they found themselves at the
mercy of the really ruthless conspirators, the rival pagan em-
pires. The rivals always possessed greater power than the half-
hearted conspirators of Israel. The rulers of Israel dabbled in
the occult, dabbled in the power religion, dabbled in rebellion
against God, and dabbled in tyranny. They were no match for
the full-time conspirators, once God let them go.

Unification: Two Strategies

The average citizen knows about various conspiracies that
proclaim “unification through execution.” We have seen their
work in history: the Jacobins in the French Revolution, the
Bolshevik Party in the Russian Revolution, and Aryan masters
of the Nazi revolution. They achieve “consensus by ter-
ror’—endless terror, Karl Marx’s vision of “the revolution in
permanence.”' The old description is true: “The Revolution

1. Karl Marx, “Address of the Central Committee to the Communist League”
(1850), in Karl Marx and Frederick Engels, Sefected Works, 3 vols. (Moscow: Progress
Publishers, 1969), I, p, 185.
