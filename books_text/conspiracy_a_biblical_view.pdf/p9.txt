PREFACE

It hes within the power of the Allantic communities to impose peace upon
the world and secure unimpeded movement and free speech from end to
end of the earth, This is a fact on which the Open Conspiracy must
insist. The English-speaking states, France, Germany, Holland, Switzer-
land, the Scandinavian countries and Russia, given not only a not very
extravagant frankness of understanding between them, and a common
disposition lowards the idea of the Open Conspiracy, could cease to arm
against each other and still exert enough strength to impose disarmament
and a respect for human freedom in every corner of the planet. It is
fantastic pedantry to wait for all the world to accede before all the world
is pacified and policed.

H. G. Wells (1928)!

There is in the hearts of men the impulse to extend their
control over others. The quest for power is an ancient one.
When the quest for power is fused with the humanitarian quest
to do good for others, whether they want it done for them or
not, it becomes difficult to resist. And when both impulses are
joined with the ability to become fabulousty wealthy in the
process, and to pass this weaith down to one’s heirs, both bio-
logical and spiritual, the lure becomes almost irresistible. To do
well while doing good: what a marvelous opportunity!

1. H. G. Wells, The Open Conspiracy: Blue Prints for a World Revolution (Garden
City, New York: Doubleday, Doran, 1928}, pp. 189-90.
