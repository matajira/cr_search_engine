Library of Congress Cataloging-in-Publication Data

North, Gary
Conspiracy : a biblical view / Gary North. ~ 2nd ed.

Originally written as the prologue and epilogue to: Call It
Conspiracy / Larry Abraham. 1985. Then it was published as
a separate work, slighty revised in 1986. This ediuion is minus
one chapter from the original.

Includes bibliographical references and index.

ISBN 0-080462-11-4

1, Dominion theology. 2. History (Theology). 3. Conspiracies.
4. International organization.

 

Council on Fo

 

gn Relations.
6. Trilateral Commission. 7. World politics-20th century.
8. United States—Poliies and goverument-20th century.

9, Decentralization in government. I. Title

 

BYT82.25.N673 1996 96-24984
909, 82-de20 CIP

 

Published by:
Dominion Press
P.O. Box 7999
‘Tyler, TX 75711
