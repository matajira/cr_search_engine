The Conspiracy’s Theology 35

through other men, and all legitimate delegated power is there-
fore limited power.

The Old Testament required that the people of Israel be
assembled once every seven years to hear the reading of God’s
law. Everyone was required to come: residents, children, wom-
en, priests, and rulers (Deutcronomy 31:9-13). No one was
exempt. All were presumed to be able to understand the law.
Everyone would know when the provisions of God’s law were
being violated. Thus, men had reasonable expectations about
law enforcement. They could predict both the State and each
other’s actions far better, for all of them knew the public, re-
vealed law.

Absolute authority ruled from the top: God. Limited authori-
ty was delegated from God to rulers, but only by means of
revealed and fixed law.* The rulers could not legitimately
change the law, and a bottom-up system of monitoring the
rulers was established by the public reading of the law.

The U.S. Constitution, as a written document which binds
the State itself, is an indirect product of this biblical approach.
So is the common law jury system. A dozen of our peers are
presumed to be better than robed judges at deciding both the
facts and the law of the case. In any given judicial dispute, the
decision of the jury is final. There is no double jeopardy: once
declared innocent, the person cannot be retried for the same
crime. The jury system is the last major bulwark against judicial
tyranny.

Authority vs. Power
We need to understand that there is such a thing as audhority.
We must distinguish authority from power. Authority is limited
power under God. It is legitimate power because it is limited by
law and ethics. Political power must be limited if it is to remain

3. Gary North, Lewiticus: An Economic Commentary (Tyler, Texas: Institute for
Christian Economics, 1994), ch. 4,
