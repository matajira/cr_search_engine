52 CHRISTIANITY AND CIVILIZATION

rights” rhetoric inevitably removes the burden. upon the State to
obey the concrete and specific demands of Biblical law. Instead of
enforcing the Law of God, the State merely “protects” the rights of
men.

You may wonder, What, precisely, are my “rights.” You needn't
worry. Our Supreme Court will decide exactly what your “rights”
are.26

Let us examine the “Right to Life” campaign of the “Religious
Right.” Virtually all the leaders of the movement have used the
“human rights” vocabulary. Surely the most important is Francis
Schaeffer. One must necessarily approach Dr. Schaeffer with a great
deal of awe and respect for the work he has done. One approaches
any hardworking saint in much the same way, yet we should not be
afraid to go against even John Calvin where Calvin is inconsistent
with the Scriptures.

‘To begin, there is obviously nothing in Scripture to support man’s
“Right to Life.” All men have only a “right to death.” Because of
Adam (Romans 5:12) and their own sin (Rom. 3:23), all men
deserve death, not life (Rom. 6:21, 23). Every second of life on
earth is the undeserved gift of God. Our lives are protected only if
society follows the Law of God (Deut. 4:40).

But Francis Schaetfer does not hold to the abiding validity of the
Law in all its detail, and in its principled application today. Instead
of calling our nation’s leaders to repent and begin to implement the
specifics of Biblical law, Schaeffer contents himself with “exposing
our rapid yet subtle loss of human rights.”?” By substituting the
thetoric of “hurnan rights” for explicit application of Old Testament
law, Schaeffer has thrown away his only real claim to “absolutes.” As
a result, he really cannot effectively challenge our nation’s leaders.

The absence of Biblical law in Schaeffer's apologetic leaves him
with a kind of “social action deism.” God does not govern the affairs
of State; His “judicial? laws have no power in the modern world.

26. After Roe v, Wade no other conclusion than that drawn by Harold O. J. Brown
is possible: “The only human rights are those granted by the state.” Idid., p. 82, his
emphasis,

27, The subtitle of the book Schaeffer co-authored with C, Everett Koop, M.D.,
Whatever Happened to the Human Race (Old Tappan, NJ: Fleming H. Revell, Co.,
1979). If the book is supposed to be a positive setting forth of the Christian view, it
should have been entided, Whatever Happened to Biblical Law? It may be, however,
that Schaefler is merely taking an assumption of the humanists and showing that the
position cannot be logically maintained without running into severe social problems
and ethical relativism. This negative critique of the humanist use of “human rights”
is the only legitimate use of the concept.
