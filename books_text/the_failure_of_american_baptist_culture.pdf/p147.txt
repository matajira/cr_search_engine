BAPTISM, REDEMPTIVE HISTORY, AND ESGHATOLOGY 131

determine the intended meaning of the sacrament when it was first
administered.

We must insist upon the illocution of baptism being salvific grace
and blessing. This remains its primary meaning for the church, not
entrance into a neutral law-order. At the same time we must take
cognizance of the perlocution of baptism. This, in turn, does not
obscure the primacy of fasth, for by faith alone do the illocution and
the perlocution of the covenant come to coincide.

In summary, if we maintain the distinctions introduced above,
then the following propositions can be held with respect to baptism:

1. It signifies and seals entrance into the new eschatological age
of the Covenant of Grace.

2. It is intended to signify regeneration, grace, and blessing to

_ the recipient.

3, It is a powerful oath, and will exercise great significance ir-
respective of the response of the recipient.

4, The promises and grace indicated in the covenant will be
realized only through faith.

5. God’s righteous vengeance, prefigured in the Cross and the
destruction of apostate Jerusalem, will be exercised toward the
unrepenting recipient of baptism.

Baptist Eschatology: Setting The Scene

One of the most powerful and cogent arguments for paedobap-
tism and against believer’s baptism remains that framed by Calvin:

God’s command concerning circumcision of infants was either
lawful and not to be trifled with, or it was deserving of censure. If
there was in it nothing incongruous or absurd, neither can
anything absurd be found in the observance of infant baptism .5?

Ironically the baptist counter to this argument appeals to
redemptive-historical eschatology. Karl Barth revived this aspect of
the debate, arguing that the Kingdom of God inaugurated by Christ
was a universal kingdom, not restricted to geography or national
line. The seed concept of the Old covenant has no counter reference
under the New, for the universality of the Kingdom is built upon
faith. The “natural seed” becomes the “spiritual seed”—~ those who
believe in Christ. Berkouwer astutely presents Barth’s argument as
follows:

52. Institutes, 4:16:20.
