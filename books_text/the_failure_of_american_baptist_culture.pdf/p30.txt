14 CHRISTIANITY AND CIVILIZATION

ing, in the halls of justice and the halls of ivy, from humanists who
are committed to the scientific method. They still maintain that they
are not arguing from a religious foundation, but rather from a
scientific foundation. What they have not yet grasped is the point
brought home in the writings of Rushdoony, and in the books writ-
ten by the man who restructured Rushdoony’s thinking in the early
years of his ministry, Cornelius Van Ti. What Van Til has argued
for so many years is this: all positions are based on a religious foun-
dation, including science. There is never a question of religion vs.
no religion; it is always a question of which religion.

The fundamentalists have picked up the phrase, “secular
humanism,” They do not know where they found it. It comes fromm
Rushdoony’s writings throughout the 1960’s.2° Rushdoony
influenced lawyer John Whitehead, who helped popularize it in a
new widely quoted article by Whitehead and former Congressman
John Conlan.?! The fundamentalists have understood the implica-
tions of this phrase, that hurmanism is a secular religion. Never-
theless, the creationists are still battling in the name of neutral,
nonreligious science, Thus, a deep intellectual schizophrenia
plagues the fundamentalists. It is ironic that Rushdoony’s second
book, Intellectual Schizophrenta (1961), pointed to just this problem in
the field of private Christian education. (The book was itself an ex-
tension of his study of Van Til’s philosophy, By What Standard?, pub-
lished in 1959.)

What has blinded Christian thinkers for so long is their belief in
the ability of men to interpret correctly the hypothetically “brute
facts” of the creation. Men claim that they can examine the external
world and make accurate conclusions about its operations. Men
assume the following: 1) that an orderly world exists external to
men’s senses; 2) that men’s minds are essentially unified, that basic
operations of each man’s mind are the same as the operations in the

 

20, Thus, from the Civil War 1o World War JI, the goals of the state were
secularized and nationalized, The purposes of law became increasingly not the
reflection of God's justice, without respect of persons, but social justice, the triumph
of humanism. After World War II, the United States saw the steady internationaliza-
tion of its religion of humanity, and at the same time, attention finally to the deal
secularization of the states.” Rushdoony, “The Attack on Religious Liberty,” in The
Nature of the American System (Nutley, New Jersey: Craig Press, 1965), p. 52; reprinted
by Thoburn Press, Fairfax, Virginia, 1978. From 1965 on, Rushdoony's monthly
newsletter, published by the Chalcedon Foundation, attacked humanism.

21, John Whitehead and John Conlan, “The Establishment of the Religion of
Secular Humanism and Ms First Amendment Implications,” Texas Tech Law Review,
X (1978):1-66,
