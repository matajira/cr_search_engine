THE BAPTIST FAILURE 165

eclecticism of theology. He graduated from the perfectionistic
school, Oberlin College, but he remained a Presbyterian. At least,
he held to elder rule and infant baptism, but he was clearly an
Anabaptist considering his other points of view. One of those is
demonstrated in perhaps his most famous book, He That is Spiritual.
The book is important because it has become ihe text on spirituality,
It is, however, pertectionistic through and through, B. B. Warfield
was quick to take note of this in a book review which he wrote on it
shortly before his death. Warfield primarily objected to the momen-
tary sinlessness which Chafer advocated. Chafer held that confes-
sion of all known sin momentarily places man in “perfect” fellowship
with God by erasing barriers between man and his Maker. Further-
more, it was at this time that one could truly know Gud’s will.

__ In criticism of this, we must hold that indwelling sin remains
and cannot be eradicated until the resurrection. Only the penalty of
sin has been rernoved, and no matter what one does, he cannot
reach a sinless state of pure fellowship, Whether he perctives it or
not, sin is always present. Confession is important, but it is not
magical, Certainly God wants man to confess his sin, but not to man-
ipulate or penetrate the secret will of God. To think that confession
can accomplish such things implies perfectionism.

  

 

‘Today, Chafer’s book has tremendous influence in evangelical-
ism. Campus Crusade for Christ has popularized what Chafer
taught in its Holy Spirit booklet, calling it “spiritual breathing.”
Many other groups show the effect of this book in their emphasis on
introspection. If confession of sin is emphasized apart from the
objective worship of the people of God, it is individualized and
internalized. Historically, the Christian Church has practiced objec-
tive and corporate confession of sin. It helps ta prevent that kind of
subjective introspection, It should not be overlooked, however, that
Chafer’s theology is essentially Anabaptistic and has extended
Anabaptist thought. It separates the objective from the subject in
the same way Platonic thought did, and is in the final analysis an
internalized subjective theology.

Any retreat into the inner, personal, and subjective world of
man takes him away from God's righteous objective law. Even the
scripture memory of the Pharisees and the Antonians obscured the

  

Scripture itself because it was performed. in the context of bad
theology and practice. A subjective emphasis carries one away from
objectivity. Once this happens, man begins to see his sin in terms of
himself. As a result his sin disappears because of his own self decep-
tion. In other words, he thinks he is perfect, when he may in fact be

  
