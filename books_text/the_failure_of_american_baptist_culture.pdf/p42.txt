26 CHRISTIANITY AND CIVILIZATION

no man is divine,*° and no institution mediates monopolistically
between God and man. The sin of man means that God restrains
the outworkings of sinful man’s evil tendencies (Rom. 13).

There are tasks that the civil government must accomplish.
These relate to the restraint of public evil. All such evil must be
defined in terms of God’s law-order. There is no law without exclu-
sion. Certain acts must be excluded in public life. Every law-maker
knows this. Someone’s “rights” must be infringed on. God grants
men rights, but he does not grant to all men the right to perform all
conceivable acts at all times. The Bible tells us which acts are to be
prohibited from public places. It also provides principles af law that
can be extended to areas unforeseen by biblical authors (traffic con-
trol, for example). The point is: freedom of religion does not, in a
Christian commonwealth, involve total license. There is no such
thing as legitimate Christian anarchism. The civil government is
allowed to restrain public evil.

T stress the word public. The idea that the state has the right to
get inside one’s mind, or attempt to do so, is humanistic, It makes
the state into a pseudo-Ged, It also drains the resources of the state,
which means that the state must collect taxes far above the tithe, yet
the state’s taking a tithe was considered an affront to God (I Sam.
8:15). The state is not God. It, too, must be limited. It is limited,
before God, by the tithe: no state ever has the right, before God, to
collect in taxes what God collects, through the tithe, from His
people, A civil tax of 10% or more of one’s annual increase is
satanic.

Thus, the Christian view of religious liberty is “liberty with
limitations,” as every view of liberty must be. The Christian view of
religious liberty is exclusionary, as every view of liberty must be.
Mormons are not allowed to take more than one wife. This is a
distinctly religious law. They have been wise enough to drop the
doctrine of polygamy from the general public’s view, although the
doctrine is still held officially. They have beome visible defenders of
the monogamous family, which is one reason why they have grown
so rapidly, and have gained so much public support. Are they
hypocrites? Na more than any other defender of religious liberty.
They are biding their time, just as the rest of us are.

Does this mean that there is such a thing as a distinctly Christian
social order? Of course. If we do not affirm this, then how can we

 

30. Nor is the humanity of Jesus Christ divine; rather, the Person of Christ is bath
divine and human, without mixture.
