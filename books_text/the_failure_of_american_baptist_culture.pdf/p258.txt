CHRISTIANITY AND RELIGIOUS LIBERTY:
A Letter to the Reverend Kemper D. Smith*

Craig S. Bulkeley

trust that this letter finds you well and in good spirits. Presently I

am perplexed. As of late I have become increasingly dissatisfied
with a religious notion which seems to be gaining wider acceptance
every day. It is not only a religious notion, but a religious philoso-
phy which seems to me to be fraught with difficulty and bound to
cause Christians much trouble if we are taken in by it. I hope that
you worrt mind my sharing these dissatisfactions with you, since I
consider you not only a noble Christian saint, but a father in the
faith as well,

I must confess that I lay these criticisms out before you with
some trepidation, since I know that this religious philosophy is one
to which you have given some support in years past. The godly
character of your daughter and of her husband are certainly a
testimony to your ministry over the years; and I understand that
they also have a godly seed. | ask myself, “Who am I to criticize any
theological tenet to which K. D. Smith subscribes?”

But it has been a while since we last communicated, and it may
well be that your own circumstances have made for a change in your
theological perspective. If not, then I know that you will be able to
straighten out any misconceptions which J might have. In any case,
T cannot help but bring up this subject, since it seems to me bound
to bring us great trouble if we are not careful. I fear for Christ's
church and need your counsel.

Some call it “religious liberty,” others call it “liberty of cons-
cience,” I call it trouble. As I understand it, this view holds that
every man should be permitted to practice whalever religion he chooses,
according to the dictates of his own conscience. No man may tell any other

* Kemper D. Smith was the minister of Rocky Springs Baptist church in Aiken, $.C.
Craig $. Bulkeley is instructor in New ‘Testament Studies, Geneva Divinity School,
Tyler Texas.

244
