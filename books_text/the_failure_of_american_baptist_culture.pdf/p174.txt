160 CHRISTIANITY AND CIVILIZATION

mortification... , A life of spiritual liberty, in the sense of not
being found to abeciience to any other person, such as a superior,
nor to any one place was also typical. For them the pursuit of
holiness meant involvement in a spiritual warfare, not only against
one’s passions and fleshly temptations (especially regarding
chastity), and against melancholy and depression, but actually
against ‘demonic’ powers.”?"

On the other hand the Anabaptist movement entered into a
Superior-General type of monastic structure. The Pachomian com-
munities illustrate the point. Contrasting Anthony, “in the Pacho-
mian revision, chastity and poverty remain essential, but solitude is
temporarily mitigated. Instead, at the basis of the new communal
life, obedience becomes the new mode of self-renunciation. The
monk voluntarily renounces his own will in order to do that of the
superior, Consequently, the community becomes hierarchically
organized with the Superior-General at the top.”2?

Why the extreme divergence? Only subjective theology can
explain it. The covenant is defined in terms of the individual. When
the focus of attention becomes such, the traits of an individualized
theology appear. With the Antonians their subjective-individual
theology turns them to the inner and contemplative. If the move-
ment of God is fram the internal to the external, why not? By dwell-
ing on the subject and subjective, the Antonians tried to produce
holiness, Furthermore, their emphasis made them extremely inde-
pendent. If the basic unit of the covenant is singular, then plurality
becomes restrictive,

Ironically, this individuality feature explains the tyranny of the
Pachomians, The surrender of one’s will to an idisidual is a simple
jogical extension of a theology which is constructed around the
individual. Historically speaking this has been the case. These
examples that have been cited are extreme, but they serve the pur-
pose of most extremities: They point out the latent problems of a
theological system that has vacillated between the polar opposites of
anarchy and tyranny. These macrocosmic examples also point out
some interesting trends in modern evangelicalism.

One: evangelicalism, like the Antonians, views subjective aspects
such as meditation on Scripture, prayer, and renunciation as the end
of holiness. These things are not to be disparged, but they are the
means to another end, objective obedience. Jesus told His disciples

21. Davis, pp. 42-43.
22. Idem,
