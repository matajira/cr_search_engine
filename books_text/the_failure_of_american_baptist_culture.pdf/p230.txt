216 CHRISTIANITY AND CIVILIZATION

of members of the New Covenant in the sense of “general election”
being designated as covenant-breakers. Calvin sees the reality of
covenant-breaking associated with baptism in the case of the papal
church.

The same thing that the Prophet brought against the Israelites
may be also brought against the Papists; for as soon as infants are
born among them, the Lord signs them with the sacred symbol of
baptism; they are therefore in some sense the people of God. We
see, at the same time, haw gross and abominable are the supersti-
tions which prevail among them: there are none more stupid than
they are. Even the Turks and the Saracenes are wise when com-
pared with thera. How great, then, and how shameful is this
baseness, that the Papists, who boast themselves to be the people
of God, should go astray after their own mad follies: *6

Even though they have the sign of the covenant, they fail to keep
God’s Word by their superstitious practices. Not only does Calvin
see this form of covenant-breaking, but he also is keenly aware of
the reality of hypocrisy, both in the papal church and in the church
of the Reformation as well.

Since then the sacrifices were daily performed and since the
kingdom still retained its outward form, they thought that God
was, in a manner, bound to them. The same is the case at this day
with the great part of men; they presumptuously and absurdly
boast of the external forms of religion. The Papists possess the
name of a Church, with which they are extremely inflated; and
then there is a great show and pomp in their ceremonies. The
hypocrites also among us boast of Baptism, and the Lord’s Sup-
per, and the name of Reformation; while, at the same time, these
are nothing but mockeries, by which the name of God and the
whole operation of religion are profaned, when no real piety
flourishes in the heart.??

Because of the ever present danger of disobedience, one who is
in the “special election” sphere must be ever mindful of his responsi-
bility to keep the covenant. Even one who believes that he is truly
elect may stumble and prove himself to be a hypocrite. Perhaps no
passage in Calvin’s writings more graphically presents the necessity
of taking the warnings of Scripture seriously and thus repenting
from sin than his comments on Leviticus 26:40:

Whence too, it fellows, that all punishments are like spurs to
arouse the inert and hesitating to repentance, whilst the sorer

26. Hosea 4:12.
27, Micah 3:11, 12.
