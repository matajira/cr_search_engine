GALVIN’S COVENANTAL RESPONSE 191

The Lord held to this orderly plan in administering the covenant
of his mercy: as the day of full revelation approached with the
passing of time, the more he increased each day the brightness of
its manifestation. Accordingly, at the beginning when the first
promise of salvation was given to Adam it glowed like a feeble
spark. Then, as it was added to, the light grew in fullness, break-
ing forth increasingly and shedding its radiance more widely. At
jast-when all the clouds were dispersed—Christ, the Sun of
Righteousness, fully illumined the whole earth. (II. 10. 20):°

Since all of God’s people have enjoyed the same law and doctrine
albeit in different degrees of revelation and varying administration,
it follows that they have always known Christ as Mediator. Speaking
of the Old Covenant saints, Calvin says, “, . . they had and knew
Christ as Mediator, through whom they were joined to God and
were to share in his promises” (II. 10. 2). Again he asserts, “There
are two remaining points: that the Old Testament fathers (1) had
Christ as pledge of their covenant, and (2) put in him all trust of
future blessedness” (II. 10. 23).

And if the Old Covenant was blessed with Christ, it is just as cer-
tain thal they also possessed the grace af justification.!! So Calvin

argues,

For the same reason it follows that the Old Testament was
established upon the free mercy of God, and was confirmed by
Christ's intercession. For the gospel preaching, toa, declares
nothing else than that sinners are justified apart from their own
merit by God’s fatherly kindness; and the whole of it summed up
in Christ. Who, then, dares to separate the Jews from Christ,
since with them, we hear, was made the covenant of the gospel,
the sole foundation of which is Ghrist? (IT. 10. 4)

But if the grace of the covenant was equal in the Old Covenant
era to that of the New Covenant era, then the sacraments must also
have equal significance in both eras, Calvin contends that Paul held
this

10. In view of Calvin’s dear understanding of redemptive history as revealed by
this text, the charge that he failed to discern any progress in revelation cannot be
legitimately lodged against him. Gf Ryrie, ap. cit., p. 19. In fact, this emphasis on
the history of redemption led some Anabaptisés tv criticize the Reformed theology at
this point, Cf, TH. 11, 13 and note 17. The real state of the question is whether there
is progress in unity (Reformed viewpoint) or progress by discontinuity (Anabaptists,
Raptists, Dispensationalists)?

IE. Besides the use of the covenant in the context of justification in HI. 17 men-
tioned above, ¢f, the following passages in the commentaries: Gen. 7:1, Dt. 30:11,
Ps, 18:20, 19:11, Ezk. 14:14, 18:17, 20:11, Mal. 3:17, Luke 1:6, 17:10.

 
