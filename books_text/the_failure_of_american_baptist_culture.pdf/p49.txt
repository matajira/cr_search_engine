INTELLEGTUAL SCHIZOPHRENIA 33

tyranny vs. suicide), of The Doors of Perception fame (psychedelic
drugs), and of that famous family of evolutionists and atheists,
made no bones about the humanists’ motivation. He chose mean-

inglessness as his philosophy — the end result of the myth of neutrality
and relativism — for distinctly perverse reasons:

For myself as, no doubt, for most of my contemporaries, the
philosophy of meaninglessness was essentially an instrument of
liberation. The liberation we desired was simultaneously libera-
tion from a certain political system of morality. We objected to the
morality because it interfered with our sexual freedom; we ob-
jected to the political and economic system because it was unjust.
The supporters of these systems claimed that in some way they
embodied the meaning (a Christian meaning, they insisted) of the
world. There was one admirably simple method of confuting
these people and al the same time justifying ourselves in our
political and erotic revoli: we could deny that the world had any
meaning whatsoever.%9

Rushdoony’s analysis is relevant. We must not worship the state,
and it is Christianity, above all, that frees men from faith in the
state. The creed of Chalcedon, formulated in 451 a.p., is the foun-
dation of Western liberty: Christ, and He alone, serves as full
mediator between Gad and man.** Christian civilization must be
built on the implications of this creed. The state must protect Christian
civilization from rival religious wews which would deify the state. This is
why the Founding Fathers put constitutional restraints on the Fed-
eral government. Again, quoting Rushboony:

Tf man’s faith is in the state, then the state is the protector of
man’s freedor, and the author therof. Then, in every area, we
are dependent upon the state: the state giveth, and the state taketh
away: Blessed by the name of the state!

The national favorite of the United States, “America,” still cele-
brates in song an older and theocratic faith. The last stanza of
Rey. Samuel Smith’s song (1832) declares

Our father’s God, to Thee,
Author of liberty,
to Thee we sing.

39. Aldous Huxley, Ends end Means: An Inguiry into the Nature of Ideals and into the
Methods Emplayed for Their Realization (New York: Harper & Bros., 1937), p. 316.
Cited by Stanley Jaki, Cosmos and Creator (Edinburgh: Scottish Academic Press,
1980), p. 116.

34, R. J. Rushdoony, “The Council of Chalcedon: Foundation of Western
Liberty,” in Rushdoony, Foundations of Social Order: Studies in the Creeds and Councils
of the Early Church (Fairfax, Virginia; Thoburn Press, [1968] 1978), ch. 7.
