REVIEWS OF BOOKS

W. J. Rorabaugh, The Alsoholic Republic: An American Tradition, New York:
Oxford University Press, 1979 (hardcover), 1981 (paperback). xvi + 302
pages. Paperback GB-653, $5.95, Reviewed by James Michael Peters,

A quantitative approach to writing history is a relatively recent innova-
tion in the craft and William J. Rorabaugh’s lively and readable Alcoholic
Republic is a fine example of the clarity that can be brought to an historical
subject by the proper use of such a tool. In an analysis of American drinking
patterns between 1790 and 1840, Rorabaugh brings a significant amount of
concreteness to the social circumstances surrounding the emergence of the
temperance organizations of that period; organize

  

ions that, according to
Rorabaugh, quickly solidified into the Protestant institutions that came to
dominate much of the social reform throughout the 19th century.

Past-Revolutionary Spirits

The America that is described for us in The Alcohaltc Republic, is not the
America that a reader living in the late 20th century is familiar with, and it
takes a considerable mental effort to think in such terms, Rorahaugh is
quick to point out that America at this ime was physically primitive and
economically agrarian. Manufacturing was something America’s leadership
spoke of in terms of a future destiny. Overland transportation systems were
almost non-existent and the American frontier in 1800 began at the
Appalachian mountain range. The people who lived, or we might say
struggled, to the west of this natural barrier were for all practical purposes
independent of the developing federalism to the east of it. Any significant
contro] that the new government might have over the lives of its citizens
effectively stopped at the Appalachians.

Within the context of this primitive setting, the distilling of alcohol, or
spirits as they called it then, became the manufacturing base of America’s
struggling economy. Rorabaugh points cut thar the logic of such a move was

irresistable and the production of spirits almost singlehandedly moved

284
