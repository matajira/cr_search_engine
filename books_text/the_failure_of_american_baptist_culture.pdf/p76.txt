60 CHRISTIANITY AND CIVILIZATION

always makes his final stand.

The truly Biblical view, on the other hand, applies atomic power
and flame-throwers to the very presupposition of the natural man’s
ideas with respect to himself. It is assured of a point of contact in the
fact that every man is made in the image of God and has impressed
upon him the daw of God, That fact makes men always accessible to
God. That fact assures us that every man, to be a man at all, must
already be in contact with the truth. He is so much in contact with
the truth that much of his energy is spent in the vain effort to hide
this fact from himself. He efforts to hide this fact from himself are
bound to be self-frustrating.

Only by thus finding the point of contact in man’s sense of God's
deity that lies underneath his own conccption of himself as ultimate
can we be both true to Scripture and. effective in reasoning with the
natural man. Man, knowing God, refuses to keep Him in remem-
brance (Rom. 1:28).

Failure to reject the Myth of Neutrality inevitably leads to the
fourth and final myth we must avoid at all costs if we are to engage
in fruitful, Biblical Social Action.

Myth # 4: The Myth of “Practical Compromise.”

Kent Kelly, Pastor of Calvary Memorial Church in Southern
Pines, North Carolina, and extremely active in the struggle for
Christian Schools, has written a book that, were it not for its accep-
tance of many of the myths we have discussed, would be a fine
book.## The research is thorough, and the task of reducing
arguments for abortion to rubble is admirably discharged. There
have been many good books against abortion that are not written
from a Biblical perspective, and this is onc of them. Only about 3%
of the book has to do with the Bible, and Kelly is very open, about his
appeal, neé to the authority of Scripture, but to the authority of the
wuregenerate mind of man:

Most of this volume is directed toward reason for a specific pur-
pose. Pro-abortionists across the entire spectrum have little or no
regard for the Bible and its implications for the question at hand.
Our message to the nation must be commiunicated in a practical
manner or no change will be forthcoming. Approaching the world
at large with Bible in hand is an exercise in futility.4?
48. Kent Kelly, Abertion: The American Holocaust (Southern Pines, NC: Galvary
Press, 1981). Thesis: Ifyou adopt a humanist view of abortion you are on the ethical
road that leads to Naziism. An excellent critique of the Humanist position.
49. Ibid., pp. 63-64, my emphasis.
