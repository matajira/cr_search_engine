CHRISTIANITY AND RELIGIOUS LIBERTY 249

But let me save the criticism for later. Williams's Nature/Grace
dichotomy, it seems to me, made him think that the Scripture itself
was not a clear enough statement of absolute truth. We have to allow
for every religion because we can’t maintain firmly “Thus saith the
Lord.”

Now, I don’t think that Williams, his followers then, or his more
modern Baptist supporters would claim a radical independence from
the Scriptures, that their consciences were not dependent upon it.
Scripture, they thought, had established the principle of “liberty of
conscience,” especially in interpreting the Scripture for themselves.
(Strange, though, that they claimed a fallible conscience, yet each
trusted his own alone for his interpretation. You would think that
they all would have pooled their insights and opinions, at least.)

But though they claimed some authority for Scripture, I think that
they really denied it by their version of the doctrine of the right to
private interpretation. One fellow, in describing the position and its
effects, said that “a Protestant theocracy must always suffer from a
grave inner contradiction: for one significant tenet of Protestantism
is the individual’s ability to interpret the Bible free of ecclesiastical
dictates. Although particular Protestant creeds may have no inten-
tion of countenancing or permitting dissent, the Protestant stimulus
to individual interpretation must inevitably provoke that very dis-
sent.” Now I think that he has missed the boat, K. D. He has com-
pletely misunderstood the “significant tenet.” While it is true that
the individual Christian is responsible before God for his interpreta-
tion of the Bible, he is not supposed to base his interpretation on his
own private conscience. That is not the Protestant doctrine.

For the true Biblical approach, which the Protestants took I
think, the key interpretive principle is not the individual, but the
Word of God itself. The text itself is the key. “Scripture interprets it-
self,” they said, There may be cases in which one passage is not
clear, but through diligent application of one part of the Bible to the
other, the difficult passages can be understood in the light of the
clearer ones. If anything is true, it is that one should not read indo the
Scripture his own religious opinions. Scripture is clear enough in
and of itself to make it free from the need of conscience’s genius.
But, as I mentioned before, there is a paradox here in Williams's
Baptist thinking: On the one hand, the conscience is so depraved as
to prevent any one man from exercising his fallible conscience over
the conscience of any other—he is bound to misinterpret; but on the
other hand, the conscience is not so depraved as to be prevented from
being the key interpretive principle for Scriptural understanding.
