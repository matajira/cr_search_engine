CALVIN’S GOVENANTAL RESPONSE 201

considered without Christ in his narrow office (gi camm. ad Rom.
410:44f.) as lawgiver, his message was only letter and hence produced
only death. But if Moses is considered in his whole teaching, he is
seen to preach Christ as well. In that case, he must be considered as
a preacher of the gospel, the same gospel as is found in the New
Covenant.

Calvin’s explanation of this critical point of the differences
between the Covenants in the midst of the one Covenant of Grace is
even more fully explained in two other texts in his commentaries. In
Galvin’s comments on Psalm 19:8, a “question of no small
difficulty” is considered. David has been extolling the virtues of the
Jaw, but Paul later in his epistles seems to overthrow entirely the
commendations of the law which David has cited—how can these
two biblical authors be made to agree? Calvin spells the contrast out
in sharp clarity. The law restores the souls of men, yet it is only a
dead and deadly letter. It rejoices men’s hearts, yet by bringing in
the spirit of bondage (Calvin’s fourth difference between the Old
and New Covenants), it strikes men with terror. David says the law
enlightens the eyes, yet Paul says that it casts a veil before men’s
minds, and so excludes the light which ought to penetrate it. What
Calvin here indicates is that the differences between the Covenants,
presented by Paul and Jeremiah, actually contradict David's under-
standing of the “Old” Covenant if they are taken in an absolute
sense as the Anabaptists were wont to do.!8 Calvin’s answer to the

18, This is well illustrated in Pilgram Marpeck's letter translated in the Mennonite
Quarterly Restew XX XIE 198-99: “Ah, my brethren, how diligently and carefully we
have to take heed that we do not consider our own imapulse the impulse of the Holy
Spirit, our own course the course and walk of Ghrist. . . . God also uses such ser-
vants now, often as « provisional forerunner and preparer of the way for those who
are rightly driven of the Holy Spirit of Christ, that they may make the path and the
road, clear it, and weed it; they are however only servants and not friends or chil-
dren, who do not know what their master is doing nor what he has in mind, Such a
servile compulsion has taken place in our time, for quite a while now and by con-
iributing to all divisions and sects, in order that the righteous driven by the Holy
Spirit of Qhrist may become manifest.

“Luther, Zwingli, M. Hofman, G. Schwenckfeld, $, Frank, and others have been
only servants who did not know what their Lord would do.” [This last line was
printed in red for emphasis.| Just as the Old Testament saints were only servants
and not friends of God due to their not having the Spirit, so also, Marpeck says that
not only the Reformers but other Anabaptists were without the Spirit and were also
only servants while the true Anabapuists were the friends due to their possession of
the Holy Spirit. Williams points out the interesting implications of this aspect of
Marpeck's thought for his view of the civil order: “At issue with Bucer was Marpeck’s
insistence that the freedom of the gospel should never be thrust upon the whole

 
