176 CHRISTIANITY AND CIVILIZATION

tule of the Baptists, the Bible teaches a different government.
Ordination: It is a government established by God, not by the
people. The ordination of God, not the gathering of the people,
makes a church, The congregation chooses whom they would have
to rule over them (Acts 6:3), but other edders ordain their candidates
(I Timothy 4:14). The people may not ordain, symbolic of the fact
that God’s structures come from above, not below. ‘Thus, these men
become the God-ordained representatives. Where Baptist theology
has gone, representative or elder rule has disappeared. The Baptists
of Friesland and Waterland in the Netherlands pointed this out.
They “were inclined to assert the independence of the individual
congregation; they also laid a good deal of emphasis upon individual
freedom in general. This attitude was opposed to the spirit of those
groups which were administratively centralized, and which exer-
cised a strict Church discipline. From this centre the spirit of inde-
pendence spread through the whole Baptist movement, till at last it
either did away altogether with the systern of Church government
by a supreme Board of Elders, or at least, il limited their powers to
such an extent that, in the end, they becarne merely nominal,”6
Creed: Biblical government rules objectively. As suspected, the
adjudication processes of the Anabaptists have historically been sub-
jective. For one, they most often operated without creeds, at least in
the designated form. R. L. Dabney, the great Southern Presbyter-
ian theologian of the nineteenth century, pointed out the in-
escapability of creeds. He said that many documents of a church
become and function as a creed, such as translations of the Bible
(which are always in part interpretations), hymns, sunday schaol
materials, and Bible notes. All express doctrinal positions.®! Bap-
tists, however, have generally not recognized the creedal nature of
these documents. As a result they function without any designated
creed. This situation leads to a subjective approach to church disci-
pline in the area of doctrine, The following scenario often takes
place. Someone in the church, through study or influence from
some other place, begins to believe and promote a doctrine which is
not acceptable to the church, like predestination. Suddenly the
pastor and board of deacons meet with the individual to tell him that
Baptists do not believe in such a doctrine. A creedal statement is not

 

60, Troultsch, p. 706.
61. Robert L. Dabney, “The Necessicy and Value of Greeds," Memorial Volume of

the Westminster Assembly (Ricnamond: The Preshyterian Committee of Publication,
1897),

 
