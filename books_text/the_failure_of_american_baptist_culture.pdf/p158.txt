142 CHRISTIANITY AND CIVILIZATION

These resident aliens were free men. They could hire out their ser-
vices (Dt. 24:14), They were especially commended to the Israelites
for charity and compassion (Ex. 22:20; 23:9); they could glean with
the rest of the poor in Israel (Lev. 19:10; 23:22); they were under
the protection of God (Dt. 10:18). Israelites were charged to love
aliens as they loved themselves (Ley. 19:34). The aliens could share
in the poor tithe (Dt. 14:29), and in the Sabbatical year (Lev. 25:6).
They were entitled to the protection of the cities of refuge (Num.
35:15), They were entitled to the same legal rights in the courts as
Israelites (Dt, 1:16). De Vaux concludes that “in everyday life there
was no barrier between gerim and Israelites.”72 Thus, these uncir-
cumcised ones clearly partook of the blessings of the covenant. But,
they were of to partake of the Passover unless they had been circum-
cised (Ex. 12:48, 49), This singular fact serves to underscore the
igious, spiritual significance of circumcision, The case of the resi-
dent alien, however, does show that the right of partaking in general
covenant privileges was not given by circumcision. Residence in the
land established that right.

 

But if all these advantages were enjoyed by the uncircumcised as
well as the circumcised, except for the passover, we are justified in
concluding that these external, temporal blessings and privileges
were not the heart of the Old Covenant. The essence of the Cove-
nant remains ethical and religious. If any baptist disputes this and
asks, Why, then, were infants circumcised? the answer is clear—
because God commanded it so. Infants were circumcised as they are
baptized today: because Yahweh, the all-sovereign Lord of the
Covenant, exercised His divine prerogatives and declared “My
covenant is with you and pour descendants after you . . . to be a God to
you and to your descendants after you” (Gen. 17:7). Because the Lord
has so declared in covenant, we must obey with thankful, grateful
hearts,

In conclusion, we regard the baptist insistence that circumcision
had a two-fold reference ~an external and an internal reference —
and that participation in the former equally qualified one for cir-
cumcision, as @ schematization foreign to the Bible.’”? It is a reading
into the Bible what one hopes to find there. Jewett concedes as much
when he writes: “While it is true that no Scripture passage speaks of

72. Ibid, p. 75.

73. Jewett, for example, arguing from Barth, writes: *. .
right to the sign of cireumcision by virtue of their participation in the earthly bless-
ings of the covenant community: they were citizens of the nation of fsrael by birth,

. all Israelites had a

 

 
