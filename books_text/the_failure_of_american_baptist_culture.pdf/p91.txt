_ SOCIAL APOLOGETICS 75

Second, and simultaneously, we will see the full power of Biblical
preaching. James Robison has recognized the potential power of the

pulpit.

The American clergyman has one of the few channels by which
morally sound conservatives can go directly to the people without
media distortion. For example, I can speak with Americans not
only in church buildings and on crusades, but also through our
pulpit ministry on television.

In America, the Establishment media play such a role in distor-
ting the truth that we need people who will challenge the false
gods of the state’s secular “religion.” Preachers can deliver this
challenge directly to the American people, and they must. A
businessman cannot communicate his message directly to the peo-
ple; a housewife cannot make her voice heard widely. Only the
President among politicians has the ability to use the mass media
to go directly to the American people with an unfiltered message.
We preachers therefore have an extraordinary responsibility and a
unique opportunity. 53

The responsibility of the pulpit is two-fold: First, to challenge
unbelievers to forsake their supposed ultimacy as would-be-gods.

The depravity and alleged autonomy of man’s thinking prevent
the regenerate Christian from seeking common ground in the
unbeliever’s understanding of things, whether they be the laws of
logic, the facts of history, or the experience of human personality,
Rather than agreeing with the sinner’s conception of his ex-
perience, the Christian seeks his repentance—repentance in the
world of thought. Our approach should be that of Tsaiah 55:7:
“Let the wicked forsake his way, and the unrighteous man his
thoughts: and let him return unto the Lord.”**

Second, the preacher must set before believers the vision of a
Biblical society. This is an encouragement toward sanctification and
Christian maturity. The Biblical system works! The Bible really
does have the answers, and any preacher who doesn’t show these
answers, who does nat think along the lines set forth in the seminal
social action work, Institutes of Biblical Law, and does not present the
full ideal of a consistent Christian society, cannot inspire his con-

83. James Robison, interviewed by John Rees, Review of the News, Vol. 17, No.
26 (July 1, 198%), pp. 43-45.

84. Greg L, Bahnsen, Apolagetics, ch. 11. Needless to say, the Old Testament
propheis as a whole should be our guide in social action,
