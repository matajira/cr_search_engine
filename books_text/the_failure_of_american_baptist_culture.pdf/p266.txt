252 CHRISTIANITY AND CIVILIZATION

K.D., by what standard will we judge our differing consciences? The only
thing that you can be certain of is that you can’t be certain, And
because we are certain that we can’t be certain, we must allow for
anything and everything. We know for certain that Christianity is
not absolutely true; we know that all men need not be obedient to
God’s laws as they relate to their fellowmen. Our consciences are
truly liberated when they understand their bondage to uncertainty.

One thing is for certain here, and that is that when one believes
as Williams did, he ends up in great confusion with an irrelevant
Christianity.

Convinced that all men are bound to err, Williams trusted no
one. He became a “perfectionist.” I'm sure you're somewhat
familiar with the term. He was satisfied with no one’s judgments but
his own and those who agreed with him in every detail— those who
understood grace as truly as he did. He expected all or nothing.

In search of the perfect church, I am told, Williams separated
himself from the Church of England. It promoted same errant prac-
tices, and so he could be no part of it. Many others of his day, as
well, were leaving that church. But his perfectionism then forced
him to spurn the Boston church and the Puritan commonwealth of
Massachusetts because it too, despite the ocean’s distance, had not
separated itself completely from the English church. The Puritans
were not pure enough for him. He then separated himself from the
Plymouth congregation (itself known for its perfectionism and
separatism) because it had not proved as perfectionistic and
separatistic as he had at first thought it to be. One account of this
last situation J think you will find interesting, As Mr. Edmund
Morgan records it: “The cause of Williams's discontent, by his own
account, was the fact that the Plymouth church had not proved as
separatist as he first supposed it to be. When members of the church
returned on visits to England, they attended Church of England ser-
vices there, and were not cast out of the Plymouth church for doing
so. In this way the Plymouth church was communicating with the
churches of England and by implication acknowledging them to be
true churches. Williams, by remaining a member, shared in this
acknowledgment, therefore he must leave them.” He ended up tak-
ing communion only with his wife.

Williarns did have some strange ideas on the nature of the
Church. Just as the individual was to be perfect, separated from the
world into the mystical body of Christ, so the church also was to be
separated from the natural, physical realm. It was so holy and
perfect, so full of grace, that it could have no connection to the
