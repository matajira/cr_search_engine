INTELLEGTUAL SCHIZOPHRENIA 29

judge right and wrong? If there is no universal morality, then there
can be no universal politics. All legislation is exclusionary. Which
public or private acts are to be excluded? How do we know which acts
should be excluded from public life by law? By an appeal to neutral
human reason? Tradition? Or the Bible?

As fundamentalists begin to face the political implications of the
loss of faith in neutrality, they see their old political assumptions
sinking. The Marxists faced this from the beginning: Marx's savage
attacks in the Communist Manifesio (1848) on the “utopian socialists”
who believed in a universal morality made his position clear. He was
willing to impose “proletarian ethics” on everyone. Opponents
should be liquidated. Marx was a consistent thinker; once he aban-
doned the myth of neutrality, he followed one of its two logical
pathways. He became a theocrat for his god, the proletariat.

The other pathway is anarchism. No civil government is legiti-
mate because all law is legislated morality. Avoid politics. Seek to
eliminate politics by non-political means (generally revolution, but
possibly education). Let politics self-destruct. (Marx actually be-
lieved that communism would be state-less, but only after genera-
tions of revolution, liquidation of opponents, and compulsory re-
education.) This pathway has been taken by the more consistent
Anabaptists— Mennonites, Amish, and the anti-political sects—
and, at least in their actions, by generations of premillennial dispen-
sationalists. These pietists recognize where Rev. Falwell is headed,
despite his protests of “political neutrality,” and he has received
criticism about “getting involved in politics” (the fundamentalists’
criticism), which parallels the humanists’ criticism that he is “mixing
religion and politics.”

I feel sorry for those visible leaders of the New Christian Right
who have to face the savage attacks of the humanists, and who also
face the moralistic attacks of those former supporters who are re-
maining true to their anti-covenantal, anti-political presuppositions.
The radical independents are upset that men like Falwell and
Robison are challenging them with new, unfamiliar responsibil-
ities— responsibilities that are meaningful only within a Christian
framework of covenant theology. But neither Falwell nor Robison
believes in covenant theology, with its doctrine of multiple institu-
tions, including churches and civil governments, that possess law-
ful, quasi-hierarchical chains of command. Covenant theology flies
in the face of independency, and the more consistent of the inde-
pendents are not misled by the protests from the New Christian
Right’s leaders that they are not mixing politics and religion — that
