BAPTISM, REDEMPTIVE HISTORY, AND ESCHATOLOGY 115

operative only through faith. The very power of the sacrament lies in
its vivid declaration of the Gospel and the promises of God in the
covenant of grace, But the infant is oblivious to this sacrament-
sermon, and therefore beyond its efficacy—the powerful, pointed
proclamation of the Gospel and the promises of grace. Its efficacy
can only be proleptic at best.

This makes the sacrament rather dry and barren for the infant,
which it should not be. Worse, it’ makes it somewhat formal and
devoid of meaning for the church, which it should not be. After all,
the sacraments of the church should be of the deepest, most tren-
chant religious significance. Unfortunately, in the minds of many
the sacrament has become a sori of “dry run” in hopes that the real
thing will transpire later. In an attempt to retrieve the power and
significance of the sacrament administered to infants, at times the
Reformed have slid toward the Lutheran ex opere operato position.
Hence we find Walaeus, writing in 1640, contrasting the Reformed
with the Lutheran position as follows:

So they lay it down that baptism is both the ordinary means of
regeneration for children and accordingly necessary in the same
way. We too admit that the Holy Spirit ordinarily effects by bap-
tism the things sealed in baptism, Yet we deny that the action of
the Holy Spirit is always tied to the act of baptism, in a way in
which the virtue of the word is not always tied to the word of
preaching.®
In the context Waleaus is speaking of children. While the Lutherans
place the efficacy in the elements—the Holy Spirit always uses them
to work faith—Walaeus insists upon the sovereign work of the
Spirit, both are alike in that faith is virtually irrelevant. This is a
significant Reformed concession.

So there has been a long history of ambivalence and confusion
amongst the Reformed on the meaning of the sacrament. Many, for
example, have been uncomfortable with Calvin’s formulation:

T ask, what the danger is if infants be said to reecive now some
part of that grace which in a little while they shall enjoy to the full?
For if fullness of life consists in the perfect knowledge of God,
when some of them, whom death snatches away in their very first
infancy, pass over into eternal life, they are surely received to the
contemplation of God in his very presence. Therefore, if it please
him, why may the Lord not shine with a tiny spark at the present
time on those whom he will illumine in the future with the full

9. Cited in Hoppe, p. 618.
