268 GHRISTIANITY AND CIVILIZATION

the very liberty which he is trying to preserve. In this case, Harris's
religious principle of “no lords, no masters” was the logical conclu-
sion of “religious freedom.” How could anyone be a “lord” or a
“master,” or for that matter any ruler over another, when each man
is “free” to do just as his conscience dictates, and whatever seems
reasonable to him? His “conscience” came into direct conflict with
Williams’s “conscience,” if you see what I mean. The right to
religious freedom had to mean the abolition of all authority, political
and ecclesiastical. The Christian “cormnmanders” who preserved “the
common laws” and “the common peace” of the land no longer had
any meaning—because there was no longer any “commonness,”
Each man was “free” to do “that which seemed right in his own
eyes” and, well, you can guess the outcome. The “reason” was, well,
becoming irrational,

From looking at the problems of Rhode Island, K. D., I think
that the case against the “liberty of conscience’/“religious liberty” is
clear-cut. It is trouble. In the end, one conscience always ends up
competing with another conscience. It’s inevitable. The principle of
the Quakers—that the free conscience is unleashed from nature to
unite with the divine—can lead only to religious confusion with
every man hearing the voice of the spirit in his own way. And how
will we argue against the Spirit? At the same time, the anarchism of
the Harris and Hutchinson/Scott variety, with its foundation in the
natural, individual free conscience can lead only to a conflict in
which each contestant is determined to maintain his position “with
his blood.” Once each man was guaranteed the inviolable right of
religious “liberty,” founded upon the judgment of his own con-
science, there was no higher court of appeal to which each could
bring his complaint and see the conflict resolved. Dissent in this sys-
tem is inevitable. In fact, K. D., I wouldn’t be surprised if one day
some “free”-thinker with a “liberated” conscience comes up with a
philosophy that says that the only way for a man to be truly “free” is
for him to be a rebel against society; that’s the only way that he can
be sure that he hasn’t been made a slave to someone else’s con-
science, someone else’s religion!

As I see it, K. D., the most important thing in this whole subject
is not that Williams and those who thought like him had a
dichotomy between the world of nature and the world of grace.
That’s bad enough, And it’s not that they stole, or borrowed, the
moral basis for their society from God’s revealed Word and at-
tributed it to the “Reason” in man. That, too, is bad. And it’s not
