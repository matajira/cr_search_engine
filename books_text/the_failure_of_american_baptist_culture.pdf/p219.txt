GALVIN’S COVENANTAL RESPONSE 205

Calvin lists four distinct uses of the law which highlight this twofold
use of the term “law.” The first two are for instruction and condem-
nation. The second two correspond to the first two respectively as
explanations of them. Thus the third is that the law is used by the
Spirit in His regenerating work in the believer (¢f. instruction). The
fourth is an explanation of why Paul “seems” to abrogate the law (ef.
condemnation).2¢ The fourth point is once again Calvin’s under-

 

extends to thern the blessings which under the covenant were owed to the observance
of his law. I therefore admit that what the Lord has promised in his law to the
keepers of righteousness and holiness is paid to the works of believers, but in this
repayment we must always consider the reason that wins favor for these warks.”

In this same context, a further criticism of Kline’s viewpoint may be discerned.
Kline avers that “the systematic theologian possesses [p. 25] ample warrant to speak
both of ‘promise covenant’ and, in sharp distinction from that, af ‘law covenant.’
For Calvin, however, this sharp distinction does not exist once Gad begins to exer
cise his “fatherly indulgence” toward His people (gf Commentaries on the Last Four
Books of Moses, I:199-205, 214, 218, 227). See the references cited in note 11 as
well.

Nor would Calvin accept the idea thal Lhe “covenant of mercy” or Kline’s cove-
nant of promise does not include the believers oath of obedience. First, Calvin insists
that“. . . in all covenants of bis mercy the Lord requires of his servants in return
uprightness and sanctity of life, lest his goodness be mocked ar someone, puffed up
wilh empty exultation on that account, bless his own soul, walking meanwhile in the
wickedness of his own heart. Consequently, in this way he wills to keep in their duty
those admitted to the fellowship of the covenant; nonetheless the covenant is at the
outset drawn up as @ free agreement, and perpetually remains as such.” Thus when
Calvin speaks of covenant, the condition of obedience remains.

In fact, Calvin insists that the covenant of grace still demands the perfect obe-
dience of the believer. Calvin explains in IV. 13, 6, “All believers have one common
vow which, made in baptism, we confirm and, so to speak, sanction by catechism

  

and receiving the Lord’s Supper. For the sacraments are like contracts by which the
Lord gives us his mercy and from it eternal life; and we in turn promise him obe-
dience. . . . And there is no obstacle in che fact that ne one can maintain in this life
the perfect obedience to the law which God requires of us. Kor inasmuch as this stipulation is
included in the covenant of grace under which are contained both forgiveness of sins and
the spirit of sanctification, the promise which we make there is joined with a plea for
pardon and a petition for help” (italics mine). Since the “vow” or oath made in cove-
nant of grace by baptism includes the believer's obedience to the law, Calvin must be
seen to be in deep disagreement with the Kline perspective. Grace does not exempt
absolute obedience, rather it pardons imperfect obedience by the believer (“plea for
pardon”) and it enables the believer ta begin toe accornplish this goal (“petition for
help”).

20. Calvin in his Last Four Books of Moses, I11:199 says: “Further, because Paul
seems tn abrogate the Law, as if now-a-days it did not concern helievers, we must now
see how far this is the case” (italics rnine), Contrast this with what Melanchthon
says, “That part of the law called the Decalogue or the moral commandments has
been abrogated by the New Testament.” Aelanchtion and Bucer, ed. Wilhelm Pauck
(Philadelphia: The Westminster Press, 1969), p. 121. Luther, in fact states, “The
