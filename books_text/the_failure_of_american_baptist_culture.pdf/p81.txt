SOGIAL APOLOGETICS 65

we are left, with humanistic history.5? We must have a Biblical
history, yet that will, as Kelly rightly fears, offend the natural man.
But without it, we have a humanistic history that cannot win us a
thing.

We have established; from Romans 1 and elsewhere, that all men
know God. Man cannot be aware of himself without also being
aware of objects about him and without also being aware of his re-
sponsibility to manage himself and all things for the glory of God.
But man’s consciousness of himself and the created world is not
static. He is conscious of God’s handiwork in time, This means that
his consciousness of himself and of God’s creation in time results in
an awareness of history in relationship to the predestined plan of God
in back of history.59

But man suppresses the knowledge of God in unrighteousness
(Rom. 1:18). This means he distorts history. No unbeliever is an
honest historian. Bluntly, he is a liar. Never was this more evident
than in the Supreme Court decision legalizing abortion (Roe ».
Wade). The Court very definitely used history to justify the decision
on abortion, but it was not Christian history; it was “neutral”
history.

First, the Court said, “It perhaps is not generally appreciated that
the restrictive criminal abortion laws in effect in a majority of States
today are of relatively recent vintage.”® It is not cnough that a ma-
jority of the states have the laws;®! the Court wants to look at their
history.

It is certainly true that the statutes enacted against abortion were
enacted in the 1800's. 6 It might be assumed that the laws against
murder were swficient, but it is easy to understand why they were
not. The law “grows,” in applications and precedents. As men gain
experience in judging, new applications of case law are encountered
and codified. We presently have few, if any, criminal laws against
ritual cannibalism. Perhaps, if the current punk rock craze

58. For a Calvinistic treatment of history, see Gordon H. Clark, Historiography:
Secular and Religious (Nutley, NJ: The Craig Press, 1971).

59, Van Til, Defense, p. 91

60, Roe» Wade, 410 U.S, 113, 129.

61, We see already problems with Pastor Kelly’s “argument from public
consensus.”

62. And we must also admit that these laws were not strictly Biblical. They were
plagued with compromises that weakened the position (¢.g., abortion allowed when
mother's “health” was endangered), and by Aristotelian, pagan notions af the
“Quickening” of the soul, a few months into the pregnancy, as the Court rightly
notes at 133-135.
