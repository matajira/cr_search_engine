EDITOR’S INTRODUCTION

James B. Jordan

“The Failure of American Baptist Culture” might seem a puz-
zling topic for a symposium of essays, but the contention of the
editors of Christianity and Civilization is that American culture or civi-
lization has been, in the main, a Baptist modification of old catholic
and Reformed culture. The New Christian Right, in its attempts to
stem the tide of degeneracy in American life, is a Baptistic move-
ment, and this is the reason why the New Christian Right finds itself
in a condition of crisis, confusion, and indeed impotence. The thesis
the editors are setting forth, then, is that American Christianity
must return to a full-orbed Biblical and Reformed theology, and set
aside Baptistic individualism, if it is to have anything to say to
modern problems— indeed, if it is to survive.

The purpose of this introduction is to set forth, in broad strokes,
the kinds of problems that the various essays in this symposium deal
with, This introduction, then, is a kind of road map to the sym-
posium as a whole, and it is our hope that the reader will read this
introduction before turning to any of the particular essays of the
symposium itself.

Most Christians who have wrestled with the question of infant
baptism (or paedobaptism), over against professor’s baptism (the
Baptist position), have noticed that each side seemingly has strong
Biblical arguments for its case. For several centuries, theologians
and preachers have hurled Bible texts and theological arguments
back and forth, without convincing the other side. Even at this date
in history, the vast majority of Christendom holds to and practices
infant baptism (and a large segment practices paedocommunion as
well), In America, however, the Baptistic mindset has prevailed to a
very great extent, In fact, we might say that Americans are instinc-
tive Baptists. During the brief efflorescence of the Jesus Movement
a decade ago, we saw virtually no new converts who did not “feel a
need” to be baptized by immersion. An individualistic, voluntaristic
decisionism is endemic and pandemic to American culture.

These observations, we believe, point us to the true character
of the debate between Baptists and catholics (broadly speaking,
of course; catholicity is a matter of intent, and its opposite is

v
