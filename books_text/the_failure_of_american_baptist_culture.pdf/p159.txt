BAPTISM, REDEMPTIVE HISTORY, AND ESGHATOQLOGY 143

an external participation in the covenant in just these words, there is
no other interpretation that can be reasonably imposed on the data.’?*
This is an honest, if damning adrnission. We may add a more ac-
curate gloss: there is no other reasonable interpretation on
neoplatonic assumptions and with neoplatunic eschatology, but
since this is not found in the Bible, it must indved by imposed on the
data contained therein.

Natural birth never gave one the automatic right to be part of
the people of God, just as it did not give a right ta cireumcision, The
command of God established the right to circumcision. This was
administered with the illocution of the child’s being a child uf God in
truth. Faith caused the perlocution to coincide with the iHocution.

‘The prophets constantly criticized the notion that mere external
descent entitled one to the sign and seal of the covenant without
spiritual rebirth and conversion. So also Christ, who declared that
without spiritual rebirth and conversion natural descent meant that
one was a child of the devil (John 8:33ff). Accordingly, the new
birth was apparently well known under the Old Govenant, or would
have been had not Israel been so blinded in Christ’s day. How else
can we understand Jesus’ rebuke to Nicodemus to the effect that a
teacher in Israel should understand these things (John 3:10)?

The baptist construction of the Old Covenant actually turns out
to be a reconstruction along neoplatonic lines. The Old Covenant
ible,

 

  

 

clearly embraced internal, eternal, spiritual, heavenly, inv
subjective, and individual realities.

2, External Realities Under the Old Covenant.

We believe the coup de grace against the baptist position is found in
a consideration of the New Covenant. Here we shall see that the
very realities that baptists claim have passed away from the purview
of redemption are very much alive and well, as it were, in the New
Covenant.

We have seen that both paedobaptists and baptis that the
New Covenant heralds the entrance into a new eschatological order.
The one baptized is understood to have entered the “ends of the
age.” But what is this New Age like? More specifically what aspects
of human reality arc included in this realized eschaton? What

 

  

gree

 

 

 

Since, how

baptize indiscriminately in the New Testament age is either to cause disciptine in ad-

ministering the rite or to be guilty af hypocrisy in receiving it." Jafar Baptism, p. 102
74, Ibid., p. 1026,

er, this outward form af the covenant was done away with in Christ, ©
