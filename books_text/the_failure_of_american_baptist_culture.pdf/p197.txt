CALVIN’S COVENANTAL RESPONSE
TO THE ANABAPTIST VIEW OF BAPTISM

Peter A, Lillback

Introduction

LTHOUGH many baptistic theologians drink deeply from the
well of Calvin’s theology, his doctrine of infant baptism is
deemed to be at best unpalatable, at worst poisonous.! Tt is con~
sidered one of the unfortunate carryovers of Romish doctrine in the
Reformers’ thought.? Consequently, the baptists and those who hold
a baptistic view of baptism see themselves as the completion of

 

L. Several examples of Reformed or Calvinistic Baptists can be given to illustrate
their abrupt about-face in their attitude toward Calvin's theglogy when the question
of paedobaptism arises. Charles Spurgeon wrote, “If I thought it wrong to be a Bap-
ust, I should give it up and become what I believed to be right... . If we could find
infant baptism in the Word of Ged, we would adopt it. It would help us out of a great
difficulty, for ir would take away from us that reproach which is attached to us—~that
we are odd and do not as other people do. But we have looked well through the Bible
and cannot find it, and do not believe it is there; nor de we believe that others can
find infant baptism in the Scripture, uniess they themselves first put it there.”
Autobiography 1, London: Passmore and Alabaster, 1899-1900, cited by Paul K.
Jewett, Infant Baptism @& the Covenant of Grace (Grand Rapids: Wm, B. Eerdmans,
1978), p. v. G. Beasley-Murray, Bapitsm in the New Tistament (Grand Rapids: Wm,
B, Eerdmans, 1962), p, 339 writes, “. . , it is difficult to see how this view (the Re
formed view] is reconcilable with the teaching of Paul on the covenant in Galatians
3,” The greatest of the Reformed Baptist theologians John Gill wrote, “It is nat fact,
as has been asserted, that the infants of believers have, with their parents, been taken
into covenant with God in the furmer ages of the church if by it is meant the covenant
of grace; . . .” (italics mine) in A Body of Divinity (Grand Rapids: Sovereign Grace
Publishers, 1971), p. 903. Jewett describes Calvin's view as “a study in paradox” (p
99) and as a “palpable incongruity” (p. 100).

2. Again, several examples of this sentiment can be found. A. H. Strong in his
Systematic Theology (New York: A, C. Armstrong & Son, 1889), p. 538 writes, “There
is therefore no logical halting place between the Baptist and the Romanist positions.
The Roman Catholic Archbishop Hughes of New York, said well to a Presbyterian
minister: ‘We have no controversy with you, Our controversy is with the Baptists.”
Lange of Jena: ‘Would the Protestant Church fulfill and attain to its final destiny, the
baptism of infants must of necessity be abolished.’ ” William R. Estep, The

185
