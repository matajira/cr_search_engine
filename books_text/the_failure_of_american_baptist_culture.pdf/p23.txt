INTELLECTUAL SCHIZOPHRENIA 7

become a candidate for President, few people outside of Georgia
had ever heard of him. The humanists within the Trilateral Com-
mission sensed a shift in the political wind. The public was ready for
a self-professed “political outsider,” a new face totally unconnected
with Washington’s establishment. Carter’s campaign was successful.
He freely admitted that he was a “born-again Christian.” The fun-
damentalists were happy to receive attention at long last (although a
majority of white voters in the South actually voted for President
Ford), and few of thern realized that Carter's preferred theologians
were liberals: Karl Barth, Tillich, and especially Reinhold Niebuhr.
He was also appreciative of the existentialist philosopher, Sdren
Kierkegaard. It took a non-Christian, libertarian columnist Jeffrey
$t. John, to point this out in his 1976 paperback, Jimmy Carter's
Betrayal of the South. It was St. John who wrote, prior to the
November 1976 election: “A Carter victory in 1976 would usher in
an administration led by various liberal-to-lefi activist groups who
have long pleaded for vast government powers over the private sec-
tor of industry and over middle-class Americans. In short, Carter
appears to be leading a coalition of political and economic radicals
who would go far beyond the massive expansion of the powers of the
federal government Franklin Roosevelt instituted in 1933.”" His
words were prophetic. The Carter administration had not a single
self-proclaimed born-again Christian in the Cabinet, or in any other
high position. It was staffed by liberals and Trilateralists, along with
some old hands from the Council on Foreign Relations who had not
entered the inner sanctum of Trilateralism.'*

By 1980, the New Christian Right knew how little Carter's self-
identification of himself as a born-again Christian really meant. It
meant about as much as the same claim by Hustler publisher Larry
Flynt, who was “converted” by the President’s sister. The New
Christian Right spoke for far more of the fundamentalists than the
Trilateral Commission’s incumbent did in 1980. The fundamen-
talists decided to “come out of the closet” and send the Baptist from
Georgia back to Plains.

The Carter Presidency was a turning point. It made it clear to
fundamentalist voters that platitudes about being born again did nor

9, Jeffcey St. John, Jimmy Curter’s Betrayal of the South (Ottawa, Winois: Green
Hill, 1976), p. 3.

10, Antony Sutton and Patrick Wood, Trilateral Over Washington (Box 582, Scotts-
dale, Arizona: The August Gorp., 979). The Trilateral Commission is not a secret
conspiracy, but an above-board cooperative of liberal humanist thinkers and
activists. It publishes its agendas for all 1o see. Few people pay any attention.
