58 CHRISTIANITY AND CIVILIZATION

But without the light of Christianity it is as little possible for
man to have the correct view about himself and the world as it is
to have the true view about God. On account of the fact of sin
man is blind with respect to the truth wherever the truth appears,
And truth is one. Man cannot truly know himself unless he truly
knows God.*¢

This is why we cannot accept the myth of neutrality. The unbeliever
is.at war with himself and with the world around him; both reveal
God, and he is at war with God. He is not subject to the Word of
God, and cannot be, because he enters this world behaving as
though fe himself were God, deciding for himself what is right and
what is wrong. The non-Christian may ¢hink of himself as neutral:

The non-Christian thinks that his thinking process is normal. He
thinks that his mind is the final court of appeal in all matters of
knowledge. He takes himself to be the reference point for all inter-
pretation of the facts. **

But the Bible tells us that he Aates God, and cannot submit to Him.

The non-Christian seeks to suppress the truth, to distort-it into a
naturalistic scheme, to preclude the interpretation of the God who
makes things and events what they are (determining the end from
the beginning. Isa. 46:10). Specifically, and very much at the heart
of disagreements with unbelieving scholars or thinkers, we should
see that the unbeliever has an incorrect diagnosis of his situation
and of his own person.

This is, in the last analysis, the question as to what are one’s ultimate
presuppositions. When man became a sinner he made of himself in-
stead of God the ultimate or final reference point. And it is pre-
cisely this presupposition, as it controls without exception all
forms of non-Christian philosophy that must be brought into
question. If this presupposition is left unquestioned in any field all
the facts and arguments presented to the unbeliever will be made
over by him according to his pattern. The sinner has cemented
colored glasses to his eyes which he cannot remove. And all is
yellow to the jaundiced eye. *

The notion of neutrality must be challenged head-on. We cannot

40, Cometius Van Til, The Defense of the Faith (Phila, PA: Presbyterian ahd
Reformed, 1967), p. 73.

41, Greg L. Bahnsen, A Biblical Introduction to Apologetics (Fairfax, VA: Thoburn
Press, 1976), ch. 11.

42, Ibid.

48, Van Til, Defense, p, 77.
