64 CHRISTIANITY AND CIVILIZATION

why the Christian involved in social action must not be like the
humanist, battling on his grounds. Kent Kelly has said that we must
not use the Bible as our authority: “In speaking to media, in public
debate, as we write to newspapers and lobby the lawmakers, we
need not appeal to faith in the Word of God.” Then how shall we be
the “salt of the earth”? “Far better to say~ history is against abor-
tion, public conscensus (sic), science, and logic all militate against
abortion. . . .”58 Let us examine each of these and show that they
simply cannot be our battleground.

History

Pastor Kelly says that when we battle the pro-death humanists,
“We have history on our side. American history says that ‘We are
endowed by our Creator with certain inalienable rights, that among
these are . . . the right to life!”*? It is true that history is on our
side. But it is only because every event and object in history has
been predestined by the God of Scripture. Thus, all things in history
have a certain, unique meaning: the meaning that God gives them,

This does not mean, however, that we can appeal to some kind of
“neutral” history in which humanists will agree with us. All history
is Christian history. It is not neutral. All men by no means agree on
what history is or means. Christians have one view of history (the
Biblical view) and non-Christians have quite another (the humanist
view[s]). If we do not impose God’s meaning on history by speaking
in terms of the Word of God, we are not left with “neutral” history;

 

is some basis for these fears. If we tell the world we are Bible-believers, what is the
world (initially) going to think? When the world thinks of fundamentalists, or evan-
gelicals, the world thinks of incompetence, isolationism, and reactionary obscuran-
tism, And do you know what? The world has almost 100 years of theological in-
competence, isolationism, and reactionary obscurantism to back them up! In
general, ever since Christian scholarship went out with the postmillennialism of
B. B. Warfield, the Hodges, J. Gresham Machen, et al., Bible-believers have been in-
competent! Their inability to answer even “pseudo-intellectual hatchet jobs” is a sign
of this incompetence. Because most evangelicals and fundamentalists have never seri-
sus considered the Bible in all ils social implications, they feel, deep down, that the
Bible has nothing to say about politics, law, education, or anything else that requires
social action. If they would study the Bible for its political and economic implica-
tions, they would be more assured that the Bible really does “have all the answers.” If
we begin with the Scriptural assumption that the Word of God is forever, not just for
Israel, we will make progress. The Christian need not be ashamed of being a Bible-
believer, and should be able to defénd his position, not be forced to hide it.

56. Kelly, Adortion, p. 66.

57. Ibid.
