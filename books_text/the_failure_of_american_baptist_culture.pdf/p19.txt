INTELLECTUAL SQHIZOPHRENIA 3

recognize the threat that higher criticism, theological liberalism,
and pragmatism posed for Christian values. The enemy was in the
gates, and it turned out that it was not a specifically Protestant
enemy,

Meanwhile, the Protestants suffered the same shocks. The
denominational seminaries were almost universally liberal, or
Barthian, which meant that the gap between the new pastors and
their congregations widened.? The handful of conservative
seminaries were generally committed to premillennial, dispensa-
tional theology, and they were almost defenseless intellectually
against the rising tide of social and political activisrn within the neo-
evangelical carnp. The visible leadership of conservative Protestant-
ism increasingly drifted toward a mild social activism —the cast-off
political slogans of humanist hberalism—which left most laymen
uncomfortable. The popularity of The Genesis Flood, by Henry
Morris and John Whitcomb, indicated a hardening of the lines be-
tween traditional fundamentalism, with its commitment to biblical
inerrancy, and the neo-evangelical Protestant leaders, who were vir-
tually all opposed to six-day creationismm.? The Genesis Flood was pub-
lished in 1961 by the tiny Presbyterian and Reformed Publishing
Company, which was not fundamentalist in orientation, but was
just what its namme announced. R. J. Rushdoony convinced the two
authors to submit the manuscript to P&R after other Christian
publishers demanded that the authors soften their hard line against
theistic evolutionism. The traditional fundamentalist and evangeli-
cal publishers, by 1961, had long since abandoned any commitment
to “anti-intellectual” creationism. The Genesis Flood became P&R’s
first book to approach anything like best-seller status. The book cre-
ated a new market for creationist materials.

Then, in 1973, the U.S. Supreme Court legalized abortion.

 

Ph.D. in classics. He was radicalized in the mid-1960's. His book reflects his own
shift in perspective.

2. The works of Cornelius Van Til stand as the most astute theological criti
of this drift into theological liberalism. See, for example, The Case for Calvinism
(Nutley, New Jersey: Craig Press, 1964); Christionity and Barthionism (Phillipsburg,
New Jersey: Presbyterian & Reformed, 1962); The New Modernism (Presbyterian &
Reformed, 1947). See also the historical account of the National Council of
Churches by C. Gregg Singer, The Unholy Alliance (New Rochelle, New York:
Arlington House, 1975).

3. See Charles Clough, “Biblical Presuppositions and Historical Geology: A
Case Study,” The Journal of Christian Reconstruction, 1 (Summer, 1974), Walter E.
Lammeris, “The Creationist Movement in the Unted States: A Personal Account,”
ibid.

 
