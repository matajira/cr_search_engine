30 CHRISTIANITY AND CIVILIZATION

is, that they are not campaigning for a new social order based-on
Christian law, Christian covenants, and the suppression of the bu-
manist world order. The Baptists who are influential in the New
Christian Right movement are being torn apart, epistemologically
speaking, Their political conclusions lead straight into covenantal
theocracy, but their Anabaptist presuppositions lead right back into
pietisrn and ultimately into anarchism. Once a man acknowledges
that there is no neutrality, he has to confront this crucial intellectual
problern. Will it be covenant theology or Anabaptism? Will it be
theocracy or anarchism? Or will it be a life of being caught in the
middle, with humanists and independents both calling for your
scalp, and with covenant theologians standing on the sidelines,
watching you get ripped to pieces?

It is revealing that Bailey Smith, who was President of the
Southern Baptist Convention in 1980, when he spoke at the Nation-
al Affairs Briefing Conference, was not in agreement with its aims.
He complained to the humanist press after his speech that he was
not impressed by “people who worry more about missiles than mis-
sions,” and said that he had agreed to speak only because his friends
James Robison and Adrian Rogers had asked him to come. (Yet it
was Smith who made the widely quoted remark, “God does not hear
the prayer of the Jew.” I was sitting on the speakers’ platform behind
him at the time, and I thought then that it was not the thing to say.)
Smith is a more consistent representative of the tradition of Baptist in-
dependency. He could see where the New Christian Right is headed.

How can Christians avoid anarchism, avoid religtous tyranny, and.
avoid humanistic statism? We want to avoid all three. It does no good
to accept one or two of these possible results as an ideal, just to avoid
the others. While we may have to decide among them temporarily,
as we work to create a biblical alternative, we cannot accept as a
long-term goal any one of the three possibilities, Christian covenan~
talism needs an alternative. How can we achieve this?

Rushdoony has struggled with this problem, in an article called
“Religious Liberty,” which was Chalcedon Position Paper No. 12 (no
date, but sent out in mid-1980), He speaks of the co-operation be-
tween Martin Luther and the Roman Catholic Elector of Saxony,
Frederick III (“the Wise”). Frederick did not attempt to control
Luther. (Luther, however, was a philosophical dualist, who believed
in aneutral state, and who once wrote that if all citizens were Chris-
tians, there would be no need of a state.?! So this co-operation is

 

31, On Luther's dualism, see my essay, “The Economic Thought of Luther and
