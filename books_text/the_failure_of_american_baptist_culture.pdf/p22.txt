6 GHRISTIANITY AND CIVILIZATION

Churches tried to maintain some unity in 1912 by instituting a
commission on evangelism to counterbalance its well-known
social activism. By this time the balance was precarious, and the
issue of evangelism as opposed to social service was widely
debated. World War I exacerbated the growing conflict. When
fundamentalists began using their heavy artillery against liberal
theology, the Social Gospel was among the prime targets. In the
barrage against the Social Gospel it was perhaps inevitable that
the vestiges of their own progressive social attitudes would also
become casualties.*

This shift in religious perspective was complete by 1930.
Pessiraism concerning this world’s future (premillennial dispensa-
tionalism) had replaced optimism about the future, in time and on
earth (postmillennialism). Reliance on Old Testament law also
diminished still further, a decline which had begun as early as 1660
in Puritan New England, accelerated after 1676,° was institu-
tionalized politically by the Great Awakening of the 1740s,” and
died in the twentieth century, Only with the postmiilénnial and
theonomic movement of the 1965-75 period did a revival of interest
in a revamped “puritanism” re-establish the outlook of the first
generation of colonial American Puritans. But this revival was
confined alrnost entirely to the Reformed tradition, and only a small
minority of this tradition: a few Reformed Baptists and a handful of
representatives within tiny Presbyterian splinter groups. American
fundamentalism, not to mention neo-evangelicalism, was unaware
of this theological revival in 1975.8

The Carter Presidency

When Jimmy Carter was band-picked by David Rockefeller
and the Trilateral Commission (which was formed in 1973) to

5, Ibid., p. 91.

6. Gary North, “From Medieval Economics to Indecisive Pietism: Second-
Generation Preaching in New England, 1661-1690," The Journal of Christian
Reconstruction, VI (Summer, 1979).

7. Richard Bushman, From Puritan to Yankee: Character and the Social Order in Connec
teu, 2690-1765 (New York: Norton, [1967] 1970); Alan Hetmert, Religion and the
Anmericun Mind: From the Great Awakening to the Revolution (Cambridge, Massachusetts:
Harvard University Press, 1966), especially chapter 6.

8. The leading figure in this revival has been R. J. Rushdoony, whose books
pioncered the revival, especially The Institutes of Biblical Law (Nutley, New Jesey:
Craig Press, 1973). A third aspect of this revival is philosophical presupposi-
tionalism, developed primarily by Cornelius Van Til, which Rushdoony helped to
develop in his first book, By What Standard? (Presbyterian & Reformed, 1959).
