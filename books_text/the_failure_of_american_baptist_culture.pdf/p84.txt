68 CHRISTIANITY AND GIVILIZATION

made an abortion so much safer than it was decades ago, the court
saw no reason to forbid it.7° Medical science, it seems, was on their
side. The Court wrestled with the problem of when life begins,
Some say life begins at conception. But the Court said science was

not on their side:

Substantial problems for precise definition of this view are posed,
however, by new embryological data that purport to indicate that
conception is a “process” over time, rather than an event, and by
new medical techniques such as menstrual extration, the
“morning-after” pill, implantation of embryos, artificial in-
semination, and even artificial wombs.7?

We cannot appeal to the “neutral” facts of “science.”
Logie

The “Religious Right,” as it seeks the favor of the humanists for
their policies, grants to the natural man, the right and the ability to
engage in history, science, and all things else, utterly unhindered by
sin. The Fall, it seems, affected the religious dimension of man, but
nothing else. Certainly sin does not affect logic. If ever there were
anything “neutral,” logic is it. Surely we can appeal to lagic. Logic is
something that operates rightly wherever it is found. Van Til
dissents:

But the “reason” of sinful men will invariably act wrongly.

70. 410 US. 113, 148-149.

71. At (61, Of course, “science” usually boils down to public consensus. The
Court relied heavily on public consensus to determine the beginning of life:

I should be sufficient to note briefly the wide divergence of thinking on this most

sensitive and difficult question. There has always been strong support far the view

that life does not begin until live birth. This was the belief of the Stoics, It appears
to be the predominant, though not the unanimous, attitude of the Jewish faith, It
may be taken 10 represent also the position of a large segment of the Protestant

comumunity, insofar as that can be ascertained; organized groups that have taken a

formal position on the abortion issue have generally regarded abortion as a matter

for the conscience of the individual and her family. (At this point, in a footnote,
the Court cites the “Amicus Brief for the American Ethical Union et al. For the
position of the Nacional Council of Churches and of other denominations, see

Lader 99-101.")

Any Christian who recognizes the humanist character of those two groups must
surely recognize also the need for an explicit statement of the Biblical position to
counter it. The non-neutrality of the Court is surely evident here. If it were a “fact”
that 99% of all Christians vigorously opposed abortion, it would mean nothing toa
humanistic Gourt. When the Court sets out to make a judgment concerning the “will
of the people,” their hatred of God and His servants will determine who “the people”
are, The “facts” are molded and interpreted to fit the faith.
