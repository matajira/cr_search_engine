130 CHRISTIANITY AND CIVILIZATION

is precisely what circumcision-baptism entails. But from these con-
siderations Kline draws the unwarranted conclusion that the mean-
ing of circumcision is merely to ratify entrance into a law-order,
with no recognition of a predisposition for grace or blessing. This is
a lamentable watering down of. the significance of circumcision or
baptism in our opinion. The argumentation and evidences adduced
do not justify the conclusion Kline draws.

In the first place it is misleading to suggest that the self
maledictory oath in a covenant was taken or was administered
neutrally, and that it simply and only signified entrance into a law
order. It was administered with the zlention that the covenant be
kept and the proper blessing result. [t was intended for blessing,
then. The self-maledictory oath was designed to ensure that grace
and favour resulted. So with God’s covenant. Circumcision and
baptism point to and seal salvific grace and they are intended to
ratify the same. ,

Secondly, Kline allows the perlocution of the covenant, the
actual effect, to govern the intent of the sacrament. This is a com-
mon confusion in certain theological circles, as witness those who
deny that the Gospel offer can be well meant because it is a savour of
death to the non-elect. Another example comes from those who
deny that common grace exists because it results in greater condem-
nation upon those who receive its benefits but do not glorify God.
These errors come from predicating the meaning of theological con-
structions upon the perlocution of God's revelation, not upon the
illocution. Because the covenant bond does actually result in curse
for some is not to say that the curse was the illocution of the cove-
nant; the one does not follow from the other. Thus, we would argue
that Murray has allowed the illocution of the covenant (and sacra-
ment) to obscure the perlocution. Kline has allowed the perlocution
to obviate the illocution. Neither position is acceptable.

If the distinction between the illocution and perlocution of the
yacrament is maintained, the differences between Murray and Kline
are removed. When one enters the covenant one enters a law-order
sovereignly administered by Christ, The act of entrance, and the
sacrament signifying the same, whether the subject is an adult or an
infant, is intended to be a gracious aci. It presupposes or presumes
the relationship of grace and union and communion with God. But
because of the sinfulness of men’s hearts as an immediate cause, and
the secret decree of God as a final cause, some in the covenant may
never be regenerated. The actual effect of that gracious bond, then,
becomes a curse for them. This actual effect, however, does not
