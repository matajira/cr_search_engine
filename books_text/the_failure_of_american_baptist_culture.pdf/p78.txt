62 CHRISTIANITY AND CIVILIZATION

strategy is brilliant: by taking away the Bible, Satan has disarmed
the Church.

What is it that seems to be worrying the “Religious Right”? Kelly
voices his fears:

Our response will determine our ability to be heard. We need not
give them the ammunition they seek with which to shoot our
arguments. If they can relegate us to some isolated corner of the
theological world by slander and innuendo, they will do so. If, on
the other hand, we meet them on their own ground, we will get
results. This book is designed to give you ideas which defeat hu-
manists on their own battlegrounds. God says abortion is wrong.
But to lead with that statement is to play into the hands of those
who would negate sound argument by pseudo-intellectual hatchet
jobs on the source of our reasoning. Far better to say—history is
against abortion, public conscensus [sic], science, and logic all
militate against abortion... .

A Two-Fold Social Apologetic

Is the Christian position so weak and untenable that we have
nothing to say even against “pseudo-intellectual hatchet jobs”?
Proverbs 26 gives us a two-fold social action program. Verse 5 says
we are to “answer a fool according to his folly, lest he be wise in his
own conceit.” The conceit of the humanist is his claim to be able to
construct a cohesive pattern of society and of life in general apart from
the Word of God. But no knowledge, no political action, no real func-
tioning in life, is possible unless you begin by assuming the truth of
the Word of God. The humanist says he will construct a political
order based not on God’s Law, but on himself. Where will this lead?
The Christian must give a scenario “according to Ars folly.”

The Christian apologist must place himself upon the position of
his opponent assuming the correctness of his method for argu-
ment’s sake, in order to show him that on such a position the
“facts” are not facts and the “laws” are not laws, It is not as though
the Reformed apologist should not interest himself in the nature
of the non-Christian’s method. On the contrary, he should make a
critical analysis of it. He should, as it were, join his “friend” in the
use of it. But he should do so self-consciously with the purpose of
showing that its most consistent application not merely leads away
from Christian theism, but in leading away from Christian
theism, leads to destruction of reason and science as well.5?

Van Til has done much work in showing how science is impossi-

5k, Kelly, Abortion, pp. 65, 66.
52. Van Til, Defense, pp. 100, 102
