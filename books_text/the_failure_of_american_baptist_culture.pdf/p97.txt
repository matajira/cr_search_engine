THE MORAL MAJORITY: AN ANABAPTIST CRITIQUE 81

manifested through the actions of the church, not through the state,
Liberalism, however, has always insisted that statist action, not
ecclesiastical action, is the remedy, thus aligning itself with the Beast
against the Bride, This is no small matter. Mr. Webber includes
Ronald Sider among the “centrists,” but Sider is a militant statist.?
One cannot have it both ways.3

The present reviewer believes that Mr. Webber was unwise in
selecting the term “centrist” to denote prophetic Christianity, The
proper position for the Biblical prophet is not in the center between.
two extremes, but at the extreme of Godliness. An attempt to recon-
cite liberal pseudo-Christianity with conservative Christianity is
bound to fail, not only because neither side can give in, but pri-
marily because it does not attract the true Christians in either camp.
After all, conservative Christians, and Mr. Webber himself, also
sinfully pick and choose what to accept in Scripture and what to
reject, generally according to a quasi-Marcionite rule that the Old
Testament is inferior to the New and is not to be hearkened to. The
only way to develop a genuinely prophetic Christian voice, such as
Mr. Webber calls for throughout his book, is to raise high the flag of
Biblical truth and see who rallies to it.

Misreading History

A second group of criticisms one must make of Mr. Webber's
book centers on the accuracy with which he sets forth and/or inter-
prets history. Capitalism, for instance. On p. 31, Mr. Webber says,
“The history of capitalism and the free enterprise system shows how
sinful persons motivated by the desire for material wealth and power
have created an imbalance between the rich and the poor, promoted
or allowed discrimination, abused nature, and contributed to the
dehumanization of the working masses.” This is hardly the case.
The tendency of the free enterprise system, when left alone, has
always been to bring rich and poor closer to the middle. It is only
when the “haves” are given access to the power of the state to protect
their position that the imbalance between rich and poor is increased;
but such an invasion by the power of the state to assure monopolistic

2. Gf. David Chilton, Productive Christians in an Age of Guilt-Manipulators (Box 8000,
Tyler, TX: Institute for Christian Economics, 1981). Chilton’s book is a critique of
Sider from a Christian standpoint. Sider’s statist committments, however, are no
secret.

4. The best treatment of liberal “Christianity” in Amnerica is C. Gregg Singer, The
Unholy Alliance (New Rachelle, NY: Arlington House, 1975).
