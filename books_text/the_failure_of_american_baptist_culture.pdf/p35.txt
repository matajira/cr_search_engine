INTELLECTUAL SCHIZOPHRENIA 19

never fooled: they make continual references to the underlying pre-
supposition of the creationists, namely, that God did create the
universe in six days a few thousand years ago. The creationists call
“foul!” but the evolutionists keep talking about the God of the Bible.
They see far better than the creationist apoligists how intimate the
relation is between one’s view of God and one’s view of causality.

“It is a dangerous view,” the New York Times reports Dr. Wayne
A. Moyer as having said. Dr. Moyer is the executive director of the
national Association of Biology Teachers. “There is not a shred of
evidence to indicate any scientific basis for the creationist view.
They have the big truth and are trying to give it to everyone else. It
is the big lie.”2" Not a shred of evidence! Indeed, how could there be, for
it there were, God would have a potential claim on His creation
once again. So not a shred of evidence can be accepted. This is pre-
cisely how the creationist scientists must argue against the Dar-
winists, and until they do —until they grant to Darwin not a shred of
evidence for his Godless universe—they cannot hope to win their
case, Satan has no potential claim on God’s creation.

So they go into the courts and claim equal rights. They come in
the name of the discredited myth of neutrality. The government-
financed schools are legally established on just this myth. Yet school
administrators seem to be able to resist the creationists every time.
Why? Because the legal foundation is as mythical as the neutrality
myth itself, The government schools are established as a humanist
religion aimed at stamping out Christianity. This is what Rush-
doony said in his pathbreaking scholarly study, The Messianic
Character of American Education (1963), The creationists are still
schizophrenic. They do not recognize the mythical nature of the ob-
jectivity hypothesis, and therefore they have chosen to do battle in
terms of that mythical framework. They therefore have to grant to
the evolutionists, in advance, equal rights with God’s own revelation
of Himself. If they refused to do this, they would have no legal case
to get their materials into the public schools. Yet the public schools
are a fraud; they are humanist schools that have had as their goal,
since the days of Horace Mann, the express goal of wiping out
Christianity. So the judges smile inwardly, as the creationists sell
out their case—their epistemological case — to buy the mess of pot-
tage, namely, the right to get “equal time” (or any time at all) in the
socialist, humanist schools. They know better than to take their case

26. New York Times (April 7, 1980).
