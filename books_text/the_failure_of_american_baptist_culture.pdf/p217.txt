GALVIN’S COVENANTAL RESPONSE 203

yarious ages to do this, Thus in his evaluation of the citation of
Jeremiah 31:31f. found in the eighth chapter of Hebrews, Calvin
indicates that there is also the important difference of the com-
parison of the lesser to the greater. Thus Calvin asks if the Spirit’s
regeneration and Christ's forgiveness of sins were benefits enjoyed
by the Old Testament saints. These he has already called “the two
main parts in this covenant.” He affirms that they indeed had these
benefits of the covenant even in the Old Testament administration
of the covenant of grace, but to a lesser extent than the New Testa-
ment saint. Calvin points to three ways in which the New Covenant
is greater than the Old Covenant. First, he indicates that the power
of the Spirit is greater. God the Father has more fully put forth the
power of the Spirit under the kingdom of Christ. Second, He has
poured forth more abundantly his mercy on mankind, such that in
comparison to this the grace of God on the fathers is insignificant.
Third, while the promises of God with respect to salvation were
known in the Old Covenant, they were obscure and intricate in
comparison to the clarity of revelation of the New Covenant. Calvin
likens this to the light of the moon and the stars in comparison to the
clear light of the sun.

Yet Calvin is aware that this interpretation can be challenged by
the case of Abraham. In comparison to him, New Covenant
believers are lesser, and he is the greater. Calvin’s response is that
this comparison is not to be made of specific persons, but with
respect to the economical condition of the church. Thus under the
Old Covenant economy of the Covenant of Grace the fathers’
spiritual gifts were accidental to their age. They had to direct their
eyes to Christ in order to possess them. So Calvin says that the apos-
tle’s comparing of the law to the gospel as two different covenants
was taking away from the law what was peculiar to the gospel.
Nevertheless, Calvin asserts, “There is yet no reason why God
should not have extended the grace of the New Covenant to the
fathers.” This Calvin says is the “true solution of the question.”

In attempting to summarize Calvin’s viewpoint on the relation-
ship of the Old and New Covenants in light of the letter-spirit
distinction, it is helpful to keep in mind that he uses the term “New
Covenant” in two distinct senses. In the strict sense of Biblical
redemptive history, Calvin understands the New Covenant as the
gospel era brought to pass by Christ's redemptive work and His
subsequent sending of the Holy Spirit in His full apostolic
manifestation and power. But Calvin also understands the New
Covenant in a broader sense, that is, the New Govenant has always
