254 GHRISTIANITY AND CIVILIZATION

Yes, Williams was a strange one. After taking communion with
his wife alone, he revolted against any restriction and welcomed
everyone into the church regardless of profession. Having found no
one as perfect as himself, he concluded that no perfection could be
found and thar the church belonged in a different world. Grace was
completely removed from mature. His adherence to the Nature/
Grace dichotomy eventually forced him to abandon grace
altogether; his perfectionism led him to abandon the church.

But particularly annoying to me, K. D., is the low opinion
which the “religious liberty” proponents haye.of the church, despite
the accolades with which they praise it. Williams thought that the
church was to be “pure,” as he defined purity. But at the same time,
he refers to the church as though it is irrelevant. Somehow, it seems,
God would have been better off to leave the world alone, let it have
its right to do what it pleases, without the life-giving influence of the
preaching of the Word and communal prayer and praise. As far as
Williams and, it seems to me all thase with him, are concerned, the
church of Jesus Christ is completely unnecessary for social stability.
The church, according to Williams is “. . . like a corporation . . .
(which) may dissent, divide, break into schisms, yea, wholly break
up... and yet the peace of the city be not in the least measure im~-
paired or disturbed. Because the essence... of the city... is
essentially distinct. . . . The city was before and stands entire when
such a corporation . . . is thrown down,” The one social institution
through which grace is preeminently made available (through the
preaching of the Word of God) now has no necessary connection to
the properly functioning city. As I sec it, that was Cain’s attitude.
Grace is not needed for nature. Society can function well without it.
To me, K. D., this doesn’t seem like Christianity. It seems like the
religion of man apart from God. The world is “a dung heap” and the
church and the Christian message are not really needed. Men are
free to do what they want, and we don’t make any effort to yank
them from the dung. After all, our consciences might be mistaken
about the truth.

Now this brings me to that sccond misconception which I men-
tioned at the outset. The one about the reasonableness of man and
the uniformity of religion throughout the world. They go hand in
hand. I think that these are important assumptions, since without
them one couldn’t possibly entertain the idea of “religious liberty”
for everyone. In other words, you, K. D., would have to be assured
that the rest of the individuals in the world are as reasonable as you
