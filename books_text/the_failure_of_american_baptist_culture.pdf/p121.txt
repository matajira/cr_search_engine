MEDIA THEO-POP 105

most productive countries in the world. The core values of religion,
“good” and “bad,” “sinful” and “wicked,” have been redefined to
refer merely to psychic states or processes. Quebedeaux sees Robert
H. Schuller as the epitome of the modern-day evangelists of success.
God exists because He is useful, and God helps those who help
themselves. For Schuller “theology is not primarily conceptual, it is
functional and relational.” Faith, and even God Himself, have been
‘instrumentalized.” A “technology of salvation” has been devel-
oped. Salvation has become a mental technique in the “theology” of
positive thinkers like Schuller and Norman Vincent Peale. Christ
Himself is seen as the “world’s greatest possibility thinker.” “Living
with Jesus” is seen as the source of “health, friendship, and moral
support.” It is an “experience for born-again Christians that

_ motivates them to persist with the positive thinking method” (p.
90). But more than anything else, Quebedeaux goes on to say,
positive-thinking as an expression of the religion of mass culture, is
“a popular reassertion of personal self-worth in a technological soci-
ety marked by the demise of honor, widespread anonymity, and the
common feeling that ‘I don’t count any more’ ” (p.90). Human
beings are no longer “sinners in the hands of an angry God,” to
quote Jonathan Edwards, but rather are viewed as persons with an
infinite value, fully capable of achieving personal and social well-
being through a willful change of consciousness. To quote Schuller,
“Jesus never called anyone a sinner” (p. 91).

The third new value that has been introduced as an indispen-
sable part of popular religion is immediate results. In traditional
American culture, the original Protestant work ethic promised that
by thrift and industry both material success and spiritual fulfillment
were within the reach of every man. As a result, great stress was
placed on individual initiative. The traditional American hero, the
self-made man, who owed his success to God, sobriety, moderation,
self-discipline, and the avoidance of debt has been replaced with
the “Buy now, pay later” generation. Results are promised—
immediately. This general attitude behind mass culture as a whole
has quite naturally made its way into popular religion as well. “In its
rational mental technology of salvation,” writes Quebedeaux, “we
can not only be saved in a matter of minutes, we can also achieve the
fulness of ‘sanctification,’ of Christian maturity, in ‘ten easy steps’ ”
(p. 95). This relatively easy method for abundant living in the here
and now, as well as the hereafter, has led a large “clientele” of con-
sumers to find what they believe to be a sense of meaning in the
midst of the mundane, modern, workaday world drained of meaning.
