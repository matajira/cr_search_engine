A REFORMED VIEW OF FREEMASONRY 281

infiltrated Freemasonry especially on the European continent.
George Washington, a 32nd degree Mason and Master of the Alex-
andria Lodge, warned of this infiltration. All this should certainly
raise questions for the Christian, concerning Masonry.

The Islamic flavor of the Shrine is obvious. The Shriners’ places
of meeting are called temples and are given Islamic names. The
Masonic or Shriners temple in Chicago is called Medinah, and in
Savannah is known as Alec (“my God” in arabic). The Masonic
temple in Ft. Myers, Florida, is constructed to look like a Muham-
medan Mosque complete with minarets! The Shriners wear the
Islamic Fez (hat) and wear Islamic earrings. All of this is quite in
keeping with the theology of Masonry, which openly panders to
Muhammedanism.

Masonry is Antithetical to Christianity

Freemasonry is in fact a complex organization with several
“rites” and ancillary organizations. The two rites with which I am
familiar are the Scottish and the York, At the apex of the York Rite
is a select group known as the Knights Templar and I have
previously made reference to the Shriners. It is this latter that in my
opinion is the most evil branch of Masonry. The Knights Templar
present, perhaps, the best imagé of Masonry. In addition to these
there are dozens of Masonic “Colleges” and Councils such as The
Council of the Nine Muses, The Grand College of Rites, or The
Societies Rosicruciana In Civitatibus Foederatis, etc. There are also
Masonic organizations for the ladies and young people, such as
Eastern Star. However “Christian” some of the Masons may appear
to be, there is yet a great polyglot embracing of all forms of religion,
cult, and occult. Then, too, many great men have been Masons
such as George Washington, and Warren G. Harding, a Knight
Templar. My point here is this: The great thrust of Masonry does
not establish the Kingdom of Christ; it is in fact hostile to Christ.
The Scottish Rite Creed states, for example, “The cause of human

 

Jewish secret order B'nai Brrith, as Masonic. He seeks to show that B'nai B'rith asa
part of Freemasonry and a Zionist organization, is concerned with world revolution.
Another source for this material is the B’nai B’rith Magazine, September, 1940. In
addition, Albert Pike signed a meary in 1874 stating “The supreme Dogmatic Direc-
tory of Universal Freemasonry recognizes the Jewish Lodges, such as they already
exis( in the principal countries, He further establishes by this treaty, the Bai B'rith
headquarters in Hamburg Germany known as the Sovereign Patriarchal Council of
Haroburg.”

 
