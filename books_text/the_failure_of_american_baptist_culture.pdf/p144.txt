128 CHRISTIANITY AND CIVILIZATION

curse or blessing. There remains the potential of both condemnation
or justification and the sacrament signifies neither one nor the other.
It simply signifies coming under the juridical sphere. of Christ's
Lordship. He writes:

covenant is no longer identified with election and guaranteed
blessing, and especially when the baptismal sign.of incorporation
into the covenant is understood as pointing without prejudice
to a judgment ordeal with the potential of both curse and
blessing... .5°

 

Kline’s final and concluding definition is that “baptism is a sign of
incorporation, within the judicial sphere of Christ's covenant lord-
ship for a final verdict of blessing or curse.”5+

What, then, can be said as we consider these two contrasting
views? The differences appear manifest and not inconsiderable. For
Murray, the covenant is a bond of sovereign grace; for Kline it is
incorporation into a legal, judicial sphere. For Murray its intent is
salvific and gracious; for Kline its final intent is gracious, but its
mediate intent is much broader including elements of judgment and
curse. For Murray the sign of the covenant is a seal and authentica-
tion of a gracious relationship between God and his people already
established at the highest religious level; for Kline it is a self
maledictory oath indicating a neutral potential of either curse or
blessing. At a formal level the differences are quite stark as a square
of opposition shows:

Diving Administration of Administration of
sovereign grace sovereign law order
+
~ a
~ “
~ “
~ -
=“
MURRAY Se KLINE
aN
- ~
- ~
- ~
~“ ~~
Human Conditional Conditional upon a
upon obedience future ordeal of judgment

50. [bid., p. 90. Emphasis mine.
BL. Thid., p. 102.
