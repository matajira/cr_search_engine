202 CHRISTIANITY AND CIVILIZATION

dilemma is similar to what he said in Jeremiah 31. Just as the law of
Moses can be viewed with the Spirit and so be gospel, or without the
Spirit and so be the letter that kills, so also David must be seen ag
speaking not just of the moral law, but of the “whole covenant by
which God had adopted the descendants of Abraham.” Thus David
is seen by Calvin to be joining to the law—the rule of living well—
the free promises of salvation, or Christ Himself. On the other
hand, Paul must be interpreted in light of the opponents he was
dealing with, He was addressing persons who abused and perverted
the law by making it a basis of human meritorious salvation. Thus it
was Paul’s point to show that the law without the Spirit was
unprofitable and deadly to men’s souls. The law without Christ
could only be inexorable rigor which consequently curses all
mankind to wrath and the curse of God. Calvin’s conclusion is that
Paul must be seen to be rehearsing what the law can do by itself
without the promise of grace. In this capacity, the law strictly and
vigorously exacts men’s duty owed to God, which none fulfills.
David's praise of the law, however, is because he is considering the
whole doctrine of the law, which includes the gospel. Thus Calvin
concludes, “. . . under the law he comprehends Christ.” It is clear,
therefore, that Calvin does not see the law as antithetical to the
gospel since it inchides Christ. It is only so when Christ is excluded
from it as the Judaizers had done, and as was consequently con-
sidered by Paul in his refutation of their doctrine of salvation by
human. merit.

But Calvin does not simply explain the passages of Jeremiah
and Paul on the differences between the Old and New Covenants by
viewing the Old Covenant in a narrow sense without Christ and in a
normative sense in which Christ or the whole of the blessings of the
covenant are included. He is too much aware of the history of
redemption and God's distinctive administration of the covenant in

 

population, indeed that the untutored masses in so far as they were incapable of self-
discipline should remain under the yoke of the law of the Old Covenant. Marpeck
sought for the «cue, self-discipling evangelicals (the Anabaptists) the public authori-
zation of their use of at least one of the church edifices of the city,

“Constitutive for such evangelical or truly New Covenantal church living under
the gospel rather than under the law was the acknowledgment of personal sin, the
entry into the New Covenant by believers’ baptism, and the observance of the evan-
gelical law. This meant expressly the separation from the world, including the whole
sphere of the law and its /egitienate fut subchristian institutions such as the state”
(pp. 274-75), In light of this, one can also understand why the magistrates of the
Reformation period saw the Anabaptist movement as seditious and hence by
dlefinition dangerous for the state.
