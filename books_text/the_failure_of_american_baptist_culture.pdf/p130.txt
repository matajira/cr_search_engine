il4 CHRISTIANITY AND CIVILIZATION

tially and primarily soteric, then the spectre of ex opere operate is raised.
What, after all, is the efficacy of baptism? How is the thing signified
(regeneration and cleansing) to be related to the sign? Should we nat
conclude that the very elements are made efficacious through the
Holy Spirit? This was the opinion of some Lutherans and of the
Papists. The reformed rejected this, arguing there was a distinction
between the sign and what was signified. The sign of baptism
signified the covenant promise, and the reality of the promise was
made operative only through faith. According to Heidegger, baptism

seals and exhibits to those baptized the things pertaining to them
in terms of the covenant of grace. Not however as efficient by an
inherent cause, or present power, but as a seal, earnest and most sure
pledge, creating fatth tn the things recetved or to be recetved.®

Baptism, then, is a kind of visible sermon signifying certain central
aspects of the Gospel. As such it could be used to bring or seal faith,
if blessed by the Holy Ghost, but in all cases the faith would be in
the Word and the promises, not in the (baptismal) signs and seals of
the promises.

‘The Leiden Synopsis put it most clearly when it taught:

This union of sacramental sign and thing signified is not a real
and subjective conjunction... but relative, consisting in the
mutual respect in which sign places and seals the thing signified
for the believer before his eves and the thing signified is supplied and
offered by the principal cause on condition of faith and repen-
tance... . [And] our faith is both more strongly roused and
rendered active.7

Thus the efficacy lies not in the elements of baptism, but in the Word
they illustrate.* Under the Holy Spirit, baptism can be a powerful
means of conversion and faith.

An additional problematic is added, however, in the case of the
baptism of infants. If the sign is a revelation of the Word, its poten-
tial efficacy is lost or virtually nugatory for the infant, wha is
unconscious of this Word visually displayed in the sacrament. It can
hardly call to faith. This has led to an ambivalence in classic
Reformed theology. On the one hand the sacrament is essentially
and primarily a sign of regeneration, while on the other-its efficacy is

6. Thid., p. 617. Emphasis mine.

7. Ibid., p. 618, Emphasis mine.

8, See G. Berkouwer’s discussion of Calvin's doctrine of the Sacraments in his The
Sacraments, Studies in Dogmnaties, tr. Hugo Bekker, (Grand Rapids: Wm. B. Eerd-
mans, 1969), pp. 76, 77.
