SOCIAL APOLOGETICS 59

simply appeal to vague notions of “human rights” or of the creation
of man in the image of God, and let the unsaved man re-work them
according to his hatred of the things of God.*#

The unbeliever does not accept the doctrine of his creation in the
image of God. It is therefore impossible to appeal to the intellec-
tual and moral nature of man, as men themselves inierpret this nature,
and say that it must judge of the credibility and evidence of reve-
lation. For if this is done, we are virtually telling the natural man
to accept just so much and no more of Christianity as, with his
perverted concept of human nature, he cares to accept.

Van Til asks the practical question, Is there an area known by
both the unbeliever and the believer from which, as starting point,
we may go on to that which is known to believers but unknown to
unbelievers? What point of contact is there in the mind and heart of
the unbeliever to which the believer may appeal when he presents to
him the Christian view of life? 46 Is it just “the facts”? Is it “logic”?
The point of contact for the gospel must be sought within the natural
man. *? Deep down in his mind, every man knows that he is the crea-
ture of God and responsible ia God. Every man, at bottom, knows that
he is a souenant-breaker. But every man acts and talks as though this

- were nat so. It is the one point that cannot bear mentioning in his
presence. A man may have internal ‘cancer. Yet it may be the one
point he will not have one speak of in his presence. He will grant
that he is not feeling well. He may admit that he is experiencing
many terrible symptoms, He will accept any sort of medication so
long as it does not pretend to be given in answer to a cancer
diagnosis. Will 2 good doctor cater to him on this matter? Certainly
not. He will tell his patient that he has promise of life, but promise
of life on one condition, that is, of an immediate internal operation.
So it is with the sinner, He is alive, but alive as a covenant-breaker.
But his own intellectual and political activity proceeds on the
assumption that such is not the case, The “Religious Right,” by fail-
ing to appeal exclusively to that which is within man, but which is
also suppressed by every man, virtually allows the legitimacy of the
natural man’s view of himself. They do not seek to explode the last
stronghold to which the natural man always flees and where he

44, Romans 2:15 tells us that the unbeliever has the work of the law written on his
heart. Whatever policy the “Religious Right” will propose, the unbeliever will
oppose, unless God restrains his lawlessness.

45. Van Til, p. 81.

46. Ibid, p, 67.

47, What follows is adapted from Defense, pp. 94-95.
