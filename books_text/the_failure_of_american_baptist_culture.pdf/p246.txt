232 CHRISTIANITY AND CIVILIZATION

violate God’s covenant. Obedience to God requires the baptism of
one's children and in this sense baptism is necessary.

While Calvin's interpretation of the sacrament of baptism may
be distasteful to many, it must not be thought of as an irrelevant or
minute part of his theology. Baptism means covenant to Calvin, and
covenant means almost everything else! To preserve the Calvinian
system, paedobaptism is not an option but a prerequisite. It is thus
clear that Calvin’s answer to thc Anabaptist perspective on baptism
was that they failed to understand this foundational doctrine of the
covenant between God and his people and their children.
