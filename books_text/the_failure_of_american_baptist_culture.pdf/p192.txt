180 CHRISTIANITY AND CIVILIZATION

This inspiration motive is wrong. The Reformers were right. Man
is the creature. He should come to worship God by being a con-
tributor, not a spectator.

The passive spectator approach usually involves another aspect
of Anabaptist/Baptist worship. Since the preacher is the only one
who is active it puts him in a powerful position, and this sometimes
results in the mistreatment of God’s people. Once again, the effect of
a subjective theology causes the preacher to approach the people
with scepticism, When he preaches, it is as though the people are not
Christians. He scathes and lashes at them, continually reminding
them that they might not be truly saved. A Baptist minister once
said that he preached as though no one in the congregation were
saved. Why would he take such a view? It goes back to the highly
individualized theology of this movement. The Baptist approach to
salvation looks for “real” faith and that means someone must make
that kind of evaluation, on the basis of Ais own personal-individual
faith, Usually it is the preacher, and he uses his own personal-
individual experience as a gauge, and his preaching as a club to fog
out the truly saved.

Tronically, the sceptical approach to preaching is contradictory
to a Believer’s Baptism doctrine. If only true believers have been
baptized, then why should the congregation be approached as un-
believers? Because, there is no infallible way of knowing whether
one is truly converted, The basis for assurance is an objective
witness (Hebrews 6:13-18). Some subjective experience must of
necessity become the basis, however, in the Baptistic system. There-
fore, due to the variability of personal experience, one will not trust
the salvation of another with a different experience. The classic
example of a personal experience which Baptists look for is the time
of conversion. In fact the person is usually not baptized until it can be
produced. This type of error results from an over-emphasis on the
subject-individual. In the hands of an individual preacher with indi-
vidualistic theology, worship can be a brutal experience.

The overall worship of the Anabaptist system is man-centered.
It cannot be avoided in the movement of theology under analysis,
Worship, however, begins in the home. The errors therein will show
up in corporate worship, Thus Anabaptist practices in the home
must be examined.

The Home

In general, Anabaptist theology had an atomizing effect on the
