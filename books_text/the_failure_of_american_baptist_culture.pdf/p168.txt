THE BAPTIST FAILURE

Ray R. Sutton

HE historical effect of the largest “protestant” movement

might be illustrated by Francisco Goya’s famous painting
“Executions of the Third of May, 1808,.”! At a time when Napoleon
was marching triumphantly across Europe, Goya sought to portray
another side of his victory. The picture views several French soldiers
shooting some Spaniards against a wall as well as several other vic-
tims dead on the ground. His purpose —to show what Napoleon was
really doing to the people of Europe. In the same way, Baptist
history, theology, and sociology must be presented. Its underlying
presuppositions are devastating to civilization. They break down
every sphere of society. Aristotle said that to be human is to partici-
pate in the ordering of one's society. Improving on what he said, to
contribute to the dissipation of civilization is not only inhuman but
diabolical. This strong charge against the baptist schema can only
be born out by first understanding its historical roots, which flow
back to the Anabaptist movement of the sixteenth century.

History of the Anabaptists

After the Reformation, Anabaptism emerged as a distinct social
structure. Its characteristics were life apart from the “world,” a
voluntary basis of membership, re-baptism (ana-baptism) of adults,
the rejection of infant baptism, and a “pure” church consisting of
the “truly” converted who desire a “holy community” separated
from the world.? With these distinctives they faced the frightening
task of being called upon to create new worlds.? In their case, how-

1. William Fleming, Arts & Ideas (New York: Holt, Rinehart and Winston, 1974),
p. 301.

2. Eenst Troeltsch, The Social Teaching of the Christian Churches, tr. by Olive Wyon
(New York: Harper & Row [in two volumes], 1911] 1960), p. 695.

3. Max L. Stackhouse, Ethics and the Urban Ethos (Boston: Beacon Press, 1972),
p. 2

152
