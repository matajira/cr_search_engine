204 CHRISTIANITY AND CIVILIZATION

been the saving relationship between God and His elect throughout
the ages. It either laoked forward in promise to Clrist’s coming or it
harks back te His accomplishment of redemption, While this view-
point is distinctively a mark of Calvinism, it is not unimportant to
realize that Calvin was fully conscious of his indebtedness to
Augustine at this point. Referring to Augustine, he writes,

In the same passage he very aptly adds the following: the children
of the promise, reborn of Gad, who have obeyed the commands
by faith working through love have belonged to the New Cove-
nant since the world began. This they did, not in hope of carnal,
earthly, and temporal things, but in hope of spiritual, heavenly,
and eternal benefits. For they believed especially in the Mediator,
and they did not doubt that through him the Spirit was given to
them that they might do good, and that they were pardoned
whenever they sinned, It is that very point which I intended to
affirm: all the saints whom Scripture mentions as being peculiarly
chosen of God from the beginning of the world have shared with
us the same blessing unto eternal salvation. (If. 11. 10)

Calvin in full agreement with Augustine understands that the New
Covenant has always been the place of salvation, So Calvin must be
read with care with respect to which of the two meanings of the New
Covenant he is ‘employing.

It is also true that Calvin has a twofold use of the term “law.” [t
can be used either in the strict sense of the Pauline usage to combat
selfcongratulatory works of human merit, or in the broad sense of
the rule of living well which is coupled with the Spirit’s enablement
and Christ’s forgiveness. In the first sense, there is a very profound
difference between Jaw and gospel. In the second, however, there is
no longer any difference between the law and gospel since the Spirit
has been added to the law and Christ’s forgiveness as well. Calvin
states this with succinctness in his comments on Dt. 30:11 where he
argues that law and gospel are one by the New Covenant. !9 Under
the heading of “The Use of the Law” in the same commentary,

19. Calvin comments on Dr, 30:11: “But this is the peculiar blessing of the new
covenant, that the Law is weltven on men’s hearts, and engraven on their inward
parts; whilst that severe requirement is relaxed so that the vices under which
believers still labour are no obstacie to their partial and imperfect obedience being
pleasant in Gad.” In light of this doctrine, Calvin can ga an to say that believers
actually receive the promises of the law, that is the reward promised to obedience,
because of their being viewed in light of the promises of che gospel. Galvin writes in
UI, 17. 3: “But when the promises of the gospel are substituted, which proclaim the
free forgiveness of sins, these not only make us acceptable to God but also render our
works pleasing to him, And not only does the Lord adjudge them pleasing; he alsa
