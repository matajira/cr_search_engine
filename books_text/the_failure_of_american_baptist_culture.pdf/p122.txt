106 CHRISTIANITY AND CIVILIZATION

In the third section of his book, Quebedeaux discusses religious
authority, particularly modern religious authority. Traditional
religious authority, the authority of Christ’s Church and His office
bearers, has almost become extinct in popular religion. The reason
for this, Quebedeaux points out, is two-fold, “When the church, the
clergy, and theology itself-once the queen of the sciences~ lost
their high status in modern culture as a whole, they also lost a large
measure of their authority within the institutions and movements of
religion themselves. The erosion of clerical status and the authority
once inherent in it was the direct result of the rise of advanced
scientific discovery and the ever expanding growth of pluralism
over the same period of time, in both Europe and America” (p.
103).

As a result of this decline in traditional authority, the very con-
cept of authority has been redefined to meet the peculiar needs of
modern America. Authority and power have come to be identified
with influence rather than with the legal right to command and
cause others to act upon those commands. “In this kind of pluralistic
and egalitarian society, shaped by rationality and scientific analysis,
the ideal of leadership has been democratized to such a degrce that
‘extraordinary’ qualities, however much needed, are not normally
reckoned with in the framework of social planning. Authority itself
is questioned, and its scope severely limited, while power, expressed
in ‘influence’ is gained more often than not by indirect and unstruc-
tured means. Thus all authority in modern America—inclading
religious authority ~ must be evaluated in this context” (p. 110).

Quebedeaux goes on to describe the way that this modern
religious authority works itself out in the various “ministries.” First
ofall, the influence is strictly tied to the effectiveness of the organiza-
tional structure (e.g., The Billy Graham Evangelistic Association).
Directly proportional to the number of followers or fans that an
evangelist has, is the complexity and size of the organization that he
must also have to transmit the message. This has created a tension
within some organizations between the “visionary leader” and his
down-to-earth bureaucrats. The organization must do more than
carry on the will of its founder. It must also perpetuate itself, and
does so by controlling the public image of its head. Eccentricities
that are common in the early careers of many leaders (e.g., Oral
Robert's miracle healings) are carefully culled to present the “spit
and polish” image that is necessary to make it to the big-time.

The second variable in the amount of influence that a particular
leader or group has is the form of communication that they and their
