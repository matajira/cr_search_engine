CHRISTIANITY AND RELIGIOUS LIBERTY 269

that the idea of “liberty of conscience”/“religious liberty” ultimately
Jeads to the very dissent it attempts to avoid. These things are
enough for God to condemn it. But the biggest dissatisfaction I have
with this idea is that it is grounded on the idea that this can be a
“neutral” world in which Jesus Christ is a dispensable part.

This is not a neutral world, K. D. You know that. God has not
called us to bide our time and allow the life-giving gospel of Christ
to be slapped down at every point, treated as nothing more than a
shot of morphene for socially maladjusted religious fanatics. God
has put us here to be salt and light in the world; to be like a city set
on a hill; to let our good works shine before men so that they will
glorify our Father who is in heaven. And if we are truly responsible
citizens of Christ’s kingdom, then we will seek to promote His law
before all men, Our social well-being depends on it. The safety of
our families depends upon it. If we're not for Christ, then we are
against him; at least that is what He said. And for my money, I
think He cares about the society in which we live, politics and all.
No area of life is unimportant to Him. And as the Proverb says,
“Those who hate Me love death.”” Whenever the life-giving Word of
the gospel goes out, there will inevitably be those who hate God and
promote an alternative, destructive form of life, which is really only
death—and all along it will masquerade as life-giving under the
banner of “liberty.” That liberty is not Christianity; it is humanism,
Any commitment to neutrality is mot in accordance with God’s
Word. He wants men to be obedient to Him. When they are not,
they are the ones that make the war. Not the Christians. But they will
turn the argument around and say that we are the oppressive ones.
And all we want to do is be faithful sons of God, obedient to His law
in Christ.

That's what bothers me about Roger Williams and this
“religious liberty”/“liberty of conscience” idea. It does seem strange
to you, doesn’t it K. D., that a Christian man would promote a
religious philosophy and social system in which Christianity is
dispensable? The Great Commission is clear; the Christian’s chief
task is teaching the commands of Christ to an ignorant and often re-
bellious world—not voiding His commands in the face of a
“higher,” “more reasonable” religious system in which wickedness
triumphs while the light of God’s Word is hidden.

From my reading of the Bible, hardship and warfare are to be
expected; as Jesus said: “Do not think that I came to bring peace on
the earth; I did not come to bring peace, but a sword . . . he who
does not take his cross and follow after Me is not worthy of Me. He
