EDITOR'S INTRODUCTION xv

tions a study by the Christian Reformed Church in North America,
which 1 personally regard as the best long analysis of Freemasonry
available. Write to the CRC in NA, 2850 Kalamazoo Ave., Grand
Rapids, MI 49560, and ask for the Acts of Synod 1974. Send $5.00
plus §1.50 for postage.)

The symposium concludes with two book reviews. James
Michael Peter’s review of Rorabaugh’s The Alcoholic Republic
touches on one of the pseudo-moral issues common in Baptist cul-
ture. The Bible praises the joy-inducing effects of the fruit of the
vine, though it warns against drunkenness. The peculiar antipathy
to all aleohol, which has led to the wicked substitution of grape juice
(and soda-pop, and coffee, etc.) for communion wine, has its origins
in the lawlessness of the American frontier. Mr. Peters’s review is an
introduction to this fascinating topic.

Finally, Ray Sutton reviews Kenneth Davis's Anabaptism and
Asceticism, pointing out that Mr. Davis's work is probably the finest
introduction to the study of the non-Christian influences in carly
Baptistic theology.

Appendix

Just as this issue of Christianity and Civilization was going to press,
we received a copy of Willem Balke’s Caluin and the Anabaptist Radicals
(trans. by William J. Heynen; Grand Rapids: Eerdmans, 1981).
This excellent study is a history of the relationship between Calvin
and the various Anabaptist personalities and groups of his day, It is
very readable, and we cannot recommend it too highly. Anyone in-
terested in further pursuit of the kinds of things discussed in the
essays by Rev. Suiton and Mr. Lillback, should obtain this book.
