THE INTELLECTUAL SCHIZOPHRENIA OF THE
NEW CHRISTIAN RIGHT

Gary North

And Elijah came unio all the people, and said, How long halt ye be-
tween two opinions? If the LORD be God, follow him: but if Baal, then
follow him, And the people answered not a word (I Kings 18:21).

He that is nat with me is against me; and he that gathereth not with
me scatiereth abroad (Matthew 12:30).

N_ August of 1980, the Religious Roundtable sponsored the

National Affairs Briefing Conference in Dallas, Texas. The
Religious Roundtable is a non-profit organization with headquar-
ters in Washington, D.C. It serves as a kind of clearing house for the
New Christian Right, with many of its board members selected
from the leadership of the movement known to the press as the
“moral majority.” The Briefing Conference was specifically designed
to bring thousands of American fundamentalists into the American
political mainstream.

Some 15,000 people assembled in a large arena to hear dozens of
the nation’s religious conservatives tell them about the Equal Rights
Amendment (ERA), abortion, inflation, the nature of political
organization, taxes, and the political responsibilities of Christian
laymen. Over 2,000 pastors attended. Leaders of the New Christian
Right asked them to take the message of political involvement back
to their congregations,

The highlight of the conference was a speech by Presidential
candidate Ronald Reagan. Both Jimmy Carter and third-party
candidate John Anderson had been invited. Both of these men
claimed to be Bible-believing Christians early in their careers
(although neither was using this label extensively in the 1980 elec-
tion), yet neither accepted the invitation. The final meeting was, in
effect, a kind of political rally for Ronald Reagan, despite the fact
that the sponsoring organization could not take a partisan political
stand. Perhaps the other two candidates knew it would be a pro-

1
