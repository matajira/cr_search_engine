SOGIAL APOLOGETICS 43

areality until the church is awakened and begins to apply the Law
in every area of life, including (relevant to our subject) the civil
magistrate. ® In both the Old and New Testaments, then, we have a
complete social program that must be implemented, and will be
implemented.

But any program of social action that seeks to impose a Christian
political order on a non-Christian population is doomed to failure.
No law-order can be imposed, for the simple reason that al/ political
systems are a reflection of the faith of the people. If the people are slaves,
you will have a slave-State. If the people are responsible and mature
Ghristians, then—and only then—will you have an appropriately
Biblical political system. Without doubt, then, education and
evangelism are necessary components of Biblical social action. R. J.
Rushdaony, who has been accused of overemphasizing politics and
minimizing evangelism,’ has made these observations:

The only true (social) order is founded on Biblical Law. All law
is religious in nature, and every non-Biblical law-order represents
an anti-Christian religion. But the key to remedying the situation
is no revolution, nor any kind of resistance that works to subvert
jaw and order. The key is regeneration, propagation of the gospel,
and the conversion of men and nations to God’s Law-Word.?

Eyil men cannot produce a good society. The key to social
renewal is individual regeneration.? :

In terms of God's Law, true reform begins with regeneration
and then the submission of the believer to the whole Law-Word of
God,1°

If not enough regenerate men exist in a society, no law-order
can be maintained successfully. Thus, a healthy society needs an
operative law-order and an operative evangelism in order to
maintain its health, 11

 

“Christecracy” under Jesus Christ is described in Isa. 11:4 and Rev. 19 (esp. vs. 15).
When the saints rule according to the whole Law of God (Ps. 149:4-9) then God gov-
erns that nation, and Ghrist the King is here with us (Matt. 28:20), A theocracy is
not be equated with an ecclestocragy, the political rule of the clergy.

6. The basic text of Christian Reconstruction is R. J. Rushdoony’s Institutes of
Biblical Law, (Vol. 1, the Craig Press; Vol. Il, Ross House Books). Here the
substance of the whole law of God is applied to contemporary social problems. H'you
have not read and digested Jnstitules, you are not prepaced to be invalved in Biblical
social action.

7. Chiefly by those who have never fully read che Institutes

8. Institutes, Vol, I, p. 113,

9. Ibid, p. 122.

10. Ibid., p. 627.
il, fbid., p. 321.
