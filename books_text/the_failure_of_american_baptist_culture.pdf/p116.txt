100 CHRISTIANITY AND CIVILIZATION

“pulpiteers” and the revival meeting. Finally, Quebedeaux attempts
to define the nature of modern religious authority as exercised in
popular religion through the mass media, its problerns, and some
suggested cures for its weaknesses.

In the 1976 global survey of religious attitudes by the Gallup
organization, the United States was shown to stand at the top of
industrial societies in the importance religion plays in the lives of its
citizens. “The high incidence of Americans,” writes Quebedeaux,
“who profess a belief in God and in life after death, who attend
church or synagogue regularly, and who affirm confidence in
organized religion can surely be taken as one kind of evidence that
religion is important~and popular~in American life” (p.2). He
goes on to say that popular religion as a concept can no longer be
identified with the institutional church, its faith, teaching, or work.
Nor can it be completely explained in terms of the high percentage
of believers in God and in immortality. Popular religion is that dom-
inant brand of religion that is carried and shaped by the mass
media. It is always an integral part of mass or popular culture as a
whole. Indeed, the institutional church and the beliefs of its
members are themselves affected by popular religion as transmitted
to them by the mass media, because popular religion merely
confirms and strengthens the values the viewing, listening, and
reading public already hold dear.

Now, in order to understand popular religion, Quebedeaux
believes that one must first come to grips with che modern phenome-
non of mass, media-oriented culture. In the 20th century, the rise of
modern technology has given birth to an entirely new market: the
leisure time of the masses. Modern life falls into two very distinct
categories: work (the means) and play (the end). ‘he primary func-
tion of mass culture is to relieve the boredom inherent in affluence
and surplus leisure time. Because of this, popular culture ‘is
manufactured by a group (Hollywood or Madison Avenue) for sale
to an anonymous mass market. It is their job to supply this new
mass mnarket with products and entertainment suited to its desires.
The products offered must be bland enough to satisfy the “average
taste,” easily accessible, and inexpensive. Despite what some fun-
damentalists may believe, the creators of popular culture, and of
popular religion for that matter, are not some conspiratorial group
of atheistic socialists cramming “their” morality down the throats of
unsuspecting consumers. Their business is retailing products and
entertainment with sales in mind. In other words, they give the peo-
ple what they want.
