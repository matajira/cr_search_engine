50 CHRISTIANITY AND CIVILIZATION

“Religious Right” has no intention of bringing this nation’s laws
captive to the obedience of Jesus Christ (IT Cor, 10:5). They are ap-
parently not committed to the Great Commission of our Lord, to
make al! nations Christian by teaching them to observe whatsoever
Jesus Christ commanded (Matt. 28;18-20). Biblical social activists
must command all men everywhere, even in Washington, D.C., to
repent of their political disobedience, and turn to the Word of God
for social guidance, submitting to the absolute authority of the Bible
in the realm of politics.

 

Myth #2: The Myth of Human Rights: The “Religious Right”
Will not Challenge Man’s Claim to Autonomy.

Like the Myth of Pluralism, the Myth of autonomous human
rights is rooted in a denial of God and His Law. The idea of “rights”
is the outgrowth of theories of government from Enlightenment
humanism. T. R. Ingram has countered the notion that men, sim-
ply because they are men, have certain “inalienable rights.”
“Human rights”

. arc in fact a kind of back-handed statement of benefits of the
common law which Christians enjoy. Common law rights are
blessings of public peace in which wrongs are punished. Wrongs
are not seen as infrmgements of individual rights, but as viola-
tions of God’s commands. It is wrong to murder, not because each
has a right to live, but because God said it is wrong. . . .*? It is
easy to understand why the human rights idea came into
popularity in Christendom, It is sirply that men living in
Christendom enjoyed that “blessed liberty wherewith Christ hath
made us free” to such an extent and over so many centuries that
they found it easy to take for granted. Liberty, instead of being
recognized as the gift of Christ and the reward of Christian
justice, was something that would easily be seen as an end in
itself, Tt was easy to confuse logical origins. The common law
punishes any dishonest violation of each man’s person or his goods

 

from? On what basis de we restrict our cannibal “friend” above? Isn’t this the im-
position of a moral system? How can we cali this pluralism? Much can and should be
done by way of exposing the contradictions and inconsistencies of the humanists.
Use their premises against them,

22, Not that God could ever legislate “arbitrarily,” i.e., against His character.
The point is that God, not man, has legislated. God surely commands us to respect
our fellow man because he reflects the image of God (James 3:9). God also com-
mands capital punishment for the same reason (Gen. 9:5-6), But understanding wby
God was commanded is not license for man to legislate. God is our Law-giver (Isa.
35:22), We as‘men cannot pick a doctrine (c.g., man's dignity) and attempr to con-
struct public policy deductively. Public policy is found in Biblical Jaw.
