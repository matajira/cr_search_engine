THE MORAL MAJORITY: AN ANABAPTIST CRITIQUE 89

ment and other aspects of the social order with the claims of Christ
and his church. But it must be remembered that this confronta-
tion is a witness, not an attempt to take over the social order and
make it obey the moral mandates of the Christian faith. That
would be moralism. The claim of the church goes deeper. It calls
people to regeneration and invites them to enter the church,
where true values are lived out. Through the witness of the church
the immorality of society may be temporarily restrained. But to
hope to convert the powers and to create a Christian nation or
society is to reject Christian eschatology. Only God can do that
and only after the consummation when the powers of evil have
been completely destroyed (p. 139).

There are several things badly wrong with these assertions. First,
suppose the civil magistrate does, wonder of wonders, hearken to
the prophetic voice of the church? Mr, Webber, tells us that the
social order must not “obey the moral mandates of the Christian
faith.” What is the poor magistrate to do? If he hears the prophetic
Word, he is forbidden to obey it. This is ridiculous. Mr. Webber
goes on to say, second, “That would be moralism.” No, moralism is
the belief that men can earn their way to heaven autonomously by
good works. A Christian state, restraining evil by the sword, is not
trying to get anyone to heaven; all it is doing is acting as a terror to
evildoers. This has nothing to do with moralism. Third, Mr. Web-
ber says that any attempt to convert the world to Christian order is a
rejection of Christian eschatology. This simply is not the case.
Historic orthodox Christianity has always expected the church to
make significant progress toward the Christianization of the world
prior to the return of Christ.?"Mr. Webber rightly says that the
“claim of the church goes deeper. It calls people to regen-
eration... ,” but, that is just the difference between the form
Christianity takes in the church and the form it takes in the state, By
pitting the two against each other, Mr. Webber falls into a gnostic
construction,

Since gnosticism sees salvation in terms of power, it focuses on the
experience of believers, The focus of orthodoxy, by way of contrast, is
on the objective government of God. The defining mark of a Christian is
not, as in gnosticism, some “conversion experience,” but rather an
evident submission to the government of God, signed and sealed
in baptism and manifest in a holy walk. There is, to be sure, an

7. For a comprehensive demonstration of this fact, see Roderick Campbell, Jsrae!
and the New Covenant (Tyler, TX: Geneva Divinity School Press, 1982). This book
can be ordered for $12.95, postpaid, from GDS Press, 708 Hamvasy, Tyler, TX
75701,
