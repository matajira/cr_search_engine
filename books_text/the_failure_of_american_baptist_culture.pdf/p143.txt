BAPTISM, REDEMPTIVE HISTORY, AND ESCHATOLOGY 127

covenant, or the final purpose, which is the salvation of the elect;
and there is an intermediate or instrumental purpose which is
broader. This includes not only the salvation of the elect, but the
destruction of all the works of the devil.45 Covenant theology, he
observes, has exhibited a strong tendency to reduce the covenant to
election. If we do this, he concludes, there is nd way that covenant
theology can incorporate the aspect of divine vengeance and wrath
within the covenantal framework. These aspects of the messianic,
mediatorial work of Christ (I Jn. 3:8 & Jn. 3:18) must be seen as
extra-covenantal—an unfortunate and weak position for covenant
theologians who wish to see God akuays dealing with his creation
covenantally. Rather, we must understand that the covenant con-
tains both curse and blessing. God is king over all creation; those
who submit to his Lordship becomes objects of His favour; those
who do not become objects of his curse.

If the covenant has these dual aspects of curse and blessing,
while the final purpose is the blessing of the elect, what then is the
purpose and meaning of circumcision or baptism? Kline argues that
circumcision is the self-maledictory oath of the covenant: the
references in Gen. 17:9-14 to cutting the covenant, cutting the
foreskin, and cutting off the recalcitrant member are all interrelated:
all point to the fact that circumcision was the sign of the oath-curse
of covenant ratification.*® He concludes: “In this light circumcision
is found to be an oath-rite and, as such, a pledge of consecration and
a symbol of malediction. That is its primary, symbolic signifi-
cance.”47 When a man was circumcised he simply “confessed him-
self to be under the juridicial authority of Yahweh and consigned
himself to the ordeal of his Lord’s judgment for the final verdict on
his life**

It follows’ that the meaning of baptism is similar. We read:

Now if the covenant is first and last a declaration of Christ’s lord-
ship, then the baptismal sign of entrance into it will before all
other things be a sign of coming under the jurisdiction of the cove-
nant and particularly under the covenantal dominion of the
Lord.*9

From this, Kline draws the conclusion that the actual import of the
sign remains general and it is neutral in its signification toward

45, Ibid., p. 34.
46, Itid., p. 43.
47, Ibid., p. 48.
49, Idem.
