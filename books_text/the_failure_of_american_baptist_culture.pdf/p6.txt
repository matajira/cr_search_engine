vi CHRISTIANITY AND GIV{LIZATION

independency, also a matter of intent). The editors, obviously, are
catholic, though by no means Roman. To be precise, we are
Reformed catholics, committed to the theology of Calvinism as it
has found expression (for instance) in Scottish Presbyterian govern-
ment, Dutch Reformed thought, and (to an extent) in Anglican
worship. We recognize, obviously, that evangelical Baptists are our
Christian brethren. These essays are not a declaration of war
against Baptists, but an invitation to them to reconsider their
position.

What, then, is the true character of the debate between Baptists
and Calvinists, between independents and catholics? That character
is presuppositional, rather than exegetical. The purpose of the
essays in this symposium is to expose these presuppositions, so that
a more intelligent discussion of the problems can ensue.

In this introduction, I should like to paint the picture in broad
strokes, I have no doubt but that much of what I have to say here
will be offensive to some Baptist brethren. I ask only that they listen
carefully. I am not trying to be offensive, but to provoke thought
along unaccustomed lines. So to get right to it, let us look at some
differences between Reformed and Baptistic thought in the area of
the Trinity.

‘The Bible teaches us that God is a Person, and so we can pray to
God. The Bible also teaches us that God is three Persons, and so we
can pray to the Father, to the Son, and ta the Holy Spirit. God's
Oneness is not the same as His Threeness, but Ged is every bit as
much One as He is Three, and every bit as much Three as.He is
One. Consistent Christians, therefore, are not Tri-theists (three
gods), nor are they pure Mono-theists (one God); rather, they are
Trinitarian.

The doctrine of the Trinity teaches us that the ane and the many
are equally ultimate in God, and thus are equally ultimate in God’s
creation. This Christian belief preserves us from the errors of in-
dividualism on the one hand and corporatism on the other, Let’s
take some examples.

In some societies, the family is such a closed, corporate body
that the individual has no place outside the family. Indeed, in many
of the ancient world cultures, the father might kill his wife or chil-
dren or slaves if he chose to do so. On the other hand, modern soci-
ety has virtually destroyed the family through individualism.
Rebellion surrounds us on every hand. The Bible presents the fam-
ily as a balance between the one and the many. The family is a real
entity, a genuine corporation, and God deals with the family as a
