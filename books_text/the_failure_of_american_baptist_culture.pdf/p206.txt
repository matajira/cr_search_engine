192 CHRISTIANITY AND CIVILIZATION

Indeed, the apostle makes the Israelites equal to us not only in the
grace of the covenant but also in the ‘signification of the
sacraments. In recounting examples of the punishments with
which, according to Scripture, the Israelites were chastised of old,
his purpose was to deter the Corinthians from falling into similar
misdeeds. So he begins with this premise: there is no reason why
we should clairn any privilege for ourselves, to deliver us from the
vengeance of God, which they underwent, since the Lord not only
provided them with the same benefits but also manifested his
grace among them by the same symbols. (II. 10. 5)

Because the Word of God was present in the Old Covenant, eter-
nal fife was also a key blessing of the covenant that the Old Cove-
nant saints shared with the New Covenant believers,

... the spiritual covenant was also common to the
patriarchs. .-. . Now since God of old bound the Jews to himself
by this sacred bond, there is no doubt that he set them apart to the
hope of eternal life... . Adam, Abel, Noah, Abraham and other
patriarchs cleaved to God by such illumination of the Word.
Therefore I say that without any doubt they entered into God’s
immortal kingdom. For theirs was a real participation in God,
which cannot be without the blessing of eternal life. (II. 10. 7)

The very formula of the covenant which was possessed by the Old
Testament saints for Calvin demanded that they be seen to be
possessors of eternal life.1?

. .. let us pass on to the very formula of the covenant... . For
the Lord always covenanted with his servants thus: “I will be your
God, and you shall be my people.” The prophets also commonly
explained that life and salvation and the whole of blessedness are
embraced in these words. . . . He is our God on this condition:
that he dwell among us, as he has testified through Moses. But
one cannot obtain such a presence of him without, at the same
time, possessing life. And although nothing further was ex-
pressed, they had a clear enough promise of spiritual life in chese
words: “I am... your God.” For he did not declare that he
would be a God tu their bodies alone, but especially to their souls.
Still souls, unless they be joined to God through righteousness,
remain estranged from him in death. On the other hand, such a
union when present will bring everlasting salvation with it. (II.

10. 8)

12. One of Gaivin's great themes in IL. 6-11 is that eternal life is the great benefit
of the covenant regardless of what point in the history of redemption one is discuss-
ing. In Psalm 67:2 Calvin calls the covenant “the source and spring of salvation.” In
Zech. 12:1 he says that the “hope of salvation is founded on the covenant.”
