186 CHRISTIANITY AND CIVILIZATION

the Reformation begun by Luther and advanced by Calvin.3
Calvinistic Baptists believe that no great injustice is done to Calvin’s

 

Anabaptist Story (Grand Rapids: Wm. B, Kerdmans, 1975), p. 145 writes, “Luther's
battle ery, ‘Justification by faith, became his plumb line for interpreting the Bible.
However, due to the persistent Roman Catholic appendages of his theology, he was
never able to give this truth consistent expression.” James A. Kirkland, The People
Called Baptisis (Texarkana: Bogard Press, 197(), p. 28 states, “The Reformation was
not a full return to New Testament teachings. The Protestants brought some of
Rome's errors, modified somewhat, into their new churches.” Again on p. 29, he
writes, “Che Reformers refused to renounce Roman Baptism.” Jewet’s remarks im-
ply a similar merging of Gerholic and Protestant thought at this juncture, “Con-
fronted with this argument from ancient custom, early Baptists used to remind their
Paedobaptist brethren that subjects of the triple crown are fond of tradition, and that
it i becomes a Protestant to cry, ‘The Fathers, the Fathers," p. 15.

3. Kirkland writes, “When the Angbaptists saw that the Reformers were halting
short of a full rerarn to the New Testament faith, they separated completely trom the
Reformation movement” (p. 30). C.. E. Tulga writes in Way Baptists are Not Protes-
tants, “Luther, Calvin, and Zwingli considered them heretics and consented and
sometimes encouraged their punishment and death as heretics. . . . The Reformers.
must share with the Roman Catholics the responsibility for the bloody persecution
and death of a great host of Anabaptists. It is true that the history of the Baptists can
be traced by their bloady footprints an the sands of time; it is also true that the hands
of the Reformers are stained with the blood of many of the saints of God who dared
to stand by the Word of God and oppose their sinful compromises.” Cited by
Kirkland, p. 30.

While the point of these citations is to illustrate the baptistic conception of their
role in “completing the Reformation,” it also raises the question of the problem of
persecution. First, it must be admitted that both Rome and the original
“Protestants” at the Diet of Speyer in 1429 joined in reenacting the death penalty for
rebaptism as provided by the Justinian Code. Yet, this was nat the standard prac-
tice. Luther for several years refused to accept the notion that the sword could be
brought in defense of the gospel and thus advocated exile as the extreme form of
punishment. Later, in the wake of what he viewed as seditious activities, he allowed.
for capital punishment as a means of restraining Anabaptist threats to the social
order. It is well known that Zwingli executed Anabaptists. The reason, however, ap-
pears w be less theological than political. In view of their official defeat at the two
baptismal disputations against Zwingli in Zurich, their continued activities were
seen as directly contrary to the authority of the civil leaders. Once this became a
repeated pattern of resistance, the death penaity was imposed. While it can be
argued that religious tolerance should have prevailed, it must also he admitted that
in that age, Zwingli was carrying out his understanding of obedience to the civil
order.

 

 

 

 

 

 

Martin Bucer, the theological mentor of Calvin, is undoubtedly the best example
of religious toleration among the early Reformers. He sought to exercise Christian
love and acceptance if this was mutually shared by hoth the Reformed and the
Anabaptists. If this proved impossible, che penalty was not then death, but rather ex-
ile, Bucer’s attitude two years alter the Diet of Speyer is well iustrated in his letrer
written ta Margaret Blaurer in September 19, 1531, concerning Pilgram Marpeck:
“What is the view of your Anabaptist of whom you write co me but that of the ancient

 
