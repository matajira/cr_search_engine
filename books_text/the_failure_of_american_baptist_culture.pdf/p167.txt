 

BAPTISM, REDEMPTIVE HISTORY, AND BSCHATOLOGY

Conclusion

We have scen that baptism is the sign and seal of an entrance
into a new eschatological acon. The ground of the dispute between
baptists and paedobaptists les in their differing conceptions of this
aeon. Baptist theologians argue for the administration of the sacra-
ment to belicvers only on the ground that many human institutions
are no longer under the purview of redemption, so that the New
Acon is radically and essentially an immaterial realm. Administra-
tion of baptism to children on the other hand can only be predicated
upon a view of the eschaton grounded in the continuity of the crea-
tion itself,

The Bible docs not support baptist conceptions of redemptive-
history or eschatology. We have shown that the Old Covenant
embraced both internal and external realities, as does the New. The
New Covenant differs from the Old in that it is the Age of Realiza-
tion, where redemption is powerfully accomplished both in internal
and external realms. If no objection could be adduced againsl cir-
cumcising children under the Old Covenant, far less can be adduced
against baptizing infants in the New Covenant, since that Covenant
redeems all aspects of creation in prepotent intensity.

Gan we now retreat back into our ecclesiastical ghettoes after
this debate? Are these issues important? We must insist that they
are. Not to baptize infants breaks covenant with God, for it impli-
citly surrenders much of the creation to the Devil. The Church fails
to believe in the saving work of Christ in all its power and extensive
glorious reality. The church compromises the work of her Head,

 

being unfaithful to her Saviour. She is unfaithfut in his household.
She denies, and in thal sense, profanes his redemption. And for this,
the Lord will require an accounting from his servants.
