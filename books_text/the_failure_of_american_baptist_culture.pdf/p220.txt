206 CHRISTIANITY AND CIVILIZATION

standing of Panl’s special task of refuting the attempt to gain salva-
tion by meritorious observance of the law. Calvin himself offers a
helpful summary of these matters in his comments on Galatians
3:25 and 4:1. In the first text he asks, “How is the law abolished?”
His answer is that it is not abolished as a rule of life and cites II
Timothy 3:16-17. It is abolished, however in all that differs in com-
parison of Moses with the covenant of grace, These differences he
lists as an unabating demand for exact obedience without
forgiveness, a severe reckoning of the smallest offenses, Christ is not
openly exhibited, but rather He and His grace are seen only dis-
tantly in ceremonies. In the second text he concludes, “AI this leads
to the conclusion, that the difference between us and the ancient
fathers lics in accidents, not in substance. In all the leading
characters of the testament or covenant we agree. .. . ” In light of
these considerations, two texts already cited take on a greater depth
of meaning. Calvin’s statement that God’s people “since the begin-
ning of the world were covenanted to him by the same daw and by the
bond of the same doctrine” (II. 10. 2) can be seen to be understood
by him as the normal use of the law as a rule of life. Similarly,
Calvin's view that “Moses was not made a lawgiver to wipe out the
blessing promised to the race of Abraham. Rather, we sce him
repeatedly reminding the Jews of the freely given covenant made
with their fathers . . .” is also clearly a further affirmation that the
Pauline interpretation of law is not the normal use of law, but rather
a special application of it. In interpreting Calvin's theological
perspective on the relationship of the Old and New Covenants,
then, one must be cognizant of his twofold use of the terms “New
Covenant” and “law.”

 

Can Men Break the New Covenant?

Yet one last matter of importance for Calvin’s understanding of

 

greatest art of Christians is to be ignorant af the whole of active righteousness and of
the law; whereas outside the peaple of Gud, the greatest wisdom is to know and to
contemplate the law. . . . For if 1 do not remove the law from my sight and turn my
thoughts to grace, as though there were no law and only pure grace, I cannot be
blessed.” Cited in Gerhard Ebeling, Luther, trans. R. A. Wilson (Philadelphia: For-
tress Press, 1972), pp. 128-24, It should from this be clear that Luther and Melan-
chthon understond that the law Agd been abrogated even in the form of the moral law
while Calvin believed that this was a misreading of Scripture. Calvin insists that
Paul only seems to abrogate the law. While Luther would have the Ghristian ignorant
of the law, Calvin would remind them that the covenant of grace includes perfect
obedience to the law as one of its stipulations!
