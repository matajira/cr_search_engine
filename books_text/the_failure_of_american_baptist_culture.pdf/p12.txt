xii GHRISTIANITY AND CIVILIZATION

rhetoric of human rights, but hold forth the law of God as the an-
swer to men’s ills. (Parenthetically, the second issue of Christianity
and Civilization is scheduled to deal with Christianity and resistance
to tyranny, and in this volume the question of human rights will
more fully be dealt with.)

Let me say again, in conclusion, that neither I nor the other
editors of this symposium are maintaining that Baptists are wolves
in the clothing of Christian sheep, nor that the ills of modern
American civilization are to be blamed on the Baptists. We believe
that our Baptist brethren have been too much influenced by certain
secular trends, and we earnestly pray and desire that they rethink their
position, and return to the catholic and Reformed faith.

Sok eR Rk

The first part of the symposium is concerned with the Crisis of
American Baptist Culture, as it comes to expression in the activities
of the New Christian Right. Suddenly aware that our civilization is
moving rapidly toward sodomy and tyranny, the New Christian
Right is trying to mount an offensive to return America to more
Ghristian moorings. The New Christian Right, however, has in-
dicated time and again that it does not know what it is doing, and
that its program is riddled with contradictions. For instance, many
are concerned to get prayer to some nebulous and non-existent
“deity” returned to the public schools. This has nothing to do with
Christianity. Others are striving to have some “neutral” creationism
(completely ignoring the Bible) taught in public schools, One was
tempted to rejoice when this seemingly blasphemous attempt was
thwarted in Arkansas. A third example of contradiction is the Moral
Majority, which seeks for “moral” government based on “human
rights” and completely apart from Christianity! Obviously, the New
Christian Right is in deep crisis. The essays by Gary North and
Kevin Graig deal with these contradictions, and show how the New
Christian Right can find sure footing.

My own piece is a review of Robert Webber’s book The Moral
Majority. Mr. Webber’s perspective is anabaptistic, and I suggest un-
wittingly gnostic as well, While Mr. Webber scores a few good
observations against the New Christian Right, his book fails to
come up with Biblical suggestions for reconstruction. This book,
however, has been widely touted as a definitive answer to the New
Christian Right, and it is important to answer it. (I have reviewed
another book by Robert Webber, The Secular Saint: A Case for Evangel-
