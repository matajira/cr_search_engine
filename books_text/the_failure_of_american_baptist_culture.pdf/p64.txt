48 CHRISTIANITY AND CIVILIZATION

If our social action is going to advocate the passage of certain laws,
then those laws musi, if we are Christian, be the laws of the Bible.
There is no other choice.

Can two religious-legal systems co-exist? A man’s religion holds
as the pinnacle of morality the ritual sacrifice and cannibalistic consump-
tion of his first-born. According to the myth of pluralism, where no
single religion is allowed to impose its views on others, this man
would fit in just fine, even if everyone else’s religion commanded
“Thou shalt not kill.” Nonsense. Pure pluralism would result in
social chaos and the breakdown of law and order. It could not func-
tion, The religion of the people of a nation determines its laws,
Three hundred years ago, the morality of this nation was directed
by Biblical Christianity. This moral system has very nearly been
replaced by the morality of the religion of secular humanism. The
moral system of the secular humanists seems to parallel the moral
system of Christianity in many respects. Many humanists do not
murder. But they refrain from murder because they want to, not
because the God who is there has said, “Thou shalt not kill.” The
humanist could decide to kill at any mament. A fundamental tenet
of secular humanism is “freedom of individual choice.” As a result,
many humanists do murder, and a Christian nation ought to pass
laws forbidding, for example, abortion, even if it restricts the “free-
dom of individual choice” of the humanists. The government ought
not allow secular humanists to do all that their religion says is moral,
If humanists decide not to kill their children, and, consistent with
their religion, they do so because they want to (i.e,, because they do not
want to suffér the penal sanctions of a godly government [Ex,
21:23]), fine. But where the Bible restricts the “religious freedom” of
the humanists, governments ought to restrict accordingly.

Itis impossible that a government could give absolute freedom to all religions.

 

Battie for the Mind (Old Tappan, NJ: Fleming H. Revell Cv.), p, 187, and many
others, Although Newsweek identified Chalcedon as the “Religions Right” “think
tank," itis hoped thal the “Religious Right” will begin doing their homework a little
more diligently, Newsweek, Vol. XCVII, No. 5 (Feb. 2, 1981), p. 60.

19, The rhetoric of the “Religious Right” is surprisingly two-faced. Appeals to
Christians are usually Biblical. Jerry Falwell admonishes a crowd at an “I Love
America” rally, holding up a Bible, “Ifa man stands by this book, vote lor himn. Ifhe
doesn't, don't” (Time, Oct. 1, 1979, p. 62), But when appealing to unbelievers, he
changes his tune. When the interviewer for Penihouse accused Falwell of imposing his
own view of the way things “ought to be,” he responded, “Well, I think that the
American tradition—forger the Bible—is the sanctity of the family, the husband,
wife, legally married relationship, is unquestionably the cornerstone of this
republic.” Penthouse (March, 1981), p. 152. “Forget the Bible”?! Dr. Falwell!
