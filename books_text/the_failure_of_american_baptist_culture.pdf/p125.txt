MEDIA THEO-POP 109

various religious experiences and the methods used to obtain them,
There has been a shift from objective theological knowledge and
objectives to the pragmatic application of a mental method which
promises to give the practitioner all the good things in life. The
stress is on the person as the measure of the truth. If it makes you
happy, then it is true; if it does not, then it cannot be true. The cross
is seen as a “plus” sign, not the symbol of life through death. Sin is
meaningless; all that matters is the self and its desires, regardless of
the consequences to others. Modern religion does not care anything
for God, it only wants to use Him. As Quebedeaux sadly writes,
“Thus there are no prophets on TV —only profits” (p. 152).

The superficiality that is characteristic not only of popular
religion, but also of the entire culture, is the result of “the fact that
most individuals view religion primarily as a therapeutic means to
get relief from boredom through entertainment” (p. 154). The celeb-
rity leaders are in reality mere entertainers who have fans, not fol-
lowers. “And because celebrities merely entertain and do not offer
deep teaching, they can hardly impose a discipline on others to in-
corporate in their daily lives” (p. 154). As a result of this unique
relationship, it can be asked whether or not the celebrity leaders are
really “leaders” at all, Once again, “leadership” has been redefined
to include the “stars” of that vast, multi-media oriented, invisible
religion of popular culture. This type of leadership represents the
ultimate in relational superficiality. The only contact the “leader”
has with his “followers” is through the TV, on the printed page, or
through the mass-produced “personal” computer letter. “Such rela-
tionships,” writes Quchedeaux, “are not real at all” (p. 154).

Summary

The strengths of this book lie in its penetrating analysis of
popular culture and its religion. Quebedeaux is an excellent
reporter and has done his homework. [is book is tempered with
both insight and an acute grasp of the movement as a whole. He not
only describes the current situation, but gives an excellent overview
of the last 150 years of American religious history that form the
backdrop for the situation that we find ourselves in today.

The glaring weakness of the book is found in the solutions that
he offers. Quebedeaux sees the main problem as a “lack of love”
among the nebulous quagmire of religious leaders. I, for one, don’t
understand how more “love” will solve the mammoth problems that
confront the “electronic church.” The “electronic church’ is built on
