CALVIN’S GOVENANTAL RESPONSE 211

to his salvation. To be taught humility before God, the warnings are
anecessary means of grace. Galvin does not stop with his adherence
to the necessity of warnings for the elect in his explanation of how
covenant-breaking applies to New Covenant people. To this idea, he
adds a highly developed scheme of how an individual is grafted into
and excised from the covenant. Calvin explains,

But if it be asked respecting individuals, “How any one could be
cut off from the grafting, and how after excision, he could be
grafted again,” — bear in mind, that there are three modes of insi-
tion, and two modes of excision. For instance, the children of the
faithful are ingrafted, to whom the promise belongs according to
the covenant made with the fathers; ingrafted are also they who
indeed receive the seed of the gospel, but it strikes no root, or it is
choked before it brings any fruit; and thirdly the elect are in-
grafted, who are illuminated unto eternal life according to the im-
mutable purpose of God.

Calvin begins his approach to this question with three possible
modes of entrance into the covenant: by birth into a Christian
home, by hypocritical faith, and by true conversion growing out of
divine election. To these three modes of insition, Calvin adds two
modes of excision,

The first are cut off, when they refuse the promise given to their
fathers, or do not receive it on account of their ingratitude; the se-
cond are cut off, when the seed is withered and destroyed; and as
the danger of this impends over all, with regard to their own
nature, it must be allowed that this warning which Paul gives
belongs in a certain way to the faithful, lest they indulge them-
selves in the sloth of the flesh. But with regard to the present
passage, it is enough for us to know, that the vengeance which
God had executed on the Jews, is pronounced on the Gentiles, in
case they become like them.

Covenant children according to Calvin can be cut off from the cove-
nant by refusing the promise or by ingratitude. Hypocrites are cut
off from the covenant when the seed of the Word of God is desiroyed
in their lives, With respect to the elect, Calvin once again affirms his
conviction that this warning is also applicable to the elect since they
in this life are burdened with the lust of the flesh and could from the
vantage point of human responsibility apostatize. For Calvin, it is
highly significant to realize that the warnings of apostasy are not to
be ignored, since there are always members of the church ~ the cor-
porately elect people of the covenant—wha will fall away from the
promise of their baptistn or their profession of faith. While the truly
