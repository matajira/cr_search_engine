A REFORMED VIEW OF FREEMASONRY 279

knowing that if they use different names for the nameless one of a
hundred names, they are yet praying to one God and Father of all,
knowing also that while they read different volumes they are in fact
reading the same vast book of faith of Man as revealed in the strug-
gle and sorrow of the race in its quest for God. . . . We honor every
book of faith. What Homer was to the Greeks, the Koran to the
Arabs, the grand old Bible is to us.” Thus is Masonry opposed to the
Christian doctrine of the Trinity, because it would limit Masonic
adherents to one religion,

There is a strange view of God, however, given to the Mason
who is preparing for the 19th degree. This is know as the “Royal
Arch” degree. He, to this point, does not know the name of the
GAOTU or Ged but over the “Arch” is written JEHBULON, an
abbreviation for Jehovah Baal Osiris! And consider the following
blasphemy. Orie of the Lodge Hymns sung to the tune “Come Thou
Almighty King” has this first stanza:5

Hail, Masonry divine!
Glory of ages shine,
Long may’st thou reign!
Where e’er thy Lodges stand,
May they have great command,
And always grace the land!
Thou art divine.

Masonry Is Humanistic In Its Soteriology

The means of salvation consistently presented in Masonry is the
performance of virtuous works. This is the stated meaning of the
“Lambskin” or white leather Apron which is the emblem of in-
nocence to the Mason when worthily worn. “By it we are reminded
of that purity of life and conduct so essentially necessary to gaining
admission to the Celestial Lodge above, where the Supreme
Architect of the Universe presides,”® Likewise, the Common Gavel
is said to teach Free and Accepted Masons “the more noble and
glorious purpose of divesting our hearts and consciences of all the
vices and superfluities of life, thereby fitting our minds as living
stones for that spiritual building, that ‘House not made with hands;
eternal in the heavens.’ "7

To maintain that people gain entrance to heaven by God's

. Masonic Manual and Code elc,, p. 173.
. Ibid, p. 4A.
» Tid, p. 19.

sin
