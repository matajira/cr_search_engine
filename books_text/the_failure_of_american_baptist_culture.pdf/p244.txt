230 CHRISTIANITY AND CIVILIZATION

to the provisions of the covenant, {IV. 16. 24)

Having shown this basic distinction between adult and infant in
covenant with God, Calvin proceeds to clarify how an aduli
unbeliever is outside of the benefits of the covenant. Then, Calvin
contrasts this with the right an infant of believers has in the cove-
nant:

But he who is an unbeliever, sprung from impious parents, is
reckoned as alien to the fellowship of the covenant until he is
joined to God through faith. No wonder, then, if he does not par-
take in the sign when what is signified would be fallacious and
empty in him! Paul also writes to this effect: that the Gentiles, so
long as they were immersed in their idolatry, were outside the
covenant. The whale matter, unless Tam mistaken, can be clearly
disclosed in this brief statement, Those who embrace faith in
Christ as grown men, since they were previously strangers to the
covenant, are not to be given the badge of baptism unless they
first have faith and repentance, which alone can give access to the
society of the covenant. But those infants who derive their origin
from Christians, as they have been born directly into the in-
heritance of the covenant, and are expected by God, are thus to be
received into baptism. (IV. 16. 24)

With this development of the believer’s entering the covenant as an
adult by faith, and the simultaneous inclusion of his children by
right of the covenant, Calvin believes that he has successfully
answered the Anabaptist argument of the universal necessity of faith
before the administration of baptism.

Calvin concludes his analysis of paedobaptism wich a denial and
an affirmation. He first denies that his doctrine of infant baptism
implies that all unbaptized are lost. Having said this, however,
Calvin nevertheless resists the implication that baptism is unimpor-
tant. To despise baptism is ta despise God’s covenant:

The promise of the Lord is clear: “Whosoever believes in the Son
will not see death, nor come into judgment, but has passed from
death into life.” Nowhere do we find that he has ever condemned
anyone as yet unbaptized. I do not want anyone on this account to
think of me as meaning that baptism can be despised with impun-
ity (by which contempt I declare the Lord’s covenant will be
violated —so far am I from tolerating it!); it merely suffices to
prove that baptism is not so necessary that one from whom the
capacity to obtain it has been taken away should straightway be
counted as lost. (IV, 16, 26)

In these words, Calvin is attempting to insist on the necessity of
