THE BAPTIST FAILURE 169

the Pharisees had become an exclusive group who thought that they
alone possessed righteousness and salvation. They had constructed a
separate system of ethics and self-righteously inflicted it on the peo-
ple. When the people would not comply, they were alienated from
their religious leaders (Matthew 5:19ff.). This introduces another
problem attached to subjectivism,

IV. Theology of Anabaptism and Exclusivism

Subjective ethics produces exclusivistic doctrine and practice,
When Menno Simons gathered the Anabaptists into a peaceful com-
munity they were encouraged to separate from the world, In fact,
there was not to be “the taking of oaths, participation in war and in
the administration of justice... . Great stress was laid on separa-
tion from all non-Baptist Christians, this went so far as to demand
that a marriage should be dissolved in which the husband or wife
had been either excommunicated or convicted of unbelief (Gin
anabaptism).”#? With that pattern established, Baptists have usually
been exclusivists. They have separated from everything and
everyone that counters their doctrines. Why? Because they in-
dividualize the faith. If the covenant is defined around oneself and
one’s personal belief, then he must isolate to grow in faith. On the
other hand, if the covenant incorporates both the singular and the
plural, such as the family and the historic church, isolation is
avoided. Since this has not been the case, the intrinsically subjective
definition of the covenant of God has led to separation. Thus, “those
who break entirely with the world and with sin have the task of plac-
ing a new arder alongside of it, which is not erected upon the Family
and the State, upon property and dominion, but upon . . . ideas of
the equality of all possessions and social relations.”#9

Such efforts have failed. The theology endemic to Anabaptism,
mentioned in these four criticisms, serves as an introductory
analysis of their failure. Recapping what has been said thus
far~ Franciscan/Anabaptist thought can be summarized as subjective
theology. The outworking of this theology has led ta the presence of
four major errors within Anabaptist groups and those influenced by
them: (1) monastic theology: (2) perfectionism: (3) a subjective
standard of faith and practice; (4) exclusivism. The social out-

 

42. Troelesch, p. 705
43. [bid,, p. 365.
