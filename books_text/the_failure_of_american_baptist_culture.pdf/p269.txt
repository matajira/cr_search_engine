CHRISTIANITY AND RELIGIOUS LIBERTY 255

are, and that they will maintain certain fundamentals in their
religions like those you maintain in yours. Unless you have that
guarantee, you're looking for trouble.

Now first of all, I don’t think that everyone who promotes the
“liberty of conscience”/“eligious liberty” idea really takes seriously
the idea that all the world’s a “dung heap.” A good portion of it is
rotten, but certainly not so rotten that none of us can stand it. I
means it’s not as rotten as it was in the days of Sodam and Gomor-
rah; God could no longer stand the stench at that point.

You see, I don’t really think that Roger Williams and his Baptist
followers really understood what a “dung heap” the world is—at
least how much of a dung heap the world could be—like in the days
of Sodom and Gomorrah, Williams and the rest of them did not
believe that they were abandoning their culture to destructive
forces. Irrelevant Christian dogma didn’t make for a stable society,
as far as they were concerned. But while the world was filled with
“natural, sinful, inconstant men,” immorality in the seventeenth
century American colonies was far less pervasive than the immorali-
ty to which some of the world has since become accustomed. In
some places of the world I have heard that there are actually
churches of Satan, and homosexuals kidnapping young boys, not to
mention murderers going free while innocent men are called
criminals for defending their families and homes. But the seven-
teenth century was a bit more tame. The colonial era had its vices
and crimes, but in the New World especially, civil infractions of the
generally accepted moral code were met with effective civil
penalties. ‘Ihe effects of sin, or the smell of the dung, was readily
sensed only on a small scale.

But this was part of the problem—I mean, this generally ac-
cepted moral code. You see, K. D., I think that it provided a conve-
nient solution to the otherwise perplexing problem of religious
confrontation —a solution which, as I see it, proved as untenable for
Roger Williams as it is for some places in the world today (not to
mention the problems it could cause for us).

There were certain religious beliefs, Williams and his followers
assumed, which were common among all men because they were so
obviously reasonable. [t was on these religious “basics” that men
could build a peaceful society without having to quarrel and contend
over doctrinal issues. While men’s consciences were fallible, there
were some things which all men confessed — at least so the “liberty of
conscience’/“religious liberty” supporters assumed. All men seerned
to profess certain things, and they all seemed to condemn certain
