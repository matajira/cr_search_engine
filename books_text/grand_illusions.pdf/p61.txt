All that Glitters 4

The powerful, the would-be-powerful, and the wish-they-
were-powerful have always relied on such tactics. Plato and
Thucydides observed the phenomenon during Greece’s classical
era.?09 Plutarch and Augustine identified it during the Roman
epoch.?!° Sergios Kasilov and Basil Argyros noted it during the
Byzantine millennium.” Niccolo Machiavelli and Thomas More
recognized its importance during the European Renaissance.?!?
And Aleksandr Solzhenitsyn and Colin Thubron have pointed it
out in our own time.?43 Most of the myth-makers never actually
betteved in the gods upon Olympus, across the River Styx, or with-
in the Kremlin Palace. After all, they knew all too well from
whence those lies came. But as high priests of deceit, they used
the lies to dominate the hearts and minds and lives of the masses.

The Bible says that such men are full of deceitful words
{Psalm 36:3). Their counsel is deceitful (Proverbs 12:5). Their
favor is deceitful (Proverbs 27:6). And their hearts are deceitful
(Mark 7:22). They defraud the unsuspecting (Romans 16:18),
displaying the spirit of anti-Christ (2 John 7), all for the sake of
wealth, prestige, and prerogative (Proverbs 21:6).

Such puissance is in the long run all too fleeting, however
(Revelation 21:8), because myth-makers do not go unpunished
(Proverbs 19:5). Ultimately, their sin finds them out ( Jeremiah
17:11).

Still, because their lies wreak havoc among the innocent
{Micah 6:12), it is essential that we not be taken in. Not only are
we to be alert to deception (Ephesians 4:14), testing the words
and deeds of the myth-makers against the Truth (1 John 4:1-6),
but we are to expose their deceptions as well (Ephesians 5:11).

Planned Parenthood, not at all unlike Jeroboam and the
other infamous myth-makers throughout history, has thus far
been able to parlay its deception into a substantial empire. But
now, the truth must be told. The illusion must be exposed. The
Big Lie must be demythologized.

Woe to the bloody city, completely full of lies and pillage; Her
prey never departs (Nahum 3:1).
