154 Granp ILLusions

Rockefeller,3! Emperor Hirohito,3? and Henry Ford.34 The six-
ties brought her tremendous fame and acceptance. Before her
death, she received the enthusiastic endorsements of former
Presidents Harry Truman and Dwight Eisenhower.*4 She won
over arch-conservatives like Mrs. Barry Goldwater,?> and arch-
liberals like Margaret Mead®°~— ideology didn’t seem to matter.

In addition, Margaret Sanger was a tenacious organizer. Her
days with the Socialist Party? and the Communist Labor move-
ment3® not only trained her in effective propaganda techniques,
they taught her how to solicit, train, and activate volunteers.2?
Using these skills, Margaret literally combed the country, and
ultimately the world, searching for donors.*? She left no stone
unturned. She applied for every grant, appealed to every foun-
dation, made presentations to every corporation, and appealed
to every charity.*! She wanted a piece of every philanthropic pie,
and she would go to great pains to make her case to any who
would listen.4? She was a dogged promoter. And, like the per-
sistent widow in Christ's parable, she was so unrelenting, she
prevailed more times than not (Luke 18:1-8).

Perhaps Margaret's greatest coup came when she was able to
gain for her organization an IRS charitable tax-exempt status.*?
That move put Planned Parenthood in the same legal category
as a local church or a philanthropic society.4+ All donations be-
came tax-deductible, and that made solicitation and donor
development all too easy.*5

The fund-raising apparatus that she set in place has only
grown in size and sophistication in the years since she died. It has
garnered hundreds of celebrity endorsements.*¢ It has affiliated
with every major national and international professional and
educational association even remotely related to Planned
Parenthood’s work.*” And it has tapped into the fiscal lifeblood
of virtually every major charitable resource available.**

The United Way
Founded in 1918, United Way of America is the world’s larg-
est cooperative coalition of charity organizations in the world.*9
Tts multi-million dollar annual effort not only distributes much
needed cash to local private-sector service providers, but it pro-
vides program support and consultation in the areas of “fund-
raising, budgeting, management, fund distribution, planning,
