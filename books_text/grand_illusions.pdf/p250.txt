230 Granp InLusions

Bogomiles gained wide followings in the Balkans, along the
edge of Byzantium. The Beguines and Beghards gained wide-
ranging popularity in the Low Countries of Flanders and Lothier.
The Waldensians and the Albigensians won devotees throughout
Toulouse, Lyons, and all of Southern France. Before long, the
thriving towns along the Mediterranean were honeycombed
with apostates zealously proselytizing for their cause. “The
cities,” one Bishop exclaimed, “are filled with these false proph-
ets.”* Indeed, Milan alone had seventeen new religions. Cults
outnumbered churches in Viterbo, Ferrara, and Rimini. And
defections in Orleans, Avignon, Sardica, and Nicomedia had
become terribly alarming.

Most of the sects had an amazing uniformity of dogma.
They all shared an Arian conception of Christ and a Manichean
notion of creation. They were pacifistic and ascetic. They were
communistic and anarchistic. They exalted individual auton-
omy, liberty, and equality. They were generally sexually
indulgent, opposed to traditional family life, and rejected child-
bearing and rearing as oppressive. They revived the popularity
of abortion, infanticide, and euthanasia. Because they aban-
doned vocations and work, they often resorted to theft, confisca-
tion, looting, deception, conspiracy, and divisiveness. They
indulged in drunkenness, carousing, and all manner of sensual-
ity. They dabbled in sorcery, witchcraft, alchemy, astrology,
divining, and idolatry. Of course, these tendencies and practices
varied somewhat from time to time, from place to place, and
from cult to cult, but their general conformity was such that
churchmen often referred to them all by the name of the largest
and best-organized group: the Cathari.

At first, the church was tolerant and indulgent. But, as the
cults became more and more extreme, and as their excesses
wreaked more and more havoc, it saw the necessity of confront-
ing and exposing them.

The Apostle Paul had warned believers not to tolerate the
evil and unfruitful “deeds of darkness,” but, rather, to “expose
them.” The question was, were the cults actually evi/, or were
they simply misgutded ?

Bernard of Clairveaux, the greatest Western preacher of the
day, answered by saying that evil was “not a matter of opinion.”®
The Scriptures had afforded men, he said, with an objective,
measurable, and tangible definition of evil:
