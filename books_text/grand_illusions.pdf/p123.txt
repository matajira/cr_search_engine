A Race of Thoroughbreds: The Racial Legacy 103

uable citizens. If, on the other hand, individual Whites demon-
strate “dysgenic traits,” then their fertility must be curbed right
along with the other “inferiors and undesirables.”9%

Scientific Racism is an equal opportunity discriminator. In
other words, anyone with a “defective gene pool” is suspect. And any-
one who shows promise may be admitted to the ranks of the elite.

The Theology of Racism

Racism is a vile and detestable sin (Deuteronomy 23:7).
According to the Bible, bigots are “wicked” and “proud” (Psalm
94:1-6). They are accursed (Deuteronomy 27:19). They are
under the judgment of God (Ezekiel 22:7, 29-31). And they face
His stern indignation (Malachi 3:5).

The stranger, the alien, and the sojourner are to be cared for
and sustained (Deuteronomy 24:20), not vexed and oppressed
(Exodus 22:21). They are to be loved (Deuteronomy 10:19) and
protected (Exodus 23:9). They are to be relieved (Leviticus
25:35) and satisfied (Deuteronomy 14:29). They are to receive
equal protection under the law (Exodus 12:49, Leviticus 24:22,
Numbers 15:16) and special attention in times of need (Leviticus
23:22, Deuteronomy 24:17-19). They are to share fully in the
blessings that God has graciously poured out on us all (Deuter-
onomy 14:29, 16:11-14).

When Jesus was asked what men must do to inherit eternal
life, He responded by guiding His interrogator to the Scriptures
(Luke 10:26). On the question of eternal life the Scriptures are
quite explicit: “You shall love the Lord your God with all your
heart, and with all your soul, and with all your strength, and
with all your mind; and you shall love your neighbor as yourself”
(Luke 10:27).

“Do this,” Jesus said, “and you will live” (Luke 10:28).

Not satisfied with this answer, the interrogator pressed the Lord
to clarify: “And who zs my neighbor?” he asked. “No sense in lov-
ing someone I don’t have to,” he must have thought (Luke 10:29).

Ever patient, ever wise, Jesus responded with a parable— the
beloved parable of the Good Samaritan:

A certain man was going down from Jerusalem to Jericho; and
he fell among robbers, and they stripped him and beat him,
