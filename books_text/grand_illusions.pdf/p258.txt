238 Granp ILLusrons

It was for freedom that Christ set us free; therefore keep stand-
ing firm and do not be subject again to a yoke of slavery (Gala-
tians 5:1).

Nevertheless, the righteous shall hold to his way, and he who
has clean hands shall grow stronger and stronger ( Job 17:9).

Steadfastness is not an attribute that we can simply conjure
up by sheer force of will. It demands a long-term commitment.

Marsha Gatlin is a member of her local school board and has
been for more than a decade. She first ran for the seat when her
teenage daughter brought home several Planned Parenthood
booklets from school one day.

“I made up my mind then and there,” she told me, “that I was
going to do something about the moral plague that was infecting
our children. My children. Well, I'ti tell you, it’s not been easy.
It's been one. fight after another. Ten years of conflict. But I’m
confident that I’ve been able to make a difference. So, I'm gonna
continue to hang in there. ’'m not about to back down on my
commitment now.”

That kind of commitment is what the Bible calls vision. It is
the willingness to sacrifice unceasingly for the Gospel’s sake. It is
the willingness to bypass immediate gratification, instant satis-
faction, and momentary recognition for the good of the future.
For the good of Christ’s Kingdom, Where faith is “the assurance
of things hoped for” (Hebrews 1t:1), vision is “the hope of things
assured of.” Where faith is “the conviction of things not. seen”
(Hebrews 11:1), vision is “the seeing of those convictions.”

Visionary men and women are confident, assured, and un-
daunted even in the face of calamity and catastrophe, because
they can see beyond the present. They can remain sure, secure,
and steadfast, because they have a broader perspective. They
can look past petty defeats and setbacks. They can plot and plan
far in advance of the day of vindication and victory. They have a
future-orientation. They believe in the idea of progress.

Though that progress may come in very small stages over
very long epochs, visionary men know that it eventually will
come. They believe in its inevitability.

And so they remain faithful. Toiling day in and day out, year
in and year out. Never despising “the day of small beginnings”
(Zechariah 4:10).
