170 Granp ILLusions

abortion rights on their editorial pages, and two major media
studies have shown that eighty to ninety percent of U.S. journal-
ists personally favor abortion rights. Moreover some reporters
participated in a big abortion rights march in Washington last
year, and the American Newspaper Guild, the union that repre-
sents news and editorial employees at many major papers, has
officially endorsed freedom of choice in abortion decisions,?¢

The article went on to assert:

Responsible journalists do try to be fair, and many charges of
bias in abortion coverage are not valid. But careful examination
of stories published and broadcast reveals scores of examples,
large and small, that can only be characterized as unfair to the
opponents of abortion, either in content, tone, choice of lan-
guage, or prominence of play.”

The fact is, the media’s Milli Vanilli lip-synching of the
Planned Parenthood party line provides stark evidence that it
has tossed any semblance of impartiality or objectivity to the
four winds.

But that is not the most disturbing aspect of the media’s cov-
erage of Planned Parenthood. Bias is a fairly straightforward
vice. What is even more insidious than an absence of factual
objectivity is an absence of professional integrity —journalists
have been ventriloquists instead of orators.

The essence of science is precision. The essence of sentiment
is presumption. Because the media has difficulty distinguishing
one from the other, it is both precise and presumptuous —but
about exactly the wrong things. When it comes to their coverage
of Margaret Sanger’s Eugenic dystopianiam, the media has been
very scientific and sociological about sentimental things, but
very sentimental about scientific and sociological things.

They- have, in short, not checked the facts, not verified the
data, and not understood the issue. They have invariably taken
the easy way out-by retrofitting news releases from pro-abortion
lobbyists and publicists and simply adding their byline.

Instead of working harder, they shouted. louder. Instead of
striving for professional excellence, they have settled for profes-
sional expediency. Instead of attempting to grasp their subject
matter, they have grasped at straws—and straw men.

That kind of dull dishonesty is either a sign of faulty disci-
pline or faulty ethics—or maybe both.
