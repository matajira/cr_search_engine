272 Granp ILLusions

fact-laden, action-oriented, positive, and helpful letters launched
the first American Revolution. Perhaps they can launch a second.

Second, we can attend our legislator’s home district meetings.
Most Congressmen come home for town meetings, or issues
seminars or fundraisers or social events or campaign stops or
holiday celebrations. Only a very small part of the constituency
regularly attends these meetings. Why not take it upon ourselves
to make as many of those appearances as we possibly can? Or
delegate various meetings out to several people in the church
who can report back to the whole group? If our legislator knows
that a very informed, very active, very vocal core of his constitu-
ency is unalterably opposed to Planned Parenthood funding and
contracting and coddling, he is much more likely to examine the
issues with some degree of care. And if we have the opportunity
at those personal appearances to hand him fact sheets, press kits,
tracts, or books, then all the better.

Third, we can respond to our legislator’s polls, Most Con-
gressmen use the mails to gauge constituency concerns. Why not
make the most of those prime opportunities? Why not answer
his polls thoughtfully and carefully with hand-written comments
in the margins? ‘A personal, neat, well-composed comment
shows congressional staffers that we really care about an issue,
and they are more likely to consider our thoughts.

Fourth, we can testify before congressional committee and
sub-committee hearings. There are at least sixteen different
stages that a bill must pass through before it actually becomes
law. It must be introduced. It must be referred to committee. It
must proceed through the appropriate sub-committees within
that committee. It must then be subjected to hearings and mark-
up. Next, the bill must be reported to the full House. Then it is
put on the calendar. When the date for discussion on the floor
finally comes up, consideration must be obtained. Then dis-
cussion, amendment, and debate occurs. Next comes the voting
stage. If the bill passes the House it is referred to the Senate
where a similar, albeit shorter, consideration, committee, and
floor process is followed. Once the Senate passes its version, the
bill goes before a joint committee of both branches to consolidate
any differences. The completed bill then goes to the president
for signing. Only then does the bill become law. The thing is,
