Slaying Dragons: The Character to Confront 251

Character in Context

Christian character traits are essential. Alertness, stead-
fastness, courage, strength, and love are indispensable. Con-
fronting the evil of Planned Parenthood would be impossible
without them.

But, by themselves, even those character traily are not
enough. They must be placed in their proper context. And that
context is the church,

The church has become the spurned and neglected step-
child of the modern era. It is perceived as being moss-backed
and archaic. Or awkward and irrelevant. It is regarded as little
more than a water-boy to the game of life.

Part of the reason for this horribly low estimation of the
church is due to the fact that the church has always Himped
through history.2? Men look at the all too evident, all too appar-
ent, sometimes even glaring, weaknesses of Christ’s Bride and
just assume that its lame and crippled state is ample justification
for dismissing its importance.

The fact is, though, the church’s limp is actually a confirma-
ton of its power, relevance, and significance.

After the Fall, God told Satan that the Righteous Deliverer,
Jesus Ghrist, would crush his head. But God also said that, in
the process, the heel of the Lord would be bruised (Genesis
3:15). The limp, then, that Christ’s Body displays is actually a
sign of great victory, not a sign of defeat or incompetence. It is
an emblem of triumph.

This reality is portrayed all throughout the Bible. For in-
stance, when Jacob, the father of Israel’s twelve tribes, wrestled
through the night at Penial, he limped ever after as a sign that he
had prevailed (Genesis 32:31).

The Apostle Paul, father of the Gentile church, was given a
thorn in the flesh. Since thorns grow along the ground, Paul was
pricked —at least symbolically —in the foot. It kept him limping
in the eyes of men (2 Corinthians 12:7). Even so, it was in this
weakness that Christ's power was affirmed and perfected (2
Corinthians 12:9).

Thus, when the church limps through history, as believers
we need not be frustrated or discouraged. On the contrary, we
