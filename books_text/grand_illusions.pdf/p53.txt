All that Glitters 53

Parenthood has been railing against for three-quarters of a cen-
tury —the very things that sex education courses are designed
to circumvent."

Planned Parenthood’s program of sex education is not, by
any stretch of the imagination, a necessary or effective means of
preventing teen pregnancies. Instead, it does just the opposite.
We simply cannot contend or pretend otherwise.

The Abortion Illusion

Planned Parenthood claims that its efforts to provide abortion services
have at last removed the specter of dangerous back-alley abortions from our
dand."** But that is an illusion.

The specter remains, darker and more ominous than ever
before.

The truth is, many of the butchers who ran the old back-alley
operations have simply moved uptown to ply their grisly trade
for Planned Parenthood. 1#5

The same unsafe techniques, the same lecherous motiva-
tions, and the same twisted and perverse ethics that marred their
criminal careers continue to haunt them."6 The 1973 Roe x. Wade
decision did nothing to change that. Planned Parenthood’s
“efforts” do nothing to change it, either.

Abortions are dangerous. Planned Parenthood’s own liabil-
ity release forms say so—in very fine print, of course.!#’ There is
no such thing as a “safe and legal” abortion. Legal, yes.48 Safe,
no way.1#9

Recently the Centers for Disease Control conducted a study
of maternal deaths and discovered that abortion is now the
sixth most common cause. The results of the study, released
in the journal Obstetrics and Gynecology, admitted that those
abortion-related deaths may be under-reported by as much as
fifty percent. 15°

According to a Johns Hopkins University study, nearly
twenty percent of all mid-trimester abortions result in serious
genital tract infections.'5! And a study conducted by two UCLA
obstetrical and gynecological professors concluded that “abor-
tion can be a killer,” due to “pelvic abscess, perforation of the
uterus, and sometimes also of the bowel.” 52 But even if such in-
fections and abscesses do not prove to be fatal, they can cause
