136 Granp ILLusions

cost of an initial cash-paid birth control visit is just over thirty-
two dollars.2* But when the taxpayer foots the bill, Planned
Parenthood charges an average of just over seventy-seven dol-
lars. An annual birth control check-up costs a cash client ap-
proximately forty dollars.2 But when the government pays,
Planned Parenthood charges more than eighty-five dollars.?”7 A
repeat visit costs a cash client just over twenty-five dollars.28 For
the same service, the taxpayer is charged almost thirty-six dol-
jars.29 A birth control supply visit costs an average of under
eleven dollars cash.3° But when that same visit is paid for out of
public funds, the cost averages more than thirty-five dollars.
When public hospitals, local communities, school districts,
county health departments, and state governments contract with
Planned Parenthood to perform abortions, the same pattern of
price inflation holds true.3? The cost of family planning services at
Planned Parenthood is directly related to who pays the bill —the
client or the taxpayer. When the taxpayer pays, the services are
between two and four times more expensive.33

Ifa defense contractor can get two hundred dollars out of the
Pentagon for a common ball peen hammer, then by golly it will—
all moral compunction aside, It is simply a matter of supply
and demand. Likewise, if Planned Parenthood can get sixty dol-
lars out of the welfare establishment for a cheap little rabbit test,
be assured it will.35 And just as the defense contractor robs the
treasury in the name of patriotism, so Planned Parenthood soaks
the taxpayer in the name of philanthropy.

Is it any wonder then that Planned Parenthood is so intent
on qualifying girls like Roxanne Robertson for government
grants and subsidies?

Highway Robbery Made Easy

Prior to 1981, states and organizations that wanted to partici-
pate in the various federal family planning programs provided by
the Tydings legislation and its prolific progeny were required to
submit a detailed spending proposal to the Department of
Health and Human Services. Only if and when they could dem-
onstrate legitimate need were funds approved. In addition, they
were obligated to report back to the department on a regular
basis how they spent the funds and how effective those expen-
ditures were.°6
