260 Granp ILLusions

vaunted greatness of its church, though steeped in the mighty
weigh of orthodoxy, is surely rooted in the power and suasion of
its prophetic proclamation from the pulpit.”#! The church taught
its people then. And, as a result, those people had built a mar-
velous culture with liberty and justice for all.

When Alexis de Tocqueville visited the United States in
1830, he had a similar experience. He attributed much of the
greatness he observed in the land to the vibrancy and relevancy
of the church’s prophetic role. He noted that virtually every
community in the vast new republic was indelibly marked by
“pulpits aflame with righteousness.”3? The church taught its peo-
ple then. And, as a result, those people had built a marvelous
culture with liberty and justice for all.

The prophetic church applies the Bible to every sphere of
life: art, music, ideas, government, education, medicine, his-
tory, economics, agriculture, and science.*3 It believes that “the
Bible is authoritative on everything of which it speaks.”34 And it
believes that “it speaks of everything.” It believes that “all Scrip-
ture is God-breathed, and is useful for teaching, rebuking, cor-
recting, and training in righteousness, so that the man of God
may be thoroughly equipped for every good work” (2 Timothy
3:16-17). It believes that “not one jot or tittle” has in any wise
passed from it (Matthew 5:18). It believes that it is “settled in
heaven” (Psalm 119:89), and “established on earth” (Psalm
119:90). It believes the Bible, and it teaches it systematically at all
times, “in season and out of season” (2 Timothy 4:2).

Jesus therefore was saying to those Jews who had believed
Him, “If you abide in My word, then you are truly disciples of
Mine; and you shall know the truth, and the truth shall make
you free” (John 8:31-32).

The prophetic church does not just feach the Bible though. It
also applies it. The word prophet literally means guide. Whenever
and wherever sin exists, the prophetic church is there: expos-
ing, rebuking, reproving, correcting—and guiding.

Let no one deceive you with empty words, for because of these
things the wrath of God comes upon the sons of disobedience.
Therefore do not be partakers with them; for you were for-
