I74 Gran ILLusIONs

Over the years, the New York Times, and its various ancillary
organs, has been the primary forum used by Planned Parent-
hood to spread its merciless message.®” Virtually every new
campaign, every new strategy, every new rhetorical ploy, and
every new emphasis has been launched in its pages.® Full-page
advertisements, op-ed articles, feature stories, editorial car-
toons, literature reviews, public service announcements, and
opinion page commentary have been scrupulously exploited for
the cause.®

The Washington Post. Like the New York Times, the privately-
held ‘Washington Post is a communications giant, with annual
sales of almost $600 million and more than five thousand em-
ployees.”° Besides its Capitol Hill flagship, it owns three news-
papers, including the International Herald Tribune.” It operates
television stations in several major markets.”? It owns its own
paper mill and a newsprint manufacturing company.7? Most im-
portantly, though, it publishes one of the two most influential
news magazines in the world: Newsweek.

Both the Post and Newsweek steered fairly clear of the abortion
issue throughout the sixties.”* With only a few obligatory reports
on various court cases and medical developments, the issue was
otherwise ignored.” But, then, with the dawning of the seventies,
they allied themselves unreservedly with Planned Parenthood.”*
They began to laud the heroics of pro-abortion advocates as
“humane and compassionate” and to denigrate pro-lifers as rabid
“missionaries” and “crusaders.””’ By the nineties, both publica-
tions had become overtly pro-abortion and anti-Christian.7®

Gannett. The nation’s largest. newspaper chain, with some
eighty-eight daily newspapers, was founded in 1906 by an up-
state New York entrepreneur, Frank Ganneit.’? By the time he
died in 1957, he had acquired twenty-three very profitable small-
town papers in five states.®¢ His successors built the company
from that small base into an international phenomenon. Today,
the company’s local newspapers have a combined circulation of
more than six million copies a day.*! Its shareholders have en-
joyed eighty-two consecutive quarterly earnings gains— more
than twenty years of uninterrupted growth in profits.®? It owns
thirty-two print sites besides its eighty-eight local news facilities,
including one in Switzerland and another in Singapore.* It has
seven television stations and twelve radio stations.®+ And it runs
