Back-Alley Butchers: The Medical Legacy 87

Where true Christianity is not practiced, true medicine can-
not be practiced. Where faith degenerates into faithlessness,
healing of necessity degenerates into killing. Where there are no
moral or ethical standards, there can be no basis for the nurture
and protection of life.

The great lesson of history is that Proverbs 8:35-36 is abso-
lutely and inescapably true: all those who hate God love death,
but he who finds God loves life.

Sexual Balance

Sex is good.

In its proper place. And in its proper perspective.

God created it. He endowed it with great value and benefit.
He filled it with beauty, dignity, glory, honor—and, of course,
dfe. He made it a multi-dimensional blessing, a lavish gift, and a
gracious inheritance for all those who have entered into the life-
long covenant of marriage.

Sex is good because it affirms and confirms that covenant
with intimacy and joy. The marriage bed is rich with pleasure. It
is romantic. It is lush with merriment and celebration. And this
recreational aspect of marital sex is never to be despised.

Sex is also good because it anoints the coyenant with perpe-
tuity. The great blessing of children is a special inheritance
bestowed by God’s own sovereign hand, This procreational
aspect of marital sex is never to be despised.

Sex is good. It is a celebration of life.

Of course, sex can be defiled, When it is ripped out of its

covenant context to be used and abused as an end in itself, it is
corrupted and polluted. Outside of the marriage covenant, sex
becomes a rude and crude parody of itself. It is sullied and
putrefied. It becomes a specter of death. The fact that Planned
Parenthood’s abortion and birth control programs create incen-
tives for premarital sex should be indictment enough.
_ But not only is sex defiled when it is stolen from the sanctity
of marriage, it is also defiled when it loses its delicate balance
between recreation and procreation. Any attempt to drive an
absolute wedge between those two God-ordained dynamics
diminishes the glory of the marriage bed.12+
