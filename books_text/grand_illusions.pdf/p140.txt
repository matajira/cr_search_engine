120 Granp ILLusIons

Defining the Terms

The irrepressible John Selden once quipped that “syllables
govern the world.”® If that is true, then the definers of words are
the most powerful of men.

George Orwell explored that notion, with chilling effect, in
his classic 1984.°* The book tells the story of a society where a re-
pressive bureaucratic elite attempts to manipulate the very
thoughts of men by controlling their language. Old words, with
comfortable, familiar meanings, are either scuttled into disuse,
or are redefined to fit the elite’s pernicious perspective. They are
either slyly sidled, or are emptied of their common significance,
only to be filled with some alien denotation. Thus, according to
this Newspeak, words like honor, justice, morality, science, and religion
cease to exist altogether, while words like war, peace, freedom, slav-
ery, and ignorance have their meanings completely transposed.

Orwell meant the book to be a warning.2® Like Rudyard
Kipling, he believed that “words are the most powerful drugs
used by mankind.”9? Like Tristram Gylberd, he believed that
“whoever controls the language controls the culture.”®8 And, like
John Locke, he believed that “whoever defines the words defines
the world.”®® Thus, he implored his readers to beware of logo-
gogues—word tyrants. He warned us to resist the seductive
allure of lexographic molesters.

Sadly, we have failed to heed that warning. It seems that we
are presently witnessing the emergence of our own Newspeak.
Allan Bloom, author of The Closing of the American Mind, argues
that we have begun to develop “an entirely new language of good
and evil, originating in an attempt to get beyond good and evil,
and preventing us from talking with any conviction about good
and evil,”100

Not surprisingly, Planned Parenthood has been one of the
primary practitioners of this Newspeak.!©! By manipulating cer-
tain words, Planned Parenthood has attempted — and, in all too
many cases, succeeded—to manipulate reality.

The word responsible was once synonymous with frust-
worthy. A responsible person could be counted on to uphold his
commitments and fulfill his obligations. 9? Now, though, accord-
ing to Planned Parenthood’s Newspeak, responsible simply means
“to use birth control” during illicit sexual liaisons.‘
