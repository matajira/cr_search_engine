Select Bibliographic Resources 341

Derek Humphrey, Let Me Die Before I Wake: Hemlock's Book of Self-Deliverance
Jor the Dying (New York: Hemlock/Grove, 1984}.

Jean Johnson, Natural Family Planning (London: Catholic Truth Society,
1981).

Everett C. Koop, To Live or Die? Facing Decisions at the End of Life (Ann
Arbor, MI: Servant Books, 1987).

Gerald A. Larue, Euthanasia and Religion (Los Angeles, CA: Hemlock
Society, 1985).

Lane P. Lester with James C. Hefley, Cloning: Miracle or Menace
(Wheaton, IL: Tyndale House, 1980).

Robert J. Lifton, The Nazi Doctors: Medical Killing and the Psychology of
Genocide (New York: Basic Books, 1986).

Samuel A. Owen, Letting God Plan Your Family (Westchester, IL: Cross-
way Books, 1990).

Ron Paul, Abortion and Liberty (Lake Jackson, TX: The Foundation for
Rational Economics and Education, 1983).

Franklin E. Payne, Jr., Biblical/Medical Ethics (Milford, MI: Mott
Media, 1985).

Franklin E. Payne, Making Biblical Decisions (Escondido, CA: Hosanna
House, 1989).

Charles D. Provan, The Bible and Birth Control (Monongahela, PA:
Zimmer, 1989).

Suzanne M. Rimi, Beyond Abortion: A Chronicle of Fetal Experimentation
(Avon-by-the-Sea, NJ: Magnificat Press, 1988).

Barbara Seaman, The Doctor's Case Against the Pill (Garden City, NY:
Doubleday, 1980).

Thomas A. Shannon and Jo Ann Mantra, Law and Bioethics: Texts with
Commentary on Major U.S. Court Decisions (Ramsey, NJ: Paulist Press,
1982).

Howard I. Shapiro, The Birth Control Book (New York: Avon, 1977).

Beth Spring and Ed Larson, Euthanasia (Portland, OR: Multnomah,
1988),
