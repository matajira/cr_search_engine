56 Granp InLusions

all, we should reprobate specific remedies for ravaging diseases;
and restrain those benevolent, but much mistaken men, who
have thought they were doing a service to mankind by projecting
schemes for the total extirpation of particular disorders.5°

Malthus’s disciples — the Malthusians and the Neo-Malthus-
jans—believed that if Western civilization were to survive, the
physically unfit, the materially poor, the spiritually diseased, the
racially inferior, and the mentally incompetent had to be elimi-
nated. And while Malthus was forthright in recommending plague,
pestilence, and putrification, his disciples felt that the subtler ap-
proaches of education, contraception, sterilization, and abortion
were more practical ways to ease the pressures of over-population.

As historian Paul Johnson has shown, the Malthusians “were
not men of action.”>! Instead, “they tried to solve the problems of
the world in the quiet of their studies, inside their own heads. . . .
They produced a new vocabulary of mumbo-jumbo, It was all
hard-headed, scientific, and relentless.”5? Even so, their doctrines
were immensely appealing to the intellectual elite. According
to Johnson:

All the ablest elements in Western society, the trendsetters in
opinion, were wholly taken in by this monstrous doctrine of
unreason. Those who objected were successfully denounced as
obscurantists, and the enemies of social progress. They could
no longer be burned as heretical subverters of the new orthodoxy,
but they were successfully and progressively excluded from the
control of events.53

This, despite the fact that the Malthusian mathematical
scheme had been proven by historical verities to be utterly
obsolete, if not entirely false.5+

Margaret immediately got on the Malthusian bandwagon.
She was not philosophically inclined, nor was she particularly
adept at political, social, or economic theory, but she did recog-
nize in the Malthusians a kindred spirit and a tremendous op-
portunity. She was also shrewd enough to realize that her notions
of Radical Socialism and Sexual Liberation would not ever have
the popular support necessary to usher in the revolution without
some appeal to altruism and intellectualism. She needed somehow
to capture the moral and academic “high ground.” Malthusianism,
