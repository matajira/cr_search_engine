‘This book is filled with such pertinent information about Planned Parenthood that readers
will be astounded, The research is impeccable, in depth, and absolutely surprising,

—Charles A. Provan, M.D.

 

Grand Tusions is the book for anyone who thinks, or knows anyone who thinks, that
Planned Parenthood is a fine, high-minded organization. In this award-winning book,
George Grant traces not on-y the history of Planned Parenthood, but goes behind the
scenes to shaw you in person the wreckage it is causing in young women's lives right now.

~Mary Pride, Author and Speaker

 

Planned Parenthood is gnilty of moral crimes so ghastly they are scarcely believable.
‘That’s why the gripping case presented in Grand Ilusions is must reading. The horrify-
ing facts behind the abortion and sex education industry simply must be exposed, and
George Grant does the job in spell-binding fashion.

Marlin Maddoux, Host, Point of View

 

Grand Hlusions is by far the best exposé of Planned Parenthood ever written, Everyone
shouid read this book. Everyone should use it. The only way for truth to triumph is if
the illusion is shattered. And this book shatters the iilusion.

—Joseph M. Scheidler, Director, Pro-Life Action League

 

Planned Parenthood has spent millions to erect a false public relations front, Hundreds
of journalists have cooperated. Not George Grant. With passion and accuracy, he
courageously knocks down the screen to reveal what everyone who cares about life
should know.

Marvin Olasky, Ph.D., Professor of Journalism, The University of Texas

 

Tam horrified to read the extensive documentation about the anti-family strategies of
Planned Parenthood, Not before I read this book was I aware of the abundance of
amoral information being distributed to our kids at (axpayer’s expense. It is hard to
believe what is at the very heart of the multi-billion dollar Planned Parenthood move-
ment. You need to read Grand Illusions for yourself — the documented facts speak loudly.

—Josh McDowell, Author/Youth Communicator

 

Grand Iusions is a dramatic and challenging wake-up call to complacent Christians. As
an attorney who has fought on the defending side against Planned Parenthood’s well
bankrolled lawsuits, I am painfully aware of their efforts to suppress pro-life speech,
protest, or abortion regulation,

—Paige Comstock Cunningham, Attorney

 

George Grant exposes how U.S. tax dollars have been used under Democrat and
Republican presidents to subsidize the Dr. Mengeles of the pro-abortion movement.

—Howard Phillips, President, The Conservative Caucus
