Selling Sex: The Educational Scandal 113

teens,” In one “learning activity” for these “leaders-in-training,”
Planned Parenthood recommends “Brainstorming all the terms
used for penis, breast, intercourse, vagina, homosexuality, and
VD. . . . This will familiarize group members with all forms of
sexual terms they might hear from their peers and should lower
their shock value. This exercise also helps set an atmosphere for
questions in training sessions, in effect giving students permission
to discuss sexuality in whatever terms they are most familiar.”38

Walt Maxwell was a teen trainee in a Peer Facilitator pro-
gram sponsored by Planned Parenthood in Northern Virginia.
Briefly. “I only lasted a week in the program,” he told me. “I just
couldn’t handle it. Watching porno films and talking dirty is not
exactly my idea of a healthy extra-curricular activity.”

After he dropped out of the program, he was called in to talk
to his school counselor and two assistant principals. “They
wanted to know why I had such a bad attitude about the class, and
why I was being so uncooperative. I told them that I thought the
whole program was disgusting. They just looked at me like I was
from another planet or something.”

Sensitivity Training. Planned Parenthood often uses small,
informal discussion groups to “raise the sexuality awareness of
children.”39 Using the “social pressure” of carefully designed
classroom situations, teachers are able to break down “home
training” and then to instill the precepts of “the new
morality’—the amoral morality of Margaret Sanger’s sexual
revolution.*°

In one Sensitivity Training program, the “teacher-change
agent” is instructed to divide students into small groups, giving
each an envelope containing cards with topics to be discussed:
“Virginity, Oral-Genital Sex, Intercourse, Masturbation, Steril-
ity, Group Sex, Homosexuality, Extra-Marital Relations, Abor-
tion, and Nudity—with acquaintances, with family, with the
opposite sex, with the same sex, and with close friends.”#*

The students are “to identify and express their present attitudes
and feelings about these matters and to practice active listening and
honest self-disclosure.”*? Once this “self-disclosure” process is com-
plete, the group is to “bring consensus by winning over other
members.”43 Those members of the group who refuse to change
“are considered non-conformists or deviants.”*+
