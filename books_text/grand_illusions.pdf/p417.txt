39.

54.
55.
56.
57.
58.
59.
61.

62.

63.

End Notes 397

See Frances Moore Lappe and Joseph Colline, World Hunger: Twelve Myths

(New York: Grove Press, 1986).

. See George Grant, “The Reclamation of Biblical Charity? in Christianity and
Civilization: The Reconstruction of the Church, James B. Jordan, ed., 1985, pp.
299-525,

. See Ruth Sidel, Women and Children Last: The Plight of Poor Women in Afftuent
America (New York: Penguin, 1986), p. 3.

. Lid.

. Ibid.

. Wid. p. £.

. Bid.

. Lbid.

. Bid. p. Ml.

. See Grant, Shadow, pp. 123-128,

. Did. p. 136-158.

. Lenore J. Weitzman, The Divorce Revolution: The Unexpected Social and Economic
Consequences for Women and Children in America (New York: Free Press, 1985),
p. xii,

. Wid.

. Lid.

. Sylvia Ann Hewlett, 4 Lesser Lifer The Myth of Women’s Liberation in America (New

York: William Morrow, 1986), p. 14.

Ibid.

See Grant, The Dispossessed, pp, 712-83.

See Joni Eareckson Tada, Friendship Unlimited: How You Can Help a Disabled

Friend (Wheaton, IL: Harold Shaw Publishers, 1987).

Joni Eareckson Tada and Gene Newman, All God's Children (Grand Rapids,

ME: Zondervan, 1987), p. 22.

Ibid. p. 32.

Ibid., p. 43.

. [Lis actually God who changes things as we humble ourselves before Him. But

prayer is the method He has chosen to effect that humbling process.

Sce James B. Jordan, The Sociology of the Church (Tyler, TX: Geneva Ministries,

1986) pp. 279-282.

One of the best books on fasting, although it tends at times 10 lean toward

platonic pietism, is Arthur Wallis’s classic Ged’: Chosen Fast (Fort Washington,

PA: Christian Literature Crusade, 1968). Other books with excellent insights

into fasting include Connie Marshner, Decent Exposure (Brentwood, TN:

Wolgemuth & Hyatt Publishers, 1988); and Alexander Schmemann, Great Lent

(Crestwood, NY: St. Vladimit’s Seminary Press, 1969).

. C. 8. Lewis, The Weight of Glory and Other Addresses (Grand Rapids: Wm. B.

¥erdmans, 1965), p. L.

 

Chapter 15—This Too Shall Pass

1, “From start to finish,”

2

. Hilaire Belloc, The Path ty Rome (London: Penguin Books, 1902, 1957), p. 192.
