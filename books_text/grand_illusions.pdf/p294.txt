274 Granp InLusions

In Colautti v. Franklin, in 1979, the Court struck down a
Pennsylvania statute creating a standard for the determination
of the viability of unborn children.

In Bellotti v. Baird, also in 1979, the Court invalidated a Mas-
sachusetts statute requiring parental consent for minor children
receiving abortions.

In Akron v, Akron Center for Reproductive Health, in 1983, the
Court struck down a city ordinance requiring all second and
third trimester abortions to be performed in hospitals. It also in-
validated a statute requiring abortionists to inform their patients
of the medical risks of abortion and of possible alternatives to
abortion, It argued, in essence, that the “pro-choice movement”
need not provide women with choices.

In Planned Parenthood of Kansas City v. Ashcroft, also in 1983,
the Court invalidated a state statute requiring second trimester
abortions be performed in hospitals.

In Thornburgh v. American College of Obstetricians and Gynecolo-
gists, in 1986, the Court struck down all mild forms of abortion
regulation, including restrictions for informed consent, informa-
tional reporting requirements, and performance of abortions
after viability. The decision, for all intents and purposes, made
abortion the only completely unregulated surgical procedure in
the United States.

In Webster v, Reproductive Services, in 1989, the Court opened
the door for state perogative in regulating abortion facilities.
Heralded as a victory for “anti-choice” forces by the media, the
ruling in fact did not strike down a single provision in Ree or
create any form of restriction on child-killing procedures.

In Rust v. Sullivan, in 1991, the Court upheld the right of the
federal government to determine who could and who could not
receive Public Health Service block grants. Again, though widely
advertised as a “pro-life” victory, the decision did not strike down
any aspect of the childkilling standards of Roe.

In Planned Parenthood v. Casey, in 1992, the so-called “conserva-
tive” Court actually reinforced and upheld the child-killing-on-
demand principles of Roe while at the same time confirming the
“compelling interest of the various states.”

The great liberties that we enjoy in America have been secured
against the arbitrary and fickle whims of men and movements
