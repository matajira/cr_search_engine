79.
. Tbid., pp. 52-53.
at.
82.
83.
. Did., p. 77.
. Did.

, Hid, p. 340,
a7.
88.

89.
90.
91.
92.
93.
94.
95.
96.
97.
. Schaeffer, Anger, p. 27; also see Marvin Olasky’s marvelously balanced and his-

106.
107.
108.

109.
110.
Wi.
112.
113,
14,
115.
116,

117.
118.
119.
120.
121,
122.
123,
124,
125.

End Notes 383

Prichard, pp. 51-52.

Bid., p. 349.
Ihid., p. 348,
Tbid., p. 346,

Wall Streei Journal, February 4, 1988; and Deane, p. 3.
See Marvin Olasky, “Abortion Rights: Anatomy of A Negative Public Rela-
tions Campaign,” Public Relations Review, Fall 1987, pp. 12-23.

Standard and Poor's, p. 3706.

Tid,

Tid.

Ibid., p. 3707

Thid., p. 3706.

Thid

Nathanson, p. 69.
Ibid., p. 70.
Ibid.

torically grounded treatment of this subject in Prodigal Press.

. Olasky, “Abortion Rights,” p. 12.
100.
101.
102,
103.
104,
105,

Ibid. pp. 14-18,
USA Today, July 23, 1986; and Olasky, “Abortion Rights,” p. 4.

Olasky, “Abortion Rights,” p. t4.

Tid.

Ibid., pp. 14-15.

Ibid., and Faye Wattleton, Planned Parenthood Federation of America Preas
Release, January 22, 1987.

Olasky, “Abortion Rights,” pp. 15-16.

Sbid., p. 14,

“CPCs Under Fire,” Action Line Special Report 9:10, December 27, 1986; and
Olasky, “Abortion Rights,” pp. 12, 15-16.

Bid.

USA Today, July 23, 1986; Newsweek, September 1, 1986.

Olasky, ‘Abortion Rights,” pp. 16-17.

Bbid., p. 17.

 

Tbid., pp. 14, 20.
Newsweek, September 1, 1986.

Mark J. Leonard, Party Politics in the Batkan Age (New York: Plassium Press,
1982), p. 33.

Pro Vitas Europe, January 1991.

Carnaium UK Report, September 1, 1989.

Ibid,

Coral Springs Forum, March 15, 1990.

Time, August t2, 1991.

The Boca News, September 10, 1990.

Fort Lauderdale Sun-Sentinel, July 10, 1991,

Chemical and Enginsering News, March 11, 1991.

Time, February 26, 1990.
