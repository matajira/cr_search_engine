Preface to the Second Edition xvit

book with deafening silence. It has almost completely ignored
the book. In fact, only when it is absolutely forced to comment
on the issues raised by the book does it deign to acknowledge its
existence. And even then, it merely cynically sneers—dispatch-
ing ail its evidence with a semi-lethal barrage of name calling
and derision.

The only two official responses to the book from Planned
Parenthood came in a single issue of the organization’s in-house
newsletter, Insider.

The first response is a simple note of warning from Douglas
Gould, Planned Parenthood’s Vice President for Communications:

Please do not encourage Mr. Grant by agreeing to debate him
should he come to your town, With one exception he has not
appeared on any mainstream broadcasts, and his book has not
been given serious attention,>

That is the voice of desperation. It is aiso the voice of decep-
tion. Gould knows full well that innumerable national “main-
stream” broadcasts as well as print mediums have indeed profiled
me and my book—and that many more would like to, if only
they could get an opposing viewpoint from Planned Parenthood.
The bottom line is that he is exercising a bit of covert damage
control.

The second response purports to be a book review— despite
the fact that it is less than five paragraphs and three hundred
words long. Instead of dealing with the book substantively, the
author of the “review,” Planned Parenthood staffer Peter Grimaldi,
resorts to name calling:

Mr. Grant's book, like so many of its ilk, is a textbook example
of the paranoid style, filled with heated exaggeration, sus-
piciousness, and conspiratorial fantasy.®

No specifics. No countering evidence. No solid defenses. No
debating ideas, philosophies, or methodologies. Grimaldi be-
lieves that it is sufficient to simply assert the PPFA orthodoxy
and leave it at that:

“The claims made in this book,” he says, fare lies.””
