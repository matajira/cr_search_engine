End Notes 393

Chapter 13—Idols for Destruction: A Strategy

oe =

~

10.
i.
12.
13.
14,
15.
16.
17,
18,
19.
20.
21.
22.
23.
24,
25.

26.

for Prophets and Priests

. in this sign conquer?
. Hilaire Belloc, The Craise of the Nona (London: Constable & Co., Ltd., 1925),

p 242,

|. The Lakeland Post-Dispatch, October 14, 1986.
. Ibid.
. See Gary DeMar, Ruler of the Nations: Biblical Principles for Government (Ft. Worth:

Dominion Press, 1987).

. See John W. Whitehead, The Separation IMusion: A Lawyer Examines the First

Amendment (Milford, MI: Mott Media, 1977).

. See Tim LaHaye, Faith of Our Founding Fathers (Brentwood, TN: Wolgemuth &

Hyatt Publishers, 1987).

. See Rus Walton, One Nation Under God (Nashville, TN: Thomas Nelson

Publishers, 1987).

. Joseph Story, Commentaries on the Constitution (Philadelphia: Patriot Publications,

1833, 1967).

See John W. Whitehead, The Second American Revolution (Elgin, IL: David C.
Cook, 1982).

See Henry Van Til, The Calvinistic Concept of Culture (Philadelphia: Presbyterian
& Reformed, 1959).

See George Grant, In the Shadow of Plenty: Biblical Principles of Welfare and Poverty
(Nashville: Thomas Nelson, 1986).

See Dennis Peacock, Winning the Battle for the Minds of Men (Santa Rosa, CA:
Alive and Free, 1987).

See John Chrysostom, On Wealth and Poverty (New York: St. Vladimir's
Seminary Press, 1984).

See David Chilton, Paradise Restored: An Eschatology of Dominion (Ft. Worth:
Dominion Press, 1985).

See R. J. Rushdoony, The Nature of the American System (Tylet, TX: Thoburn
Press, 1978).

See R. J. Rushdoony, This Independent Republic (Tyler, TX: Thoburn Press,
1978).

See John W. Whitehead, The Stealing of America (Westchester, IL: Crossway
Books, 1983).

See Gary North, Unconditional Surrender (Tyler: Institute for Christian Econom-
ics, 1981, 1988).

See Francis A. Schaefler, A Christian Manifesto (Westchester, IL: Crossway
Books, 1981).

See Gary DeMar, Gad and Government: A Biblical and Historical Study (Atlanta:
American Vision Press, 1982).

See Otto Scott, The Secret Six: John Brown and the Abolitionist Movement (New York:
Times Books, 1979).

See Clarence B. Carson, The Sections and the Civil War (Greenville, AL: Ameri-
can Textbook Committee, 1985).

See Robert B. Webster, The Rise and Fall of the American Republic (Los Angeles:
Freedom Bookhouse, 1979).

See James Hitchcock, What Is Secular Humanism? (Ann Arbor, MI: Servant
Books, 1982).

See jon W. Whitehead, The End of Man (Westchester, IL: Crossway Books,
1986).
