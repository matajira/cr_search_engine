330 Granp ILLUSIONS

* Elasah Drogin, Margaret Sanger: Father of Modern Society (New Hope,
KY: CUL Publishers, 1980, 1986).

Peter Fryer, The Birth Controllers (New York: Stein and Day, 1965),

* Madeline Gray, Margaret Sanger: A Biography of the Champion of Birth
Control (New York: Richard Marek, 1979).

David M. Kennedy, Bisth Control in America: The Career of Margaret Sanger
(New York: Yale University Press, 1970).

Margaret Sanger, Afargaret Sanger: An Autobiography (New York: Dover
Publishers, 1971).

Margaret Sanger, The Pivot of Civilization (New York: Brentano’s Pub-
lishers, 1922),

Margaret Sanger, Woman and the New Race (New York: Brentano's
Publishers, 1920).

Mortimer Brewster Smith, Evangels of Reform (New York: Round
Table Press, 1934),

Life Facts

John Ankerberg and John Weldon, When Does Life Begin? And 39 Other
Tough Questions about Abortion (Brentwood, TN: Wogemuth & Hyatt,
1989).

Clifford E. Bajema, Abortion and the Meaning of Personhood (Grand Rapids,
MI: Baker Book House, 1974).

Jim Brooks, Origins of Life (Belleville, MI: Lion Publishing Corporation,
1985).

Colin Clark, Population Growth: The Advantages (Santa Ana, CA: Life
Quality, 1975).

Erma Clardy Craven, Abortion, Poverty, and Black Genocide (New York:
Archdiocese Community Relations, 1972).

John Jefferson Davis, Abortion and the Christian: What Every Christian
Should Know (Phillipsburg, NJ: Presbyterian and Reformed, 1984).

Paul deParrie and Mary Pride, Unholy Sacrifices of the New Age (West-
chester, IL: Crossway Books, 1988).
