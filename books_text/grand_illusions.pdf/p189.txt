The Camera Blinked: The Media Legacy 169

the “unreliable” perspective that the media has. What is bad is
its worldview.

God’s condemnation of ancient Israel came because “their
ways were not His ways, and their thoughts were not His
thoughts” (Isaiah 55:8). They did not have a worldview shaped
and informed by God’s Truth. Instead, they “did.what was right
in their own eyes” (Judges 21:25).

Similarly, the news media today is driven by a worldview
that is entirely alien to the Biblical standards of truth, justice,
mercy, and integrity. Its ways are not His ways, and its thoughts
are not His thoughts. Instead, it does what is right in its own eyes.
And, having eaten from the forbidden tree, it has become like a
god in its own sight, knowing good and evil (Genesis 3:5, 22).

According to the now famous study of the media conducted
by three political scientists, Linda Lichter, Robert Lichter, and
Stanley Rothman, the overwhelming majority of newsmen in
major media outlets are hostile. to Biblical values.!? Only eight
percent are regular church-goers.'® Eighty percent believe that
homosexuality is a perfectly acceptable alternative lifestyle.19
Eighty-four percent said that they did not have strong aversions
to adultery.2° Ninety-two percent oppose traditional family
structures.21 And a full ninety-seven percent take a clear pro-
abortion stand.?

Meanwhile, nearly seventy percent endorse the idea that the
media should actively promote its ideas, values, and perspectives.?4

As Frank Schaeffer has argued:

With such widespread agreement about basic issues, which can
only stem from the same philosophic outlook, it hardly takes a
conspiracy for the media machine to speak with one smother-
ing voice.2*

Even if we had not been told that the media was overwhelm-
ingly pro-abortion, we might have guessed it—after all, if it
looks like a duck, walks like a duck, and quacks like a duck, it
probably is a duck.

Thus, as the Los Angeles Times reported:

It’s not surprising that some abortion-rights activists would see
journalists as their natural allies. Most major newspapers support
