378

12.
13.
14,

Granp ILLusions

See Grant, In she Shadow of Plenty, pp. 52-53.

Ibid., pp. 54-55.

Cathy Fonseca Marshall, “A Few Rotten Recipients Spoil the Whole Fund,’
A.LL:, About Issues, September 1983, pp. 20-21.

. “Celebrating Seventy Years of Service,” 1986 Annual Report, Planned Parent-

hood Federation of America, p. 29 (computed by adding Federation and
Affiliate private contributions, bequests, endowments, and Institute funding).

. Michael Patrick Dugan, “United Way Funding of Abortion,” Texas Nurses for

Life News, Spring 1987, p. 1.

. dbid.
. Lbee

 

also see Richard D. Glasow, “A Look at PPFA Finances,” Ti Vietory Is
Won: Planned Pasenthood’s Abortion Crusade (Washington, DC: National Right to
Life Educational Trust Fund, 1981), p. 5.

. See “Stop United Way Support of Anti-Life Agencies,” information packet,

American Life League, 1986; and “March of Dimes” information packet,
American Life Lobby, 1985.

. See Madeline Gray, Margaret Sanger: A Biography of the Champion of Birth Control

(New York: Richard Marek, 1979), pp. 37-113.

. See David Kennedy, Birth Control in America: The Career of Margaret Sanger (New

Haven, CT: Yale University Press, 1970), pp. 112-170.

. Linda Gordon, Woman’s Body, Woman's Right: A Social History of Birth Control in

America (New York: Penguin Books, 1976), pp. 249-390,

. Donald Ben-Feinberg, Sangerizing the Negroes and Jews (New York: Shalom

Books, 1949), p, 38,

. Galatians 5:19-21; Jude 4; Romans 8:5-17.
. Gordon, pp. 301-340,

. Gray, pp. 373, 386.

. DBid., pp. 303-304.

. Bid, p. 427.

. Did, p. 423.

|. Fbid., pp. 426-427.

. Mbid., p. 430.

, Tbid., p. 441.

. Tid., pp. 430, 438.

. Diana Shaman, “Margaret Sanger: The Mother of Birth Control,” Coronet,

March 1966.

. Gray, p. 441.
. Julian L. Simon, The Ultimate Resource (Princeton, NJ: Princeton University

‘Press, 1981), p. 328.

. Gray, pp. 29, 37-39, 40-44, 52,
. Ibid., pp. 45, 47-52, 86, 78, 101.
. Bic
. Lbid., p. 438.

. Ben-Feinberg, p. 69.

, Gordon, pp. 341-390,

. The New American, April 21, 1986,

. Tid.

. Ironically, Planned Parentheod has not restrained itself to abide by the IRS

 

+ Pp. 37-39, 40-41, 47-52,

Coide’s 501(c)3 conditions. It continues to improperly pursue an aggressive pro-
grain of political lobbying. At the sare time, it has attempted to deny churches
and pro-life groups the same privilege, It reaps the benefits of tax exemption
without upholding its responsibilities. See “Church Batiles Challenges,” Ruther-
ford Institute News Release, January 2, 1988.
