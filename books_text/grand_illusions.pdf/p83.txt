Bad Seed: The Historical Legacy 63

that their founders and leaders passed evil onto them as their legacy,
as their inheritance (Genesis 9:25; Leviticus 18:24-25; Amos 1:3-12).

Similarly, the Moabites and the Ammonites were a rebel-
lious and improvident people. They railed against God’s Law
and God’s People. Why were they so defiant? Again, the Bible
tells us that their founders and leaders passed insurrection on to
them as their degacy, as their inheritance (Genesis 19:30-38; Num-
bers 2t:21-23; Amos 1:13-15; Amos 2:1-3). A seed will always
yield its own kind (Genesis 1:11). Bad seed brings forth bitter
harvest (Ezra 9:2; Isaiah 1:4; Isaiah 14:20). You reap what you
sow (Galatians 6:7). A nation or an organization that is sown,
nurtured, and grown by deceit, promiscuity, and lawlessness,
cannot help but be evil to the core (Hosea 8:7).

Planned Parenthood is a paradigmatical illustration of this
principle. Margaret Sanger’s character and vision are perfectly
mirrored in the organization that she wrought. She intended it
that way. And the leaders that have come after her have in no
wise attempted to have it another way.

Dr. Alan Guttmacher, the man who immediately succeeded
her as president of Planned Parenthood Federation of America,
once said, “We are merely walking down the path that Mrs.
Sanger carved out for us.”72 Faye Wattleton, president of the
organization during the decade of the eighties, has claimed that
she is “proud” to be “walking in the footsteps” of Margaret Sanger?
And the president of the New York affilliate is Alexander Sanger,
her grandson,7*

Thus, virtually everything that she believed, everything that
she aspired to, everything that she practiced, and everything
that she aimed for is somehow reflected in the organization and
program of Planned Parenthood, even today. The frightening
thing about Planned Parenthood’s historical legacy is that the
legacy is not just historical. It is as current as tomorrow morn-
ing’s newspaper,

Abortion. In her book Women and the New Race, Margaret
Sanger asserted that “the most merciful thing a large family can
do to one of its infant members is to kill it.”75 Today, Planned
Parenthood’s commitment to that philosophy is self-evident. The
organization is the world’s number-one abortion provider.’® It
has aggressively fought the issue through the courts.”7 It has made
killing infant members of large families its highest priority.”
Bad seed brings forth bitter harvest. The legacy continues.
