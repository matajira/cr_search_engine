128 Granp ILLusions

Conclusion

In his remarkable article entitled “The Fraud of Educational
Reform,” Samuel L. Blumenfeld has stated, “The more I read
what secular educators write these days, the more convinced I
become that their grasp of reality has slipped beyond retrieval.”1
The schools have failed so miserably in accomplishing their basic
tasks—teaching our children how to read and write and com-
pute and complete and compete—that Blumenfeld says those
educators know that they will have to reform their precious sys-
tem. But what will that reform be like? Blumenfeld tells us that
we should “expect the next phase of educational reform to be
dominated by radical ideas disguised in pedagogic clothes. Such
phrases as critical thinking, emancipatory pedagogy, and master teach-
ers will sound benign to the public but will convey the right
message to the radicals.” Certainly that is what we saw in the
now defunct America 2000 reform proposals. “#5

Thus, reforming the present educational system is net the
answer. Planned Parenthood and the other radical educational
activists are so deeply entrenched in the public school machinery
that reform can only mean more of the same: more debauchery,
more brazenness, more humanism, and more wickedness.

The early promoters of public, state-controlled education
rallied around the slogan, “It costs less money to build school-
houses than jails.”“*° The great patriot-theologian of the South,
Robert L. Dabney, responded to this in 1876, saying, “But what if
it turns out that the state’s expenditure in school-houses is one of
the things which necessitates the expenditures in jails?”*7 To
which we might add: What if that expenditure also necessitates the
expenditure in AIDS hospitals, nationalized child care, and an ever-
burgeoning abortion industry?

The only hope for our children—and their children, and
their children’s children~to escape these horrendous hazards
are uncompromising, unwavering, unmitigating Christian
schools: Christian day schools and Christian home schools.‘
Christian educator Robert Thoburn has argued, “Salvation is by
grace, not by education.”!49 Even so, he says we have a “moral
obligation”45° to work hard, building up Christian schools"! and
restoring moral sanity to our nation.15?
