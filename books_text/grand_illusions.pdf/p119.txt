A Race of Thoroughbreds: The Racial Legacy 99

lower unwanted pregnancy rates is entirely unfounded, however.
A number of studies have demonstrated that as contraception
becomes more accessible, the number of unwanted pregnancies
actually rises, thus increasing the demand for abortion.6? And
since minority communities are the primary targets for con-
traceptive services, Blacks and Hispanics inevitably must bear
the brunt of the abortion holocaust.

A racial analysis of abortion statistics is quite revealing.
According to a Health and Human Services Administration
report, as many as forty-three percent of all abortions are per-
formed on Blacks and another ten percent on Hispanics.”° This,
despite the fact that Blacks only make up eleven percent of the
total U.S. population and Hispanics only about eight percent,”
A National Academy of Sciences investigation released more
conservative —but no less telling—figures: thirty-two percent of
all abortions are performed on minority mothers.?2

Planned Parenthood’s crusade to eliminate ail those “dysgenic
stocks” that Margaret Sanger believed were a “dead weight of
human waste” and a “menace to the race” has precipitated a
wholesale slaughter.’? By 1975, a littke more than one percent of
the Black population had been aborted.’ By 1980 that figure
had increased to nearly two and a half percent.’5 By 1985, it had
reached three percent.’6 And by 1992 it had grown exponentially
a full four and a half percent.’? In most Black communities today
abortions outstrip births by as much as three-to-one.7®

Milly Washington, Lanita Garza, and Denise Rashad at-
tended high school together in Minneapolis. Two years ago, the
district installed an experimental school-based clinic on their
high school campus. “At first I thought it was a real good idea,”
Denise told me.

“Yeah. Me too,” Lanita chimed in.

“I mean, there’s been lotsa girls that’s left school ’cause they got
in trouble,” said Denise, “and I believed this might help some.”

“But it hasn’t,” Milly said. “All it’s done is make it so gettin’ in
trouble is normal now.”

“And with an easy out,” Denise added.

“Yeah. Abortion. Its weird, but you know, a couple of years
ago I didn’t know anybody who'd had an abortion,” said Milly.
“Now it’s like everybody's had at least one. Lots have had two. Or
even more than that.”
