404

Paine, Thomas, 275
Palamas, Gregory, 231, 232-234
Paley, William, 171
parental consent, 26, 141-143
peer facilitation, 112-113
Pelvic Inflammatory Discase, 72, 81
persecution, 11-21, 240-244, 255-256,
282-287
Philippines, 196
phrenology, 57
Pill, the, 30-31, 31-32, 78-80, 89
Pincus, Gregory, 78-79
Planned Parenthood
abortion and, 23, 25-27, 33-34, 58, 63,
67-89, 126, 141-143
affiliation and, 1, 23
AIDS and, 30, 31-32, 128
as a gencric movement, 2
birth control and, 24, 27, 30-31, 67-89,
135-136
bureaucracy and, 279-281
charity and, 27-28, 149-164
choice and, 24, 25-27, 91-106, 126,
176-183
church and, 207-223
coercion and, 25-27, 126
courts and, 24, 138-141, 273-279, 350
deception and, 24, 32, 39-41, 65, 70,
126, 145-147, 165-187
education and, 1, 24, 26, 32-33,
107-129, 265
finances and, 1, 28-36, 118-119, 131-148,
149-164
health services and, 24, 65-89, 96-98
international activities of, 2, 23
lobbying efforts and, 24
media and, 24, 165-187, 267-269
medical malpractice and, 70-71, 126
misinformation and, 24, 26, 32, 39-41,
65, 70, 126, 145-147, 165-187
organization and, 1, 23
philosophy of, 1, 62-65, 91-106, 110-112
poor and, 24, 27-28, 302 os
population growth and, 29, 38-39
publishing.and, 24, 117-119, 126
racism and, 27-28, 91-106
reputation of, 24, 62, 118-119, 164,
165-187
sex education and, 32-33, 107-129,
145-147, 265
sexually transmitied diseases and,

31-32

Granp ILLusions

social services and, 24, 96-98
socialism and, 64-65
taxes and, 28-30, 118, 133-136
teen pregnancies and, 30-31, 32-33,
64, 118-119, 125-126, 265
venereal diseases and, 31-32
Plato, 41, 209
Plutarch, 41
Poland, 196
Pomeroy, Wardell, 110
Ponzi, Charles, 139
poor women, 27-28, 133-134, 296-303,
377
Pope Paul VI, 198
population growth, 38-39, 55-56,
133-138
pornography, 54, 57
positive imaging, 115-117
post-abortion syndrome, 126, 183
Potts, Malcolm, 195
poverty, 27-28, 133-134, 296-303, 377
power, 244-247
prayer, 249-250, 303-306
Presbyterian Church U.S.A., 215
presidency, the, 289
priest, 261-264
pro-choice, 17, 25-27
procreation, 87-88
promiscuity, 31, 32-33, 0-111, 123-125
prophet, 259-261
Prostaglandin, 76
PTA, 129, 145, 152, 265-266, 317
public schools, 107-129, 264-267

racism, 27-28, 46, 55-59, 61, 91-106, 111,
220-221

Rand, Ayn, 206

rape, 71

rape kits, 71

Rather, Dan, 191

RGA, 159, 171-172

Reagan, Ronald, 26, 29, 139, 276,
289

Reardon, David, 88, 350, 360, 372

Reasoner, Harry, 173

recreation, 87-88

recreational drugs, 88

Reed, John, 50

Restell, Madame, 183-185

Reynolds, R. J., 159

Rivera, Geraldo, 168

Robbins, Frederick, 78
