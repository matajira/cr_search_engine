Selling Sex: The Educational Scandal 42

The word chastity was once synonymous with purity. A person
who practiced chastity could generally be considered virtuous and
modest.1°* Now, though, according to Planned Parenthood’s New-
speak, chastity is just “a stage in life,” a temporary “immaturity.7105

The word neutral was once synonymous with objective. A per-
son who took a neutral position on a subject could be counted on
to be impartial and teachable. Now, though, according to
Planned Parenthood’s Newspeak, neuéral refers to an “open-
minded,’ and “amoral” relativism.'°7

The word choice was once synonymous with freedom. The
right to choose protected men from every assault of life and
limb. Now, though, according to Planned Parenthood’s New-
speak, choice is “the right” of one person “to prevail” over another —
even to the point of death—whenever the fancy strikes.

The word feius was once synonymous with unborn child. A
fetus was universally recognized. as a baby, a blessing from
Almighty God.!!® Now, though, according to Planned Parent-
hoad’s Newspeak, fetus is nothing more than “disposable tissue,” or,
worse, a unique form of “venereal disease.”11!

The word gay was once synonymous with happy. If someone
was gay, he was cheerful, jolly, and content.'2 Now, though, ac-
cording to Planned Parenthood’s Newspeak, gay is a noun, not an
adjective, meaning a sodomizing homosexual.''3

The word morality was once synonymous with virtue. A moral
person could be counted on to act uprighily and with all
integrity."'* Now, though, according to Planned Parenthood’s
Newspeak, morality is an “outdated” and “judgmental” value sys:
tem rooted in “fear,” “prejudice,” and “ignorance,”1'5

The word relationship was once synonymous with friendship.
A person who had a relationship with another simply had a rap-
port with them.!!® Now, though, according to Planned Parent-
hood’s Newspeak, a relationship is an adulterous affair, or an occa-
sional opportunity for fornication.'7

This is the vocabulary that Planned Parenthood has grafted
into their sex education programs and literature. Even if those
books, pamphlets, films, and curricula did not openly en-
dorse perversion, promiscuity, and prurience, they would still be
damningly destructive just because of the way they use and con-
trol the language.
