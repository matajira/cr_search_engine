360

oe

~

10,

1.

12,

Granp ItLusions

. Planned Parenthood of Houston and Southeast Texas Minor Consent tv Abortion

Form,

. See David C, Reardon, Aborted Women: Silent No More (Westchester, IL: Cross-

way Books, 1988); Deborah Maine, “Does Abortion Affect Later Pregnancies?”
Family Planning Perspectives 1:2. (March/April 1979), pp. 98-101; and Thomas
Hilgers, Dennis J. Horan, and David Mall, New Perspectives on Human Abortion
(Frederick, MD! University Publications of America, 1981), pp. 45-181.

. Author interview conducted in Los Angeles, July 1987.
. Based on California obstetrical/gynecological tabulated responses to a five-year

statistical survey.

. Leslie Iffy, M.D., “Abortion Statistics in Hungary,” Obsietrics and Gynecology

45:15, 1975; Alras Klinger, “Demographic Consequences of the Legalization of
Induced Abortion in Eastcrn Europe,” Inlernational Journal of Gynecology and
Obstetrics, 8:680, 1970; A, Arvay, M, Gorgey, and L. Kapn, “La Relation Entre
Les Avortements et Les Accouchements Prematures,” La Review de La Francais
Gynecolog et Obstetricie 62:81, 1967, and O. Pohonka and J. Torok, “A Gestatios
Esemenyekj Alakulasa es a Koraszules-Kerdes Osszefuggese Hazankban 1934
es 1970 Kozott,” Orviatie Hetiliak a Hungary 117: 965, 1976; all cited in Luc
Segond, La Vie Etait La Lumiere (Bruxelles: Societe Biblique de Belgique, 1979),
pp. 131-132; also see Hilgers, Horan, and Malt, pp. 69-127; as well as Reardon,
pp. 103-105.

K. G. B, Edstrom, “Early Complications and Late Sequelae of Induced Abor-
tion: A Review of the Literature,” Bulletin of the World Health Organization
52:3332, 1975; and Y. Moriyama and ©. Hirokawa, “The Relationship Be-
tween Artificial Termination of Pregnancy and Abortion on Premature Birth,”
Harmful Effects of Induced Abortion (Tokyo: Family Planning Federation of Japan,
1966); all cited in Luc Segond, La Vie Etait La Lumiere (Bruxelles: Societe Bibli-
que de Belgique, 1979), pp. 131-132; also see Hilgers, Horan, and Mall, pp.
69-127; as well as Reardon, pp. 103-105.

S. N. Pautekakis, G. C. Papadimitriou, and 8. A. Dixiadis, “The Influence of
Induced and Spontaneous Abortions on the Outcome of Subsequent Pregnan-
cies? American Journal of Obstetrics and Gynecolog, 116: 799, 1973; and G.
Papaevangelou, et al., “The Effect of Spontaneous and Induced Abortion on
Prematurity and Birthweight,” The Journal of Obstetrics and Gynecology in the British
Commoniweatth 80: 418 1973; all cited in Luc Segond, La View Etait La Lumiere
(Bruxelles; Societe Biblique de Belgique, 1979), pp. 131-132; also see Hilgers,
Horan, and Mall, pp. 69-127; as well as Reardon, pp. 103-105,

J. A. Richardson and G. Dixon, “The Effects of Legal ‘lermination on Subse-
quent Pregnancy,” The British Medical Journal 1:1303, 1976; C. S. W. Wright, S.
Campbell, and J. Beazley, “Second Trimester Abortion After Vaginal Termina-
tion of Pregnancy,” Lancet 1:1278, 1972; and Lela Lampe, “Az Elso Terhesseg,”

Ooriatte Hetilizak a Hungary 119: 1331, 1978; all cited in Luc Segond, La Vie Etait
La Lumiere (Bruxelles: Societe Biblique de Belgique, 1979), pp. 131-132; also see
Hilgers, Horan, and Mall, pp. 69-127; as well as Reardon, pp. 103-105.

. 3. Harlap and A. M. Davies, “Late Sequelae of Induced Abortion: Complica-

tions and Outcome of Pregnancy and Labor,” American Journal of Epidemiology
102: 247, 1975; and WHO Task Force on the Sequelae of Abortion, “The Out-
come in the Subsequent Pregnancy,” Workshop on Risks, Benefits, and Controversies
in Fertility Control (Arlington, VA: Program of Applied Research on Fertility
Regulution, 1977); all cited in Luc Segond, La Vie Elait La Lumiere (Bruxelles:
Societe Biblique de Belgique, 1979), pp. 131-132; also see Hilgers, Horan, and
Mall, pp. 69-127; as well as Reardon, pp. 102-105.
