Idols for Destruction: A Strategy for Prophets and Priests 263

you drink, but there is not enough to become drunk; you put on
clothing, but no one is warm enough; and he who earns, earns
wages to put into a purse with holes.” Thus says the Lord of
hosts, “Consider your ways! Go up to the mountains, bring
wood and rebuild the temple, that I may be pleased with it and
be glorified,” says the Lord, “You look for much, but behold, it
comes to little; when you bring it home, f blow it away. Why?”
declares the Lord of hosts, “Because of My house which lies des-
olate, while each of you runs to his own house” (Haggai 1:1-9).

When we neglect the priestly role of the church, when we
neglect worship, all else goes to seed.

If our first response to Planned Parenthood—or any other
evil, for that matter —is political, or social, or organizational, or
litigal, or judicial, we are no better than they, for we have put
our trust in human action as the ultimate determiner of history.

The priestly church believes in the power of worship (Rev-
elation 8:1-8). It believes in the primacy of worship (John
4:23-24). It believes in the preeminence of worship (Mat-
thew 16:19). It believes that worship reorients us to God’s plan,
God's purpose, and God’s program (Psalm 73:1-28). It believes
that worship is the starting point for cultural transformation
(1 Timothy 2:1-4), for it is worship that ultimately brings about
the demise of the wicked and the exaltation of the righteous
(Psalm 83:1-18).

O come, let us sing for joy to the Lord; let us shout joyfully to
the rock of our salvation. Let us come before His presence with
thanksgiving; let us shout joyfully to Him with psalms. For the
Lord is a great God, and a great King above all gods, in whose
hand are the depths of the earth; the peaks of the mountains are
His also. The sea is His, for it was He who made it; and His
hands formed the dry land. Gome, let us worship and bow
down; let us kneel before the Lord our Maker. For He is our
God, and we are the people of His pasture, and the sheep of
His hand (Psalm 95:1-7).

The priestly church does not just perform worship though. It
also applies it. The word priest literally means guardian. Thus, a
priest is someone who guards. He protects. He preserves. He
stays the hand of destruction and defilement.
