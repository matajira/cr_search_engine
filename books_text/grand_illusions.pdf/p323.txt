Altars for Construction: An Agenda for the Future 303

(1 Corinthians 16:1-2). And we should give to those who have
ministered to us through gifts (Galatians 6:6). Giving is an
aspect of worship (Deuteronomy 16:10-11). Thus it should be
done with a cheerful heart (2 Corinthians 9:7), whether out of
prosperity (1 Corinthians 16:2), or paucity (2 Corinthians 8:2).
After all, giving is an investment in the Kingdom of God (Mat-
thew 6:19-21).

We can’t fight something with nothing. We can’t simply say “no”
to Planned Parenthood; we must say “yes” to the fullness of the
Christian faith and its disciplines. Like Chrysostom, Hus, and
Spurgeon, we must say “yes” to good works, We must say “yes”
to almsgiving.

Prayer
If we are going to conform ourselves to God’s holy and per-
fect will, we must pay heed to the eternal and established Word
of Truth.

The grass withers, the flower fades, but the Word of our Ged
stands forever (Isaiah 40:8).

The Law of the Lord is perfect, converting the soul; The testi-
mony of the Lord is sure, making wise the simple (Psalm 19:7).

The entrance of Your Words gives light; It gives understanding
to the simple (Psalm 119:130).

For the Commandment is a lamp, and the Law is light;
Reproofs of instruction are the way of life (Proverbs 6:23).

To ever go beyond Scripture would mean to evade the pur-
poses of God {1 Corinthians 4:6). That is why the heroes of the
faith were so driven to the discipline of prayer. Diligence in
prayer always grounds God’s people in a dependence on His
Word. Such was the case of David (Psalm 51:1-19), Nehemiah
(Nehemiah 1:1; 2:1), Jeremiah (Lamentations 5:1-22), Jonah
(Jonah.2:2-9), the disciples of Jesus (Acts 1:8-14), and the first
Jerusalem church (Acts 2:1-47).

A failure to seek God in prayerful fellowship leads invariably
to a violation of God’s Word and a rejection of His purposes. Such
was the case with Cain (Genesis 4:3-8), Korah (Numbers 16:1-35),
Balaam (Numbers 22:2-40), and Saul (1 Samuel 13:5-14).
