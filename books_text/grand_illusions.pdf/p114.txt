94 Granp ILLUSIONS

restrictive containment measures and established Eugenic asy-
lums.?2 Eugenics departments were endowed at many of the
most prestigious universities in the world.?3 Funding for
Eugenic research was provided by the Rockefeller, Ford, and
Carnegie Foundations.2+ And Eugenic ideas were given free
reign in the literature, theater, music, and press of the day.?

The crassest sort of racial and class bigotry was thus embraced.
against the bosom of pop culture as readily and enthusiastically
as the latest movie release from Hollywood or the latest hit tune
from Broadway. It became a part of the collective consciousness.
Its assumptions went almost entirely unquestioned. Because it
sprang from the sacrosanct temple of “science”—like Aphrodite
from the sea—it was placed in the modern pantheon of “truth”
and rendered due faith and service by all “reasonable” men.

Of course, not all men are “reasonable,” and so Malthusian
Eugenics was not without its critics. The great Christian apolo-
gist G. K. Chesterton, for example, fired unrelenting salvos of
biting analysis against the Eugenicists, indicting them for com-
bining “a hardening of the heart with a sympathetic softening of
the head,”?¢ and for presuming to turn “common decency” and
“commendable deeds” into “social crimes.”?” If Darwinism was
the doctrine of “the survival of the fittest,” then he said, Eugenics
was the doctrine of “the survival of the nastiest.”% In his
remarkably visionary book Eugenics and Other Evils, Chesterton
pointed out, for the first time, the link between Neo-Malthusian
Eugenics and the evolution of Prussian and Volkish Monism
into Fascist Nazism. “It is the same stuffy science,” he argued,
“the same bullying bureaucracy, and the same terrorism by
tenth-rate professors, that has led the German Empire to its re-
cent conspicuous triumphs.”29

But singular voices like Chesterton’s were soon drowned out
by the din of acceptance. Long latent biases heretofore held at bay
by moral convention were suddenly liberated by “science.” Men
were now justified in indulging their petty prejudices. And they
took perverse pleasure in it, as all fallen men are wont to do.5?

The Planned Parenthood Connection
Margaret Sanger was especially mesmerized by the scientific
racism of Malthusian Eugenics. Part of the attraction for her was
surely personal: her mentor and lover, Havelock Ellis, was the
