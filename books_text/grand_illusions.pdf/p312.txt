292 Granp [LLusions

followed him wherever he went. This despite the fact that he
readily denounced sin in high places, he unhesitatingly confronted
vice and corruption, and he fearlessly exposed the powers and
the principalities.

Why was he so popular? He seemed to break every homileti-
cal rule i the book. His messages were too long. His arguments
were too complex. His rhetoric was too harsh. And his counsel
was too demanding. How then was he able to so mesmerize the
crowds and to catalyze repentance?

According to Thrasymachos of Trace, a fifth-century church
historian, Chrysostom’s sermons were almost entirely Biblical
and thus had “all the vibrancy and authority and suasion of Holy
Writ itself.”4 He believed that “the dispensation of the Word to
the people was sacramental” and thus necessitated “a careful ex-
egesis of Scripture” as well as “a practical exposition of Scripture
every time he took to the pulpit.”5 Unlike many of the preachers
in his day, he refused to indulge in either “philosophical specula-
tion or recreational pleasantry.”® He avoided “pastoral mun-
dania and communal absurdia’” serving his listeners instead “the
pure meat of the Gospel, turning neither to the right nor the left,
distracted from his Holy Duty by neither clamoring urgency nor
beckoning tyranny.”

Chrysostom was a Biblical preacher. And no doubt that alone
set him apart from many, if not most, of his contemporaries.

Even so, that still does not adequately explain his great
power and popularity. And Thyrasymachos admits as much. “As
important as his Biblio-centricity was,” noted the historian, “it
was his spiritual estate that lent Chrysostom such anointing and
favor. . . . It was his personal commitment to the basic disci-
plines of the faith . . . almsgiving, prayer, and fasting.”*

John Chrysostom did not simply say “no” to sin and male-
volence; he said “yes” to the fullness of the Christian faith and
its disciplines.

Jan Hus also knew that you can’t fight something with noth-
ing. The great fourteenth-century reformer was, like Chrysos-
tom, renowned far and wide for his scintillating and prophetic
preaching. His sermons, first in the city of Prague and then in
the countryside surrounding Bohemia, attracted enthusiastic and
rapturous crowds. He was known as the “firebrand of the Czechs”
and renewal broke out spontaneously wherever he went. This
despite the fact that he openly challenged papal exclusivity, he
