The Camera Blinked: The Media Legacy M75

the largest outdoor advertising and billboard company in North
America.®5 But its greatest asset is USA Today, the phenomenally
popular national newspaper known for its bright colors, short ar-
ticles, breezy, upbeat style, and TV-shaped dispensers. Since it
was launched on September 15, 1982, USA Today has exploded
onto the American scene, attracting nearly five million readers a
day, more than any other newspaper in history.°6

Even though Gannett has become a new and powerful pres-
ence in worldwide communications, it has not carved for itself a
unique editorial niche. Thus, despite its attempt to produce
news with “an unrelenting cheerfulness” and a “journalism of
hope,” USA Today, and the other Gannett outlets, have toed the
Planned Parenthood line.’7 The militant pro-abortion tack of
Gannett’s papers have, at times, even surprised Planned Parent-
hood with their vehemence and intolerance.®8

Time-Life. With annual sales of nearly three billion dollars,
and more than 25,000 employees, Time-Life is the biggest, rich-
est, and most powerful of all the communications monoliths.®9

It owns and operates six magazines, including Life, Fortune,
Money, People, Sports Illustrated, and its flagship, Time.°° It runs
the Book-of-the-Month Club and the Quality Paperback Book
Club.*! Its other publishing ventures include the Little, Brown,
and Company, the New York Graphics Society, the St. Paul
Pioneer Press, and the Angelina Free Press.°? It operates the
Home Box Office cable network, as well as forty-one local cable
TV companies,%? It has a controlling interest in several trans-
portation businesses, mortgage companies, insurance concerns,
steel mills, music publishers, real estate agencies, hotel chains,
and computer software manufacturers.

Time first covered the abortion issue in 1965 with an openly
sympathetic article in the medicine section.®> But that was only a
hint of things to come. By 1967, it had burst out of the closet and
declared itsclf “unequivocally in favor of the repeal of restrictive
abortion laws.”9 From that day forward, Time has remained
resolutely in the Planned Parenthood vanguard with the rest of
the mega-press.9”

Frank Schaeffer has said: “Given the concentration of the
media’s power in relatively few hands, and their shared values,
