To the Uttermost: The International Legacy 419

sense hypocritical. “In the past when the Serbian bureaucrats
who controlled the Communist regime targeted certain undesirable
and unfit ethnic elements for extermination, the West actually
applauded their cutting-edge political-correctness,” said Ruza.
“But that was when the operation was carried out by Planned
Parenthood. Now that renegade vigilante and para-military
groups have taken it over, the West suddenly looks askance.”

“The same ethnic communities that are victimized by
genocidal tyranny in Bosnia, Herzegovina, Montenegro, and
Dalmatia today were long ago targeted by the sanitary totali-
tarianism of Eugenic abortion and Malthusian social-planning,”
said Nada. “In the end, both forms of prejudicial cruelty achieve
the same result.”

“When Tito invited Planned Parenthood to join the govern-
ment in order to form a special division of the Yugoslav Health
Service in 1967, immediate plans were made to target certain
sectors of our society. Besides troublesome concentrations of
Croats and Slovenes, the scattered Moslem, Catholic, Lutheran,
and Bohemian Reformed communities in Bosnia, Kosovo, and
Dalmatia were especially hard hit,” she explained.

Indeed they were. With a population of only 22 million,
Planned Parenthood-run government clinics averaged between
four hundred and eight hundred thousand abortions every year
during the last decade of centralized rule.? The ratio of abortions
to births was nearly four to one.® And some seventy percent of
those were performed on the targeted Yugoslav minorities out-
side Greater Serbia.?

“What people don’t realize is that this awful holocaust has
been going on for quite some time,” said Nada. “Now it is exe-
cuted with bombs and bullets; before it was executed with pills
and procedures. But either way, the undesirables are eliminated.
They suffer, regardless.”

They had reached the great stone gate of the Kamentia Vrata
where a busy shrine was illuminated by a blaze of votive candles.
Dozens of peasants, college students, priests, and shopkeepers
had gathered~as they did each dawn—for prayer and con-
templation. After a brief but tense silence that seemed to bear
the weight of untold sorrows, Nada spoke in flat, silent tones. “I
remember how my mother was ostracized, how my father was
