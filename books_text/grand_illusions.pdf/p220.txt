200 Granp ILLUSIONS

East, for instance, missionary. endeavors were practically non-
existent in China and paralyzed by persecution in Japan. In
India, the higher castes were virtually untouched by the Gospel,
and even the lower castes showed only transitory interest. The
Islamic lands were as resistant as always to the inroads of the
church. South America’s conversion to the conquistador’s Cath-
olicism was tenuous at best. And tropical Africa had proven to
be so formidable and inhospitable that Western settlements were
confined to a few small outposts along the coast. Clearly, Christi-
anity was still very much a white man’s religion.

There had been, of course, a few bursts of expansion. In
1453, a series of catastrophic events—~both good and bad—freed
European monarchs to cast their vision outward for the first time
since the early crusades. That year saw the defeat of Gonstantine
XI by Sultan Mohammed II —thus, at long last, bringing to an
end the storied Byzantine Empire. In addition, the Hundred
Years War between England and France ended, as did the wars
between Venice and Milan, Russia and Lithuania, and Prussia
and Poland, The Habsburgs and the Medicis were both bolstered
in their respective realms. And Guttenberg’s press in Mainz
altered the transmission of knowledge and culture forever with
the publication of the first printed book—a Bible.

Explorers began to venture out into uncharted realms.
Scientists began to probe long hidden mysteries. Traders and
merchants carved out new routes, new markets, and new ‘tech-
nologies. Energies that had previously been devoted exclusively
to survival were redirected by local magistrates into projects and
programs designed to improve health, hygiene, and the com-
mon good. Africa, India, China, Indonesia, and the Americas
were opened to exploration and exploitation. From colonial out-
posts there, a tremendous wealth of exotic raw resources poured
into European cities.

But despite all these advantages, European advances were
limited and short lived—and the Gospel made only halting and
sporadic progress. Internecine warfare and petty territorialism
disrupted — and very nearly nullified —even that much Christian
influence. From 1688—when William and Mary concluded the
Glorious Revolution in England by ascending to the throne,
Louis XIV canonized the iron-fisted notion of “Divine Right,”
