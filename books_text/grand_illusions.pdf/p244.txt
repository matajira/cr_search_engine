22¢ Granp ILLusIONs

specified their religious preference, they identified their local
church membership and pastor, !2+

The notion that it is primarily “rank heathens” or “flaming
liberals” who are aborting their future away simply doesn’t hold
up under the facts. The scandal of Planned Parenthood has be-
come the scandal of the church. Sowing the wind, it has reaped
the whirlwind (Hosea 8:7).

Achan in the Camp

Flushed with confidence following their spectacular victory
at Jericho, the people of Israel advanced to the much smaller,
much weaker city of Ai. So certain were they of another victory
that they sent up only a very small contingent to take the city
(Joshua 7:2-3).

Much to their surprise, however, the men of Ai routed their
vastly superior forces (Joshua 7:4-5).

Joshua and the leaders of the nation fell on their faces in fear
and trembling before God. ‘Trying to make rhyme or reason out
of the lopsided battle, they begged for an explanation (Joshua
7:6-9), And what was God’s reply?

The Lord said to Joshua: “Get up! Why do you lie thus on
your face? Israel has sinned, and they have also transgressed
My covenant which I commanded them. For they have even
taken some of the accursed things, and have both stolen and

deceived; and they have also put it among their own stuff’
(Joshua 7:10-11).

It seems that one man, Achan, had violated God’s specific
commands, hiding his sin in his tent, and thus had brought
judgment and condemnation upon the entire nation.

Ai was lost, and lives were lost. All because Achan was in the
camp. All because sin was in the tent.

There are any number of Christians today who are dumb-
founded in the face of our impotence against Planned Parent-
hood. They cannot understand why we are so constantly and
consistently defeated when we contend with its forces in our
schools, our courts, and our assemblies. But the reason is abun-
dantly clear. We cannot effectively assault the gates of Planned
Parenthood until we cast Achan out of the camp. We cannot
