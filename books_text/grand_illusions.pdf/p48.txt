28 Granp ILLusions

To this day the thrust of Planned Parenthood’s literature
focuses on the terrible burden that the poor place on the rich.7@ It
continually reminds us of the costs that welfare mothers incur for
the elite.7° It constantly devises new plans to penetrate Black,
Hispanic, and ethnic communities with its crippling message of
Eugenic racism.*° It seems that its only use for the deprived and
rejected is as bait for huge federal subsidies and foundation
grants. “If we must have welfare,” Sanger argued, “give it to the
rich, not to the poor.”®! Her organization has for years attempted
to translate that philosophy into public policy.

Among measures Planned: Parenthood has recently spot-
lighted in its literature are such things as the elimination of child
care, medical attention, scholarships, housing, loans, and subsi-
dies to poor families.*? In addition it has given voice to such no-
tions as maternity benefits being drastically reduced or even
eliminated, substantial, across-the-board marriage and child
taxes being imposed, and large families not being given preferen-
tial charitable relief.83

Planned Parenthood is not, by any stretch of the imagina-
tion, an advocate of the poor. It is instead a great oppressor and.
exploiter of the poor. Its image-conscious rhetoric of compassion
is a paragon of Orwellian Newspeak-double think. We simply
cannot contend or pretend otherwise.

The Private Funding Illusion

Planned Parenthood claims to be a privately funded, non-profit fam-
ily planning organization.** But that is an illusion.

First of all, Planned Parenthood is not an “organization” — it
is instead an association of more than three hundred separately
incorporated organizations worldwide.®5 Second, it is nof in-
volved primarily in “family planning” —it is instead involved in
“family banning.”®¢ Finally, and perliaps most importantly, it is
by no means “privately” funded, either.8”

The truth is, a vast proportion of Planned Parenthood’s fund-
ing at every level — from the local level to the international level —
comes right out of the American taxpayer’s pocket. It has become
for all intents and purposes an unofficial — and thus unrestrained
and unrestricted—branch of the federal government.

It is widely known that Planned Parenthood receives tens of
millions of tax dollars through the Title X appropriations of the
