10.
1.

12,
13.

14,
16.
17,
18.
19,
21.
22.
23.
24,
25,
26.
27.
28.
29.
31,
32.
33.
34,
35.
36.
37.
38.

39,
40.

41.

42,
43.

End Notes 369

. Alfred Kinsey, Sexual Behavior of the Human Male (Philadelphia: Saunders

Publishers, 1948).

William Masters and Virginia Johnson, Human Sexual Response (Boston: Lite,
Brown, and Co., 1966).

William Masters and Virginia Johnson, Human Serual Inadequacy (Boston:
Lite, Brown, and Co., 1970).

Alex Comfort, The Joy of Sex (New York: Simon and Schuster, 1974).

lan F. Guuumacher, Pregnancy, Birth, and Family Planning (New York: Signet,
1973).

Wardelt B. Pomeroy, Boys ana Sex (New York: Dell, 1981).

. Mary S. Calderone, Questions and Answers About Love and Sex (New York:

St. Martin’s Press, 1979).

Shere Hite, The Hite Report (New York: Dell, 1976).

Ruth Westheimer, Dr. Ruth's Guide to Good Sex (New York: Warner Books, 1983).
Sol Gordon, Ten Heavy Facts About Sex (Syracuse, NY: Ed-U Press, 1975).
Sheri Tepper, After Long Silence (New York; Bantam Books, 1987).

. Margaret Sanger, An Autobiography (New York: W. W. Norton, 1938).

See note number two above; and “Sexuality Alphabet,” Planned Parenthood
Federation of America, 1982.

Ibid.

Ibid.

Margaret Lundstrom, “Women Exploited by Planned Parenthood: From the
Classroom to the Boardroom, The Deception Continues,” Lifeways Newsletter,
April 1982, pp. 6-8.

Barrett L. Mosbacker, School-Based Clinics (Westchester, IL: Crossway Books,
1987), p. 79.

Lena Levine, “Psycho-Sexual Development, Planned Parenthood News, Summer
1953, p. 10.

Tepper, Perils.

Tepper, Combination.

Tepper, Robbery.

. Calderone and Jobnson, p. 226.

Leonard Gross, “Sex Education Comes of Age,” Lock Magazine, March 8, 1966.
Judges 21:25.

Sandalyn McKasson, “Sex and Seduction in the Classroom,” in Richie Martin,
ed., Judgment in the Gate: A Call to Awaken the Church (Westchester, IL: Crossway
Books, 1986), p. 102.

Barbara Marris, Change Agents in the Schools (Upland, CA: Barbara M. Morris
Report, 1979), p. 144.

Lleland Hindernan, Values Clarification: The Children Trap (Mortis, CA: Chris-
tian Awareness Publications, 1977), p. 42.

McKasson, p. 95.

Hindeman, p. 49.

Peer Education in Human Sexuality, Planned Parenthood of Metropolitan Wash-
ington, p. 16.

Hindeman, p. 48.

Carol Craig, Decisions About Sex (White Plains, NY: Planned Parenthood of
Westchester, 1975), p. 4.

Eleanor $. Morrison and Miln Underhill Price, Values in Sexuality (New York:
Hart Publishing Company and A & W Publishers, 1974), p. 100.

McKasson, p. 97.

Ibid., p. 98.
