68 Granp ILLusions

observe the city’s teeming crush of variety. And it had afforded
us a unique opportunity to talk. Anonymity and privacy are
never so available than when surrounded by thousands of
strangers on a New York subway.

Caroline was twenty-two when she had her first abortion.
Eight months later, she had another. “The first one seemed to go
just fine,” she told me. “There was a little bleeding and some
pain for the next few weeks. Nothing serious, though.”

But it was serious. That became readily apparent when she
went in for the second abortion. “There was quite a bit of scar
tissue in my cervix. The physician seemed hesitant at first, but
decided to go ahead with the procedure.”

That was not the last mistake that the doctor would make
that day. His sharp, blindly wielded curette inadvertently per-
forated Caroline’s already scarred cervix. When he inserted the
suction apparatus, it passed through to the body cavity. The
shearing force of the suction then seriously lacerated the bladder
and tore loose the right ureter —the tube that carries urine from
the kidneys to the bladder. The delicate parametrium and peri-
toneum membranes were ruptured and a pooling hematoma
surrounded the entire right renal system.

Completely unaware of the damage he had caused, the doc-
tor finished the procedure, sent Caroline to the recovery room,
and turned his attentions to other matters. After a forty-five
minute rest, he released her.

“I collapsed on the subway on my way home. I think I was in
shock,” she said, She was suffering from a lot more than shock.
An emergency room examination revealed heavy hemorrhaging
and leakage of urine per vaginam. Attendants rushed her into
the operating room where surgeons reluctantly performed an
emergency right nephrectomy and oophorectomy ~ the removal
of the right kidney and ovary. They also evacuated the hema-
toma and resectioned the torn endometrium.

“I spent about ten days in the hospital after that,” she told me
as we walked past the Juilliard toward the Hudson River. Those
ten days had cost her a place in the school’s renowned drama de-
partment. “But the worst was still yet to come.”

Over the next several weeks, Caroline suffered from recur-
ring abdominal pain, high fever, vaginal discharge, and abnor-
mal bleeding. She was scheduled for both a cystoscopy and a
laparoscopy and was once again admitted to the hospital. The
