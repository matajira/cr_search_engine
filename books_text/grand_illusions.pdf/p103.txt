Back-Alley Butchers: The Medical Legacy a3

Still undeterred, Robin’s counselor at Planned Parenthood
had her try still another spermicide. “I don’t know why I couldn't
get it through my thick skull that those things just aren’t safe.
When I got another infection—this time with fever, nausea,
headaches, and swollen lymph glands—I knew Id really blown it.”

Indeed she had. The back-to-back infections, though easily
treated, caused a good deal of scarring in Robin’s fallopian
tubes. Two years later when she and Bill decided that they
wanted to have children, she found that she couldn’t get preg-
nant. A fertility specialist informed her that if she ever wanted to
restore her fertility she would have to undergo reconstructive
surgery. “Can you believe it? I have to have surgery now because
those people at the clinic are handing out dangerous drugs. It
makes you wonder how they can possibly stay in business.”

Medicine and the Lost Legacy

The past one hundred years have been called “The Goiden
Age of Medicine.”8* And for good reason. It has been a period in
which mankind has gained “unprecedented insights into diseases
that for millennia have held millions of people in a cruel and un-
relenting grasp.”89 With a dizzying array of new technologies
and treatments at our every beck and call, we are now able to do
far more than our grandfathers could have ever imagined. The
miracles of organ transplants, software implants, and cybernetic
replicants now make it possible for us to help the blind to see, the
deaf to hear, and the lame to walk. With the advent of biomedi-
cal research, laser surgery, macro-pharmacology, fiber optic
scanning, and recombinant DNA engineering, has come new
hope. The afflicted are raised up, the broken and distressed are
sustained, and those once left for dead are somehow restored.
We can now do all this and more.

But sadly, amidst this great hymn of victory, a dissonant chord
has sounded. The immoral, barbaric, backward, and scandal-
ously unsafe medical practices of Planned Parenthood have sidled
their way into mainstream medicine. And thus the victory cele-
bration has been crashed with the shadowy figures of statistics
that do not lie~new diseases, new defects, new ways to die.

Medicine is a tool for the preservation of health and life.
Technology is a tool for the enhancement of productivity and
