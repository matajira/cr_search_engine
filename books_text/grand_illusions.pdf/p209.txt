TEN

TO THE UTTERMOST:
THE INTERNATIONAL
LEGACY

Pactum Serva!

 

The great cleavage throughout the world lies between what is with, and what is
against, the faith.2

Fitlaire Belloc

When Karl Marx and Frederick Engels began their Communist
Manifesto with the eloquent sentence, “A specter is haunting
Europe,” they were at least partly right.3 A specter was indeed
haunting Europe—though it was not quite the one that those
pampered armchair revolutionaries thought.

In fact, a specter was not just haunting Europe — it was vexing
the whole earth. And it still is.

Even apart from the contemporary brutalities of abortion,
infanticide, and euthanasia, the twentieth century has been the
bloodiest, cruelest, most destructive in all of human history.*
Genocidal atrocities have hideously pockmarked the best efforts
of modern humanistic societies with an unimaginable barbarism:
Auschwitz, Campuchea, Gulag Oranov, Entebbe, Tiananmen,
Addis Ababa, Tehran, Beirut, Hanoi, and Baghdad.

But the most devastating nightmare of all may yet still be un-
folding in the Balkans—in the mountainous territories once forcibly
federated together as the Socialist Federation of Yugoslavia.

Long a hot-bed of terrorism, torture, and triage, those stun-
ningly beautiful lands along the Adriatic—just east of Italy,
south of Austria, and north of Greece—are even now convulsing
under the strain of a bitter ethnic war that has tragically gripped

189
