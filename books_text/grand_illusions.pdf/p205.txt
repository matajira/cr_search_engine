The Camera Blinked: The Media Legacy 185

Late that year, he gave one of his top-flight reporters an
undercover assignment in order to expose the most prosperous
abortuaries and their. medical malpractice. The explosive story
that resulted, “The Evil of the Age,” by Augustus St. Clair,
launched the Times into a moral crusade that would not end
until pro-life legislation, outlawing abortion and protecting ~
women, was passed in every state in the Union.!7 Not only
that, it triggered a wave of support that engulfed much of the
rest of the media, and saw to the prosecution of the most offen-
sive abortionists, including Lohman.!7!

Clearly, the media has come a long, long way on the down-
grade in the last one hundred years.

The Cloak of Conspiracy

Absalom was the passionate third son of David, King of
Israel. His personal comeliness and charisma was matched in
greatness only by his undisciplined ego and ambition. Thus, he
was forever getting himself into trouble and embroiling the pal-
ace in controversy and scandal (2 Samuel 13:38-39; 14:28).
When finally his father received him back into favor, the old king
was repaid by a plot against his throne.

And Absalom used to rise early and stand beside the way to the
gate; and it happened that when any man had a suit to come to
the king for judgment, Absalom would call to him and say,
“From what city are you?” And he would say, “Your servant is
from one of the tribes of Israel.” Then Absalom would say to
him, “See, your claims are good and right, but no man listens
to you on the part of the king.” Moreover, Absalom would say,
“Oh, that one would appoint me judge in the land, then every
man who has any suit or cause could come to me, and I would
give him justice.” And it happened that when a man came near
to prostrate himself before him, he would put out his hand and
take hold of him and kiss him. And in this manner Absalom dealt
with all Israel who came to the king for judgment; so Absalom
stole away the hearts of the men of Israel (2 Samuel 15:2-6).

Playing the part of the people’s advocate, Absalom stole
away their hearts. With delicious whisperings and twisted mur-
murings he plied circumstances in his favor. With great skill and
