PART FOUR

RESOURCES

The worship of will is the negation of will, To admire mere choice is to refuse
to choose. You cannot admire will in general, because the essence of will is
that it is particular, Every act of will is an act of self-limitation. To desire
action is to desire limitation. In that sense every act is an act of self-sacrifice.
When you choose anything you reject everything else.

G. K. Chesterton
