Back-Alley Butchers: The Medical Legacy 69

exploratory surgeries revealed that a portion of the fetal skull
had been imbedded into the resected intra-abdominal tissue.
They also revealed a severe pelvic inflammation caused by
bacteria from the mangled renal system.

“The doctors said that I had ne choice but to undergo a com-
plete hysterectomy.” Tears began to well up in her eyes. “I was
only twenty-two. My whole life was ahead of me. I was happy.
Carefree. And then this . . . well, I just couldn’t believe that this
was actually happening to me.”

The next day, the doctors removed Caroline’s remaining
ovary, along with her uterus, cervix, fallopian tubes, and lymph
glands. She would never again be able to bear children.

“The counselors at Planned Parenthood told me that abor-
tion was the only responsible choice in my situation,” she said.
“Now look at me. My health is broken. My career is ruined. My
emotions are shot. And the only two children Ill ever bear are
dead and gone.”

By now we were looking out over the dark roiling waters of
the Hudson. Angry swells broke over the old stone walls at the
river’s edge, drenching the sidewalk with a bone-chilling spray.
The blaring sounds and glaring lights of the city had receded
into the background as the grey turbulent channel before us
filled up our senses.

After a long and uncomfortable silence, Caroline turned to
me, her tears no longer contained. “Why didn’t someone tell me?
Why didn’t anyone tell me that abortion wasn’t safe? Why?”

Why indeed?

The Medical Risks of Abortion

While Planned Parenthood continues blithely promoting
their “safe and legal” abortions,’ thousands of women just like
Caroline Ness all across America and around the world suffer
from the “inherent risks”* and “complications”5 that those pro-
cedures present.6 For some that suffering is but a minor and
temporary inconvenience. For others, like Caroline, it becomes
a permanent disability.

Dr. Horton Dean is a gynecologist with a private practice in
a fashionable neighborhood near Los Angeles. Since 1973 he has
seen a marked increase in the number of patients with signifi-
cant complications—both mental and physical—as a result of
legal abortions. “I am convinced,” he told me, “that the Planned
