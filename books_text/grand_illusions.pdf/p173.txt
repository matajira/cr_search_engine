Strange Bedfellows: The Institutional Scandal 153

Unfortunately, we are se willing to give, that we fail to ask
enough questions. Or we fail to ask the right questions.

And that is bad.

The fact is, huge sums of the money we faithfully donate
every year to those charitable organizations wind up in the
hands of Planned Parenthood.!+ To the tune of seventy million
dollars.'® About one-third of that comes from United Way and
other community chest combined charities.!6 Another one-third
comes from corporate grants and foundation bequests.” And the
final one-third comes from miscellaneous endowments, match-
ing gifts, individual donations, and direct-mail solicitation.'®

Most people simply don’t realize that when they give they
are actually aiding and abetting Planned Parenthood’s Eugenic
agenda. Only occasionally does someone —like Dana Meier—ask
questions, raise objections, and voice concerns. Only occasionally
is the conventional wisdom — that donations will be applied where
most needed—actually challenged. And, thus, only occasionally
do charitable contributions end up going where they were origi-
nally intended.'9

Program Sweepstakes

From its earliest days, Planned Parenthood wooed corpora-
tions, foundations, celebrities, and charities in the hopes of
securing operating capital.

Margaret Sanger rubbed shoulders and shared beds with the
radical chic in the roaring twenties—the artists, actors, writers,
musicians, and activists in New York’s Village and London’s
Fabian Enclave.” She shrewdly used her proximity to them to
promote her revolutionary ideas.?! And she carefully networked
with them to gain contacts in the political and financial world.??

Single-minded in her commitment to the cause, her per-
sistence and unflagging enthusiasm began to open doors. She
was tireless and driven. Some even said she was “possessed”? ~
which, no doubt, she was.?4 At any rate, her crusade quickly be-
came a cause célébre. By the thirties, corporation grants and foun-
dation bequests began to pour the money into her coffers.?5 By
the forties, she had won the endorsements of such notables as
Eleanor Roosevelt? and Katherine Hepburn.?’ By the fifties,
she had attained international renown and counted among her
supporters Julian Huxiey,?® Albert Einstein,?? Nehru,®° John D.
