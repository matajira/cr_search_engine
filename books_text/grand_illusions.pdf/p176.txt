156 Granp ILLusIoNs

to see if there was any possibility of dropping that support,” he
said. “But they were all thoroughly committed to maintaining
the status quo. The thing is, they were very defensive. It was like
this subject had already been driven into the ground and they
weren't even willing to discuss it any more.”

Of course, they didn’t want to lose Jim altogether, so they
proposed an alternative. “They told me that if I didn’t want my
donation to go to Planned Parenthood,” he remembered, “all I
had to do was to ask for a negative designation. They really wanted
me to stay with the program. The trouble with negative designa-
tons, though, is that Planned Parenthood gets a set percentage
of the United Way total. That is pre-arranged. So, no matter

~ how I designate or don’t designate, the very fact that ’'ve made a
United Way contribution raises the total that Planned Parent-
hood gets. Negative designations are, thus, a sham. My protest
would be irrelevant.”

Frustrated, Jim pulled his company out of the program. “The
whole reason I worked so hard for years to raise money was to
help people. To learn that my efforts were actually having the ap-
posite effect was terribly sobering. Now, every time I see one of
those United Way ads during football games, I have to wonder
how many other folks there are in the same boat— wanting to help,
but doing it in a completely misbegotten fashion. It is tragic.”

The March of Dimes

Founded in 1938 by President Franklin D, Roosevelt, the
March of Dimes is one of the world’s premier private-sector health
and medical associations.®! Dedicated to the prevention of birth
defects, it raises nearly millions of dollars each year for education,
research, and service. It works to improve maternal and newborn
health. It makes basic clinical grants to hospitals and univer-
sities for perinatal and genetic study programs.® It sponsors med-
ical conferences, coordinates symposia, and publishes literature.®*
Since it successfully led the fight to cure polio during the early
fifties, the March of Dimes has become a symbol of hope for mil-
lions of parents all around the globe.®

But it has also placed itself at the forefront of the Planned
Parenthood movement.5*

Since the early sixties, it has increasingly turned its attentions
away from curing genetic disorders and birth defects to
