Strange Bedfellows: The Institutional Scandal 161

was also the era of graduated taxes and heavy governmental reg-
ulation.% Family foundations were, thus, not merely altruistic,
they were very pragmatic hedges against high tax rates and
magisterial interference.®* In other words, they provided the
rich with exemptions and loopholes.®”

That is not to say, of course, that the foundations did not ac-
complish great things that contributed to the public good. They
did, and often still do.

The Carnegie Foundation, established in 1911 by Andrew
Carnegie, almost single-handedly endowed the public library
movement in the United States.°* With assets of nearly half a
billion dollars at work every year funding universities, hospitals,
and community development projects, it has been a remarkable
institution of help and hope.%

The Rockefeller Foundation, established in 1913 by John D.
Rockefeller, likewise has endowed, over the years, innumerable
worthy philanthropic projects.1°° It has endowed hunger relief
projects worldwide.‘ It has established hospitals and health care
facilities on every continent.'°? And its support of educational
progress has led to the development of research centers, the pub-
lication of academic advances, and the subsidizing of private in-
stitutes and universities. !°> With assets of almost a billion dollars
at work every year, it has been a powerful force for good.

The Ford Foundation, established in 1936 by Henry Ford,
has greatly advanced the public welfare by trying to identify and
contribute to the solution of significant national and international
problems.1% Interested primarily in improvement of educational
quality and opportunity in schools, colleges, and universities, it
has endowed alternative learning projects, scholarship pro-
grams, and research management seminars.'°6 With assets of
nearly three billion dollars at work every year, it, too, has wrought
tremendous grace around the world.‘°” Dozens of other family
foundations— established by the Mellons, the Astors, the Morgans,
the Johnsons, the Roosevelts, the Kennedys, the Vanderbilts,
and many lesser-known clans, like the Kelloggs, the Dukes, the
Watumulls, the Motts, and the Kaufmans— have similarly exer-
cised considerable charitable effect.

Sadly, though, much of the good work that these foundations
have done has been at best minimized, at worst nullified, by their
counterproductive contributions to organizations like Planned
