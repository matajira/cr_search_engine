xit Granp ILLustons

process of writing and researching the first edition. After it was
determined that a revised edition would be necessary, many
more friends set aside their valuable time to contribute their ex-
pertise, research, and constructive criticism: Ken Pachal,
Eileen Lichwiarz, Mercedes Wilson, Douglas Scott, Patricia
Bainbridge, Ann Scheidler, Cheryl Eckstein, Jim Sedlak,
Magaly Llaguno, and Jerry Horn. To all these I am profoundly
grateful.

My sharp-eyed editors, Linda and Jerry Bowyer spotted
every errant jot and tittle and encouraged me every step along
the way. Their tireless effort, keen insight, and abiding friend-
ship are deeply appreciated.

The soundtrack for the second edition was ably provided by
Charlie Peacock, Wolfgang Mozart, Susan Ashton, Out of the
Grey, Steve Camp, Felix Mendelsohn, Michael Card, and Joze
Banic while the midnight musings were supplied by Martin
Gilbert, Paul Johnson, John Judis, Carroll Quigley, Joseph
Sobran, and R. Emmett Tyrrell.

A number of accomplished journalists like Joel Belz, Nick
Eicher, Cal Thomas, Kerby Anderson, William Hoar, and
Marvin Olasky have helped me hone my writing skills. Practic-
ing physicians like Ed Payne, Robert Dotson, and Hilton Terrell
helped to sharpen my thinking. And several pastors and minis-
ters around the country have been unswerving in their personal
and spiritual support: James Bachmann, Bob Coy, Dale Smith,
David Hall, Tom Clark, Gordon Walker, Peter Leithart, and
Don Finto:

All the stories and vignettes in this book are true. In some,
names have been changed, in others editorial liberties have been
taken to combine certain events for purposes of clarity or illus-
tration: But in all instances, the events and conversations
accurately reflect factual situations.

Many thanks are due to the hundreds of women I have talked
with and interviewed over the years. I owe a special debt of
gratitude to the women of the Crisis Pregnancy Center of
Houston, of the HELP Services Women’s Center in Humble,
and of the many Women Exploited By Abortion chapters
around the country. These stories are their stories.
