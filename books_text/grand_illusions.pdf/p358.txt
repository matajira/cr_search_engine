338 Granp ILLusions

* Marvin and Susan Olasky, More Than Kindness: A Compassionate Approach
to Crisis Childbearing (Westchester, IL: Crossway Books, 1990).

Ronald Reagan, Abortion and the Conscience of a Nation (Nashville, TN:
Thomas Nelson, 1984),

David C. Reardon, Aborted Women: Silent No More (Westchester, IL:
Crossway Books, 1987). .

Francis A. Schaeffer, A Christian Manifesto (Westchester, IL: Crossway
Books, 1981}.

Francis A. Schaeffer, C. Everett Koop, Jim Buchfuehrer, and Franky
Schaeffer V, Plan for Action: An Action Alternative for Whatever Happened
to the Human Race (Old Tappan, NJ: Revell, 1980).

Francis Schaeffer and C. Everett Koop, Whatever Happened to the Human
Race (Old Tappan, NJ: Revell, 1980).

Franky Schaeffer, A Time for Anger: The Myth of Neutrality (Westchester,
IL: Crossway Books, 1982).

Franky Schaeffer and Harold Fickett, 4 Modest Proposal: For Peace,
Prosperity and Happiness (Nashville, ‘TN: Thomas Nelson, 1984).

Joseph M. Scheidler, Closed: 99 Ways to Stop Abortion (Westchester, IL:
Crossway Books, 1985).

Phyllis Schlafly, ed., Child Abuse in the Classroom (Westchester, IL: Cross-
way Books, 1986).

R. C. Sproul, Lifeviews (Old Tappan, NJ: Revell, 1986).

Susan M. Stanford, Will I Crp Tomorrow? Healing Post-Abortion Trauma
(Old Tappan, NJ: Revell, 1986).

Charles R. Swindoll, Sanctity of Life: The Inescapable Issue (Dallas, TX:
Word, 1990).

Joni Eareckson Tada, All God's Children: Ministry to the Disabled (Grand
Rapids, MI: Zondervan, 1987).

Randall Terry, Higher Laws (Binghamton, NY: Project Life, 1988).

Randall A. Terry, Operation Rescue (Springdale, CA: Whitaker House,
1988).

* Randall A. Terry, Accessory to Murder (Brentwood, TN: Wolgemuth &
Hyatt, 1990),
