Idols for Destruction; A Strategy for Prophets and Priests 271

The Legislature

Ostensibly, Congress is the source of all federal law in this
country. The president cannot make law; he only administers it.
The courts cannot make law; they only adjudicate it. Only the
combined efforts of the two branches of Congress—the House
and the Senate—can make law.

The House of Representatives has four hundred thirty-five
members apportioned on the basis of population, and elected
every two years from among the fifty states. The Senate has one
hundred members, two from each state, elected to staggered six-
year terms. Together those five hundred thirty-five magistrates
have the power to stop abortion, defund Planned Parenthood,
clean up our schools, regulate the profligate birth control drug
trade, revoke tax-exempt status for abortuaries, enforce par-
ental consent, and increase legal and medical liabilities.6* If
only they would.

And they would if we could only bring the right kind of pres-
sure to bear.

If we are going to serve our society as prophets and priests,
guiding and guarding the land, we are going to have to influence
the legislature.

There are several things that each of us can do to do just that.

First, we can initiate correspondence with our legislators,
Only one out of every twenty Americans has ever written to their
Congressmen.® Only one out of every two hundred has written
more than once. And only one out of every ten thousand has
written more than five times.6? So while the mail room on
Capitol Hill may receive several thousand letters every day, only
a few hundred actually address specific policy issues—the rest
are trivial or intercessory. Because the congressional staff tabu-
lates these letters and then surveys them in the manner of a poll,
any one letter can leave a tremendous impact on the policy-
making process. And a regular, consistent correspondence may
mean the difference between a pro or con vote on something as
important as Title XX appropriations for Planned Parenthood
or Hyde Amendment restrictions or a Freedom of Choice Act or
a Fetal Harvesting bill. So why not invest the price of a stamp and
fifteen minutes’ time and write a letter? But not just one letter. Why
not develop a habit of correspondence? Informative, cordial,
