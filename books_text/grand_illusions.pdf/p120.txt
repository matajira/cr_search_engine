100 GRanb ILLusIons

“T get scared sometimes,” Lanita said “It’s like we’ve opened
up this Pandora’s Box or something, you know?”

‘Really, man, that’s it,” Milly agreed. “Pandora’s Box. One
giant mess.”

Racism and Sterilization

In order to realize Margaret Sanger’s Eugenic ideal of elimi-
nating the “masses of degenerate” and “good-for-nothing” races,
Planned Parenthood has not only emphasized contraception and
abortion, it has also carried the banner of sterilization.? And, of
course, that sterilization vendetta has been primarily leveled
against minorities.

The sterilization rate among Blacks is forty-five percent
higher than among whites.8° Among Hispanics the rate is thirty
percent higher.®! As many as forty-two percent of all Amerind
women and thirty-five percent of all Puerto Rican women have
been sterilized.8?

As was the case with Carrie and Doris Buck, many of these
sterilizations have been performéd coercively. “Women in the
United States are often pressured to accept sterilization in order
to keep getting welfare payments,” says feminist writer Linda
Gordon.® And non-White welfare recipients are apparently
pressured at.a significantly higher level than Whites, resulting in
a disproportionate number of sterilizations.®+

The Association for Voluntary Sterilization has estimated.
that between one and two million Americans a year are sur-
gically sterilized.5 But there may be another two hundred fifty
thousand coercive sterilizations disguised in hospital records
as hysterectomies.%¢

A hysterectomy —the removal of the female reproductive
system—should only be performed when its organs and tissues
become severely damaged, diseased, or malignant. Never should
it be performed to achieve sexual sterilization, says Dr. Charles
McLaughlin, president of the American College of Surgeons.
That would be ‘like killing a mouse with a cannon.”*7 It is also
much more lethal than simple tubal ligation sterilization opera-
tions. Currently, some twelve thousand women a year die re-
ceiving hysterectomies.®°

Nevertheless, since Planned Parenthood’s Eugenic hysteria
was unleashed, the annual number of hysterectomies has sky-
