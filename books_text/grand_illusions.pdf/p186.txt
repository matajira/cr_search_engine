166 Granp ILLusions

equally by the Bobst Library, the Provincetown Playhouse, and
the Washington Mews. The grand promenade down Fifth
Avenue is shared equally by the Empire State Building, the
‘Trump Tower, and St. Patrick’s Cathedral.

The city’s gestalt of raw tension and hungry ambition calves
its persona like an Arctic berg—splinters drifting away. The eclec-
ticism is everywhere evident. It is a bright matrix of contradic-
tion unfolding across a tantrum of logic and illogic, of antiquity
and modernity, of substance and illusion, of objectivity and bias,
of bondage and freedom, and of honesty and deception.

That odd juxtaposition, that almost schizophrenic New York
yin-yang, is nowhere more evident than in its mid-town infor-
mation agribusiness. With a proud legacy dangling like a
medallion upon its chest, the New York media— which is the na-
tional media—simultaneously belies that legacy with a brash
bravado of contemporancity.

In other words, it an? what it seems to be.

Why Don’t We Know?

“This kind of country can’t work,” says television journalist
Charles Kuralt, “unless people have a reliable way of finding out
what’s going on.”3

The news media is supposed to be that reliable source of
information.

But it’s not.

And, perhaps, that is part of the reason why this country
doesn't work very well right now.*

Much of the information in this book is probably surprising,
even shocking, to you. “Could this possibly be true?” you may be
asking yourself. “If it is, then why haven’t I heard it before? Why
don’t I already know about it?”

The answer is that the media has acted as a filter, screening
out most of the information that could “damage” groups like
Planned Parenthood “in the public view.”5 Instead of helping
people find out what is going on, it is insuring that they dort.
And won't. And can't.

The media has an agenda. It imposes its values on its au-
dience. As Herbert Gans, renowned media analyst, has argued,
“Journalism is, like sociology, an empirical discipline. As a
result, the news consists not only of the findings of empirical
