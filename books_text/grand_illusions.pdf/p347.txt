Appendix B: Pro-Life Activism 327

The Belgic Confession (1561). “It is impossible that the holy.
faith can be unfruitful in man.”!*

The Heidleberg Catechism (1563). “It is impossible that those
who are unplanted into Christ by true faith, should not bring
forth fruits of mercy and graciousness.”!9

The Westminster Confession (1646). “Good works, done in obe-
dience to God’s commandments, are the fruits and evidences of
a true and lively faith: and by them believers manifest their
thankfulness, strengthen their assurance, edify their brethren,
adorn the profession of the Gospel, stop the mouths of the adver-
saries, and glorify God whose workmanship they are, created in
Christ Jesus thereunto; that, having their fruit unto holiness,
they may have the end, eternal life.”2

Matthew Henry (1662-1714). “Man may as soon take pleasure
in a dead body, void of soul, or sense, or action, as God takes
pleasure in a dead faith, where there are not works.”?!

Jonathan Edwards (1703-1758). “That religion which God re-
quires, and will accept, does not consist in weak, dull, lifeless
wishes, raising us but a little above a state of indifference. God,
in His Word, greatly insists upon it, that we be in good earnest,
fervent in spirit, and our hearts vigorously engaged in mercies.”??

On and on the testimony of faith through the ages affirms a
Gospel evidenced by social concerns but condemning a Social Gos-
fel dependent upon those concerns.

This is indeed the Gospel of light and life.
