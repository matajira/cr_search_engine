Ldols for Destruction: A Strategy for Prophets and Priests 257

Likewise, the church must not meddle in the affairs of the
state. That is simply not its concern. The state is outside of the
church’s jurisdiction. Again, the framers of the American Con-
stitution wanted to protect their fledgling republic from any and
all tyrannies: both statism—in the form of imperialism, social-
ism, or democracy; and oligarchy—in the form of ecclesiocracy,
agathism, or caesaro-papism. The church has no authority over
the state and, therefore, must not regulate or interfere in the
work of the state.@ They are separate.

This, of course, does not mean that church and state are to
have nothing to do with each other. On the contrary, it is essen-
tial that they interact and cooperate with each other. They are to
balance each other. They are to serve each other. They are to
check each other. So, even though they are institutionally sepa-
rate, their separation is not absolute and exclusive. There is no
wall of separation. Rather, church and state are distinct but
cooperative. They are separate but interdependent.

The wail is a myth.

Joseph Story, perhaps America’s foremost nineteenth-
century legal historian, underscored this truth in his book on the
Constitution saying:

The First Amendment was not intended to withdraw the Chris-
tian religion as a whole from the protection of Congress. . . .
At the time, the general, if not universal, sentiment in America
was that Christianity ought to receive encouragement from the
state so far as was compatible with the private rights of con-

science and the freedom of worship. . . . Any attempt to level
all religions, and to make it a matter of state policy to hold all in
utter indifference would have created . . . universal indignation.?

The state was to encourage the church.

At the same time, the church was to cooperate with and en-
courage the state. For instance, the church was to teach the citi-
zenry the Bible, the common standard of law for both church
and state.!° The church was to instruct all men and women in
the basic principles of godly citizenship and righteous cultural
action.!! The church was to mobilize the forces of mercy, truth,
and justice in times of difficulty or crisis.12 The church was to
recruit from the ranks of the congregation able men and women
for the work of civil service.1* The church was to confront evil,
expose sin, and denounce injustice whenever and wherever it
