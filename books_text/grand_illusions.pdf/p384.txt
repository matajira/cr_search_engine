364

Bo,
on.

92.
93.

95.
97.
98.
99.
100,
101.
102,
103.

104.
105.

105,

107.
108.
109.
110.

tl.
112.

113.

114.
115.

Granp ILLustons

. Michael Woods, “The National Institutes of Health,” The World and I, Septem-

ber 1987, p. 280.
Ibid., pp. 280-81.

. Deuteronomy 28:58-66.

James C. Moth, Abortion in America: The Origins and Evolution of National Policy
(New York: Oxford University Press, 1978).

Deuteronomy 28:1-14

See David Chilton, Power in the Blood: A Christian Response to AIDS (Brentwood,
‘TN: Wolgemuth and Hyatt Publishers, 1987).

. See Lan Ti Kwo, The New Frontier: Medicine at the Crossroads (Boston: Medical

‘Technology Press, 1982),
Bid.

, Joe Levine, “Help from the Unborn,” Time, January 12, 1987,

Elizabeth Mehren, “Surrogate Mothers: Let’s Stop This,” Los Angeles Times,
September 1, 1987.

Francis A. Schaeffer and G. Everett Koop, Whatever Happened to the Human Race
(Old Tappan, NJ: Fleming Revell, 1979), pp. 89-118.

William Winslade and Judith Wilson Ross, “High-Tech Babies: A Growth
Industry,” Gatoeston Gazeite, June 17, 1983.

Schaeffer and Koop, pp. 5-87.

W. H, Hibbard, “The Real Neuromancer,” Late Breaking News, August 14, 1986,
‘Ted Howard and Jeremy Rifkin, Who Should Play God? (New York: Dell Pub-
lishing Go., 1977).

Golda Lamartia and Thomas Gibson, Time Enough for Sorrow: The Deinstitution-
alization of America’s Mentally I (Los Angeles: Kardia Publications, 1984).
Jeremy Rifkin, Aigeny (New York: Viking Press, 1983),

Tol Abelard and Rus Contola, War: The Next Phase, The Next Century (London:
Shambalah Bookworks, 1981),

Michael J. Wilson, John F. X. Uhelu, and Robert L. Quay, Decliaforcation:
Experiments and Procedures (London: The Research Council of the Institutes of
Immunology, 1979).

Marvin Morrow, Big Business: The Corporatication of Health Case (Dallas: White
Paper Reports, 1982).

Jim Lester, “Biocleatics: How Safe are the High-Tech Procedures?” Waichcourt
‘Review of Technical Literature 2:4, pp. 16-43.

Lucas Berry and Davis Lowery, “Human Experimentation in Berkeley Draws
Fire,” Local Times, February 8, 1978.

Roger Howard and Shannon Lec-Bailey, “Neuroclatology: Defining the Para-
meters,” Pathology Texts 16:4, 1984.

Daniel J. Kevles, In the Name of Eugenics (New York: Penguin Books, 1985).
Garson L, B. Trang, “Renatalization of Cell Life and Synthetic Steroids,”
Harper Standard, July 1977.

Stephen J. Gould, The Mismeasure of Man (New York: W. W. Norton and Co.,
1981).

Robert J. Lifton, The Nazi Doctors (New York: Basic Books, 1986).

Although this genealogy may have a few generational gaps, its veracity is
highly likely. See Barbara De Jong, Tracing the Javish Diaspora (Amsterdam:
The English Press, 1967); Ronald L. Numbers and Darrell W. Amundsen,
eds., Caring and Guring: Health and Medicine in the Western Religious Traditions (Lon-
don: Collier Macmillan Publishers, 1986); F. F. Cartwright, A Social History of
Medicine (New York: Longman, 1977); and Latimer Wright, Music in My Ears:
Jews and Greeks, Barbarians and Gentiles (New York: the Albert Kroc House, 1967).
