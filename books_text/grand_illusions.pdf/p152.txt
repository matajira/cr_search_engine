132 Granp ILLusions

Roxanne took a seat next to a petite teen~a girl about her
own age, she guessed —and across from an older woman, an un-
settling vision of frustrated resignation. Neither of them seemed
to notice her as Roxanne sat on the edge of her seat scratching bits
of biographical information onto the form on her lap—life, love,
and hope hemorrhaging from her soul with each completed line.

Saturday morning at Planned Parenthood. Day of decision.
Valley of decision.

Roxanne handed the completed form to the haggard recep-
tionist and returned to her seat. Tossed to and fro on waves of
doubt, weak from resisting swells of guilt, and helpless in the
face of a flood of loneliness, she felt as if she were drowning.

But then the tide rolled out and resolve rushed in when she
heard her name called.

She followed a tall and elegant woman down a dimly lit hall
to a small office cubicle. Spare and unfurnished, save for a small
round table and two straight-backed chairs, the room bore the
unmistakable mark of bureaucratic impersonalism. And the
woman’s expressionless features did little to dispel that institu-
tional gloom. A shiver ran up and down Roxanne’s spine.

After a brief exchange of rote pleasantries the woman turned
her attentions to a small folder of paperwork. “Tell me, Ms.
Robertson,” she intoned without looking up, “what is your actual
monthly income?”

“Well, I'm going to school full-time right now,’ Roxanne re-
plied. “I work at the Student Union about fifteen hours a week—
or whenever they need me. That comes out to about $50.00.
And then my parents send me about $150.00 a month for other
expenses, clothes, supplies, gas for the car, and stuff. Course,
they pay tuition, books, and my room and board at the dorm.”

“So then, the only money that actually passes through your
hands is the $50.00 a week from your part-time job and the
$150.00 a month from your parents?” the woman asked.

“Yeah, I guess so.”

“Well, then according to state guidelines, you qualify for gov-
ernment subsidy for your care today. You will only be responsi-
ble for co-pay of twenty percent.”

“Oh well, I don’t need to do that. My parents have sent me
the money I need.”

“You qualify. You might as well take advantage of your
benefits.”
