372

108.
109,
110,
iil,

112.
113,
il4,
115,
116,
7.

118,
119,
120.
121,
122.
123.

124,
125.

126,

127,
128.
129,
130.

131,
132.
193.

134.
135.
136.
137.

138.
139,
140,
4h.

142,

Granp ILiusions

Deuteronomy 30:15-20,

“Celebrating,” pp. 3, 18-19.

Psalm 127:3-5.

Warren Hearn, “Is Pregnancy Really Normal?” Family Planning Perspectives 3:1
(January 1971), p. 9; and Willard Cates, “Abortion As A Treatment for Un-
wanted Pregnancy: The Number Two Sexually-Transmitted Condition,” an
address presented to the Association of Planned Parenthood Physicians,
Miami, Florida, November 11-12, 1976.

Proverbs 15:13.

“Sexuality Alphabet,” p. 14; and LaHaye, p. 107.

Psalm U.
“Control,” p. 2; and LaHaye, p. 019.

Proverbs 17:17.

Peter J. Leithart, “Modern Sex-Speak,” Chalcedon Report, Number 270, January
1988, p. 3.

Alcorn, pp. 68-73; and LaHaye, pp. 133-136.

Orwell, p. 246.

Alcorn, pp. 68-73.

Leithart, p. 3.

Cole, p. 92.

See Chapter 4 for a detailed discussion of the medical risks of abortion and
birth control.

See Phyllis Schlafly, ed., Child Abuse in the Classroom (Westchester, IL: Crossway
Books, 1984).

“What is Peer Education in Human Sexuality” brochure, Planned Parenthoad
of Greater Metropolitan Washington, p. t-

Louis Harris and Associates, American Teens Speak: Stx, Myths, TV, and Birth Con-
trol, “The Planned Parenthood Poll” (New York: Louis Harris and Associates,
1986).

Ibid., p. 71.

Ibid., pp. 71-72,

Tbid., p. 13.

Ibid., p. 24; and see Robert Rufl’s analysis in Aborting Planned Parenthood
(Houston; New Vision Books, 1988), p. 19.

Ibid., p. 18.

Ibid., p. 83.

David C. Reardon, Aborted Women: Silent No More (Westchester, IL: Crossway
Books, 1987), p. xxiv.

Ibid., p. xxv.

Ibid., p. 17.

Ibid., p. 19.

Note: Planned Parenthood Association of Nashville, Board President, Nancy
A, Ransom’s “Planned Parenthood, An Excellent Resource,” a letter to the
editor, Tennessean, January 22, 1988.

Reardon, p. 19.

Ibid.

Tid.

Sam Gitchel and Lorri Foster, Let's Talk About Sex: A Read-and-Discuss Gutde for
People Nine to Twelve and Their Parents (Fresno, CA: Planned Parenthood of Cen-
tral California, 1986), pp. 17-18.

Dan Smoot, “Contributing to the Delinquency of Minors,” Dan Smoot Report,
March 31, 1969, p. 51.

   
