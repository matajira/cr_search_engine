48 Granp InLusions

make the wheat, which makes the bread.” After a thoughtful
pause her father rejoined, “Well, well, so that’s the idea. Then
why didn’t you just say so? Always say what you mean, my
daughter, it is much better.”?9

In spite of Michael's concerted efforts to undermine Margaret’s
young and fragile faith, her mother had her baptized in St.
Mary’s Catholic Church on March 23, 1893. A year later, on
July 8, 1894, she was confirmed. Both ceremonies were held in
secret—her father would have been furious had he known. For
some time afterward she displayed a zealous devotion to spiri-
tual things, but gradually the smothering effects of Michael’s
cynicism took their toll. By the time she was seventeen her pas-
sion for Christ had collapsed into a hatred of the church—a hat-
red that would be her spiritual hallmark for the rest of her life.3°

Margaret moved away from her unhappy home as soon as
she could. First, she went away to a boarding school, Claverack
College of the Hudson River Institute, where she got her first
taste of freedom. And what a wild and intoxicating freedom it
was: She plunged into radical politics, suffragette feminism, and
unfettered sex,3! When she could no longer afford the tuition,
she moved home only long enough to gather her belongings and
set her affairs in order. She had drunk from the cup of con-
cupiscence and would never again be satisfied with the quiet vir-
tues of domestic tranquillity.

She decided to move in with her older sister in White Plains,
taking a job as a kindergarten teacher. Assigned to a class made
up primarily of the children of new immigrants, she found that
her pupils couldn’t understand a word that she said. She quickly
grew tired of the laborious routine of teaching day in and day
out, and quit after two terms. Next, she tried nursing. But hos-
pital work proved to be even more vexing and taxing than teach-
ing. She never finished her training.2? She escaped from the
harsh “bondage” of labor and industry in the only way a poor girl
could in those “unenlightened” days when the Puritan Work
Ethic was still ethical: She married into money.

The Winter of Her Discontent
William Sanger wasn’t exactly rich, but he was close enough

for Margaret. He was a young man of great promise. An archi-
tect with the famed McKim, Mead, and White firm in New York
