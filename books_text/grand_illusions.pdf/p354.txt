334 Granp ILLusions

* Jacqueline Kasun, The War Against Population: The Economics and Ideology
of Population Control (San Francisco, CA: Ignatius Press, 1988).

Kathy Keeton, Woman of Tomorrow (New York: St. Martin’s Press,
1985).

Daniel J. Kevles, In the Name of Eugenics (London: Viking Books, 1986).

Alfred C. Kinsey, Wardell C. Pomeroy and Clyde E. Martin, Sexual
Behavior in the Human Male (New York: W. B. Saunders, 1948).

Kristin Luker, Abortion and the Politics of Motherhood (Los Angeles, CA:
University of California Press, 1984).

Andrew H. Merton, Enemies of Choice: The Right-to-Life Movement and Its
Threat to Abortion (Boston, MA: Beacon Press, 1981).

James C. Mohr, Abortion in America (New York: Oxford University
Press, 1978).

Bernard N. Nathanson, Adorting America (Garden City, NY: Double-
day, 1979).

Bernard N. Nathanson, The Abortion Papers: Inside the Abortion Mentality
(New. York: Frederick Fell, 1983).

Leonard J. Nelson, The Death Decision (Ann Arbor, MI: Servant
Books, 1984).

Lesley Jane Nonkin, J Wish My Parents Understood (New York: Penguin
Books, 1986).

Marvin Olasky, Abortion Rites: A Social History of Abortion in America
(Wheaton, IL: Crossway Books, 1992).

* Marvin Olasky, Patterns of Corporate Philanthropy: Public Affairs Giving and
the Forbes 100 (Washington, DC; Capitol Research Center, 1987).

*Marvin Olasky. The Press and Abortion, 1838-1988 (Hillsdale, NJ:
Lawrence Erlbaum Associates, 1988).

Hyman Rodman, Susan H. Lewis and Saralyn B. Griffith, The Sexual
Rights of Adolescents: Competence, Vulnerability and Parental Control (New
York: Columbia University, 1984).

Barbara Katz Rothman, The Tentative Pregnancy: Prenatal Diagnosis and
the Future of Motherhood (New York: Viking Penguin, 1986).

Rousas John Rushdoony, The Myth of Over-Population (Tyler, TX:
Thoburn Press, 1969).
