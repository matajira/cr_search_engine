160 Granp ILLusions

abortion and birth control crusade.®! Corporate giving has proven
to be so lucrative over the last several years that several Planned
Parenthood affiliates have full-time corporate development staffs
that do nothing but wine and dine executives in the hopes of
wooing more dollars into its Midasized vaults.°? Efforts by or-
ganizations like Life Decisions International have done much to
educate the public and corporate boards alike—the result has
been that several major companies have dropped their support
of Planned Parenthood—but millions of dollars every year still
travel from board rooms to abortion clinics.%3

Ruth Demmik is a systems engineer for a large Silicon
Valley manufacturing company. She is also very active in the
pro-life movement, donating ten hours a week to counsel bat-
tered women and abused children at her church’s crisis outreach
center. Last year, she was instrumental in helping her pastor or-
ganize a community-wide protest of Planned Parenthood.

To her dismay, she discovered just two weeks before the
march that her company was actively cooperating with a Na-
tional Organization for Women. and Planned Parenthood effort
to diffuse the effects of the picket. “The pro-abortion people went
around to business and civic leaders soliciting donations,” she
said. “But they didn’t just ask for donations straight out; instead
they asked contributors to pledge a certain amount for every
pro-lifer that turned out for the picket. In other words, the big-
ger our turnout, the greater their gain. When they came to our
company, they asked for a corporate matching grant: however
much they could raise out in the community, matched dollar for
dollar by the company. Double or nothing. Well, our board said
yes. I was flabbergasted.”

Ruth went from boss to boss all the way up the corporate lad-
der, trying to get the board to reverse their decision. “They had
their minds made up, though,” she said. “They didn’t want me to
confuse them with the facts. I don’t think I've ever been more
frustrated in all my life. Here I was, working for a company that
was working against everything that I believe in. That’s a tension
that will have to be resolved scon. One way or another.”

Family Foundations
During the first three decades of this century, a number of
family-controlled philanthropic foundations were established.
‘The era of the industrial tycoon and the manufacturing monopoly
