In the Heat of the Fight B

Invariably, I forget where that “safe place” is and am forced to
spend precious moments racking my stilt anesthetized brain in
order to discover how “clever” I was the night before. On this
particular morning, I hunted through drawers, in closets, and
under mattresses for almost twenty minutes before I remem-
bered that I had hidden the wallet in the ice bucket. I've since
been told that’s one of the first places a thief checks when he’s
casing a room.

I didn’t have time for breakfast now, so I went down to the
lobby to await my ride to the picket.

The drive was pleasant and uneventful. Talk ranged from
baseball scores to department stores, from amusement parks to
broken hearts, from movie releases to dry-cleaned creases. No
one said a word about the dark portent of danger that we all
felt. I, for one, was trying to ignore it, hoping that it would just
go away.

But it wouldn't.

And I knew why.

I knew what would be waiting for us at the clinic. And no
amount of hoping could erase that knowledge.

We parked just across from the old building. It was a real
oddity in the once distinguished neighborhood. Situated on the
main strect in town, it was down four or five blocks from where
the commercial section began, in an area that had long ago
sported wide lawns and overarching elms. Now, all those trees
were gone, victims of Dutch Elm disease, and the grand prom-
enade had an exposed, befuddled air. Gallant English gardens
and proud Tudor homes had given way to a barren wasteland of
asphalt and gaudy metal warehouses.!* The clinic occupied a
remnant of the past. It was once a stately mansion — brownstone
and ivy, leaded glass and cedar shakes. It was an island of anti-
quity amidst a sea of modernity.

When it was built around the turn of the century, it was seri-
aus and simple to excess. Contemporary men rarely appreciate
that style. They prefer the esoteric eccentricity of modernism.
No doubt they are right, since they are restless space-time
nomads. But men and women who have lived long and are
tired of wandering— who wani rest, who have done with tempo-
ral aspirations and ambitions, whose life in the urban Negev has
