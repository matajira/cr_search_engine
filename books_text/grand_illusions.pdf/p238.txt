a8 Granp ILLUSIONS

Suitable cases for abortion would fall within the scope of the
American College of Obstetricians and Gynecologists State-
ment on Abortion.®!

As to whether or not the performance of an induced abortion is
always sinful we are not agreed, but about the permissibility for
it under certain circumstances we are in accord.®?

The Christian physician will advise induced abortion only to
safeguard greater values sanctioned by Scripture. These values
should include individual health, family welfare, and social
responsibility.23

Much human suffering can be alleviated by preventing: the
birth of children where there is a predictable high risk of genetic

disease or abnormality. This appears to be a reasonable Chris-
tian objective.*4

Though many of the theologians and institutions that parti-
cipated in the symposium later amended their pro-abortion
stance, it was, by then, too late.

Not only did much of the church jettison their historic pro-
life commitments to officially embrace the old pagan consensus
about abortion and infanticide, they also succesfully diverted
millions of dollars —intended by parishioners for missions ~ into
radical causes and militant organizations like Planned Parent-
hood.®§ Additionally, many of them have even developed organ-
izational ties,® recruited staff volunteers,®’ pioneered cooperative
programs,®* sponsored seminars and conferences,®? co-published
educational literature, filed amicus briefs,® lent the use of
church properties,9* and established public testimony? for
Planned Parenthood.

In less than a generation they were able to whisk away the
two-thousand-year-old voice of ecumenical affirmation, the eter-
nal witness of Divine Revelation, and the spiritual service of
charitable application. In less than a generation they were able
to betray the most basic principles of the Christian faith.

The Balak Strategy
When Balak, King of Moab, was confronted with the advanc-
ing armies of Israel immediately following the Exodus sojourn,
he began to cast about for a strategy to stop them (Numbers
