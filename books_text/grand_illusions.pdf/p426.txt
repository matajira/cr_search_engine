406 Granp ILLusions

sorcery, 392

speech, 126-127

spermicide, 82

Spurgeon, Charles H., 246, 294-295,
296, 299, 303, 304, 306, 307, 309

St. Clair, Augustus, 185

Stalin, Josef, 46, 197, 231

Standard Oil, 159, 160

statism, 257

steadfastness, 237-240

sterility, 34

sterilization, 25, 27, 91-93, 100-102

Stewart, James, 173

Stoddard, Lothrop, 95-97

Story, Joseph, 257

strength, 244-247

Suction-Aspiration, 72

Sullivan, 274

Sun Oil Company, 159

Sunday, Billy, 59

Sutnik, Amy, 176-178

Sweden, 197

syphilis, 31-32

 

Tada, Joni Eareckson, 397

‘Tax Exemption Equity Act, 139

taxes, 28-30, 132-138

teaching, 259-261, 291-295

teen pregnancies, 30-31, 32-33, 118-119,
125-126, 137

‘Tehran, 189

‘Tepper, Sheri, 110

Tertullian, 211

‘Theosophy, 60

‘Yhird World, 25

Thoburn, Robert, 128, 373

Thompson, Scott, 119

Thornburgh v. American College of
Obstetrics and Gynecology, 274

‘Thrasymachos of Thrace, 292

‘Thubron, Colin, 41, 356

Thucidides, 41

Tiananmen, 189

Time magazine, 175

Tisdale, Sally, 362

Title V, 28-30, 133-134, 138, 351

‘Title X, 28-30, 101, 133-134, 136, 159,
280, 351

Title XIX, 133-134, 138, 351

le XX, 133-134, 138, 271, 351

, 191

‘Tocqueville, Alexis de, 260

 

    

Tolfier, Alvin, 167

Trotsky, Leon, 64

‘Truman, Harry, 47, 154
Tryer, Louise, 46

‘Tydings Act, 133-134, 135, 136
tyranny, 255-259, 282-287

 

Ukraine, 197

UNESCO, 192

UNICEF, 192

TLS. «, Dougherty, 278

unholy alliances, 162-164

Union Pacific, 160

United Church of Christ, 215

United Methodist Church, 215

United Nations, 29, 192

United Way, 151, 154-156, 163, 302,
379

Upjohn, 76

USA Taday, 174-175, 177

values clarification, U1-l12

Van Til, Henry, 393

Vejzovic, Ruza, 190-192, 198, 205
venereal diseases, 31-32

Vickery, Alice, 194

Videossis, 16

Village Voice, 187

vision, 237-240

Vogue magazine, 177

wall of separation, 255-259

Wallace, Mike, 171

Walters, Barbara, 173

Walton, Rus, 393

Washington Post, 174

Waitleton, Fayc, 63, 105-106, 345, 356,
361

Webster, 274

Weed, Stan, 367

Wells, H. G., 47, 57, 98, 282

Westheimer, Ruth, 110

Westminster Confession, 327

White Supremacy, 91-106

Whitehead, John, 284, 393

Whitney, Leon, 95

WHO, 192

Why Wait?, 225

Wilder, Thornton, 91

Willke, J. C., 359

Wilkinson, W. C., 294-295

Will, George, 137, 375
