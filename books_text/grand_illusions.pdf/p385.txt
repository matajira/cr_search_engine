End Notes 365

116, See Numbers and Amundsen; and Chilton, pp. 177-182.

117, Demetrios J. Constantelos, Byzantine Philanthropy and Social Welfare (Paris:
Orthodox Classics, 1968).

118. Timothy S. Miller, The Birth of the Hospital in the Byzantine Empire (Paris: Ortho-
dox Classics, 1985).

119, John Scarborough, ed., Byzantine Medicine (Paris: Orthodox Classics, 1984).

120. See Numbers and Amundsen, pp. 65-107.

121. Rupert Masson, “Raison DBtre,” L’Auize, January 1962

122, See Numbers and Amundsen, pp. 47-52.

123. Leo Kuper, Genocide: lis Political Use in the Twentieth Century (New Haven, OT:
Yale University Press, 1981).

124, The word absolute here is of utmost importance.

125. Reardon, p. 232.

126. Tbid., p. 233.

127. Ibid., pp. 233-34.

128, Communiqué, October 30, 1992.

129, See George Grant, The Quick and the Dead: RU-486 and the New Chemical Warfare
Against Your Family (Wheaton, IL: Grossway Books, 1991).

Chapter 5—A Race of Thoroughbreds: The Racial Legacy

1, “Man's inhumanity against man”
2. Hilaire Belloc, Notes ofa Curmudgeon (London: Albright’s Ltd., 1956), pp. 42-43.
3. Daniel J. Kevles, In the Name of Eugenics: Genetics and the Uses of Human Heredity
(New York: Penguin Books, 1985), p. 110.

4, Ibid.

5. Dana Atwell and Irene Paige, Eugenic Sterilization (New York: Case Memorial
Foundation, 1998), pp. 43-44.

6. Buck v. Bell, 274 U.S., 201-3, (1927).

7, Stephen Jay Gould, The Mismeasure of Man (New York: W, W, Norton, 1981),
p. 336.

8, Atwell and Paige, p. 45.

9. Gould, p. 336.

10. Kevles, p. Ill.

11, Madeline Gray, Margaret Sanger: A Biography of the Champion of Birth Control (New
York: Richard Marek Publishers, 1979), pp. 240-41; Linda Gordon, Woman's
Body, Woman's Right: A Social History of Birth Control in America (New York:
Penguin, 1974), pp. 229-340, 353-359; Elasah Drogin, Margaret Sanger: Father of
Modern Society (New Hope, KY: CUL Publications, 1986), pp. 11-38; John W.
Whitehead, The End of Man (Westchester, UL: Crossway Books, 1986),
pp. 161-200.

12. See Allan Chase, The Legacy of Malthus: The Social Casts of the Now Scientific Racism
(New York: Alfred A. Knopf, 1977).

13. See Whitehcad, pp. 161-165.

14. See Kevles, pp. 57-69.

15, Jbid.

16. [bid,

17. See Germaine Greer, Sex and Desting: ‘The Politics of Human Fertility (New York:
Harper and Row, 1984).

18, See Whitehead, pp. 166-167.

19. Ibid.
