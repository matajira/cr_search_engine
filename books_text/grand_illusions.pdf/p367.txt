10.
1k.

12,
13.
14.
15.

16

7.
18.
19.

20

21.
22,
23.
24,
25.
26.
27.
28.
29.
30.
31.
32,
33.
34.

35.
36.
37.

End Notes 547

Ecclesiastes 11:6.

Though it was fairly late, I was able to get at least a cursory understanding of
the city’s disposal situation by contacting several twenty-four hour operations.
It was not a comprehensive survey, but it was quite enlightening.

Genesis 3:24; Exodus 14:26-35.

 

Hebrews 4:1-11,
Psalm
Psalm q
Romans 8719-22.
Romans 3:9-24.

1 Corinthians 12:4-7
Hebrews 10:24-25.
John 3:20,

Psalm 38:20.

Psalm 109:4.
Proverbs 24:11-42.
Ezekiel 3:17-19.

2 Corinthians 5:20.
Daniel 5:25.

Isaiah 6:5.

Isaiah 6:8.

Romans
Proverbs 8:
Revelation
Psalm 10:2.

Again, the only operations I was able to contact were all-night labs and disposal
waste companies, but with the scant information I was able to cull from them, I
was able to accurately extrapolate the facts,

2 Chronicles 28:3; Isaiah 30:33, Jeremiah 7:30-33.

Mark 5:1-5.

Psalm £7;9-12,

 

 

13; Revelation 9:21.

  

 

 

Chapter 2— All That Glitters

1
2.
3.
4,

5.

. "Behold the sign; examine the evidence.”

. Hilaire Belloc, The Servile State (London: TN. Foulis, 1913), p.148.

Curt Young, The Least of These (Chicago: Moody Press, 1984), p. 30.

. LeBeth Myers, Wome Around the Globe: International Status Report (London:

Guyon Social Resource Center, 1986), p, 137.

Debbie Taylor, et al., Women: A World Report (New York: Oxford University

Press, 1985), p. 10; and Paul B. Fowler, Abortion: Toward an Evangelical Consensus

(Portland: Multnomah Press, 1987), p. ll.

. Frederick 8. Jaffe, Barbara L, Lindheita, and Philip R. Lee, Adortion Politics:
Privaie Morality and Public Policy (New York: McGraw-Hill, 1981), p. 7.

. Numbers 13:33,

. “Celebrating Seventy Years of Service,’ 1986 Annual Report, Planned Parent-
hood Federation of America, pp. 23, 32.

. “Seventy Years of Family Planning in America: A Chronology of Major

Events,” Planned Parenthood Federation of America, pp. 3, 8.
