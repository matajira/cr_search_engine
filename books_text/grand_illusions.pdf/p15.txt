 

PREFACE TO THE
SECOND EDITION:
GRAND EVASIONS

hic se apertt diabolis*

10 introduce a book supposes the greatest difficulty — because of the inevitable, ines-
capable presupposing,?

‘Hilaire Belloc

it was Francis Bacon who first asserted that: “A man is known
by the company that he keeps—for in both the positive and the
negative, interaction unveils the voluminous mysteries of the
heart.”3 As true as that principle may be, G. K. Chesterton’s re-
vision is truer still: “A man is known by the company that he
does not keep—for in both the positive and the negative, non-
interaction unveils the voluminous mysteries of the heart."*

I have considered this notion intently over the past several
years as I have—often in wonderment—witnessed the non-
interactive evasions of the Planned Parenthood Federation of
America and its sundry institutional cohorts in the abortion in-
dustry to this book,

Awaiting Their Reply
Wherever I go throughout North America for various kinds
of speaking engagements—and even as I venture beyond this
continent from time to time—a barrage of questions inevitably
confronts me concerning the book:

© What has been the response of Planned Parenthood?
© Have you been sued yet?

xD
