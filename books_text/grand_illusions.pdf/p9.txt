Contents
15. THIS TOO SHALL PASS: GOOD PROVIDENCE /313

PART IV: RESOURCES

Appendix A: The Faithful Witness: Making a Presentation /327
Appendix B: Pro-Life Activism: A Social Gospel? /32/
Appendix C: Select Bibliographic Resources /329

End Notes /345

Index /399
