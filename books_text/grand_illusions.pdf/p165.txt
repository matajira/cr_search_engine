Robber Barons: The Financial Scandal 145

Parenthood always seem to find a way to do whatever they jolly
well please. And to make a buck while doing it.”

Sadly, Leslie Forrester’s experience is all too typical.

And that is the financial legacy of Planned Parenthood.

Through the Back Door: Recruitment

Planned Parenthood does not offer comprehensive health care
at any of its eight hundred clinics, two hundred affiliates, or fifty
chapters in this country.!® It is in the sex business. It does not
treat the flu, or give hearing tests, or perform vaccinations, or
set broken bones, or clean teeth, or check for heart murmurs, or
assuage coughs. If a treatment, or procedure, or prescription, or
therapy, or examination, or remedy, or medication, or regimen,
or cure is unrelated to sex, then Planned Parenthood doesn’t
offer it. It wants nothing to do with it.

Which makes its latest strategy to place sex clinics in high
schools all the more intriguing.1#4

Since it is tough to sell parents at the local PTA or school
board meeting on confidential birth control and abortion ser-
vices for their children, Planned Parenthood has resorted to
other tactics.!!2 Instead of asking those parents to ante up sev-
eral hundred thousand additional tax dollars every year to cor-
rupt community standards and teen morals, it simply asks them.
to provide “good comprehensive health care.”"'3 Then later, when
no one is looking, it slips its sex services in on the sly."*

One school-based sex clinic supporter writing in Planned
Parenthood’s journal reported: “Most school-based clinics begin
by offering comprehensive health care, then add family planning
services later, at least partly in order to avoid local controversy.
. .. Aclinic limited to providing family planning services, preg-
nancy testing, prenatal, and post-partum care, and testing for
and treatment of sexually transmitted diseases will be unaccept-
able even to many of the students,”

Another clinic advocate wrote: “The most common strategy
adopted to avoid controversy is to maintain a low profile ~gen-
erally by keeping programs out of sight . . . by relying on word
of mouth for recruitment, and by giving names to programs that
obscured their functions,”!16

Thus, to the students, to the parents, and to the taxpaying
community at large, the “clinics generally are presented as com-
