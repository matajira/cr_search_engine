Index

Leatherton, William, 119
legacy, 62-63

legislation, 133-138,. 143-145
legislature, the, 131-138, 271-273
Leithart, Peter, 372

Lenin, 57, 95, 231

Levine, Irving R., 168

Lewis, C. §., 310-311

Liberty Report, The, 384

Lichter, Linda, 169

Lichter, Robert, 169

Life magazine, 175

LifeNet, 394

Lincoln, Abraham, 276
Lippman, Walter, 180

local governments, 141-143, 269-270
Locke, John, 120

Lofton, John, 106

Lohman, Ann, 183-185

Louis XIV, 16

love, 248-250

Lacarius, Cyril, 236

Luther, Martin, 16,-326
Lutheran Church, Evangelical, 215

Machiavelli, Niccolo, 4

Madison, James, 278

Madonna, 16

malpractice suits, 70-71

Malthus, Thomas, 55-58, 93

Malthusianism, 55-58, 91-106

Malthusian League, 192-195

Manufacturers Hanover, 159

March of Dimes, 152, 156-158, 163, 302,
379

Margaret Mead, 154

Marshall, Robert, 345, 351, 352, 359,
375, 376

Marshner, Connie, 353

Marx, Karl, 189

Masters, Edgar Lee, 52

Masters, William, 110

masturbation, 57-58, 64, 112-114

McDowell, Josh, 223, 353, 371

McKasson, Sandalyn, 14, 359, 369

McLaughlin, Charles, 100

media, 165-187, 267-269

Medicaid, 28-30, 100-102

medical risks, 30-32, 33-34, 67-89

Medicare, 100-102

medicine, 83-87

Mellon Foundation, 61

403

Menninger, Karl, 59
menstrual extraction, 71
Merril Lynch, 159

Methodist Church, United, 215
Methodius, 211

Mexico City Policy, 139
Millay, Edna St. Vincent, 52
Mincius, 2it

minorities, 27-28
Montenegro, 190
Montesquieu, 16

More, Thomas, 41

Morgan Guarantee Trust, 160
morning after pill, 82
Mosher, Stephen, 349

Mudd, Roger, 172

murder, 71

Mussolini, Benito, 46

myths, 39-41

Napoleon, 269

Nathanson, Bernard, 173, 376

National Legal Foundation, 278

National Organization for Women, 159,
160

Nazism, 46, 61, 85, 94, 183

NBC, 171-172

Nehru, 153

New Age, the, 64

New York Daily News, The, 177

New York Times, 173-174, 184

Newspeak, 115, 120-122

Newsweek, 174, 177

New World Order, 198

Nietzsche, Frederick, 55

Nixon, Richard, 134

North, Gary, 393, 396

ONeill, Eugene, 52

obedience, 243-244

objectivity, 166-168

obscenity, 53-54, 107-110, 122-127

occultism, 60, 62, 64, 85-87, 229-232,
392

Olasky, Marvin, 159, 379, 361, 384, 304

oligarchy, 257

oppression, 28

Origen, 211

Orwell, George, 120, 122

Packer, J. 1., 40, 356
paganism, 198-205
