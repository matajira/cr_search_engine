The Camera Blinked: The Media Legacy 179

¢ Emphasize the dearth of other contraceptive options avail-
able—particularly in comparison with what is available in
other parts of the world;

© Emphasize the issues of privacy, ease, safety, choice, and
freedom, rather than of abortion and politics;

Emphasize the possibility of other medical benefits of the drug,
such as treatment of breast cancer and Cushing’s Syndrome;

© Emphasize the threat to the freedom of ongoing medical
research that a rejection of the drug might bring."8

Apparently, sometime thereafter, Bass and Howes began to
supply key media contacts with a series of carefully developed
press kits highlighting each emphasis. The kits included sample
stories, charts, graphs, photos, and interviews.

Charles Durran, a reporter for a major daily newspaper in
the Midwest told me: “Those press kits were impressive. In fact,
they were a lazy reporter's gold mine. Everything you needed for
a really fantastic story—or series of stories—was right there at
your fingertips. I don’t think I’ve ever seen anything like it.”

Before long, stories began appearing in newspapers and
magazines all over the U.S. and Britain—many of them were
rumored to bear a striking resemblance to the materials in the
press kits, while some were copied verbatim.1!9

Time, Newsweek, The Economist, and virtually every major
daily newspaper began to trumpet the five emphases that Bass
and Howes had outlined. As if on cue, they quoted the various
tenets of the credo without a hint of incredulity.

They claimed the drug was safe.12° They claimed it was easy
to use.!21: They claimed it could short circuit all the political
animosity.'22 They claimed it could cure all manner of other
ills.123 They claimed it was a litmus test for scientific freedom.1?+
And. they claimed it was merely an advanced contraceptive—
and everybody knows that only religious cranks and impotent
curmudgeons could possibly be against that.125

Of course, such fictions rank right up there with Elvis sightings,
Martian landings, Beatles reunions, and Ginsu knife commer-
cials.126 But the tragedy is not just that the stories were untrue
but that they were cribbed from someone else’s script. The media
