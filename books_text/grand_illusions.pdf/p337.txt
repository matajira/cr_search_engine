APPENDIX A

THE FAITHFUL
WITNESS: MAKING
A PRESENTATION

bona opera?

 

In history's mixture of good and evil, the thing we should note— the thing the his-
torians will note with amazement —is the profundity and the rapidity of change.3

Hilaire Belloc

With knowledge comes responsibility (Luke 12:42-48). Now
that you know what you do about Planned Parenthood, you
have a responsibility to share that information with others. Not
only that, you have a responsibility to use that information to
effect justice, mercy, and truth in your family, in your commu-
nity, and in your circle of influence.

If you are ever given the opportunity to speak before a
precinct caucus, or a PTA meeting, or a School Board session,
or a County Commissioner’s hearing, it is important that you
have your facts straight, your material organized, and your
focus clearly in view.

Making an effective presentation of the facts about Planned
Parenthood need not be a daunting experience. Most of the
work is already done for you. All you need to do is to pick and
choose your quotes, statistics, and anecdotes and, then, boldly
go forth (Matthew 28:19-20; Acts 1:8).

Here are a few tips on how to do just that:

First, write your whole presentation out, even if only in out-
line form. This will keep you on track and accurate. There is
nothing worse than obscure generalities or irrelevant rabbit

317
