348

11.
12,

13.
id,
15.

16.
7.
18.
49.
20,
QL.
22,
23,
24.
25.
26.
27,
28.
29,
30.
31.
32.
33.
34.

35.

36.

37.
38.
39,

Granp ILLusions

. The myth persists that Margaret Sanger, Planned Parenthood’s founder, was a

trained “public health nurse” (as the 1986 Planned Parenthood Annual Report
puis it), but that is patently untrue. See Madeline Gray, Afargaret Sanger: A
Biography of the Champion of Birth Control (New York: Richard Marek Publishers,
1979), p. 326.

“Celebrating, p. 14.

“Serving Human Needs, Preserving Human Rights,” 1983 Annual Report,
Pianned Parenthood Foundation of America, p. ii.

“Celebrating,” p. 32.

Ibid.

Planned Parenthood Affiliates, Chapters, and State Public Affairs Offices
Directory, 1984.

Ibid.

“Celebrating,” pp. 22-23.

Ibid., pp. 9, 12.

Ibid., pp. 9-11.

Ibid., pp. 18-19, 25-27.

Ibid., pp. 9-10.

“Serving,” p. 4.

*Celebrating,” p. 22.

Thid,, pp. 10, 21, 27.

Tbid., p. 24.

“Serving,” pp. 14-16.

Ibid., pp. 5-6.

“Celebrating,” pp. 3, 23.

Ibid., p. 13.

Ibid., pp. 8-9.

Ibid., pp. 16, 23.

Ibid, p. 9.

“Serving,” p. 13.

A full-page advertisement placed in the New York Times and several other news-
papers around the country during 1984 by Planned Parenthood Federation of
America stated, “We . . . have spent the better part of this century supporting
and fighting for everyone's freedom to make their own decisions about having
children. Without government interference.” See also Planned Parenthood Review
2:4 (Winter 1982).

‘Throughout this chapter and then beyond into the remainder of this book, the
term illusion is used both in a practical and a theological sense: contrary to the
facts of the Bible and contrary to the created order.

In 1969 Planned Parenthood President Alan Guttmacher asserted that “eventu-
ally coercion may become necessary.” It was shartly thereafter that his head of
research, Frederick Jaffe, issued a now infamous memo entided, “Examples of
Proposéd Measures to Reduce U.S. Fertility.” See Richard D. Glasow’s analy-
sis in “Ideology Compelis Fervid PPFA Abortion Advocacy,” National Right to
Life Naws (March 28, 1985), p. 5.

Ibid.

Ibid.

Ibid.

. See Frederick §. Jaffe, “Activities Relevant to the Study of Population Policy

for the U.S.,? Memorandum to Bernard Berelson, March 11, 1969, quoted in
Family Planning Perspectives special supplement no. 1129, 10-70/30, p. ix.
