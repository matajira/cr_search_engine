8s Granp ILLusions

Sex is not fruitful just because it’s fun—just because it en-
hances intimacy and communion. Some couples who enjoy extra-
ordinary bedroom exploits never know the fullness of fruitfulness.

Neither is sex fruitful just because it produces babies. Some
couples who are never able to have children are nonetheless
blessed with abundant ‘fruitfulness.

Fruitfulness is a wholistic concern. It encompasses both
recreation and procreation through a careful stewardship of
faith, hope, love, time, money, resources, and circumstances. It
balances them, like sovereignty and responsibility, in a life of
covenant faithfulness.

But Planned Parenthood is not too terribly interested in bal-
ance. Instead, it promotes dangerous drugs and surgical proce-
dures that guarantee — or at least purport to guarantee — an absolute
division between recreation and procreation. It promotes con-
traceptive methods that attempt to usurp God’s design for sex. It
promotes birth control measures that reduce sex to mere sport.

Abortion, then, is nothing but a recreational surgery. A dan-
gerously lethal recreational surgery. Similarly, the Pill—~along
with most of the other forms of prescription birth control—is
nothing but a recreational drug. A dangerously toxic recrea-
tional drug. Neither qualify as legitimate “health care.”

Isn’t it about time we told American teens and single adults
to “just say no”? Isn’t it about time we told them that there is a
better way —God’s way? Isn’t it about time modern medicine
stopped toying with the minions of perversion and death and re-
turned to the sanctity of life? Isn’t it?

Conclusion

“Abortion in America is a commodity,” argues author David
Reardon, “bought and sold for the convenience of the buyer and
the profit of the seller. Though abortion utilizes medical knowl-
edge, it is not medical—that is, abortions are not being prescribed
in order to heal the body or cure illnesses. . . . Even in the rare
cases where serious medical problems do exist because of the
pregnancy, abortion is still not good medicine.” !25

Amazingly, this dangerous and brutal procedure is the only
surgery which is legally protected from any sort of governmental
