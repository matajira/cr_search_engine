Idols for Destruction: A Strategy for Prophets and Priests 283

At times, it seems that the loopholes in the system are open
so wide that our prophetic and priestly duties are entirely unen-
cumbered.

Sadly, there are other times when the loopholes built into the
system close up tighter than a drum. Reba Elvin and the mem-
bers of her church found that out the hard way.

There are times, in fact, when the system seems only to work
against the cause of life and liberty.

Church historians for years have pondered with inquisitive
awe the uncharacteristically ferocious persecution that the New
‘Testament era believers faced. In a day of tolerance, prosperity,
and governmental stability, why did the intense public outrage
break forth?

Was there something about the early church rituals that par-
ticularly irked the Romans?

Almost unquestionably not. The Empire sheltered within its
folds all manner of esoterica and erratica. In comparison, the
church was tame to say the least.

Was there something about the early church doctrine that
particularly irked the Romans?

Again, that’s highly unlikely. Mystery cults, occultic covens,
and theosophic sects of the wildest order thrived under the toler-
ant wings of the Empire.®

So what was it that singled Christianity out to be so awfully
despised by the civil magistrates and the populace at large?

According to Francis Schaeffer, “The early Christians died
because they would not obey the state in a civil matter . . . they
were civil rebels. The Roman State did not care what anybody
believed religiously; you could believe anythirg or you could be
an atheist. But you had to pay homage to Caesar as a sign of
your loyalty to the state.”

The Christians said “no.” That is why they were thrown to
the lions. They were civil rebels. They were not imprisoned,
beaten, reviled, stoned, exiled, and executed because of their pe-
culiar dogmas. They met with persecution because they refused
to obey the government.

An excerpt from the first-century Law of the Twelve Tables
states that no Roman citizen was to “have gods on his own,
neither new ones nor strange ones, but only those instituted by
