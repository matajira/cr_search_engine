In the Heat of the Fight 7

the park, supervised children’s activities. Two men, in whose
veins flowed zealot’s blood, were taking turns reading passages
of Scripture over a megaphone. One was dressed in a banker’s
gray flannel suit. The other wore ragged jeans and a chambray
work shirt. Hardly heterogeneous, yet they testified as one that
the old building bore the sorry stains not only of bad taste, but of
bad will as well.°

I joined them.%

Every thirty minutes for the next two and a half hours, we
watched as a fresh clutch of doe-eyed girls were whisked into the
clinic by “pro-choice escorts.” They met the girls at their cars and
quickly aimed them up the sidewalk. They snarled at our offers
of help and batted away our literature. If a girl displayed the
least hint of hesitation, the “escorts” would take her by the arm
and rush her toward the door. So much for “choice.”#!

When, despite their best efforts, a frightened and confused
teen slipped their grasp and turned aside to talk to one of the
protesters, to read a Gospel tract, the “escorts” flew into a fren-
zied rage. They lunged at the picket line. Taunting, jeering,
cursing, and reviling, they tried to recapture their prey. One
turned her contorted, wild-eyed gaze toward me.

“You pig,” she sputtered. “You damned, chauvinist pig. Let
the girl go.”?2

T looked over my shoulder where the girl was kneeling in the
grass, quietly praying with several picketers, utterly incognizant of
the efforts of this thrashing, yammering champion for “choice.”?%

“Why don’t you go home? Mind your own business!” She
was right in my face, yelling in my ear, shoving, red-faced, and
livid. “You’re traumatizing the girl, you pig.”

She went on and on, clichés repeated like a worn-out record.
But all to no avail. The girl was walking away, arm in arm with
her new-found friends. She said she was keeping her baby.?*

Frustrated, the “escorts” retreated to the building, A quick
conference ensued with the clinic director, two nurses, and a
security guard. They were clearly disturbed and kept gesturing
in our direction with stabbing fingers and malevolent stares.
After a few moments of haggling between themselves, they dis-
patched the guard, presumably to “restore order” to this now
thoroughly unpleasant Saturday morning.
