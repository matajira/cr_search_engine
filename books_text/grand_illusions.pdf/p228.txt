208 Granp ILLustons

of life and hope and truth. The civilized world would never be
the same.

Back in the days when historians were but simple men, the
catastrophic conquest of Constantinople was believed to mark
the end of the Middle Ages. In these more complex and cosmo-
politan times we know only too well that “the stream of history
flows on relentlessly and there is never a barrier across it.”? As
discontinuous as the events in 1453 were, we must say that there
is no sudden or precise point at which the medieval world was
transformed into the modern world. Long before 1453, the Ren-
aissance had begun in Florence, Venice, Genoa, and Paris.
Long after 1453, the feudal life persisted in Flanders, Bavaria,
and Russia. Long before 1453, the great navigators and discov-
erers had begun to explore the ocean routes that would ultimately
alter the economy of the whole world. Long after 1453, vast, vast
uncharted realms were still left for the stout of heart to claim.

, As calamitous as the events in 1453 were, they \ were largely
symbolic, They were neither the beginning nor the end.

Since Calvary there have been no absolute divisions in his-
tory, only benchmarks.

Thus in 1973, when another citadel of majesty fell, the stream
of history, though disturbed, continued to flow ever onward. As
in the sack of Constantinople, this modern assault on life and
hope and truth would reverberate throughout the civilized
world, shaking the foundations of every institution: family,
church, and state. And yet, long before the Supreme Court’s
Roe v. Wade decision legalizing abortion that year, liberty and
security had already been seriously jeopardized. Several states
had already liberalized their abortion laws.t Tax funding for
Planned Parenthood had already been appropriated.5 And
epidemic promiscuity had already begun to ravage the land.®
Long after 1973 strong and righteous resistance challenged the
seductive tyranny of death on demand.’ The church arose from
its cultural slumber and began to reassert its disciplining role in
society. Pro-lifers developed creative alternatives for women
and children in crisis.? And serious legal challenges continued
to threaten Planned Parenthood’s death grip on the nation’s
purse strings. 1°

As calamitous as the decision in 1973 was, like the fall of
Constantinople, it was largely symbolic. It was. neither the be-
ginning nor the end.
