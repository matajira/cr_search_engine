322 Granp [LLusions

it (Romans 9:32; Galatians 3:10; Ephesians 2:9); and Peter con-
demned it (2 Peter 1:3-4; 2:1-22),

“Salvation by works” is heretical in every way imaginable. It
abolishes the significance of the cross (Galatians 5:11). It makes
light of Christ’s sacrifice (Galatians 2:21). It mullifies the work of
the Holy Spirit (Galatians 3:3-5). It abrogates the necessity of
grace (Romans 4:4). “Faith is made void and the promise is
nullified” (Romans 4:14) because it makes man and man’s ability
the measure of all things (Matthew 15:6-9). Thus, the Social Gos-
pel is nothing more than another form of Humanistic Paganism.5

Salvation according to the Bible is a work of sovereign grace.
There is absolutely nothing we can do to merit God’s favor: “He
saved us, not on the basis of deeds which we have done in right-
eousness, but according to His mercy, by the washing of regen-
eration and renewing by the Holy Spirit, whom He poured out
upon us richly through Jesus Christ our Savior, that being justi-
fied by His grace we might be made heirs according to the hope
of eternal life” (Titus 3:5-7).

All men are without exception sinners. And thus, all men are
both doomed and damned apart from God’s providential and
unilateral grace provision. Thus, the most fundamental factor in
understanding anthropology® is not ideological, psychological,
or sociological. It is instead soteriological. The most fundamental
factor in understanding anthropology is the thanatos factor.”

Very. simply, because of sin all men have morbidly em-
braced death and apart from Christ are utterly without hope
(Romans 5:12).

At the Fall, mankind was suddenly destined for death (Jere-
miah 15:2), We were all at that moment bound into a covenant
with death (Isaiah 28:15).

There is a way that seems right to a man, but its end is the way
of death (Proverbs 14:12: 16:25).

Whether we know it or not, we have chosen death (Jeremiah
8:3). It has become our shepherd (Psalm 49:14), Qur minds are
fixed on it (Romans 8:6), our hearts pursue it (Proverbs 21:6),
and our flesh is ruled by it (Romans 8:2). We dance to its cadences
(Proverbs 2:18) and descend to its chambers (Proverbs 7:27).
