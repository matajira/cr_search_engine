Bad Seed: The Historical Legacy 65

mercenary avariciousness is everywhere apparent.°2 Bad seed
brings forth bitter harvest. The legacy continues.

Religion. In her first newspaper, The Woman Rebel, Margaret
Sanger admitted that “Birth contro] appeals to the advanced
radical because it is calculated to undermine the authority of the
Christian churches. I look forward to seeing humanity free
someday of the tyranny of Christianity no less than
Capitalism,”93 Today, Planned Parenthood is continuing her
crusade against the church. In its advertisements, in its liter-
ature,°? in its programs,%¢ and in its poticies,°’ the organization
makes every attempt to mock, belittle, and undermine Biblical
Christianity. Bad seed brings forth bitter harvest. The legacy
continues.

Deceit. Throughout her life, Margaret Sanger developed a
rakish and reckless pattern of dishonesty.®* She twisted the truth
about her qualifications as a nurse,°? about the details of her
work, !©° and about the various sordid addictions that controlled
her life. Her autobiographies were filled with exaggerations,
distortions, and out-and-out lies.!°2 She even went so far as to
alter the records in her mother’s family Bible in order to protect
her vanity.!3 Today, Planned Parenthood faithfully carries on
her tradition of disinformation. The organization continually
misrepresents the facts about its lucrative birth control, sex
education,!°5 and abortion enterprises.!%° Bad seed brings forth
bitter harvest. The legacy continues.

A recent Planned Parenthood report bore the slogan “Proud
of Our Past— Planning the Future.”/07 If that is true—if the or-
ganization really is proud of its venal and profligate past, and if it
really is planning the future—then we all have much to be con-
cerned about.

Those who plow iniquity and those who sow trouble harvest it,
By the breath of God they perish, and by the blast of His anger
they come to an end (Job 4:8-9).
