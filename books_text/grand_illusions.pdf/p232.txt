212 Granp ILLUSIONS

the sanctity of life out of thin air. Their common affirmation was
but an obedient response to God’s Revelation, the Bible,

It was abundantly clear to those faithful early Christians that
the Scriptures commanded a reverence for life. Embedded in
every book and interwoven into every doctrine was the un-
wavering standard of justice and mercy for all: the weak and the
strong, the great and the smail, the rich and the poor, the lame
and the whole, the young and the old, the unborn and the born.

The Bible declares the sanctity of life in its account of God’s
creation (Genesis 1:26-28; 1 Timothy 6:13; Psalm 36:9, Psalm
104:24-30; John 1:34; Acts 17:25).

Woe to him who strives with his Maker! Let the potsherd strive
with the potsherds of the earth. Shall the clay say to him who
forms it, “What are you making?” Or shall your handiwork say,
“He has no hands”? Woe to him who says to his father, “What
are you begetting?” or to the woman, “What have you brought
forth?” Thus says the Lord, the Holy One of Israel, and his
Maker: “Ask Me of things to come concerning My sons; and
concerning the work of My hands, you command Me. I have
made the earth, and created man on it. It was My hands that
stretched out the heavens, and all their host I have commanded
(Isaiah 45:9-12).

The Bible declares the sanctity of life in its description of
God’s sovereignty (Deuteronomy 32:39; Psalm 22:9-10; Job
10:12; John 5:21; Romans 11:36; Colossians 1:16-17).

For You have formed my inward parts; You have covered me
in my mother’s womb. I will praise You, for I am fearfully and
wonderfully made; marvelous are Your works, and that my
soul knows very well. My frame was not hidden from You,
when I was made in secret, and skillfully wrought in the
Jowest parts of the earth. Your eyes saw my substance, being
yet unformed, And in Your book they all were written, the
days fashioned for me, when as yet there were none of them
(Psalm 139:13-16).

The Bible declares the sanctity of life in its discussion of
Christ’s incarnation (John 3:16; John 11:25; John 14:6; Acts
2:22-28; Colossians 3:4; Romans 5:21).
