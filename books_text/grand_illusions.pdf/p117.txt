A Race of Thoroughbreds: The Racial Legacy 7

They wasted no time in getting started.

In 1939, they designed a “Negro Project” in response to “south-
ern state public health officials”— men not generally known for
their racial equanimity.5* “The mass of Negroes,” the project
proposal asserted, “particularly in the South, still breed carelessly
and disastrously, with the result that the increase among
Negroes, even more than among Whites, is from that portion of
the population least intelligent and fit."5+ The proposal went on
to say that “Public Health statistics merely hint at the primitive
state of civilization in which most Negroes in the South live.”5>

In order to remedy this “dysgenic horror story,” the project
aimed to hire three or four “Colored Ministers, preferably with
social-service backgrounds, and with engaging personalities” to
travel to various Black enclaves and propagandize for birth con-
trol.56 “The most successful educational approach to the Negro,”
Margaret wrote sometime later, “is through a religious appeal.
We do not want word to go out that we want to exterminate the
Negro population and the Minister is the man who can
straighten out that idea if it ever occurs to any of their more re-
bellious members.”5? Of course, those Black ministers were to be
carefully controlled—mere figureheads, “There is a great danger
that we will fail,” one of the project directors wrote, “because the
Negroes think it a plan for extermination. Hence, let’s appear to
let the colored run it.”5* Another project director lamented, “I
wonder if Southern Darkies can ever be entrusted with... a
clinic. Our experience causes us to doubt their ability to work
except under White supervision.”*? The entire operation then
was a ruse—a manipulative attempt to get Blacks to cooperate in
their own elimination.

The project was quite successful. Its genocidal intentions
were carefully camouflaged beneath several layers of condescend-
ing social service rhetoric and organizational expertise. Like the
citizens of Hamelin, lured into captivity by the sweet serenades
of the Pied Piper, all too many Blacks all across the country hap-
pily fell into step behind Margaret and the Eugenic racists she
had placed on her Negro Advisory Council. Soon clinics
throughout the South were distributing contraceptives to Blacks
and Margaret’s dream of discouraging “the defective and dis-
eased elements of humanity” from their “reckless and irresponsi-
ble swarming and spawning” was at last being fulfilled.6
