Idols for Destruction: A Strategy for Prophets and Priests 267

gram? Building relationships with the students and disciplng
them for Christ will do more to stymie Planned Parenthood than
almost anything else.

One way or another, we need to reach out to our schools. We
need to serve them as prophets and priests, guiding and guard-
ing the land.

The Media

Planned Parenthood, for all intents and purposes, has the
media in its pocket. It has accomplished that feat in part by
shrewdly developing and craftily implementing a comprehensive
system of news releases, news contacts, media spokesmen, data
retrieval, source procurement, public relations campaigns, ex-
pert analysis, advertising, and interview leads.** As a result, it
has been able to nurture stories, do lead generation, time break-
ing events, create desirable publicity, snowball feature items,
and shape public opinion,*>

Why has the church failed to develop that kind of media
acumen? Why have pro-life groups not been able to exploit the
media as a wedge against public policy the way Planned Parent-
hood has? Certainly, it is not because the opportunities are lack-
ing. It is not because the technology is unavailable. Nor is it
because the resources are inaccessible. But, for some reason, we
have yet to make the long-term commitments necessary to turn
the media around.

That must change. If we are going to serve our society as
prophets and priests, guiding and guarding the land, we are
going to have to recapture the media.

There are several things that each of us can do to do just that.

First, we can write letters to the editor. If we have something
to say, we ought to say it. Instead of constantly murmuring and
complaining about the biased coverage in our local newspapers
and magazines, we can voice our opinion. Why not offer an al-
ternative perspective of the latest Planned Parenthood initiative?
Or, perhaps, we can gain publicity for the positive programs that
our local church or alternative center is offering? A brief,
polite, respectful, and well-phrased letter may be able to spark
a community-wide dialogue where the facts about Planned
Parenthood can actually come to light.
