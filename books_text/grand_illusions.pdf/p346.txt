326 Granp ILLusIONs

salvation, not the cause of salvation. It is a way of Life, not a way
of salvation. And therein lies all the difference.

This then is the faith “which was once for all delivered to the
saints” (Jude 3). This is the faith that orthodox believers have
held to throughout the centuries — from the Patristic Age, through
the Protestant Reformation, and to the present day:

Ignatius of Antioch (14-107). “Carnal people cannot act spiri-
tually, or spiritual people carnally, just as faith cannot act like
unbelief, or unbelief like faith... . Those who profess to be
Christ’s will be recognized by their actions. . . . They will give
and be given graciously unto the poor just as the Lord gave and
was given unto them,”9

Clement of Rome (34-100). “Let us not merely call Him Lord.
... Let us acknowledge Him by our actions of mercy and
charity”!

The Didache (110). “Every prophet who teaches the truth but
fails to practice what he preaches is a false prophet. . . . Ministry
to the afflicted is but a mark of faith. Yet the absence of that
mark is evidence of an absence of that faith.”!!

Basil of Caesarea (330-379). “A man who has two coats or two
pair of shoes, when his neighbor has none, evidences a lack of
grace in his life. . . . The redistribution of wealth is in no wise
the point. The revealing of faith is the point.”!2

John Chrysostom (347-407). “The essence of the Gospel is not
concern for the poor but it certainly provokes that concern. In
fact without that concern, the essence of the Gospel surely has
not been grasped.”!3

Ambrose of Milan (339-397). “Faith is the begetter of a good
will and of good actions to the least of these.”!*

Jon Hus (1369-1415). “Doubt must be cast on fruitless lives.
Profession must be followed by deeds of charity, otherwise that
profession is false.”

Martin Luther (1483-1546). “Where there are no good works,
there is no faith. If works and love do not blossom forth, it is not
genuine faith, the Gospel has not yet gained a foothold, and
Christ is not yet rightly known.”!6

The Augsburg Confession (1530). “It is necessary to do good
works; not that we may trust that we deserve grace by them, but
because it is the will of God that we should do them,”!7
