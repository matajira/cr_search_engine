62 Granp ILLustons

beyond repair. And her entire uterus was raw and inflamed.
The doctor had no choice but to schedule her for a complete
hysterectomy.

“f'm so. thankful that my doctor knew just what to do,” she
later told me. “I could very easily have died. My neighbor, of
course, keeps insisting that my problems were unusual and
isolated. But I know better. Everywhere I go, I hear horror stor-
ies from women who have been misled by Planned Parenthood
and suffered the consequences.”

Other Devices. Although the Pill and the IUD are Planned
Parenthood’s favored contraceptive technologies, several alter-
native methods are available at its clinics as well.

Ever since Margaret Sanger began smuggling diaphragms
into the country—in liquor bottles through rum-runners—
Planned Parenthood has offered the soft rubber barrier devices
to its clients.24 Used alone, the diaphragm’s effectiveness is
dubious at best. Used in conjunction with spermicidal foams,
creams, and jellies, its effectiveness increases significantly, but its
safety plummets. Several spermicides have been forced off drug-
store shelves due to serious medical complications and a number
of multi-million dollar lawsuits.®°

Vaginal contraceptive sponges and cervical caps may share
that same fate.°6 Although both offer some contraceptive protec-
tion as a partial barrier within the uterus, their real effectiveness
depends on supplementary spermicidal agents. And there is the
rub, The spermicides have simply not proven to be safe. They can
cause everything from minor allergic reactions to serious vaginal
infections, from cervical irritation to serious hormonal imbal-
ances, from a slight genital abscess to chronic endometritis.°”

Robin Cohen began to us¢ a spermicidal jelly after being
fitted for a diaphragm at a Planned Parenthood clinic near her
home. Both she and her husband experienced a good deal of ir-
ritation during intercourse, so she changed brands. “With the
second brand, both Bill and I broke out in a painful genital
rash,” she said. “So I quickly changed brands again. This time,
I tried a cream. And that was even worse. I came down with a
serious vaginal infection and had to take antibiotics for almost
two wecks.”
