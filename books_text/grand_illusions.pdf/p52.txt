32 GRAND ILLUSIONS

part of the problem, serving as a conduit for “unsafe” sexual
practices. We simply cannot contend or pretend otherwise.

The Sex Education Illusion

Planned Parenthood claims that sex education is a necessary and effec-
tive means of preventing teen pregnancies.1°? But that is an illusion.

Planned Parenthood’s multi-million-dollar, tax-funded edu-
cational efforts have proven to be anything but necessary and
effective.

The truth is, Planned Parenthood’s sex education programs
have backfired, actually increasing teen pregnancies. According
to its own survey, conducted by the Louis Harris pollsters,
teens who have taken “comprehensive” sex education courses
have a fifty percent higher rate of sexual activity than their “un-
enlightened” peers.#3 And yet the courses had no significant
effect on their contraceptive usage.'%+ The conclusion, one that
even Planned Parenthood researchers have been unable to
escape, is that sex education courses only exacerbate the teen
pregnancy problem.™5

In an effort to denounce the threat that such a conclusion
poses to its precious empire, Planned Parenthood has erected a
scaffold of spurious statistics, studies, and surveys.'36 A long
fusillade of figures come clamoring out of it. Fresh salvos of
arithmetic are marshalled to the cause. But all to no avail. The
cold hard facts like granite tetons straddling its path have forced
Planned Parenthood to go on a quixotic offensive.13”

In 1970 fewer than half of the nation’s school districts offered
sex education curricula and none had school-based birth control
clinics.'38 Today more than seventy-five percent of the districts
teach sex education and there are more than one hundred clinics
in operation.!89 Yet the percentage of illegitimate births has only
increased during that time, from a mere fifteen percent to an
astonishing fifty-one percent.'#©

In California, the public schools have required sex education
for more than thirty years, and yet the state has maintained one
of the highest rates of teen pregnancy in the nation.'#!

According to the Harris poll, the only things that effectively
impact the teen pregnancy problem are frequent church aitten-
dance and parental oversight,'#2 the very things that Planned
