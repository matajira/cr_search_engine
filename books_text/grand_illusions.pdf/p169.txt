EIGHT

STRANGE BEDFELLOWS:
THE INSTITUTIONAL
SCANDAL

asinus asinum fricat!

 

One can only say to ancient sophistical difficulties, that the sense of men establishes
jor itself the true limits of any object, as of freedom.?

Hilaire Belloc

Beyond the tall, eighth-fioor windows, the Houston city-scape
darkened to a drab December twilight, then blossomed into the
glittering and sparkling gem that is the city’s trademark. Below,
the crowds had begun thinning along Milam Street. The light
changed at Rusk and headlights began moving again, poking
through the thin, slanting winter rain. Across the street, a long,
weary ‘line of pedestrians dodged puddles and potholes—some
straggling home after a full day’s grind, some catching a bite to
eat before taking on the snarled freeways, others going to assig-
nations over cocktails in boisterous corner bistros. They moved
past the endless upward thrust of new, grimly skeletal con-
structions that seemed to punctuate each block. Stopping, even
in the rain, to peer through holes cut into the wooden walkways,
they stared at now-quiet earthmovers and watched the arcs, pink
and orange, of the helmeted welders up among the girders,

Dana Meier squinted into the night. Rain blew streaks
against the window. The nervous glare of neon and halogen
refracted a pulsing nimbus of come-on colors.

It was a familiar scene —a daily ritual for her. Over the last
several months, Dana had watched the Republic Bank complex,
directly across from her window, rise magnificently from the

49
