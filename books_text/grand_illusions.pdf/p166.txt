I46 Granp ILLusions

prehensive, multi-service units that emphasize physical exam-
inations and treatment of minor illnesses.”1"7

But that is hardly their real purpose. Planned Parenthood re-
member, isn’t interested in scrapes and bruises or sniffles and
sneezes. It is interested in sex.

That is why Planned Parenthood advises that “when a stu-
dent comes to the clinic ostensibly for other reasons, the clinic
staff can take that opportunity to see if the student wants to dis-
cuss sexual behavior and birth control.”18

Planned Parenthood’s former medical director Louise Tryer
urges that, “every medical contact should be utilized as an
opportunity to offer the option of contraception.”!!9

The plan is to encourage the clinic personnel “to become ag-
gressive counselors to young women. If, for example, a young
patient comes in for tennis elbow, the physician should manage to
introduce birth control... . The conversation should then be
directed to use of abortion in the first trimester if traditional
methods fail.”!7°

This kind of deception is the third prong of Planned Parent-
hood’s scheme to scalp the American taxpayer. By funneling
federal money through state and local governments behind the
taxpayers’ backs, it is able to establish its programs. By applying
the money to teens behind their parents’ backs, it is able to expand
those programs. And by planting covert clinics in the schools
behind everybody's back, it is able to recruit for those programs.

Without the school-based clinic network, Planned Parent-
hood’s ambitious plan to achieve “the perfect contracepting soci-
ety” becomes little more than a pipe dream.’ Without that
recruitment program, the financial foundations that the organi-
zation has laid over the years through arduous injustice would
be washed away by the tides of time.

So the deception is essential. For both the short run and the
long run.

Kelley Frias and Roma Ratiglia are classmates in the ninth
grade at a large high school in the upscale Orange County
suburbs of Los Angeles. When they both decided to try out for
the school’s swim team, they went together to the health clinic
for a mandatory physical exam.

“One of the first things the nurse asked me,” Kelley related
“was whether I was on birth control. When I told her I wasn’t she
