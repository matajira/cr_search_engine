Select Bibliographic Resources 335

William Sanger, The History of Prostitution (New York, NY: Eugenics
Publishing, 1939).

Robert L. Sassone, Handbook on Population, 4th edition (Santa Ana,
CA: Sassone Press, 1978).

Monica Sjoo and Barbara Mor, The Great Cosmic Mother: Rediscovering
the Religion of the Earth (San Francisco, CA: Harper & Row, 1987).

Lothrop Stodard, The Rising Tide of Color Against White World-Supremacy
(New York: Charles Scribners, 1920).

Abraham Stone and Norman E. Himes, Planned Parenthood: A Practical
Handbook of Birth Control Methods (New York: Collier Books, 1965).

Hannah and Abraham Stone, 4 Marriage Manual (New York: Simon
and Schuster, 1968).

Pro-Life Movement

Randy C. Alcorn, Is Rescuing Right? Breaking the Law to Save the Unborn
(Downers Grove, IL: IVP, 1990).

Randy G. Alcorn, Pro-Life Answers to Pro-Choice Objections (Portland,
OR: Multnomah, 1992).

Joan Andrews with John Cavanaugh-O’Keefe, I Will Never Forget You:
The History of the Rescue Movement in the Life of Joan Andrews (San Fran-
cisco: Ignatius Press, 1989).

Joan Andrews, You Reject Them, You Reject Me: The Prison Letters of Joan
Andrews (Brentwood, TN: Wolgemuth & Hyatt, 1988.)

Dave Andrusko, To Rescue the Future: The Pro-Life Movement in the 1980s
(Harrison, NY: Life Cycle Books, 1983),

Dave Andrusko, Window on the Future: The Pro-Life Year in Review —1986
(Washington, DC: The National Right-to-Life Committee, 1987).

Mark Beltz, Suffer the Little Children: Christians, Abortion and Civil Disobe-
dience (Westchester, IL: Crossway Books, 1989).

Dr. Caroline Berry, The Rites of Life: Christians and Bio-Medical Decision
Moking (London: Hodder and Stoughton, 1987).

Lynn Buzzard and Paula Campbell, Holy Disobedience: When Christians
Must Resist the State (Ann Arbor, MI: Servant, 1984).
