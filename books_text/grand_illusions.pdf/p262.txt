242 Granp ILLustons

own; but because you are not of the world, but I chose you out
of the world, therefore the world hates you. Remember the
word that I said to you, “A slave is not greater than his master.”
If they persecuted Me, they will also persecute you; if they kept
My word, they will keep yours also (John 15:18-20).

We need not be disheartened by this. Persecution does not
just bring with it pain and privation; it also brings great purpose
and promise. It brings with it the hope of resurrection.

“God causes all things to work together for good to those
who love God, to those who are called according to His pur-
pose” (Romans 8:28). When we refuse to compromise, we may
risk rejection or rebuke. But we know all the while that rejec-
tion and rebuke become opportunities under the sovereign direc-
tion of God Almighty.

Joseph risked everything he had by refusing to compromise
his obedience to God (Genesis 39:7-16). As a result, he was
thrown into prison (Genesis 39:19-20). But God used his prison
experience for good. It became the first stage of dominion for
him, It was not long before Joseph was raised up out of the depths
to rule over the whole land (Genesis 41:37-45). Because he re-
mained courageous and uncompromising, he emerged victorious.

David, too, risked all that he had by refusing to compromise
his obedience to God (1 Samuel 18:1-6). As a result, he was cast
into exile (1 Samuel 19:11-18), But God used his exile experience
for good. It became the first stage of dominion for him. It was
not long before David was raised up out of the depths to rule
over the whole land (2 Samuel 2:4). Because he remained cour-
ageous and uncompromising, he emerged victorious.

Similarly, Daniel risked all that he had by refusing to com-
promise his obedience to God (Daniel 6:10). As a result, he was
thrown into the lions’ den (Daniel 6:16-17). But God used his
lions’ den experience for good. It became the first stage of do-
minion for him. It was not long before Daniel was raised up out
of the depths to rule over the whole land (Daniel 6:23-28).
Because he remained courageous and uncompromising, he
emerged victorious.

The early Christians also risked all that they had by refusing
to compromise their obedience to God (Acts 4:19-20). As a
result, they were thrown into prison (Acts 5:19). But God used
