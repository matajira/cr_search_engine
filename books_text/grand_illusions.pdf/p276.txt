256 Granp ILLusions

by non-profit organizations. So, besides the fact that the pastor
and the membership were trespassing on private property and
attempting to restrict the constitutional rights of others, they
were also transgressing the time-honored . . . wall of separation
between church and State.”*

But that is not exactly how eye witnesses of the incident
reported it.

“First of all,” Reba told me, “no one was on Planned Parent-
hood’s property. Everyone was on the public sidewalk in front of
their building. No one even stepped on the grass. Secondly, no
one was attempting to restrict anybody’s constitutional rights.
The people from the church were just there to pray—dquietly,
peaceably, and in order. No one blocked ingress or egress. No
one stopped clients from entering if they wanted to. Now really,
since when is praying an attempt to restrict somebody's rights?
Thirdly, this wall of separation business is nothing more than an
attempt to keep Christians out of public life. If we're ever going
to adequately deal with groups like Planned Parenthood, we're
going to have to put that myth to rest.”

Church and State

According to the design for society outlined in the Bible,
church and state are separate institutions.5 They have separate
jurisdictions. They have separate authorities. And they have
separate functions.

A balanced social order depends on that kind of institutional
differentiation, When any one institution begins to encroach
upon another, anarchy and tyranny inevitably result. Checks
and balances begin to break down.

Thus, it is important that the state not meddle in the affairs
of the church, That is simply not its concern. The church is
outside of the state’s jurisdiction. The framers of the American
Constitution recognized this fundamental plank of liberty and,
thus, the first article in the Bill of Rights states, “Congress shall
make no law respecting an establishment of religion, or pro-
hibiting the free exercise thereof.” The state has no authority
over the church and, therefore, must not regulate or interfere in
the work of the church, Local municipalities, and even individ-
ual commonwealths, might render support to the church—as
they often did—but never were they to control the church.?
church and state are separate.
