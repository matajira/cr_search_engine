INDEX

 

abortion
alternatives to, 176-182
aticient times, in, 209-241
birth control and, 30-31, 61, 209-211
choice and, 17, 25-27
church and, 207-226
DEC, 73
D&E, 73-74
dangers of, 26, 33-34, 69-78
death and, 33-34, 70
Dilatation and Curettage, 73
Dilatation and Evacuation, 73-74
language and, 120-122
medical risks of, 26, 33-34, 69-78
Menstrual Extraction, 71
murder and, 71, 209-211, 212-215
occultism and, 392
pain and, 77
Planned Parenthood and, 23, 25-27,
58, 63, 67-89, 141-143
post-abortion syndrome, 126, 183
profits, 23, 136-138
Prostaglandin, 76
psychological effects of, 126, 183
racism and, 27, 61, 98-100
RU-486, 82
Saline Amniocentesis, 74-76
sex education and, 117-119, 265
social effects, 23, 126
sorcery and, 392
sterilily and, 34, 70
Suction Aspiration, 72
U.S. statistics, 23, 99
world-wide statistics, 23, 25, 70, 182
ABC, 172-173
Absalom, 185-187
Achan, 224-225
ACLU, 187
Adams, John, 278
Adeimantos of Thessily, 295

Actna, 159

agathism, 257

Ai, 224-225

AIDS, 30-31, 31-32, 128

Akron v. Akron Center for Reproductive
Health, 274

alertness, 234-237

Alexandrian model, 7

Alllicd Stores, 159

Allyson, June, 173

almsgiving, 296-303

alternative centers, 26, 176-182, 302

Ambrose of Milan, 86, 209, 211, 326

American Baptist Church, 215, 220

American Center for Law & Justive, 278

American Express, 159

Ameritech, 159

amniocentesis, 56-158

Anarchism, 50-51, 58

Argyros, Basil, 41

Aristotle, 209

Aryanism, 91-106

AT&T, 159

Athanasius, 16, 236

Athenagoras, 210, 284

Atlantic Richfield, 159

Augsburg Confession, 326

Augustine, 41, 244, 236

Aurelius, Marcus, 210

Auschwitz, 19, 61

Babbit, 64
back-alley abortions, 33

Balak strategy, 218-223

Balzac, Honore de, 148

Baptist Church, American, 215-220
barrenness, 83-87, 88

barrier devices, 31

Basil the Great, 16, 86, 211, 236, 326
Bass & Howes, 178

399
