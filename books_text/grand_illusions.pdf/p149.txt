Selling Sex: The Educational Scandal 129

Certainly, we need to battle the blazing concupiscence of
Planned Parenthood’s sex education programs by sounding the
alarms in PTA meetings, community forums, and school board
hearings. But, in the meantime, it is essential that we rescue our
own children from the flickering flames of promiscuity and per-
dition. At all costs.
