Introduction: Ad Vitum 3

simply that the book might be quickly dismissed and its message
ignored in the marketplace of ideas. They felt that it might be
perceived as impractical, apolitical, and unpragmatic when in
point of fact it is none of those things. In a milieu where tradi-
tional values have been exiled to a desolate cultural outback and
where the proponents of those values have voluntarily
sequestered themselves in a squalid spiritual and intellectual
ghetto, those concerns are not at all unwarranted.

Even so, I feel that the only appropriate response to Planned
Parenthood is a distinctively Christian response. And I am en-
tirely at ease in announcing that from the start.

G. K. Ghesterton once quipped that any new book of mod-
ern social inquiry is bound to be dullardly predictable in both
form and function:

Tt begins as a rule with an analysis, with statistics, tables of
population, decrease of crime among Congregationalists,
growth of hysteria among policemen, and similar ascertained
facts; it ends with a chapter that is generally called The Remedy.
It is almost wholly due to this careful, solid, and scientific
method that The Remedy is never found. For this scheme of
medical question and answer is a blunder; the first great
blunder of sociology. It is always called stating the disease be-
fore we find the cure. But it is the whole definition and dignity
of man that in social matters we must actually find the cure be-
fore we find the disease.?

This book is obviously an exploration, explanation, and ex-
position of the disease of Planned Parenthood. But as Chesterton
has said, we need not approach our subject medically —which
might lead us to a repugnant victimization or co-dependency model
of social relationships. In this case, it is entirely appropriate for
us to announce the cure before we engage in examination and
diagnosis or indulge in recovery and relating. The cure is, very
simply, the Word of God. The Scriptures. The Bible.

The Bible is God’s own revelation of wisdom, knowledge,
understanding, and truth. It is not simply a marvelous collection
of quaint sayings and inspiring stories. It is God’s message to
man. It is God’s instruction. It is God’s direction. It is God's
guideline, His plumb line, and His bottom line.

All those who in faith have gone on before us— forefathers,
fathers, patriarchs, prophets, apostles, preachers, evangelists,
