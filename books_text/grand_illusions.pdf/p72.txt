52 Granb IiLLusions

attracted the support of Governors, Congressmen, and even
President Taft. The battle was won, but the war was lost—the
revolution never made it to the streets because the anger of the
rebellion was diffused by the acceptance of the establishment.
The IWW was unable to recover from its victory and was never
again able to stage a successful strike. Margaret returned to
William and the children, despondent and discouraged.

In the weeks that followed, she occupied herself by dabbling
in midwifery by day and by holding court in Mabel Dodge’s
salon by night.

Dodge was a wealthy young divorcée, recently returned
from France, where she had spent most of her married years.
She had a stunning Fifth Avenue apartment where she started
a salon modeled after those in the Palais Royale and Paris’s
Left Bank: Her series of evenings were opportunities for intel-
lectuals, radicals, artists, actors, writers, and activists to
gather, mingle, debate, aspire, and conspire. Each night had its
own theme: sometimes it would be politics, sometimes drama,
or perhaps poetry or economics or art or science. Ideas and li-
quor flowed freely until midnight, when Dodge would usher in
a sumptuous mieal of the finest meats, poultry, cheeses, and
French pastries.

Margaret's topic of discussion was always sex. Her detour
into labor activism had done little to dampen her interest in the
subject. When it was her turn to lead an evening, she held
Dodge’s guests spellbound, ravaging them with intoxicating no-
tions of “romantic dignity, unfettered self-expression, and the
sacredness of sexual desire.”3® Free love had been practiced
quietly for years by the avant-garde intellectuals in the Village.
Eugene O'Neill took on one mistress after another, immor-
talizing them in his plays. Edna St. Vincent Millay “hopped
gaily from bed to bed and wrote about it in her poems.”59 Max
Eastman, Emma Goldman, Floyd Dell, Rockwell Kent, Edgar
Lee Masters, and many others had for some time enjoyed unre-
strained sexploits.‘? But no one had championed sexual free-
dom as openly and ardently as Margaret.*! When she spoke, the
others became transfixed. Dodge was especially struck by her
sensuous didactae. Later she would write in her memoirs:
