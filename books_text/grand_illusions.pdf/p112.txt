92 Granp ILLusions

Carrie had recently been committed to a state institution for
epileptics, where her mother Emma was also a patient. Upon
admission she had been given an I.Q. test and was found to
have “a mental age of nine years.” Emma was said to have “a
mental age of slightly under eight years,” and Carrie’s seven-
month-old baby was said to have “a look” that was “not quite nor-
mal.”3 That was evidence enough for the state health officials.
They invoked a Virginia Law that required sterilization in fami-
lies where “hereditary mental deficiency” and “feeblemindedness”
could be demonstrated over three successive generations.

Medical experts supplied the court with depositions claiming
that Carrie’s alleged “feeblemindedness” was “unquestionably
hereditary” — without ever having examined Carrie, her mother,
or her daughter in person.* One of these long-distance experts, a
renowned genetic biologist, asserted that Carrie’s family belonged
to “the shiftless, ignorant, and worthless class,” that “modern
science and beneficent social legislation is obligated to eradicate
for the greater good of the White Civilization.”5

Apparently the court agreed. In his opinion, Justice Holmes
wrote, “We have seen more than once that the public welfare
may call upon the best citizens for their lives. It would be strange
if it could not call upon those who already sap the strength of the
State for these lesser sacrifices... . in order to prevent our
being swamped with incompetence.” He concluded, “Three gen-
erations of imbeciles are enough.”6

So, one fine day in 1927, when most. of the rest of the world
seemed to be celebrating Babe Ruth’s record-setting sixtieth
home run of the season, Carrie Buck entered a hospital in
Lynchburg, Virginia, and had her fallopian tubes severed.

Carrie, now seventy-nine, lives near her sister Doris in
Charlottesville, Virginia.’ Doris had been sterilized under the
same law, only she never knew it. “They told me,” she recalled,
“that the operation was for an appendix.” Later, when she was
married, she and her husband tried to have children. They con-
sulted with a number of specialists at three hospitals throughout
her child-bearing years, but none of them detected the tubal
ligation. It wasn’t until 1980, fifty-two years after the fact, that
Doris finally uncovered the cause of her lifelong sadness. It was
only then that she was given access to Carrie’s medical records,
where a cryptic marginal note revealed that she shared her
sister's fate.8 “I broke down and cried,” she said. “My husband
