34 Granp ILLusions

serious and permanent medicai complications. According to one
physician, writing in the British Journal of Venereal Disease, “infec-
tion in the womb and tubes often does permanent damage. The
Fallopian tube is a fragile organ, a very tiny bore tube. If infec-
tion injures it, it often seals shut. The typical infection involving
these organs is pelvic inflammatory disease, or PID.”!5 This
condition affects nearly fifteen percent of all those who submit to
induced abortion.!5+

Other medical complications of abortion include sterility —as
many as twenty-five percent of all women receiving mid-trimester
abortions; !55 hemorrhaging — nearly ten percent of all cases re-
quire transfusions; '5¢ viral hepatitis— occurring in ten percent of
all those transfused; *” embolism —in as many as four percent of
all cases; ‘58 cervical laceration, cardio-respiratory arrest, acute
kidney failure, and amniotic fluid embolus—occurring in as
many as forty-two percent of ail Prostaglandin abortions.159

As a result of these complications, women in America have
seen a massive increase in the cost of medical care. '®* While the
average cost of normal health maintenance for men has increased
nearly twelve percent over the last eight years due to inflation,
the average cost for women has skyrocketed a full twenty-seven
percent. 16

Planned Parenthood has not removed the specter of danger-
ous back-alley abortions. Not by any stretch of the imagination.
As the world’s number one abortion provider and promoter, it
has instead extended that dark and dismal shadow all across the
land.16? We simply cannot contend or pretend otherwise.

The Innovation Illusion

Planned Parenthood claims that it is on the cutting edge of medical
technology with its birth control and abortifacient research. But that is
an illusion.

The truth is, Planned Parenthood’s research and develop-
ment projects have almost universally proven to be utter failures.
The recent RU-486 debacle is a case in point.

Spawning a spate of news stories and editorials in the inter-
national news media extolling the homeric virtues of the French
abortion drug, Planned Parenthood pundits have heralded
RU-486 as a stunning breakthrough—not only as a conven-
tional abortifacient but possibly as an effective “morning after”
