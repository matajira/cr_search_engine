I6 Granp ILLUSIONS

been a broken arch ~ feel its repose and self-restraint as they feel
nothing else.'* The quiet strength of its curved lines, the solid
support of its tall columns, the moderate proportions of its
gables and transepts, even its absence of display, of effort, and of
self-consciousness, satisfy them as no other art does. They come
back to it to rest, after a long circle of pilgrimage—the cradle of
rest from which their forefathers started.

Here though, they find that rest none too deep.

The apex of the brownstone rose like a sugarloaf forty feet
above the foundations, majestic and unfettered. But the foun-
dations below had been despoiled by the creeping convenience
of contemporaneity.'6

The bottom floor of the building was apparently remodeled
sometime during the irreverent days of the sixties in order to ac-
commodate the clinic. A false facade and broad spans of plate
glass invited clients into a sterile beige foyer. The renovations of
the sixties, like the buildings of the sixties, show bland economy,
and sometimes worse. The world grew cheap, as the world’s
world must.17

Contemporary men may like it all the better for being less
serious, less heroic, and more what the French call
“bourgeois” —just as they may like the style of Louie Louie better
than that of Louis XIV, Madonna better than Montesquieu,
and videos better than Videossis—for taste is as free as will.
Athanasius called such freedom “captivity.” Luther called it
“bondage.” Calvin called it “depravity.” Basil called it “vanity.
Chrysostom called it “debauchery.” And Solzhenitsyn called it
“irresponsibility.” Sin’s shackles severely limit the latitude of both
taste and will.18

A scraggly line of picketers were already doing their paces
back and forth in front of the building. They made for a motley
crew. A sweetly attired grandmother was walking with a fully
festooned college student. A young mother pushing a double
stroller and carrying a gargantuan sign was accompanied by a
teenager who preened a tragically hip haircut and a phosphores-
cently decorated T-shirt. A middle-aged couple, perfectly type-
cast fundamentalists, were engrossed in a conversation with
three nuns. Several young families, who looked as if they had
suddenly been sidetracked from a trip to the zoo or a picnic in
