278 Granp ILLusions

Second, we can begin to exercise the principle of jury nullifi-
cation. The purpose of a jury is to put a check on the power of
the magistrates by putting ultimate power in the hands of indi-
vidual citizens. In a very real sense, the Founding Fathers gave
each citizen three votes: the first was the free elections vote in or-
der to choose their representatives; the second was the Grand
Jury vote in order to prevent overzealous prosecutors from
harassing the citizenry; and the third was the jury vote in order
to restrain the courts from unjustly applying legitimate laws or
from legitimately applying unjust laws. Thus, the true function
of the jury is to try not only the actions and the motives of the de-
fendant, but the actions and the motives of the prosecution, the
court, and the law as well.

According to the 1972 decision of U.S. ». Dougherty, juries
have the “unreviewable and irreversible power . . . to acquit in
disregard of the instruction on the law given by the trial judge.”
In other words, the jury can ignore the prosecutor, ignore the
judge, and even ignore the law if the courts and the law seem to
be out of line with Biblical directives. This is the cherished doc-
trine of jury nullification that men like John Adams, James
Madison, John Jay, Alexander Hamilton, and John C. Calhoun
struggled for for so long.”8

Sadly, it is an almost forgotten doctrine. That is why it is so
essential that we become informed jurors, to hold the courts in
check. Thus, we need to aspire to jury duty, not avoid it. We need.
to apply ourselves diligently so that we can be selected. We need
to be certain never to disqualify ourselves in pro-life cases just
because we hold moral convictions on those matters. Moral con-
viction is just exactly what the courts need right now.

So, why not take the opportunity next time jury duty comes up
and serve willingly? Why not exercise jury nullification and throw
a legal wrench in the judicial works? It can only help matters.

Third, we can begin to go on the offensive. Why should we
always be on the defensive? Why should Planned Parenthood be
the only organization to take cases before the courts as a means
of policy advocacy?

Although a few Christian pro-life groups, like the Ruther-
ford Institute, the American Center for Law and Justice, and the
National Legal Foundation have begun to scratch the surface in
