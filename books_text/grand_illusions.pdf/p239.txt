A Divine Tragedy: The Religious Legacy 219

22:2-3), Military confrontation seemed hopeless. Diplomatic ap-
peasement seemed suicidal. And defensive alliances seemed
delinquent (Numbers 22:4). So in desperation he sent for
Balaam, a diviner, who was thought to have the power to bless
and bind through spells and incantations (Numbers 22:5-6).

At first the magician was reluctant to take part in Balak’s
ploy despite his generous offer (Numbers 22:15-35), But eventu-
ally he gave in and delivered four oracles (Numbers 22:36-24:25).
Much to Balak’s chagrin, however, each of the oracles predicted
that Israel was invincible from without. No army, no king, no
nation, and no empire would be able to stand against it. The
only way God’s Chosen People could be defeated was if they de-
feated themselves through moral defilement.

That was all Balak needed to know. He didn’t need an army.
He didn’t need diplomats. He didn’t need allies. And he didn’t
even need diviners. He would rely on wolves in sheep's clothing
(2 Timothy 3:6).

The next time the curtains of history draw back, the women
of Moab have gone down into Israel’s camp at Peor. Enticing the
people to play the harlot, those women were able to do what no
warrior or general possibly could: trap and defeat Israel. And
not a sword was drawn. Not an arrow was unsheathed. Not a
javelin was hurled.

It would be several hundred years before Moab would be
able to consummate their victory and actually sack the capital of
Israel. But that future conquest in Jerusalem was ensured by the
moral defeat wrought by Balak’s women at Peor.

Early on, Planned Parenthood adopted a similar strategy
against thd church, Margaret Sanger recognized that the
church was “the enemy” of her crusade.°* But she also recog-
nized that:an all-out frontal assault on God’s People was
suicidal.9° And so she put together a “Balak strategy.” She relied
on wolves in sheep's clothing (2 Timothy 3:6).

Margaret began by wooing young and ambitious ministers
with the trinkets and baubles of power, prestige, privilege, and po-
sition. She doted on them, feeding their sense of self-importance.
She enticed them with honors.*’ She invited them to sit on her
boards.®8 She patronized their pet projects.°9 She wined them
and dined them.1° She rewarded them with trips, junkets, and
