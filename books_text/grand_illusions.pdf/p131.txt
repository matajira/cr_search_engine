Selling Sex: The Educational Scandal ii

Another Planned Parenthood publication for teens asserts:
“There are only two kinds of sex: sex with victims and sex without.
Sex with victims is always wrong. Sex without is always right.”?8

“Relax about loving,” admonishes still another Planned
Parenthood booklet, “sex is fun and joyful, and courting is fun
and joyful, and it comes in all types and styles, all of which are
Okay. Do what gives pleasure, and enjoy what gives pleasure,
and ask for what gives pleasure. Don’t rob yourself of joy by
focusing on old-fashioned ideas about what's normal or nice. Just
communicate and enjoy!”?9

That is a far cry from dispelling childish myths about storks
and cabbage patches. But that is what Planned Parenthood’s sex
education programs and materials are like. They are not
designed to simply provide accurate biological information. In-
stead, they are designed to change the minds, morals, and moti-
vations of an entire generation. They are designed to completely
reshape the positions, perspectives, and personalities of children
everywhere— including yours and mine. One former Pianned
Parenthood medical director, Mary Calderone, has forthrightly
admitted that, in sex education, “Mere facts and discussion are
not enough. They need to be undergirded by a set of values.”3°

But whose values? Why, Planned Parenthoad’s, of course: the
values of Margaret Sanger; the values of Revolutionary Social-
ism; the values of Eugenic Racism; and the values of unfettered
sensuality. Thus, according to Calderone, curricula need to,
first, separate kids from their parents; second, establish a new sex-
ual identity for them; third, help ther determine new value sys-
tems; and, finally, help them confirm vocational decisions .*!

In addition to utilizing traditional inductive and deductive
teaching techniques, Planned Parenthood utilizes several
different experimental methodologies in its sex education pro-
grams in order to accomplish these four aims: Values Clarifica-
tion, Peer Facilitation, Sensitivity Training, Role Playing, and
Positive Imaging.

Values Clarification. Based on the notion that everyone should
“do what is right in his own eyes,”3? Values Clarification is a
strategy designed to help children “choose” their own value sys-
tem from a wide variety of alternatives.53 The idea, according to
Values Clarification pioneer Sidney Simon, is to step parents and
teachers from defining for children “their emotional and sexual
