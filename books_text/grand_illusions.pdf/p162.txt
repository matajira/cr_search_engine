142 Granp ILLusions

community school districts, and city council administrations
for its abortion programs.

In eleven states the use of tax dollars for abortion is quite
open and up front.®9 In others it is rather clandestine. In only a
very few is it completely outlawed. But in all, Planned Parent-
hood is actively involved in funneling new tax income sources
into its bank accounts.

Mac Lawton lives in North Carolina where state matching
funds and service provider grants to Planned Parenthood have
been cut off. Despite innumerable lawsuits brought by the five
affiliates in the state, lawmakers have stood firm in denying ac-
cess to state-controlled tax dollars.

But last year when Mac was elected to a county commission-
ers seat he made a startling discovery. His county had been
coerced into a revenue sharing program that benefited Planned
Parenthood to the tune of eighty-five thousand dollars a year.

“J just couldn't believe it when I first saw those figures,” he
told me. “Here we are in the middle of the most conservative sec-
tion of the most conservative state in the whole country. Who'd
have ever guessed that we'd be pouring any money into that
abortion business? Much less that much money? Frankly, I was
shocked. And outraged.”

Mac went to work right away to defund the county’s family
planning program. “The more I got into it the more I realized
that the whole thing was a wicked perversion of the law,” he said.
“Deals had been struck. Favors had been called in. Heads had
been turned. Money had been exchanged under the table. It was
a mess.”

Needless to say, when Mac began to turn over a few stones
and ask a few questions, he began to feel a good deal of political
heat. “Several of my colleagues were up to their eyeballs in graft,
and my investigations were a real threat to them,” he explained.
“Their political survival was on the line. They were afraid that I
was going to blow the whistle on them. Then all their dirty little
deeds would be exposed and the public would discover that their
taxes were being used to support Planned Parenthood and to
skirt the law.”

When he began to receive threatening phone calls and let-
ters, Mac only stepped up his efforts. “If they wanted to play
hardball I was ready,” he said. “Or at least I thought I was.”
