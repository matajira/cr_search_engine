78 Granb ILLUSIONS

that women by the thousands are being exploited by the abor-
tion trade. They see it every day.

Even if unborn children were not murdered>’ by abortion,
men and women of conscience would still have to stand un-
waveringly opposed to it. It is dangerous. Scandalously so.

The Medical Risks of Birth Control

The dangerous propagation of abortion is not the only medi-
cal scandal that Planned Parenthood has been involved in over
the last several years. Its birth control programs have also been
terribly flawed.

All birth control methods are subject to FDA approval, and
must be tested on laboratory animals and human subjects before
they can be marketed to the public. This process may take any-
where between three and ten years to complete.** At first blush,
that appears to be a ponderously slow approval process, but in
fact, it is far too hasty. It may take as long as fifteen to twenty
years for the complications of various birth control methods to
become apparent.5? Planned Parenthood and its birth control
allies in the medical-industrial complex*? have pressured the
FDA to rush products to market long before those complications
can be known.*! The result is that women using birth control are
little more than guinea pigs— unwitting subjects in prolonged
and deathly dangerous experiments.® A number of important
studies have shown that, indeed, Planned Parenthood’s favored
contraceptive programs are all unwarranted medical risks—
from Depo-Provera and Ovral to Norplant and RU-486.%

Dr. Frederick Robbins, a noted figure in population
research, justified Planned Parenthood’s dependence on unsafe
birth control products saying, “The dangers of overpopulation
are so great that we may have to use certain techniques of con-
traception that may entail considerable risk to the individual
woman.”6+ Once again, “choice” is thrown to the wind.

The Pill, For Margaret Sanger, birth control was not simply a
technique, it was a religion. And the Pill was the Holy Grail of that
religion.® Beginning in the late twenties and early thirties, she
helped fund a number of research projects that she hoped would
one day produce a safe and effective chemical contraceptive.** In
1950 she met a brilliant biologist named Gregory Pincus. His
stunning successes in ovulation research with laboratory animals
