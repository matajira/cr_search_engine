220 Granp ILLusions

tours./°! She knew just how to tantalize them with attention and
appreciation.1°2 Her winsomeness was irresistible and her thor-
oughness was incomparable!

But even without all that alluring charm, Margaret’s cam-
paign to seduce Christians would probably have won support in
three broad sectors of the church.

Racists. Especially during the Great Depression when tensions
were high and jobs were scarce, racists saw Margaret's Eugenic
plans and programs as an open opportunity to eliminate whole
“undesired” races and “dysgenic” classes.!°%* Tragically many of
them carried out their vendetta in the name of Christ.195

Lettie Perkins grew up in a small sharecropper’s cabin deep
in the heart of rural South Carolina. When she was just a
youngster she remembers a countywide tent revival meeting
cosponsored by several large all-White Baptist churches and
several social service agencies, including Planned Parenthood.
“We were all excited,” she said. “The revival was always the so-
cial event of the year. And that year was to be the first time
Blacks were allowed to attend. We could hardly contain our-
selves. Most of the women bought or made new dresses. We got
hats and gloves. We really were going to do things right. It was
like a debutante’s coming-out for us.”

The pastor of one of the sponsoring churches spent a good
deal of his time for nearly a month before the revival making
sure that black pastors in the area turned their people out. “I
don’t know why we were so naive at the time,” Lettie told me. “It
was so obvious that he was setting us up for something.”

Indeed, he was. On the day that the revival was to begin,
Planned Parenthood set up several tents. Blacks were herded
into them and “counseled” on the “benefits” of sterilization and
birth limitation. “We weren't even allowed to go into the revival
meeting itself until we'd listened to their whole spiel,” Lettie said.
“And even then, they segregated us off to one side. Like usual.”

Most of the Black families were outraged. The blatantly
racist collusion between Planned Parenthood and the all-White
churches was shocking to them, even in that day of raw and fes-
tering prejudice. “I can still remember my Mamma just shaking
with anger and humiliation,” Lettie said. “Our family never went
to. church again. Not any church, Black or White. Mamma
