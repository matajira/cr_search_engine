59,

61.

73,

74.
75.
76,
77.

Granp ILLusions

. Bid,

. R. J. Gorsini, Role Playing in Psychotherapy (Chicago: Aldime Books, 1966).
. McKasson, p. 100.

. Dbid,, p. 101.

. Tbid., pp. 97, 99.

. Morrison and Price, pp. 167-168.

. Tbid,

 

- Glen E. Richardson, “Educational Imagery: A Missing Link in Decision

Making?” The Journal of School Health, October 1981, p. 561.

. “What is Sexual Fantasy?” What's Happening (Atlanta: Emory University, Grady

Memorial Family Planning Program, 1976), p. 4.

. Jacqueline Kasun, “Turning Children into Sex Experts,” The Public Interest,

Spring 1979, p. 14.
“Control,” Planned Parenthood Association of the Chicago Area, April 8, 1978,
p. 2.

, See Carol Cassell, Three-Year Plan and Long-Range Program Goals (New York:

Planned Parenthood Federation of America, Department of Education, 1979).
Sce Randy C. Alcorn, Christians in the Wake of the Sexual Revolution (Portland:
Multnomah Press, 1985), pp. 61-77, 108-109; Sandalyn McKasson, “Sex and
Seduction in the Classroom,” in Richie Martin, ed., judgment. in the Gate
(Westchester, IL: Crossway Books, 1986), pp. 90-119; and Tim LaHaye, The
Battle for the Public Schools (Old Tappan, NJ: Fleming H. Revell Company,
1983), pp. 99-164,

. Lawrence LaDuc, “Statistical Correlations Between Sexuality Education and

Family Planning Services: Deterrent or Feeder System?” Sexual Ethics in Britain,
2:3, (Fall 1978), pp. 14-17,

. “Serving Human Needs, Preserving Human Rights,” 1983 Annual Report,

Planned Parenthood Federation of America, p. 6; and Cassell, pp. 3-7.

. Ibid.
. Edie
. Ibid.; and “Celebrating Seventy Years of Service,” 1986 Annual Report, Planned

  

Parenthood Federation of America, pp. 24-25.

. Bid.
. bid.
. Ibid.
. Ibid.
. Bid,
. Richard Weatherly, et al., “Comprehensive Programs for Pregnant Teenagers

and Teenage Parents: How Successful Have They Been?” Family Planning Per-
spectioes 18:2 (March/April 1986), pp. 76-77.

Roberta Weiner, Zéen Pregnancy: Impact on the Schools (Alexandria, VA: Capitol
Publications, 1987), p. 17.

Mosbacker, p. 74,

‘Jacqueline R. Kasun, “The Truth About Sex Education,” in Mosbacker, p. 29.
Mosbacker, pp. 75-78.

Deborah Dawson, “The Effects of Sex Education on Adolescent Behavior,”
Family Planning Perspectives 18:4 (July/August 1986), p. 169.
