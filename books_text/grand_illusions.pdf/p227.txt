ELEVEN

A DIVINE TRAGEDY:
THE RELIGIOUS LEGACY

anguis in herba’

 

There is a complex knot of forces underlying any nation once Christian; a smoldering
Of the old fires.

Hilaire Belloc

Easter is the greatest of all the Christian festivals. It is the
day when every believer rejoices in the knowledge of His Re-
deemer’s resurrection. Even so, there was precious little joy in
the hearts of the people of Constantinople on Easter Sunday,
1453. It fell that year on April the first. After a long and stormy
winter, spring was coming at last to the Thracian peninsula. In
the lush orchards throughout the venerable city the fruit trees
were bursting into flower. The nightingales had returned to sing
in the Lycus thickets and the storks were already rebuilding their
nests on the peaked rooftops all along the Mese. The sky was
mottled with long thick lines of migratory birds flying to their
summer havens way away in the north, But the Bosphorus was
rumbling with the sounds of war: the men, armaments, and ac-
coutrements of a great and dreaded army.

Hagia Sophia was thronged with the faithful, as were the
hundreds of other Churches throughout the city. They cul-
minated Holy Week surrounded by a millennium of glory and
majesty. Within eight weeks all of them would be exiled, captive,
or dead. The infidel Turks that began gathering outside the
great Theodosian Walls that day would soon be upon them. The
glory and majesty was doomed. And everyone knew it.

The fall of the greatest city in all of Christendom would send
devastating quakes throughout the West, shaking the foundations

207
