i

52.
53,

55,

56.

5?.
58.
59.

61.
62.
63.
64.
65.
66,
67.

68,
69,
70.
7h.
72.

73.

74.
75.

76.
77.
78,
79.
80,

8t.

Granp ILLusions

 

. William H. Bradenton, The Comstock Era: The Reformation of Reform (New York:

Laddel Press, 1958), p. 126.

, Gray, p. 280.
. Cited in Allan Chase, The Legacy of Malthus: The Social Costs of the New Scientific

Racism (New York: Alfred Knopf, 1977), p. 6.

Paul Johnson, A History of the English People (New York: Harper and Row
Publishers, 1985), p. 276.

Ibid.

I

. Ttid., p. 270.

For a description and definition of each of these Malthusian offshoots, see
Stephen Jay Gould, The Mismeasure of Man (New York: W, W. Norton and
Company, 1981),

For more on cach of these perversions, see Phyllis Grosskurth, Havelock Bilis: A
Biography (New York: Alfred A. Knopf, 1980).

Margaret Sanger, The Pivot of Civilization (New York: Brentano's, 1922), p. 264.
Gray, pp. 61, 71, 163, 224, 227, 487.

Proverbs 7:6-23.

. Gray, p. 199.

Bid., p. 201:

Gordon, pp. 257-272, 341,

Gray, pp. 284-288.

Gordon, pp. 396-397.

Tbid., pp. 264-66, 261, 320-21, 326-29.

Ibid., pp. 329-34,

Margaret actually resisted changing the name at first, She felt that birth control
was her own proprietary label. But she soon accepted the fund-raising reality
that her reputation needed a whitewashing.

Gray, p. 406.

Ibid., pp. 306, 389.

Ibid., pp. 408, 429-30.

Ibid., p. 442.

Quoted in Morgan Scott LaTrobe, The Path of Destruction (Cleveland: The Ohio
Life Alliance Fund, 1974), p. 4.

Faye Wattleton, “Humanist of the Year Acceptance Speech,” The Humanist,
July-August 1986.

‘New York Times, May 4, 1992.

Margaret Sanger, Women and the Naw: Race (New York: Brentano's, 1920, Reprint.
Geo. W. Halter, 1928), p. 67.

PPFA service reports 1982-1986. Alsa see National Right to Life’s special
tabloid, “Til Victory is Won: Planned Parenthood’s Abortion Crusade,” 1982.
Ibid.

Ibid.

Gray, pp. 227-8.

See such PPFA recommended literature as Wardell B. Pomeroy, Bays and Sex
(New York: Dell Publishing, 1968, 1981), pp. 43-57.

See PPFA’s medical director’s books re: Mary S. Calderone and Eric W.
Johnson, The Family Book About Sexuatity (New York: Bantam Books, 1983), pp.
120-123.
