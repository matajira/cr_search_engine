72 THE GREATNESS OF THE GREAT COMMISSION

radical commitment to and promotion of all Scripture as “profi-
table . . . that the man of God might be thoroughly furnished
for every good work.”

Paul, as Christianity’s greatest missionary, provides for us an
important example in the application of this aspect of the Great
Commission, when he writes: “For though we walk in the flesh,
we do not war according to the flesh, for the weapons of our
warfare are not of the flesh, but divinely powerful for the de-
struction of fortresses. We are destroying speculations and every
lofty thing raised up against the knowledge of God, and we are
taking every thought captive to the obedience of Christ” (2 Cor.
10:4-5, emphasis added).

Rather than conforming to the world, Paul urges a radical
transforming of the mind by the ascertaining of the will of God
(Rom. 12:1,2). He promoted an “exposing of the works of dark-
ness” (Eph. 5:11), wherever they were found, in every aspect of
life, because God-less thinking and acting is blindness and vani-
ty.” He challenged the very intellectual underpinnings of non-
Christian culture, urging their being “destroyed” (not by the
sword, but by the spiritual instruments available in God’s Word)
and being replaced with “captive obedience” to Christ. He
firmly believed that in Christ alone was “the truth’® and true
knowledge and wisdom.”

Christ taught that His converts were to follow Him (John
10:27) on a new path (Matt. 7:13-14). He claimed to be “the
way, the truth, and the life” (John 14:6). Thus, early Christians
were initially called a people of “the way,’ because they fol-
lowed a new way of life. They were also known as “disciples”
because they were trained in the truth and application of “the
way." They did not simply receive “testimony” or hear
“preaching.” They responded positively to that testimony and

  

6:1, 2, 7; 9:1, 10, 19, 25, 26, $6, 38; 11:26, 29; 13:52; 14:20, 22, 28;
15:10; 16:1; 18:23, 27; 19:1, 9; 30; 20:1; 20:7, 30; 21:4, 16,
