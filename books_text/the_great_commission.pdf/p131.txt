The Church and the Great Commission 115

a small group book-of-the-month fellowship and discussion pro-
gram,” a local theological seminary program for members and
the community,” and either the setting up of or the support-
ing of an already established Christian day school.

4, Training in hierarchical covenantalism. The church is to be
composed of a system of courts designed to locate responsibility
and resolve problems, as Christ's people have His authority
ministered to them.

The influences of the democratic spirit and of voluntarism
are alive and well in American Christianity. And this is unfortu-
nate, The Church of the Lord Jesus Christ is viewed by many as
so many islands in the stream of history, unconnected and
unconnectable. Bold claims to independency are proudly dis-
played on thousands of church signs across the land.

Yet the Scripture has ordained a covenantal government of
elected hierarchical rule in the church —a rule patterned on the
Old Testament revelation (Exo. 18:19-23; Deut. 1:13-15). In the
Old Testament, elders possessed jurisdictional authority (Exo.
12:21, cp. v. 3) and were organized into graduated levels of
courts (Deut. 1:15). The New Testament office of rulership in
the Church even adopts the same name as the Old Testament
office: elder (1 Tim. 3:1ff).*

We need to teach our churches of the divinely ordained
system of covenantal government in the Church. In the New
Testament Church, each church was to have a plurarlity elders
{Acts 14:23; Tit. 1:5). New Testament elders are vested with real
governmental authority not exercised by the congregation at
large, as the following indicate: (1) Though Christ ultimately

1984).

20. Tt might be profitable to have participants read: Read Mortimer Adler and
Charles Van Dorenen, How To Read a Book (Rev. ed.: New York: Simon and Schuster,
1972 f19s9p.

21. Whitefield Theological Seminary (PO. Box 6821, Lakeland, Florida 88807), has
a program that is designed to operate in local communities.

22, Robert Thoburn, How zo Esiablish and Operate a Successful Christian School (Fairfax,
VA: Thoburn, 1975).

28. Se also: Num. 85:12, 24 (cp. Josh. 20:4); 2 Sam. 5:8 (cp. ¥. 1).

24. "Elder" appears 100 times in the Old Testament and thirty-one times in the New
‘Testament.
