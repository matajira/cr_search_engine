148 THE GREATNESS OF THE GREAT COMMISSION

Examples of Eschatological Pessimism

‘Two best-selling authors in our day, well-known representa-
tives of dispensationalism are Hal Lindsey’ and Dave Hunt.*
These men have recognized the significant difference between
their dispensational understanding of the Great Commission
and its implications and the postmillennial understanding with
its implications. In fact, they have written recent works for the
very purpose of countering the postmillennial understanding of
the Great Commission.® But, as we shall see, these two men are
not the only evangelicals who dispute the historic postmillennial
view.

Dispensationalism

The dispensational view sees the Great Commission in this
age as having only a very restricted influence in bringing men
to salvation. The hundreds of thousands of evangelical Chris-
tians who read dispensational literature have had continually
drummed into their minds the teaching that under no circum-
stance will the gospel be victorious in our age. Let me demon-
strate this by a quick survey of quotations from several dispen-
sational authors.

Hal Lindsey states the situation about as strongly as can be:
“Christ died for us in order to rescue us from this present evil
age. [Titus 2:11-15] show what our focus, motivation, and hope
should be in this present age. We are to live with the constant
expectation of the any-moment appearing of our LORD to this
earth.”

H. A. Ironside notes in his comments on the Great Commis-
sion: “We know that not all nations will accept the message in
this age of grace.”* William MacDonald points out that the

4. Lindsey is best known for his multi-million best-seller, The Late Great Planet Earth
1970).

‘ 5, Hunt is best known for his best-seller, The Seduction of Christianity: Spiritual Discer=
ment in the Last Days (Eugene, OR: Harvest House, 1985).

8. Hal Lindsey, The Road to Holocaust (New York: Bantam, 1989). Dave Hunt,
Whatever Happened to Heaven? (Eugene, OR: Harvest House, 1988).

7. Lindsey, Holocaust, p. 279.

8. Harry A. Ironside, Expository Notas on the Gospel of Matthew (New York: Loizeaux
