12

PESSIMISM AND THE GREAT COMMISSION

Then Caleb quieted the people before Moses, and said, “Let us go up at
once and take possession, for we are well able to overcome it.” But the men
who had gone up with him said, “We are not able to go up against the
people, for they are stronger than we.” And they gave the children of Israel
@ bad report of the land which they had sped out, saying, “The land
through which we have gone as spies is a land that devours its inhabitants,
and all the people whom we saw in it are men of great stature. There we
Saw the giants (the descendants of Anak came from the giants); and we were
like grasshoppers in our own sight, and so we were in their sight.” Then all
the congregation lifted up their voices and cried, and the people wept that
night" (Numbers 13:30-14:1).

For whatever is born of God overcomes the world. And this is the victory
that has overcome the world; our faith (1 John 5:4).

The Issue

The dispensationalist Christian has a different understanding
of the Great Commission from the postmillennialist. In addi-
tion, so do many amillennialists and historic (non-dispensa-
tional) premillennialists. And that difference of understanding
is not merely one of a shading of grey tones, but of a stark
contrast of black and white, as we shall see.

The three eschatological systems mentioned in the preceding
paragraph may be categorized as “pessimistic,” whereas the
postmillennial view may be seen as “optimistic.” In categorizing
them as “pessimistic,” I am speaking of the following issues:
