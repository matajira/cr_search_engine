Pessimism and the Great Commission 149

Great Commission “does not presuppose world conversion.”® In
fact, the opposite is rue, according to J. Dwight Pentecost, for
“during the course of the age there will be a decreasing res-
ponse to the sowing of the seed” of the Gospel.’° Stanley Tous-
saint concurs, when he notes that “evil will run its course and
dominate the [Church] age.”"' Warren Wiersbe agrees: “Some
make this parable [of the Mustard Seed] teach the worldwide
success of the Gospel. But that would contradict what Jesus
taught in the first parable. If anything, the New Testament
teaches a growing decline in the ministry of the Gospel as the
end of the age draws near.”"* In fact, he notes later that “it
would seem that Satan is winning! But the test is at the end of
the age, not during the age.”"*

Charles C. Ryrie denies any postmillennial hope based on
the Great Commission, when he speaks in opposition to the
postmillennial hope: “Their confidence in the power of God
causes them to believe that the Great Commission will be fulfil-
ied in that most of the world will be saved.”"* The postmillen-
nial view of Church history is wrong, he says, because “defec-
tion and apostasy, among other things, will characterize that
entire period.” Consequently, Dave Hunt argues that “only
a small percentage of mankind is willing . . . to come to Christ
in repentance and be born again by the Spirit of God” and that
“the vast majority of people will continue to reject Christ in the
future just as they have in the past.”"° Hal Lindsey scorns the
postmillennialist for believing “that virtually the whole world
population will be converted. I wish this were possible, but God

Bros., 1948), p. 405.

9, William MacDonald, The Gospel of Matthew: Behold Your King (Kansas City: Walter-
ick, 1974), p. 328.

10. J. Dwight Pentecost, Things to Come: A Study in Biblical Eschatology (Grand Rapids:
Zondervan, 1957), p. 146.

11. Stanley D. Toussaint, Behold the King (Portland, OR: Multnomah, 1980), p. 182.

12, Warren W. Wiersbe, Bible Expositor’s Commentary, 2 vols., (Wheaton, IL: Victor,
1989), 1:46.

13. Ibid.

14, Charles C. Ryrie, Basic Theology (Wheaton, IL: Victor, 1986), pp. 441-442.

15, Ibid., p. 461.

16. Dave Hunt, Whatever Happened to Heaven? (Eugene, OR: Harvest House, 1988),
