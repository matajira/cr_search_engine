To my covenant children:

Amanda, Paul, and Stephen

As “the heritage of the Lord,” they are
“like arrows in the hand” (Psalm 127:3, 4).
