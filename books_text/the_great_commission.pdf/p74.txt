58 THE GREATNESS OF THE GREAT COMMISSION

22:

He raised Him from the dead, and seated Him aé His right hand in
the heavenly places, far above all rule and authority and power and
dominion and every name that is named, not only in this age, but also in
the one to come. And He put ail things in subjection under His feet,
and gave Him as head over all things to the church. (Emphasis added)

These passages are supplemented by several other verses,” as
well as the Revelation statement that He is “Lord of lords and
King of kings.””

Regarding the civil-political area of man’s culture, which is
perhaps the stickiest aspect of the question, this explains why
kings are obligated to rule by Him,” under Him,” and as
His “ministers” to promote His Law,” according to the
glorious gospel of the blessed God.”> The earthly political
authority to which Satan arrogantly laid claim, by which he
oppressed the nations, and which he offered to Christ” was
righteously won by Christ's glorious redemptive labor.

His “all authority” over “all the nations” demands we preach
His crown rights over all men and all their institutions, cultures,
societies, and nations. The saving of multitudes of individuals
must eventually lead to cultural Christianization under Christ's
rule and to His glory by His providence, in conformity with
God’s creational purpose. This world order was designed to
have man set over it, to the glory of God.” This is why at the
very beginning of human history unfallen man was a cultural
creature.”®

The salvation wrought by the implementation of the Great

70. For example: Col. 2:10; Rom, 14:9, 12; 1 Cor, 15:27; Heb. 1:4; and 1 Pet. 3:22,

71. Rev. 17:14; 19:16,

72, 2 Chron. 20:6; Prov. 8:15; Luke 18:8.

‘78. Psa. 2:10-12; Psa. 47:2, 7, 8; 72:8-11; 148:11; Dan. 4:1, 25-27, 37; 6:250%; Acts
12:7; Rev. 1:

74, Rom. 18:4, 6. Other religious titles are applied to civil rulers, such as “servant”
and “anointed.” See: Isa, 44:28; 45:1; Jer. 25:9; 27:6; 43:10.

 
 

 

  
 

76. Matt. 4:8-9; 5,6.
97, Gen. 1:26-28; 9:2; Job 35:11; Psa. 8; 115:16; Heb. 2:6-8.
‘78. See earlier discussion in Chapter 1.
