120 THE GREATNESS OF THE GREAT COMMISSION

Erosion of the Covenantal Family

The humanistic assault on the family largely has been suc-
cessful.? Too few Christian parents have implemented basic
biblical principles for family living; fewer still recognize the
principles applicable to the family that may be drawn from the
Great Commission. In the midst of a pervasively humanistic
culture, Christians have tended to live by the 1960s Greyhound
Bus slogan: “Leave the driving to us.” Too many children of
believers become “prodigals” (Luke 15:11-13) by leaving the
Church to seek the temporary comforts and pleasures our secu-
lar society affords. They have no awareness that the foundation
of the technological progress that allows such creaturely com-
forts has been the Christian faith. They do not realize such
luxury has lasted only due to the inertia of our past Christian
heritage?

Yet the Christian should with all seriousness view marriage
as a covenantal institution with covenantal obligations. For
marriage “is the primary training ground for the next genera-
tion. It is the primary institution for welfare, care of the young,
care of the aged, and education. It is the primary agency of
economic inheritance. The family is therefore the primary institution-
al arrangement for fulfilling the terms of the dominion covenant (Gen.
1:26-28).”* In an important sense, as goes the family, so goes

2. Phoebe Courtney, The American Family Under Attack! (Littleton, CO: Independent
American, 1977; James Robison, Attack on the Family (Wheaton, IL: Tyndale, 1980);
Charles Murray, Losing Grotnd: American Social Policy 1950-1980 (New York: Basic Books,
1984). Even the federal government has recognized this, although it is not the least aware
of the real nature af the problems; see: While House Conference on Families: Listening to
America’s Families: Action for the 80's: The Report to the President, Congress ond Families of the
‘Nation (Washington, D.C.: White House Conference on Families, October 1980).

3. For an insightful consideration of the effect of Scripture on economic and techna-
logical progress, see Gary North's The Dominion Covenant: Genesis (1982), Mases and
Pharaoh: Dominion Religion Versus Power Religion (1985), and The Sinai Strategy: Economics
and the Ten Commandments (1986). See also: David Chilton, Productive Christians in an Age
of Guilt Manipulators (Fylet, TR: Institute for Christian Economics, 1981). For the role of
Christianity in Western Culture, see: Francis Schaeffer, How Should We Then Live? The Rise
and Decline of Western Thought and Culitire (Old Tappan, NJ: Revell, 1976) and Hebert W.
Schlossberg, Idols for Destruction: Christian Faith and its Confrontation with American Society
(Nashville: Thomas Nelson, 1983).

4, North, Tools of Dominion, pp. 214-215.
