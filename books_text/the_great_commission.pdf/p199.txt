Bible &, 2, 75-76
Christian, 73n, 105, 118, 147, 159-160
culture &, 51-58
post-Adamic, 10
upheaval, 73, 77, 163
Son, God the
Christ “appointed,” 32, 42
creation &, 12
man as, 127
see also Christ, Jesus
Soul, 8, 66, 77
Sovereignt
Christ's, 22, 27-40, 62, 79
covenant &, 16, 18
redemption &, 56, 59
terms of, 64-78
victory &, 93
see also Providence
Spirit/spiritual
blessings, 59, 88-90, 149
death, 59
inner life, 73
kingdom is, 140
New Creation is, 11
Platonic, 159
realities, 7
vexation of, 92n
warfare, 72, 77
Spirit, Holy
baptism (see Baptism — Spirit)
empowerment by, x, 59, 93
indwelling, 59, 81, 96
inspiration &, 27, 68
salvation &, 81
State
basic institution, 111, 160
Bible &, 75
Christ &, 56
Christian &, 127-132
Church & (see Church — State &)}
dominant today, 128
role of, xii-xili, 112
see also Government
Stipulations (see Covenant — terms of)

183
Succession (see Covenant — succession

of)
Subdue/submission
Christ &, 58, 79, 111, 185
cultural submission denied, 139, 151
God's design, 56, 98
see also Dominion
Sutton, Ray, 18n, 20n, 64
Symbol(ic), 17, 30, 80, 85, 118n

Televangelists, 112
Temple, 21, 141, 142, 143
Ten Commandments
Deuteronomy’s structure &, 19n
fundamental law, 19
Ten Suggestions, 48
two tablets, 17
Tertullian, In
Theology/theological, xiv, 1, 27, 111-
112, 135-137, 143
Transcendence (see Covenant -
transcendence)
Transfiguration (see Christ —
transfiguration)
Transform(ation)
cultural model, 151
Great Commission &, 80, 99
mind, 72
Tesistance to, 154
salvation & cultural, 60-61, 77, 140,
162n
Trinity/Triune God
baptism &, 81-83, 101
Creation &, 12
redemption &, 42
teaching on, 37
Truth(s)
Bible &, 71n
Christ as the, 72
education &, 124
foundational, 8
life &, 111
redemptive, 15, 66-67
