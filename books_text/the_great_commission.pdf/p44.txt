28 THE GREATNESS OF THE GREAT COMMISSION

‘Testament theologian Donald B. Guthrie has noted of the Gos-
pels: “Whereas they are historical in form, their purpose was
something more than historical. It is not, in fact, an accident
that they were called ‘Gospels’ at an early period in Christian
history .. . . [T]here were no parallels to the Gospel form which
served as a pattern for the earliest writers.”* The Gospels were
written by common men, who organized the material according
to a thought-out structure, plan, and purpose (cf. Luke 1:1-4).*
So something of the literary structure of Matthew will also be
helpful in opening to us the sovereignty of the covenantal Great
Commission. Let us consider, then, the place, time, and literary
setting of the Commission.

The Geographical Context

As we turn to the geographical matter, we will note the
covenantal significance of both the region and the topography
of the place where the Commission was given. The region was
in “Galilee”; the topographical setting was on a “mountain.”

Galilee

The Gospels teach us that Christ's disciples were instructed
by Him to go a certain, specified place in Galilee to meet Him
after the resurrection.* And, of course, the Matthew 28:16 ref-
erence is from the very context of the Great Commission.

Itis interesting that Christ instructs His disciples to meet him
in Galilee® Of course, Christ lived there in His youth,* called

2, Donald B. Guthrie, New Testament Introduction (2rd ed.: Downers Grove, IL: Inter-
‘Varsity Press, 1970), pp. 13-14. See also F. F. Bruce, Bulletin of the fokn Rylands Library,
xlv (1968), pp. 319-839; C. FD, Moule, The Birth of the New Testament Grd, ed.: San
Francisco: Harper and Row, 1982), ch. 5; and A. E. J. Rawlinson, The Gospel according to
St. Mark, Westminster Commentary (7th ed.: Landon: Macmillan, 1949), pp. xviii

8. Notice, for instance, the structure of John's Gospel around seven miracles and
Matthew's around five major discourses, which alternate narrative and discourse. See:
Robert H. Gundry, New Testament Introduction (Grand Rapids: Zondervan, 1970), pp. 294,
309m,

4, Mark 16:7; Matt. 26:32; 28:7, 10, 16.

8. ‘There has been an intense scholarly debate on this appearance in Galilee in an
effort to harmonize it with Luke’s record of Christ's appearing in Jerusalem in Judea.
‘The time and effort required to move back and forth between the two regions is part of
