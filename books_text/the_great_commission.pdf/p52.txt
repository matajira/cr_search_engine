36 THE GREATNESS OF THE GREAT COMMISSION

us” has come! B. In the conclusion of Matthew and in the Great
Commission, we have the same theme: “And, lo, I am with you
always” (Matt. 28:20b). “God with us” remains!

2. The Royalty of Jesus. A. In Matthew 1:1 the royal genealogy
of Christ is pushed forward: “The book of the genealogy of
Jesus Christ, the son of David, the son of Abraham.”** Here
not only do we have Christ’s human name (“Jesus”) coupled
with His Messianic name (“Christ”), but with the royal title “Son
of David,” a familiar Messianic ascription in Matthew.” Thus:
“The genealogy presented in Matt. 1:1-17 is not an appendix
but is closely connected with the substance of the entire chapter;
in a broader sense, with the contents of the entire book.”4"

B. In Matthew 28:18 Christ comes to His disciples in the
exercise of His recently secured royal authority: “All authority
is given Me in heaven and on earth.” A fitting conclusion to a
work opening with a royal genealogy.

3. Gentiles and the King. A. In Matthew 2:1ff we read of gentile
magi coming from “the east” in search of Christ. They seek
Him “who has been born King of the Jews” (Matt. 2:2a). B. In
Matthew 28:19 we read of the sovereign King with “all authori-
ty in heaven and earth” sending His disciples in search of the
gentiles: “Go, make disciples of all the nations."**

89, As has been said, “This first sentence is equivalent to a formal declaration of our
Lord’s Messiahship.” J. A. Alexander, The Gospel According to Matthew (Grand Rapids:
Baker, [1860] 1980), p. 2.

40. See: Matthew 9:27; 12:23; 15:22; 20:30; 21:9, 15; 22:42. The very structure of
this genealogy hinges on “David the king” (Mait. 1:6a). Its self-conscious division into
fourteen generations (Matt. 1:17) uses David twice as a pivot: The genealogy is traced
with an upbeat thrust from Abraham to David, who closes the first cycle (Matt. 1:6). Then
from David (Matt. 1:6b) downward (in decline) to the Babylonian Captivity (Matt. 1:11).
Then it moves again upward to Christ (Matt. 1:16-17).

41. William Hendriksen, The Gospel According to Matthew (Grand Rapids: Baker, 1978),
p- 107.

42, Interestingly, the opening verse of Matthew is: “The book of the genealogy of
‘Jesus Christ, the son of David, the son of Abraham.” In Genesis 12:8 similar terminology to
Matt, 28:19 is found in referring to “all the nations.” Reflecting back on the Genesis 12:3,
Paul writes: “And the Scripture, foreseeing that God would justify the Gentiles by faith,
preached the gospel beforehand to Abraham, saying, ‘Al dle nations shall be blessed in
you’ ” Gal. 8:8; the language here is identical to Matt. 28:19). Consequently, in the
closing verses of Matthew we see the means of that blessing for “all the nations”: the
Great Commission.
