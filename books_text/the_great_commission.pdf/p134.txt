118 THE GREATNESS OF THE GREAT COMMISSION

Again, this promotes a biblical model for social concern and
Christian culture building (Matt. 28:19).

Also the Church should pray about and study social and
political issues and encourage social/political involvement
through letter writing campaigns and other means. Of
course, there is a need to be careful not to endorse candidates
and become too “political.”** In America's colonial history, the
Church played an important role as a source of direction and
information regarding social and civil affairs. Unfortunately, the
Church today is too often a study in irrelevance. Yet Christ calls
His Church to be “the light of the world” and “the salt of the
earth” (Matt. 5:13-14). Hence, Paul's appointment to take the
gospel to nations and kings (Acts 9:15).

92. The following books by George Grant are excellent resources: Bringing in the
Sheaves: Transforming Poverty into Productivity (Atlanta, GA: American Vision, 1985); The
Dispossessed: Homelessness in America (Ft. Worth, TX: Dominion, 1986); and Fn the Shadow
of Plenty: Biblical Principles of Welfare and Poverty (Nashville: Thomas Nelson, 1986). Also
see: Gerard Berghoef and Lester De Koster, The Deacons Handbook: A Manual of Sieward-
ship (Grand Rapids, MI: Christian’s Library Press, 1980) and Leonard J. Coppes, Who Will
Lead Us? A Study in the Development of Biblical Offices with Emphasis on the Diaconate (Phillips-
burg, NJ: Pilgrim, 1977),

83. For several resources of socio-political issues of Christian concern, see: Kenneth
L. Gentry, Jn, The Christian Case Against Abortion (2nd ed.: Memphis, TN: Footstool, 1990)
{abortion morality), Journat of the American Family Association, P. ©, Drawer 2440, Tupelo,
MS 38802 (pornography). Candidates Biblical Scoreboard, P.O. Box 10428, Costa Mesa, CA
92627 (political candidates), A Letterwriter’s Guide to Congress, Chamber of Commerce of
the United States, 1615 H St. N. W., Washington, CD 20002 (letter writing campaigns).
Renmant Review, P.O. Box 8204, Ft. Worth, TX, 76124 (economic/social issues). Ruther-
ford Institute Report, P O. Box 5101, Manassas, VA, 22110 (legal issues). Franky
Schaeffer, A Time for Anger: The Myth of Neutrality (Westchester, IL: Crossway, 1982).

84. George Grant, The Changing of the Guard: Biblical Principles for Political Action (Ft.
Worth: Dominion, 1987); Lynn Buzzard and Paula Campbell, Holy Disobedience: When
Christians Must Resist the State (Ann Arbor, MI: Servant Books, 1984), Robert L. Thoburn,
The Christion and Politics (Tyler, TX: Thoburn Press, 1984).
