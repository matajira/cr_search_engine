32 THE GREATNESS OF THE GREAT COMMISSION

in most translations of Romans 1:4 is never translated thus
elsewhere. The word is generally understood to mean: “deter-
mine, appoint, ordain.” As Murray notes: “There is neither
need nor warrant to resort to any other rendering than that
provided by the other New Testament instances, namely, that
Christ was ‘appointed’ or ‘constituted’ Son of God with power
and points therefore to an investiture which had an historical
beginning parallel to the historical beginning mentioned in”
Romans 1:3.

Of course, Christ was not “appointed” the Son of God. But
on this recommended reading, Romans 1:4 does not suggest
that; it says He was “appointed the Son of God with power.” The
very point of Romans | is that Christ came in history as the
“seed of David” (Rom. 1:3), not that He dwelled in eternity as
the Son of God. Thus, at the resurrection, Christ “was instated in
a position of sovereignty and invested with power, an event
which in respect of investiture with power surpassed everything
that could previously be ascribed to him in his incarnate
state.”**

Returning to Matthew 28:18, we should note that a literal
rendering of the verse reads: “And having come near, Jesus
spake to them, saying, ‘Given to me was all authority... .””*”
Both the position and the tense of the word “given” should be
noted. In Greek, words thrown to the front of a sentence are
generally emphasized — as “given” is here in Christ's statement.
Not only is “given” emphasized as being particularly significant,
but according to the Greek verb tense,* His being “given”
authority was at some point in past time.

 

2 vols. (Grand Rapids: Wm. B. Eerdmans, 1959, 1965), 1:9. The Revised Standard
Version and The Amplified Bible transtate the verb: “designated.”

25. Ibid.

26. Ibid., 1:10.

27. Rabert Young, Young's Literal Translation of the Holy Bible (Grand Rapids: Baker,
Tep. n.d. [1898]), New Testament, p. 23.

28. The Greek for “given” is edathe, which is the aorist passive indicative of didomi.
‘The word “aorist” is made up of two Greek words: @ (“no”) and herize (“horizon”), which
means “unlimited.” Normally, therefore, an aorist tense has no temporal connotation. In
the indicative tense, however, it carries the connotation of a past action conceived as in
a point of time,
