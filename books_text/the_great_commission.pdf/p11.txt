Publisher's Foreword xi

Biblical Evangelism

When God says to “evangelize,” He means we should tell the
good news to the world. Not easy news, or inexpensive news,
but good news, The good news is that Jesus Christ has overcome
the world. “Ye are of God, little children, and have overcome
them: because greater is he that is in you, than he that is in the
world” (I John 4:4).

What fundamentalists want is a watered-down gospel mes-
sage suitable for children, and only for children. The problem
is, children grow up. What do you tell a newly converted adult
when he asks the question, “All right, I have accepted Jesus as
my Lord and Savior. Now what do I do?” The modern funda-
mentalist says all he has to do is tell someone else about what
just happened to him. Then that person can tell another, and so
on, until the Rapture ends the whole process.

Modern fundamentalism looks at the gospel as if it were
some kind of gigantic chain letter scheme. Nothing is of value
in God’s sight, they say, except keeping the chain letter alive.
But the gospel is not a chain letter. It is the good news that
Jesus has overcome this world.

It is our job to demonstrate this victory in our lives, meaning
every aspect of our lives, We are to exercise dominion. We
should do this as Church members first, but in all other realms.

The Dominion Covenant

Had there not been a fall in Eden, every person would self-
consciously define himself or herself in terms of dominion. This
was what God told man his task must be: to serve as God's
intermediary over the earth. This assignment is called the cul-
tural mandate by Dutch Calvinists in the tradition of Abraham
Kuyper. I call it the dominion covenant.? The dominion cove-
nant did not cease with Adam’s fall; it was reconfirmed at the
“new creation” after the Flood:

2. Gary North, The Dominion Covenant: Genesis, 2nd ed. (Tyler, Texas: Institute for
Christian Economics, 1987).
