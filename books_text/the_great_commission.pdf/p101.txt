The Commitment to Sovereignty 85

looming judgment, suffering, and death as a “baptism.””” The
writer of Hebrews also speaks of the “various baptisms” in
the Old Testament, “baptisms” performed with the blood of
slain sacrificial animals, which clearly speak of judgmental death
(Heb. 9:10, 13, 19, 21).

Christian baptism is itself tied to judgment. The Pentecostal
call to baptism was given in the shadow of looming fiery judg-
ment: the destruction of Jerusalem (Acts 2:19-21, 40-41). Like-
wise, Peter relates baptism to life and death issues, when he
speaks of it in the context of Noah’s Flood (1 Pet. 3:20-21).
Thus, the escape from judgment that baptism relates is through
the redemptive sufferings of Christ, as Paul makes clear in
Romans 6. There he specifically mentions the death aspect
entailed in baptism, when he says: “Therefore we were buried
with Him through baptism into death."**

In the final analysis it may be said that baptism is “an oath-
sign of allegiance to Christ the Lord. . . . And if the immediate
function of baptism in covenant administration is to serve as the
ritual of an oath of discipleship, we have in that another indica-
tion that baptism is a symbolic portrayal of the judgment of the
covenant. For, as we have seen, covenant oath rituals were
enactments of the sanctions invoked in the oath.”*° As Gary
North puts it: “. . . where there is an oath, there is also implicitly a
curse, Without the presence of a curse, there can be no oath.”*"

Baptism and Culture

Most Christians agree that baptism is the appropriate, bibli-
cal sign to be applied to new converts to the faith. We see a
number of examples in the New Testament of individuals re-

27, Matt. 20:22-28; Mark 10:88-89; Luke 19:50.

28. The Greek of Hebrews 9:10 has baftismois as the word rendered “washings.” Itis
the noun form of the verb baptizo, “to baptize.”

29, Rom. 6:4; ep. Col. 2:11-12.

30. Meredith G. Kline, By Oath Gonsigned: A Reinterpretation of the Courant Signs of
Circumcision and Boptism (Grand Rapids: Wm. B. Eerdmans, 1968), p. 81. For a corrective
of some imbalance in Kline, see Flinn, “Baptism, Redemptive History, and Eschatology,”

. 122-181.
PPh Gary North, The Sinai Strotegy: Economics and the Ton Commandments (Tyter, TX:
Institute for Christian Economics, 1986), p. 56,
