The Covenant and the Great Commission 17

protects specified rights.

Furthermore, each party to the covenant was to have a copy of the
covenant contract. This is why the covenantal Ten Commandments
were on fwo tables of stone.’ Each stone held a complete copy of the
Ten Commandments for each party, God and man.®

A Covenant Establishes a Particular Relation. The purpose of a covenant
is to establish a favorable relationship. The heart of God's “covenants
of the promise” (Eph. 2:12) is: “I will be your God and you will be My
people.” This idea occurs a great number of times in Scripture.” The
divine covenants establish a favorable relationship between God and
His people. By means of the covenant, the covenant people become
intimately related to the God of Creation and Redemption.

A Covenant Protects and Promoies Itself. Favorable covenantal relations
are conditional. They are maintained only by a faithful keeping of the
specified legal terms. Thus, of the covenant set before Israel in Deuter-
onomy 34:15,19, we read: “See, I have set before you today life and
prosperity, and death and adversity. . . . I have set before you life and
death, the blessing and the curse.” Obedience to covenantal demands
brings blessings; disobedience brings cursings.

A Covenant Is Solemnly and Formally Established. Covenants are not
casual, informal, and inconsequential arrangements. They are estab-
lished in a most solemn manner by means of designated symbolic
actions. The manner in which they are established is quite significant.
For instance, in Genesis 15 God sovereignly and graciously established
His covenant with Abram by passing alone between the pieces of the
animals Abram had sacrificed (Gen. 15:8-17). The symbolic covenantal

 

 

5. Exo. 81:18; 82:16; 84:14; Deut. 4:18, See Meredith G. Kline, “The Two Tables of the
Covenant,” chapter 1 in Part Two of The Siructure of Biblical Authority, vev. ed. (Grand Rapids: Win.
B. Eerdmans, 1972), pp. 113-190.

6. Interestingly, in divine covenants the prophets were God's “lawyers.” Their ministry
involved prosecuting God’s “lawsuit” against Israel for “breach of contract.” For example, notice
the legal terminology in Micah 6:1,2: “Hear now what the Lord is saying, Arise, plead your case
before the mountains, and let the hills hear your voice. Listen, you mountains, to the indictment of
the Lord, and you enduring foundations of the earth, because the Lord has a case against His
people; even with Israel He will dispute.” This explains, too, why “witnesses” were called to God's
covenant. In Deuteronomy. 3019 Moses said, “I call heaven and earth to witness against you today”

81:

 

Sam. as Pra. 108: Ta. 43 Jer. 24:7; 31:38; 82:38; Eze. 11:20; 34
Zech, 8:8; 13:9; 2 Cor. 6:18; Rev. 21:3, 7.

 
