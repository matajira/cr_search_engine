x THE GREATNESS OF THE GREAT COMMISSION

Holy Spirit. Evangelism means obedience. This is the message of
John, who wrote the Gospel of John: “If ye love me, keep my
commandments” (John 14:15).

He that hath my commandments, and keepeth them, he it is that
loveth me: and he that loveth me shail be loved of my Father, and
1 will love him, and will manifest myself to him (John 14:21).

This is not the message of modern fundamentalism, however.
Fundamentalism’s message is “no creed but the Bible, no law
but love.” Its message is that Christians have nothing specific to
say to a dying world except tell individuals to get ready to be
pulled out of it, either at the Rapture or at death (preferably
the former, of course, since fundamentalists really do expect to
get out of life alive). They intend to leave nothing behind. They
plan to disinherit the future. Dave Hunt tells us why the Rapture
is so much better than dying:

(1) If we are in a right relationship with Christ, we can genuine-
ly look forward to the Rapture. Yet no one (not even Christ in the
Garden looks forward to death. The joyful prospect of the Rapture
will attract our thoughts while the distasteful prospect of death is
something we may try to forget about, thus making it less effective
in our daily lives.

(2) While the Rapture is similar to death in that both serve to
end one’s earthly life, the Rapture does something else as well: it
signals the climax of history and opens the curtain upon its final
drama. It thus ends, in a way that death does not, all human stake
in continuing earthly developments, such as the lives of the children
left behind, the growth of or the dispersion of the fortune accumu-
lated, the protection of one’s reputation, the success of whatever
earthly causes one has espoused, and so forth.’

This is dispensationalism’s program of “scorched-earth evan-
gelism”: Preach the imminent Rapture. Tell Christians to leave
nothing valuable behind to future generations! But this is not
what God tells us our task is. Our task is far greater.

1. Dave Hunt, “Looking for that Blessed Hope,” Omega Leiter (Feb. 1989), p. 14.
