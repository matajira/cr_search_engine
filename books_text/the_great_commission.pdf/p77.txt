The Exercise of Sovereignty 61

“do business” until He comes (Luke 19:13).

There is another angle from which we may expect the
culture transforming effect of redemption: the negative angle,
the correction of sin. As poetically put in the great Christmas
hymn “Joy to the World”:

No more let sins and sorrows grow,
Nor thorns infest the ground;

He comes to make His blessings low
Far as the curse is found.

The salvation that Christ brings is salvation from sin. His
redemption is designed to flow “far as the curse is found.” The
angel who appeared to Joseph instructed him, “You shall call
His name Jesus, for it is He who will save His people from their
sins” (Matt. 1:21b).

Now, then, how far is the curse of sin found? How wide
ranging is sin? The curse of sin is found everywhere throughout the
world! It permeates and distorts every area of man’s life! For this
reason Christ’s Commission to His Church, as recorded in Luke
24:47 (and implied in Matt. 28:19-20), reads: “Thus it is
written, that the Christ should suffer and rise again from the
death the third day; and that repentance for forgiveness of sins
should be proclaimed in His name to all the nations, beginning
from Jerusalem.” Again we are confronted with salvation — here
via repentance from sin — for “all the nations.”

If man is totally depraved,™ then that depravity extends to
and accounts for the pervasive corruption of all of man’s
cultural activities. Instead of the “Midas Touch,” fallen man has
the “Minus Touch.” The sinner’s touch reduces the quality,

widespread personal repentance before God. We therefore need a Holy Spirit-initiated
Christian revival to extend the kingdom of God across the face of the earth.” North,
Political Polytheism, p. 611 (gee also pp. 133,157, 585-6),

95. Zech. 4:6; Matt. 26:51-52; John 18:36-87; 2 Cor. 10:4-5,

96. "Total depravity” indicates man is sininfected in every aspect of his s being
including his will, emotions, intellect, strength, etc. See: Gen. 6:5; 8:21; Eccl. ’
17:9; Mark 7:21-23; John 3:
is “dead in trespasses and sin,” he is not sick (Eph. 2:1, 5; ep.
2:18),

   
 
