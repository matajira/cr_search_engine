Copyright, Kenneth L. Gentry, Jr., 1990

 

Library of Congress Cataloging-in-Publication Data

Gentry, Kenneth L.

The greatness of the Great Commission : the Christian enterprise
in a fallen world / Kenneth L. Gentry, Jr.

ls cm.

Includes bibliographical references and index.

ISBN 0-930464-48-6 : $25.00 (alk. paper). -- ISBN 0-930464-50-8
(pbk.} : $8.95 (alk. paper)

1. Great Commission (Bible). 2. Evangelistic work -- Philosophy.
3. Dominion theology. 4. Fundamentalism -- Controversial literature.
5. Dispensationalism -- Controversial literature. 6. Millennialism.
7. Sociology, Christian. I. Title.

BV2074.G45 1990 90-15500
269--dc20 CIP
rg90

 

Institute for Christian Economics
P. O. Box 8000
Tyler, TX 75711
