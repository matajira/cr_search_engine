60 THE GREATNESS OF THE GREAT COMMISSION

God within, and secures the intercession of Christ in his
behalf.

All of this must lead to confrontation with and the altering of
non-Christian culture, for Paul commands: “work out your
salvation with fear and wembling, for it is God who is at work
in you, both to will and to work for His good pleasure” (Phil.
2:12-13), Paul is not saying we are to “work for our salvation”
(as if guilty sinners could merit God’s favor!), but that the
salvation we possess must be “worked out” into every area of
our lives. In short, we are to work out the salvation that is now
ours. Consequently, we are driven by divine obligation and
salvific duty to “expose the works of darkness” (Eph. 5:11) by
being “the salt of the earth” and “the light of the world” (Matt.
5:13, 14), Salvation, then, exercises a gradualistic, dynamic and
transforming influence in the life of the individual convert to
Christ. This is progressive sanctification. But this process is not
limited to a hypothetical, exclusively personal realm of ethics.
As salvation spreads to others, it also establishes a motivated,
energetic kingdom of the faithful who are organized to operate
as “a nation producing the fruit of” the kingdom.*

Thus, in 2 Corinthians 10:4-5 we read:

For the weapons of our warfare are not of the flesh, but divinely
powerful for the destruction of fortresses. We are destroying
speculations and every lofty thing raised up against the knowledge
of God, and we are taking every thought captive to the obedience of
Christ.

The One who claims “all authority in heaven and on earth” and
who has been given “a name above every name that is named”
is He who has commissioned us to destroy “every lofty thing
raised up against the knowledge of God” and to take “every
thought captive to the chedience of Christ” — not some thoughts
or inner-personal thoughts only. This is to be done imperceptibly
from within,* not by armed revolution from without,” as we

 
