162 THE GREATNESS OF THE GREAT COMMISSION

new book Sword Over America, that is said to point to the early
1990s as the time for the Battle of Armageddon."

It is sad to say, but these men are following in a tong train
failed prophets.” This “any moment” viewpoint kept Graham
diligently working, even if not carefully preparing for the long
haul. This has kept too many other Christians sitting back, away
from the fray (except for a few notable areas of exception, such
as anti-pornography and pro-life advocacy), while expecting the
end. But a long, developmental, hope-filled view of history is
fundamental to any serious Bible-based approach to the whole
of life."* If there is a tendency to promote a “blocked future,” there will
be no promotion of @ holistic Christian faith.

Conclusion

A proper understanding of the Great Commission will be
essential for the Church to collect itself in preparation for the
1990s and the challenges of the looming new century. Even
though we are hearing many reports of “the staggering growth
of the church in previously non-Christian parts of the world”
and that “in all of Asia and most of Africa, unprecedented num-
bers are coming to Christ,” for which we are thankful, we
must not become lax in our promotion of sound doctrine and
practice. A recent report showed that such popular evangelical
theologians as J. N. D. Anderson, Clark Pinnock, and Charles
Kraft, allow that “if any unevangelized person repents and
desires God’s mercy, he will be saved by the work of Christ even

ie” (p. 8, emphasis his).

12, See: Jim Ashley, “Rubling Believes ‘Crisis’ Events Neax.” Chattanooga News-Free
Press, October 7, 1989, Church News Section.

18. See: Dwight Wilson, Armageddon Now! The Premillmarian Response to Russia ond
Israel Since 1917 (Grand Rapids, MI: Baker, 1977).

14. “Christianity is a force for total transformation, even of the cosmos (Rom. 8:18-
22), Nevertheless, it is not self-consciously revolutionary. It does not teck to overthrow
civil governments by elitist-imposed force. Instead, it steadily overthrows all governments
— personal, familistic, church, and civil — by the cumulative spread of the gospel and the
process of inslitutional replacement. This is the New Testament kingdom principle of leaven
(Matt. 13:33),” North, Tools of Dominion, p. 189. This, obviously, is a program that requires
much time.

15. Terry C. Muck, “Many Mansions?” in Christianity Today 84:8 (May 24, 1990) 14.
