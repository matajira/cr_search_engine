The Terms of Sovereignty 71

men will be judged in terms of the Law’s just demands.*®

Second, His command included the yet future teaching of the
apostles. Before He left this world He left the promise that He
would direct the revelation that would come by means of the
Holy Spirit.*° Christ is the One who grants the Spirit.” His
apostles were given His Spirit to lead them in the production of
Scripture.“

In light of this, it would appear that in urging the teaching
of “all things He commanded” the Great Commission urges us
to promote “the whole counsel of God” (Acts 20:27), And the
whole counsel of God is found in the teaching of Moses and the
Prophets (the Old Testament record), in the teaching of Christ
(the Gospel record), and in the teaching of His apostles (the
remaining New Testament record).*

The Scope of Instruction

The absolutely authoritative Word of God/Christ is the be-
liever's blueprint for living all of life in God’s world, Consequent-
ly, true discipleship and worship, as commanded in Christ's
Great Commission, will involve promoting a holistic Christian
world-and-life view." Christ's Commission, then, involves a

 

38, Matt. 7:23; 13:41; Luke 13:27; Rom. 2:12-15; 3:19.
5-18, 26; 15:26-27; 16:5-15.
Acts 2:32-88; Eph. 4:8,

41, Cf. 1 Cor, 2:18; 1 Thess. 1:5.

42, Itis not found in any alleged charismatic prophetic utterances today. The Church
is built upon an established foundation of final truth brought by the authoritative apostles
and prophets, with Jesus Christ being the chief cornerstone (Eph. 2:20-21), Christ led His
disciples into “all truth” (John 16:13). The completed Scripture, then, is all that is needed
to “thoroughly equip” the believer for “every good work” (2 Tim. 8:16-17). The faith has
been “once for all delivered to the saints” (Jude 8). See: Kenneth L. Gentry, Jr, The
Charismatic Gift of Prophecy: A Reformed Response to Wayne Grudem (Qed.: Memphis, TN:
Footstool, 1989).

43, For an illustrative sampling of the application of Scripture to various academic
disciplines, see: Kenneth L. Gentry, Jr. “The Greatness of the Great Commission” in
Journal of Christian Reconstruction, VIL2 (Winter, 1981), 49-45; Gary North, ed., Founda-
tions of Christian Scholarship: Essays in the Van Til Perspective (Vallecito, CA: Ross House,
1976); and the groundbreaking, 10-volume Biblical Blueprint Series, edited by Gary North
(Ft. Worth, TX: Dominion Press, 1986-87). For essays on various aspects of the Christian
worldview, see also my book, Light for the World: Studies in Reformed Theology (Alberta,
Canada: Still Waters Revival Press, forthcoming).

 
  

 
