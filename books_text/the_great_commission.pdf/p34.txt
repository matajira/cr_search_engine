18 THE GREATNESS OF THE GREAT COMMISSION

action represented to Abram was a graphic “pledge-to-death” by God.
He solemnly promised that He would perform His covenant promise,
or else be “destroyed” (as were the sacrificial animals). Thus, in the
Hebrew language the phrase “to make a covenant” may be translated
literally: “to cul a covenant.”

Formal Structure

Ancient sovereignly established covenants between imperial kings
(“suzerains”) and jesser kings and conquered nations and peoples
(“vassals”) often had a five-fold structure, generally found in the order
below. A brief introduction to this structure will help us understand
God's covenant, which also follows this covenantal pattern.*

1. Transcendence: Usually a preamble offering an introductory statement
idemtifying the sovereignty of the covenant-making king.

2. Hierarchy: An historical prologue summarizing the king’s authority
and the mediation of his rule, by reminding of the historical circum-
stances of it.

3. Ethics: A detail of the legal stipulations defining the ethics of faithful
living under the covenant bond,

4, Oath: The setting forth of the sanctions of the covenant, specifying the
promises and the warnings of the covenant by the taking of a formal
oath.

5, Succession: An explanation of the arrangements transferring the cove-
nant to future generations.’

As I mentioned above, God's covenant follows the same pattern. As
a matter of fact, this covenant structure appears frequently in the
Bible. One prominent example is the entire book of Deuteronomy,
which I will outline by way of illustration.'®

8. More detaited information may be found in Ray Sutton, That You May Prosper: Dominion By
Covenant (Tyler, TX: Institute for Christian Economics, 1987).

9, The Greek word for “God? is dees. “THEOS” can serve as a handy acronym for remember-
ing the identifying features of the covenant: Transcendence, Hierarchy, Ethics, Oath, and Succes
sion.

10. For a more thorough investigation of this outline of Deuteronomy, see: Sutton, That Yow
