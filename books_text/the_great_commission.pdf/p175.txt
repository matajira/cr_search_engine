Conclusion 159

influential evangelist, Dwight L. Moody, Weisberger writes: he
“completed the reduction of evangelism to a matter of technique

and personality."* North’s comments are apropos:

Is it any wonder that the doctrine of eternal damnation is de-empha-
sized in preaching today? Is it any wonder that God is spoken of
mostly as a God of love, and seldom as the God of indescribable
eternal wrath? D. L. Moody, the turn-of-the-century American evan-
gelist, set the pattern by refusing to preach about hell. He made the
preposterous statement that “Terror never brought a man in yet.”
That a major evangelist could make such a theologically unsupported
statement and expect anyone to take him seriously testifies to the
theologically debased state of modern evangelicalism, It has gotten no
better since he said it”

If there is no sound doctrinal base to the Christian life, there can be no
proper starting point for a holistic Christian faith.

The Church and Retreatism

Regarding the matter of retreatism, Francis A. Schaeffer has
written:

The basic problem of the Christians in this country in the last eighty
years or so, in regard to society and in regard to government, is that
they have seen things in bits and pieces instead of totals. . . .

Why have the Christians been so slow to understand . . . ? [It is
traceable to] a deficient, “platonic” spirituality. It fis} platonic in the
sense that Pietism made a sharp division between the “spiritual” and
the “material” world — giving little, or no, importance to the “materi-
al” world... .

Christianity and spirituality were shut up to a small, isolated part
of lifes

4. Bernard Wisberger, They Gathered at the River (Boston: Little, Brown, 1958), p. 177.
For the problems created by Moody's revivalism, see: George Dollar, A History of Funda
meniatiom in America (Greenville, SC: Bob Jones University, 1973), ch. 5, “New Winds
Blowing.”

5. Gary North, Thols of Dominion: The Case Laws of Exodus (Iyler, TX: Institute for
Christian Economics, 1990), p. 167. North got this quote from Stanley N. Gundry, who
was cited by George M. Marsden. He then comments: “Perhaps someone will cite me,
making it three-stage faith in footnotes” (p. 167, n. 126). Consider it done!

6. Francis A. Schaeifer, A Christian Manifesto (Westchester, IL: Crossway Books, 1981),
