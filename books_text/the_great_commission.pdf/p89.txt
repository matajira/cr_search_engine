The Terms of Sovereignty 73

preaching; they were “discipled” in a new faith, a new approach
to all of life.”

Resistance to the Commission

Surprisingly, there have been several evangelicals who have
recently expressed dismay over the growth in the numbers of
Christians who promote God's will among all the affairs of men,
not just the inner spiritual life of individuals and families."
These writers are disturbed that some Christians seek to pro-
mote the Christian faith in the world with a view to its actually
prevailing among and exercising dominion over all the affairs
of men. The impression left by these writers is clearly that such
thinking advocates political revolution, social upheaval, and the
fostering of a Church-State. Such is clearly wrong.

The rallying cry of concerned Christians is not in the least
the call for dominion through political manipulation and mili-
lary conquest. The promotion of ihe crown righis of King Jesus,
as it may be expressed, is through the means of the Great Com-
mission’s evangelistic call to disciple the nations. Clearly the
means of Christ’s dominion in the world is to exercise, through
His people, a spiritual influence, not an influence through
carnal warfare or political upheaval.*

In fact, we are reminded once again that “The term ‘make

50. Even the Jews were learning a “new” way, because they had long forsaken the
written Law of God in deference to the “sayings of the elders,” e.g., Mate. 15:11 See
Gary North, The Judeo-Christion Tradition (Tyler, TX: Institute for Christian Economics,
1989), chaps. 6, 7, for an analysis of the anti-biblical nature of the Jewish Talmud.

51. David Wilkerson, Set The Trumpet to Thy Mowh (Lindale, TX: World Challenge,
1985), Jimmy Swaggert, “The Coming Kingdom,” The Evangelist (September, 1986), pp.
4-12; House and Ice, Dominion Theology, Hal Lindsey, The Road to Holocaust (New York:
Bantam, 1989).

52, Acts 17:7; Rev. 1:5-6.

58. "The basis for building a Christian society is evangelism and missions that lead
to a widespread Christian revival, so that the great mass of earth’s inhabitants will place
themselves under Christ’s protection, and then voluntarily use His covenantal laws for
self-government. Christian reconstruction begins with personal conversion to Christ and
self-goveroment under God's law; then it spreads to others through revival; and only
later does it bring comprehensive changes in civil law, when the vast majority of voters
voluntarily agree to live under biblical blueprints.” Gary North, Political Polytheism: The
Myth of Pluralism (Tyler, TX: Institute for Christian Economics, 1989), pp. 585-586.
