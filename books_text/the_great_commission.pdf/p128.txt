112 THE GREATNESS OF THE GREAT COMMISSION

a proposed additional chapter to its confessional statement.
That chapter was entitled “Of the Christian Mission.” Para-
graph 4 of that revision reads:

As a ministry of worship, the mission of the church is to organize
the communal praise of the saints. As a ministry of redemptive
grace, the church has been given the mission of calling men back
into full fellowship with the Creator. The church proclaims the
Word of God. To those outside the Kingdom, she calls for repen-
tance and faith in Christ Jesus. To those within, she calls for obedi-
ence and growth in grace in every sphere of life. While the church
must not usurp the duties of state and family, she must witness
prophetically to those laboring in those institutions, calling on them
in the Name of God to conform their labors to the requirements of
Scripture. .
Since Christ has promised to His Kingdom a glorious future,
when ail nations will flow to the house of the Lord, the growth of
the church is usually to be expected. This growth, however. is to be
accomplished not through any means which may come to hand, but
only through means which are consonant with Holy Scripture.”

 

The Problem We Face

With all the recent negative publicity regarding the misdeeds
of certain televangelists and the theological distortions by oth-
ers, the Church of Jesus Christ is suffering a credibility and
integrity crisis.* But defection from church attendance did not
begin in the late 1980s with those errant men. It has for a
number of decades been a problem in America.

Church is seen as optional to the Christian life by too many
Christians today. Many who profess to be Christians know too
little of devoted commitment to Christ.’ They seem oblivious to

8. "Of the Christian Mission” in The Failure of the American Baptist Culture, vol. 1 of
Christianity ond Civilization (Tyler, TX: Geneva Divinity School, 1982), pp. 95-98.

4. Mike Horton, ed., The Agony of Deceit: What Some TV Preachers Are Really Teaching
(Chicago: Moody, 1990). James R. Goff, Jr, “The Faith that Claims,” Christianity Today
24:8 February 19, 1990) 18-21.

5. Ryrie is concerned over Lordship doctrine as taught by John MacArthur, myself,
and others, when he asks: “where is there room for carnal Christians?” Charles C. Ryrie,
Balancing the Christian Life (Chicago: Moody, 1969), p. 170. See: John F. MacArthur, Jr,
The Gospel According to Jesus (Grand Rapids: Zondervan, 1988) and Kenneth L. Gentry, Jt,
“The Great Option: A Study of the Lordship Controversy,” Baptist Reformation Review 5:52
