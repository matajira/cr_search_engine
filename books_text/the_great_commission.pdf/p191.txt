birth, 20-21, 35-36

covenant &, 20-23, 67

death/crucifixion of, 21, 37, 85, 93,
95n, 148

deity, $3n, 42 (see also Son — God the)

exaltation, 57

federal headship, 12

genealogy, 36

intercession, 60

investiture, 31-35, 38, 39, 58, 142

Judge, 141

kingship, 29-39, 41-63, 73-74, 102-
107, 186-142

Last Adam, 12

Law & (see Law — Christ &)

Lord, 82, 83

parables of, 49, 103n, 123, 149

presence, 96-102

priest, 34-35, 34n, 38, 80

prophet, 34-35, 34n, 38, 64

redemptive labor, 7, 10, 56-58, 81,
127, 140-142

resurrection, 31-34, 42-44, 56, 58, 61,
80

resurrection appearance(s), 29, 106

return of, 97, 136-140, 148, 147, 150-
152, 161

Savior, 82

second humiliation, 143

sovereignty, 27-40, 94

temptation, 38, 38n

transfiguration, 21, 33

youth, 28, 87

see also Son — God the

Christian/Christianity

calling (see Practice — of Christian

Faith; Mission)

dominion &, xiii, 39, 62, 77

growing, 65-66, 73

life, 69, 77, 129, 135

minority, 106, 147

persecution of, 93, 186

raised with Christ, 89

State &, 54n

175

“Way,” 72-73
weakness, 92, 95, 157-164
see also Faith — Christianity
Church
Age, 135-144, 149, 152
attendance, 12, 113
credibility crisis, 112, 158-159
decline of, 120, 136-139, 142, 154
discipline, 115-116
discussion of, 111-118
early church, 46, 83, 97
government, 115, 116, 129
Great Commission &, xii
growth, 48, 73, 113, 137-140, 158, 162
Israel &, 2, 52n-58n, 136, 138, 141-
142
megachurch, 66, 92n
membership, x, 113
ministry, 1-2, 45, 65-67, 77, 113, 119,
127
mission (see Mission)
New Covenant &, 48n
officer(s), 113, 115, 116 (see also
Elder)
responsibility, xiii-xiv, 67, 79
State &, 54n, 73, 117-118, 128, 130-
181, 160 (see also Christianity —
State &)
superficiality &, 157-160
Circumcision, 81n, 85n, 87-88, 101n
Civilization, xiii, 76 (see also Culture}
Clement of Rome, 52n
Convert(s), x, 60, 72, 82, 88, 92n, 114
Conversion(s)
church office &, 116
effects of, 74, 76
implications of, 83
Great Commission &, 63, 86
growth of, 4, 91, 100 (see also
Church - growth of)
world, 97, 99, 105, 108
Covenant(s)/covenantal
Abrahamic, 16, 17, 87
authority/sovereignty, 21, 39, 79
