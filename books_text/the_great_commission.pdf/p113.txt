The Continuance of Sovereignty 97

The Permanence of the Hope

And this powerful, enabling presence is not limited to the
apostolic era of the Church. For the fourth time in these three
verses He speaks of “all.” In verse 18 He claimed “all authori-
ty.” In verse 19 He commanded the discipling of “all the na-
tions.” In verse 20a, He commanded the observing of “all
whatever He commanded.” In verse 20b, He promised “I am
with you ‘all the days'* — til the full end of the age.”*

Here Christ's promise is a covenantal promise establishing
succession arrangements. His perpetual presence will be with
His people for however long they are upon the earth. The
grammar here suggests He will be with them each and every day,
through “all the days” that come their way. And due to the
magnitude of the work before them — He commanded them to
“disciple ali the nations” — His Second Advent, which will close
the gospel age, necessarily lays off in the distant future. As
Bruce perceptively observes: “all the days, of which, it is im-
plied, there may be many; the vista of the future is lengthen-
ing.” Nevertheless, the resurrected Christ promises, “I my-
self” with “all authority in heaven and earth” am with you until
that distant end.

The Commission’s Goal

I have shown that by establishing the succession of the cove-
nant, the powerful Christ promises to be with His people al-
ways. But this only renders the glorious prospect of world con-
version and the glorious future resultant from that a theoretical
possibility, With Christ's presence the magnitude of the job cer-

28, The Greck here translated literally is: pasas tas Remeras, “all the days.”

24. Young, Literal Translation, New Testament p. 23.

28. A.B. Bruce, Englishman's Greek Testament (Grand Rapids: Win. B, Eerdmans, rep.
1980 [n.d.}), 1:340, See the expectation of a long period of time between the First and
Second Advents of Christ in Matt. 21:33; 24:48 25:14, 19; Mark 12:1; Luke 12:45; 19:12;
20:9; 2 Pet. 3:4-9; Luke 12:45. Fora helpful article on the anticipated delay in the Second
‘Advent of Christ, we refer the reader once again to: Herbert W, Bowsher, “Will Christ
Return ‘At Any Moment’?”, The Journal of Christian Reconstruction 7:2 (Winter, 1981), 48-
60. See also: Greg L. Bahnsen and Kenneth L. Gentry, Jr, House Divided: The Break-up of
Dispensational Theology (Tyler, TX: Institute for Christian Economics, 1989), pp. 217-222.
