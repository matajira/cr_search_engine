104 THE GREATNESS OF THE GREAT COMMISSION

to Myself” (John 12:31). Christ is called “the Savior of the
world,” because of the comprehensive design and massive influ-
ence of His redemptive labors. The Great Commission is the
means by which God will draw all men to Christ.

The Hope Denied

Despite the clear statement in the Great Commission, there
are evangelical Christians of influence who somehow miss what
seems so obvious. At an academic discussion held over this
whole question, some evangelicals maintained “the futility of
trying to change the world in the current age.’ Dispensa-
tional theologian Harold Hoehner replied against the postmil-
lennial hope: “I just can’t buy their basic presupposition that we
can do anything significant to change the world.”*

Another evangelical, Albert Dager, has stated: “To ‘disciple
all the nations,’ or, ‘make disciples [out of] all the nations,’ does
not mean that every nation as a whole is one day going to...
learn the ways of Truth. The Great Commission requires us to
go into all the nations and disciple ‘whosoever will’ be
saved.”* It is interesting to note that in order to discount the
glorious expectation of the Commission, Dager has to import
words into the text. The following italicized words show his
textual additions: “make disciples [ow of] all the nations,” “go
into all the nations” and “disciple ‘whosoever will.’” Christ simply
says: “make disciples of all the nations,” without all the embel-
lishments. The basic issue is this: discipling (disciplining) na-
tions means extending God’s kingdom authority in history.

One recent evangelical book attempts a strong case against
the obvious meaning of the Great Commission, a case which
forms, in essence, the whole point of that book. Popular writer
Hal Lindsey vigorously assaults the very interpretation of the
Great Commission, which I am suggesting. He cites the Great

48, See also: 1 Tim. 2:6.

49, Cited by Randy Frame, “Is Christ or Satan Ruler of This World?” Christianity
Thday, 34:4 (March 5, 1990) 42,

50. Ibid, p. 43.

51. Albert James Dager, “Kingdom Theology: Part II,” Media Spoltight (January-
June, 1987), p. LL.
