The Creation Mandate and the Great Commission 13

Both Mandates Engage the Same Task

Both the Creation and New Creation Mandates are designed
for the subduing of the earth to the glory of God. The Creation
Mandate was to begin at Eden (Gen. 2:15) and gradually to
extend throughout all the earth (Gen. 1:26-28). It was restated
after the Great Flood (Gen. 9:1-7).

The New Creation Mandate, which supplements, undergirds,
and restores man ethically to the righteous task of the Creation
Mandate, was to begin at Jerusalem (Luke 24:47) and gradually
to extend throughout the world (Matt. 28:19). As we will show
in the following chapters, the Great Commission sets forth the
divine obligation of the true, created nature of man. It seeks the
salvation of the world, the bringing of the created order to
submission to God's rule. This is to be performed under the
active, sanctified agency of redeemed man, who has been re-
newed in the image of God.*

Both Mandates Were Originally Given to Small Groups

The Creation Mandate originally was given to Adam and Eve
(Gen. 1:27), and then renewed to Noah and his sons (Gen. 9:1).
The New Creation Mandate was given to Christ's disciples
(Matt. 28:16) for all ages (Matt. 28:20).

It is clear from the New Testament that the few original
disciples, though initially intimidated by the resistance to Christ
from their native countrymen, eventually overcame their cow-
ardly hesitance. Upon witnessing the resurrection of Christ,
they became convinced of the power of God. They received the
command to “disciple all nations” on the basis of “all authority
in heaven and on earth.” They accepted the obligation to
preach the gospel to “every creature” (Mark 16:15).

Both Mandates Require the Same Enablement

As I have shown above, the Creation Mandate establishes a
close connection between the interpretive revelation regarding
man’s being created in God's image (Gen. 1:26a, 27) and His

24, Col. 3:10; Eph. 4:24,
