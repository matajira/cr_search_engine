76 THE GREATNESS OF THE GREAT COMMISSION

social and economic injustices, or marches for civil rights, higher
wages, or better education.”

That book continues elsewhere:

‘We are sent not to preach sociology but salvation; not economics
but evangelism; not reform but redemption; not culture but conver-
sion; not a new social order but a new birth; not revolution but
regeneration; not renovation but revival; not resuscitation but
Tesurrection; not a new organization but a new creation; not democ-
racy but the gospel; not civilization but Christ; we are ambassadors
not diplomats.®

But should we not preach “biblical sociology” so that the
recipients of salvation might know how they ought to behave as
social creatures? Should we not preach “biblical economics” to
those who are evangelized, so that men might know how to be
good stewards of the resources God has entrusted into their
care, resources they use every day of their lives? Should we not
promote a “biblical culture” to those who are converted so that
they might labor toward a transforming of a godless culture
into a God-honoring one? On and on we could go in response.

There are even Christian colleges advertising along these
lines. The following advertisement was see in Faith For the Fami-
Jy, advertising a Christian university: “Christianize the world?
FORGET IT! .... Try to bring Christian values, morals, pre-
cepts, and standards upon a lost world and you’re wasting your
time . . . . Evangelize — preach the Gospel; snatch men as
brands from the burning .. . . All your preaching won’t change
the world, but the Gospel ‘is the power of God unto salvation to
everyone that believeth.” (We might ask: What academic
course work could be assigned that would be consistent with
such a view of Christian thought? What textbooks does such a
university assign? The answer is obvious: textbooks written

61. George W, Peters, A Biblical Theology of Missions (Chicago: Moody Press, 1972), p.
ait.

62. Mid., p. 208.

68. Cited in Herbert W. Bowsher, “Will Christ Return ‘At Any Moment'?” in The
Journal of Christian Reconstruction 7:2 (Winter, 1981) 48.
