96 THE GREATNESS OF THE GREAT COMMISSION

are used with the verb, as here, much emphasis is being cast on
the statement. As Robertson puts it: “When the nominative
pronoun is expressed, there is a certain amount of emphasis for
the subject is in the verb already.”"®

In the Greek, Christ could have said merely: “I am with you”
(Gk: meth humon eimi). But He is much more emphatic; He is
determined to drive the point home to these frail disciples.
Here He says literally: “I with you I am” (Gk: ego meth humon
eimi). As Lenski notes: “Ego is decidedly emphatic,” meaning
essentially: “ ‘I myself.’ That is, paraphrasing this into English
phraseology, He says: “I myself am with you.” The drift is
obvious: His scattered, fearful disciples should “let their eyes
and their hearts remain fixed on him.”!” He who claims “all
authority in heaven and earth” and who has arisen from the
dead will be with them.*

Believers are adequately empowered for the task of world
evangelism and the Christian culture transforming labor that
follows in evangelism’s trail.'* The Christian has the abiding
presence of the resurrected Lord of glory” through the spiri-
tual operation of the indwelling Holy Spirit,” Whom Christ
says grants “power from on high” (Luke 24:49). The Christian
should not read the newspapers and fear the encroachments of
the various branches of secular humanism in history, for secular
humanism in all of its manifestations is but an idol for destruc-
tion.”

16. A. T. Robertson and W. Hersey Davis, A New Short Grammar of the Gretk Testoment
(10th ed.: Grand Rapids: Baker, 1958), p. 264.

17. Lenski, Mathew, p. 1180.

18. Interestingly, Matthew's Gospel opens with Christ's prophetic name “Immanuel,”
which means “God with us” (Matt. 1:23). He also promises to be with His people in Matt.
18:20.

19. Christ will strengthen His people to de His work, John 16:83; Acts 26:16-18; Phil.
4:13; Rev, 1:9-20.

20, Matt. 1: ; John 15:18; Acts 18:10; Gal. 2:20; Heb. 13:5,

21. John 7:39; 14:16-18; Rom. 8:9; 1 John 4:4.

22. Herbert Schlossberg, Idols for Destruction: Christian Faith and Its Confrontation with
American Society (Nashville: Thomas Nelson, 1983).

  
