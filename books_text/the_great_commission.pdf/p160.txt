144 ‘THE GREATNESS OF THE GREAT COMMISSION

....A time is coming when this great commission here will be
carried out by a remnant of Jewish disciples ... .”"' In keep-
ing with the Postponed Kingdom Theory, Gaebelein also puts
the Commission’s institution off into the future.

A more recent dispensationalist, Charles F Baker, explains
that when the Great Commission was given “there had been no
revelation as yet that the program of the prophesied Kingdom
was to be interrupted by this present dispensation of the mys-
tery." George Williams agrees."* (Notice should be made of
the fact that the Church Age “interrupted” God's Kingdom Pro-
gram.)

Dispensationalist Stanley Toussaint, in his recent commen-
tary on Matthew, mentions the debate among current dispensat-
ionalists,* while another dispensationalist David L. Turner
comments regarding modern dispensationalists that “most would
agree that the stirring mandate for discipleship with which
Matthew concludes is incumbent upon the Church today.””

Conclusion

The study of eschatology is an important matter for the
Christian. What we believe the future holds for us and our
children has a great impact on the prioritizing of our life’s
concerns. Eschatology should not be approached as if but an
interesting aside to the study of Scripture. It is a Fundamental
aspect of it, having a great bearing even on the understanding
of evangelism itself.

In closing, I would like to point out to the reader that I write
a monthly newsletter that analyzes the numerous distortions
inherent in dispensationalism. Despite its widespread populari-

11, Arno GC. Gaebelein, The Annotated Bible, vol. 6: Maithew ta the Acis (Traveler's Rest,
SC: Southern Bible Book House, n.d.), p. 61.

12, Charles F. Baker, A Dispensational Theology (Grand Rapids: Grace Bible College,
1971), p. 558.

18. George Williams, The Student's Commentary on the Holy Scriptures (4th ed.: Grand
Rapids: Kregal, 1949), pp. 730-731.

14, Stanley D. Toussaint, Behold the King? (Portland, OR: Multnomah, 1980), p. 318.

15. David L, Turner, “The Structure and Sequence of Matthew 24:1-41: Interaction
with Evangelical Treatments,” Grace Theological Journal 10:1 (Spring, 1989) 6 (emphasis
mine).
