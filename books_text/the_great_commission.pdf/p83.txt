The Terms of Sovereignty 67

necessarily there, of course. But the idea of discipling involves
the proclamation of truth with a view to its effecting the appropriate
response in the disciple. In fact, “To disciple a person to Christ is
to bring him into the relation of pupil to teacher [sovereignty],
‘taking his yoke’ of authoritative instruction (11:29) [hierarchy],
accepting what he says as true because he says it and submitting
to his requirements as right [ethics] because he makes them.”

Discipling involves turning people from sinful rebellion
against God to a faithful commitment to Christ’ and training
them in the exercise of that faith commitment in all of life,"
Not just a non-descript “growing.”

William Hendriksen insightfully observes: “The term ‘make
disciples’ places somewhat more stress on the fact that the mind,
as well as the heart and will, must be won for God.”" In other
words, it is designed to win the obedience in all of life of the
disciple. It is to promote ethical covenant living. But how shall
the Church win the heart to God? How may the will of man be
turned to follow after His will? And since the ministry of the
Church is to promote the worship of God in all of life, where
shall God's will for all life be found? In mystic contemplation?
Charismatic prophecy? Human logic? Warm feelings? Pragmatic
considerations?

For the orthodox Christian, the answer should be obvious:
We determine the will of God through Spirit-blessed study of
the written Word of God, the Bible. As the Lord says: “Go
therefore and make disciples of all the nations, . . . teaching them
to observe all that I commanded you” (Matt. 28:18-19).

(Grand Rapids: Regency Reference Library, 1984), 8:597.

©. John A Broadus, Commentary on the Gospel of Meithew in Alvah Hovey, ed., An
Amarican Commentary (Valley Forge: Judson Press, 1886 (rep.], p. 598. Bracketed words
are mine, KLG.

10. Acts 20:21; 26:18,

11. Acts 20:27; Col. 3:17; 2 Cor 10:31; Heb. S:L1-14,

12, William Hendriksen, Matthew (New Testament Commentary) (Grand Rapids: Baker,
1973), p. 999.

13, Matt. 4:4; John 17:17; Aces 17:11; 2 Tim. 8:15-17; 1 Pet. 2:2; Heb. 4:12. See:
John Murray, “The Guidance of the Holy Spirit” in Colleced Writings of John Murray, vol.
I: The Claims of Truth (Edinburgh: Banner of Truth, 1976), pp. 186-189. Garry Friesen,
Decision Making & the Will of God: A Biblical Alernative io the Traditional View (Portland, OR:
Multnomah, 1980).
