I. Covenantal Transcendence

3
THE DECLARATION OF SOVEREIGNTY

The eleven disciples proceeded to Galilee, to the mountain which Jesus had
designated. And when they saw Him, they worshiped Him (Matthew 28:16-
17a).

The first point of the covenant model is the establishment of
the sovereignty of the covenant maker. As we approach the Great
Commission from a covenantal perspective, we discover that its
contextual setting clearly points to its sovereign disposition in a
number of ways.

As we begin our study of the matter, we must recognize that
the books of Scripture were written by real, flesh-and-blood,
historical men under the inspiration of the Holy Spirit. Thus
the books were given in particular, concrete historical contexts
(2 Pet. 1:21). The Scriptures did not fall from heaven as a
book of mysteries. Consequently, at least a general understand-
ing of the historical and geographical contexts of any given
passage is helpful to its fuller and more accurate apprehension.

In addition to being aware of the historical and geographical
contexts of any given passage, it is often helpful to understand
something of the literary structure of the particular book of
Scripture in which it is found. This is especially true of the
Gospels, which represent a new literary genre that is neither
biography nor theology. This literary genre is “gospel.” As New

1, See the emphasis on the historical, for example, in Isa. 7:3; Zech. 1:1; Luke 2:1,2;
3:1-8.
