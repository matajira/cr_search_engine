50 THE GREATNESS OF THE GREAT COMMISSION

the world is to be omitted.” Consequently, it merely means
that “the purpose of the church in this present age [is] that of
a witness." The Great Commission is said to involve the
salvation of “individuals” from among the nations, because
“making a disciple in the Biblical sense is an individual
thing.”®

Who among us would disagree with these statements — as far
as they go? Surely Christ’s words, “Go therefore and make
disciples of all the nations” cannot mean ess than that the
gospel is a universal gospel to be proclaimed to people in all
nations. And just as surely is it held by all evangelicals that the
Great Commission demands the salvation of individual sinners
from their own sins.

But is this ai/ that it entails? Does the Great Commission
merely seek the proclamation of the gospel to in all nations? That
is, does it only seek the salvation of scattered individuals from
throughout the world? Or is there more here than we may have
supposed?

Christ’s command is to make disciples of all the “nations.”
The Greek word translated “nations” here is edhne (the plural of
ethnos), which is an interesting word that serves a vital function
in the Great Commission. Let us consider the word meaning
itself, then note how it is more appropriate for the Great
Commission than any other similar words might have been.

The Meaning of Ethnos

1, Its Etymological Derivation. The word ethnos was common in
the Greek language from ancient times. It is widely agreed
among etymologists that it was derived from another Greek
word, ethos, which means “‘mass’ or ‘host’ or ‘multitude’ bound
by the same manners, customs and other distinctive features”*

40. Howard Vos, Mark, p. 141.

41. House and Ice, Dominion Theology, p. 165. See also: Anthony A. Hockema, The
Bible and the Future (Grand Rapids: Wm. B. Eerdmans, 1979), p. 138.

42. Lindsey, Road to Holocaust, p. 277. In another context, J. D. Pentecost is aware of
the precariousness of the dispensationalist individvalization of the term “sthnas” and
attempts an argument supportive of it (Pentecost, Things to Come, p. 421).

43. Karl Ludwig Schmidt, “ethnos” in Gerhard Kittle, ed., Theological Dictionary of the
