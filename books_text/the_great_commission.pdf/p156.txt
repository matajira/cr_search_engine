140 THE GREATNESS OF THE GREAT COMMISSION

and the saints. The resurrection and the judgment of the wick-
ed will occur and the eternal order will begin.

Representative Adherents: In the Ancient church: Papias (A.D.
60-130) and Justin Martyr (A.D. 100-165). In the modern
church: Henry Alford, E, B. Elliott, A. R. Faussett, Henry W.
Frost, H. G. Guinness, Robert H. Gundry, S. H. Kellog, George
Eldon Ladd, Alexander Reese, and Nathaniel West.
Postmillennialism

Definition: Postmillennialism is that system of eschatology
which understands the Messianic kingdom to have been found-
ed upon the earth during the earthly ministry and through the
redemptive labors of the Lord Jesus Christ in fulfillment of Old
Testament prophetic expectation. The nature of that kingdom
is essentially redemptive and spiritual and will exercise a trans-
formational socio-cultural influence in history, as more and
more people are converted to Christ. Postmillennialism confi-
dently anticipates a time in earth history in which the gospel
will have won the victory throughout the earth in fulfillment of
the Great Commission. After an extended period of gospel
prosperity, earth history will be drawn to a close by the person-
al, visible, bodily return of Jesus Christ {accompanied by a
literal resurrection and a general judgment).

Descriptive Features: 1. The Church is the kingdom prophesied
in the Old Testament era and is the millennial age. It is com-
posed of Jew and Gentile merged into one body in Christ, as
the New Israel of God.

2. The kingdom was established in its mustard seed form by
Christ during His earthly ministry at His First Coming. It will
develop gradualistically through time.>

3. Satan was bound by Christ in His earthly ministry and is
progressively hindered as the gospel spreads.

8 It does not develop uniformly, but gradualistically in spurts. In a sense, it is lke
seed, which is planted and grows and produces other seed (see: Matt. 13:3-9, 23). Thus,
we can expect it to grow in certain areas and perhaps even to die, but eventually to come
back, because the productivity of seed involves its death and renewal (see: John 12:24; 1
Cot. 18:36). In addition, we may expect God's pruning from time to dime (John 15:5-6).
