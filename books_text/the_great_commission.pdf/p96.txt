80 THE GREATNESS OF THE GREAT COMMISSION

subordinate.’

‘The Great Commission is noé just a tool of cultural transforma-
tion, nor is it initially such. The cultural effects of the Great
Commission flow from the redemptive power that is inherent in
Christ's kingdom.

In the oath-worship aspect of the Commission, we have
Christ exhibited in His priestly office. He is our Great High
Priest, Who secures our redemption, which is symbolized and
sealed to us in baptism.

Baptism and Worship

In the very context of the giving of the Great Commission
we see the response of the delighted disciples to the presence of
the risen Christ: “And when they saw Him, they worshiped
Him” (Matt. 28:17a). Upon this notice, the Great Commission
is given (Matt. 28:18-20). The going and the discipling of the
Great Commission are a going for and a discipling under the
authority of One who is infinitely worthy of our worship.
These “disciples” saw Christ and “worshiped Him.” As these
disciples were immediately instructed to “disciple all nations,”
they obviously were to instruct all nations in the worship of
Christ.

Without a doubt the starting point of Christ’s gracious influe-
nce among men is the personal salvation wrought by the sover-
eign grace of almighty God.’ Evangelicals agree on this point,
and I certainly confirm this truth in this book. Because of the
inherent depravity of man,* man cannot know the things of
God.’ Nor can he save himself or even prepare himself for
salvation.’ In addition, neither can he function properly in

1. Geerhardus Vos, The Teaching of Jesus Concerning the Kingdom of God and the Church
(Nutley, NJ: Presbyterian and Reformed, rep. 1972), p. 103. (This is the title given on the
tide page. The tide shown on the cover is only: The Kingdom of God aud the Church.)

2. Eph. 1:20-21; Phil. 2:9-11; Heb. 1:6; Rev. 5:9-14; 5:3-4.

3. His influence is as a king over a spiritual kingdom, Matt. 4:23; John 3:3; Acts 8:12;
Col. 1:13.

4, Psa. 51:5; Jer. 17:9; Rom. 3:10; Eph. 2:3.

8, John 8:19; 1 Cor. 2:14; Eph. 4:17-19.

6, Job 34:4; Jer, 13:23; Joha 6:44, 65; Rom. 3:10; 8:8,

 
