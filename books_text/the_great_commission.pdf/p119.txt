The Continuance of Sovereignty 103

“Truly I say to you, there are some of those who are standing
here who shall not taste of death until they see the kingdom of
God after it has come with power” (Mark 9:1).

4, Christ is even now at the throne of God ruling and reign-
ing over His kingdom: “He who overcomes, I will grant to him
to sit down with Me on My throne, as I also overcame and sat
down with My Father on His throne” (Rev. 3:22).

5. His rule will grow to encompass the entire world until He
has put down all opposition: “He, having offered one sacrifice
for sins for all time, sat down at the right hand of God, waiting
from that time onward until His enemies be made a footstool
for His feet” (Heb. 10:12-13).%

The design and results of Christ's redemption. It is evident from
the New Testament record that Christ's design in salvation was
to secure the redemption of the world, as I showed earlier.*

1. He died in order to redeem the “world.” The Greek word
for “world” (Rosmos) signifies the world as the system of men and
things. God created this world of men and things; Christ has
come to redeem it back to God. “God did not send the Son into
the world to judge the world; but that the world should be
saved through Him” (John 3:17).7

2. He died with the expectation of drawing “all men” to
Himself: “I, if I be lifted up from the earth, will draw all men

 

43. See also: Matt. 16:18, 19; 26:64.

44, Acts 2:29-36; Rom. 8:34; Eph. 1:20-23; Phil. 2:8-11, Heb. 1:3, 13; 1 Pet. 3:22;
Rev. 1:5-6,

45, See also: Matt, 13:31-33; 1 Cor. 15:20-26; Heb, 1:13; 10:12-13. Hal Lindsey
disputes the use of Matt. 13:33 in this connection: Some “try to make the symbol of
deaven in this parable refer to the kingdom of God and how it will spread to take domin-
ion over the earth, However, there's one big problem with that interpretation ~ leaven in.
the Bible is always used as a symbol of evil’s explosive power to spread. It is never used as a
symbol of good.” Lindsey, Holocaust, p. 47, However, there are three “big problema” with
Lindsey's interpretation: (1) Leaven does not explode; it works slowly and subtly. (2) It is,
used in some offerings to Ged in the Old Testament and surely does not represent an evil
gift to God (Lev. 7:13 and 23:7). (8) It is absurd to say that Christ preached “the kingdom
of heaven is like evil (leaven)!

46. Sec below, pages 60-62, for a fuller exposition of these points.

47. See also: John 1:29; 1 John 2:2; 4:14; 2 Cor. 5:19. See: B. B. Warfield, “Christ
the Propitiation for the Sins of the World,” ed. by John E. Meeter, in The Selected Shorter
Writings of Benjamin B, Warfield (Nutley, NJ: Presbyterian and Reformed, 1970 (1915)},
1s
