114 THE GREATNESS OF THE GREAT COMMISSION

specific. That is, worship is to be engaged in every day life,'*
as well as in specific, formal exercise on the Lord’s Day.'? The
various elements of Christian worship are to be engaged with
the whole heart, soul, mind, and strength (Mark 12:30), not
while asleep, in a trance, or fidgeting while wondering about
lunch, The manner of worship is legislated by God in Scripture;
we must approach the Covenant God on His terms (e.g., Lev.
10:1,2). Hymns, prayers, offerings, exhortations, confessions,
Scripture readings, sermons, and other aspects of worship are
not to be performed by mere rote reflex. They are to be en-
gaged with devotion, as unto the Lord. In other words, we must
remember that Christ is with us “all the days” (Matt. 28:20, Gk.}
— including while we worship. We are to rejoice in the baptism
of new converts, as an aspect of our worship and as we witness
the discipling of the nations (Matt. 28:19).

3. Training in the truth. The Christian should seek a church
that promotes sound doctrine and the development of a Chris-
tian worldview based on biblical teaching.

The church should be a covenant-community fellowship,
committed to the historic creeds of the Christian faith (the
Apostle’s Creed, Nicene Creed, etc,).'* It should not be associ-
ated with the National or World Council of Churches, It should
have a solid educational program.

The piecemeal Christian faith so widespread today does not
measure up to the calling of discipling toward a Christian cul-
ture (Matt. 28:19). The church should actively train people to
submit to Christ's authority (Matt. 28:18) and work (Matt.
28:19-20). As a leading officer in the Church, Paul was con-
cerned to promote “the whole counsel of God” (Acts 20:27).

Several programs could be used to promote education in the
truth.” These include: catechetical training, a church library,

16. Rom, 12:1-2, 1 Cor 10:31.

17, Acts 20:7; 1 Cor, 11; 26; Heb. 10:25.

18. See: R. J. Rushdoony, The Foundations of Social Order: Studies in the Creeds and
Councils of the Early Church (Fairfax, VA: Thoburn, 1968) and Kenneth L. Gentry, Jr, The
Useftdness of Creeds (Mauldin, SC: GoodBirth, 1980).

19. An excellent resource and idea book is Gary North, Backward Christian Soldiers: An
Action Manual for Christian Reconstruction (Tyler, TX: Institute for Christian Economics,
