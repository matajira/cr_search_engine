The State and the Great Commission 129

We should note also that this retreatism would not be in
accord with biblical precedent. Both Paul and Peter give express
biblical principles applicable to civil government (Rom. 13:1-47;
1 Pet. 2:13-17). John Baptist and Christ even rebuked civil
authorities for their immoral conduct (Matt. 14:1-12; Mark
6:18; Luke 13:32). Scripture encourages our prayer for civil
authority (Ezra 6:10; Psa. 72:1; Jer. 29:7).

2. Obedience to civil government. The various governmental
spheres — family (1 Tim. 5:8; Eph. 5:22-6:4), Church (Heb.
13:17; 1 Pet. 5:1-5), and State (1 Tim. 2:2-3; 1 Pet. 2:14) are
ordained by God for our good.

Paul and Peter specifically oblige us to submit to civil govern-
mental authority (Rom. 13:2-7; 1 Pet. 2:12-17). Since the Great
Commission shows Christ laying claim to “all authority” on
earth, civil government must submit to His design. As a basic
and general rule, therefore, the Christian should live an order-
ly, Christ exhibiting life in terms of his civil relations {1 Pet.
9:15).

Nevertheless, the Christian at all times holds God and Christ
as supreme authority and must refuse any governmental direc-
tive which would obligate his doing that which is contrary to
God’s revealed will.* The government may not act as God;? it

 

Tt was generally due to an unstudied lack due to theological superficiality, rather than to
a researched conviction. Recently, however, there has been a resurgence of well-thought
out (though ill-conceived) anabaptist thought, calling for Christians to resist seeking
political influence. See: Charles Scriven, “The Reformation Radicals Ride Again” and
Stanley Hauerwas and William H. Sillimon, “Peculiar People” in Christianity Taday (March
5, 1990), pp. 13H

7. Paul's statements here are ideals for government, not an historical account of
Roman government.

8, For more information sce: Junius Brutus, 4 Defence of Liberty Against Tyrants
(Edmonton, Alberta: Still Waters Revival Books, 1989 [rep. 1689]). Herbert Schlossberg,
Idols for Destruction: Christian Faith and is Confrontation with American Society (Nashville:
Thomas Nelson, 1983). Francis Schaeffer, A Christin Manifesto (Westchester, IL: Cross-
way, 1981, chs, 7-10. Gary North, The Dominion Covenant: Genesis (Tyler, TX: Institute for
Christian Economics, 1982), ch. 19. Gary North, ed., Christianity end Civilization (Tyler,
TX: Geneva Divinity School, 1989), vol 2: “The Theology of Christian Resistance” and
vol. 3: “Tactics of Christian Resistance.”

9, Matt. 22:21; Acts 20:20-23, Gp. Isaiah's taunt against the king of Babylon, Isa.
14:4, 12-21,
