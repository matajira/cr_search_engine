Other books by Rev. Kenneth L. Gentry, Jr.

The Christian Case Against Abortion, 1982, rev. 1989
The Christian and Alcoholic Beverages, 1986

The Charismatic Gift of Prophecy, 1986, rev. 1989

The Beast of Revelation, 1989

Before Jerusalem Feil: Dating the Book of Revelation, 1989

House Divided: The Break-Up of Dispensational Theology (with Greg
L. Bahnsen), 1989
