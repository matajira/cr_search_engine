102 THE GREATNESS OF THE GREAT COMMISSION

concurs.”

The discipling is of “all the nations” (Matt. 28:19a). The
preaching of repentance is to “all the nations” (Luke 24:47).
Why should not all the nations be baptized (Matt. 28:19b)? In
fact, do not the Old Testament prophets expect such? For ex-
ample, Isaiah 52:12-15 prophesies of Christ:

Behold, My servant will prosper,
He will be high and lifted up,
and greatly exalted.
Just as many were astonished at you, My people,
So His appearance was marred more than any man,
And His form more than the sons of men.
Thus He will sprinkle many nations,
Kings will shut their mouths on account of Him.” (Emphasis added.)

Fourth, the eschatology of Scripture elsewhere expects world-
wide conversions, Although space prohibits our full discussion
of the evidence, I will select just two classes of evidence for the
discipling and baptizing — the Christianization — of the world.”

The presence and prospects of Christ's kingdom. That Christ’s
kingdom is powerfully present and growing in influence is
evident upon the following points considerations:

1. The time of the kingdom came in Christ’s ministry: “The
time is fulfilled, and the kingdom of God is at hand” (Mark
1:14-15),

2. The kingdom was declared present and operative during
His ministry: “If I cast out demons by the Spirit of God, then
the kingdom of God has come upon you” (Matt. 12:28).

3. In the lifetime of His hearers it would show its power:

89, Lenski, Matthew, p. 1179.

40. For a fuller discussion see: Greg L. Bahnsen and Kenneth L. Gentry, Jr, House
Divided: The Break-up of Dispensational Theology (Tyler, TX: Insticute for Christian Econom.
ics, 1989), pp. 139-286. See also: Roderick Campbell, Israel and the New Covenant (Tyler,
‘TX: Geneva Divinity School Press, 1983 [1954]; David Chilton, Paradise Restored (Ft.
Worth: Dominion, 1985); Jobn Jefferson Davis, Chris's Victorious Kingdom: Postmillennialism
Reconsidered (Grand Rapids: Baker, 1986). Consult also the systematic theologies by
Charles Hodge, A. A. Hodge, W. G. T. Shedd, and Robert L. Dabney:

AL. See also: Matt, 8:2; 4:17. Cp. Luke 4:16-21; Gal. 4:4; 2 Cor. 6:2.

42. See also: Matt. 11:11-14; 12:28; Luke 11:20; 16:16; 17:20-21.

 
