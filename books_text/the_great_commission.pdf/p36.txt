20 THE GREATNESS OF THE GREAT COMMISSION

4, Oath (specifying the solemn sanctions of the covenant). In Deuter-
onomy 27:1-30:20 the sanctions of the covenant are recorded. These
sanctions encourage ethical conduct by promising reward and discour-
age ethical rebellion by threatening curse.

5. Succession (specifying the transfer of the covenantal arrangements
into the future), In Deuteronomy 31-33 Moses is approaching death
(31:2). He encourages future strength (31:6-8) and involvement of all
the people, including the children (31:9-13). Obedience insures future
continuity of blessing (32:46-47) upon all their tribes (33:1-29).

Clearly, the covenant idea is a fundamental concept in Scripture.
Just as clearly the covenant is framed in concrete terms to avoid any
confusion as to obligations and responsibilities.

The Covenant and the Great Commission

We come now to the heart of the matter: whether Christ's Great
Commission is a covenant. If it is, then it will display the five-point
structure of the biblical covenant model. There would be other indica-
tions of the covenantal aspects of His ministry. If the Great Commis-
sion really is a covenant, then all Christians come under its stipula-
tions. They are required by God to work in history to carry it out.

The Christ of the Covenant

Christ is the fulfillment of the most basic promise of the covenant. In
Him all the promises of God find their ultimate expression (1 Cor.
1:20).'* He is the confirmation of the promises of God (Rom. 15:8).
Thus, at His birth, the joy of God’s covenant promise came to expres-
sion in inspired song in Zacharias’ prophecy: “To perform the mercy
promised to our fathers, and to remember his holy covenant” (Luke
1:72, emphasis added). The fundamental promise of the covenant (“I
will be your God; you will be My people”) comes to expression in the
birth of the One called “Immanuel” (“God with us,” Matt. 1:23), who

fifth in Deut. 16:18-18:22; the sixth in 19:1-22:8; the seventh in Deut. 29:9-98:14; the eighth in
Deut. 23:15-25:4; the ninth in Deut. 24:8-25:
Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tylet, TX: Institute for Christian
Economics, 1984), pp. 199-206 and Sutton, That You May Prosper, App. 1. For additional, similar
information, see: Walter Kaiser, Toward Old Tistanent Ethics (Grand Rapids: Zondervan, 1988), ch.
8

13, See also: Acts 19:28, 82; 26:6.

  

 
