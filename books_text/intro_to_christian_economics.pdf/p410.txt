398 An Introduction to Christian Economics

economic planning. On the so-called “Swedish miracle,” see Roland
Huntford’s The New Totalitarians (Stein & Day, 1972).

For high school use, the study by Mrs, Bettina Bien Greaves,
Principles of Economics: Syllabus for Introductory Course, is best
(Foundation for Economic Education, 1973).
