Subsidizing a Crisis: The Teacher Glut 299

Since the vast majority of the people holding the Ph.D. and other
higher degrees are not really committed to anything beyond the
Jatest fad among the professorial guild, the serious man who holds a
degree but who also holds a systematic philosophy of life now is in
a position to distinguish himself from the hordes of other applicants
for jobs. The savings in search costs that the Ph.D, once offered
(“no non-Ph.D.’s need apply”) no longer works in a glutted market.
There is an oversupply of degree-holders, but not an oversupply of
free market advocates holding the degree. If the swing away from
the intellectually castrated philosophy of neutral education (the
only kind legally permitted by State-financed schools) continues,
there should be a new demand for men and women committed te a
consistent view. Only with such a view can serious education that
is content-oriented rather than mere technique-oriented, ie., liberal
education in the traditional meaning, be maintained, Only value-
oriented teaching can pick up the institutional pieces. This should be
the hope of those behind private educational institutions.

There is one last consideration. The imposition of price and wage
controls becomes more and more of a possibility. These controls have
disastrous effects in the long run, but initially certain zones of the
economy are favored.'® One of these, as Prof. Hans Sennholz has
pointed out, is private education, As money continues to be printed
by the State and the State’s central bank, it seeks markets. Controlled
markets within the economy dry up, as capital and labor shift to the
uncontrolled zones—collectors’ markets, luxury goods, entertain-
meant, travel, and education. State-financed educational institutions
are caught in the wage-price squeeze: legislatures and bond voters
are tight-fisted (as their purchasing power continues to decline).
But the private schools reap at least an initial subsidy. State schools
limit or close enrollments, but people have money to spend, and
these funds find their way increasingly into educational outlets. We
should expect to see the expansion of private education of all kinds;
high schools, colleges, night schools, cultural institutions. A true
opportunity for the establishment of truly universal, pluralistic edu-
cation would make itself available. The shift away from the public
educational monopoly that is already showing signs of life would be
subsidized by the very imposition of statist controls.

In the Jast analysis, the educational system has become overly
dependent upon the State and the necessary educational philosophy
of all State-financed education, i.e., the philosophy of neutral edu-
cation. Today we see the erosion of the monopolistic foundation of

18. Gary North, “Price-Wage Controls: Effects and Counter-Effects,” Com-
mercial and Financial Chronicle (Aug. 21, 1969) [chap. 12, above].
