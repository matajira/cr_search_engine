396 An Introduction to Christian Economics

trand, 1960), is devastating. (It is available, at present, from the
FEE library list.) He also edited The Critics of Keynesian Econom-
ics (Van Nostrand, 1960), a collection of scholarly articles and ex-
tracts criticizing various aspects of Keynes’s thought.

On Keynes in general there are hundreds of books. The standard
introduction, favorable, is Dudley Dillard’s The Economics of John
Maynard Keynes (Prentice-Hall, 1948). Robert Lekachman’s The
Age of Keynes (Random House, 1966), is also available in paper-
back, David McCord Wright's The Keynesian System (Fordham,
1961) and Lawrence R. Kiein’s The Keynesian Revolution (Mac-
millan, 1965) are useful introductions. The refutations are many:
W. AHL Hutt, Keynesianism: Retrospect and Prospect (Regnery, 1963),
Arthur Marget’s enormous Theory of Prices (Kelley, [1938]), the
Hazlitt books, and L, Albert Hahn’s The Economics of Mlusion
(Fraser, [1949]), The latest is the compilation of Hayek’s critical
comments by Miss Sudha Shenoy, A Tiger by the Tail (London:
Institute of Economic Affairs, 1972). On the causes for Keynes’s
acceptance, see Peter Drucker’s Men, ideas and Politics (Harper &
Row, 1971).

Murray N. Rothbard, an anarchist and the major intellectual leader
of the anarcho-capitalist movement in the United States, has written
the important work, Man, Econonry and State (1962), and America’s.
Great Depression (1963), both reprinted by Nash Publishers, Los
Angeles. What Has Government Done to Our Money? (1964) is
also very useful; it is available from FEE. Two of his essays on
monetary theory are important: “The Case for a 100% Gold Dollar,”
in Leland B, Yeager (ed.), In Search of a Monetary Constitution
(Harvard University Press, 1962}, and “Money, tbe State, and
Modern Mercantilism,” in Schoeck and Wiggins (eds.), Central Plan-
ning and Neomercantilism (Van Nostrand, 1964).

Textbooks proper are useful for learning how economists play the
theoretical games that occupy their time and the taxpayers’ confis-
cated money. Textbooks are used for getting into grad school. They
are almost always Keynesian. Only Rothbard’s Man, Economy and
State is truly Austrian (Misean) in perspective. Several University
of Chicago oriented books are available. They are marred by re-
liance upon positivist assumptions, a fallacious monetary theory and
too many graphs. They are all better than Samuelson’s Economics.
The best ones are Armen Alchian and William R, Allen, University
Economics (Belmont, Calif.: Wadsworth), Thomas Sowell, Econom-
ics (Scott-Foresman), and (most delightfully) Henry Sanborn’s Whar,
How, For Whom (Box 8466, Baltimore, Md.).

There is only one really good intermediate level textbook: Israel
