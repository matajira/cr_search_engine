194 An Introduction to Christian Economics

Nevertheless, the Treasury Department does not expect (publicly)
such an increase to continue in the near future, as the August 19,
1969 letter from Eva Adams to me indicates: “The cost of the metal
in the Ll-cent coin is substantially less than the value of the coin,
and therefore there is no need for seeking a change in the legislation
by the Treasury Department.” Why should I evaluate the economic
situation in such a radically different way?

Two assumptions lie behind my expectation:' 1) there is such a
thing as economic law; and 2) the federal government will not be
able to withstand the pressures for increased expenditures, and its
present policies of balanced budgets and mild inflation will be re-
versed soon. I will not try to defend the first assumption; the coin
collector who denies it ought to sell his coins and go into home
gardening. The second assumption needs an explanation.

The government of the United States is politically committed
to a philosophy of governmental welfare. With the Keynesian
(just for the record, that word is pronounced CANES-ee-an)
and neo-Keynesian revolutions in economic thought, the welfare
goal has become linked directly to the goal of cconomic growth.
This philosophy of economic “growthsmanship” rests on a further
assumption, namely, that the free market is incapable of creating
rapid economic growth through its equilibriating device, the price
mechanism.

Thus, we are told, the government must enter the market and
make purchases with newly created credit or fiat currency in order to
stimulate economic growth. Unfortunately for this theory, if the
printing presses stop, and the monetary inflation slows, the “boom”
turns into a “bust,” just as it did in 1929: production slows, money
gets “tight” (ie., interest rates go up), usemployment rises, new
construction ceases, and large blocs of minority groups vote for
the political party which is out of power,

Given the fact that almost everyone is a member of some kind of
“minority group,” the results at the polls can be politically disastrous
for the incumbent party, So we find that in the conflict among the
competing economic goals—welfare, economic growth, and stable
prices—the latter invariably gets sacrificed in mid-twentieth century
America.

There is another factor. The demands for increased welfare
spending by all levels of government are becoming prominent.
Present programs are to be expanded and new ones added. Welfare
costs doubled, at the very minimum, during the decade of the 1960s.
In order to finance this expansion, as well as to pay for the war in
Viet Nam, the government turned to the printing presses.
