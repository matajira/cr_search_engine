394 An Introduction to Christian Economics

The Theory of Money and Credit (Irvington, N. Y.: Foundation
for Economic Education, [1912])
Bureaucracy (New Rochelle, N. Y.: Arlington House, [1944])
Pens for Freedom (South Holland, Ill: Libertarian Press,
5:
The Anti-Capitalist Mentality (Princeton: Van Nostrand, or Lib-
ertarian Press, 1956)
A full bibliography of Mises’ works is available from the Foundation
for Economic Education, Most of his books are also available from
FEE, No longer in print are The Free and Prosperous Common-
wealth, The Ultimate Foundation of Economic Science, and Episte-
mological Problems in Economics, all published by Van Nostrand.

Mises’ influence on F. A. Hayek, Wilhelm Roepke, and Lionel
Robbins is generally ignored by the economics profession. The stu-
dents have become more “respectable” than the teacher, so the source
of the original ideas tends to be ignored. It may tend to be, although
not entirely. But Mises has never been given the recognition he de-
serves, The special award by the American Economic Association in
1969, making Mises a Distinguished Fellow, was not so much an
ideological mellowing on the part of AEA’s mostly neo-Keyncsian
membership, but rather an award engineered by the Chicago School
members who believed that another free-market advocate should
be granted his due recognition. The economics guild in general re-
mains blissfully unaware of the crucial nature of Mises’ contribu-
tions, (See the Sept., 1969, issue of The American Economic Review.)

F. A, Hayek’s contributions are more widely known. His most im-
portant economics books are all published by the University of
Chicago Press: The Road to Serfdam (1944), The Constitution of
Liberty (1960), Individualism and Economic Order (1948), and
Studies in Philosophy, Politics and Economics (1967). Routledge
and Kegan Paul (London) publishes his Pure Theory of Capital
(1941), Prices and Production (1932), and the hook he edited,
Collectivist Economic Planning (1935). Monetary Theory and the
Trade Cycle (1936) is available in a reprint from Augustus Kelley
in New York. The Counter-Revolution in Science: Studies on the
Abuse of Reason (1955) is published by the Free Press and is a
distinguished contribution to the history of social science,

Wilhelm Roepke, leader of the so-called Ordo School (there is
a German scholarly publication, Ordo, which is basically a free-
market journal), died in 1964. His concerns were always geared
to the problems of industrial society and the moral order. He never
believed that a pure market economy would salve the basic prob-
lems of mankind. He, like Ortega y Gasset, was fearful of the effects
