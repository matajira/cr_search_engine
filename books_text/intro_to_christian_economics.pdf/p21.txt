The Biblical Critique of Inflation 9

22:7). Thus, the New Testament lays down this rigorous principle:
“Owe no man anything, but to love one another...” (Rom. 13:84).
The message of the Scriptures is not perfectionist, however, so that
this general principle may legitimately be transgressed under certain
emergency situations, but very definite restrictions are placed upon
every believer’s entry into debt.
The believer cannot mortgage his future, His life belongs to God,
and he cannot sell out his tamorrows to men, nor bind his family's
or country’s future. This means that long-term personal loans,
deficit financing, and national debts involve paganism, What we
cannot do to ourselves we cannot permit either our families or
our fellow believers to do to themselves. A country which is
Christian is similarly to be governed. But we cannot expect un-
believers to live by our faith or by God's law; and to allow them
the liberty of their way is no sin, providing we deal justly with
them."

This is why it was legitimate to take interest from the unbeliever, but
not from the believer (Lev, 25:36-37; Deut, 23:19-20), The un-
believer is, by definition, a slave to sin; the believer is not.

In Exodus 22:25-27, we find one of the key passages dealing with
indebtedness, It lays down two general rules: no interest shall be
taken, from fellow believers for a charity loan; and the collateral,
if it is necessary for the debtor's existence, must be returned ta him
when he needs it. In the first ease—the loan to a believer—the fore-
gone interest constitutes a charitable donation to the one in need,
That seems clear enough; the lender could have used the goods or
money for his own purposes during the period of the loan, yet he
forfeits his right to receive compensation for the loss of the use of
his goods, The secand clause, however, is not generally understood,

The raiment taken by the creditor as collateral must be returned
to the debtor in the evening. ‘This is a very peculiar kind of col-
lateral. The more common kind is the kind that 1 once heard a
priest used for loans in his predominantly Mexican-American parish:
he took two of the ear tires. There was a great incentive, he said, for
the family to get its loan paid off. But a garment which must be
returned to the debtor each evening, and taken by the creditor during
the day, is strange, on the surface. It does the ereditor no visible
good, and the debtor does not forteit the use of hig collateral when he
really needs it, ie., during the cold of the night. If anything, it seems
to be a nuisance for the creditor,

The collateral (“surety”) in this case is a benefit to the creditor

 

 

 

8. Ibid. p. 243 9, Thid., p. 249,
