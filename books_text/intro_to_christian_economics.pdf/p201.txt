Making Money Out of Money 189

sion, That is why, dollar for dollar, the rare coin is a riskier invest-
ment than bulk coins. It will appreciate rapidly with inflation, but
it will fall rapidly in a depression. As long as you are aware of this
in advance, you can plan accordingly. The reason for the fall should
be clear: a collector's pride and joy in prosperous times may be a
luxury in a time of falling prices, Collectors will tend to dump their
rare coins to meet current expenses of the family or outside business.
This is to be expected. It is in no sense an irrational decision.

Conelusion

In times of change and crisis, you want a liquid asset that appreci-
ates in value. The coin is a prime example of just this kind of asset.
If a man expects times like these, then he should consider coins as
one part of his overall investment package. Now is clearly the time
to enter the market, The tight money conditions that have prevailed
since Mr. Nixon took office have depressed the economy by tightening
credit. Men have been forced to dump their coins in order to gain
cash. Profits are being squeezed by rising costs of borrowed money,
plus increasingly militant demands of labor unions, minority groups,
and other dissident elements that are thinking in terms of the in-
flation of the earlier part of the decade, The coin market is presently
depressed. When the inflation begins again, the market will reverse
itself and shoot upward for as long as the inflation continues. And if
my analysis so far is correct, that should be a long, long time.
