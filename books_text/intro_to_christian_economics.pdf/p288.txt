 

 

276 An Introduction to Christian Economics

its glory. Thereafter, even more massive applications of science
and technology to basic human needs will have become so ur-
gently necessary that no further diversion of available talent and
resources to manned space flights can be permitted.1¢

We can hope that he is correct, but who knows for certain? The
government was so successful, as it usually is, in achieving a feat
“which served no useful purpose” other than its own glory, that we
may have more of the same. But this much should be clear: the
analogy of spaceship earth is more than an analogy; it is a call to re-
figious commitment, The call is to faith in centralized planning,

At the beginning of this essay, I pointed to the dual theories of
regeneration, symbolized by the Yogi and the Commissar, They feed
on cach other, take in each other’s intellectual washing, so to speak,
Tf we are to confront the mythology of spaceship earth, it must be
in terms of a rival moral philosophy, one which has social and ecto-
nomic implications, as well as technological implications, We must
deny the validity of any vision of man as central planner, a little god
who would arrange in an omniscient fashion the lives of all men in
all the spheres of their existence, as if we were some permanent
military crew. We must acknowledge the validity of the late C. S.
Lewis’ warning in The Abolition of Man that when we hear men
speaking of “man’s taking control of man,” we should understand that
it implies certain men taking control of all the others.

When men seek to divinize the State, they succeed merely in
creating hell on carth, The Christian church fought this point out
with the Roman emperors, both pagan and Arian. The State may
not claim to be God’s exclusive or even chicf representative on
earth.!7 The theology of spaceship carth would have us return to the
religious political theory of the ancient world, all in the name of
progressive technolagy and planning.

The astronauts are back on carth, We must seek to keep them
here. It is time to ground our spaceship programs, both interplane-
tary and domestic, Let the captains go down with their ideological
ship, There arc better ways of allocating our scarce resources than
in constructing spaceship earth.

16. William G. Pollard, Man on a Spaceship (Claremont, Calif.: Claremont
Colleges, 1967), pp. 59-60.
i Rushdoony, Foundations of Sacial Order (Nutley, N. J.: The
cals Pres, 1968).
