104 An Introduction to Christian Economics

it is change that is constant in human life, not expansion or linear
development. There are limits on secular expansion.

Stil, it is reasonable to expect that the growth in the number of
goods and services in a free market will exceed the number of new
gold and silver discoveries, If so, then it is equally reasonable to
expect to see prices in the aggregate in a slow decline, In fact, by
calling for increased production, we are calling for lower prices,
if the market is to clear itself of all goods and services offered for sale.
Falling prices are no less desirable in the aggregate than increasing
aggregate productivity, They are economic complements.

Businessmen are frequently heard to say that their employees are
incapable of understanding that money wages are not the important
thing, but real income is. Yet these same employers seem incapable
of comprehending that profits are not dependent upon an increasing
aggregate price level. It does not matter for aggregate profits whether
the price level is falling, rising, or stable, What does matter is the
entrepreneur's ability to forecast future economic conditions, includ-
ing the direction of prices relevant to his business, Every price today
includes a component based on the forecast of buyer and seller con-
cerning the state of conditions in the future. If a man on a fixed in-
come wants to buy a product, and he expects the price to rise
tomorrow, he logically should buy today; if he expects the price to
fall, he should wait. Thus, the key to economic success is the accuracy
of one’s discounting, for every price reflects in part the future price,
discounted to the present. The aggregate level of prices is irrelevant;
what is relevant is one’s ability to forecast particular prices.

Tt is quite likely that a falling price level (due to increased produc-
tion of non-monetary goods and services) would require more mone-
tary units of a smaller denomination. But this is not the same as an
increase of the aggregate money supply. It is not monetary inflation.
Four quarters can be added to the money supply without inflation so
long as a paper one dollar bili is destroyed. The effects are not the
same as a simple addition of the four quarters to the money supply.
The first example conveys no increase of purchasing power to anyone;
the second does. In the first example, no one on a fixed income
has to face an increased price level or an empty space on a store’s
shelf due to someone else’s purchase. The second example forces a
redistribution of wealth, from the man who does not have access to
the four new quarters into the possession of the man who does. The
first example does not set up a boom-bust cycle; the second does.?¢

Prices in the aggregate can fall to zero only if scarcity is entirely

16. North, “Repressed Depression,” op. cit.
