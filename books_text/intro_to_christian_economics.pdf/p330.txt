318 An Introduction to Christian Economics

the owner must have protection from self-proclaimed buyers who are
not prepared to buy. Some group of malicious individuals, or some
minority with a “cause,” could force up the tax rates of a community
merely by systematically going through a neighborhood, making false
offers far too high, declaring the offer to the tax assessors, and then
they could quietly disappear, leaving the owners with new assess-
ments, The protection would be in the form of a requirement on
the part of those making an offer to purchase at that price if the
owner decides to sell, given either the pressure of higher taxes or the
temptation of a highcr purchase price than he had imagined possible,
Any prospective buyer or buyers, upon failing to make good their
offer to buy, would then be assessed a fine, ta be paid to the owner
(not the county, or at least not solely to the county). A reasonable
fine might be triple the annual increase in taxes that the offer would
have forced upon the incumbent owner. The owner, as the one
potentially damaged by the false offer, would be the recipient of
the fine—restitution to him for an act of potential theft (economic
loss) that the false offer would have caused him.

One final provision seems in order. What would prevent an
owner from accepting the higher tax figure for one year, and then
declaring a lower figure the next year, on the assumption that the
previous potential buyer has probably purchased elsewhere? The
county could counter with one last bit of coercion: it could publish
a list of all addresses at which the owners had declared a drop in
property value since the previous year. The list would not list
names of owners, nor would it list the actual declared valuation, It
would list only the percentage of the drop, and the book could be
printed with the properties experiencing the greatest fall in declared.
valuation appearing higher on the fist. Prospective buyers would
be alerted to bargains, Property owners would be encouraged to
list their properties at figures approaching true market valuc. No nian
would be compelled to sell, but he would pay in terms of what the
market determines that his property is worth.

Benefits

The major advantages have already been sketched, There are
others. First, the assessed valuation would now tend to reflect market
valuation. Not only would this reduce the arbitrariness of the tax as-
sessor (and perhaps even the size of the bureaucracy, or at least the
costs associated with training “skilled” assessors), but it would reduce
another arbitrary factor in government, It would eliminate the need
for disputes over eminent domain. From a Christian standpoint, it
would be preferable, theoretically, to abolish the right of any civil
