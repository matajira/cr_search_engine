The Theology of the Exponential Curve 87

4:3, 4), the Keynesians and Chicago School monetarists have adopted
as gospel truth. (Macroeconomists may resent my caricature of
their position; if so, they will begin to understand my resentment
against the use of this very caricature by proponents of inflationary
policies in the popular press: the public has swallowed an academic
stone under the impression that it was steak.)

Any increase of the moncy supply at a rate above zero percent (yes,
even as “low” as 3 to 5 percent) will eventually create a money
supply which will defy calculation, producing what Mises has called
the “crack-up boom.” Men will abandon the use of the particular
currency in favor of another currency or even in favor of barter,
just as they did in Germany in the early 1920’s, When I presented
this argument recently, it evoked this response:

Compound interest, which has been with us for centuries and
forms the mainspring of all savings is an exponential curve, My
first admonition as a child was “put your money in a bank and
watch it grow.”

It is very true that this growth has been interrupted in the past
by bank failures and the like, but this has not taken place to a
great extent in the last 30 years, Surely every right-thinking per-
son does not want things like that to occur again. Banks must
maintain sufficient currency to protect their depositors, and to do
so, there must be [a] constantly increasing money supply to cover
interest accumulations.

The thing which astounded me when I first read this letter was
that a man whose instincts are clear procapitalism argues in favor of
inflation because he has been led to believe that the best way to de-
fend capitalism is in terms of the logic of the exponential curve. Yet
it is because the exponential curve cannot be sustained over time,
since we live in a world of scarcity, that men should make their de-
fense of the free market. If all goods were free, economics as a
science would not exist. Men would not then need ta economize.
Instead of re-examining the philosophy of compound interest, the
man. I’ve quoted felt compelled to recommend the inflation of the
exponential curve.

Medieval theologians, who were social and economic thinkers,
followed the Old Testament’s provisions against the taking of usury.
They saw, in an admittedly obscure fashion, that a man must not
demand an automatic payment of interest unless he shares in the
tisk of the failure of the particular enterprise. Using modern eco-
nomic categories, we can say that profit, which stems from an ac-
curate forecast of future demand and efficient planning in terms of
