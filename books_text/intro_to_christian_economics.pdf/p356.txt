344 An Introduction to Christian Economics

sponse on the part of business firms—-consolidation for the sake of
efficiency on an increasingly inflationary market—prosecuted by the
State which has created those very inflationary pressures, There is
an inconsistency somewhere.

Tariffs Are Taxes

A tariff is a special kind of tax. Tt is a tax paid by importers for
the right to offer foreign products for sale on a domestic market.
Indirectly, however, the tax is borne by a whole host of people, and
these people are seldom even aware that they are paying the tax.

First, let us consider those in the United States. One group af-
fected adversely by a tariff is that made up of consumers who actually
purchase some foreign product. They pay a higher price than would
have been the case had no duty been imposed on the importer.
Another consumer group is the one which buys an American product
at a high price which is protected by the tariff. Were there no tariff,
the domestic firms would be forced either to lower their prices or to
shift to some line of production in which they could compete suc-
cessfully, Then there is the nonconsumer group which would have
entered the market had the lower prices been in effect; their form of
the “tax” is simply the inability to enjoy the use of products which
might have been available to them had the State not intervened in
international. trade.

Others besides the consumers pay, The importer who might have
been able to offer cheaper products, or more of the products, if
there had been no tariff, is also hurt. His business is restricted, and
be reaps fewer profits, All those connected with imports are harmed.
Yet, so ate exporters, They find that foreign governments tend to
impose retaliatory tariffs on our products going abroad. Even if
those governments do not, foreigners have fewer dollars to spend on
our products, because we have purchased fewer of theirs.

Two groups are obviously aided. The inefficient domestic producer
is the recipient of an indirect government subsidy, so he reaps at
least short-run benefits. The other group is the State itself; it has
increased its power, and it has increased its revenue. (It is con~-
ceivable to imagine a case where higher revenues in the long run re-
sult from lower tariffs, since more volume would be involved, so we
might better speak of short-run increases of revenue.) We could
also speak of a psychological benefit provided for all those who
erroneously believe that protective tariffs actually protect them, but
this is a benefit based on ignorance, and I hesitate to count it as a
positive effect.

A second consideration should be those who are hurt abroad, al-
