246 An Introduction to Christian Economics

gods. Destroy the system of plural sovereignties, each with its own
legitimate realm of authority, and society faces the creation of a
vast bureaucracy. In fact, the existence of the deadening bureaucratic
hand will be the single sovereignty that can compete effectively with
the capriciousness of the will of the ruler, The citizen is caught in
the cross-fire between the impersonal cage of bureaucracy and the
contingent world of the totalitarian leader. Total personalism doing
battle with total impersonalism, with the individual citizen crushed
in the middle. (Nazi Germany has been described as a “confusion
of private armies and private intelligence services,” in which the tradi-
tional army, the Reichswehr, was confronted with, first, the S.A.
forces under Roehm, and second, the S.S. forces under Himmler, and
few men ever knew where they stood in relation to the various bu-
reaucracies. The faceless bureaucrats and Hitler were the founda-
tions of men’s lives.)

The one institution which is universally acknowledged by con-
servative social analysts and philosophers as being inescapable for
the maintenance of a free society is the family. The family’s relation-
ship with that other crucial institutional arrangement, private owner-
ship of property, is inescapable, Rushdoony has pointed this out in
his study, Bread Upon the Waters:

Biblical law places power and authority into the hands of the
parents, especially the father, and, as long as the family has
liberty, liberty based on the power of property, the parents have
authority. The primary purpose of the inheritance tax has been
to destroy this parental power; the total financial gain to the
state by means of inheritance taxes is small. Similarly, transfer
of power over education, income, and property from the family
to the state has undercut parental power and authority.

Because the modern state controls the education, income, prop-
erty, and labor of all its citizens, it thus controls the totality of
powers within the country. The result is totalitarianism. Every
country that weakens the independence and liberty of the family
and property moves steadily into totalitarianism. It makes no
difference in which country this occurs, and what laws the state
passes as a restraint on itself. Property is power, and when the
slate grows in its controls over property, it grows in the same
degree towards totalitarian power. No political program can
stop this growth unless it restores to the family its control over
property, income, and education. As long as the state retains
the control, it will retain the power and the authority, and it is
naive to expect anything but tyranny.”

It is therefore not surprising that the Soviet Union officially

2. R. J. Rushdoony, Bread Upon the Waters (Nutley, N. J.: The Craig
Press, 1971), pp. 70-71.
