Gertrude Coogan and the Myth of Social Credit 157

to write IOU’s. Instead of the monetary inflation caused by fractional
rescrve banking, she would give us monetary inflation produced by
the State bureaucracy. But monetary debasement in any form is
robbery, an enforced redistribution of wealth, and all money not
backed by 100 percent reserves gold or silver is inflationary.” She
would stop one kind of inflation only to inaugurate another. Her
apswer is no answer at all, at least not for the free market,

Economics, of course, reveals the pages of economic nonsense
contained in her writings. She knows this ta be true, and she has a
pathological hatred for all economists, In her bibliographies, there
is not one trained economist represented, She thinks that all econo-
mists have merely aped Adam Smith’s system. She apparently has
no knowledge of the revolution in economics wrought by Menger,
Boehm-Bawerk, Weiser, and Mises, a revolution which threw out
Adam Smith’s mistakes and retained his accurate contributions. Yet
she classifies all economists as paid hirelings of the money trust.
“Economist,” she writes, “is the learned term still applied to those
who write unintelligible discussions of money, prices, public finance,
and so-called political science.” So defined, Coogan is actually
the world’s leading economist, for no more garbled, unintelligible,
dangerous statist, pleading in the name of statistical science, could
be imagined.

This is a woman’s economic treatise. She personalizes all her
enemies, and anyone who deals with ideas is a tool of the money
trust. Ideas are her real enemy, even more than gold. Her books
are a mass of feeling, rather than accurate and careful economic
analysis. Ideas are superficial; hatreds are the basis of her writings,
not anything resembling free market analysis. Gertrude Coogan
deserves the words that are given by James Burnham in his review
of Eleanor Roosevelt’s India and the Awakening East:

92. To some extent, this criticism would apply to gold and silver mining.
But mining produces metals for a market, and the market may allocate the
uses of the metals in many ways, including’ omamentation, industrial purposes,
or religious uses. Mining is not fraudulent; it does not offer anyone something
for nothing; it is based on voluntary contracts; it does not create new money
very rapidly; it does restrain the expansion of State power. If some aspects of
mining result in the redistribution of wealth from those who do not have early
access to the new gold or silver to those who do have early access, then we
would have to say these effects are comparable to the effects of free. competi-
tion. Some people, through ignorance or weakness, may lose. But to attempt
perfectionist solutions—the abolition of mining; State control of money-
creation-—is to invite disaster. Mining may produce some effects that are
unfortunate, in this imperfect world, among many legilimate blessings; fiat
paper money or credit produces few temporary benefits and fewer long-run
benefits, and creates untold economic disasters. Mining may have minor de-
fects economically; fiat money is almost entirely defective, in principle and in
operation.

93. Coogan, p. 205.
