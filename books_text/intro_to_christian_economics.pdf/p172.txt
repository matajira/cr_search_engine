160 An Introduction to Christian Economics

communicated on all-sides, prompting the wealthy to lay out for
private mansions greater sums than were expended for public
works, and creating a permanent source of employment, by the
future expenses which the repairs and occupation of these man-
sions will require,'

This is the old cry: “Full employment by public inflation.” The
old fallacy of employment through repairs is brought up, an idea
thoroughly repudiated by Bastiat in his argument of “the things un-
seen.” Henry Hazlitt reproduces it in the opening pages of his little
classic, Economics in One Lesson. If repairs really bring public
benefit, why not throw rocks through the windows of the mansions?
In fact, why not throw rocks through all the windows in the town?
The council forgot that for every pound spent in repairs, a pound
is withdrawn from usc in other areas, For the employment of the
stonemason, the mansion owner cuts down in his consumption of
cake, and the baker suffers a loss of business, But this loss to the
baker is unseen, in the same way that the loss to the pensioner because
of inflation js not seen. What is seen is the employed stonemason.
What is seen is the government WPA project built with the counter-
feit currency, The argument in support of robbery which the council
employed collapses under its own weight.1°%

What about the prosperity of Guernsey? Do not the facts disprove
all this economic theory? That is difficult to say. The “facts”
have been selected by inflationists only. Proponents of sound, gold-
backed paper currency had never heard of Guernsey until the in-
flationists at Omni began to praise Guernsey’s many achievements.
Which facts have been selected? Only those facts which seem to
support the thesis. Nothing is said of domestic price levels, or of the
amount of deprivation caused to people who were unable to buy at
the new, inflated prices. We are told nothing of the numbers of
businesses which were undoubtedly thrown out of existence by
rising costs in raw materials and labor. Laborers were being used
to build state monuments; all who were so employed by the State
at this economically wasteful project were not available for some Jocal
businessman to employ at an economically profitable task, But all
this is ignored,,

They did let one fact slip out: the tariff rate was very low all
through these years of inflation.°* This may have helped to in-
crease the prosperity of the local buyers by permitting lower prices.

104, Ibid., p. 24,

105. Cf, Frederic Bastiat, “What Is Seen and What Is Not Seen” (1850),
Selected Essays on Political Economy (Foundation for Economic Education,
1968}, chap. 1.

106. Guernsey, p. 12.
