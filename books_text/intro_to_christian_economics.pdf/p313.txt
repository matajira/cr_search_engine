Chapter XXVI

URBAN RENEWAL
AND THE DOCTRINE OF SUNK COSTS

[The concept that I am trying to convey to the reader in the
following essay is that past economic decisions are past. Good
or bad, they are past. Therefore, the rational thing to do is
to make the best of the present, which includes making the
best of the future, given the limitations of the present. People
must not let fallacious, meaningless economic concepts like
“equity” or “unused capacity” misguide them in their deci~
sions. Godly stewardship requires that each man use his gifts
io the best of his ability. The doctrine of sunk costs should
help men to accomplish this ethical goal.]

One of the most frustrating experiences in the area of economic
reasoning is to explain in detail why a particular government welfare
project is economically unsound and therefore wasteful. of . scarce
resources, After giving assent to point after point of the argument, the
listener refuses to accept the logical conclusion that the project should
be abandoned: “But we can’t stop now. We’ve already sunk too much
into it. If we stop now, it would mean that we’ve lost everything!”
On the face of it, this answer seems convincing. So, how does one
deal with it?

Take, for example, the urban renewal program. It has been in
operation for two decades, and apparently is a permanent and
expanding part of the expenses of the federal government, Its
spectacular failure to accomplish its stated goals—-to provide inex-
pensive housing for low-income groups—has been thoroughly ex-
plored in Professor Martin Anderson’s study, The Federal Bulldozer
(M.LT. Press, 1964). We can ignore here such aspects of the
program as the destruction of community bonds which ‘relieve the
alienation of urban life, the inevitable result of tearing down old,
familiar neighborhoods. We need only point to the conclusion of
Professor Anderson: “Most of the new buildings constructed in
urban renewal areas are high-rise. apartment buildings for high
income familics; only 6 percent of the construction is public housing.”
This fact is amply demonstrated: “The median monthly rent of the

301
