Social Antinomianism 357

the social fabric of Roman society. This disturbs him, and therefore
he returns to his old theme: “It would, however, be entirely at
variance with the spirit and intention of the gospel, with the Message,
if from the above we were logically to draw up socio-economié con-
clusions which would then have to be applied in practical politics.
Not a few Christians perpetuate in this way an economic and political
conservatism. The same goes for progressivist-socialistic conclusions
from biblical ‘data’. . .” (p. 34). Common property in Acts 4:32 is
somehow relevant today; conservative elements in the Bible are not.
He reasserts himself once again: “The biblical message of the king-
dom of God does not directly address itself to the betterment of
human society which includes, among other things, property rela-
tions. But, to be sure, it does indeed affect them!” To be sure of
what? How does it affect them? In his answer, Troost arrives at
a position of total antinomian mysticism: “In order to exercise our
property tights in everyday life in the right manner, and to handle
our possessions before the face of God in a way pleasing to him,
nothing less is required than the merciful intervention of God, from
above, through the Holy Ghost. Unless regenerated, common sense
will change nothing. Renewal must come from the Top down; it will
not come up by itself from the bottom. Our natural reason can
achieve nothing here” (p .34).

Consider what Troost is saying. The Bible, he has said, does not
provide any concrete data—no applicable kind of special revelation—
in the area of economic and political affairs. Yet he is also saying
that “Our natural reason can achieve nothing here.” Not only is
there no special revelation in social affairs, there is no general reve-
lation on which we can rely. And so we must sit quietly and wait
for the mystical intervention of the Holy Spirit to guide us in all of
our private community decisions; God has seen fit to leave us with-
out any concrete standards in such matters. This, I am compelled to
conclude is antinomianism. It is strangely like the mystical brand of
Christianity that is called Penielism. I am unable to see how it is
even remotely Reformed.

This does not mean that Troost has no recommendations for the
contemporary world, Naturally, he does not derive them from the
Bible, and apparently the “common sense” of the unregenerate world
has given him no aid. In fact, he does not specify any source for his
recommendations. Nevertheless, he is able to conclude that “It
is part of our religion to engage whole-heartedly in the battle for a
just distribution of income (nationally, but also internationally,
through foreign aid), for just property relations, and for a just eco-
nomic order. It is part of our religion because we are called to it
