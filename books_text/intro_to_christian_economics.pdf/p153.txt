Gertrude Coogan and the Myth of Social Credit 141

property that may legitimately be controlled by a free market. She
implicitly denies what she proclaims to be the glory of capitalism,
the right to hold property. Naturally, she sees no contradiction here,
and therefore she fails to try to comment upon it.

She advocates a modern version of the old Roman Catholic
Scholastic doctrine of the “just price” and the “just wage.” She calls
for “equitable” price levels"* and “decent” prices.2* She does not
take her theory of prices beyond the erroneous level of the 13th
century, never mentioning the idea of balancing supply and demand
through the price mechanism.** This ancient economic heresy, “just”
wages and prices, led the local governments of the Middle Ages to
enforce the prices set by the guilds, and thus to fix prices and stifle
industry. Coogan recommends virtually the same thing, demanding
that a board of National Monetary Trustees be established to set all
prices at the “desired price level.”?” Yet she calls herself a capitalist.
But, then again, so did Keynes.

Coogan rightfully refers to private counterfeiting as “theft.”*8 She
does not call the State’s printing press money counterfeit, although
the private bills are counterfeits. Naturally not; in her view the very
ink of the State’s presses turns cheap paper into valuable money.
Private counterfeits do not have that “mystical something” which
turns paper into money, That something is possessed only by the
Sovereign Authority. Somehow (she cannot explain why), the State
bills are money and the private bills are not, in spite of the fact that
both look alike and both circulate just as easily. The private bills
only act as money, but they are not really lawful money. This is
not economics; it is mysticism of the most dangerous type.

Counterfeiting is theft for one reason, and only one reason. Paper
bills are issued which look exactly like bills which are backed by
100 percent of their face value in money metals, but these bills do
not have such a backing. In other words, if all the individuals went
to claim their money metals at the same time, some people could
not collect. The storage warehouse, whether a treasury building or

24. Coagan, p. 98.

25. Ibid., p. 2.

26. The same failure of understanding marred the economic thinking of the
colonial Puritans of New England. They tried, unsuccessfully, to legislate “fair
wages” and “reasonable prices.” By 1676 this policy had failed so many times
that it was no longer attempted. Only with the coming of the American Revolu-
tion did the political authorities reinstitute price controls, and the immediate
result was the devastating shortage of goods at Valley Forge: Percy L. Greaves,
“From Price Control to Valley Forge 1777-1778," The Freeman (February,
1972). On the development of Puritan economic thought, see North, Puritan
Economic Thought: Winthrap to Franklin forthcoming, 1973).

27. Coogan, p. 338.

28. Ibid., p. 14.
