The Next Cain Crisis 197

allocate parking space far more than they are revenue devices.)
As larger token coins replace the penny, people will not notice the
loss of the copper when it is time to debase the coin. If the penny
meters are replaced by nickel or dime meters, redesigning the penny
will not be so difficult a task. There should not be much resistance
to the debased coin,

Collectors should respect the value of bulk copper pennies as an
inflation hedge. If mass inflation comes, as it could by the late 1970's
or early 1980's, the penny will prove to be a sound investment, if
only as a survival coin, You may not want to spend the more
precious silver coin for some product if your change will be made
in debased coins or cheap paper. The copper coin may serve this
purpose.

An example of the value of the penny was brought home to me
by the Rev. R. J. Rushdoony. The Rev. Karl Treutz, minister of the
Protestant church of Geislingen am Kocher, Germany, during World
War II, saw what Hitler's inflation was doing to the economy, From
his experience in the German inflation of the early 1920's, he knew
that silver coins would be difficult to collect, and perhaps illegal to
hoard, so he concentrated on collecting copper coins. He would
remove the coins from the collection plate, replacing them with the
paper money of his salary (the coins would have gone to the bank,
in any case). By the time the war ended, he had a bathtub full of
copper coins, and he was able to survive the ravages of the postwar
inflation and shortages without much inconvenience. Those effects
were terrible for most other Germans. The head of Abby Rents in.
California began his multimillion dollar collection of Bibles in these
years; a desperate German sold him an original Gutenberg Bible for
$50, American. It is now worth over a million dollars.

Planning at this stage may be very profitable later. One’s planning
in terms of economic law would be wiser than planning in terms of
Treasury Department explanations. As Senator Yarborough of
Texas said to Miss Adams (referring to the questionable information
distributed by the Treasury in regard to the silver certificates and
their redemption): “It has left a legacy of people not believing.
They say you can’t trust what they are going to tell about the
money now.”

He who has ears to bear, as the Bible says, let him hear.
