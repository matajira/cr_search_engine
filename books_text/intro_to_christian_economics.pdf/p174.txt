162 An Introduction to Christian Economics

rigorous anti-gold restrictions. If the “conservatives” advocate the
same things, as Social Credit proponents do, how will we see any
change? The only advance along the road of economic sanity that
the advent of a Social Credit regime might bring us would be the
abolition of fractional reserve banking.1¢? But instead of the bankers
we would have our elected representatives, through the Treasury,
debase our currency directly, without the restraining factor of in-
terest rates holding them back. Free money, come and get it!

As the child said, when offered broccoli by his mother, “I say it’s
spinach, and I say nuts to it!” If Social Credit is conservatism, I
say nuts to itr?

108. Not all Social Credit advocates advocate the complete abolition of
central banking and fractional reserves, although the American Social Crediters
do. Basing his analysis on the English branch of Social Credit, Hansen there-
fore concludes: “Social Credit therefore approaches, but does not go quite so
far, as the 100 per cent Reserve Principle advocated by Sir Frederick Soddy,
Irving Fisher and others. The latter plan would take away altogether from the
banks the privilege which they now hold of lending funds by creating bank
deposits." Full Recovery or Stagnation, p. 94. In all likelihood, however,
American readers of Coogan, Vennard, and Kenan would follow Soddy and
Fisher. Fisher, in fact, was cited by a Social Crediter I once met as the econo-
mist who “sees things basically the way we do on the question of stable money.”
Fisher, you may recall, was the economist who said there would never be an-
other depression, just one month before the stock market crash,

109. Not all proponents of the conspiracy theory of the origin of the Federal
Reserve System are Social Credit inflationists. Gary Allen, writing in the John
Birch Society’s American Opinion (April, 1970), concluded his study of the
Federal Reserve with these words: “Still, the argument over whether the
socialist bureaucrat or the radical international bankers should have a monopoly
on printing funny money is one of those false alternatives. The only thing that
will stop politicians and the Insiders of international banking from taking
control of this country by destroying its economy with inflation and bust is to
have a currency that is backed by tangible wealth—gold and silver.” This
statement should be required reading for every “conservative” Social Credit
adyocate in the world, and the radical Social Crediters like Jerry Voorhis, as
well. Allen bases his economic analysis of the phenomenon of depression on
the writings of Mises, Sennholz, and Rothbard, just as I do.’ His is the con-
servative position on the money question, not Gertrude Coogan’s or Wycliffe
B, Vennard's. Cf. Sennholz, “The Federal Reserve System,” American Opinion
(April, 1958).
