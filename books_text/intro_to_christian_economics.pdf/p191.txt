Gresham's Law Revisited: The Coin Crisis of 1964 179

This monopoly makes it possible for the State and its agents, the
banks, to reap some monopolistic profits. For example, let us say
that a State possesses three billion ounces of silver. In order to
make things clear, let us assume that the state then prints up three
billion paper bills, each of which is legally redeemable for one ounce
of silver. The state now discovers that its official bills circulate as
well as the silver would. So long as the state is considered fiscally
trustworthy by the public, very few people ever come in and demand.
their silver. The state officials now see that they can print up even
more bills, identical in appearance (other than serial numbers) with
the first batch. This time, however, there is no silver available in
case the public should demand full, immediate redemption of the bills.
The state can now increase its expenditures without raising visible
taxes: it can buy more goods, create more jobs, maintain “full
employment,” and keep voters happy—temporarily. Unfortunately
for the planners, the debasement of the paper brings Gresham’s
Law into effect.

As we saw earlier, the artifically undervalued money (silver) is
driven out of circulation by the artificially overvalued money (frac-
tional reserve paper bills). This is simply bimeallism’s old dilemma,
only in a newer, more radical garb: it is impossible to maintain a fixed
ratio between the value of silver and the value of the paper unless
the number of paper bills is regulated to conform identically with the
silver in reserve. But that, of course, would defeat the plans of the
planners, since under those circumstances, they could spend no more
money than they took into the Treasury through direct, visible, and
legally proscribed taxes. Gresham’s Law operates, in short, whenever
tax laws are superseded by State policies of inflation.

In the initial stages of the inflation, the majority of citizens remain
unaware of what is happening. The planners are still trusted, and
theze is no rush to claim the silver. The problem is not escaped for
long. The increased supply of money (due to the influx of the
unbacked paper bills) tends to drive up prices, including the price of
raw silver. This rise in the aggregate price level reflects, among other
things, a rise in the value of: the silver contained in the nation’s
coinage. For example, say that the price of silver was 75 cents per
ounce at the time when the new paper bills began to enter the
economy, Imagine a silver coin which contains slightly less than a
full ounce of silver, plus some other alloys to give it durability,
strength, etc. As the inflation continues, the free market price of raw
silver climbs steadily: 78 cents per ounce, 85 cents, 97 cents. This
particular coin, which we may designate as a silver dollar, became
a full-bodied coin, i.e., where the value of the silver it contained was
