150 An Introduction to Christian Economics

figure called the national income, Planners plan by that figure,
supposedly, setting targets for its growth. . Now, the national
income is a somewhat less than reliable “ ‘ageregate.” The data

. about the components entering into such aggregates as na-
tional income, volume of production, savings and investments,
ete., are pure “guestimates,” subject to arbitrary manipulation,
The methods to substitute what amounts to “very wild guesses” in
the place of factual knowledge are known to the statisticians as
“{nterprelating between henchworks, extrapolating from bench-
marks, blowing up sample data, using imposed weights, inserting
trends, applying booster factors... .°7

Once again, Coogan has drifted into the old Keynesian fallacy of
“aggregate economics.” Hazlitt has dealt at length with the problem,
and his arguments serve to refute Coogan as well as they refute
Keynes.” Her hope in the “scientific” Monetary Trustees is a futile
one, If a system such as this one were imposed upon the American
people, it would spell the end of freedom. These bureaucrats would
dictate what the standards of the arbitrary index numbers were,
and their word would be law. Who could challenge the validity of
such “scientific” operations? Who would be permitted to voice such
objections? Even the duly elected representatives in the legislatures
would be powerless against this economic dictatorship, in spite of the
fact that the powers of the Monetary Trusteeship are supposedly
delegated by the “trustworthy” elected officials whom Coogan re-
veres so much, As Hayek has explained, “In these instances dele-
gation means that some authority is given power to make with the
force of law what to all intents and purposes are arbitrary decisions.”
The “experts” are the true formulators of the law; their delegated au-
thority has become the final authority.7%* The Monetary Trustees
would become the economic tyrants of the nation. We would be
sold into economic slavery at the cost of a promised, but impossible,
“stable dollar.”

Coogan says that she is opposed to any State-enforced redistribu-
tion of wealth,” yet she champions an inflationary State currency
which would necessarily redistribute a nation’s wealth. Not looking
at society from the perspective of the individual, she has missed the
implications of State inflation in this respect. Her error is colossal.

71. Melchior Palyi, An Inflation Primer (Chicago: Regnery, 1961), p. 84.
72, Haulitt, Failure, chap. 27. —_ .
73, F. A. Hayek, The Road to Serfdom (University of Chicago, 1944),

TA, ibid., p. 65.

75. For ‘another Social Credit work, far more lengthy and sophisticated
than Coogan’s, see W. E. Tumer, Stable Money: A Conservative Answer to
Business Cycles (Ft. Worth: Marvin D. Evans Co., 1966).

76. Coogan, p. 293.
