Reason, Neutrality, and the Free Market 227

point of social analysis in a conservative framework; the family is.
The conservative is therefore a defender of a pluralistic community,
with each institution able to enforce a limited sovereignty over mem-
bers of the community? The alternative to this is tyranny. As
A. A. Berle puts it in his recent study, Power: “Power invariably fills
any vacuum in human organization. As between chaos and power,
the latter always prevails.”*

How is it possible that libertarians have been able to cooperate
with traditional conservatives for as long as they have? In the second
issue of the New Individualist Review (Summer, 1961), Edward C.
Facey offered this explanation:

Still, individualists merge with the conservatives in urging a
strict. adherence to the Constitution in the United States. This is
a tactical maneuver. It is the strategy of individualists to work a
Fabianism in reverse until one by one the parts of the political
sttucture, beginning with the most absurd, are up-ended and con-
tinuing until nothing is left. The Constitution, strictly interpreted,
aids in this process,
That was the perspective of a libertarian a decade ago. Since that
time a noticeable shift in tactics has taken place. The Vietnam war
issue has convinced many libertarians of the New Right that they are
more closely aligned in principle with the anarchist wing of the New
Left, As one libertarian speaker put it at a conference sponsored by
a local YAF chapter in Southern California in April, 1969, freedom
today is threatened more by conservative Republicans than by an-
archists in America.® If this is not the prevailing view of a majority
of libertarians, it is the direction in which many are moving at the
present time,

The pure anarchist has litde difficulty in establishing his definition
of freedom: where the State is, freedom is not. This does not mean,
of course, that libertarians do not quarrel among themselves. A
classic example of an operationally irreconcilable intellectual division
within the libertarian camp is the one which divides Murray Rothbard
and Robert LeFevre. Rothbard follows John Locke and argues that
the original claim of a man to the right of free exercise of property
is limited by the amount of property which he can actually utilize

3. E. L. Hebden Taylor, The Christian Philosophy, of Law, Politics and the
State (Nutley, N.J.: The Craig Press, 1966), chap. 9.

4 Adolph A. Berle, Power (New York: Harcourt, Brace & World, 1969),
Be

2 “Early examples of the links between the New Left and the Anarchist
Right are Murray N. Rothbard, “Liberty and the New Left,” Left and Right, 1
(Autumn, 1965), and Ronald Hamowy, “Left and Right Meet,” New Republic
(March 12, 1966), Both authors emphasize the similarity of goals that both
movements share.
