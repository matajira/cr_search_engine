94 An Introduction to Christian Economics

of the holy grail, the supporter of the free market is forced to add
certain key qualifications to the general demand for expausion. First,
that all costs of the growth process be paid for by those who by virtue
of their ownership of the means of production gain access to the fruits
of production. This implies that society has the right to protect itself
from unwanted “spillover” effects like pollution, ie., that the so-
called social costs be converted into private costs whenever possible.
Second, that economic growth be induced by the voluntary activities
of men cooperating on a private market. The State-sponsored proj-
ects of “growthmanship,” especially growth induced through inflation-
ary deficit budgets, are to be avoided? Third, that growth not be
viewed as a potentially unlimited process over time, as if resources
were in unlimited supply.’ In short, aggregate economic growth should
be the product of the activities of individual men and firms acting in
concert according to the impersonal dictates of a competitive market
economy, It should be the goal of national governments only in the
limited sense of policies that favor individual initiative and the smooth
operation of the market, such as legal guarantees supporting volun-
trary contracts, the prohibition of violence, and so forth.

The “and so forth” is a constant source of intellectual as well as
political conflict. One of the more heated areas of contention among

1, I am following Exodus 22:5-6 rather than R. H. Coase’s clever soph-
istry. Coase, “fhe Problem of Social Cost,” Journal of Law and Eco-
nomics, TIT (1960). Prof. Ruff has written: “This divergence between private
and social costs is the fundamental cause of pollution of ali types... .”
Larry E, Ruff, “The Economic Common Sense of Pollution,” The Public Inter-
est (Spring, 1970). Other important studies that advocate private ownership
and property rights as the approach to solving the pollution problem are
Edwin Dolan, TANSTAAFL: The Economic Strategy for Environmental
Crisis (New York: Holt, Rinehart and Winston, 1971); (Tanstaafl stands for
“there ain't no such thing as a free lunch,” and is a basic slogan for the
anarcho-capitalist movement, It was popularized by the science-fiction writer,
Robert Hetnlein.) J. H. Dales, Pollution, Property, and Prices (University of
Toronto Press, 1968); T. D. Crocker and A. J.. Rogers, Environmental Eco-
nomics (Hinsdate, Ill: Dryden Press, 1971}; Murray N. Rothbard, “The
Great Ecology Issue: Conservation in the Free Market,” The Individualist
(Feb., 1970), published by the Society for Individual Liberty, Philadelphia.
One of the first studies to argue in this fashion was the RAND Corporation's
Water Supply: Economics, Technology, and Policy, by Jack_Hirschleiffer,
James De Haven, and Jerome W. Milliman (University of Chicago Press,
1960), chap. 9. A highly technical introduction to the literature is E. J. Mishan,
“The Postwar Literature on Externalities: An Interpretative Essay,” Journal
of Economic Literature, UX (1971), 1-28, and the exchange between Mishan
and Dean Worcester: J. Econ. Lit., X_(1972}, 57-62. On the abysmal failure
of the State to control pollution, see Marshall Goldman’s study of the Soviet
Union, The Spoils of Progress (MIT Press, 1972).

2, Colin Clark, “‘Growthmanship’; Fact and Fallacy,” The Intercollegiate
Review (Jan., 1965), and published in booklet form by the National Associa-
tion of Manufacturers. On the dangers of government-sponsored growth, see
also Murray N. Rothbard, Man, Economy and State (Princeton: Van Nostrand,
1962), II, 837 ff.

3. Gary North, “The Theology of the Exponential Curve,” The Freeman
(May, 1970) [chap. 8, above}.

 
