Gertrude Coogan and the Myth of Social Credit 159

The authors report this startling fact, unparalleled in all of human
history:

During the entire experiment in Guernsey, from 1817 to date,
there has at no time been a threat of inflation from the creation
of States notes. At all times, the States were very careful in
the issue and cancellation of notes according to their ability and
requirements,1°°

According to whose requirements? The benevolent State, of course!
“Remember-—there is no inflation in Guernsey.”1°! Then we are
given a chart which proves just how badly the “uninflated” currency
was inflated.

1816 £6,000

1826 £20,000

1829 £48,000

1837 £55,000 minus £15,000 = £40,000

1914 £40,000

1918 £142,000

“Since that time, Guernsey has never looked back.”20?

1958 £542,765

In other words, in 140 years, half a million pounds of unbacked notes
were injected into the economy of a tiny island. This is “no inflation”
with a vengeance,

We read that the heavy inflation between 1914 and 1918 was
demanded by the war: “the demand for money was enormous.”105
This is the old fallacy; actually, the demand for goods was enormous,
and this made it appear as if the demand was for money. But money
does not win wars; war materials and civilian supplies win wars. The
government merely raised the money out of nothing, bought: goods
from the buyers, and those with no counterfeit bills went hungry. The
authors do not give any rough estimate of the rise in prices during
this era, naturally, That kind of information would blow their whole
appeal sky high. Government is again said to have the power of
God, It can create wealth at will, they believe. In reality, it cannot
create wealth; it can only redistribute it through taxation, either direct
or indirect (by monetary inflation).

It is interesting that the reply which the council of Guernsey gave
to the Privy Council is filled with all of the modern Keynesian and
socialistic fallacies, although in a far cruder form:

And thus it is, that the public works have not only given life and
activity to every species of industry by the immediate effects of
their utility . . . but and still more by the consequent impulse

100. Guernsey, pp. 11, 12. 102. Ibid, p. 11.
10k. Ibid., p. 16. 103. Ibid.
