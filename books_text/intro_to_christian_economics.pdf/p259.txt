Family, State, and Market 247

abolished the family as a legal entity until 1936. The conservative
would argue that Stalin was forced to return to at least a somewhat
conservative position with respect to the family because the very
nature of human society demands acknowledgment of this most
crucial of institutions. Without it, and the stability, meaning, and
purpose it brings to the lives of human beings, men cannot be
productive, and even the Communist State needs basic economic
production more than it needs ideological consistency, There is an
order that is built into creation that must be respected by men;
deny it, and you deny both order and man. Deny the legitimacy of
the family, and you deny the operational existence of human society.
That is why totalitarianism in its purest theoretical form cannot
exist over the long run or over the large geographical areas, for it
negates the possibility of social control when it negates the possibility
of society. This does not mean, however, that attempts to abolish
the family by totalitarians and radical anarchists (who work to-
gether initially to tear down the fabric of existing society) cannot
cause great social havoc. They can, and they have. They may again.

Socialism: Statist vs. Family

The family is the central law-making body in human society, not
the civil government. It is the structure which teaches children
original attitudes toward law, property, and other human beings.
It is the primary agency of social welfare, as. well as education.
Steadily, as the State appropriates the functions of the family, a
basic distortion of social life becomes manifest. Those social func-
tions that can best be ordered through the operations of a local,
highly personal, structure—-one which is basically voluntary at its
point of origin, ie., marriage—become totalitarian and inefficient
when appropriated by distant, politically controlled bureaucratic
hierarchies that use coercion to gain access to their economic
Tesources,

The family is essentially bureaucratic and socialistic in its internal
structure. For some reason, this fact seems to bother libertarians.
Some of them-—at least those conservative enough to defend the
family—actually try to deny the obvious. Robert Nisbet, the most
influential conservative sociologist in this country (and perhaps the
world——there are so few of them), writes in his book, The Social
Bond:

The difference between limited and total authority is perhaps
clear enough when we are considering liberal and totalitarian

political states. But the interest of sociology in these two types
of authority does not stop with the state. For there are several
