Gertrude Coogan and the Myth of Social Credit 147

have us believe that the collapse in value was due solely to the
privately printed counterfeits, The facts would indicate otherwise.
She fails to point out that between 1790 and 1796 the egal (in her
opinion, not in mine) State government of France issued a total of
45 billions of francs worth of unbacked (and, by my definition, coun-
terfeit) assignats, to be followed by an additional two and one half
billions in paper mandats. All of it was “legal tender,” and the State
coerced the populace into accepting them at their face, that is, their
specie valu This was inflation which was unparallelled in that
day, worse even than John Law’s vast flasco 60 years earlier. Coogan
ignores these facts. Issues like these are more easily sidestepped than
answered, Such mass inflation, in a day when “billions” were else-
where unheard of, is too damaging to her case that all duly elected
governments are monetarily trustworthy, even if the representatives
are radical revolutionaries and even Ilhuminists, as they were in
France.

True, counterfeiting—private counterfciting—did go on. Counter-
feiting of French notes went on in a scale almost as vast, perhaps
more vast, than did the counterfeiting by the French government
itself. One facet of this mass counterfeiting Coogan never chooses
to bring up. How much easier is it to print a counterfeit bill than it
is to mint a counterfeit coin? Wisely, she does not mention this very
obvious issue, She still continues to call for unbacked Treasury notes,
damning all supporters of gold as “gold bugs” or tools of the inter-
national bankers. Her own counterfeiting argument destroys her
case for “lawful” money, but she goes on, undaunted.

Consider this curious twist of logic. Point No, 1: “Did Mr. Baruch
explain that throughout the entire history of money, the only time
inflation, as he describes it, took place, was when the internationalists
wanted to destroy net only the value of the currency but also the
government of a country? Never has any government itself con-
ducted such an inflation.’*®" The incarnation of evil, the internation-
alists, alone practice mass inflation, in order to destroy a nation’s
government, Point No. 2: “These [French] assignats, as they were
called, were the money which financed the wars fought with other
nations by the revolutionaries.”*! Point No. 3, found in the very next
paragraph, has already been quoted, that the gigantic counterfeiting
operations (which are only conducted by the internationalists—Point
No, 1) that had their source in London were the sole cause of the

 

39. Andrew Dickson White, Fiat Money Inflation in France (Caldwell,
Idaho: Caxton, [1914] 1958), pp. 65-69.

60. Coogan, p. 93.

61. Ibid., p. 30.
