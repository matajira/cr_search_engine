The Intelligent Woman's Guide to Inflation 23

politicians who are elected by promising new welfare expenditures,
paukers who can make more profit-producing loans with the newly
created money, and those who supply the government with goods and
services. But others must pay for their benefits, The old rule holds
trie: no one gets something for nothing. Too many people are be-
guiled by the so-called “benefits” of inflation; as a result, they aban-
don their own common sense,

In the long run, no one profits from inflation. The process of in-
flation completely distorts the operation of the price system. The
purpose of the price mechanism is to indicate to all consumers and
producers just what the conditions of supply and demand really are.
What goad is a price system if it fails to tell us how much things cost?
Obviously, even an inflationary price system will tell us how much
things cost in paper money, but that is not what we are really con-
cerned with. What we want to know is how much we will have to give
up of one consumer good in order to obtain another. If a person goes
to New York City for a vacation, he must forego adding a room on
his house. If he sends his children to a private school, he cannot
afford a new car. An investment made in some high-risk new com-
pany keeps one from purchasing a “sure thing” blue chip stock.
These are the kinds of decisions we make every day. What we are
comparing are prices of things; money therefore helps us to make
very complex kinds of comparisons. But in tampering with the money
supply, we create distortions in our system of measurement. Prices
are now affected by the supply and demand for money, too, rather
than simply the supply and demand for goods and services. An abso-
lutely “neutral” money is impossible, but we can have reasonably stable
money. By inflating, we find ourselves in the position of a woman
who would measure the hem of a dress she wants to make when the
units of measurement in her pattern Keep changing.

Consider what happened in Germany in the early 1920's. Here in-
flation reached astronomical heights. Over 300 paper mills and 2,000
printing establishments were kept operating on 24-hour shifts just
to supply the national bank with its notes. Wives would go to
work with their husbands; when the paychecks or cash were dis-
tributed (wages were paid daily toward the end of the inflationary
period), the wives would take the paper and rush out to buy anything
they could: flowerpots, chairs, anything. By that evening, the paper
money was worth only a fraction of what it had been worth in the morn-
ing. On the final day of the inflation (November 20, 1923), one Ameri-
can dollar would purchase, at the official exchange rate, 4.2 ¢rillion
marks; the black market rate of exchange was a fantastic 11.7 trillion
marks per dollar, You can imagine what happencd to prices. A loaf
