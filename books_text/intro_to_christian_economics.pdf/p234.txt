222 An Introduction to Christian Economics

The ethical standard which Christ taught with regard to debt and
charity could not be said to be new. The heart of that teaching had
been provided by the Psalmist centuries before: “The wicked bor-
roweth, and payeth not again: but the righteous sheweth mercy, and
giveth” (Ps. 37:21). Hebrews were not to subsidize wickedness,
but they were a captive people, and under compulsion, so they were
told to be meck, In charitable activities, they were not to seek
profits. Christ never criticized moneychangers as such; what con-
cerned Him was the monopolistic privilege in the temple being
used by evil men to rob the faithful 3% He did not attack business,
but business masquerading as charity. The categories were to be kept
straight in the minds and lives of His disciples.

Finally, we have the case of the rich young ruler, one of the most
frequently encountered passages in medieval commentaries on wealth.
He is told by Jesus to gain perfection in his situation by selling all
that he possesses and giving to the poor. When he goes away de-
jected, Jesus pronounces his famous words, that it is easier for a
camel to go through the eyc of a needle than for a rich man to enter
the kingdom of heaven; nevertheless, “with God all things are pos-
sible” (Matt. 19:26b). It is the same old warning: wealth is
dangerous to a man’s soul, but some men who ate wealthy may con-
ceivably enter the kingdom, so long as they are godly stewards of
their wealth. Once again, it is the stewardship issue that is promi-
nent, as it is in so many of the parables, God, the master, requires
active, obedient, merciful activities from His servants.

The communism of the book of Acts has been the focus of heated
debates for centuries, The practice of selling goods and sharing
the proceeds with the brethren was limited to the Jerusalem church
and was strictly voluntary (Acts 5:4). It may have been the result
of the Jerusalem church’s expectation of the imminent destruction of
the city, as prophesied by Jesus.*4 The warning was clear: “Let him
which is on the housetop not come’ down to take any thing out of
his house: neither let him which is in the field return back to take
his clothes” (Matt. 24:17, 18). They were told to flee when the
armies approached the city. Therefore, there was little reason to hold
on to property, especially fixed property. In any case, communism
was never suggested as a general practice for all Christian com-
munities; as the orthodox wings of both Catholic and Protestant

33. Matt, 21:12, 13; Mark 11:15-17. Cf. my analysis of the incident with
the moneychangers in “Stewardship, Investment, Usury: Financing the King-
dom of God” [chap. 31, below).

34. Luke 21:20 ff; Matt. 24:15 ff
