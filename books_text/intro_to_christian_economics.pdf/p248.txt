236 An Introduction to Christian Economics

Ludwig von Mises has offered a theory of bureaucracy that pro-
vides us with another explanation of today’s inefficient firms. His
discussion complements Weber’s and improves upon it. Mises argues
in his little book, Bureaucracy (1944), that there are two primary
models of management: (1) the free market structure; and (2) the
statist burcaucracy. Both are necessary, he says; both perform valu-
able, but very different, functions. One form cannot be used to
perform the tasks more suited for the other. It is an unwarranted mix-
ture of the two categories, we can conclude, that has led to the
creation of a weakened free market.

The key difference between the two models is the difference of
finance. The question that we must always ask in assigning a task to
either is this: how does it receive its operating funds? If this is not
asked in advance, there will inevitably be created a system which
will not be able to do its job efficiently.

The profit management firm operates on an open market that per-
mits the entrance of competitive structures, Whatever profits it
makes or losses it sustains will be determined by its ability to satisfy
consumer demand. Assuming that it stays within the framework of
law established by the State, the only question that it must ask is
whether or not its income exceeds its expenditures. The free market
permits its bureaucratic structures to fail if they do not meet the needs
of the buying public, Thus, the top level of any bureaucracy has a
guide to the performance of the lower levels, especially with those
levels connected with sales: are they producing profits or losses?
Any bureaucracy must be hierarchical; the important differentiating
factor js the set of guidelines used by the top level to evaluate the
performance of the. lower levels.

The standard of measurement in this case delegates to the lower
levels considerable responsibility and therefore a more extensive
flexibility. The lower levels ate expected. to know the conditions of
supply and demand—the particular markets—-far better than bu~-
reaucrats at the top level can possibly know them. Thus there is
an integration of knowledge: the top level assigns the general goals—
products needed, aggregate estimates of expenditures and possible
Profits, the prospective operation of the company as a whole—while
the lower levels try to fulfill their basic responsibility, namely, to
turn a profit. If they do turn a profit, they are left alone by the upper
levels; if they fail, they can inform the upper Jévels of any corrections
needed at the top, or else they can be replaced. The free market

 

1969). Almost every issue of the University of Chicago’s Journal of Law and
Economics contains a study of some regulated, semi-monopolistic. indusiry.
Regulation usually favors the monopolies.
