46 An. Introduction to Christian Economics

the community (the miners, or those printing the money) at the
expense of another group (those on fixed incomes). Economics. as
such could never tel! us this, which should encourage us to re-examine
the presuppositions lying behind the highly inflationary recommenda-
tions of many of those enamored of the “new economics.”

Tf it is true that there is no way of supporting, through the use of
economic analysis, the idea that an increase in the money supply in
some way increases aggregate social benefits, then certain conclusions
will follow. For the sake of argument, let us assume that the supply
of paper dollars is tied, both icgally and in fact, to the stock of gold
in the federal government’s vaults. Let us assume that for each ounce
of gold brought to the government, a paper receipt called a “dollar”
is issued to the one bringing in the gold for deposit. At any time the
bearer of this IOU can redeem the paper “dollar” for an ounce of
gold. By definition, a dollar is now worth an ounce of gold, and
vice versa, What will take place either if an addition of new gold
is made by some producer, or if the government (illegally) should
print up a paper dollar? Rothbard describes the results:

An increase in the money supply, then, only dilutes the effective-
ness of cach gold ounce; on the other hand, a fall in the supply
of money raises the power of each gold ounce to do its work,
[Rothbard is speaking of the long-run effects in the aggregate.]
We come to the startling truth that it doesn’t matter what the
supply of money is. Any supply will do as well as any other
supply. The free market will simply adjust by changing the
purchasing-power, or effectiveness of its gold unit. There is no
need whatever for any planned increase in the mioney supply, for
the supply to rise to offset any condition, or to follow any artificial
criteria. More money does not supply more capital, is not more
productive, does not permit “economic growth.”

Once we have a given supply of money in our national gold system
(or wampum system), we no longer need to worry about the effi-
ciency of the monetary unit, Men will use money as an economic
accounting device in the most efficient manner possible, given the
prevailing legal, institutional, and religious structure. In fact, by
adding to the existing money supply in any appreciable fashion, we
bring into existence the “boom-bust” phenomenon of inflation and
depression. The old cliché, “Let well enough alone,” is quite ac-
curate in the area of monetary policy.

‘We live in an imperfect universe. We are not perfect creatures,
possessing ominiscience, ommipotence, and perfect moral natures.
We therefore find ourselves in a world in which some people will

2. Ibid.

3. CE. Gary North, “Repressed Depression,” The Freeman (April, 1969)
[chap. 6, below].
