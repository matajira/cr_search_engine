The Ethics of Monetary Hoarding 199

and gold (assuming they are legally able to do so) as foreign gov-
ernments are. A civil government can make its currency irredeemable
domestically, and for a time its citizens will not grow suspicious. If
the same government announces that its currency is irredeemable
internationally, foreigners are immediately suspicious, and for good
reason. If citizens were as suspicious of their own government
planners as the planners of other governments are, there would be
far less inflation in the world (and consequently fewer depressions).
Those self-righteous individuals who criticize domestic monetary
hoarding are suptemely trusting in the good will and good planning
of their State bureaucrats, In their view, the State’s money manip-
ulators are far more competent than the free market in the regulation
of monetary affairs, They would be wise to consider carefully the
warning of Professor Wilhelm Roepke, the chief theoretical economist
behind Germany’s post-war economic recovery:
‘The determination of whether the control of the quantity of
money should be submitted to the automatic forces of gold and
silver production or to the conscious decree of the government is
one of the cardinal problems confronting those entrusted with
the making of monetary policy and upon the answer to which
depends the choice of the particular monetary system in each
case. A liberal—one (in Europe) who puts his trust in eco-
nomic laws rather than in the whims of government—will gen~
erally opt for the tied or automatic standard. A collectivist—
one who is willing to trust the caprice of the government over
natural economic forces—will prefer the untied or manipulated
standard. Since, however, the linking of money to a precious
metal implies a much stricter contro] over the quantity of money
than can be expected from arbitrary government regulation, we
find that, paradoxically, it is the (European) liberal who, in
monetary matters at least, demands a discipline far stricter than
the collectivist.1
If money ceases to be scarce, it simultaneously ceases to be money.
Money, by definition, requires scarcity. Obviously, air will not func-
tion as money (unless it is in some environment in which it is scarce-—
on the top of Everest or in a submarine), This is why silver and
especially gold have served mankind’s monetary needs for so many
centuries. They are both in short supply, and their value is main-
tained, Paper is not very scarce. It is vastly easier for a government
printing office to print a million $100 bills than it is for a miner to
mine $100 millons worth of gold. Statists may blindly trust the civil
government in its handling of monetary matters, but historically
governments have proved unreliable in resisting the temptations to

1. Wilhelm Roepke, Economics of the Free Society (Chicago: Regnery,
1963), p.-100.
