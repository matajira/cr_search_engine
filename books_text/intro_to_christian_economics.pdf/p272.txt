260 An Introduction to Christian Economics

assumption of unrestricted knowledge and foreknowledge on the
part of the central planners, Needless to say, this assumption is
far from realistic. The stream of paper reports that flows from
the plants to the central authorities may belittle the majesty of
the Volga River, but it provides no assurance of real insight into
the conditions within the individual plant. The fundamental
ignorance of the central authorities restricts their ability to en-
force their will. Obversely, it is the knowledge of the manager
that assures for him his arca of freedom.*

In other words, the central planning of supply assumes the om-
niscience of the central planners. Without this omniscience, the sys-
tem is faced with overwhelming difficulties. The main one is that
to which Gerschenkron alludes: How can the on-the-spot knowledge
of the local manager be integrated into the overall central plan? Will
not the freedom to allocate scarce economic resources at one level
interfere with the planning activities of the other? This is the in-
escapable, inevitable, perpetual problem of the USSR’s economic
planners.

Despite the grandiose claims of the proponents of central planning,
the Soviet Union carries on only the most general aggregative plan-
ning at the center, Gosplan, the central planning agency, coordinates
the production of a few major products and services, In a frequently
quoted article, Herbert Levine has estimated that between cight
hundred and fifteen hundred commodities are planned totally at the
center. He outlines the planning process. The complexity is stag-
geting. First, a statistical analysis of the base period is made in the
first half of the planning year (in preparation, of course, for the
plan for the following year). A survey of the previous year is made
to gain at least a superficial aggregate estimate of what will be
needed (and possible) in the plan. As Paul Craig Roberts has
added, the individual firms present these forecasts to the central
planners, and therefore “the initiative lies essentially with the enter-
prises since they have better knowledge of their productive capacity.

. 8 Second, control figures are drawn up for a dozen or so of the
chief products and investment targets, These serve as guideposts for
economic units at lower levels. Third, and most important, is the
confirmation of the plan by the political hierarchy, and as might be
imagined, a great deal of political maneuvering takes place at this

4, Alexander Gerschenkron, Economic Backwardness in Historical Per-
spective (Cambridge, Mass.: Harvard-Belknap Press, 1962), p. 287.

5. Herbert S. Levine, “The Centralized Planning of Supply in Soviet In-
dustry,” Comparisons of the United States and Soviet Economies (Jot Eco-
nomic Committee, Congress of the U.S. 86th Congress, ist Session, 1959);
reprinted in Wayne A. Leeman (ed.), Captralism, Market Socialism, and Cen-
tral Planning (Boston: Houghton Mifflin, 1963), p. 55.

6. Roberts, p. 176.
