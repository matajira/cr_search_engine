The Ecenomics of Women’s Liberation 283

It is self-defeating. Of course, for those women already in the
system, the law would be an almost flawless grant of monopoly
returns,

Minimum Wage Law for Women

Inescapably, from the point of view of economic anaylsis, the
“equal pay for equal work” proposal is the demand for a minimum
wage law for women. The minimum wage would be equal to the
minimum pay scale for a man of comparable talents and respon-
sibility. Like all minimum wage laws, it is primarily a legally
operating barrier against all those worth less than the mioimum
wage. As shown in the earlier part of this paper, the woman initially
is worth Icss, not because of her lack of work, but because of the
higher risks and economic-institutional disutilities associated (im
the majority of American firms) with hiring women.

In general, minimum wage laws force the less productive, higher
tisk, less desirable (for whatever reasons) persons into lower paying
jobs not covered by the minimum wage laws. If the job market as a
whole is covered, then the laws tend to force them out of work
entirely. A person who generates only $1.25 worth of returns to
his company will not be hired if the minimum wage is $1.75. Those
least able to afford unemployment—the least skilled, least educated—
are the ones hurt most by the laws. In this country, as study after
study indicates, this means the Negro teen-age male, but it also
means the less skilled women. Those just entering the market,
with little experience and training, are the “first fired, last hired.”

Our WLF propagandists insist that housework is the intolerable
curse of the American woman, It is housework’s boredom and
lack of creativity that oppresses women, degrades them into beasts
of burden. That women would have to seek employment as house-
hold workers is, for the WLF, the ultimate example of male
chanvinism, So what do we find? The minimum wage laws have
been the most effective means of forcing more women into employ-
ment as household domestics!

Household employment is not covered by minimum wage laws.
As a tesult, those women who have been excluded from jobs in
the covered industries (since they are not allowed to compete by
bidding down wages) are now forced to seek less desirable em-
ployment. This means they must go to the uncavered industries.
It also means that more of them than would enter this market in
the absence of the laws now try to get in, thus forcing wages even
lower. Professor Yale Brozen of the University of Chicago made
