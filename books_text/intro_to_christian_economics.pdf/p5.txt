CONTENTS

INTRODUCTION

Part I: MONETARY THEORY coeaceseanenetee
The Biblical Critique of Inflation ..............
The Intelligent Woman’s Guide to Inflation
Inflation: The Economics of Addiction ....
Gold’s Dust... vee
Domestic Inflation versus International Solvency
Repressed Depression ..

The Fallacy of “Intrinsic Value”
The Theology of the Exponential Curve

 

 

Pen naupone

10. Fixed Exchange Rates and Monetary Cris

 

11. Gertrude Coogan and the Myth of Social Credit ..

Part Il; ECONOMIC SURVIVAL .
12. Price-Wage Controls: Effects and Counter-Effec

 

13. Inflation and the Return of the Craftsman ..0000....
14. Gresham’s Law Revisited: The Coin Crisis of 1964 ..

15. Making Money Qut of Moncey .
16. The Next Coin Crisis... .
17. The Ethics of Monetary Hoarding _........

Part III: CHRISTIAN STEWARDSHIP

  

 

AND RESOURCE ALLOCATION 00.0000.

18. An Outline of Biblical Economic Thought
19. Reason, Neutrality, and the Free Market .
20. Statist Bureaucracy in the Madera Economy .
21. Family, State, and Market .
22. The Crises in Soviet Economic Planning .
23, The Mythology of Spaceship Rasth
24. The Feminine Mistake: ‘The Feonomies
of Women's Liberation ..
25. Subsidizing a Crisis: The Teacher Glut... . ’
26. Urban Renewal and the Doctrine of Sunk Costs
27. Property Taxes: Sovereignty and
the Problem of Assessed Valuation

Vv

 

Downward Price Flexibility and Economic Growth ves

 

 

 
 
   
 
 

75
93
107

» 4

163

. 105

16%
178
183
196
198

ano

» 2il

225
242

add

_ 388

269
27
28x
301

308
