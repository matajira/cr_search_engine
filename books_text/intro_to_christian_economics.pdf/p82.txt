70 An Introduction to Christian Economics

reasoning here, simply because this unexamined train of thought is
familiar to us, can be disastrous.

Take, for example, the labor theory of value. Classical economics
—by which we mean that body of economic thought which was in
vogue from the time of Adam Smith (1770's) until the marginalist-
subjective schools arose (1870’s}—was confounded by the problem
of value, It proposed a cause-and-effect relationship between human
Jabor and value: abstract human labor (which itself was an abstract
concept derived more from mechanics than human experience) was
produced by laborers on their jobs; this abstract human labor was
in some way embodied in the products of that labor, and this is the
source of all value. Certain inescapable problems arose under this
presupposition. Why did selling prices fail to correspond to the total
payments made to labor? How did the phenomenon of profit appear?
What was the origin of interest? On a more concrete level, why did
an uncut diamond bring a higher price on the market than an in-
tricate mechanism like a clock? They could explain the disparity of
selling prices of jewels and selling prices of clocks in terms of supply
and demand, but their labor theory of value never fitted into this
explanation. It was an extraneous issue.

Karl Marx was the last major economist to hold to the labor theory.
In this sense, he was the last of the great classical economists. He
wanted to demonstrate that capitalism, by its own internal contra-
dictions, was doomed to a final destruction. Unfortunately for Marx’s
predictions, what he regarded as a basic set of contradictions of
capitalism was merely a set of contradictions in the reasoning of the
classical economists. He confused a faulty explanation of the capi-
talist process with the actual operation of the capitalist system.
Tronically, Marx fell into a pit which he always reserved for his
enemies: he looked not at the empirical data as such, but at an
interpretation of the data—not at the “substructure” of the society,
but at the ideological “superstructure.” Das Kapital was published
in 1867; by 1871, the marginalist assault had been launched by Carl
Menger of Austria and W. S. Jevons of England. The labor theory of
value which had undergirded Marx's whole analysis of capitalism
was destroyed, When Boehm-Bawerk, the Austrian economist who
was to gain fame as Menger’s most rigorous disciple, offered his
criticisms of Marx in 1884 (and again in 1896), it was clear (to
nop-Marxists, anyway) that the Marxian framework had gone down
with the classical ship.

1, Cf. Gary North, Marr's Religion of Revolution (Nutley, N. J.: The
Craig Press, 1968}, chap. 5, esp. pp. 155-170. See also Dean Lipton, “The Man
Who Answered Marx,” The Freeman (Oct., 1967).
