Reason, Neutrality, and the Free Market 229

Hayek’s now famous essay, “Why I Am not a Conservative,” which
first appeared in his Constitution of Liberty (1960), makes an im-
portant observation:

Conservatism proper is a legitimate, probably necessary, and cer-
tainly widespread attitude of opposition to drastic change. It has,
since the French Revolution, for a century and a half played an
jmportant role in European politics. Until the rise of socialism
its opposite was liberalism, There is nothing corresponding to
this conflict in the history of the United States, because what in
Europe was called “liberalism” was here the common tradition on
which American polity had been built; thus the defender of.the
American tradition was a liberal in the European sense.
American constitutionalism prior to the Civil War was pluralistic,
decentralist, and essentially liberal. J. Allen Smith, writing in his
study, The Growth and Decadence of Constitutional Government
(1939), makes the following distinction:
The basic conception of the old political order was not the divine
right of kings, but the sovereignty of God, The assumed divine
right of the temporal ruler was not an essential part of this doc-
trine. Divine sovereignty, as envisaged in the Christian theory of
the world, was simply a conception of God as the ultimate source
of authority. Direct human intermediaries, such as pope or king,
were purely adventitious features of this belief.
Thus, Smith concludes, the “ultimate sovereignty of God precluded
the idea that any human sovereignty could be unlimited.” For Ameri-
ca, this was doubly true, for its tradition after 1776 was Protestant,
dissenting, and anti-monarchical, Law was founded in terms of a
higher law——a religious law, a historical fact which stands as the
central thesis of Rushdoony’s important revisionist work, This Inde-
pendent Republic (1964).

The American constitutionalists operated within the framework
of a culture which was predominantly Christian, although some of
them, like John Adams and Jefferson, held moderately unitarian or
deist ideas in their private lives. All of them saw that civil society
without law is inoperable, and all law, they realized, is ultimately
founded on unneutral religious and moral presuppositions? The
appeal made by libertarians to a neutral, universal human reason

 

totalitarian political parties that claim their rights under the laws of free speech
is aad Lippencott, Democracy’s. Dilemma (New York: Ronald Press,

9. Conservatives from Aquinas to Burke believed in a universal “right rea~
son” of Natural Law, but this was in no way analogous to the rigid, secular
a@ priorism of Enlightenment thinkers. Cf. Louis 1. Bredyold, The Brave New
World of the Enlightenment (Anno Arbor: University of Michigan Press, 1961),
On the American view of reason, see Alice M. Baldwin, The New England
Clergy and the American Revolution (New York: Ungar, [1928] 1958), chap.
2; Adrien Koch, “Pragmatic Wisdom and the American Enlightenment,” Wil-
liam & Mary Quarterly, XVIII (1961), pp. 313-329, esp. concluding remarks.
