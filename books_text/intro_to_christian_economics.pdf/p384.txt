372 An Introduction to Christian Economics

connection to the institutional church. They were not subject to the
competitive pressures of a free market in money exchange. They were
shielded by the name of God. By so using God’s name they dishonored
Him. Monopoly profits are not to be earned in this way.

We can only surmise that the rates of exchange were unfavorable in
comparison to rates available outside the temple court. We can only
acknowledge the fact that the power to reap monopoly economic
returns is one which is unlikely to be ignored over long periods of time.
Again, we can only surmise that the moneychangers turned a portion
of their profits over to the temple authorities. It would seem reasonable
that temple authorities would demand a cut of monopoly returns that
had their origin in the very aura of the temple. It is possible that the
moneychangers were even salaried employees of the temple. But what-
ever the concrete economic arrangements, Christ’s words made their
position in God’s eyes quite clear: “It is written, My house shall be called
the house of prayer; but ye have made it a den of thieves” (Matt. 21:13).
The dove salesmen, the moneychangers, those who bought, those who
sold: Jesus cast them all out of the court. Such economic transactions were
an abomination. The house of God had its support in the tithes and
offerings of His people; He did not sanction huckstering in His name
as a means of increasing “holy” revenues.

The institutional church is the means of preaching the gospel, dis-
ciplining the. saints, and administering the sacraments. It is quite
openly a spiritual monopoly. It is ##e monopoly in men’s affairs.
Christ made it clear that this position of monopoly is not to be ex-
ploited by men for their own profit. Payment to God’s ordained
servants in the service of the institutional church is for services rendered.
Economic returns to the church are not to be in terms of the principle
made famous in. Frank Norris’ book, The Octopus: “All the traffic
will bear.” The institutional church is not a business—not a money-
changing business, not a bingo business, and not an insurance business.
It is the house of prayer.

Men who come in the name of the Lord and who claim the preroga-
tives granted to His ordained must be scrupulously careful to distinguish
their profit-making callings from their service callings. Paul was a
tentmaker. He did not use his position as an apostle to reap monopoly
returns from the brethren. He did not market his wares under the
auspices of the local church, charging a price higher than the market
price because he was ordained. He kept his trade because he desired
to relieve the institutional church of the economic burden of supporting
him, not because he intended ta set up Apostolic Tents, Inc. (available
only at your local church).

The institutional church and its related institutions possess legitimate,
