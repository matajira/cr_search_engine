Chapter XXVIL

PROPERTY TAXES: SOVEREIGNTY
AND THE PROBLEM OF ASSESSED VALUATION

[This essay should not be regarded as a defense of the idea
of either the property tax or eminent domain. But it does
Point out the flagrant arbitrariness allowed to tax assessors
under the present system. That arbitrariness should be re-
moved. Property taxes have been with us since the days of
the Puritans, and a survey of the records of their towns and
the colonies will indicate that the tax has always been arbi-
trary. Anyone who doubts this should read the book by
Holmes F. Crouch, How to Fight Taxes, published by Astro-
nuclear Press, 217 W. Julien St., San Jose, Calif. The bibli-
cal view of taxation is sketched by Rushdoony, “Taxation,”
Christian Economics (April, 1972).]

Practically, the general property tax as actually administered is
beyond all doubt one of the worst taxes known in the civilized
world. Because of its attempt to tax intangible as well as tangible
things, it sins against the cardinal rules of uniformity, of equality
and of universality of taxation, It puts a premium on dishonesty
and dehanches the publie conscience; it reduces deception to a
system, and makes a acience of knavery; it presses hardest on
those least able to pay; it imposes double taxation on one man
and grants entire immunity to the next. Jn short, the general
property tax is so flagrantly inequitable, that its rctention can be
explained only through ignorance or inertia. It is the cause of
such crying injustice that its alteration or its abolition must be-
come the battle ery of every statesman and reformer,

E. RB. A. Seligman

Essays in Taxation (1931), p. 62

Chief Justice John Marshall, in one of the key judicial decisions
of his career, M’Culloch v. Maryland (1819), laid down this princi-
ple: “That the power to tax involves the power to destroy... .” This
is hardly the kind of language that one might expect from a high
official in the federal government. The historical circumstances in-
volved in this case may help to explain such an anomaly. The state

of Maryland attempted to place a tax on the notes of all banks not
chartered by the state, Mr. M’Culloch, a cashier of the branch

308
