Financing the Kingdom of God 375

revenues, but which are in fact disiributing revenues in the name of
God. Stewardship is undoubtedly involved in the support of the church
and its appendages, but it is the stewardship of charity rather than the
stewardship of a profitable calling. To transfer the concept of the
stewardship of business to that of charity, thereby justifying usurions
joan contracts, is nothing short of blasphemy. He who accepts such
a loan is as guilty as he who offers it.

The request for loans to support the work of the church is legitimate
in times of emergency, just as the appeal is valid for an impoverished
individual. It is not legitimate otherwise. But an appeal for a loan at
interest is always usurious, always immoral, and always under the
curse of God, if it is made in terms of the need for charity, Men can
shut their eyes and stop up their ears, but that is what the Bible af-
firms. There is no escape from the truth; God only delays the
judgment,

The issue of the so-called “life-income contracts” is more difficult to
assess, at least for the person who has not had some training in eco-
nomic theory. Consider the booklet published by “Charitable Giving
Publications” and distributed by a leading conservative seminary and
the Bible Study Hour. It is written by Robert Sharpe; the seminary
version is dated 1967, and the Bible Study Hour version is dated
1968. It presents the case for “gift annuities.” These contracts involve
the payment of a lump sum in cash; the institution uses these funds, and
it pays the investor a stated annual return in dollars until he dies. On his
death, the remaining money in the fund (if any) goes to the surviving
husband or wife or else it goes to the institution, depending upon the
type of contract signed. We are informed by the booklet;

‘You accomplish two main purposes with a gift annuity.
FIRST... You make a gift to a charity, an educational or other
charitable organization. Such organizations must meet certain
qualifications for your gift to offer you the tax advantages dis-
cussed later.

Second . . . You are providing yourself with a regular and assured
income.

Both statements are misleading. A gift is a present sacrifice made
to a charity or a person without any thought of return. It is not the
same, morally or legally, as a legacy after death (as any tax collector
will be careful to explain). A gift involves the sacrifice of the living
donor, not the sacrifice of his surviving relatives. That, at least, is the
Biblical idea of a gift, An investment is not a gift, cither. For ex-
ample, an annuity may be taken out with a commercial insurance
company, a fact admitted in the brochure. Is this to be regarded as
a “gift” to Prudential or John Hancock? Is it not rather a form of
