 

Fixed Exchange Rates and Monetary Crises 109

mean? So far, no one has even been able to define a Eurodollar, let
alone explain how it works; or if someone can, no colleague agrees
with bim.*

The Keynesian economist simply does not trust the free market’s
unhampered price mechanism to clear itself of supplies of scarce
economic resources. Thus, we need fiscal policy, fine tuning of the
economy, econometric models, data gathering on a massive scale, and
controls over the money supply, Especially controls over the money
supply. Naturally, certain flaws appear from time to time: a $1.5
billion predicted surplus for fiscal 1970 became a $23.3 billion
deficit, but what’s a few billion dollars among friends? We owe it to
ourselves, right? A private firm, unless it has a cost-plus government
contract, would not long survive in terms of such a woefully inefficient
economic model, but what do businessmen know about economics,
a faithful cconometrician may ask? If reality does not conform to
the model, scrap reality, by law.

So reality is scrapped, and the Keynesian finds it necessary to aban-
don one more area of market freedom, namely the freedom of private,
voluntary international exchanges of money at prices established by
the market. Such a voluntary system of international exchange would
reduce the predictability of the government’s econometric model. It
would allow a “bleeder” in the overall device. It would aliow men
to measure the extent of the depreciation of their awn and others’
domestic currencies, thus calling attention to the policies of in-
flation and confiscation being enforced by their governments and other
governments. As for the United States, floating exchange rates on a
free international market for currencies would end, overnight, the
exported inflation of our continual budgetary deficits.* That is why
government bureaucrats do not generally approve of floating ex-
change rates.

This does not explain why various conservative economists also
oppose the extension of the market into the realm of international
monetary exchange. The late Wilhelm Roepke called the idea “a
counsel of despair.” His argument against flexible exchange rates:
“Without stability of exchange rates any international monetary sys-
tem would be flawed at an important point, because it would lack a
major condition of international economic integration.” This sounds
plausible enough, until one reads his next sentence: “Just how im-

2. Business Week (September 25, 1971), p. 91 ff.

3. On exported inflation, see Wilhelm Roepke, “The Dollar as Seen from
Geneva,” National Review (March 8, 1966); Against the Tide (Chicago: Reg-
nery, 1969), chap. 13: “The Dilemma of Imported Inflation.”

_ 4. Against the Tide, p, 229. On market-imposed (cather than politically
imposed) fixed exchange rates, see above, p. 48.
