The Theology of the Exponential Curve 89

tary exchange precisely because he wishes to avoid such crises. But
in order to be consistent with this goal, he is forced, by definition, to
give up the philosophy of perpetual economic growth. No firm can
continue ta expand forever, no national economy can continuc to ex-
pand forever: here is the essence of the free market position, Private
economic competition is to replace aggregate economic collapse.
The free market should sever be defended, as one captain of
American industry tried to defend it, in terms of the slogan, “No
jimit on tomorrow.” In a world of scarce resources there is always
a limit on tomorrow. We dare not write as this man wrote in 1955;

Tam convinced that there will be no limit on tomorrow, and that
our future will be more exciting, more thrilling than any of the
periods of progress we have yet experienced. There will, of
course, be peaks and valleys as the forces of supply and demand
continue to exert themselves, But I am confident that the Ameri-
can people will never satisfy their desires for better living, and
that our technology will never cease to accelerate and expand.

This philosophy is far closer to Hegelianism than it is to the moderna
defense of the free market, It should be left for Soviet theoreticians
to play with; defenders of capitalism, given their presupposition of
scarcity, dare not use it. Hardin is correct when he writes that “the
myth of inevitable technological progress in a laissez faire world” is
fallacious.** We must give up the myth of perpetual, irreversible
technological progress if we are to preserve the theoretical validity
of free market voluntarism.

The theology of the exponential curve has led to what Harlan
Cleveland has called the “revolution of rising expectations.” Men
begin to believe the promises for an expanding utopia, and when the
planners cannot deliver, they turn in fury on those who promised too
much, They tear down that systern which promoted that myth. If we
are to avoid this, we must be careful to defend the free market in
other terms. First, the market system provides men with a sphere of
freedom and personal responsibility in which to exercise their talents,
dreams, and faiths. Second, it provides the most efficient mechanism
known to man for the production and distribution of wealth, given
the limitations of scarcity. Third, by making men responsible as
individuals for their actions, and by imposing penalties for failure on
individuals rather than on whole economics, the market tends to
smooth out crises, thereby enabling more peaceful transitions ‘to
newer economic conditions, The market promotes social peace.

Statist propagandists have committed us to the theology of the

34, Garrett Hardin, “Not Peace, But Ecology,” Diversity and Stability in
Ecological Systems (Brookhaven Symposia in Biology, No. 22, 1969}, p. 154.
