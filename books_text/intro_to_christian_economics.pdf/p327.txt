Property Taxes: Sovereignty and Assessed Valuation 315

someone will call for progressive taxation, and someove else will deny
this on the same grounds that progressive (graduated) income taxes
are denied, but the basic procedure will remain the same, with or
without graduated taxes.

What the county tax assessor must be certain of is that the figure
submitted by each taxpayer is the latter’s true estimate of the value
of his property. It is at this point that the element of coercion would
have to be applied. No tax structure can exist without coercion; the
problem for Christians is to discover and apply the best means of
making this coercion correspond with the needs of individual freedom
and responsibility. It is possible to imagine at least three methods
of applying coercion within the framework of a system of taxpayer
estimation of property value,

First, the county could require that all estimates. be made a part
of the public record. Anyone wishing to purchase the Jand or home
or factory for the price stated by the taxpayer would be permitted to
do so, In other words, the owner would be compelled to sell. This
proposal would favor members of minority groups who wish to move
into a “restricted” neighborhood. It would also be advantageous
to large-scale land developers or factory managers who desire to
purchase land or homes in large blocks, while avoiding the phenome-
non of increasingly higher prices being demanded by the remaining
members of a neighborhood for their unsold plots, One lump sum
could purchase them all. The defects of such a system are obvious.
It would involve a subsidy to the developers, since land which would
normally have to be purchased bit by bit, thus forcing up the actual
value of the remaining plots, could now be purchased for a fower
aggregate price. It would also tend to upset prevailing patterns of
community ownership that are based on non-monetary factors. Per-
haps the worst aspect of this plan would be the requirement that
all estimations would be made public, an invasion of privacy that
might not be warranted by the advantages of owner-estimated
taxation.

Second, the county could purchase a fraction of the property in
the community, on a purely random basis, and then offer the plots
for sale at an auction. This would frighten owners into submit-
ting true estimates of property value. Also, any person who bad,
by accident, overvalued his own property, could then take the
money paid to him by the county, repurchase his property for less,
and pocket the difference. This plan, too, has defects, though prob-
ably not so glaring as the previous one. The civil government is
seldom to be trusted to exercise its massive powers in a purely ran-
dom fashion. Even with computers operating with random numbers,
