 

viii An Intreduction to Christian Economics

attempts to Iegislate away these limitations will reap the built-in
whirlwinds.

But why has God cursed us? Because on the day man alienated
himself fram his creator, he alienated himself from himself and from
other men. Adam chose to rebel ethically from God. He brought
death into the world (Rom. 5:12}. All those who hate God love
death (Prov. 8:36). Mankind necds external restraints to restrain
him in the working of evil. One of these is the division imposed by
language (Gen. 11). Another is the existence of legitimate higher au
thoritios (including, but not exclusively, civil government) (Rom. 13:
1-7). The marriage bond limits each individual’s autonomy (1 Cor.
7:4), The restraining hand of God limits men’s rejection of the
truth (John 9:4). The curse of the earth is the means of forcing
men ta cooperate with each other if they wish to increase their
wealth; the division of labar increases productivity in a world that
has limited resources? Furthermore, the universal climate which
existed prior to the flaod (mammoths are found in the ice of Siberia
and Alaska with tropical foliage still preserved in their stomachs?)
undoubtedly acted as a disincentive to trade, since geographical
differentiation, and therefore geographical specialization, were mini-
mized. By imposing cultural, linguistic, and national barriers on
men, Gou redueed their ability to cooperate in building the society
of Satan? But in separating them geographically, and by provid-
ing the varying external conditions of climate, God simultaneously
introduced incentives for cooperation, The economic burdens of
isolation and war were therefore directly increased. Thus, the balance
between the one and the many—total political unification and total
autarky and social atemism-—could be maintained? Scarcity came
as a direct result of the original human rebellion; the foundation of
geographical specialization (at least with respect to climate) was

 

1, On the relationship between scarcity and economic cooperation, see
Frank HL Knight and ‘Thornton W, Merriam, The Economie Order and Re-
lizion CLondon: Kegan Paul, Trench, Trubner & Co, 1948), pp. 78-79. ft is
ironis that this observation was made by Knight, the atheist, “rather than Mer-
lam, the religious Hberal,

2. Donald Patten, Phe Biblical Floud and the Ive Age Epoch (Grand
Rapids: Haker Rook House, 1967), discusses the mammoths at some length,
Cf Henry M. Mortis and Juhi C. Whitcomb, The Genesix Flood (Philadelphia:
Presbyterian and Reformed Publishing Co. 1961), pp, 281-291; Alfred Reh-

l, The Flond (St. Louis: Concordia, as, vhap. 35. Morris is correct
vhert he writes that the Hible i¢ a texthook of science: Studies in the Bible and
‘eicave (Philadelphia: Presbyterian and Reformed Publishing Co. 1966),
chap. 11. Christians who argue otherwise are philosophical dualists, intellec+
tually schizaphrenic, and theological compromisers,

3 R. 1. Rushdoony, “Che Saelety of Satan,” Don Rell Reports (Aug. 14,
1964),

4, RoI. Rushdoony, The One and the Many (Nutley, No i: The Craig
Press, 1971).

 

 

   

    
