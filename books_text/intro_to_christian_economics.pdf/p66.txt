54 An Introduction to Christian Economics

phrase, “freedom to pursue domestic economic policy,” invariably
means the freedom of the monetary authorities to inflate a nation’s
circulating media (currency, coins, and credit), The motivations
behind domestic inflation are varied; an important one is that the State
can raise its level of expenditure without imposing a corresponding
increase in the visible tax rate. Inflation, in short, is a form of in-
visible taxation, and those on relatively fixed incomes are the ones
who pay the tax; they must decrease their purchases of consumer
products and services when the level of prices rises.

But the primary economic argument which is used today to defend
an expansion of the domestic monetary supply is that inflation keeps
“effective demand” at high levels, that people with the newly created
money will buy more goods, and that businesses as a direct result
will be stimulated to increase production, Consequently, more peo-
ple will be employed by these firms.

Fundamental to this argument is the idea that the operation of the
free market is insufficient to insure employment for ali those who
desize. to work, Somehow, the market fails to dispose of all goods
offered for sale (through the unhampered action of the pricing
mechanism), and therefore the demand registered by purchases js
unable to encourage greater production, This perspective has been
common to most socialist parties, but it became a basic presupposi-
tion of modern nonsocialist thought through the teachings of John
Maynard Keynes in his General Theory of Employment, Interest
and Money (1936). Keynes realized that a downward revision of
the level of wages would be opposed vigorously by labor unions, and
the governments of most Western democracies would find such a
downward revision politically inexpedient. Money wages must not
be permitted to fall. However, if inflation were allowed to raise costs
and prices, real wages would fall without the organized opposition
of labor? It was clear that if real wages did not fall, the result would
be unemployment; the least productive workers would have to be
dismissed.

Keynes wrote during the depression, but an analogous situation
exists today. The structure of minimum wage laws creates a similar
problem: the low production worker would lose his job were it not
for the fact that governments are permitting real wages to fall (at
least in comparison to what the wages would be in the absence of
inflation}. Minimum wage laws have, in effect, made inflation a
political necessity. Eventually, the misallocation of scarce resources
promoted by inflation will harm both the laborers and the manu-

2. See the analysis of Keynes’s position by Murray N. Rothbard, Man, Econ-
omy and State (Princeton: Van Nostrand, 1962), Il, 683-687.
