Property Taxes: Sovereignty and Assessed Valuation 319

government to confiscate property (apart from taxation and asso-
ciated penalties). If this were done, the State could still obtain the
land it desired, but it would have to pay what the owner wanted in
exchange. By making continually higher offers, the State could force
onto the owner increasingly higher taxes. An individual owner would
have the right to refuse to sell, but it would cost him to make the
decision. On the other hand, the State would have to pay what the
owner demands, not what some State bureaucrat determines is the
“fair price.” Even if the right of eminent domain were retained by the
State, there would now be an official statement by the owner—an
estimate accepted as a legal statement by an agency of the local
civil government—with regard to the value of his property. There
would be tess tension, fewer reprisals, fewer court cases, if that figure
were established as the legal minimum that the State could pay as com-
pensation for its confiscation. It would also reduce the possibility
of that bugaboo of all “eminent domainists,” namely, the increasingly
higher prices demanded by owners of property at the very end of
the State’s building project, e.g., the last mile of the freeway. The
State could pay at one time for all the land on its proposed route;
holdouts would not be in a strong position to defend themselves,
yet they would be able to receive the price they considered fair.
There is still a degree of coercion, but at least the market, rather
than a State bureaucrat, would impose the pricing parameters.2°
There would be another effect of my proposal, Members of
minority groups would now be able to remind those dwelling in
“restricted communities” of the costs associated with their unwilling-
ness to sell to those trying to escape from some ghetto area, The in-
dividual would still retain his right to refuse to sell, but if the price
offered were greater than the owner’s declared assessment, he would
now have to pay for his decision. To the extent that a ghetto is the
product of statist interference with the free market—rent controls,
zoning laws, ete—this would point the fact out to the taxing au-
thorities, The present property tax system, to the extent that it
ignores the market’s assessment of the value of a particular piece of

10. The State in this case would have a competitive advantage over private
land developers. Civil governments can generally amass. more wealth through
coercive taxation for a single project than corporations can through voluntary,
competitive investment. The State can more easily afford the huge payment
needed to purchase an entixe unit of property in one lump sum. The right of
eminent domain would obviously permit the State to make a promise to pay
at a point in time, thus escaping the problem of rising land values due to the
very existence of the project. This, of course, involves a subsidy by owners
(who Jose their ability to bargain for higher payments) and the county (which
loses tax tevenues) ta the state or federal government agency buying the land.
On the whole question of eminent domain, see Rushdoony, Politics of Guitt
and Pity (Nutley, N. J.: The Craig Press, 1971), pp. 325-330.
