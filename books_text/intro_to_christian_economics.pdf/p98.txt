86 An Introduction to Christian Economics

Economists who see economic growth as the national goal of eco-
nomic policy are pursuing a demon. It necessitates demonic means
to sustain such growth in the aggregate. Ours is a limited world with
limited resources. Aggregate production figures cannot grow in-
definitely. (You will notice how the language of material progress
and material finttude eventually brings us to an impasse.) In order
for exponential growth of aggregate output over long periods of time
to occur, the universe would have to be infinite, which it is not.

Economists are not all fools. They know that in theory there can
be no such thing as continual growth. Nevertheless, macrosconomists
are rather like the president of some company. He knows that
fis firm cannot grow forever, just as they know an economy cannot
grow forever. But they all hope for just a little more growth to-
morrow. Admittedly, they all affirm, growth has limits, but maybe
Wwe can squeeze out more growth next ycar, even though some far-
distant year may bring the dream to an end.

If the economists were willing fo sit back and watch the economy
grow or not grow, according to the dynamics of small-scale micro-
economic activities, there would be no problem. Unfortunately, this is
not the case, Present-day Keynesians, like their mentor, and the in-
Hationists at the University of Chicago, like their intellectual leader,
Milton Friedman, combine to recommend policies of monetary ex-
pansion. The constant expansion of the money supply (as Friedman
recommends) or the “fine-tuning” of the economy by varying the
level of deficit federal expenditures (neo-Keynesians’ hope) would
ultimately require massive doses of new money each year. Yet these
programs are advocated because they would supposedly “stimulate”
the output of goods and services by an otherwise “stagnant” free
market economy. What is acknowledged as being theoretically
impossible—the constant growth of an economy over the long run—
becomes an emotional, dogmatic commitment of economic planners
who want to use monetary inflation as the means of achieving aggre-
gate economic growth, year after year, decade after decade. Mone-
tary inflation is expected to achieve the impossible, simply because
the impossible is attempted by discrete, yearly “growth models.”

Mises is correct: we have here a philosophy of “stones into
bread.”*° What Christ regarded as a demonic philosophy (Luke

30. Ludwig von Mises, “Stones into Bread, the Keynesian Miracle,” in
Henry Hazlitt (ed.), Critics of Keynesian Economics (Princeton: Van Nos-
trand, 1960). For criticisms of Friedman, see Hans F. Sennholz, “Chicago
Monetary Tradition in the Light of Austrian Theory,” in Toward Liberty
(Menlo Park, Calif.; Institute for Humane Studies), Ul, 347-366; Murray N.
Rothbard, “Milton Friedman Unraveled,” The Individuatist (Feb., 1971), esp.
pp. 5-6.
