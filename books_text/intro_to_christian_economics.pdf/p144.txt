132 An Introduction to Christian Economics

persons could control the supply of money. “What legitimate right
have a privileged few to alter the total volume of U.S. money either
up or down?” This third lecture is really a confused piece of logic,
In the first place, gold does not have to be declared to be money. It
is alteady money, by usage and private custom alone. Secondly, the
Sovereign Scal is not needed to make it money. Third, gold miners
do add money to the nation’s supply, for all the gold not going for
industry and ornament will wind up in the money supply.* Finally,
there is nothing morally or economically wrong with gold miners
being permitted to sell a produced good on the open market if they
so desire. If no one wants the gold, then he does not have to buy any,
that is, he need not part with any of his own goods to obtain the gold.
The question of the State seal is wholly superfluous; the seal only
certifies that the coin really is of the weight and fineness that its
bearer declares it to be.

Money, it must seem clear by now, is a highly marketable good
only because individual acting citizens find it useful to them in trade.
The question of money, therefore, is intimately linked to the problem
of value, So far, we have seen value presented as a subjective, per-
sonal decision of individual men and women. What is Miss Coogan’s
view on the subject?

“Value,” she writes, “is not intrinsic to commodities and services”
(No. 3). This is correct; there is no “value substance” residing in a
material good,’ Yet we know that some things are valuable, so from
whence does this value stem? Here Coogan offers a most befuddled
attempt to explain value, one of the most confused explanations
in all of economic literature. It is completely meaningless: “There
is no source of value any more than there is a source of distance.”
But if value is neither inherent in commodities nor derived from
somewhere else, how can it exist? She does not even see the contra-
diction, let alone try to answer it. “Value can be measured only
by comparison. Comparison cannot be between two or more objects,
but must be between two or more Values.” This is sheer gibberish.
She thinks that by capitalizing the word she has somehow unlocked
some mystical door to truth. But the question immediately ariscs,
how can we measure these Values (capitalized) if we cannot find
them? They are not in the goods, they are not from some source, and
we cannot measure the goods or compare the goods themselves.
Then what is value, why is it, where is it, how is it found in order to
measure it? No answers from Miss Coogan, just this statement:

6. Cf. North, “Gold’s Dust,” The Freeman (October, 1969) (chap. 4, above].
7. North, “The Fallacy of ‘Intrinsic Value,” The Freeman (June, 1969)
[chap. 7, above].
