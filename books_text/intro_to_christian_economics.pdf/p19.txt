The Biblical Critique of Inflation 7

mixed with water; and kings are not to consort with wicked men.
This is concrete preaching.

Currency debasement is the oldest form of monetary inflation.
It is not surprising that Isaiah should, in the same verse, refer to the
debasement of silver and the debasement of wine, Monetary in-
flation is very often accompanied with a disastrous fall in the quality
of economic goods, especially in the last stages of the inflation.
Professor Rothbard has described this interrelationship, and it is
a grim picture:

To gauge the economic effects of inflation, let us see what hap-
pens when a group of counterfeiters set about their work, Sup-
pose the economy has a supply of 10,000 gold ounces, and coun-
terfeiters, so cunning that they cannot be detected, pump in
2,000 “ounces” more, What will be the consequences? First,
there will be a clear gain to the counterfeiters, They take the
newly-created money and use it to buy goods and services. In
the words of the famous New Yorker cartoon, showing a group
of counterfeiters in sober contemplation of their handiwork:
“Retail spending in the neighborhood is about to get a needed
shot in the arm.” Precisely. Local spending, indeed, does get
a shot in the arm, The new money works its way, step by step,
throughout the economic system. As the new money spreads,
it bids prices up——as we have seen, new money can only dilate
the effectiveness of each dollar, But this dilution takes time and
is therefore uneven; in the meanwhile, some people gain and
others lose. In short, the counterfeiters and their local retailers
have found their incomes increased before any rise in the prices
of the things they buy. But, on the other hand, peaple in remote
areas of the economy, who have nox yet received the new money,
find their buying prices rising before their incomes. Retailers at
the other end of the country, for example, will suffer losses, The
first receivers of the new money gain most, and at the expense of
the latest receivers.

Inflation, then, confers no general social benefit; instead, it re-
distributes the wealth in favor of the first-comers at the expense
of the laggards in the race. And inflation is, in effect, a race—to
see who can get the new money earliest, The latecomers—the
ones stuck with the loss—are often called the “fixed-income
groups.” Ministers, teachers, people on salaries, lag notoriously
behind other groups in acquiring the new money. Particular
sufferers will be those depending on fixed-moncy contracts—-con-
tracts made in the days before the inflationary rise in prices.
Life-insurance beneficiaries and annuitants, retired persons living
off pensions, landlords with Jong-term leases, bondholders and
other creditors, those holding cash, all will bear the brunt of the
inflation, They wilt be the ones who are “taxed.”

Inflation has other disastrous effects, It distorts that keystone of
our economy: business calculation, Since prices de not all change
