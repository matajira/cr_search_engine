88 An Introduction to Christian Economics

that forecast, inevitably involves uncertainty.* The fact that loss may
occur with at least equal frequency with profit should indicate the im-
possibility of perpetual, guaranteed compound interest,

All firms cannot make equal profits all of the time; the very nature
of profit forbids the possibility of a universal guarantee of returns in
the agpregate. The man who expects such a universal compounding
of interest payments would have to take seriously the inevitable sub-
title: “assuming positive interest rates and no natural or man-made
catastrophe.” Population growth, if it continues, will create its own
catastrophe; the same is true of any policy of inflation used in order
to sustain the continuous payment of compound interest. In the
short run, an individual can “put his money in a bank and watch it
grow.” {n the long run, a whole population cannot. We must heed the
words of the biologist, Garrett Hardin:

Suppose, for example, that the thirty pieces of silver which Judas
earned by betraying Jesus bad been put out at 3 per cent interest.
Tf we assume these pieces of silver were silver dollars, the savings
account would loday amount to a bit more than 9 x 10%* dollars,
or more than $300,000 for every man, woman, and child on the
face of the earth. Since the real economic wealth of the world
is certainly much less than that amount, it would be quite impos-
sible for Judas’ heirs (all of us, I presume} to close out the ac-
count, The balance in the bankbook would be largely fictional.**

The figure would he fictional in the same way that the GNP figure
is fictional: the account could no more be closed out at one time than
the total GNP could be sold to a single buyer at one time. If a
market for the aggregate figure cannot exist then the “price” of the
aggregate goods in either case is fictional.*? Hardin’s conclusion is
televant to the theology of the exponential curve:

A modern William Paley, contemplating bank failures, embezzle-
ments, business collapses, runaway inflation, and revolutions,
might well argue that these catastrophes are examples of “design
in Nature,” for by their presence the.impossible consequences of
perpetual positive feedback are avoided.

Does this mean that those who Iavor the free market must be in
favor of “bank failures, embezzlements, business collapses, runaway
inflation, and revolutions”? Absolutely not. The advocate of the
free market favors the interaction of acting men in patterns of volun-

31, Frank H. Knight, Risk, Uncertainty and Profit (New York: Harper
Torchbook, [1921] 1964).

32, Garrett Hardin, “The Cybernetics of Competition,” in Helmut Schoeck
and James W. Wiggins (eds.), Central Planning and Neomercantilism (Prince-
ton: Van Nostrand, 1964), p. 65.

33. Cf. Henry Hazlitt, The Failure of the “New Economics” (Princeton: Van
Nostrand, 1966), pp. 410, 411, 418,

 
