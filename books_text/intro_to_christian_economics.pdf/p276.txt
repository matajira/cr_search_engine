264 An Iniroduction to Christian Economics

aggregate, in order to give the illusion of maintaining the ideology,
They achieve neither ideological consistency nor economic efficiency,
Peter Wiles and Leon Smolinski have drawn the necessary con-
clusion:

It is thus obvious from the administrative point of view that plan-
ling must be decentralized if it is to exist at all. It always has
been and still is: the center draws up a general skeleton and the
subordinate bodies put flesh on the bones... . Confining our-
selves still to economics, it is plain that such technical planning
disiderata as consistency and punctuality are compatible with,
even possibly favored by, decentralization,

One planning function, however, is very seriously disfavored: the
rational allocation of resources. From this point of view decision-
making should be either central or peripheral; a mixture is bad.

 

  

We see the inevitable, inescapable problem in operation: the
constant teusion between centralized, ministerial planning and local-
ized decision-making. The Soviet economic planners constantly shift
the locus of planning back and forth in their attempt to discover a
solution to their problem of administrative balance. As Gregory
Grossman says, “To put it schematically at the risk of oversimplifica-
tion: overcentralization, imbalance, and autarky are the three corners
of a triangle of hazards within which the Soviet-type economy seeks
to find an organizational solution.”!* One Sovietologist, Z. M. Fallen-
buchi, has pinpointed the issue: “Hence the perennial dilemma of the
Soviet economic organization: how ta decentralize some economic
activities without losing control over the economy and the possibility
of central planning.”27

The crisis has grown steadily more critical, “The authorities that
hand. down plans,” writes Alec Nove, “are often unaware of the tasks
already given that enterprise by other authorities.” He quotes a state-
ment made by J. Borovitski, a disgruntled enterprise manager, which
appeared in Pravda (Oct. 5, 1962}:

The department of Gosplan which drafts the production program
for Sovnarkhozy [regional economic councils] and enterprises is
totally uninterested in costs or profits. Ask the senior official in
the production program department in what factory it is cheaper
to produce this or that commodity? He has no idea, and never
even puts the question to himself. He is responsible only for the
distribution of production tasks. Another department, not really

15. Wiles and Smolinski, pp. 24-25.

16. Gregory Grossman (ed.), Value and Plan: Economic Catcutation and
Organization in Central Europe (Berkeley: University of California Press,
1960), pp. 7-8.

17." Z. M. Fallenbuchi, “How Does the Soviet Economy Function Without a
Free Market?” in Bornstein and Fusfeld (eds.), The Soviet Economy, p. 35.
