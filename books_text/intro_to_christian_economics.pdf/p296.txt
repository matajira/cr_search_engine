284 An Introduction to Christian Economics

a study of precisely this effect of the minimum wage laws in the
October, 1962 issue of The Journal of Law and Economics. He
surveyed the employment figures, before and after a rise in the
minimum wage law, in three different periods. His conclusion: “In
each instance when the minimum wage rate rose, the number of
persons employed as household workers rose.” He then made
this warning:
However, the coverage of the Fair Labor Standards Act has
been broadened, and further broadening is proposed. Much
further broadening will close the safety valve [i.c., the noncovered
industries into which the unemployed flee], We will, then, find
the amount of structural unemployment (i.e., unemployment
concentrated in certain age groups, in one sex, or race, in groups
of less than a given level of education, and in certain regions)
increasing as minimum wage rates increase.

This prospect, of course, applies only to the less desirable em-
ployees or potential employees. “For families with large numbers of
children [which can now employ cheaper servants] and women
employed in better paying occupations, further increases in minimum
wage rates and their coverage may be very desirable, however
unwelcome this may be to the less educated, less skilled female
worker foreclosed from a better paying job by the rise in the
minimum rate and coverage,”

Brozen is considering only the more familiar minimum wage law,
the kind which sets a fixed minimum wage per hour for all members
of the population in the covered industries. The WLF scheme is
not quite the same. What the “equal work for equal pay” scheme
would produce is a minimum wage law for all women throughout all
covered industries, from the secretaries to the female vicc-presidents.
It would not be limited to merely those employees im the $1.50. to
$2.50 per hour range. Instead of seeing only the bottom segment
of female employees forced to take less desirable positions, ie.,
those which the men would not be bidding for anyway, the WLF
proposal would see to it that all entering female employees would
be downgraded (except for the few token women hired for the
purpose of fending off a federal investigation}. There would be
a downgrading all the way along the employment ladder.

Companies would not outwardly break the law, of course, but
there are many ways to avoid regulations that are undesired by
personnel departments. For example, two applications are received:
a man holds a B.A. and a woman holds a B.A., and both seek the
same post. The woman had better be from a prestigious academic
institution or have had some kind of previous business experience,
