 

 

282 An Introduction to Christian Economics

will have to offer some special advantage to the company that her
masculine competitor cannot or will not.

I appeared on a Los Angeles television show in November of 1969,
Tt was one of those afternoon talk shows aimed at the “lunch
bunch”—a distinctly feminine audience. Preceding me was an ar-
ticulate, middle-aged lady from England, the founder of a female
labor exchange organization which supplies womanpower to various
corporations, By pre-1968 standards, she would have been con-
sidered a militant for women’s rights, As the director of this
multimillion-dollar organization (an even more remarkable feat
by British economic standards), she was asked what she thought
of the fact that women get paid less than men for their labor.
“Well,” she replied, “the best form of competition we women have
is our willingness to work at lower wages. If you were to eliminate
that, you would remove our most effective employment weapon.”
That woman understands the nature of competition,

The fact that the “equal pay for equal work” law is not fully
enforced makes it possible for a woman to obtain that initial access
to a previously masculine occupation. If she were to demand a
man’s wages initially, she would stand far less chance of gaining her
real objective, namely, the opportunity to prove her capacity in
the occupation of her choice. The company hesitates to hire a
woman, given the definite uncertainties in hiring women in general,
(Is she a Woman's Lib type? What is she after?) But if she can
offer the company a premium to offset the logical risks involved
(not to mention the questionable hostility), she can make it worth
the company’s risk. The most obvious premium is a willingness to
take a lower wage. If she should fail on the job, the company has
not lost so much,

By removing this most effective of weapons, the WLF would
seriously jeopardize the possibilities for advancement by women
into the higher echelons of American business. Only the most
obviously competent women, the ones from the best schools with
the highest grades and most impressive outside activities, would
have a shot at the better jobs. Actually, the WLF proposal borders
on the suicidal: certainly it would not be the WLF type who would
be hired unless she could show some overwhelming economic reason
why she should be selected over a less radical miss from a prestigious
finishing school (plus an M.B.A. from Harvard School of Business).
The upper echelon posts would be converted into semimonopolies
of these women who already hold them. If the WLF’s goal is really
to open the doors of American business to women—large numbers
of women—the “equal pay for equal work” proposal is ridiculous.
