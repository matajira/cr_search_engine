172 An Introduction to Christian Economics

worth the effort to control. Ait industries are not equally subject to
controls. Controls are put on such things as mining, steel manufac-
ture, metals of all sorts, electronics, and automobiles. (About the
only industry generally left free in the past has been farming: it is a
highly competitive market and constitutes a major voting bloc.)
Local businesses involved in retailing any of these products are faced
with a man-made crisis: shortages of goods and rising wage demands
by employees. Capital flows out of these areas of the economy and
into the so-called “luxury” trades: antiques, art collecting, coins,
stamps, rare books, rare wines and liquors. All of these industries
have experienced rapid price increases since 1965, the year inflation
began to be felt by the general public, When price controls appear,
their expansion will be that much greater, as more and more people
pour a depreciating currency into goods that are not under price
controls.

Jf manufactured goods, especially home appliances, get scarce,
then the home repair expert experiences a bonanza. People have to
make-do with the old washing machine or refrigerator. The day that
price controls are declared, the intelligent buyer will go down and
buy every $25-$50 used refrigerator he can store. He will buy old
broken motors from junk stores. The junk store man, if he is smart,
will try to increase his supplies, bolding inventories for as long as
possible, waiting for the economic boom. It will not be long in com-
ing. When price controls are in effect, a startling effect is produced:
the price differential between new and old goods begins to narrow.
In some cases the differential may even shift in favor of the used
goods: the used good is not under price controls, while the new good
is. People can bid up the prices of used goods in a way that they
cannot with the new goods. They can buy what they are willing to
pay for—but only in the used goods market. If price controls were im-
posed in 1975, by 1980 a man might be able to triple his original
investment. He could do far better if he were a repairman who had
bought junk discards to begin with. That is a good return on one’s
money; the stock market will never match it, for controls invariably
spell the death of blue chip capital stocks; it is these industries that are
placed under the controls first. Controls, by definition, are intended
to reduce profits,

The demand for repairs will skyrocket, but the larger unions—
plumbing, plastering, carpentry—are likely to be placed under wage
controls. Being more visible, and being organized into a guild, these
fields will be more easily controled by government boards of officials.
The result will be a fantastic increase in labor’s black market, or as
it will be called (as it is called today), “moonlighting.” A profes-
