Chapter XI

GERTRUDE COOGAN
AND THE MYTH OF SOCIAL CREDIT

(“Gertrude Coogan and the Myth of Social Credit” is not ex~
actly a Madison Avenue eye-stopper. But it's the best I can
do, under the circumstances, Few Christians have ever heard
of either Miss Coogan or Social Credit. Yet this essay is im-
portant, if only as a kind of reference guide. For Social Credit
is an enormously popular movement in the United States.
The resurgence of populism is strong, heralded in journals
as diverse as the sophisticated New York to the not very
sophisticated tracts of. the far Right. Social Credit is the
economics of neo-populism. It has infiltrated almost every
right-wing movement in this country. From dozens of little
right-wing groups comes the parade of Social Credit books,
by authors like Wycliffe B. Vennard, H. S. Kenan, Congress-
man McFadden, Whitney Slocum, Major Douglas, Fred-
erick Soddy, R. McNair Wilson, A. N, Field, Arthur Kitson,
aud the most famous one of all, Father Charles Coughlin.
It would seem safe to classify Wright Patman, the Chairman
of the House Banking Committee, as ane of the Social Credit
neo-populists, as well as former Congressman Jerry Voorhis
{who was defeated by Richard Nixon when the latter first
went to Congress), Gertrude Coogan is quoted as often as
any of them, so I have selected her work as representative.

The movement, at least in the United States (I know rela-
ively little about the Canadian Social Credit movement, ex-
cept that its economics is as ludicrous as the American
branch's is), is tied up in the old populist hatred of the “In-
ternational Banking Conspiracy.” This frequently drifts into
antisemitism, since the “International Tewish Banking Con-
Spiracy” is always just around the corner. Roman Catholics
(Coogan, Coughlin, Father Denis Fahey) are frequently prom-
inent in the movement, although Protestant fundamentalists
are just as numerous (but they are seldom the “intellectual”
leaders). In addition, almost every Anglo-Israelite writing
today is a Social Credit supporter (see C. F, Parker, Moses
the Economist [London: Covenant Publishing Co., 1947] or
J. Taylor Peddie, The Economic Mechanism of Scripture
(London: Williams and Norgate, 1934|). In case after case,
the advocates of Social Credit try to make their economic

124
