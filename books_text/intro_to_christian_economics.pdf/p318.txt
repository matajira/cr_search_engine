306 An Introduction to Christian Economics

1929, when the inflationary policies of 1922-29 were reversed by
officials of the Federal Reserve System. People saw that the gen-
eral level of prices in the nation was declining, especially stock
prices, but they refused to acknowledge the reality of the situation.
Instead of considering the possibility that prices might fall even more,
they concerned themselves with the amount of moncy they had put
into their investments. This in turn led them to hold on; the result
was financial disaster, as prices continued to skid. The man who
refuses to let go of the rope at fourteen feet had better be fairly
sure that the balloon is not going to carry him even higher.

One of the most common of all fallacies involved in the refusal to
accept the sunk cost doctrine is that of “equity” in a home. During a
depression, or any recession, some owners who want to sell their homes
or land refuse to sell at the prevailing prices. They argue, “I have
$5,000 equity in this piece of property; if I sell now, I'll lose it.” The
fact is that there is nothing tangible or marketable about “equity.” Once
a mortgage payment is made, it is gone, It entitles one to remain the
owner of the property until the next payment falls due. It entitles
one to make decisions now as to the sale or retention or rental of
the property. But there is nothing known as equity in economic
reasoning: you may sell a house for more than you put into it, or
less, or the same amount; but the market price is not determined by
the amount of money sunk into the property. One cannot have
something “in” the home, as if it were a refrigerator stocked with
food. We only have a title to the home which permits us to sell it
for whatever we can obtain on the open market. “Equity” is a mis-
leading concept which is stored in people’s minds, not something
which is in some mysterious way stored in a piece of property.

There is one sense in. which equity might have meaning econom-
ically. If the idea of equity is equated with net worth, and net worth
is defined as the present market value of a resource minus all eco-
nomic liabilities held against it, then it is all right to speak of
“equity.” But this is not what people mean when they use the term.
Therefore, it is best to avoid using the word “equity” unless the full
meaning is spelled out precisely.

The labor theory of value is a concept analogous to “equity.” It
assumes that an economic good is worth a given amount of money on
the market because a certain quantity of human labor has been in-
vested in producing it. This idea was basic to all economic thought
until the advent of the “marginalist-subjective” economics of modern
times (1870's). Karl Marx was the last major economist to hold to
the position; only Marxists, among serious economists, hold it today.
The concept is wrong. A buggy whip, even if it were made by a
