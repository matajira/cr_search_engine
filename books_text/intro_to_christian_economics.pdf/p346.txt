334 An Introduction te Christian Economics

it is not gratuitously possessed. Free ownership may command very
heavy costs. It is the right of free, exclusive control over property
which makes the economic burden inescapable; the free market
imposes responsibility with every grant of econamic power.

The farmer who does not wish to sel! his land, whether for senti-
mental reasons, or a fear of change, or a commitment to the ideals
of rural Hfe, or just to keep old Charlie Drackett from getting his
dirty hands on the bottom forty, is thereby compelled to pay for bis
use of that land, He has to defend his possession of exclusive con-
trol, daily, in the marketplace, It is not his legal title that is in
question; it is his economic ability to defend it against others who
think they could use his property in order to better service the needs
of the public. He does not have to defend it in the way that his
pyeat-grandfather did—shooting Indians or revenuers or Hatfields or
McCoys—but by using it to satisfy the incessant demands of an
unsentimental public. If he fails to do this, he suffers economic
losses, He may have to dip into his life savings to keep his farm
going. He may have to go decper into debt. Finally, if he continues
to fail to meet the public’s demands for more food, cheaper food,
better quality food (or even lower quality food, nutritionally, if that
is what the public wants), his mortgage will be foreclosed, The bank
will sell it, or the tax collector will sell it, to the highest bidder. The
highest bidder is a middleman. He is acting on behalf of the public.
He thinks he can use the land and other capital assets more efficiently
than anyone else can, If he is wrong, the process will start over
again. Private property is held in stewardship for the public.

Title to property is not held by “the public,” Titles are held by
individual owners. But the market combines the myriad discrete
demands of many individuals and inrposes costs on the possessors of
all desired economic resources. No owner can resist the pressure of
market demand without bearing these costs. Day after day, market
pressures force all owners to ask themselves, “What's it worth to me
to hold onto this?” The public responds, through the market, “You'll
have to meet our price if you want to keep it.” Day after day, all
those who retain free title to a particular piece of property meet this
price. They pay in the forfeited opportunities that might have been:
the vacation, the new car, the shares of IBM, and silence from “the
little woman” who wants to sell out, This is the law of survival in
the free market. May the best (most efficient) man win.

During the English Reformation the problem of the justification
of ownership came to a head with the confiscation of the property
of the monasteries, “The Reformation theorists,” writes Richard
Schlatter, “failed to solve their first great problem. They were not
