Tariff War, Libertarian Style 347

for protection should be seen for what it is: a cry for a reduction in
efficiency.

In a country like the United States, where less than 5 percent of
our national income stems from foreign trade, the cry is especially
jJudicrous. We hurt the other nations, whose proportion of international
trade to national income is much higher (West Germany, Japan),
without really aiding very many of our own producers, But there are
so few vocal interest groups representing those who benefit from freer
trade, while those who have a stake in the intervention of the State
make certain that their lobbyists are heard in Washington, The scape-
goat of “unfair foreign competion” may be small, but being small,
it is at least easy to sacrifice.

The Balance of Trade

In precapitalist days, economists believed that nations could ex-
perience permanent “favorable” balances of trade, A favorable hal-
ance was defined as one where you sold more goods abroad than you
imported, thus adding to the national gold stock. Wealth was defined
primarily in terms of gold (a position which, even if fallacious, makes
more sense than the contemporary inclination to define wealth in
terms of indebtedness}. Prior to the publication of Wealth of Nations
(1776), the philosopher, David Hume, disposed of the mercantilist
errors concerning the balance of trade. His essays helped to convert
Adam Smith to the philosophy of classical liberalism. Hume’s essay
“Of the Balance of Trade,” was published in 1752 in his Political
Discourses; it established him as the founder of modern international
trade theory.

The early arguments for free trade still stand today. Hume focused
on the first one, which is designated in modern economic terminology
as the price rate effect. As the exported goods flow out of a nation,
specie flows in. Goods become more scarce as money becomes more
plentiful. Prices therefore tend to rise, The converse takes place in
the foreign country: its specie goes out as goods come in, thus causing
prices to fall. Foreign buyers will then begin to reduce their imports
in order to buy on the new cheaper home markets; simultaneously,
consumers in the first nation will now begin to export specie and
import foreign goods, A long-run equilibrium of trade is the result.

A second argument is possible, the income effect. Export indus-
tries profit during the years of heavy exports. This sector of the
economy is now in a position to affect domestic production, as its
share of national income rises, It will be able to outbid even those
foreign purchasers which it had previously supplied with goods.

Last, we have the exchange rate effect. If we can imagine a
