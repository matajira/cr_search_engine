 

The Theology of the Exponential Curve 91

much more abundant” (Isa. 56:12). We must drop Rostow’s defini-
tion of modern industrial society as one in which “compound interest
pecomes built . . . into its habits and institutional structure.”* Mises
points out with regard to the economic contribution of the Austrian
economists:

The greatness of the service these three Austrian economists have
rendered by maintaining the cause of economics against the vain
critique of Historicism cannot be overrated. They did not infer
from their epistemological convictions any optimism concerning
mankind's future evolution. Whatever is to be said in favor of
correct logical thinking does not prove that the coming gencra-
tions of men will surpass their ancestors in intellectual effort
and achievements. History shows that again and again periods of
matvelous accomplishments were followed by periods of decay
and retrogression. We do not know whetber the next generation
will beget people who are able to continue along the lines of the
geniuses who made the last centuries so glorious, We do not
know anything about biological conditions that enable man to
make one step forward in the march of intellectual development.
We cannot preclude the assumption that there may be limits to
man’s further intellectual ascent. And certainly we do not know
whether in this ascent there is not a point beyond which the intel-
lectual leaders can no longer succeed in convincing the masses and
making them follow their lead.5”

Jn short, do not try to get more out of pure economic analysis than
economic analysis can possibly provide. Most of all, when you see
presentations of the exponential curve, with extrapolations made
into the next century, remember the words of Professor Nisbet with
regard to the use of statistical devices in explaining or predicting hu-
man affairs:

Here the Random Event, the Maniac, the Prophet, and the Genius
have to be reckoned with. We have absolutely no way of escaping
them. The future-predictors don’t suggest that we can avoid or
escape them-—or ever be able to predict or forecast them. What
the future-predictors, the change-analysts, and the trend-tenders
say in effect is that with the aid of institute resources, computers,
linear programming, etc. they will deal with the kinds of change

36. Walt W. Rostow, The Stages of Economic Growth (Cambridge: The
University Press, 1960), p. 7. For a critique on the Rostow thesis, see the
symposium of the International Economic Association, Rastow (ed.), The Eco-
nomics of Take-off Into Sustained Growth (New York: St. Martin's, 1963), es-
pecially the essay by Simon Kuznets. Rothbard’s question undermines the
Proponents of statist policies to stimulate economic growth: “By what right do
you maintain that people should grow faster than they voluntarily want to
grow?” Murray N. Rothbard, Man, Economy and. State (Princeton: Van Nos-
trand, 1962), IT, 837.

37. Ludwig von Mises, The Historical Setting of ‘the Austrian School of
Economics (New Rochelle, N. Y.: Arlington House, 1969), p. 38.
