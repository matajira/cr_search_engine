320 An Introduction to Christian Economics

property, is very likely a contributing cause of enforced “ghettoiza-
tion”; it makes it more difficult for the person who is financially able
to move out of a patticular geographical area to do so, since it grants
an indirect subsidy to the owners in the restricted areas, The form
that subsidy takes is a lower annual property tax bill than would have
been assessed under a market-oriented tax assessment system. Dis-
crimination should be a private right, but the civil government should
not enforce such discrimination with subsidies to those doing the
discriminating. Men should not receive something for nothing, in-
cluding the right to discriminate.

The Taxation of Church Property

Libertarians are inclined to support the demand made by Madalyn
Murray O’Hair, that property taxes be assessed to churches. The case
before the Supreme Court, Walz v. Tax Commission of the City of
New York, argues that Frederick Walz, a small property owner in
New York City, is forced to pay higher taxes because churches are
entitled to the exemption. One estimate runs as high as $35 million
in lost taxes for New York City (on $700 million worth of prop-
erty)41_ Martin A. Larson, a constant critic of tax exemption for profit-
making church properties, has estimated (as of 1965) that total re-
ligious property, including Roman Catholic schools but not inde-
pendent religious schools, totals $80 billion. This represents some
60 percent of the $135 billion worth of all private, tax-exempt,
charitable property.2 This means, he estimates, that about $1.6
billion worth of property taxes are lost each year, as a direct result
of the tax exemption of the religious institutions.

‘This, of course, is a purely economic analysis. It is no doubt true
that taxes might be lowered, temporarily, if all church property were
taxed. Property taxes do not go down, however, The local govern-
ments would simply absorb the new revenues. Furthermore, churches
do perform charitable activities, Taxing church properties would
reduce funds in the churches available for social welfare purposes,
thus adding to the burden of local civil governments or non-religious
charitable organizations. Given the efficiency of governmental wel-
fare organizations, this should not be a source of rejoicing for the
poor. Neverthcless, from an economic point of view, tax-exempt
church properties are indirectly subsidized.

The issue goes beyond cconomic analysis. The power to tax, as
Marshall pointed out so jong ago, is the power to destroy, for it

11. Tony Cook, “Should Churches Be Taxed?” Eternity (March, 1970),
p. 21.

42. Martin A. Larson, Church Wealth and Business Income (New York:
Philosophical Library, 1965}, pp. 50-51.
