index of Persons

Cunningham, Robert, 289n

Dabney, Robert, 16n
Dales, J. H., 94n
Daniel-Rops, Henry, 5
Darwin, Charles, 77
Deane, Herbert, 327n.

Gaulle, Charles,-25, 56, 176, 202
de Jouvena), Bertrand, 342n
de Roover, Raymond, 37in
De Wit, James, 2140
Del Mar, Alexander, 127
Director, Aaron, 325
Dolan, Edwin, 940,
Dooyeweerd, Herman, 211, 360
Douglas, Major, 124, 161
Douglas, Paul H., 58
Drucker, Peter, 59, 288
Dubos, René, 80n
Dunne, Gerald, 96
Durham, E. Alden, 297£
Durkheim, Emile, 245

Eck, John, 363a

Edward VY, xii

Einzig, Paul, 1080

Evans, M. Stanton, 225

Evans, Milton, 214n, 215n, 367n

Facey, Edward C., 227
Fahey, Denis, 124
Falienbucht, Z. M., 264
Faulkner, Harold U., 337
Field, A. N., 124

Fischoff, Ephraim, 215n
Fisher, Irving, 93, 162n, 151
Freeman, Roger, 289n
Freud, 78

Friedman, David, 225

Friedman, Milton, 27, 86, 93, 108,

126n, i51n
Fuller, Buckminster, 274

Garrett, Garet, 145

Gay, Peter, 76

George, Charles, 363n

Gessell, Silvio, 161
Gerschenkron, Alexander, 259f
Glushkov, Victor, 261f
Goldman, Marshall, 268n
Grace, Frank, 213n

Gray, Horace, 235n

Greaves, Percy, 14in

Groppi, Father, 327
Groseclose, Elgin, 14, 80n, 155
Grossman, Gregory, 264
Grubiak, O. & F., 1584

Haberler, Gottfried, 60n, 61n
Hamilton, Alexander, 140
Hansen, Aivin, 125, 136n, 162n
Hardin, Garrett, 88£, 384n
Harper, F. A., 314, 368n

407

Hayek, F. A.
capital, 62
conservatism, 229
inflation, 59
falling prices, 93
knowledge, 110n, 2590
law, 311
nevtrality, 950
obsolete! 126n
planning, 150, 270
reinfiation, 68
security, 240, 243
theory & practice, 49, 109
trade cycle, 61, 66
‘Viner on, 230
worst on top, 381
Hazlitt, Henry
credit, 10
effective demand, 135f
GNP, 88n
Keynes, 69
minimum wage laws, 277
statistics, 150
tariffs, 341
things unseen, 160
unions, 33£
Heaton, Herbert, 80
Hegel, 269; see also Subject Index:
Hegelianism
Herblock, 82
Hirschleiffer, et al., 94n.
Hitler, Adolph, 24, 246
Hobbes, Thomas, 76
Hoff, T. J. B., 117%, 259n
Hubbert, M. King, 84n, 294n
Hughes, Howard, 254
Hume, David, 347, 349

Treton, Henry, 324

Jarvis, Howard, 310

Tasny, Naum, 81, 82n, 26{n
Jaspers, Karl, 240
Jefferson, Thomas, 229
Jevons, W. S., 70

Jordan, W. K., 3670

Judas Iscariot, 88

Kant, Immanuel, 269
Kemp, Arthur, 56n, 57n
Kenan, H. S., 124
Kerr, Clark, 85, 292
Keynes, Tohn Ma mard; see also Sub-
ject Index: Keynesianism
barbarous relic,
Coogan and, 135f, 138
effective demand, 135
fixed exchange rates, 118f
Hazlitt on, 69
inflation, 154
Nixon and, 19
theory, 4
unions,
Kilns. ‘ester, 385
