The Ethics of Monetary Hoarding 205

flation, monetary hoarding is a form of investment; it repays the saver
more in the future than the interest returns at some bank would re-
pay. In other words, in today’s world, the good servant is the one
who buries his gold or silver talent!

The Philosophy of Inflation: John Law

The first mass inflationist of the modern world was a Frenchman
named John Law. He governed France’s monetary affairs in the
1720’s, and his policies almast destroyed the French economy. For
this reason, it is profitable to examine Law’s concept of the State’s
role in money creation and his ideas concerning the “crime” of
hoarding money metals. In his History of Monetary and Credit
Theory (1940), the late French economist Charles Rist quotes a
lengthy extract from Law’s writings:

The prince has direct power over those who lock away and con-
ceal coin, for this coin is the property of individuals only as a
means of circulation, and they have no right to make it their own
in any other sense; .. . AJ] the coin of the Kingdom belongs to
the State, represented in France by the King; it belongs to him
precisely the same way as the high roads do, not that he may
appropriate them as his own property, but in order to prevent
others doing so, and as it is one of the rights of the King, and
the King alone, to make changes in the highways for the benefit
of the public, of which he (or his officers) is the sole judge, so
it is also one of his rights to change the gold or silver coin into
other exchange tokens, of greater benefit to the public, which
he himself will accept as he accepted the others; that is the
position of the present government.®

In other words, thé king, and the State he represents, has a
legal and moral right to debase the currency, fix exchange rates,
and generally to manipulate the monetary standard. This, of course,
will bring Gresham’s Law into action, and people will naturally
tend to hoard the artificially undervalued coins. This, Law continues,
is something which will not be permitted under any circumstances.
Like the inflationists who have followed him, he denied the right
of private property to the citizenry; all people who oppose monetary
hoarding are essentially making the denial, but Law was more honest
and more forthright in stating his position:

However, as the coin of gold and silver bears the image of the
prince or some other public mark, and as those who keep this
coin under lock and key regard it as exchange tokens, the

prince has every right to compel them to surrender it, as failing
to put this good to its proper use, The prince has this right even

6 Charles Rist, History of Monetary and Credit Theory (New York: Mac-
milan, 1940), pp. 59-60.
