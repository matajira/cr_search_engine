382 An Introduction to Christian Economics

existence of endowed agencies within the churches: mission boards, edu-
cational institutions, denominational publishing houses, and so forth,
The financial autonomy from the weekly contributions of the members
has been a basic means of subversion.** Part of this autonomy is pro-
vided bythe irrevocable annuity schemes and long-term loan contracts.
These favor the perpetuation of the institution in question apart from
the theological commitment of the institution. Its future supposedly
rests more on income from “prudent investments” than on the preserva-
tion of its original theological standards. This, of course, is inevitable,
given the nature of the trust agreements, as indicated by the following
sales pitch:

Long after you haye gone home to heaven your influence can live
on... here on earth. A gift to missions—-to the ongoing work
of the worldwide Church—can mean that your Christian influence
will live on through the years in dedicated hearts, hands and feet
of the servants of Christ in the far corners of the earth.?*

What an irresistible appeal for some elderly, unsophisticated widow
who has a few thousand dollars saved! And what a curse to the or-
ganization which offers her the appeal; it is sealing its own doom, theo-
logically. The structure may survive, but the goals will change. Per-
petual annuities and trusts reverse the promise of Solomon: “The
wealth of the sinner is laid up for the just* (Prov. 13:22b). Instead,
we find that the wealth of the just is laid up for the sinner.

The hypocrisy of the appeal to the elderly Christian to part with
his money like this should be manifest. The donor (or in this case, the
usurer), is led to believe that the institution, in and of itself, can and
will maintain its commitment to the establishment of God’s kingdom.
What the institution needs, the donor-usurer is told, is a permanent
fund, The fund is crucial, and not a commitment to theology, Theology
will take care of itself; what is needed is money! The fund must expand,
even if this means that Christians are turned into usurers, and economi-
cally imprudent usurers at that (given the fact of inflation). If the

26, Another important factor in the take-over of the Protestant churches is
an attitude best described as “Protestant sacerdotalism.” It regards the minister
as standing above and distinct from the ordained elders, and it regards the laity
as not merely functionally sebordinate but also intellectually inferior, Cf. Paul
Ramsey, Whe Speaks for the Churches? (Nashville: Abington, 1968). For a
classic example of the arrogance of the pastors whose votes place denominations
in support of radical positions, in direct opposition to the stated. opinion of the
majority of the members, see John C. Bennett, “Christian Responsibility in a Time
that Calls for Revolutionary Change,” in John C. Raines and Thomas Dean, eds.,
Marxism and Radical Religion (Philadelphia: ‘emple University Press, 1970),
pp. 75-76. An excellent critique of Protestant sacerdotalism is provided in E. L.
Hebden Taylor, Reformation or Revolution (Nutley, N. J.: The Craig Press,
1970), p. 413 ff

27. Bob Pierce, founder of World Vision, in his introductory statement to
Faithful Stewardship Through Christian Investment, p. 1.
