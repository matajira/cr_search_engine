66 An Introduction to Christian Economics

more, since consumer demand for present goods has risen. Higher
wages are being paid and more people are receiving them. Their old
time-preference patterns reassert themselves; they really did not want
to restrict their consumption in order to save. They want their de-
marids met now, not at some future date. Long-range projects which
had seemed profitable before (due to a supposedly large supply of
capital goods released by savers for long-run investment) now are
producing losses as their costs of maintenance are increasing. As
consumers spend more, capitalists in the lower stages of production
can now outbid the higher stages for factors of production, The
production structure therefore shifts back toward the earlier, less
capital-intensive patterns of consumer preference. As always, con-
sumer sovereignty reigns on the free market, If no new inflation oc-
curs, many of the projects in the higher stages of production must be
abandoned, This is the phenomenon known as depression. It results
from the shift back to earlier patterns of consumer time-preference.”

The injection of new money into the economy invariably creates a
fundamental disequilibrium, It misleads entrepreneurs by distorting
the rate of interest. It need not raise the nation’s aggregate price
level, either: the inflation distorts relative prices primarily, and the
cost of living index and similar guides are far less relevant,® The
depression is the market’s response to this disequilibrium. It restores
the balance of true consumer preference with regard to the time
preferences of people for present goods in relation to future goods.
In doing so, the market makes unprofitable many of those incompleted
projects which were begun during the boom,

What is the result? Men in the higher stages of production are
thrown out of work, and not all are immediately rebired at lower
stages, especially if these workers demand wages equivalent to those
received during the inflationary boom, Yet they do tend to demand
such wages, and if governmentally protected labor union monopolies
are permitted to maintain high wage levels, those who are not in
the unions will be forced to work at even lower pay scales or not
at all, Relative prices shift back toward their old relationships. The
demand for loans drops, and with it goes much of the banks’ profit.
The political party in power must take responsibility for the “hard
times.” Savers may even make runs on banks to retrieve their funds,
and overextended banks will fail, This reduces the deposits in the
economy, and results in a deflationary spiral, since the deposits

 

 

7. Hayek, Prices and Production (2nd ed.; London: Routledge & Kegan
Paul, 1935), chaps. 2, 3.

8.| Ibid., p. 28; Hayek, Monetary Theory and the Trade Cycle (New York:
Kelley Reprints, [1933] 1967), p. 117n.
