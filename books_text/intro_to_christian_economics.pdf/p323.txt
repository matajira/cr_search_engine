Property Taxes: Sovereignty and Assessed Valuation 311

Angeles taxpayers’ group. He claims that 7,500 outright tax fore-
closures took place in California cach month in 1969. As soon as
the validity of the property tax is granted, the threat of coercive fore-
closure (or forced sale by the owners in order to meet present taxes)
is a distinct—indeed, inescapable—possibility.

Tax structures in most mid-twentieth-century industrial nations
are notoriously arbitrary. Examples like England’s income tax and
inheritance tax come to mind, taxes so steeply graduated that some
wealthy Britishers have fled to the Channel Islands or even to perma-
nent residence on ships in order to escape. Few taxation schemes
endow a government with more raw, arbitrary power than the pres-
ently prevailing “assessed valuation” system. When the value of a
piece of property is estimated by paid bureaucratic functionaries af
the taxing agency itself, rather than by a free market, the possibilities
for coruption and coercion are obviously multiplied. It should not
take too much thought to understand why this might be the case,
Where political monopolies can reap monopolistic taxation returns,
let the citizen beware.

The goal of any free saciety is to establish a framework of law
that makes the actions of the administrators of the law predictable
by those living under the jurisdiction of the State or those who,
living outside that jurisdiction, nevertheless have to deal with these
who live inside it. This element of predictability is basic to political
freedom, as Hayek has argued so forcefully. Without continuity of
enforcement in terms of written, previously announced Jaws, it be-
comes very difficult to plan for the future with any degree of security.
Without general, abstract law, free men find it difficult to exercise
their creative capacities, since in an arbitrary political world, creativity
in individuals may be regarded by the authorities as a threat to their
positions. As Hayek says:

There is probably no single factor which has contributed more
to the prosperity of the West than the relative certainty of the
iaw which has prevailed here. This is not altered by the fact that
complete certainty of the Jaw is an ideal which we must try to
approach but which we can never perfectly attain.®

sei .e A. Hayek, The Constitution of Liberty (University of Chicago Press,
1960).

5. Ibid., p. 208. Even as a theoretical ideal, however, it is unlikely that a
purely abstract formal legat structure is either possible or desirable, since
some degree of flexibility is necessary in ihe concrete application of all Jaw.
On thé complex interconnections of formal and substantive law, see Max
Weber, On Law in Economy and Society (New York: Simon & Schuster
Clarion Book, [1954] 1967), esp. chaps. 8, 9. This is also available as a sec-
tion of Weber's posthumously published Economy and Society (New York:
Bedminster Press, [1924] 1968).
