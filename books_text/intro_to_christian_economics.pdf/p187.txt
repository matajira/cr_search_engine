Chapter XIV

GRESHAM’S LAW REVISITED:
THE COIN CRISIS OF 1964

[This is one of three articles of mine published by coin collect-
ing journals in the United Siates. This one appeared in
COINage (Sept., 1969). Subsequent events would force me
to modify it somewhat. The floating of the dollar and the halt
in United States gold payments of August 15, 1971 officially
took the dollar off the international gold standard. Mass
inflation as a governmental policy finally took its inevitable
course. The meaningless token devaluation of December 18
made it official: the increase in the supply of paper dollars
had forced up the price of gold since its 1934 price was es-
tablished at $35 per ounce. That “definitional” relationship
—for every dollar outstanding, there is one thirty-fifth of an
ounce of gold in the Treasury—had always been a joke, and
the reality of the inflation finally forced Mr. Nixen’s hand. He
wasn't holding a full house, either; it was a pair of deuces. The
devaluation meant nothing, since the United States is not
about to pay out any gold, whether at $35 or $38 per ounce.
The dollar is now what it has been domestically for decades:
& fiat currency.)

Since 1964 collectors have been made to feel “guilty” for the
practice of hoarding coins having silver-content. In that year, the first
of the great coin shortage, massive traffic jams occured on eastern
turnpikes as drivers, short of change, waited at the entrances to have
change made for them. Smali red, white, and blue stickers appeared
on cash registers across the country proclaiming “Coin Shortage—
Use exact change.” The treasury department even had puppeteer
Shari Lewis make a short film clip asking parents to take their chil-
dren’s piggy-bank money down to the local bank and exchange it for
paper. And the party who was responsible for making motorists
wait, for frustrating the checkers at supermarkets, for taking away
children’s pennies? That’s right, the miserly collector!

But was the collector really to blame? Did he do anything which
could in any way be considered economically unsound? Apparently
the government thought so, but then the government has always found

175
