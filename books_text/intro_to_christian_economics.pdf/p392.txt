380 An Introduction to Christian Economics

tive effects of processed foods, have combined to reduce the average
life expectancy for males by as much as five years. So the purchaser
of a deferred annuity loses both ways: he lives a shorter life and is
paid off in depreciated currency,

“Strictly as an investment,” the Institute goes on to say, “most
retirement annuities are not especially desirable. The interest yield to
the maturity of the policy is lower than probably can be obtained from
a wise selection of other investments.” The study adds this warning:
“During an inflationary period, funds should be invested principally
in the types of securities that will tend to preserve purchasing power.”
Publications sent by the Institute since the issuing of this one indicate
that in the opinion of the Institute’s staff, runaway inflation is now a dis-
tinct possibility. The higher the rate of inflation, the poorer the invest-
ment in annuities of any kind. The AIER staff recommends that elderly
people purchase Swiss annuities only (April 19, 1971).

Since the economic effect of inflation on annuities and other kinds
of insurance contracts, as well as on long-term loans, is to destroy the
investor’s capital, should the churches continue to promote such con-
tracts (even if they were not usurious, which they are)? Can the
church’s leaders afford not to analyze the causes and effects of in-
flation, and then. bring the warning to their flocks? Does not this aspect
of preaching fall under the general requirement of preaching the whole
counsel of God? The answer of most of our pastors today is simple: no.

When R, J. Rushdoony spoke in a church on the nature of inflation at a
special midweek conference, he received a letter from a pastor who was
critical of such a message even being presented inside the church’s
building.# On another such occasion, one minister was threatening to
have him publicly disciplined by his denomination for having narrated
a filmstrip critical of the inflationary policies of the Federal Reserve
System, Any number of arguments can be used by the antinomian
clergy against this kind of preaching: “Separation of church and state
(and never mind about our tax law break)!” “The Bible doesn’t talk
about inflation!” “The Bible isn’t a textbook of cconomics.” “We're
under grace, not law.” So they continue to lead their unsophisticated,
trusting congregations into usury and economic self-destruction. No
warnings are offered, no attempt is made to abandon the Joan con-
tracts. There is every evidence in our churches today of judicial blind-
ness, a curse imposed by God comparable to the one promised by
Isaiah and administered by Christ: “By hearing ye shall hear, and
shall not understand; and seeing ye shall see, and shall not perceive”
(Matt, 13:14},

24, R. J. Rushdoony, The Biblical Philosophy of History (Nutley, N. J.: The
Craig Press, 1969) (chap. 32, below].
