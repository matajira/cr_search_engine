408

Kirk, Russell, 226n

Kirgner, Israel, 303

Kitson, Arthor, 124

Kline, Meredith, 212

Knight, Frank H., vilin, ix, 88, 116n,
, 313

Koch, Adrien, 229n

Kristofferson, Kris, 333

Kuyper, Abraham, 211, 370

Koestler, Arthur, 245n, 269

Kolko, Gabriel, 235n

Kuznets, Simon, 90n

Lange, Oskar, 1171,
Larson, Martin, 320
Lasky, Victor, 81n
Latimer, Hugh, xii
Law, John, 129n, 147, 205
LeFevre, Robert, 227f
Lehrman, Hal, 79
LeKachman, Robert, 125
Leoni, Bruno, 230n

Letwin, William, xii, 328n.
Levine, Herbert, 260, 262, 329n.
Lewis, C. S., 78, 276, 358
Lewis, Flora, 271

Lewis, Shari, 175

Lincoln, Abraham, 42, 127
Lipton, Dean, 70n

Liska, Tibor, 262f

Locke, John, 227

Lovejoy, Arthur, 3270

Lowe, David, 85n.

Luethy [Lithy], Herbert, 215n
Luther, Martin, 363n, 391

258

Machen, J. Gresham, 38in
Maddox, John, 80n
Malabre, Alfred, 111
Mander, Jerry, 79
Mandeville, Bernard, 168
Manne, Henry, 16n, 97n
Mao, 329
Marshall, Alfred, 125n.
Marshall, Joha, 308
Martin, Rose, 136n
Marx, Karl
alienation, 245
capitalism's anarchy, 116
labor theory of value, 74, 278, 306
nature-freedom, 269
property, 326
property-franchise, 324f
value. 70f
Weber and, 234
Mather, Cotton, 361
MeCaulay, Lord, 325
McDonald, Marie, 213n
McFadden, Congressman, 124
Mead, Sidney, 329n
Menger, Carl, 70
Merriam, Thornton, viin
Mises, Ludwig
Austrian School, 91

An Introduction to Christian Economics

boom-bust; see trade cycle
bureaucracy, 236fF
calculation, socialist, 105n, 117, 258f
collectivism, 134
crack-up boom, 63, 68
epistemology, 45
falling prices, 38n, 151
Festschrift, 93
fixed exchange rates, 113ff, 123n
free banking, 40ff, 96
gold, 47,145
hot money, 115, 181
inflation, 30f
intervention, 348f
interest rate, 35n
Liberman reforms, 268n
management, 329
mixed economy, 120, 330£
money, vii, xi, 72, 95n, 129
money contracts, 108, 138
obsolete! 126n
100% reserves, 42
profit, 116n, 234, 313n
ownership, 326, 333
property, 335f
reinflation, 37n
Rothbard, comments on, 37n
Raritania, 123n
stable money, 148
socialism, 238
social Darwinism, 72
stones into bread, 86
theory & practice, 107
trade cycle, 55n, 59f, 64, 364
yalue, 133f
world bank, 41, 110n
Mishan, E. J., 94n
Mitchell, Wesley, 43n
Mofsky, James, 235n
Mommsen, Theodore, &3n
Moran, Jim, 249
More, Sir Thomas, 335
Morgan, Edmund, 381n
Morris, Henry, vilin
Murray, Gilbert, 242
Murray, Madalyn (O°’Hair), 320

Naaman, 5

Naboth, 213

Newton, Sir Isaac, 76

Nisbet, Robert
academic freedom, 294
atomism, 226, 245n
authority & family, 2476
development idea, 76
Enlightenment, 84
family, 247f, 250f
history, 91
Marx-Weber, 234
rationalization & family, 250£
sociology, 232
stability, 244
tenure, 294
totalitarianism, 226, 245n.
