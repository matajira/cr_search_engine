272 An Introduction to Christian Economics

the construction of public housing, and the use of scarce resources
involved in such construction, on a priority scale that high in the
minds of the American public? Would a non-inflationary tax cut not
be preferable?? It is typical of socialistic thinkers to point to emer-
gency spending (e.g., a war) or some statist rocket program and
recommend a transfer of funds from one branch of the State’s plan-
ning bureaucracy to another. I have never heard them recommend a
reduction in spending by the State, Spending precedents set in war-
time, like “temporary” taxes, seem to become permanent. Finally, in
Miss Lewis’ example, is the mere application of the techniques of
applied science sufficient to end warfare and cruelty? Or could it be,
as the Apostle James put it, that our wars come from the hearts of
men?* Conversion, in and of itself, may not redeem technology,
but can Miss Lewis be so certain that technology can redeem
mankind?

Technology is a tool. Like any tool, it has its limitations. One
must be very careful to keep from using an inappropriate tool to
complete some task, It makes it imperative that the user specify the
exact nature of his task beforehand.

Any standard economics textbook will usually compare economics
with engineering, The contrast is not perfect, but it does set before
the reader the different ways an economy must plan. The engineer
must decide, given a specific goal, how to allocate the available re-
sources to complete it, The economist must look at the available
resources, and decide where to allocate them, given a multiplicity of
goals. In some cases, it will be difficult to separate the two jobs,
but the distinction is useful for purposes of conceptualization.

The Technocrats of the 1930’s urged us to accept the economic
guidance of the engineering elite, They would eliminate “waste.”
Yet the engineers of the Soviet Union have beén forced to construct
crude economic accounting techniques in order to deal with such
“capitalistic” phenomena as value and the rate of interest. Engineer-
ing—-meaning specialized, technological competcnce—cannot deal
with such psychological imponderables as consumer preferences.
Only the price mechanism of a free market can do this with any de-
gree of accuracy, which is why Ludwig von Mises rejects socialist
Planning.® If we confuse engineering with economic calculation, we
will destroy the rational allocation of scarce resources by the market.

3, Cf Gary North, “Urban Renewal and the Doctrine of Sunk Costs,”
The Freeman (May, 1969) [Chap. 26, below].

4. James 4:1.

5. For a summary of this literature spearheaded by Mises, see my chapter
on “Socialist Economic Calculation,” in Marx’s Religion of Revolution (Nutley,
N.J.: The Craig Press, 1968).
