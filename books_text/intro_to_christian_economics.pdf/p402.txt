390 An Introduction to Christian Economics

aspects of economics relating to debt. These I dealt with in an earlier
talk at Sunnyvale.)

“T take the law of God very seriously. I believe that man is saved
from the law as a handwriting of ordinances against him, so that
mam is no longer, as a Christian, under the law as an indictment,
but he is under the law as a way of life. The Jaw is now written on
the tables of his heart (the sign of the new covenant), and is his
joy to keep. Man is not saved to have other gods, commit adultery,
kill, steal, or covet, or to break any of God's laws, but, having a new
nature, delights in God’s will to the extent that he is sanctified,

“The ceremonial and sacrificial law is clearly fulfilled in Christ’s
atoning death and resurrection. Certain other laws have been sub-
jected to changes by apostolic teaching, or our Lord’s teaching, as
witness the change of the death penalty for adultery to divorce, and
the revision of the day of worship, and the end of the old sabbath
tegulations (Col, 2:16f., etc.). Certainly the Reformers did not
treat the Old Testament laws lightly, as witness their concern with
usury.

“I believe that it is a part of cur modern apostasy that we have
abandoned much of the world to the devil and restricted the gospel
to a narrow realm. The doctrine of creation is to me the cornerstone
of our faith. Because the Holy Trinity created all things, all things
are understandable only in terms of the triune God, and only He
can redeem His creation. Moreover, only under His law can the
creation function without ruin. Therefore, God’s word must be
declared for every realm: we must have a Christian economics,
philosophy (which begins with the premise of the infallible word and
the trimne God), historiography, literature, law, political science,
and so on.

“I wrote The Messianic Character of American Education as a
carefully documented statement of my thesis, that education apart
from Christian theistic principles is destructive of itself and of man.
I believe that the same is true of every other field of study.

“The doctrine of the bodily resurrection of our Lord is in part a
declaration that God’s salvation is not restricted to the soul alone,
but that time and history as well as eternity, the body as well as the
soul, are destined to share in the glorious salvation of our God.

“I would agree that the church has no jurisdiction apart from the
word of God, the sacraments, and the administration of godly disci-
pline within the church. But the word of God speaks to every con-
dition and to every realm of life.

“My point, in dealing with economics, was, in all three talks,
Biblical. I dealt with the Biblical laws concerning money and debt
