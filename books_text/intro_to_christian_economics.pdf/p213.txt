The Ethics of Monetary Hoarding 201

thing, a highly moral act. Hoarding of coins takes money out of circula-
tion, and this helps to reduce inflationary pressures, One is not hurting
the vending machine operators, since the sandwich coins can be used
for these purchases. People are only hoarding silver coins, not the
sandwich coins. Inflationists, of course, do not sce inflation as an
evil; they see it as a positive good. In their eyes, inflation is a positive
blessing conferred upon the public by the State (although private
counterfeiting is somehow immoral). Naturally, they see monetary
hoarding as an evil, as indeed it is if one accepts their original
premise, that inflation is good, But one should realize that it is only
on these grounds that monetary hoarding is immoral. Anyone who
blissfully labels the monetary hoarder as some kind of moral de~
generate should first make certain that he favors government counter-
feiting as an ethically righteous policy.

The reader may have noticed that I keep referring to monetary
hoarding. I have done so deliberately. The hoarding of money is
in n0 way comparable to the hoarding of consumption goods during a
siege or some other disaster. The reason that wo can distinguish
between two kinds of hoarding stems from a fact of economic theory:
the market always makes full use of the prevailing amount of money
that is present at any given time. Rothbard shows why this is true:

Goods are useful and scarce, and any increment in goods is a
social benefit, But money is useful not directly, but only in
exchanges. .. .. When there is Jess money, the exchange-value of
the monetary unit rises; when there is more money, the exchange~
value of the monetary unit falls. We conclude that there is no
such thing as “too little” or “too much” money, that, whatever the
social money stock, the henefits of money are always utilized to
the maximum extent. An increase in the supply of money con~
fers no social benefit whatever... . For money is used only for
its purchasing power in exchange, and an increase in the money
stock simply dilutes the purchasing power of each monetary unit.

As a Christian, I cannot accept Rothbard’s idea that any and all
consumer goods confer social benefits (brothels, for example), but
his general observation is a sound one: whatever the social money
stock, the benefits of money are fully utilized, And the social money
stock includes the meney in hoards, Money, actually, is never in
circulation, if by circulation one means the kind of circulation found
in the flow of blood in the veins, Circulation is a term used by econo~
mists which has been borrowed from biology, but it is not an alto-
gether accurate term. When a person takes a coin from his pocket
(his “cash balance”) and presents it fo a store owner, the coin now

4. Ibid., I, 670, Cf, North, “Gold’s Dust,” The Freeman (Oct. 1969)
[chap. 4, above],

 

 

4
i
i
