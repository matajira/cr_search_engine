110 An. Introduction to Christian Economies

portant this condition is will be seen if we reflect that national eco-
nomic integration (among the separate regions of one country) is
unimaginable with fluctuating rates of exchange between, say, regional
currencies.”*> The government’s answer to this “unimaginable” proc-
ess of regional currencies is to establish a central monopoly of money
creation coupled with a legal tender law. And this is precisely the
goal of international socialist planners: a single world bank with a
legal tender law to enforce its control over the entire face of the
earth. The planners want a “rational” world economy, but their
faith is-in bureaucratic rationalization—-a bureaucratic hierarchical
chain of economic command—and not the rationalization that is
provided by a voluntary free market and its sophisticated computer,
the free market price mechanism.” As yet, they have not achieved
such “rationalization” simply because all the nations want their own
domestic, inflationary, autonomous “rationalizations.” Fixed exchange
rates are as close as they can come to centralized world planning,
so they tried it, by means of the International Monetary Fund, from
1947 until August 15, 1971. On December 18 they returned to the
familiar policy of fixed exchange rates. Four months of international
monetary freedom were all they could take.

Why do conservative economists lend support to fixed exchange
rates? Because they think that this is a form of statist intervention into
the world market which can impose restraints on the State’s own
policies of domestic inflation, The State, the argument goes, will
contro! itself by law. To some extent, this may be true, temporarily.
The fear of an international run on the dollar may have restrained the
Federal Reserve System’s expansion of the domestic moncy supply
from December, 1968, through the spring of 1970. Officials may
have feared the action of foreign central bankers in demanding gold
at the promised price fixed by 1934 law of $35 per ounce. But this
slowing in the money supply created an inevitable reaction: the stock.
market fell by one third, and the government could no longer finance
its debt through sales of bonds to individuals or private corporations.
Therefore, the Federal Reserve stepped in once again to purchase the
available government bonds at the preferred low interest rate. A new
wave of inflation began in the spring of 1970, The pressures on the

5. Ebid., p. 230,

6. Ludwig von Mises, Human Action (3rd rev. ed.; Chicago: Regnery,
1966), pp. 476-478.

7. Cf. Gary North, “Statist Bureaucracy in the Moder Economy,” The
Freeman (January, 1970); Mises, Bureaucracy (New Rochelle, N. Y-: Arling-
ton House, [1944] 1969); North, “The Mythology of Spaceship Earth,” fhe
Freeman (November, 1969). On the nature of knowledge and the market's
division of Jabor, see F. A. Hayek, Individualism and Economic Order (Uni-
versity of Chicago Press, 1948), chap. 4.
