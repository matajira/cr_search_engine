362 An Introduction to Christian Economics

extort any usurious returns from God’s people, for they were oppressed
by the ravages of famine and the costs of redeeming their brethren out
of bondage (Neh. 5:1-13). The rulers were wise enough to heed his
warning, going so far as to return both principal and interest to the
debtors (5:11-12). It is unlikely that this example will be followed
in our modern, enlightened Christian circles.

Usury, Interest, and Charity

The prohibition against usury as it appears in the Mosaic law refers
specifically to the brother who is poor: “If thou Jend money to any
of my people that is poor . . .” (Ex. 22:25); “And if thy brother be
waxen poor...” (Lev. 25:35). It was legitimate to take a return
above the sum lent from the religious stranger (Deut, 23:20), A tenth
of this increase would therefore be tithed to God, thereby extracting
from the unregenerate at least a portion of the tithe that all men owe
to God. As a slave to sin, the stranger was not protected from the
bondage imposed on a poor man by every usurious contract, But to
the poor Hebrew brother his lending brother was to show mercy; no
increase of any kind beyond the original money or goods could be
legitimately claimed by the creditor (Lev. 25:37),

Historically, these restrictions were not acknowledged as binding
by the Hebrew commonwealth, The continual violations of alJ aspects
of the Mosaic law brought condemnation on the nation, God had
not left them without warning:

He that hath not given forth upon usury, neither hath taken any
increase, that hath withdrawn his hand from iniquity, hath exe-
cuted true judgment between man and man, Hath walked in my
Statutes and hath kept my judgments, to deal truly; he is just, he
shall surely live, saith the Lord God. ... (He that} Hath given
forth upon usury, and hath taken increase: shall he then live? he
shall not live: he hath done all these abominations; he shall surely
die; bis blood shall be upon him (Ezek, 18:8-9, 13}.

The definition of usury is precise Biblically: any increase taken from
the poor in return for having made a loan. There is no Biblical evidence,
nor have Christian casuists generally argued, that the prohibition re-
stricted interest received on business loans, so long as the lender shared
the risks of failure along with the borrower. This interpretation of the
usury prohibition was basic to the expositions of medieval and early
Protestant casuists.1 By sharing in the risk of a profit-making business,

1. J. Gilchrist, The Church and Economic Activity in the Middle Ages (New
York: St. Martin's, 1969), pp. 65 ff. Cf. John T. Noonan, The Scholastic Analy-
sis of Usury (Cambridge, Mass.: Harvard University Press, 1957), pp. 40, 41, 46,
59, 136. As Noonan shows, the acceptance in the late [5th century by Roman
Catholic theologians of the validity of the contractus Trinus—a partnership in
which. one partner bore all risks of failure and paid the other a fixed return
