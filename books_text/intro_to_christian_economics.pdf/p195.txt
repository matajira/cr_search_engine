Chapter XV

MAKING MONEY OUT OF MONEY

[Practical guidelines for survival in an age of mass inflation
are seldom found in popular journals. They are generally
confined to “extremist” magazines and newsletters. There-
fore, I decided it would be a good idea to get the following
essay published in as widely read a journal as possible.
it appeared in ihe second issue of a new coin collecting
magazine, Coin Mart (Winter, 1969). It covers much of the
ground which I later set forth in the article, “Inflation and
the Return of the Craftsman,” but in this case I limited my
comments to the area of coin collecting.]

Prophets who deal in specific predictions are asking for trouble. It is
too easy for people to check out the accuracy of the predictions when
the time comes around for fulfillment. If a group has acted in terms
of a prophecy, then a prophet has created a vested interest group
which has even more reason to examine in minute detail the results.
That is why any sensible market analyst covers his escape route with
phrases like “if we assume the continuance of... .” and “should there
be no interference with...” An unqualified prophecy is rare, or so
obvious that anyone could have done as well, such as “taxes will
tise in the United States sometime during the next decade.” The
prophet must steer his way between the twin icebergs of the un-
known specific and the mundanely obvious.

With this in mind, I will now make a specific prophecy: the
opportunities for profits in the coin market over the next decade are
numerous, the chances for a serious malinvestment are slight, and
the time to act is now. The qualifications I would have to put on
almost any other type of investment are unnecessary in the coin
market. The real pitfall is greediness; if a man wants.to get rich very
quickly, he must risk getting poor just as rapidly. But a sensible col-
lector—one who diversifies his purchases—-can come up with a reason-
able savings package in the coin market that is unrivaled in terms of
safety and profit potential in any other market possessing a com-
parable ease.of entry for the small-to-moderate investor.

The very wealthy investor can subscribe to expensive financial

183
