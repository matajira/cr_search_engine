Downward Price Flexibility and Economic Growth 99

continuity to cconomic affairs compared to which the “rational plan-
ning” of central political authorities is laughable.

What the costs of mining produce for society is a restrained State.
We expend time and capital and energy in order to dig metals out of
the ground. Some of these metals can be used for ornament or clec-
tronic circuits, or for exchange purposes; the market tells men what
each use is worth to his fellows, and the seller can respond accordingly.
The existence of a free coinage restrains the capabilities of political au-
thorities to redistribute wealth, through fiat money creation; in the di-
rection of the State. That such a restraint might be available for the few
millions spent in mining gold and silver out of the ground represents
the greatest potential economic and political bargain in the history
of man, To paraphrase asother patriot: “Millions for mining, but
not one cent in tribute.”

By reducing the parameters of the money supply by limiting
money to those scarce economic goods accepted voluntarily in ex-
change, prediction becomes a real possibility. Prices are. the free
market’s greatest achievement in reducing the irrationality of human
affairs. They enable us to predict the future. Profits reward the suc-
cessful predictors; losses greet the inefficient forecasters, thus reducing
the extent of their influence, The subtle day-to-day shifts in the value
of the various monies would, like the equally subtle day-to-day shifts
in value of all other goods and services, be reflected in the various prices
of monies, vis-a-vis each other. Professional speculators (predictors)
could act as arbitrators between monies. The price of buying pounds
sterling or silver dollars with my gold dollar would be available on
request, probably published daily in the newspaper. Since any price
today reflects the supply and demand of the two goods to be ex~
changed, and since this in turn reflects the expectations of all par-
ticipants of the value of the items in the future, discounted to the
present, free pricing brings thousands and even millions of forecasters
into the market. Every price reflects the composite of all predictors’
expectations, What better means could men devise to unlock the
secrets of the future? Yet monetary centralists would have us believe
that in monetary affairs, the State’s experts are the best source of eco~
nomic continuity, and that they are more efficient in setting the value
of currencies as they relate to each other than the market could be.

What we find in the price-fixing of currencies is exactly what we
find in the price-fixing of all other commodities: periods of in-
flexible, politically imposed “stability” interspersed with great eco-
nomic discontinuities. The old price shifts to some wholly new, wholly
unpredictable, politically imposed price, for which few men have been
able to take precautions. Itis a rigid stability broken by radical shifts to
