Chapter V

DOMESTIC INFLATION
VERSUS INTERNATIONAL SOLVENCY

[This essay was my first published piece in a national maga-
zine, Nothing that has happened since it was first published
has convinced me that its analysis is faulty, Naturally, I was
gratified that the editor received a letter, shortly after its
publication, from Prof. Jacques Rueff, of the Institut de
France, expressing his appreciation of the piece. However,
I was most gratified when a slightly modified version of the
essay won second prize ($1,060) in the national essay contest
sponsored by Constructive Action, Inc., in 1967. In terms of
monetary profit, my first essay still stands as my best one.]

In recent months there has been an increasing amount of dis-
cussion concerning “international liquidity,” “balance of payments,”
“dollar glut,” gold outflow, and monetary stability. Economists,
even when they agree on the nature of the problems involved, seldom
are unanimous on the solutions. The debates that are going on
among economists, bankers, and politicians are frequently phrased in
highly technical and abstruse language, but the basic issue is simply
this: how can nations continue to inflate their domestic currency
and credit systems, and at the same time preserve mutual trust in
each other’s solvency?

The “ideal” economic world, in the view of many of our leading
economists, is one in which we would have “freedom for each
country to pursue its own independent economic policy unhampered
by balance-of-payments considerations; and stability of [monetary]
exchange rates to encourage international relations.’ Unfortunately,
as the author hastens to add, “the two are incompatible... .” The
goal of today’s international finance experts, therefore, is to discover
the best compromise possible, the most workable balance between
the two alternatives,

In the context of contemporary economic theory and practice, the

1. Tibor Scitovsky, “Requirements of an International Reserve System,” in
Essays in International Finance, no: 49, November, 1965 (Princeton Univer-
sity’s International Finance Section), p. 3.

53
