Reason, Neutrality, and the Free Market 231

Freedom is necessary for the full flowering of mankind’s capacities.
Society, however, cannot exist if the market makes available the
moral acids that would render social cohesion impossible. Man is
not autonomous; he is a creature under law. The sovereignty of
autonomous, acting man is as diabolical a goal as the sovereignty of
the State, Neither man nor the State is divine.* Both principles are
antithetical to human freedom, for they would result either in the
totalitarian State or the triumph of the war lords. The market cannot
function without some degree of peace, It needs protection from
its own warlike children, the profit-seeking armies and the profit-
making courts. Let any human institution achieve full autonomy, and
society will face cither collapse or tyranny, Full sovereignty, like
perfection, belongs only to God.

14, This was the issue which divided Christians from the Caesars. Cf. Ethel-
bert Stauffer, Christ and the Caasars (Philadelphia: Westminster Press, 1955);
Charles N. Cochrane, Christianity and Classical Culture (New York: Oxford
University Press, [1939] 1957), chaps. 3-6. On the Christian church's opposi-
tion to the ancient world’s deification of the State, see R. J. Rushdoony, The
Foundations of Social Order: Studies in the Creeds and Councils of the Early
Church (Nutley, N. J.: The Craig Press, 1968).
