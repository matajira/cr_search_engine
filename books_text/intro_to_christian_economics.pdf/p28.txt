16 An Introduction to Christian Economics

comes less and less concerned with the responsible and moral
management of his company.4

Furthermore, Rushdoony argues plausibly, “limited liability bas, in
the long ran, assured a greater readiness by corporations to assume
debt.25 Given the presence of fractional reserve banking, this pro-
pensity to borrow adds to the money supply, since ninety percent of
the loan capital is created by the bank out of thin air (if the reserve
requirement is ten percent, as it generally is today for demand
deposits).

The separation between property and ownership, between owner-
ship and management, has no doubt been overemphasized in the last
three decades. So long as there is the possibility of the corporate
take-over, there will be pressures on managers to operate an efficient
firm.1® Nevertheless, there has been an erosion of personal responsi-
bility within the framework of large, impersonal firms, which in turn
has come from government intervention into the operation of these
firms.1’ Limited liability laws are one form of this intervention, and in
the long run it may end, as Joseph Schumpeter has predicted, in the
dissolution of capitalism and the free market. In a moving, terrifying
section of his important book, Capitalism, Socialism, and Democracy,
Schumpeter writes:

On the other band, the capitalist process also attacks its own in-
stitutional framework—let us continue to visualize “property”
and “free contracting” as partes pro toto-——within the precincts of
the big units. Excepting the cases that are still of considerable
importance in which a corporation is practically owned by a
single individual or family, the figure of the proprietor and with it
the specifically proprietary interest have vanished from the pic-
ture. There are the salaried executives and all the salaried mana-
gers and submanagers. There are the big stockholders. And
then there are the small stockholders. The first group tends to
acquire the employee attitude and rarely if ever identifies itself
with the stockholding interest even in the most favorable cases,
ie, in the cases in which it identifies itself with the interest of
the concern as such. The second group, even if it considers its

14. Rushdoony, Politics, pp. 256-257. For a nineteenth century theologian’s
critique of the limited liability corporation, see Robert L. Dabney, Discussions:
Philosophical (Richmond, Va.: Presbyterian Committee of Publications, {892),
TIL, p. 329 f, The main defect in Dabney’s discussion is his hope that further
government intervention can cure the problems caused by the original govern-
ment intervention, namely, the establishment of limited liability laws. But his
general criticism of limited liability is sound: it overstimulates the creation of
high-risk ventures: p. 333 ff. This is the kind of Christian teaching which the
twentieth century Protestant pictists have utterly abandoned.

18. Rushdoony, Politics, p. 260.

16. Cf. Henry Manne, Insider Trading and the Stock Market (New York:
Free Press, 1966), chap. 8

17. North, “Statist Bureaucracy in the Modern Economy,” The Freeman
(Jan., 1970) [chap. 20, below].

  

cae
