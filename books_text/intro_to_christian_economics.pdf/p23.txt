The Biblical Critique of Inflation ii

tion of all debts, as well as the jubilee year, Deut. 1521-6, 12-18:
Lev. 25).

The importance of this law for monetary affairs cannot be over-
stated, Contemporary society—indeed, sacicty since the Middle Ages
—has ignored this restriction on multiple indebtedness with im-
punity. From an economic standpoint, the chief private violators
jostitutionally are the fractional reserve banking system and the
limited Habiliry corporation, The entire public sphere of civil
government rests on the violation of the principle. The whole
structure of modern credit is based upon the idea that men should
never escape from perpetual debt. The public debt of the federal
government, already approaching half a trillion dollars (excluding
future commitments like Social Security payments, bank insurance,
and other “agency” debt), is steadily eroding the monetary unit,
in the process described by the nineteenth century theorist, Charles
Holt Carroll, as “the organization of debt into currency.” or the
monetization of debt.” The central bank of every nation—the
Federal Reserve System in the United Status-—prints up the money
to finance the deficits of the central government, and in return for
this fiat currency, the government piven an interest-bearing bond te
the bank, The Pederal Reserve System receives ahour $4 ion a
year in this way at the present time, and it will go higher as the
(and unsalable government indebtedness) continues. (Phe govern.
ment pays out over $20 billion in interest altogether-~te insurance
companies and other institutional investors, including Incal banks,
as well as to citizens, The FRS returns most of ity interest: pay
ments to the ‘Treasury each year, however.) From a biblical stand-
point, this is utterly corrupt: “The wicked borroweth and payeth not
again” (Ps. 37:21a). The civil authorities do not intend to reduce this
debt and repay the principal. They favor perpetual indebtedness, Laws
that are transgressed in God's universe ill be found Go contain their
own built-in punishment, The French Revolution came when the kine
had to assemble the Estater-General, for only they could raise
needed new taxes, and the interest af the blasted French sativagl
debt was absorbing half the revenues of the kingdom annually.
The British interest payments were about Qe same ia this same
period." It had been the atiempt of the British government ii impose
new taxes on the American colonies that had trhegered the American
Revolution, Massive national Indebtedness is highly dangertus,

 

i, Charles Holt Carroll, The Organization of Debt into Curcency pNew
York: Arno Press, 1971}.

ii. ROR, Palmer, 4 Mistery of the Modern Work! (dnd ets New York:
Knopf, 1965), p. 338,
