166 An Introduction to Christian Economics

like a war, But these rapid and radical decreases in supplies are
historically rare. When we see prices in general rising, then we can
usually be certain that policies of monetary inflation are being pur-
sued by the government and the central bank, In fact, by the time
we can chart an increase in prices, monetary inflation has been in
effect for some time. If we live in a highly productive capitalist
economy, even stable prices imply some monetary inflation; the price
level under high productivity should be slowly falling, as increases
in the number of goods and services take place within a framework
of a constant money supply,

When governments and central banks pay for increased State
expenditures and increased capital outlays by fiat increases in the
supply of money and credit, we all begin to find signs of price infla-
tion. The longer the policies of governmental deficit financing through
monetary inflation go on, the more pronounced the price inflation be-
comes, Thus, the best way to stop the causes of price inflation is to
see to it that governmental deficit financing through monetary inflation
ceases, There is no need to establish a new branch of State bureaucrats
which will spend its time and our dollars in trying to counteract the
effects of the policies of other State bureaucrats. By fighting symp-
toms, namely, price inflation, the State officials merely produce what
the late Wilhelm Roepke called “repressed inflation”: shortages of
goods, rationing problems, and long lines in front of half empty
stores, .

When policies of governmental deficit financing through monetary
inflation are in effect, prices in the aggregate will tend to rise; certain
prices will rise faster than others, e.g, the price of skilled Jabor, espe-
cially in those industries where trade unions wield considerable eco-
nomic and political power. Companies employed directly or indirectly
by the federal government will gain access to newly created fiat money,
and they will compete for labor and other capital goods with those
companies more directly concerned with satisfying private demand.
(The government-related industries will usually win out in these
contests, for obvious reasons.) The cost of key metals like steel and
copper will rise for the same reason. The fact that steel: prices did
not soar in the earlier part-of the decade (and have stayed reasonably
low, given the tremendous wartime demands for steel) was due to
the federal government’s imposition of “voluntary” restraints to hold
down prices.

Some government planners are now alarmed at the effects of their
own policies, They now want to control these effects, putting them
under wraps. Bearing in mind the ancient economic truth that we
never get something for nothing, we should look at the results of
