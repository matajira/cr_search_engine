322 An Introduction to Christian Economics

emption must be granted to other sovereign agencies of government—
social, moral, and cultural government. They should have this same
right, for they, too, have legitimate sovereignty in a world of plural,
limited human sovercignties, No single institution can claim total
sovereignty; only God is truly and absolutely sovereign. Therefore,
if the religion of the State receives its exemption (and therefore its
subsidy}, then the zeligion of God should be entitled to the same
treatment. Those Christians who naively recommend a voluntary
payment of taxes by the churches or Christian schools have not faced
the issue of sovereignty. They add to the growing secularism of the
age, for they promote a non-Christian idea of earthly human sov-
ereignty, one which elevates the State to the throne of total sover-
eignty. That was what the Christians of the ancient world struggled
and died to deny.”

Conclusion

Because of the inescapable threat of confiscation involved in all
schemes of direct property taxation, it is a tax to be avoided on
principle. Rushdoony’s comments are apt:

Basic to the Biblical law of liberty for man is property. When
a man is secure in the possession of his property, he bas an area
of liberty and dominion which is beyond the reach of other men.
If no man and no state can reach in to tax or to confiscate his
property, man can enjoy true liberty and great security, whether he
be prosperous or poor. Every attack on private property is, there-
fore, an attack on man’s liberty, Man’s freedom and security in
the possession of his property is not only basic to man’s inde-
pendence, but it is also basic to his power. A man has power if
he can act independently of other men and the state, if he can
make his stand in the confidence of liberty. Every attack on
private property therefore is also an attack on the powers of free
men as well as their liberty.

It follows therefore that a transfer of property from man to the
state is a transfer of liberty and power from the people to the
state, The necessary way for any state to become powerful and
totalitarian is to restrict and suddenly or gradually confiscate and
abolish private property. No new set of legislators can stop or
stem any state’s march towards total power if they leave un-
touched the state’s power aver property, real property, personal
property, and monetary property. No groups of “reform” poli-
ticians have kept their promises unless they set property free from
statist control and intervention.16

14. Cf. Rushdoony, Foundations of Social Order: Studies in the Creeds
and Councils of the Early Church (Nutley, N. J.: The Craig Press, 1968).

15. Ibid. Ct. Ethelbert Stauffer, Christ and the Caesars (Philadelphia:
Westminster Press, 1955).

1 Rushdoony, Law and Liberty (Nutley, N. J.: The Craig Press, 1971),
p- 65,
