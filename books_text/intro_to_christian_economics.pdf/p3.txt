DEDICATION

This book is dedicated to WAYNE ROY, a teacher who has
always believed in the principle of Christian economics, and to
FREDERICK NYMEYER, a publisher who sacrificed time and
“money to help lay the foundation for such an intellectual recon-
struction,

iti
