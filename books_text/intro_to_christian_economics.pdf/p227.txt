An Outline of Biblical Economic Thought 215

increase beyond that which was lent, either in money or other goods,
could be taken from a poor brother; however, it was legal to take
usury from a nonbelieving stranger.*?

It is affirmed in Deuteronomy 28 that God rewards covenantal
obedience by His people with material blessings. ‘The chapter also
promises poverty for transgression. This dual promise is obviously a
basic pattern in Weber’s thesis concerning the Protestant ethic and
the spirit of capitalism, At least three issues are involved, First,
what is the relationship between communal obedience and communal
prosperity? Second, what is the link, if any, between the individual's
obedience and a personal material blessing? Finally, can prosperity
be regarded as a sign of God’s favor in all cases? If not, what are
the criteria for determining that favor?

God is explicitly seen as the source of wealth: “But thou shalt
remember the Lord thy God: for it is he that giveth thee power to
get wealth...” (Deut, 8:18a). He is also the source of all human
class distinctions: “The Lord maketh poor, and maketh rich: he
bringeth low, and lifteth up” (ISam. 2:7), The heart of the religious
aspect of Weber's thesis*—that obedience. to God was seen by
Protestants as resulting in wealth, a wealth which later is sought for
its own sake—is found in the Mosaic law:

 

forbidden to take advantage of his neighbor's penury, and make gain of his
distress.” Milton Evans, p. 278. Cf. MacDonald, p. 80 ff.

12, Ex, 22:28; Deut. 23:19-20; Lev. 25:35-36. A problem seems to exist.
Who is stranger and who is brother? The first two passages permit the taking
of usury from strangers and sojoumers in the land, but the last passage reads
differently: “And if thy brother be waxen poor, and fallen in decay with
thee; then shalt thou relieve him: yea, though he be a stranger, or a sojourner;
that he may live with thee. Take thou no usury of him, or increase: but fear thy
Gad; that thy brother may live with thee,” It should be noticed that a mutually
shared catastrophe is involved: the brother has “fallen in decay with thee.”
The situation may be comparable to the one asserted by J. Coert Rylaarsdam,
in which an advance of wages is in question: “The real point is that in his
relations with a poor man, possibly his own employee, an Israelite must be
generous... . The original admonition [Ex. 22:25-27] was not so much a
prohibition on interest as that one be ready to ‘risk an advance’ without ma-
terial security.” The Interpreters’ Bible (New York: Abington, 1952), I, 1008.
It is also possible that “brother” refers to a converted foreigner who bas not
yet become a full member of the Hebrew commonwealth. Fuil citizenship took
an Egyptian or an Edomite three generations of covenantal family confession
to achieve; it took a Moabite ten generations (Deut. 23:3,7,8)}. ‘Thus, a man
could be a spiritual brother and still be an official stranger and sojourner. The
fact that Ruth could enter the commonwealth, and even be fisted in the
genealogy of Christ, is a testimony to her exceptional faith; this was not the
normal practice for a visiting Moabite (Matt. 1:5). os

13. Tam distinguishing here between the more familiar religious aspect of
Max Weber’s famous thesis on Protestantism and capitalism, and the real focus
of his studies, namely, the process of rationalization and bureaucratization in
Western civilization. Cf. Ephraim Fischoff, “Che Protestant Ethic and the
Spirit of Capitalism: The History. of a Controversy” (1944), in’S, N. Eisen-
stadt (ed.), The Protestant Ethic and Modernization (New York: Basic Books,
1968); Herbert Luethy, “Once Again: Calvinism and Capitalism” (1964), ibid.
