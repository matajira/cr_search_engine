Chapter XU
INFLATION AND THE RETURN OF THE CRAFTSMAN

{I could not find a publisher for this in the conservative press,
so I sent it to the incredible magazine (now defunct—the
only one I ever saw which folded, voluntarily, in the midst of
fantastic popularity), The Whole Earth Catalog (Jan., 1971),
Amazingly, some of my radical acquaintances on the campus
and off found it persuasive. I received a letter from one lady,
obviously a hard-working “proletarian” of relatively litile
formal education, who said her college sentor daughter had
given it to her and had said, “This is what you've told me for
years, but I never believed it before.” Unfortunately, truth
is not always its own best testimony in our world; where it
appears or who says it is what counts. But some of our “hip-
pie” citizens have more economic sense than some of our
presidential advisors on the Council for Economic Develop-
ment.

I wrote this in mid-1970. Obviously my timing was off. I had
expected the imposition of wage-price controls in 1973 or
1974; Nixon beat me to it. So far, my predictions are accu-
rate; no controls on art objects, collectors’ items, used goods,
and small businesses. Now, let's see if the rest of my
prophecy comes true. I fear that it will. So does Murray
Rothbard: “The End of Economic Freedom,” The Liber-
tarian Forum (Sept., 1971), a superb study of the controls.]

Prophets are seldom popular men, even when they predict cor-

rectly, In the arca of economic forecasting good men are often
frighteningly wrong. Thus, the man who claims to know the future
is taking a considerable risk: if he is wrong, he will look like a fool;
if he is right, everyone will hate bim (at least if he has predicted hard
times). Nevertheless, every man has to be a bit of a prophet if he
is to survive. There is no way of escaping personal responsibility;
men must plan, at least to some extent, for their economic futures.
Jf that future brings what I am fully convinced it must bring, a lot of
white collar professionals are going to be disappointed, and a lot of

hobbyists are going to reap very substantial economic rewards.
America’s greatest economic bugaboo is depression, The memory

169
