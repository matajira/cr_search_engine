Gresham's Law Revisited: The Coin Crisis of 1964 177

will be changed. But the free market’s ratio between silver and gold
can never be absolutely permanent. Like all other commodities,
money metals arc regulated by the forces of supply and demand.
New gold or silver deposits are constantly being discovered; new min-
ing and smelting processes make production cheaper for one metal
or the other. For the sake of argument, let us imagine a situation in
which several new gold mines are discovered. Gold begins to flow
into the economy. With no increase in demand, the additional sup-
plies of gold will force down the value of any given ounce of gold.
If the market were truly free, prices in terms of gold would therefore
rise. Now, perhaps, the market would establish that an ounce of
gold is only worth 15 ounces of silver, rather than the older rate
of one ounce for 16. The ratio is no more fixed than the ratio
between gold and automobiles or silver and filet mignon. Neverthe-
less, the government insists that the old ratio be maintained. Anyone
wishing to sell some item for one ounce of gold must also demand the
full 16 ounces of silver. Yet the item is really worth only 15 ounces
of silver. Anyone who wishes to purchase an ounce of gold with his
silver must pay the full 16 ounces, when he wants to pay only 15. As
a result, gold, which is now overvalued by the state, goes begging for
silver in unsold surplus, while silver becomes scarce and disappears
from circulation. Silver disappears to another country or area where
it can be exchanged at the free market price, and gold, in turn, flows
into the country. If the bimetallism is world-wide, then silver dis-
appears into the “black market,” and official open exchanges are made
only in gold.

Tt should not be difficult to understand why this is so. [f an indi-
vidual has an opportunity to buy some good, and he can pay for it
with an ounce of gold (actually worth 15 ounces of silver), he is
unlikely to pay for the item with the 16 ounces of silver which the State
demands that he pay; he will spend the ounce of gold. He will hoard the
silver, or, if he should have the opportunity, he will purchase something
from abroad where there is no requirement that he pay 16 ounces for
an item worth only 15. The silver will consequently have a tendency
to go into hoards or out of the country.

Another process occurs at the same time and-for the same reasons.
Foreign banks and governments will observe that the bimetallic nation
has guaranteed all holders of gold to exchange that gold at the old 16
to one ratio, The national Treasury stands behind this promise,
Foreigners can then take 15 ounces of silver to the free market of their
country and purchase an ounce of gold. They can now take the
ounce of gold and purchase 16 ounces of silver at the first nation’s
Treasury. A nice profit of one ounce of silver has been made on the
