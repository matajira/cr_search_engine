Chapter XXIV

THE FEMININE MISTAKE:
THE ECONOMICS OF WOMEN’S LIBERATION

[This is by Jar my most widely read economic essay. Re-
printed in Human Events and in The Christian Observer, it
drew the fire of the Director of the Women’s Bureau of the
U.S. Department of Labor’s Workplace Standards Admin-
istration, She tried to argue, in a letter sent to (but not
published by) Human Events, that the enforcement of the
Federal Equal Pay Act of 1963 has been effective in “a wide
range of industries.” Over $25 million in illegal underpay~
menis due to wage differentials have been found, she says,
between June of 1964 and December of 1970. Some 68,000
employees were supposedly affected. This, it should be
pointed out, is a tiny fractional percentage in a country in
which one-third of the labor force is composed of women,
and wage payments total in the billions each month. What
the letter proves, and the accompanying documents prove,
is that (thank heaven) the enforcement of the absurd pro-
visions of the Federal Equal Pay Act is simply not having
any measurable impact in the American economy. It may
have impact, unfortunately, very soon: U.S. News and World
Report (Dec. 11, 1972).

What I wrote concerning women is only an elementary ap-
plication of economic analysis. It is a minor ease of the more
general analysis of the effects of minimum wage laws. Yale
Brozen and Henry Hazlitt, plus a small army of graduate
students, have demonstrated beyond a shadow of a doubt
that minimum wage laws are, if enforced, causes of unem-
ployment. To that extent, they produce poverty, If you like
poverty, you'll love minimum wage legistation.]

I first read about the Women's Liberation Front in the spring of
1969 in a copy of New York, a new magazine devoted to the crucial
problem of bow to survive in New York City. That description of
WLF opened with an account of a young heiress demonstrating
karate as one of the basic skills needed for her survival, At the time
I was inclined to dismiss the WLF as just another of the freakish
movements that seem to flourish in alienated urban cultures, or in
the educated segments thereof. But in recent months I have come
to the conclusion that the WLF is important, and that it is dangerous.

277

   
