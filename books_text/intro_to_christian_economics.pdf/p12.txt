xit An Introduction to Christian Economics

universe, Christianity claims that the very aim and method of
science require these doctrines as prerequisites... . Christian
apologetics cannot be indifferent to a system of philosophy or of
science which, by its presuppositions and implications, as well
as by its open assertions, rejects the doctrine of the ontological
Trinity, the doctrine of creation, the doctrine of the fall of man
and of his redemption through Christ, On the other hand Chris-
tian theology can well afford to offer lend-lease assistance to
such systems of philosophy and science as are consistent with
these doctrines,?

The essays that follow are repetitions, duc to their publication in
numerous periadicals over several years, Repetition, as I found in
teaching, seldom loses a reader, and the failure of students to get
things straight the first time is humbling to an instructor. The person
who knows the structure of an argument, and who can predict what
the next point in an argument is likely to be, is a person who has
grasped the author’s theory, If he can reproduce the argument a
year later, he has been educated. Most people, on most topics,
are uneducated.

The reader will note how heavily I rely on an exegesis of Isaiah 1:22
for my critique of inflation. Years after | wrote my original exegesis.
I discovered that a very similar analysis had been made by Hugh
Latimer, in 1549, in a sermon delivered before the young King Ed-
ward VI# Thus, my “revolutionary insight” is rather old fashioned
after all,

7 Loppelins Van 'TH, Apolagetics (Syllabus, Westminster Seminary, 1959),
pp. 24, 25.

8. William Letwin, The Orivins of Scientific Beonomics (Garden City, N. Yo
Doubleday Anchor, 1965), p. 85 f.

 
