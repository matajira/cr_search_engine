Ownership: Free but Not Cheap 329

organized institutions possessing the right of legal compulsion.
Sovereignty might be claimed strictly on the basis of conscience:
voluntary tithing to a church, for example, or voluntary taxation
by the State (as some market advocates have argued). As a rule,
however, where we find any institution which claims sovereignty
and receives support from a majority of the citizenry, we also find
compulsion. In the United States, the classic example is the shift
in sovereignty from State-established religious denominations, that
once received tax funds to support their operations, to the gov-
ernment educational institutions. The public schools became the
institutionalized churches of the local communities, and recent court
decisions indicate that they are about to become national churches.7
Those who officially denicd that church and State onght to be
linked, in most cases simply substituted a new priesthood for the
older one, ie., the one which no longer could convince a majority of
citizens of its claims of sovereignty.*

In modern, industrialized nations, the conflict over sovereignty
is between the State and the market. In the Soviet Union, and
presumably in the other iron curtain countries, the conflict is four-
way: national state, planning region, market, and Communist Party.®
(In Communist China, who knows? Hypothetically, Mao is the
ultimate sovereign, but operationally the whole power structure is
up for grabs. What happens when the sovereign dies is anybody's
guess.) As Mises puts is, the control of scarce economic resources
can be handled in two ways: profit management or bureaucratic
management. Both are legitimate in their own spheres, but in
the modern economy, statist bureaucratic management seems to be
teiumphant everywhere we look.

7. The concept of the public schools as America’s only established church
is brought out forcefully in Sidney E, Mead’s The Lively Experiment (New
York: Harper & Row, 1963), chap. 4; cf. R. J. Rushdoony, The Messianic
Character of American Education (Nutley, N- The Craig Press, 1963).

8. The separation of church and state, it must be stressed, came to the
American colonies quite early, Rhode Island accepted the principle from the
beginning. But orthodox Connecticut was forced to adopt it as a result of the
religions tumult caused by the Great Awakening of the mid-cighteenth cen-
tury; it was brought into existence by Christians, not secularists or the tiny
handful of Unitarians and Deists: Richard L. Bushman, From Puritan to
Yankee (Cambridge, Mass.: Harvard University Press, 1967), chap. 13.

9. Paul Craig Roberts, “The Polycentric Soviet Economy,” The Journal
of Law and Economics, XII (April, 1969); Herbert S. Levine, “The Cen-
tralized Planning of Supply in Soviet Industry” (1959), in Wayne A. Leeman
(ed.), Capitalism, Market Socialism, and Central Planning (Boston: Hough.
ton Mifflin, 1963}; Gary North, “The Crisis in Soviet Economic Planning,”
Modern Age, XIV (Winter, 1969-70) [chap. 22, above]. .

10. Ludwig yon Mises, Bureaucracy (New Rochelle, N. Y.: Arlington
House, [1944] 1969).

11. Gary North, “Statist Bureaucracy in the Modern Economy.” The Free-
man (Jan., 1970) [see chap. 20, above].

 

 
