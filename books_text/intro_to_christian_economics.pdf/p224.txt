212 An Introduction io Christian Economics

The Old Testament Heritage

The Hebrews regarded themselves, above all, as the people of the
covenant. The terms of the covenant were brought before the nation
in the opening linés of the Decalogue: “I am the Lord thy God,
which have brought thee out of the land of Egypt, out of the house
of bondage. Thou shalt have no other gods before me” (Ex. 20:
2,3). This covenant was an archetype of all covenants, Meredith
Kline has argued, and it is paralleled by similar national covenants
between divine monarchs and their subjects in the second millennium,
B.c.t It was this event of the national liberation that proved both
God’s mercy and His power to the Israelites, and it was to this that
the prophets return again and again in their indictments of the way-
ward nation, God, who in times past delivered the people, who estab-
lished His covenant with them, and who brought His blessings to
them, now promises to fulfil the terms of that covenant: transgression
must bring punishment. Covenant-breaking involves heavy penalties.
Nevertheless, He will restore this transgressing people after their time
of troubles, and the new covenant will be inward and perpetual.?

The covenant and the institutions that enforced it served as the
binding force of the nation. Understandably, all interpersonal relation-
ships were seen in terms of it, beginning with circumcision, and all
institutional arrangements were provided with foundations within the
covenantal law structure. The important point for this study is
simply that the Hebrew conception of the rights of property was
unmistakably covenantal. It set boundaries on the power of both
individuals and the State to administer property; above all, it was
familistic. C. L, Taylor has argued that the prophets were fighting
for the right of the family, through its representative head, to retain
the land? Gustave Oehler has provided an able summary of the
Hebrew idea of a theocratic property system:

As the law was concerned for the continued existence of families,
so, too, provision was made for the preservation of the property
on which the subsistence of the family depended. As far as
possible, the inheritance was to be preserved entire. There the

1. Meredith G, Kline, Treaty of the Great King (Grand Rapids: Eerdmans,
1963), chaps. 1, 2.

2. The pattern of the prophetic message—the appeal to the covenant and the
promise of vengeance by God—is seen in the following passages: Isa. 1:1-25;
Jer. 2:1-8; 5; Ezek. 16: Hosea 2:1-22; 4; Amos 2:10-16; Micah 6:1-4. The
promise of ultimate restoration, ‘beyond this period of earthly judgment and
captivity is found in Isa. 1:26 2; 65; 66; Jer. 30; 31; Ezek, 34:22 ff; 36:
24; Hosea 2:13 ff.; Amos Li; Micah 4,

3. Charles Lincoln Taylor, Jr, “Old Testament Foundations,” in Joseph
Fletcher (ed.), Christianity and Property (Philadelphia: Westminster Press,
1947), p. 18. The prophets were fighting for their principles, of course.

 

 

   
