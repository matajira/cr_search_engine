Urban Renewal and the Doctrine of Sunk Costs 307

painstaking master craftsman, is only worth in 1969 what the market
will pay; the quantity of labor involved (which itself is a misapplied
concept from mechanics, since there is no way to measure labor)
is absolutely ixrolevant. The buggy whip does not have value be-
cause of the labor; the labor has value only because of the value
the buggy whip may have on the market. An hour's labor by a brain
surgeon commands a higher price than an hour’s services of a ditch-
digger (in most economic situations, anyway) .

So it is with a factory. The amount of labor invested in its con-
struction is irrelevant, once it is built; the amount of raw materials
invested is irrelevant, too, Once it is built, the factory (like the buggy
whip) must be valued in terms of what it can produce on the market
or by what it could be sold for, either now or in the future. Profit
and Joss will determine what is to be done with the factory, not the
money already invested in its construction. The doctrine of sunk
costs was the inevitable replacement for the labor theory of value.
Today, it is only the Marxist entrepreneur or planner who ignores the
doctrine of sunk costs; the inefficiency of Soviet planning is, in part,
traceable to just this ignorance.

Conclusion

Thus, we should look at any government project with an eye to the
present and the future. The past, because it is past, is economically
irrelevant. Unfortunately, the past is not politically irrelevant: poli-
ticians and bureaucrats may have made specific promises concerning
some project. But that is another issue as far as the economist is
concerned. If it is a question of economic waste versus economic
benefit, the past must be discarded as part of our thinking. Our con-
cern is in getting the greatest possible benefit from the resources
that are available now. For economics, the words of Omar Khay-
yam are most relevant:

The Moving Finger wriies; and having writ,
Moves on: nor all your Piety nor Wit

Shall lure it back to cancel half a Line,
Nor all your Tears wash out a Word of it.
