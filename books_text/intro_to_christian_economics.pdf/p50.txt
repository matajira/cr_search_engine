38 An Introduction to Christian Economics

decline, and the economy also deteriorates. New bankruptcies, soar-
ing prices, disrupted production are everyday occurrences. The price
mechanism is less and Jess responsive to the true conditions of supply
and demand, ie., “true” apart from monetary inflation.

Another fact that is not generally realized is that the price level
may remain somewhat stable while inflation is going on. Just as the
addict can take a small quantity of a drug and still seem normal, so
the productive economy can seem healthy. Both addict and economy
are filled with the foreign matter, whether the signs show or not.
Take away the drug, and both the economy and the addict would be
different. The layman, and a considerable number of economists,
forget that in a productive economy, the general level of prices
should be falling.® If the money supply has remained relatively
stable, the increased supply of goods will force down prices, if all the
goods are to be sold. In fact, the free market should generally be
characterized by increasing demand prompted by falling prices,
with increasing supplies due to increased capital investment. If
prices remain stable, then the economy is very likely experiencing
inflationary pressures, The public has erred in thinking that an in-
creasing or even stable price level is the sign of “normalcy.”

The addict, ironically, recognizes his condition for what it is,
The addicted economy is equally sick, yet its members refuse to face
the issue squarely, refusing to admit that the condition even exists,
They get used to the “junk” and demand greater and greater doses
of it.

4, The Habit Cannot Go on Indefinitely

The addict will usually run out of funds before he can reach the
limit of bis body’s toleration. If he has the funds, and if he escapes
detection, then he will eventually kill himself, Normally, legal and
financial considerations will prevent this.

Not so in the economy’s case. The legal restrictions on the cir-
culation of inflated bank credit are not restrictions at all: they are
licenses, virtual guarantees to permit fraud. Demanding ten percent
reserves is licensing ninety percent counterfeiting, Demanding a
twenty-five percent gold backed dollar is permitting seventy-five
percent fraud. The legal limits are off; the addicted economy can
supply itself almost forever with its phony wealth, It cannot, how-
ever, escape the repercussions,

The addict has greater restrictions upon his actions, but he can
die. The economy does not die, for it is not a living creature, though

9. Ibid, p. 417, Cf. North, “Downward Price Flexibility and Economic
Growth,” The Freeman (May, 1971) [chap. 10, below].
