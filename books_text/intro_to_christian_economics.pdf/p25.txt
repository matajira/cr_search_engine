The Biblical Critique of Inflation 13

adhering to a proportion of 1:10 between cash reserves and out-
standing demand deposits, with 90 per cent of the actual cur-
rency paid in being loaned ont, the bank can, by granting credits,
create new checking accounts (demand deposits) to an amount
nine times greater than that which has been paid into it. It is
clear in this case that the bank, following the same procedure as
a bank of issuc, grants credits not out of preceding savings, but
from additional resources obtained by the creation of credit. To
what extent is a bank capable of creating credit? This depends
on the bank’s liquidity requirements, that is, upon the amount
of the reserve which the bank must maintain to meet the demands
for the conversion of check money into actual cash, This pre-
occupation with the maintenance of liquidity, which no bank can
safely ignore, more or less effectively limits the bank’s power to
create credit. The liquidity requirements of banks fluctuate with
the degree of confidence placed in banks, with the amount of the
payments made to those who are outside the circle of the bank's
regular clients (payrolls, small payments to retail merchants,
farmers, etc.}, and with the turnover of individual bank accounts.
But more significantly, the fluctuations to which bank liquidity is
subject—and pro tanto the fluctuations to which the total supply
of credit is subject-—coincide to a very large extent with the
cyclical fluctuations of prosperity and depression. In a period of
expansion the economy's supply of credit increases, while the
banks’ liquidity is proportionately lowered (credit expansion);
in a period of depression the banks seek greater liquidity and are
forced, in the process, to contract credit (deflation),

It is of great importance that we thoroughly understand the above
relationships, for without such understanding we cannot ade-
quately comprehend the perils and the problems which currently
beset our economic system. Hence, no effort should be spared in
getting to the bottom of these relationships. One way of doing
this is to imagine an economy where all payments are effected
without the use of actual currency. Evidently, in such case, there
would no longer be any limit to the power of the banks to create
credit. The more widely extended is the system of transactions
effected without cash, the greater becomes the power of the banks
to “manufacture” credit. Yet again, we may compare a bank
with the cloakroom of a theatre, In both cases we deposit some-
thing: in the bank, currency and in the cloakroom, our hats; in
both cases in exchange for a receipt which authorizes us to re-
claim what we have deposited. But while the cloakroom em-
ployees cannot count on the theatre-goer’s not presenting his
reccipt because he regards it as just as good as his headgear, the
bank may safely assume that its clients will in fact consider their
Teceipts (1.c., their right to claim their deposits) to be equally as
good as their deposits. A bank is im consequence an institution
which, finding it possible to hald less cash than it promises to pay
and living on the difference, regularly promises more than it
could actually pay should the worse come to the worst, Indeed,
it is one of the essential features of a modern bank that alone it is

 
