366 An Introduction to Christian’ Economics

banking), it will not experience much price inflation.’ In fact, an ex-
panding economy, given a relatively fixed money supply, will produce a
gradually falling price level.? It could fall enough to lower the money
rate of interest (though not the actual rate of interest in terms of
purchasing power). A society could conceivably produce a negative
money rate of interest if the value of the purchasing power of money
were rising at a faster rate than the market’s registered rate of time-
preference plus the risk premium. If you could buy more with the
money received in the future, you might need to ask only for an equal
amount of paper money or coins as a return.”

With this as background to the theory of the interest rate, it should
be easier to grasp the implications of the charitable loan that comes
under the usury prohibition. The lender faces a sure loss on his loan.
First, he bears the risks associated with loans to the impoverished, for
he can ask no extra payment as a risk premium attached to the rate of
interest. Second, he receives back goods in the future, but future goods
are less valuable to a man than the same goods in the present. He
therefore forfeits the use of his goods over time without any compen-
sation. He receives back less-valuable goods, for he has lost the one
thing that creatures cannot restore: time. Third, during inflationary
times, he also forfeits the lost purchasing power-if his loan is one in
terms of paper money, as it would normally be. He therefore bears two,
and possibly three, costs of the loan. That is the extent of his charity.
He suffers a loss for the sake of his needy brother. This loss is re-
quired of him by God.

Stewardship, Investment, and Charity

The concept of Christian stewardship is a fundamental tenet of
the Christian social order. The Bible declares that God is the sovereign
owner of all His creation? He delegated the responsibility for the

5. Currency debasement is prohibited by Isaiah 1:22; cf. Gary North, “The
Sin of Debased Currency,” Christian Economics, Oct. 31, 1967, p. 4. Fractional
reserve banking is prohibited, since it is a special manifestation of multiple in-
debtedness——-more debts outstanding than resources to meet those obligations on
demand if all are presented simultaneously. Multiple indebtedness js prohibited
by Ex. 22:25 ff: the cloak taken as collateral by a lender cannot therefore be
used by the borrower fo obtain loans from other people,

6. Gary North, “Downward Price Flexibility and Economic Growth,” The
Freeman, May, 1971 {chap. 9 above}. Cf. Mises, The Theory of Money and Credit
{New Haven, Conn.: Yale University Press, [1912] 1953) p, 417; F. A. Hayek, Prices
and Production (London: Routledge & Kegan Paul, [1931] 1960}, p, 105.

7. Governments are always inflating the money supply, so this is not a state-
ment subject to historical verification in modern times. However, the rate of
interest on almost risk-free federal bonds during the 1930's fell as low as one-half
of one percent in the United States. With falling prices, increasing unemploy-
ment, failing businesses, money increased in-purchasing power. Thus, the money
tate of interest fell to almost zero. It was considered safer to buy a government
bond than to hold cash by many investors.

8. Ley, 25:23: Ps. 24:1; 50:10-12; Hag. 2:8 Cf. Gustave Gehler, Theology
of the Old Testament (Grand Rapids, Mich.: Zondervan, 1883), p. 235; Milton
