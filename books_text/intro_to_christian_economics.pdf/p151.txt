Gertrude Coogan and the Myth of Social Credit 139

worst. Nothing could be farther from the truth than to regard this
pook as a statement of a conservative casc for honest money.

Money Creators

The second book to be considered is her study, Money Creators,
published in 1935. It is more historical in approach, and for that
reason it is considerably longer than Lawful Money Explained. It
contains no statement of “first principles” and is therefore even less
of an economic investigation than is the other, if such a thing could
be imagined. The lack of any systematic statement of her economic
principles does not exempt the book from any of the fallacies reviewed
eaflier; it only makes them less apparent, It helps to hide her lack
of economic reasoning. The book has little to say about economics
or economists; it is based ostensibly upon juridical law rather than
on economic law, the latter concept which she scems to scorn. The
book’s starting point is the Constitution of the United States.

It must be recalled that the Constitution was written in 1789, in an
age of little that could be called modern economic science, In fact,
if we are to take Coogan seriously (sometimes a rather difficult thing
to attempt), economics was no science at all in those days, Econo-
mists were all mere tools of the international banking establishment,
and Adam Smith was in the same camp as was Adam Weishaupt, the
founder of the revolutionary secret society, the [luminati.24 Never-
theless, Coogan accepts as the absolute standard of economic truth,
Article I, Section 8 of the Constitution, in spite of the fact that if the
men who wrote it had read any economist at all, they had read Adam
Smith. This standard of reference, which Coogan and all American
Social Credit writers regard as absolute, is that Congress shall have
the power “To coin Money and regulate the Value thereof, and for-
eign Coin, and fix the Standard of Weights and Measures.” That is
whai the Constitution says, unquestionably. Unfortunately, the Social
Crediters who cite it haven't the slightest idea what it means.??

Paul Bakewell, a conservative lawyer whose works on money ate
very good, does understand what it means, since he understands
monetary theory and American legal history. Since this “lawful
money” argument is at the very center of Social Credit analyses,
Bakewell’s research is extremely important, for it destroys the in-
accurate legal scholarship which undergirds Social Credit. He han-
dies the argument in his refutation of error No. 7, in his 13 Curious
Errors About Money (Caxton). In 1850, before the Supreme Court

 

21, Gertrude Coogan, Money Creators (Hawthorne, Calif.: Omni, [1935]
1963), pp. 205-207.
22." Tbid., p. 8.
