Chapter XII

PRICE-WAGE CONTROLS:
EFFECTS AND COUNTER-EFFECTS

[The Commercial and Financial Chronicle published this es-
say, exactly two years (minus one week) before the day Presi-
dent Nixon announced the imposition of price and wage
controls. The essay covers the basic ground which I have
written about in several of my “practical” economics pieces.)

For the past few months we have been hearing serious discussion
by top Administration officials concerning the possibility of the im-
position of across-the-board price and wage controls by the federal
government. If this is not done, and if taxes are not increased to
cover expenditures by the federal government, we are told that the
nation will find itself on an inflationary spiral of dangerous propor-
tions. The passage of the extension of the income tax surcharge may
remove the topic from the political arena, at least for a time, but the
fact that it was even proposed is significant.

The necessity of imposing such controls implies at least two ques-
tionable assumptions: first, that the best way to battle the effects of
governmentally sponsored inflationary policies is to expand even
further the scope of government intervention; second, that wage and
price controls can, in fact, deal effectively with the causes of inflation
{rather than the outward symptoms), There are very good reasons
for rejecting both propositions.

Inflation is defined in several different ways, depending upon the
question under discussion and the economist discussing it, For pur-
poses here, I want to break down inflation into twa components:
monetary inflation and price inflation. Monetary inflation is simply
an increase in the money supply. Here is the root cause of price in-
flation. Price inflation is an effect which occurs when the money
supply is increased and/or the velocity of money (turnover) in-
creases at a rate faster than the increase in the aggregate supply of
goods and services on the market.

Obviously, the general price level can rise without monetary in-
flation; the supply of goods can be reduced by some national disaster

165
