248 An Introduction to Christian Economics

other forms af total authority to be found in society, all of them
in the liberal or democratic political order as well as in the totali-
tarian, I refer to such patterns found in the asylum, the prison,
the military organization, and, from the point of view of the smal{
child at least, the family. In each of these the authority of the
organization—what we referred to in the preceding chapter as a
“closed aggregate”—is total in its reach, fully encompassing the
individual’s life. Within each of these patterns there is, in effect,
a disappearance of limits to authority except those that are set by
the organization itself, The individual, plainly, is not free to with-
draw from the prison or asylum or military organization until his
term is up, even if its authority comes ta seem oppressive, And,
within the organization, he is subject to the full sweep of its
authority.*

Or, as it was beautifully put in a cartoon showing a confrontation
between a father and his teen-age son: “But Dad, Mom volunteered
for this outfit. I was drafted!” Unless the family is in the process
of collapse, the parents are in possession of the rights of control:
allocation of economic assets, allocation of family tasks, family dis-
cipline. Where this “internal socialism” is not present to a con-
siderable degree, the family in question is in very serious danger.

Does this mean that Christians favor the establishment of basically
socialistic institutions? Of course it does. If a society does not
establish a multitude of such institutions, it will see the establishment
of that single overarching socialist institution, the totalitarian State.
Christians favor the establishment of such bureaucratic and internally
socialistic agencies as police forces (preferably local) and armies
(preferably small and professional). They favor the establishment
of churches, libraries, schools, and other institutions that need not
always be operated on the basis of profit and loss statements, We
desperately need more externally voluntary but internally bureaucratic
institutions to act as buffers against the expanding sovercignty of the
totalitarian State. Most of all, we need strong families,

To criticize the family because it possesses features that are re-
pulsive. when found in the institution of the State is as misguided
an effort as the criticism of the State because it does not function as
a family. Radical libertarians tend to practice the first form of
criticism, while socialists are in the second category. Both fallacies
rest on the same error: the inability to understand that different
human institutions have different functions, different structures, differ-
ent means of financing, different strengths and weaknesses, different
laws governing them. Obviously, the family is not the civil govern-

3. Robert A. Nisbet, The Social Bond (New York: Knopf, 1970), p. 134.
