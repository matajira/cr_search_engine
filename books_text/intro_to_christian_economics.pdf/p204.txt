192 An Introduction to Christian Economics

from nickels and dimes, Besides, how many machines still use
pennies? When was the last time you could buy something, other
than a few minutes of parking space, with a penny?

All of this speculation by the Treasury officials is pure “flack” (the
newspaper term for public pronouncements). The phenomenon
which the Treasury is facing is a very old one. The real explanation
of why the pennies are disappearing is tied to an economic law gen-
erally associated with the name of Queen Elizabeth I’s economic
advisor, Sir Thomas Gresham.

In an earlier article, I surveyed the causes of the silver coin short-
age of 1963-67. The same phenomenon was recognized by Gresham
in the late 16th century, although he failed to explain it, namely,
that “bad money drives goad money out of circulation.” Later econo-
mists were able to explain why this is so: it is still true in 1970.

A monetary unit which is overvalued by the State will drive any
undervalued unit (undervalued in terms of what free men believe it
to be worth in their voluntary economic transactions) into hoards or
out of the nation. People will not spend a coin like a silver dollar
on some item which costs a dollar if the value of the coin’s silver
content exceeds a dollar. They will hoard the coin, or. sell it to a
silver smelter at a premium, or sell it to another collector at a
premium, or spend it in some foreign country that recognizes the
increased value of the coin. They will spend only the lower valued
coins, or paper money, on the regular markets. Thus, the “bad
money” drives the “good money” (hard currency) out of circulation.

Gresham’s Law goes into effect when the State’s policy of mone-
tary inflation (spending more money than taxes bring in, and cover-
ing the difference with fiat paper or credit money) is in operation.
The more plentiful the unbacked money is, the less valuable each
individual paper bill is (supply and demand). Prices in terms of the
paper currency rise; prices of gold, silver, and copper also rise,
assuming that price controls are not imposed (and if they are, short-
ages of the metal appear). When the value of the metal content of
the coins exceeds the face value of the coins, they go out of circulation.

The penny is next on the timetable for departure. When the market
price of copper climbs to $1.54 per pound, the penny will become a
“full-bodied coin,” that is, the value of its metal content will be
equal to the value of its legal purchasing power, one cent. When
the price of copper exceeds $1.54 per pound, it will become profitable
to hoard the coins. For the more tcchnically-minded reader, I
include the calculations provided to me in a letter from Eva Adams:
“The 1-cent coin weighs .1 troy ounce or 3.11 grams and is of an
alloy of 95 percent copper and 5 percent zinc. Therefore, each
