32 An Introduction te Christian Economics

good, but the matter never rests here. The treasury officials realize
what the banking leadership realized hundreds of years ago, that few
people ever call for theit gold or silver, Those who do are normally
offset by new depositers, and so the vast stores of money metals are
never disturbed. The paper IOU notes are easier to carry, store more
easily, and because they are supposedly one hundred percent redeem-
able from supposedly reliable institutions, these notes circulate as
easily as the gold or silver they represent, perhaps even more easily,
since the paper has so many useful properties. These paper notes
have the character of moncy—they are accepted in exchange for
commodities.

Treasury officials now see a wonderful opportunity for buying
goods and services for the government without raising visible taxes:
they can print new bills which have no gold reserves behind them,
but which are indistinguishable from those treasury notes with one
hundred percent reserves. The new, unbacked notes act exactly the
same as the old ones; they are exchanged for commodities just as
easily as the honest LOU notes are, Governments print the notes in
order to increase their expenditures, while avoiding the necessity of
raising taxes, The nasty political repercussions associated with tax
increases are thereby bypassed. The State’s actions are motivated by
the philosophy that the government can produce something for noth-
ing, that it can create wealth at will, merely through the use of the
printing press. Government attempts to usurp the role of God by
becoming the creator of wealth rather than remaining the defender
of wealth. The magic of money creation, in its modern form, makes
the earlier practice of metal currency debasement strictly an ama-
teurish beginning. If private citizens engage in paper money creation,
it is called counterfeiting, if governments do it, it is called progressive
monetary policy. In both cases, however, the end is the same: to
obtain something valuable at little expense. The result is the same:
inflation, In the private realm, with the notable exception of banking,
counterfeiting is prosecuted by the government because it is theft,
but in the public sphere it is accepted as a miracle of enlightened
statesmanship.

Banking practice is quite similar to treasury policy, and the State,
realizing that the banks are an excellent source of loans, permits and
even encourages bankers to continue this fraudutent counterfeiting.
A bank, assuming an enforced legal reserve limit of ten percent
(which is about average), can receive $100 from a depositer, per-
mitting him to write checks for that amount, and then proceed to loan
$90 of this money to a borrower, virtually allowing him to write
checks on the same deposit! Presto: instant inflation, to the tune of
