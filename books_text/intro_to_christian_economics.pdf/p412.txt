400

City of God, 83

Civil War, 97, 229

Class, 324, 365; see also future-
orientation

Climate of opinion, 75

Club of Rome, 80n

Coercion, 33, 226, 228, 230, 247,
253, 309, 315, 336, 338; see
also power, sovereignty

Coins, 5£, 21, 98, 130f, 138, 155, 179f,
185, 204; see also free coinage,
gold-coin standard.

Coins, fullbodied, 180, 185, 190, 192f

Cold War, 80, 225

Committee on Monetary Research &
Education, 1081

Commodity money, 121f, 129

Common grace, 354

Communications, 97£

Communism, 222, 229, 247, 330

Competition, 34if,-41, 61, 89f, 238f,
279f, 343

Competition, price, 281, 285f, 292

Confidence, 51, 57, 142f, 179

Congress, 135, 140

Conservatism, 125, 226f, 247f, 3568,
359

Conspiracy, 124, 154, 162n

Constitution, 44, 96, 134f, 139f, 227,
229, 342

Consumer sovereignty, 61, 66

Continuity, 73, 89, 99, LO2£, 115, 335;
see also stability

Contract, ix, $11, 220, 254, 382.

Controls: see price controls

Convertibility, 49, 51, 56, 72, 119,
346; sze also bank run

Cooperation, viii; see also trade

Copper, 196

Corporation, 15ff

Cost, 8, 36, 42, $4, 61f, 94, 99, 117,
254, 258,279, 281, 296, 299,
332ff, 338

Cost of living, 22, 24; see also infla-
tion (price), price level, statistics

Cast-pius, 238

Costs, sunk, 301ff

Counterfeiting, 6i, 32, 36f, 42, 135,
14bf, 147, 152, 201, 206

County, 310, 313, 317, 321, 324

Courts, 230f, 237, 273

Covenant, 212f

Crack-up boom, 39, 63, 68,
also inflation (imass}

Crafis, 174, 345

Credit, 9f, 39, 65, 218

Curse, vi ff, 218

Cycle; see boom-bust, trade cycle

187; see

Data, 261, 263, 314; sze also calcula-
tion, statistics

Dayton High School (Texas), 290

Debasement, ix, 6ff, 41, 156, 176, 391;
see also inflation (monetary)

An Introduction to Christian Economics

364f, 383,
Debt, monetization of, 1, 24, 33, 39,
37, 166, 17
Recentralization, 103, 264f, 267
eflation, 39f; see also bank run,
baom-bust, depression, trade
cycle
Degrees, academic, 174, 288ff, 2941
Deism, 3290
Demand curve, ix
Demand, effective; sce effective
demand.
Democracy, 20, 324f
Depression, 13, 24, 26, 28, 39f, 55,
59, 67, 102, 144, 151, 156, 170,
188, 346; see also boom-bust,
trade cycle
Devaluation, 26, 57, 98, 100, 116, 175
Discontinuity, 115; see also continuity
Discrimination, 280ff, 285, 320
Division of labor, viii, 331; see also
trade
Divorce, 249
Doctorates; see degrees (academic)
Dross, 4, 6: see Scripture Index:
1:2:

Debi, 8, 16, 56, 136, 166, 181, 221f,
s 390£

Isa. 1:
Drugs, 228, 250; see also heroin

Ecology, 94

Econometrics, 109; see also method-
ology

Economic law; see law (economic)

Education, 173, 240, 246f; 288ff;
298-4, 324) 329

Effective demand, 54, 135, 156n

Efficiency [defined: 279], ix, x, 36, 46,
61, 89, 96, 98, 102, 235, 262f,
267, 313, 334, 344

Elite, 286, 292, 300; see also planning
(state)

Eminent domain, 308f, 319

Empiricism, 79; see also methodology

Employment, 54, 65, 136; see also
depression, full employment, min-
imum wage laws

Emiployment, fair; see fair employ-
ment, discrimination

England. it, 56

Enlightenment, 74, 83, 2290, 328

Entrepreneur; see forecasting

Epistemology, ix, 69, 71, 76, 91: see
also methodology, presupposition

Equat Pay Act, 277

Equality, 217, 237

Equity, 71, 306

Eternal return, 83

Ethics, ix, aM 4, at 68, 103, 198, 202,
207,

purodovian, 25, 09

Exchange; see trade

Exchange rates; see fixed and floating
exchange rates
