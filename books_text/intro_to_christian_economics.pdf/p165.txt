Gertrude Coogan and the Myth of Social Credit 153

money at their disposal. These favored individuals and firms can
now go into the market and buy up goods which before they had not
been able to afford. In doing so they deplete supplies and/or raise
the prices of the goods they are buying. Competitors, unfavored by
the government bureaucrats, have no counterfeit bills at their dis-
posal. They had planned their purchases according to the pre-
inflation prices. Now those prices have been raised, or else they
have not been permitted to fall, by the phony Treasury notes, and
the honest competition cannot make the purchases they had planned
on. They. restrict their purchases, cut back on sales, and perhaps
must lower the wages of their employees, They may even be forced
out of business. Why? Because the government has imposed an
invisible tax on these unfavored companies. Counterfeit bills have
raised prices above the non-inflation levels, and people who do not
have access to the counterfeit government bills can no longer buy.
Thus, pensioners and those with fixed incomes are robbed by the
State Treasury officials by the policy of inflation. Government has
not created wealth by the inflation; the State has merely redistributed
wealth, from those with fixed incomes to those who are closest
(chronologically) to the State’s counterfeit money. A few get rich,
while the majority of the public is hurt by the rising prices.

Coogan has looked at the economy from the point of view of a
collectivist, never seeing the way inflation acts at the individual level.
She seems to think that ali people will have equal access to the State’s
“funny money,” but this is not the case. Those receiving it first will
be benefited, since they will be able to buy first at yesterday’s non-
inflated prices, Those farthest away (chronologically) from the
Treasury will pay for the gains of the early beneficiaries by being
forced to restrict their consumption of goods and services due to
the downward inflexibility of many prices. There has been a forced,
arbitrary, somewhat random redistribution of wealth. So when
Coogan says that she is opposed to the forced redistribution of
wealth, and at the same time proposes a “stable price level” that
would require monetary inflation to keep it stable in the aggregate,
she is contradicting herself. One cannot argue simultaneously against
tedistribution and for monetary inflation. The latter produces the
former.®?

When she calls for monetary expansion, she realizes that the
government is trying to avoid raising visible taxes, for she writes
that “Increases in currency would be made partly to defray the
expenses of the Government and in lieu of taxation,’’®* In lieu of

82. Coagan, p. 293, 83, Ibid., p. 251.
