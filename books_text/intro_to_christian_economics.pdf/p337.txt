Property Taxes: Sovereignty and Assessed Valuation 325

it abolishes the property qualification for electors and representa-
tives. .. . Is mot private property ideally abolished when the
non-owner comes to legislate for the owner of property? The
property qualification is the last political form in which property
is recognized.18
Admittedly, Marz’s prophecy has not been entirely borne out in
American experience. We have a wide franchise, but the-drift into so-
cialism was retarded in the nineteenth century and has accelerated dras-
tically only since the 1930’s. We have had serious State intervention for
a century, as Prof. D. T. Armentano’s book, The Myths of Antitrust
(1972), shows so clearly, but not full socialism. There are reasons
for this. We have been, until quite recently, an optimistic, future-
oriented people—an inheritance from first-generation Puritanism and
Great Awakening postmillennial theology’? Furthermore, this fu-
ture orientation has given the American middle classes and even
some of the lower class groups—class here being defined in terms of
income—~an upper class perspective, emphasizing thrift, personal re-
sponsibility, and the legitimacy of one’s work.®° Prof. Aaron Di-
tector’s “law” has also been in effect: “Public expenditures are made
for the primary benefit of the middle classes, and financed with taxes
which are borne in considerable part by the poor and rich,”#4
Then, too, large numbers of poor people refuse to vote, as apathy,
illiteracy, and ignorance take their toll. But as the middle classes have
become infected with guilt feclings, philosophies of wealth redistri-
bution, and a lower-class present-orientation, we have witnessed the
advent of socialistic programs, just as Marx and McCaulay predicted
in the mid-nineteenth century.?? Ideas have consequences. We
cannot halt the march into socialism’s institutional irresponsibility
simply by linking the fanchise to taxpaying, even if such a move were
politically possible. But it would certainly help.

 

18. Karl Marx, “On the Jewish Question” (1843), in T. B, Bottomore
(ed.), Karl Marx: Early Writings (New York: McGraw-Hill, 1964), pp. 11-12.
19. On the postmillennial impulse in Puritanism, see Iain Murray, The
Puritan Hope (London: Banner of Truth Trust, 1971); cf. Gary North, Puritan
Economic Thought: Winthrop to Franklin (forthcoming, 1973). On the Great
Awakening, see Alan Heimert, Religion and the American Mind (Cambridge,
Mass,: Harvard University Press, 1966).
1970) Cf. Edward Banfield, The Unheavenly City (Boston: Little, Brown,
21. Paraphrased by George Stigler, “Director’s Law of Public Income Re-
distribution,” Journal of Law and Economics, XII (April, 1971), p. 1.
22. McCaulay to H. 8. Randall, May 23, 1857: G. Otto Trevelyan, The Life
and Leiters of Lord McCaulay (New York: Harper & Bros., 1875, II, 408-410.
