Repressed Depression 63

Another factor is also present in the interest rate, the risk factor.
There are no certain investments in this world of change. Christ’s.
waming against excessive reliance on treasure which rusts or is
subject to theft is an apt one (Matt. 6:19), High risk ventures will
generally command a higher rate of interest on the market, for ob-
yious reasons. Finally, there is the price premium paid in expectation
of mass inflation, or a negative pressure on the interest rate in ex-
pectation of serious deflation. It is the inflationary price premium
which we are witnessing in the United States at present. Mises’
comments in this regard are important:

It is necessary to realize that the price premium is the outgrowth
of speculations having regard for anticipated changes in the
money relation. What induces it, in the case of the expectation
that an inflationary trend will keep on going, is already the first
sign of that phenomenon which later, when it becomes general,
is called “flight into real values” and finally produces the crack-up
boom and crash of the monetary system concerned,®
The Inflationary Boom

In the real world, money is never neutral (and even if it were, the
economists who explain money certainly never are). The money
supply is never perfectly constant: money is boarded, or lost; new
gold and silver come into circulation; the State’s unbacked money is
produced; deposits in banks expand or contract. These alterations
affect the so-called “real” factors of the economy; the distribution of
income, capital goods, and other factors of production are all in-
fluenced. Even more important, these changes affect people's ex-
pectations of the future. It is with this aspect of inflation that Mises’
theory of the trade cycle is concerned.

The function of the rate of interest is to allocate goods and services
between those lines of production which ‘serve immediate con-
sumer demand and those which serve consumer demand in the future.
When people save, they forego present consumption, thus releasing
goods and labor for use in the expansion of production, These goods
are used to elongate the structure of production: new techniques and
more complex methods of production are added by entrepreneurs.
This permits greater physical productivity at the end of the process,
but it requires more capital or more time-consuming processes of
production, or both extra time and added capital, These processes,
once begun, require further inputs of materials and labor to bring the
production process to completion. The rate of interest is supposed
to act as an equilibrating device. Entrepreneurs can count the cost
of adding new processes to the structure of production, comparing

6 Ibid, p. 541.
