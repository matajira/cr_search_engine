Copyright © 1982, 1987
by Gary North

Second Printing, Revised, 1987
All rights reserved. Written permission must be secured from
the publisher to use or reproduce any part of this book,

except for brief quotations in critical reviews or articles.

Published in Tyler, Texas
by Institute for Chiistian Economics

Distributed by Dominion Press
7112 Burns Street, Fort Worth, Texas 76118

Typesetting by Thoburn Press, Tyler, Texas
Printed in the United States of America

ISBN 0-930464-03-6
