30 THE DOMINION COVENANT: GENESIS

Fundamentalists in the twentieth century have repeatedly accused
dominion-oriented Christians of being in the same camp as the
theological and political liberals, The so-called Social Gospel move-
ment, which arose in the late nineteenth century, was strongly in
favor of social action, economic redistribution, and the elevation of
the powers of the civil government, especially the national govern-
ment. Social action, meaning political action, was subsequently
equated by fundamentalists with the Social Gospel movement. His-
torically, the argument is inaccurate; if anything, the liberal theolog-
ians of, say, 1870-1970, were imitating an older tradition of theological
orthodoxy, especially the tradition of early New England Puritanism
and early nineteenth-century Presbyterianism, both northern and
southern. The Social Gospel was a secularized reconstruction of the
oplimistic, activist, decentralist, conservative Protestant tradition in
the United States. The defenders of the Social Gospel, in effect if not
in theory, removed the sovereignty of God and the validity of God’s
revealed law-order, and then substituted a new god, the State, with
its relativistic law-order.

The twentieth century has witnessed the steady erosion of
confidence among both fundamentalists and liberal theologians. The
First World War created a major transformation in liberal theology,
The optimism began to go out of the movement.® It revived again
during the Second World War, flickered on through the brief tenure
of President John F. Kennedy, and then steadily died out during the
late 1960’s and early 1970's. A growing number of liberal theologians
now share with Protestant fundamentalists a pessimism concerning

 

8. On the optimism of pre-Civil War Southern Presbyterians, see Jack P.
Maddex, “From Theocracy to Spirituality: The Southern Presbyterian Reversal on
Ghurch and State,” Journal of Presbyterian History, LIV (1976), pp. 438-57. See also
James B. Jordan, “A Survey of Southern Presbyterian Millennial Views Before
1930,” Journal of Christian Reconstruction, U1 (Winter, 1976-77), pp. 106-21, In the
North, the faculty of Princeton Theological Seminary, the most influential of the
orthodox Presbyterian seminaries, was noted for its postmillennial optimism:
Archibald Alexander, A. A. Hodge, Charles Hodge, and Benjamin B, Warfield.
9. A classic statement of the pessimism of the theological liberals is Walter
Marshall Horton, Realistic Theolugy (New York: Harper & Bros, 1934). An extract of
this book appears in William R. Hutchison (ed,), American Protestant Thought: The
Literal Era (New York: Harper Torchbook, 1968), pp. 190-96. Reinhold Niebuhr is
the archetype of the shift from optimism to pessimism, and he is the focus of the
book by Donald Meyer, The Protestant Search for Political Reatism, 1919-1941 (Westport,
Connecticut: Greenwood, [1960] 1973). See also Robert T. Handy, “American
Religious Depression, 1925-1935,” Church Histor, XXIX (1960), pp. 3-16.
