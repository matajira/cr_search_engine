The God-Designed Harmony of Interests 97

serve the ends of the higher entity called society or the nation, most
of these features of totalitarian regimes which horrify us follow of ne-
cessity. From the collectivist standpoint intolerance and brutal sup-
pression of dissent, the complete disregard of the life and happiness
of the individual, are essential and unavoidable consequences of the
basic premise, and the collectivist can admit this and at the same
time claim that his system is superior to the one in which the ‘selfish’
interests of the individual are allowed to obstruct the full realization
of the ends the community pursues.”?

What kind of person functions well in such a regime? Not the
person who is best suited to production within a competitive free
market. Or certainly, not with the same outcome of his actions, even
if the same person could perform well under both systems. The
restraining hand of market competition —open entry for his rivals to
meet the needs of consumers—~ is now strictly political in nature,
And in a centralized regime, this is not much restraint. “To be a
useful assistant in the running of a totalitarian state, it is not enough
that a man should be prepared to accept specious justification of vile
deeds; he must himself be prepared actively to break every moral
rule he has ever known if this seems necessary to achieve the end set
for him, Since it is the supreme leader who alone determines the
ends, his instruments must have no moral convictions of their own.
They must, above all, be unreservedly committed to the person of
the leader; but next to this most important thing is that they should
be completely unprincipled and literally capable of anything. They
must have no ideals of their own which they want to realize, no ideas
about right and wrong which might interfere with the intentions of
the leader.”!© The quest for power impels them.

The more powerful the State, the morc concentrated the control
of economic resources available io State administrators, the more
opportunities for economic control through monopolistic economic
manipulation, the more ruthless will be those who satisfy their quest
for power. The bigger the stakes, the more likely the least moral,
most unscrupulous people will claw their way to the top. Why, then,
should we expect to see the flourishing of the harmony of interests in
a socialistic society in which central power is enormously strength-
ened by the fact that the administration of scarce economic resources
is monopolized through public ownership of the means of produc-

9. fbed., p. 149.
10. Feid., pp. 150-1,
