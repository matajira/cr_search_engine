24
THE MISAPPLICATION OF INTRINSIC VALUE

And when money failed in the land of Egypt, and in the land of Canaan,
all the Egyptians came unto Joseph, and said, Give us bread: for why
Should we die in thy presence? for the money faileth. And Joseph said,
Give your cattle, if money fail (Gen. #7:15-16).

Like all of God's relationships with men, famine can be simultan-
eously 4 curse and a blessing. The curse aspect is far easier to under-
stand. The threefold curse promised by God to the Israelites in-
volved the sword, pestilence, and famine (Deut, 28:21-22), The
famine promised by Isaiah was a witness to the “fury of the Lorn,
the rebuke of thy God” (Isa. 51:20b). The people of Egypt were being
placed under a long-term curse in the form of perpetual servitude to
a bureaucratic State. The famine was the means of producing this
servitude. Egyptians would henceforth live externally in terms of the
religious faith which they held: the religion of a divine ruler.

The blessing accrued not to the Egyptians, but to the family of
the house of Jacob. During his journey from Canaan, where he was
still a stranger in the land (Gen. 36:7; 37:1), Israel (Jacob) was spe-
cifically told by God: “Fear not to go down into Egypt: for I will
there make of thee a great nation” (36:3). In Egypt, they multiplied
greatly (Ex. 1:7), even in the face of affliction (Ex. 1:12). A single
family and its covenanted servants (Gen, 36:6-7) became a nation of
600,000 men, plus women and children (Ex, 12:37), in a little over
two centuries, if Courville’s estimate is correct.4

Famine can also be a means of enforcing the cultural mandate.
In forcing the Israelites down into Egypt, the ultimate conquest of

1. Donovan A. Courville, The Exodus Problem and Its Ramifications (Loma Linda,
Calif; Challenge Bonks, 1971), I, p.-151, North, Afoses and Pharaoh: Dominion Religion
vs. Power Religion (Tyler, Texas: Institute for Christian Economics, 1985), Appen-
dix A.

231
