424 THE DOMINION COVENANT: GENESIS

annihilated; scotched, if not slain. But orthodoxy is the Bourbon [re-
ferring to the French monarchy, the House of Bourbon—G.N.] of
the world of thought, It learns not, neither can it forget; and though,
at present, bewildered and afraid to move, it is as willing as ever to
insist that the first chapter of Genesis contains the beginning and the
end of sound science; and to visit, with such petty thunderbolts as its
half-paralyzed hands can hurl, those who refuse to degrade Nature
to the levels of primitive Judaism.”?! His next paragraph begins
with this unforgettable sentence: “Philosophers, on the other hand,
have no such aggressive tendencies.” Why not? “The majesty of Fact
is on their side, and the elemental forces of Nature are working for
them, Not a star comes to the meridian of their methods: their be-
liefs are ‘one with the falling rain and with the growing corn” By
doubt they are established, and open inquiry is their bosom friend.
Such men have no fear of traditions however venerable, and no respect
for them when they become mischievous and obstructive; . . ."202

He knew his contemporary enemics well, He realized clearly, as
they did not, that their hypothesis of continuing spccial creations
“owes its existence very largely to the supposed necessity of making
science accord with the Hebrew cosmogony; but it is curious to
observe that, as the doctrine is at present maintained by men of
science, it is as hopelessly inconsistent with the Hebrew view as any
other hypothesis,”2°3 Darwinian scientists from Huxley’s day to the
present have been able to make the same criticism of later attempts
of Christian scholars to compromise the teachings of Genesis 1 and
evolution. Sadly, Huxley’s barb applies quite well to these profes-
sional academic compromisers: they are likc the Bourbon kings.
They never seem to learn that there can be no successful com-
promise between the. rival cosmologics.

The six-day creation is not a narrow cosmology. It is as broad as
the creation itself and the revelation of that creation given by its
Creator. Evolution and uniformitarian geology (however modificd
the uniformitarianism may be) may appear very broad-minded, but
only in the sense of Matthew 7:13: “Enter ye in at the strait (narrow,
tight] gate: for wide is the gate, and broad is the way, that leadeth to
destruction, and many there be which go in thereat.”

201, Ibid, p. 106
202. Ibid., pp. 106-7.
203, bid., p. 108.
