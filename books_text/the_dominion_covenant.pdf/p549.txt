Index

store of, 233, 233
subjective, 37H, 100f
Van Til, Cornelius, xxix, 325, 350,
382, 431n
atheism, 434
creation, 6
exhaustive knowledge, 2n, 7
facts, 2n, 7
God's plan, 2n, 6f
heresies, 3n
proofs of God, 450n
radio button, 434
reasonable faith, 444
revelation, 6, 434
sovereignty, 446
transcendence, 432f
washing, 64
Vapors, 431
Vegetarianism, 113
Veil of Temple, 445
Victory, 203, 217
Vietnam, 160
Viner, Jacob, 154
Void, 371f
Voyager, 284
Vulcanism, 379, 381, 389

Wage, 224, 227

Wage controls, 232

Wallace, A.R., 260, 329, 395f, 398

Walras, Leon, 41

War, 115, 126, 155
deception in, 196

Ward, Lester F.
biography, 297
censorship, 307f
control, 307
crime, 310
democracy, 310
education, 302, 305f
elites, 304f, 309, 312
influence of, 21
legislators, 304, 311f
masses, 305, 306, 309, 314
morals, 299, 312

  

505

natural laws, 300
nature’s waste, 301
nature vs. man, 299f, 314
population, 314f
religion, 298f
scientists, 309f, 312
Social Darwinism, 21, 299f
social progress, 300
society = state, 312
State industry, 3121
summary, 316
teleology, 299, 301, 304, 306
waste, 298
Waste, 106, 298, 301
Watchmaker, 257
Wealth, 156, 241
character and, 156ff
Christ's remnant, 109
desire for, 206, 208
division of labor, 116, 153
gold and silver, 156
judgment and, 124
transfers, 44, 53f, 142f
Weber, James, 171
Weights, 82
Welfare, 130
Welfare economics, 44fl, 53f
Wells, William, 398
Whipple, Fred, 14n
White, Lynu, 33f, 124
Whitrow, Gerald, 14n
Wigglesworth, Michacl, 386
Wigner, Eugene, 397n, 438n
Wilson, James M., 250f
witness, Appendix E
Wittfogel, Karl, 228
Wolf, 113
Work (see calling, labor)
Worship, 7, 433

YVardstick, 46, 54, 61f
“Ylem,” 374, 418

Young, Davis, 287ff, 385, 420
Young, Edward, 429n
Young, Robert, 259
