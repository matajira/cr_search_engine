464 HE DOMINION COVENANT: GENESIS

pil was in the wilderness (37:22)— another familiar Bible theme.

Robes are given by God or those who are God's lawful subordin-
ates. Adam and Eve wanted to manifest their self-appointed author-
ity as judges, but without robes, they were visibly usurpers. They
had judged God’s word, and by implication, they had to judge God
and execute the sentence. Yet they were naked. A naked judge is not
in a position to render judgment.

Adam and Eve were naked, not because they were sinless, but
because they were children. As they matured, they would have been
given clothing by God as a sign of their maturity, and as a sign of
their authority as judges. Now that they had autonomously and pre-
maturely grabbed judicial authority, they felt compelled to sew cover-
ings for themselves. They were the image of God, and God wears
clothing. He wears the glory cloud. In Daniel 8:9, the Ancient of
Days is adorned in a white robe. In Revelation 1:13, the son of man
has a robe that covers his feet. But God is not a sinner in need of cov-
ering. He is a judge who wears a robe. They, too, wanted to wear
such robes.

It is significant that God in His mercy killed animals and made
coverings for them. He simultancously saved their skins (temporar-
ily) and covered their hides, but only by sacrificing the life of an ani-
mal whose hide became man’s covering. They were covered physic-
ally because of the shed blood of one or more animals. Their physi-
cal shame was temporarily removed from sight. (Ultimately, the
shame of death can no longer be successfully hidden by clothing.)
But this act of slaying the animal pointed to the necessity of the
death of an innocent victim to cover man ethically.

Perhaps they were too ashamed to be seen running for the tree of
life. They may have decided to clothe themselves before heading in
the direction of the tree. They may have believed that with their cov-
erings, they would not be ashamed in front of each other or the ser-
pent; they could always eat of the tree of life after their. coverings
were in place. “First things first.”

Prior to the judge’s rendcring of final judgment, witnesses can
change their testimony, If they have perjured themselves, they must
admit their guilt, but they can avoid the penalty by throwing them-
selves before the mercy of the court. They incur shame, but they avoid the
penalty for perjury. But Adam and Eve would not accept shame.
Rebellious man never docs. They preferred to risk the penalty.
Rebellious man always docs.

 

 
