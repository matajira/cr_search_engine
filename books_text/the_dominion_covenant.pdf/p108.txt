64 THE DOMINION COVENANT: GENESIS

legitimately say anything about aggregates. It cannot make compar-
isons about wealth over time, or wealth across borders, Neither sys-
tem of valuc theory can survive by itself, and the proponents of each
theory borrow liberally from the methodology and conclusions of the
other. As Van Til once remarked in another context, they profit by
taking in each other’s washing.

Conclusion

Point four of God’s covenant structure is judgmeni, or sanctions.
God evaluates His creation continually in terms of His purposes,
decree, and covenant requirements. Men are made in God’s image,
so we necessarily must judge in history. The Bible says that redeemed
mankind will judge the angels (I Cor, 6:3). Life for the covenant-
keeper is a training ground for rendering better judgments. (See
Appendix E: “Witnesses and Judges.”)

In the field of economics, this means that men can and must im-
pute value to scarce economic resources, As creatures made in the
image of God, we can imputc valuc to economic goods. We can
trade with others at discrete prices. These prices arc the product of
competitive bargaining among acting men. We can record such
prices. We can also make rough estimates of aggregates of these prices,
and make rough estimates of the meaning attached to such aggregates
by other acting men. The constant factor in market imputations over
time is therefore the image of God in men, as far as our a! ment of
other people’s imputation of meaning is concerned. The ultimate
canstant is God’s evaluation of worth and His plan. There is objec-
tive value in the universe, and men, to one degree or another, must
conform themselves to, or react against, this standard of value.

Miscs was correct in his attempt to compare the wealth and out-
put of socialist and capitalist nations, just as Rothbard was correct in
concluding that the capital of the Unitcd States would be worth more
than one vat of French wine. But the accuracy of their conclusions is

   

 

 

   

 

in sharp contrast to their presuppositions concerning subjective
value theory. The Bible invites us to make such comparisons. We are
specifically told that the economic productivity of a godly society will
be greater than the long-run productivity of rebellious societies
(Deut. 8:11-18; Ezck. 36). We are able to make such estimates
because there really is a single, consistent, comprehensive plan, and
a single Planner who has made economic assessments in terms of an
omniscient plan. All capital belongs to the ultimate Planner (Ps.
