Basic Implications of the Six-Day Creation 427

stand. More important than tactics, however, is the centrality of the
doctrine of creation to Christian faith. Langdon Gilkey, a neo-
orthodox theologian who does not believe in the verbal, plenary in-
spiration of the Bible, has nonetheless seen the issue more clearly
than most supposedly evangelical theologians. His Maker of Heaven
and Earth announces forthrightly;

Te is quite natural, of course, that Christian devotion and Christian thought
should concern themselves miost with God's redeeming activity in Jesus
Christ, for upon this our knowledge of God as loving Father, and so of our
hope for salvation, most directly depends, Nevertheless, the centrality of
God's redeeming activity to our life and thought should not blind Christians
to the divine work of creation, which, if not so close to our hearts, is just as
significant for our existence and just as important if we are to think righily
about God. Through God's redeeming works we know that He is supremely
righteous and supremely loving. But when we ask whe is: supremely
righteous and loving, the answer comes in terms of God’s original activity,
creation: the Creator of heaven and earth, the Lord, is He who judges and
redeems us. The transcendent “Godness” of God, what gives Him deity and
so ultimate significance to our lives, is most directly manifested to us
through His creative activity as the transcendent source of all being and of
all existence, Without this transcendent aspect of “deity,” the judgment and
love of Gad would be ultimately unimportant te us, and the redemption
promised by them impossible for God. The idea of creation, therefore, pro-
vides the most fundamental, if not the most characteristic, definition of God
in the Christian faith. Among all the activities of God, creation is that ac-
tivity or attribute which sets him apart as “God” (pp. 83-84).

The doctrine of the Trinity—the eternal, infinite, fully self
revealing and communing holy God who is three persons—is the
starting point of Christian theology. But insofar as He has any rela-
tionship with men, the doctrine of creation is absolutely central. The
fact that Gilkey, who is not orthodox, can see this, and evangelicals
do not, testifies to the disastrous effects of syncretism. Christianity
and antitheism cannot be successfully fused without destroying
Christianity.

Creation Defined

The Bible testifies to the fact that a personal God created all
things— matter and energy, structure and motion—out of nothing:
creatio ex nthilo. The opening words of the Bible are concerned with
