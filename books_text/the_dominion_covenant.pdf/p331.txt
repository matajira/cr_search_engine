From Cosmic Purposelessness to Humanistic Sovereignty 287

The modern evolutionist is a defender of a concept of process
that removes God and His control from the universe, so that man
and man’s sovereignty can be substituted for the supposedly non-
existent, God. It is meaningless process which is the evolutionist’s
god of origins. Only when a meaningful God who created the uni-
verse in terms of His eternal, unchanging decree is finally removed from
our thought processes, can our thought processes take control of all
other processes, the modern evolutionist argues. Evolutionary proc-
ess is the humanist’s god of origins, a god whose crucial purpose for
man is to remove from the question of origins any concept of pur-
pose. Man’s monopoly of cosmic purpose is supposedly assured as a
direct result of the non-purposeful origins of the universe. This is
why Rushdoony has taken such pains to contrast process philosophy
and creationism. 48 It is startling, therefore, to read the “refutation” of
Rushdoony written by a self-proclaimed orthodox Christian geolo-
gist (who argues for a 4.6 billion-year-old earth)."!° “Rushdoony’s
fears are unfounded. An affirmation of process in itself certainly does
not constitute an attack on the sovereignty of God. Scripture

118. Rushdoony, The Mythology of Science (Fairfax, Virginia: Thoburn Press,
[1967] 1978), pp. 38-39, 64, Process philosophy, which is basic to all evolutionary
systems, leads inescapably into relativism. The implicit relativism of evolutionism
cannot be reconciled with the implicit authoritarianism of the biblical doctrine of
creation. Rushdoony’s discussion of evolutionism is fundamental: “In this concept,
being is evolving and is in process. Because being is in process, and being is
one and undivided, truth ilself is tentative, evolving, and without finality.
ing has not yct assumed a final form, since the universe is in process and not yet a
finished product, truth itvelf is in process and is continually changing. A new move-
ment or ‘leap of being’ can give a man a new truth and render yesterday’s truth a lic.
But, in an order created by a perfect, omnipotent, and totally self-conscious Being,
God, truth is both final, specific, and authoritative. God's word can then be, and is
inevitably, infallible, because there is nothing tentative about God himself. More-
over, truth is ultimately personal, because the source, God, is personal, and truth
becomes incarnate in the person of Jesus Christ and is communicated to those who
believe in Him, Jesus Cibrist as Lord and Savior, as the way, the truth, and the life,
is also the Christian principle of continuity. ‘The Christian doctrine, therefore, in-
volved a radical break with the pagan doctrine of continuity of being and with the
doctrine of chaos, It also involved a break with the other aspect of the dialectic, the
pagan, rationalistic concept of order. Order is nat the work of autonomous and
developing gods and men but rather the sovereign decree of the oranipotent God.
This faith freed man from the sterile-autonomy which made him the helpless
prisoner of Fate, or the relentless warkings of a blind order.” Rushdoony, The One
and the Many: Studies in the Philosophy of Order and Uliimacy (Fairfax, Virginia: Thobum
Press, [1971] 1978), p. 143.

U9. Davis A. Young, Creation and the Flood: An Alternative to Flood Geology and
Theistic Evolution (Grand Rapids, Michigan: Baker Book House, 1977), p. 87.

 

  

 
