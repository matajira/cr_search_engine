16 THE DOMINION COVENANT: GENESIS

The best a scientist can do is to guess about the state of the universe
a billion years ago, and “the chance that this guess is realistic is negli-
gible. If he takes this guess as the starting point for a theory, this is
unlikely to be a scientific theory but very likely will be a myth. . . .
To try to write a grand cosmic drama leads necessarily to myth.”9 If
he is.correct—and I think he is correct—then why should orthodox
Ghristians try to rewrite the story of the six-day creation in order to
make it seem a bit more respectable, slightly more in conformity to
the latest secular version of the three archetypal cosmic myths con-
cerning origins that happens to be popular at the time?

The third, and by far the most important reason why it is useless
and counterproductive to modify the plain teaching of Genesis 1 con-
cerning the sequence of creation, is that the heart of modern science’s
opposition te this account is not the chronology as such. The reason why
modern science has adopted the ancient Greek accounts of cosmol-
ogy~not the details, of course, but the basic outlines—is that mod-
ern scientists, like the ancient Greeks, are attempting to escape from
the concept of God-ordained purpose. What is most offensive to modern
science is the idea of cosmological purpose prior to the evolutionary advent of
man, The heart of the Bible’s account of the creation is God and His
purposeful word, while the heart of modern evolution is the denial of
purpose, whichever of the secular cosmologies a man decides to ac-
cept. Apparently this fact has not been understood by conservative
Bible expositors who have chosen to rewrite Genesis 1. What we
must bear in mind is that it was Darwin's insistence on the un-
planned, purposeless nature of geological and biological change that
won him instant success in the world of secular humanism. Darwin
denied all the old arguments for divine purpose as a cause of the
orderliness of nature. Natural order proves no such thing, he in-
sisted; natural selection of randomly produced mutations, not super-
natural design, accounts for nature’s orderliness. Evolutionary
scientists accepted Darwin’s denial of cosmic purpose long before
there was any idea that the universe might be ten billion years old.
The heart of the Darwinian intellectual revolution was not evolu-
tion, The central factor was Darwin’s hypothetical explanation of
undesigned order. Jt was his denial of final purpose, of the universe’s
ends-orientation, of éeleology.

9. Ibid, p. 13,
