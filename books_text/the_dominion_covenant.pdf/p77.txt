The Dominion Covenant 33

worth carrying off—were forced out of the land. “None remained,”
the Bible says, “save the poorest sort of the people of the land” (II Ki,
24:14b). Those who could barely exercise dominion stayed; the land
was not deserted entirely. Only in the rare case of the total and ir-
reversible judgment of God against a city was the land to be left to
the rule of nature (Jer. 50:39). This was understood as the ultimate
social curse.

Yet there is another possibility for rebellious man; an attempted
retreat from the responsibilities of ecological dominion. The idea of
ecological romantics, Eastern mystics, and numerous primitive
cultures is that man must live ‘in harmony with nature.” Man must
conform himsclf to the laws of nature. Of course, it is difficult to
determine which laws apply in specific instances, but the idea of the
overall sovereignty or normativity of the natural order is paramount
in these cosmological systems. Man is nothing more than one small
part of an autonomous natural process, but a force for evil when he
allows his powers to take control of nature. Rather than seeing man
as the agent of dominion over nature, these systems place man
under the dominion of nature. Rebellious man, in short, actively
defied God by abandoning his responsibilities under the covenant of
dominion, and in doing so, he eventually becomes essentially passive
before nature or passive before the State.

The Christian acknowledges that man has become a rebellious
destroyer. We know that the whole creation groans to be delivered
from “the bondage of corruption” (Rom. 8:21). The earth is under a
curse because of man. But Christians are “saved by hope” (Rom.
8:24), a hope in God's redemption, not in hope of some hypothetical
return to a natural paradise. Man is indeed a destroyer, an ethical
rebel who seeks release from the comprehensive requirements of
God’s law-order. Nevertheless, “man, the destroyer” is not the result
of “man, the controller”; He is the product of “man, the ethical
rebel.” It is not man’s dominion over the earth that is illegitimate,
but rather man’s attempt to dominate the earth apart from God’s
control over man. The only foundation of man’s right to dominion is
his conformity to the requirements of God. Captains who rebel
against generals can expect their corporals to be insubordinate. Our
polluted regions of the earth are rebelling against man’s rebellious,
lawless rulership, not against rulership as such.

In a widely quoted and reprinted essay, “The Historical Roots of
Our Ecological Crises” (1967), medieval historian Lynn White, Jr.,
