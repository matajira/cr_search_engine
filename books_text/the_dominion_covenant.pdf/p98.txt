54 TIE DOMINION COVENANT: GENESIS

sion? Obviously, the State should take away income from the
tobacco-smoking (or chewing or snuffing) Americans, buy water,
and send it to the nomads, Any other conclusion defies the econom-
ics of egalitarian redistribution. Any other economics defies “com-
mon sense.” While the authors are not quite this radical in their con-
clusions — almost, but not quite—the direction in which their logic
would carry international socicty is clear enough, To quote them, “It
is ay simple as that.”

Once the secular humanistic economist acknowledges the fact—
and for finite minds, it # a fact—that he cannot, as a scientist,
measure subjective utility, and that he therefore cannot make in-
terpersonal comparisons of subjective ulility, most of what we know
as modern economics disintegrates. Like an acid, the argument
systematically and relentlessly erodes the philosophical, intellectual,
and moral foundations of every economic doctrine that it touches,
and it touches virtually every aspect of applied economics. It is the
inescapable conclusion of all subjective valuc theory, yct it under-
mines the economics based — supposedly based—cexclusively on the
idea of subjective utility.

If there is only subjective value, then these valucs, unlike objec-
tive prices, cannot be compared. This is the thrust of Robbins’ argu-
ment. It is as impossible to measure subjective utility as it is to
measure hate, love, or any other human emotion. A person can order
his preferences, bul he cannot measure them. No yardstick is
available.

Comparing Statistical Aggregates

This being the casc, the logic of subjective utility leads to some
very unorthodox conclusions, For example, consider the possibility
of nuclear war. Assume that war breaks out in Europe. All of France
is destroyed, except for one man, who happens to love French wine
more than anything on earth, and one enormous vat of his most
loved wine. So large is this supply that he will be able to spend the
remainder of his days consuming all he wants of this wine—the at-
tainment of his lifelong dream. On the other hand, the United States
is untouched by the war. All of its cities are intact, all of its capital
structure is intact. Using the law of subjective value, with its cor-
ollary prohibiting the interpersonal comparison of subjective
utilities, the fully consistent economist cannot say whether the
Frenchman’s capital is greater or less than the capital structure of the
