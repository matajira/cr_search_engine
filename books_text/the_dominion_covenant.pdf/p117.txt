God's Week and Man’s Week 73

Why the eighth day? The Bible points to the sin of man, on
Adam’s proclaimed first day of his week of creation, or better put, on
the first day of man’s first week of fully responsible dominion. The
first week had becn God's week exclusively. He rested on the seventh
day, He removed Himself from the physical presence of Adam and
Eve. It was their first full day of life. Adam had served as an appren-
tice the previous day, but on the seventh day, he and his wife received
their independence. God would see how they would handle tempta-
tion. ‘They did not handle it well. They sinned. He returned to judge
them in the evening. This is why the sabbath is “the day of the Lord,”
meaning judgment day. (The church recognizes this in the sacrament
of the communion meal, which is preceded by self-judgment.)

Adam also should have rested and judged, just as God did, He
should have condemned Satan at the time of the temptation, as
God’s delegated representative. He could then have waited for God
to return to render final judgment. (See Appendix E: “Witnesses and
Judges.”) Having condemned Satan, he and Eve could then have
had a communion meal with God (as the church does covenantally:
Lord’s Supper) at the tree of life. Instead, they had communion at
the forbidden tree —a satanic communion, like the one forbidden by
Paul in I Corinthians 10:20-21.

Adam rebelled against God. Satan had told Eve that on the day
the two of them ate of the forbidden fruit, they would become as
gods (Gen. 3:5). What had they both learned about God’s activity?
They knew that He had created the world in six days. They had not
been present at the creation, but they had seen part of the sixth day
of the weck. They could begin their first week as subordinates to
God or as imitation gods. They could rest, and the next day begin to
work under the authority of God, or they could attempt to establish
themselves as sovereign creators apart from God and in rebellion
against God. By resting on that seventh day, waiting one day to
begin work, they could begin the dominion assignment in the second
week of the earth’s history, clearly derivative in their authority. On
the other hand, by rebelling they could declare a new creation, a
new beginning, as autonomous creators. They could declare “man’s
week” as an altcrnative to, and a program superior to, Ged’s week.
Would man begin the full-scale tasks of dominion acknowledging his
secondary importance in the second week, or would he deny the rele-
vance of the week that had preceded “man’s week”?

If he chose to become the new god, he would have to act fast. In
fact, his first act would have to be an act of rebellion, in order to
