390 HE DOMINION GOVENANT! GENESIS

species adapted themselves to changes in their environments. This
process of adaptation was supposedly hereditary; thus, the doctrine of
acquired characteristics was born. It was never to be taken seriously
officially; unofficially, it became an escape hatch in the later editions
of Charles Darwin’s On ihe Origin of Species. But their major premise,
namely, the unlimited possibility of species variation, did become the
touchstone of Darwinian evolution. It was this premise that broke
the spell of the great fixed chain of being.

One of the most important books of the early nineteenth century
was Rev. William Paley’s Natural Theology (1802). Paley’s work syn-
thesized many of the then-prominent arguments for God’s provi-
dence on earth. He argued that Newton’s clock-like universe offers
us testimony to God’s sustaining providence. We can see it if only we
look at nature’s intricate design; the harmonious interdependence of
the infinite number of parts assures us that only an omnipotent Cre-
ator could have designed, created, and sustained it for all these
years, The language of design had become universal by Paley’s day,
and his book only reinforced an established dogma. Darwin himself
had been greatly influenced by Paley’s providentialism in his college
days, as he admitted much later: “I do not think I hardly ever ad-
mired a book more than Paley’s ‘Natural Theology.’ I could almost
formerly have said it by heart.”40 At the heart of all these schemes of
God’s mechanistic providence was the doctrine of final causation: the
whole universe was designed to serve the needs of man. All things
were planned ih advance to further man’s affairs; in every being
created in the mists of time there were the materials available to deal
with the survival of the species. (This posed a serious theoretical
problem: how to explain extinct fossils.) The evolutionary form of
this doctrine is obviously Lamarckianism: species have the power of
adaptation, individual by individual, organ by organ. Unconscious
adaptation is the mechanism of organic evolution. When Darwin
finally broke with Rev. Paley, he therefore also had to break with
Lamarkianism, a position which he had never held anyway.

Providence implics control by God; control implies purpose. The
doctrine of final causation had provided Western man with philo-
sophical purpose since the days of Aristotle.!"° So long as scien-

109. Darwin to John Lubbock (Nov. 15, 1859); in Francis Darwin (ed.), The Life
and Fetters of Charles Durwin (New York: Basic Books, [1888] 1959), II, p. 15.

110. F.8.C. Northrop, ‘Evolution and Its Relation to the Philosophy of Nature,” in
Stow Persons (ed.), Evolutionary Thought in America (New York: Gcorge Braziller,
