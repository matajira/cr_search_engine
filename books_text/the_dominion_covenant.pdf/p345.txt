From Cosmic Purposelessness to Humanistic Sovereignty 301

other form is active or previstonal. The former represents natural prog-
ress, the latter artifictal progress. The former results in a growth, the
latter in a manufacture. The one is the genetic process, the other a deleo-
logical process.”5° Ward was clearly a proponent of activism.

How did Ward refute the “passive” evolutionists (Social Dar-
winists) in the name of Darwin? Ward came up with this fundamen-
tal idea: nature’s processes are wasteful.'5? This is completely in accord
with Darwin and Wallace. It was their recognition of the enormous
pressure of multiplying populations — a multiplication which pressed
upon the limits of the environment —which led to the survival of cer-
tain genetically advantaged members of any given species. It was the
failure to survive that caught their attention — the millions of extinct
species that did not gain the advantage of random genctic changes
that would have enabled them to compete successfully in the slowly
changing environment, as well as the enormous number of non-
survivors in each generation. The idea began with Malthus: the
assertion that populations multiply far more rapidly than the food
supply necessary to ensure the survival of all members of the
multiplying spectes. Darwin cited Malthus’ observation in the first
paragraph of Darwin’s 1858 essay which appeared in the Linnean
Society’s Journal. 5* Waste is nature’s way. And waste was Ward’s
sworn enemy. “The prodigality of nature is now a well-understood
truth in biology, and onc that every sociologist and every statesman
should not only understand but be able to apply to society, which is
still under the complete dominion of these same wasteful laws. No
true economy is ever attained until intellectual foresight is brought
to bear upon social phenomena. Teleological adaptation is the only
economical adaptation.”159 Here was Ward’s battle cry against Social
Darwinism: the ctvil government alone is capable of stamping out unplanned,
natural, non-teleolagical waste.

Where do we find waste? In natural processes and in the free
market. Free trade is enormously wasteful. “Free trade is the imper-~
sonation of the genetic or developmental process in nature.”'6° He
also understood that free trade is the archetype of all free market

156. fbid., I, p. 72.

157. Thid., IH, p. 494.

158. Charles Darwin, “Ihe Linnean Society Papers,” in Appleman (ed.),
Danwin, p. 83.

159. Ward, I, pp. 74-75.

160. Zbid., I, p. 74; IL, p. 398.
