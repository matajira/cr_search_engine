252 THE DOMINION GOVENANT: GENESIS

mankind by making him the product of ape-like beings, which in
turn were products of impersonal random forces governed only by
the law of natural selection, He writes: “It has become almost a com-
monplace that Darwin’s discovery of biological evolution completed
the downgrading and estrangement of man begun by Copernicus
and Galileo. I can scarcely imagine a judgment more mistaken.
Perhaps the central point to be argued in this book is that the op-
posite is true. Evolution is a source of hope for man. To be sure,
modern evolution has not restored the earth to the position of the
center of the universe. However, while the universe is surely not
geocentric, it may conceivably be anthropocentric. Man, this
mysterious produci of the world’s evolution, may also be its pro-
tagonist, and eventually its pilot. In any case, the world is not fixed,
not finished, and not unchangeable. Everything in il is engaged in
evolutionary flow and development.”!6 A changing, evolving world
is at last free from the providence of God. “Since the world is evelv-
ing it may in time become different from what it is. And if so, man
may help to channel the changes in a direction which he deems
desirable and good. . , . In particular, it is not true that human
nature does not change; this ‘nature’ is not a status but a process.
The potentialities of man’s development are far from exhausted,
either biologically or culturally. Man must develop as the bearer of
spirit and of ultimate concern. Together with Nietzsche we must say:
‘Man is somcthing that must be overcome.’”!7 Man, in short, must
transcend himself, He must evolve into the pilot of the universe. He
can do this because he alone is fully self-conscious, fully self-aware.
“Self-awareness is, then, one of the fundamental, possibly the most
fundamental, characteristic of the human species. This characteris-
tic is an evolutionary novelty. . . . The evolutionary adaptive signi-
ficance of self-awareness lies in that it serves to organize and to in-
tegrate man’s physical and mental capacities by means of which man
controls his environment.”!#

Understandably, Dobzhansky despises Protestant fundamental-
ism. Above all, he must reject the idea of creationism. To accept
such a creed would be to knock man from his pedestal, to drag him
away from the pilot’s wheel. In fact, scholarly fundamentalists
enrage him. “There are still many people who are happy and com-

 

16, Dobzhansky, Biology of Ultimate Concern, p. 7.
17. Ibid., pp. 8-9.
18. Ibid., pp. 68-69.
