22
THE BLESSING OF RESPONSIBILITY

And the Lord was with Joseph, and he was a prosperous man; and he
was in the house of his master the Egyptian. And his master saw that the
LORD was with him, and that the Lorp made all that he did to prosper
in his hand, And Joseph found grace in his sight, and he served him: and
he made him overseer over his house, and all that he had he put into his
hand (Gen. 39:2-4).

Like Jacob his father, who had served Laban for many years,
Joseph was proving to be an efficient, highly profitable servant.
Potiphar, like Laban, recognized that God had some special rela-
tionship with his servant, and he was determined to benefit from this
fact. Both Laban and Potiphar sought to appropriate the fruits of
their servants’ productivity by delegating increased responsibility in-
to their hands (Gen. 30:27-28). Furthermore, during the period
when each master dealt justly with his servant, he saw his own eco-

nomic affairs prosper.

We are not told what duties Potiphar had as captain of the guard.
We are told that as far as his own household was concerned, he
delegated all authority to Joseph, “and he knew not ought he had,
save the bread which he did eat” (39:6). In one respect, Potiphar
proved that he was a successful businessman, for one of the most
important aspects of the entrepreneur's tasks is to locate and employ
able subordinates. Frank H. Knight, whose pioneering work, Rsk,
Uncertainty and Profit (1921), presented the first systematic, accurate
analysis of profit, put it even more emphatically: “. . . this capacity
for forming correct judgments (in a more or less extended or
restricted field) is the principal fact which makes a man serviceable
in business; it is the characteristic human activity, the most impor-
tant endowment for which wages are received. The stability and suc-
cess of business enterprise in general is largely dependent upon the

213
