212 THE DOMINION COVENANT: GENESIS

ings, at least until the time of the great famine when they journeyed
to Egypt. The Canaanites, who dominated the land in the era of the
famine, were to enjoy their independence for only a few centuries
after that famine. Both Jacob and Esau were to increase their do-
minion of the carth as a result of the law of diminishing returns. It
forced them to seek new lands to conquer.
