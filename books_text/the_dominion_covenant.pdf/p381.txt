The Evolutionists’ Defense of the Market 337

order—or even in today’s social order, which is no longer the en-
vironment of the late nineteenth century, let alone Scotland in the
mid-eighteenth century. Evolutionism is another variety of historicism,
and historicism offers man no fixed, reliable, universal, and perpetu-
ally binding principles of law, legislation, and liberty (to use the tide
of Hayck’s trilogy}. How can any decentralist version of cvolution
win mcn’s minds, in an era in which the second stage of evolution-
ism, the infamous “sleight of hand”— elitist planners as the source of
future evolution for the benefit of species man—has become the
reigning faith?

Men must believe in some authority. They must obey that
authority if they are to survive. It may be the market economy, or
the civil government, or the Bible, but men need a source of reliable
authority to commit themselves to. There can be no division of labor
without such subordination, Men necessarily obey someone. Hayck fully
understands this principle of human action, He calls men to obcy the
laws and conventions of the undesigned free market order. They are
to cxercise faith in the benefits and reliability of this order, They are
to believe that it is, in fact, a true order, and not a capricious, ran-
dom, and destructive anti-system. Hayek does not minimize the in-
dividual’s obligation to obey: “. . . the individual, in participating in
the social processes, must be ready and willing to adjust himself to
changes and to submit to conventions which are not the result of in-
telligent design, whose justification in the particular instance may
not be recognizable, and which to him will often appear unintelligi-
ble and irrational.”24 Men must, in short, exercise blind faith. They
must subordinate themselves faithfully to social processes that they
do not-understand, and that even appear irrational to them. They
must do this if the free market order is to survive.

Tf survival is the criterion of success, then the frec market order
in the twentieth century has begun to resemble a social dodo bird,
headed for extinction, If success in the open marketplace of ideas is
the criterion, then the free market’s undesigned structure has not pro-
duced the intellectual defenses that might insure its survival.
Perhaps someday people will belicve in the market as fervently as
Hayek wants men to believe—a blind faith in an undesigned order
—but throughout the twentieth century, such faith has been shrink-

 

24. Hayck, “Individualism: True and False” (1945); reprinted in Hayek, Individ-
ualism and Economic Order, p. 22.
