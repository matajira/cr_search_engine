370 THE DOMINION COVENANT; GENESIS

fore the God who created it must be infinite. ‘his is metaphysically
humbling, but for the regenerate it can be the promise of wiumph.
The key is not the size or shape of the universe, but the reliability of
the revelation of the God of creation. The problem is not size, but
ethics, not geographical position, but ethical position. The great
danger, soon witnessed, of the expanded size of God’s universe was
the next step, wholly illegitimate: infinite tame.5+

Modern historians have often been remiss, lazy, or deliberately
misleading in their unwillingness to comment on another aspect of
the conflict between medieval Roman Catholic orthodox science and
the Renaissance discoveries. Renaissance speculation was not the
product of a group of armchair college professors. It was deeply in-
volved in magic, demonism, and the occult arts. C.S. Lewis is quite
correct when he observes that it was not the Middle Ages that en-
couraged grotesque superstitions; it was the “rational” Renaissance.
These men were searching for power, like Faustus, not truth for its
own sake.55 For example, it is generally today accepted that the first
fate-medieval or carly medern figure to advance the old Greek con-
cept, of an infinite universe was Giordano Bruno,®6 Yet it was
Bruno’s reputation, well-deserved, as a magician, a Kabbalist, and
an astrologer, that brought him to his disastrous cnd.*” It was not
simply that Copernicus, in the name of mathematical precision,
placed the sun at the center of the universe. Ptolemy’s system was as
accurate in its predictions as Copernicus’ system (for Copernicus
erroneously favored circular planet orbits instead of ellipses).5%
Copernicus was involved in a neoplatonic, Pythagorean revival
against the Aristotelian universe of the late-medieval period. Mathc-
matics governs everything, this tradition teaches, contrary to Aristo-
tle’s teachings.*? It was also a deeply mystical and magical tradition.

 

54. ‘The crucial aspect of time in cosmological speculation will be discussed more
fully in the section dealing with geological evolution.

55. C. $. Lewis, The Abolition of Man (New York: Macmitian, [1947] 1965), pp.
87-89. The atiempt of modern science to fuse rational scientific technique and magi-
cal power is the theme of Lewis’ magnificent novel, That Hideaus Strength (1945).

36. Lovejoy, Great Chain, pp. 116-17; Koyré, Closed World, p. 39.

57. Frances Yates, Giordano Bruno and ihe Hermetic ‘Itadition (New York: Vintage,
[1964] 1969), This is required reading for anyone who still believes the myth of the
“rational” Renaissance.

38. E. A. Burit, The Metaphysical Foundations of Modern Physical Science (Garden
Gity, N. ¥.: Doubleday Anchor [1925] 1954), p. 36. This is a very fine study of the
mind-matter dualism of modern scientific and philosophical thought.

59. Ibid, p. 52-56.

  
