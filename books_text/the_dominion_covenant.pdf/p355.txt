From Cosmic Purposelessness to Humanistic Sovereignty Su

mouthpiece of a conscious society is the legislature.”2°” In short, the
visible symbol of a fully conscious society is the self-conscious divinity of the
State. Society must agree about any particular course of action, but
once unanimity of opinion is reached~and it is the function of
public education to promote it— then debate ends. “Let there be no
excuse for any onc to debate a question which has at any time or
place, or in any manner, been once definitively answered.”2°8 Like
the laws of the Medes and the Persians, once the divine ruler has
made a law, it must not be broken (Dan. 6:8, 12).

Does this mean that democracy will allow all men to have a veto
power over the decisions of the rulers? Of course not. The elite must
continue to rule. “Deliberative bodies rarely enact any measures
which involve the indirect method. If individual members who have
worked such schemes out by themselves propose them in such
bodies, the confusion of discordant minds, coupled with the usual
preponderence of inferior ones, almost always defeats their adop-
tion. Such bodies, miscalled deliberative, afford the most ineffective
means possible of reaching the maximum wisdom of their individual
members. A radical change should be inaugurated in the entire
method of legislation. By the present system, not even an average
expression of the intelligence of the body is obtainable. The uniform
product of such deliberations falls far below this average. True
deliberation can never be reached until all partisanship is laid aside,
and each member is enabled to work out every problem on strictly
scientific principles and by scientific methods, and until the sum
total of truth actually obtained is embodied in the enactment. The
real work can not be done in open session. The confusion of such
assemblies is fatal to all mental application, There need be no open
sessions. The labor and thought should be performed in private
seclusion, the results reached by others should in this way be calmly
compared by each with those reached by himself, and in a general
and voluntary acquiescence by at least a majority in that which
really conforms with the truth in each case should be deliberately
embodied as law. The nature of political bodies should be made to
conform as nearly as possible with that of scientific bodies. . . .”#09
What, then, becomes of unanimity, of open covenants openly arrived

207. Ibid., UL, p. 467.
208. Tbid., I, p. 407.
209. Zid, I, p. 395.
