60 THE DOMINION COVENANT: GENESIS

absolute control. Therefore, the mind of God integrates all facts and
judges all facts in terms of His perfect plan. Men cannot make ab-
solute, comprehensive value imputations, since men are creatures.
But they can, as limited creatures, make value imputations that are
valid in God's eyes, and before the rebellion of man in the garden,
this is what man did. Each man still makes these value imputations,
and man, as a creature responsible to God, cannot escape the revela-
tion and restraint of God. Men do make value imputations. They
live and act in terms of God’s laws, either as rebels or faithful men.
As living creatures, they must deal with the universe as it objectively
exists, if they wish to succeed. They must interpret the information
they receive from the universe through their senses, and they must
interpret correctly, meaning objectively, meaning in terms of God’s
law-order for His universe, if they are to remain successful. Thus,
their subjective tnterpretations are supposed to conform to the objective
standards which God requires for man, who is made in His image.

There is an overall economic plan in Gad’s mind, This forces
men, to some degree, to conform themselves to this plan and to
adjust their plans in terms of it. We can therefore say, along with
Kirzner, that in order to make assessments of comparative wealth,
there must be a single, integrated plan. Furthermore, unlike
Kirzner, we can say that such a plan exists. As creatures made in
God’s image, we can make at least reasonable, useful estimations of
the value of capital or other goods, even though we could not do so
legitimately if all value were exclusively subjective, as if there were
no overall plan of God.

Economists are generally self-consciously atheistic in their pre-
suppositions, Man, and man alone, does the imputing of value. Yet
at the same time, all economists, without exception, use such mental
aggregate constructs as “capital,” “income,” “national income,” and
“productivity.” None of these mental constructs is valid, given the
logic of modern subjectivism, yet the economists use them con-
stantly. Professor Mises, an important figure in the development of
modern subjectivism, and perhaps the most important figure if we
are to believe the assessment of his more famous pupil, F. A.
Hayek,®? argued throughout his career against the validity of all
aggregates in economics, yet when he attempted to explain the pro-
ductivity of workers under capitalism, he used the concept of per

33. Hayek, Coanter-Revolution of Science, p. 240, footnote 25.
