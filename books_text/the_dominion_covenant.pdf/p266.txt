222 THE DOMINION COVENANT: GENESIS

sumers served by profit-seeking, competitive entrepreneurs” vs.
“consumers served by Civil Service-protected, guaranteed tenure,
monopolistic government planners.” The fact that government plan-
ners have access to reams of data concerning past decisions of con-
sumers and producers means very little. The crucial ability ts to make
correct assessments about the uncertainties of the future, meaning those
aspects of the economic future that are not subject to computeriza-
tion or even statistical probabilities, It is the presence of incessant
change in human affairs that calls forth the skilled and not-so-skilled
entrepreneurs in the quest for profits, The market provides a
mechanism of economic competition which sorts out the successful
from the unsuccessful entrepreneurs. There is no comparable
mechanism operating in government, for government has a monop-
oly of support (taxation) and very often a monopoly of supply opera-
tions, such as the delivery of first-class mail, which insulates it from
the competitive framework of the open market.3 Knight’s warning is
significant: “lhe real trouble with bureaucracies is not that they are
rash, but the opposite. When not actually rotten with dishonesty and
corruption they universally show a tendency to ‘play safe’ and
become hopelessly conservative. The great danger to be feared from
a political control of economic life under ordinary conditions is not a
reckless dissipation of the social resources so much as the arrest of
progress and the vegetation of life.”* Bureaucracy favors present-
oriented risk- (uncertainty-) averters.5

What service is it that the entrepreneur performs in order to
receive his residual? He perceives a special opportunity in the future. He
believes that consumers will be willing and able at a specific point in
time to pay more for a particular good or service than today’s entre-
preneurs think they will be willing and able to pay. Because of this
lack of perception on the part of his competitors, the entrepreneur
finds that the scarce economic resources that are used in the produc-

3. Ludwig von Mises, Bureaucracy (New Rochelle, New York: Arlington House,
[1944] 1969). On the inability of governments to make accurate economic
assessments of costs and benefits, see Mises’ essay, “Economic Calculation in the
Socialist Commonwealth,” (1920), in F. A. Hayek (ed.), Collectivist Economic Planning
(London: Routledge & Kegan Paul, [1935] 1963), ch. III. He expanded his analysis
of this topic in his book, Socialism: An Economic and Sociological Analysis (New Haven,
Conn.: Yale University Press, [1922] 1962), Book TI, chaps. 1, 2.

4. Knight, of. cit., p. 361

5. Gary North, “Statisc Bureaucracy in the Modern Economy,” in North, An
Introduction to Christian Economics (Nutley, New Jersey: Craig Press, 1973), ch. 20.
