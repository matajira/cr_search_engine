The Evolutionists’ Defense of the Market 325

ics, They wind up citing secular economists who favor Keynesian in-
tervention by the civil government into economic affairs, or they cite
even more radical secular economists. But both sides rely heavily on
the conclusions of the warring camps of humanistic economists.

IfT have rejected environmental determinism, evolutionism, and
humanism in general, how can I legitimately use the arguments of
environmental determinists, evolutionists, and humanists to support
my case for a free market social order? Can I evade the accusation of
the “Christian socialists” and “liberation theologians” that what I pro-
pose is simply a disguised version of secular capitalism, a baptized
version of anarchism? The only way I can legitimately evade this
criticism is to show that I do not accept Darwinian evolution as the
scientific foundation of Christian economics, and then demonstrate
that to the extent that the defenders of the free market accept such a
foundation, they wind up without a logical position to defend. I also
have tried to show in Appendix A, on Social Darwinism and Lester
Frank Ward’s refutation of Social Darwinism, that the demise of
ninetcenth-century Classical liberal economics was assured from the
start, precisely because Darwinism really does not believe in the
“survival of the fittest” and “evolution through natural selection,”
once man, the rational planner, appears in history. In other words, to
paraphrase Cornelius Van Til, the humanistic economists have bor-
rowed their accurate conclusions from Christianity. They cannot tell
us why human minds agree, or why such minds can interpret the
universe, or why the universe is coherent (since it has its origins in
randomness or chaos), or why there is human freedom in a deter-
ministic universc, or why the noumenal realm of ethics (outside of
the determined realm of scientific law) can determine affairs in the
external, cause-determincd world of matter. Yet they say they can
make all kinds of statements about economic events. How can they
do this? They do not say.

The Christian cconomist can say. He points to a sovereign God
who is the Creator. He points to a record of the creation in Genesis,
chapter 1, He points to man, who is made in the image of God. He
points to God’s assignment to man in Genesis 1:28 to subdue the
earth. He points to man’s ability to name the animals. All of these
facts of the Genesis account provide the foundation of Christian
thought in general and Christian economics in particular. The
orderly creation reflects an orderly, sovereign God. Man has been
