Index

Nuclear war, 54

Objective value, 43, 64°
(see also values: objective)
Occultism, 121f, 130, 216, 277, 283,
370
O'Driscoll, Gerald, 58n
Omniscience, 57, 101, 220, 352, 355
man’s, 223, 225, 227f, 330
(see also equilibrium)
One-many, 153, 155, 331, 349, 354,
364
One-six, 72
One-world order, 151, 155
Opportunities, 130
Oppression, 177, 183
Optimism, 300, 286, 373, 386, 441
Order
origin of, 19f
Oriental despotism, 228
Ownership, 208f, 241, 444F

Padan, Aram, 189n
Pagan festivals, 452f
(see also Ghaos festivals)
Paganism, 176
Paley, William, 17, 256f, 259, 390,
409f
Pantheism, 251, 417, 431
Parables, 192f, 444
Paradise, 76, 88, 89, 113, 118, 453
restoration, 123
return to, 128
Passive-active, 31, 33, 84
Passover, 72, 452f
Pastors, xxiv, 426
Peace, 451
Perjury, 464
Penial, 202
Perkins, William, 193
Persons, Stow, 17
Pertec Gomputer Corp., 283
Pessimism, 4f, 30

Pharaoh
divinity of, 228
dream of, 219

revelation to, 227
Pharisees, 170, 437

499

Pietism, 203
Pigeons, 399
Pilgrim, 157, 163
Pin-sticking, 44, 332f, 354
Plague, 232
Plan (God’s}, 59f, 64
Planning, 229
central, 10, 22, 23, 26, 56f
decentralized, 23, 229
God's, 24, 64
Jacob’s, 202
omniscience, 57
time and, 126¢f
two forms, 338
Plan revisions, 348
Plans
Jacab’s, 202
market, 566
meshing, 57, 99
(see also coordination)
Plato, 360, 418
Pleasure and pain, 44ff
Point of contact, 450
Pollution, 338, 115f, 123
Policy-making, 47, 49f, 52
Poole, Matthew, 184
Poor Richard, 200
Population, 124, 148, 165ff, 175,
205, 314f, 318
Positive feedback, 175
Potiphar, 213f
Pottage, 181f (see also stew)
Poverty, 115, 130
Powell, Enoch, 4in
Power, 173, 266
central planning, 10
knowledge and, 86, 439
law and, 35, 36
liberal theologians and, 31
quest for, 10
Pragmatism, 216f
Prayer, 199, 202
Predestination, 23f, 436
Premises (see presuppositions)
Present-orientation
childlessness, 168
Esau’s, 181
