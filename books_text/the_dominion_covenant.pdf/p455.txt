Cosmologies in Conflict: Creation us. Evolution Ait

So he feigned modesty. These questions are beyond human intellect:
Questions of biology, factual and theoretical, are answerable, but
not questions that are raised as a direct product of the biological
answers. This has been a tactic of “neutral” scientists for years:
challenge the conclusions of a culture’s presuppositions by referring
to neutral science, but claim honest ignorance when discussing the
presuppositions of the methodology of neutral science. As he wrote
to W. Graham, two decades later, contradicting his carlier defense of
designed natural laws: “You would not probably expect any one fully
to agree with you on so many abstruse subjects; and there are some
points in your book. which I cannot digest. The chief one is that the
existence of so-called natural laws implies purpose. I cannot see
this.” Here is the dilemma of modern, post-Kantian philosophy:
Law or no law? When defending the total reliability and stability of
“autonomous” natural science against the clairns of Christians in favor of
God’s miraculous interventions, natural law is absolute (for exam-
ple, the statement by T. H. Huxley on p. 397). But when faced with
the totalitarian implications of absolute natural law —-a law so complete
and systematic that it indicates design rather than randomness as its
foundation—the “neutral” scientist throws out “so-called natural
laws.” God may neither thwart absolute natural law, nor claim credit
for the existence of such law, because it really is not absolute after
all. Absolute randomness is therefore a philosophical corollary of ab-
solute, impersonal law, and Darwin was uncomfortable with both
horns of his:dilemma. So he appealed once again to ignorance, since
he had to agree that chance is not sovereign: “But I have had no
practice in abstract reasoning, and I may be all astray. Nevertheless
you have expressed my inward conviction, though far more vividly
and clearly than I could have done, that the Universe is not the
result of chance. But then with me the horrid doubt always arises
whether the convictions of man’s mind, which has been developed
from the mind of the lower animals, are of any value or at all trust-
worthy. Would anyone trust the convictions of a monkey’s mind, if
there are any convictions in such a mind?”!78

Notice Darwin's implicit faith. He has absolute confidence in his
“monkey-descended” (or, for the purists, “ancestor-of-monkey-
descended”) mind when it concludes that his mind has, in fact,
descended from some lower animal. But when the implications of

178. Darwin to W. Graham (July 3, 1881), ibid, I, p. 285.
