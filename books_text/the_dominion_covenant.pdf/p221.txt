18
COMPETITIVE BARGAINING

And Jacob said, Sell me this day thy birthright. And Esau said, Behold,
1 am at the point in die: and what profit shall this birthright do to me?
And Jacob said, Swear to me this day; and he sware unto him: and he
sold his birthright unto Jacob (Gen, 25:31-33).

Throughout the Bible, there are warnings against economic op-
pression. Men are not supposed to take advantage of their weaker
neighbors, or widows, or strangers, or those temporarily in need.
Regarding food supplies, men are warned not to hold supplies off the
market in order to obtain a higher price. “He that withholdeth corn,
the people shall curse him: but blessing shall be upon the head of
him that selleth it” (Pr. 11:26). It should be understood that this pro-
hibition on “forestalling” does not involve the civil government. The
State is not to set arbitrary prices for the sale of food (or any other
scarce economic resource). The sanctions are social (cursing by the
public) and the loss.of a blessing from God. Food may legitimately
be sold at a profit, but it is supposed to be sold, not hoarded for the
sake of obtaining a higher price. The passage in Proverbs appears to
refer (o large-scale commercial agriculture, since the forestalling in-
volves sufficient quantities of corn to enrage the gencral public. It is
unlikely that such an outcry would be aimed at someone who was
hoarding only enough corn to feed his family, since a man is respon-
sible for the welfare of his family (I Tim. 5:8), and since the with-
holding of such small quantities could hardly impose a major burden
on the whole community. The frame of reference is the withholding
of so much grain that local prices are affected, so that the sale of this
grain represents a substantial benefit to the community.

Given this perspective on food sales, must we categorize Jacob as
an oppressor? Or is it legitimate to classify him simply as a successful
bargainer? Bargaining in his family was an accepted tradition, after

177
