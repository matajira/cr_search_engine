Subordination and Fulfillment 85

Nature was allowed to operate briefly without man for five days.
Man was allowcd to operate briefly without woman for less than one
day. Neither could be fully comfortable without its complement.
Nature needed subordination under man. Man needed subordina-
tion under God. Man was unable to achieve the fullness of dominion
alone. Dominion requires a division of labor, so he received his helper fit
for him. The familiar phrase “helpmeet” has distorted the meaning of
the original words. Eve was a helper “meet” or fit for Adam. She was
the product of design. Adam knew he could not perform his tasks
efficiently without another person to assist him. Like nature, he had
been created good but incomplete. He knew from the very beginning
that he was not self-sufficient.

Man’s Calling

God assigned Adam an initial task to be completed by himself.
He was to name the animals of the field and the birds. This meant
that he had to classify them, intellectually integrating their functions
into an overall design. The “many” were to be arranged in terms of
the “one,” meaning the plan of God as perceived and interpreted by
Adam, God’s image. We are not told whether this classification in-
volved all the beasts of the earth, or whether it was limited to the
field of the garden. If it involved all animals, the task is barely con-
ceivable in retrospect. We cannot imagine how ‘such a task could
have been completed by one individual in a few hours. Even if the
assignment involved only the beasts and birds of the garden, it
would have been an awesome task. Yet Adam completed it in a few
hours. His mind, prior to the Fall, was efficient beyond anything we
can imagine. Modern man, with the aid of enormous capital, the
division of labor, and the modern computer, has only begun to
match the skills of the first man in the garden.

Adam worked before he married. His definition of himself was
set in reference to his subordination to God and the covenant of do-
minion. Man’s work is fundamental to his very being. Eve was given to
him within the framework of his calling or vocation before God. The
family has its meaning in terms of the covenant of dominion. The in-
dividual family is influenced overwhelmingly by the particular call-
ing of the husband. Wives are to be selected in terms of the man’s
calling. They are to help their husbands fulfill their callings (Prov.
31:12, 23, 27). By departing from this interpretation of the meaning
of marriage, we find that religions, cultures, and individual families
