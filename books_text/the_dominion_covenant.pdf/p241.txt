 

 

The Uses of Deception 197

Conclusion

This is not “situation ethics.” Situation ethics denies the existence
of any permanent moral standards, Situation ethics denies the exist-
ence of a standard of action for which each man will be responsible
on the day of judgment. Situation ethics argues that the existential
moment determines the ethics of an action, that time is the god of
man, What the Bible says, in this particular and limited case of
deception, is that deception can be warranted. It provides us cases in
which deception was warranted, primarily cases of war or, in the in-
stance of Jacob and Esau, conflict between the representative heads
of two separate nations. Just because deception is valid in cases of
war, as the Bible affirms, we should not be led to the conclusion that
there is nothing wrong with deception in general. We know that we
are not supposed to bear false witness against our neighbor, for ex-
ample. But we are also informed by the Bible that righteous saints
have sometimes been forced to deceive others, in cases where the
others were acting unrighteously and therefore in direct defiance of
God and God's law. And far from being criticized by a single word in
the Bible, they have been praised openly (in the case of Deborah’s
praise of Jacl), or grafted into the covenant line (Rahab), or blessed
with enormous wealth ( Jacob). Jacob tricked his father into bestow-
ing the blessing on the one to whom it belonged by law and by prom-
ise, and sure enough, the blessings were poured out on him. We
should take this lesson seriously. If we fail to do so, the world will
(and should) conclude that our religion is simply not serious and is
therefore unworthy of serious consideration. If Jacob and Rahab
and Jael were serious actors in history, we should do our very best to
emulate their seriousness, their understanding of the meaning of the
covenant, and their understanding of the historical circumstances in
which their part in that covenant was being played.
