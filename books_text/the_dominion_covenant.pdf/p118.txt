74 THE DOMINION COVENANT: GENESIS

establish man’s first day of sovereign lordship over his new creation.
For Satan to make an effective case for rebellion, he would have had
to approach Eve on the morning of the seventh day, the first day of
man’s week, To interrupt man’s labors mid-week would have meant
that man had labored for part of his first week under God’s sovereign
authority. Obviously, at least part of the week-would have been
visibly God’s week, not man’s. So Satan probably began his tempta-
tion on the morning of the seventh day.

God, as totally sovereign over history, resting assured, rested the
seventh day. Autonomous man cannot rest in confidence that his
labors will be successful. He dares not “waste” time. He cannot al-
ford to- waste any resource as precious as time. Covenant man can
rest on God's sabbath, for he knows that God is sovereign, and that
he, as God’s obedient subordinate, possesses the grace of God. His
work will persevere. He can enjoy the day of rest because he knows
that every week is God’s week. The law of God is his tool of domin-
ion, and he knows that the law of God is in conformity to the opera-
tions of the world. He does not have to tabor seven days a week in
order for God to bless his efforts as dominion man. He is subor-
dinate to God, so he can be confident as a dominion man over Gad’s
ercation. Covenant man enjoys his rest.

Autonomous man’s week never ends. The eighth day is like the
sixth day, and the seventh day is like the second day. The week is
never-ending, and the work is never-ending, Man’s week is not a
week at all; it is a life of frantic labor, for man must establish his
dominion over foreign territory —God’s creation—in terms of anti-
nomian rebellion. But law is man’s tool of dominion, so the task
becomes an ever-greater burden as rebellious man departs more and
more from God’s revealed law-order. There is no day of rest—
psychological, confident rest—in man’s week. Covenant-breaking
man cannol enjoy his rest as a zero-cost blessing.

Satan wanted to make man his slave. He wanted to drive his new
slave unmercifully, just as the Pharaoh of the oppression wanted the
Hebrews to serve as slaves, and the Pharaoh of the exodus did with
his Hebrew slaves (Ex. 5:5-14). God wants servants; Satan wants
slaves. God wants men to prosper and rest; Satan wants men to fail
and bleed at their labors. God’s weck gives covenant man confidence
in his own labors, for it gives him a day of rest. Satan’s week—for
man’s week apart from God is Satan’s week, ethically—is a weck
without confidence or rest.
