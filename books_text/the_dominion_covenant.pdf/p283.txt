Conclusion 239

Without an understanding of the economics of the Book of
Genesis, it is not possible to understand economics. Secularists or
other anti-God writers may think they understand economics. And
they may well discover truths about men’s economic relationships,
but whatever they discover that is correct will be misinterpreted by
them, if they use their anti-Genesis presuppositions to analyze their
discoveries. Because God does not allow men to think completely
consistently with their anti-God presuppositions, He restrains their
errors. They make contributions to human thought and culture that
are beneficial. The common grace of God, meaning His unmerited
gifts to men in general, allows secular economists to make valid con-
tributions, But it is the Christians who will be the ultimate benefici-
aries of-these contributions, for they alone have the key to under-
standing, namely, the Bible. The Christians will make the best use of
the discoveries of secularists in all fields of thought. The Christians
will steadily integrate the valid findings of secular science into a
biblically sanctioned framework. This is why we need to take the
Book of Genesis seriously as a source of information concerning the
foundations of economic analysis.

Admittedly, Christians have failed to understand the crucial
position of Genesis as the foundation of economics, education, and
social science in general. They have been paralyzed, since the days
of Justin Martyr (the second century, a.p.), by the myth of neutral-
ity. They have tried to establish a common intellectual ground with
Greeks of all nations. Greek philosophy and its spiritual heirs have
misled the Christians almost from the beginning, Only when Chris-
tians recognize Genesis for what it is—the foundation of all human
thought —will they begin to make culture-reconstructing intellectual
contributions. They must no longer be satisfied with the scraps of
stulen wisdom that fall from the humanists’ tables.

The irony is that to the extent that humanistic cconomists have
made any lasting and valid contributions, they have used biblical
categories of thought. Most obviously, they have acknowledged the
effects of scarcity. They have come to grips with the economic conse-
quences of God’s curse of the ground. To the extent that economists
have departed from the reality of Genesis 3:17-19, as is the case with
Marxism, socialism, and other forms of collectivism, they have
become irrational. When they argue that institutional changes will
produce a world of zeroscarcity — a utopian world of universal abun-
dance at zero price—they have adopted satanic principles of inter-
