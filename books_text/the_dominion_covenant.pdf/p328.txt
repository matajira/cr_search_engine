284 THE DOMINION COVENANT: GENESIS

the forward-driven computer (impersonal, purposeless mechanism,
to purposeful organic agent, to personalized mechanism): here is the
standard, post-Darwin account. But Asimov blazes new trails. The
two forms of intelligence are too different to be compared on the
same scale, We cannot make such comparisons. We must keep the
systems distinct, Each should specialize. “This would be particularly
true if genetic engineering was deliberately used to improve the
human brain in precisely those directions in which the computer is
weak.” We must avoid wasteful duplication he says, “Consequently
the question of ‘taking over’ need never arise. What we might see,
instead, would be symbiosis or complementation, human brain and
computer working together, each supplying what the other lacks,
forming an intelligence pair that would be greater than either could
be alone, an intelligence pair that would open new horizons, not
now imaginable, and make it possible to achieve new heights, not
now dreamed of, In fact, the union of brains, human and human-
made, might serve as the doorway through which human beings
could emerge from their isolated childhood into their combination
adulthood.”

The advertisement sells no product, asks the reader nothing, and
does not instruct him to clip a coupon or take any sort of action. It
simply offers a message—a message of a new evolution,

The same theme is found in the $40 million movie, released in
December of 1979, Star Trek. The movie’s science advisor was
Asimov. The movie deals with a future space ship crew which con-
fronts an unimaginably powerful intelligence. This intelligence
turns out to be an enormous machine, one which in turn was built
by a civilization run entirely by machines. It literally knows
everything in the universe, yet it is travelling back to earth to seek
the “Creator” and to join with the “Creator” in a metaphysical union
(Eastern mysticism), The machine is perfectly rational, totally
devoid of feeling, and is a “child” at the very beginning of its evolu-
tion. It turns out that the center of the machine’s guidance system is
a centuries-old U.S. space probe, the Voyager, which had been sent
into space to seek knowledge and send back that knowledge to earth.
Hence, the “Creator” was man. The movie ends when an offi-
cer of the crew joins in metaphysical union with the machine, along
with a mechanical robot built by the machine—a robot which
duplicated his ex-lover. The officer, the female robot, and the enor-
mous machine then disappear. The science officer (a human-Vulcan
