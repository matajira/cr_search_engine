104 THE DOMINION COVENANT: GENESIS

the rejection of God's way to eternal death.

Covenant-breaking man in the garden would have reasoned as
follows: “God’s word is not reliable, so I need not protect myself from
any hypothetical effects of eating from the forbidden tree, His word
is not reliable concerning death, so His word is not reliable concern-
ing life. The odds against His word coming true are astronomically
high, so it would be a denial of my own sovereignty, my own assess-
ment of the low reliability of God’s word, for me to eat from the tree
of life as a calculated way of reducing the risk of disobedience. I have
already determined that there is virtually no risk in disobedience. I
had better not consider eating from the tree of life.” Covenant-
breaking man builds up his own self-confidence by adhering to his
self-proclaimed autonomous word. To choose God’s way to eternal
death necessarily involves the rejection of God's way to eternal life.

After the Fall, of course, man knew experimentally how wrong
his assessment had been. Then he would have been willing to eat
from the trec of life. But the tree was closed to him. To eat of it now
would have been theft, Adam would not be permitted to gain access
to eternal life on his own terms, as a proven covenant-breaker. He
had made his choice. His choice was the way to death.

The presence of two special trees in the garden, one leading to
life and the other leading to death, offers us a solution to an interest-
ing question: “How long was Adam’s period of probation to be?”
Adam could do three things, essentially, First, he could go straight to
the forbidden fruit, thereby ending the period of testing in the
garden. Second, he could go straight to the trec of life, thereby re-
moving the threat of eternal death, but only by affirming God’s word
and by subordinating himself to God as a covenant-keeper. Third,
he could postpone a choice between the two trees, concentrating his
attention on other trees or other tasks in the garden.

There is no revelation concerning a specified period of testing.
The Bible does not tell us that Adam had a day, a week, or a millen-
nium to make up his mind, This should tell us that the period of
testing involved the tree of life. If the test had been simply “the tree
of the knowledge of good and cvil” vs. “dressing the remainder of the
garden,” then the temptation would have been before him forever,
or until God stepped in to tell him that it was over, that his refusal to
eat the forbidden fruit for all this time proved that he was serious
about obeying God. At that point, God would have granted him
