Cosmologies in Conflict: Creation os, Evolution 407

The continutty of change was as dear to Darwin as the continuity of
being. Uniformitarianism pervaded all of his writings. Nature, he
asserted, “can never take a great and sudden leap, but must advance
by short and sure, though slow steps.”6? Admittedly, “The mind
cannot possibly grasp the full meaning of the term of even a million
years; it cannot add up and perceive the full effects of many slight
variations, accumulated during an almost infinite number of genera-
tions.” But cven though the mind cannot grasp this, we are expected
to drop our unwarranted prejudices against what we cannot grasp,
and accept it. “Whoever is led to believe that species are nnutable will
do good service by conscientiously expressing his conviction; for
thus only can the load of prejudice by which this subject is over-
whelmed be removed.”!83 We should not “hide our ignorance” by us-
ing terms like “plan of creation” or “unity of design.” Instead, we
should stand firm alongside those “few naturalists, endowed with
flexibility of mind, and who have already begun to doubt the im-
mutability of species,” and wrap our newly flexible minds around a
concept of uniformitarian change which no mind can grasp.'** This,
you understand, is the scientific method.

The third feature of Darwin's thought is cosmic impersonalism.
Obviously, this is the product of both his philosophy of indeter-
minacy and uniformitarianism., They are intertwined. There is no

 

 

personal God in Darwin’s system who can in any way affect the
operations of random variation and statistical natural law. In
gencral, this is regarded as the heart of the system. Biology, the last
refuge of a personal God, was finally cleared of this embarrassing
influence. While he regarded nature as wholly impersonal, Darwin
was never able to escape the language of personification in describ-
ing natural processes. ‘Ihe very phrase “natural selection” implied an
active power, as he admitted, but he reminded his readers that this
was simply a metaphor. Bul metaphors are powerful devices,
however candid Darwin's admission might have been. It made the
transition from cosmic personalism to cosmic impersonalism that
much easier, “So again it is difficult ta avoid personifying the word
Nature; but I mean by Nature, only the aggregate action and project
of many natural laws, and by laws the sequence of events as ascer-

162. Origin, ch. 6, p. 144.
163, Tbid., ch. 15, p. 368.
164, Idem,
