xiii THE DOMINION COVENANT: GENESIS

4.. Judgment/Sanctions

Chapters Four and Five deal with judgment. Chapter Four dis-
cusses value theory in economics. Is value objective or subjective?
This is the question that has baffled economists for two centuries.
The biblical answer is that value is both objective and subjective in
God. God declares good and bad, Men are to think God’s thoughts
aftor Him as creatures. They are to render godly judgment as His dele-
gated agents. Thus, rendering judgment in history is basic to man’s
calling before God (see also Appendix E: “Witnesses and Judges”).

Chapter Five deals with the sabbath rest idea. In the Ten Com-
mandments, this comes as the fourth commandment. In The Sinat
Strategy, I discuss this commandment under point four of the biblical
covenant: “Sabbath and Dominion.” By resting on God’s sover-
eignty, man can achieve rest. He acknowledges his position as God’s
subordinate agent. God will honor His covenant, and bring bless-
ings to those who obey Him. Therefore, resting one day in seven is a
covenantal acknowledgment that God is sovereign, not man.

5. Inheritance/Continuity

Chapter Six deals with the concept of money. There is no more
fundamental aspect of money than continuity over time. Any com-
modity that is perccived to be valuable over long periods in the past
can become a Candidate for an economy's money. People will be will-
ing to consider the use of such a commodity as a means of exchange
and “storehouse of value,” meaning a valuable thing to store. As 1 write
in my study of the biblical basis of moncy: “In short, money is the
most marketable commodity. It is marketable because people expect
it to be valuable in the future.”®

The fact that 1 instinctively adopted the five points of the cove-
nant in my original exposition is indicative of just how central the
covenant is in biblical documents. It is the ordering principle in the
Bible, even reflected in the names Old Covenant and New Covenant
(Heb, 8:13), We take communion under the authority of God’s cove-
nant (I Cor, 11), Sinners take communion under Satan’s covenant (I
Cor, 10:20-21). We eat the tree of life or the forbidden trec. “Cove-
nant” is an inescapable concept. There is no escape from covenants:
the question is: Which covenant?

9. Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Warth, ‘Texas: Dominion Press, 1986), p. 20.
