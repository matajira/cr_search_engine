The Evotutionists’ Defense of the Market 341

it sensible to adopt social, economic, and genetic planning in order
to guarantee man’s triumph? Aren’t we in a war against other spe-
cies? Can any army be successful that has no chain of command, no
centralized leadership? The social impulse of Darwinism is to estab-
lish man’s position as the new sovereign over nature. Man, the central
planner is a powerful image. How can Hayek’s version of evolutionism
—analogous to the pre-human, purposeless, undesigned evolution-
ary process ~ compete with “the real thing,” namely, elitist planning
by scientific experts? Hayek’s reasoning has failed to convince men
in the marketplace of ideas. What other standard can be used by
Hayck or his followers to appeal to beyond the marketplace of ideas?
Mises, Hayek’s teacher, knew there was no such appeal for a true
Austrian economist, which is why he was incapable of optimism re-
garding the future of man: “Whatever is to be said in favor of correct
logical thinking does not prove that the coming generations of men
will surpass their ancestors in intellectual effort and achievements.
History shows that again and again periods of marvelous mental ac-
complishments were followed by periods of decay and retrogression.
We do not know. whether the next generatidn will beget people who
are able to continue along the lines of the geniuses who made the last
centuries so glorious, We do not know anything about the biological
conditions that enable a man to take one step forward in the march
of intellectual advancement. We cannot preclude the assumption
that there may be limits to.man’s further intellectual ascent. And
certainly we do not know whether in this ascent there is not a point
beyond which the intellectual leaders can no longer succeed in con-
yincing the masses and making them follow their lead.”2? Or as he
wrote in a manuscript as he was about to flee Switzerland in 1940:
“Occasionally I entertained the hope that my writings would bear
practical fruit and show the way for policy. Constantly I have been
looking for evidence of a change in ideology. But I have never allowed
myself to be deceived. I have come to realize that my theories explain
the degeneration of a great civilization; they do not prevent it. I set
out to be a reformer, but only became the historian of decline.”*8 The
historian of decline: a sad task for an economist. His Darwinian evo-

27, Mises, The Historical Setting of the Austrian School of Economics (New Rochelle,
New York: Arlington House, 1969), p. 38.

28. Mises, Notes and Recollections (South Holland, Mlinois: Libertarian Press,
1978), p. 115.
