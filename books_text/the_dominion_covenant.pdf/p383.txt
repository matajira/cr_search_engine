The Evolutionists’ Defense af the Market 339

out that without a system of permanent, universal morality, there is alsa
no way to make accurate economi
stant which survives over time — from the beginning of acting man’s
plan to its conclusion—by which any man can evaluate the success
of his efforts. As the Bible says, what does it profit a man if he gains
the whole world and loses his soul? Here is the most.crucial of all
economic decisions—the question of profitable stewardship before
God—and secular man cannot make this decision accurately. He has
no fixed moral standards by which to evaluate his success. Process
philosophy cannot provide standards, for no man can be sure that he has
not entered into a new world order between the time he began to
plan and the time he believes he has brought it to completion. Con-
tinutly over time—moral, epistemological, social, economic — cannot
be affirmed by means of any evolutionary philosophy.

How do we know that the market order still works? How do we
avoid Marx's argument that capitalism was far more productive
than feudal production methods, but its day has come at last, now
that proletarians are about to bring in a new world order? Hayek
cannot tell us. How do we know that our capitalist tools still are per-
forming better than socialist tools? Hayek writes:

 

calculations, for there is no con-

. we command many tools—in the widest scnse of that word — which the
human race has evolved and which enable us to deal with our environment.
‘These are the results of the expcrience of successive gencrations which are
handed down. And, once a more efficient tool is available, it will be used
without our knowing why it is better, or even what the alternatives are.

These “tools” which man has evolved and which constitute such an im-
portant part of his adaptation to his cnyironment include much more than
material implements. They consist in a large measure of forms of conduct
which he habitually follows without knowing why; thcy consist of what we
call “traditions” and “institutions,” which he uses because (hey are available
to him as a product of cumulative growth without ever having been de-
signed by any one mind, Man is generally ignorant not only of why he uses
implements of one shape rather than of another but also of how much is de-
pendent on his actions taking one form rather than. another... . Every
change in conditions will make necessary some change in the use of re-
sources, in the direction and kind of human activities, in habits and prac-
tices. And each change in the actions of those affected in the first instance
will require further adjustments that will gradually extend throughout the
whole of society. Thus every change in a sense creates a “problem” for soci-
cty, even though no single individual perccives it as such; and it is gradually
