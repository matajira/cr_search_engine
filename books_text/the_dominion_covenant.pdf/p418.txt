374 THE DOMINION COVENANT: GENESIS

speculations modern philosophy is built, also set forth the presuppo-
sitions in terms of which supposedly neutral “eternal oscillation”
astronomers have constructed their footnoted cosmologies. Reli-
gious presuppositions govern modern astronomical science and
modern geological science.

Men have abandoned the revelation of God. In the name of
science, they inform us that the belief in a creation by God a few
thousand years ago is preposterous — reversing St. Augustine’s dic-
tum. Yet in place of this creation account, physicist George Gamow
asks us to believe that the universe began its existence as a con-
densed droplet of matter at an extremely high density and
temperature. This primordial egg —the “ylem”— generated fantastic
internal pressures and exploded. As it expanded its temperature
dropped. As Robert Jastrow summarizes Gamow’s theory: “In the
first few minutes of its existence the temperature was many millions
of degrees, and all the matter within the droplet consisted of the
basic particles—clectrons, neutrons and protons. . . . According to
the big-bang theory, all 92 elements weré formed in this way in the
first half-hour of the existence of the universe,”?? Jastrow offers this
as a serious possibility; he is the Director of the Goddard Institute
for Space Studies, and the lectures were originally viewed over CBS
television in 1964 as a “Summer Semester.” The public is expected to
believe this, but not expected to take seriously the biblical account of
creation,

We are told that the laws of probability probably govern the
universe. The universe evolved in terms of these laws. Prof.
Charles-Eugéne Guye once estimated the probability of evolving an
imaginary (but given) random assortment of atoms into an equally
imaginary protein molecule containing a minimum of four atoms:
carbon, hydrogen, nitrogen, and oxygen. He did not assume the
coming of all 92 elements or even life itself—just the components of a
single protein molecule. The volume of original random atomic
substance necessary to produce—randomly—the single protein
molecule would be a spherc with a radius so large that light, travel-
ling at 186,000 miles per second, would take 10 years to cover the

 

72. Robert Jastrow, Red Giants and White Dioarfs (New York: Now American
Library, 1969), p. 69. This happened 10 billion ycars ago, says Jastrow. This figure
was revised to 13 billion in 1973, possibly older: 16 billion. Associated Press release:
Dec. 25, 1973.

 

  
