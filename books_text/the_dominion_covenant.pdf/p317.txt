From Cosmic Purposelessness to Humanistic Sovereignty 273

doing wrong so long as they cling to any absolute or unyielding certi-
tude.” (We might ask the obvious questions: Would the “absolutizing”
theologian or moralist always be wrong? Can Huxley be absolutely cer-
tain of this?) In a later essay, Huxlcy criticized his grandfather's view
of ethics as being too static. “We can now say that T. H. Huxley’s
antithesis between ethics and evolution was false, because based on a
limited definition of evolution and a static view of cthics. . . . More
than that, we perceive that ethics itself is an organ of evolution, and
itself evolves. And finally, by adopting this dynamic or evolutionary
point of view of ethics as something with a time-dimension, a process
rather than a system, we obtain light on one of the most difficult but
also most central problems of ethics—the relation between in-
dividual and social ethics, and perceive that the antithesis between
the individual and socicty can also be reconciled.”?? Evolution
means, above all, process—the ethics of historical relativism. _

How can these two forms of ethics be reconciled? In his 1943 lec-
ture, Huxley had argued for the supremacy of individualistic ethics,
since “it is clear on evolutionary grounds that the individual is in a
real sense higher than the State or the social organism. . . . All
claims that the State has an intrinsically higher value than the indi-
vidual are false. They turn out, on closer scrutiny, to be rationaliza-
tions or myths aimed at securing greater power or privilege for a lim-
ited group which controls the machinery of the State.”® He delivered
this spccch during World War II, and he made certain that his au-
dience knew where he stood. “Nazi ethics put the State above the in-
dividual.”®} The Nazi method is against evolutionism “on the
grounds of efficiency alone.”®? All of a sudden, evolutionism’s cthics
of relativism grew rock-hard: “Furthermore, its principles run coun-
ter to those guaranteed by universalist evolutionary ethics. . . .”8
The Nazis are doomed to fail, he concluded.

78. Did, p. 138.

79, Ibid, p. 217.

80. Tbid., pp. 138-39.

BL. fbid., p. 147,

82. dind., p. 148.

83, Idem. Huxley cannot resist taking a swing at the Old ‘lestament for its ex-
altation of the idea of a special chosen people. “In this the Nazis merely translate into
modem terms the ethics of tribes or peoples in an early barbarous phase of
the world’s history, such as the ancient Hebrews before the prophetic period”
{p. 147).
