‘the Misapplication of Intrinsic Value 233

By the second ycar of the famine, neither gold nor silver functioned
as a means of exchange any longer. “The money faileth,” cried the
hungry Egyptians, and so it had. The Pharaoh had in his possession
both the metal and the grain by the second ycar of the famine. If
anything functioned as money in Egypt, it was grain,

Gold and silver were no longer acceptable means of payment.
The bulk of the population no longer had much desire to possess
metals. This indicates a total breakdown of the economy. The reason
why men accept gold and silver in voluntary exchange for other
scarce resources is because they believe that other people will do the
same later on. Because people cxpect others to give up scarce
resources for the money metals sometime in the future, the metals
have exchange value in the present. This is what is usually meant by
the phrase, “storehouse of value.” Of course, value is not some
physical aspect of the metal. Valuc is imputed by acting men to the
metals because of estimates they have made concerning the will-
ingness of men later on to continue to.impute value to the metals.
Money metals, like all forms of money, are valued because of the future-
orientation of acting men. They use money in exchange today because
they expect to be in the market buying other goods and services with
money tomorrow or next year. They expect the traditional estima-
tions of others to prevail in the marketplace. They expect familiar in-
stitutional arrangements to prevail over time.

Yet we are told that the money failed. What also must have failed
was men’s commitment to long-term planning. They needed food.
They could not expect to survive over the long run unless they had
access to food in the present. The long run was discounted to prac-
tically zero. The famine made Egyptians intensely present-oriented.
Thus, the value of traditional monetary units fell to zero— “failed.”
Men lost confidence in the marketplace to supply them with their
needs. They looked to the State, with its warehouses filled with
grain, for their salvation. This shift in faith, or rather this shift in
confidence, destroyed the monetary unit of account that had pre-
vailed in the marketplace prior to the catastrophe. The State was
able to collect the money metals until they no longer served as
money (47:14-15),

What could then serve as a means of payment? Joseph set the
terms of exchange because he controlled the one asset the whole
world wanted (41:57). First he asked for their cattle, and they
capitulated (47:16-17). At the end of the year, they were back,
