20 THE DOMINION COVENANT: GENESIS

acknowledged sovercign in the twentieth —and not in God’s purpose
and design. To quote the Medawars’ statement once again: “It
is upon the notion of randomness that geneticists have based their
case against a benevolent or malevolent deity and against. there
being any overall purpose or design in nature.” The Medawars have
spoken not simply for geneticists, but for the whole of modern
science.

To overcome the logic of Paley, late-nineteenth-century scientists
took the first crucial step: to ascribe the origin of perceived order to
random change. This Appothesis was the major intellectual revolution of the
nineteenth century. The importance of this scientific presupposition
cannot be overestimated: it. served to free secular science from
critics, potential and actual, who might have succeeded in redirec-
ting the work of scientists along biblical lines. But there was a more
fundamental aspect of this affirmation of randomness: to shove God out
of the universe, once and for all. Man wanted to escape the threat of con-
trol by a supernatural Creator, Once that step had been taken,
scientists took a second step: to assert the sovereignty of man. Since
there is no cosmic purpose in the universe, secularists concluded,
man is lef free to make his autonomous decisions in terms of his own
autonomous plans. Man becomes the source of cosmic purpose. The
purposcless forces of random evolutionary change have at long last
produced a new, purposeful sovereign, man, and man now asserts his
sovereignty over creation. He takes control, by means of science, of
the formerly purposeless laws of evolutionary development. The
universe needs a god, and man is that god.

The concept of an order which developed bul which was not
transcendently designed appeared first in the social sciences,
especially in the writings of the Scottish rationalists, most notably
the two Adams, Ferguson and Smith. These two mid-eighteenth-
century social theorists were attempting to explain the rationality of
the market economy in terms of human actions that had never been
intended to produce the market order, The market was explained as
the product of human action, but not of human design. The evolu-
tionary nature of this explanation should be clear: society is the pro-
duct of spontaneous forces that are not controlled by any overall pur-
pose of a personal authority. F. A. Hayck, the twentieth-century
economist and social philosopher, has devoted the bulk of his later
academic career to a comprehensive consideration of the implica-

 
