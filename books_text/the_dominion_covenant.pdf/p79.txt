The Dominion Covenant 35

‘ecologic’ crisis is really only one aspect of the pervasive moral and
cultural crisis of our time, and the cause of this crisis is pride. For too
long we have believed that no bounds need be placed on human am-
bition and desire, but now it has been discovered that even scientific
technology, the instrument of modern man’s intended self-deifica-
tion, must bow to the finitude of reality,”!* In short, it is the arro-
gance of autonomous man, who has inherited the products of a
Christian vision of dominion through adherence to law, but who no
longer acknowledges the sovereignty of the God who establishes the
law-order which transfers power to man, that has created the pollu-
tion crisis. It was not the fault of Christianity, which always regarded
the earth as capital wealth entrusted to man as something to be
treated with deference..Man is a steward in the Christian view, not
an owner of the earth (Ps. 24:1). The secularists denied God and
transferred God’s sovereignty to man. “Man, the steward” became
“man, the autonomous owner,” and modern ecological devastation
began in earnest.

Maz is to subduc the earth, not destroy it. Man is to replenish it,
care for it, use it to God’s glory. This permits him to benefit from the
fruits of the land, for he is made in God’s image. When man tries to
appropriate the fruits of the earth apart from the restraining law of
God, then he can expect results that are costly to him, Ours is a
universe of law, and the moral law of God is more fundamental than
the natural regularities of the created realm. Moral law is primary,
and God has built into’ His world a kind of “negative feedback.”
When men consistently and systematically violate the moral law of
God in a certain area of life, external events— seemingly unrelated
to the moral realm— begin to place restraints on the rebels. The best
example in Scripture is the tree of the knowledge of good and evil.
Men rebelled against God through their father, Adam (Rom. 5).
Adam ate of the tree. The whole creation was cursed as a result.
God’s word predicted the penalty of death, but Satan implied that
God’s word could not be trusted. How could any “neutral” scientist
have predicted any cause-and-effect relationship between the eating
of a particular fruit and the cursing of the universe? But that super-
naturally controlled cause-and-effect relationship was there. Ours is
a universe of cosmic personalism. God respects His word more than

14, R. V. Young, Jr., “Christianity and Ecology,” National Review (Dec. 20, 1974),
p. 1479.
