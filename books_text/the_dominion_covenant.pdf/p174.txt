130 THE DOMINION COVENANT: GENESIS

creation, but it is not fundamentally a burden, as it is for the rebel-
lious. Those who wish to escape time altogether are rebellious. They
choose occult methods of achieving secret knowledge, mystical il-
lumination, or some secret formula to give them perfect power or
perfect protection, Work-in-time was an opportunity for Adam.
Work-in-time-under-the-curse is a burden for sinful man. The unre-
generate hope to escape work and time by becoming omniscient and
omnipotent, and even eternal, like God. The godly man hopes to
escape the curse of time by overcoming sin and working in time,
directed by the guidelines provided in God's law.

Conclusion

Time, matter-energy, and space appear to be constants in the
creation. Certainly, we do not delay the coming of God’s day of judg-
ment by manipulating the universe in some way. Time does not
change, but the processes within time’s fixed limits do change, and
so do men’s attitude toward those processes, Men may view time’s
processes as opportunities to be used to the glory of God, or as
burdens to be overcome through autonomous scientific techniques,
or through magic, or mystical illumination. But time remains man’s
theater of response to God,

It is therefore understandable that the modern State should
attempt to seek control of the processes of time, as well as to seek to
control those features of the economy that are the product of a par-
ticular religious perception of the meaning of time. The secular
humanist State seeks to attain the benefits of low interest rates by
imposing usury laws that force down the legal, visible rates of in-
terest in the various loan markets. The bureaucrats try to promote
economic growth by lowering short-term interest rates by increasing
the money supply, in a vain attempt to increase long-term produc-
tion. They attempt to stimulate economic growth in the ghetto, or in
backward foreign nations, through humanistic, tax-supported
schools, or through tax-financed welfare programs of all kinds. They
do not deal with the central problem of the ghetto and the underdeveloped na-
tion, namcly, the present-oricntation of thosc who make up the bulk
of the backward or poverty-stricken population. Indeed, “poverty-
stricken” is a phrase indicating that poverty is an active force that
suppresses the innate creativity of man—a hypothetical universal
creativity. But men’s time perspective can overcome all the foreign
aid funds that any government agency might send into a high time-
