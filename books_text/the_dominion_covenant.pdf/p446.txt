402 THE DOMINION COVENANT: GENESIS

in 1857 that “I am a firm believer that without speculation there is no
good and original observation,”!#1 And in 1860 he wrote to Lyell that
“without the making of theories I am convinced there would be no
observation.”“? Thus, we can side safely with Himmelfarb’s judg-
ment: “As the notebooks amply demonstrate, he was speculating
boldly from the very beginning of this period [1837], and his specula-
tions were all directed to a particular theory—that of mutability.
What is impressive about these early notebooks is not the patient
marshaling of the evidence, which in fact was conspicuously absent,
but rather the bold and spirited character of his thought. What clearly
urged him on was theory capable of the widest cxtension and a mind
willing to entertain any idea, however extravagant.”

In the fall of 1838, Darwin read Rev. Thomas Malthus’ classic
study in political economy, An Essay on the Principles of Population
(1798). This, he later said, transformed him, Malthus’ hypothesis of
a geometrically expanding population pressing against an arithmet-
ically expanding food supply convinced him that the key to the
species question is the struggle for existence. It is doubly interesting
that Wallace admitted that it was his recollection of Malthus’ theory,
during his fever, that triggered his formulation of the theory of natu-
ral selection. Once again, a minister had been crucial— indirectly,
this time — in the steady progress of the theory of evolution. Darwin’s
theory was basically complete as carly as 1838. Lest we forget the cir-
cumstances of this intellectual breakthrough: “Darwin was only
twenty-nine and barely out of his apprenticeship, so to speak, when,
by this second leap of imagination, his theory took full shape. If this
chance reading—or misreading—of Malthus, like his first general
speculations about evolution, scemis too fortuitous a mode of inspira-
tion, the fault may lie not with Darwin but with the conventional no-
tion of scientific discovery. The image of the passionless, pains-
taking scientist following his data blindly, and provoked to a new
theory only when the facts can no longer accomodate the old, turns
out to be, in the case of Darwin as of others, largely mythical.”!**

There was another interesting coincidence during this period.

141. Ibid., £, p. 465.

142. Ibid, T, p. 108.

143. Himmelfarb, p. 156.

144, Tbid., p. 66. See also'Thomas Kuhn, The Structure of Scientific Revolutions (2nd
ed.; Chicago: University of Chicago Press, 1970) and James D. Watson, Zhe
