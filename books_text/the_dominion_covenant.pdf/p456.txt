412 THE DOMINION COVENANT: GENESIS

this religiously held belief come into direct conflict with a belief that
man’s mind can be relied upon precisely because man is made in the
image of God, then he doubts the capacity of his monkey-descended
mind to grapple with such abstract questions. We are intelligent
enough to know that we are not intelligent enough to know; we can
have. sufficient confidence in our minds to rest assured that we can
have no confidence in our minds. God is locked out of His universe
by man’s simultaneous confidence and lack of confidence in his own
logic. Neither doubt nor confidence is allowed to point to God,
Cosmic impersonalism is thereby assured; autonomous man is
defended by his supposedly autonomous science. Like the universe
around man, his thought processes are simultaneously absoluée (man
is descended from lower animals; no other theory is valid!”®) and con-
tingent (man cannot trust his own speculations when they concern ab-
solutes),

Anyone who imagines that the implications for philosophy of
Darwinism are not both widespread and important in modern life is
impossibly naive. It was not the details of the Darwinian system that
captivated European thought— Darwin had to repudiate much of his
system anyway. He once admitted to his earliest supporter, J. D.
Hooker, that he was proficient “in the master art of wriggling.”!8¢
Few biologists could follow all of his arguments; if they had done so,
they would have grasped the fact that his retreat into the categories
of “use and disuse” represented a revival of Lamarckianism. But
they did not read his works that closely. Liberated men scarcely
question the logic or fine points of their liberator’s scriptural canon.
What did capture the minds of intellectuals, and continues to captivate them, is
Darwin’s rejection of meaning or purpose; the post-Darwintan universe has no
traces of final or ultimate causation.

A marvelous statement of the Darwinian faith was presented in
the Britannica Roundtable (Vol. 1, #3, 1972), a slick magazine which is
on the intellectual level of the Sunday newspaper’s magazine insert,
but which parades under the banner of high culture. C. P. Snow,
ballyhooed in the early 1960’s because of his propaganda favoring the
fusion of the “two cultures”—autonomous rational science and the
equally autonomous humanities—offered us his personal credo in
“What I Believe”: “I believe life, human life, all life, is a singular

179. Origin, ch. 11, p. 268; quoted earlier.
180. Darwin to Hooker (Dec. 10, 1866), Life & Letters, TI, p. 239.
