152 THE DOMINION COVENANT: GENESIS

be wasted to maintain control. Productivity drops.

As always, there was an element of grace within God’s external
judgment. While He stymied the pagan’s centralized religious-
political order, with its pretensions of autonomy and absolute sover-
eignty, He simultaneously gave men conditions that were more
favorable to political freedom. Localism, the criterion of a decentral-
ized free order, could then be infused with another requirement of a
free society, biblical faith.

Second, God tied this decentralization to the existence of separate
languages. Apart from religion and direct family ties, there are few
bonds, if any, that are culturally more binding than a shared lan-
guage. When a language ceases to be spoken, it is because the soci-
ety in which the language once flourished has either been destroyed,
scattered, or died out. God provided men with a key factor in the
creation of a sense of community, without which human society can-
not survive.

Third, God scattered them geographically. Economically, this was
a very important aspect of Gad’s judgment. Prior to the great flood,
there seems to have been a common climate. Mammoths found in
the Arctic still have semi-tropical foliage in their stomachs, indicating
a rapid cooling (otherwise the contents of the animals’ stomachs
would have rotted inside the stomachs before the frozen outer bodies
had time to pass the cold to the inner parts). One estimate has placed
the necessary external temperature at minus 150° Fabrenheit—in
the middle of the “arctic” topics! The change was widespread and
rapid, After this cataclysmic alteration of the earth’s tropical or semi-
tropical climate, the mammoaths no longer munched foliage in
Siberia; a jungle no longer bloomed beneath the new ice of Antare-
tica. Men would now live in differing climates and on land with
varying agricultural resources. They could specialize their economic
production, which was a necessity in order to increase output per

3. The most sophisticated estimates of the temperatures required lo quick-freeze
a Siberian mammoth are found in Joseph C. Dillow, The Waters Above: Earth's Pre-
Fload Vapor Canopy (Chicago: Moody Press, 1981), pp. 383-96. His book provides an
extensive bibliography of the source material, including Henry H. Howorth’s out-
of-print dassic, The Mammoth and the Flood (London: Sampson Low, Marston, Searle
& Rivington, 1887); Charles Hapgood, The Path of the Pole (Philadelphia: Chilton,
1970); Bassett Digby, The Mammoth: And Mammuth-Hunting Grounds in Northeast Siberia
(New York: Appleton, 1926), Dillow’s book is the best, to date, on the universal
climate before Noah's flood, and the catastrophic climatic changes that the flood pro-
duced,
