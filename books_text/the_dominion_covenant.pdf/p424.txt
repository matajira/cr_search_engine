380 THE DOMINION COVENANT: GENESIS

resented, They say that “his fundamental aims were conservative
and devout.” He was just an honest observer of facts, letting them
carry him to some cosmically neutral conclusion. They ask: Why did
his contemporaries attack him? For one thing, it was not simply
theology that motivated his opponents; his position was undermin-
ing Vulcanism’s catastrophism, while simultaneously undermining
Neptunism, since Hutton laid great emphasis on the power of slowly
acting subterranean heat.®8 He was stepping on everyone’s method-
ological toes. But some of the opposition was theological. Naively,
Toulmin and Goodfield remark: “Yet there was, in fact, nothing in
Hutton’s system—apart from the unbounded chronology—that
could legitimately give offense.”® That, however, was precisely the
point, as Eiseley understands so well; “The uniformitarians were, on
the whole, disinclined to countenance the intrusion of strange or
unknown forces into the universe. They eschewed final causes and
all aspects of world creation, feeling like their master Hutton that
such problems were confusing and beyond human reach. The
uniformitarian school, in other words, is essentially a revolt against
the Christian conception of time as limited and containing historic
direction, with supernatural intervention constantly immanent [im-
manent—“inherent, operating within’—not imminent—“about to
happen’--G.N.], Rather, this philosophy involves the idea of the
Newtonian machine, self-sustaining and forever operating on the
same principles.”9¢

There should be no confusion on this point: the great theological
debate centered around the question of time. All good men— French-
men excepted, naturally —believed in a personal God in the period
1750-1850. This God was allowed io be a creator in some sense or
other. But by pushing the time or order of God’s creative acts back
into a misty past, men were relegating this God into a mere intellec-
tual construction—a kind of useful myth, like Plato’s creator god.
One’s concept of time is fundamental in defining one’s concept of God.

Prior to Lyell’s conversion to Darwinism, his view of time was
almost static. Some geological forces tend to raise portions of the
earth’s crust; there are forces elsewhere which tend to allow land to
sink. If elevation is happening in one region, leveling or erosion is
taking place somewhere else. It has been this way indefinitely. The

88. Greene, Adam, p. 84.
89, Discovery of Time, p. 156
90. Darwin's Century p. U4. Cf. Nisbet, Social Change and Histon, p. 184.
