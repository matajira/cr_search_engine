210 THE DOMINION COVENANT: GENESIS

the costs, if the asset is publicly owned and therefore not subject to
the subjective evaluation of costs by the legal owners, namely the
voters? Who is to say whether the bureaucrats’ assessment of the
“true” costs and benefits to the “public” are the same as the “public”
would assess them? And how is the “public”—a collection of in-
dividuals —to register its collective judgment? Who pays the piper,
whose ox will be gored, and who cats the cake? So the management
of publicly owned resources tends to swing between policies of
overuse and no use, between the profligate squandering of resources
through “free” Icases that lead to erosion, and the profligate
squandering of resources by allowing valuable asscts to sit inactively.
First the bureaucrats allow erosion, then they require total conserva-
tion, which means that productive assets are rendered unproductive,
or productive only for those few people who enjoy using the resource
in a legally acceptable way (such as hikers who enjoy the wilderness
and who do not enjoy the sound of chain saws or other tools of pro-
duction).

The Puritans of New England learned these lessons early. After
1675, with half a century of mismanaged common lands behind
them, they steadily sold off the communally owned property to
private owners. The bickering about who was to pay for the cattle
herders, how many trees were to be cut down yearly, whose fences
were in disrepair, and the costs of policing the whole unmanageable
scheme, finally ended. So ended the “tragedy of the commons.””

Dominion and Diminishing Returns

The law of diminishing returns, when structured through the private own-
ership of scarce resources, becomes an incentive for the fulfillment of the cultural
mandate. Men reach the limits of productivity of a particular produc-
tion process, and then they are forced to find better methods of pro-
duction, or to find additional quantities of some overextended factor
of production. They musi either intensify production through better
technology and more capital, or search for more of the resource
which has reached its limits of productivity under the prevailing pro-
duction “formula” or “recipe.” The overextended resource may be
land, or a building, or the labor supply, or managerial talent, or
forecasting skill, or any other scarce economic resource. When its

7. Gary North, “The Puritan Experiment in Common Ownership,” The Freaman
(April, 1974).
