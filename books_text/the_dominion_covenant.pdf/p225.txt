Competitive Bargaining 181

selves from the subsidy by our protest.”” His conclusion is straight-
forward: “We need therefore to call most of which passes for charity
today exactly what it is. First, it is a subsidy for evil. Second, it in-
volves a penalizing and taxing of the righteous in order to subsidize
evil, and this penalizing of the godly is an important part of this false
charity. Third, basic to this kind of action is a love of evil, a prefer-
ence for it and a demand that a new world be created in which evil
will triumph and prevail.”®

Had one of us had the responsibility of offering counsel to Jacob,
what would have been the proper advice? Would we have told him
that he owed a meal to his lawless brother? If so, then our counsel
would have meant the loss of the value associated with the birth-
right. If Esau was willing to trade his birthright for a meal, which
was the case, then he was asking Jacob not simply to give him a free
meal, but rather to give him permanent possession of the birthright, If Jacob
was in a position to ask for and receive the cherished birthright—
cherished by Jacob but despised by Esau, the Bible says — then to fail
to make the transaction meant giving up the birthright that was virtually
in his hands. It was not simply the value of the food that Jacob would
have had to forfeit, but the value of both the food and the birth-
right— the birthright which Esau valued less than food.

To understand the nature of this exchange, we must also con-
sider verse 34: “Then Jacob gave Esau bread and pottage of lentiles;
and he did eat and drink, and rose up, and went his way: thus Esau
despised his birthright.” What an astounding recovery from the
brink of death! A few moments before he had announced that he was
facing a life-and-death crisis. Yet after one hearty meal, he sauntered
out undaunted, The New Testament does not say that he traded his
birthright away for his life; he traded it away for “one morsel of
meat.” One morsel of meat is not the dividing line between life and
death. He wanted a handout. He did not deserve mercy.

What are we to make of Esau’s words? Did he really believe that
he was facing death from imminent starvation? If so, he was present-
oriented to a fault. His stomach was growling, and he simply could
not bear the discomfort, Another possible interpretation is that he
was lying to Jacob about his condition. He wanted a meal and

7. R. J. Rushdoony, Bread Upon the Waters (Fairfax, Virginia: Thoburn Press,
[1969] 1973), p. 5, This book is a compilation of columns Rushdoony wrote for The
California Farmer.

8. Lid, p. 6.
