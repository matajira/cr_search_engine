Bibliography 473

(1942); all published in London by William Hodge & Co. (British
books arc most casily ordered through BlackwelPs, Broad Strect,
Oxford, England.)

The books written by Murray Rothbard, the anarcho-capitalist,
are all very clear, well documented, and powerfully argued. They in-
clude Man, Economy and State (1962), published by several firms, but
most recently by New York University Press; America’s Great Depres-
sion (1963), available from the Institute for Humane Studies, 1177
University, Menlo Park, CA 94025: and What Has Government Done to
Our Money? (1964), available from the Foundation for Economic
Education, Irvington, NY 10533. His book denying the legitimacy of
all civil government is very good on the economic effects of various
kinds of government regulation and taxation: Power and Market: Gov-
ernment and the Economy (1970), published by Institute for Humane
Economy.

F, A, Hayek’s books are basic to any understanding of the free
market. The most important are: The Road lo Serfdom (1944), The Con-
stitution of Liberty (1960); Individualism and Economic Order (1948);
Studies in Philosophy, Politics and Economics (1967); and the trilogy, Law,
Legislation and Liberty (1973-80), all published by the University of
Chicago Press. Also important is his study of the rise of socialist
thought, The Counter-Revolution of Science (1952), which has been
reprinted by Liberty Press.

By far the best book on the economics of information is ex-
Marxist economist, Thomas Sowell:. Knowledge and Decisions (Basic
Books, 1980), which provides more unique insights, page for page,
than any economics book I have ever read. His Race and Economics
(David McKay Co., 1975) is also very good, as are Markets and
Minorities (1981) and Ethnic America (1981), both published by Basic
Books.

Two books by Bettina Greaves are suitable for an introduction to
free market thought: Free Market Economies: A Syllabus and Free Market
Economics: A Basic Reader, published by the Foundation for Economic
Education, FEE also publishes the monthly magazine, The Freeman
(subscription by request, free). Address: Irvington, NY 10533.

Very important are the works of R. J. Rushdoony, which are
related to questions of economics: The Institutes of Biblical Law, Vol. I
(Craig Press, 1973), and Vol. II (Ross House Books, P.O. Box 67,
Vallecito, CA 95251); Politics of Guilt and Pity (1970) and The One and
the Many (1971), both published by Thoburn Press, P.O. Box 6941,
