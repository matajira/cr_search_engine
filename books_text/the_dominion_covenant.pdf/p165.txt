The Burden of Time lai

find the answer on hand-held calculators, or she can find the cube of
777. She can instantly tell you what factorial 13 is, which is-13 times
12 times 11, and so forth, down to one: 6227020800. “Never use com-
mas,” she says. “They'll only confuse you.” She can tell you what day
of the week it was on say, Nov. 3, 1949. But she cannot tell you how
she accomplishes these feats.3 Eric Jablow taught himself how to
read by the age of 20 months, or possibly sooner, and he taught
himself calculus at age 6. He graduated from Brooklyn College with
highest honors at age 15. He had attended graduate school lectures
in mathematics as early as age 7. These people are obviously abnor-
mal, yet they are common enough in every generation to remind us
of what we have lost since the Fall.

A question could legitimately be raised concerning the source of
these abnormal powers of mind. Is it possible that demonic, occult
forces are behind them? In some cases, it is not only possible but
probable. But no universal generalization can be made with com-
plete confidence. A case of one occultist who developed extraor-
dinary mathematical powers as a result of his family’s trafficking
with demonic forces was the great Indian mathematician, Ramanu-
jan, who died at the age of 32 in 1920. His biographer, S. R.
Ranganathan, devotes several pages to a discussion of Ramanujan’s
occult background. He reported having a dream as a young man.
The family goddess, Namagiri, “wrote on his tongue. Thereafter his
precosity developed suddenly. It has been stated by his mother that
he was born after her parents had prayed to the Goddess to bless her
with a son. There is another piece of information current in
Ramanujan’s family. His maternal grandmother was a great devotee
of Goddess Namagiri. She would often go into a trance and speak as
Goddess Namagiri. In one such trance, before the birth of Ramanu-
jan, she is said to have uttered that, after her own death, the God-
dess would speak through the son of her daughter.”® His mother was
an astrologer, and she predicted her son’s death a month before it
happened. She consulted another professional astrologer about her
son’s horoscope, without revealing whose it was, and he confirmed
her fears.6 The mathematician would narrate occult experiences to

3. Washington Post (Oct. 4, 1976).

4, Washington Star (May 22, 1977).

5. $.R. Ranganathan, Ramanujan: The Man and the Mathematician (Bombay: Asia
Publishing House, 1967), p. 13

6. Ibid., pp. 13-14.
