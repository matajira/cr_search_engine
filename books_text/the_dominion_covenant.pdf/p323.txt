From Cosmic Purposelessness to Humanistic Sovereignty 279

must be in control, That is the first and last ethical word.’ Promises a
third scientist, our newly developed eugenic potential will lead hu-
manity to ‘a growth of social wisdom and glorious survival —toward
the evolution of a kind of superman.’ "1? The book is well titled; Who

Should Play God?

Darwin’s Revolution

What a magnificent sleight-of-hand operation the defenders of
evolution and humanism have accomplished! First, the universe was
depersonalized. Darwin put it very forcefully: “It has been said that I
speak of natural selection as an active power or Deity; but who ob-
jects to an author speaking of the attraction of gravity as ruling the
movement of the planets? Every one knows what is meant and im-
plied by such metaphorical expressions; and they are almost
necessary for brevity. So again it is difficult to avoid personifying the
word Nature; but I mean by Nature, only the aggregate action and
product of many natural laws, and by laws the sequence of events as
ascertained by us.”!°3 God was shoved out of the universe, leaving
only humble man, whose power seems to be limited to “ascertaining
laws,” which are the sequence of events observed by us. Second, man
was reduced to being a mere cog in a mighty machine, not the
representative of an infinite God, governing the earth as a subor-
dinate in terms of the dominion covenant. A few paragraphs later,
Darwin wrote: “How fleeting are the wishes and efforts of man! how
short his time! and consequently how poor will be his results, com-
pared with those accumulated by Nature during the whole geological
periods! Can we wonder, then, that Nature’s productions should be
far ‘truer’ in character than man’s productions; that they should be
infinitely better adapted to the most complex conditions of life, and
should plainly bear the stamp of higher workmanship?”!° Not the
higher workmanship of the Gad of the Bible, or even the deistic gad
of Paley’s Natural Theology, but the “higher workmanship” of planless,
meaningless, “random, yet cause-and-effect-governed” geological
and biological process. 7ird, evolutionists added a purposeful,

102. Ibid., p. 21. See also Michael Rogers, Biohazard (New York: Knopf, 1977);
June Goodfield, Playing Gad: Genetic Engineering and the Manipulation of Life (New York:
Random House, 1977); Nicholas Wade, The Ultimate Experiment: Man-Made Evolution
(New York: Walker & Walker, 1977).

103. Charles Darwin, Origin of Species, ch. 4; in The Origin of Species and the Descent
of Man (Modern Library edition), p. 64.

104. Ibid., p. 66.
