The Law of Diminishing Returns 209

immediately realized; the costs associated with the depletion of the
resource are borne by all taxpaying citizens — an infinitessimal addi-
tional cost to the actual user. Since it is not his land, he need not con-
serve its long-run productivity. A kind of “positive feedback” occurs.
It generally pays to add one more cow or cut down one more tree,
unless the variable costs— supervising the cow, sharpening the saw,
spending the time — have risen so high that even the “free” land is not
a sufficient subsidy to continue production, The “positive fecdback”
process can continue until che ecological crisis hits, and the produc-
tivity of the “free” resource plummets. The “negative feedback” of
the law of diminishing returns is temporarily blunted, since the
retarding factors increased costs of maintaining the long-term pro-
ductivity of the resource —are not registered forecfully in the mind of
the user. Others bear these costs, and his personal benefits far
outweigh his share of these costs. Eventually, the law reasserts itself
visibly, since it is simply a discovered regularity based_on a real fact,
namely, the curse of the ground: But the crisis may give few warn-
ings, at least few that the user will recognize or respect. It comes all
at once, not in smaller portions that an owner of private property
would be more likely to recognize and take steps to alleviate or re-
verse. Without private ownership of the means of production, the law
of diminishing returns does not produce those warnings concerning
the impending advent of radically reduced output from an overused
resource.® Or more accurately, the warnings are not heeded so
rapidly. (Economists call this the problem of “externalities.”)

It is extremely difficult and costly for bureaucracies to evaluate
the full effects of the use of any publicly owned resource. The costs of
upkeep in relation to the benefits of use are evaluated by different
people. The reality of the subjective theory of value asserts itself.
The bureaucrats in charge of managing or leasing the public prop-
erty must estimate the value produced by the users of the resource,
and this is inevitably impossible to estimate without prices. But even
prices do not tell the administrators everything they wish to know. Is
the subsidy to the public of “free” land, for example, really the best
way to benefit the public? How can any bureaucrat determine the
answer? Are the costs too high? Again, how can he put a price tag on

5. Garrett Hardin, “The Tragedy of the Commons,” Science (Dec. 13, 1968); re-
printed in Garrett de Bell (ed.), The Environmental Handbook (New York: Ballentine,
1970).

6. C.R. Batten, “The Tragedy of the Commons,” The Freeman (Oct., 1970).
