470 THE DOMINION COVENANT: GENESIS

Conclusion

The development of a godly sense of judgment takes many years.
The emphasis of the Bible on the importance of training in the law is
central to the question of godly judgment. “And these words, which I
command thee this day, shall be in thine heart: And thou shalt teach
them unto thy children, and shalt talk of them when thou sittest in
thine house, and when thou walkest by the way, and when thou liest
down, and when thou risest up” (Deut. 6:6-7). The mastery of God's re-
vealed law is fundamental for rendering righteous, provisional, subordinate
Judgment, just as it was on that first working day in Eden.

One of the main reasons why Christians are culturally impotent
today is that for well over a century, they have been taught alter-
native theories of law. They have been told that Christianity can sur-
vive under any system of law, The accent is on mere survival. There
is supposedly no prospect of Christians exercising godly rule in every
area of life. Of course, we are told, Christianity cannot be expected
to flourish under any system of law, not because of specific kinds of
flaws in humanistic law systems, but because the church is sup-
posedly impotent by nature in history. For many of thase wha
believe that Christianity is doomed to historical impotence, there
seems to be no reason to call forth ridicule, let alone persecution, on
themselves by declaring that all humanists are wearing fig leaves,
and that revealed law is the only way that God wants us to cover our
nakedness, through grace. Meanwhile, they can buy an “off the
rack” fig leaf wardrobe from the latest humanist collection—well,
maybe not the latest, but a discount version that is only ten years out
of date. “Better to be trendy ten years late than never to be trendy at
all!”

Fig leaves do not stand up to the howling winters of a cursed world, When
Christians finally learn this lesson, they will be ready to begin to ex-
ercise godly judgment.
