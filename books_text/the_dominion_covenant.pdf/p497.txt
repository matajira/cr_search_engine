Basic Implications of the Six-Day Creation 453

and truth” (I Cor. 5:7-8), The Passover feast was covenantal and
ethical. The pagan creation festivals are exclusively metaphysical.
They assume a common bond between God and man—a common
bond of pure being. The Passover assumed a covenantal and ethical
bond between God and His people; in the communion service, this is
symbolized by the eating of bread and the drinking of wine. Christ’s
body and blood are symbolized, and men participate in His perfect hu-
manity. They hope for the day when they shail be recreated and dressed
in perfection like His body (Phil, 3:21). But we can never participate
in Christ's divinity, God is fundamentally different from man.

The pagan festivals have basic similarities. They all are based on
the idea that the world was created by God in a massive struggle
with chaos. Creation was not out of nothing; it was the triumph of
order over chaos. God therefore is said to confront chaos. The impli-
cation is that God, no less than men, faces zones of pure chance and
unpredictability. He faces a world which is only partially known to
Him, In other words, we are like God, only less powerful and less
knowledgeable, relatively speaking. By reenacting the original crea-
tion, men believe that they can participate in the original pre-time
event. Men can share the act of creation, thereby escaping ritually
(and, some cultures believe, actually) the bondage of time. Satur-
nalia, Mardi Gras, and Carnival are all chaos festivals. Laws are
broken, mores are violated, masks are worn, and men are revitalized
from below. They become co-creators, co-participants with God in
the act of original creation.2? The creation, since it was not an ab-
solute creation out of nothing by the fiat word of a sovereign God,
can therefore be thought of as just one more finite event, however
important. Paradise is to be reestablished through ritual chaos—
total moral discontinuity brings back the age of gold.

The biblical promise of the new creation is based upon the grace
of a totally sovereign Creator. He restores men ethically. He puts
His law in their hearts, This was the promise in Jeremiah 31:31-34, it
was fulfilled by Christ (Heb. 8:9-43; 10:16-17). God’s promises and
His prophecies are being fulfilled or have been fulfilled in this age,
the age of the Church, the body of Christ. We can thus celebrate the
covenant of God with the people of Israel, for we are called “the
Israel of God” (Gal. 6:16).23 Our celebrations are not disorderly, for

22. See the references to the works of Mircea Eliade, footnote no. 13, p. 444.
23. Roderick Campbell, Israel and the New Covenant (Tyler, Texas: Geneva Divinity
School Press, [1954] 1982).
