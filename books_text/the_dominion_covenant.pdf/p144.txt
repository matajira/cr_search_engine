9
COSTS, CHOICES, AND TESTS

And when the woman saw that the tree was good for food, and that it was
pleasant io the eyes, and a tree to be desired to make one wise, she took of
the fruit thereof, and did eat, and gave also unto her husband with her;
and he did eat (Gen. 3:6).

Eve had already made a series of crucial assumptions about the
nature of reality before she offered the fruit to her husband. She had
already renounced the system of interpretation which God had given.
to her husband. God’s revelation of Himself and the creation no
longer impressed Adam and Eve. They had decided to test the
validity of God’s word against the validity of the serpent’s. In fact,
they had already decided that God’s revelation could not possibly be
true, since He said that His word is true, and that they would be
punished for sure if they ate of the tree. God had revealed an all-or-
nothing universe, for it did not permit them the option of eating the
fruit without punishment. They concluded that this all-or-nothing
proposition could not possibly be true, for if it were true, they would
surely perish. By affirming the hypothetical possibility that they
might not perish, they were simultaneously affirming that God’s
denial of such a possibility had to be false. There was simply no
possibility that they might eat of the fruit and retain the status quo
anie, Everything would change, They hoped things would change for
the better. They miscalculated.

All value is subjective, meaning personal. This docs not mean
that no value is ever objective, When we speak of subjective valua-
tion, we simply mean “economic valuation made by a person.” God
is a personal being. He imputed value to His creation, calling it
good, thereby confirming the goodness of His handiwork. The crea-
tion was not good in itself, meaning autonomously good or intrin-
sically good, irrespective of God’s work and evaluation. God, not the

100
