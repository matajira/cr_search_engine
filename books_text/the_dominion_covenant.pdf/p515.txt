BIBLIOGRAPHY

Economics

It is difficult to recommend a list of books on Christian eco-
nomics, since the thesis lying behind my writing of this book is that
the Christian world has neglected the whole question for three cen-
turies. There is nothing on a par with Adam Smith’s Wealth of Nations
or Ludwig von Mises’ Human Action. The best we have available are
collections of essays, monographs on certain topics, and one very in-
troductory textbook.

The textbook is Torn Rose’s Economics: Principles and Policy from a
Christian Perspective (Mott Media, P.O. Box 236, Milford, MI 48042).
He co-authored (with Robert Metcalf) The Coming Victory (Christian
Studies Center, P.O. Box 11110, Memphis, TN 38111).

I compiled a collection of 31 essays, An Inireduction to Christian Eco-
nomics, published by Craig Press in 1973. It is presently out of print,
It is scheduled to be reprinted by the Institute for Christian Eco-
nomics, I wrote these essays for several magazines and newspapers
from the late 1960’s through the early 1970's.

For those interested in Marxism, my out-of-print book, Marx’s
Religion af Revolution: The Dostrine of Creative Destruction (1968) might
be useful. No one who makes a detailed study of socialism can afford
to miss Ludwig von Mises’ classic refutation, Socialism: An Economic
and Sociological Analysis (1922), which is available from Liberty
Classics (7440 N. Shadeland, Indianapolis, IN 46250). It was
published in the United States by Yale University Press in 1951.

I offer a critique of the epistemology of modern schools of eco-
nomic thought in my essay, “Economics: Fram Reason to Intuition,”
in North (ed.), Foundations of Christian Scholarship (Ross House Books,
P.O. Box 67, Vallecito, CA 95251). Two humanist books criticize the

471
