142 THE DOMINION COVENANT: GENESIS

productive citizens are sheared of their wealth, while the welfare
recipients are sheared of their self-respect, their independence, and
their incentive to work.?

God’s answer to Cain’s lie and his rhetorical question: He knew
where Abel was, Cain knew where Abel was, and judgment had now
arrived. In short, He paid no attention at all to Cain’s rhetorical
question. Cain was a murderer, not a keeper. Abel had not been a
sheep. He had been a man. Judgment had arrived.

Some of those who parrot the “brother’s keeper” phrase may be
nothing more than ignorant, misled, goodhearted people who know
little about the Bible and less about responsible living apart from
coercive wealth redistribution. But those who first made the phrase
popular were not ignorant about the Bible. They knew very well
what the Bible said, and they rejected its testimony. They were
determined to rewrite the Bible, misinterpret the Bible, and create a
new secular humanist religion in the name of the Bible. They knew
the power of the pulpit in the United States, and they sought to cap-
ture the seminaries, religious publishing houses, and religious news-
papers in every denomination, With few exceptions, they had
achieved their goals by 1940 in the Nortb, and by 1965 in the South.
The seminaries had become liberal by the 1930’s in most of the
denominations, so it was just a matter of time. Commenting on the
career of Walter Rauschenbusch, perhaps the most influential
defender of the Social Gospel in his cra (around 1910), Singer writes:
“Rauschenbusch was keenly aware of the necessity of a policy of
deception in introducing his brand of Christian Socialism into the
churches of this country. He thus gave it a name that was designed to
make it seem evangelical in character and not revolutionary at the
same time. Calling for the Christianization of the social order for the
realization of the kingdom of God, Rauschenbusch avoided de-
manding the government ownership of the railroads and other
public utilities. He simply called for governmental controls of
various kinds, confident that such a program would eventually bring
the kind of socialism he wanted. He was willing to uphold a policy of
gradualism in his program of social and democratic revolution.”3

The statist theology of “my brother’s keeper” is consistent,
though its advocates are deeply involved in the deception of Christians

2. Gary North, “The Hidden Costs of Free Lunches,” The Freeman (April, 1978).
3. C. Gregg Singer, The Unholy Alliance (New Rochelle, New York: Arlington
House, 1975), p. 24.
