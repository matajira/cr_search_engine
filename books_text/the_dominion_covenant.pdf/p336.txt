292 THE DOMINION COVENANT: GENESIS

charity. “If left to operate in all its sternness, the principle of the sur-
vival of the fittest, which, as ethically considered, we have seen to
imply that each individual shall be left to experience the effects of his
own nature and consequent conduct, would quickly clear away the
degraded .”132

Through the competition of individuals in a free market, the
greatest possible output will be achieved, and this leads to greater
wealth for those who survive, as well as greater strength for the
species as a whole. Social Darwinism did not argue that there is not
purpose in the universe, or that individuals do not belong to a
species. Through voluntary cooperation in production, the division
of labor increases each participant’s wealth, Yet the higher a species,
the more an individual member must live in terms of his own pro-
duction and skills.73 Man cannot escape this law of nature, Spencer
wrote. “Of man, as of all inferior creatures, the law by conformity to
which the species is preserved, is that among adults the individuals
best adapted to the conditions of their existence shall prosper most,
and that the individuals least adapted to the conditions of their ex-
istence shall prosper least —a law which, if uninterfered with, entails
the survival of the fittest, and the spread of the most adapted
varieties. And as before so here, we see that, ethically considered,
this law implies that each individual ought to receive the benefits and
the evils of his own nature and consequent conduct: neither being
prevented from having whatever good his actions normally bring to
him, nor allowed to shoulder off on to other persons whatever ill is
brought to him by his actions.”3* This is the methodological in-
dividualism of Social Darwinism.

The Social Darwinists had to assume that there is a relationship
between the prosperity of the productive individual and the prosper-
ity of the species. In other words, the prosperity of the effective com-
petitor leads to an increase of strength for the species. One obvious
and troublesome exception seems to be success at offensive warfare,
where the most courageous and dedicated men wind up killing each
other, leaving the cowards and weaklings to return home to
reproduce. Spencer realized this and specifically denied that offen-
sive wars are a productive form of intra-species competition, 35 On

132. Zeid, IL, p. 408.
133. Ibid, IT, p. 278.
134. Ibid, II, p. 33.
135. Ieid., I, pp. 37-39.
