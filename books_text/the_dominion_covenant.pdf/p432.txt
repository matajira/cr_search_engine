388 THE DOMINION COVENANT: GENESIS

made up of fixed species. There was progress possible within one’s
species, but not between the fixed categories. Part of the magical im-
pulse of alchemy was the desire to change lead into gold, not
primarily for the sake of wealth, but for the power involved. The
magical “philosopher’s stone” would enable the magician-scientist to
transcend the limits of creation. Thus, the search for the magical
talisman, thus, the quest for magical salvation: metaphysical manip-
ulation rather than ethical repentance and regeneration was the
magician’s means of grace,'°6 To break the limits of creaturehood!

Enlightenment progressivists now offered a new theory: there
had been progress of species through time. There had been develop-
ment, and to Enlightenment thinkers, it was easy to assume that
biological modification implied ethical improvement. There had
been progress! And there would continue to be progress, not just
politically and economically, but in the very nature of mankind. The
religious impulse was clear cnough: there were no longer any fixed barriers
in the creation, given sufficient time io transcend them, The great chain of
being could now be temporalized. Heaven was.no longer above
men; it was in front of mankind chronologically. Genetics would
serve as a substitute for the alchemical talisman.

Not many thinkers were convinced by the biological evidence in
1750, or even in 1850. But the comparative method which had always
been implied in the concept of the great chain of being was now em-
phasized by a newly developed discipline, natural history. The crucial
figure in this field in the eighteenth century was.the Swedish natural-
ist, Linnaeus. He possessed an unparalleled reputation in 1750; in-
deed, from the publication of the first edition of his Sustema Natura in
1735, he became world-famous, “a phenomenon rather than a man,”
as Eiseley puts it."7 He had a mania for naming things, and he
created the system of dual names which still exists today, generic and
species (which H. L. Mencken used in classifying the boobus Ameri-
canus). He was not an evolutionist in any sense, but in popularizing
comparative anatomy as the means of classification — a method to be
applied to every living organism —he added the crucial third axiom
of the developmental hypothesis!

106. Yates, Giordano Bruno and the Hermetic Tradition, chaps. 2, 8.

107. Darwin's Century, p. 16.

108, Linnaeus did admit, in later years, that nature had a “sportiveness” about
her, that is, surprising variations within species, But not even Eiseley or Greene can
conclude that he ever leaned toward biological developmentalism.
