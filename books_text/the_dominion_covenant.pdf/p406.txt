362 THE DOMINION COVENANT: GENESIS

classical thought by denying the impersonalism of the cosmos. It
provided an alternative to the collapsing classical civilization, for it
offered a wholly new cosmology, As Cochrane says, “The fall of
Rome was the fall of an idea, or rather of a system of Jife based upon
a complex of ideas which may be described broadly as those of
Classicism; and the deficiencies of Classicism, already exposed in
the third century, were destined sooner or later to involve the system
in ruin.”

Eastern Monism

The major philosophical religions of China and India are Bud-
dhism and Hinduism, Both are ultimately monistic faiths. They
hypothesize an ultimate oneness of beng underlying all reality. ‘Vhis
total oneness became plural at some point in the past, thus produc-
ing the creation out of itself; at some later point in history, it will
overcome this dualism to become unified again. The change and
multiplicity of life are therefore maya— illusions. Only unity can be
said truly to exist. Somehow, the ultimate reality of one has included
in itself the illusion of plurality. Swami Nikhilananda, a respected
Hindu scholar whose article appears in a symposium of Darwinian
evolutionists, has tried to explain his system’s cosmology: “According
to the Upanishads, which form the conclusion and the essence of the
Vedas and are also the basis of the Vedanta philosophy, Atman, or
the unchanging spirit in the individual, and Brahman, or the un-
changing spirit in the universe, are identical. This spirit of con-
sciousness ~ eternal, homogeneous, attributeless, and self-existent —
is the ultimate cause of all things. . . . Vedanta Philosophy speaks of
attributeless reality as beyond time, space, and causality. It is not
said to be the cause of the Saguna Brahman [first individual] in the
same way as the potter is the cause of the pot (dualism), or milk of
curds (pantheism). The creation of Saguna Brahman is explained as
an illusory superimposition such as one notices when the desert ap-
pears as a mirage, or a rope in semi-darkness as a snake. This super-
imposition does not change the nature of reality, as the apparent
water of the mirage does not soak a single grain of sand in the desert.
A name and a form are thus superimposed upon Brahman by maya,
a power inherent in Brahman and inseparable from it, as the power
to burn is inseparable from fire. . . . According to Vedanta, maya is

19. ibid., p. 355.
