248 THE DOMINION COVENANT: GENESIS

This so-called diminishing of man was accompanied by the risc of
humanism, and in fact Copernicus’ theory was basic to humanism’s
growth. A diminished view of man somehow led to an elevated view
of man. How was this possible?

One lucid answer has been provided by Arthur O. Lovejoy, the
historian of ideas. He argued that the traditional account of the
significance of Copernicus’ theory has been erroneous. It has
misunderstood the place of the earth in the medieval cosmology. “It
has often been said that the older picture of the world in space was
peculiarly fitted to give man a high sense of his own importance and
dignity; and some modern writers have made much of this supposed
implication of pre-Copernican astronomy. Man occupied, we are
told, the central place in the universe, and round the planet of his
habitation all the vast, unpeopled spheres obsequiously revolved.
But the actual tendency of the geocentric system was, for the
medieval mind, precisely the opposite. For the centre of the world
was not a position of honor, it was rather the place farthest removed
from the Empyrean, the bottom of the creation, to which its dregs
and baser elemenis sank. The actual centre, indeed, was Hell; in the
spatial sense the medieval world was literally diabolocentric. And
the whole sublunary region was, of course, incomparably inferior to
the resplendent and incorruptible heavens above the moon. . . . It
is sufficiently evident from such passages that the geocentric cosmog-
raphy served rather for man’s humiliation than for his exaltation,

 

Virginia Press, 1972), no page pumber, but introductory paragraph. This kind of
language goes back to the early years of the Darwinian controversy. Thomas H.
Huxley, one of Darwin's earliest defenders, and the most influential promoter of.
Darwin’s gospel in England in the nineteenth century, wrote these words: “For, as
the astronomers discover in the earth no centre of the universe, bul an eccentric
speck, so the naturalists find man to be no contre of the living world, but one amidst
endless modifications of life; and as the astronomer observes the mark of practically
endless time set upon the arrangements of the solar system so the student of life finds
the records of ancient forms of existence pepling the world for ages, which, in rela-
tion to human experience, are infinite... Men have acquired the ideas of the
practically infinite extent of the universe and of its practical eternity; they are
familiar with the conception that our earth is but an infinitesimal fragment of that
part of the universe which can be seen; and that, nevertheless, its duration is, as
compared with our standards of time, infinite. . . . Whether these ideas are well or
ill founded is not the question. No one can deny that they exist, and have been the
inevitable outgrowth of the improvement of natural knowledge. And if so, it cannot
be doubted that they are changing the form of men’s most cherished and most im-
portant convictions.” Huxley, “On Improving Knowledge” (1886), in Essays, edited
by Frederick Barry (New York: Macmillan, 1929), pp. 227-29
