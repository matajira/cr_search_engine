Costs, Choices, and Tests 103

would have protected them from death. Even in their fallen state,
the tree of life would have given them eternal life, which is why God
drove them out of the garden (Gen. 3:22-23),

‘To have taken God's word so seriously that they could have fore-
seen the likelihood of their destruction as a result of their rebellion,
they would have had to recognize the extremely high stakes in their
gamble. If man needed the protection of God’s tree of life in order to
protect him from God's wrath, then man was indeed dependent on
God’s grace. If man can trust God’s word regarding the basis of eter-
nal life, then man can trust God’s word concerning the basis of
eternal death. In order for Adam truly to have reduced the risk of
rebellion, namely, by eating from the tree of life, he would have been
forced to acknowledge the sovereignty of God over life, and the ab-
solute reliability of God’s word regarding life. Had he taken God's
word that seriously, Adam would not have rebelled. It was only
because he regarded himself as the arbitrator between God’s word
and Satan’s, and therefore the true source of judgment, that Adam
discounted God’s word, Adam had to assume that God’s word could
not possibly (or very, very improbably) be true in order to make the
risk of rebellion worthwhile. To have gone first to the tree of life
would have meant that man did take God’s threat seriously, and that
man needed the promised protection of God’s tree of life. To have
relied on the tree of life for protection would have meant the end of
man’s pretended claims of autonomy.

Adam had a choice: to choose life or to choose death. By the very
nature of man’s rebellion, he could not have deliberately chosen life
first, since he would have been acknowledging ritually what his
rebellion implicitly was denying: that the source of life is man’s con-
formity to God’s promises. His calculation of costs and benefits had
to be made as covenant-keeping man or covenant-breaking man. As
a covenant-keeping man, he would have reasoned as follows: “God's
word is reliable, so I had better eat from the tree of life first, in order
to protect myself. Protect myself from what? From the reliability of
God’s word concerning eating from the forbidden tree. But if His
word is reliable regarding life, then His word is reliable concerning
death. I had better not consider eating from the forbidden tree.”
Covenant-keeping man protects himself by adhering to God’s word,
by taking God's word seriously: He does not make calculations (in
his state of innocence, anyway) concerning the odds for or against

God’s word. ‘lo choose God’s way to eternal life necessarily involves
\
