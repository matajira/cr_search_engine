330 THR DOMINION COVENANT: GENESIS

He is trying to return social theory to the Scottish evolutionism of
the eighteenth century. He is trying to get the model of impersonal,
physical competition in biology out of market theory. He wants to
return to cighteenth-century social evolutionism. But he has been
unsuccessful in his attempt. The modern version of evolutionistic
social theory goes ahead, not backward; its promoters want to bring
to the forefront Man, the purposeful central planner—a source of
coherence and design in an otherwise impersonal universe. Man,
the decentralized actor is not sufficiently powerful to assure the
species of survival, let alone domination and Godless dominion.
Modern socialists want the dominion covenant, but they do not want
God, except insofar as man as a species is God. Men want design: they
just refuse to believe in a sovereign, supernatural Designer.

Hayek’s defense of the free market social order rests on his con-
cept of human knowledge. He argues for the division of labor in
knowledge.'* Men are not ommiscient. Each individual knows his
own talents and weaknesses, challenges and: successes, better than
anyone else. What is needed is an indegrating system to call forth the
most accurate and relevant knowledge that each man possesses to
deal with the economic problems of a universe of scarce resources.
This system needs a feedback process, so that erroneous information of
inapplicable approaches is not funded endlessly, thereby wasting
resources, Men need to learn from their mistakes. They also need to
imitate successful strategies. Only by decentralizing the decision-making
process, Hayek argues, can mankind call forth its greatest reserves in
order to achieve... . what? Each individual’s highest personal
goals, Hayek is an individualist. He believes that we begin our social
analysis with the individual decision-maker, and the social order
should permit him to pay to achieve his goals. Through allowing
each person to achieve his goals by whatever voluntary and non-
coercive approach he decides is best-fitted to his skills and capital,
we create a social order which allows each of us to prosper. What is
best for a majority of economic actors is best for the society as a
whole, Out of individual competition comes collective prosperity.
This is the essence of Adam Smith’s Wealth of Nations, and it is still
the essence of modern free markct social theory. Hayek defends the
whole idea.

14, Hayek, “The Use of Knowledge in Socicty” (1945); reprinted in Hayek,
Individualism and Economic Order (University of Chicago Press, 1948), ch. 4. This re-
mains one of the seminal essays in economic theory.
