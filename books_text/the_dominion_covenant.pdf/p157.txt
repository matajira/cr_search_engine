Scarcity: Curse and Blessing 3

ical pleasure and incentive attached to labor. Man would now be
compelled to labor by his environment; no longer would his mere hu-
tmanity be relied on by God in order to encourage man to fulfill the
terms of the dominion covenant.

God had created an environment which allowed man the option
of linear economic and personal development. Adam would receive
basic training in the garden, and from there he was to have gone into
the world, with his heirs, to subdue it, spreading paradise across the
face of the earth. Adam’s rebellion broke this linear development,
God's curse in response to Adam’s rebellion brought death into the
cycle: birth, growth, and death. Man was placed under the bondage
of this cycle (“dust to dust”), as was the creation, which longs to find
release (Rom. 8:18-23). In terms of the standards of the garden, this
cycle was unnatural. There had not been death in the garden, at least
not of animals; vegetarianism prevailed. Man and the animals ate
the seeds of herbs and trees for meat (Gen. 1:29-30). Isaiah’s
language indicates that the blessings of restoration also involve an
eventual return to vegetarianism, where the wolf and lamb shall feed
together, and the lion shall eat straw (Isa. 65:25), This is not con-
fined to the post-judgment world; it takes place in time and on earth,
for the serpent is still cursed, still cating dust (Isa. 65:25).! But the
curse of the gound brought the animals under the rule of “tooth and
claw.”

The curse would of necessity slow down the fulfillment of the do-
minion covenant, simply because of the restraints placed on animal
multiplication, The animals would now cat each other, and their

 

It is possible that Isaiah’s language is allegorical, and that he was referring to
political tranquility rather than a world of vegetarianism, It is interesting, however,
that so many religions of the East, and pseudo-religions of the West, have pro-

_ claimed the ethical requirement of vegetarianism. They want a return to
vegetarianism prior to the to1al transformation of culture through regeneration and
the extension of biblical law. Paul warned against these calls to a “premature”
establishment of mandatory vegetarianism: “Now the Spirit speaketh expressly, that
in the latter times some shall depart from the faith, giving heed to seducing spirits,
and doctrines of devils; speaking lies in hypocrisy; having their conscience scared
with a hot iron; forbidding to marry, and commanding to abstain from meats, which
God hath created to be received with thanksgiving of them who belicve and know
the truth” (I Tim. 4;1-3). The cating of Hesh was basic to the ritual celebrations of
Israel (Deut, 12:15, 20). Parts of certain offerings belonged to the pricsts, the right
shoulder going to the priest as a heave offering, and the breast going to Aaron and
his sons (Lev. 7:31-32). ‘Vhe idea that the Bible teaches vegetarianism as a man-
datory way of life is unquestionably heretical. As an ideal for a period of external
kingdom blessings, during a millennial reign, it appears to be valid, though voluntary.
