From Cosmic Purposelessness to Humanistic Sovereignty 267

intuitive or naturalistic, has been a failure.”5? There are no fixed
ethical principles. “They become ethical principles only if man
chooses to make them such.”*? Man, the creative force behind
today’s evolution, becomes at the same time the creator and judge of
his own ethics. “Man cannot evade the responsibility of choice.”5+
Whatever the outcomes of our search for ethical principles, this
much is certain: “The purposes and plans are ours, not those of the
universe, which displays convincing evidence of their absence.”™ Wz
are the new predestinators, the source of the universe’s new
teleology. “Man was certainly not the goal of evolution, which
evidently had no goal. He was not planned, in an operation wholly
planless. . . . His rise was neither insignificant nor inevitable. Man
did originate after a tremendously long sequence of events in which
chance and orientation played a part. Not all the chance favored his
appearance, none might have, but enough did. Not all the orienta-
tion was in his direction, it did not lead unerringly human-ward, but
some of it came this way. The result is the most highly endowed
organization of matter that has yet appeared on the earth—and we
certainly have no good reason to believe there is any higher in the
universe.” Man proposes, and man, working with nature, also
disposes.

Evolutionism’s Sleight-of-Hand

The humanistic philosophy of Darwinism is an enormously suc-
cessful sleight-of-hand operation. It has two primary steps. Firsi,
man must be defined as no more than an animal, the product of the
same meaningless, impersonal, unplanned forces that produced all
the forms of life. This axiom is necessary in order to free man com-
pletely from the concept of final judgment. Man must not be under-
stood as a created being, made in Gad’s image, and therefore fully
responsible before God. Man is no more unique, and therefore no
more responsible, than an amoeba. Second, man, once freed from the
idea of a Creator, is immediately redefined as the unique life form in
the universe. In short, he is and is not special, depending on which
stage of the argument you consider.

52. ibid, p. 3M.

58. Idem.

54. Idem.

55. Ibid., p. 293.
56. Ibid., pp. 293-94
