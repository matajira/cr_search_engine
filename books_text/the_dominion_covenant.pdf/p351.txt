From Cosmic Purposelessness to Humanistic Sovereignty 307

able.”187 (Aidous Huxley, brother of Sir Julian Huxley, and grand-
son of Thomas Huxley, saw this clearly. He wrote Brave New World to
describe the techniques usable by some future State to “render medi-
ocrity, such as it is, more comfortable”; drugs, orgiastic religion, and
total central control.)

Then the goal of total educational equality is really a myth. Why,
then, the emphasis on public education? Conirol! Teachers are to
serve as the new predestinators. “One of the most important objects
of education, thus systematically conducted, should be to determine
the natural characteristics of individual minds. The real work of
human progress should be doubled with the same outlay of energy if
every member of society could be assigned with certainty to the duty
for whose performance he is best adapted. . . . Most men are out of
place because there has been no systematic direction to the inherent
intellectual energies, and the force of circumstances and time-
honored custom have arbitrarily chalked out the field of labor for
each,”!88 Ward’s next paragraph tells us how we can overcome this
Jack of external directions. “The system of education here described
affords a means of regulating this important condition on strictly
natural principles. . . . A school should be conducted on scientific
principles.” Teachers can discover “the true character of any partic-
ular mind,” and then a safe conclusion can be drawn “as to what
mode of life will be most successful, from the point of view of the in-
terest both of the individual and of society.”#°

‘There is another important function of public education, and
indeed of all public information services: the total control of informa-
tion and its distribution. We cannot make progress compulsory, Ward
said. “No law, no physical cocrcion, from whichever code or from
whatever source, can compel the mind to discover principles or in-
yent machines. . . . To influence such action, other means must be

 

 

employed.” Men act in terms of their opinions, “and without
changing those opinions it is wholly impossible perceptibly to change
such conduct.” Here is the planner’s task: “Instill progressive prin-
ciples, no matter how, into the mind, and progressive actions will

187, Ibid., IT, p. 600.
188. Thid., II, pp. 623-24.
189. Zbid., IL, p. 624.
190, Zbid., Il, p. 347.
191. Idem.
