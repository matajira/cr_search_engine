500

exchange, 190
ghetto, 130
Isaac’s, 187, 195
underdevelopment, 126
(see also future-orientation)
Presuppositions, xxix, 412
anti-God, 239, 393
Christian scholarship, xx«xiii
methodologies, 418
professions’, xxiv, 374
subjective value, 63f
Price, 41, 64, 108, 111, 177
clearing, 218, 223
Price index, 58n, 61
Pride, 34f, 440
Primitivismn, 136
Prison, 214
Private property (see property)
Probability, 215, 262, 358, 374
Process philosophy, 273, 287, 3231,
333f, 335, 339, 351, 355, 419f
Pracess theology, 373, 430
Prodigies, 1208
Production mix, 206
Productivity, 206, 217
Profit, 101, 220f, 223, 224
dominion and, 210
Progress, 36, 258, 300, 388
myth of, 5
worship of, 34
Progressives, 296
Promised land, 163
Proofs of God, 450
Property, 445
confiscation, 139
ecology and, 208ff
private, 99, 115f
Prophet, xxiii, 228
Prosperily, 107, 158f, 218
Protestant ethic, 170
Providence, 1, 390, 430£
Augustine’s view, 368
economics and, 9, 328, 355
fixed species idea, 393
regularities, 9, 26, 328
social order, 155
Proxmire, William, 169

  

THE DOMINION COVENANT: GENESIS

Psychological equality, 48
Ptolemy, 247, 369, 370
Public, 210
Pulpit, 142, 143
Puritans, 127, 210, 367, 373, 386
Purpose

astronomy, 12if

God's, 441

human action, 38, 319f, 328, 338

immanent, 23

man-given, 249, 329

man’s, 267, 290, 319f

science vs., 3f, 16, 19f, 256, 391,

413

society and, 138

sovercignty and, 22

Zen Buddhism vs., 366
Purposelessness, 16, 342ff

Quasars, 255n

Race, 215
Rachel, 197
Radio button, 434
Rahab, 164f, 195n
Rainbow, 146, 147
Ramanujan, 12if
Randomness
origin of universe, 19f, 22, 374f
revolt against, 22
science and, 3f, 19f, 257, 261, 408
(see also chance)
Ranganathan, §. R., 121
Rauschenbusch, Walter, 142
Ray, John, 389
Rebekah, 179, 188, 193
Rebellion, 439, 446
active-passive, 33
Adam's, 69ff, 73
bondage of time, 122
cost of, 112
division of labor, 115
escaping time, 130
ethical, 98
imputation, 38, 60
Tsaac’s, 190, 195
judgment, 119
