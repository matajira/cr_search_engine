Basic Implications of the Six-Day Creation 435

their willful rebellion against that revelation. God’s creation reveals
Him.

The Sovereignty of God

Job 38-41 is an important testimony to the sovereignty of God.
God, who created all things and sustains all things, rules all things.
Nothing happens outside the decrees of God; Satan had to ask per-
mission in order to harass Job, and God set limits to everything he
did (Job 1:12; 2:6). Everything is known to God beforehand, of
course: “Known unto God are all his works from the beginning of the
world” (Acts 15:18). But in Isaiah 45 we learn of the extent of God’s
total direction of all events:

I form the light, and create darkness: I make peace, and create evil: I the
Loxp do all thesc things. Drop down, yc heavens, from above, and let the
skies pour down righteousness: let the earth open, and let them bring forth
salvation, and let rightcousness spring up together; I the Lorw have created
it. Woe unto him that striveth with his Maker! Let the potsherd strive with
the potsherds of the carth. Shall the clay say to him that fashioncth it, What
makest thou? or thy work, He hath no hands? . . . I have made the earth,
and created man upon it: I, even my hands, have stretched out the
heavens, and all their host have I commanded (Isa. 45:7-9, 12).

God is not the author of confusion (I Cor. 14:33), yet He controls
and directs all things, There is no solution to this seeming intellec-
tual dilemma in terms of the logic of autonomous man.

The image of the poiter and his workmanship is a recurring one
in the Bible. “But now, O Lorp, thou art our Father; we are the clay,
and thou our potter; and we all are the work of thy hand” (Isa. 64:8).
Jeremiah 18, God’s confrontation with Israel, is constructed upon
this analogy: “O house of Israel, cannot I do with you as this potter?
saith the Lorp. Behold, as the clay is in the potter’s hand, so are ye
in mine hand, O house of Israel” ( Jer. 18:6). But in Romans 9, the
great chapter in the New Testament dealing with the total predesti-
nation of the world by God, Paul uscs the potter analogy to stifle the
apostate and illegitimate conclusion of those who would argue that
God’s predestination is opposed to human responsibility. Paul’s use
of the potter analogy has no meaning except in terms of such an ille-
gitimate use of human logic; he answers that issue, and only that
issue, in these words:
