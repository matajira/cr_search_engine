168 THE DOMINION COVENANT: GENESIS

munity.”'3 What does this mean? That a world government should
control people’s decisions to have children? That some international
committee should establish guidelines? Is such a goal feasible in the
real world? Should it even be considered? What are the implications
for the growth of the messianic State?

What we find, then, is that the optimistic future-orientation of
Western industrial populations, 1850-1960, had important effects on
the growth of population. Now, however, that confidence is fading,
along with birth rates. The present-orientation of young couples who
delay having children for the sake of higher present income is creating
a demographic disaster for the State-created retirement programs,
since not enough young workers will be able to fund them by the year
2000, and certainly by 2020, unless birth rates increase, or unless
the older generation is systematically exterminated by the young ina
program of euthanasia.!4 The welfare state faces bankruptcy.

The American Social Security system was doomed, statistically,
from the very beginning, Those who entered the system at the begin-
ning, in 1937, paid in $30 per year (maximum bracket), and their
employers paid in $30. By 1989, unless the 1977 revision of the tax
schedule is altered, each worker will pay a maximum of $3,560, and
his employer will match this payment.‘S Of course, many families
have two members in the work force, so the family payment may be
more. The very first lady to receive a Social Security check, Ida
Fuller, retired in 1940, after having paid in $22.54. She died in 1975
at age 100, Her total benefits exceeded $20,000.16 She was a winner.
‘The taxpayers paid her winnings, Ironically, it was 1975 that marked
the first year of a deficit in the Social Security program.’”

Government officials assure voters that all benefits will be paid.

13, Congressional Research Service, Library of Congress, World Population Con-
trol, Issue Brief #1B74098 (June 25, 1976).

14, On the looming crisis in the American Social Security compulsory retirement
benefit system, see Warren Shore, Social Security: The Fraud in Your Future (New York:
Macmillan, 1975); Rita Ricarda Gampbell, Social Security: Promise and Reality
(Stanford, Calif.: Hoover Institution, 1977). Scc also the series on Social Security
that appeared in the Wall Street Journal (June 4, June 6, June 8, 1979). In 1979, there
were 3.1 persons receiving Social Scoutity benefits for cvery 10 workers who were
paying Social Security taxes. By the year 2030, assuming a birth rate of 2.1 children
per woman, the figure will be 5 recipients for every 10 workers: U.S. News & World
Report (April 30, 1979), p. 27. But the birth rate is below 2.1.

15. U.S. News & World Report (April 30, 1979), p. 24.

16. Los Angeles Times (Jan. 28, 1975).

17. Los Angeles Times (May 6, 1975).

 

 
