116 TIE DOMINION COVENANT: GENESIS

ecological disturbances.? Men rush in to get “their” share of the “free”
goods, with little thought of the future, simply because they have lit-
tle or no control over the future use of public property. They can
control the future use of private property, and the costs come out of
their capital resources, which provides a great incentive to use the
resources in a cost-effective manner—one which regards the future
expected benefits of the resources.t There is a strong tendency,
though not an invariable law, for men to take better care of the crea-
tion when they are allowed to take possession of the fruits of their
labor on their parcel of the creation.® Again, it is scarcity which pres-
sures innately lawless men to respect the laws of God concerning the
creation,

The curse of the ground is a form of grace to the godly, for it
allows them to work out their faith with less fear and trembling con-
cerning the actions of the ungodly. It is also a form of grace—
unearned gift — to the ungodly, for it allows them to work out the im-
plications of their antichristian faith in ways that are less harmful to
other ungodly men, godly men, and the creation: grace leading to
destruction (Luke 12:47-48). The ungodly are given life. They arc
given power. They participate in history—a kind of stay of execu-
tion, Their labors increase the wealth of the believers, since all share
in the blessings of greater productivity. Common curse (sweat,
death, and thistles), common grace (time, incentives to cooperate),
special curse (final judgment), and special grace (salvation): all are
involved in God's retaliation against evil.

 

Conclusion

The goal of a godly man is to overcome the curse of the ground
and the curse of his own flesh. He is to accomplish this through
applied faith. Biblical law is to serve as the tool of overcoming the
curses. The cycles of nature are to be overcome through godly agri-

3. Garret. Hardin, “The Tragedy of the Commons,” Sciewe (13 Dee. 1968);
reprinted in Garrett de Bell (ed.), The Environmental Handbook (New York: Ballen-
tine, 1970).

4. CG. R. Batten, “Lhe Tragedy of the Commons,” The Freeman (Oct., 1970).

5. Edwin G. Dolan, TANSTAAFL: The Economic Strategy for Environmental Crises
(New York: Holt, Rinehart and Winston, 1971); J. H. Dales, Pollution, Property and
Prices (University of Toronto Press, 1970). T. D, Crocker and A. J. Rogers, Environ-
mental Economics (New York: Holt, Rinehart and Winston, 1971). On the disastrous,
State-enforced and State-subsidized pollution in the Sovict Union, see Marsball
Goldman, The Spoils of Progress (Cambridge, Mass: MIT Press, 1972).

 
