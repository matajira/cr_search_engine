Cosmic Personalism $

achievement must inevitably be buried beneath the debris of a
universe in ruins—all these things, if not quite beyond dispute, are
yet so nearly certain, that no philosophy which rejects them can hope
to stand, Only within the scaffolding of these truths, only on the firm
foundation of unyielding despair, can the soul's habitation hence-
forth be safely built.”

If anything, as Russell grew older, he became even more
pessimistic, more thoroughly consistent with his presuppositions
concerning man and the universe. He saw through the glib
theologians who had adopted some version of evolution and bad
then attempted to integrate it into their religious framework. What
foolishness, ‘Russell concluded. “Why the Creator should have
preferred to reach His goal by a process, instead of going straight to
it, these modern theologians. do not tell us.” But this is only part of
their problem, “There is another and a graver objection to any
theology based on evolution, In the ’sixties and ’seventies, when the
vogue of the doctrine was new, progress was accepted as a law of the
world, Were we not growing richer year by year, and enjoying
budget surpluses in spite of diminished taxation? Was not our
machinery the wonder of the world, and our parliamentary govern-
ment a model for the imitation of enlightened foreigners? And could
anyone doubt that progress would go on indefinitely? Science and
mechanical ingenuity, which had produced it, could surely be
trusted Lo go on producing it ever more abundantly. In such a world,
evolution seemed only a generalization of everyday life. But even
then, to the more reflective, another side was apparent. The same
laws which produce growth also produce decay. Some day, the sun
will grow cold, and life on the earth will cease. ‘The whole epoch of
animals and plants is only an interlude between ages that were too
hot and ages that will be too cold, There is no law of cosmic prog-
ress, but only an oscillation upward and downward, with a slow
trend downward on the balance owing to the diffusion of cnergy.
This, at least, is what science at present regards as most probable,
and in our disillusioned generation it is easy to believe. From evolu-
tion, so far as our present knowledge shows, no ultimately optimistic
philosophy can be validly inferred.”* Humanism is pessimistic,

4. Bertrand Rusvell, “A Free Mar’s Religion” (1903), in Russell, Mysticism and
Logic (New York: Doubleday Anchor, [1917]), pp. 45-46.

5. Bertrand Russell, “Evolution,” in Religion and Science (New York: Oxford
University Press, [1935] 1972}, pp. 80-81.
