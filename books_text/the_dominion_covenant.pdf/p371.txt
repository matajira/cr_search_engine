The Evolutionists’ Defense of the Market 327

premises. The second phase of Hayek's career was more deeply
social and philosophical, and it began in the 1940's. Hc is far more
famous for the books and essays that he produced during this later
period, especially The Road ta Serfdom (1944), Hayek has offered us
the finest statement of late-nineteenth-century classical social theory
in his later books. They are erudite, heavily footnoted, eloquent
defenses of post-Darwin, post-Kantian social philosophy. They all
rest on an explicit foundation of evolutionism.

One of the recurring themes in Hayek’s writings is this one: there
have been two forms of rationalism in the West. The first form is best
represented by the writings of the Scottish social theorists of the eigh-
teenth century, most notably Adam Ferguson, who wrote: “Nations
stumble upon establishments, which are indeed the result of human
action, but not the execution of any human design.”® Hayek uses this
phrase repeatedly, most notably in an essay “The Results of Human
Action but not of Human Design” (1967). The second form of ration-
alism is the rationalism of the central planner. Human action is seen
as being rational only when it is the result of human design, namely,
the design of a sovereign, rational, scientific planning agency. The
origin of this second position, as far as the history of the modern
West is concerned, is the French Revolution, Hayek’s book, The
Counter-Revotution of Science: Studies on the Abuse of Reason (1952), is an
historical study of the origin and development of “designing rational-
ism” in social theory.® He calls this “constructivist rationalism.” Men
rationally construct social institutions.

If we can use Darwinian categorics, we can better understand
the two rationalisms. The frst form, which Hayek favors, is that pro-
pounded by Adam Ferguson, Adam Smith, Edmund Burke, and other
eighteenth-century social theorists. Their view is that human institu-
tions are the product of long years of unregulated development.
Legal, economic, and other institutional arrangements were not
consciously designed by any human planning agency. Nevertheless,
they are coherent, rational, and productive. It was this argument
which impressed the early evolutionists, who took the paradigm and

8. Adam Ferguson, An Essay on the History of Civit Society (1797), p. 187; cited by
Hayek, “The Results of Human Action but not of Human Design” (1967), in his
book, Studies in Philosophy, Politics and Economics (University of Chicago Press, 1967),
p. 96n.

9. Hayek, The Counter- Revolution of Science: Studies on the Abuse of Reason (Indianapolis,
Indiana: Liberty Press, [1952] 1979).
