354 THE DOMINION GOVENANT: GENESIS

Conclusion

The dualisms of post-Kantian thought are inescapable. To evalu-
ate change, you need a fixed standard, To evaluate the success of
human action, you need a model which denies human action,
whether you call it equilibrium or “the evenly rotating economy,” as
Mises does, To measure the progress of mankind, you need a
changeless standard which thwarts the progress of mankind. (How
can we have progress in a static order?) To assert that personal irra-
tionality is compatible with market rationality, you need an equilib-
rium model which rests on personal infallibility. To argue that we
need a decentralized market order to preserve and expand human
knowledge, we wind up affirming that no one can understand the
market order, The laws of the market order arc the result of eons of
development—the product of human action but not of human
design—yet we are asked to believe that this order is as useful to
mankind as a whole (when we can legitimately say nothing as meth-
odological individualists about mankind as a whole) as if it had been
designed specifically for mankind as a whole. We must affirm cause
and effect in a world that is the product of chance. We must affirm
that man the planning individual cannot effectively make decisions
for other men, because no man has sufficient knowledge to integrate
the knowledge of other acting men, yet we are also supposed to
affirm that the market is a reliable institution for the progress of the
species, when we do not know how the market ever developed, and
we cannot speak of an aggregate like “man, the species.” We want the
one (the market order) to conform to the needs of the many (acting in-
dividuals), yet we cannot, as methodological individualists, make
any scientifically legitimate statements about the needs of the many.
(One man “needs” to stick pins in others, while others insist that they
“need” to avoid being stuck.) We make our case for the market in
terms of imperfect knowledge (Alchian and Hayek), yet we are then
forced to make judgments about evolution as a process, the best in-
terests of mankind as a species, the reliability of the market in a
world of evolutionary change, and so forth. From ignorance (we
need the market to integrate and expand knowledge) to near-
omniscience (we know that the market will provide us with this
needed knowledge). From irrationality (Becker's irrationality thesis)
to rationality (Becker's equilibrium analysis). The logic of humanis-
lic economics is hopeless.
