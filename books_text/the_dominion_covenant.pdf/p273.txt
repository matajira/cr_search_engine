The Entrepreneurial Function 229

worthy than any other human institution. All institutions are to be
under the jurisdiction of God. There is no monopoly of sovereignty
on earth, except God’s written word, the Bible.

By decentralizing the office of prophet, God insured that His
people would not be compelled to listen exclusively to false prophets
catering to the civil government rather than serving God. When the
people were stiff-necked, refusing to heed His word, He punished
them by allowing them to believe the false reparts provided by the
court’s false prophets (Ezek. 14:1-5). The curse on their ethical
rebellion was quite specific: the imposition of centralized decision-
making by corrupt kings and their officially sanctioned prophets.

We never face a choice of “planning” vs. “no planning.” The only
question is: “Whose plan?” When economic planning is decentralized,
and decisions are made by owners of private property, society is shielded
from the risks of massive, centralized error. An erroneous decision.
made by a particular privately owned firm may cost its shareholders
dearly, and consumers who would have purchased the goods that
would have been made, had the firm not embarked on its error-filled
course, no doubt are harmed. There has been waste. Nevertheless,
the majority of consumers are protected because competing firms and
suppliers can step in and satisfy consumer demand, The rival suppli-
ers help to smooth out the disruptions caused either by the unforescen
external circumstances or the operations of the misallocating firm.

When a monopolistic central planning agency makes an error in
forecasting the economic future, large segments of the population
suffer. There are few legal alternatives open to potential buyers.
Black market operators may step in, for a high price, and smooth out
the disruptions in supply, but buyers will bear higher risks in dealing
with these suppliers, and they will pay higher prices than would have
been necessary had private firms been allowed to compete with State
planners. Bureaucrats, wielded as they are by tenure, trade union
restrictions, or Civil Service regulations, do not have the same
incentives to bear uncertainty successfully, when compared to the
incentives offered to the private entrepreneur. Bureaucrats are not
rewarded directly with profits, nor are they immediately fired. The
carrot and the stick are only indirectly related to any given decision
made by a central planning agency. Blame for error is easily trans-
ferred in anything as complex as a national economy. The con-
sumers cannot weed out the inefficient planners in a direct, forceful
manner when planners are paid functionaries of the political State.
