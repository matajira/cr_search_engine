Subordination and Fulfillment 89

too, serves as a training ground. It is a good place, but it is not our
final resting place, any more than the garden was intended to be our
final resting place. It is an “intermediate” state. We are still to exer-
cise dominion over the earth. Heaven is a place which, like the gar-
den was before the Fall, is essentially unfulfilled. Men in heaven are
separated from their eternal bodies (I Gor, 15:35-55). They cry out
constantly: “How long, O Lord, holy and true, dost thou not judge
and avenge our blood on them that dwell on the earth?” (Rev, 6:10).
The focus of their concern, even in heaven, is the earth. It is to be
our concern as well. We are required to extend the paradise of heaven to the
earth, Heaven has replaced the paradise of the garden. Each was de-
signed to be temporary. Our goal is heaven on earth, to be com-
pleted after the final judgment; we are to dwell in the New Heavens
and New Earth (Rev. 21:1). Revelation 22:2 describes a developed
paradise: the fulfilled city. Ours should be the same concern which
was supposed to have been Adam’s concern in the garden, the initial
paradise. The dominion covenant is eternal,

Because of Adam’s transgression, we are receiving our training
in time and on a cursed earth. We are supposed to be improving our
skills of dominion. We are working out the terms of the dominion
covenant, but we labor under a curse. Our work has meaning, both
now and in eternity. We will receive our rewards in heaven (I Cor.
3), but these are not our sole and final rewards. Heaven’s rewards
are like military medals or the prizes of the athletic field (I Cor, 9:24;
Phil. 3:14; If Tim. 4:7-8), They are things warth competing for,
again and again, if long life permits. Heaven’s rewards are a legiti-
mate goal of human action. But these rewards are the starting point,
like Adam's successful classifying of the animals. Heaven's rewards
are given in response to a preliminary task well done. They are our
graduation diplomas, which we will receive on judgment day (Rev.
20). Then we will go forth into the world to work, Men and the created
realm will at last find completion, ethically speaking, under the sov-
ereignty of God. The curses on man, man’s labors, and nature will
be permanently removed (Rev. 22:3). With ethical perfection as the
foundation, the creation will be subdued and cared for, throughout
elernity. The battle with nature will at last be over. The dabor over
nature will never end.
