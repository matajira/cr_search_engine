The Evotutionists’ Defense of the Market 351

recognized that the market process (whether within a given industry
or for an entire economy) is a means of communicating knowledge. The
knowledge that a market cormmunicates is made up of preciscly those
elements of information necessary to bring about the systematic
revisions of plans in the direction of equilibrium (whether partial or
general), Each market decision is made in the light of market infor-
mation. Where the decisions of a// market participants dovetail com-
pletely, all of them can be implemented without disappointments
and without subsequent alterations of plans; the market is in equilib-
rium,”48 So Kirzner, like all other market-oriented economists, must
judge the real world of human action in terms of a hypothetical, in-
tellectually inconsistent world of equilibrium—~a world in which
forecasting is perfect, everyone’s actions are fully known in advance,
and men have no freedom of choice. In such a world, humans re-
spond as automatons to stimuli, Cause and effect rule supreme: the
triumph of Kant’s phenomenal realm of science over Kant’s nou-
menal realm of free human personality. In short, Kirzner must rely
on a limiting concept, equilibrium, in order to judge the success or fail-
ure of market institutions in dovetailing the varying plans of acting
men. Yet this limiting concept is in total opposition to the methodo-
logical individualism (autonomous man) that Kirzner and the
Austrian School economists constantly preach. To explain human
action, economists use a model which denies human action.

Here is one of the important assumptions of Alchian: “Com-
parability of resulting situations is destroyed by the changing en-
viranment.”*? The changing environment in an evolving universe may have
changed the rules of survival, This is also true for Hayek’s evolving
universe. This is the plight of all process philosophy. Hayek relies on the
market to guide men in their quest to dovetail their plans, but how
can he be sure that the laws of the market process are still supreme?
After all, we live in a world of constant change. Where is his measur-
ing rod which tells us whether or not we are progressing according to
our individual plans? How can he or Kirzner use equilibrium as the
standard, when equilibrium analysis is absolutely contrary to the
concept of free, autonomous human action? Alchian, as a consistent
evolutionist, says that survival is the only criterion. This leads us
back to the old debate: Who or what is to insure the survival of mankind?

48. Idem
49. Alchian, of. cit., p. 31.
