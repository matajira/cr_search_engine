314 THE DOMINION COVENANT! GENESIS

of him in any of the discussions.”2"7 Ward called citizen C “the rich,”
and let it go at that. His intellectual heirs have not improved much
on this strategy, especially when they run for public office.

Let us understand precisely what Ward was trying to create: a
totalitarian State. As he wrote, “the present empirical, anti-progressive
institution, miscalled the art of government, must be transformed
into a central academy of social science, which shall stand in the
same relation to the control of men in which a polytechnic institute
stands to the control of nature.”28 He was a defender of despotism.

Is it surprising, then, that he should have been elected the first
president of the American Sociological Association?

There is one final feature of his system which bears mentioning.
The basis of Darwin's analysis of evolution through natural selection
was Malthus’ observation that species reproduce too fast for their en-
vironments. Then only a few will survive, concluded Darwin and
Wallace. Ward accepted this as it pertained to nature. But man is a
new evolutionary life form, and man’s ways are not nature’s ways.
Man’s successful heirs are not supposed to be those individuals who
by special genetic advantages or inherited wealth will be able to
multiply their numbers. Man, unlike the animals, advances by
means of State planning. If society is to prevent suffering, as Ward
said is necessary, then the multiplication of those who receive charity
must be prohibited. (This was the same problem that baffled
Spencer.) “This fact points to the importance of all means which tend
to prevent this result.”° Three children are probably the maximum
allowable number. “In an ignorant community this could not be en-
forced, but in a sufficiently enlightened one it could and would
be.”220 In short, “What society needs is restriction of population,
especially among the classes and at the points where it now increases
most rapidly.”2#! But who are these classcs? The masses, of course,
since the present moral code (1883) of having large families ‘is tacitly
violated by intelligent people, but enforced by the ignorant and the
poor, a state of things which powerfully counteracts all efforts to
enlighten the masses.”2?2 The State needs to provide universal

217. Sumner, Social Classes, p. 22.
218. Ward, IL, pp. 251-52.

219. Tid., TL, p. 307.

220. Ibid., II, p. 465.

221, Ibid., UL, p. 466.

222, Ibid., I, p, 465.
