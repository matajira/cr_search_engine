228 THE DOMINION COVENANT: GENESIS

then central economic planning initially has no greater claim to
biblical sanction than private economic forecasting does.

Is the assumption correct? Is there some feature about becoming
a State official which in some way endows a person with a prophetic
mantle? What biblical evidence do we have for such an assumption?
Is any forcign prisoner who has served two years of an indeterminate
jail sentence a predictably effective interpreter of visions given to na-
tional leaders? Would anyone wish to build a theory of political econ-
omy on such a premise? Could we create a governmental planning
structure in terms of such an operating presupposition about the
nature of civil government?

The consequence of Egyptian economic planning by Joseph must
also be borne in mind. 'The entire nation, excepting only the priests,
went into'bondage to the Egyptian State (Gen. 47:13-22). All land, ex-
cept that owned by the priests, became the possession of the Pharaoh.
The people survived the famine, which they might not have been
able to do had it not been for Joseph’s entrepreneurship, but they
and their heirs became servants of the Pharaoh and his heirs.

It was basic to the religions of antiquity that the State was in
some fundamental way divine, or linked to the divine through the
ruler, Egypt’s theology was especially notable for its adherence to the
theology of a divine ruler. The Pharaoh was supposedly the descen-
dent of the sun god. Only the Hebrews, with their doctrine of the
Creator-creature distinction, avoided the lure of a theology of imma-
nent divinity. The outcome of such a theology, when coupled with a
mechanism of State economic planning, was enslavement. This was
the curse of what Wittfogel has called oriental despotism.®

The Hebrews, in stark contrast, were told to worship God, and
only God, as divine. The State, clearly, is not divine, and any
attempt to make it divine — the sole representative of God on earth —
was understood to be demonic. Officials were constanily told to re-
main honest stewards of the great King. The office of prophet was
decentralized, and prophets continually challenged kings, princes,
priests, and average Hebrew citizens when they turned away from
God and His law. God sent a shepherd like Amos to speak to the
people; they were expected to’ heed this shepherd's word, not the
king and his court priests. The civil government is no more trust-

 

8. Karl A. Wittfogel, Oriental Despatism: A Comparative Study of Total Power (New
Haven, Gonn.: Yale University Press, 1957).
