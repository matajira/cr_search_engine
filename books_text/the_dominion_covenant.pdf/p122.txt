6
THE VALUE OF GOLD

And the gold of that land is good: there is bdellium and the onyx stone
(Gen. 2:12).

In describing the land of Havilah, Moses singles out its supplies
of precious metal and stones. (Some think bdellium was a plant or
plant byproduct.) This is the sole reference to inanimate objects
prior to the rebellion of man which specifies their unique quality.
Moses understood that the people of his day would comprehend the
value of a land which possessed jewels and gold. Man’s place of orig-
inal responsibility was a splendid land, and the presence of fine gold
was one of its marks of splendor, God's generosity to man was im-
mediately apparent to anyone reading or hearing Moses’ account of
Adam’s environment.

Precious metals and jewelry have ben regarded as basic wealth
objects for as long as man has left records. Gold has been a form of
money for as far back as we can investigate. Its brilliance, durability,
mailleability, and universal respect as a metal of continuing value
have made it a unique economic resource. Its scarcity in relation to
the high value men place on the ownership of the metal (high
marginal utility) has made gold a universal currency, Gold is some-
thing werth owning. Even Adam in the garden could be regarded in
retrospect as blessed, Moses made it clear—all the more reason to
condemn Adam’s ethical rebellion. In a perfect creation, which God
had announced as being good, gold and jewels were something
special.

Gold is the universal money. Wherever men truck and barter,
they respect gold as a means of exchange. Why? What is money, and
why should gold serve as its universal archetype? Money is simply
the most marketable commodity.! To onc extent or another, money must

4. Ludwig von Mises, The Theory of Money and Credit (New Haven, Gonn.: Yale
78
