Cosmologies in Conflict: Creation us. Evolution 415

sonal nature. Once this transfer of sovereignty has taken place, Dar-
winian man reclaims this sovereignty, as the legitimate heir of
nature. Man then becomes the official king of nature, and like
Napoleon Bonaparte, he has been careful to place the crown on his
own head (not relying on the Pope or any other theological agent).
Eiscley is quite correct when he says that Darwin’s work destroyed
the labors of the parson-naturalists. This did not keep the parsons
from flocking to him in droves, bearing symbolic frankincense and
myrrh, in his later years. This typical yet pathetic development only
served to intensify his hostility to religion. His cousin remarked that
he was far more sympathetic to religious critics than the fawning ec-
clesiastics who lauded his work.'8’ Preposterously, “The religious
managed to find in Darwinism a variety of consolations and virtues
not dreamed of even in natural theology. One distinguished botanist
bewildered Darwin by declaring himself a convert on the grounds
that the theory finally made intelligible the birth of Christ and
redemption by grace. A clergyman was converted on the grounds
that it opened up new and more glorious prospects for immortality.
And theologians declared themselves ready to give up the old doc-
trine of ‘the fall’ in favor of the happier idea of a gradual and unceas-
ing progress to a higher physical and spiritual state.”1"°
Himmelfarb has hit the nail on the head when she writes that the
Darwinian controversy was not between theists and evolutionists, but between
the reconcilers and irreconcilables on both sides of the controversy.° In our
century, the irreconcilable Christians (and, I gather, conservative
Jews) have diminished in number. The new evolutionists do not care
enough one way or the other whether Christians do or do not rewrite
their religion to conform to the Darwinian universe. The historian,
Greene, has bent over backward to say nice things about the various
theological compromises of men like Russell Mixter and James O.
Buswell III, but he is only stating an inescapable fact (from the
consistent Darwinian point of view) when he concludes: “These
theories may help to conserve belief in the inspiration of the Bible,
but it is difficult to see how they can be of much scientific value. . . .
[When Greene refers to the inspiration of the Bible, he has in mind the
heretical Barthian variety, as he says two pages later.] As science ad-
vances, morcover, the maintenance of what these writers call ‘verbal

187. Himmelfarb, p. 386.
188, Tbid., p. 394.
189, Ibid., p. 397.
