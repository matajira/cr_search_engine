Purpose, Order, and Sovereignty 23

the concept of iranscendent cosmic purpose. ‘The result was the crea-
tion of an intellectual monstrosity which almost no one has been
willing to accept. Mcn usually desire purpose, which means that
they desire a purposeful, personal sovereign. A new sovereign was
brought forth: planning mankind, which has meant, in the twentieth
century, a planning elite. Humanism has created a philosophy of
sovereign purpose, and it has thereby helped to bring us the neces-
sary concomitant of such a philosophy: the bureaucratic cage.

A few traditional humanists, whose intellectual roots are still in
the nineteenth century, have attempted to revive the fading faith in
the acceptability and even beneficial nature of decentralized pur-
posefulness. They have continued to quote favorably Adam Fergus-
on’s eighteenth-century observation that human institutions are the
product of human action — decentralized, individualistic planning —
but not of human design. The economic theories of virtually all defend-
ers of free market economics, but especially the theoretical framework
of the so-called Austrian School —Ludwig von Mises, F. A. Hayek,
Israel Kirzner, Murray Rothbard—have been constructed in terms of
this eighteenth-century cosmology. Despite the cogent economic argu-
ments of these men, the modern world has systematically refused to
take these arguments seriously. Men want to believe in a concept of
immanent cosmic purpose, and this means a concept of a coherent,
competent, order-producing planner. Men refuse to believe that suc-
cessful social and economic coordination which is beneficial for all or
most of the members of society can be the product of uncoordinated
human actions that are somehow coordinated through a system of
private property and freely fluctuating prices. They cling religiously
to the concept of personal design, Most men want to live in a universe
with meaning and purpose, but this requires the concept of predestination. As
Rushdoony writes: “The only alternative to the doctrine of predesti-
nation is the assertion of the reign of total chancc, of meaningless
and brute factuality. The real issuc is what kind of predestination we
shall have, predestination by God or predestination by man?”?! In
other words, it is never a question of predestination or no predesti-
nation. It is always a question of whose predestination.

Modern men have rejected the concept of predestination by God.
They have been forced to locate some other predestinator: random

2. R. J. Rushdoony, The Biblical Philosophy of History (Phillipsburg, New Jersey:
Presbyterian and Reformed, [1969] 1979), p. 6.
