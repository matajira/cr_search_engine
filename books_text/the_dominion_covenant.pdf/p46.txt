2 THE DOMINION GOVENANT: GENESIS

time. Ours is not a mechanistic world, nor is it an autonomous
biological entity, growing according to some genetic code of the
cosmos. Ours is a world which is actively sustained by God on a full-
time basis (Job 38-41). All creation is inescapably personal and
theocentric. “For the invisible things of him from the creation of the
world are clearly seen, being understood by the things that are
made, even his eternal power and Godhead . . .” (Rom. 1:20).

Hf the universe is inescapably personal, then there can be no
phenomenon or event in the creation which is independent from
God. No phenomenon can be said to exist apart from God’s all-
inclusive plan for the ages. There is no uninterpreted “brute factu-
ality.” Nothing in the universe is autonomous, an English word derived
from two Greek words that are transliterated autos (self) and nomos
(law). Nothing in the creation generates its own conditions of ex-
istence, including the law structure under which something operates

 

or is operated upon. Every fact in the universe, from beginning to
end, is exhaustively interpreted by God in terms of His being, plan,
and power.!

Modern Science’s Impersonalism

The doctrine of creation in its biblical form therefore denies one
of the most cherished doctrines of the modern world, namely, the
doctrine of cosmic impersonalism, This doctrine asserts that all life is
the product of impersonal, self-generated, random forces of nature.
Cosmic impersonalism is the heart and soul of the modern doctrine
of evolution, which asserts that evolution operates through the
process of natural selection. Undergirding the concept of natural
selection is the idea of randomness. The idea of evolution is not new;
in fact, it was the universal belief of ancient societies, with the excep-
tion of the Hebrews, Ancient paganism held a concept of a deity or
deities that struggled with the primeval chaos (randomness) in order
to produce a somewhat orderly, partially controlled universe — one
which is constantly threatened by either too much law or a breakdown

1, Gornelius Van Til, A Christian Theory of Knowledge (Nutley, New Jersey:
Presbyterian & Reformed Publishing Co., 1969), p. 28. Writes Van Til: “All facts of
history are what they are ultimately because of what God intends and makes them to
be. Even that which is accomplished in human history through the instrumentality
of men still happens by virtue of che plan of God. God tells the stars by their names,
He identifies by complete description. He knows exhaustively. He knows
exhaustively because he controls completely.”
