4 THE DOMINION COVENANT: GENESIS

design in nature,”* The “god of the physicists,” however, had also
been a god of randomness, lurking in the shadows of the “as yet-
unknown,” moving instantly away from any “light” thrown on events
by rationalism’s laws of physical science. The god involved was
Kant’s god of the hypothetical “noumenal’ realm—a mental con-
struct who cannot influence the external events of nature. So
Medawar and his wife ‘have abandoned such a god as being un-
necessary, which indced such a god is. They have forthrighdy
accepted the new god of creation, randomness, giving him all due
respect, honor, and glory. Purpose and design, the intolerable evils
of Christian theology, must be banished from the kingdom of ran-
domness, at least until man appears on the scene.

Cosmic impersonalism is a way of banishing personal responsi-
bility from the universe. It enables men to ignore the possibility of
final judgment in terms of a fixed set of ethical standards. It allows
men to ignore the possibility of ctcrnal punishment. It allows man to
reinterpret all facts according to his purposes and ideals, both in-
tellectual and moral. Man becomes the determiner and interpreter
of the universe. Understandably, secular man prefers not to inter-
pret the universe in terms of God’s categories. He much prefers to
live in the hypothetically random universe posited by modern
humanism —a universe which is slowly grinding to inevitable extinc-
tion (the second law of thermodynamics, entropy). Perhaps the most
eloquent statement of what this means has been written by Bertrand
Russell, the influential British philosopher-mathematician. The
world of modern science, he writes, is “more purposeless, more void
of meaning” than the world outlined by Mephistopheles to Dr.
Faustus. ‘he modern world has no meaning. “Amid such a world, if
anywhere, our ideals henceforward must find a home. That Man is
the product of causes which had no prevision of the end they were
achieving; that his origin, his growth, his hopes and fears, his loves
and beliefs, are but the outcome of accidental collocations of atoms;
that no fire, no heroism, no intensity of thought and feeling, can
preserve an individual life beyond the grave; that all the labours of
the ages, all the devotion, all the inspiration, all the noonday
brightness of human genius, are destined to extinction in the vast
death of the solar system, and that the whole temple of Man’s

3. Peter and Jean Medawar, “Revising the Facts of Life,” Harper's (Feb. 1977), p.
4l.
