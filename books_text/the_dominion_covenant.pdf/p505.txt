Witnesses and Judges 461

Had Adam served as a righteous witness, he would have asked
Satan to repeat the interpretation of God’s word which Satan had
given to Eve (unless he had been silently present with her during the
temptation). Then Adam would have awaited God’s return, so that
He might testify to Eve concerning His words to Adam. Then Adam
and Eve would have testified against Satan.

They could testify against God or against Satan, but they could
not escape testifying. God subpoenas all witnesses. There is also no
constitutional “fifth amendment” in God’s court ~ ne right to remain
silent even if the testimony might convict the witness. From the mo-
ment of temptation, man became a witness. This ts the very heart of the
experience in the garden: man had to serve as a witness before he could serve as a
judge. This is also the experience of mankind throughout history,
with some men testifying for Satan and against God, hoping to
become autonomous judges, and others testifying against Satan and for
God, hoping to become subordinate judges. Before becoming judges,
men must first exercise judgment concerning which kind of witness
they will be. They must also decide whose court it is, and who the
prosecutor is. Most important of all, who is the presiding judge:
God, Satan, or man?

If they testified against the serpent, and he was convicted, they
would also have to execute justice against him. They would crush his
head. It is clear why God establishcd stoning as the normal mode of
execution in a covenantal commonwealth. Stoning is the symbolic equiva-
lent of head-crushing. ‘To crush the convicted person’s head is to destroy
him. Also, the witnesses for the prosecution must take full responsi-
bility for their testimony. This is the requirement of God for human
courts, and it was the requirement in Eden. Bringing charges against
Satan, they would have to execute the Judge’s judgment.

There was no escape from the ethical obligation to witness
against Satan and for God. There still isn’t. There is also no escape
from the ethical duty of crushing the head of the serpent (Gen, 3:15).
It is done progressively, through cultural dominion. Man will even-
tually judge the angels (I Cor. 6:3).

Man is to crush the head of the serpent, and redeemed man does
so as he witnesses against Satan, but now man is vulncrable to the
bite of the serpent (Gen. 3:15). This would not have been true if
Adam and Eve had gone directly to God upon His return to the
garden and had brought charges against the serpent. While they
were waiting for God to return, they could have eaten of the tree of

 

 
