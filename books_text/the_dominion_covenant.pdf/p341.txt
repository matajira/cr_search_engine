From Cosmic Purposetessness to Humanistic Sovereignty 297

free market competition to modern statism’s central planning and in-
terference with market forces. It was not Karl Marx, It was a long-
forgollen government burcaucrat, one of the founders of American
sociology, Lester Frank Ward.

Lester F. Ward’s Planned State

Lester Frank Ward wrote Dynamic Sociology (1883), the first com-
prehensive sociological treatise writlen in the United States." He
has been described as the father of the American concept of the
planned society. 39 He was born in Illinois in 1841, His father was an
itinerant mechanic and his mother the daughter of a clergyman, He
was poor as a youth, but he still found time to teach himself Latin,
French, German, biology, and physiology. He was self-disciplined.
He joined the U.S. Treasury Department in 1865. He continued his
studies at night school, and within five years he had carned degrees
in medicine, law, and the arts. In the mid-1870’s he worked for the
Bureau of Statistics, and it was at this time that he concluded that a
study of statistics could lead to the formulation of laws of society,
which in turn could be used in a program of social planning. He con-
tinued his self-education in the field of paleontology, and in 1883, the
year Dynamic Sociology appeared, he was appointed chief palean-
tologist of the U. $. Geological Survey. Finally, after publishing five
books in sociology, he was appointed to the chair of sociology in 1906
at Brown University, the same year that he was elected the first pres-
ident of the newly formed American Sociological Association.4°

Ward's Dynamic Sociology was ignored for a decade after its publi-
cation, selling only 500 copies.'*t In 1897, a second edition was
issued, and within three years he was considered one of the leaders
in the field. After his death in 1913, his reputation faded rapidly. He
had laid the groundwork for American collectivism in the name of
progressive evolution, but he was forgotten by the next and subse-
quent generations.

Ward broke radically with Spencer and Sumner. He had two
great enemies, intellectually speaking: the Social Darwinist move-

138. Hofstadter, Social Darwinism, p. 69.

139, Qlarence J. Karler, Shaping the American Educational State: 1900 to the Present
(New York: Free Press, 1975), p. 139. Cf. Sidney Fine, Laissez-Faire and the General
Welfare State (An Arbor: University of Michigan Press, 1956), p. 253-64.

140. Ward’s biography is supplied by Hofstadter, pp. 68-71.

141, Ibid. p. 70.
