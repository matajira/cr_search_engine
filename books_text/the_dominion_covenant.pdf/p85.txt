Economic Value: Objective and Subjective 4

for $6.98, no one would spend $17.50 to get an identical clock (as-
suming everyone really believes the clocks are identical). The value
of the final good, or marginal good, determines the value of each of
them being offered for sale (disregarding transportation and infor-
mation costs).

This explanation of market pricing created an intellectual
revolution in the field of economics. The late nineteenth century saw
the advent of this explanation and its triumph among academic
economists by 1900. Menger, Jevons, and Walras buried the
arguments for intrinsic value as the basis of market value. As one
observer has put it: “If people value it, it has value; if people don’t
value it, it doesn’t have value; and there is no ‘intrinsic’ about it.”6
Value is therefore imputed by acting men. The act of imputation is the
foundation of the subjective theory of value.

Men wish to achieve their goals with the minimum expenditure
of scarce resources possible. They prefer giving up less to buy a good
than giving up more. They want to buy cheap and sell dear. It is this
goal which has led to the development of the free market. The
market permits men to impute their own personal value to a
multitude of scarce resources, depending upon their knowledge,
goals, and available resources. It enables them to make judgments
through a system of competitive bidding. Men compete for specific
quantities of specific goods and services. They offer specific prices.
This competition leads to the establishment of market prices for
specific units of scarce resources. The market price of a resource is
therefore the product of a multitude of subjective imputations of
value; it is established through competitive bidding. Market prices
are therefore the products of a grand auction process, in which buyers
and potential buyers compete against each other for specific quan-
tities of a particular resource, while scllers compete against potential
sellers in order to sell to the highest bidding buyers. A market price
is therefore an objective result of competitive subjective valuations.

Let us consider an example which illustrates some of the implica-
tions of this view, We might call it the Bible-pornography paradox. The
Bible is the very word of God and infinitely precious to mankind. Yet
in a perverse culture, it is quite likely that a capitalist could earn far
more income by selling pornographic literature than by selling
Bibles. The market does not evaluate the Bible in general versus
pornography in general. The market only informs us about the

6, The statement was made in a specch which I attended in 1967. It was delivered
by the then-Member of Parliament, Enoch Powell.
