The Evolutionists’ Defense of the Market 335

ics! This is precisely what Marx and his followers have been arguing
since the 1840’s. This has been an extremely successful argument. It
is the argument of all historicist systems: eras change, and morals
change with them. How can Hayek, as an evolutionist, deal with
historicism? He states his preference for the traditionalism of
Ferguson and Burke, which “is based on an evolutionary interpreta-
tion of all phenomena of culture and mind and on an insight into the
limits of the powers of the human reason.”2° Miller comments: “The
fact is, however, that ‘tradition’ is not a single, unified phenomenon.
What we call ‘Western civilization’ is but one of many traditions of
mankind; and internal to it are many divergent and conflicting
strands, Hayck himself acknowledges that the tradition of construc-
tivist [designing] rationalism is as old and as strong within Western
civilization as the tradition of critical [evolutionary] rationalism.
What are we to do in the face of this conflict among and within tradi-
tions? Hayek leaves us only with the options of submitting humbly
to the tradition which makes the most forceful claim upon us or else
of choosing boldly but blindly among competing traditions. He
eliminates the possibility that we can make a rational choice among
traditions on the basis of what is true or good by nature. Reason
cannot judge among traditions, because it can function only within
such a matrix as tradition itself supplies; and this matrix is non-
rational and devoid of meaning. Moreover, there are no permanent
values by reference to which reason could make this judgment, All
human values are the result of a long process of evolution, and they
continue to change in the course of this process.”?! Hayek’s system,
like all other modern systems of economics, is epistemologically
committed to process philosophy, better known as historicism. It leaves
his defense of the market intellectually defenseless against those
more self-consistent historicists who boldly proclaim a change in
eras, the arrival of a new world order.

Hayek, for all his immense erudition, is caught in a familiar bind
of all humanistic scholarship: the problem of structure and change. He
wants a moral order, but he does not want it imposed by a sovereign
God who is outside the processes of history. He wants a moral order
which provides stability, so that the free market has recognized “rules
of the game.” His later career was marked by a series of studies relat-

20. Hayek, Studies, p. 161.
21, Miller, “Hayek’s Critique,” of. cit., pp. 392-93.
