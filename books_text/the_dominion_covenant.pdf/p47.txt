Cosmic Personalism 3

of order.? It was Darwin's contribution to the modern world to have
convinced men that evolution occurs through random changes in liy-
ing creatures. The interaction between a deterministic external envi-
ronment, governed by strict cause and effect, and living creatures
that survive or perish on the basis of random mutations in their
genes, is the mechanism of evolutionary progress, This is natural
selection. The ‘deterministic universe, itself the product of random
materialistic forces, brought: forth life, and living creatures devel-
oped through the uncontrollable randorn mutations of their genes
and the interaction of the biological results of these mutations with
the environmental changes external 1a each species. In its original
formulation, Darwinism presented a world which is governed by
random variation; randomness begetting randomness in a sea of
randomness, yet governed entirely by the universally valid and
iotally unbreakable iron law of natural selection.

Peter Mcdawar and his wife Jean, two prominent biological
scientists, have stated the case very plainly in their book, The Life
Science (1977), and in the intellectual journal, Harper’. The opening
words of the Harper’s extract from the book are illuminating: “Not so
very many ycars ago people talked about ‘God and the physicists,
but today the geneticists have elbowed their way to the footlights,
and a great change has come about in relations between science and
religion: the physicists were in the main very well disposed towards
God, but the geneticists are not. It is upon the notion of randomness
that geneticists have based their case against a benevolent or
malevolent deity and against there being any overall purpose or

2. Cf. Mircea Eliade, A History of Religious Ideat (Chicago: University of Chicago
Press, 1978), pp. 60, 72-73, 91-93; Eliade, Patterns in Comparative Religion (New York:
Sheed & Ward, 1958), ch. 12; Fliade (ed.), From Primitives to Zen (New York: Harper
& Row, 1967), ch. 2. Eliade demonstrates that creation myths other than the
Hebrew version were marked by a belief that God either struggled with an existing
matter to form the world, or the world stemmed from some aspect of the god’s being,
usually from his anatomy. Only the Hebrews proclaimed an absolute distinction be-
tween creature and Creator, i.e., two separate types of being, as Van Til points out:
“All forms of heresy, those of the early church and those of moder times, spring
from this confusion of God with the world, All of them, in some manner and lo some
extent substitute the idea of man's participation in God for that of his creation by
God.” Van Til, The Theology of James Daane (Nutley, New Jersey: Presbyterian &
Reformed, 1959), p. 122. On the Mesopotamian evolutionary creation myths, see
‘Vhorkild Jacobsen, “Mesopotamia,” in Henri Frankfort, et af., Before Philosophy, The
Intellectual Adventure of Ancient Man (Baltimore, Maryland: Penguin Books, [1951]
1964), pp. 187-89, 214-16. See also Rousas John Rushdoony, The One and the Many
(Fairfax, Virginia: 'Thoburn Press, [1971] 1978), ch. 3.

 

 

 

 
