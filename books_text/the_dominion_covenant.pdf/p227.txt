Competitive Bargaining 183

form by Moses, it was Esau’s right as the firstborn, as long as Esau
remained faithful to the covenant. God knew in advance that he
would not (Rom. 9:10-13). Jacob used this incident in Esau’s life to
purchase the birthright at a remarkably low price. Esau’s character
flaw, plus the presence of his temporary hunger, combined to present
a unique economic opportunity to Jacob. Jacob was not one to let an
opportunity hke this escape. By despising the covenant, his birth-
right, Esau forfeited his rights as the firstborn son.

Conclusion

There is not the faintest hint in the Bible that Jacob’s transaction
with Esau was in any way immoral. Any attempt on the part of com-
mentators to draw conclusions from this incident concerning the im-
morality of sharp economic bargaining is wholly unwarranted
exegetically. It is in no way immoral to bargain competitively with
anyone whose lack of vision, lack of foresight, lack of self-discipline,
and lack of a strong future-orientation have combined to place him
in a weak bargaining position. Such men are entitled to purchase
their heart’s desire, namely, instant gratification, at whatever price
they are willing to pay. We should ask them to pay a lot.

This conclusion is not sufficient to justify overly sharp bargaining
with righteous men who have been forced by unpredictable circum-
stances into a position of competitive weakness. Mercy is to be
shown to victims of external crises. The moral rule against the fore-
stalling (withholding) of grain is a case-law application of the biblical
law against economic oppression. But this valid rule in no way in-
hibits men from getting the best return they can in exchanges with
undisciplined, ‘present-oriented men. There is no doubt that the
present-oriented man is at a distinct competitive disadvantage when
bargaining with a future-oriented person, and it is quite possible
that he will forfeit something as valuable as his birthright for some-
thing as valucless as a single meal. The fact that asscts tend to flow
in the direction of future-oriented, thrifty, and self-disciplined eco-
nomic actors is a testimony to the godly order of a free economy. To
stand in judgment of Jacob's competitive bargaining with Esau is to
stand in judgment of a moral and economic order which penalizes
the present-oriented man, benefits the future-oriented man, and in
no way imposes compulsion on either, Such a critical judgment cer-
tainly goes beyond the Bible’s assessment of Jacob’s actions.

  
