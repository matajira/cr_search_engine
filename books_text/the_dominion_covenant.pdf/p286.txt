242 HE DOMINION GOVENANT: GENESIS

diminishing returns. The latter law pressures men to expand their
area of responsibility, finding new means of increasing production,
as well as subduing new lands to the glory of God (or at least to the
benefit of their own pocketbooks),

Perhaps more important than anything else for a proper under-
standing of science, Genesis teaches us the concept of cosmic personal-
ism. There is purpose in the creation: God’s purpose foundationally,
but also men’s purposes derivatively. Life has meaning in terms of
God’s plan for the ages. This means that our labor has meaning, in
time and on carth, but also in the post-judgment world to come
(I Cor. 3). While some aspects of these truths are taught later in the
Bible, Genesis provides us with the basics. The theocentric nature of
all existence is the message of Genesis. This is assuredly zot the oper-
ating presupposition of humanistic science, including economics.

The Bible lays the legal and social foundations for a society which
permits human freedom. This legal and social order, when respected
by the rulers and the subjects of a nation, produces the economic
framework which is known today as the free enterprise system. Bibli-
cal economics is free enterprise economics. It is not anarchistic
economics, but it is certainly not Keynesian economics or Marxist
economics. When birthrights can be validly exchanged for a pot of
stew, and Gad honors such an exchange, we are talking about free
enterprise. When legal authorities try to intervene to reverse the
consequences of such a voluntary exchange, as Isaac tried to do,
those who are about to lose what their exchange entitled them to can
legitimately take steps to defend their lives, their property, and their
sacred honor, including the use of deception.

The example of Joseph in Egypt is just that: Joseph in Egypt, He
did not bring all of Egypt into bondage to the State as an example to
be followed by Christian societies, although there are ordained min-
isters and Ph. D.-holding economics professors, and especially politi-
cal science and sociology professors, in Christian colleges who would
dearly love to be top administrators in such an Egyptian-type cen-
tralized bureaucracy. Then, at long last, uneducated and unor-
dained laymen would have to pay attention to them—something
which most of them are riot used to, for good reason.

It would be very difficult—I would say impossible—to make a
case for “Christian socialism” by means of the Book of Genesis. As I
demonstrate in the commentaries to follow, the Pentateuch offers no
hope to the socialists who have for too long tried to argue that the
