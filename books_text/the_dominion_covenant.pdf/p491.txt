Basic Implications of the Six-Day Creation 447

Satan and man: “Let no man say when he is tempted, I am tempted
of God; for God cannot be tempted with evil, neither temptcth he
any man: But every man is tempted, when he is drawn away of his
own lust, and enticed. Then when lust hath conceived, it bringeth
forth sin; and sin, when it is finished, bringeth forth death” ( James
1:13-15).

Ethical rebellion led Gad to curse the world (Gen. 3:17-18), Men
are now ethically blind and willfully rebellious (Rom. 1). But this
evil is restraincd, as in the case of the Tower of Babel (Gen. 11:6). It
must not be regarded as a permanent phenomenon. The final end of
rebellion is the lake of fire, into which hell, death, Satan, and all his
followers shall be dumped on the day of judgment (Matt. 25:41; Rev.
20:13-14). It is a place of true existence —the eternal reminder of the
results of ethical rebellion, eternally glorifying Gad and His justice
—but a place of utter impotence. But even as hell is only a tempor-
ary dwelling place of disembodied rebellious souls, so is heaven an
equally temporary dwelling place for disembodied regenerate souls.
Heaven is not a place of total bliss and perfection, just as.hell is not a
place of total desolation, for final bliss and final desolation come only
after souls and bedies are reunited on the day of judgment (I Cor.
15:39-57). The souls of the slain saints of God are in heaven, John
informs us, crying, “How long, O Lord, holy and true, dost thou not
judge and avenge our blood on them that dwell on the earth?” (Rev.
6:10). Yet even this scene is temporary, for evil is limited in time,
however strong it may appear prior to the final judgment.

God has promised a final restoration of edenic bliss for His elect
(Rev. 21; 22). Yet He graciously gives us a foretaste of this ultimate
internal and external victory as an “earnest”—down payment—on
our blessed hope. Isaiah 65. and 66 tell of a preliminary manifesta-
tion of the new heavens and new earth, prior to the day of judgment,
for in these promised days of earthly peace, there shall be sinners
still alive (Isa. 65:20). Similarly, Ezekiel 37 presents us with the
famous vision of the valley of dry bones. The dead shall be resur-
rected. But this passage can be interpreted in terms of spiritual death
as well as physical death. In fact, it must be seen as applying te both
forms of death and both forms of resurrection. Ezckicl was called to
“Prophesy upon these bones”; it was a preaching ministry to the
spiritually dead people of Israel. Men are spiritually dead (Luke
9:60); he who believes in Christ “is passed from death unto life”
