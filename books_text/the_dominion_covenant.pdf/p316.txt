272 THE DOMINION GOVENANT: GENESIS

ods of evolutionary operation at its disposal. Biological and organic
evolution has at its upper end been merged into and largely suc-
ceeded by conscious social cvolution.””3 This, of course, is the sec-
ond great discontinuity in the history of evolution.

Earlier, I argued that evolutionists have reversed the order of
creation. Instead of affirming that a sovereign, autonomous, omnip-
otent personal God created the universe, they argue that a sovereign,
autonomous, omnipotent, and impersonal universe has created a
now-sovereign personal god, mankind. Julian Huxley took this
argument one step farther. He also abandoned uniformitarianism,
the device by which God was supposedly shoved out of the universe.
The slow time scale of cosmic evolution now speeds up, for it now
has a planning agent directing it. The new god, mankind, has the
power to speed up evolutionary proce: even as Christians have
argued that God demonstrated His power over time in creating the
world in six days. “With this, a new type of organization came into
being — that of self-reproducing society. So long as man survives as a
species (and there is no reason for thinking he will not) there seems
to be no possibility for any other form of life to push up to this new
organizational level. Indeed there are grounds for supposing that
biological evolution has comc to an end, so far as any sort of major
advance is concerned. Thus further large-scale evolution has once
again been immensely restricted in extent, being now it would secm
confined to the single species man; but at the same time immensely
ated in its speed, through the operation of the new
mechanisms now available.””* Why should this be true? Because
man has replaced genetic mutation (ordered by natural selection)
with language, symbols, and writing. “The slow methods of varia-
tion and heredity are outstripped by the speedier processes of acquir-
ing and transmitting experience.” Therefore, “in so far as the
mechanism of evolution ceases to be blind and automatic and
becomes conscious, ethics can be injected into the evolutionary
process.”76

Huxley, predictably, argued for ethical relativism. There can be
na “Absolute” ethics.77 “The theologian and the moralist will be

 

 

 

a

73. Ibid., pp. 133-34.
WA. Tbid., p. 134.

75, Tid, p. 135.

76, Idera

77. Tbid., p. 129.
