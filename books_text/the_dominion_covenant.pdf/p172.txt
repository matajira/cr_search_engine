128 THE DOMINION COVENANT: GENESIS

future goods that they value so highly. People get what they pay for,
and future-oriented societies demand far more future goods than
present-oriented societies do. They want economic growth in the
future, and this is exactly what they buy by giving up present goods.
This is a major factor in economic development,

It is a familiar aspect of the human condition to desire a return to
paradise. After all, paradise is what God originally intended for us.
But the flaming sword was to remind Adam and his heirs that the
rclurn to paradise must be in terms of God’s saving grace and His
law-order, not in terms of man’s autonomous labors. Godly men are
to strive, through faith and labor, to cenguer the burdens (curses) of
time and scarcity. It is when men try to escape these burdens
altogether, or to conquer by means of statist tyranny (the Moloch
State), that their quest for paradise is illegitimate. There are
numerous ways that men have devised to escape from time and its
burdens, but all are illegitimate: drunkenness, drugs, nudity,
primitive chaos festivals (Carnival, Mardi Gras), mystical union
with a monistic god through asceticism, Marxian revolution, and so
forth. The numerous books written by Mircea Eliade are accounts of
these various atlempts to escape limce’s bondage.'?7 The Christian
answer is hard work in terms of biblical law, and low interest rates
that are the product of a religiously based future-orientation, These
are the fruits of personal and cultural maturity.

It is important to understand, however, that low interest rates must
be the products of voluntary exchange. They are not to be legislated by a
civil government which is seeking to play God by increasing produc-
tivity through legislative fiat. When men are honest, ready to repay
loans at any cost, the risk premium in any interest rate will drop.
When governments refuse to inflate the currency, the price inflation
premium disappears, or even becomes negative, also keeping
interest rates down. When men are future-oriented, interest rates
will be low. But when the State attempts to legislate the benefits of
godly social order apart from these three features —low risks, low or
no price inflation premium, and low time-preference—then bureau-
crats merely succeed in drying up the supply of loanable funds on
the free market. They impose a price ceiling on Joans, and as always,
the result is a shortage of the price-conirolled good, which in this

17. On Eliade’s works, see Guilford Dudley HI, Religion on Trial: Mircea Eliade and
His Critics (Philadelphia: Temple University Press, 1977), chaps. 2-4.
