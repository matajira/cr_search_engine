192 THE DOMINION COVENANT: GENESIS

how honestly and efficiently he had served him for two decades,
sweltering during the day, freezing at night, and getting little sleep
(31:38-42). Had God not been with him, Jacob charged Laban, surely
Laban would have sent him away empty-handed (31:42). Yet it was
Jacob who had the wealth, not Laban.

The results of Jacoh’s deception of Isaac were altogether benefi-
cial to Jacob. The results of Laban’s deceptions of Jacob were eco-
nomically beneficial for Jacob. What are we to conclude? That de-
ceptions as such always backfire? Obviously, Jacob’s deception of his
father did not backfire. It was Esau who wailed his despair, not
Jacob. Are we to conclude that deception as such always wins?
Hardly; Laban’s losses testify to the opposite conclusion. What,
then, are we to conclude?

The Holy Pretense

We are to conclude that it is better to conform ourselves to the ex-
plicit revelation of God, unlike Isaac and Esau, and to the visible
signs.of God's favor, unlike Laban and his sons, than to defy God.
We have evidence that God blesses those who conform themselves to
His covenantal law-order. We have evidence that a similar tactic to
gain personal advantage, namely, the use of deception, can result in
vastly different results, depending upon a person’s place in the plan
of. God. God honored Isaac’s blessing because He honored the
deception by Rebekah and Jacob. The deception saved Isaac from a
crucially important error of judgment. The deception enabled Jacob
to gain that which was rightfully his, both by God’s promise and
Esau’s voluntary sale. The deception in no way led to Jacob's im-
poverishment; indeed, the words of Isaac’s blessing were fulfilled in
Jacob’s life over the next 20 years, as his heirs and capital grew
rapidly.

If Jacob’s action was categorically wrong, the Bible’s testimony
against him is inferential, not explicit. It would no doubt have been
better if Isaac had never indulged his taste for meat at the expense of
God's promises. It would no doubt have been better if Jacob, at
Rebekah’s insistence, had never had to use deception. But the decep-
tion was unquestionably preferable to Isaac’s giving of the blessing to
Esau, and that was the situation faced by Rebekah and Jacob. Jacob
and Rebekah accepted their historical circumstances and acted in
terms of them. Jacob prospered.

God deceives the unbelievers. Christ Himself spoke in parables
