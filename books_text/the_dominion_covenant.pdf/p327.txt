From Cosmic Purposelessness to Humanistic Sovereignty 283

Evolution is the religion of modern humanism. It was also the
religion of ancient humanism. The explanation is different—
evolution by natural selection—but the religion’s readly important
dogma has not been changed significantly since the primary version
was presented to mankind by Satan: ¥ shall be as gods (Gen. 3:5).

Fictional Science, Science Fiction

One of occultism’s universal themes is the appearance of a new
creation, some sort of positive human mutation.!!? But do serious
scientists take this vision very seriously? Some do, as indicated by
their explicit statements concerning recombinant DNA and genetic
engineering. Another bit of evidence appeared in The Wall Street Jour-
nai (Sept. 10, 1979), on the back page, An expensive advertisement
was run by Pertec Computer Corporation, apparently some sort of
“public service” advertisement. [t featured a photograph of America’s
miost prolific author, Dr. Isaac Asimov, whe had written over ‘200
books at the time the ad appeared."? He holds a Ph.D. in biochem-
istry, but he is more famous for his science fiction stories and his
popularizations of modern natural science. During one period of 100
months, Asimov turned out 100 books. He does all his own typing
(90 words a minute), almost every day, for most of the day. One
librarian pointed out that he has a book in each of the ten major
Dewey decimal system classifications." In short, he is no raving
lunatic. The advertisement read: “Will computers take over?”

Asimoy addressed himself to the question of computer in-
telligence. Could they ever become more intelligent than men?
Asimov's answer; the knowledge stored by a computer is not the
same as man’s knowledge. They are two separate developments.
“The human brain evolved by hit-and-miss, by random mutations,
making use of subtle chemical changes, and with a forward drive
powered by natural selection and by the need to survive in a par-
ticular world of given qualities and dangers. The computer brain is
evolving by deliberate design as the result of careful human thought,
making use of subtle electrical charges, and with a forward drive
powered by technological advancement and the need to serve partic-

 

ular human requirements.” From the “hit-and-miss” random evolu-
tion of man’s brain, to man the battling and planning survivor, to

U2. Cf. Gary North, Unholy Spirits, ch. 10.
113. Time (Feb. 26, 1979).
14. New York Times Book Review (Jan. 28, 1979)
