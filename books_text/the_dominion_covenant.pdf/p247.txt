Contingency Planning 203

of his brother (33:14), and he did not tarry with Esau, probably
because he was unwilling to risk another emotional shift by Esau,
from friendship to rage. His wisdom, and his knowledge of his oppo-
nent’s psychological weaknesses, allowed him to triumph one more
time.

Gonclusion

Jacob stands like a beacon of common sense and careful eco-
nomic planning. His example is not one to be ashamed of; it is to be
imitated. There is no shame in continual victory in the face of seem-
ingly overwhelming opposition. The world needs more godly men
who can successfully wrestle with God and circumstances, and still
emerge victorious, though possibly limping. It is Isaac, not Jacob,
who tends to be favored by the modern pietistic commentators, the
supposed victim of ungodly deception, rather than a short-sighted,
near-sighted, present-oriented old man who refused to take seriously
God’s promise concerning the respective destinies of his two sons.
Isaac was ready and willing to defy God and unlawfully sell ‘his
blessing for a plate of venison stew. The sympathy for Isaac and the
criticism showered on Jacob by modern commentators is indicative
of the power of pietism —a systematic retreat from the hard decisions
of daily life—to distort men’s judgment of the Scriptures. Future-
oriented Jacob, not present-oriented Isaac, should be our represent-
ative guide. When Isaac was old (though at least two decades away
from death), he wanted a plate of stew as his final reward before
joining his fathers in death (Gen. 27:4). When Jacob was old, he
wrestled with God and asked for still another blessing, that he might
pass through yet another danger to safety —arid with at least half his
family’s capital intact. May godly old men live like Jacob rather than
Isaac. May godly young men live like Jacob, too, in order to learn
the successful way to grow old. Victory, like any other skill, takes
practice. And if we are to learn anything from the careers of Esau
and Laban, it is that defeat takes practice, too. Esau and Laban may
have been successful men in their dealings with lesser men, but
when they faced the likes of Jacob, thcy were conditioned to defeat.
Jacob had courage, shrewdness, and a commitment to the future.
Esau and Laban were not prepared to deal successfully with a godly
man like Jacob. And neither is the unregenerate world today, Time
and God are on the side of the Jacobs of the world. They shail
become Israels.
