Purpose, Order, and Sovereignty a

tions of this explanation of social development.‘* Social evolutionary
theory preceded btological evolutionary theory. Darwin and Wallace
invented the idea of evolution through natural selection after reading
Thomas Malthus, the parson who pessimistically predicted that pop-
ulation growth would continually outrun man’s ability to increase
agricultural production.!7 Even the concept of “the survival of the
fittest” was coined originally by a social philosopher, Herbert
Spencer, another defender of the unhampered free market.18

The question arose early in post-Darwinian science: Now that
man has appeared, can the random processes of nature be left alone
ta work out their endless non-destiny? Or should man begin to
redirect the forces of evolution? Darwin’s cousin, Francis Galton,
became the founder of eugenics, the idea of genetic planning.?® In
the United States, an early founder of sociology, Lester F, Ward,
concluded that the unhampered free market cannot be trusted to
produce humane ends, any more than the unhampered forces of
nature can be relied upon to promote the purposes of humanity.
He began to publish his opinions in the early 1880's, and he was
ignored; by the early 1900's, his ideas had overthrown the arguments
of the Social Darwinists (primarily Herbert Spencer and William
Graham Sumner). The formerly autonomous and spontaneous
forces of the market must now be redirected by social and

16. F. A. Hayek, “Ihe Use of Knowledge in Society,” American Economic Revie,
XXXV (Sept. 1945); reprinted in Hayek, Individualism and Economic Order (Chicago:
University of Chicago Press, 1948), ch. 4.

17, Gertrude Himmelfarb, Denvin and the Darwinian Revolution (Gloucester,
Mass.; Peter Smith [1939] 1967), p. 66; Loren Eiseley, Darwin's Century (Garden
Gity, New York: Doubleday Anchor, [1958] 1961), pp. 181-82, 331-32. Cf. R. M.
Young, “Malthus and the Evolutionists,” Past and Present, No. 43 (1969), pp. 109-45.

18, Darwin ataributed the phrase to Herbert Spencer in the 5th edition of Origin of
Species (1868), chap. IIT (Modern Library edition: p, 52). It was the first time
Darwin used the phrase, Spencer first used it in his 1852 essay, “A Theory of Popula-
tion, deduced from the General Law of Fertility.” Cf, J. D. ¥. Peel, Herbert Spencer:
The Evolution of a Sociologist (New York: Basic Books, 1971), pp. 137-38.

19, Galton’s most influential book was Hereditary Genius (1869), Cf, D. W. Forrest,
Francis Galion: The Life and Work of a Victorian Genius (New York; Taplinger, 1974). For
a highly critical assessment of Galton and cugenics, see Allan Chase, The Legacy of
Maithus: The Social Costs of the New Scientific Racism (New York: Knopf, 1977), pp.
100-4,

20. Lester Frank Ward, Dynamic Sociology (New York: D. Appleton, 1883), 2 vals
Reprinted by Johnson Reprints (1969) and Greenwood Books (1968). For a discus-
sion of Ward's importance, see Richard Hofstadter, Social Darwinism in American
Thought (New York: George Braziller, 1959), ch. 4. See below, pp. 297-317.
