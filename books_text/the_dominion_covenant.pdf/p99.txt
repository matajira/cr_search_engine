Economic Value: Objective and Subjective 55

entire United States. Who is richer? We cannot say. We cannot legit-
imately, scientifically, economically compare the subjective utilities
of 240 million U.S. citizens and that single ecstatic Frenchman,
Subjective utilities, being subjective, cannot be added up like a col-
umn of figures. The economist may intuitively know that the United
States has more capital and wealth than “France,” meaning that one
happy Frenchman, but he cannot prove it using the laws of modern
subjective economics.

Readers may think that this a frivolous cxample. It is anything
but frivolous, A debate over its implications took place at a 1974 con-
ference of “Austrian School” economists held at South Royalton,
Vermont. Prof. Israel Kirzner, who took his Ph.D. under Mises, de-
fended the idea that economists, as scientists, cannot state whether
or not “France” has more capital and wealth than “the United
States,” since all such aggregates are fictions, and we cannot make
interpersonal comparisons of subjective utility. Prof. Murray
Rothbard, on the other hand, challenged this view as nonsensical.
Of course the United States would be richer under such conditions.
In short, Rothbard took the “common sense” position, while Kirzner
remained true to the logic of subjective value theory.

It should be clear that Rothbard is correct. The United States
would unquestionably be richer than France in the example. Yet our
knowledge of this obvious truth cannot be proven, or even con-
sistently defended, in terms of the subjectivist axiology (value
theory) of modern economics. Kirzner’s position is the systematic
one, We have to conclude that the problems associated with the
interpersonal comparisons of subjective utility are presently
unsolvable in some instances. The logic of the subjectivist position
leads directly to intellectual dead-ends, or “nonsense.” Like purely
objective explanations of value, the purely subjective explanations
are equally contradictory in certain instances. The antinomies (con-
tradictions) in the reasoning of self-proclaimed autonomous man are
inescapable. No one can make intelligent, consistent, systematic
judgments in evcry area of life by means of some hypothetically
logical, hypothetically rational, hypothetically consistent version of
pure autonomous thought and value. Each philosophical system dis-
integrates because of the contradictions of its own presuppositions
and applications.

Kirzner has discussed the theory of capital at some length in his
book, An Essay on Capital (1966). He asks very pointed questions
