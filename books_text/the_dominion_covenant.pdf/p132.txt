838 THE DOMINION COVENANT: GENESIS

appointed purposes. “Completion” refers to Aistorical fulfillment, not
an advance in “being.”)

It is not generally understood by Christians that paradise, as
represented by the garden or heaven, is impermanent. Adam was to
use the beauty of the garden as a temporary resting place, a place of
joy, almost in the same way that Western cultures regard the honcy-
moon, It was a place of learning and training. Like the honeymoon,
the garden experience was to serve as a preliminary blessing which
would lead to fulfillment in dominion. Marriage, not the honey-
moon, is central to dominion, just as the world, not the garden, was
to have been the focus of Adam’s concern. But Adam wanted para-
dise on other terms. He wanted instant knowledge, not the progres-
sive knowledge which is the fruit of dominion, first in the garden and
subsequently in the whole earth, He wanted a “higher consciousness”
apart from the labor of dominion, He wanted special knowledge, in-
stant knowledge, not the knowledge of expcericnce as a subordinate.
His eyes turned to the tree in the midst of the garden, rather than
outward toward the world, which would remain unfulfilled apart
from his active dominion. He subsequently abandoned his calling
under God. Rather than spread the zone of paradise from the
garden to the world, turning the world into a paradise, he decided to
choose instant illumination through a prohibited action, in defiance
of God.

As Adam discovered to his consternation, God would not allow
him to abandon his calling, for this calling is central to all humanity.
He was cast into the world prior to the completion of his training.
He was still responsible before God. He still had to exercise domin-
ion. Nature deserves its fulfillment. Adam would not be allowed to
abandon nature. He could not remain in the garden, that most plea-
sant of training camps, seeking higher consciousness. He had to
work. So do his heirs.

Conclusion

God will achieve His goals. Man will achieve dominion over
nature. Nature will become fully fulfilled (Rom. 8:19-23). But the
long process of dominion is now cursed. Having failed in our pain-
free training, we are now forced to learn painfully “on the job.” This
was not the case in the garden, Adam rejected pain-free training.

It is a mistake for Christians to focus their long-term hope on the
joys of heaven. Heaven is paradise (Luke 23:43; II Cor. 12:4). It,
