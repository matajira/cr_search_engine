17
THE GROWTH OF HUMAN CAPITAL

And he [God] brought him [Abram] forth abroad, and said, Look now
toward heaven, and tell the siars, if thou be able to number them: and he
said unto him, So shall thy seed be. And he believed in the Lorn, and he
counted it to him for righteousness (Gen. 1575-6).

The word of the .ord came to Abram in a vision, saying: “Fear
not, Abram: I am thy shield, and thy exceeding great reward” (Gen.
15:1). Abram’s response is illuminating. After learning of his
covenantal protection (shield) by God and his reward from God,
Abram immediately asked for more. What is significant is that he
asked about his lack of children. “And Abram said, Lord Gop, what
wilt thou give me, seeing I go childless, and the steward of my house
is this Eliezer of Damascus? And Abram said, Behold, to me thou
hast given no seed: and, lo, one born in my house is mine heir”
(15:2-3),

Abram’s candid response reveals that he knew a great deal about
biblical covenants. He knew that the protection and favor of God
accompany a calling before God. This meant that Abram’s capital
assets would now be administered within an explicit covenantal
framework. Who, then, would be the heir of these assets? Who
would ‘carry on the faithful administration of Abram’s capital?
Abram clearly understood the long-term nature of property under a
covenant. Capital is to be used faithfully, expanded, and directed
into the hands of one who will continue the faithful administration
of the assets. Capital is primarily familistic capital. This trans-
generational responsibility required that somcone else in Abram’s
house would have to be trained for long-term capital management—
management in terms of a theocratic covenant. Who should it be?
Eliezer, the Damascan? Was this the person God had chosen to con-
tinue the faithful administration of Abrams capital?

162
