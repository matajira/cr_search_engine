God's Week and Man’s Week 75

Man sinned, and he sinned early. He did not taste the fruits of
righteousness for eons before he rebelled. He went straight to the
forbidden fruit, in a rebellious assertion of his own autonomy. He
was not content with the glorious rest he had been offered. He had
been offered a taste of the fruits of labor, a rest without a week of
human labor preceding it. God had shown him what lay ahead, if
only he would conform his heart and his labor to Him. Like a dessert
before the meat and potatoes, God had offered Adam and Eve the
blessing of godly rest. In the face of this, they turned their backs on
God and declared man’s week. They converted man’s day of rest into
a day of seeming economic loss, for man would henceforth be faced
with alternative costs. For every hour he remained at rest, man
would lose the income which that hour’s labor might have produced.
Outside the covenant, man can no longer count on the fixed rela-
tionship between God’s law and God’s blessings. Outside the cove-
nant, rebellious man can no longer rest assured that his rest will
have its reward. In man’s week, men are faced with a decision: steal
time from God's sabbath rest, but increase their short-run income;
or forfeit short-run income on the day of rest, but reap the rewards of
faithfulness that God promises to His covenantally faithful people.
Had Adam not rebelled, he would not have acknowledged the valid-
ity of this choice. He would have rested, confident that he was not
stealing from God, and confident that he was not forfeiting any in-
come that he might otherwise have earned. He would have known
the fruits of righteousness. His day of rest would never have
appeared to him as an expense, but as a blessing from God. In God’s
week, the day of rest is an unmitigated blessing, a cost-free blessing,
not a day for agonizing over the costs of resting (the forfeited eco-
nomic benefits of working), When Adam declared man’s weck, he
robbed himself of a blessing he might have experienced: a day of rest
which is free of charge.

While the one-six and six-one patterns are those that we associ-
atc with a weck, we should also recognize the life-sabbath-dominion
pattern of three days. Adam was created on the sixth day. He served
briefly as an apprentice under God, getting a taste of the nature of
God’s dominion assignment. He should have rested the next day, his
first full day of life. This can also be understood as the second of
three days. The third day, he was to have begun his work. He was to
have begun as a covenant-keeper. His dominion assignment would
have brought fulfillment to him, for his work was to have been
