Basic Implications of the Six-Day Creation 439

over earthly affairs through the godly exercise of biblical law (Deut.
8). God covenants with men in terms of His law; though men violate
His statutes, yet He still shows mercy to many, as chapters 5-8 of the
epistle to the Romans indicate. God's covenant, through grace, is
sure, for man can trust in God’s word. Becausc of Christ’s sacrifice
on the cross, God’s wrath is placated (Rom. 5:8). Men can therefore
subdue the earth in confidence through God’s law (Gen. 9:1-7), for
“the earth hath he given to the children of men” (Ps. 115:16).

Fall and Restoration

By breaking the law of God, Adam brought destruction to hu-
manity (Rom. 5:12-21). Deny this historic event, and you deny the
doctrine of original sin. Deny the doctrine of original sin, and man is
left without an understanding of his desperate plight. He will think
that his own efforts can bring him eternal life. Without a compre-
hension of the effects, both in time and eternity, of the ethical rebel-
lion of man, it becomes impossible to appreciate the extent of
Christ’s atoning sacrifice on the cross. Theological modernism, so
closely linked with an evolutionary cosmology, has produced pre-
cisely this state of disbelief.’

The ethical rebellion took place in time and on earth, The death
and resurrection of Christ took place in time and on earth, The first-
fruits of the ncw heaven and new carth are now manifested and will
continue to manifest themselves in time and on earth. As men sub-
due their own hearts in terms of God’s law, they work out their gift of
salvation (Phil. 2:12). God’s gift of sanctification, personal and
social, is added unto His great gift of personal justification. God
gives the increase (I Cor. 3:7). Every good gift is from God (James
1:17). The possibility of the restoration of the external world is set
before God’s people (Deut. 8; 28; Isa. 2; 65; 66).

The Fall of man involved a false claim of divinity on the part of
man. Man, following the devil's lead, came to the conclusion that his
own word, rather than God’s, is ultimately creative. He made him-
self the judge of the reality of God’s word. He would stand between
God and the devil to test which one was telling the truth. He made
his own hypothetical neutrality as the standard of judgment. He
wanted to determine good and evil (Gen. 3:5), for knowledge is
always preliminary to the exercise of power. This was the devil’s sin

9. See above, pp. 2504.
