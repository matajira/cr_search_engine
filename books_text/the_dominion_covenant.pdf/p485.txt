Basie Implications of the Six-Day Creation 441

sonal, chance multiverse. Ours is a providential world. As Gilkey
writes: “Now in a world created by a transcendent and purposive
God, such an ultimate coherence and significance is possible. . . .
The belief that existence finds its ultimate origin in God sets each
creaturely life in a context of coherence and significance impossible on
any other terms. . . . And the sole basis for such a faith is the knowl-
edge of the Creator. Without such knowledge, there is no basis for
this context for coherence and significance, and without that context
the meaning of life quickly evaporates.”*? If a neo-orthodox theolo-
gian can see this so clearly, why is the doctrine of creation so
neglected in the pulpits of the supposedly evangelical churches? It is
this optimism concerning God's decree in history which made modern
science possible.) Without a faith in the possibility of progress,
science loses meaning. By destroying the faith in creation, apostate
science has almost entirely eroded the foundation of its own existence.

Because God’s eternal decree undergirds time, and because in
His grace He assures His people that “all things work together for
good to them that love God, to them who are the called according to
his purpose” (Rom. 8:28), Christians need not fear time, Time
brings with it the curses imposed by God as punishment for the
rebellion of man, and not until death is finally subdued and the new
heavens and new earth appear will time lose all of its characteristic
burdens, but Christians are not time’s prisoners. Our citizenship is
in heaven (Phil. 3:20). Unlike the pagans, whose chaos festivals like
Mardi Gras and Carnival have symbolized a desperate attempt to
escape time, Christians are told to walk circumspectly, redeeming
the time, that is, buying it back, prolonging it, conserving it, and us-
ing it diligently (Eph. 5:16). It is a tool for one’s calling, a gift of God
to His people. It is a resource to be used efficiently for the glory of
God, and not a burden to be escaped by means of ritual debauchery
or bloody revolution.'* Time is therefore a means of production, not

10. Gilkey, Maker of Heaven and Earth, pp. 188-89.

11, Zbid., pp. 65-66. See also Jaki, Road of Science, chaps. 1, 6, 19, 20

12, Gf. Gunther Stent, The Comming of the Gulden Age: A View of the End of Progress
(Garden Gity, New York: Natural History Press, 1969)

13. For various cxamples of this attempted “escape from time,” see the works of
the comparative anthropologist, Mircea Eliade, such as Patterns in Comparative
Religion (New York: Sheed & Ward, 1958), pp. 399-407; Myth and Reality (New York:
Harper Torchbook, [1963] 1968), chaps. 3, 5; Cosmos and History: The Myth of the Eter-
nat Retum (New York: Harper Torchbook, [1954] 1959).

14, See my study on Marxismn, Marx's Religion of Revolution: The Doctrine of Creative
Destruction (Nutley, New Jersey: Craig Press, 1968)
