492

Fasting, 179
Fate, 287n, 361
Faustus, 4
Feedback, 209
Ferguson, Adam, lin, 20, 23, $27
Fertility rate, 166
Field, George, 14n
Final judgment, +
Fisstfruits, 72, 449
Flash Gordon, 277
Flood, 381f, 389
Flyspeck earth, 248n, 275, 282
Fontenelle, Bernard de, 373
Forecasting (see entrepreneurship)
Foreign aid, 120, 180
Forestalling, 177, 183
Form-matter, 360
France, 166
Franklin, Ben, 200
Fraud, 81
Freedom, 129, 234, 242
Free enterprise, 242
Free market
autonomy, 10, 26
biblical, 99
choices and costs, 108
clearing price, 218
coordination, 22, 25, 57, 99
decentralization, 10, 22, 108, 229
defense of, 108, 320, 322A
designed, 11, 20
economic growth, 159
ethical defense, 26
harmony of interests, 94ff, 99
“impersonalism,” Off, 25, 2151
loss of faith in, 24f
process, 342, 344f, 347f
vs. self-deification, 10
service to, 217/
survival, 337
spontaneity, 20, 21f, 319
theory, 330
Friedman, Milton, 238
Fundamentalism, 28, 30, 149, 2526
Future-orientation
bargaining, 183
covenants and, 164

THE DOMINION COVENANT: GENESIS

Jacob, 183, 203

money and, 233
population and, 171f
Rebekah, 188

wealth and, 183, 215

(see also present-orientation)

Galileo, 252, 369
Galton, Francis, 21
Gambling, 226
Gamow, George, 374, 418
“Gap” theory, 420f, 4290
Garden of Eden, 84
bactlefield, 106
training camp, 68n, 69, 87, 118
Genetic engincering, 278, 283
Genetics, 3, 388
Geology, 378f
Geocentricity, 369
Germany, 166
Ghetto, 126n, 130
Ghiselin, Michael, 401n, 404
Gilkey, Langdon, 427, 437f, 441
Gillispie, Charles, 381, 393, 422
Gnosticism, 277
GNP, 53
God
chastens, 443
city of, 151
Commander 27, 174
constant, 64
Creator, 427; 454
cross-examination by, 465
decree 441
decree of, 440
eliminating, 247f
exhaustive knowledge, 6f
fatherhood of, 139, 449i
grace, 102
Holy Spirit, 428
honoring, 27
image of, 27ff, 32, 35, 36, 64f
immanence 433ff
imputation, 64, 100f, 236
Imputer, 37
Israel of, 454
judgment, 70, 119, 124, 142, 175f,
447
