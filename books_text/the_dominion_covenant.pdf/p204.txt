160 THE DOMINION COVENANT: GENESIS

lation pressure, and a very restricted domestic market.”? Those ac-
counts of economic growth which have focused so narrowly on
natural resources have too frequently been undergirded by a
phitosophy bordering on environmental determinism, and in some
cases this intellectual presupposition has been openly admitted,

On the other hand, Hong Kang is not an unquestioned case of
superior morality. Its commitment to the free market and interna-
tional free trade is too comprehensive. Hong Kong has become,
since World War II, one of the major centers of the drug traffic. One
of the reasons why Communist China allows Hong Kong io exist is
because Hong Kong serves as a funnel for opium, heroin, and other
illegal drugs that are produced in China and in Southeast Asia, This
traffic serves a dual purpose for Communist China: it provides
much-needed foreign currency, and it is part of China’s systematic
war against the West in general and the United States in particular.
In 1965, at the beginning of the escalation of the war in Vietnam,
China’s prime minister and foreign affairs specialist Chou En-lai met
with Egypt’s leader, General Nasser. Speaking of the U. 8. troops
then stationed in Vietnam, Chou said: “. . . some of them are trying
opium. And we are helping them. We are planting the best kinds of
opium especially for the American soldiers in Vietnam. . . . Do you
remember when the West imposed opium on us? They fought us
with opium. And we are going to fight them with their own
weapons, We are going to use their own methods against them. We
want them to have a big army in Vietnam which will be hostage to
us and we want to demoralize them. The effect this demoralization is
going to have on the United States will be far greater than anyone
realizes.” Hong Kong is an important part of Communist China's
war against the West.

There is also considerable evidence that Hong Kong’s drug
traffic is now, as Shanghai’s was before it, part of the multi-billion
dollar operation financed at least in part by international banking
agencies, especially those that make their headquarters in the City,
that unique free banking center located in the heart of London.*
Free trade is therefore no guarantor of human morality.

 

2. ibid., p. 37.

3. Quoted by Mohammed Heikal, The Cairo Documents (Garden City, New York:
Doubleday, 1973), pp. 306-7. Heikal heard Chou say this,

4. This is the thesis of the informative, though erratic book, Dape, Inc.: Britain’s
Opium War Against the U. S. (New York: New Benjamin Franklin House, 1978). The
