166 THE DOMINION COVENANT: GENESIS

The Demographics of Defeat

It is indicative of the widespread secularism and defeatism in late
twenticth-contury Wesiern culture that population in the industrial
states has slowed down radically. French population growth has
been slow since the middle of the nineteenth century, increasing by
about 41% from 1861 to 1974 (37 million to 52 million),? Ireland,
after the devastating famines of the 1840's, became Europe's only
zero population growth state. In fact, Ireland’s population actually
shrank, from 8 million in 1841 to 6.5 million in 1851, and from there
to about 4,5 million (counting the population of Northern Ireland,
which is part of Great Britain today) in the early 1970’s.? Ireland,
however, has remained an essentially agricultural nation, and
France was far more agricultural after 1850 than the other Western
European industrial nations, which did experience population
growth in the same period, The Netherlands grew from 2 million in
1816 to 3 million by 1849, and by 1975 the population was well over 13
million.* In 1871, Germany had some 41 million; by the mid-1960’s,
the combined populations of East and West Germany were in the
range of 73 million.’ Where European industrialization flourished,
1850-1950, there was considerable population growth,

A shift has begun to catch the attention of the demographers, the
specialists in population changes. After 1957, the birth rate in the
United States began to plunge. The fertility rate in 1957 was 3,767
births per 1,000 women, meaning that the average woman was bear-
ing almost four children in her years of fertility.6 By 1975, the fertility
rate had fallen to about 1,800 per 1,000 women, or 1,8 children per
woman. Since the replacement level of population in the United
States is 2.1 children per woman (because some children do not bear
children), the United Siates is no longer reproducing sufficient
children to replace the parents when they die. This is the lowest birth
rate in United States history.” It is a prosperity-induced slowdown,

2. BL R. Mitchell, European Historical Statistics, 1750-1970 (New York: Columbia
University Press, 1976), p. 20; The World Almanac & Book of Facts, 1976 (New York:
Newspaper Enterprise Association, 1976), p. 615.

3, Mitchell, p. 21; World Almanac, pp. 627 (Ircland), 663 (Northern Ireland).

4, Mitchell, p. 22; World Almanac, p. 640.

5. Mitchell, p. 20; World Alnanac, p. 618.

6. U. S. Department of Commerce, Social Indicators, 1976 (Washington, D.C.
Government Printing Office, 1977), Table 1/6, p. 26.

7. Final Report, Sclect Committee on Population, U.S. House of Representatives,
95th Congress, Second Session, Serial F, House Report No. 95-1842 ( Jan. 5, 1979), p. 5.
