Investment and Character 157

preservation of capital (and even ils great expansion) and invest-
ment decisions based on principle. Lot lost what he had, while
Abram multiplied his capital.

By the standards of his day, the 75-year-old Abram was in the
prime of his life. Sarah was ten years his junior (17:17), yet she was
sufficiently attractive that Abram devised a scheme of deception,
calling himself her brother (he was, in fact, only her half brother:
20:12), implying that he was not her husband, on two different occa-
sions (12:11-20; 20:1-18). In this later incident, Sarah must have been
in her nineties, unless Genesis 20 is a recapitulation of a journey
earlier than the period in which God established His covenant with
Abraham (Gen, 17). So Sarah was able to maintain her good looks
well into her later years. Abram himself lived until age 175 (25:7),
which the Bible describes as “a good old age” (25:8). He had over
half his lifetime before him when he was called by God to leave
Haran and enter the land of Canaan. He wandered for many years
without finding a place of permanent settlement.

At first, he dwelt in a mountain, along with Lot (12:8). He
waited 24 years for God to establish His covenant with him. He was
circumcised at age 99 (17:24). Thirty-seven years after his circumci-
sion, Abram purchased a final resting place for his wife and family,
the burial field for Sarah. Even then, he proclaimed to the children
of Heth, “I am a stranger and sojourner with you” (23:4). Though he
was no primitive nomad, he nevertheless wandered through Canaan
for many decades. It was not the sort of life that would commend.
itself to a patriarch, or a long-term investor, or a man who had been
promised the whole territory (12:7). He was a pilgrim—a wanderer
with a destination.

In stark contrast to Abram, his nephew Lot was a man who
seemed to passess solid, reliable economic instincts. He understood
the value of land, When strife between his shepherds and Abram’s
convinced them both that a geographical parting of the ways had
become a necessity, Abram gave Lot his choice of settlement. Lot
chose the land in the plain of Jordan, for “it was well watered every
where, before the Lorp destroyed Sodom and Gomorrah, even as
the garden of the Lorp, like the land of Egypt, as thou comest into
Zoar” (13:10). He decided to dwell in the cities of the plain, pitching
his tent toward, Sodom. The Bible informs us that “the men of
Sodom were wicked and sinners before the Lorp excecdingly” (13:13).
Lot chose good land and poor company. He assumed that the ultimate
