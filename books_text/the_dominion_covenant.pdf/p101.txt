Economie Value: Objective and Subjective 57

artificial construct wholly in opposition to the presuppositions of free
market economics, and in conflict with the methodological in-
dividualism of subjective value theory?

Kirzner himself catls this a “holistic capital concept.” He spells
out the assumptions of such a holistic capital concept; “The truth is
uhat the aggregate conecpt of capital has meaning only on assump-
tions according to which all parts of the capital stock are completely
integrated with one another, Each piece of capital cquipment in the
stock is assumed to have been constructed as part of the same central
plan which led to the rest of the stock. Each capital good has its part
lo play; no two capital goods have a function which precludes the full
utilization as planned, of the other. But these conditions can exist in
a market economy (in which planning is decentralized) only in the
state of equilibrium [a technical concept which hypothesizes perfect
foreknowledge on the part of everyone in the economy, a concept
which Kirzner himself denies can ever be applied to the real
world—G. N.]. The essential function of the market is, after all, to
bring individual plans which do not mesh, into greater mutual coor-
dination. So that it turns out that the aggregate concept of capital
presupposes conditions that are not only violated in the real world,
but which assume away some of the major problems which it is the
task of a market theory of capital to elucidate.”#!

Kirzner has understood the implications of radical subjectivism
in economics far better than the majority of his professional peers.
He has seen that in order to make accurate, meaningful comparisons
of capital stocks, we must assume the existence of a comprehensive, om-
nisctent, integrated plan which is made in advance and then executed
perfectly by an omniscient planning agent. Yet this is precisely what the
logic of the frec market denies to man or any group of men, What,
then, are we supposed to give up? Are we supposed to abandon our
wholly common practice of comparing the value of capital stocks in
different nations, or under different economic systems? Are we
therefore supposed to cease comparing the output-per-unit-of-
resource-input under socialism with output under capitalism? Are
we supposed to abandon the impressive argument— impressive to
common sense, anyway — that the high output of laborers who live in
capitalist nations is due to the far higher investment in capital per
capita in capitalist nations, compared to the low output and low per

31. dbid., pp. 121-22.
