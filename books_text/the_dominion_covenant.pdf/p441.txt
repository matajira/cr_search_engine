Cosmologies in Conflict: Creation us. Evolution 397

[Darwin] supposes variation to come about ‘by chance, and that the
fittest survive the ‘chances’ of the struggle for existence, and thus
‘chance’ is substituted. for providential design. It is not a little
wonderful that such an accusation as this should be brought against
a writer who has, over and over again, warned his readers that when
he uses the word ‘spontaneous,’ he merely means that he is ignorant
of the cause of that which is so termed; and whose whole theory
crumbles to pieces if the uniformity and regularity of natural causa-
tion of illimitable past ages is denied. But probably the best answer
to those who talk of Darwinism meaning the reign of ‘chance’ is to
ask them what they themselves understand by ‘chance’? Do they
believe that anything in this universe happens without reason or
without a cause? Do they really conceive that any event has no
cause, and could not have been predicted by any one who had a
sufficient insight into the order of Nature? If they do, it is they who
are the inheritors of antique superstition and ignorance, and whose
minds have never been illuminated by a ray of scientific thought.
The one act of faith in the convert to science, is the confession of the
universality of order and of the absolute validity in all times and
under all circumstances, of the law of causation. ‘This confession is
an act of faith, because, by the nature of the case, the truth of such
propositions is not susceptible of proof. But such faith is not blind,
but reasonable; because it is invariably confirmed by experience,
and constitutes the sole trustworthy foundation for all action.”!21 At
least he called this view what it was: faith.

That is one of the endearing qualities about science, especially
nineteenth-century, pre-Heisenberg science: its candid lack of
modesty.!22 We know where Huxley stands—at the vanguard of ir-
refutable truth —because he tells us so.

121. T. H. Hualey, “On the Reception of ‘Origin of Species'” (1887), in Francis
Darwin (ed.), Life & Latiers of Charles Darwin, 1, p. 553.

122. Werner Heisenberg, an influential physicist of the early twentieth century,
destroyed the Newtonian view of the universe. Instead of a mathematically regular,
precise warld, the modern conception is that of a world governed by the highly im-
probable laws of probability. Radical contingency was substituted for Newtonian
order. Individual events are random; only aggregates can be dealt with
statistically ~- order in the aggregate out of chaos in the individual. Huxley’s faith is,
by twentieth-century standards, hopelessly naive. For a superb study of modern
physics, see the article by the Nobel prize winner, Eugene Wigner, “The
Unreasonable Effectiveness of Mathematics in the Natural Sciences,” Communications
on Puse and Applied Mathematics, XIII (1960), pp. 1-4. Basically, the pessimism of Ec-
clesiastes 9:11-12 comes closer to modern temper than Hualey’s aptimism.
