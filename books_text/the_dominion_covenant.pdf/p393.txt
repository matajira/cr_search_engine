The Evotutionists’ Defense of the Market 349

Kirzner does not explicitly say so, but his methodology denies the
existence of any such construct, let alone its rationality. Becker does
not want to discuss the market process explicitly (how the hypotheti-
cally random actions of individuals are merged into an aggregate
which is rational), and Kirzner wants to discuss nothing else. Becker
avoids discussing the market process, and Kirzner avoids discussing
market (collective) rationality. Neither man really addresses the cen-
tral feature of the other’s position, namely, the implicit assumptions
about the one (the aggregate market) and the many (acting men).
What each scholar refuses to come out and say explicitly ip the heart
of each man’s analysis. Such is the fate of scholarly disdussions in
academic journals. :

Why the failure of cach man to “go for the throat”? T contend that
it stems from a sort of unwritten agreement among humanistic
scholars: they will not “expose the nakedness” of their opponents, if
their opponents politely reciprocate. Humanism cannot deal with the
problem of the one and the many, 50 when discussions involving this fun-
damental issue arise, neither participant is immune from a devastat-
ing attack from the other. Becker never said in his rebuttal: “Look,
Prof. Kirzner, you cannot even discuss the rationality of the market
as an aggregate, All you can discuss is the individual. All you can
discuss is a market process. You are unable to say anything about
whether the market as a whole theoretically responds to high or low
prices in predictable ways. You have no right even to use a model of
‘the market,’ since your presuppositions deny the possibility of such a
model. In fact, you cannot claim to be an economist at all, since you
are far too consistent with your own presupposition about the scien-
tific legitimacy of making interpersonal comparisons of subjective
utility, Any model of the markct must abstract from reality, and
human action is not conccivable in such terms. Without a model of
human action, you ought Lo get out-of the economics profession,
Why not sell insurance for a living?”

If Becker had attacked him so forthrightly, Kirzner might have
replied: “You cannot explain how a market works. Your system is
totally static. You cannot integrate human actions by means of a
theory of market process, because acting men are rational, they
learn from the past, and they are low price-seekers. You draw a lot of
charts that show indifference curves, but no such curves exist in real-
ity; they are all mental consiructs. All things never remain equal.
Your slatic system is a sham. You must rely on some version of
