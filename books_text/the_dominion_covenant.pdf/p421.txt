Cosmalogies in Conflict: Creation vs. Evolution 377

race in an hourglass of space; but we're only the toys in time’s great
game: time gives and time takes away.”’? Only the French censors
kept his language even remotely orthodox.

Buffon also abandoned one of the fundamental beliefs of ortho-
dox Christianity and non-Christian Aristotelian speculation (fused
temporarily in one of Thomas Aquinas’ proofs of God): the doctrine
of final causes. The universe, Buffon believed, is not headed any-
where in particular. This is one of the crucial tenets of all modern
science: teleology cannot be assumed by or proved by modern
science. In fact, it was only by Charles Darwin’s rejection of teleology
—final cause, ultimate direction, etc. — that modern biological evo-
Jutionism became possible. As we shall see, the earlier systems of
biological evolutionism assumed to some degree a teleological frame-
work. Buffon set the standard over a century before the publication
of Darwin’s Origin.”

Furthermore, Buffon rejected the idea that the present order of
existence was set immutably by God in the original creation. As
Greene summarizes Buffon’s position, “it tried to conceive organic
phenomena as the outcome of temporal process rather than a static
expression of a pattern of creation,.”’9 Providence disappears, and with
it, the idea that each kind reproduces after its own kind indefinitely
(Gen. 1:24)..He did not take this next step, Greene says, but he
could not dismiss the idea of the mutability of species from his mind.

Thus, by removing God from the realm of science, Buffon
thought he had transferred sovereignty to man, “There is no bound-
ary to the human intellect, It extends in proportion as the universe is
displayed. Hence man can and ought to attempt everything: He
wants nothing but time to enable him to obtain universal knowl-
edge.”8° Greene’s comments are significant: “Buffon had come a long
way from the Christian concept of the earth as a stage for the drama
of man’s redemption by divine grace. Burning with the thirst for

77. “Toys in Time,” by Bob Kimmel and Ken Edwards, BMI.

78. Buffon was not a biological evolutionist, however: Lovejoy, “Buffon and the
Problem of Species,” in Glass, (ed.}, Forerunners to Darwin, ch. 4, He did not believe
in the mutability of the species. Writing as he did before the development of strati-
graphy —an eatly nineteenth century science —he did not feel compelled to deal with
the problem of fossils in some temporal succession. The question had not yet arisen.
He could have both time and stable species.

79, Greene, Adam, p. 145.

80. Quoted in ibid., p. 154.
