20
CONTINGENCY PLANNING

Then Jacob was greatly afraid and distressed: and he divided the people
that was with him, and the flocks, and herds, and the camels, into two
bands; And said, If Esau come to the one company, and smite it, then the
other company which is left shall escape (Gen, 32:7-8).

Jacob had left his home at his mother’s suggestion, in order to
avoid the wrath of his brother (Gen, 27:42-45). He also wished to
fulfill his parents’ desire that he not marry a Canaanite (27:46;
28:1-5). He had left empty-handed; he returned with massive
wealth, Now, as he travelled through the land which was inhabited
by his brother, he feared for his life. He was afraid of Esau’s
vengeance. His messengers had informed him that Esau was com-
ing, accompanied by 400 men (32:4). This did not appear to be a
peaceful welcoming committee, as far as Jacob was concerned. His
mother had believed that Esau’s fury would last only a few days
(27:44), and Jacob had been absent for twenty years (31:38). Never-
theless, he was not so certain of his brother’s present-orientedness.
Perhaps Esau still bore a grudge against the brother whe he believed
had defrauded him of his blessing.

Jacob's immediate goal was to preserve at least a portion of his
capital, He divided his flocks into two sections, on the assumption
that in case of a direct confrontation, at least half of his goods would
be saved from destruction or confiscation, This willingness to forfeit
half his goods in order to save the other half, rather than risk every~
thing in an “all or nothing” situation, testifies to Jacob’s economic
realism. He intended to minimize his losses. There was too much at
stake to invest all of his assets in terms of the present-orientedness of
his unpredictable brother.

What is not generally understood is that Jacob was an old man
by this time. Joseph is spoken of as “the son of his old age” (37:3),

198
