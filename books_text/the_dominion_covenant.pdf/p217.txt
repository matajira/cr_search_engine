The Growth of Human Capital 173

establish the throne of his kingdom forever” (I Chr. 22:10). Psalm 89
is even. more explicit: “I have made a covenant with my chosen, I
have sworn unto David my servant, Thy sced will I establish forever,
and build up thy throne to all generations. Selah” (3-4). Again, “His
seed also will I make to endure for ever, and his throne as the days of
heaven” (29). The ultimate fulfillment of this promise came with
Jesus Christ.

And the angel said unto her, Fear not, Mary: for thou hast found favor with
God. And, behold, thou shalt conceive in thy womb, and bring forth a son,
and shall call his name JESUS, He shall be great, and shall be called the
Son of the Highest: and the Lord God shall give him the throne of his father
David: And he shall reign over the house of Jacob forever; and of his
kingdom there shall be no end (Luke 1:30-33).

It is Jesus Christ, the “seed born of a woman,” who is the rectpient
of, and fulfillment of, the promises. It is Jesus who finally an-
nounces, “All power is given unto me in heaven and in earth” (Matt.
28:18).

Christ has total power today. He is steadily subduing His
enemies. This is why Paul could write to the Roman church, “the
God of peace shall bruise Satan under your feet shortly” (16:20a). We
believers are now the seed of Christ: “And if ye be Christ’s, then are
ye Abraham’s seed, and heirs according to the promise” (Gal. 3:29).
‘The church is the Israel of God: “And as many as walk according to
this rule, peace be on them, and mercy, and upon the Israel of God”
(Gal. 6:16).

What does it mean, to be hcirs of the promise? Arc we to receive
everything apart from any conditions? In the area of justification, all
is by grace (Eph. 2:8-9), bul sanctification is equally by grace: “For
we are his workmanship, created in Christ Jesus unto good works,
which God hath before ordained that we should walk in them” (Eph.
1:10). Both sanctification and justification are uncarned gifts of God,
in the sense that both are freely bestowed by God. Nevertheless, the
grace of God was operating in the Old Testament era; justification
was by grace in that era, and so was sanctification, What, then, is
the source of our external blessings? Sanctification: the progressive
disciplining of ourselves and our institutions to conform to God's
criteria of righteousness. We are His sced; we arc therefore to sub-
due the earth. The seed-subduing relationship still exists. As we ex-
ercise godly dominion in terms of the concrete standards of biblical
