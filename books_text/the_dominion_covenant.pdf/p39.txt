Introduction Xxxix

Is the Dominion Covenant Truly a Covenant?

It is he personal covenant. Man’s very definition is in terms of
the dominion covenant of Genesis 1:26-28. Man is made in God’s
image, and he is to exercise dominion in God’s name. There is no
escape from this aspect of man’s being. It extends beyond the resur-
rection in the resurrected New Heaven and New Earth, and it
thwarts covenant-breakers in the lake of fire eternally. The covenant
first appeared in the garden. There is transcendence: God is the
Creator, and He assigns the dominion task to mankind. God was
present in the garden to teach man the basics (naming the animals).
There is a hierarchy, for God placed Adam under Him and over the
creation. There was a law, and it was manifested in the garden by a
forbidden tree, There was an implicit oath: violate it, God promised,
and the curse is inevitable; obey it, and blessings are assured. There
is continuity~the promise of eternal life—alongside of discontin-
uity: the promise of death the day that man rebels.

The dominion covenant is the most fundamental covenant. It
governs all the others: personal, family, church, and civil, Man is
defined in terms of the dominion covenant —not in terms of family, church,
or State.

When I wrote this book, I was unaware of the five-point struec-
ture of the covenant. In revising it, I was struck by a most remarka-
ble fact: the first six chapters conform to this covenant structure.
Chapters Four and Five are both related to the fourth point: judg-
ment. Thus, it would pay us to preview these chapters in terms of
the five-point covenant model.

1. Transcendence/Immanence (Presence)

Chapter One deals with cosmic personalism. This is based on the
Creator-creature distinction. God is wholly distinct from His crea-
tion, It shares no common being. There is no “chain of being” be-
tween God and man. This is the continuing theme of Sutton’s That
You May Prosper, and it is crucial. The Roman Catholic doctrine of
lransubstantiation (the bread and wine become the literal body and
blood of Christ) is based on a chain-of-being doctrine. So is natural
law theory. In contrast to this view is that of the Reformed faith: the
covenantal connections between man and God, God and the sacra-
ments, man’s law and God’s law. God is transcendent to the sacra-
ments, yet present in them covenantally.

The covenant is basic to the proper understanding of creation,
