The Law of Diminishing Returns 205

observed that as population increases, poorer and poorer lands are
brought into cultivation in order to feed the newcomers, so that
equal amounts of productive effort yield progressively smaller
harvests. (Of course, this statement of the problem implicitly
assumes that other factors remain equal, especially agricultural tech-
nology.) The second formulation, put forth by Turgot, is far more
relevant, the so-called law of the “intensive margin.” Professor
Schumpeter’s summary of Turgot’s position is a good one. As equal
quantities of capital (or labor) are applied to a given piece of land,
the quantities of the product that result from each apnlication will at
first increase, then decrease. If more applications of the same
resource are added, given a fixed quantity of land and fixed tech-
nology, then output will eventually fall to zero. Schumpeter writes:
“This statement of what eventually came to be recognized as the gen-
uine law of decreasing returns cannot be commended too highly.”!
After 1900, American economists termed this observation by Turgot
“the law of variable proportions.” First an increase, then a decrease
in output per unit of resource input.

It is easiest to understand in the case of agriculture. Assume that
there is a single acre of land, One man works the land by himself.
He has trouble lifting large rocks, and he cannot move boulders.
Rolling logs is very difficult. Then he hires an assistant. Now certain
jobs become manageable, and some, which were previously impossi-
ble, become possible, The total output produced by two men may be
more than double the cost of each man’s wages. So the owner of the
land hires another man, and another, and another. Eventually, the
men begin to get in each other’s way. Production sags. Costs in-
crease, It no longer pays to hire more men. It may even pay to fire
one or more of them, Marginal net returns the profits from the
addition of one resource factor to the “production mix” — eventually
fall to zero, or even become negative. The costs of employing an
additional laborer eventually exceed the benefits derived from that
additional laborer.

This is precisely the problem which faced Jacob and Esau. With-
in the confines of the available land, the two families could no longer
remain productive. The land had “filled up.” This did not mcan that
cattle were standing side by side, or that the tents of Esau’s servants

1. Joseph A. Schumpeter, A History of Economic Analysis (New York: Oxford
University Press, 1954), p. 260.
