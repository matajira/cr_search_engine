The Ecological Covenant 149

practice the same things? Should newly converted pornographers
continue reaping a fortune from selling pornography? Does the con-
version, so-called, of the pornographer somehow baptize all future
pornography published by “converted” publishers? Why is the task of
evangelism so narrowly defined? If the Synod had been consistent,
at least the preaching of the dominion covenant would have been
understood as valid for agricultural pursuits. But then the require-
ments for preaching the whole counsel of God would necessarily
spread from agricultural pursuits to agricultural equipment manu-
facturing, and government land policy, and so forth, right back into
the fearful world of reality, from which twentieth-century fundamen-
talism has been fleeing for two generations or more. Prior to 1980,
twentieth-century fundamentalists did not wish to be bothered with
the hard discipline of providing guidelines in every area of human
life — distinctly Christian guidelines —so they constructed a theology
of zero or little social responsibility in order to justify their own
laziness and lack of competence in the world outside the sanctuary.”

Conclusion

The ecological covenant of Gencsis 9 cannot be scparated from
the dominion covenant of Genesis 1. The ecological covenant is sim-
ply a corollary to the more comprehensive dominion covenant.
Every man operates under the terms of this ecological covenant,
whether he acknowledges the fact or not. No man can escape being
judged in terms of his responsibilities before God to adhere to the
terms of this covenant. Any theology which in any way mitigates or
denics the existence of this covenant is antinomian, meaning that it
is in direct and flagrant opposition to the revealed will of God. Such
a theology must be avoided at all costs,

2. See George Marsden, Fundamentalisra and American Culture: The Shaping of
Twentieth-Century Evangelicalism, 1870-1923 (New York: Oxford University Press,
1980), especially chapter 10. See also Douglas W. Frank, Less Than Conguerors: How
Evangelicals Entered the Twentieth Century (Grand Rapids, Michigan: Eerdmans, 1986).
On the legitimacy of Christian social action see The Journal of Christian Reconstruction,
VII (Summer, 1981): “Symposium on Social Action.”
