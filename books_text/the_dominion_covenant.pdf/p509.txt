Witnesses and Judges 465

They could have gone to the tree of life. They could have had a
ritual communion meal with God. They could have attained cternal
life. They refused. It was more important to cover their shame.

They had another option. When they heard God walking in the
cool of the day, they could still have run to him and admitted their
guilt, Instead, they hid from him, thereby abandoning their last op-
portunity to escape the penalty.

The Judicial Process

Immediately upon His return, God began His investigation, He
looked at the evidence, They were wearing fig leaves, He concluded
that their eyes were open. This meant that they had eaten from the
forbidden tree (3:11). Adam admitted that he had eaten, but first he
blamed his wife. He refused to “take the rap” by himself. Misery
loves company. He wanted the “bone of his bone” to suffer the pen-
alty, too.

Then God asked Eve about what she had done. She blamed the
serpent. In effect, both Adam and Eve blamed their environment,
which God had made. They blamed God indirectly, But neither
wanted to suffer the penalty alone.

This is the response of ethical rebels. It is not a godly response.
What did Christ say? “Greater love hath no man than this, that a
man lay down his life for his friends” ( John 15:13). He accepted the
full punishment. This is what Isaiah said the messiah would do for
Israel: “Surely he hath borne our griefs, and carried our sorrows: yet
we did esteem him stricken, smitten of God, and afflicted. But he
was wounded for our transgressions, he was bruised for our ini-
quities: the chastisement of our peace was upon him, and with his
stripes we are healed” (Isa. 53:4-5). What is rebellious men’s
response? Isaiah points to shame: “He is despised and rejected of
men; a man of sorrows, and acquainted with grief: and we hid as it
were our faccs from him; he was despised, and we esteemed him not”
(52:3). Men hide their faces in shame, just as our parents did in the
garden.

God judged the serpent without asking him to testify. No cross-
examination was necessary. Adam had admitted guilt; Eve had
blamed the serpent. He had lured them into sin, The serpent had
nothing to say in his defense. The serpent stood condemned, Soon
he would crawl condemned. God had seen and heard. But God did
not declare final judgment against the serpent. He declared definitive
