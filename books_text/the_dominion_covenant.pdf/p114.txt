70 THR DOMINION COVENANT: GENESIS

biblical principle is stated in Proverbs: “Train up a child in the way
he should go: and when he is old, he will not depart from it” (22:6),
If this is a general rule for the fallen sons of fallen man, how much
more true of sinless Adam and Eve?

Second, not only is man a developing being, but he is a mortal
being. If sin must be visited with death, then they would need the
mercy of God the moment they transgressed if they were not to die
physically that very day. There could be no period of suspended
judgment on God’s part. Either they would receive mercy im-
mediately, ar they would die immediately. In short, they would need
a substitutionary sacrifice.

What do we know of God's sacrificial system? We know that all
male children in Israel had to be circumcised. This ritual had to be
performed on newborn male infants on the eighth day (Lev. 12:3).
Furthermore, the sacrifice of the firstborn male animals of Israel also
had to be made on the eighth day (Ex. 22:30). The mother of the
animal could keep it for seven days; she lost it forever on the eighth.
The mother of the Hebrew boy could cuddle him as he had been
born for seven days; on the eighth day, he was taken from her and
physically marred. There was sorrow for mothers in Israel. God
reminded them of their sinfulness, and of the sin of their mother,
Eve. They were reminded graphically of the blood that has to be
shed for the remission of sins (Heb. 9:22).

The ultimate sacrifice, of course, was Jesus Christ, whose blood
was shed for the remission of sins (Matt. 26:28). He rose on the first
day of the week, the day after the Hebrew sabbath, the day of rest.
He inaugurated the restored week, the new beginning. The Chris-
tian sabbath is the first day of the week, the new beginning. The
Christian sabbath is the first day of the week, for our rest is in princi-
ple established. Christ has overcome the world. No longer do we
proclaim autonomous man’s week. Christ, the perfect human, has
re-established redeemed man’s week. The day of rest for man is the
first day of the week, the eighth day. Man now has a covering for his
original transgression.

What Christians should understand is that the eighth day is a
day of rest for us because the seventh was the day of Adams sin.
Adam announced by his self-proclaimed autonomous action in cat-
ing the forbidden fruit that he would be as God, that he would in-
augurate man’s week: six days of labor followed by a day of rest. His
weck would imitate God's original week, for he was imitating God,
