182 THE DOMINION COVENANT: GENESIS

thought he could play upon Jacob’s sympathy. This indicates that the
two did not get along well to begin with —a reasonable assumption,
given Jacob’s plain ways and Esav’s skills as a hunter (25:27). But in
all likelihood, Esau was present-oriented, How else could he justify
his willingness to give up his birthright for a mess of pottage? Would
any future-oriented man have traded so much for the sake of so lit-
tle? So he justified his willingness to enter into such a woefully ridic-
ulous exchange by feigning near-starvation. Once the transaction
was consummated, he did not even bother to keep up the pretense.
He got up and went his way, leaving his birthright behind. Then he
despised his birthright, a phenomenon known among children, by
way of “Aesop's” medieval fables, as “sour grapes.” Anything worth
as little as less than a single meal, yet so much in the eyes of the
culture of that era, had to be despised by anyone so short-minded as
to sell it so cheaply, He had wanted a handout. He had feigned help-
lessness in order to receive mercy, His trick had failed.

Why would anyone have entered into such a transaction? Esau’s
present-orientedness was his downfall. He wanted immediate grati-
fication. The benefits of the birthright seemed so far in the future to
Esau, and the food was so tempting. Why cling to something so
valucless in the present (the value of the birthright, discounted by his
very high rate of intcrest, or time-preference), when one might get
something quite valuable right now (a mess of pottage)?

Jacob knew his brother's character quite well. His offer of some
stew for his brother’s birthright was, on the surfacc, nothing short of
preposterous. He knew his brother’s price because he understood.
Esau’s preposterously high present-orientedness. Esau possessed a
very high lime-preference; he wanted instant gratification. He there-
fore made economic decisions in terms of a high rate of interest, so
that he discounted the present value of future goods quite steeply,
forcing the present value of future goods almost to zero on his per-
sonal preference scale. Jacob understood the economic implications
of his brother’s preference for instant gratification, and he made him
the offer: birthright for food. The food was worth a lot to Esau; the
future value of the birthright meant practically nothing to him. The
result was this remarkable exchange. Jacob purchased legal title to
his promised birthright.

The birthright had been promised to Jacob. Yet Jacob bargained
in a free market to obtain it. By God’s law, later put into written
