Baste Implications of the Six-Day Creation 443

perceived the breadth of the carth? declare if thou knowest it all. Where is
the way where light dwelicth? and as for darkness, where is the place
thereof, That thou shouldst take it to the bound thereof, and that thou
shouldest know the paths of the house thereof? . . . Knowest thou the or-
dinances of heaven? canst thou set the dominion thereof in the earth? Canst
thou lift up thy voice to the clouds, that abundance of walers may cover
thee? Canst thou send lightnings, that they may yo, and say unto thee,
Here we are? Who hath put wisdom.in the inward parts? or whe hath given
understanding to the heart? (38:4-7, 18-20, 33-36).

The lessons of these latter passages in the book of Job are repeated
by Paul: “For who hath known the mind of the Lord? or who hath
been his counselor? Or who hath first given to him, and it shall be
recormpensed unto him again? For of him, and through him, and to
him, are all things: to whom be glory for ever. Amen” (Rom.
11:34-36). As the Creator, He controls; as the Redeemer, He reveals.
All things are known to Him: ‘I will praise thee; for I am fearfully
and wonderfully made: marvelous are thy works; and that my sou!
knoweth right well. My substance was not hid from thee, when I was
made in secret, and curiously wrought in the lowest parts of the
earth. Thine eyes did see my substance, yet being unperfect; and in
thy book all my members were written, which in continuance were
fashioned, when as yet there was none of them” (Ps, 139:14-16). He
knows all things because He creates all things; His book sets forth
what is or is not possible and actual. And in grace He redeems: “He
that chastiseth the heathen, shall not he correct? he that teacheth
man knowledge, shall not he know? The Lorp knoweth the thoughts
of man, that they are vanity. Blessed is the man whom thou chasten-
est, O Lorp, and teachest him out of thy law; That thou mayest give
him rest from the days of adversity, until the pit be digged for the
wicked” (Ps. 94:10-13). God has revealed Himself. preeminently
through His Son (John 1). “Father, I will that they also, whom thou
hast given me, be with me wherc I am; that they may behold my
glory, which thou hast given me: for thou lovedst me before the foun-
dation of the world? (John 17:24).

Men are not autonomous from God; they are analogous to God.
Their knowledge should therefore be analogical to God’s knowledge,
that is, in conformity to His revelation concerning Himself, man,
and the creation. Men are told that they are not the source of knowl-
edge because they are not the source of the creation. They have
knowledge only to the extent that they think God’s thoughts after
