Basic Implications of the Six-Day Creation 431

understanding. When he uttereth his voice, there is a multitude of
waters in the heavens; and he causeth the vapors to ascend from the
ends of the earth: he maketh lightnings with rain, and bringeth forth
the wind out of his treasures” (Jer. 51:15-16). Psalm 104 is a lengthy
presentation of God’s creative, sustaining providence in history. This
applies equally to matters spiritual and physical: “Fear thou not; for
Tam with thee: be not dismayed; for I am thy God: I will strengthen
thee; yea, E will help thee; yea, I will uphold thee with the right hand
of my righteousness” (Isa. 41:10; cf. 42:5-6). The doctrine of provi-
dence reveals the total sovereignty of Gad.

Creator-Creature Distinction

Is God wholly removed from the world, as an eighteenth-century
deist would have argued? Is God wholly identified with the world, as
the pantheists have argued? As far back as we have written records
men have answered both ways. Sometimes, as in the case of the phi-
losopher Plato and the neo-orthodox theologian Barth, secularists
have held both positions simultaneously.? Aristotle’s “thought think-
ing itself,” deism’s watchmaker god, or Plato’s Forms or Ideas are all
wholly transcendent, wholly aloof gods. Eastern religious monism
and Western pantheism are examples of the god who reveals himself
wholly in his creation. The first god has no point of contact with life
and change, the second god cannot be distinguished from life and
change. Neither is therefore truly personal.

The Bible affirms the existence of a personal Creator who is simul-
taneously transcendent and immanent. This is not held, as in the
casc of nco-orthodoxy, on the basis of modern philosophical dualism,
but rather on the basis of a personal God’s verbal and therefore
understandable revelation of Himself to those creatures made in His
image. God is not to be identified with His creation, yet the creation
testifies to His existence, There is no uniform being that in some way
links God and the creation—some ultra something that both God
and creation participate in. There is no scale of being between the
devil and God, with God as the possessor of more being than anyone

2. On Plato’s position, see Comelius Van Til, A Survey of Christian Epistemology,
vol, II of Fn Defense of the Faith (den Dulk Foundation, 1969), ch. 3. (This was pub-
lished originally in 1932 as The Metaphysics of Apologetics.) On Barth's dualism
between God as wholly revealed, yet wholly hidden, see Van Til, Christianity and
Burthianism (Philadelphia: Presbyterian and Reformed, 1962), ch. 6.
