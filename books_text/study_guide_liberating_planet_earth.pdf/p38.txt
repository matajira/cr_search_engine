“The Liberation of the Individual” 35

41s there a universal Brotherhood of Man? (p. 62)

OA. Yes, people from all parts of the Genesis 6:5-11
world have an essential biological Ecclesiastes 7:29
sameness, Remans 3:10-17

*] B. No, languages, social customs, and
faiths are too varied for us to say that the human race is a
true brotherhood.

OC. Yes, we have the U.N. Declaration of Human Rights.

*] D. Yes, and the one thing that all men have in common is that
they are rebellious and disinherited sons of God.

5. What kind of sonship will count for men’s salvation? (p. 63)

QA. Natural sonship. Remans 8:15
OB. Naturalized sonship. Galatians 4:4, 5
*1 C. Adopted sonship. Ephesians 1:5
*1 D. Paternal sonship.

6. What is the point of contact between the saved and the lost? (p. 63)

 

*] A. They have the same biological Genesis 1:27; 5:1
makeup. James 3:7-10

*] B. The image of God in man.

*1 C. Equality before the law.

*1 D. Good will.

 

 

 

7. If unbelievers fight against the truth, what hope is there that they
will ever accept it? (p. 63)

*] A. They can never stop resisting the Deuteronomy 30:6
truth unless God gives them a new Jeremiah 31:31-33

heart. John 6:44,65
*1 B. There seems to be little hope, Philippians 2:13

because truth is elusive.

OC. We can persuade the unbeliever of truth if we will de-
emphasize dogma and minister to felt needs.

*1 D. None.
