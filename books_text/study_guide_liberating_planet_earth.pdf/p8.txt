INTRODUCTION
pp. 1-15.
Memory verses: John 8:31-32; Colossians 1:15-17

. Whywasthis book originally written? (p. 1)

QA. To help promote the most consistent and powerful secular
religion of all time: Marxism.

*1 B. To defend the rights of supporters of “liberation theology.”

*1 C. To help Spanish-speaking Christians defend themselves
against atheism, Communism, and liberation theology.

OD. To assist English-speaking Christians to realize that “two
or three prayers per week” are all that is needed to truly
liberate planet Earth.

. How can this book serve English-speaking Christians? (p. 1)

QO A. Dr. North reminds us that the faithful remnant has histori-
cally had to be content with having hope for the life to come
and sufficient power to undergo the persecutions of the world.

OB. By introducing them to the comprehensive Christianity of
the Bible which provides Biblical solutions to the complex
problems of the modern world.

*1 C. It will demonstrate that Christianity has survived almost
2,000 years by not involving itself in the unspiritual prob-
lems of society in general.

OQ D. By helping English-speaking Christians realize that two or
three prayers per week are all that is needed to truly liberate
planet Earth.
