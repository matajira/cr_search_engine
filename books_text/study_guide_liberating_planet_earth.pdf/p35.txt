32 A Siudy Guide to Liberating Plaint Earth

19, Since Satan does not have the power of God, how does he try to
rule? (pp. 58-59)

QO A. By rebelling against God he has Luke 4:1-13
become His equal. 2 Corinthians

OB. By establishing a chain of command 11:13-15
without any law. Ephesians 6:12

*1C. Satan does not have to pretend; he Revelation 2:10
does rule. Revelation 12:9

*1 D. By telling the truth, he knows that
people will find happiness through following him.

20. What is the nature of his chain of command? (p. 58)
QA. It is ultimately founded on mistrust Genesis 3:3-5

and deception. 2 Chronicles
*1 B. It is based on truth and honesty. 18:20-22
*1 C. He is a natural leader with a John 8:44
winning personality. 2 Corinthians 11:3;
*] D, He knows he cannot defeat God, so 13-15
his confidence keeps him going. 2 Thessalonians
2:9-11

21. Since Satan will not agree to the ethics of the Bible, he attempts
to gain followers through: (p. 58)

OA, Integrity and wisdom. Mark 5:2-9
OB. Good recruiting principles. 2 Timothy 2:25-26
*1C. A public relations agency in New 1 Peter 5:8

York City.

¢] D. Manipulation and tyranny.

22, Satan seeks power that is: (p. 59)

OA, Unrestricted by law. Genesis 3:3-5
OB. Shared between himself and God. John 8:44
*] C. Based on democratic principles. Acts 13:10

Q D. Founded on principles known to all. 1 John 3:8-10
Revelation 21:8
