Introduction 7

6. What is the first step that must be taken to halt our massive slide
into economic slavery? (p. 3) a

*] 4. We must create more jobs. Isaiah 45:21-23

*] B, We must control interest rates. John 3:14-17

*1 c, There must be one currency for Remans 16:19-20,
international trade. 24-27,

LD D. We must be regenerated by grace 2 Corinthians 4:6
through faith in Jesus Christ. Ephesians 2:8-10

1. This book is about two theologies of liberation. One is —————>
the other ig ———________________. (p. 3)
QA. Marxist; Christian. *] C. Capitalist; socialist.
OB. Well-known; relatively new. *]1 D. Russian; American.

8. Karl Marx taught that man is the highest form of being; in other
words, man is: (p. 4)
*] A. God. OD. A being with the power
(1B. A social animal. of reason.
«1 C. Spiritual.

9, Since Jesus claimed to be the Son of God, we
put Christianity and Marxism together. (p. 4)

QO A. May naturally OC. Should at least try to
OB. Might, under certain *1 D. Can not
circumstances,

10, The religious system popularly known as “liberation theology”

attempts to (p. 5)

CA. Guarantee everyone the right to vote.

*] B. Secure equal rights for oppressed minorities.

*1 c, Combine the Marxist theory of social revolution with Bibli-
cal words and phrases.

C1 D. Make it possible for all workers to possess their own tract
of land.
