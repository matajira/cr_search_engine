8.

10,

11,

“The Covenant of Liberation” 29

Although Satan and God are at war, it is important to remember
that Satan is not equal with God, for Satan is: (p. 54)

QO A. Neither transcendent nor immanent. Matthew 4: 10;
*1 B. Transcendent but not immanent. 16:23

QO C. Immanent but not transcendent. Acts 26:18
0D. Both transcendent and immanent. Remans 16:20

. The covenant between God and man establishes a personal rela-
tionship based on: (p. 54)
QA. Mutual cooperation. Deuteronomy
(1B, Authority and submission. 17:2-7

Oc. Rights and privileges on both sides. Jeremiah 11:3-5
*1 D. Unconditional love.

Because God is sovereign and deals with individuals on a personal
basis: (pp. 54-55)

(1 A. There should always be a wall of | Romans 14:4
separation between church and state.

(1 B. God does not allow social institutions, like civil govern-
ments, to take over His sovereign position over individuals
of being their final Commander and Monitor.

uc. Religion is a private matter.

OD. All men have the right of private interpretation.

 

 

 

 

Other men may rightfully sit in judgment over an individual:
@. 55)
ofA, At all times. 2 Chronicles 19:5-7
18. And make judgments on any area Ezra 7:25, 26

of his life. Matthew 18:15-17

Q ¢. Only if the government was demo-
cratically elected.
«lp. Only when he commits public evil.
