50

A Study Guide to Liberating Planet Earth

3. HIERARCHY

ela

Os.

ele

4, LAW
Qa.

*le

Church members obey the pronounce- 1 Timothy 3

ments of the highest cardinals and | 1 Peter 5:1-13
bishops because they are the final

interpreters of Scripture.

Ultimate power resides in the majority vote of the local
congregation.

Elders rule over deacons, and both rule through service to
the members. Elders also adjudicate disputes within the
church.

 

The church preaches the gospel and Matthew 28:20
declares God’s law for every area of John 14:21, 23
life, Acts 20:20, 21, 27

. The minister harmonizes the teach- 1 John 2:3-4;

ings of Christianity with the best teach- 3:22-24
ings of other religions, so that his

messages are both morally sound and

universally acceptable.

Ministers must teach onlv those things which are in agree-
ment with logic plus the tradition of the church. *

5. JUDGMENT

ola.

Os.

The Bishop of Rome is empowered Matthew 18:15-18
with authority over all things spiri- Remans 16:17-18
tual, temporal, and purgatorial. 1 Corinthians 5:3-5,
The church is empowered to excom- 9-13

municate offenders as a prelude to 2 Thessalonians 3:6,
the final judgment of God upon those 14-15

who refuse to obey Him.

. Churches have no powers beyond those which are allowed

under the constitutions of the particular nations in which --
they exist.
