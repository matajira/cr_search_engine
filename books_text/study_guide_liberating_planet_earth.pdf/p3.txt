TABLE OF CONTENTS

Editor’s Preface by Geoffrey W Donnan... 2.0... 20 eee I
Instructions 6 0 te 3
Introduction © 0. 5
Chapter 1. “Christ and Liberation? .. 0.0.0.0. 00000 11
Chapter 2, “TheGod of Liberation... 2... 0. ee 17
Chapter 3. “The Enemies of Liberation? ............ 21
Chapter 4. “The Covenant of Liberation? .........00.5 27
Chapter 5. “The Liberation of the Individual” ......... 34
Chapter 6. “The Liberation of the Family? ........... 41.
Chapter 7. “The Liberation of the Church”. .......... 49
Chapter 8. “The Liberation of the State? ... 0... ce 58
Chapter 9. “The Liberation of the Economy”.......... 65
Chapter 10. “The Inevitability of Liberation” .......... 69
Conclusion © 6. es 75

 

Answer Sheets 6. vid

m
