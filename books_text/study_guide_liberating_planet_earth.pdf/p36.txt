“The Covenant of Liberation” 33

23. Why do Satan and his followers threaten and torture people in this
life? (p. 59)

DA. To emphasize the point that it is Ecclesiastes 5:8
through much tribulation that we Jeremiah 8:7
enter the kingdom of God. Remans 1:18-32

OB. People are basically masochistic (they Remans_2:1-5
enjoy pain).

*1 C. He does not respect the Geneva Convention or Amnesty
International.

#1 D. To make them forget that God is going to judge all creatures
at the end of history.

24, Why can followers of Satan have no long-term confidence in the

future? (p. 60) _
*] A. Satan’s work will prosper for a while, Psalm 37

but then God will cut it short. Matthew 10:24-33
*1 B. Satan is invisible. Luke 10:17-20
*1C. Satan’s plans are not clearly laid out

in Biblical prophecy.
*] D. Satan keeps his work a secret.

DISCUSSION QUESTIONS FOR CLASSES OR GROUPS

1. Pastor Ray Sutton teaches that there are five essential parts to
Biblical covenants. Memorize this basic outline of that pattern.
Quiz one another. (p. 51)

A. Presence: Who's in charge here?

B. Submission: To whom do I report?

C. Stipulations: What are my orders?

D. Sanctions: What happens if I obey/disobey?
E, Survival: Does this outfit have a future?

2. Discuss the various ways in which Satan is causing God’s people
to have a defeatist attitude.

3. In what ways has Satan deceived you and your church into thinking
that there is nothing that can be done to change the world besides
evangelizing and waiting for Jesus to come?
