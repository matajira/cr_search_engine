20 A Study Guide to Liberating Planet Earth

10. Liberation theologians appeal to the book of Exodus when it seems
that there is support for expanding the economic power of the state,
but they hardly ever refer to the: (p. 36)

OA. Books of 1 and 2 Kings, which illustrate clearly the danger
of concentrated state power.

OB. New Testament, which says that if a man does not work,
he shall not be allowed to eat.

OC. The Wisdom literature of the Old Testament (Psalms, Prov-
erbs, Ecclesiastes, Song of Solomon), which de-emphasizes
the role of social activism and stresses contemplation.

OD. The case-law applications of the Ten Commandments in
the Book of Exodus.

DISCUSSION QUESTIONS FOR CLASSES OR GROUPS

1. Read literature that has been used in your church dealing with the
Biblical story of the Exodus. Search this literature for answers to the
following questions:

A. Can you tell whether the person who wrote this literature be-
lieved that the Biblical account of the Exodus was actually
written by Moses or by others, centuries later?

B. Can you tell whether or not the person who wrote this literature
believes that the ten plagues visited upon the Egyptians are
explainable in purely natural terms?

2, Ask the pastor of your church if he will discuss with you the topic,
“The Ten Commandments Today”. Ask him the following ques-
tions about each one of the Ten Commandments in turn:

A. Are New Testament Christians obligated to obey this com-
mandment today?

B. If not, why not?

C. If so, is it necessary or desirable for it to be enforced by law?

3. Failure to develop a Biblical government leaves a people with no
other option than exchanging various humanistic forms of tyranny.
Dr. North cites two instances from the history of 20th Century
Russia. Explain them. (pp. 31-32)
