28 A Study Guide to Liberating Planet Earth

4. When we speak of God’s “immanence”, we mean that He is:
(pp. 52-54)

QA. Transcendent. Psalm 103

Q)B. Close to and concerned about the | Matthew 6:25-34
creatures that He has made.

OC. Bright and shiny.

 

 

 

 

5. Deism has a non-Biblical view of transcendence. It teaches that:
(p. 52)

*]1 A. God is present *] D. God is so far above the

everywhere. creation that He really
OB. God is in all things, no longer interacts with
QC. God’s being evolves. it,

6. Pantheism has a non-Biblical view of immanence. It teaches that:
(p. 52)
OA. God is immersed in — or is actually a part of- the creation.
*]1 B. God is so far above the creation that He really no longer
interacts with it.
#1 C. God sees and knows all things.
Q D. God often works His will through angels.

7. How does the nature of God as revealed in the Bible relate to
transcendence and immanence? (pp. 52-54)

QA. God is distinct from the creation, yet Psalm 104
personally active in it; God is close
to His creatures, but He still exercises authority over them.

*] B. God has authority over His creation, but He does not exer-
cise it fully because there must be room for free will.

OG. God has put the universe under self sustaining natural laws,
so that He has power over the universe, yet it does have a
certain ability to run on its own.

*] D. God is the dominant principle in the course of history, so
He changes with the world.

 
