60 A Study Guide to Liberating Plaint Earth

8, INHERITANCE

OA. The system for settling disputes was | Deuteronomy
given only to Moses and a new one 31:1-29; 34:9
had to be devised when the
Israelites settled in Canaan.

CB. The judgments of Moses were for cases in which religious
issues were at stake. In this way the most important part of
Israelite life was preserved for the future.

OC. This system was set up for administering judgment “at all
times”, including after Moses himself was gone.

 

 

 

 

In questions 9-13 below, choose the correct statement under each
heading which best describes how the Satanic modem state tries
to imitate God’s covenant order. (pp. 102-103)

9, TRANSCENDENCE

OA. The state seeks to establish an Daniel 3:1-12; 6:1-9
explicit connection between
Christianity and its own authority.

QO B. The state sets up some ideal (the Party, the voice of the
people, etc.) as the highest one — thus replacing the sover-
eign God.

*] C. The wall of separation between church and state prevents
discrimination against any particular sect.

 

 

 

 

10. HIERARCHY

QA. The state installs quality control 1 Samuel 8:10-18
programs in every department to
maximize efficiency and stretch tax
dollars.

*1 B. The state installs a top-down bureaucracy with the intention
of ultimately controlling everything in the lives of the people.

OC. The state encourages the enlargement of power on the local
level so that it will not be crushed under an impossible
workload.

 

 

 

 
