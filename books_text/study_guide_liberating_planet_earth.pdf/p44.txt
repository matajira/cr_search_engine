CHAPTER 6
“The Liberation of the Family” pp. 73-87

Memory verses: Exodus 20:12;
Ephesians 5:22-25; 6:1-3; 6:4

1, What about marriage indicates that it is a covenantal arrange
ment? (p. 73)
OA. Marriage vows are built around a self-valedictory oath.
OB. It is one of the sacraments of the church.
OC. It is valid only for Christians.
*] D. Pastors can perform marriage ceremonies.

In questions 2-6 below, choose the statement under each category
which best describes the covenantal aspects of the family.
(pp. 73-74)

2, HIERARCHY

QA. Husbands have authority over their Genesis 3:16
wives; parents have authority over Ephesians 5:22-24;
their children. 6:1-2

QB. Husbands and wives rule the family Colossians 3:18
in equal partnership; parents have 1 Peter 3:1
authority over their children.

OC. Leadership roles may switch from culture to culture depend-
ing upon whether the culture is paternalistic (dominated
by the father) or maternalistic (dominated by the mother).

*] D. Children need to submit to their parents only when they
want to.

41
