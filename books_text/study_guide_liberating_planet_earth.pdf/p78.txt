6 A Study Guide to Liberating Planet Earth

4. What is the significance of the five-point structure of Biblical cove-

nants for liberation? (pp. 154-155)

QO A. It is easy to remember because it can be counted on the
fingers of one hand.

OBIma society in which the structure is respected, people can
work out their salvation with fear and trembling without
having to ask for permission from men.

OC. It is remarkably similar to Chairman Mao’s “Five-Year
Plan”,

OD. The law of Moses is contained in the first five books of the
Bible.

5. Why is a decentralized social order necessary? (p. 155)
QA. It allows for creativity and innovation.
*1 B. It makes it easier for the government to collect taxes.
*1C. It presents less of an attraction to terrorist groups.
*] D. It allows antagonistic ethnic groups to stay away from one
another.

6. What is Satan’s substitute for the omniscience and omnipotence
that belong to God alone? (p. 155)
*1 A. Voodoo.
O/B. Drugs and alcohol.
*1 C. Rock music.
*1 D. Concentrated political power.

DISCUSSION QUESTIONS FOR CLASSES OR GROUPS

1. If we as individuals can find true liberty only under Jesus Christ,
why should we expect to find true liberty in a society that is not
under Jesus Christ?

2. Why is liberation an inevitable effect of Christianity?

3. Having finished this book, do you think that the word “evangelism”
has been defined too narrowly in our generation? Why or why not?
