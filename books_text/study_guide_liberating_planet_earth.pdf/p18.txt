15,

16,

1.

“Christ and Liberation” 15

If God is in control, why does Satan seem to have so much ability
to influence history? (p. 23)

C1 A. Satan and his angels get what they 1 Chronicles 21:1
want by influencing men who are Mark 4:13-20
not busy exercising God-given Acts 5:1-3
dominion over the earth.
OB. God has withheld His judgment on evil until the Second
Coming.
*1 c. God’s people have been sent out like sheep among wolves.
OD. Satan has not yet been defeated by the archangel Michael.

Because they are creatures of God, men and angels possess:
Qn. 25-26)

¢] A. Supreme authority (they are the Psalm 8:3-8
final and ultimate authority). 1 Samuel 2:6-8

*] B. Original authority (they were the first Daniel 2:20-23
authority, but they lost it all). Ezra 1:2

*1 c, Conjectural authority (their author- John19;10, 1)
ity is merely symbolic, not real).

*] D, Delegated authority (God has assigned them to carry out
his plans).

How can the earth be liberated from the power of Satan? (p. 26)

QA. Christians should cooperate with Deuteronomy
other faiths now with the ultimate 28:1-14
goal of converting them to 2 Chronicles 7:14
Christianity. Psalm 37:27-40

*] B. Christians must make up their minds Remans 16:17-20
to eliminate their doctrinal differences __1 Peter 4:12-5:10
and present a united front to the unbe-
lieving world.

*1 c, Renewed devotion to Mary, diligence to recite our novenas,
and regular visits to the confessional.

*] D. Only through the renewal of men’s characters and the re-
sulting transfer of authority to God’s people.
