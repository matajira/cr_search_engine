82 The Reduction of Christianity

Homer Kent, who writes from an eschatological perspective simi-
lar to Hunt’s, is a representative of the position articulated by the
Old Testament commentators listed above:

Jesus based his answer on such passages as Psalm 82:6 and
Exodus 4:16 and 7:1, where God's spokesmen who minister his
word are called “gods.” His point was that if Scripture can term
such men “gods” because they were the agents to interpret divine
revelation, how could Christ be a blasphemer by claiming the
title “Son of God” when he was sent from heaven as the very reve-
lation of God himself?

In all of our discussion thus far, we have shown that the term
“gods,” elohim in the Hebrew, refers solely to magistrates, rulers,
and judges. The reference is to a God-ordained offce. It is not a
position that a// Christians hold. In this sense, it is inappropriate
and exegetically improper to apply this text to al! Christians.
Thus, since there is so much confusion today over what the
Psalmist meant in Psalm 82:6 and what Jesus meant in John
10:34, 36, Christians from all camps should avoid the use of the
terms “deification,” “little gods,” or anything else that smacks of
Mormonism and New Age philosophy. Those within the positive
confession camp should work on their Christology and an-
thropology before they get into any more semantic trouble.

 

xxii. 28, while the singular is employed in I Samuel ii. 25. In all these passages
except the last the Revised Version reads ‘God’ in the text and ‘the judges’ in the
margin, while in the last the marginal reading is in the singular, ‘the judge.’ There.
does not seem much doubt but that the judicial processes are envisaged in_all these passages,
however we translate the term, Nor need we doubt that the judicial process is seen as.something
ofa high dignity and to be performed only as in the sight of God.” Leon Morris, The Bibli-
cal Doctrine of Judgment (Grand Rapids, MI: Eerdmans, 1960), pp. 33-4. Emphasis
added. See his entire discussion, pp. 33-36.

45. Homer A. Kent, Jr, Light in the Darkness: Studies in the Gospel of John
(Grand Rapids, MI: Baker, 1974), p. 144.

46. This is being done. See Bowman, “Ye Are Gods?,” p. 22, note 14.
