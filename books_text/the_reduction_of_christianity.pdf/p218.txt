178 The Reduction of Christianity

" or each group participating feels their beliefs will eventually come
out on top. And the Christians may, in the back of their minds,
have the goal that “Well, eventually we'll convert the whole
world.” But in the meantime, they are laying the foundation for
the anti-christ’s one-world religion.®

Dave Hunt and others want to give Christians one of only two
options: choose either heaven or earth. If you choose heaven, then
you are an orthodox Christian. On the other hand, if you choose
the earth, then you “are being deceived by a new world view more
subtle and more seductive than anything the world has ever ex-
perienced,”® This is a false dichotomy. Hunt has committed the
bifurcation fallacy.™ 8. Morris Engel, in his classic work on infor-
mal fallacies, writes that “this fallacy presents contraries as if they
were contradictories.”* There is nothing contradictory in saying that
both heaven and earth are domains where the Christian shows his
faithfulness to His Lord.

The Great Commission

Hunt contends that the mission of the church involves only
personal discipleship and salvation. The Great Commission, in
the eyes of Hunt and many others, is fulfilled by preaching and
tract passing and saving individual souls. The mission of the
church is to “prepare people for heaven.”2

This is certainly part of the church’s mission in the world. But it
is not all that Jesus commanded His disciples to do. He com-
manded them to “make disciples of all the nations” (Matt. 28:19), It
is important to observe several things about this commission. First,
the task is not to “save souls” or to “prepare people for heaven.” The
task is to “make disciples.” William Hendriksen writes:

18. Dominion: A Dangerous New Theology, Tape #1 of Dominion: The Word and
New World Order, distributed by Omega-Letter, Ontario, Canada, 1987.

19. Back cover copy of The Seduction of Christianity,

20. The other names for this fallacy are: either/or fallacy; black-and-white
fallacy; false dilemma,

21. 8. Morris Engel, With Good Reason: An Introduction to Informal Fallacies (3rd
ed.; New York: St. Martin’s, 1986), p. 137.

22. Peter Waldron, Interview with Dave Hunt, “Contact America,” August
12, 1987.
