A Christian Reconstruction Library 371

ters on: the ancient tyrannical states, Greece, Rome, the early
church, medieval thought, and the rise of the modern power state.

R. J. Rushdoony, Politics of Guilt and Pity. Fairfax, Virginia:
Thoburn Press, (1970) 1978. In this collection of 34 essays, Rush-
doony shows how Christianity is at war with humanism in the
field of politics and civil government. Humanism teaches that
man can save himself through political action; Christianity
teaches that only God can save man, and politics ig only one of
many spheres of action and responsibility for the redeemed man.
The book is divided into four sections: the politics of guilt, the pol-
itics of pity, the politics of money, and the sociology of justifica-
tion. Its: most important essay is “Calvin in Geneva,” first pub-
lished in 1954. It also includes, “The United Nations: A Religious
Dream,” one of three important essays he has written on the U.N.

7, Economics '

David Chilton, Productive Christians in an Age of Guilt-
Manipulators: A Biblical Response to Ronald J. Sider. 4th ed.; Tyler,
‘Texas: Institute for Christian Economics, 1986. This is a detailed
answer to Ronald Sider’s case for government intervention in the
name of Jesus. Chilton presents a positive case for the free market
in terms of fundamental biblical principles. He then shows that
Sider’s more socialistic position is based on anti-Bible standards.
Sider refused to respond to.Chilton in the second edition of his
book in 1984. Chilton then rewrote his own book to include an-
swers to Sider’s second edition. Sider has remained silent.

Tan Hodge, Baptized Inflation: A Critique of “Christian” Kepnes-
ianism. Tyler, Texas: Institute for Christian Economics, 1986. This
book is similar to Chilton’s Productive Christians, It singles out a
particular economist who has offered modern economic interven-
tionism in the name of Jesus and attacks his system, line by line.
At the same time, this strategy of negative criticism offers Hodge
an opportunity to present the positive biblical case for economic
liberty. His target is the Keynesian economist Douglas Vickers,
