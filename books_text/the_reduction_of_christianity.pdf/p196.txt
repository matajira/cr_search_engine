156 The Reduction of Christianity

-Charles Ryrie, a leading dispensationalist theologian, dispensa-
tionalism teaches that Christ offered the Davidic kingdom to
Israel. Because Israel rejected the kingdom, its establishment was
‘postponed. In the millennium, however, Christ will establish this
Davidic kingdom." In other words, Ryrie is saying that Christ
will establish the kingdom during the millennium. Another lead-
ing dispensationalist theologian, John Walvoord, wrote a book in
1959 called The Millennial Kingdom.® Lewis Sperry Chafer, whose
massive Systematic Theology has been a dispensationalist standard,
claimed that the kingdom was postponed when the first-century
Jews rejected the Messiah. It will, however, be realized when
Christ returns and offers the kingdom again to the Jews.® Her-
man Hoyt of Grace Theological Seminary describes in glowing
terms the “richness and greatness of the kingdom” during the mil-
Jennium.* Postmillennial writer Loraine Boetiner says that dis-
pensationalism teaches. that the rejected: kingdom “is held in
abeyance until the return of Christ, at which time it is to be estab-
lished by overwhelming power." Amillennialist Anthony
Hoekema writes that, in the dispensational view, Christ’s second
coming establishes His “millennial reign,” during which Christ
“rules over a kingdom."* Thus, both dispensationalists and non-
dispensationalists agree that the teaching of mainstream dispensa-
tionalism is that Christ establishes His kingdom in the millen-

11. Charles G. Ryrie, Dispensationalism Today (Chicago, IL: Moody Press,
1965), pp. 170-173. Like Hunt, Ryrie admits that in a “spiritual” sense, Christ's
kingdom is already established on earth. This kingdom refers to God’s rule over
the hearts of men. What was postponed, therefore, was the establishment of the
external, earthly, Davidic kingdom.

12. Grand Rapids, MI:. Zondervan, 1959.

13, Chafer’s views are summarized in Clarence Bass, Backgrounds to Dispensa-
Hionalism: Hts Historical Genesis and Ecclesiastical Implications (Grand Rapids, MI:
Baker, 1960), p. 31.

14. "Hoyt, “Dispensational Premillermialisen,” in The Meaning ofthe Millennium:
Four Views (Downers Grove, IL: InterVarsity, 1977), pp. 82-83.

15. Loraine Boettner, The Millennium (3rd rev. ed.; Phillipsburg, NJ: Presby-
terian and Reformed, [1957] 1984), p. 284.

16. Hockema, The Bible and the Future (Grand Rapids, MI: Eerdmans, 1979),
p. 191.
