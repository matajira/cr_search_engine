Turning the World Right-Side Up 397

The Psychology of Pessimism

Politics is the “quick fix” approach to cultural transformation.
“The next presidential election will turn the tide. A change in the
Supreme Court will bring our nation back to righteousness. If we
could only get more conservatives elected to. office.” None of this
will do it. Only a long-term effort to change all facets of society
will bring about significant and lasting transformation. This
means changing the hearts and minds of millions of people. All
this takes time, time that is not on the side of the pretribulational
premillennialist.

There is a psychology to pessimism. A belief in impending
judgment influences the development of a strategy for building,
Where should our efforts go? If you have a vision for an earthly
future that includes reconstruction, then your efforts will be
multifaceted. While it will include politics, there will be more em-
phasis on building churches and schools and universities. Our
children will be trained to be doctors and lawyers as well as medi-
cal missionaries and engineers, two of the best ways to get into
countries. usually closed to missionaries. The design will be to
reconstruct the world from the bottom up. All facets of life will
come under the sway of the gospel and biblical law.

The short-term solution will be to change things at the top and
hope and pray that change will first come through the legislative
process, While legislative change is certainly important, espe-
cially in the case of abortion, it will not be lasting if the people
who put legislators into office do not hold biblical views, Anew
generation needs to be retaught the things of God. This will take
time, more time than the present prophetic time table will give it.

We should also keep in mind that influence for change comes
from influential professions. Christians have finally started to de-
velop Christian schools. This is a sign of activism and obedience.
But the greatest threat to the Christian school movement is the
legal establishment, made up of lawyers and judges who hold a
world view in conflict with the Christian world view. There is.also
a large lobbying group, the NEA, that has a vested interest in see-
