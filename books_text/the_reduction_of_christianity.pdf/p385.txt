Appendix A
THE NICENE CREED!

I believe in one God the Father Almighty, Maker of heaven
and earth, And of all things visible and invisible:

And in one Lord Jesus Christ, the only-begotten Son of God;
Begotten of His Father before all worlds, God of God, Light of
Light, Very God of Very God; Begotten, not made; Being of one
substance with the Father; by whom all things were made: Who
for us men and for our salvation came down from heaven, And
was incarnate by the Holy Ghost of the Virgin Mary, And was
made man: And was crucified also for us under Pontius Pilate; He
suffered and was buried: And the third day he rose again accord-
ing to the Scriptures: And ascended into heaven, And sitteth on
the right hand of the Father: And he shall come again, with glory,
to judge both the quick and the dead; Whose kingdom shall have
no.end,

And I believe in the Holy Ghost, the Lord and Giver. of Life,
Who proceedeth from the Father and the Son; Who with the
Father and the Son together is worshipped and_ glorified; Who
spake by. the Prophets; And I believe one Catholic and Apostolic
Church: I acknowledge one Baptism for the remission of sins:
And I look for the Resurrection of the dead: And the Life of the
world.to come. Amen,

1. Quoted in R. J. Rushdoony, The Foundations of Social Order (Fairfax, VA:
‘Thoburn Press, [1968] 1978), p. 16. The Western version of the creed-is quoted
here. -

345,
