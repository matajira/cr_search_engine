9
DAVE HUNT'S HEAVENLY KINGDOM

As we have seen, Hunt believes that the kingdom is predomi-
nantly a future reality. His view of the timing of the kingdom is
very closely linked with his view of what the kingdom is. In other
words, the zwhen of the kingdom determines and is determined by
the what of the kingdom. In this chapter, we will examine Hunt's
understanding of the nature of the: kingdom of God by looking
closely at the passages that he quotes in defense of his position.

As with the timing of the kingdom, Hal Lindsey is not to be
classified with Hunt on this particular issue. Lindsey writes:

God's [millennial] kingdom will be characterized by peace
and equity, and by universal spirituality and knowledge of the
Lord. Even the animals and reptiles will lose their ferocity and no
longer be carnivorous, Ail men will have plenty and be secure.
There will be a chicken in every pot and no one will steal it! The
Great Society which human rulers throughout the centuries have
promised, but never produced, will at last be. realized under
Christ’s rule. The meek and not the arrogant will inherit the
earth (Isaiah 11),1

In this respect, Lindsey is much closer to the standard dispensa-
tionalist view of the kingdom than are Dave Hunt and others.
Actually, Hunt’s view of the kingdom is hard to come by. So,
we have been forced to examine the statements of some of Hunt’s
allies in an attempt to discern what Hunt might believe about the
kingdom. Their views are no easier to obtain. An indication of

1. The Late Great Planet Earth (Grand Rapids, MI; Zondervan, [1970] 1973), p. 177,
ii
