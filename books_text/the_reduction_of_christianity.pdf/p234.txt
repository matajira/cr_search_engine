194 The Reduction of Christianity
Christ’s rule. Like Jacob, the Church limps; but like Jacob, the
Church wrestles with God and man and prevails (Gen. 32:22-32).

Conclusion

Hunt believes that Christ’s kingdom is other-worldly, “spiri-
tual,” heavenly. In a certain sense, all of these things are true. The
problem with his position is that he understands these terms in an
unbiblical way and draws unbiblical conclusions from these
truths. We have tried to show that many of the passages that he
usés to support his position do not in fact do so. Thus, though
Hunt’s view of the kingdom has some biblical support, it is one-
sided and therefore distorts what the Scriptures teach about the
kingdom of God.

49. See James B. Jordan, “The Church: An Overview,” in Jordan, ed., The
Reconstruction of the Church. Christianity and Civilization, No. 4 (Tyler, TX:
Geneva Ministries, 1985).
