A Christian Reconstruction Library 365

liberation, plus individual chapters on the liberation of the indi-
vidual, the family, the church, the state, and the economy. It con-
cludes with a chapter on the-inevitability of liberation.

Gary North, Unconditional Surrender: God's Program for Victory,
Revised edition; Tyler, Texas: Institute for Christian Economics,
(1981) 1987. This inexpensive paperback book is filled with Bible
verses, is simply written, and covers the many of the basic issues
of Christian Reconstructionism, with chapters on: God, man,
law, and time; family, church, state, and economy; the kingdom
of God and a strategy for dominion,

Gary North, 75 Bible Questions Your Instructors Pray You Won't
Ask. Revised edition; Ft, Worth, Texas: Dominion Press, (1984)
1988. This little book covers three areas: predestination, biblical
jaw, and eschatology. Each section contains 25 one-page ques-
tions, each tied to a Bible verse, and 25 one-page responses to fa-
miliar (and questionable) responses to these questions. The book
is aimed at students who attend Christian colleges.

Rousas John Rushdoony, The Jnstitutes of Biblical Law. Phillips-
burg, New Jersey: Presbyterian & Reformed Publishing Co.,
1973. This is the central document of Christian Reconstruction. It
is almost 900 pages long. It covers all of the themes of Christian
Reconstruction, but it focuses on what the Ten Commandments
teach and how they can be applied and should be applied in the
modern world. This book was the first to present the Christian
Reconstruction position in its entirety.

2. Dominion

Abraham Kuyper, Lectures on Calvinism. Grand Rapids, Michi-
gan: Eerdmans, (1898) 1961. This book has gone through many
editions. Kuyper served as Prime Minister of the Netherlands at
the turn of the century. He was a distinguished theologian and the
founder of several Christian newspapers. He was also the founder
of the (now liberal) Free University of Amsterdam. There has
