Crying Wolf? : 53

inherent goodness of man. World War I greatly disheartened this
group and World War II virtually wiped out this viewpoint. No
self-respecting scholar. who looks at the world conditions and the accelerating .
decline of Christian influence today is a “pastmillennialist.”®

Let’s rephrase Mr. Lindsey’s assertion in the light of Numbers
13-14 and Joshua 2:8-14: “No self-respecting Israelite who looks at
the land of Canaan and the decline of Israel's faithfulness can ever
believe that we can take the land because ‘we became like grasshop-
pers in our own sight, and so we were in their sight” (Num. 13:33).

In the minds of many students of eschatology, postmillennial-
ism” was stripped of the centrality of the gospel message and be-
came the darling of the purveyors of the “Social Gospel.” The
reaction of many Christian leaders was to repudiate not only theo-
logical liberalism but also postmillennialism and the social dimen-
sion of the gospel. This is a mistake and a misreading of history.

Now, the formerly withdrawn church is emerging from the
sanctuary of the cave to take on the world of unbridled secularism
(see Judges 6:1-18). Many who have moved to earthly optimism

19, Hal Lindsey, The Late Great Planet Earth (Grand Rapids, MI: Zondervan,
[1970] 1973), p. 176, Emphasis ours.

20. See Greg L. Bahnsen, “The Prima Facie Acceptability of Postmillennial-
ism” and James B. Jordan, “A Survey of Southern Presbyterian Millennial Views
Before 1930," ed., Gary North, Tie Journal of Christian Reconstruction, Symposium
on the Millennium, Vol. IE, No. 2 (Winter 1976), pp. 48-121.

21. Since 19th-century postmillennialism spoke of “progress” (the result of obe-
dience) and early 20th-century liberalism apoke of progress (“in terms of rational
and scientific planning by an intellectual elite”), postmillennialism became
suspect. Progress was equated with liberalism. While the ideals seemed similar,
the ways of getting there were quite different, This was guilt by association.
*[SJince the publication of H. Richard Niebubr’s The Kingdam of God in America
(1937), it has been widely assumed that postmillennialism Jed to the social
gospel. . . . The heart of the problem, however, has been a simplistic confusion
in the minds of many that historical-succession means necessary logical connec-
tion and succession. Hence, it is held, because postmillennialism was the original
kingdom of God idea in- America, the social gospel idea of the kingdom of God is
a logical and necessary product of postmillennialism. This ‘proves’ too much.”
R. J. Rushdoony, “Postmillennialism Versus Impotent Religion,” Journal of Chris-
tian Reconstruction, Symposium on the Millennium, p. 122.
