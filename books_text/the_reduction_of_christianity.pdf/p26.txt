xxvi The Reduction of Christianity

have got to be concerned about earth, the threat of ecological col-
lapse, the threat of a nuclear holocaust.4

Here we have the continuing historical theme in all traditional
Christian pessimism: the radical separation of heaven and earth,
which necessarily implies the increasing connection between hell
and earth. The pessimists are promoting the spread of Satan’s im-
itation New World Order when they protest the validity of Christ's
New World Order, which He established definitively with His
death, resurrection, and the sending of the Holy Spirit at Pente-
cost. Pessimism delivers the world to Satan and his followers by
default, and all in the name of biblical orthodoxy.

Whose New World Order?

Now, let me say right here: I believe in the New World Order
of Jesus Christ, inaugurated at Calvary and visibly sanctioned in
history by the resurrection and ascension of Christ to the right
hand of God, where He now reigns in power and glory. What I re-
ject is the imitation New World Order of humanism. But there isa
biblical New World Order. There is a new creation in Christ. “There-
fore, if anyone is in Christ, he is a new creation; old things have
passed away; behold, all things have become new” (2 Cor, 5:17;
New King James Version). This new creation was established de-
Jinitively at Calvary. It is being established progressively in history.
And it will be established finally at the day of judgment.

‘We cannot expect to beat something with nothing. We cannot
expect to defeat the humanists’ New World Order with a theology
of guaranteed historical defeat, the theology of traditional pessi-
mistic eschatologies. We must fight theological hellfire with thea-
logical heavenfire, just as God fought it at the destruction of
Sodom. The Sodomites lost that confrontation, not Lot, and cer-
tainly not Abraham. Pessimists forget this. Nevertheless, just be-
cause Christian Reconstructionists preach victory for the Church
in history, we are now being linked to the New Age Movement—a

23. Dominion: A Dangerous Naw Theology, Tape #1.
