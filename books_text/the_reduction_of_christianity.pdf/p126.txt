86 The Reduction of Christianity

Millions of Christians were raised on this teaching. The chick-
ens have now come home to roost, and they have now laid some
colossal theological eggs. If a person does not keep the law to
please God, then he must look elsewhere. So, then, the seduction
of Christianity has not come so much from the New Agers, who
were little known as recently as 1976, when Gary North’s None
Dare Cali It Witchcraft first appeared. The seduction of Christianity
has been in the midst of the camp of those who are New Age
humanism’s most vocal critics.

Hal Lindsey, a critic of “dominion theology,” has a chapter in
his best-selling book Satan is Alive and Well on Planet Earth (4972)
that describes “legalism” as the Christian’s obligation to keep the
law. He goes an to write:

Legalism~seeking to live for God by the principle of the law
—is the first and the worst doctrine of demons, It is the dent in.
your armor at which Satan will chip away until he has a hole big
enough to drive a truck through. I don’t know another doctrinal
distortion that has been more devastating to believers. The awful
thing is that it can sidetrack a mature believer as well as a young
one. In fact, this demonic doctrine seems to find especially fertile
soil in the life of a growing believer who is intent upon pleasing
God in this life.

Now, if Mr. Lindsey means by “legalism? that an individual is
justified on the basis of keeping the law, then his warning is justi-
fied. But he seems to go beyond this traditional interpretation of
the term. If he means that the Christian is not obligated to keep
the objective, inscripturated law as a standard of righteousness for
holy living, then he is out of accord with the testimony of Scripture.

55. Hal Lindsey, Satan is Alive and Well on Planet Earth (Grand Rapids, MI:
Zondervan, 1972), pp. 168-9.

56. Greg L. Bahnsen, Thconomy in Christian Ethics (rev. ed.; Phillipsburg, NJ:
Presbyterian and Reformed, [1977] 1984); By This Standard: The Authority of God's
Law Taday (Tyler, TX: Institute for Christian Economics, 1985).
