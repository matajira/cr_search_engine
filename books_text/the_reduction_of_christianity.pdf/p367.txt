Building a Christian Civilization 327
Wilson, Thomas Hooker, and John Eliot, who entered the Uni-
versity of Cambridge at fourteen, were educated in old England.

The University of Cambridge as they knew it, not as it has
since become, was the standard which the New England puritans
attempted, however imperfectly, to attain. . . . And the intellec-
tual life of Cambridge set the pace for the intellectual life of New
England.

The English universities, in 1630 as in 1230, were regarded
primarily as feeders to the church. Every holder of a college fel-
lowship had to be in holy orders, the ambitious young men
looked forward to becoming prelates; most of the students who
took degrees intended to be clergymen.®*

Of course, Churchill could have had in mind the anti-
Christian practices of Adolf Hitler and how they compared to
English society. Nazism was antithetical to an English society that
was nurtured on the Christian world view. Nazi Germany was
vehemently opposed to Christianity. Under the leadership of
Alfred Rosenberg, an outspoken pagan, “the Nazi regime intended
eventually to destroy Christianity in Germany, if it could, and
substitute the old paganism of the early tribal Germanic gods and
the new paganism of the Nazi extremists. As Bormann, one of the
men closest to Hitler, said publicly in 1941, ‘National Socialism
and Christianity are irreconcilable’ ”* William L. Shirer would

55. Morison, The Intellectual Life of Colonial New England, p. 20.

56. William L, Shirer, The Rise and Fall of the Third Reich (New York: Simon
and Schuster, 1960), p. 240. “By the end of 1933, a sizeable minority of Protestant
clergymen, convinced that the regime’s support of the German Christians would
corrupt pure Lutheran doctrine, had become critical of National Socialism.
Within a few months these pastors had met together and formed the Confessing
Church (Bekennende Kirche).

“The way of the Confessing Church was by no means easy. The regime ar-
rested leaders, closed special pastoral training centers, conscripted a dispropor-
tionately large number of members, and made public attacks through the news
media... .

“An excellent, and a generally neglected source, for examining the activities
of the churches in Germany during the war is the reports of the Sicherieitsdienst
(S.D.), the internal intelligence agency of the SS... .

“Ordinarily, the 8.D. was precise in designating religious groups and individ-

   
