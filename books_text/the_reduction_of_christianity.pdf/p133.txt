What Is New Age Humanism? 93

structionist theologians are being seduced by New Age human-
ism, then why haven’t they adopted any of its central planks?
Why haven’t they adopted monism (pantheism) or evolutionism?

Who's Really an Ally of the New Age?

It’s possible that those who hold to a pessimistic earthly world
view, can be seduced by some New Age premises. New Age hu-
manists believe, as John Naisbitt says, that it is possible to re-
invent “the world we live in.” Christians who fail to counter this
secularized, man-centered, power-oriented religion will find
themselves unsuspecting allies with numerous militant humanist
groups. As we have already noted, the humanists fear Christians
oriented toward dominion far more. than Christians oriented to-
ward defeat.”2

Christians may also be unwitting allies of the New Age in
another sense. If Christians retreat from the cultural issues of the
day, who will, humanly speaking, visibly control the future course
of history? If Christians won't, humanists will. Thus, Hunt’s vi-
sion of the future becomes the worst kind of self-fulfilling proph-
ecy when it is taken seriously by Christians. Christians retreat
because there is no hope. As more Christians retreat, there is less
hope. Finally, the whole cultural field is left to humanists who in-
sist on taking us down the road to an international statist utopia.

Hunt's critique of Christian reconstruction and dominion the-
ology is curiously one-sided. This is partly because his view of the
New Age is one-sided. Hunt concentrates on the upbeat, opti-
mistic side of New Age humanism. But there is a pessimistic side
as well. Douglas Groothuis quotes froma California Democratic

71. Naisbitt, Megatrends: Ten New Directions Transforming Our Lives (New York:
Warner Books, 1982), p. ix.

72. See Frederick Edwords and Stephen McCabe, “Getting Out the ‘Vote: Pat
Robertson and the Evangelicals,” The Humanist, Volume 47, Number 3 (May/June
1987), pp: 5-10, 36; and CovertAction, Special Issue on the Religious Right, Num-
ber 27 (Spring 1987). We don’t know who publishes CovertAction, but it's indexed
in the “Alternative Press Index.” We don't want to fall prey to guilt by association,
but we think that tells us something about the publisher's political preferences,
