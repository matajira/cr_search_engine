SUBJECT INDEX

Abortion, 54, 128, 309
American Revolution, 255-56
Amillennialism, 40-41, 352-53
Anabaptism, 203-4, 2360.
Antichrist, souiv

Creeds, 15

History, 63

New Age Humanism, 97

Pretribulational Dispensationalism,

xxv ool

Antinomianism, 87-88, 90-91
Apollinarianism, 12
‘The Aquarian Conspiracy, 99, 101, 129
Arianism, 11-12, 75n
Armageddon, 62
Artist Class, 105-6
Ascension of Christ, 164, 211-12
Athanasian Creed, 348-50
Augsburg Confession, 236

Babylon, 112-14
Banner of Truth Trust, 270
bastléia, 207-8
The Beatles, 99
Beyond Seduction, xxxiv, 1, 8, 15, 146
Biblical Law, 341n
Christian Reconstruction, 31, 85
Dispensationalism, 84-91
Grace, 27n
Justification, 87
Black Death, 60-61

Calviniem
Education, 34
Christian World View, 36

Politics, 34
Progress, 34-35, 236-38
Resistance, 196n
Sin, xxi
See also Puritanism, Westminster
Standards
The Calvinistic Concept of Culture, 36
Chain of Being, 70, 73-74
Chalcedon Foundation, 31
Chiliasm, see Millennialism
Christian Civilization, 121-23, 272,
282-86, 300-32
Christian Reconstruction, 30-37
Biblical Law, 31, 85
Creeds, 8, 335-36
Dave Hunt, 19
Dispensationalism, xiv, xxxy,
293-94, 338-40
Great Commission, 32
New Age Humanism, ix, xxiii-xxiv,
20
Politics, 21-23
Postmillennialism, 42
Presuppositionalism, 32
Progress, 31
Puritanism, 32
Revolution, 196-98, 201
Sovereignty of God, 31
‘Westminster Standards, 31
Christian Reconstruction from a
Pretribulational Perspective, 293-94,
338-40
Christian Unity, 5-7, 10
Christianity Today, 269
Ghildren of God, 144

397
