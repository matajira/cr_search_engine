198 The Reduction of Christianity

What distinguishes biblical dominion religion from satanic
power religion is ethics.”

Biblical Militancy

On the other hand, the Bible itself uses military metaphors to
show that the Christian is engaged in relentless battle with the
enemy. Of course, the Bible calls us to fight our ethical battles with
spiritual weapons, but the People for the American Way folks don’t
understand that. The church has sung the hymn, “Onward,
Christian Soldiers,” for decades. Consider the militant words and
how unbelievers would respond to the militaristic tone:

Onward Christian soldiers, marching as to war,
With the cross of Jesus going on before

Christ the royal Master leads against the foe;
Forward into battle, see His banners go.

The Apostle Paul tells Christians to “put on the full armor of
God” (Eph. 6:11), Of course, Paul is talking about a spiritual bat-
tle, but those outside the church community may not perceive it
in those terms, just as Norman Lear and People for the American
Way (PAW) misconstrue our intentions. What if a pastor quoted
Paul’s words in Ephesians 6:11 to his congregation and a represen-
tative from PAW was there? Imagine the headlines: “Minister ad-
vocates taking up arms. Every man is to be armed with weapons
to defeat the enemy.” The word would go out warning Americans
that Christians are advocating armed conflict.

At first, even Pilate considered Jesus’ kingdom to be militaris-
tic and political (John 18:28-40). In Acts, the Christians were de-
scribed as a sect preaching “another king, Jesus” (Acts 17:7).
Their persecutors were the. forerunners of People for the American

7. (Tyler, TX: Institute for Christian Economics, 1985), p. 2. Dr. North distin-
guishes between “Power Religion,” “Escapist Religion,” and “Dominion Religion”
(pp. 2-5). He makes it very clear that “Power Religion” is the militant religion.
