Here a Conspiracy, There a Conspiracy 125

struggle is still to be enacted in history.?

The new age.has already been ushered in. . . . the New Testament
believer was conscious that he was living in the last days and the
last hour. . . .3 :

‘Among biblical writers no one has laid so much stress on the
fact that Christ has ushered us into a new age as has the Apostle
Paul. In Colossians 1:13 he says that God “has delivered us from
the dominion of darkness and transferred us to the kingdom of
his beloved Son,” implying that we have been delivered from the
power of the old aeon [age] of sin (cf, Gal. 1:4).4

There was a new age after the fall of man, an age of sin and
death. The new age brought on by Adam's sin became the old age
after the coming of the second Adam, Jesus Christ (2 Cor. 5:17).
The New Christian Age is also described as the “new covenant”
(Jer. 31:27-34; Heb. 8:8-12).

 

Jesus’ New Age

Long before the “New Age” became identified with pagan oc-
cultic practices, the term was used to describe the new age that
Jesus inaugurated through His death, resurrection, and ascension.
One such book that expresses this view is Roderick Campbell's
Israel and the New Covenant, originally published in 1954 by Presby-
terian and Reformed Publishing Company and recently reprinted.>

2. Millard J. Erickson, Christian Theology, 3 vols. (Grand Rapids, MI: Baker,
1985), yol. 3, p. 164. Emphasis added.

3. Anthony A. Hoekema, The Bible and the Fature (Grand Rapids, MI: Eerd-
mans, 1979), p. 30.

4. Idem.

5. Presbyterian and Reformed Publishing Company (P&R) is nothing but ar-
thodox. They have published books by Jay Adams, Comelius Van Til, and
Henry Morris, In fact, it was P&R that brought Whitcomb and Morris’ Genesis
Flood into print when a number of evangelical publishers would not. While P&R.
does not push a single eschatological position, the books they have published or.
distributed on the subject have’ been either amillennial (William Hendriksen,
William Cox, and Jay Adams) or postmillennial (R. J. Rushdoony, Marcellus
Kik, and Loraine Boettner). Boettner’s The Millennium had gone through thirteen
printings by 1984.

‘The Foreword to Israel and the New Covenant was written by O, T. Allis, who,
was for seven years Professor in the Old Testament Department of Westminster
