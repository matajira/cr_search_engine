Let’s Define Our Terms 39

and the resurrection. In broad terms, the premillennialist does
not believe that Christianity will triumph over all other systems
on earth without Christ’s sudden intervention.

One particular brand of premillennialism has been called “dis-
pensational premillennialism.”6 As a general system, dispensa-
tionalism is distinguished by several emphases, First, dispensa-
tionalists rely on what they consider to be a literal interpretation
of the text of Scripture. Second, the dispensationalist distin-
guishes sharply between Israel and the church. They are two
separate peoples of Gad. God has different purposes for these two
peoples. The church is God’s “heavenly people,” while Israel re-
tnains, even after Christ’s first advent, God’s “earthly people.”

In addition to these more general differences, the dispensa-
tional premillennialist differs from the historic premillennialist on
several details of the “end times.” Dispensationalists, for example,
relying on a literal interpretation of Ezekiel 40-48, conclude that
“in the millennium, the Jewish temple will be rebuilt and the en-
tire sacrificial system reinstituted.”*? Furthermore, the dispensa-
tionalist interpreter has a clear idea of God’s purposes for ethnic
Israel during the millennium. On the other hand, there are some
overriding similarities between the two forms of premillennialismi,
Like historic premillennialism, dispensationalism teaches that
Christ will return physically to establish His millennial kingdom
on earth. Both, furthermore, believe that the.church will be vic-

50. In the past century, most popular American premillennialists have been
dispensationalists, including. Wiliam E. Blackstone, Dwight L. Moody, C. 1.
Scofield, Alva J. McClain, Herman A. Hoyt, Charles Ryrie, Dwight Pentecost,
Hal Lindsey, H. A. Ironside, and John Walvoord.

54. Charles Ryrie, Dispensationatism Today (Chicago, IL: Moody Press, 1965),
pp. 44-47. Ryrie lists a third distinctive feature of dispensationalism: that the un-
derlying purpose of God is His own glory. He compares thix to what he perceives
to be covenant theology’s emphasis on- salvation. It is hard to see how Ryrie
might come to this conclusion about covenant theology. The covenantal West-
minster Shorter Catechism’s first queation says that man’s chief end is “to glorify
God and to enjoy Him forever” It bas always been a hallmark of covenant theol-
ogy to emphasize the centrality of bringing glory to God.

52. Clouse, Meaning of the Millennium, p. 26.
