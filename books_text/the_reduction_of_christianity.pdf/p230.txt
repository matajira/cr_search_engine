190 The Reduction of Christianity

new creation, they are in faith and obedience to their Savior-King
to make of their sphere and the whole world a new creation.” A
book published by American Vision Press stresses that the basic
form of government is self-government. “A self-governed individ-
ual is one who has been born again, where the heart of stone has
been removed and replaced with a heart of flesh.”#

These quotations indicate two things. First, “reconstruction-
ists” teach that being born again is a prerequisite to exercising
godly dominion. And, of course, evangelism is a prerequisite to
being born again. Second, they show that their idea of evangelism
is much broader than that of many other evangelical Christians.
The point is that we evangelize to increase and serve the kingdom
of God, not merely to save men from hell.

We agree with Hunt that the fundamental peace established
by the gospel is peace with God. This is basic. It is the foundation of
everything else. But peace in Scripture is not confined to internal
and spiritual peace. Biblical peace, which is extended as the king-
dom spreads throughout the earth, is much fuller. It refers to a
comprehensive prosperity, healthfulness, and harmony. This
peace flows from heaven to earth. The peace established by Christ
between the Holy God and sinful men emanates into man’s entire
social life. The peace of Christ has produced and should produce
peace among men. As men are reconciled to God, they should be

40, Rushdoony, “Evangelism and Dominion,” Journal of Christian Reconstruction
vol. 7, no. 2 (Winter 1981), pp. 11. Later in the same article, he wrote, “We are
not converted merely to die and go to heaven but to serve the Lord with all cur
heart, mind, and being. We are born again to be God’s people, to do His will, to
serve His Kingdom, and to glorify Him in every area of life and thought” (p. 18).

41, Gary DeMar, God and Government: A Biblical and Historical Study (Atlanta,
GA: American Vision Press, 1982), p. 13. In another American Vision publica-
tion, we find this statement: “seff groemment supports all other forms of govern-
ment. Christian self govemment requires God's grace in regeneration. The far
reaches of civil government will not be changed until rebels against God are
tumed into faithful subjects. . . . God changes the heart, a new spirit is put in-
side the once-dead rebel, a teachable heart is implanted, then we will walk in His
statutes, and then we will observe all His ordinances.” In Gary DeMar, Ruler of the
Nations; Biblical Principles for Government (Ft. Worth, TX: Dominion Press, 1987),
pp. 164-65.
