Let’s Define Our Terms 33

obedience to the Lord and for His glory. The American Puritan
preacher John Cotton said,

A true believing Christian . . . lives in his vocation by his
faith. Not only my spiritual life but even my civil life in this
world, and all the life live, is by the faith of the Son of God: He
exempts. no life from the agency of his faith.

The Puritans were not, however, abstract theorists who’ sat
idly in their towers spinning abstract philosophies.

Puritanism was a reform movement, Its identity was determined
by its attempts to change something that already existed. At the
heart of Puritanism was the conviction that things needed to be
changed and that “business as usual” was not an option. . . .. Of
all the key terms used by the Puritans, the foremost were reform,
reformation, or the adjective reformed. These terms were not the
coinage of later historians but were the words on everyone’s lips
during the Puritan era itself. It was an age in which rulers were
urged “to reform their countries,” churchmen to effect “the refor-
mation of religion,” and fathers “to reform [their] families.” At a
more personal level, the Puritan impulse was to “reform the life
from ungodliness and unrighteous dealing.”**

The Puritans’ vision “was nothing less than a totally re-formed
society based on biblical principles.” In short, the Puritans “were
activists to the very core of their being.” Significantly, as we shall
see in detail in chapter 13, the Puritans were confident that their
efforts would succeed.

Thus, we find in the Puritans many of the distirictive qualities
of the “reconstruction movement”: commitment to the authority

33. Quoted in ibid., p. 26.
34. Bid, p. Ml.
35. Mid., p. 212,
