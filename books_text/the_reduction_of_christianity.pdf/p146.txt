106 The Reduction of Christianity

ment of technology and try to take humanity into a period of his-
tory in which we can reach for a utopia. Of course, it is possible
for the technology to be misused~we could end up with a Big
Brother—but we could also have a balanced society, with an art-
ist class leading the culture toward something approximating a
happy family or tribe.

At the moment, the nation is in a fog, and we've got to put
our headlights on. Artists—those who rely on their intuition—
can be the nation’s headlights.2

Coppola’s world view comes on bold and bright through the
larger-than-life silver screen. He doesn’t set out to teil you: “This:
is my world view; God does not matter.” Rather, he describes and
promotes his world view by creating a world that leaves out Jesus
Christ. Yes, Jesus is often. mentioned in film, but only as an
obscenity. Most audiences don't really note the expletives on film
because Jesus has been trivialized in life. He was a great man. He
may have been god-like, but we all have a similar “spark of divin-
ity.” In a sense, we're all god-like but to a different degree.

Since no religion prevails in society, young people are.suscep-
tible to the latest attractions, There is no future. They are being
told this by those advocating unilateral disarmament in the face of
the threat of nuclear annihilation,® and by those Christians who
say “you cannot polish brass on a sinking ship.”*! The sinking boat

29, “A Conversation with Francis Coppola,” U.S. News & World Report, (April
5, 1982), p: 68.

* 30. Ronald J, Sider and Richard K. Taylor, Nuclear Holocaust & Christian Hope
(Downers Grove, IL: InterVarsity Press, 1982).

31. Dominion: A Dangerous New Theology, Tape #1 of Dominion: The Word and New
World Order, Peter Lalonde comments: “It’s a question, ‘Do you polish brass ona
sinking ship?’ And if they're [advocates of dominion theology} working on setting
up new institutions, instead of going out and winning the lost for Christ, then
they're wasting the most valuable time on the planet earth right now.” Souls can
be lost because of pressure from institutions that have abandoned the faith. How
many young people have lost their faith through the humanistic university sys-
tem, a system that was at one time Christian? “If God has decreed that the world’s
future is one of a downward spiral, then indeed Christian reconstruction is futile,
As a prominent premillennial pastor and radio preacher, the Rev, J, Vernon
McGee, declared in the early 1950s, ‘You don’t polish brass on.a sinking ship.’ If
the world is a sinking ship, then efforts to eliminate prostitution, crime, or any
