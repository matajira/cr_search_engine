98 The Reduction of Christianity

This scenario of the “last days” is typical of many of the books
that have come out criticizing the sinister designs of New Age hu-
manism. It seems that this new form of secular humanism’ is the
final satanic conspiracy that will bring on the Great Tribulation,
the rise of Antichrist, and the rapture of the saints.

Is there another explanation for these new humanistic expres-
sions? Is it possible that, although New Age humanism is
demonic, it really is no long-term threat to a healthy church?
Could God, in fact, be using New Age humanism to spur His
people on to kingdom work?

Instead of fearing New Age humanism, Christians should be
working for the advancement of God’s kingdom through the.
preaching of the gospel and the application of God's law to every
area of life. The advances of New Age humanism are the result of
Christians acting as if no good.can be accomplished before Jesus
returns to establish His millennial kingdom. The same could be
said for the advances of the Social Gospel, communism, Islam,
secular humanism, scientism, evolutionism, atheism, and every
other ‘ism” that works to counter the effects of the gospel and
copies the ideals of God’s kingdom. We tend to blame the devil for
our neglect. We should recall that paganism did not advance in
Israel until Israel denied God.

In this chapter, we will show that the threats of New Age human-
ism are real. At the same time, we hope to demonstrate that New Age
humanism is simply a perverse counterfeit of biblical Christianity. New Age
humanism has advanced because the modern church has not been
a diligent teacher of sound biblical doctrine, and at the same time,
the modern church has not been receptive to the primary tenets of
the Christian faith. This has led many Christians to adopt a smor-
gasbord view of religion. The counterfeit nature of Satan’s kingdom
cannot be recognized because few Christians realize the nature of
the genuine kingdom now present and operating in the world.

5. “The New Age and secular humanism are more like cousins than strangers,
and the competition between the two world views is more of an in-house feud
than a dispute between opposites. A better metaphor might he to view the One as
taking the baton from a once robust but now failing secular humanism so that the
race to win Western civilization might be won by a new kind of humanism —cos-

mic humanism.” Douglas Groothuis, Unmasking the New Age (Downers Grove,
IL: Inter-Varsity Press, 1986), p. 52; also pp. 53, 161-63,
