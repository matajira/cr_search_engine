374 The Reduction of Christianity

Ray R. Sutton, Second Chance: Biblical Blueprints for Divorce and
Remarriage. Ft. Worth, Texas: Dominion Press, 1987. Volume 10 of
the Biblical Blueprints Series, Pastor Sutton takes the five-point
biblical covenant model and applies it first to divorce and second
to remarriage. He argues that lawful divorce is always by death:
primarily by covenantal death, and secondarily (where societies
enforce biblical civil law) by execution. Remarriage is based on
covenantal adoption. Because no major Christian counseling ap-
proach has fully understood that the covenant is the basis of mar-
riage, none has seen that the Bible’s rules regarding covenant-
breaking govern divorce and remarriage. This is a revolutionary
book, one which truly does offer a guilt-free second chance to the
innocent victims of covenant-breaking marriage partners.

9. Education

David Chilton (editor), The Biblical Educator, Tyler, Texas: In-
stitute for Christian Economics. This is the assembled collection
of the ICE newsletter, The Biblical Educator, published from 1979 to
1982. It includes essays on education theory, teaching methods,
and other issues related to the war between Christian day school
education and the public school system.

Gary North (editor), Foundations of Christian Scholarship: Essays
in the Van Til Perspective. Vallecito, California: Ross House Books,
1976. This is a compilation of scholarly essays that expose the
myth of neutrality in several academic disciplines: education, psy-
chology, history, mathematics, economics, politics, sociology, and
philosophy. It is written at the level of an upper division college
student. There is no other book quite like it. It is mandatory read-
ing for all college students. The authors include Gary North,
R. J..Rushdoony, Greg Bahnsen, Vern Poythress, William Blake,
Larry Pratt, and John Frame, whose concluding essay on Van Til
is regarded by many of Van Til’s followers as the classic summary
of Van Til’s position.
