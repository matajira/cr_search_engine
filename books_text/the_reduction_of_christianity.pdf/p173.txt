Here a Conspiracy, Therea Conspiracy 133

Paranoia for Jesus

Second, preoccupation with conspiratorial designs leads to
paranoia and immobility, even if the devil is orchestrating the
whole mess.” If you look hard enough, you can see conspiracy
and the devil everywhere, Any idea that connects with some
aspect of “New Age” thinking will immediately label the entire or-
ganization or person as part of the conspiracy. “Well, I heard the
same thing from a known New Ager. He must be part of the con-
spiracy too.”

Some have maintained that “getting your colors done” is a
New Age concept. Now, it may very well be that there are a lot of
New Agers who get their colors done because of some cosmic col-
or scheme that supposedly puts them in tune with the spiritual
forces of nature, but this does not make the practice evil and part
of some New Age conspiracy. God created color, Arranging the
colors of our wardrobe so the look is pleasing to the eye did not
originate with New Age thinkers. Art in all its forms “is Gods
gift" The Christian should not reject art, color-coordinated
clothes, design forms, and beauty because some pagans distort
and pervert their meaning, What do. these people think about
Joseph’s coat of many colors? Were his brothers right in getting
rid of him? Was he a secret New Ager?

20. Even under the Old Covenant, the devil had to ask God's permission to
afflict Job ( Job 1:6-22). Satan could do nothing without God's sanction (2:1-10).
In.the New and “better covenant” (Heb. 7:22; 8:6), we are led to believe that the
devil has more power than he had under the Old Covenant. Supposedly he is in
control of the world because he is described as the “god of this world [lit., agel”
(2 Cor. 4:4). But this is not the proper conclusion to draw. First, the devil is
chosen as a god by “those who are perishing,” and he must blind them before they
will follow him: “The god of this world sar blinded the minds of the unbelieving, that
they might not see the light of the gospel of the glory of Christ, who is the image of
God” (2 Cor. 4:4). The point of the passage is that unbelievers are fooled into be-
Hievinig that Satan is a god. Like idols in general, the devil is “by nature” not a god
(Gal. 4:8; cf. Deut. 32:17; Psalm 96:5; Isa. 44:9-20; 1 Cor, 8:4; 10:20). In Philip-
pians 3:19, Paul tells us that those who are “enemies of the cross of Christ” wor-
ship “their appetite.” Is the appetite a god?

21. Gene Edward Veith, Jr., The Gift of Art: The Place of Arts in Scripture (Downers
Grove, IL: InterVarsity Press, 1983), p. 19.
