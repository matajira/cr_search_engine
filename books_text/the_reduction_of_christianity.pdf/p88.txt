48 The Reduction of Christianity

critics of Hunt's sweeping indictment believe that he failed to raise
the possibility that these men are mistaken, but are not consciously
perpetuating false doctrine. Doug Groothuis, a well-published ex-
pert on the New Age Movement, states that while Hunt’s criticisms
of the Positive Confession Movement are valid his analysis overall
is “[slometimes too heavy-handed.”? In a review of The Seduction of
Christianity Groothuis warns that

the reader should be careful, though, to assess each person sepa-
rately. Some of those cited have strayed far from the truth; others
have committed only minor errors. Unfortunately, the authors
have not drawn careful distinctions.

This is the greatest flaw in Seduction. It is indeed a blast of the
trumpet and lacks the clarity of sharply, individual notes of warning.

Offenders are sometimes lumped together unfairly. For ex-
ample, Hunt and McMahon are critical of Christians who call
for an exercise of dominion over the earth and concern for soci-
ety. They have succumbed to a selfish “we can do it” attitude, ac-
cording to the authors. Many Christians who pursue social re-
newal, however, are doctrinally sound. They look to God, not
self, to turn the world right side up again. The late Francis
Schaeffer was a shining example.”

The Apostle Paul reminded the early church leadership that false
doctrines will find their way into the fellowship of the saints. Even
with the apostles still. preaching and teaching, the early church was
not immune to false doctrine. Paul writes about those who will “fall
away from the faith, paying attention to deceitful spirits and doc-
trines of demons” (1 Tim. 4:1). He even mentions some by name:

This command f entrust to you, Timothy, my son, in accord-
ance with the prophecies previously made concerning you, that

 

and man remained as great as ever.” The Foundations of Social Order: Studies in the
Guat and Councils of the Early Church (Nutley, NJ; Presbyterian and Reformed,
1968), p. 65.

9. ‘Douglas R. Groothuis, Unmasking the New Age (Downers Grove, IL: Inter-
Varsity Press, 1986), p. 192. For a critique of the “Modern Faith Movement” and
its metaphysical connections, see D,. R. McConnell, A Different Gospel: A Historical
and Biblical Analysis of the Modern Faith Movement (Peabody, MA: Hendrickson
Publishers, 1988).

10. Douglas R. Groothuis, “Guarding Pure Doctrine,” a review of The Seduc-
tion of Christianity, in Moady Monthly ( January 1986), pp. 63-5. On the other hand,
see Groothuis's critique of positive confession in Unmasking the New Age, p. 172.
