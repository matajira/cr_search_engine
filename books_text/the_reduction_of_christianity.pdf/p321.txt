Turning the World Right-Side Up 281

pline, of which all have become partakers, then you are illegiti-
mate children and not sons... . All discipline for the moment
seems not to be joyful, but sorrowful; yet to those who have been
trained by it, afterwards it yields the peaceful fruit of righteous-
ness (Heb. 12:6-8, 11).

We look for the discipline of God for we are legitimate chil-
dren, heirs according to the promise (Rom. 8:15-17). If there are
false doctrines, immorality, coldness, and false pride found in the
church, then God will root them out because He loves His church,
David Wilkerson is right in his assessment of the church, but this
does not mean that final judgment is close at hand.-He has misin-
terpreted the seven churches of Revelation 2 and 3, and he has
unwittingly forsaken the mercy, grace, love, and patience of God.

Methods of Change

The secularist trusts in the inherent goodness of man and the in-
evitability of progress that resides in the evolutionary dogma to bring
about change." These two approaches break down into further
variations, Bryan R. Wilson classifies seven types of “salvationists”
in his book Magic and the Millennium:

The conversionist believes that only by changing men can the
world be changed. . . . The revolutionist is convinced that only the
destruction of the world (and usually he means the present social
order) will suffice to save men, . . . A third response is to with-
draw from the world, since it is so hopelessly evil. The introversionist
may do this as an individual or as a member of a community. . . .
[T]he manipulationist’s response . . . consists basically of applying
religious techniques which allow men to see the world differently
and explain evil away. A similar, but narrower type of response is
the thaumaturgicat. Relief from present ills is sought by means of
magic. Such salvation is personal and local, and does not as a
tule call for any elaborate doctrine. Another response, the re-

Sormist, is close to the position of secular social reformers, and in

M1. The Humanist Manifesto 11 triumphantly states: “[N]o deity will save us; we
must save ourselves.”
