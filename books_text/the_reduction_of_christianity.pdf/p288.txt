248 Tha Reduction of Christianity

would be a posterity to be educated. ‘These quotations also show
that the Puritan founders were interested in a specifically Chris-
tian education.”

To counter the theological drift of Harvard, Yale College was
established in 1701, The founders of Yale yearned to return to the
Christian foundation first laid at Harvard: “Yale in the early 1700s
stated as its primary goal that ‘every student shall consider the
main end of his study to wit to know God in Jesus Christ and an-.
swerably to lead a Godly, sober life?”

The colonists understood the relationship between a. sound
education based upon.-biblical absolutes and the future of the na-
tion. Yale College demanded the same rigorous education as Har-
vard: “All Scholars Shall Live Religious, Godly, and Blameless
Lives according to the Rules of God’s Word, diligently Reading
the holy Scriptures the Fountain of Light and Truth; and constantly
attend upon all the Duties of Religion both in Publick and
Secret.”

The influence of these early colleges should not go unnoticed.
Not only were church leaders educated in their classrooms, but
civil rulers gained an understanding of the application of biblical
law te civil affairs.

Puritans also rapidly began publishing concerns to educate
their children for the future. “The first printing press'in the Amer-
ican colonies was set up at Cambridge in 1639, and from it in 1640

10. A curious thing happened to me (Gary DeMar) when I was doing research
for a book project. I wrote to Columbia University and asked them to send a
copy of their original seal. They informed me that they no longer make it avail-
able. Instead, they sent their current seal. It is nothing like the original. The
original Seal of Columbia University, New York, was adopted in 1755. Over the
head of the seated woman is the (Hebrew) Tetragrammaton, YHVH ( jebovak);
the Latin motto around her head means “In Thy light we see light” (Psalm 36:10);
the Hebrew phrase on the ribbon is Uni £1 (‘God is my light), an allusion to
Psalm 27:1; and at the fect of the woman is the New Testament passage com-
manding Christians to desire the pure milk of God’s word (1 Peter 2:1, 2).

11. William C. Ringenberg, The Christian College: A History of Protestant Higher
Education in America (Grand Rapids, MI: Eerdmans, 1984), p. 38.

12. “Yale Laws of 1745,” in Hofstadter and Smith, eds., American Higher Educa-
tion, p. 54.
