394

Lear, Norman, 198-99

Lee, Francis Nigel, 301

Lee, Richard Henry, 136

Leith, John, 239

Leithart, Peter, 193, 264

Lenski, R. C. H., 151, 174

Lerner, Max, 129

Lester, Téd, 341

Leupold, H. C,, 79

Lewis, C. 8., 120-21, 152, 343

Lewis, David A., 63, 293, 65

Lichter, 8. Robert and Linda S., 298

Linder, Robert D., 328

Lindsey, Hal, xiv, xvii, xviii, xoWvi,
xxvii, 1, 14, 39, 52-53, 63, 85,
96-87, 126, 144, 145, 155, 171,
293, 335

Livingston, John H., 6t

Lloyd-Jones, D. Martyn, 5

Lofton, John, 174

Lucretius, 330

Luther, Martin, 37, 61, 64, 236, 237

Machen, J. Gresham, 260, 269

McCabe, Stephen, 51, 93

McClain, Alva J., 39, 172

McDowell, Josh, 74

McGee, J. Vernon, xiv, 106

Mcintire, Carl, 38

MacLaine, Shirley, 92, 105, 104

McLoughlin, William G., 266

McMahon, T. A., xxxvi, 13, 14, 48,
49, 76, 84, 131, 132, 149-50, 155, 173

MacPherson, Dave, 14

Mani, 56-57

Mantaaridis, Georgios L., 75

Marrs, Texe, 97

Marsden, George M., 3, 267-69, 285,
319

Martin, Richie, 22

Marty, Martin E., 263, 264, 301

Marx, Karl, 273, 317

Mather, Cotton, 251-52

The Reduction of Christianity

Means, Pat, 92

Mede, Joseph, 62

Melanchthon, Philip, 6

Miller, Perry, 249, 250

Miller, William, 314

Mondale, Walter, 296

Montgomery, John Warwick, 72

Montgomery, Joseph Allen, 136

Moo, Douglas J., 38

Moody, Dwight L., 39

Moorhead, James H., 257, 267

More, Thomas, 273

Marey, Robert A,, 31, 92, 143, 317

Morison, Samuel Eliot, 326, 327

Mortis, Henry, 125

Morris, Leon, 40, 81, 277

Mosher, Steven W., 302

Mueller, John Theodore, 236

Muntzer, Thomas, 204

Murray, Charles, 321

Murray, lain, xxxviii, 41, 58, 59; 237,
243, 270

Murray, John, 41, 89, 269, 289

Mussolini, Benito, 63

Naisbitt, John, 93, 99, 135, 146

Nash, Ronald, 114

Nathanson, Bernard, 143

Neilson, Lewis, 352

Neuhaus, Richard John, 336

Newton, Isaac, 62, 244

Newton, John, 107

Niebuhr, H. Richard, 53, 252

Nisbet, Robert A., xxiii, 234,
235, 238, 241

Noll, Mark A., 61, 63-64, 255, 302

North, Gary, xx, xxiji, xxiv, xxv,
xxxvii, xxxvili, 20-30 passim, 44-60
passim, 68-95 passim, 100, 110, 112,
132, 147, 184-85, 195, 197-98, 246,
260, 291, 307, 324, 336

Niifiez C., Emilio A,, 114
