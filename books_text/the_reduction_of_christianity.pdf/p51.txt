Orthodoxy: Seiting the Record Straight pr
fession of faith in Christ. There has never been a time in which
church members were not required to say, credo, “I believe.”

There would have been creeds even if there had been no doc-
trinal controversies. In a certain sense it may be said that the
Christian Church has never been without a creed (Ecclesia sine
symbolis nulla). The baptismal formula [Matt. 28:19-20] and the .
wards of institution of the Lord’s Supper [1 Cor. 11:23-34; ef.
15:1-8] are creeds; these and the confession of Peter [Matt. 16:16]
antedate even the birth of the Christian Church on the day of
Pentecost. The Church is, indeed, not founded on symbols, but
on Christ; not on any words of man, but on the word of God; yet
it is founded on Christ as confessed by men, and a creed is man’s
answer to Christ’s question, man’s acceptance and interpretation
of God's word,”

Councils and Creeds

‘The early church encountered doctrinal controversy that was
broader than its battle with apostate Judaism. The Judaizers were
dealt with through letters and councils which clarified doctrinal
controversies for the first-century church (Acts 15:1-35). As the
church extended its boundaries throughout the pagan world, it
faced additional challenges to the faith that had to be answered.
The Pharisees questioned Jesus’ claim that He was the promised
Messiah, Here we find the seeds of controversy that were settled
in a number of very important creedal formulations. How could
God become man? Were the natures of Jesus mixed? Were there
two natures present within the one person?

Christians in A.D. 325 met in what has been called the Ecumen-
ical Council of Nicea to settle the question raised by the Arians: ®

17. Philip Schaff, The Creeds of Christendom: With a History and Critical Notes, 3
vols, (6th ed.; Grand Rapids, MI: Baker [1931] 1983), vol. 1, p. 5.

18. The Arian heresy shows itself in nearly every cult. In fact, you can test a
suspicious religious movement by asking its members what they think of Jesus. Is
He Ged in human flesh, the Second Person of the Godhead (Trinity)? Or is He
“a god” or just a great spiritual teacher? Cornelius Van Til was correct when he
