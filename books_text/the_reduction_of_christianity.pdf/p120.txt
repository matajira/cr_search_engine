80 The Reduction of Christianity

either by quoting similar Scripture passages or by quoting Bible ex-
positors who are well respected in the Christian community. There-
fore, we should not be too quick to look for a novel interpretation,
when so many capable and gifted men throughout the centuries have
understood “gods” to mean civil magistrates who rule in God’s
name. Thomas Scott,** FS, Delitzsch,™ J. J. Stewart Perowne,®
David Dickson,® Joseph Addison Alexander,*” William S, Plumer,®

33. “The rulers of Israel, as immediately. appointed by JEHOVAH to be his
representatives, to judge according to his law, and to be types of his Anointed,
were especially honoured with this high title, ‘Ye are gods.” Scott, The Holy Bible
Containing the Old and New Testaments, According to the Authorized Version; With Ex-
planatory Notes, Practical Observations and Copious References, 3 vols, (New York: Col-
Jins and Hannay, 1832), vol. 2, p. 182.

34, “[T]hey are really elohim [gods] by the grace of God.” C. F. Keil and F. S.
Delitesch, Commentary on the Old Testament: Psaims, 3 vols, (Grand Rapids, MI:
Eerdmans, 1980), vol. 2, p. 404.

35. “He declares that it was He Himself who called them to their office, and
gave them the name, together with the dignity which they enjoy. (This interpre-
tation falls in readily with our Lord’s words in John x. 34.) Perowne, The Book of
Psalms, 2 vols. (Grand Rapids, MI: Zondervan, [1878] 1966), vol. 2, pp. 106-7.

36. “Princes, magistrates, chief rulers, and judges, have allowance from God,
of honour, power, and strength, tribute and revenues, for the better discharge of
their office under him: I have said, Ye are gods, and all of you are children of the most ~
High; that is, I have put the image of my superiority on you, and given you pre-
eminence of place, power, and gifts, over others in my name.” Dickson, Psalms, 2
vols. (London: Banner of Truth Trust [1653-5] 1959), vol. 2, p. 62.

37. “Their sin did not consist in arrogating to themselves too high a dignity,
but in abusing it by malversation, and imagining that it relieved them from
responsibility, whereas it really enhanced it. They were God's representatives,
but for that very reason they were bound to be pre-eminently just and faithful.”
Alexander, The Psalms Translated and Explained (Grand Rapids, MI: Baker [1873]
1975), pp. 350-51.

38, “The office of the magistrate was as dignified and awful [full of awe] as any
of them claimed it to be. They were invested with the character of representatives
of God. Therefore they acted under the highest responsibility, Their name was
dreadful; so was their position; and, if their power was abused, their doom
should be dreadful also.” Plumer, Psalms:,.A Critical and Expository Commentary with
Doctrinal and Practical Remarks (Edinburgh: Banner of Truth Trust, [1867] 1975), p.
782.
