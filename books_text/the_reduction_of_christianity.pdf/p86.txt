46 The Reduction of Christianity

troubles an indication that Jesus will return in “our generation,”*
or are we misusing the events of history to form a strained view of
Bible prophecy? ‘For centuries, various Christian and other
groups have tried to attach dates to these prophecies, with spec-
tacularly little success.”? Will modern prophetic writers suffer a
similar fate?

Hunt's Challenge

Dave Hunt’s books have been helpful in many ways. They ex-
pose dangerous trends in theological thinking. Many of today’s
“new theologies” thrive because there is little familiarity with the
Bible and the centuries of theological debate during which the
basics of orthodoxy were developed. This is most clearly evident,
for example, in the teaching by some that Christians are ‘little
gods.” An experienced cult watcher like Dave Hunt immediately
saw the dangers inherent in such thinking. Dr. Gary North,
whose None Dare Call It Witchcraft (1976)* exposed the festering sore
of New Age humanism in the mid-seventies, points out that today

there is no doubt that some of [the “positive confession” preachers]
have not come to grips with the Bible’s teaching on Christology:
that Jesus Christ in His incarnation was alone fully God and per-
fectly human. Some of them have verbally equated Christian con-
version with becoming divine. This is unquestionably incorrect.
At conversion, the Christian dginitively has imputed to him Christ’s
perfect humanity (not. His divinity), which he then progressively mani-
fests through his earthly lifetime by means of his progressive ethi-
cal sanctification. But their confusion of language is a testimony of
their lack of theological understanding; they mean “Christ's perfect
humanity” when they say “Christ's divinity.” Those who don’t mean
this will eventually drift away from the orthodox faith.>

2, Ed Dobson and Ed Hindson, “Apocalypse Now?: What Fundamentalists
Believe About the End of the World,” Policy Reviaw (Fall 1986), p. 18.

3. Idem.

4, New Rochelle, New York: Arlington House, 1976.

5. Gary North, “The Attack on the ‘New’ Pentecostals,” Christian Reconstruction
(Jan./Feb. 1986), p. 3. Published by the Institute for Christian Economics, P.O,
Box 8000, Tyler, Texas 75711.
