270 The Reduction of Christianity

tained at Westminster and elsewhere, though admittedly as a mi-
nority position.

A resurgence of postmillennialism seems to be traceable to the
influence of Iain Murray and the Banner of Truth Trust, in Great
Britain, and of R. J. Rushdoony, whose first books were pub-
lished in the late 1950s. Rushdoony has written two books specifi-
cally on eschatology, Thy Kingdom Come (1970) and God's Plan for
Victory (1977), and one issue of The Journal of Christian Reconstrution
was devoted to the millennium. Since then, the number of post-
millennial writers has grown rapidly. The most prominent is
David Chilton, who has written three major works on eschato!-
ogy: Paradise Restored (1985), The Days of Vengeance (1987), and The
Great Tribulation (1987). Rushdoony’s and Chilton’s works have
sparked a renewal of optimism among many pastors and teachers,
and even some seminary professors have reexamined the biblical
basis for postmillennialism.

Conclusion

We have seen that the major view of American Christianity in
the 19th century was that the kingdom of God would progressively
triumph on earth. This hope was shattered in the early 20th cen-
tury by a series of theological and social movements that splin-
tered the “kingdom theology” that had already been weakened by
revivalism, nationalism, secularism, and sentimentalism. The
long-range optimism of “reconstructionists,” therefore, is no re-
cent development in this country. Instead, it is the pessimistic
view of the future that is a relative newcomer on the American
theological scene.
