Crying Wolf? 59

Charles Haddon Spurgeon (1834-1892), the great Baptist preacher
and evangelist of the 19th century, shows how pessimism robs the
church of its vitality and stunts its growth.

David was not a believer in the theory that the world wil!
grow worse and worse, and that the dispensation will wind up
with general darkness, and idolatry, Earth’s sun is to go down amid
tenfold night if some of our prophetic brethren are to be believed.
Not so do we expect, but we look for a day when the dwellers in all
lands shall learn righteousness, shall trust in the Saviour, shall wor-
ship thee alone, O God, “and shall glorifp thy name.” The modern
notion has greatly damped the zeal of the church for missions,
and the sooner it is shown to be unscriptural the better for the
cause of God. It neither consorts with prophecy, honours God,
nor inspires the church with ardour. Far hence be it driven.®

For nearly a hundred years, Christians have been in retreat.
Through the adoption of pagan ideas about the world, some
Christians have concluded that matter (this world) is of little value
while spiritual things (heaven) are the only real focus of a Christian's
attention. While Christianity became more and more piétistic™®

 

for believing that the world must grow worse and worse has always been the evi-
dence of abounding moral decay. Confronted by this evidence. it has too often
been supposed that the only work left for God is judgment. Yet the history of re-
vivals should teach us that even in the midst of prevailing evil it is possible to
form precisely the opposite conviction. For example, when John Wesley arrived
in Newcastle-upon-Tyne in May, 1742, he wrote these memorable words; ‘T was
surprised; so much drunkenness, cursing and swearing (even from the mouths of
little children) do I never remember to have seen and heard before in so small a
compass of time. Surely this place is ripe for Him who “came not to call the right-
eous, but sinners to repentance”.’” Murtay, The Puritan Hope, pp. xix-xx.

33. The Treasury of David: An Expository and Devotional Commentary on the Psalras, 7
vols. (Grand Rapids, MI: Guardian Press, [1870-1885] 1976), vol. 4, p. 102.

34. Douglas W. Frank, Less Than Conquerors: How Evangelicals Entered the Twen-
tieth Century (Grand Rapids, ME: Eerdmans, 1986).

35. We must distinguish between “piety” and “pietism.” Originally, a “pious” per-
son was one whose whole life was ordered by his relationship to God. Today, piety
is generally used to describe one’s personal devotional life, such as prayer, Bible
study, fellowship with the Lord, and so forth. In both these senses, piety is essential
to Christian living. By contrast, we are using the term “pietism” to describe the
belief that there is nothing to the Christian life except personal piety. A ‘pietistic®
Christian says that Christians should not become involved with political and social
issues, but should devote themselves entirely to personal devotional practices.
