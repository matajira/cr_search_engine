xviii The Reduction of Christianity

Christ are going to walk upon this earth and see it in perfect con-
dition. Pollution will be passé. Jesus Christ is going to recycle the
late great Planet Earth.”!3 All this “kingdom perfection” during
the millennium is abandoned by Dave Hunt, in his desperate yet
consistent attack on Dominion Theology. He has scrapped tradi-
tional dispensationalism’s last remaining traces of optimism about
history in order to paint a picture of inconceivable despair. Even
God cannot set up a kingdom on earth.

Yet we Christian Reconstructionists are criticized by a minor-
ity of activist dispensationalists for saying that dispensationalism
is inherently a pessimistic worldview. If it isn’t, then why did Dave
Hunt’s books become the best-selling Christian books of the
1980s? Because his traditional dispensational readers apparently agree with
him. They recognize that today’s growing number of dispensa-
tional political and social activists are no longer voicing the origi-
nal theology of dispensationalism, but have adopted Dominion
Theology, an implicitly postmillennial worldview.

Dave Hunt has presented to his traditional dispensationalist
readers a theology of historical despair, a world forever without
any cultural manifestation of the kingdom of God. If this is not a
truly consistent version of dispensational theology, then why are
all the leaders of dispensationalism silent about his books? If Hal
Lindsey rejects Hunt's totally pessimistic cultural conclusions,
then why doesn’t he say so publicly? Why don’t the faculty mem-
bers at Dallas Seminary and Grace Seminary voice their disap-
proval? Do they agree with him or not?

" Power or Ethics?

Here is Hunt's second message: the gospel in history is doomed to
cultural failure. (The first message is that God’s Old Testament law
‘is no longer binding in New Testament times, which is why he is

13. Hal Lindsey, Selan Is Alive and Well on Planet Earth (Grand Rapids, MI:
Zondervan, 1972), p. 113.
