8
THE TIMING OF THE KINGDOM

One of the central issues in Hunt's critique of “dominion” or
“kingdom” theology is the doctrine of the kingdom. What is the
kingdom of God? When was it (or will it be) established? Do we
have to wait for the millennium? Or do we have to wait until afer
the millennium? Does the kingdom affect the earth? Will it exist
on-earth during a future millennium? In this chapter and in the
following one, we will try to answer these questions, We will first
Jook at the question of the timing of the kingdom.

A Future Kingdom

Hunt believes.that the kingdom ‘is primarily a future reality.
Though he does admit that the kingdom “begins in the hearts of
all who obey Christ as King,” he emphasizes that “the outward
manifestation of this kingdom will not come in its fullness until
God has destroyed this present universe and created a new one
into which sin will never enter (2 Peter 3:10-13; Rev. 21:1; etc.).”!
Thus, his emphasis is almost entirely on the future coming of the
kingdom,

Making temporary solutions to social problems the over-
riding concern of Christians blunts the gospel and obscures God's
eternal solution: The focus is turned from heaven to this earth,
from a new universe that only God can create to a new world that

1. Dave Hunt and T..A. McMahon, The Seduction of Christianity: Spiritual
Discernment in the Last Days (Eugene, OR: Harvest House, 1985), p. 224. We
agree that the kingdom will be fully realized only after Christ's return,

149
