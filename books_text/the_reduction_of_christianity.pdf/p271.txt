From the Church Fathers to the Reformation: 231

cording to the British historian J. N. D. Kelly, however, the em-
phasis of the apostolic church was that a decisive victory had
already been won by Christ’s death and resurrection. Thus, “his-
tory had reached its climax and the reign of God [i.e., the kingdom
of God], as so many of our Lord’s parables imply, had been effec-
tively inaugurated.” ‘The hope of the early church “was a twofold
consciousness of blessedness here and now in this time of waiting,
and blessedness yet to come.”6 Kelly notes that this “assurance of
living in the Messianic age” gradually weakened in the second gen-
eration of the church, and a view arose that the kingdom was an ex-
clusively future reality. In spite of this decline in the apostolic view
of the kingdom, “wherever religion was alive and healthy, the
primitive conviction of enjoying already the benefits. of the age to
come was kept vividly before the believer's consciousness.”

Athanasius

This view continued into the following centuries. Athanasius
(c. 305-373), called “the Father of orthodoxy” and the major ortho-
dox theologian during the Nicene controversy, placed central em-
phasis on the significance of Christ's first advent. In fact, one of
the main points of his classic work, On the Incarnation of the. Word,
was that the incarnation, life, death, and resurrection of Christ
had changed the course of human history.

 

tament were partly fulfilled in the first advent of Christ, and will be partly ful-
filled in His Second Coming. “Dialogue with Teypho,” chapter OXVI, in Ante-
Nicene Fathers, vol. 1, p. 257. Thus, while Justin's emphasis is on the future king-
dom, he seems also to claim that the kingdom ‘is present already as well.

6. J..N. D. Kelly, Early Christian Docirines, (vev. ed.; San Francisco, CA: Harper
and Row, 1978), pp. 459-60.

7. Ibid., pp. 460-61. Kelly points out that this view of the present reality of the
kingdom is especially seen in the sacramental theology of the early church. The
sacraments provided Christians with “a foretaste of the blessedness in store for
them” (p. 461). Russian Orthodox theologian Alexander Schmemann agrees with
Kelly’s description of the apostolic view of the kingdom: “In one Man the king-
dom of God~ of love, goodness, and eternal life—has penetrated the realm of sin
and death. Christ did not win this victory for Himself, but for all men—to save
them all and lead chem into that kingdom which He brought into being.” The His-
teal Road of Eastern Orthodoxy (Crestwood, NY: St. Vladimir's Seminary Press,
1977), p. 6.
