302 The Reduction of Christianity

trol of the communist State. The State still dominates the nation.
Families are limited to one child, Forced abortions are a State pol-
icy.* As with the Soviet Union, the State is supreme. The State is
god. The State directs the nation, and, thus, civilization develops
or dies as the statist god mandates.

What of the United States? The United States was at one time
Christian.’ A survey of the religious commitments of the people,
its public declarations, and the evaluation from abroad will give
us at least some indication of what the impetus was behind our
nation’s civilization.

The United States: A Christian Nation

In 1892, the United States Supreme Court in the case of Church
of the Holy Trinity vs. United States,® determined that the United

4, Steven W. Mosher, Broken Earth: The Rural Chinese (New York: Free Press/
Macmillan, 1983).

5. There is a tremendous amount of debate over this assertion. We do not
maintain that everyone was a Christian or that those who professed to be Chris-
tians were consistent in their beliefs, A Christian world view prevailed in the col-
onies and later in the states. In time, however, this Christian base eroded. A
natural law ethic was substituted for revealed religion. The Bible was still the na-
tion’s Book, but so was a “Common Sense” philosophy.

‘We believe that the issue of a “Christian America” must be argued in two
ways. First, the Christian must set forth the case that our founders had no inten-
tion of secularizing the nation. It was not their desire to eradicate religion. The
Hberal and secularized courts, media, civil libertarians, and public (government)
school educators must be confronted with the facts,

Second, Christians must be brought back to reality. Our nation’s founders
were not perfect. Many of them brought a compromised Christianity into govern
ment. The appeal of natural law was alluring to many of them. To equate “Chris-
tian America” with a “perfect America” is a mistake.

For a healthy discussion of the issue see: Jerry S$. Herbert, America, Christian
or Secular? (Portland, OR: Multnomah Press, 1984); Mark A. Noll, et al., The
Search for Christian America (Westchester, IL: Crossway Books, 1983); Robert T.
Handy, A Christian America: Protestant Hopes and Historical Realities (2nd ed.; New
York: Oxford University Press, 1984); Nathan O. Hatch and Mark A. Noll, The
Bible in America (New York: Oxford University Press, 1982). Also see, Gary
DeMar, Ruler of the Nations (Adanta, GA: American Vision, 1987), and Gary
DeMar, “Response to Dr. William Edgar and National Confession,” Geneva Col-
lege Consultation on the Bible and Civil Government, June 2-3, 1987, Available
from American Vision, P.O. Box 720515, Atlanta, Georgia 30328 ($6.00).

6. 143 US 226 (1892),
