282 The Reduction of Christianity

fact differs only in positing divine guidance. The intention is to
amend the world gradually in the light of supernaturally given in-
sights. Lastly, there is the ufopian response in which men seek to
construct a perfect society, free from evil.®

As can be seen, the Christian position is converstonist. People
must change if there is to be any effective change in the broader
society. Any Christian who rejects this fundamental point misses
the substance of the gospel.

Optimism is a rejuvenating emotion. The belief that life can
change spurs us all on. The hopeless and disenfranchised, when
given a ray of hope, can be lifted out of the pit of despair. “A com-
parable restless certainty that however good or bad experience is, it can
be better, routinely infects even the most thoroughgoing secular-
isms.” But only the Christian has the elements of real optimism,
because only the Christian has the life-transforming gospel of the
Lord Jesus Christ to make dead men and women live.

From the Inside Out

The reformation of the world should result from the reforma-
tion of the individual. A look at personal salvation will show the
relationship between the individual and the world. What happens

12. J. FC. Harrison, The Second Coming: Popular Millenarianism: 1780-1850
(New Brunswick, NJ: Rutgers University Press, 1979), pp. 8-9. Emphasis added.
Wilson's discussion of these “ideal-type constructs" can be found in his Magic and
the Millennium: A Sociological Study of Religious Movements of Protest Among Tribal and
Third-World Peoples (New York: Harper & Row, 1973), pp. 22-26.

43. Lionel Tiger, Optimism: The Biology of Hope (New York: Simon and
Schuster, 1979), p. 23. While the author is preoccupied with biological evolution
as the source of man’s optimistic nature (“our huge cerebral cortex [produced] . . .
an ever more complex and imaginative stock of optimistic schemes” [p. 16]), he
cannot get away from an optimism that finds its reality in the God of the Bible.
“Even if the biblical assertion is incorrect that ‘where there is no vision, the people
perish,’ it is difficult to think what could be the engine or stimulus for social be-
havior in a nihilistic system committed only to the certainty of the passage of
time, without any energetic relationship to another principle or purpose” (p. 22).
For Tiger, “optimism is a biological phenomenon; since religion is deeply inter-
twined with optimism, clearly I think religion is a biological phenomenon, rooted
in human genes, which is why it keepa cropping up” (p.. 40).

 
