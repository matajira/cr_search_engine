196 The Reduction of Christianity

a book series called “The Biblical Blueprints Series,” published by
Dominion Press. The advertisement copy reads in part:

For the first time in over 300 years, a growing number of
Christians are starting to view themselves as an army on the
move. This army will grow. This series is designed to help it grow
and grow tougher.

Lalonde responds by saying that “They're very militant about
this. They’re really giving cause to the . . . People for the Ameri-
can Way.” Hunt replies: “Right, right, they literally are, because
they’re: saying, ‘Well, these Christians want to take over the
world,’ Well, indeed they do.”2

Several comments on these views are in order. First, Lalonde
describes this view as “very militant.” The word “militant” con-
jures.up images of armed conflict or “Islamic fundamentalism.”
Yet; though not pacifistic in matters of national defense, “recon-
structionists” regularly condemn revolutionary armed conflict or
direct civil disobedience as the way to extend the kingdom.’ R. J.

 

2, Dominion: A Dangerous New Theology, Tape #1 of Dominion: The Word and New
World Order, distributed by Omega-Letter, Ontario, Ganada, 1987.

3. A word must be said about the legitimacy of certain acts of civil disobedi-
ence. Scripture gives us clear examples, such as Daniel (Dan. 6) and the apostles
(Acts 4:19-20), of men who refused to obcy laws that directly conflicted with
God's Law. Thus, we may disobey the State when obedience to the State would
mean disobedience to God.

The question of legitimate resistance is much more complex, too complex to
be treated fully here. Suffice it to say that we believe, with Calvin.and many Eng-
lish, Dutch, and Scottish Calvinists (see Calvin’s Jnstitutes, 4.20.31), that a subor-
dinate government, such as a state or a colony, may legitimately resist against an
oppressive master. In saying this, we are not advocating ariarchy. The resistance
must be led by a legitimate government, as it was for example in the American
Revolution. Moreover, every legal means of relief must be exhausted before an
oppressed people rebels. Having said all this, we think that rebellions of this kind
are successful only in very rare instances, and we think it is far more important
that Christians resist the humanistic culture through other means.

For reconstructionist opinions on the American Revolution, see Journal of
Christian Reconstruction vol. 3, no. 1 (Summer 1976), a symposium on “Christianity
and the American Revolution.” For a traditional Calvinist view of civil disobedi-
ence and legitimate rebellion, see Samuel Rutherford, Lex, Rex (Harrison, VA:
Sprinkle, [1644] 1982).
