176 The Reduction of Christianity

not mean “to be relieved of all responsibility in the world” or the
like. The. ek here quite obviously implies separation. It could
mean separation from several things. It could mean simply that
the disciples have been chosen out of the whole mass of humanity
to be Christ’s own people. Probably it means that the disciples
have been separated from the world-system that dominates the
unbeliever. Particularly in the book of John, “world” (Greek,
kosmos) often refers to a system and world order under the control
of Satanic forces. It refers to the “world below” in contrast to the
“world above.” The word has ethical significance. It does not
refer simply to the planet earth or to mankind. It refers to the king-
dom of darkness. Thus, to be chosen “out of the world” means to be
separated by the sovereign choice of Christ from the world-system
that is beaded for destruction. It means that the disciples have
been liberated from bondage to Satan.

Finally, the strongest point undergirding our interpretation is
the parallel that Jesus draws between His relation to the world
and the relation of His disciples to the world. Jesus says that the
disciples are not of the world, “just as” He is not of the world.
Now, in what sense was Jesus, during His earthly ministry, not “of
the world”? What does it mean when we say that Jesus is not “of
this world”? Does it mean that He didn’t have ‘any impact on his-
tory? Does it mean that He didn’t have a physical. body? No.
Hunt would certainly not say these things. But if we apply what
Hunt is saying about the kingdom to Jesus, we would have to con-
clude that jesus never left heaven to take human flesh. If “not of the
world” refers to a location, a “geographic position,” then these
verses imply that Jesus was never really incarnate on earth.»

Jesus was not of the world in the sense that He did not derive

14. George Eldon Ladd, A Theology of the New Tistament (Grand Rapids, MI:
Eerdmans, 1974), p. 225. Ladd shows that while John often uses Aosmos in more
general senses, he also uses it to refer to “fallen humanity,” which is “enslaved” to
an “evil power.”

15. We wish to emphasize that this is net what Hunt is saying. We are trying to
show inconsistencies in his interpretation of this text by pressing him to the
logical conclusion.
