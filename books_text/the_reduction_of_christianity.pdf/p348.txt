308 The Reduction of Christianity

as God was sovereign and monarch over all in heaven, so Con-
stantine was sovereign and monarch on earth, Eusebius wrote,
“Thus, as he was the first to proclaim to all the sole sovereignty of
God, so he himself, as sole sovereign of the. Roman world, ex-
tended his authority over the whole human race.”*

In time, the Eastern church “gladly surrendered herself to the
care and protection” of the State. While the State should have a
protective function regarding the church, the church does not
“surrender: herself” to the State, giving up jurisdiction. The
church has its own courts, rulers, and jurisdiction. The Western
church maintained its own courts because of rampant paganism
in the. legal system. Administratively and: institutionally the
Eastern church “merged with the empire to form with it but one
politico-ecclesiastical organism and acknowledged the emperor’s
right to administer her.”

Even when the State is Christian and its courts function on a
Christian base, the church must maintain itself as a complimen-
tary government. The church’s courts should function regardless
of the spiritual condition of the State.” When the courts are
Christian, the church still has jurisdiction over its members. In
fact, the church has primary jurisdiction, When the State courts
are corrupt, the church offers a refuge for those seeking justice.

A Christian civilization means more than converting the State
so that it will follow the dictates of God’s law. All institutions must
be guided by biblical law. Individuals, families, and churches are

 

14. Rushdoony, The One and the Many, p. 149.

15. Alexander Schmemann, Church, World, Mission (Crestwood, NY: Se.
Vladimir's Seminary Press, 1979), p. 37.

16. Iden.

17. ‘When Christianity became the religion of the empire [under Constan-
tine], the church gladly closed down her own courts and gave everything over to
the transformed Christian courts of the state. Thus, the check and balance of
church courts and state courts was lost in the East, and this led in practice to a so-
cial monism that eventually became stifling. In the West, because the church
continued to exist in a pagan environment, the church maintained her own
courts, and these. continually discipled and checked the actions of the state
courts.” James B. Jordan, “Workshop on Church Law and Government,” Sup-
plement to The Geneva Review, Tylet, Texas, February, 1985.
