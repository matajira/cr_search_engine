4
WHAT IS NEW AGE HUMANISM?

Although it is possible that Christians have been seduced by
New Age concepts, yet it is wrong to identify someone as a New
Age humanist simply because he or she uses terminology stolen
by New Age advocates. After all, it’s equally possible that some
Christians who believe in a “kingdom theology” are not being
seduced because they may fully understand that New Age
humanism is man-centered, while “kingdom theology” is Christ-
centered in the most biblical sense. They also know, as we hope to
demonstrate, that New Age humanism is a counterfeit of the
kingdom of God.

What, then, would someone have to believe in order to be
labeled a New Ager? We've chosen four foundational presupposi-
tions of New Age philosophy, but there are many more New Age
concepts that we will not critique.t

One New Age principle that seems to get tremendous atten-
tion in this debate is an optimistic view of our earthly future.
Since this is a crucial topic for Dave Hunt, a number of our later
chapters are devoted exclusively to the subject. Optimism is not,
however, a prerequisite for someone to be a New Ager, although it
is a prominent strain in the movement.? There are plenty of.
pessimists who are part of New Age humanism. Jeremy Rifkin is

1, See Douglas Groothuis, Unmasking the New Age (Downers Grove, IL: Inter-
Varsity Press, 1985), pp. 13-36 for a detailed description of New Age presupposi-
tions.

2. “This reality, this ‘New Consciousness, is hoping to bring about a ‘New
Age’ of hope and human fulfillment.” Ibid., p. 16,

67
