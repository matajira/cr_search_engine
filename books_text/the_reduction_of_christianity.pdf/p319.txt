Turning the World Right-Side Up 279

David Wilkerson and others use the Laodicean church to describe
conditions in the church as it exists now. For them, the rapture is
just around the corner. But other prophetic teachers have taught
the same thing, applying the characteristics of the Laodicean
church to their generation.

Why are there no such saints in Scotland now? Because their
wine is mingled with water — their food is debased. It will nourish
men no longer, but dwarflings.

Oh, Scotland! oh, Scotland! how I groan over thee, thou and
thy children, and thy poverty-stricken Church! Thy Humes are
thy Knoxes, thy Thompsons are thy Melvilles, thy public dinners
are thy sacraments, and the speeches which attend them are the
ministrations of their idol,

And the misfortune is that the scale is falling everywhere in
proportion, ministers and people, cities and lonely places; so that
it is like going into the Shetland Islands, where, though you have
the same plants, they are all dwarfed, and the very animals
dwarfed, and the men also.

« » » How well the state of our Church, nay, of the Christian Church in
general, is described by the account of the Laodicean Church. It almost
fempis me to think . . . that these seven Churches are emblems of the seven
ages of the Christian Church, to the last of which men are now arrived.>

Irving wrote these words in the 1830s, over 150 years ago! Taking
David Wilkerson’s description of today’s church with that of
Irving’s description of the church in his day, we end up with an
impossible situation. Any hope for societal reform is dashed to
pieces since the Laodicean church, as Irving and Wilkerson main-
tain, is an unrepentant church ripe for imminent judgment.
Those expecting an imminent judgment have becn waiting since
Irving’s time. The only thing left for the church to do is to wait.
All hope is lost for earthly transformation.

Trving’s description of the future fueled the fire of prophetic
speculation. Prophetic speculation was rampant in Erving’s day as

9, Edward Irving (1792-1834), cited by Arnold Dallimore, Forérunner of the
Charismatic Movement: The Life of Edward Iroing (Chicago, UL: Moody Press, 1983),
p. 100. Emphasis added.
