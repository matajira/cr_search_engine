60 The Reduction of Christianity

and retreatist, secularism became (because of little opposition
from dominion-oriented Christians) aggressive and dominating.%
At last, Christians are beginning to fight back. This is why Hunt
and many others are upset, This confident and optimistic vision of
the future, according to Hunt, indicates that we are in the final
apostasy. The idea of cultural victory by Christians is anathema to
Dave Hunt, The idea of cultural defeat is pure orthodoxy—the
“old time religion” of 1830.

Is This Really the End?

Hunt concludes that 1 Timothy 4:1 addresses this very situa-
tion: “But the Spirit explicitly says that in /ater times some will fall
away from the faith.” The advocates of the near-end-of-the-world
scenario of future events want to project Paul’s warning into what
would have been the distant future when Paul wrote his epistle.
Little thought is given to the possibility that the “later times” that
Paul had in mind were in the early church’s near future, the end of
the Jewish age just prior to a.p. 70. We use similar language with
little if any confusion. A politician might remark that he will an-
nounce his candidacy at a “later time.” The audience understands
this as “in the near future.” He is biding his time, but not for nine-
teen hundred years.

In fact, there have always been Christians who have been pre-
occupied with the end of the world and the return of Christ. The
sack of Rome by the Vandals (a.p. 410) was supposed to bring on
the end;-the birth of the Inquisition (1209-1244) prompted many
well-meaning saints to conclude that it was the beginning of the
end; the Black Death that killed millions was viewed as the

36. “At the tum of the century, political and conspiratorial elites began a long-
term program to ‘capture the robes’ of American culture. They recognized the
importance of judges, professors, and ministers, I remember hearing a speech by
a former Communist, Karl Prussion, in 1964, in which he told of the assignment
he received from the Party. He became a theology student at Union Theological
Seminary in New York. The Party knew what it was doing.” Gary North,
Backward, Christian Soldiers? (Tyler, TX: Institute for Christian Economics, 1984),
Bp. 60.
