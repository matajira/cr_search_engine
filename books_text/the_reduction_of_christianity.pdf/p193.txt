The Timing of the Kingdom 153

the weakness and corruption of the fleshly existence.*

This interpretation is strengthened by the fact that the natural
body is contrasted throughout this passage with the spiritual body
(vv. 43, 46). For Paul, “spiritual” almost invariably refers to the
Holy Spirit. Certainly it does in this passage. Thus, a “natural” or
“flesh and blood” existence is the living death of men apart from
the-Holy Spirit. When Paul says that “flesh and blood” cannot in-
herit the kingdom, he is simply applying Jesus’ statement in John.
3:5-6 to the final kingdom. Jesus said, “unless one is born of water
and the Spirit, he cannot enter into the kingdom of God. That
which is born of the flesh is flesh, and that which is born of the
Spirit is spirit.” These words appear to be in the background of
Paul's statement in 1 Corinthians 15:50.

It is also possible that Paul uses “flesh and blood” to refer to
natural generation. Thus, he might he saying that men do not in-
herit the kingdom of God because they are born into the “right
family.” People do not inherit the kingdom because they are born
as Jews, or because their parents are Christians. They inherit the
kingdom only by Spiritual generation. John uses flesh and blood

8. A similar interpretation is adopted by Calvin, Commentary on the First Epistle
to the Corinthians, p. 341: “we must understarid flesh and blood to mean flesh and
blood as they are at present constituted; for our flesh will share in the glory of
God, but only after it has been renewed and restored to life by the Spirit of
Christ.” Though we do not agree with everything that he says, F. W. Grosheide,
Commentary on the First Epistle to the Corinthians (Grand Rapids, MI: Eerdmans,
1952), p. 391, does note that “flesh and blood” should be taken figuratively. It des-
ignates man “as he is today in a world that has to bear the consequences of si
The flesh and blood man is one “whose only connection is with this earth.”
Candilish, Life in a Risen Savior, p. 217, notes that “flesh and blood is identified with
corruption. Corruption is its characteristic, Corruption is its distinguishing at-
tribute; not, I again remind you, moral pollution; but if we may so speak, physi-
cal divisibility, liability to be broken into parts, dissolved or resolved into par-
ticles of dust. That is corruption; and that is flesh and blood.” Gordon Fee under-
stands “flesh and blood” in a broader sense: “Most likely it refers simply to the
body in its present form, composed of flesh and blood, to be sure, but subject to
weakness, decay, and death, and as such ill-suited for the life of the future.” The
First Epistle to the Corinthians (Grand Rapids; MI: Eerdmans, 1987), p. 799. All of
these. commentators agree that the emphasis of Paul’s phrase is not merely on the
physical nature of man, but on the corruption and weakness that characterize
our present mode of life.

  
