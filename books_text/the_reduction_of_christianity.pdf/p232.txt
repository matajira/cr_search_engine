192 ‘The Reduction of Christianity

Hunt points out again that the millennium itself will not even
usher in a utopia. “A perfect Edenic environment where all eco-
logical, economic, sociological, and political problems are solved
fails to perfect mankind, So much for the theories of psychology
and sociology and utopian dreams.”# In fact, no one is talking
about utopia. Utopian theories are always based on an environ-
mentalist view of man. Change the environment and. you'll
change men, says the utopian. Hunt disagrees with this outlook,
and so do we. Change must begin in the hearts of men. After that,
men must be disciplined by the Word of God. As they grow and
mature in God’s grace, they will restore the environment around
them, The environment does not change men. Redeemed men
change the environment.®

In another place, Hunt writes,

How could the church be expected to establish the kingdom
by taking over the world when even God cannot accomplish that
without violating man’s freedom of choice? During His
thousand-year reign, Christ will visibly rule the world in perfect
righteousness from Jerusalem and will impose peace upon all na-
tions. Satan will be locked up, robbed of the power to tempt.
Justice will be meted out swiftly.

This is an interesting statement in several ways. First, we do
not believe that the church “establishes the kingdom.” The king-
dom has already been established. The New Testament clearly
teaches that Christ established the kingdom in His life, death, and
resurrection. In one sense, both Christians and non-Christians

44, Ibid., p. 251,

45, We must be careful, of course, not to go to an opposite extreme and con-
clude that the environment has nothing to do with our behavior. In fact, Scrip-
ture teaches that there is a created, built-in relationship between man and the
world, Man is made from dust, so he has affinities with the earth. The many
analogies that the Bible draws between men and plants and animals are all based
on the fact that God has built analogies into His creation. Thus, as every parent
knows, a good environment is important to healthy and sane behavior. But we
are not environmentalists who claim that sin can be eradicated by changing the
environment.

46. Beyond Seduction, p. 250.
