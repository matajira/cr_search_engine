168 The Reduction of Christianity

wheat and tares also implies a progressive development of the
kingdom. This is again a central feature of the parable. The
owner of the field knows there are weeds in his wheat field, but he
delays: the harvest. He lets the wheat and the weeds grow and
mature before he sends his laborers to harvest them (Matt.
13:24-30, 36-43).

What, then, did Jesus say would happen to the kingdom after
its establishment? The parables cited above teach that the king-
dom would grow. It began as a seed in a field, or as leaven in a
loaf. Gradually, almost imperceptibly, it has grown into a tree and
has leavened the whole lump. This same principle of permeation
and growth and extension is found in some of the Old Testament
prophecies of the kingdom. Isaiah says that a child. would be born
aking, an obvious reference to the first advent of Christ. Once His
kingdom is set up, there will be no end to the increase of His gov-
ernment and peace (9:2-7). It’s not just that the Aingdom is ever-
lasting. Its increase is everlasting. In Daniel 2, Nebuchadnezzar
has a dream in which “the God of heaven [sets] up a kingdom
which will never be destroyed” (vv. 44-45). The kingdom is com-
pared to a rock “cut without hands” that becomes “a great moun-
tain” and fills “the whole earth” (vv. 31-34). In the New Testament,
in addition to the parables of Christ, Paul says that the end will
come after “He has put all His enemies under His feet” (1 Cor.
15:24), and that “the last enemy that will be abolished is death”
(1 Cor, 15:26).

In other words, Jesus will return to a world in which nearly all His
enemies have been conquered. The only enemy that will remain is
death. This is the distinctive teaching that characterizes our view
of the future. We believe that Christ’s rule is a victorious and
triumphant reign that will someday, in the present age, through
His church, extend from sea to sea and from the mountains to the
ends of the earth.

43. Of course, sin will never be eradicated from the earth before Christ
returns. There will always be sinners and unbelievers on earth, until the final
coming of Christ: But where sin has abounded, grace will much more abound.
Nor do we mean to imply that the kingdom will advance without hardship and
