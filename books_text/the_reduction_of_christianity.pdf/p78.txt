38 The Reduction of Christianity

Finally, there are numerous variations of these views, Not
every premillennialist-will agree with every other premillennialist.
In fact, not every dispensational premillennialist agrees with all
other premillennial dispensationalists,*” Therefore, any insistence
on making millennial views a test of orthodoxy will only create
greater divisions in the church. Still, these categories help to dis-
tinguish in a general way the different positions that Christians
have taken with respect to the future.

Premillennialism

The “premillennial” view, as the name suggests,. says that
Christ will return physically efore the millennium begins. Christ’s
return will be preceded by “the preaching of the gospel to all na-
tions, a great apostasy, wars, famines, earthquakes, the appear-
ance of the Antichrist and a great tribulation.” Thus, Christ
returns physically to a world in turmoil and sets up His kingdom
on earth for a thousand years. At the end of the millennium, there
will be a final, cataclysmic battle, followed by the final judgment

47. Gleason L. Archer, Jr., Paul D. Feinburg, Douglas J. Moo, and Richard
R. Reiter, The Rapture: Pre-, Mid-, or Post-Tribulational? (Grand Rapids, MI:
Zondervan/Acadamie, 1984).

48, The historic premillennial position was supported by many of the eatly
church fathers, including Justin Martyr, Irenaeus, and Tertullian. There is a dis~
tinction between “historic” premillennialism and “dispensational” premillennial-
ism. Prior to the 20th century nearly all premillennialists were historic premillen-
nialists. Francis A. Schaeffer, Carl F. H. Henry, George Eldon Ladd, Alan John-
son, Carl McIntire, and J. Barton Payne would be classified as historic premil-
lennialists. When dispensationalists claim the Mathers, for example, they often
fail to mention that their 17th-century brand of premillennialism is not the 20th-
century dispensational variety, One of the elements that distinguishes historic
premiliennialism from dispensational premillennialism is the timing of the rap-
ture. For the dispensational premillennialiét, the rapture occurs before a period of
intense persecution of the church known as the Great Tribulation, In effect, Jesus
actually comes two times: for His saints before the Tribulation and then with His
saints after the Tribulation. For the historic premillennialist, Jesus will return in
a single event after a period of interise persecution of the church known as the
Great Tribulation.

49. Robert G. Clouse, ed., The Millennium: Four Views (Downers Grove, TL:
InterVarsity Press, 1977), pp. 7-8.
