24 The Reduction of Christianity

Dominion Theology

Dominion theology is best understood by first looking at the
dominion that God, through Jesus Christ, exercises in the world.
Jesus has dominion because he is “the King of kings, and Lord of
lords” (Rev. 19:16). A synonym for dominion is lordship. The
Bible states in numerous places that dominion belongs. to Jesus:
“Now to Him who is able to keep you from stumbling, and to
make you stand in the presence of His glory blameless with great
joy, to the only God our Savior, through Jesus Christ our Lord, be
glory, majesty, dominion, and authority, before all time and now
and forever. Amen’ (Jude 24-25). Those who hold to a.dominion
theology believe the Bible when it states that the dominion of
Jesus is “before all time and now and forever.” God exercises His do-
minion now. His lordship is over all things, in time and in eternity.

Because Jesus has dominion, His people, who are united to
Him by faith, also have dominion. The Bible says we are adopted
“children of God” and “fellow-heirs with Christ” (Rom. 8:17). As
Christians, created in the image of God and restored in Jesus
Christ, we inherit what was given to Jesus. We therefore share in
His dominion,

But the exercise of this dominion is ethical. It does not come
automatically, nor is it imposed top-down by a political regime or
by an army of Christians working frantically to overthrow the
governments. of the world. Such a concept of dominion is rather
the essence of secular humanism: the religion of revolution.”
God's people exercise dominion in the same way that Jesus exer-
cised dominion — through sacrificial obedience and faithfulness to

15. For an extended discussion of dorninion, s¢e Gary DeMar, God and Govern-
ment: Issues in Biblical Perspective (Adanta, GA: American Vision, 1984), chapter 3.

16. Gary North, Moses and Pharaoh: Dominion Religion Versus Power Religion
(Tyler, TX: Institute for Christian Economics, 1985). Rushdoony writes: “Those
who render unto God the things which are God's, believe rather in regeneration.
through Jesus Christ and the reconstruction of all things in terms of God's law. In
such a perspective, a tax revolt is a futile thing, a dead end, and a departure from
Biblical requirements.” R. J. Rushdoony, “Jesus and the Tax Revolt,” The Journal
of Christian Reconstruction, ed., Gary North, Vol. If, No. 2 (Winter 1975), p. 141.

17. David Chilton, Prodisctive Christians in an Age of Guilt-Manipulators (3rd rev.
ed.; Tyler, TX: Institute for Christian Economics, 1985), pp. 3-16.
