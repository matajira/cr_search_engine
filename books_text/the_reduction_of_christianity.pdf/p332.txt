292 The Reduction of Christianity

know where their next meal is coming from. The terminology of
biblical Christianity is used to draw in the nominally religious and
usually hopeless. But it’s the promise for land, food, housing, and
political power now that motivates many of them to embrace the
Liberationist’s gospel.

The gospel of “Christian fatalism” must compete with the
Marxist “gospel” of immediate social reform. Many of the evan-.
gelical groups doing missionary work in Latin America are
“millennialist,. preaching Christ's imminent return to earth—and
thus favor a passive response to social injustice. ‘I've got nothing
in the world but a mission in the next,’ announces a favorite
song.”26 What does this type of thinking do to multi-generational
thinking? There is no long-range planning. Planning and build-
ing are irrelevant in a world of temporal insignificance. Western
civilization was not built using the world view of “I’ve got nothing
in the world but a mission in the next.” Rather, it was built with
this in mind: “I’ve got something in this world because Ihave a mis-
sion in the next.”

What does a next-world-only gospel do for the impoverished?
It throws them right into the arms of the Marxists. The Marxists
stand by and offer (wrong) answers, but for the poor they seem
better than what they have. The Christian comes with hope, but a
hope that only has meaning when they die. This is not the Chris-
tian message. Eternal life begins now for the believer. The benefits
of heaven are ours now. We live heavenly lives now (Col. 3:1-4).

The anti-Christian mentality that pervades our world is con-
tent to have the church cloistered in its own world of cultural non-
engagement. Christians are tolerated as long as they do not make
waves, that is, as long as they do not engage the world for Christ.
The time has come for Christians to think about what it will take
to build a Christian civilization in the next 200 years. That's right.
We must begin to think multi-generational. While the next elec-

26. “The Protestant Push,” Newsweek (September 1, 1986), p. 64.
