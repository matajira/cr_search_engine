48 The Reduction of Christianity

where dispensational premillennialism is taught. Yet Dave Hunt
has cautiously implied, and his less astute followers have repeat-
edly made, just this sort of erroneous, preposterous association.
Such a conclusion is unfair to Christians who teach “dominion
theology,” and it ignores the possibility that New Agers may in
fact be imitating dominion theology.
