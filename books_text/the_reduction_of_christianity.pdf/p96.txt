56 The Reduction of Christianity

1, The idea that a Christian like Pat Robertson may run for
President is “very frightening”

2. Pat Robertson is “an ultrafundamentalist.” The emphasis
ison: extremism. He's not just a fundamentalist; he’s an
ultrafundamentalist.

3. Pat Robertson is “one of the most radical right-wing lead-
ers in America.” Notice the term “radical.”

4. Pat Robertson is “one of the most powerful public figures in
America today.” 1s power evil?

5. According to Mr. Kirk, “Pat Robertson is beginning to
worry the leaders of beth the Democratic and Republican parties.”

After listing the impact that Pat Robertson. has through his
donor list, television network, and the recently disbanded Free-
dom Council, Mr. Kirk makes this statement: “But his greatest.
threat is not his powerful organization, It is the enormous political
muscle of the Religious Right.” So then, Pat Robertson is not the
only perceived threat. All Christians who hold to certain funda-
mental beliefs are the enemies of the political faith. The real issue
is Christian involvement. Pat Robertson is just a visible target,
someone to raise funds by shooting at. Ifa representative of a per-
ceived monolithic movement can be shot down, then the move-
ment itself is immobilized.

It is not our purpose to endorse Pat Robertson, nor to criticize
his desire to seek the presidency. Neither is it our purpose to judge
Democrats. We are firmly convinced that there are Republicans
who hold similar views. The point we are trying to make is that
Christian involvement is seen as a threat by some very powerful
people. We have to ask why.

The Heresy of the Faithful

The humanists are opportunists. They go after weak points.
One significant weak point that they have exploited is the fling
that many Christians have with Manichaean® and Neo-

27. Mani, a Babylonian philosopher born around a.p. 216, was the founder of
the Manichaean school of philosophy. Mani taught that only the spiritual realm
is good, while material things are inherently evil. There is an eternal struggle
between Good and Evil, which are equally powerful. Man is a mixture of the
