Name Index

Hall, Verna M., 136

Halsell, Grace, 51

Handy, Robert 'T., 258, 262, 302

Hanning, Robert, 234

Harrington, Michael, 320

Harris, Samuel, 258, 262

Harrison, George, 99

Harrison, J. E..C., 62, 282

Hatch, Nathan O., 255, 265, 302

Hatfield, Mark,.95

Hegel, Georg W. F., 317

Heimert, Alan, 255

Hendriksen, William, 28, 40,
125, 178-79, 278

Henry, Carl F. H., 38

Henry, Matthew, 81, 180, 242

Herbert, Jerry S., 302

Hexham, Irving, 35, 36, 130, 144

Hill, Christopher, 202

Hindson, Edward F., 46, 115-16, 323

Hitler, Adolf, 63, 142, 327

Hodge, Archibald Alexander, 164,
208, 212, 223, 225

Hodge, Charles, 41, 259

Hoekema, Anthony A., 40, 125, 156

Hofstadter, Richard, 137, 247, 248

Holmes, Arthur F., 69

Hooker, Thomas, 327

Hooykaas, R., 272

Hopkins, Samuel, 254

Horton, Michael Scott, 31

House, Kirk, 251

Howard, Ted, xxiii

Howard, Thomas, 128

Hoyt, Herman A., 39, 156, 207

Hudson, Winthrop, 262-63, 265

Hughes, Philip E., 16, 119

Hunt, Dave, ix-xxxi, xxxiii-xl, 1-17
passim, 19-20, 23,30, 37, 46-49, 55,
60, 7, 76-96 passim, 106, 114-16,
220- a, 126, 131, 192, 138, 14t,
142-148 passim, 149-158 passim,
171-194, 195-96, 204-05, 206, 220,
221, 229, 293, 333-36, 341-44

  

393

Hutcheson, George, 83
Hutchinson, John A., 301
Huxley, Aldous, 105, 273

Irenacus, 38, 230, 232
Tronside, Harry A., 39
Irving, Edward, 62, 279, 280

Johnson, Alan, 38

Johnson, Paul, 319

Johnson, Rheta G., 54

Jordan, James B., xiv, 5, 30, 37,
41, 53, 90, 91, 194, 195, 257-58,
264, 308

Josephus, 324

Jossclin, Ralph, 202

Justin Martyr, 38, 229-31, 232

Kant, Immanuel, 317

Keil, C. F, 80

Kelly, J. N. D., 7, 231

Kent, Homer, 82

Kidner, Derek, 81

Kik, Marcellus, xxix, 7, 41, 125,
165, 269, 307, 351

King, Edward, 62

Kipling, Rudyard, 73

Kirk, Paul G., 55-56

Kilpatrick, William Kirk, 211

Kissinger, Henry, 63

Klaaren, Eugene M., 272

Kline, Meredith, G., 352

Kroll, Woodrow Michael, 81

Kuyper, Abraham, 35-36

Kuyper, Abraham, Jr.; 212

Ladd, George Eldon, 38, 161,
162, 176, 163

Lalonde, Peter, xv, xvii, 106, 172,
177, 184-85, 193, 195-96, 199, 335

Larson, Bruce, 166

Larson, Gary, 10¢

Latourette, Kenneth Scott, 199-200,
261, 318
