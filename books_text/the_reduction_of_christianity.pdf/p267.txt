The Kingdom is Now! 227

relationships. All families are already under Christ's rule.
Families that rebel against Him will be judged unto the third and
fourth generations. When a family comes under Christ’s gracious
rule, they receive His blessing, and commit themselves to live by
His standards for family life. Wives must submit themselves.to
their husbands, and children must be obedient to their parents
(Eph. 5:22-6:4), Over time, our families should become more
faithful to Christ and more obedient to His commands. In this
way, Christ’s rule is acknowledged and progressively manifested
in our homes. Moreover, as the gospel is preached to all creatures,
More and more families will enjoy the blessings of Christ’s rule.

Christ is still our King when we enter the workplace. He owns
all things and has given us whatever we have. We are His stew-
ards. We must use His resources as He directs, Thus, for exam-
ple, we must avoid debt (Rom. 13:8). As employees and employ-
ers, we must acknowledge His rule and submit to His commands.
Employers are to treat their employees fairly (Eph. 6:9), and em-
ployees are to render good service as to Christ (Eph. 6:5-8;
2 Thess. 3:6-12). Christ blesses any business.or organization that
functions in this way.

Christ is King over all civil officials and civil governments, the
King of kings and the Lord of lords (1 Tim. 6:15; Rev. 11:15;
19:16). Civil officials must acknowledge the lordship of Christ,
and obey His rules for civil governments (Psalm 2:10-12; Rom.
13:1f.). The blessings of the kingdom— peace, stability, and jus-
tice—will come to ali nations that acknowledge the King and en-
force His laws.

Richard B. Gaffin, Jr., in writing of the filling of the Spirit,
makes a comment that captures well the implications of what we
mean by “submitting to the rule of Christ.”

Being filled with the Spirit means marriages that work and are
not poisoned by suspicion and bitterness; homes where parents, chil-
dren, brothers and sisters really enjoy being with each other, free
from jealousy and resentment; and job situations that are not op-
pressive and depersonalizing, but meaningful and truly rewarding.®

28. Richard B, Gaffin, Jr., Perspectives on Pentecost: New Testament Teaching on the
Gifts of the Holy Spirit (Phillipsburg, NJ: Presbyterian and Reformed, 1979), p. 33:
