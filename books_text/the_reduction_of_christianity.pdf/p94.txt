54 The Reduction of Christianity

have not formally rejected their dispensational premillennial
views. Ali they know is that they are tired of getting their heads
kicked in by the humanists, and they are willing to work to change
things, no matter when Jesus returns. Their children are being
propagandized in the public schools, abortion is making them
feel guilty for doing little if anything about the issue in 1973 dur-
ing the infamous Ree a Wade pro-abortion decision, and they
sense the constant ridicule in the press for their deeply held reli-
gious convictions.®

“No More Mr. Nice Guy!”

For these energized Christians, it's no more Mr. Nice Guy.
Jerry Falwell is a good example of someone who had shifted his
emphasis from quietism in 1965 to action beyond the four wails of
the church. In a sermon delivered in 1965, entitled “Ministers and.
Marchers,” Falwell said:

. . » a8 far ag the relationship of the church to the world, [it] can
be expressed as simply as the three words which Paul gave to
Timothy—“Preach the Word.” This message is designed to go
right to the heart of man and there meet his deep spiritual need.
Nowhere are we commissioned to reform the externals. We are

22. Paul C. Vitz, Censorship: Evidence of Bias in our Children’s Textbooks (Ann.
Arbor, MI: Servant Publications, 1986).

23. In Greenville, Tennessee, a group of Christian parents wanted alternative
textbooks for their children. Here’s what a syndicated columnist had to say about
them: “These poor children are being denied the most basic of childhood’s free-
doms, the right to imagine and learn, Someone should remind their parents the
law of this land still requires we educate our children in qualified schools with
qualified teachers. That a sound education involves free exploration of ideas and |
fact. That they may rant and rave against humanism and feminism and any
other ‘ism’ on Sunday, but come Monday the children belong in school.

“It is time for someone to remind [Christians who want to have a say in what
their children learn] that a majority in this country believe in God, but only a
fanatic few feel their beliefs exempt them from laws written by the people in this
democracy.” Rheta Grimsley Johnson, “‘People’ vs. Fundamentalists,” The
Marieita Daily Jounal (September 2, 1986), p. 4A.

24. Stephen Brown, No More Mr. Nice Guy!: Saying Goodbye to “Doormat” Christi-
anity (Nashville, TN: Thomas Nelson, 1986).
