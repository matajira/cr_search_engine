Foreword xv

classic statement about the futility of social reform: “You don’t
polish brass on a sinking ship.” This phrase has become a favorite
jibe against dispensational social pessimism and defeatism among
Christian Reconstructionists. Rushdoony has quoted it for three
decades. It is remarkable that Peter Lalonde, publisher of the
Omega-Letter, repeats it favorably in his taped interview with Dave
Hunt: “Do you polish brass on a sinking ship? And if they're
{Reconstructionists] working on setting up new institutions, in-
stead of going out and winning the lost for Christ, then they're
wasting the most valuable time on the planet earth.*?

Thus, Dave Hunt denies the progressive maturation of Christianity and
Christian-operated social institutions in history (meaning pre-Second
Coming history). The millennium ruled by Christ, Hunt says,
will be a world in which “Justice will be meted out swiftly."8 Jesus
will treat men as fathers treat five-year-old children: instant pun-
ishment, no time for reflection and repentance. Christians today
are given time to think through their actions, to reflect upon their
past sins, and to make restitution before God judges them. Today,
they are treated by God as responsible adults. Not in the millen-
nium! The Church will go from maturity to immaturity when
Ghrist returns in power. And even with the testimony of the per-
fect visible rule of Jesus on earth for a thousand years, Satan will
still thwart Christ and Christ's Church, for at Satan’s release, he
will deceive almost the whole world, leading them to rebel against
“Christ and the saints in Jerusalem.”>

Dave Hunt vs. the Kingdom of God

In short, Hunt argues, the plan of God points only to the de-
feat of His Church in history. He is saying that Satan got the upper
hand in Eden, and even the raw power of Ged during the millen-
nium until the final judgment at the end of history will not wipe

7. Dominion: A Dangerous New Theology, Tape #1 of Dominion: The Word and New
World Order.

8. Dave Hunt, Beyond Seduction: A Retum to Biblical Christianity (Eugene, OR:
Harvest House, 1987), p. 250.

9, Idem,
