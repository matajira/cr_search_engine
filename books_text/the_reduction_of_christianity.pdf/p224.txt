184 The Reduction of Christianity

dience to the gospel, and he is to contribute to the church’s mis-
sion of bringing others into the kingdom of Christ.

R. J. Rushdoony, who could be described as the father of
modern-day dominion theology, clearly spelis this out in The
Philosophy of the Christian School Curriculum:

All too many pastors and teachers assume that the goal of
their work is to save souls for Jesus Christ. This is not the goal: it
is the starting point of their calling. The goal is to train up those
under our authority in God’s word so that they are well-fitted and
thoroughly equipped for all good work, to go forth and to exer-
cise dominion in the name of the Lord and for His kingdom
(Gen. 1:26-28; Joshua 1:1-9; Matt. 28:18-20). We are not saved
just to be saved, but to serve the Lord. We are not the focus of
salvation: the Lord’s calling and kingdom are.2#

In an interview with Hunt, Peter Lalonde, publisher of The
Omega-Letter,*? took a narrow view of the gospel when he responded
to.a statement made by Dr. Gary North about David Wilkerson’s
brand of theology. Gary North wrote of David Wilkerson:

He is clinging to a worn-out view of what the gospel is all
about, a view which did not become widespread in American
Protestant circles until the turn of this century. By shortening
their view of the time Jesus supposedly has given to His people to
accomplish their comprehensive assignment, fundamentalists
after 1900 chose to focus their concerns on preaching and tract-
passing. These are necessary minimal activities, but they are
only the beginning in God’s program of comprehensive redemp-
tion. The dominion covenant requires men to subdue the earth to
the glory of Ged (Gen. 1:28; 9:1-17). His people still must accom-
plish this task before He comes again to judge their success. They
have been given sufficient time; they must redeem it.

31. Rousas John Rushdoony, The Philosophy of the Christian School Curriculum
(Vallecito, CA: Ross House Books, 1981), p. 148.

32. Dominion: A Dangerous New Theology, 1987.

33. North,’“The Attack on the ‘New’ Pentecostals,” Christian Reconstruction,
Vol. X, No. 1 (Jan/Feb. 1986), p. 2.
