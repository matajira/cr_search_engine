152 The Reduction of Christianity

sense, it has no suggestion of sin, but simply emphasizes that man
is man and not God. He is weak and subject to decay.> As Ridder-
bos puts it, “ ‘flesh’ has for [Paul] the significance of what is human
in its weakness, dependence on God, and perishableness in itself.”
Man in his entirety is “flesh and blood.”6 C. S. Lewis’s charac~
terization of heavenly beings as “the Solid People” captured an im-
portant truth, In Lewis’s dream, it is not heaven that is vaporous,
but earth. The earth-bound “Ghosts” could not even walk on the
grass of heaven because it was too solid.” Lewis was not making a
theologically precise statement, but his description isa vivid
reminder that we: will be resurrected with bodies.

In order to understand what Paul meant by flesh and blood in
1 Corinthians 15:50 specifically, we should note that verse 50 is a
summary statement of the previous discussion about different
kinds of bodies. Thus, “flesh and blood” is equivalent to the “natu-
ral body” that Paul describes in verses 42-46. What characterizes
this natural body, this flesh and blood existence? Corruption (v.
42), dishonor (v. 43), weakness (v. 43). These characteristics define
what Paul means by “flesh and blood.” “Flesh and-blood” does not
refer exclusively. to man’s physical nature. All of these things—
corruption, dishonor, weakness—could just as easily describe
man’s soul or mind. Thus, Paul doesn’t mean that men cannot in-
herit the kingdom of God as long as they have bones and sinews
and muscles. He means that they cannot inherit the kingdom in

5, Even Hebrews 2:14 can be understood in this way: Christ took on weak
human flesh. He did not take on the flesh of the uncorrupted Adam, but of the
corruptible sons of Adam. After all, he took His flesh from the fallen nature
of the Virgin Mary. ‘This does not mean, of course, that Christ was morally
corrupt.

6. Herman Ridderbos, Paul: An Oulline of His Theology (Grand Rapids, MI:
Eerdmans, 1975), p. 93. Thus, in Paul's terminology, even the souls and minds of
men are “fleshly” (cf. Rom. 8:6-8; especially the phrase “mind set on the flesh”).
Ridderbos notes, “Just as the Old Testament concept ‘flesh’ (e.g. , Isa. 31:3; Jer.
32:27; Job 10:4), or ‘flesh and blood,’ it denotes in Paul especially the human as
such and taken by itself, as distinguished from and in contrast to the divine.
There is not yet here per se an indication of human sinfulness, but only of human
limitation and weakness . . .” (p. 94).

7. ©. 8. Lewis, The Great Divorce (London: Geoffrey Bles, 1946).
