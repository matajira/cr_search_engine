286 The Reduction of Christianity

The negative reaction to social reform comes from secularized
attempts to do what only the gospel can do. This reaction is legiti-
mate, but it should not deter Christians from being truly evangeli-
cal in their attempts at reform. Why should we abandon an area
of legitimate biblical concern just because humanists have per-
verted our methods and goals? Christians should strive to be a
“light on a hill” to unbelievers.

See, I have taught you statutes and judgments just as the
Lorp my God commanded me, that you should do thus in the
land where you are entering to possess it. So keep and do them,
for that is your wisdom and your understanding in the sight of
the peoples who will hear all these statutes and say, “Surely this
great nation is a wise and understanding people.” For what great
nation is there that has a god so near to it as is the Lorp our God
whenever we call on Him? Or what great nation is there that has
statutes and judgments as righteous as this whole law which I am
setting before you today? (Deut. 4:5-8),

: The statutes and laws that God has given to His people are the
standards of reform. Obedience to the law is the “good works” that
those outside of Christ are to “see” (Matt. 5:16).

Justification, Sanctification, and Regeneration

One helpful way to look at the relationship between personal
renewal and societal renewal and reformation is to study the bibli-
cal doctrines of justification, regeneration, and sanctification.
When we are united to Christ by faith, we receive these blessings.
Christ Himself is our righteousness, our sanctification, and our
new life (1 Cor. 1:30; John 1:4; 5:26; 11:25; 14:6), and in Him we
have righteousness, holiness, and life. ‘Those who believe that so-
cietal transformation is impossible concentrate more on regenera-

 

footnote, Marsden tells us that “Letters on Revivals—No. 23” is “left out of mod-
em editions of these letters” (p. 252, note 5). The reader can draw his own con-
clusions as to why.
