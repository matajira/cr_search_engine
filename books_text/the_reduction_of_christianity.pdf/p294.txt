254 The Reduction of Christianity

At times, Edwards used revolutionary language to describe
this change: “There are many passages in Scripture which do
seem to intend, that as well the civil as the ecclesiastical polities of
the nations, shall be overthrown, and a theocracy ensue.” But he
qualified these statements very carefully.

Not that civil government shall in any measure be over-
thrown, or that the world shall be reduced to an anarchical state;
but the absolute and despotic power of the kings of the earth shall
be taken away, and liberty shall reign throughout the earth.°

While we may disagree with certain details of Edwards's inter-
pretations, he clearly and forcefully taught the earthly victory of
Christ and His people. Edwards's followers held out the same
hope. Samuel Hopkins, in a 1793 “Treatise on the Millennium,”
attempted to prove from the Scriptures that “the church of Christ
is to come to a state of prosperity in this world.” The multitude
of languages would be replaced by a single, international lan-
guage, so that “God will be praised in one tongue, as with one
voice.” In sum,

The church of Christ will then be formed and regulated, ac-

cording to his laws and institutions, in the most beautiful and

" pleasing order, ... There will then be but one universal,

catholic church, comprehending al! the inhabitants of the world,

formed into numerous particular societies and congregations, as

shall be most convenient, to attend on public worship, and the in-
stitutions of Christ.22

Joseph Beliamy, another of Edwards's disciples, taught that a per-
iod of peace and righteousness would be achieved on earth, with:
out any cataclysmic divine intervention.

This renewed optimism fueled the hopes of the generation of

30. Quoted in Davidson, Logic of Millennialion, pp. 220-21.
3L. De Jong, As the Waters Cover the Sea, p. 209.

32. Quoted in ibid,, p. 211.

33. See Davidson, Logic of Millennialism, pp. 221-22.
