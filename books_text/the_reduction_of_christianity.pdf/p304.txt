264 The Reduction of Christianity

ogy and its governmental structure. Revivalism emphasized “in-
dividual decision and personal piety” and “tended to minimize the
importance of social structures and practices."®5. Already in the
first Great Awakening, numerous schisms occurred, fracturing
the church and preventing it from playing a central part in Ameri-
can society. As the church's authority declined and as revivalistic
individualism grew, the authority of the states increased. Histor-
ian Richard Bushman concluded from a study of the Great Awak-
ening in Connecticut that

the civil authority was the sole institution binding society [by the
17608]. The state was the symbol of social coherence, as once the
Established churches had been. Group solidarity depended on
loyalty to the government. United action in the [French and In-
dian} wars of 1745 and 1756 restored a society rent with religious
schisms. . . . Patriotism helped to heal ecclesiastical wounds.

The voluntary societies filled the social gap left by the decline of
the churches, but they were simply not equipped to play the role
that God has ordained for the church.

The decline in the authority and social role of the church thus
provided an opening for the rise of a nationalistic understanding
of the kingdom. In other words, with the churches in decline, the
American nation became for many the chief instrument for the
advancement of the kingdom of Christ. Jonathan Edwards had
suggested that the millennium might begin in America, but. Ed-
wards meant that American churches would be the hub of world

24, See Peter J. Leithart, “Revivalism and American Protestantism,” in
James B. Jordan, ed., The Reconstruction of the Church, Christianity and Civiliza-
tion 4 (Tyler, TX: Geneva Ministries, 1985), pp. 46-84; Leithart, “The Great
Awakening and American Civil Religion,” unpublished paper.

25. Gary Scott Smith, The Seeds of Secularization: Calvinism, Culture, and Plural
ism in America 1870-1915 (Grand Rapids, MI: Eerdmans/Christian University
Press, 1985), pp. 50-51.

26. Richard L. Bushman, From Puritan to Yankee: Character and Social Order in
Connecticut, 1690-1765 (Cambridge, MA: Harvard, 1967), p. 208. Marty suggests
that the disestablishment of the churches was the most basic change in ecclesiasti-
cal administration since Constantine (Righteous Empire, pp. 67-68).
