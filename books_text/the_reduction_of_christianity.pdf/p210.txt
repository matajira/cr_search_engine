170 The Reduction of Christianity

principle and not yet fully consummated.

Conclusion

The Bible teaches that the kingdom of Christ is a present real-
ity. It was established by Christ through the work He performed
in His first advent. It is advancing by His power as He works in
His people by His Spirit. His church will reach a glorious climax,
becoming the chief mountain among the mountains of the earth.
Then, Christ shall return in glory to judge all men and to bring in
the fullness of the new heavens and the new earth.

44, How are we to understand the relationship between the kingdom that is
already present and developing and the kingdom that is yet future? Are they
totally unrelated? It is best to think of the future kingdom as breaking into the
history of the world at the time of Christ. As the commercial used to say, the
fature is now. We now enjoy the first-fruits of the new creation that will be fully
manifested when Christ returns and the dead are raised. Or, as Vos puts it, “our
Lord’s conception was that of one-kingdom coming in two successive stages.”
“The Kingdom of God,” in Redemptive History and Biblical Interpretation: The Shorter
Writings of Geerhardus Vos, ed, , Richard B. Gaffin, Jr. (Phillipsburg, NJ: Presbyter-
ian and Reformed, 1980), p. 309. In a sense, then, time flows backward. It flows
from the future to the present, It flows from the consummation into the present

kingdom.
