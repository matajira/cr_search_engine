Building a Christian Civilization 323

The author raises a number of unsupported points that need.
direct answers. First, we now have the Holy Spirit, who is God,
operating in the hearts of Christians. God is. now in the world.
Second, subjective reasoning as a substitute for an objective
standard is doomed to fail whether now or in the millennium.
What standard will Jesus use in the millennium? Will His law be
different from the Bible? What standard should Christians use
now? If the Bible is good enough to show a sinner how to get to
heaven, can we say that it is not adequate to build a civilization?
(2 Tim. 3:16-17). Third, who proposes that dominion theology
operates “apart from the will of God”? Reconstructionists insist
that the will of God is being denied by those who say the kingdom
of God is not now operating and that it cannot expand as Chris-
tians obey God and get to work to disciple the nations. God’s will
is that His kingdom come, that His will be done on earth as it is in
heaven (Matt. 6:10). Fourth, totalitarianism arises when all
power is invested in an individual, a powerful elite, or a single gov-
ernment like the Church or the State. Christian reconstructionists
hold to a very decentralized view of government. Government for
them is more than the State. Government includes the: family,
Church, and various levels of civil jurisdiction. Rushdoony writes:

[We do not equate government with the state. To do so is to-
talitarianism. Government is first of all the self-government of
man; it is also the family, the church, the school, our vocation,
society and its institutions, and finally, civil government. Our
problem today is that government is equated with the state, an
anti-Christian view.46

45, All the petitions in the Lord’s Prayer refer to earthly present benefits. Some
of the aspects of the kingdom are that God provides our “daily bread,” that He
“forgives our debts,” and that He “delivers us from temptation.” Some want to
maintain that the Lord’s Prayer is a millennial prayer, that we should not pray it
now. Of course, there is no support for thisin Scripture. It is a kingdom prayer,
and we are in the kingdom!

46. Response to Ed Dobson and Ed Hindson, ‘Apocalypse Now?: What Fun-
damentalists believe About the End of the World,” Policy Review, Fall 1986, pp. 6,
17-22, Rushdoony’s response appéars in the Winter 1987 issue, p. 88. See
DeMar, Ruler of the Nations (Atlanta, GA: American Vision, 1987), pp. 3-38 and
God sa Government: A Biblical and Historicat Study (Atlanta, GA: American Vision,
1982).
