ORTHODOXY: SETTING THE
RECORD STRAIGHT

The Reduction of Christianity is a response particularly to two
books written by cult watcher Dave Hunt, The Seduction of Christi-
anity and Beyond Seduction. Mi. Hunt, moreover, has been joined
by David Wilkerson,! Hal Lindsey,? Jimmy Swaggart,? and a
growing list of others in a struggle against what they perceive to
be dangerous and even heretical. tendencies in modern churches.
As we explain more fully throughout this book, they believe, for
example, that Christians who support social and political involve-
ment with any chance of long-term success are leading people
astray. Dave Hunt does make passing reference to the Christian’s
responsibility to be involved in what are typically described as “so-
cial issues."* But in all of his writings and in the writings of those
who support his theological position of impending eschatological
disaster, there is the denial that any of these activities can ever be
successful. In effect, Christians are wasting their time trying to fix
what can never be fixed this side of heaven.

More particularly, we wish to respond to Mr. Hunt's implica-

1, Wilkerson, Set: The Trumpet to Thy Mouth (Lindale, TX: World Challenge
Tne., 1985) and “The Laodicean Lie!” (Lindale, TX: World Challenge Inc., n.d).

2. Hal Lindsey’s criticisms have come from radio and television debates on
the subject.

3, “The Coming Kingdom,” The Evangelist (September 1986), pp. 4-12. Rev.
Swaggart has had Dave Hunt on his daily Bible study program “A Study in the
Word.”

4, Dave Hunt, Beyond Seduction: A Return to Biblical Christianity (Eugene, OR:
Harvest House, 1987), pp. 247-48,
