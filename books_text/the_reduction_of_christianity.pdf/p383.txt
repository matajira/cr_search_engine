Conclusion 343

“obedience” mean for a congressman? What would Congressman
Hunt, for example, do about abortion? Would he work for legisla-
tion to change the existing laws? What if the majority of Ameri-
cans liked abortion? Would Hunt “impose” a law against murder-
ing unborn children on an unwilling populace? What about
homosexuality? Would Congressman Hunt work to prohibit this
perversion that is explicitly condemned in Scripture? What if he
were representing San Francisco? Would he represent his constit-
uency by working for gay rights?

We must emphasize again that we are not obsessed with the
political sphere. Christians should promote good politics because,
to paraphrase C, 5. Lewis, there is bad politics all around us. But
politics is not the answer to our cultural dilemmas. We focus ori
Hunt's view of politics because it illustrates the centrality of eth-
ics, and indicates, we think, his confusion about Christian ethics
in general. Hunt’s comments on law and morality illustrate that
he does not consistently apply his basic, very sound premise about
the place of obedience to the Bible in the Christian’s life.

We admit that there are many complexities and ambiguities in
political life. But we believe that the issues we have referred to
and many others finally come down to simple obedience to the
Lord. We cannot imagine a Christian justification for legalized
abortion on demand, nor for legitimized sodomy. We believe that
Hunt shares our opinions on these issues. The problem is imple-
menting biblical principles in society. Obedience is, for Hunt, an
essential part of the Christian life. Yet, he tends to restrict the
realm of obedience to the personal and individual sphere. Wit-
nessing to other Congressmen would be a fine thing to do. It is the
most important thing Congressman Hunt could do. But we
suspect that Congressman Hunt would also seek to change the
abortion laws. We suspect that he would do what he could to limit
homosexual activity, and to prevent it from being an accepted,
legal “life-style.” Such activity would be consistent with Hunt’s
principle of obedience. But it would be inconsistent with Hunt’s
public statements about morality and politics. We hope that Hunt
would be consistent to his principle.
