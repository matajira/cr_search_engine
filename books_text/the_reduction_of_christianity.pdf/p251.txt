The Kingdom is Now! oti

the strong man so that He could plunder His house (Mark 3:27).
Through the cross, Christ disarmed the demonic powers, and
publicly triumphed over them in His resurrection (Col. 2:15).5
One aspect of Christ’s triumph over Satan can be discovered
by examining the book of Job in the light of the New Testament.
In the first two chapters of Job, we find Satan, the accuser, in the
‘heavenly courtroom of God (cf. Zech. 3:1-2). He is among the
angels who report to the King. He has a position of power and au-
thority. In fact, one of Job’s complaints throughout the book is
that he has no “Advocate,” no one to argue his case before the
Judge. When Christ came, however, He cast Satan from heaven
(Luke 10:18; Rev. 12:7). Thus, instead of having the accusing
Satan in heaven, we now have an Advocate, Jesus Christ the
Righteous One, who argues our case before the Father (1 John
2:1). Satan no longer has authority to accuse us before God.

The Reigning King

Having completed His definitive conquest of Satan, Christ was.
exalted to the right hand of the Father, and given the nations as
His inheritance. This exaltation fulfills the prophecy of Psalm 2:
“Ask of Me, and I will surely give you the nations as Thine inher-
itance and the very ends of the earth as Thy possession” {v. 8).
Daniel also prophesied that when the Son of Man ascended to the
Ancient of Days, He would be given dominion, glory, and rule
over all nations (Dan. 7:13-14), The New Testament everywhere
teaches the same truth. As a result of His suffering and death, He
is “crowned with glory and honor” (Heb. 2:9). After Jesus humbled

5. The picture of Jesus in the gospels is not of a meek teacher of non-violence.
Jesus is not a Gandhi-Christ. To be sure, Jesus is supremely kind and gentle. But
the Jesus pictured in the gospels is much more a warrior than a benign guru.
William Kirk Kilpatrick describes the same things in different terms; “The Gos-
pels . . . give us a picture of a man of powerful passions who wept openly and
threw people around bodily. It is difficult in places to avoid the impression of an
impassioned lover: the kind of man willing to take rash action to win over his
beloved, willing to make public scenes; willing to do almost anything short of ty-
ing her up and dragging her off.” Psychological Seduction (Nashville, TN: Thomas
Nelson, 1983), p. 215.
