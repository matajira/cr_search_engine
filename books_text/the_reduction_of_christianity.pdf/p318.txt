278 ‘The Reduction of Christianity

church and not just seven churches in Asia Minor. For those who
hold to a literal interpretation of Scripture, seven churches would
seem to mean seven churches and not seven ages. Second, each
church is mentioned in a specified geographical area: Ephesus,
Smyrna, Pergamum, Thyatira, Sardis, Philadelphia, and Laodicea.
There is no mention of ages. In fact, John is to write in a book
what he sees, and is to “send it to the seven churches” (1:11). Third,
in the first chapter of Revelation, John tells us “for the timne is near”
(4:3). What he is about to see “must shortly take place” (1:1). The
view that these churches extend over 2000 years of church history
contradicts these very clear passages that whatever is about to
happen will happen in a short time frame. “He who testifies to
these things says, ‘Yes, I am coming quickly” Amen, Come, Lord
Jesus’ (Rev. 22:20). William Hendriksen comments on the seven
churches/seven ages view in his critically acclaimed commentary
on the Book of Revelation.

The notion that these seven churches describe seven suc-
cessive periods of Church history hardly needs refutation. To say
nothing about the almost humorous—if it were not so deplorable
—exegesis which, for example, makes the church at. Sardis,
which was dead, refer to the glorious age of the Reformation; it
should be clear to every student of the Bible that there is not one
atom of evidence in all the sacred writings which in any way cor-
roborates this thoroughly arbitrary method of cutting up the his-
tory of the Church and assigning the resulting pieces to the
respective epistles of Revelation 2 and 3.8

But there is a further problem with the seven ages interpreta-
tion. How do we know when the period of the Laodicean church
begins? Some aspect of the church in nearly every generation can
be described in some measure as “lukewarm” (Rev. 3:16). If it
refers just to a few years prior to the rapture, that’s one thing. But
if it’s made to apply to a long period of time, then the church could
be immobilized for centuries because of prophetic miscalculation,

8. Hendriksen, More Than Conquerors: An Interpretation of the Book of Revelation
(Gtand Rapids, MI: Baker, [1940] 1982), p. 60. See footnote 6 above.
