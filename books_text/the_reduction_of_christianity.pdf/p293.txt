From the American Puritans to the Revolution 253

in the death and resurrection of Christ. The old world is passing
away, and the new world is beginning and growing.

«. . the state of things which is attained by the events of this per-
iod [the death, resurrection of Christ, etc.], is what is so often
called the kingdom of heaven, or the kingdom of God.*

Second, Edwards believed that there had been several decisive
events in the advancement of the kingdom since the time of
Christ. These included the destruction of Jerusalem in a.p. 70,
the reign of Constantine, the rise of the Papacy, and the Reforma-
tion. He expected an even fuller outpouring of the Spirit in the
future, so that “the gospel shall be preached to every tongue, and
kindred, and nation, and people, before the fall of Antichrist; so
we may suppose, that it will be gloriously successful to bring in
multitudes from every nation: and shall spread more and more
with wonderful ‘swiftness.”"* This great outpouring of the Spirit
would be met with vicious opposition. Though Edwards admitted
that “we know not particulatly in what manner this opposition shall
be made,” of one thing he was certain; “Christ and his church shall in
this battle obtain a complete and eiire victory over their enemies.””

As a result, Satan’s kingdom would be fully overthrown. In its
place, Christ’s kingdom would be “set up on the ruins of it, every
where throughout the whole habitable globe." These events would
usher in a new era for the church. The church would no longer be
under affliction, but would enjoy undiluted success. Edwards be-
lieved that “this is most properly the time of the kingdom of heaven
upon earth.” The Old Testament prophecies of the kingdom would be
fulfilled in this era. It would be a time of great Spiritual knowledge,
holiness, peace, love, orderliness in the church. All of this would be
followed by the great apostasy and the second coming of Christ.”

25, Edwards, “History of Redemption,” III.I.IV. In Edward Hickman, ed.,
The Works of Jonathan Edwards, 2 vols, (Edinburgh: Banner of Truth Trust, [1834]
1976), vol. 1, p. 584.

26. Ibid., ‘p. 606.

27. Tod.

28, Ibid., pp. 607-8.

29. ibid.; pp. 609-11.
