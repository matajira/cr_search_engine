238 The Reduction of Christianity

such an extent the kingdom of Ged, which brings with it perfect
righteousness, is not yet come.” Ultimately, “the kingdom of God
. « « [will] be extended to the utmost boundaries of the earth . . .
so as to occupy the whole world from one end to the other.”
Thus, “the worship of God will flourish everywhere” and “his law
{will] be known to all nations, so that his will might be known
everywhere.”

Other reformers heid similar views about the future of the
kingdom of God. The Reformer of Strassburg, Martin Bucer
(1491-1552), taught, according to one scholar, an “eschatology
[that] was less quietistic and more dynamic, leaving more room
for the renewal of this world and for the realization of the will of
God in history, than that of Luther.”

English Puritanism

As heirs of the Calvinistic Reformation, the early English Pur-
itans almost invariably held to an optimistic view of the future of
the church. As Nisbet writes, the Puritans “had a philosophy of
human progress that united past, present, and future into one
seamless web that pointed to. a golden future on earth, one of a
thousand, or perhaps many thousands of years.”™ Puritan theo-
logians taught that the kingdom of God would triumph on earth
before the return of Christ. This view of the future of the kingdom
was held by English Calvinists from the 16th through the early
18th centuries. In his commentary on Revelation, first published
in Latin in 1609, Thomas Brightman wrote that after the conver-
sion of the Jews

shall the end of all prophets come, both wher all the enemies shall
be utterly and at once abolished, and when there shall be one -

27. Quoted in ibid., pp. 71-72.

28. Quoted in ibid., p. 73.

29, Quoted in ibid,, p. 74,

30. Johannes Van Den Berg, Constrained By Jesus’ Love (Kampen: J. H. Kok,
1956), p. 10. Quoted in James R. Payton, Jr., “The Emergence of Postmillen-
nialism in English Puritanism,” Journal of Christian Reconstruction, vol. 6, no. i
(Summer 1979), p. 90.

31. Nisbet, History of the Idea of Progress, p. 115.
