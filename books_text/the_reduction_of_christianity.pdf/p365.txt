Building a Ghristian Civilization $25

Magistrates (not the king only but all the princes of the land)
and judges are to mainiain religion by their commandments (Deut.,1:16;
2 Chron. 1:2; Deut. 16:19; Eocles. 5:8; Hab. 1:4; Mic. 3:9; Zech.
7:9; Hos.-5:10-11), and to take care of religion.

‘The king may not dispose of men as men, as he pleaseth; nor of
laws as he pleaseth; nor governing men, killing or keeping alive,
punishing and rewarding, ashe pleaseth, . ... Therefore, he hath the
trust of life and religion, and hath both tables of the law in his custody.”

This is the very office or official power which the King of
kings hath given to all kings under him, and this is a power of the
royal office of a king, to govern for the Lord his Maker.

When men deny God’s rule, they implement their own. So
then, the question is not, “Theocracy or no theocracy?” but,
rather, “Whose theocracy?” Theocracy is an “inescapable concept.”
The humanists who deny God’s government over all of life work
to implement man’s government over all of life. Since man.sees
himself as god, we may legitimately say that humanism is “theo-
cratic.” The Humanist Manifest II states: “No deity will save us; we.
must save ourselves.” How do humanists hope to save us? Well,
they want humanist laws, humanist schools, humanist courts, a
humanist civil government, and humanist economics. In fact,
they want the world to be humanistic. And who do you suppose
they believe ought to run the world? Humanists, of course.

Remember, theocracy is simply the “rule of God in the world.”
If you believe in the lordship of Jesus Christ then you believe in
theocracy as defined above. This does not mean, however, that
you believe in a Church-State or a State-Church,

A Forgotten Legacy
*The Battle of Britain,” said Winston Churchill on the 18th of
June 1940, “is about to begin. Upon this battle depends the sur-
vival of Christian civilization.”* It would be difficult to learn how

 

49. Ibid, p. 735
50. Ibid., p. 72; ef. p. 232.
51. John Baillie, What is Christian Civilization? (London: Oxford University
Press, 1945), p. 5.
