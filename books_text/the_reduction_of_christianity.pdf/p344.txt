304 The Reduction of Christianity

But all of this doesn’t make a nation or a civilization Christian.
A Christian civilization will have as its foundation the basics of
the Christian faith. The majority of the people will be professing
Christians. They will adhere to their faith in a self-conscious man-
ner and will practice it with little hypocrisy. Those who do not em-
brace the tenets of the Christian religion will still benefit by its
effects on the culture. Tocqueville points out:

It may fairly be believed that a certain number of Americans
pursue a peculiar form of worship from habit. more than from
conviction. In the United States the sovereign authority is reli-
gious, and consequently hypocrisy must be common; but there is
no country in the world where the Christian religion retains a
greater influence over the souls of men than in America; and
there can be no greater proof of its utility and of its conformity to
human nature than that its influence is powerfully felt over the
most enlightened and free nation on the earth.®

Notice that Tocqueville states that “the sovereign authority is
religious.” What did he mean? Religion, and here we mean Chris-
tianity, permeated and pervaded all aspects of the society, though
no one ecclesiastical institution did. Neither the church nor the ~
State was sovereign, but religion, Christianity, was the founda-
tion for both. While a man might not belong to a church or pro-
fess the Christian faith, he would have been considered an outcast
if he did not at least follow the rules laid down by the “sovereign
authority” of religion.

The “sovereign authority” of religion ought to prevail today.
As Christians, we're not looking for a church/state or a state/
church. The: prevailing set of presuppositions, however, should be
Christian.

 

Knopf, (1834, 1840] 1960), vol. 1, p. 308. In a footnote, Tocqueville writes: “The
New York Spectator of August 23, 1831 relates the fact in the following terms: ‘The
Court of Common Pleas of Chester County (New York) a few days since rejected
a witness who declared his disbelief in the existence of God. The presiding judge
remarked, that he had not before been aware that there was a man living who did
not believe in the existence of God; that this belief constituted the sanction of all
testimony in a court of justice; and that he knew of no cause in a Christian coun-
try where a witness had been permitted to testify without such belief’” (p. 306).
8. Ibid., pp. 303-4.
