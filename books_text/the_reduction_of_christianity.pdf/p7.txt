TABLE OF CONTENTS

Foreword by Gary North
Preface by Gary DeMar . .
. Orthodoxy: Setting the Record Straight .
. Let’s Define Our Terms,
. Crying Wolf? ....
. What Is New Age Humanism?.....
New Age Humanism: A Kingdom Counterfeit.
Here a Conspiracy, There a Conspiracy ..
. Guilt by Association. . .
. The Timing of the Kingdom .....
. Dave Hunt’s Heavenly Kingdom . see
10. Myths of Militancy .
11, The Kingdom Is Now!. peeeueee
12. From the Church Fathers to the Reformatio
The Theology of the Kingdom ............+45 229
13. From the American Puritans to the Revolution:
The Theology of the Kingdom .....
14. The Zenith and Decline of Optimism.
15, Turning the World Right-Side Up ..
16. Building a Christian Civilization .
Conclusion ...
APPENDIX A —The Nicene Cree
APPENDIX B—Westminster Confession of Faith
Chapters XXXII and XXXII1........ 346
APPENDIX C—The Athanasian Creed ....
APPENDIX D—This World and the Kingdom.
of God by Greg L, Bahnsen .. 351
A Christian Reconstruction Library .... 359
Scripture Index ...........-++
Name Index « .
Subject Index. . «

 
  
 
 
 
    
  
 
 

WON Dabone

   
  

 

  
 
  
 

 

 
