276 The Reduction of Christianity

nants of promise” (2:12). Gentile believers “have been brought
near by the blood of Christ” (v. 13). There are no longer “two” men
but one “new man” in Christ, a man consisting of Jews and Gen-
tiles (v. 15). God reconciled “them both in one body to God
through the cross, by it having put to death the enmity” (v. 16).

So then you [Gentiles; 2:11] are no longer strangers and
aliens, but you are fellow-citizens with the saints, and are of
God's household, having been built upon the foundation of the
apostles [who were Jews] and prophets [who were Jews], Christ
Jesus Himself [who was a Jew] being the corner stone, in whom
the whole building, being fitted together is growing into a holy
temple in the Lord;.in whom you also are being built together
into a dwelling of God in the Spirit (Eph. 2:19-22),3

Why do some say that restoration cannot come today under a
better covenant? David Wilkerson states that “America Will Not
Repent.”* How does he know? God always leaves room for re-
pentance. Repentance is always offered to a society. Even a cur-
sory study of America’s history will show that America is a cove-
nant nation, Of course, the humanists are trying to deny this, but
the evidence is unmistakable.> God, if He works like He has done
in the past, will leave room for repentance. The church is not per-
fect. Wilkerson identifies many evils within the church that must
be expunged. But this is the repentance process. This is what the
grace of God is all about. We deny the gospel if we say that
America, or any nation, will not repent. God is sovereign. Even
Jonah had doubts about Nineveh. God’s grace proved him wrong.

3. For those who claim that the “church” was not prophesied in the Old Testa-
ment, see Isaiah 57:19, quoted by Paul in Ephesians 2:17, For the dispensational-
ist, church means “Gentile believers.” The Bible describes the “church” as the
“congregation of God,” something that existed in the Old Testament (Acts 7:38).
In the New Testament, the church includes both Jew and Gentile. Gentiles were
“grafted in among them” ( Jewish believers) to become partakers “with them of
the rich root of the olive tree” (Rom. 11:17).

4, Wilkerson, Set.the Trumpet to Thy Mouth (Lindale, TX: World Challenge,
1985), p. 17

5. Gary DeMar, Ruler of the Nations: Biblical Principles for Government (Adanta,
GA: American Vision, 1987), pp. 203-240.
