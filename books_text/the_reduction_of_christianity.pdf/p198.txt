158 The Reduction of Christianity

simply denying the sovereignty of God. There seems to be no
other way to interpret his statement. He does not say that God
does not want to establish His kingdom. He says that God can?
establish His kingdom. This statement reveals the rock bottom of
Hunt's objections to dominion. The issue, it turns out, is not
eschatology, but Hunt’s doctrine of God. Hunt, perhaps uninten-
tionally, says that God is unable to do what He wills to do. This,
we think, is hardly an accurate description of the Almighty God of
Scripture, the God who does as He pleases in heaven and on earth
(Dan, 4;34-35). Such statements do not attribute to our God glory
and strength, as Scripture exhorts us to do,

The Last Days

The main issue, of course, is not whether Hunt is an orthodox
dispensationalist. The issue is whether the New Testament sup-
ports the belief that the kingdom is exclusively a future reality. We
believe that it does not.

‘One source of confusion in this whole area is the biblical use of
the terms “last days” and “latter days.” Hunt and many other dis-
pensationalists believe that this refers to the last days of history,
that is, the very end of the world. Very often, however, this is ob-
viously not the way that the Bible uses this phrase. At Pentecost,
Peter defended the apostles from charges of drunken carousing by
quoting from Joel 2:28-32: “And it shall be in the dast days that I will
pour forth of My Spirit upon all mankind” (Acts 2:17a). When
was this “last days” prophecy fulfilled? Peter said that the events of
Pentecost fulfilled Joel’s prophecy (Acts 2:16).

Similar language is used in the first verses of Hebrews 1: “In _
these last days [God] has spoken to us in His Son” (Heb. 1:2),
Again, we might ask when God spoke to us in His Son. Clearly,
the writer of Hebrews is referring to the first advent of Christ.

20. David Chilton notes: “Contrary to some modern expositions of this text,
Peter did not say that the miracles of Pentecost were dike what Joel prophesied, or
that they were some sort of ‘frote-fulfillments’ of Joel’s prophecy; he said that this
was the fulfillment.” Paradise Restored: A Biblical Theology of Dominion (Ft. Worth,
TX: Dominion Press, 1985), p. 117.
