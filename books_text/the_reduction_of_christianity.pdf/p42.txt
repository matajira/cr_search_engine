2 The Reduction of Christianity

tion that those who teach that Christianity will be victorious in
history and on earth before the rapture are on the verge of apos-
tasy. With this we enter the area of eschatology, the study of the
“last things.” While the church has always believed that Jesus will
come again to judge both the “quick and the dead,” Mr. Hunt and
others tend to make a specific eschatological position a test of
orthodoxy.

In addition, there is the implied association of Christian re-
construction and various strains of “dominion theology” with the
atheistic views of the New Age Movement. As we will demon-
strate, this accusation is clearly false and borders on the absurd.
As we will show in this chapter and subsequent chapters, Chris-
tian reconstructionists have led the way in fighting against secular
humanism and New Age humanism. The writings of Christian
reconstructionists give clear indication that they have had a real
understanding of these movements long before they became an
issue in the broader Christian community, This is why we are
shocked to read in books and periodicals that somehow Christian
reconstructionists are being seduced by the stupidity and silliness
of the New Age Movement.

Moreover, we will address a subtle current in the writings and
interviews of those who criticize the theology of Christian recon-
structionists. With the radical division these. men make between
the Old and New Testaments, law and grace, and Israel and the
Church, there is no objective ethical standard that the world can
‘use to make societal transformation possible. They believe some~
thing like the following:

While there is a personal ethic for the Christian, there is no uni-
aersal ethical standard for the nations, While a Christian can run
for political office, he cannot, for example, bring his biblical
views regarding civil affairs with him into the law-making proc-
ess. The law was for Israel. There is no longer a universal biblical
law that applies to Christians and non-Christians. For Christians,
the law has been internalized.

‘We will spend considerable time refuting this viewpoint.
