114 The Reduction of Christianity

because renaming and redefining are sovereign acts. Like the
counterfeiter who hopes to grow rich through his engraving tech-
niques, New Agers who fill biblical words and concepts with
occultic content do so in hopes of possessing the bounty of God’s
order through magic. Words like “God,” “holistic,” “meditation,”
and “healing” are emptied of their biblical meaning and are then
filled with New Age concepts with the intention of deceiving the
unsuspecting.

The kingdom of Christ is counterfeited to meet the needs of
man. It is Babel revisited. We see this with Nebuchadnezzar’s at-
tempt to counterfeit God’s kingdom by building a golden statue of a
man, In the king’s mind there would be no end to his reign (a gold
statue endures), and man would be the focal point. God had
shown the king in a dream that any kingdom built on the shaky
foundation of man is doomed to failure and judgment (Dan.
2:19-45), On the other hand, God’s kingdom is a “kingdom which
will never be destroyed” (Dan. 2:44). The issue, therefore, is not
whether there is a kingdom; rather, it is whose kingdom will rule
all other kingdoms.

Denying the Real Thing

Dave Hunt assumes that an operating earthly kingdom does
not exist. He does not recognize that New Age humanism is a

 

“Think of the use of labels to categorize political activity. Some labels are used to
neutralize the actions of certain groups; others denote being ‘one of us, acceptable.

“The words Tight wing, ‘fundamentalist,’ ‘pro-life, ‘absolutist,’ and ‘deeply re-
ligious’ are put-downs more than categories. Conversely, think of the unspoken
pat on the back and blessing that the following words convey: ‘moderate,
‘pluralistic, ‘liberal, ‘civil libertarian,’ ‘pragmatic, and ‘enlightened’” Franky
Schaeffer, A Time for Anger: The Myth of Neutrality (Westchester, IL; Crossway
Booke, 1982), p. 15.

This is also the tactic of Liberation Theology and Procesa Theology. See.
Emilio A. Nijfiez C., Liberation Theology, trans. Paul E. Sywulka (Chicago, EL:
Moody Press, 1985); Ronald Nash, ed., On Process Theology (Grand Rapids, MI:
Baker, 1987). .

40. Other critics of Christian reconstruction make similar assumptions. This
ig why some declare that Christian reconstructionists are working at “establishing
the Kingdom of God.” Albert James Dager, “Kingdom Theology: Part IIL,” Media
