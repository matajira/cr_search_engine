Conclusion: 341

biblical ethics leads to an optimistic eschatology: Aside from this
flaw, however, Schnittger is on the right track.

The reason why Hunt and others object to “Christian recon-
struction” is-not merely that they have a different eschatological
position, A more significant underlying reason is that “reconstruc-
tionists” advocate the application of biblical law to every area of
life.*® Hunt himself, to his credit, emphasizes obedience:

Being a Christian does not come about through superficial
belief in the existence of a historical Person named Jesus of
Nazareth who did miracles and taught sublime truths. It involves
personally receiving Him into one’s heart and life as Savior and
Lord and believing that He died for one’s sins and rose. from the
dead. This is the gospel (good news) which, if truly believed, will

. transform one’s life. Genuine faith is based upon understanding
and results in obedience. Acts 6:7 tells us that a “great company of
the priests were obedient to the faith.” Paul preached “ebedience to
the faith among ail nations” (Romans 1:5; 16:26) and warned of
the judgment that would one day come upon-all who “know not

 

cannot choose what we want to teach from Scripture. We must teach everything
that Scripture teaches. These two strains of biblical teaching seem contradictory
ta us, but they are not. We can reconcile these two emphases in a variety of ways,
First, biblical prosperity, as Hunt often points out, is not the same as worldly
prosperity. A Christian can be successful without being considered successful by
the world. Success in the Christian life is not judged by our economic or social
status, but by our holiness, by how pleasing we are to the Lord, Second, we gain
success through suffering, after the pattern of our Lord (cf. Phil. 2). Thus, when
we say that God's people prosper, we are not adopting a worldly perspective. We
are simply trying to live and think by every word that proceeds from the mouth of
the Lord.

18. We cannot enter fully into this discussion here, but allow us to make our
position a bit clearer. We believe that the Bible applies to all of life. We also be-
Hieve that it is impossible to understand any area of life or thought properly apart
from the special revelation of-Scripture. Finally, we believe that the whole Bible
is relevant to us in the New Covenant. As Rev. Ted Lester, pastor of Cherokee
Presbyterian Church, puts it, we are not Old Covenant Christians, nor are we
New Testament Christians; rather, we are New Covenant and whole-Bible
Christians, This does not mean that everything in the Old Testament applies in
the same way as it did under the Old Covenant.’ We do not even profess to under-
stand how. the Old Testament applies in every instance. We do insist that, in every
aphere of individual and corporate'life, the Bible must be the primary authority,
