FOREWORD
by Gary North

I was not aware that I had written “books against Dominion
Theology.” I have made some mention of Dominion Theology in
the final chapter of each of my last two books, but I doubt that it
would require an entire volume to respond to what I have said.

Dave Hunt!

It is a bit perplexing to find how little credit Mr. Hunt wants
to take regarding the origin of the widely circulated accusation
that “Christian reconstructionists” are implicit theological allies of
the New Age Movement. Given the amount of time that at least
one television evangelist devotes Sunday evening after: Sunday
evening to attacking Dominion Theology, and given the fact that
he admitted to me personally that he received this information
originally from Mr. Hunt’s books, this statement by Mr. Hunt
was. unexpected, to say the least. Like an arsonist caught in the
act who insists that he lit only one small match, Mr. Hunt’s reluc-
tance to take full credit seems. somewhat self-interested.

Mr...Hunt is correct in one respect: it does not require an
entire volume to refute what he has said. Refutation is never suffi-
cient; the critic has an obligation to offer-a positive alternative.
Therefore, it does require an entire volume to show that what
Dave Hunt has said rests on a specific view of the Bible, the

 

1. Letter to Gary North, July 20, 1987, in response to an offer to allow Mr.
Hunt to read and respond to the first draft of this book.

ix
