132°. The Reduction of Christianity

chartered goals.

To his credit, Hunt does tell us that “these organizations
{Trilateralists, Masons, Hluminati, and New. Age networks] are
only pawns in the real game. . . . The mastermind behind the
scenes is Satan himself, and the world takeover is his move.”!”
Isn’t it at least possible that the “world takeover” is God’s move?
With what we know about God and His infinite power, and what
we know about the devil and his limited power, what leads Hunt
and others to conclude that there is no hope for the world?
Doesn’t Scripture tell us that “greater is He that is in you than he
who is in the world”? (1 John 4:4). Gary North writes:

Why is it that Satan’s earthly followers, who violate God's
principles for successful living, supposedly will remain in control
of the world until the Rapture? Are we supposed to believe that
Satan’s principles produce personal failure but cultural success,
while biblical principles produce personal success but cultural
failure? Does this make sense to you? It doesn’t to me.®

16. The Coalition on Revival is one such group. Nearly 100 Christian leaders
mect once a year to discuss the content of 17 world view documents. Some Hu-
manists consider this conspiratorial: “The drive for unity has brought a variety of
shepherding streams together under one umbrella organization, the California-
based Coalition on Revival (COR). The groups represented in COR are the
most politically active and, therefore, the most worthy of our attention.” Sara
Diamond, “Shepherding,” CovertAction, Number 27 (Spring 1987), p. 20. Nearly
the entire issue of Covertdction is designed to counter the “conspiratorial” strategies
of Christians groups that seen to be aligning themselves to overthrow pro-humanist
organizations.

17. Hunt and McMahon, Seduction of Christianity, p. 50.

18, The Bible says that Satan is defeated, disarmed, and spoiled (Col. 2:15;
Rev. 12:7ff.; Mark 3:27). He has “fallen” (Luke 10:18) and was “thrown down”
(Rev. 12:9), He was “crushed” under the feet of the early Christians (Rom.
16:20). He has lost “authority” over Christians (Col. 1:13). He has been “judged”
(John 16:11). He cannot “touch” a Christian (1 John 5:18). His works have been
destroyed (1 John 3:8). He has “nothing” ( John 14:30). He “flees” when “resisted”
(James 4:7). He is “bound” (Mark 3:27; Luke 11:20). Surely Satan is alive, but
he is not well on planet earth. Because of the present status of the devil, Scripture
tells us that as Christians actively involve themselves in this world, the gates of hell
“shalt not overpower” the advancing church of the Lord Jesus Christ (Matt. 16:18).

19. “A Letter to Charismatics,” Christian Reconstruction (July/August 1985),
Institute for Christian Economics, P.O. Box 8000, Tyler, Texas, 75711.
