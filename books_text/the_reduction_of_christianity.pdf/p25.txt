Foreword XV

from this world, from the materialization and all of the ambi-
tions, and so forth, to a hope in the heaveniies, in a new creation,
and it ought to motivate us to godliness. But these people are say-
ing “no, the motivation we need is the desire to build, to recon-
struct planet earth, to realize that ecologically we've got prob-
Jems.” I mean we should be concerned about all that. I’m not
denying that, but that’s not our-hope; that’s not the primary goal
of the church: social transformation, But the primary goal is to
save souls, and to bring men to the crass of Jesus Christ, and I
feel—I don’t feel, I'm convinced—that the kingdom-dominion
teaching is playing into the hands of the very lie that turns us
from the cross and from the gospel and the true solution to a hu-
manistic idea, but all done in the name of Jesus Christ, and for
good cause.2*

Even the idea of cleaning up the earth is a socialistic New Age
deception, in Dave Hunt's view. He is quite specific about the link
between the New Age Movement and ecology:

But forgetting that for the moment, people will say, “Well I
mean, you know, whether we are going to be taken to heaven, or
whether the kingdom is on this earth, or, you know, whether we
are going to be raptured, or whether we are not going to be rap-
tured, those are future events. Let’s not worry about that; let’s
unite in our common concern for our fellow man,” and so forth.
That opens the door to a very deceptive lie which literally turns
us from heaven as our hope to this earth, which is at the heart of
the kingdom-dominion teaching, that we—man—was given do-
minion over this earth, and the problem is that he lost the domin-
ion to Satan, and the big thing is that we need to regain the do-
minion. . . . But it opens the door to a marriage with New Age
beliefs, as you know, with humanistic beliefs, so that we will all be
joining together in working for ecological wholeness, working for
peace, working for prosperity, because we are not concerned
about heaven, or the return of Christ, or the Rapture, but we

22. Dominion and the Cross, Tape #2, in Dominion: The Word and New World Order.
