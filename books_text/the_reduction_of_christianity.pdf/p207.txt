The Timing of the Kingdom 167

positions is exactly the same. Both positions are based on a short-
term mentality. It was precisely this kind of perspective that led to
the revolutionary debacles in Munster and Mublhausen during
the sixteenth century.“ We must therefore stress again that the
decisive events of the “end times” are past, 2000 years past. The
kingdom does not grow by revolution, but by grace, obedience,
and faith. There may be dramatic changes in the coming years,
but they will not usher in the kingdom, The kingdom has been
ushered in. It is here. It has been here since Christ’s day.

Progressive
What we are now engaged in is the long-term extension of the
kingdom. And by long-term, we mean long-term: century after cen-
tary of building, block by block. Dominion does not come overnight.
There is no instant dominion. Dominion comes over a period of dec-
~ades and centuries, through self-sacrificing service and obedience.
This progressive aspect of the kingdom is seen most clearly in
Jesus’ parables. In fact, one of the dominant notes of many para-
bles is this progress.of the kingdom. The kingdom of heaven is like
a mustard seed that starts -very small and grows into a huge tree,
providing a resting place for the birds of the air (Matt. 13:31-32).#
The kingdom is also like leaven placed in a loaf that eventually
spreads throughout. the loaf (Matt. 13:33).42 The parable of the

40, See Norman Cohn, The Pursuit of the Millennium, (rev. and ex. ed.; New
York: Oxford University Press, [1957] 1970); Igor Shafarevich, The Socialist
Phenomenon, trans. William Tjalsma (New York: Harper and Row, [1975] 1980);
Christopher Hill, The World Tuned Upside Down (Middlesex, England: Penguin
Books, 1975).

41. The very image of a “seed” to describe the kingdom implies that a process
of growth will occur. The kingdom is not a pebble in the field. I's a seed. Seeds
grow when they're planted.

42, Some have argued that “leaven” in this passage has evil connotations. To
be sure, there are many places in Scripture where leaven is a symbol of invisible
evil influence (see Matt. 16:6, 14; 1 Cor. 5:7-8; Gal. 5:9). But leaven ie not always
a symbol of evil. A cake made with leaven was brought with the fellowship offer-
ing in the Old Testament (Lev. 7:13), and the wave offering was made with
leavened loaves of bread (Lev. 23:17). Thus, the context should determine what
the leaven is to symbolize. In Matthew 13:33, Jesus equates the kingdom of
heaven with leaven, and there is nothing in this context to suggest that the leaven
has an evil connotation.
