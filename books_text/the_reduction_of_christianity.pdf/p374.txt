334 The Reduction of Christianity

positions of these writers have deep roots in the history of the
church, particularly in American church history. Those who ad-
vocate these positions are far from being New Age sympathizers.
Most importantly, we have tried to show that these teachings are
based on the Bible.

The most visible issue between Mr. Hunt and “reconstruction-
ists” is eschatology. “Reconstructionists” are postmillennialists;
that is, they believe that the gospel of Christ will triumph in his-
tory over all idolatries, and that men and societies will be trans-
formed as the gospel penetrates the world. Mr. Hunt is a premil-
lennialist. He believes that Christ will return soon, and that He
will not defeat His enemies in history. Instead, Hunt believes that
Christ will come to rescue His people from destruction, and reign
on earth for a thousand years. Only after the millennium ends (in
failure) will the kingdom be established in any tangible way,
Though Hunt believes that Christ presently reigns in the hearts of
Christians, He will establish the kingdom only in the new heavens
and new earth in which righteousness dwells.

We have written this book to clarify the debate. Mr. Hunt's
books have raised questions in many people’s minds about the or-
thodoxy of some “dominion” teachers. Whether or not Hunt in-
tended to raise such questions, we do not know. Whatever his in-
tentions, his books have had that effect. Other premillennial writ-
ers, David Wilkerson in particular, have been more explicit, call-
ing the “reconstructionist” position on the timing of Christ's return
the “final apostasy.”2

As we have stressed throughout this book, millennia! positions
have never been tests of orthodoxy. Certain doctrines of eschatol-
ogy—the Second Coming, the resurrection of the dead, and the
life everlasting ~ have been included in the creeds, but throughout
the history of the church, various millennial positions have coex-
isted within Christ's church. Christians have always differed on
the timing of Christ’s return, While we’ believe that one’s millen-
nial position is important, and while ‘we should not be indifferent

2. Omega-Letter 2 (April 1987), p. 1.
