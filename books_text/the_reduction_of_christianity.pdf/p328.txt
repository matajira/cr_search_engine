288 The Reduction of Christianity

justification. Without justification, there is no gospel. The justi-
fied sinner is no longer condemned by God: “Who will bring a
charge against God’s elect? God is the one who justifies; who is the
one who condemns?” (Rom. 8:33). This is a crucial point. Nor do
we wish to minimize the importance of regeneration. The point is
that the Christian life is just that,.a life; it is not merely a one-time
event of “getting saved.”

Much of the church today is theologically immature. The basics
of the Christian faith are known, but there is little else in their
storehouse of theological knowledge. There is no progress in god-
liness. In fact, the writer to the Hebrew Christians says an aston-
ishing thing. He tells his readers to leave the “elementary teaching
about the Christ” behind (Heb. 6:1). They are to “press on to ma
turity.” The foundation has been laid. It’s time to build on it.

God does more than justify us. His action is not only judicial
and external. It is also recreative and internal. God gives us new
life in union with Jesus Christ. The Holy Spirit, through regener-
ation, brings the dead sinner to life. Prior to regeneration we
“were dead in . . . trespasses and sins,” but “even when we were
dead in our transgressions, [God] made us. alive together with
Christ” (Eph. 2:1, 5). The results are comprehensive in their effect
on the once-dead sinner: He is a “new creation” (2 Cor, 5:17) and
a “new man” (Eph. 4:24); he has a “new life” (Eph. 2:1-5) and a
“renewed mind” (Rom. 12:2). Regeneration is the “new birth” and
makes growth possible (Eph. 4:15; 1 Peter 2:2; 2 Peter 3:18).

 

The Process of Sanctification

The process by which we are more and more conformed to the
image of Christ is sanctification. In one sense, sanctification is a de-
finitive, once-for-all act of God.® Usually, though, sanctification
is described as a process that accompanies the judicial act of justi-
fication and the life-transforming power of regeneration. Greg
Bahnsen summarizes the relationship between justification and
sanctification for us:

19. John Murray, “Definitive Sanctification,” Collected Writings of John Murray, 4
vols. (Edinburgh: Banner of Truth Trust, 1977), vol. 2, chapters 21-22.
