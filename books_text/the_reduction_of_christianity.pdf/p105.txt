Crying Wolf? 65

A mischievous lad was set to mind some sheep, used, in jest,
to ery “Wolf! Wolf!” When the people at work in the neighboring
fields came running to the spot, ‘he would laugh at them for their
Pains. One day the wolf came in reality, and the boy this time
called “Wolf! Wolft” in earnest; but the men, having been so often
deceived, disregarded his cries, and the sheep were left at the
mercy of the wolf.

: . Ofcourse, if you cry long enough, you,just might be the one to
get it right, but by then there might not be anyone listening.
Preaching about the end of the world has long been used by relig-
ious groups as a-way of pleading with the lost to commit them-
selves to Jesus Christ before He returns, Such a motivating device
can backfire on even the most well-intentioned evangelist, What
happens if a listener shouts out, “Preachers like you have been
telling us for decades that the world is coming to an end. Why
should we believe you now?”

Those who are sure that the end is near should heed the warn-
ing from someone who does believe that Jesus is returning soon:

The date-setters will have a heyday ‘as the year 2000 ap-
proaches. It will be a fever. It will sell pamphlets and books by the
millions. But if Jesus does not come back by the year 2000, it is
hard to imagine any credibility being left for the Bible prophecy
message unless we begin a strong program right now to offset the
heresy of date-setting.

Ignoring it will not make it go away. Only by preaching the
true and dignified message of the Lord’s return and by strongly
denouncing date-setting can we hope to maintain confidence in
the Bible message of Jesus’ return.

50, The New Testament does use the imminent coming of Jesus in judgment
asa way of spurring the church on to greater works, But the imminent judgment
spoken of in Scripture is the destruction of Jerusalem in a.p. 70. Peter writes:
“The end of alll things és at Aand; therefore, be of sound judgment and sober spirit
for the purpose of prayer” (1 Peter 4:7). In Luke’s gospel we read these words of
Jesus: “But keep on the alert at all times, praying in order that you may have
strength to escape all these things that are about to take place, and to stand before the
Son of Man” (Luke 21:36). John writes in his first epistle: “Children, it is the dast
four, and just as you heard that antichrist is coming, even now many antichrists have
arisen; from this we know. that it is the last hour” (1 John 2:18).

51. David Lewis, ‘The Dating Game,” The Pentecostal Evangel, no page or
month, 1975.
