298 The Reduction of Christianity

ing parent-funded education kept to a minimum. School systems
also have a vested interest. Public school systems receive funds
from various governmental jurisdictions to finance their schools.°8
If there are fewer students, then there is less funding. Private edu-
cational institutions threaten the financial base of statist education
because a student in a private school means decreased tax dollars
to the school system. Should Christians expect to have their views
expressed in a fair and impartial way in newspapers, on radio,
and on television? There are additional areas where Christians
have little influence.*

It is time that Christians see every area of life as spiritual and
ripe for reformation and reconstruction, You. can’t change just
some things. Suppose a church sets up a Christian school, and the
state says that it must meet “minimum requirements” established
by the legislature in conjunction with the educational establish-
ment. So the church hires a lawyer to fight the requirements. He
goes before a judge who was appointed by the state’s liberal gov-
ernor. He rules in favor of the state. Let's suppose you want to |
fight abortion. A group from your local church decides to picket
your town’s abortion mill. You're arrested for interfering with the
traffic flow of a “legitimate” business. Again you face lawyers and
judges who are part of the governmental process. The solution?
Christians must become lawyers and judges as well as legislators.
But where will these Christians get their training? Most law
schoois are not very sympathetic to the Christian world view.
There are only a handful of good. Christian colleges and even
fewer Christian law schools. So another facet of our agenda is the
building of Christian colleges, universities, and law schools. But
all this takes time,

33, The average cost in the United States is $3,970 per student per school year
with a graduation rate of only 70.6%. These figures for spending are for the
1986-87 school year; graduation rates are for 19B5. New York spends §6,299 per
student with a graduation rate of only 62.7%. U.S. Naws & World Report (Sept. 7,
1987), p. 67,

34, S. Robert Lichter, Stanley Rothman, and Linda 8, Lichter, The Media
Elite: America’s New Powerbrokers (Bethesda, MD: Adler & Adler, 1986).
