Dave Hunt’s Heavenly Kingdom 177

His authority, His power, His standards of conduct from the
world. In the same sense, Christians are not of the world. In the
same sense, the kingdom of God is not of the world.

Thus, in one sense, Christians are to be separated from the
world. We are not to live by its standards or seek its acclaim or
seek power from below. In another sense, however, we are not to
go “out of the world” (i Cor. 5:9-10). Instead, we are to transform
it as we bring the redeeming message of the gospel to all nations
and as we obediently implement Christ’s dominion over the earth,
Just as Christ came from heaven to earth, so also the kingdom
flows from heaven to earth. As we pray, “Thy will be done on
earth as it is in heaven.”

“Both/and,” not “Either/or”

One of the most prevalent criticisms.of dominion theology is
that its proponents stress man and his relationships on the earth.
Hunt, for example, wants Christians “to make a choice between
earth and heaven.” Now, it is true that where the gospel is con-
cerned the choice is abundantly clear: either Jesus or self, heaven
or earth, forgiveness or judgment, good or evil, life or death. As
far as we can tell, those who hold to a dominion theology agree
wholeheartedly with Hunt's assertion that “every solution to
earth’s problems which is not founded wpon the lordship of Jesus
Christ and the forgiveness of sins we have in Him is temporary at
best and ultimately doomed to fail.”

‘Yet, Hunt has obscured the argument by forcing the Christian
into’a false dilemma. While he has a token interest in the earth,
the force of his arguments leads Christians to believe that any in-
terest in the things of this world is mistaken:

Now... when... your focus turns from heaven to this
earth, you have pretty much aligned yourself with the goals of the
humanists, the New Agers, of various religions, and, of course,
as you mentioned [speaking to Peter Lalonde], each participant

16. Beyond Seduction, pp. 254-55.
17. Bid., p. 254.
