306 The Reduction of Christianity

In the pyramid society the State controls everything. The
ruler is both priest and king. He “is the person who has contact
with the gods.”? In modern pyramid societies the State is god, and
politics is its priesthood.

The Pharaohs were not incorporating a new idea in the devel-
opment of their political philosophy. All those who reject the true
God want to be “like God” (Gen, 3:5). God is the controller of all
things. Rebels against God want to control, to manipulate, and
eventually to enslave. This is the dream of all empire-builders.
Given enough power and authority, these power merchants be-
lieve that all of life can be controlled by man and for man.

Decentralization: The Essence.of Freedom

There is a great danger in following the political model of
Egypt, no matter how good the intentions. Political centralization
creates a society of potentially endless political controls. The Bible
outlines a decentralized social order where power is diffused and
the potential for corruption and tyranny are minimized. Freedom
is enhanced because of the diluted strength of the one by the
maintenance of the many.

The biblical soctal order is utterly hostile to the pyramid society. The
biblical social order is characterized by the following features.
First, it is made up of multiple institutional arrangements, each
with its own legitimate, limited, and derivative sovereignty
under God’s universal law. Second, each institution possesses a hi-
erarchical chain of command, but these chains of command are
essentially appeals courts—“bottom-up” institutions — with the pri-
mary duty of responsible action placed on people occupying the
lower rungs of authority. Third, no single institution has absolute
and final authority in any instance; appeal can be made to other
sovereign agents of godly judgment. Since no society can attain
perfection, there will be instances of injustice, but the social goal
is harmony under biblical law, in terms of an orthodox creed.
God will judge all men perfectly, The State need not seek perfect

9, R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Utimacy (Nutley, NJ: Craig Press, 1971), p. 41,
