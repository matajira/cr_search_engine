New Age Humanism: A Kingdom Counterfeit il

Him, ‘All these things will I give You, if You fall down and wor-
ship me’” (Matt. 4:8-9), Satan wanted Jesus to give up the real
for the counterfeit. Jesus’ finished work of obedience and sacrifice
leads John to write: “The kingdom of this world has become the
kingdom of our Lord, and of His Christ; and He will reign forever
and ever” (Rev, 11:15). The Kingdom belongs to Jesus. It’s His now!
With this fact established, John writes that “He will reign forever
and ever.” Because of Jesus’ obedience, “becoming obedient to the
point of death, even death on a cross, . . . God highly exalted
Him, and bestowed on Him the.name which is above every name,
that at the name ‘of Jesus every knee should bow, of those who are
in heaven, and on earth, and under the earth, and that every
tongue should confess that Jesus Christ is Lord, to the glory of
God the Father” (Phil. 2:8-11).

New Age, New Names: Babel Revisited

God has a present, operating kingdom in the world that Satan
has been trying to duplicate for centuries. Counterfeiting the
kingdom of God has.been going on since the building of Babel.
These kingdom rebels wanted to supplant God’s “name” with a
“name” of their own. In the Bible naming something is a mark of
dominion. God names Himself (Ex. 3:14); thus, man has no

35. In Scripture naming is not arbitrary. Generally, to give a name to some-
thing is to say what something truly is. Names are given to interpret. Where Adam
named the animals, he tells what those animals really are. When God changes the
name of Abram to Abraham, He gives him a name which has redemptive histori-
cal significance, that he will be a father of a multitude. Naming is also an act of
pouser; it is to declare and claim auéhority over the thing that is named. When some-
one would encounter a city, sometimes he would change the name over that city.
For example, Jacob changed the name of Luz to Bethel, “the house of God” .
(Gen. 28:19). The act of man naming the animals is not only man interpreting,
telling what the animals are, but because he is the one who shows who the ani-
mals are; he is showing his sovereignty and displaying his dominion over alll the
lower creatures. Adapted from lecture notes on Ethics by John Frame, Professor
of Ethics and Systematic Theology, Westminster Theological Seminary, Escon-
dido, California.
