Dave Hunt's Heavenly Kingdom 189

as saying that only those who find their ultimate value in the next
world are.much good in this world.’ This is precisely our point:
Those who are citizens of heaven are alone fit instruments for ex-
tending the kingdom in this world.

Peace and Liberty

Hunt claims that the gospel has to do with “peace. with God,”
established between God and the individual sinner. Peace is es-
tablished only through “transformation. of the human heart
through Christ.” He castigates those who say that “the world [is]
seemingly able to solve ‘all its problems without embracing the
true gospel of our Lord Jesus Christ.”**

Who says this? Who says that the world is able to solve any
problem apart from embracing the gospel of Christ? Certainly
“reconstructionists” are not saying this. The whole point, reiter-
ated again and again, is that evangelism is the starting point of social
transformation. The whole point has been that the world can? solve.
any of its problems without embracing the true gospel of Christ.
As far back as 1973, R.J. Rushdoony wrote that “the only true or-
der is founded on Biblical law.” He added,

But the key to remedying the [modern] situation is not revolu-
tion, nor any kind of resistance that works to subvert law and or-
der. The New Testament abounds in warnings against disobedi-
ence and in summons to peace. The key is regeneration [being
born again}, propagation of the gospel, and the conversion of
men and nations to God’s law-word.

In a 1981 article, Rushdoony again emphasized the centrality
of evangelism and regeneration when he wrote that evangelism
“places men under the dominion of the Lord and then orders them
to exercise dominion in and under Him. Having been made a

37, Beyond Seduction, p. 255,,

38. Ibid., pp. 248-9.

39, Rushdoony, The Institutes of Biblical Lino (Phillipsburg, NJ: Presbyterian
and Reformed, 1973), p. 113. See also p. 449: “Clearly, there is no hope for man
except in regeneration.” Also, p. 627: *. . . true reform begins with regeneration
and then. the submission of the believer to the whole law-word of God.”
