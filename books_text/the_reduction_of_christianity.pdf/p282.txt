242 The Reduction of Christianity

In 1641, shortly before the outbreak of the English civil war, one
member of Parliament expressed the hope the Parliament might
“lay the cornerstone to the world’s happiness.” Another contem-
porary expressed his belief that Parliament was “able if need re-
quire to build a new world.”#!

By the 1660s, however, this optimism was waning in England.
De Jong conchides that the restoration of the pro-Catholic Stuart
monarchy to the English throne threw a damper on the expecta-
tions of many Puritans. Yet, the early 18th-century commentator
Matthew Henry retained optimism about the future of Christ's
kingdom. He had this to say about Daniel 2:44-45, where Daniel
interprets the “stone made without hands” that grows into a
mountain:

Tt is a kingdom that shall be victorious over all opposition.
. ++ The kingdom of Christ shall wear ouf all other kingdoms,
shall outlive them, and flourish when they are sunk with their
own weight, and so wasted that their place knows them no more. All
the kingdoms that appear against the kingdom of Christ shall be
broken with a red of izon, as a potter's vessel, Ps. ii. 9. And in the
kingdoms that submit to the kingdom of Christ tyranny, and
idolatry, and every thing that is their reproach, shall, as far as the
gospel of Christ gets ground, be broken. The day is coming when
Jesus Christ shall have put down all rule, principality, and power, and
have made all his enemies his footstool, and then this prophecy will
have its full accomplishment, and not until then, 1 Cor. xv. 24,
25.8

41. Quoted in Lawrence Stone, The Causes of the English Revalution(New York:
Harper and Row, 1972), p. 52. We have the same objections to these sentiments
that we have to Cromwell’s views. See footnote 40, above. These statements, more-
over, place hope in political change as the instrument for realizing the kingdom.
This, we have emphasized earlier, is an idolatrous view of politics, and we repudi-
ate such messianic dreams. Again, our main objective is to document that the Eng-
lish Puritans believed 1) that the kingdom affects the earth, and 2) that the future of
the kingdom will be triumphant. Though they had flaws in their thinking, these
flaws are not at all logically necessary to these two teachings about the kingdom.

42. De Jong, As the Waters Cover the Sea, p. 78.

43. Matthew Henry, Commentary on the Whole Bible: Isaiah to Malachi, 6 vols.
(Old Tappan, NJ: Fleming H. Revell, [1712] n. d.), vol. 4, p. 1032.
