130 The Reduction of Christianity

true “good news”; “And the angel said to them, ‘Do not be afraid;
for behold. I bring you good news of great joy which shall be for
all the people; for today in the city of David there has been born
for you a Savior, who is Christ the Lord’” (Luke 2:10-11). Any
other claim to “good news” is a counterfeit gospel.

Christians must insist that content is what is really important.
In this age of creedless Christianity, sloppy theology, and blind ec-
clesiastical unity, the church is easily thrown onto the mat by the
masters of verbal jujitsu, or persuaded by artful slight of hand.
Some newly initiated dominion thinking Christians are now con-
fused because “dominion,” “kingdom theology,” and “Christian re-
construction” are being linked to the anti-God, man-is-god, we-
are-all-god world view of New Age humanism, The devil couldn’t
be happier. The church, after basking in the light of God’s prom-
ises of victory for the faithful, now seems to be retreating back to
the seemingly comfortable surroundings of their previously occu-
pied caves waiting for Jesus to rapture them home. No such res-
cue will take place. The rapture is a sign of victory for the church,
not defeat. 2

Is There a New Age Conspiracy?

Conspiracy.* The word strikes a note of terror (or excitement)
in the heart of the little guy. What can a few Christians do when

12, No one we know denies the rapture, the ascension of the saints, although
critics often accuse us of such a denial. The question of the rapture is one of tim-
ing. Even dispensationalists disagree on when the rapture will occur. Will it hap-
pen before the tribulation (pre-trib), in the midst of the wibulation (mid-trib), or.
after the tribulation (post-trib)? Postmillennialists hold that the rapture will oc-
cur gfter the millennium. Since it is in the distant future for the postmillennialist,
some have seen this as a denial of its existence.

13. “Normally conspiracy suggests something sinister, But [Marilyn] Ferguson
intends it to mean a breathing together of like-minded individuals in the spirit of
the age, which she contends is the Age of Aquarius, characterized by the ‘sym-
bolic power of the pervasive dream in our popular culture: that after a dark, vio-
lent age, the Piscean, we are entering a millennium of love and light—in the
words of the popular song, "The Age of Aquarius,” the time of “the mind's true
Hiberation.”’” Irving Hexham and Karla Poewe, Understanding Cults and Naw
Religions (Grand Rapids, MI: Eerdmans, 1986), pt 37.
