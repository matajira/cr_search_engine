12 The Reduction of Christianity

Was Jesus really God or was He a creature, albeit the greatest of
God’s created beings? The Nicene Creed® stated emphatically
that Jesus was “very God of very God, begotten, not made, being
of one substance with the Father.” But there were still questions
and disputes. The Council of Constantinople assembled in a.p.
381 to take up the question of Jesus’ complete humanity. At this
council the true, complete humanity of Jesus was maintained over:
against Apollinaris of Laodicea who insisted that Jesus was.God
but denied that He was also man. But the issue of the relationship
between Jesus’ divinity and humanity was still not solved, Nestor-
ianism maintained that the divine and human natures in Christ
constitute two persons. This was condemned by the Creed of the
Council of Ephesus in a.p. 431, The opposite heretical belief was
Eutychianism, which insisted that the divine and human natures
are so united in Christ that they form but one nature. This was
condemned by the Council of Chalcedon, a.v. 451. The conclu-
sion of these debates resulted in the belief that Jesus has two natures
in one person. Orthodoxy was measured by these creedal formula-
tions. The orthodox churches have unified around these essential
beliefs about the person and work. of Jesus Christ for centuries.

 

argued that all heresies in the church have begun with subordinationism: making
Jesus less than God the Father in His very being or essence. Van Til, The Defense
of the Faith (rev. ed.; Philadelphia, PA: Presbyterian and Reformed, 1963), p. 25.

The Jehovah's Witnesses have made the Arian heresy famous with their belief
‘that Jesus is “a god” based on a very strained interpretation of John 1:1 and vari-
ous other verses, To support this conclusion they create a Greek verb tense, the
“perfect indefinite” tense, to deaden the effect of Jesus’ comments to the Pharisees
when He told them: “Truly, truly, I say to you, before Abraham was born, I AM”
(John 8:58), an obvious reference to His divinity (Ex. 3:14). “I AM” becomes “I
have been.” You will find this “Scripture twisting” in the 1950 edition of their New
World Translation of the Christian Greek Scriptures, now out of print and nearly impos-
sible to locate.

Colossians 1:16-20 states very clearly that Jesus created “all things.” But if
Jesus is a creature (a “thing”), how can Scripture say that He created all things?
Very simple. The Jehovah's Witnesses’ own New World Translation inserts the word
“other” in brackets before the word “things.” So now they have Jesus creating “all
[other] things” since as a created being He too would be a “thing.”

19. See Appendix A.
