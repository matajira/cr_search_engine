Guilt by Association 147

Hunt a follower of the humanistic psychology advocated by Dr.
Szasz? Of course not,

Because they counterfeit the Christian faith, humanists often
have some good things to say. In fact, humanism has made the
major intellectual and scientific advances in recent decades
because Christians have failed to understand that the Bible ap-
plies to every area of life, Humanists believe that their world view
is comprehensive enough to include the world. They have been
frantically working, with little opposition from Christians, to im-
plement their crumbling world view in places where Christians
have pulled up stakes and left culturally barren ground. Where do
these anti-Christian thinkers and writers get their often valuable
insights? They are “stolen from the Bible when they are correct.
When men come to conclusions that are also the conclusions of
the Bible, we should use their discoveries. These discoveries are
our property, not theirs. God owns the world; the devil owns
nothing. We are’ God’s adopted children; they are God’s disin-
herited children.”"*

Conclusion.

It is true that “dominion theologians” use some of the same
terms that New Agers use, In certain areas, the ideas may even be
similar. The same can be said of premillennial, pretribulational
dispensationalists. But these facts do not prove that “dominion
theologians” have been seduced by the New Age Movement, nor
does it mean that Jeremy Rifkin has seduced the faculty of schools

14. Gary, North, Moses and Pharaoh; Dominion Retigion Versus Power Religion
(Tyler, TX: Institute for Christian Economics, 1985), p. x. Dr. North cautions us
with these words: “The inost important thing is how well I integrate such human-
istic insights into my biblical reconstruction of economics [the topic of his book],
without 1) losing the importance of these insights or 2) becoming a slave of the
humanist presuppositions which officially undergird such insights. But this is the
most important task in any field. Every Christian faces this problem, We buy and
sell with pagans in many marketplaces, and one of these marketplaces is the mar-
ketplace for ideas. We.must use their best ideas against them, and we must ex-
pose their worst ideas in order to undermine men’s confidence in them. In short,
in God's universe, it is a question of ‘heads, we win; tails, they lose’” (p. xi).
