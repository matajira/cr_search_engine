268 The Reduction of Christianity

Because the social gospel was also identified with an optimistic
view of the kingdom, this too began to seem “liberal” to many or-
thodox Christians.

Theological changes on the American scene also contributed
to the decline of a confidence in earthly victory for the church.
One of these was the rise of dispensational premillennialism. As
Marsden notes, the first stage of the “Great Reversal” was marked
by a “change from postmillennial to premillennial views of the re-
lation of the kingdom to the present social and political order.”39
By 1875, dispensationalism, first articulated by the English theo-
logian John Darby, began to gain ground in the United States.

Social changes contributed to the decline of confidence in
Christ’s earthly victory. Industrialization. and modernization
created social dislocations, People used to rural life moved to the
city, where life moved faster and morality was looser, Moreover,
the savagery of the late 19th and early 20th century led many to
adopt a more pessimistic view of the future. The Civil War, the
First World War, the revolution in Russia—all these contributed
to a changing mood.#

Evangelicals did not retreat all at once. In fact, as Marsden
shows, premillennial evangelicals were very active in the period
after World War'I. Yet, each subsequent tragedy forced evangeli-
cals further and further into the protective walls of their own com-
munities and of a private faith. Each tragedy reminded Ameri-
cans of man’s sinful nature. Thus, each of these setbacks contrib-
uted to a climate of pessimism.

Finally, a major blow to the credibility of fundamentalism came
with the Scopes trial of 1925. In the wake of this fiasco, Marsden

39. Ibid., p. 86.

40. See Timothy P. Weber, Living in the Shadow of the Second Coming, (2nd ed.;
Grand Rapids, MI: Academie Books/Zondervan, 1983), pp. 164.

41. On the civil war’s impact, see Douglas W. Frank, Less Than Conquerors:
How Evangelicals Entered the Twentieth Century (Grand Rapids, MI: Eerdmans,
1986), pp. 66, 138. On World War I, see Marsden, Fundamentalism and American
Culture, chapter XVI.

42. Marsden, Fundamentalism and American Culture, pp. 141-153.
