10
MYTHS OF MILITANCY

One of the major distortions of postmiliennial and “recon-
structionst” teaching is that this position leads to revolutionary
militancy. It is true that the rhetoric of some Christian “recon-
structionist” writers is confrontational and militant, in some cases
overly so, But it is misleading to equate militant language with.
advocating revolution. Jesus used militant language in condemn-
ing the Pharisees, but He was certainly no advocate of revolution.

Our position is that Christians should follow the examples of
biblical characters such as Joseph, Daniel, and Jesus Christ Him-

- self. Joseph and Daniel both exercised enormous influence within
the world’s greatest empires. But they attained their positions by
hard work, perseverence in persecution and suffering, and faithful
obedience. Jesus Christ attained to His throne only by enduring
the suffering of the cross. Christians are no different. We are not
to attain positions of leadership by revolution or rebellion. In-
stead, we are to work at our callings and wait on the Lord to
place us in positions of influence, in His time.

Bringing Persecution on Ourselves

Dave Hunt and Peter Lalonde perpetuate the myth that post-
milicnnialism is militant. In the Omega-Letter’s taped interview
with Dave Hunt, Peter Lalonde responds to an advertisement for

1, Sce David Chilton, The Days of Vengeance (Ft. Worth, TX: Dominion Press,
1987), pp. 5i-12; James B. Jordan, “Rebellion, Tyranny, and Dominion in the
Book of Genesis,” in Gary North, ed., Tactics of Christian Resistance, Christianity
and Civilization No. 3 (Tyler, TX: Geneva Ministries, 1983), pp. 38-80.

195
