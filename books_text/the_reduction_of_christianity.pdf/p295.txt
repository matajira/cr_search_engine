From the American Puritans to the Revolution 255

the American Revolution.* Eighteenth-century colonials held
many of the same expectations that the 17th-century Puritans had
voiced before them. To many American clergymen in particular,
“it seemed increasingly likely that the millennial age would arise
from this struggle for liberty and Christianity in which the colo-
nists were engaged.” One of these, Ebenezer Baldwin, speculated
that America might be “the principal Seat of that glorious King-
dom, which Christ shall erect upon Earth in the latter Days.”35
Another New England preacher, Samuel Sherwood, said in 1776
that the government of George III “appears to have many of the
features, and much of the temper and character of the image of
the beast.” He believed that the revolution was essentially an
effort to advance the kingdom, and speculated that the war with
England “may possibly be some of the last efforts, and dying
struggles of the man of sin.”

34. See Alan Heimert, Religion and the American Revolution from the Great Awaken-
ing to the Revolution (Cambridge, MA: Harvard University Press, 1966).

35.. Quoted in Mask A. Noll, Christians in the American Revolution (Washington,
D.C.: Christian College Consortium, 1977), p. 58. See also Nathan O. Hatch,
The Sacred Cause of Liberty: Republican Thought and the Millennium in Revolutionary
New England (New Haven: Yale, i977).

36. Quoted in Stout, New England Soul, p. 309.

Throughout this survey, we have discovered that Christians at various times
have “nationalized” the prophecies of Scripture, and used prophecy to interpret
contemporary events. English Puritans believed that 17th-century England
would be the focus of the kingdom. American Revolutionaries believed it would
be America. It should be noted that this nationalism is not in any way necessary
to the view of the kingdom that we are advocating. In fact, it is 2 gross distortion
of our view. Our point in quoting from these sources is not that we agree with
every opinion expressed. Rather, we are simply trying to show that our optimism
ig not unique in the history of the church.

The United States, for all its historical importance, is not at all a unique peo-
ple. of God. We believe that God deals with nations in the New Covenant, as in
the Old, and that nations are blessed insofar as they remain faithful to the Lord.
But this does not mean that any nation is the “New Israel.” As the New Testa-
ment explicitly teaches, we believe that the church is the New Israel (Gal. 6:16;
1 Pet. 2:9 with Ex. 19:6). Nations receive blessings through the church. Even the
revolutionary clergy, in spite of their apparently blind patriotism, noted this.
‘They claimed that the kingdom would not advance by war or revolution, but
only if the people were faithful to God and His covenant law (Stout, p. 310).
