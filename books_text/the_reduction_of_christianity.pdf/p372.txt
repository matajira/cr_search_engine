932 ‘The Reduction of Christianity

stranger as well as the native, for I am the Lorp your God” (Lev.
24:22; cf. Ex. 12:49). The alien was given “equal protection under
the law,” Aliens could acquire property and accumulate wealth
(Lev, 25:47). They were protected from wrong-doing and treated
like the “native” Israelite (Lev. 19:33-34). A native-born Israelite
could “not wrong a stranger or oppress him” (Ex. 22:21; 23:9). If
the alien was hound to keep the law of God, then the law of God
was. the standard for protecting him against injustice as well
(Deut. 1:16; cf. 24:16; 27:19). John the Baptist saw no restriction
attached to him when he confronted King Herod and his adulter-
ous relationship with Herodias, the wife of his brother Philip: “For
John had been saying to Herod, ‘Tt is not lawful for you to have
your brother’s wife’” (Mark 6:18; cf. Ex. 20:14).

At a time when the world is looking for firm ground, Chris-
tians should be ready, willing, and able to turn people to the Bible
as the blueprint by which we can build a Christian civilization.
