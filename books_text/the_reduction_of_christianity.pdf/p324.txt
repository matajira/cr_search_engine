284 The Reduction of Christianity

better—but. only for those individuals who give their hearts and
lives to the Lord Jesus Christ and by so doing become “the salt of
the earth.™™

For the most part, we agree with Rev, Swaggart. His éni-
phasis on the gospel is just where it ought to be: Evangelism must
come first in any attempt to change anybody or anything. Apart:
from changed lives no lasting external changes are forthcoming.
Without changed lives there will not be a changed society. But the
gospel has a benefit for those who do not come under its immedi-
ate sway. The world is blessed when the Christian is blessed. This
was God’s promise to Abraham: “And I will bless you, and make.
your name great; and so you shall be a blessing; and I will bless
those who bless you, and the one whe curses you I will curse. And in
you all the families of the earth will be blessed” (Gen. 12:2-3). We are
Abraham’s spiritual descendants, and thus we reap the benefits of
those initial gospel promises to him (Rom. 4:9-25; 9:6-9). As.we
prosper, that is, as we are blessed by God, the world is blessed.
The history of western civilization attests to it. Being salt and light
to the world is a blessing to the world. Surely it’s a temporal bless-
ing, but it’s a blessing nevertheless. So then, the Christian’s new
life in Christ should benefit the world. If the world is decaying,
then it is due to the refusal of Christians to see their new life in
Christ as a blessing to the world.

But Rev. Swaggart is inconsistent at one point. He tells us that
the world is only made better “for those who give their hearts and
lives to the Lord Jesus Christ” and by so doing become “the salt of
the earth” What about orphanages, charities, homes for unwed
mothers, rescue missions, and Christian schools? Do these min-

14. Jimmy Swaggart, “The Coming Kingdom,” The Evangelist, September
1986, p. 9.

15, Ona September 12, 1987, television broadcast, Rev. Swaggart told his view-
ing audience that “Jimmy Swaggart Ministries” has fed 450,000 people in 50
countries, helps to fund 66 medical units around the world, and has helped to
build over 300 schools, 110 Bible schools, and 210 churches. This seems to be a
benefit beyond salvation. We applaud him and his ministry for their efforts,
