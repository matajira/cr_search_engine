xii The Reduction of Christianity

kingdom on earth, He always uses representatives: “the beast,”
“the antichrist,” etc. No Christian commentator ever argues that
-Satan’s use of human representatives is somehow any less of a
satanic kingdom. Yet many Christians deny that Christ also rules
His earthly forces through human representatives. They deny
that a king normally rules through his representatives. This is why sev-
eral of Jesus’ parables begin with the story of a king or a Jand-
owner who journeys to a far country, but leaves his representa-
tives (stewards) behind to rule in his name.

It is. true that Dominion Theology teaches that we can, do,
and will have a kingdom of God on earth without Jesus’ physical
presence in Jerusalem. This is somehow regarded as an out-
rageous doctrine. One tract-writer says that this is the number-
one error of Dominion Theology: “And in this we can isolate the
error of hardcore Dominion theology/Reconstruction/postmillen-
nialism. A universal kingdom, but without a personal, physical,
literal universal king!”? Fine; now would he argue that there is no
satanic kingdom either, because Satan is not visible and physically
present on the earth? Of course not. Then why does he think that
Dominion Theology is necessarily incorrect about the reality of
Christ’s kingdom reign without His physical manifestation in
Jerusalem?

Dave Hunt even denies that Christ’s personal, physical reign
from Jerusalem is a sign of the kingdom. Yet his supporters think
that he is a defender of “the old-time religion.”

Dave Hunt vs. Dominion Theology

Let us begin with the words of Jesus:.“All power is given unto
me in heaven and in earth” (Matt. 28:18; KJV). We should then
ask the obvious question: Where is the earthly manifestation of Christ's
power? Dave Hunt is adamant: only in the hearts of believers and
(maybe) inside the increasingly defenseless walls of a local church

 

2, QGPA With Charles P. Schmitt (Silver Spring, MD: Foundational Teachings,
no date), second page.
