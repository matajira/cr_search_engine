New Age Humanism: A Kingdom Counterfeit 123

wanting. It has been found difficult; and left untried.”°3 The New
Agers are just a testimony to these words: “The sons of this age
are more shrewd in relation to their own generation than the sons
of light” (Luke 16:8).

The philosophy and actions of the New Age Movement should
shame Christians. New Agers at least believe that change can
come, yet they only have confidence in man, or at most, some cos-
‘mic impersonal force. We have the Lord of Glory, the Ruler of the
kings of the earth, God Almighty. For too long, Christians have
had only a bleak earthly future to offer the lost. Even today, many
Christians do not believe there is an earthly future. The world is
despised and rejected. The secularists are doing what we should
have been doing. Although they have done a terrible job, they are
in visible control, for now. No wonder things look bad. What do
we expect when we turn the world over to people who deny God
and the power of His gospel? °

It’s time for Christians to present alternatives to the bankrupt
New Age philosophy without jettisoning the realities of a Christian
civilization. We can either react in despair or compete head to head
and win the battle through excellent kingdom work (Zech. 1:18-21).

Conclusion

The ideology of the New Age is satanic and humanistic. It is a
result of the influx of Eastern religious thought into the West. It is,
therefore, a dangerous movement that must be resisted by Chris-
tians. In order to resist the movement effectively, we must recog-
nize New Age humanism for what it is: a counterfeit of the true
New Age and the true kingdom, which were both inaugurated by
the life, death, and resurrection of Christ. New Age humanism
cannot be resisted by retreating, hopeless Christians. In fact, a
Christian retreat will. aid and abet the New Age’s program. In-
stead, Christians must resist confidently, knowing that the true
King fights with and for them.

53,. “The Unfinished Temple,” G. K. Chesterton: Collected Works, 28 vols. (San
Francisco, CA: Ignatius Press, 1987), vol. 4, p. 61
