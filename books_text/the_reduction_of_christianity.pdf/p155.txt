New Age Humanism: A Kingdom Counterfeit 15

counterfeit of God’s progressive kingdom activity on earth, in
time and in history, because he has no conception of an earthly
manifestation of the kingdom. For Hunt, then, the kingdom that
is being counterfeited is heaven itself because God’s kingdom does
not even find expression in the earthly millennium. He writes:
*The millennial reign of Christ upon earth, rather than being the
kingdom of God, will in fact be the final proof of the incorrigible
nature of the human heart. But this does not conform to Scrip-
ture. In Isaiah 65:17-25, there is a description of what all Chris-
tians would certainly describe as kingdom-like conditions: “No
Jonger will there be in it an infant who lives but a few days, or an
old man who does not live out his days; for the youth will die at
the age of one hundred and the one who does not reach the age of
one hundred shall be thought accursed” (v. 20). This cannot be a
description of heaven, since people will not die in heaven. Houses
will be built, vineyards will be planted (v. 21), and the “wolf and
the lamb will graze together,” and “the lion shall eat straw like the
ox” (v. 25).

For the traditional premillennialist, Isaiah 65:18-25 is describ-
ing conditions during the earthly millennium—the “kingdom
age.” Most premillennial commentators see this as the millennial
reign of Christ on the earth. In Jerry Falwell’s Liberty Bible Commen-
tary, which is described in the Preface as “Eschatologically Premillen-
nial” without “many of the excessive divisions of extreme dispensa-
tionalism,” Edward F. Hindson comments on Isaiah 65:18-20:

In this kingdom to come, time itself shal] begin to fade away;
and both the infant and the ofd man shall have filled (lived to fulfill)
their days. The phrase, the child shall die a hundred years old, means

 

‘Spotlight, Vol. 8, No. 1 ( January-June 1987), p. 8. The kingdom of God is oper-
ating-in the world now. There is no kingdom to establish. Mr. Dager creates a
false impression for those who have not read rauch Christian reconstruction
literature, If the kingdom is a present reality, then as kingdom-subjects, Chris-
tians and.non-Christians are responsible to live in terms of the King’s demands.
Christian reconstructionists believe that as King, Jesus calls all men everywhere to
repent (Acts 17:30), to obey His commandments ( John 14:15), and to recruit addi-
tional kingdom members through the proclamation of the gospel (Matt. 28:18-20).

41. Hunt, Beyond Seduction: A Return t Biblical Christianity (Eugene, OR:
Harvest House, 1987), p. 250. Emphasis added.
