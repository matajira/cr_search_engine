xxxvi The Reduction of Christianity

cause pretribulational dispensationalism has always taught that
the Antichrist is supposed to come only after the rapture! First the
rapture, then the Antichrist, and finally the tribulation, Dispensa-
tional theologians have always maintained that the Antichrist will
come to power only after the rapture.? Hal Lindsey wrote these
words in his best-selling book, The Late Great Planet Earth: “There
would be no earthly advantage in being alive when the Antichrist
rules. We believe that Christians will not be around to watch the
debacle brought about by the cruelest dictator of all time.”

So why is Mr. Hunt going around the country warning Chris-
tians about the imminent appearance of the Antichrist? Why
bother ourselves about the Antichrist? If pretribulational dispen-
sationalism is true, not one Christian alive today will be around to
identify the Antichrist, let alone serve him, All Christians will be
raptured defore Antichrist makes his appearance. This is why Hal

3. Post-tribulational dispensationalists do have a legitimate worry about the
appearance of the Antichrist, but Mr. Hunt is not generally recognized by his
readers as a post-tribulationist, nor are most of his readers. Hunt, aa far as we
have been able to determine, has never explicitly called kimself a “pretribber.” It
is clear from his book, Peace Prosperity and the Coming Holocaust (Eugene, OR:
Harvest House, 1983), that he does not believe that Christians will go through
the tribulation. In that book, Hunt proposes a “contrary scenario” in response to
the “gloom-and-doom and frightening forecasts” of other premillennialist writers
{p. 18). Jesus will return to a prosperous, peaceful, wealthy, and utterly corrupt
world. The prophecies of Jesus’ Second Coming are “hardly indicative of either.a
worldwide financial collapse or a nuclear holocaust” (p. 18). ‘Thus, it seems clear
that Hunt believes in a pretribulational rapture.

Or is it? Certain portions of Hunt's other books are difficult to reconcile with
this position. In The Seduction of Christianity, for example, Hunt and T. A.
McMahon lament the “growing rejection within the church of [the] fundamental-
ist scenario as negative, ‘gloom-and-doom’ eschatology” (p. 216). What is the fun-
damentalist scenario (which appears to be the authors’ own)? This view stresses
that “the world is heading for a great tribulation climaxing in the Battle of Ar-
mageddon” (p. 216). Of course, it may be possible to reconcile this with Hunt’s
tejection of the “gloom-and-doom” scenario. But it appears to us a-wee bit incon-
sistent. We assume in this book that Hunt is a pretribber, though we must admit
that we are not quite sure what his position on the rapture is,

4, Hal Lindsey, The Late Great Planet Earth (Grand Rapids, MI: Zondervan,
[1970] 1973), p. 113.
