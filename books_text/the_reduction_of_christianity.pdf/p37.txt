Preface xaxvii

Lindsey warns that “we must not indulge in speculation about
whether any of the current figures is the Antichrist.”> It is just one
more nonexistent problem for Christians to worry about. Gary
North writes: “This needless fear of the antichrist is paralyzing
Christians’ required fear of God; God tells us to serve as prophets
who are required to confront a sinful civilization with the ethical
demands of God’s covenant, but the Jonahs of this age are too
busy packing for their trip to the heavenly Tarshish, ‘Antichrist
fever is being added to ‘rapture fever’ "6

This misguided belief in the power of the Antichrist certainly
puts a damper on any long-term program that expects success in
turning back the tide of evil in our society. Of course, we want to
be faithful to Scripture, and, if Mr. Hunt is correct, we shall have
to change our views. But if he is wrong, then we must sound a
different warning to the church, a warning to wake up and get
busy with the work at hand.

The Advance of Christianity

Question: Is it possible that the Bible teaches that the gospel
will have worldwide success, that nations will be discipled, and
that we-will see the Word of the Lord cover the earth as the waters
cover the sea before Jesus returns in glory to rapture His saints?
(Isa. 11:9). But even if this were not possible, is it possible that the
Antichrist will come to power before the rapture? Pretribulational
dispensationalists have always said no, until.Mr. Hunt. came
along.

‘The tragic thing is this: well-meaning dispensational Chris-
tians upset themselves about a problem that the leading teachers
of dispensational theology have always insisted is not a problem at
all. They are worried about something that is a non-event as far
as pretribulational dispensationalism is concerned.

5, Idem.
6: Gary North, ds the World Running Down? Crisis in the Christian Worldview
(Tyler, Texas: Institute for Christian Economics, 1988), p. 288.
