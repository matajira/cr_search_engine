Here a Conspiracy, There a Conspiracy 131

the whole world is controlled by a liberal media, international
bankers, and the Council on Foreign Relations, especially when

. they are in league with one another and the devil? Of course, the
natural response is that we can’t do much. Dave Hunt states that
“itis no longer a question of whether but when humanity will be
united both economically and politically under a one-world gov-
ernment.” He goes on to quote from the Washington Post's evalua-
tion of the Carter administration:

If you like conspiracy theories about secret plots.to take over
the world, you are going to love the administration of President-
elect Jimmy Carter. At last count 13 Trilateralists had gone into
top positions . . . extraordinary when you consider that the Tri-
lateral Commission only has about 65 American members,¥

The Biblical View

The Bible has something to say about the conspiracies of men:
“You are not to say, ‘It is a conspiracy!’ in regard to all that this
people call a conspiracy. And you are not to fear what they fear or
be in dread of it” (Isa. 8:12). Basically, God is saying that the con-
spiracies of men mean nothing in the long run. First, we should
not call everything conspiratorial just because a number of anti-
Christian groups think alike and often work in the same areas.
The anti-Christian “conspirators” often look to Christian groups
and make the same assessment. There are hundreds of Christian
ministries that are not officially related, but their common beliefs
and goals give the impression that they are working together, con-
spiring to bring an end to humanism wherever it is found. There
are times when many of these Christian groups might work
together on common projects to display a show of force. Usually,
when the battle is finished, each group goes back to its original

14. Dave Hunt and T. A. McMahon, The Seduction of Christianity: Spiritual Dis-
cernment in the Last Days, (Eugene, OR: Harvest House, 1985}, p..49.
15, January 16, 1977, cited in Idem.
