342 The Reduction of Christianity

God and that obey not the gospel of our Lord Jesus Christ?
(2 Thessalonians 1:8).2

Hunt insists, “If we are to be biblical Christians, God’s Word must
be our guide in all we say and do, no matter how unpopular that
makes us.””

We agree wholeheartedly with that statement. We cannot
stress too strongly our agreement with Hunt's principle. As Chris-
tians, we are governed by Scripture in every area of life and
thought. Though we are in agreement with Hunt on this princi-
ple, he is inconsistent, we believe, in applying it. In an interview
with Peter Waldron, for example, Hunt said that, if he were to be-
come a congressman, he could not enforce his Christian beliefs,
because he would have to represent people who did not share
those beliefs. He would witness to his colleagues, but he would
not “impose” Christian morality on a non-Christian populace.

Tn one sense, we agree with Hunt fully. We do not believe that
Christianity can be imposed from the top down. As we have
pointed out previously, we are not “political pyramidists.” We be-
lieve that Christianity will transform society as people are trans-
formed by the gospel. In another sense, all law is imposed moral-
ity, Every law is involved with ethics. The question is not ethical
Jaw versus unethical law. The question is which ethical system will
provide the foundation for law. We believe that the Bible should
provide the moral foundations for law. There are, for example,
clear standards in Scripture for civil government. The Bible gives
the State authority to punish with the sword (Gen. 9; Rom. 13).
The State has the authority to punish murder and other crimes.
Hunt seems to agree in principle that the Bible should be the
foundation of civil law, yet when it comes to passing laws in con-
gress, Hunt indicates that he would not impose biblical morality.

Given the biblical requirements for the State, what would

19. Hunt, Beyond Seduction: A Return to Biblical Christianity (Eugene, OR:
Harvest House, 1987), p. 259.

20. Ibid., p. 249.

21. Interview with Dave Hunt, “Contact America,” August 12, 1987.
