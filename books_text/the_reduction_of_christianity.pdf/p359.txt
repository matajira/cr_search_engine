Building a Christian Civilization 319
legislation, and the responsibility for bringing in the millennium
is man’s own—to be produced in his own strength.”

The State plays a large role in the Social Gospel approach to
building a Christian civilization. Advocates of the Social Gospel
see “a one undivided realm, the state, as the true order of God and
man. The state is given the overall jurisdiction and sovereignty
over church, school, family, business, farming, and all things else
which belong only to God. The essential function of the social gos-
pel is to render all things unto Caesar and nothing to God.38

It should be remembered that evangelicals who opposed the
Social Gospel believed that Christians should influence society.
Their animosity was toward those who put all of their emphasis
on the public and political side of Christian activity. The evangeli-
cals believed that the first step in societal transformation must
come through repentance for sin and total dependence on God’s
grace supplied to us in the sacrificial death of Jesus. The Social
Gospel had degenerated into “religious morality, that is, moral-
ity without Christ. “The antisupernaturalism and the radical em-
phasis upon the social and political application of Christianity
which often accompanied the Social Gospel dimmed enthusiasm
for political action among fundamentalists; it even stigmatized

37. Bahnsen, “The Prima Facie Acceptability of Postmillennialism,” p. 50.

38, R. J, Rushdoony, The Foundations of Social Order: Studies in the Creeds and
Councils of the Early Church (Nutley, NJ: Presbyterian and Reformed, 1968), pp.
134-35.

39. Even John Dewey acknowledged that Christianity was a beneficent force
in society: *, . . the church-going classes, those who have come under the influ-
ence of evangelical Christianity . , . form the backbone of philanthropic social
interest, of social reform through political action, of pacifism, of popular educa-
tion, They embody and express the spirit of kindly goodwill towards classes
which are at an economic disadvantage and towards other nations. . . .” “The
American Frontier,” The New Republic, May 10, 1922. Quoted by Paul Johnson,
Modem Times (New York: Harper & Row, 1983), p. 209.

40. “Following the lead of philosophical pragmatism, proponents of the Social.
Gospel held that the only test of truth was action. ‘Religious morality, said
Walter Rauschenbusch, is ‘the only thing God cares about.” George. M.
Marsden, Fundamentalism and American Culture: The Shaping of Twentieth
Evangelicalism: 1870-1925 (New York: Oxford University Press, 1980), pp. 91-92.
