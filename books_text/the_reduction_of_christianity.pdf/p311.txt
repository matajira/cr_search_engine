15
TURNING THE WORLD RIGHT-SIDE UP

Adam and Eve's descent into sin and judgment brought about
a corrupt world. While the image of God was not destroyed by the
Fall, the likeness of God was certainly defaced. Now man reflects
the attributes of the fallen Adam. Deception and death followed
the sin of man. A time came when “the Lord saw that the wicked-
ness of man was great on the earth, and that every intent of the
thoughts of his heart was only evil continually” (Gen. 6:5).

The Old Testament is the story of a series of similar judg-
ments. Each time God “recreated” the world, men fell again. Gad
came to dwell with Israel in the wilderness, as He had dwelt with
Adam in the Garden, but Israel refused to obey (Num. 14). God
enabled Israel to conquer the land ( Joshua), but Israel quickly fell
{ Judges). God established His victorious anointed in Jerusalem (2
Sam. 10), but David’s heart led him astray (2 Sam. 11). Salomon
built a glorious temple (1 Kings 6), but half of the kingdom was
ultimately torn from him (1 Kings 11:29-40).

Jesus’ Renewal of This World

Jesus again renewed all things. But, unlike the “recreations” of
the Old Testament, the recreation of the world by Jesus is irreversible.
The resurrection of Christ is the definitive renewal of all things.
Even those outside the Christian tradition understood the impli-
cations of Jesus’ work: “These men who have upset the world have
come here also” (Acts 17:6). God, through His Spirit, transforms
individuals. But there are societal and global ramifications of the
work of Christ just as there were global ramifications of the work

271
