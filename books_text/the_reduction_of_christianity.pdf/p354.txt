314 The Reduction of Christianity

the world. “[Flor a long time great numbers of Christians were
convinced not only that Christ would soon return in power and
majesty but also that when he did return it would be to establish a
messianic kingdom on. earth. And they confidently expected that
kingdom to last, whether for a thousand years or for an indefinite
period.” The Montanists of the second century went to Phrygia
te await the “imminent coming of the Kingdom” where the “New
Jerusalem was about to descend from the heavens on to Phrygian
soil, where it would become the habitation of the Saints.”

It seems that every generation has those. who believe that Jesus
will return “in their lifetime” to set up His millennial reign. While
such a belief can encourage, it can also debilitate. The Millerites*
of the 19th century are an extreme case in point:

Utterly convinced that Jesus Christ would appear on October
22, 1844, many Millerites took decisive action. Some left their
jobs, boarded up their businesses, confessed to unsolved crimes,
sold their farms and everything they owned, and let their crops
go unharvested so that they could spread the word of Christ’s
coming and meet him with clean consciences and free of debt. As
the expected day approached, thousands of people found it diff-
cult if not impossible to live normal lives.2

The beliefs of the Millerites fortunately are no longer widely
held. But, while the extremism of the Millerites is gone, some of
the passivity remains. There is little interest in long-term civiliza-
tion-buiiding. If the world cannot be saved in a month, maybe a

22. Norman Cohn, The Pursuit of the Millennium (London: Secker & Warburg,
1957), pp. 6-7.

23. Dbid., pp. 8-9.

24. William Miller (1782-1849), founder of Adventism, -was converted from
Deism in 1816. After fourteen years of Bible study, he decided that Jeaus would
return in 1843, at the outside, October 22, 1844. His book, Evidence from Scripture
and History of the Second Coming of Christ, About the Year 1843, published in 1836, was
instrumental in winning many to his views.

25. Timothy Weber, Living in she Shadow of the Second Coming, p. 43.
