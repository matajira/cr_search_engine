70 The Reduction of Christianity

writings of Dr. Greg L. Bahnsen,? Rev. Ray Sutton,® David
Chilton,” R. J. Rushdoony,” and Dr. Gary North,” all of whom
hold to an optimistic eschatological position called “postmillen-
nialism,” and ail of whom could be identified as “reconstruction-
ists.” There is nothing in any of their writings that would suggest
that man ascends the great “chain of being” and becomes one with
God or that the creation in some way is a part of God, Gary North
writes about the Creator-creature distinction in these terms: —

There is @ basic difference between God and the universe,
between God and man. Man is a created being. No man stands
alone. No man stands independent of God. No man merges into
God, either. God tells us very specifically that “my thoughts are

8, “The Reformation of Christian Apologetics,” The Foundations of Christian
Scholarship: Essays in the Van Til Perspective, ed., Gary North (Valiecito, CA: Rosa
House Books, 1976), p. 210.

9. “Biblical transcendence means there is a fundamental distinction between
the Creator's Being and the creature's being. . . . God’s Being is uncreated, and
man’s is created. God is original, and man is derivative. . . . God is independent
(aseity) and man is dependent. God is God, man is man, and the latter is never
able to become God, although God did become man in Jesus Christ. Further-
more, Gad is ‘near’ by means of the covenant.” Ray Sutton, That You May Prosper:
Dominion By Covenant (Tyler, TX: Dominion Press, 1987), pp. 24-26.

10. “Ethical Theology teaches that my relationship with God is covenantal and
legal; that my salvation has taken place objectively in Another, Jesus Christ. In
salvation I am not metamorphosed into a higher level of reality; rather, God
saves me from my sins and conforms me ethically to the image of Christ, so that I
am restored to the purpose for which God originally created man: godly domin-
ion over the earth, This means that the Christian life is primarily to be defined in
terms of personal communication with God and obedience to God's word. Rap-
turous experiences are not discounted, but they must be recognized as of second-
ary importance. More than this, those subjective experiences must be inter-
preted in the light of the objective word of God, the Bible. No experience makes
me anything more than a finite creature. 1 will always be a finite creature, and
nothing more. Salvation is not deification.” David Chilton, “Between the Covers
of Power for Living,” in Biblical Economics Today, Vol. VII, No. 2 (Feb./Mar.,
1984), p. 4.

11. Rushdoony, By What Standard? An Analysis of the Philosophy of Comelius Van
Til (Tyler, TX: Thoburn Press, [1958] 1983), pp. 122-26, 130-31, 150-64; The One
and the Many: Studies in the Philosophy of Order and Utimasy (Fairfax, VA: Thoburn
Press, [1971] 1978), pp. 58-60, 132-33, 168-70, 190-97, 259-60.

12. North, Unhely Spirits: Occultism and New Age Humanism (Ft. Worth, TX:
Dominion Press, 1986), pp. 58-61.
