350 The Reduction of Christianity

30. For the right Faith is, that we believe and confess: that
our Lord Jesus Christ, the Son of God, is God and Man;

31. God, of the Substance [Essence] of the Father; begotten
before the worlds: and Man, of the Substance [Essence] of his
Mother, born in the world.

32. Perfect God: and perfect Man, of a reasonable soul and
human flesh subsisting.

33. Equal to the Father, as touching his Godhead: and infe-
rior to the Father as touching his Manhood.

34, Who although he be [is] God and Man; yet he is not two,
but one Christ.

35. One; not by conversion of the Godhead into flesh: but by
taking [assumption] of the Manhood into God.

36. One altogether; not by confusion of Substance [Essence]:
but by unity of Person.

37, For as the reasonable soul and flesh is one man: so God
and Man is one Christ;

38. Who suffered for our salvation: descended into hell
[Hades, spirit-world]: rose again the third day from the dead.

39. He ascended into heaven, he sitteth on the right hand of
the Father God [God the Father] Almighty.

40. From whence [thence] he shail come to judge the quick
and the dead.

41. At whose coming all men shall rise again with their bodies.

42. And shall give account for their own works.

43, And they that have done good shall go into life everlast-
ing: and they that have done evil, into everlasting fire.

44. This is the Catholic Faith: which except a man believe
faithfully [truly and firmly], he can not be saved.
