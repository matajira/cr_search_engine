Let’s Define Our Terms 25

the commandments, Dominion comes through service. The Gen-
tiles, those outside of Christ in Jesus’ day, “lord it over” the people,
looking to the power of the State to grant favors and protection to
loyal subjects (Luke 22:25). It's something of a master-slave rela-
tionship. As a result, these lords are described as “benefactors.”
They, through force, work to “benefit” some of the people for their
own political ends. This is not the way the dominion-oriented
Christian rules with Christ. Again, service is the prescription for
dominion: “But not so with you, but let him who is greatest
among you become as the youngest, and the leader as the servant,
For who is greater, the one who reclines at table, or the one who
serves? Is it not the one who reclines at table? But I am among
you as the one who serves” (vv. 26, 27), It is idolatrous to seek do-
minion primarily by political means, whether by domination or
anarchic revolution.

When Christians “serve” the world, they will be seen as “bene-
factors,” wanting nothing in return but to bring glory to God.
Dominion will then be established progressively over time, not
through oppression, but through faithful service. Notice the goal
in Jesus’ statement. He.does not say that Christians should not
have authority, that they should not be the leaders. To the con-
trary, He asserts that Christians ought to do things differently in
order to reach results that are much better than anything the Gen-
tiles can offer, The task for the Christian is to be “light” in a world
of darkness. How does he do this? Again, he serves. For what
purpose? To extend the dominion of the Lord Jesus Christ into
every area of life, a dominion that is His by divine right, a domin-
ion that He shares with His subordinates.

The dominion of Christians is a benefit to the world only be-
cause Christ works in and through them. The benefits do not
come ultimately from Christians, those who do the nitty gritty
work of service in the world, but from Christ. How then are non-
Christians pointed to Jesus as their true “Benefactor”? Through
our works of service: “You are the light of the world. A city set on
a hill cannot be hidden. Nor do men light a lamp, and put it
under the peck-measure, but on the lampstand; and it gives light
