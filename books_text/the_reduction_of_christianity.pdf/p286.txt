246 The Reduction of Christianity

Throughout the colonial period, ministers rarely preached speci-
fically on millennial prophecies pointing to the end of time, and
when they did it was generally in the most undogmatic and spec-
ulative of terms. For the most part, they did not base their
preaching on the assumption that history would stop tomorrow,
and in this respect they differed radically from popular millenar-
ian movements in Europe and post-Revolutionary America
whose plans of action were governed exclusively by apocalyptic
considerations. The past was the tried-and-true key invariably
invoked by [Puritan] ministers to interpret the present.*

In many cases, they were optimistic about the future even in
the face of seemingly inconquerable odds. One scholar notes:

, . . from the very beginning, the bent of the colonists in Massa-
chusetts. Bay—unlike their brethren in Plymouth—was not to
withdraw from the world but to reform it, to work within the in-
stitutional continuities of history rather than to deny them. . . .
Somehow this world’s institutions had to be refashioned to con-
form to Christ’s spiritual Kingdom.

The vision of our Puritan forefathers, given expression by
John Winthrop in his “Model of Christian Charity” in 1630 aboard
the Arabella, was that there was an earthly future for the faithful
people of God.

. . . the Lord will be our God and delight to. dwell among us, as
His own people, and will command a biessing upon us in all our
ways, so that we shall see much more of His wisdom, power,
goodness, and truth than formerly we have been acquainted
with, We shall find that the God of Israel is among us, and ten of
us shall be able to resist a thousand of our enemies, when He
shall make us a praise and glory, that men shall say of succeeding
plantations: “The Lord make it like that of New England.” For we

4. Ibid., p. 8.

5. Quowd by Gary North, “Editor’s Introduction,” Journal of Christian Recon-
struction, vol. 6, no. 1 (Summer 1979), .p. 7. This thesis is developed at greater
Iength in Aletha Joy Gilsdorf, “Purity and Progress: New England's First
Generation,” Journal of Christian Reconstruction, vol. 6, no. 1 (Summer 1979), pp.
107-135.
