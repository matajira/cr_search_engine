364 ‘The Reduction of Christianity

where is the Roman Empire today? The Church overcame it, cen-
tury by century. The Church had something even better to offer,
The Church had the foundations of social order, as Rushdoony has titled
his book on the social impact of the Christian creeds, It still has
these foundations, and only the Church has them. Nevertheless,
there are millions of Christians today who have been taught, im-
plicitly. or explicitly, that Christianity has nothing as good as hu-
manism to offer society, outside of the individual heart, the family,
and the local-church. Because their teachers recognize that you
can’t beat something with nothing, they have long recommend
that Christians stop fighting. They would rather have Christians
surrender, losing by default. They recommend that Christians
cease devoting scarce economic resources—time, money, and
effort—to challenging humanist civilization, and instead adopt a
program of “tract-passing.” (And even this tool is gone. When was
the. last time you passed a tract, or even saw one? Not since the
early 1960s, probably.) In short, they teach that we just can’t win.

But we can win! Christianity has a better program for the
world than Satan does. It has always had a better program. The
trouble is, Christians have forgotten their own history. They have
been taught history by Humanists. They have allowed their ene-
mies to teach them just about everything, but with Christians
paying the compulsory tuition fees (tax-supported schools). It is
time for Christians to relearn their history.

To do this, they will have to start reading serious books. They
should start with the following list.

1. General Introduction

Gary North, Liberating Planet Earth: An Introduction to Biblical
Blueprints. Ft. Worth, Texas: Dominion Press, 1987. This is the
first volume in the multi-volume Biblical Blueprints Series, edited
by Dr. North. It-was written originally for use by Latin American
pastors who are confronting the Marxist and socialist movement
known as liberation theology. It provides a full-scale alternative to
liberation theology. There are chapters on Christ and liberation,
the God of liberation, the enemies of liberation, the covenant of
