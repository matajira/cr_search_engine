The Last Days 21

on earth, when the “Holy Mountain” began its dynamic growth
and all nations began to flow into the Christian faith, as the
prophets foretold (see Isa, 2:2-4; Mic. 4:1-4). Obviously, there is
still a great deal of ungodliness in the world today. But Christi-
anity has been gradually and steadily winning battles since the
days of the early church; and as Christians continue to make
war on the enemy, the time will come when the saints possess the
Kingdom (Dan. 7:22, 27).

This is why Paul could comfort believers by assuring them
that “the Lord is at hand” (Phil. 4:5). Indeed, the watchword of
the early Church (1 Cor. 16:22) was Maranatha! The Lord
comes! Looking forward to the coming destruction of
Jerusalem, the writer to the Hebrews warned those tempted to
“draw back” to apostate Judaism that apostasy would only
bring them “a certain fearful expectation of judgment, and fiery
indignation which will devour the adversaries” (Heb. 10:27).

For we know Him who said, “Vengeance is Mine; I will repay,
says the Lord.” And again, “The Lord will judge His people.” It
is a fearful thing to fall into the hands of the living
God. ... For you have need of endurance, so that after you
have done the will of God, you may receive the promise: “For
yet. a little while, and He who is coming will come, and will not
tarry. Now the just shall live by faith; but if anyone draws back,
My soul has no pleasure in him.” But we are not of those wha
draw back to perdition, but of those who believe to the saving of
the soul (Heb. 10:30-31, 36-39).

The other New Testament authors wrote in similar terms.
After James warned the wealthy unbelievers who oppressed the
Christians of the miseries about to descend upon them, charging
that they had fraudulently “heaped up treasure in the last days”
(James 571-6), he encouraged the suffering Christians:

Therefore be patient, brethren, uatil the coming of the Lard. See
how the farmer waits for the precious fruit of the earth, waiting
patiently for it until it receives the early and latter rain. You also
be patient. Establish your hearts, for rhe coming of the Lord is
at hand, Do not grumble against one another, brethren, lest you
be judged. Behold, rhe Judge is standing at the door! (James
5:7-9),
