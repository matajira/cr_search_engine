A Brief Summary of the Revelation 171

have become an accomplished fact. The witnessing, prophetic
Church, seemingly annihilated by the Jewish persecution, is res-
urrected; and it is the persecutors who are crushed. With the de-
struction of Jerusalem and the overthrow of the Old Covenant
scaffolding, the completion and filling of the new and final Tem-
ple are revealed to the world.

Chapter Twelve forms a dramatic interlude, portraying the
basic batile of history in the cosmic conflict between Christ and
Satan. The Son of God ascends the throne of His kingdom, un-
harmed and victorious, and Satan then turns to persecute the
Church. Again, this assures God’s people that all their persecu-
tions originate in the total warfare of the forces of evil against
Christ, the Seed of the Woman, who has been predestined to
shatter the Dragon’s head. With Him, the Church will be more
than conquerors.

Chapier Thirteen reveals the all-out warfare which was ap-
proaching between the faithful Church and the pagan Roman
Empire (the Beast). God’s people are warned that the religious
forces of apostate Judaism will be aligned with the Roman
State, seeking to enforce the worship of Caesar in place of the
worship of Jesus Christ. With confident faith in Christ’s lord-
ship, the Church is to exercise steadfast patience; revolution is
condemned,

Chapters Fourteen, Fifteen, and Sixteen reveal the victor-
ious army of the redeemed, standing on Mount Zion singing a
song of triumph. Christ is seen coming in the Cloud of judg-
ment upon rebellious Israel, trampling on the ripened grapes of
wrath. The Temple is opened, and while the Glory-Cloud fills
the sanctuary the divine judgments are outpoured from it,
bringing Egyptian plagues upon the apostates.

Chapters Seventeen and Eighteen expose the essence of Jeru-
salem’s sin as spiritual adultery. She has forsaken her rightful
husband and is committing fornication with pagan rulers, wor-
shiping Caesar, “drunk with the blood of the saints”; the holy
city has become another Babylon. God issues one final call for
His people to separate themselves from Jerusalem’s harlotries,
and abandons her to the ravaging armies of the Empire. At the
sight of the utter ruin of apostate Israel, the saints in heaven and
earth rejoice.

Chapter Nineteen begins with Communion — the joyful wed-
