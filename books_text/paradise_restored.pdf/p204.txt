Who, then, is He Who has done these things and
has united in peace those who hated each other, save
the beloved Son of the Father, the common Saviour
of ours, Jesus Christ, Who by His own love under-
went all things for our salvation? Even from the be-
ginning, moreover, this peace that He was to ad-
minister was foretold, for Scripture says, “They shall
beat their swords into ploughshares and their spears
into sickles, and nation shall not take sword against
nation, neither shall they learn any more to wage war”
Usa. 2:4].

St. Athanasius, On the Incarnation [52]
