The Hope 5

It is no more Biblical than its twin sister, the false view of Spirit-
uality. Instead of a message of defeat, the Bible gives us Hope,
both in this world and the next. The Bible gives us an eschatol-
ogy of dominion, an eschatology of victory. This is not some
blind, “everything-will-work-out-somehow” kind of optimism.
It is a solid, confident, Bible-based assurance that, before the
Second Coming of Christ, the gospel will be victorious through-
out the entire world.

For many, that will seem incredible. It goes against the whole
spirit of the modern age; for years, Christians have been taught
to expect defeat. Certainly, it’s a good idea to be careful about
“new” doctrines. Everything must be checked by the Scriptures.
One thing to consider, however, is that the idea of dominion is
not new. In fact, until fairly recently, most Christians held an es-
chatology of dominion. Most Christians throughout the history
of the Church regarded the eschatology of defeat as a doctrine
of crackpots.

The Hope of worldwide conquest for Christianity has been
the traditional faith of the Church through the ages. This fact
can easily be demonstrated again and again. We can see it in the
words of St. Athanasius, the great Church Father of the fourth
century whose classic book On the Incarnation of the Word of
God reveals his strong eschatology of dominion, He summarized
its thesis:

Since the Saviour came to dwell in our midst, not only does idol-
atry no longer increase, but it is getting less and gradually ceas-
ing to be. Similarly, not only does the wisdom of the Greeks no
longer make any progress, but that which used to be is disap-
pearing. And daemons, so far from continuing to impose on
people by their deceits and oracle-givings and sorceries, are
routed by the sign of the cross if they so much as try. On the
other hand, while idolatry and everything else that opposes the
faith of Christ is daily dwindling and weakening and falling, the
Saviour’s teaching is increasing everywhere! Worship, then, the
Saviour “Who is above all” and mighty, even God the Word, and
condemn those who are being defeated and made to disappear
by Him. When the sun has come, darkness prevails no longer;
any of it that may be left anywhere is driven away. So also, now
that the Divine epiphany of the Word of God has taken place,
the darkness of idols prevails no more, and all parts of the world
in every direction are enlightened by His teaching.
