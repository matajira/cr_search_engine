10 Paradise Restored

The nomads of the desert will bow before Him;

And His enemies will lick the dust.

The kings of Tarshish and of the islands will bring presents;
The kings of Sheba and Seba will offer gifts.

All kings will bow down before Him;

All nations will serve Him. (Ps. 72:8-11}

All nations whom Thou hast made shall come and worship be-
fore Thee, O Lorp;
And they shall glorify Thy name. (Ps. 86:9)

All the kings of the earth will give thanks to Thee, O Lorn,
When they have heard the words of Thy mouth.

And they will sing of the ways of the Lorp;

For great is the glory of the Lorp. (Ps. 138:4-5)

Let the godly ones exult in glory;

Let them sing for joy on their beds,

Let the high praises of God be in their mouth,
And 4 two-edged sword in their hand,

To execute vengeance on the nations,
And punishment on the peoples;

To bind their kings with chains,

And their nobles with fetters of iron;

To execute on them the judgment written;
This is an honor for all His godly ones.
Praise the Lorn! (Ps. 149:5-9)

What Difference Does It Make?

The eschatological issue centers on one fundamental point:
Will the gospel succeed in its mission, or not? Regardless of their
numerous individual differences, the various defeatist schools of
thought are solidly lined up together on one major point: The
gospel of Jesus Christ will fail, Christianity will not be successful
in its worldwide task. Christ’s Great Commission to disciple the
nations will not be carried out. Satan and the forces of Antichrist
will prevail in history, overcoming the Church and virtually wiping
it out—until Christ returns at the last moment, like the cavalry in
B-grade westerns, to rescue the ragged little band of survivors.

Does it make a difference? Does your view of prophecy really
affect your life? I think we have already seen much of the answer
to that question. The basic issue has to do with your attitude to-
ward the future. I recall a “Jesus People” newspaper of the early
