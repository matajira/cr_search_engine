iy
INTERPRETING REVELATION

From the outset, two problems confront us when we attempt
to study the Book of Revelation. First is the question of ensur-
ing that our interpretation is correct — placing checks on our im-
agination, so that we do not force God’s holy Word into a mold
of our own inventions. We must let the Book of Revelation say
what God intended it to say. The second problem is the issue of
ethics —what to do with what we’ve learned,

The Biblical Standard for Interpretation

In the very first verse of Revelation, John provides us with
an important interpretive key: “The Revelation of Jesus Christ,
which God gave Him to show His servants what must shortly
take place; and He sent and signified it by His angel to His ser-
vant John” (Rev. 1:1), The use of the term signify tells us that the
prophecy is not simply to be taken as “history written in ad-
vance.” Instead, it is a book of signs: symbolic representations
of the coming events. The symbols are not to be understood ina
literal manner. We can see this by John’s use of the term in his
Gospel (see John 12:33; 18:32; 21:19). In each case, it is used of
Christ signifying a future event by a more or less symbolic in-
dication, rather than by a clear and literal description. And this
is generally the form of the prophecies in the Revelation. This
does not mean the symbols are unintelligible; the interpretation
is not up for grabs. On the other hand, I am not saying that the
symbols are in some kind of code, so that all we need is a dic-
tionary or grammar of symbolism to “translate” the symbols
into English. Prophecy is poetry, not naive or static allegory.
The only way to understand its symbolism is to become familiar
with the Bible. The Biblical standard for interpretation is the
Bible itself.

We have already taken note of the fallacies and inconsisten-

151
