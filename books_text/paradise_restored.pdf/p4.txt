Copyright ®Dominion Press

First Printing, January, 1985
Second Printing, April, 1985
Third Printing, March, 1987
Fousth Printing, December, 1994

Library of Congress Cataloging-in-Publication Data

Chilton, David
Paradise restored : a biblical theology of dominion / David
Chilton.
. om.
Includes bibliographical references and index.
ISBN 0-930462-52-1 : $17.95
1, Dominion theology. 2. Eschatology. 3. Bible. N.T.
Revelaticn--Criticism, interpretation, etc. 4. Prophecy—
Christianity. I. Title.
BT82.25.C48 1994

230'.046--dc20 84-62186
CIP

All rights reserved. Written permission must be secured from
the publisher to use or reproduce any part of this book,
except for brief quotations in critical reviews or articles.

Quotations from On the Incarnation, by St. Athanasius (trans-
lated and edited by Sister Penelope Lawson, C.S.M.V.; New
York: MacMillan, 1981), are reprinted with the permission
of MacMillan Publishing Company.

Published by Dominion Press
P.O. Box 8000, Tyler, Texas 75711

Printed in the United States of America
