1,

TABLE OF CONTENTS

   

PREFACE oo... sce c eee e ccc etcc erent rere nee e tenes ix
Part One: AN ESCHATOLOGY OF DOMINION
The Hope... cee cee ete t renee 3
Part Two: PARADISE: THE PATTERN FOR PROPHECY
. How to Read Prophecy .... 15
. The Paradise Theme... - 23
. The Holy Mountain ... 29
. The Garden of the Lord ... 39
. The Garden and the Howling Wilderness 49
. The Fiery Cloud ............0..0-2 022000 5?

NAW Pp

 

Part Three: THE GOSPEL OF THE KINGDOM

8.

9.
10.
ll.
12.
13.
14,
13,
16.

The Coming of the Kingdom
The Rejection of Israel...
The Great Tribulation. .
Coming on the Clouds .
The Rise of Antichrist .
The Last Days .
The Restoration o! of Is ael .
The Day of the Lord.......
The Consummation of the Kingdom ....,...

 
 
 
 
 
 
  
  

Part Four: STUDIES IN THE BOOK OF REVELATION

17.
18,
19.
20.
21.
22.
23.

Interpreting Revelation ............. 02.02 eee ees 151
The Time Is at Hand............
A Brief Summary of Revelation ..

   
 

  

The Beast and the False Prophet (Revelation 13) 175
The Great Harlot (Revelation 17-19) ......... . 187
The Kingdom of Priests (Revelation 20) . 195
The New Creation (Revelation 21-22)........ - 203

vii
