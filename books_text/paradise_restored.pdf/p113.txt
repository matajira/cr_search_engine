Coming on the Clouds 103

1 kept looking in the night visions,

And beheld, with the clouds of heaven

One like a Son of Man was coming,

And He came up to the Ancient of Days
And was presented before Him.

And to Him was given dominion,

Glory and a Kingdom,

That all the peoples, nations, and men of every language
Might serve Him.

His dominion is an everlasting dominion
Which will not pass away;

And His Kingdom is one

Which wiil not be destroyed. (Dan. 7:13-14)

The destruction of Jerusalem was the sign that the Son of
Man, the Second Adam, was in heaven, ruling over the world
and disposing it for His own purposes. At His ascension, He
had come on the clouds of heaven to receive the Kingdom from
His Father; the destruction of Jerusalem was the revelation of
this fact. In Matthew 24, therefore, Jesus was not prophesying
that He would literally come on the clouds in a.p. 70 (although
it was figuratively true). His literal “coming on the clouds,” in
fulfillment of Daniel 7, had taken place about 40 years earlier.
But in a.p, 70 the tribes of Israel would see the destruction of
the nation as the result of His having ascended to the throne of
heaven, to receive His Kingdom.

The Gathering of the Elect

Finally, the result of Jerusalem's destruction will be Christ’s
sending forth of his “angels” to gather the elect. Isn’t this the
Rapture? No. The word angels simply means messengers (cf.
James 2:25), regardless of whether their origin is heavenly or
earthly; it is the context which determines whether these are
heavenly creatures being spoken of. The word often means
preachers of the gospel (see Matt. 11:10; Luke 7:24; 9:52; Rev.
1-3). In context, there is every reason to assume that Jesus is
speaking of the worldwide evangelism and conversion of the na-
tions which will follow upon the destruction of Israel.

Christ’s use of the word gather is significant in this regard.
The word, literally, is a verb meaning to synagogue; the meaning
is that with the destruction of the Temple and of the Old Cove-
