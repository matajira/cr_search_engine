Josephus on the Fall of Jerusalem 269

without regarding their own weapons, which are made of silver and
gold? Moreover, do the Arabians and Syrians now first of all begin to
govern themselves as they please, and to indulge their appetites in a
foreign war, and then, out of their barbarity in murdering men, and
out of their hatred ta the Jews, get it ascribed to the Romans?” — for
this infamous practice was said to be spread among some of his own
soldiers also, Titus then threatened that he would put such men to
death, if any of them were discovered to be so insolent as to do so
again: moreover, he gave it in charge to the legions, that they should
make a search after such as were suspected, and should bring them to
him; but it appeared that the love of money was too hard for all their
dread of punishment, and a vehement desire of gain is natural to men,
and no passion is so venturesome as covctousness, otherwise such pas-
sions have certain bounds, and are subordinate to fear; but in reality it
was God who condemned the whole nation, and turned every course
that was taken for their preservation to their destruction.

This, therefore, which was forbidden by Caesar under such a
threatening, was ventured upon privately against the deserters, and
these barbarians would go out still, and meet those that ran away be-
fore any saw them, and looking about them to see that no Roman
spied them, they dissected them, and pulled this polluted money out of
their bowels; which money was still found in a few of them, while yet a
great many were destroyed by the bare hope there was of thus getting
by them, which miserable treatment made many that were deserting to
return back again into the city.

6. But as for John, when he could no longer plunder the people,
he betook himself to sacrilege, and melted down many of the sacred
utensils, which had been given to the temple; as also many of those
vessels which were necessary for such as ministered about holy things,
the caldrons, the dishes, and the tables; nay, he did not abstain from
those pouring-vessels that were sent them by Augustus and his wife;
for the Roman emperors did ever both honor and adorn this temple;
whereas this man, who was a Jew, seized upon what were the dona-
tions of forcigners; and said to those that were with him, that it was
proper for them to use divine things while they were fighting for the
Divinity, without fear, and that such whose warfare is for the temple,
should live of the temple, on which accaunt he emptied the vessels of
that sacred wine and oil, which the priests kept to be poured on the
burnt-offerings, and which lay in the inner court of the temple, and
distributed it among the multitude, wha, in their anointing themselves
and drinking, used [each of them] above an hin of them: and here I
cannot but speak my mind, and what the concern I am under dictates
to me, and it is this:
