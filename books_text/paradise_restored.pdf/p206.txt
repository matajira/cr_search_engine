196 Paradise Restored

image, and had not received his mark upon their forehead and
upon their hand; and they lived and reigned with Christ for a
thousand years. The rest of the dead did not live until the thou-
sand years were completed. This is the First Resurrection.
Blessed and holy is the one who has a part in the First Resurrec-
tion; over these the Second Death has no power, but they will be
priests of God and of Christ and will reign with Christ for a
thousand years (Rev. 20:4-6).

In the first place, we can dispose of the Amillennial position
right away, by pointing out the obvious: this is a resurrection, a
rising again from the dead. Dying and going to heaven is
wonderful, but, for all its benefits, it is not a resurrection. This
Passage cannot be a description of the state of disembodied
saints in heaven; moreover, the context as a whole is set on earth
(cf. v. 7-9).

Second, however, this is not a bodily resurrection. John
gives us a clue that he means something special by calling it the
First Resurrection. What could this mean? We saw in a previous
chapter that there is only one bodily resurrection, at the end of
the world. To find the answer, we again go back to Genesis,
which tells us of the First Death: “And the Lorp God com-
manded the man, saying, ‘From every tree of the Garden you
may eat freely; but from the tree of the knowledge of good and
evil you shall not cat, for in the day that you eat from it you
shall surely die’ ” (Gen. 2:16-17). As we know, Adam and Eve
did not actually die physically on the day that they ate the for-
bidden fruit. But that was the Day of their Spiritual death, their
alienation from God. This Spiritual death was inherited by the
children of Adam and Eve, so that we all are born “dead in
trespasses and sins” (Eph. 2:1). The First Death is this Spiritual
death. And thus the First Resurrection is Spiritual as well:

God, being rich in mercy, because of His great love with which He
loved us, even when we were dead in our transgressions, made us
alive together with Christ (by grace you have been saved), and
raised us up with Him, and seated us with Him in the heavenly
places, in Christ Jesus (Eph. 2:4-6; ef, Col. 2:11-13; 1 John 3:14).

It is the bodily, physical Resurrection which takes place at
the Last Day, when “there shall certainly be a resurrection of
