The Beast and the False Prophet 179

Fifth, “the whole Land wondered after the Beast; and they
worshiped the Dragon, because he gave his authority to the
beast; and they worshiped the Beast, saying, ‘Who is like the
Beast, and who is able to make war against him?’ ” (13:3-4}.
John is not speaking of the worfd following the beast; the word
he uses here should be translated Lend, meaning Jsrael. We
know this because the context identifies his worshipers as ‘hose
who dwell on the Land (Rey. 13:8, 12, 14)—a technical phrase
used several times in Revelation to denote apostate Israel. In the
Greek Old Testament (the version used by the early Church), it
is a common prophetic expression for rebellious, idolatrous
Israel about to be destroyed and driven from the Land (Jer. 1:14;
10:18; Ezek. 7:7; 36:17; Hos. 4:1, 3; Joel 1:2, 14; 2:1; Zeph. 1:8),
based on its original usage in the historical books of the Bible
for rebellious, idolatrous pagans about to be destroyed and
driven from the Land (Num, 32:17; 33:52, 55; Josh. 7:9; 9:24;
Jud. 1:32; 2 Sam. 5:6; 1 Chron. 11:4; 22:18; Neh. 9:24), Israel
had become a nation of pagans, and was about to be destroyed,
exiled, and supplanted by a new nation. It is true, of course,
that Nero was loved all over the Empire as the benevolent pro-
vider of welfare and entertainment. But it is /srve/ in particular
which is condemned for Emperor-worship. Faced with a choice
between Christ and Caesar, they had proclaimed: We have ne
king but Caesar! (John 19:15). Their reaction to Caesar’s appar-
ently victorious war against the Church (Rev. 11:7) was awe and
worship. Israel sided with Caesar and the Empire against Christ
and the Church. Ultimately, therefore, they were worshiping the
Dragon, and for this reason Jesus Himself called their worship
assemblies synagogues of Satan (Rev. 2:9; 3:9).

Sixth, the Beast was given “authority to act for forty-two
months” (13:5), “to make war with the saints and to overcome
them” (13:7). The period of 42 months (three-and-one-half
years—a broken seven) is a symbolic figure in prophetic lan-
guage, signifying a time of sadness, when the enemies of God
are in power, or when judgment is being poured out (taken from
the period of drought between Elijah’s first appearance and the
defeat of Baal on Mount Carmel). Its prophetic usage is not
primarily literal, although it is interesting that Nero’s persecu-
tion of the Church did in fact last a full 42 months, from the
middle of November 64 to the beginning of June 68.
