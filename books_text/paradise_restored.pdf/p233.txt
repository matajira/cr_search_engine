APPENDIX A
THE ESCHATOLOGY OF DOMINION: A SUMMARY

For those who like their eschatology wrapped up in a neat
package, I have fisted 45 of the major arguments of this book,
in the general order in which they were presented (chapter
numbers are in parentheses). The reader should consider each
one in the light of the Biblical arguments in the text of the book.
Following these “Theses on Hope” is a brief section answering
some of the common misunderstandings of the eschatology of
dominion.

Theses on Hope

1, The Bible teaches us to have hope, not despair; to expect
victory and dominion for the gospel, not flight and defeat. (1)

2. Biblical prophecy is written in both literal and symbolic
language. The choice is not between “literalism” and “sym-
bolism,” but between a Biblical and a speculative method of in-
terpreting the Bible, (2)

3. Salvation is re-creation. In redemption, Jesus Christ
restores man to the image of God. (3)

4. Salvation and its blessings are presented in the Bible as
definitive, progressive, and final. (3)

5. We are not saved out of our environment; rather, salva-
tion works to restore the earth as a whale, God’s Holy Maun-
tain (the Garden} will grow until it fills the entire world. (3-7)

6. God blesses obedience and curses disobedience; this pat-
tern will become dominant as history progresses. (3-7)

7. Through generations of obedience, the godly will increas-
ingly become competent and powerful, while the ungodly will
grow weak and impotent. (3-7)

8. The wicked are “raptured” first (i.c., driven out of the
earth and disinherited), as the righteous increasingly come into

223
