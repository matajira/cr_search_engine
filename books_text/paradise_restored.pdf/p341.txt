Publisher's Epilogue 331

. The transcendence and immanence of God
. Authority/hierarchy of God’s covenant

. Biblical law/ethics/dominion

. Judgment/oath: blessings and cursings

. Continuity/inheritance

Vaud

This may seem too intellectual, but after you have read Sut-
ton’s book, it becomes almost second nature. Let me put it in
simpler terms:

. Who's in charge here?

. To whom do I report?

. What are the rules?

. What happens to me if | obey (disobey)?
. Does this outfit have a future?

Ubwre

Simple, isn’t it? Yet it has implications beyond your wildest
imagination. Here is the key that unlocks the structure of
human government. Here is the structure that Christians can use
to analyze church, state, family, and numerous other non-cave-
nantal but contractual institutions. Gary DeMar shows this
clearly in his book, Ruler of the Nations (Dominion Press,
1987). With this outline in your mind, you can begin to unlock
the fundamental message of the Old Testament prophets and the
Book of Revelation.

With his insights concerning the covenant, Sutton completed
the outline for a major modification of Christian Reconstruc-
tion theology. This modification has unfortunately become
known as “Tyler theology,” and we are stuck with the phrase, so
far. (While its outlines were developed initially in Tyler, Texas in
the mid-1980’s, there is no assurance that this geographical iden-
tification will make contemporary sense in years to come, any
more than Calvin’s Geneva is relevant to today’s Geneva, except
as a fact of history and a tourist attraction.)

David Chilton’s Paradise Restored and The Days of Ven-
geance are by far the most eloquent applications of this theolog-
ical perspective in the field of eschatology. I doubt that they will
be exceeded in style, brilliance, and relevance during my lifetime
(if ever). fam biased, of course; 1 am one of the two primary
publishers of dominion theology. (Geneva Ministries is the
other, also located in Tyler.)
