264 Paradise Restored

and took pieces of what they were eating, almost up out of their very
throats, and this by force; the old men, who held their food fast, were
beaten; and if the women hid what they had within their hands, their
hair was torn for so doing; nor was there any commiseration shown
either to the aged or to infants, but they lifted up children from the
ground as they hung upon the morsels they had gotten, and shook
them down upon the floor; but still were they more barbarously cruel
to those that had prevented their coming in, and had actually swallowed
down what they were going to seize upon, as if they had been unjustly
defrauded of their right.

They also invented terrible methods of torment to discover where
any food was, and they were these: to stop up the passages of the privy
parts of the miserable wretches, and to drive sharp stakes up their fun-
daments! and a man was forced to bear what itis terrible even to hear,
in order to make him confess that he had but one loaf of bread, or that
he might discover a handful of barley-meal that was concealed; and
this was done when these tormentors were not themselves hungry; for
the thing had been less barbarous had necessity forced them to it; but
this was done to keep their madness in exercise, and as making
preparations of provisions for themselves for the following days.

These men went also to meet those that had crept out of the city by
night, as far as che Roman guards, to gather some plants and herbs
that grew wild; and when those people thought they had got clear of
the enemy, these snatched from them what they had brought with
them, even while they had frequently entreated them, and that by call-
ing upon the tremendous name of God, to give them back some part
of what they had brought; though these would not give them the least
crumb; and they were to be well contented thai they were only spoiled,
and not slain at the same time.

The Worst Generation
(vix:5)

5. It is therefore impossible to go distinctly over every instance of
these men’s iniquily. I shall therefore speak my mind here at once
briefly: — That neither did any other city ever suffer such miseries, not
did any age ever breed a generation more fruitful in wickedness than
this was, from the beginning of the world... .

The Rate of Crucifixions: 500 Per Day
(v:xiz]-2)
1. So now Titus’s banks were advanced a great way, notwithstand-
ing his soldiers had been very much distressed from the wall. He then
sent a party of horsemen, and ordered they should lay ambushes for
