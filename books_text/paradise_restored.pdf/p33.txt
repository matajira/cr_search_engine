3
THE PARADISE THEME

The story of Eden contains three basic ideas, concepts which
confront us repeatedly as we study the Bible: Creation, the Fall,
and Redemption in Christ. As these ideas are developed
throughout the history of salvation, we see familiar images and
actions reappearing and patterns beginning to take shape, until
the last book of the Bible finally answers all the questions that
began in the first book. God’s self-revelation is a coherent, con-
sistent whole; and it comes to us in very beautiful literary forms.
Our proper understanding of the message will be inadequate
unless we attempt to understand and appreciate the form in
which that message is communicated. By beginning our study
where the Bible itself begins, we can more readily understand
not only the Book of Revelation, but the Bible itself —wéAy the
writers of the Bible said what they said in the way they said it.
And our reasons for doing so are that we might more fully trust
in God’s promises, obey His commands, and inherit His bless-
ings.

The Nature of Salvation

One of the basic themes of Scripture is that salvation
restores man to his original purpose. In the beginning Ged cre-
ated man in His own image, in order that man would have do-
minion (Gen. 1:26-28), That task of dominion began in the
Garden of Eden, but it was not supposed to end there, for man
was ordered to have dominion over the whole earth: Adam and
Eve (and their children) were to extend the blessings of Paradise
throughout the entire world, But when man rebelled, he lost the
ability to have godly dominion, because he lost fellowship with
his Creator. While fallen man is still the image of God (Gen.
9:6), he is now a naked image (Gen. 3:7), for he has lost his orig-

23
