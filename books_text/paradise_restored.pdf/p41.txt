The Holy Mountain 31

mountaintop). This follows from the fact that the source of
water for the world was in Eden; the river simply cascaded down
the mountain, parting into four heads as it traveled. Further-
more, when God speaks to the king of Tyre (referring to him as
if he were Adam, in terms of Man’s original calling) He says:
“You were in Eden, the Garden of God... . You were on the
holy mountain of God” (Ezek. 28:13-14).

That Eden was the original “holy mountain” explains the sig-
nificance of God’s choice of mountains as sites for His redemp-
tive acts and revelations, The substitutionary atonement in place
of Abraham’s seed took place on Mount Moriah (Gen. 22:2). It
was also on Mount Moriah that David saw the Angel of the
Lord standing, sword in hand, ready to destroy Jerusalem, until
David built an altar there and made atonement through sacrifice
(1 Chron. 21:15-17). And on Mount Moriah Solomon built the
Temple (2 Chron. 3:1). God’s gracious revelation of His pres-
ence, His covenant, and His law was made on Mount Sinai. Just
as Adam and Eve had been barred from the Garden, the people
of Israel were forbidden to approach the holy mountain, on
pain of death (Ex. 19:12; cf. Gen. 3:24). But Moses (the Mediator
of the Old Covenant, Gal. 3:19), the priests, and the 70 elders of
the people were allowed to meet God on the Mountain (after
making an atoning sacrifice), and there they ate and drank com-
munion before the Lord (Ex. 24:1-11). It was on Mount Carmel
that God brought His straying people back to Himself through
sacrifice in the days of Elijah, and from whence the ungodly in-
truders into His Garden were taken and destroyed (1 Kings 18; in-
terestingly, carmel is a Hebrew term for garden-land, plantation,
and orchard). Again, on Mount Sinai (also called Horeb) God re-
vealed His saving presence to Elijah, and re-commissioned him as
His messenger to the nations (1 Kings 19).

In His first major sermon, the Mediator of the New Cove-
nant delivered the law again, from a mountain (Matt, 5:1f.).
His official appointment of His apostles was made on a moun-
tain (Mark 3:13-19). On a mountain He was transfigured before
His disciples in a blinding revelation of His glory (recalling asso-
ciations with Sinai, Peter calls this “the holy mountain,” in 2
Pet, 1:16-18). On a mountain He gave His final announcement
of judgment upon the faithless covenant people (Matt. 24).
After the Last Supper, He ascended a mountain with His disci-
