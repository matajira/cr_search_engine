Josephus on the Fail of Jerusalem 283

had hitherto been so insolent and arrogant in their wicked practices, to
be cast down and to tremble, insomuch that it would pity one’s heart
to observe the change that was made in those vile persons.

Accordingly they ran with great violence upon the Roman wall
that encompassed them, in order to force away those that guarded it,
and to break through it, and get away; but when they saw that those
who had formerly been faithful to them, had gone away (as indeed
they were fled whithersoever the great distress they were in persuaded.
them to flee) as also when those that came running before the rest told
them that the western wall was entirely overthrown, while others said
the Romans were gotten in, and others that they were near, and look-
ing out for them, which were only the dictates of their fear which im-
posed upon their sight, they fell upon their faces, and greatly lamented
their own mad conduct; and their nerves were so terribly loosed, that
they could not flee away; and here one may chiefly reflect on the power
of God exercised upon these wicked wretches, and on the good for-
tune of the Romans; for these tyrants did now wholly deprive them-
selves of the security they had in their own power, and came down
from those very towers of their own accord, wherein they could have
never been taken by force, nor indeed by any other way than by
famine.

And thus did the Romans, when they had taken such great pains
about weaker walls, get by good fortune what they could never have
gotten by their engines; for three of these towers were too strong for
all mechanical engines whatsoever; concerning which we have treated
of before.

5. So they now left these towers of themselves, or rather they were
ejected out of them by God himself, and fled immediately to that
valley which was under Siloam, where they again recovered themselves
out of the dread they were in for a while, and ran violently against that
part of the Roman wall which lay on that side; but as their courage
was too much depressed to make their attacks with sufficient force,
and their power was now broken with fear and affliction, they were
repulsed by the guards, and dispersing themselves at distances from
each other, went down into the subterranean caverns.

So the Romans being now become masters of the walls, they both
placed their ensigns upon the towers, and made joyful acclamations
for the victory they had gained, as having found the end of this war
much lighter than its beginning; for when they had gotten upon the
last wail, without any bloodshed, they could hardly believe what they
found to be true; but seeing nobody to oppose them, they stood in
doubt what such an unusual solitude could mean. But when they went
in numbers into the lanes of the city, with their swords drawn, they
