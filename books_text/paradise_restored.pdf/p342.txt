332 Paradise Restored

Chilton’s commentary on Revelation reveals that the entire
book is a long worship service. As you read it, you begin to see
the genius of the historic liturgy of the church. You also begin to
see how far modern worship services have departed from the
modei in Revelation. If the church is ever to regain its authority
in the world, it will have to pay more attention to the structure
of the worship service. But most important, Chilton ties the
structure of the worship service to the structure of the covenant.
This is what makes The Days of Vengeance a classic. While his
style is brilliant and penetrating, the great strength of the book
is its covenant-liturgical structure. It will be remembered
because of this outline, if only because the outline, once under-
stood, is almost impossible to forget.

This does not mean, of course, that Chilton agrees with
every jot and tittle of Tyler’s version of dominion theology. He
is his own man. He no longer lives in Tyler, nor is he on my pay-
roll. But what it does mean is that there are at present no more
effective, path-breaking statements of “Tyler's” theological
method than David Chilton’s books on eschatology. If the do-
minion approach to the Bible becomes widespread, it should be
remembered that it was David Chilton who first broke through
to the Christian public at large with this unique system of bibli-
cal interpretation, This is another reason why this book is so
important.

What should be an inspiration to any dedicated Christian
layman is the knowledge that another layman with a bachelor’s
degree in history and only one year of seminary wrote two of the
most important works in eschatology in the history of the
church—perhaps tHe most important. It makes a person
wonder: Why didn’t some distinguished seminary professor
write them? I believe the answer is simple: if distinguished theol-
ogy professors write at all, they write mainly to impress other
distinguished theology professors, and this is the kiss of death.

Optimism Is Not Enough

Let me say, however, that if all a person gains from the
Christian Reconstruction movement in general is its optimistic
eschatology, then he is skating on thin ice. Optimism is not
enough. In fact, optimism alone is highly dangerous, The Com-
