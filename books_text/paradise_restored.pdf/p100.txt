90 Paradise Restored

pangs” (Matt. 24:8). In themselves, Jesus warned, they were
not to be taken as signals of an imminent end; thus the disciples
should guard against being misled on this point (v, 4). These
“beginning” events, marking the period between Christ's resur-
rection and the Temple’s destruction in a.p, 70, were as follows:

1. False Messiahs. “For many will come in My name, saying, ‘1
am the Christ,’ and will mislead many” {v. 5).

2. Wars. “And you will be hearing of wars and rumors of wars;
see that you are not frightened, for those things must take place,
but that is not yet the end, For nation will rise against nation,
and kingdom against kingdom” (v, 6-7a),

3. Natural disasters. “And in various places there will be
famines and earthquakes. But all these things are merely the be-
ginning of birth pangs” (v. 7b-8).

Any one of these occurrences might have caused Christians
to feel that the end was immediately upon them, had not Jesus
warned them that such events were merely general tendencies
characterizing the final generation, and not precise signs of the
end. The next two signs, while they still characterize the period
as a whole, do bring us up to a point near the end of the age:

4. Persecution. “Then they will deliver you up to tribulation,
and will kill you, and you will be hated by all nations on account
of My name” (v. 9).

5. Apostasy. “And at that time many will fall away and will
betray one another and hate one another. And many false
prophets will arise, and will mislead many. And because
lawlessness is increased, the love of many will grow cold, But the
one who endures to the end, he shall be saved” (v. 10-13).

The last two items on the list are much more specific and
identifiable than the preceding signs. These would be the final,
definitive signs of the end—one the fulfillment of a process, and
the other a decisive event:

6. Worldwide evangelization. “And this gospel of the Kingdom
shall be preached in the whole world for a witness to all the na-
tions, and then the end shall come” (v. 14).

At first glance, this seems incredible. Could the gospel have
