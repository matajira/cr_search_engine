286 Paradise Restored

of the punishment intended for the Jews in honour of him: for the
number of those that were now slain in fighting with the beasts, and
were burnt, and fought with one another, exceeded two thousand five
hundred. Yet did ali this seem to the Romans, when they were thus
destroying ten thousand several ways, to be a punishment beneath
their deserts. After this, Caesar came to Berytus, which is a city of
Phoenicia, and a Roman colony, and staid there a longer time, and ex-
hibited a stil! more pompous solemnity about his father’s birth-day,
both in the magnificence of the shows, and in the other vast expenses
he was at in his devices thereto belonging; so that a great multitude of
the captives were here destroyed after the same manner as before.

Suicide at Masada”
Gii:ix:1)

1. Now as Eleazar was proceeding on in his exhortation, they all
cut him off short, and made haste to do the work, as full of an uacon-
querable ardor of mind, and moved with a demoniacal fury. So they
went their ways, as one still endeavoring to be before another, and as
thinking that this eagerness would be a demonstration of their courage
and good conduct, if they could avoid appearing in the last class; so
great was the zeal they were in to slay their wives and children, and
themselves also!

Nor, indeed, when they came to the work itself, did their courage
fail them, as one might imagine it would have done, but they then held
fast the same resolution, without wavering, which they had upon the
hearing of Eleazar’s speech, while yet every one of them still retained
the natural passion of love to themselves and their families, because
the reasoning they went upon appeared to them to be very just, even
with regard to those that were dearest to them; for the husbands
tenderly embraced their wives, and took their children into their arms,
and gave the longest parting kisses to them, with tears in their eyes.
Yet at the same time did they complete what they had resolved on, as if
they had been executed by the hands of strangers, and they had
nothing else for their comfort but the necessity they were in of doing
this execution to avoid that prospect they had of the miseries they were
to suffer from their enemies. Nor was there at length any one of these
men found that scrupled to act their part in this terrible execution, but
every one of them dispatched his dearest relations. Miserable men in-
deed were they! whose distress forced them to slay their own wives and
children with their own hands, as the lightest of those evils that were
before them.

So they being not able to bear the grief they were under for what
they had done any longer, and esteeming it an injury to those they had
