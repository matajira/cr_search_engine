26 Paradise Restored

Thus says the Lord GOD, “On the day that I cleanse you from
all your iniquities, I will cause the cities to be inhabited, and the
waste places will be rebuilt. And the desolate land will be culti-
vated instead of being a desolation in the sight of all who passed
by. And they will say, ‘This desolate land has become like the
Garden of Eden; and the waste, desolate, and ruined cities are
fortified and inhabited.’ Then the nations that are left round
about you will know that I, the Lorp, have rebuilt the ruined
places and planted that which was desolate; I, the Lorp, have
spoken and will do it (Ezek. 36:33-36).

But there is much more in these prophecies (and others) re-
garding the restoration of Eden than we might notice at first
glance. Indeed, there are many, many passages of Scripture
which speak in terms of the Edenic patterns which do not men-
tion Eden by mame. The Paradise Theme runs throughout the
whole Bible, from Genesis to Revelation; but in order to recog-
nize it we must first familiarize ourselves with what God’s Word
says about the original Garden itself. God has gone to the trou-
ble to tell us some very specific information about the Garden,
and the rest of Scripture is built on this foundation, referring
back to it regularly. Note well: this study is not merely a collec-
tion of trivia, of “strange and interesting facts about the Bible”
(e.g., the sort of irrelevant data that is often to be found in the
“encyclopedia” sections of big family Bibles). It is, 1 repeat, a
major Biblical theme, dramatically illuminating the message of
the Book of Revelation—and, by the way, helping us to under-
stand the message of the Bible as a whole. Thus, in the chapters
to follow, we will examine the various characteristics of the
Garden of Eden, taking special notice of how each of these
becomes a “sub-theme” in itself, in terms of the general theme
of Eden-restoration in salvation.
