260 Paradise Restored

Dagon, and as also did that entire nation of those that carried it away,
how they were smitten with a loathsome distemper in the secret parts
of their bodies, when their very bowels came down together with what
they had eaten, till those hands that stole it away were obliged to bring
it back again, and that with the sound of cymbals and timbrels, and
other oblations, in order to appease the anger of God for their viola-
tion of His holy ark. [tu was God who then became our general, and ac-
complished these great things for our fathers, and this because they
did not meddle with war and fighting, but committed it to him to
Judge about their affairs.

“When Sennacherib, king of Assyria, brought along with him all
Asia, and encompassed this city round with his army, did he fall by the
hands of men? were not those hands lifted up to God in prayers, with-
out meddling with their arms, when an angel of God destroyed that
prodigious army in one night? when the Assyrian king, as he rose next
day, found a hundred fourscore and five thousand dead bodies, and
when he, with the remainder of his army, fled away from the Hebrews,
though they were unarmed, and did not pursue them! You are also ac-
quainted with the slavery we were under at Babylon, where the people
were captives for seventy years; yet were they not delivered into free-
dom again before God made Cyrus his gracious instrument in bringing
it about; accordingly they were set free by him, and did again restore
the worship of their Deliverer at his temple.

“And, to speak in general, we can produce no example wherein our
fathers got any success by war, or failed of success, when without war
they committed themselves to God. When they staid at home they con-
quered, as pleased their Judge; but when they went out to fight they
were always disappointed: for example, when the king of Babylon
besieged this very city, and our king Zedekiah fought against him,
contrary to what predictions were made to him by Jeremiah the
prophet, he was at once taken prisoner, and saw the city and the tem-
ple demolished. Yet how much greater was the moderation of that
king, than is that of your present governors, and that of the people
then under him, than is that of you at this time! for when Jeremiah
cried out aloud, how very angry God was at them, because of their
transgressions, and told them that they should be taken prisoners,
unless they would surrender up their city, neither did the king nor the
people put him to death; but for you (to pass over what you have done
within the city, which I am not able to describe, as your wickedness
deserves) you abuse me,and throw darts at me who only exhort you to
save yourselves, as being provoked when you are put in mind of your
sins, and cannot bear the very mention of those crimes which you
every day perpetrate, For another example, when Antiochus, who was
