The Fiery Cloud 61

Simple restoration to Eden is never all that is involved in sal-
vation, just as it was not God’s plan for Adam and his posterity
simply to remain in the Garden, They were to go into all the
world, bring the created potentiality of earth to full fruition.
The Garden of Eden was a headquarters, a place to start. But
godly rule by King Adam was to encompass the entire world.
Thus, the Second Adam’s work is not only restorative (bringing
back Eden) but consummative: He brings the world into the
New Jerusalem.

Paradise: Restored and Consum#ated

Throughout redemptive history, as God called His people to
the restored Paradise, he brought them into His City. We can see
this in the contrast between the rebellious, autonomous city-
builders of Genesis 11 and Abraham, who journeyed to the
Promised Land “looking for the City which has foundations,
whose architect and builder is God” (Heb. 11:10); and Scripture
assures the New Covenant community that we “have come to
Mount Zion and to the City of the the living God, the heavenly
Jerusalem” (Heb. 12:22).

In the final vision of the Revelation, John is shown the ful-
fillment of the cultural mandate, the full restoration and con-
summation of Eden: “And he carried me away in the Spirit to a
great and high mountain, and showed me the holy city, Jerusa-
Jem, coming down out of heaven from God, having the glory of
God” (Rev. 21:10-11). Like the Holy of Holies, the City’s length,
width, and height are equal (Rev. 21:16; 1 Kings 6:20): there is no
Temple within the City, for the City itself is the inner sanctuary
(cf. Eph. 2:19-22); and, at the same time, “the Lord God, the
Almighty, and the Lamb, are its Temple” (Rev. 21:22). The City
is ablaze with the brilliant glory of God, enlightening the na-
tions (Rev, 21:11-27), and through its main street flows the River
of Life, as it flowed originally from the Garden of Eden (Rev.
22:1-2); “and there shall no longer be any Curse” (Rev, 22:3).
Moreover, we are not to regard this vision as wholly future, for
our Lord has said much the same about us in this age: “You are
the light of the world. A City set on a Hill cannot be hidden. . . .
Let your light shine before men... .” (Matt. 5:14-16).

In many more ways, Edenic imagery is taken up and expanded
in the New Testament, which records the fulfillment of the prom-
