296 Paradise Restored

Terry, Milton. Biblical Apocatyptics: A Study af the Most
Notable Revelations af God and of Christ in the Canonical
Scriptures. New York: Eaton and Mains, 1898.

Vanderwaal, Cornelis. Search the Scriptures. Ten vols. St.
Catherines, Ontario: Paideia Press, 1979.

Wallace, Foy E., Jr. The Book of Revelation. Ft. Worth, TX:
Foy E. Wallace Jr. Publications, 1966.

Wilcock, Michael. J Saw Heaven Opened: The Message of Rev-
elation. Downers Grove, IL: InterVarsity Press, 1975.
