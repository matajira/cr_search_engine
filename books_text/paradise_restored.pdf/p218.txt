208 Paradise Restored

You will be far from oppression, for you will not fear;

And from terror, for it will not come near you. . . .

No weapon that is formed against you shall prosper;

And every tongue that accuses you in judgment you will condemn.
This is the heritage of the servants of the Lorp,

And their vindication is from Me, declares the Lorp.

(Isa. 54:13-17)

John saw that in this new City of God there is no Temple,
“for the Lord God, the Almighty, and the Lamb, are its Temple.
And the City has no need of the sun or of the moon to shine
upon it, for the glory of God has illumined it, and its lamp is the
Lamb” (Rev. 21:22-23). This is also based on Isaiah (see Isa.
60:1-3, 19-20), emphasizing that the Church is lighted by the
Glory of God, indwelled by the Cloud, shining with the original
Light. This is the City on the Hill (Matt. 5:14-16), the light of the
world, shining before men so that they will glorify God the
Father. Drawing from the same passage in Isaiah (Isa. 60:4-18),
John tells of the City’s influence in the nations of the world:

And the nations shall walk by its light, and the kings of the earth
shall bring their glory into it. And in the Daytime (for there shall
be no Night there) its gates shall never be closed; and they shall
bring the glory and the honor of the nations into it; and nothing
unclean and no one who practices abomination and lying, shall
ever come into it, but only those whose names are written in the
Lamb’s Book of Life (Rev, 21:24-27; cf. Ps. 22:27; 66:4; 86:9;
Isa. 27:6; 42:4; 45:22-23; 49:5-13; Hag. 2:7-8).

This is written of a time when the nations still exist as na-
tions; yet the nations are all converted, flowing into the City and
bringing their treasures into it. As the light of the gospel shines
through the Church to the world, the world is converted, the na-
tions are discipled, and the wealth of the sinners becomes in-
herited by the just. This is a basic promise of Scripture from be-
ginning to end. This is the pattern of history, the direction in
which the world is moving. This is our future, the heritage of
generations to come.

The River of Life

We look forward to the turning back of the Curse in every
area of life, both in this world and the next, as the gospel flows
