The New Creation 205

blessings of Deuteronomy 28 in what is probably their greatest
earthly fulfillment. Thus, when John tells us that he saw “a new
heaven and earth,” we should recognize that the primary
significance of that phrase is symbolic, and has to do with the
blessings of salvation,

John next saw “the Holy City, New Jerusalem, coming down
out of heaven from God, made ready as a Bride adorned for her
Husband” (Rev. 21:2), No, it’s not a space station. It is some-
thing which should be much more thrilling: it is the Church. The
Bride is not just iz the City; the Bride is the City (cf. Rev.
21:9-10), We are in the New Jerusalem now, Proof? The Bible
categorically tells us: “You have come to Mount Zion and to the
City of the living God, the heavenly Jerusalem, and to myriads
of angels, to the general assembly and Church of the firstborn
who are enrolled in heaven . . .” (Heb. 12:22-23; cf. Gal. 4:26;
Rev. 3:12), The New Jerusalem is a present reality; it is said to be
coming down from heaven because the origin of the Church is
heavenly. We have been “born from above” (John 3:3) and are
now citizens of the Heavenly City (Eph. 2:19; Phil. 3:20).

This thought is expanded in John’s further statement. He
heard a loud voice from the throne, saying: “Behold, the Taber-
nacle of God is among men, and He shall dwell among them,
and they shall be His people, and God Himself shall be among
them” (Rev. 21:3). Like Paul, John connects these two concepts:
we are citizens of heaven, and we are God’s dwelling place, His
holy Temple (Eph. 2:19-22). One of the Edenic blessings God
promised in Leviticus was, “I will make My Tabernacle among
you” (Lev. 26:11); this is fulfilled in the New Testament Church
(2 Cor. 6:16). The voice John heard continued:

“And He shall wipe away every tear from their eyes; and there
shall no longer be any death; there shall no longer be any mourn-
ing, or crying, or pain; the first things have passed away.” And He
who sits on the throne said, “Behold, I am making all things new.”
And He said, “Write, for these words are faithful and true.” And
He said to me, “It is done. 1 am the Alpha and the Omega, the Be-
Binning and the End. I will give to the one who thirsts from the
spring of the water of life without cost” (Rev. 21:4-6).

Ultimately, this will be fulfilled in heaven to the utmost ex-
tent. But we must recognize that if is true already, God has
