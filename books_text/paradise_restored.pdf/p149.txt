The Day of the Lord 139

of Christ, the resurrection of all men, and the general judgment
to be fundamental, non-negotiable articles of the Christian
faith. The closing words of the Athanasian Creed (one of the
three universal creeds of the faith) underscore the importance of
these truths:

He ascended into heaven; He sitteth on the right hand of the
Father, God Almighty; from whence He shall come to judge the
quick and the dead.

At whose coming all men shall rise again with their bodies and
shall give an account of their own works.

And they that have done good shall go into life everlasting; and
they that have done evil, into everlasting fire.

This is the catholic faith, which except a man believe faithfully,
he cannot be saved.

This basic tenet of the Church Universal is solidly based on
Scripture. While there have been many “Days of the Lord” in
history, the Bible assures us that there is a “Last Day” which is to
come, the Final Judgment, when all accounts will be settled and
both just and unjust receive their eternal rewards. Each time He
used the term, Jesus inseparably connected “the Last Day” with
another event:

Thave come down from heaven, not to do My own will, but the
will of Him who sent Me. And this is the will of Him who sent
Me, that of all that He has given Me I lose nothing, but raise it
up on the Last Day. For this is the will of My Father, that
everyone who beholds the Son and believes in Him, may have
eternal life; and J Myself will raise him up on the Last Day (John
6:38-40).

No one can come to Me, unless the Father who sent Me draws
him; and / will raise him up on the Last Day (John 6:44).

He who eats My flesh and drinks My blood has eternal life, and J
will raise him up on the Last Day (John 6:54).

The Resurrection, therefore, is an event inextricably bound
up in the events of the Last Day, the final Day when the judg-
ment of the Spirit in the Cloud will be absolutely comprehensive
and complete, when God’s final and ultimate verdict is pro-
nounced upon all creation. That is the Day when the dead will
