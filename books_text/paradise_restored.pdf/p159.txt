PART IV

STUDIES IN THE BOOK OF REVELATION

Forth rushed with whirlwind sound
The chariot of Paternal Deity,
Flashing thick flames, wheel within wheel undrawn,
Itself instinct with spirit, but convoyed.
By four Cherubic shapes, four faces each
Had wondrous, as with stars their bodies all
And wings were set with eyes, with eyes the wheels
Of beryl, and careering fires between;
Over their heads a crystal firmament,
‘Whereon a sapphire throne, inlaid with pure
Amber, and colours of the show’ry arch.
He in celestial panoply all armed
Of radiant Urim, work divinely wrought,
Ascended, at his right hand Victory
Sat eagle-winged, beside him hung his bow
And quiver with three-bolted thunder stored,
And from about him fierce effusion rowled
Of smoke and bickering flame, and sparkles dire.

John Milton, Paradise Lost [6.749-66]
