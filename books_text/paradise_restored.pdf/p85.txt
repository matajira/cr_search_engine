The Coming of the Kingdom 75

toriously, the clear winner by a mile; Rome and Jerusalem didn’t
get past the starting gate, The last twenty centuries have witnessed
progress that only the willfully blind could deny. Has the yeast
of the Kingdom spread everywhere? Of course not; not yer. But
it will. God has predestined us for victory.
