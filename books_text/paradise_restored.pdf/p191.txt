The Beast and the False Prophet 181

grasped it instantly. The numerical values of the Hebrew letters
in Neron Kesar (Nero Caesar) are:

3=50 \=200 J=6 3=50 > =100 D-60 “-200
thus:

ADP, 317) = 66s

It is significant that all early Christian writers, even those
who did not understand Hebrew and were therefore confused
by the number 666, connected the Roman Empire, and especially
Nero, with the Beast. There should be no reasonable doubt
about this, John was writing to first-century Christians, warning
them of things that were “shortly” to take place. They were
engaged in the most crucial battle of history, against the Dragon
and the evil Empire which he possessed. The purpose of the
Revelation was to comfort the Church with the assurance that
God was in control, so that even the awesome might of the
Dragon and the Beast would not stand before the armies of
Jesus Christ. The number of Man is six (Gen, 1:27, 31); Christ
was wounded in His heel on the sixth day (Friday)— yet that is
the day He crushed the Dragon’s head. At his most powerful,
John says, Nero is just a six, or a series of sixes; never a seven.
His plans of world dominion will never be fulfilled, and the
Church will overcome.

The Beast from the Land

Just as the Beast from the sea was in the image of the
Dragon, so we see another creature in Revelation 13 who is in
the image of the Beast. John saw this one “coming up out of the
Land” (13:11), arising from within Israel itself. In Revelation
19:20, we are told the identity of this Land Beast: he is “the False
Prophet.” As such, he represents what Jesus had foretold would
take place in Israel’s last days: “Many will come in My name,
saying, ‘I am the Christ,’ and will mislead many. . . . Many false
prophets will arise, and will mislead many” (Matt. 24:5, 11). The
rise of the false prophets paralleled that of the antichrists; but
whereas the antichrists had apostatized into Judaism from
within the Church, the false prophets were Jewish religious
leaders who sought to seduce Christians from the outside.
