Rn
THE RISE OF ANTICHRIST

According to Jesus’ words in Matthew 24, one of the increas-
ing characteristics of the age preceding the overthrow of Israel
was to be apostasy within the Christian Church. This was men-
tioned earlier, but a more concentrated study at this point will
shed much light on a number of related issues in the New
Testament —issues which have often been misunderstood.

We generally think of the apostolic period as a time of
tremendously explosive evangelism and church growth, a
“golden age” when astounding miracles took place every day.
This common image is substantially correct, but it is flawed by
one glaring omission. We tend to neglect the fact that the early
Church was the scene of t#e most dramatic outbreak of heresy in
world history.

The Great Apostasy

The Church began to be infiltrated by heresy fairly early in
its development. Acts 15 records the meeting of the first Church
Council, which was convened in order to render an authorita-
tive ruling on the issue of justification by faith (some teachers
had been advocating the false doctrine that one must keep the
ceremonial laws of the Old Testament in order to be justified).
The problem did not die down, however; years later, Paul had
to deal with it again, in his letter to the churches of Galatia. As
Paul told them, this doctrinal aberration was no minor matter,
but affected their very salvation: it was a “different gospel,” an
utter distortion of the truth, and amounted to a repudiation of
Jesus Christ Himself. Using some of the most severe ter-
minology of his career, Paul pronounced damnation upon the
“false brethren” who taught the heresy (see Gal. 1:6-9; 2:5, 11-21;
3:1-3; 5:1-12).

107
