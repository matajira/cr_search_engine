222 Paradise Restored

of increasing godliness ahead of it, before the Second Coming
of Christ.

T am not interested in setting dates. [ am not going to try to
figure out the date of the Second Coming. The Bible does not re-
veal it, and it is none of our business. What the Bible does reveal
is our responsibility to work for God’s Kingdom, our duty to
bring ourselves, our families, and all our spheres of influence
under the dominion of Jesus Christ. “The secret things belong to
the Lorp our God, but the things revealed belong to us and to
our sons forever, that we may observe all the words of this law”
(Deut. 29:29). God has not told us when the Second Coming
will occur, But He Aas told us that there is a lot of work to be
done, and He expects us to get to it.

What would you say if you hired a worker, gave him detailed
instructions, and all he did was to sit around wondering when
the quitting bell will ring? Would you regard him as a faithful
worker? Does God regard you as a faithful worker for His King-
dom? I repeat: the purpose of prophecy is ethical. It is God’s
assurance that history is under His control, that He is working
out His eternal purposes in every event, and that His original
plan for His creation will be fulfilled. He has placed us into the
great war for world history, with the absolute guarantee that we
will win. Even if He has to make the whole universe stand still
for us (Josh. 10:12-13), the day will last long enough for us to
achieve victory. Time is on our side. The Kingdom has come,
and the world has begun again.

Now: Get to work.
