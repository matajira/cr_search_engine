24
FULFILLING THE GREAT COMMISSION

“Go therefore and disciple ail the nations, baptizing them in
the name of the Father and of the Son and of the Holy Spirit,
teaching them to observe all that I commanded you; and lo, I
am with you always, even to the end of the age” (Matthew
28:19-20).

The Great Commission to the Church does not end with sim-
ply witnessing to the nations. Christ’s command is that we disci-
pile the nations —ai/ the nations. The kingdoms of the world are
to become the kingdoms of Christ. They are to be discipled,
made obedient to the faith. This means that every aspect of life
throughout the world is to be brought under the lordship of
Jesus Christ: families, individuals, business, science,
agriculture, the arts, law, education, economics, psychology,
philosophy, and every other sphere of human activity. Nothing
may be left out. Christ “must reign, until He has put all enemies
under His feet” (1 Cor. 15:25), We have been given the responsi-
bility of converting the entire world.

In his second letter to the church at Corinth, St. Paul out-
lined @ strategy for worldwide dominion:

For though we walk in the flesh, we do not war according to the
flesh. For the weapons of our warfare are not of the flesh, but
mighty in God for the destruction of fortresses. We are destroy-
ing speculations and every lofty thing raised up against the
knowledge of God, and we are taking every thought captive to
the obedience of Christ; and we are ready to punish all disobe-
dience, once your obedience is complete (2 Cor. 10:3-6).

As Paul observes, the army of Christ is invincible: we are not
fighting in mere human power, but with weapons that are
“mighty in God” (cf. Eph. 6:10-18), divinely powerful, more

213
