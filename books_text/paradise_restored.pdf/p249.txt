Josephus on the Fall of Jerusalem 239

his Roman soldiers, while all the people assisted him in his attack upon
them, insomuch that, when it came to a battle, the Egyptian ran away,
with a few others, while the greatest part of those that were with him
were either destroyed or taken alive; but the rest of the multitude were
dispersed every one to their own homes and there concealed themselves.

6. Now, when these were quieted, it happened, as it does in a dis-
eased body, that another part was subject to an inflammation; for a
company of deceivers and robbers got together, and persuaded the
Jews to revolt, and exhorted them to assert their liberty, inflicting
death on those that continued in obedience to the Roman government,
and saying, that such as willingly chose slavery ought to be forced
from such their desired inclinations; for they parted themselves into
different bodies, and lay in wait up and down the country, and
plundered the houses of the great men, and slew the men themselves,
and set the villages on fire; and this till all Judea was filled with the
effects of their madness. And thus the flame was every day more and
more blown up, till it came to a direct war.

The Tyranny of Gessius Florus?
Gii:xiv:2)

2, And although such was the character of Albinus, yet did
Gessius Florus, who succeeded him, demonstrate him to have been a
most excellent person, upon the comparison: for the former did the
greatest pari of his rogueries in private, and with a sort of dissimula-
tion; but Gessius did his unjust actions to the harm of the nation after
a pompous manner; and as though he had been sent as an executioner
to punish condemned malefactors, he omitted no sort of rapine, or of
vexation; where the case was really pitiable, he was most barbarous;
and in things of the greatest turpitude, he was most impudent; nor
could anyone outdo him in disguising the truth; nor could anyone con-
trive more subtle ways Of deceit than he did. He indeed thought it but
a petty offence to get money out of single persons; so he spoiled whole
cities, and ruined entire bodies of men at once, and did almost publicly
proclaim it all the country over, that they had liberty given them to
turn robbers, upon this condition, that he might go shares with them
in the spoils. Accordingly, this his greediness of gain was the occasion
that entire toparchies were brought to desolation; and a great many of
the people left their own country, and fled into foreign provinces.

  

Massacre in Jerusalem
(ii:xiv:8-9)
8. Now at this time Florus took up his quarters at the palace; and
on the next day he had his tribunal set before it, and sat upon it, when
