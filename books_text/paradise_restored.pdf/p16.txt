6 Paradise Restored

You must not suppose that Athanasius was just a positive-
thinking optimist, relaxing in quiet, peaceful surroundings. On
the contrary: he lived through one of the most severe persecu-
tions the world had ever seen, the Emperor Diocletian’s all-out
attempt to stamp out the Christian faith. Later, Athanasius had
to stand practically alone for 40 years in his defense of the doc-
trine of the Trinity against rampant heresy, being exiled by the
government on five occasions and sometimes in peril for his life.
In fact, his story gave birth to a proverb: Athanasius contra
mundum (Athanasius against the world). Yet he never lost
sight of the basic fact of world history, that the Word had be-
come flesh, conquering the devil, redeeming mankind, flooding
the world with Light which the darkness could not overcome.

The Church’s eschatology of dominion radically shaped the
history of Western civilization. For example, think about the
great cathedrals of Europe, and compare them to the church
buildings of today. Those old cathedrals, magnificent works of
art constructed over decades and sometimes generations, were
built to last for centuries —and they have. But modern evangeli-
cal churches are usually built to last a generation at most. We
don’t expect to be around long enough to get much use out of
them, and we certainly don’t expect our great-grandchildren
to worship in them. We don’t even expect to fave great-
grandchildren, It is safe to say that the thought of descendants
living five hundred years from now has never even entered the
minds of most evangelicals today. Yet, for many Christians of
previous generations, the idea of future generations benefiting
from their labors was not strange in the slightest degree. They
built for the ages.

Let’s look at a very different field: exploration. Not one his-
torian in a hundred knows what motivated Christopher Colum-
bus to seek a western route to the Indies. Trade? Yes, that was
part of the reason. More than this, however, it was unfulfilled
prophecy. Before he began his expeditions, Columbus crammed
his journals with quotations from Isaiah and other Biblical
writers, in which he detailed the numerous prophecies that the
Great Commission to disciple all nations of the world would be
successful (see, for example, Isa. 2:2-5; 9:2-7; 11:1-10; 32:15-17;
40:4-11; 4221-12; 49:1-26; 56:3-8; 60:1-22; Gl:1-11; 62:1-12;
65:1-25; 66:1-24). He figured that if the Indies were to be con-
