Josephus on the Fail of Jerusalem 259

suade them by such open good advice, he betook himself to the
histories belonging to their own nation; and cried out aloud, “O
miserable creatures! Are you so unmindful of those that used to assist
you, that you will fight by your weapons and by your hands against
the Romans? When did we ever conquer any other nation by such
means? and when was it that God, who is the Creator of the Iewish
people, did not avenge them when they had been injured? Will not you
turn again, and look back, and consider whence it is that you fight
with such violence, and how great a Supporter you have profancly
abused? Will not you recall to mind the prodigious things done for
your forefathers and this holy place, and how great enemies of yours
were by him subdued under you? I even tremble myself in declaring
the works of Gad before your ears, that are unworthy to hear them:
however, hearken io me, that you may be informed how you fight, not
only against the Romans but against God himself.

“In old times there was one Necao, king of Egypt, who was also
called Pharaoh; he came with a prodigious army of soldiers, and seized
queen Sarah, the mother of our nation. What did Abraham our pro-
genitor then do? Did he defend himself from this injurious person by
war, although he had three hundred and eighteen captains under him,
and an immense army under each of them? Indeed, he deemed them to
be no number at all without God’s assistance, and only spread out his
hands towards this holy place, which you have now polluted, and
reckoned upon him as upon his invincible supporter, instead of his
own army. Was not our queen sent back, without any defilement to her
husband, the very next evening? — while the king of Egypt fied away,
adoring this place which you have defiled by shedding thereon the
blood of your countrymen; and he also trembled at those visions
which he saw in the night-season, and bestowed both silver and gold
on the Hebrews, as on a people beloved of God.

“Shall I say nothing, or, shali [ mention the removal of our fathers
into Egypt who, when they were used tyrannically, and were fallen
under the power of foreign kings for four hundred years together, and
might have defended themselves by war and by fighting, did yet do
nothing but commit themselves to God? Who is there that does not
know that Egypt was over-run with all sorts of wild beasts, and con-
sumed by all sorts of distempers? how their land did not bring forth its
fruits? how the Nile failed of water; how the ten plagues of Egypt fol-
lowed one upon another? and how, by those means, our fathers were
sent away, under a guard, without any bloodshed, and without running
any dangers, because God conducted them as his peculiar servants?

“Moreover, did not Palestine groan under the ravage the Assyrians
made, when they carried away our sacred ark? as they did their idol
