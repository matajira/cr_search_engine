And when He Who spake unto Moses, the Word
of the Father, appeared in the end of the world, He
also gave this commandment, saying, “But when they
persecute you in this city, flee ye into another” [Matt.
10:23]; and shortly after He says, “When ye therefore
shall see the abomination of desolation, spoken of by
Daniel the prophet, stand in the holy place (whoso
readeth, let him understand); then let them which be
in Judea flee into the mountains: let him which is on
the housetop not come down to take any thing out of
his house: neither let him which is in the field return
back to take his clothes” [Matt. 24:15]. Knowing these
things, the Saints regulated their conduct accordingly.

St. Athanasius, Defence of His Flight [11]
