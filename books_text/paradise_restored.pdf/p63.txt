The Garden and the Howling Wilderness 53

derness and the solitary place will be glad; and the desert will re-
joice, and blossom as the rose” (Isa, 35:1).

The poor and needy search for water, but there is none;
Their tongues are parched with thirst.

But I the Lorp will answer them;

I, the God of Israel, will not forsake them.

I will make rivers flow on barren heights,

And springs within the valleys.

I will turn the desert into pools of water,

And the parched ground into springs.

] will put in the desert the cedar and the acacia,

The myrtle and the olive.

I will set pines in the wasteland,

The fir and the cypress together,

So that people may see and know,

May consider and understand,

That the hand of the Lorp has done this,

That the Holy One of Israel has created it. (Isa. 41:17-20)

This, then, is the direction of history, in what may be called
“the First Rapture”—God gradually uprooting unbelievers and
unbelieving cultures from the land, and bringing His people into
a full inheritance of the earth.

Tam not denying, of course, the Biblical teaching that God’s
people will someday meet the Lord in the air, at His return (1
Thess.4:17); but the modern doctrine of the “Rapture” is too
often a doctrine of flight from the world, in which Christians are
taught to long for escape from the world and its problems,
rather than for what God’s Word promises us: Dominion. How
common it is to hear Christians say, when confronted with a
problem: “I sure hope the Rapture comes soon!”— rather than:
“Let’s get to work on the solution right now!” Even worse is the
tesponse that is also too common: “Who cares? We don’t have
to do anything about it, because the Rapture is coming soon
anyway!” And worst of all is the attitude held by some that all
work to make this a better world is absolutely wrong, because
“improving the situation will only delay the Second Coming!” A
good deal of modern Rapturism should be recognized for what
it really is: a dangerous error that is teaching God’s people to ex-
pect defeat instead of victory.
