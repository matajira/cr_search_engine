178 Paradise Restored

Fourth, John saw “one of his heads as if it had been slain,
and his fatal wound was healed” (13:3). Some have pointed out
that, after Nero was killed, the rumor began to spread that he
would rise again and recapture the throne; in some way, they
suppose, John must be referring to that myth. This, it seems to
me, is a very unsatisfactory method of dealing with Scripture.
John mentions the Beast’s “death-wound” three times in this
passage (see v. 12, 14); clearly, this is much more than a casual
symbol, and we should attempt a Biblical explanation for it.

The Beast, as we saw, resembles the Dragon. The fact that he
receives a head wound should make us think of the scene in the
Garden of Eden, when God promised that Christ would come
and crush the Dragon’s head (Gen. 3:15). Daniel had prophesied
that in the days of the Roman rulers, Christ’s Kingdom would
crush the Satanic empires and replace them, filling the earth.
Accordingly, apostolic testimony proclaimed that Christ’s
Kingdom had come, that the devil had been defeated, disarmed,
and bound, and that all nations would begin to flow toward the
mountain of the Lord’s House. Within the first generation, the
gospel spread rapidly around the world, to all the nations;
churches sprang up everywhere, and members of Caesar’s own
household came into the faith (Phil. 4:22). In fact, Tiberius
Caesar even formally requested that the Roman Senate officially
acknowledge Christ’s divinity. For a time, therefore, it looked as
if a coup were taking place: Christianity was in the ascendant,
and soon would gain control. Satan’s head had been crushed,
and with it the Roman Empire had been wounded to death with
the sword (Rev. 13:14) of the gospel.

But then the tables were reversed. Although the gospel had
spread everywhere, so had heresy and apostasy; and under per-
secution by the Jews and the Roman State, great masses of
Christians began falling away. The New Testament gives the
definite impression that most of the churches fell apart and
abandoned the faith; under Nero’s persecution, the Church
seemed to have been stamped out entirely, The Beast had received
the head-wound, the wound unto death —yet it still lived. The
reality, of course, was that Christ Aad defeated the Dragon and
the Beast; but the implications of His victory still had to be
worked out; the saints had yet to overcome, and take possession
(Dan. 7:21-22; Rev. 12:11),
