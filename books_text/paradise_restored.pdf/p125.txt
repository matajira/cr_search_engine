B
THE LAST DAYS

As we began to see in the preceding chapter, the period
spoken of in the Bible as “the last days” (or “last times” or “last
hour”) is the period between Christ’s birth and the destruction
of Jerusaiem. The early church was living at the end of the old
age and the beginning of the new. This whole period must be
considered as the time of Christ’s First Advent. In both the Old
and New Testament, the promised destruction of Jerusalem is
considered to be an aspect of the work of Christ, intimately con-
nected to His work of redemption. His life, death, resurrection,
ascension, outpouring of the Spirit, and judgment on Jerusalem
are all parts of His one work of bringing in His Kingdom and
creating His new Temple (see, for example, how Daniel 9:24-27
connects the atonement with the destruction of the Temple).

Let’s consider how the Bible itself uses these expressions
about the end of the age. In 1 Timothy 4:1-3, Paul warned:

Now the Spirit expressly says that in latter times some will depart
from the faith, giving heed to deceiving spirits and doctrines of
demons, speaking lies in hypocrisy, having their own conscience
seared with a hot iron, forbidding to marry, and commanding to
abstain from foods which God created to be received with
thanksgiving by those who believe and know the truth.

Was Paul talking about “latter times” which would hap-
pen thousands of years later? Why should he warn Timothy
of events which Timothy, and Timothy’s great-great-
grandchildren, and fifty or more generations of descendants,
would never live to see? In fact, Paul tells Timothy, “If you in-
struct the brethren in these things, you will be a good minister of
Jesus Christ” (1 Tim. 4:6). The members of Timothy’s congrega-
tion needed to know about what would take place in the “latter

115
