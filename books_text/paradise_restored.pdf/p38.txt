*

When therefore the servants of the Chief Priests
and the Scribes saw these things, and heard from
Jesus, “Whosoever is athirst, let him come to Me and.
drink” [John 7:37]; they perceived that this was not a
mere man like themselves, but that this was He Who
gave water to the saints, and that it was He Who was
announced by the prophet Isaiah. For He was truly
the splendour of the light, and the Word of God. And
thus as a river from the fountain He gave drink also
of old to Paradise; but now to all men He gives the
same gift of the Spirit, and says, “If any man thirst,
Jet him come to Me and drink. Whosoever believeth
on Me, as saith the Scripture, rivers of living water
shall flow out of his belly” [John 7:37-38]. This was
not for man to say, but for the living God, Who truly
vouchsafes life, and gives the Holy Spirit.

St. Athanasius, Letters [xliv]
