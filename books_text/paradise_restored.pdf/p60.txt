50 Paradise Restored

is impossible apart from the work of the Holy Spirit, who
enables us to keep the righteous requirements of God’s law
(Rom. 8:4).

All heathen cultures have been statist and tyrannical, for a
people who reject God will surrender themselves and their prop-
erty to a dictator (1 Sam. 8:7-20), Ungodly men want the bless-
ings of the Garden, but they attempt to possess them by unlaw-
ful means, as Ahab did with Naboth’s vineyard (1 Ki. 21:1-16),
and the result is, as always, destruction (1 Ki. 21:17-24). The gen-
uine, free possession of land is the result of salvation: God
brought His people into a land, and divided it among them for
an inheritance (Num. 26:52-56); and, as He had done in Eden,
He regulated the land (Lev. 25:4) and the trees (Lev. 19:23-25;
Deut, 20:19-20).

As we have seen, when God banished Adam and Eve from
their land, the world began to become a wilderness (Gen.
3:17-19). From this point the Bible begins to develop a Land-vs.-
Wilderness theme, in which the redeemed, obedient people of
God are seen inheriting a /and that is secure and bountiful, while
the disobedient are cursed by being driven out into a wilderness.
When Cain was judged by God, he complained; “Today You are
driving me from the land, and I will be hidden from Your pres-
ence; I will be a restless wanderer on the earth” (Gen. 4:14). And
he was correct, as Scripture records: “So Cain went out from the
Lord’s presence, and lived in the land of Nod, on the east of
Eden” (Gen. 4:16). Nod means Wandering: Cain became the
first nomad, a wanderer with no home and no destination.

Similarly, when the whole world became wicked, God said:
“T will blot out man whom I have created from the face of the
land” (Gen. 6:7), and He did so, by the Flood—leaving only
Noah and his household alive in the ark (which God brought to
rest, incidentally, on a mountain; Gen. 8:4). The ungodly were
driven out of the land, and the people of the covenant repopu-
lated it.

Again, the ungodly tried to build their own “Garden,” the
tower of Babel. They were seeking to make themselves a name
—to define themselves in terms of their own rebellious standards
—and to prevent themselves from being scattered from the land
(Gen, 11:4). But man cannot build the Garden on his own terms.
God is the Definer, and He is the only One who can give us
