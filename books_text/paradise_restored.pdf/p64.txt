54 Paradise Restored

Indeed, a very common evangelical worldview is that “the
earth is the devil’s, and the fulness thereof”’—that the world
belongs to Satan, and that Christians can expect only defeat un-
til the Lord returns. And that is exactly the lie that Satan wants
Christians to believe. If God’s people think the devil is winning,
it makes his job just that much easier. What would he do if
Christians stopped retreating and started advancing against
him? James 4:7 tells us what he would do: he would flee from
us! So why isn’t the devil fleeing from us in this age? Why are
Christians at the mercy of Satan and his servants? Why aren’t
Christians conquering kingdoms with the Gospel, as they did in
times past? Because Christians are not resisting the devil! Worse
yet, they’re being ¢o/d by their pastors and leaders ot to resist,
but to retreat instead! Christian leaders have turned James 4:7
inside out, and are really giving aid and comfort to the enemy —
because they are, in effect, saying to the devil: “Resist the
Church, and we will flee from you!” And Satan is taking them at
their word. So then, when Christians see themselves losing on
every front, they take it as “proof” that God has not promised
to give dominion to His people. But the only thing it proves is
that James 4:7 is true, after all, including its “flip side” — that is,
if you don’t resist the devil, he won’t flee from you.

What we must remember is that God doesn’t “rapture”
Christians out of the world in order to escape conflict — He “rap-
tures” non-Christians! The Lord Jesus prayed, in fact, that we
would not be “raptured”: “My prayer is not that You take them
out of the world but that you protect them from the evil one”
(John 17:15). And this is the constant message of Scripture.
Ged’s people will inherit all things, and the ungodly will be dis-
inherited and driven out of the land. “For the upright will live in
the land, and the blameless will remain in it; but the wicked will
be cut off from the land, and the treacherous will be uprooted
from it” (Prov. 2:21-22). “The righteous will never be uprooted,
but the wicked will not remain in the land” (Prov. 10:30). God
described the land of Canaan as having been “defiled” by the
abominable sins of its heathen population, saying that the land
itself “vomited out its inhabitants”; and He warned His people
not to imitate those heathen abominations, “so that the land
may not vomit you out also” (Lev. 18:24-28; 20:22). Using the
same Edenic language, the Lord warns the church of Laodicea
