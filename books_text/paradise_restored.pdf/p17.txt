The Hope 7

verted, a sea route would be a much more efficient way to bring
them the gospel; and he credited his discoveries not to the use of
mathematics or maps, but rather to the Holy Spirit, who was
bringing to pass what Isaiah had foretold. We must remember
that America had been discovered numerous times, by other
cultures; yet successful colonization and development took
place only in the age of exploration begun by Columbus, Why?
Because these explorers were bearers of the gospel, and their
goal was to conquer the world for the kingdom of God. They
came expecting that the New World would be Christianized.
They were certain of victory, and assumed that any obstacles they
met had been placed there for the express purpose of being over-
come. They knew that Christians are destined for dominion.

Examples could be multiplied, in every field. The whole rise
of Western Civilization — science and technology, medicine, the
arts, constitutionalism, the jury system, free enterprise, literacy,
increasing productivity, a rising standard of living, the high
status of women—is attributable to one major fact: the West
has been transformed by Christianity. True, the transformation
is not yet complete. There are many battles ahead. But the point
is that, even in what is still largely an early Christian civilization,
God has showered us with blessings.

Many Christians do not realize it, but the Hope is the basis
for many of the great old hymns of the faith, written before the
modern era of evangelical despair and pessimism. Think about
that the next time you sing Martin Luther’s “A mighty Fortress is
our God,” Isaac Watts’s “Jesus shall reign where’er the sun doth
his successive journeys run,” or George Duffield’s “Stand up,
stand up for Jesus.” Do you really believe that Jesus is now
leading us “from victory unto victory . . . till every foe is van-
quished, and Christ is Lord indeed”? That is what the Church
has historically believed. That is what they sang in their hymns.
This can be seen most clearly in the traditional Christmas carols,
which, like Athanasius’s reflections on the Incarnation, are
unabashed expectations of Christ’s triumph over the world
through the gospel. Carols such as “Come, thou long-expected
Jesus,” “O come, O come, Emmanuel,” “Hark! the herald
angels sing,” “God rest you merry, gentlemen,” and many others
are written from the same basic perspective as the present book.
The conviction that—as a result of His first advent—Christ is
