16 Paradise Restored

days,” because they would be personally affected by those
events. In particular, they needed the assurance that the coming
apostasy was part of the overall pattern of events leading up to
the end of the old order and the full establishing of Christ's
Kingdom. As we can see from passages such as Colossians
2118-23, the “doctrines of demons” Paul warned of were current
during the first century, The “latter times” were already taking
place. This is quite clear in Paul’s later statement to Timothy:

But know this, that in the last days perilous times will come; for
men will be lovers of themselves, lovers of money, boasters,
proud, blasphemers, disobedient to parents, unthankful, un-
holy, unloving, unforgiving, slanderers, without self-control,
brutal, despisers of good, traitors, headstrong, haughty, lovers
of pleasure rather than lovers of God, having a form of
godliness but denying its power. And from such people turn
away! Far of this sort are thase who creep into households and
make captives of gullible women loaded down with sins, led
away by various lusts, always learning and never able to come to
the knowledge of the truth. Now as Jannes and Jambres resisted
Moses, so also do these resist the truth; men of corrupt minds,
disapproved concerning the faith @ Tim. 3:1-8}.

The very things Paul said would happen in “the last days”
were happening as he wrote, and he was simply warning
Timothy about what to expect as the age wore on to its climax.
Antichrist was beginning to rear its head.

Other New Testament writers shared this perspective with
Paul. The letter to the Hebrews begins by saying that God “has
in these last days spoken to us in His Son” (Heb. 1:2); the writer
goes on to show that “now once at the end of the ages He has
appeared to put away sin by the sacrifice of Himself” (Heb.
9:26). Peter wrote that Christ “was foreknown before the foun-
dation of the world, but has appeared in these last times for you
who through Him are believers in God” (1 Pet. 1:20-21), Apostolic
testimony is unmistakably clear: when Christ came, the “last
days” arrived with Him. He came to bring in the new age of the
Kingdom of God. The old age was winding down, and would be
thoroughly abolished when God destroyed the Temple.
