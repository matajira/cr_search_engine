152 Paradise Restored

cies involved in the so-called “literalist” school of Biblical interpre-
tation. Another problem, which is especially severe among certain
“pop” theologians, is their arbitrary understanding of prophetic
symbols. | have heard preachers speak of the locusts in Rev. 9:3-11
as showing forth a bewildering variety of horrors: bombers,
ballistic missiles, Cobra helicopters, and even the dreaded “killer
bees” of South America. Which of these do the locusts represent?
Without a standard of interpretation, there is no objective way to
tell—and thus the Book of Revelation becomes in practice what
its very title insists it isn’t: an unintelligible hodgepodge of
“apocalyptic” fire and wind, signifying nothing.

Actually, John tells us hundreds of times throughout the
Book of Revelation exactly what the standard of interpretation
is, for the book is positively crammed with quotations from and
allusions to the Old Testament. The Book of Revelation de-
pends on the Old Testament much more than does any other
New Testament book, This fact alone should warn us that we
cannot begin to fathom its meaning apart from a solid grasp of
the Bible as a whole—which is why I wrote Part Two of this
book, and why I am harping on the subject again. The early
churches had such an understanding. The Gospel had been
preached first to the Jews and Gentile proselytes; often churches
had been formed by worshipers at synagogues, and this was true
even of the churches of Asia Minor (Acts 2:9; 13:14; 14:1; 16:4;
17:1-4, 10-12, 17; 18:4, 8, 19, 24-28; 19:1-10, 17). Moreover, it is
clear from Galatians 2:9 that the Apostle John’s ministry was to
Jews in particular. Therefore, the first readers of the Revelation
were steeped in the Old Testament to a degree that most of us to-
day are not. The symbolism of the Revelation is saturated with
Biblical allusions which were commonly understood by the early
Church. Even in those rare congregations that did not have
some Hebrew members, the Scriptures used in teaching and
worship were primarily from the Old Testament. The early
Christians possessed the authoritative and infallible key to the
meaning of John’s prophecies. Our modern failure to appreciate
this crucial fact is the main cause of our inability to understand
what John was talking about.

For instance, let’s take a much-abused symbol from Revela-
tion and apply this principle. In Revelation 7, 9, 14 and 22, John
sees Gad’s people sealed on their foreheads with His name; and
