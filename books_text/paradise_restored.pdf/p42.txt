32 Paradise Restored

pies, and proceeded from there to a Garden where, as the Last
Adam, He prevailed over temptation (Matt. 26:30; cf. Matt.
4:8-11, at the beginning of His ministry). Finally, He com-
manded His disciples to meet Him on a mountain, where He
commissioned them to conquer the nations with the Gospel, and
promised to send them the Holy Spirit; and from there He
ascended into the cloud (Matt. 28:16-20; Acts 1:1-19; for more
on the significance of this cloud, see Chapter 7).

I have by no means exhausted the list that might be given of
Biblical references to God’s redemptive activities on mountains;
but those which have been cited are sufficient to demonstrate the
fact that in redemption Ged is calling us to return to Eden: we
have access to the Holy Mountain of God through the shed
blood of Christ. We have come to Mount Zion (Heb. 12:22),
and may boldly approach the Holy Place (Heb. 10:19), granted
by God’s grace to partake again of the Tree of Life (Rev. 2:7).
Christ has built His Church as a City on a Hill, to give light to
the world (Matt. 5:14), and has promised that the nations will
come to that light (Isa. 60:3). The prophets are full of this
mountain-imagery, testifying that the world itself will be trans-
formed into Eden: “In the last days, the mountain of the House
of the Lorp will be established as the chief of the mountains,
and will be raised above the hills; and all the nations will stream
to it” (Isa. 2:2; cf. Isa. 2:2-4; 11:9; 25:6-9; 56:3-8; 65:25; Mic.
4:1-4). Thus the day will come when God’s Kingdom, His Holy
Mountain, will “fill the whole earth” (see Dan. 2:34-35, 44-45),
as God’s original dominion mandate is fulfilled by the Last
Adam.

Minerals in the Garden

The Pishon River, originating in Eden, flowed “around the
whole land of Havilah, where there is gold. And the gold of that
land is good; the bdellium and the onyx stone are there” (Gen.
2:11-12), The intent of these verses is clearly to connect in our
minds the Garden of Eden with precious stones and minerals;
and this point is made in other Biblical references that speak of
Eden. The most obvious reference is in God’s statement to
fallen Adam (part of which was quoted above):
