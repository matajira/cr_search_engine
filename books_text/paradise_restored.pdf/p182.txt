172 Paradise Restored

ding feast of Christ and His Bride, the Church. The scene then
shifts to reveal the coming worldwide dominion of the gospel, as
the King of kings rides forth with His army of saints to wage
holy war for the reconquest of earth. The agent of victory is His
Word, which proceeds from His mouth like a sword.

Chapter Twenty gives a capsulized history of the new world
order, from the first coming of Christ until the end of the world.
The Lord binds Satan and enthrones His people as kings and
priests with Him. Satan’s final attempt to overthrow the King is
crushed, and the Last Judgment is ushered in. The righteous
and the wicked are eternally separated, and God’s people enter
into their eternal inheritance.

Chapters Twenty-one and Twenty-two record a vision of the
Church in all its glory, comprehending both its earthly and its
heavenly aspects. The Church is revealed as the City of God, the
beginning of the New Creation, extending a worldwide influ-
ence, drawing all nations into itself, until the whole earth is one
glorious Temple. The goals of Paradise are consummated in the
fulfillment of the dominion mandate.

With this broad overview in mind, we can now proceed toa
more detailed study of the imagery of Revelation, concentrating
on four of the most dramatic and controversial symbols: the
Beast, the Harlot, the Millennium, and the New Jerusalem. As
we shall see, each one of these images spoke to the first-century
Chureh about contemporary realities, assuring God’s people of
Christ’s universal lordship and encouraging them in the Hope of
the gospel’s worldwide triumph.
