The Time Is at Hand 165

of modern imitators, so you still have a chance to get beheaded.
Unfortunately, those who had. hoped to escape the fireworks in
the rapture aren’t so lucky. They’lt just have to slog through to
victory with the rest of us.

The early Church had two great enemies: apostate Israel and
pagan Rome. Many Christians died at their hands (indeed, these
two enemies of the Church often cooperated with each other in
putting Christians to death, as they had with the crucifixion of
the Lord Himself), And the message of the Revelation was that
these two persecutors, inspired by Satan, would soon be judged
and destroyed. Its message was contemporary, not futuristic.

Some will complain that this interpretation makes the Reve-
lation “irrelevant” for our age. A more wrong-headed idea is
unimaginable. Are the books of Romans and Ephesians “irrele-
vant” just because they were written to believers in the first cen-
tury? Should 1 Corinthians and Galatians be dismissed because
they dealt with first-century problems? Is not aé/ Scripture
profitable for believers in every age (2 Tim. 3:16-17)? Actually, it
is the futurists who have made the Revelation irrelevant — for on
the futurist hypothesis the book has been inapplicable from the
time it was written until the twentieth century! Only if we see the
Revelation in terms of its contemporary relevance is it anything
but a dead letter. From the outset, John stated that his book was
intended for “the seven churches which are in Asia” (1:4), and
we must assume that he meant what he said. He clearly expected
that even the most difficult symbols in the prophecy could be un-
derstood by his first-century readers (13:18). Not once did he im-
ply that his book was written with the twentieth century in
mind, and that Christians would be wasting their time attempt-
ing to decipher it until space stations were invented. The
primary relevance of the Book of Revelation was for its
first-century readers. It still has relevance for us today as we un-
derstand its message and apply its principles to our lives and our
culture. Jesus Christ still demands of us what He demanded of
the early Church: absolute faithfulness to Him.

Several lines of evidence for the contemporary nature of the
Revelation may be pointed out here. First, there is the general
tone of the book, which is taken up with the martyrs (see, e.g.,
6:9; 7:14; 12:11), The subject is clearly the present situation of
the churches: the Revelation was written to a suffering Church
