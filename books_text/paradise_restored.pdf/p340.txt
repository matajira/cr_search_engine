330 Paradise Restored

for it was this explicit model, not Kline’s vague one, that Chilton
adopted for The Days of Vengeance.'* (If Kline’s outline had
been that clear, why did it take over 20 years for anyone to write
about its implications for New Testament theology?)

I will never forget that Wednesday evening prayer meeting at
which Sutton presented his discovery, Chilton rushed up to the
pulpit after Sutton’s presentation and began asking him ques-
tions. It was as if a mental fog had lifted from him, He must
have stood there asking questions for half an hour, and Sutton
stood behind the pulpit and kept answering them. Sutton
showed Chilton the pear! that had been locked tight in Kline’s
clamshell prose for over twenty years.

Sutton’s breakthrough had the same effect on me when I
reread my Own manuscript on the Ten Commandments, The
Sinai Strategy, which was published in early 1986.'° I hurriedly
wrote a Preface incorporating the five-point covenant structure
{commandments 1-5 parallel 6-10) just before it went to press.
This made me the first person to go into print with this outline,
but as I stated in the Preface, I got the whole idea from Sutton.
None of us had spotted what Sutton saw in Kline, although we
had all read Kline’s essays on the ancient suzerainty (kingly)
treaties. Kline without Sutton produces confusion about the
covenant model, just as Kline without Jordan produces confu-
sion about biblical symbolism, and Kline without Chilton pro-
duces confusion about eschatology. (In short, “Kline with-
out...” produces confusion.)

Here, then, is the five-point structure of the Biblical cove-
nant, as developed by Sutton in his excellent book, That You
May Prosper. The Book of Deuteronomy is structured around
it.

18. Sutton’s appendix on Kline explains why Kline’s model is so vague.
First, he misses transcendence, calling it simply the “preamble” section of the
covenant. Second, he ignores the authority aspect of point two, calling it in-
stead “historical prologue.” Third, he does not discuss adoption in the third
section (“sanctions”). In point five, he does not develop historical continuity
and inheritance because his amillennial theology does not lend itself well to
such concepts. Most important, he does not discuss this covenant structure in
terms of historic Protestant theology. For him, developing the model is
primarily a historical exercise. Sutton, That You May Prosper, Appendix 7.

19, Tyler, Texas; Institute for Christian Economics.
