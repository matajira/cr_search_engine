18 Paradise Restored

hard work, necessitating a greater familiarity with the Bible.
The main drawback of the speculative method, for all its sensa-
tionalism, is that it just isn’t Biblical.

The Language of the Prophets

As I mentioned above, much of the Bible is written in sym-
bols. A helpful way to understand this, perhaps, would be to
speak of these symbols as a set of patterns and associations. By
this I mean that Biblical symbolism is not a code. It is, instead, a
way of seeing, a perspective. For example, when Jesus speaks of
“living water” (John 4:10), we rightly recognize that He is using
water as a symbol. We understand that when He spoke to the
woman at the well, He was not merely offering her “water.” He
was offering her eternal life. But He called it “water.” We should
immediately ask: Why did He do that? He could have simply
said “eternal life.” Why did He speak in metaphor? Why did He
want her to think of water?

Now this is where we can make a big mistake, and this is the
primary error of some interpreters who try to take a “symbolic”
approach. It is to think that Biblical symbolism is primarily a
puzzle for us to solve. We can suddenly decide: “Aha! Water is a
special code-word which means eternal life. That means that
whenever the Bible talks about water symbolically, it is really
talking about eternal life; whenever someone takes a drink, he is
really becoming a Christian.” It just doesn’t work that way (as
you will see if you try to apply it throughout the Bible). Besides,
what sense would it make for the Bible simply to put everything
in code? The Bible is not a book for spies and secret societies; it
is God’s revelation of Himself to His covenant people. The
puzzle-solving, mystical interpretation tends to be speculative; it
does not pay sufficient attention to the way the Bible itself
speaks.

When Jesus offered “water” to the woman, He wanted her to
think of the multiple imagery connected with water in the Bible.
In a general sense, of course, we know that water is associated
with the Spiritual refreshment and sustenance of life which
comes through salvation. But the Biblical associations with
water are much more complex than that. This is because under-
standing Biblical symbolism does not mean cracking a code. It is
much more like reading good poetry.
