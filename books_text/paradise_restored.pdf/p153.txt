16
THE CONSUMMATION OF THE KINGDOM

We can now begin to draw some very significant overall con-
chisions from our study so far. As we saw in the previous
chapter, the Last Day is a synonym for the Last Judgment, at
the end of the world. Moreover, Jesus declared that those who
believe in Him will be resurrected at the Last Day (John 6:39-40,
44, 54), This means that Judgment Day is also Resurrection
Day; both occur together, at the close of history.

We can add to this what the Apostle Paul tells us about the
Resurrection: it will coincide with the Second Coming of Christ
and the Rapture of living believers (1 Thess. 4:16-17). Some have
tried to evade the force of this text by suggesting a series of
Resurrections — one at the Rapture, another at the Second Com-
ing (perhaps some years later), and at least one more at the con-
summation of the Kingdom, the end of history (where it
belongs). This does not by any means solve the problem, how-
ever, For Jesus specifically said that whoever believes in Him will
be raised “at the Last Day.” That means that e// Christians will
be resurrected at the Last Day. Again, 1 Thessalonians 4 says
that aff believers will be raised at the Rapture. Obviously, in
terms of these texts, there can be only one Resurrection of
believers. And this Resurrection, which coincides with the Rap-
ture, will take place on the Last Day.

One Resurrection

Before we can consider these points in greater detail, we
need to be clear on one issue which was stated, but somewhat
underplayed, in the preceding chapter. One of my most crucial
assumptions is that there is one Resurrection, of both the
righteous and the wicked. To many, of course, that will seem ob-
vious. But it needs to be stated explicitly because there is much

143
