Select Bibliography 293

Rushdoony, Rousas John. The Foundations of Social Order:
Studies in the Creeds and Councils of the Early Church. Fair-
fax, VA: Thoburn Press, [1968] 1978.

. The Institutes of Biblical Law, Phillipsburg, NJ:
The Presbyterian and Reformed Publishing Co., 1973.

Schlossberg, Herbert. Idols For Destruction: Christian Faith
and Its Confrontation with American Society. Nashville:
Thomas Nelson Publishers, 1983.

Thoburn, Robert L. The Christian and Politics. Tyler, TX:
Thoburn Press, 1984.

Yan Til, Cornelius. The Defense of the Faith. Philadelphia: The
Presbyterian and Reformed Publishing Co., [1955] third rev.
ed., 1967.

Wallace, Ronald $. Calvin's Doctrine of the Christian Life.
Tyler, TX: Geneva Ministries, [1959] 1982.

. Calvin's Doctrine of the Word and Sacrament.
Tyler, TX: Geneva Ministries, [1953] 1982.

Warfield, Benjamin Breckinridge. Biblical and Theological
Studies, Philadelphia; The Presbyterian and Reformed Pub-
lishing Co., 1968.

Wines, E. C. The Hebrew Republic. Uxbridge, MA: American
Presbyterian Press, 1980.

Biblical Interpretation

Brinsmead, Robert D, The Pattern of Redemptive History. Fall-
brook, CA: Verdict Publications, 1979.

Caird, G. B., The Language and Imagery of the Bible. Philadel-
phia: The Westminster Press, 1980.

Farrer, Austin. A Rebirth of Images: The Making of St. John’s
Apocalypse. Gloucester, MA: Peter Smith, [1949] 1970.

Jenkins, Ferrell, The Old Testament in the Book of Revelation.
Grand Rapids: Baker Book House, [1972] 1976.

Kaiser, Waiter C., Jr., Toward an Old Testament Theology.
Grand Rapids: Zondervan Publishing House, 1978.

Kline, Meredith G, Images of the Spirit. Grand Rapids: Baker
Book House, 1980.

—_________.. Kingdom Prologue. Two vols. Privately printed
syllabi, 1981, 1983.

Martin, Ernest L. The Birth of Christ Recaiculated. Pasadena,
CA; Foundation for Biblical Research, 2nd ed., 1980.
