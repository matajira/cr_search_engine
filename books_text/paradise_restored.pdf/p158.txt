148 Paradise Restored

ture” (the “catching up” of the living saints “to meet the Lord in
the air”), The Bible does not teach any separation between the
Second Coming and the Rapture; they are simply different
aspects of the Last Day. And the fact is that throughout the en-
tire history of the Church no one ever heard of the (so-called)
“pretribulation Rapture” until the nineteenth century; it did not
become widespread until a few decades ago. Recently, as
younger generations have begun to recognize the lack of Scrip-
tural foundation for this novel view, a move toward a more
Biblically grounded eschatology has started to take place. The
eschatology of dominion, the historic Hope of the Church, is
again on the rise, Because of the renewed interest in developing
a Biblical worldview and applying Biblical standards to every
area of life, dominion eschatology is increasingly being dis-
cussed and accepted. And, because it is the truth, its establish-
ment as the dominant eschatology is inevitable.

Conclusion

The Biblical doctrine of the Second Coming is relatively un-
complicated and straightforward. We can summarize our
findings from the last several chapters as follows:

1. The reign of Jesus Christ began at His Resurrection and
Ascension, as the prophets had promised. His Kingdom (“the Mil-
lennium”) is now in force and will continue until He is universally
acknowledged as Lord. By means of the gospel, His people are ex-
tending His rule over the face of the earth, until all nations are dis-
cipled and Paradise comes to its most complete earthly fulfillment.

2. On the Last Day, at the end of the world, Jesus Christ will
return to resurrect all men for the Judgment, both the righteous
and the wicked. Those Christians who are still living at the Sec-
ond Coming will be raptured to join the Lord and the resur-
rected saints in the Glory-Cloud, where they will be trans-
formed, fully restored into the image of God.

3. The doctrine that Christ’s Kingdom will begin only after
His Second Coming is utterly contradicted by Holy Scripture.
The Bible teaches that the Second Coming of Christ, coinciding
with the Rapture and the Resurrection, will take place at the end
of the Millennium, when history is sealed at the Judgment. Until
then, Christ and His people are marching forth from strength to
strength, from victory to victory. 4 shall overcome.
