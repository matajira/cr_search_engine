190 Paradise Restored

this kind of worldwide political power? The answer is that Reve-
lation is not a book about politics; it is a book about the cove-
nant. Jerusalem did reign over the nations. She had a covenan-
tal priority over the kingdoms of the earth. It is rarely appreci-
ated sufficiently that Israel was a kingdom of priests (Ex. 19:6),
exercising this ministry on behalf of the nations of the world.
When Israel was faithful to God, offering up sacrifices for the
nations, the world was at peace; when Israel broke the cove-
nant, the world was in turmoil. The Gentile nations recognized
this (1 Kings 10:24; Ezra 1; 4-7; cf. Rom, 2:17-24), Yet, per-
versely, they would seek to seduce Israel to commit whoredom
against the covenant — and when she did, they would turn on her
and destroy her. That pattern is repeated several times, until
Israel’s final excommunication in a.p. 70, when Jerusalem was
destroyed as God’s sign that the Kingdom had been transferred
to His new people, the Church (Rev. 11:19; 15:5; 21:3).

Since Israel was to be destroyed, the apostles spent much of
their time during the last days warning God’s people to separate
themselves from her and align themselves with the Church (cf.
Acts 2:37-40; 3:19, 26; 4:8-12; 5:27-32). This is John’s message in
Revelation. Jerusalem’s apostasy has become so great, he says,
that her judgment is permanent and irrevocable. She is now
Babyion, the implacable enemy of God. “And she has become a
dwelling place of demons and a prison of every unclean spirit,
and a prison of every unclean and hateful bird” (Rev. 18:2).
Because Israel rejected Christ, the entire nation has become
demon-possessed, utterly beyond hope (cf. Matt. 12:38-45; Rev.
9:1-11). Therefore, God’s people must not seek to reform Israel,
but to abandon her to her fate. Salvation is with Christ and the
Church, and only destruction awaits those who are aligned with
the Harlot: “Come out of her, my people, that you may not par-
ticipate in her sins and that you may not receive of her plagues”
(Rev. 18:4; cf. Heb. 10:19-39; 12:15-29; 13:10-14).

And so Jerusalem is destroyed, never to rise again: “And a
strong angel took up a stone like a great millstone and threw it
into the sea [cf. Luke 17:2], saying, ‘Thus will Babylon, the great
city, be thrown down with violence, and will not be found any
longer’ ” (Rev. 18:21}. But “Jerusalem” is still standing in the
twentieth century, is it not? How was it destroyed forever in
A.D, 70? What this means is that Israel, as the covenant people,
