102 Paradise Restored

But how is it that they would see Christ coming on the
clouds? Those who have read Chapters 7 and 8 of this book
should have little trouble answering that question. In the first
place, all through the Old Testament God was coming “on
clouds,” in salvation ef His people and destruction of His
enemies: “He makes the clouds His chariot; He walks upon the
wings of the wind” (Ps. 104:3). When Isaiah prophesied God’s
judgment on Egypt, he wrote: “Behold, the Lorn is riding on a
swift cloud, and is about to come to Egypt; the idols of Egypt
will tremble at His presence” (Isa. 19:1). The prophet Nahum
spoke similarly of God’s destruction of Nineveh: “In whirlwind
and storm is His way, and clouds are the dust beneath His feet”
(Nah. 1:3). God’s “coming on the clouds of heaven” is an almost
commonplace Scriptural symbol for His presence, judgment,
and salvation.

More than this, however, is the fact that Jesus is referring to
a specific event connected with the destruction of Jerusalem and
the end of the Old Covenant. He spoke of it again at His trial,
when the High Priest asked Him if He was the Christ, and Jesus
replied:

1 AM; and you shall see the Son of Man sitting at the right hand
of power, and coming with the clouds of heaven (Mark 14:62; cf,
Matt. 26:64).

Obviously, Jesus was not referring to an event thousands of
years in the future. He was speaking of something that His con-
temporaries—‘“this generation”—would see in their lifetime.
The Bible tells us exactly when Jesus came with the clouds of
heaven:

And after He had said these things, He was lifted up while they
were looking on, and a cloud received Him out of their sight
(Acts 1:9}.

So then, after the Lord had spoken to them, He was received up
into heaven, and sat down at the right hand of Gad (Mark
16:19).

We noted in Chapter 8 that it was this event, the Ascension
to the right hand of God, which Daniel had foreseen:
