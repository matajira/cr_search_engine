232 Paradise Restored

6. World War I greatly disheartened this group and Worid
War I virtually wiped out this viewpoint.

Let us momentarily suppose, for the sake of argument, that
this statement is correct. The proper answer is: So what? This
does not prove that the Christian Hope is untrue—only that
people stopped believing that it is true. The implication of the
statement, however, is that the fact of two world wars consti-
tutes evidence that the Hope is mistaken, since the world is not
“getting better and better.” I will grant this much: The two world
wars (and the threat of a third) did considerably damage the
hopes of those humanists who believed in the heretical doctrine
of “automatic” human progress toward peace and brotherhood.
Often falsely confused with postmillennialism, it is actually no
closer to the eschatology of dominion than pagan sacrifices are
to the Lord’s Supper. The Christian does not need to become
discouraged in the face of world war or widespread persecution.
His faith is in God, not in man; his hope is not tied to the
destiny of any particular culture. If his nation or civilization
falls under the righteous judgment of God, the faithful Chris-
tian realizes that God is being faithful to His promises of bless-
ing and cursing. The Hope is no guarantee of blessing for the
disobedient. ft is a guarantee of judgment unto blessing for the
world,

But let us now tackle the question head-on: Did the two
world wars destroy the Hope? In reality, the origins of
postmillennialism’s decline began long before World War I, with
the rise of theological liberalism (which taught that the Bible’s
predictions could not be relied upon) and evolutionary “pro-
gressivism” (which taught that progress was “natural” rather
than ethical). In reaction to these enemies of Biblical Christian-
ity, many evangelical Christians despaired of seeing victory for
the gospel. They gave up hope. Like Peter walking on the Sea of
Galilee, they looked at “nature” rather than at the Lord Jesus
Christ; like the Israelites on the border of Canaan, they looked
at the “giants in the land” instead of trusting the infallible pro-
mises of God; they were filled with fear, and took flight. They
began to listen to false prophets of despair who taught that the
Church is doomed to failure, and that it is “unspiritual” for
Christians to seek dominion over civilization. They then demon-
strated a major principle of life: If you believe that you will lose,
