The Holy Mountain 35

To the honor of the Lorp your God,

The Holy One of Israel,

For He has endowed you with splendor. . . .

Your gates will always stand open,

They will never be shut, day or night,

So that men may bring you the wealth of the nations. . . .”
(Isa. 60:5-6, 9, 11)

In line with this theme, the Bible describes us (Mal. 3:17) and
our work for God’s kingdom (1 Cor. 3:11-15) in terms of jewelry;
and, at the end of history, the whole City of Gad is a dazzling,
brilliant display of precious stones (Rev. 21:18-21).

The story of Paradise thus gives us important information
about the origin and meaning of precious metals and stones,
and therefore of money as well. Right from the beginning, God
placed value upon gold and gems, having created them as re-
flections of His own glory and beauty. The original value of
precious metais and stones was therefore aesthetic rather than
economic; their economic significance grew out of the fact that
they were valued for their beauty. Aesthetics is prior to econom-
ies,

Historically, gold came to serve as a medium of exchange
precisely because its value was independent of, and prior to, its
monetary function. Gold is not intrinsically valuable (only God
possesses intrinsic value); instead, it is valuable because man, as
God’s image, imputes value to it. Biblically, a medium of ex-
change is first a commodity, an item which men value as such.
Scripture always measures money by weight, by hard currency
(Lev. 19:35-37), and condemns all forms of inflation as a debase-
ment of currency (Prov. tt:1; 20:10, 23; Isa. 1:22; Amos 8:5-6;
Mic. 6:10-12).

God has placed value upon precious metals and stones, and
He has built in us an attraction for them; but He has also made
it clear that these things cannot be permanently owned or en-
joyed apart from fellowship with Him. The ungodly are allowed
to mine for these materials, and to own them for a time, in order
that their wealth may ultimately be possessed by the restored
people of God:

Though he [the wicked man] piles up silver like dust,
And prepares garments as plentiful as the clay;
