The Holy Mountain 33

You were in Eden, the Garden of God;

Every precious stone was your covering:

The ruby, the topaz, and the diamond;

The beryl, the onyx, and the jasper;

The lapis lazuli, the turquoise, and the emerald;
And the gold... . (Ezek, 28:13)

In fact, the ground seems to have been fairly littered with
sparkling gems of all sorts, according to the next verse: “You
walked in the midst of the stones of fire.” The abundance of
jewelry is regarded here as a blessing: fellowship with God in
Eden meant being surrounded with beauty. Moses tells us that
the gold of that land was good (i.e., in its native state, unmixed
with other minerals). The fact that gold must now be mined
from the earth by costly methods is a result of the Curse, partic-
ularly in the judgment of the Flood.

The stone that is called onyx in Scripture may be identical to
the stone of that name today, but no one knows for sure; and
there is even less certainty regarding the nature of bdeflium. But
some very interesting things about these stones appear as we
study the Biblical history of salvation, When God redeemed His
people from Egypt, He ordered the High Priest to wear special
garments. On his shoulders, the High Priest was to wear two
onyx stones, with the names of the 12 tribes written upon them;
and God declares these stones to be “stones of memorial” (Ex.
25:7; 28:9-12), A memorial of what? The only mention of the
onyx prior to the Exodus is in Genesis 2:12, with reference to the
Garden of Eden! God wanted His people to look at the High
Priest — who was in many ways a symbol of man fully restored
in Gad’s image —and thus to remember the blessings of the Gar-
den, when man was in communion with God. The stones were
to serve as reminders to the people that in saving them God was
restoring them to Eden.

An even more striking example of this is in what we are told
about God’s provision of manna. In itself, manna was a re-
minder of Eden: for even while God’s people were in the wilder-
ness (on their way to the Promised Land of abundance), food
was plentiful, good-tasting, and easy to find—as, of course, it
had been in the Garden. But, just in case they might miss the
point, Moses recorded that manna was the color of bdellum
(Num. 11:7)—the only occurrence of that word apart from its
