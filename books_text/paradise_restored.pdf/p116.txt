The plain fact is, as I say, that there is no longer
any king or prophet nor Jerusalem nor sacrifice nor
vision among them; yet the whole earth is filled with
the knowledge of God, and the Gentiles, forsaking
atheism, are now taking refuge with the God of
Abraham through the Word, our Lord Jesus Christ.

St. Athanasius, On the Incarnation [40]
