The Time Is at Hand 161

of Asia. Octavian changed his name to Augustus, a title of
supreme majesty, dignity and reverence. He was called the Son
of God, and as the divine-human mediator between heaven and
earth he offered sacrifices to the gods. He was widely proclaimed
as the Savior of the world, and the inscriptions on his coins were
quite frankly messianic—their message declaring, as Ethelbert
Stauffer has written, that “salvation is to be found in none other
save Augustus, and there is no other name given to men in
which they can be saved” (Christ and the Caesars (Westminster,
1955], p. 88).

This pose was common to all the Caesars. Caesar was God;
Caesar was Savior; Caesar was the only Lord, And they claimed.
not only the titles but the rights of deity as well. They taxed and
confiscated property at will, took citizens’ wives (and husbands)
for their own pleasure, caused food shortages, exercised the
power of life and death over their subjects, and generally at-
tempted to rule every aspect of reality throughout the Empire.
The philosophy of the Caesars can be summed up in one phrase
which was used increasingly as the age progressed: Caesar is
Lord!

This was the main issue between Rome and the Christians:
Who is Lord? Francis Schaeffer pointed out: “Let us not forget
why the Christians were killed. They were nor killed because
they worshiped Jesus. . . . nobody cared who worshiped whom
so long as the worshiper did not disrupt the unity of the state,
centered in the formal worship of Caesar. The reason the Chris-
tians were killed was because they were rebels... . they wor-
shiped Jesus as God and they worshiped the infimite-
personal God only. The Caesars would not tolerate this wor-
shiping of the one God only. It was counted as treason” (How
Shall We Then Live? [Revell, 1976], p. 24).

For Rome, the goal of any true morality and piety was the
subordination of all things to the State; the religious, pious man
was the one who recognized, at every point in life, the centrality
of Rome. R. J. Rushdoony observes that “the framework for
the religious and familial acts of piety was Rome itself, the cen-
tral and most sacred community. Rome strictly controlled all
rights of corporation, assembly, religious meetings, clubs, and
street gatherings, and it brooked no possible rivalry to its cen-
trality. .. . The state alone could organize; short of conspiracy,
