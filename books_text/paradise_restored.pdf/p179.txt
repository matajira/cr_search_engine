»
A BRIEF SUMMARY OF THE REVELATION

The Book of Revelation is not impossible to understand, but
it is exceedingly complex. Its extensive use of Old Testament im-
agery would require volumes to explore fully. My purpose in the
present book, of course, is simply to present in broad outline a
Biblical exposition of the eschatology of dominion. (Those
wishing for a more complete treatment of these issues should
consult my commentary on Revelation, The Days of Vengeance,
as well as other works listed in the Bibliography.)

As a whole, the Book of Revelation is a prophecy of the end
of the old order and the establishment of the new order. It is a
message to the church that the terrifying convulsions coursing
throughout the world in every sphere comprised the final “shak-
ing of heaven and earth,” ending once and for all the Old Cove-
nant system, announcing that the kingdom of God had come to
earth and broken Satan’s hold on the nations. In the destruction
of Jerusalem, the old kingdom, and the Temple, God revealed
that they had been merely the scaffolding for His eternal City,
His Holy Nation, and the most glorious Temple of all.

See to it that you do not refuse Him who is speaking. For if
those did not escape when they refused Him who warned them
on earth, much less shall we escape who turn away from Him
who warns from heaven. And His voice shook the earth then,
but now He has promised, saying, “Yet once more I will shake
not only the earth, but also the heaven.” And this expression,
“Yet once more,” denotes the removing of those things which can
be shaken, in order that those things which cannot be shaken
may remain. Therefore, since we are receiving a kingdom which
cannot be shaken, let us show gratitude, by which we may offer
to God an acceptable service with reverence and awe; for our
God is a consuming fire (Heb. 12:25-29),

169
