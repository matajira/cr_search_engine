Publisher's Epilogue 329

The Five Points of Biblical Covenantalism

Obviously, I believe in the eschatology of victory. I wrote a
book about it in 1981.'° Nevertheless, the biblical case for op-
timism can be overemphasized. Chilton knows this, too. He and
I wrote an essay outlining what we then believed to be the four
points of Christian Reconstruction: 1) the sovereignty of God;
2) biblical law; 3) Cornelius Van Til’s biblical presuppositional-
ism (the Bible alone judges the Bible, not human logic); and 4)
biblical optimism.!’? Postmillennialism is at most only one
quarter of the message. But since that time, he and I learned
from Pastor Ray Sutton that there is another even more funda-
mental doctrine: the covenant.

Yes, I know: covenant theology is as old as John Calvin,
over four centuries old. But in the fall of 1985, Sutton made a
stunning clarification in covenant theology, one vaguely hinted
at by Westminster Seminary Professor Meredith G. Kline in the
early 1960's, but never developed by him or his disciples. What
Sutton discovered was that Kline’s earlier discovery of a five-
point covenant structure in Deuteronomy also applies through-
out the Bible, and it applies to the three covenant institutions of
church, state, and family.

Sutton presented his findings at a Wednesday evening Bible
study which Chilton attended. Immediately, Chilton recognized
the implications of this five-point structure for the Book of Rev-
elation. He had become mired for months in the manuscript,
looking for a key to unlock Revelation’s structure. (I remember
this well; I was paying his salary to write it.) Sutton’s discovery
of Kline’s insight opened the door. Within a few weeks after
reading Sutton’s very rough 60-page preliminary manuscript
that became That You May Prosper, Chilton had completed his
long-awaited first draft of The Days of Vengeance.

Chilton’s commentary on Revelation is a masterpiece, but it
eannot be understood properly without an understanding of
Ray Sutton’s development of Kline’s five-point covenant model,

16, Gary North, Unconditional Surrender: God's Program for Victory (Ft.
Worth, Texas: Dominion Press, [1981] 1987).

17. Gary North and David Chilton, “Apologetics and Strategy,” Christianity
and Civilization 3 (1983).
