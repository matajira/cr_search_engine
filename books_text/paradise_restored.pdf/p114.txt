104 Paradise Restored

nant system, the Lord sends out His messengers to gather His
elect people into His New Synagogue. Jesus is actually quoting
from Moses, who had promised: “If your outcasts are at the
ends of heaven, from there the Lorp your God will synagogue
you, and from there he will take you” (Deut. 30:4, Septuagint).
Neither text has anything to do with the Rapture; both are con-
cerned with the restoration and establishment of God’s House,
the organized congregation of His covenant people. This
becomes even more pointed when we remember what Jesus had
said just before this discourse:

O Jerusalem, Jerusalem, who kills the prophets and stones those
who are sent to her! How often I wanted to synagogue your chil-
dren together, the way a hen gathers her chicks under her wings,
and you were unwilling. Behold, your House is being left to you
desolate! (Matt. 23:37-38).

Because Jerusalem apostatised and refused to be synagogued
under Christ, her Temple would be destroyed, and a New
Synagogue and Temple would be formed: the Church. The New
Temple was created, of course, on the Day of Pentecost, when
the Spirit came to indwell the Church. But the fact of the new
Temple’s existence would only be made obvious when the
scaffolding of the Old Temple and the Old Covenant system was
taken away. The Christian congregations immediately began
calling themselves “synagogues” (that is the word used in James
2:2), while cailing the Jewish gatherings “synagogues of Satan”
(Rev. 2:9; 3:9). Yet they lived in anticipation of the Day of Judg-
ment upon Jerusalem and the Old Temple, when the Church
would be revealed as the true Temple and Synagogue of Gad.
Because the Old Covenant system was “obsolete” and “ready to
disappear” (Heb. 8:13), the writer to the Hebrews urged them to
have hope, “not forsaking the synagoguing of ourselves
together, as is the habit of some, but encouraging one another;
and all the more, as you see the Day approaching” (Heb. 10:25;
ef. 2 Thess. 2:1-2).

The Old Testament promise that God would “synagogue”
His people undergoes one major change in the New Testament.
Instead of the simple form of the word, the term used by Jesus
has the Greek preposition epi prefixed to it. This is a favorite
