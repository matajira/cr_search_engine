1272

Public opinion, 445-46
Public schools, 20
Public works, 159, 162
Punishment
fits crime, 415, 436
respect for law, 440
right of, 422-23
see also Lex talionis, Prison, Sanction
Puritanism, 14, 889n
Puritans
commen property, 547
creeds and power, 186
Deuteronomy, 84n
just price, 682
preaching, 4i4n
sanctions, 446-47
slavery, 178, 186
Pyramid, 885-88, 920

Quakers, 19-20, 181, 186, 189, 200n,
232, 372, 527

Quality of life, 583, 588

Quarantine, 475, 580

Quigley, Carroll, 788n

Quotas, 544, 595

Rabbi Judah, 498, 840, 1008

Rabinowitz, J. J., 116-17

Race relations, 156n

Rachel, 137, 212, 226, 246, 254, 262,
650

Racism, 180, 183

Radiation, 475

Rahab, 798, 800

Railroad, 1149-50

Raines, John, 368n

Rambam (see Maimonides)

Ramban (see Nachmanides)

Randolph, Edmund, 1195

Randomness, 77, 763-64

Ransom, 326-28

Ransom, Roger, 375-77

Rapture, 172, 966

Rationalism, 1092-93, 1136-38 (see
also Dialecticism, Dualism,
Epistemology)

Reasoning, 760-61

TOOLS OF DOMINION

Rebekah, 252, 254, 261
Rebellion, 440, 960, 964, 978, 983, 991
Rebellious son, 283-84, 310-11
Reciprocal harm, 1105-7
Reconstruction, 447n
Redactors, 1080
Reddaway, P., 169n
Redemption, 145, 209, 213, 225
Reductionism, 364-65, 367
Reformation, 986
Regan, D., 1110
Rehabilitation, 118, 222, 324, 395,
402, 1168
Rehoboam, [62-63
Reimann, Gueuter, 613n
Relationships, 263
Religious tests, 1197-98 (see
also Oath)
Remarriage, 663
Renaissance, 888
Rent
definition, 726
interest &, 507, 1182
money on, 1179, 1181-83
prisons, 118n
responsibility, 638-39
seeking, 787
subordination, 706
Reporters, 202
Representation
adoption, 252
costs and benefits, 555
covenantal, 77-79, 670, 883
criminal and victim, 287-89
final judgment, 288
future generations, 591-95
God's lawsuit, 296
inescapable, 287
Jesus or Pilate, 289
parents, 278
point two, 278
satanic, 279, 285-86, 288
sin, 289
unmarried women, 275
victim, 294
wedding, 270
which deity?, 429
