Wives and Concubines 263

betrothal was the same covenantally as a marriage vow, Sexual rela-
tions with such a woman was a capital crime (Deut. 22:23-24).
Thus, there were two kinds of betrothals; they were covenantally
and legally different. The covenantal sign that distinguished between
them was the dowry. The difference was covenantal — free vs. unfree—
but the visible manifestation of this difference was economic,

The question then arises: Which was the determining factor in
determining her status, the legal or the economic? The Bible always
places the foundational status of all human relationships in the legal sphere, noi
the physical, intellectual, emotional, or economic sphere. It is this legal rela~
tionship that governs all of God’s relationships with mankind, either
saved or lost. What was the covenantal basis of her legal status as a
wife? Her position as an adopted daughter. Her father allowed her to be
adopted by another family. He relinquished his position as her cove-
nantal representative before God.

What about her status as a concubine? Her father determined
the economic terms of her adoption. He chose to keep the bride price
for himself. In so doing, he placed her in a second-best legal status.
His motivation was no doubt deeply tied to his personal or familistic
economic goals, but the basis of her status as a concubine was the
result of a legal transfer of covenantal authority over her, not economics as
such. Her primary status was that of wife, meaning an adopted sister
(Song 4:9-10). Her secondary legal status as a concubine stemmed
from the nature of the one-step transfer of wealth from the bride-
groom to her father. Biblical law recognized her vulnerability and
took steps to protect her. Her father determined her legal status; eco-
nomics was his motivating factor in making this legal determination.

Consummation and Legal Protection

Once their sexual union had taken place, the marriage was cove-
nantally complete. It then became a capital crime for another man to
take her sexually. Thus, she became a true wife. We now return to
the original question: Could her husband then sell her to anyone
who would pay him what he had paid to her father? The text does
not indicate any such right on his part. He could sell her to another
Hebrew during the betrothal period, with her family’s consent. He
could thereby transfer her covenantal position as an adopted woman,
though not to a resident alien, who did not have the legal right of
adopting Hebrews into his household. But once covenantally bonded
sexually before God, she became his wife. He could not divorce her,
