408 TOOLS OF DOMINION

being arrested. It went on for eleven minutes. Finally, one man, a
factory director, stopped clapping and sat down, then the whole
group immediately stopped and sat down, That night the man was
arrested, and he then received a ten-year sentence.”

There is only one way to explain this: the desire of the State to become
God and to impose hell on earth, It became a goal of State policy to destroy
men’s lives, to leave them without earthly hope in the future. It was
easy to go to jail without a trial. The Special Boards attached to the
secret police, the OSO’s,"8 handed down “administrative penalties,”
not sentences. “The OSO enjoyed another important advantage in
that its penalty could not be appealed. There was nowhere to appeal
to. There was no appeals jurisdiction above it, and no jurisdiction
beneath it. It was subordinate only to the Minister of Internal
Affairs, to Stalin, and to Satan.”® It is not surprising that the camps
became the closest thing in recorded history to hell on earth.

The prison is a bureaucracy, not a market-oriented institution, It
is run by the State through taxes; it is a bureaucratic management
system, not a profit management system. Men are trained to fol-
low orders, not to innovate, take risks, and meet market demand.
There are many arguments against prisons, as revealed by an enor-
mous bibliography on alternatives to prisons,?! but the most impor-
tant one is that they thwart the biblical principle of restitution.

The prison also creates other horrors, such as homosexuality and
training in criminal behavior for the younger inmates by the “skilled”
older inmates. It puts too much power in the hands of prisoners, who
ran commit rape and even murder with their AIDS infections.” It

 

87. fbid., pp. 69-70

88. Thid., p. 275.

89. Fhid., p. 285.

90. See Gary North, “Statist Bureaucracy in the Moder Economy,” in North, 4n
Introduction to Christian Economics (Nutley, New Jersey: Craig Press, 1973), ch. 20. See
also Ludwig von Mises, Bureaucracy.

91. James R. Brantley and Marjorie Kravitz (eds.), Alternatives to Institutionaliza-
tion: A Definitive Bibliography, published by the National Criminal Justice Reference
Service of the National Institute of Law Enforcement and Criminal Justice, a divi-
sion of the Law Enforcement Assistance Administration, U.S. Depariment of
Justice (May 1979), 240 pages.

92. National columnist Mike Royko has actually recommended prison sentences
for computer file break-ins rather than fines because intelligent middle-class
prisoners will be raped in jail. “If the computer vandals are as bright as they think
they are, they'll decide that they don't want to he forcibly betrothed to some hulk of a
cellmate with a shaved head and 10 tatoos.” Mike Royko, “No software in his heart
for hackers,” Washington Times (Nov, 11, 1988). This is a politically conservative news-

paper,

 
