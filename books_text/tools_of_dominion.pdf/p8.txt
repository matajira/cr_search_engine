vill TOOLS OF DOMINION

25. Finders Should Not Be Keepers .............+4-
26. Bribery and Judgment .......... 6000s eee ees
27. Sabbatical Liberty.......
28, Feasts and Citizenship .
29. The Curse of Zero Growth
30. God's Limits on Sacrifice...
31. The Economics of the Tabernacle . .
32, Blood Money, Not Head Tax...
33. Sabbath Rest vs. Autonomy... .
34. The Ability to Teach .

Conclusion

Part III: APPENDIXES

APPENDIX A— Common Grace, Eschatology,
and Biblical Law ........0.2..02.00.

APPENDIX B— Maimonides’ Code: Is It Biblical? ......

APPENDIX C — The Hoax of Higher Criticism ..........

APPENDIX D— The Epistemological Problem

of Social Cost... 2.0... 0c cece eee eae
APPENDIX E-— Pollution in the Soviet Union ...........
APPENDIX F— Violent Crime in the United States, 1980 ..

APPENDIX G— Lots of Free Time: The Existentialist

Utopia of S.C. Mooney ...............

APPENDIX H — Selling the Birthright: The Ratification

of the U.S. Constitution ...............
SCRIPTUREINDEX .....- 0.00.0 eee e cece cence eee
GENERAL INDEX ..... 0.0... ccc ec eee

 

 
 
 
 

 

774

-. 785
.8ti

 

826
849
874

892
- 903
- 913
.. 919

- 928

 

 

 

 

- 953

 

- 998

.- 1063

1085
.- 1160
. 1167

-- 1179

- 1190
» 1217
.. 1231
