120 TOOLS OF DOMINION

against criminals to protect itself. Today, the sanctions are themselves
criminal: they penalize the victims a second time (tax bills for
prisons). The victims are robbed twice. They are threatened with vio-
lence if they refuse to pay: first by the robber, then by the tax collector.

The perverse nature of the modern criminal justice system is
slowly beginning to be recognized. A handful of judges are trying to
impose a system of restitution by criminals to victims. But this at-
tempt suffers greatly from the fact that there is no ready market for
the capitalized value of the criminal’s future stream of production
because indentured servitude is illegal. The dilemma of the judge
has been stated by Judge Lois Forer: “Most defendants I see are un-
employed, often unemployable. A sentence of restitution or repara-
tion may not be enforceable. On occasion it has spurred a defendant
who has been on welfare for years to go out and get a job. It rarely
produces adequate compensation to the victim. But the underlying
principle seems to me to be valid. If people are to be held legally re-
sponsible for their acts, it follows that they should be responsible for
the harm they have caused.”2! What the modern humanist dares not
admit to himself or in public— and what embarrassed Christians also
are fearful of admitting —is that the Bible’s system of indentured ser-
vitude for criminals was and still is basic to the Bible’s system of jus-
tice in which criminals are made legally and economically responsible
for the harm they have caused.

Confusion Over Definitions: Slave and Bondservant

The Ten Commandments summarize for man the moral and
legal foundations of a free society. Immediately following the pres-
entation of these principles of freedom, God gives the laws regarding
the freeing of indentured servants. A nation of former slaves could
appreciate these laws. They had endured the trials of a slave society.
If ever there were people who were ready, historically and
environmentally, for a message of cultural liberation, they were the
Hebrews of the exodus period. Therefore, before considering the
specific implications of this verses governing slave marriages (Ex.
21:2-11), we need first to consider the role of slavery in the Old Testa-
ment period.

Before considering slavery in general, let us consider the use of

the term in the Bible. This following fact may astound some readers;

 

21. Lois G. Forer, Criminals and Victims: A Trial Judge Reftects on Crime and Panish-
ment (New York: Norton, 1980), p. 12.
