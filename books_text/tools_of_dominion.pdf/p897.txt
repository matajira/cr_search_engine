Ged’s Limits on Sacrifice 889

despite their sin in building the calf. Their giving was voluntary,
meaning beyond the mandatory tithe, These were what Protestants call
“gifts and offerings.” So powerful was the motivation for sacrificial
giving that the people had to be restrained. They were not to make
the same mistake again: believing that the work of their hands could
save them from the wrath of God, believing that the greater their
giving, the less the wrath. Furthermore, they were to preserve capi-
tal for future productive uses.

Men need to sacrifice to their gods. They insist on it. Their sac-
rifice links them to a source of power. But God warns men that He is
not so concerned about men’s material sacrifices; instead, He is con-
cerned about justice, humility, and mercy (Deut. 10:12; I Sam.
15:22; Micah 6:8). He desires the sacrifice of a contrite heart: “For
thou desirest not sacrifice; else would I give it: thou delightest not in
burnt offering. The sacrifices of God are a broken spirit: a broken
and a contrite heart, O God, thou wilt not despise” (Ps. 51:16-17).
Only on these terms are burnt offerings acceptable to God. God
grants wealth and power, not in terms of ritualistic precision, but in
terms of conformity to an ethical law-order (Deut. 8). Righteousness
is more important than ritual (II Chron. 30:13-20).

Early Protestantism, especially Puritan and Anabaptist Protestant-
ism, criticized the cathedral builders. They argued that the money
spent on cathedrals should have been given to the poor, or used for
other purposes. As it has turned out historically, the great cathedrals
have become tourist attractions, as the Christian faith of the public
has waned. But these magnificent structures still stand as testimon-
ies to the dedication, skill, and sacrifices of men for their God. The
cathedrals reflect the builders’ and worshippers’ conception of the
authority and majesty of God. The long-run perspective of the
builders is still evident: they expected their work to survive. They
expected it to glorify God for centuries. This long-run perspective is
an important aspect of serious Christian faith. Men’s time perspec-
tive is reflected in their architecture.*” So is their view of God.*

37, The cathedral becomes a pyramid rather than a home for God if the faith of
the builders has been transferred to another god, The cathedrals of Europe have be-
come tourist attractions. The cnormous, unfinished Episcopalian pyramid, the
Church of St. John the Divine, is still being built in New York City after a century of
tabor and fund-raising. Meanwhile, the Harlem ghetto has moved almost to its bor-
ders, and it is unsafe to visit it at night.

38, Little that is orthodox remains in today’s mainline Anglican and Roman
Catholic churches, even in their liturgies, although there are pockets of orthodoxy.
Nevertheless, their cathedrals have survived, What visible token remains of Crom-
well's reign? A creed: the Westminster Confession of Faith. Almost nothing visible
remains of Puritanism; its legacy was almost entirely ideological and theological.
