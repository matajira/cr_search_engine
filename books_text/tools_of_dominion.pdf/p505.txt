Knowledge, Responsibility, and the Presumption of Guilt 497

owner has an incentive to maximize the proceeds from the sale of the
survivor, since both of them gain an equal share of the sale price.
‘The owner of the dead beast cannot come before the judges and
claim that his beast was worth ten times as much as it really was
worth. The judges do not have to call in specialists in assessing retro-
actively the value of dead cattle. They can leave it to both owners to
settle their differences. Each man has an incentive to get the transac-
tion over with, Neither can trick the other (or the judges) as to the
former value of the dead ‘beast. The market then reveals the live
beast’s value.

The dead beast is also worth something. The Old Testament
rules prohibiting the sale of unclean dead beasts (Deut. 14:21; Lev.
17:15) do not apply in the New Covenant era, Even under the Old
Covenant, the beast could be sold to a resident alien gentile (Deut.
14:21b). Today, the beast can lawfully be sold to Jew or gentile if the
carcass meets public health standards. Each owner reccives an equal
share of the returns.

What if a run-of-the-mill bull kills a champion? The owner of the
champion suffers the greater loss. But since it cannot easily be deter-
mined which bull initiated the violence, the court is not required by
God to examine the detailed question of what is owed to the owner
of the dead beast. This law implicitly recognizes the limitations on
courts in assessing responsibility in the case of the behavior of animals.
Owners of prize animals are forewarned to take care of their property.

Jewish Law: Whose Ox Is Gored?

The Mishnah makes some very peculiar exemptions to this law.
“If an ox of an Israelite gored an ox that belonged to the Temple, or
an ox that belonged to the Temple gored the ox of an Israelite, the
owner is not culpable, for it is written, The ox of his neighbour [Ex.
21:35]—not an ox that belongs to the Temple.”! This is most pecu-
liar, One would think that if any ox was to be protected by the threat
of damages imposed on the owner of the killer ox, it would be an ox
belonging to the temple. Why the word “neighbor” excluded the tem-
ple is not explained.

The Mishnah continued: “If an ox of an Israelite gored the ox of
a gentile, the owner is not culpable. But if the ox of a gentile gored
the ox of an Israclite, whether it was accounted harmless or an at-

1. Baba Kamma 4:3, The Mistnah, edited by Herbert Danby (New York: Oxford
University Press, [1933] 1987), p. 32

 
