Criminal Law and Restoration 397

ognized private sphere clearly distinct from the public sphere, and
the private individual cannot be ordered about but is expected to
obey only the rules which are equally applicable to alll. It used to be
the boast of free men that, so long as they kept within the bounds of the
known law, there was no need to ask anybody’s permission or to obey
anybody’s orders. It is doubtful whether any of us can make this
claim today.”# If men must ask permission before they act, society
then becomes a top-down bureaucratic order, which is an appropriate
structure only for the military and the police force (the “sword”),**
The Bible specifies that the proper hierarchical structure in a biblical
covenant is a bottom-up appeals court structure (Ex. 18).

Adam was allowed to do anything he wanted to do in the garden,
with only one exception. He had to avoid touching or eating the for-
bidden fruit. He did not have to ask permission to do anything else.
He was free to choose.** This biblical principle of legal freedom is to
govern all our decisions.” This is stated clearly in Jesus’ parable of
the laborers who all received the same wage. Those who had worked
all day complained to the owner of the field. The owner responded:
“Friend, I do thee no wrong: didst not thou agree with me for a penny?
Take that thine is, and go thy way: I will give unto this last, even as
unto thee. Is it not lawful for me to do what I will with mine own? Is
thine eye evil, because I am good?” (Matt. 20:13-15). Neither the
owner nor the workers had to get permission in advance from some
government agency. God leaves both sides free to choose the terms of
labor and payment.

Because God alone is omniscient, He controls the world per-
fectly. Men, not being omniscient, must accept judicial restrictions
on their own legitimate spheres of action. In doing so, they acknowl-
edge their position as creatures under God. They must face the real-
ity of their own limitations as creatures. They must not pretend that
they can foresee the complex outcome of every activity of every per-

 

43, B.A, Hayek, The Constitution of Liberty (Cniversity of Chicago Press, 1960),
pp. 207-8,

44, Ludwig von Mises, Bureaucracy (Cedar Fails, lowa: Genter for Putures Educa-
tion, [1944] 1983), ch. 2, Distributed by Libertarian Press, Spring Mills, Pennsylvania,

45. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 2.

46. Milton and Rose Friedman, Free to Choose: A Personal Statement (New York:
Harcourt Brace Jovanovich, 1980).

47. Grace Hopper, who developed the computer language Cobol, and who served
as an officer in the U.S. Navy until she was well into her seventies, offered this
theory of leadership: “It’s easier to say you're sorry than it is to ask permission.”

 
