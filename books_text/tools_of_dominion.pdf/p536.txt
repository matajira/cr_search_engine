528 TOOLS OF DOMINION

cause all crimes are above all crimes against God. An unwillingness on
the part of civil magistrates to enforce God’s specified sanctions
against certain specified public acts calls forth God’s specified cove-
nantal cursings against the community. This threat of God’s sanc-
tions is the fourth section of God’s covenant; without this covenant,
either explicit or implicit, no community can exist. Only when we
clearly recognize the theocentric nature of deterrence — and when we
are ready to seek to have it recognized publicly in our civil and ecclesias-
tical statute books — can we legitimately begin to speak about deterring
criminal behavior for the protection of the community.

The Bible does not distinguish between civil law and criminal
law. All sins are crimes against God, for they break His law. All pub-
lic sins must be restrained by one or more of God’s covenantal agen-
cies of government: family, church, and State. Certain public trans-
gressions of God’s law are specified as acts to be punished by the civil
magistrate. In the modern world, we call these acts crimes. (The
King James Version uses the word “crime” only twice, and “crimes”
only twice.) ‘he civil government enforces biblical laws against such
acts. The general guideline for designating a particular public act as
a crime is this: if by failing to impose sanctions against certain speci-
fied public acts, the whole community could be subsequently threat-
ened by God’s non-civil sanctions war, plague, and famine— then
the civil government becomes God’s designated agency of enforce-
ment. The civil government’s primary function is to protect the community
against the wrath of God by enforcing His laws against public acts that
threaten the survival of the community,

The perverse practice of modern jurisprudence of allowing a per-
son who has been declared legally innocent of a crime to be subse-
quently sued for damages in civil court by alleged victims cannot be
found in the Bible. There is no distinction in the Bible between
criminal law and civil law; if the civil magistrates are entitled to en-
force a rule or law, then this rule or law should be classified in the
modern world under a criminal statute. Because the State is not om-
niscient, God allows self-proclaimed victims of lawless behavior to
sue other individuals in the presence of a civil magistrate, which we
call civil procedure or torts, but if the State is the lawful agency of
enforcement, then we are always talking about criminal! acts. Con-

44, Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4.
