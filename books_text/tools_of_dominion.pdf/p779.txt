Impartial Justice and Legal Predictability 71

one: Chinese in Southeast Asia and the United States, Indians in
Africa, West Indies blacks in New York, and Jews everywhere.

This raises an interesting question. What if the false witnesses
accuse successful people in general of wrongdoing? What if they argue
that the rich are inescapably economic oppressors unless they give
their wealth, or a major portion of their wealth, to the poor? False
witness need not be directed against an individual in order to have
evil consequences. It can be directed against any group: religious,
racial, national, or class. In this case, false witness against “rich men
in general” falls under God’s condemnation.

A philosophy or ideology that condemns the rich in general is
equally as perverse as a philosophy that condemns the poor in gen-
eral. If men are rich because they or their entire society have con-
formed themselves to biblical law (Deut. 28:1-14), they are not to be
condemned. To condemn them is to condemn God and His law-order, Gon-
versely, if men are poor because they or their entire society are in re-
bellion against God and God’s law (Deut. 28:15-68), they are not to
be upheld. Jo uphold them is to uphold Satan and his law-order.©

The twentieth century has seen the temporary triumph of many
philosophies that advocate State-enforced policies of compulsory
wealth redistribution. Generally, these philosophies are promoted in
the name of democracy. In effect, advocates of these philosophics
propose a revision of the eighth commandment: “Thou shalt not
steal, except by majority vote.” Other versions of collectivism are
promoted as elitist programs that need to be imposed on the “rich” in
the name of the poor, even when a majority of voters are opposed to

 

hard-working Jews. At the same Lime, the lingering sense of being set apart from the
society at large has given Jews a sense of covenantal mission: to outperform the gen-
tile majority. If the acids of modernism do their predictable work, economic and so-
cial success will tend to produce Jews who no longer have the “outsider’s” mentality,
and the humanistic quest for unity will undertnine the sense of covenantal or family
mission. We are seeing this in the United States today, where Jews commonly marry
non-Jews, since (hey come into social contact with each other in the secular univer-
sities. As Schumpeter said of capitalism, Jewish performance is likely to fail in the
long run because of its success. The very secular institutions that allow Jews to com-
pete without religious, social, or racial restrictions will undermine their sense of
“Jewishness.”

39. Thomas Sowell, The Econumics and Politics of Race: An International Perspective
(New York: William Morrow, 1983), Pt. I.

40. R. J, Rushdoony, “The Society of Satan,” Christian Economics (July 7 and
Aug. 4, 1964); reprinted in Biblical Economics Today, II (Oct./Nov. 1979).
