62 TOOLS OP DOMINION

They have refused to tell the world that God really does have specific
answers for every area of life, including economics and politics.
Christians have preferred to comfort themselves as they have sat in
their rocking chairs in the shadows of history, rocking themselves
back and forth, and saying over and over: “I am not a theocrat. Iam
not a theocrat.”

What this phrase means is simple; God does not rule, so neither will I.

But what if God does rule? What if He has given us the unchang-
ing laws by which He expects His people to rule? What if He has
given us the tools of dominion, and we have left them in the rain to
rust? What will He do with our generation?

Just what He did with Moses’ generation: He will leave them be-
hind to die in the wilderness.
