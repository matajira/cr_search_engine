466 TOOLS OF DOMINION

The beast was roaming around as if it had no prior record of vio-
lence. The victim did not recognize the danger involved in being
near the beast.

The Bible does not reveal in these passages regarding goring oxen
the evidence that constitutes judicially binding prior knowledge.
What kind of information did the owner have to possess in order for
the court to declare him guilty? The rabbinical specialists in Jewish
law said that the animal had to have gored someone or other animals
on three occasions before the owner became personally liable.”
Maimonides spelled it out in even greater detail: any domesticated
animal must first kill three heathen (gentiles), plus one Israelite; or
kill three fatally ill Israelites, plus one in good health; or kill three
people at one time, or kill three animals at one time.*!

This is an excessive number of prior infractions in order to ac-
tivate capital sanctions. Subsequent victims need more protection
than these Talmudic rules would provide. It is far more reasonable
to conclude that a single prior conviction should suffice to identify
the beast as dangerous. What should be obvious in any study of tra-
ditional Rabbinic laws regarding killer oxen is the extent to which
the rabbis would go in order to exempt the owners. Maimonides’ ex-
ample is remarkable, found in Chapter Ten of the first treatise on
torts, “Laws Concerning Damage by Chattels”:

11. No owner need pay ransom unless his animal kills outside his prem-
ises. But if it kills on his premises, then although it is liable for stoning, the
owner is exempt from paying ransom. Thus if one enters a privately owned
courtyard without the owner’s permission—even if he enters to collect
wages or a debt from the owner—and the householder’s ox gores him and
he dies, the ox must be stoned, but the owner is exempt from paying ran-
som since the victim had no right to enter another's premises without the
owner’s consent.

12, If one stands at the entrance and calls to the houscholder, and the
householder answers, “Yes,” and he then enters and is gored by the house-
holder’s ox and dies, the owner is exempt, for “Yes” means no more than
“Stay where you are until I speak to you.”

He even exempted the owner of a notorious ox that has gored a preg-
nant woman whose child is born prematurely. “For Scripture im-

20. Albeck, Jewish Law, col, 322.

21. Maimonides, Torts, “Laws Concerning Damage by Chatiels,’ Chapter Ten,
Scetion Three, p. 36.

22, Ibid., p. 38.
