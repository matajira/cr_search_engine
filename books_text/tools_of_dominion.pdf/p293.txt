Victim’s Rights os. the Messianic State 285

The Govenant Lawsuit

Adam and Eve had to serve as witnesses and judges in the gar-
den. There was no escape from these two offices. The serpent had
forced their hand. They had heard Satan's temptation, namely, that
they could be as God if they disobeyed God (Gen. 3:5). They had
become witnesses. ‘lhey could not escape from their knowledge of
the serpent’s words. He had spoken in their presence. !° They could
stand with God and God’s law by obeying God’s word concerning
Himself, the forbidden fruit, and the promised sentence of execu-
tion, or they could stand with Satan and his word concerning God,
the forbidden fruit, and the promised execution, But when called
upon by God to testify in His court, they would be required to tes-
tify, either against themselves if they stood with Satan or against
Satan if they stood with God.!! They both sought to escape self-
incrimination. Adam blamed Eve, and Eve blamed the serpent.
Still, there was no available judicial escape. Their fig leaves testified
against them. They knew they were guilty, and their wardrobes testi-
fied to their sense of guilt.

They also had to serve as judges. They could issue a condemna-
tion of God by eating the forbidden fruit, or they could issue a con-
demnation of Satan, either by eating of the tree of life, or by eating
from any tree except the forbidden one, or by not eating anything at
all. But they could not avoid serving as fudges. They had to decide.
They had to act. They had to render judgment.'?

The two offices, witness and judge, were inherent in their posi-
tion as God’s authorized representatives on earth (Gen, 1:26-28). Be-
cause of Satan’s rebellion and his temptation of them, they were forced
to decide: Against whom would they bring the required covenant lawsuit:

 

10. This assumes that Adam was at Eve’s side when the serpent spoke. [f he was
not, then only Eve heard him speak. She should then have gone to Adam for confir-
mation, and he would have had to ask the serpent to repeat his claim. As I argue in
my study of the incident, in order for Satan to gain the biblically specified pair of
witnesses against God, they both had to act against God's law. I think that Adam
was uext ta Eve when the serpent spoke, Adam let her act in his name. He allowed
her to test the serpent’s claim.

U1. This is the theological foundation of the idea of the subpoena. The State has a
legitimate right to compel the appearance of an individual in court, as well as com-
pel his truthful testimony. This right is denied by some libertarians. Cf. Murray N.
Rothbard, Far a New Liberty: The Libertarian Manifesto (rev. ed.; New York: Collier,
1978), p. 87.

12, North, Dominion Covenant: Genesis, Appendix E: “Witnesses and Judges

 
