714 TOOLS OF DOMINION

of accuracy to identify cases where a charity loan is legally obligatory
for the potential lender?”

The lender decides who is deserving of his loan and who is not.
This is his moral choice. God will judge him, not the State. How-
ever, once the lender grants this unique, morally enjoined charity loan, he
may not extract an interest payment. This is a negative injunction—
not doing something which is forbidden by law —and therefore it is
legitimately enforceable by civil law, as surely as the civil magistrates
in ancient Israel were supposed to enforce the release of debt slaves®
in the seventh (sabbatical) year (Deut. 15:12-15), The requirement to
lend to the brother in need under the terms specified in biblical law,
being a positive injunction, therefore comes under the self-govern-
ment provisions of the conscience and the negative sanctions of God.
This positive injunction is not under the jurisdiction of the civil
courts. On the other hand, the prohibition against interest on these
unique loans, being a negative injunction, does come under the cn-
forcement of both civil courts and church courts.

The key to understanding the Bible’s civil definition of poverty is
the loan’s contract. There must be a mutually agreed-upon contract,
explicit or implicit, in order to establish a legally enforceable loan. If
the borrower comes to the lender and calls upon him to honor Deu-
teronomy 15:7-8, then the borrower admits that his is a special case,
a charity lean, and it is governed by the civil law’s terms of the sab-
batical year and the prohibition against interest. The borrower
makes his request a matter of conscience.

In so doing, he necessarily and inescapably places himself under
the terms of biblical civil law, If he cannot repay his debt on time, he can be
legally sold into bondservice. This is not a collateralized commercial
loan. The borrower is so poor that he has no collateral except his
land. He chooses not to use his land as collateral. He therefore
chooses not to become a Jandless man, meaning landless until the
next jubilee year. Yet he is still in dire need. All he can offer as col-
lateral is his promise, his cloak, and his bodily service until the next
sabbatical year should he default. Thus, the borrower admits that he
in principle has already become a bondservant. He admits through

7. This is the question that S. C. Mooney raises in his attempt to remove any dis-
tinction between charity loans and business loans. Mooney, Usury, pp. 123-27.

8, A debt slave was a person who had asked his neighbor for a morally man-
datory, zero-interest charity loan, and who had then defaulted. He was then placed
in bondage until the sabbatical year, or until his debt was paid.
