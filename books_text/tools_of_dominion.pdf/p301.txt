Victim's Rights us. the Messtanic State 293

is totally misleading for people to use this passage as a proof text that
Jesus established a new civil penalty, or even no penalty at all, for
the civil crime of adultery. He did not abandon the Mosaic law in
John 8. On the contrary, He followed the Mosaic law’s procedural
requirements to the letter. She was publicly innocent in terms of the pro-
cedural requirements of the Mosaic law. Thus, He did not execute His his-
torical wrath upon her in His capacity as perfect humanity. Only the
witnesses were allowed to do that, and they had departed. He would
deal with her later as God, the perfect Witness, on judgment day in
His court; until then, she was granted time to repent and reform her
ways. So are all the rest of us,

Obvious, isn’t it? Yet for several generations, pietists and anti-
nomians (those who reject biblical law) have persuaded Christians
that John 8 represents some remarkable break with the Old Testa-
ment. Christians who hate God’s law also hate the New Testament,
so they do whatever they can to distort it and misinterpret it, even
when their misinterpretations lead to obviously preposterous conclu-
sions. They do not worry about preposterous conclusions; they
worry instead about a sovereign God who threatens individuals and
society with judgments in history for sin. They are in principle
adulterers themselves, and they are looking for an escape from God's
authorized civil sanctions against adultery, should they someday fall
into this sin. They are looking for loopholes—civil, ecclesiastical,
and psychological.

Witnesses as Unauthorized Prosecutors

There is another aspect of this incident that must be considered.
Jesus dealt directly with the sins of the witnesses. He did not focus
on questions of legal procedure. He did not point out that they
should have gone immediately to a civil court. He did not ask them
rhetorically, “Who made me a judge over you?” He did not remind
them that the other guilty party was missing. It is clear that His
main concern was not with the procedural details of the incident; He
preferred instead to deal positively with the sinful condition of the
accused woman. She was the focus of His concern, not her accusers.
He acted to remove them from His presence, so that He might re-
store her to moral and judicial wholeness. This was His tactic in all
of His public confrontations with His accusers. He did this with
Israel in 70 a.p. He removed Israel from His presence, so that He
might restore the gentiles to moral and judicial wholeness. (When
He has accomplished this, He will then redeem Israel: Romans 11.)
