Maimonides’ Code: Is It Biblical? 1003

How many Jews, let alone Christians, have ever been informed of
the following information, presented by Jewish scholar and art his-
torian Joseph Gutmann?

‘The Magen David is a hexagram or six-pointed star. It appears as carly
as the Bronze Age and is at home in cultures and civilizations widely
removed in time and geographic area. Mesopotamia, India, Greece, and
Etruria are among the places where it has been found—but without any
discoverable meaning. Possibly it was an ornament or had magical con-
notations. Only occasionally before the 1890's is it found in a Jewish con-
text; the oldest Jewish example is from seventh-century 8.0.£. [B.c.} Sidon,
a seal belonging to one Joshua ben Asayahu, In the synagogue at Caper-
naum, Galilee, a synagogue which may date from the fourth century c.e.
[a.p.], the Magen David is found alongside the pentagram and the swastika,
but there is no reason to assume that the Magen David or the other signs on
the synagogue stone frieze served any but decorative purposes.

In the Middle Ages, the Magen David appears quite frequently in the
decorations of European and Islamic Hebrew manuscripts and even on
some synagogues, but appears to have no distinct Jewish symbolic connota-
Gon; it is also found on the seals of the Christian kings of Navarre, on
mediaeval church objects, and on cathedrals. As a matter of fact, what is to-
day called Magen David was generally known as the Seal of Solomon in the
Middle Ages, especially in Jewish, Christian and Islamic magical texts. In
the medieval Islamic world the hexagram was popular and was widely used.
Generally known, especially in Arab sources, as the Seal of Solomon, it
gradually became linked with a magic ring or seal believed to give King
Solomon control over demons. An early Jewish source in the Babylonian
Talmud (Gittin 68a-b) already mentions it.

The hexagram and pentagram, it should be pointed out, beth carried
the designation “Seal of Solomon” and were employed in both Christianity
and Islam as symbols with magical or amuletic power. On the parchment of
many medieval mecuzot (capsules placed on the doorposts of every Jewish
home) the hexagram and pentagram (Seal of Solomon) were written out
and also served as a talisman or had magical powers to ward off evil spirits.”°

 

 

The point is, few Jews or gentiles are aware of any of this. That
the flag of the state of Israel bears an ancient pagan symbol is not a
well-known fact either to those who respect it or who resent it. In
short, the vast majority of Christians and many Jews know very little
about the history of Judaism. Jews and Christians are aware that

20. Joseph Gutmann, The Jewish Sanctuary (Leiden: E. J. Brill, 1983), p. 21. This
study is Section XXIIE: Judaism, of the Iconography of Religions, produced by the
Institute of Religious Iconography of the State University Gronigen, Netherlands.
