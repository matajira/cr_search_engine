The Prohibition Against Usury 735

scheme. “According to my theory it [the stamping tax] should be
roughly equal to the excess of the money-rate of interest (apart from
the stamps) over the marginal efficiency of capital corresponding to a
rate of new investment compatible with full employment.’® But
Keynes aiso taught that the marginal efficiency of capital could fall
to zero “within a single generation. . . .” In fact, he said that it
would be “comparatively easy to make capital-goods so abundant
that the marginal efficiency of capital is zera. . . .°’ Thus, when the
marginal efficiency of capital falls to zero, then there will be no eco-
nomic reason for the rate of interest not to do the same. Just tax in-
terest and rents out of existence! In short, under his system of eco-
nomics, “the rentier would disappear. . . .”%

This is so clearly an example of crackpot economic utopianism
that his respectable academic disciples have spent two generations
either ignoring it or explaining it away as really meaning something
else. But he meant what he said. One reason why the General Theory
is so incoherent, in sharp contrast to his earlier economic writings, is
that it is an attempted defense of a program to produce the impossi-
ble: a world without scarcity, a world where capital is free for the
asking, a world without interest.

It is not surprising, therefore, to find that Keynes was also a pro-
moter of the basic monetary theory and policy of Social Credit.
Social Credit economics teaches that the government should create
fiat money to match the aggregate economic growth of the nation.
This, we are told, will keep effective demand high enough to promote
full employment. This is what Keynes taught, too; “There will be a
determinate amount of increase in the quantity of effective demand
which, after taking everything into account, will correspond to, and
be in equilibrium with, the increase in the quantity of money.”5?
Keynes was unquestionably a monetary crank.

I agree with Sir Eric Roll, at least on this one point: the growth
of such utopian ideas represented a reaction to the Great Depression
of the 1930’s, and it also represented a decline in the influence of ra-
tional economic reasoning. “In particular, the social and political
roots of the monetary doctrines of Major Douglas, of the mystical

55. Idem,
56. Ibid., p. 220.
57. Lbid., p. 221.
58. Idem.
59. Ibid., p. 299.
