4
A BIBLICAL THEOLOGY OF SLAVERY

The Spirit of the Lord is upon me, because he hath anointed me to
preach the gospel tu the poor; he hath sent me to heal the brokenhearied, to
preach deliverance to the captives, and recovering of sight to the blind, ta
set at liberty them that are bruised, ‘lo preach the ucceplable year of the
Lord (Luke 4-18-19).

We know! that the law is good if a man uses it properly. We also know
that law) is made not for good men but for lawbreakers and rebels, the un-
godly and sinful, the unholy and irreligious, for those who kill their fathers
or mothers, for murderers, for adulterers and perverts, for slave traders

[kidnappers] and liars and perjurers .. . (I Tim. 1:8-10, NIV).

Without a proper understanding of the theological foundation
and institutional functions of indentured servitude in the Old Testa-
ment, the reader will be baffled by several of the case laws of Ex-
odus. Modern man’s automatic negative reaction against the word
“slavery” makes it imperative that the serious Bible student under-
stand the biblical concept of servitude before he begins a study of Ex-
odus 21; otherwise, he will be tempted to conclude in advance that
these case laws do not apply today, that they were designed for use
by “primitive desert tribes” rather than designed for use by all soci-
eties everywhere.

The principle of interpretation that I adopt in my economic com~-
mentaries is this: whenever we can discover it, we must begin with
the theocentric principle that governs any particular biblical case
law, for it is this theocentric relationship which is reflected by the
person-to-person relationships established by any particular law.
The theocentric principle that governs the Book of Exodus is God’s
deliverance of His people in history. We read at the beginning of Exodus
6, “Then the Lorn said unto Moses, Now shalt thou see what I will
do to Pharaoh: for with a strong hand he shall let them go, and with

ii
