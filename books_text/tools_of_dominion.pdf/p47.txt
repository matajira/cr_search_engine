The Restoration of Biblical Casuisiry 39

physical. The invisible world of the inner religious life, the right-
eousness of the disposition, the sonship of God are in it made
supreme, the essence of the kingdom, the ultimate realities to which
everything else is subordinate. The inherently ethical character of the
kingdom finds subjective expression in the demand for repentance.”?’

The primary need today, as always, is the need for widespread
personal repentance before God. We therefore need a Holy Spirit-
initiated Christian revival to extend the kingdom of God across the
face of the earth. If we do not get this revival soon, my work and the
work of those who are involved in the Biblical Blueprints project will
remain curiosities, and then become antiquarian curiosities, until
the revival comes.

Blueprints and Responsibility

Without a bottom-up religious transformation of civilization, the
policies that we Christian Reconstructionists recommend will at best
have only a peripheral influence on society. The reader should
understand, however, that we expect the revival and this bottom-up
transformation, if not in our own lifetimes, then eventually. The
Bible’s blueprints for society will eventually be universally adopted
across the face of the earth as the waters cover the sea (Isa. 11:9).78
Christian Reconstructionists regard this as historically inevitable.
This confidence is what makes the theonomic postmillennial world-
view so hard-nosed and uncompromising. We annoy almost every
Christian who has doubts about the earthly triumph of God’s king-
dom, which means that we initially alienate just about everyone who
reads our materials, Our antinomian Christian critics call us arro-
gant. Bear in mind that the word “arrogant” usually means “a confi-
dent assertion of something I don’t approve of.”

Christians who doubt the future earthly triumph of God’s king-
dom tend to be less confident and less sure about the practical
reliability of the Bible’s blueprints. Sometimes they even deny that
the Bible offers such blueprints. If it does offer such blueprints, then
evangelical Christians have major responsibilities outside the sanc-
tuary and the family, This prospect of worldwide, culture-wide re-
sponsibility frightens millions of Christians. They have even
adopted eschatologies that assure them that God does not hold them

27. Vos, Kingdom and the Church, pp. 102-3.
28. J. A. De Jong, As the Waters Cover the Sea: Millennial Expectations in the Rise of
Anglo-American Missions, 1640-1810 (Kampen, Netherlands: J. H. Kok, 1970).
