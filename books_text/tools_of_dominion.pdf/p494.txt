486 TOOLS OF DOMINION

“Pit” in Rabbinical Literature

“Pit” is a classification used for centuries by the rabbis to assess
responsibility and damages. The Mishnah specified that any pit ten
handbreadths deep qualifies as deep enough to cause death, and
therefore is actionable in cases of death. If less than this depth, the
pit is actionable in case of injury to a beast, but not if the beast died.'
Writes Jewish legal scholar Shalom Albeck: “This is the name given
to another leading category of tort and covers cases where an
obstacle is created by a person’s negligence and left as a hazard by
means of which another is injured. The prime example is that of a
person who digs a pit, leaves it uncovered, and another person or an
animal falls into it. Other major cxamples would be leaving stones
or water unfenced and thus potentially hazardous. The common fac-
tor is the commission or omission of something which brings about a
dangerous situation and the foreseeability of damage resulting. A
person who fails to take adequate precautions to render harmless a
hazard under his control is considered negligent, since he is pre-
sumed able to foresee that damage may result, and he is therefore
liable for any such subsequent damage.”?

Samson Raphael Hirsch, the brilliant mid-nineteenth-century
Jewish Torah commentator, analyzed the economics of negligence
under the general heading of property, and property under the more
general classification of guardianship. “Man, in taking possession of
the unreasoning world, becomes guardian of unreasoning property
and is responsible for the forces inherent in it, just as he is responsi-
ble for the forces of his own body; for property is nothing but the ar-
tificially extended body, and body and property together are the
realm and sphere of action of the soul—z.e., of the human personal-
ity, which rules them and becomes effective through them and in
them. Thus is the person responsible for all the material things under

 

his dominion and in his use; and even without the verdict of a court
of Jaw, even if ne claim is put forward by another person, he must
pay compensation for any harm done to another’s property or body

1. Baba Kamma 5:5, The Mishnah, edited by Herbert Danby (New York: Oxford
University Press, [1933] 1987), p. 338.

2. “Avot Nezikin: (2) Pit,” The Principles of Jewish Law, edited by Menachem Eton
(Jerusalem: Keter, 19752), col. 326. This compilation of articles taken from the
Eneyclopedia, Judaica was published as Publication No. 6 of the Institute for Research
in Jewish Law of the Hebrew University of Jerusalem,
