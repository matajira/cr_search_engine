The Auction for Substitute Sanctions 47

removed.®

This incident raises some difficult exegetical questions. Was the
“eye for eye” principle literally applied in ancient Israel after the de-
feat of Canaan? Did Israel’s courts really poke out people’s teeth and
eyes? If not, why not? Or is it merely that there are no clear-cut bib-
lical records of such physical penalties being imposed by Israelite
judges on Israelite citizens?

The incident also raises some difficult historical questions. In the
Christian West, judges have consistently refused to impose “eye for
eye” physical penalties. In non-Christian societies, permanent physi-
cal vengeance is quite common, e.g., Islam’s Shari’ law. Why not in
the West? What is it about inflicting permanent physical mutilation —
in contrast to whippings or other relatively impermanent forms of
physical violence—that so repels Westerners?

The West's Future-Ortentation

The West’s impulse toward dominion in history is one possible
answer. The West has been future-oriented, as a direct result of its
Christian eschatological heritage: a faith in linear history, with a God-
created beginning, a God-sustaining providence, and a God-
governed final judgment.’ This vision of linear time made possible
the development of modern science.’ The future-orientation of the
West, especially from the seventeenth century onward, and espe-
cially in Protestant societies, led to faith in long-term progress, in-
cluding long-term economic growth.? Western people have under-
stood the importance to the community of full production from all
members. There is (ar was) the psychological and social phenome-
non called “the Protestant Ethic,” Begging, for example, has nat

6. Without a thumb, a person cannot grasp a tool or weapon. Without a big toc,
he cannot balance himself easily. See James B. Jordan, Julyes: God's War Against Hu-
manism (Tyler, Texas: Geneva Ministries, 1985), pp. 4-5.

7. Karl Léwith, Meaning in History (University of Chicago Press, 1949), ch. 11:
°The Biblical View of History.”

8. Stanley Jaki, The Road of Science and the Ways to God (University of Chicago
Press, 1978), chaps. 1, 2; Science and Creation: From eternal cyetes to an oscillating universe
(Edinburgh and London; Scottish Academic Press, [1974] 1980); “The History of
Science and the Idea of an Oscillating Universe,” in Wolfgang Yourgrau and Allen
D. Reck (eds.), Cosmology, History, and Theology (New York: Plenum Press, 1977).

9. Gary North, “Medieval Economics in Puritan New England, 1630-1660,” Jour-
nal of Christian Reconstruction, V (Winter 1978-79), pp. 157-60.

10. Max Weber, The Protestant Ethic and the Spirit of Capitalism (New York:
Scribner's, [1904-5] 1958). See alsa Gary North, “The ‘Protestant Ethic’ Hypothesis,”
Journal of Christian Reconstruction, VE (Summer 1976); Daniel T. Rodgers, ke Work
Ethic in Industrial America, 1850-1920 (University of Chicago Press, 1978).
