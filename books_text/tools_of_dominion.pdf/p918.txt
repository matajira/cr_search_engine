910 TOOLS OF DOMINION

man does not exist. Man is either a covenant-keeper or a covenant-
breaker, either obeying God in faith, or in revolt against God as a
would-be god.”*9 Everything man does is therefore religious. This
being the case, an appeal to religiousness as such cannot solve the
crucial question he is dealing with: Yo which institution or institutions
has God delegated the lawful sovereignty to collect His taxes and His tithes?
God was surely both King and Priest in Israel, but that is not the
issue here. The issue is: Did He delegate to a single institution the
lawful sovereignty to collect payments owed to Him in His capacity
as both King and Priest?

It is obvious that King Uzziah violated the temple by going into
it to burn incense. God struck him down with Old Testament leprosy
as a punishment (II Chron. 26:16-23). Rushdoony uses this example
to defend the institutional separation of church and state.” Speaking
of priest and king, he writes, “The two offices were not to have an
immanent union but only a transcendental one,” But to allow one
of these offices to collect payments owed by people to the other is un-
questionably declaring an immanent (earthly) union of the two
offices, as surely as Uzziah’s attempt to offer incense in the temple
was such a declaration.

The State is not to collect payments owed to the tabernacle for
atonement purposes. Similarly, the priesthood is not to collect taxes
owed to the civil government. The fact that the tabernacle, and later
the temple, was the civil center of Israel was manifested symbolically
by the fact that the Ark of the Covenant inside the holy of holies was
the center of all Israel, and that inside the Ark were the two tablets
(tables or copies)®? of Gad’s law. God's law was the center of life in
Israel, and God was present with His law in the holy of holies. This
has nothing to do with the institutional details of tax collecting or
tithe collecting; it has everything to de with the inescapable reli-
giousness of all life.

19. Rushdoony, “Implications for Psychology,” in Gary North (ed.), Foundations of
Christian Scholarship: Essays in the Van Til Perspective (Vallecito, California: Ross House
Books, 1976), p. 43.

20. R. J. Rushdoony, Foundations of Social Order: Studies in the Creeds and Councils of
the Early Church (Fairfax, Virginia: Thoburn Press, [1969] 1978), p. 70.

21, Idem.

22. Meredith G. Kline, The Structure of Biblical Authority (rev. ed.; Grand Rapids,
Michigan: Eerdmans, 1972), pp. 123-24.
