Proportional Restitution 507

fact, this interpretation is quite traditional among Jewish scholars.5

This interpretation seems to get support from the laws of at least
one nation contemporary with ancient Israel. The Hittites also im-
posed varying penalties according to which animal had been stolen.
Anyone who stole a bull and changed its brand, if discovered, had to
repay the owner with seven head of cattle: two three-year-olds, three
yearlings, and two weanlings.* A cow received a five-fold restitution
payment.’ The same penalty was imposed on thieves of stallions and
rams.* A plow-ox required a 10-fold restitution (previously 15).? The
same was true of a draft horse.'° Thus, it appears that trained work
animals were evaluated as being worth more to replace than the
others. Anyone who recovered a stolen horse, mule, or donkey was
to receive an additional animal: double restitution.!! The original
animal that had received training was returned; thus, the thief did
not have to pay multiple restitution.

It seems reasonable to conclude that the Bible’s higher payment
for a sheep or ox is based on the costs of retraining an equivalent ani-
mal. But what seems reasonable at first glace turns out to be mistaken.

Discounted Future Value and Capitalization

We need to consider carefully the argument that the higher resti-
tution penalty is related to the increased difficulty of training
domestic animals. No doubt it is true that the owner must go to con-
siderable effort to retrain a work animal. But is a sheep a work ani-
mal? Does it need training? Obviously not. This should warn us
against adopting such an argument regarding any restitution pay-
ment that is greater than two-fold.

It is quite true that the future value of any stolen asset must be
paid to the victim by the thief. What is not generally understood by
non-economists is that the present market price of an asset already includes
us expected future value. Modern price theory teaches that the present

5. See the citations by Nehama Leibowitz, Studies in Shemat, Part 2 (Jerusalem:
World Zionist Organization, 1976), p. 364.

6. “Hittite Laws,” paragraph 60. Ancien! Near Eastern Texls Relating to the Old Testa-
ment, edited by James B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton Uni-
versity Press, 1969), p. 192.

7. Tdem., paragraph 67.

8. Idem., paragraphs 61, 62.

9, Ldem., paragraph 63.

10. Fdem., paragraph 64.
11, Idem., paragraph 70.

 
