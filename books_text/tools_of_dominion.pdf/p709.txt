Oppression, Omniscience, and Judgment 701

ciently, and as profit-free as a hypothetical economy in which each
participant has equally good knowledge — perfect knowledge —as all
other participants. Knowledge, in a “decent” social order, should be
a zero-price resource, equally available to all, and equally acted
upon by all. Socialist arguments implicitly assume that it is only the
temporary existence of such factors as private property, personal
greed, and people’s willingness to exploit the poor, that has created a
world of scarcity, profits, and losses. Knowledge concerning the
future should be regarded as a free good, they implicitly assume.
Profits are therefore evil, not to mention unnecessary, in a sound
economy. This has been the underlying line of reasoning for cen-
turies of all those who equate economic profits with exploitation.

Men are not omniscient, This angers the socialists. They strike out
in wrath against the free market institutional order that encourages
men to seek out better information, day by day, so that they might
profit individually from its application in economic affairs. The so-
cialists prefer to create legislative barriers that interfere with the
operation of the market’s “auction for information.”

It should be clear why so little innovation takes place in socialist
economies. The development —or rather, the lack of development —
of commercial technology in the Soviet Union is a representative his-
torical example.# Innovation is not a service that people normally
offer free of charge to others. It involves creativity, capital, and the
willingness to take risks. In a socialist commonwealth, the entrepreneur
who is willing to bear uncertainty cannot legally receive payment for
the full economic value to society—as determined by market forces—
of his innovation, For entrepreneurs to receive full value for services
rendered, the socialist commonwealth would have to abandon the
collective ownership of the means of production-distribution.*®

Those who discover treasures in “collectively owned” fields,
meaning State-controlled and bureaucracy-administered fields, have

45. Antony Sutton’s three-volume study of Soviet technulogy, 1917-1965, indi-
cates that almost none of the Soviet Union’s industrial technology {as distinguished
from its military technology) originated in the U.8.8.R. Out of 75 different major
technologies surveyed, the percentage of Soviet technology was zero, 1917-30, 10
percent, 1930-45, and 1I percent, 1945-63, “It should be emphasized thal this is the
most favorable interpretation possible of the eupirical findings.” Sutton, Western
‘Tachnology and Soviet Economic Decelopment, 1945 to 1965 (Stanford, California: Hoover
Institution Press, 1973), p. 370.

46. Svetozar Pejovich, “Liberman’s Refurms and Property Rights in The Soviet
Union,” journal of Law and Economics, XU (April 1969).
