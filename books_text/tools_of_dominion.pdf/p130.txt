122 TOGLS OF DOMINION

ery in the Old Testament is likely to create many initial misunder-
standings. The only form of permanent slavery in the Old Testa-
ment was the enslavement of heathens: “Both thy bondmen, and thy
bondmaids, which thou shalt have, shall be of the heathen that are
round about you; of them shall yc buy bondmen and bondmaids.
Moreover of the children of the strangers that do sojourn among
you, of them shall ye buy, and of their families that are with you,
which they begat in your land: and they shall be your possession,
And ye shall take them as an inheritance for your children after you,
to inherit them for a possession; they shall be your bondmen for
ever: but over your brethren the children of Israel, ye shall not rule
over one another with rigour” (Lev. 25:44-46). I argue later in this
chapter that this slave law was abolished with the fulfillment of the
jubilee year by Jesus.

It was legal for the Israelites to make permanent slaves of de-
feated non-Canaanite enemies, but only of the women and children.
A distant non-Canaanite nation was always to be given an oppor-
tunity to surrender; if it did, its inhabitants were to become distant
tributaries, not household slaves. The Old Testament required the
total annihilation of all male opponents in a distant (non-Canaan)
war if the city had been offered terms of peace and had refused to
surrender (Deut. 20:10-13). Thus, a common means of gaining large
numbers of slaves—military conquest—was partially closed to
Isracl. It was lawful to take captive only the women and children
(Deut. 20:14-15). Captured women were likely to become wives or
concubines, with their offspring becoming truc family members in
the household, not a subclass of slaves. Furthermore, if a man mar-
ried one of them and then grew tired of her, she had to be released as
a free woman (Deut. 21:10-14). Israel’s policy regarding militarily de-
feated cities was a dual process of adoption (females and children) and
annihilation (adult males) through conquest. Men, as the heads of
households, either surrendered to the invading Israelites or else they
perished, but the system was not primarily one geared to permanent
enslavement.*6

 

26. The fulfillment of the jubilee year by Christ would seem to have annulled
these wartime slave provisions: no more permanent heathen slaves. Tt may be that
this fulfillment is also the basis for anmulling the requirement of the extermination of
foreign male enemies after a military victory, More likely, however, extermination is
annulled by the requirement to honor the universal extension of the gospel. Instead
of killing all the men, a victorious Christian nation would root out the defeated na-
tion’s public pagan religious practices, and then foster cooperation between churches
