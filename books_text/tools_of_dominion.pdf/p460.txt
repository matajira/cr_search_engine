452 TOOLS OF DOMINION

The freedom-seeking bondservant might think to himself: “If I
attack the man in private, and he mutilates me, I can go free. I will
do it. I want my freedom more than I want my tooth. On the other
hand, he might punch out my eye. There are risks here. I can go free
in a few years anyway. This is not a permanent position of servitude.
Is it worth the possible loss of my eye to gain my freedom a few years
early? I may not be able to hurt him very much, and he will not hesi-
tate to beat me to a pulp. Is an attack worth the risk?” The bond-
servant counts the cost. In a Christian society governed by biblical
civil law, in which servitude is not permanent, but can extend at
most for seven years, will he risk forfeiting his eye for the rest of his
life? He must pay a high price for rebellion-based freedom. The
court may decide against him anyway and convict him of assault on
the owner. Attacking the bondservant-owner in secret is a very risky
act. The bondservant is restrained by the threat of physical punish-
ment by the owner, and the court may not impose any penalty on the
owner. The master is restrained, at most, by the threat of losing the
bondservant. The master has the edge in this case.

The Foreign Slave

The foreign slave, like the committed criminal sold into perma-
nent bondage, was in a different situation. He was not guaranteed
release after a fixed period of time (Lev. 25:44-46). He therefore
might have been willing to attack the slave-owner in secret, not fear-
ing physical retribution, for the reward would be freedom. Provok-
ing a Hebrew master to excessive punishment might have been to
the advantage of a foreign slave. The price of freedom was mutila-
tion—a price that some slaves might have been willing to pay.

This would have been an incentive for masters to avoid being
alone with foreign slaves. In the absence of witnesses, the slave could
do two evil things. First, he might attack the owner in order to cause
the owner to mutilate him in self-defense. Then he could claim to be
the victim. Second, he might self-mutilate himself and then claim
that the owner had struck him. In the absence of witnesses, the court
might decide in his favor, especially if the slave-owner had a reputa-
tion for violence. These possibilities increase the risks to an owner of
being alone with a foreign slave.

By separating foreign slaves from Hebrew masters, the law also
tended to separate the religious rites of foreign slaves from their mas-
ters. In the Old Testament commonwealth, there would have been
