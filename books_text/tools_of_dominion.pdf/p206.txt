198 TOOLS OF DOMINION

cycle.?#? Fiat money allows.the government to lower the economic
burden of its debts through monetary debasement. Economist Franz
Pick always defined a government bond as a certificate of guaranteed
confiscation.

To avoid repaying these debts, governments continue to roll
them over by selling new debt obligations to pay off old ones. This is
the philosophy of perpetual government debt for perpetual national
(and now international} prosperity. This view of government debt
has become basic to Western economic thought and practice ever
since the creation of the privately owned Bank of England in 1694.
This economic outlook has now spread to private debt markets, debt
which is also collateralized by promises to pay rather than by actual
physical assets, The world economy is now threatened by a com-
bination of rising debt, lengthening debt maturities, fiat money,
fractional reserve banking, uncollateralized private debt, and tax-
payer collateralized government debt. The entire world is now
caught in a demonic race against the inevitable: the extension of
more debt, the threat of mass inflation, and the inevitable world de-
pression that will hit when the debtors at last go into default.

Interestingly, it was only after the abolition of chattel slavery that
the rapid spread of long-term indebtedness became a way of life in
the West, first for the newly developed institutions known as limited
liability corporations in the final third of the nineteenth century, and
then for the modern State.?#! Only when creditors could no longer
get the civil government to impose either a period of servitude (the
biblical approach) or a jail sentence (the humanist solution prior to
the late 1860’s)2#2 on those who defaulted did the modern institutions
of debt capitalism and debt socialism become triumphant. Today
this very institutional arrangement threatens to topple the world
economy in a wave of defaults, either outright (unlikely) or infla~
tionary (highly likely), taking with it the bloated remains of the cul-
ture of secular humanism,

Slavery is an inescapable concept theologically, as well as an inescapable
concept economically, for as long as sin is in the world. The question is:
Who will be enslaved, creditor or debtor? In the Old Testament, it

240, Mises, Human Action, ch. 20.

241. On the earlier period, sec Carleton Hunt, The Develupment of the Business Cor-
poration in England, 1800-1867 (Cambridge, Massachusetts: Harvard University
Press, 1936).

242. “Debt,” in The Encyclopaedia Britannita (Ulth ed.; New York: Encyclopaedia
Britannica, Inc., 1910), Vol. WH, p. 906.
