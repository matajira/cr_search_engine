538 TOOLS OF DOMINION

go before the civil magistrates. If the neighbor is found guilty, he
pays double restitution. “For all.manner of trespass, whether it be for
ox, for ass, for sheep, for raiment, or for any manner of lost thing,
which another challengeth to be his, the cause of both parties shall
come before the judges; and whom the judges shall condemn, he
shall pay double unto his neighbour.”

Why should the neighbor be required to pay only double restitu-
tion for a sheep or ox in this case? What about five-fold and four-fold
restitution? My answer: because the neighbor cannot conceal the
crime the way that the outsider can when he slaughters or sells the
animal, In short, if is easier for the victimized owner ta prove his legal case
against a neighbor than it is for him to prove his case against an unknown thief
who disposes of the evidence. Thus, the penalty imposed on the neighbor
is double restitution, which is the standard requirement for the theft of
all other goods except slaughtered or sold oxen and sheep. Since the
owner faces reduced difficulties in recovering his property, and the
thief therefore faces increased risk, the penalty payment is reduced.

Conclusion.

What will be the marks of civil justice during an era of biblical.
justice? Victims will see the restoration of their stolen assets, while
criminals will see their ill-gotten capital melt away because of the
financial burden of making restitution payments. The dual sanctions
of curse and blessing — part four of the biblical covenant model®— are
invoked and imposed wherever the principle of restitution is honored in
the courts, both civil and ecclesiastical. Restitution brings both judgment
and restoration, which affect individual lives and social institutions.

There are limits to biblical restitution. First, the full value of
whatever was stolen is returned by the thief to the original owner.
Second, the thief makes an additional penalty payment equal to the
value of the item stolen. To encourage criminals to admit their guilt
and seek restoration before their crimes are discovered, the Bible im-
poses a reduced penalty of 20 percent on those who admit their guilt
voluntarily (Lev. 6:2-5).

There are two exceptions to double restitution. The law singles
out oxen and sheep as deserving special protection in the form of
five-fold and four-fold restitution in cases where the stolen animals
are killed or sold. Because oxen and sheep are symbolic of mankind,
the law thereby points to the need of protecting men from oppression

60. Sutton, That You May Prosper, ch. 4.
