1234 TOOLS OF DOMINION

masters, 451
Rushdoony on, 148
service &, 854-55
slave master, 442
tenth generation, 151
see also Hierarchy

Automatons, 1124, 1129, 1139

Automobile, 576-78, 588,
806-8

Autonomous man, 1125

Autonomy
anarchy &, 814
antinomianism &, 9
Bible exposition &, 958
brawl, 339, 345
choosing masters, 78
Eve, 77-78
free will, 68-69
sanctions, 414
slavery &, 116
tyranny &, 429

Awe, 923

Axe, 472

Aztecs, 185

Baal, 78
Bacon, Francis, 789
Bahnsen, Greg L.
case laws, 97
divorce, 290
eschatology & theonomy, 985
mortgages, 718
replies to, 12, 13n, 985
Baikal (Lake), 1163
Baird, Charles, 1085, 1142
Balaam, 462n
Banfield, Edward, 331
“Bangladesh with missiles”
(USSR), 1166
Banker, 749-51
Bankers, 742-43
Banking, 739-40
Bank run, 739, 741
Bankruptcy, 12, 221-22, 473,
737, 743
Banks, 709, 740-43, 744-46

Baptism
deacons, 460n
infant, 2670
mark of ownership, 782
transfer of authority, 270
voting, 844
Barabbas, 289, 386
Baran, Paul, 989
Barbados, 177, 178, 231
Bargaining, 688, 694, 702, 1110
Barker, David K., 1450
Bar Kochba, 251n, 840, 1055-56
Barnett, Randy, 526n
Barnhouse, Donald, 16
Barr, James, 1065
Barth, Karl, 2, 85, 108, 1074,
1077-78
Bastard, 148, 259, 851
Bathsheba, 522, 650n
Batten, C, R., 547n
Baxter, Richard, 23-24, 27-28,
174, 192, 192n, 203
Beads, 1125
Bean, R. N., 173
Beast, 79
Beasts, 961 (see also animals)
Beauty, 898-99, 925, 1088
Becker, Gary
crime, 1140-49
crime’s benefits, 1145
euthanasia, 1147
murder?, 1147, 1149
restitution, 1143
Becket, Thomas, 789
Beckles, Hillary, 178n
Beckman, Petr, 479n, 573
Bentham, Jeremy, 1147
Beringer, Richard, 175n
Berman, Harold, 35, 1060, 1062, 1211
Bestiality, 100, 1015, 1016
Best, Robert, 559n
Betrothal, 262-63, 263, 267, 306,
644-45
Bezalcel, 920
Bibb, Henry, 443
Bible
American Revolution &, 1199
