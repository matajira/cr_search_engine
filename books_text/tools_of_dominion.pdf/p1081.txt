The Hoax of Higher Criticism 1073

viving Early Writings of Two Religious Groups, Judaism und Christianity,
Regarding the Acts of a Gad Who Does Not Really Interact With History,
Had they done so, of course, their academic charade would have
been obvious from the beginning.

Historical Resurrection and Final Judgment

It is not only the creation of man and his subsequent fall from
grace that must be discreetly covered up by the blanket of hypotheti-
cally objective history; it is also the resurrection of Christ. Both sin
and redemption must be discussed apart from biblical revelation, for
if the Bible’s account of sin and redemption is taken seriously, then
the issue of God's final judgment once again becomes a fundamental
problem. This is the problem that autonomous man wishes most of
all to avoid. So, the resurrection is relegated to the mythic past, and
once again the authors focus on what a small group of people have
thought about this non-historical event.

Finally, what shall we say about the resurrection of Christ, as under-
stood in the New Testament? This cannot be an objective fact of history in
the same sense as was the crucifixion of Christ. The latter was a fact avail-
able to all men as a real happening, and pagan writers like Tacitus and
Josephus can speak of it. But in the New Testament itself the Easter faith-
event of the resurrection is perceived only by the people of the faith. Christ
as risen was not seen by everyone, but only by the few. Easter was thus a
reality for those in the inner circle of the disciples and apostles. That is not
an arena where a historian can operate. Facts available to all men are the
only data with which he can work, thc facts available to the consciousness of
a few are not objective history in the historian’s sense.”

They distinguish the “real happening” of the crucifixion from the
“faith-event” of the resurrection, which was an event of a very differ-
ent character. Only “facts available to all mcn”— meaning facts that
are implicitly possible for all men to have seen—are “real happen-
ings.” This means that the resurrection was somehow not a fact that
in principle all men might have seen and verified, in the same way
that they could have scen and verified the crucifixion. In other
words, the resurrection was not a “real happening,” although the cal-
culating deceivers who wrote The Book of the Acts of God are too wise to
say this blatantly, for fear of tipping their hand. They argue that the

 

28. Wright and Fuller, Acts of Ged, p. 25.
