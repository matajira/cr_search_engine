The Epistemological Problem of Social Cast 1125

valid with regard to all transactions without exception.”!” For the
modern economist, all human action tends toward a final state in
which human beings become omniscient and therefore take on one
of the attributes of God. The problem is, their view of God is that
He could not possibly act if He existed. He would be a “rule-following
automaton,”! because “A perfect being would not act.”"!°

 

Timeless Metaphysical Models

Mises relies on this limiting concept of a hypothetical economy
filled with soulless people in order to explain the operations of real
world market forces. “This final state of rest is an imaginary con-
struction, not a description of reality. For the final state of rest will
never be attained. New disturbing factors will emerge before it will
be realized. What makes it necessary to take recourse to this im-
aginary construction is the fact that the market at cvery instant is
moving toward a final state of rest.”!! He calls this movement toward
(or “tendency toward”) a final state of rest a fact, But this “fact” is pre-
cisely what must be demonstrated. It is the same old pre-Socratic
contradiction between Parmenides’ changeless logic and Heraclitus’
ceaseless flow. These two worlds cannot be shown to be connected;
they are, however, correlative in the thinking of humanistic scholars.

To explain this intellectual dilemma, Van Til uses the delightful
analogy of someone who is trying to put together a string of beads,
but the string is infinitely long, and the beads have no holes. The im-
aginary world of timeless logic (Van Til’s “string”) which cannot pos-
sibly cxist serves as the limiting concept (to use Kant’s terminology for
the “noumenal”),)” or limiting notion (to use Mises’ term)" for our
understanding of the world which does exist — the world of ceaseless
flux (Van Til’s “beads”). This world of timeless logic is, in short, a
logical backdrop which cannot ever exist in the real world—and

107. Zbid., p. 245.

108. Mises writes: “No matter whether this thirsting after ormniscience can ever
be fully gratified or not, man will not cease to strive after it passionately.” Mises,
Ultimate Foundation, p. 120.

109. Buchanan, Cost and Chote, p. 96.

110, Mises, Epistemological Problems of Economics (Princeton, New Jersey: Van
Nostrand, 1960), p. 24. Cf. Mises, Llimate Foundations, p. 3.

Ati. Idem.

112. Immanuel Kant, Critique of Pure Reason, translated by Norman Kemp Smith
(New York; St, Martin’s, [1929] 1965), B3i1, p. 272.

113. Human Action, p. 249.
