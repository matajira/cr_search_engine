1186 TOOLS OF DOMINION

the physical or the contractual item.!! The “front end” of this stream
of future services is close at hand; how long it will continue to flow is
guesswork. The initial flow of services may in fact be somewhat
removed, as indicated by the warning in the fine print on the side of
the box, “some assembly required.” The beginning of that expected
flow of services may be a day away or a week away or a year away.
The point is, there is just barely a “now” in any economic decision. There
are only present expectations of varying degrees of the future. So,
contrary to Mr. Mooney, who insists that there are no future goods
in the present, I insist that from a rational decision-maker’s point of
view, there are mostly future goods in the present—and this “mostly”
is very, very close to only.

Infinite Interest Rates

If everyone were to conclude that the expected future stream of
services provided by physical goods is irrelevant for present eco-
nomic calculation, as Mr. Mooney insists that it is, then free market
interest rates would approach infinity, for no one would voluntarily give
up present goods for the sake of receiving economically “irrelevant”
future goods. Also, the price of durable capital goods and durable
consumer goods would fall almost to zero, for no one would value
them for the sake of their expected future productivity, meaning any
expected value three seconds away. Or two seconds away. Or a split
second away. In short, we would say goodbye to civilization. This is
the “economics of love.” It is also the economics of existentialism: the
philosophy of the autonomous moment.

Decapitalization

I single out Mr. Mooney’s analysis because he is the only person
I have ever seen who so forthrightly confronts the issue of time-
preference in his denial of the moral legitimacy of interest. He offers

1. Mr. Mooney tries (a argue exclusively from the physical. But I as a lender
may not want to own the physical object, such as a farm. I may prefer to own a
promise to pay (mortgage) made by the owner of the farm, with the farm serving as
legal collateral should he default on his promise. If he defaults, I will probably try to
get someone else to buy the farm and make me another promise. Yes, the contract
may be based on the productivity of the farm, as administered by someone, but the
focus of my concern may be the promise, not the physical asset itself, Perhaps the
person decides to get out of farming and use the property as a resort, or as a con-
sumer good. I care only about the promised payment, so long as his decision regard-
ing the use of the land does not reduce its collateralized market value.
