1192 TOOLS OF DOMINION

estants worked together politically to overturn the existing
Trinitarian judicial foundations of colonial citizenship. The Fed-
eralists (nationalists) persuaded the electorate in 1788 to ratify the
proposed Constitution which substituted a new concept of judicial office,
one based on the presupposition of a universal humanity. The key provision
of the U.S. Constitution that made this transformation possible,
long ignored by the history textbooks, is Article VI, Section 3:

The Senators and Representatives before mentioned, and the Members
of the several State Legislatures, and all the executive and judicial Officers,
both of the United States and of the several States, shall be bound by Oath
or Affirmation, to support this constitution; but no religious Test shall ever
be required as a Qualification to any Office or public Trust under the
United States.

‘This was the section of the Constitution that cstablished the required
judicial oath for the new government. It was a new oath, and therefore
it was a new covenant. But this would not be an act of covenant renewal
with the God of the Bible, under whom the state constitutions had
been legally constituted. It instead was an act of covenantal apostasy.

Citizenship Under the State Constitutions

Prior to this, the states had generally required Christian profes-
sions of faith of voters. The colonies’ state constitutions were explicitly
religious. This was especially true of the New England constitutions.
The old Puritan rigor was still noticeable. Vermont's 1777 constitution
begins with the natural rights of man (Section I), goes to a defensc of
private property (Section LL), and then sets forth the right of religious
conscience, “regulated by the word of GOD. . . .” There is religious
freedom for anyone to worship any way he chooses, just so long as he
is a Protestant: “. . . nor can any man who professes the protestant
religion, be justly deprived or abridged of any civil right, as a citi-
zen, on account of his religious sentiment. . . .” The public author-
ities have no authorization to interfere with people’s rights of con-
science; “nevertheless, every sect or denomination of people ought to
observe the Sabbath, or the Lord’s day, and keep up, and support,
some sort of religious worship, which to them shall seem most agree-
able to the revealed will of GOD.”* (Not reproduced in the A.B.A.

8. Richard L. Perry and John C. Cooper, The Sources uf Our Liberties (American
Bar Association, 1952), p. 365.
