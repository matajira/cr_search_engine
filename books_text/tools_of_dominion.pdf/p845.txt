Feasis and Citizenship 837

quired feasts, the man had placed himsclf in the camp of the uncir-
cumcised strangers. He would have been kept from attending future
ritual feasts. He would have been barred from attendance at local
worship conducted by the priesis. If he was an Israclite with an in-
heritance in the land, he would also have forfeited this inheritance,
for he had renounced his family’s ownership rights in Isracl when he
renounced God’s ownership rights over Him and His family. Only if
his sons or distant heirs later denied their father’s rebellion and affrined
the family covenant under God when they became adults could they
reclaim the forfeited inheritance. However, this re-covenanting pro-
cedure did give them the ability to reclaim what had been legally
removed. This was God’s promise to the future dispossessed sons of
Israel whenever they were removed from the land:

And ye shall perish among thc heathen, and the land of your enemics
shall eat you up, And they that are left of you shall pine away in their iniquity
in your enemies’ lands; and also in the iniquities of their fathers shall they
pine away with them. If they shall confess their iniquity, and the iniquity of
their fathers, with their trespass which they trespassed against me, and that
also they have walked contrary unto me; And that I also have walked con-
trary unto them, and have brought them into the land of their encmies; if
pt of the

 

then their uncircumcised hearts be humbled, and they then a

 

punishment of their iniquity: Then will T remember my covenant with
Jacob, and also my covenant with Isaac, and also my covenant with Abra-
ham will I remember; and I will remember the land (Lev. 26:38-42).

Ifa man had no inheritance in the land, he had no legal access to
judicial office. This was another aspect of God’s threat of imposing
the physical sanction of removing them from their geographical
sanctuary in the land. They would become slaves and strangers in a
foreign land. Only through extraordinary faithfulness did certain
Israelites become leaders in foreign lands, as Joseph had become in
Egypt, as Daniel later became in Babylon and Medo-Persia, and as
Esther became in Medo-Persia, Israelites would suffer by becoming
subordinates to foreign gods whose spokesmen did not respect the
principle of equality before the law. They would not again serve as

 

Judges in the land, declaring God’s civil law, unless they repented.

To be an uncircumcised stranger in Israel was to be someone
outside the congregation. Circumcision was a judicial act. It was a
physical mark of covenantal subordination, not a magical mark of
initiation. A man could make his circumcision null and void by re-
