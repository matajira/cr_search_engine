The Prohibition Against Usury 745

destroyed by just this kind of insurance contract, Medieval business-
men agreed to finance various maritime enterprises, but only if the
shipper guaranteed repayment. Instead of taking a percentage of the
profits from a particular ship’s voyage, the less risk-oriented in-
vestors agreed to a fixed percentage (interest rate), leaving more
profits (or more losses) to the adventurer.

Then third parties entered into the transaction, probably begin-
ning in the fourteenth century. They agreed to act as insurers for
ship owners who did not want to offer such a guarantee to investors,
or who could not because they owned only one ship, and if its voyage
failed, there was no way to repay the loans, This third-party loan
was called the contractus érinus, and it eventually sank the usury prohi-
bition to the bottom of the historical sea.” For what was the
“insurer” offering, if not a guaranteed, fixed-interest return on loans?
Tt may have been called shipping insurance, but it was identical to
the medieval definition of usury. Yet it took over a century for even
one scholastic commentator to spot the problem, and no one paid
any attention to him.”!

When the insurance feature of non-shipping business contracts
was first introduced, it was initially rejected by the theologians. In
partnerships, where there was shared risk of failure, interest pay-
ments had always been acceptable, but not in contracts where there
was a guaranteed rate of return, irrespective of the outcome of the
particular business or business venture. But step by step, the resis-
tance of the church to interest payments in business loans was weak-
ened. By Luther’s day, the old prohibitions were almost gone.
Incredibly, by the late fifteenth century, the Roman Church had ac-
tually approved charitable loans (called “contracts”) that paid 5 per-
cent to 6 percent per annum, the montes pietatis,”? The church by the
late medieval period had reversed the original meaning of the bibli-
cal prohibition, which forbids interest from charitable loans, but
which places no restraints on interest from business loans. The
church prohibited interest from business loans and itself collected in-
terest from charitable loans.

The prohibition against interest could not be sustained, The
future is always discounted. So when we read in the Bible about
loans without interest, we are talking about charitable loans, not

70. Noonan, The Scholastic Analysis of Usury, pp. 202-3.
71. Ibid., p. 203. His name was John Consobrinus.
72. Ibid,, p. 295.
