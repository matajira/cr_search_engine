670 TOOLS OF DOMINION

representative feature — we find that there is only one: their lack of cov-
enanial representation, It is appropriate that this should be the focus of
the law in the Book of Exodus, the premier book in the Bible and in
the Pentateuch on hierarchical representation. The widow has no
husband; the orphan has no parents; the resident alien has no tribe
and no legal status in the assembly. The first two have no family
head above them; the third has no ecclesiastical or judicial place in
the hierarchy. No earthly agent speaks for the resident alien in the as-
sembly. No one listens to the widow and orphan. No one has a major
cultural incentive to protect them.

Nevertheless, they are not covenantally defenseless. Their lack of
a covenantal intermediary between them and God does not leave
them without judicial recourse, Prayer can bring them before the
judgment seat of the King. Their prayers indicate that they honor
God in their hearts by subordinating themselves to Him. Prayer tes-
tifies to a person’s faith in the hierarchical nature of the universe.
God will therefore listen to them. “If thou afflict them in anywisc
[any way], and they cry at all unto me, I will surely hear their cry.”
God will protect them. They have honored His sovereignty and His
hierarchy through their prayers. In contrast, their oppressors have
ignored His revealed law. He will therefore uphold His law. He will
intervene, acting on their behalf: “And my wrath shall wax hot, and I
will kill you with the sword; and your wives shall be widows, and
your children fatherless.” He will bring judgment in history, on the
basis of fax dalionis: an eye for an eye, a dead husband for the victim's
dead husband, dead parents for the victim’s dead parents, The in-
visible God of the Bible will intervenc in history as their representa-
tive agent. He becomes their kinsman-redeemer, and in doing so,
He becomes the wicked oppressor’s blood avenger. He cuts off the
oppressor’s inheritance. In short, God defends all five parts of His
covenant.

God's Negative Sanctions in History

God says that Biblical law is to be honored by individuals and
their courts above all considerations of race, family, or other per-
sonal relationships. Judges are required to uphold its terms without
respect to persons. “Ye shall not respect persons in judgment, but ye
shall hear the small as well as the great; ye shall not be afraid of the
face of man; for the judgment is God’s; and the cause that is too hard
for you, bring it unto me, and I will hear it” (Deut. 1:17), If for any
