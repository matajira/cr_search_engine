Common Grace, Eschatology, and Biblical Law 977

daughters committed sin (19:30-38). But it had been Lot’s presence
among them that had held off destruction (19:21-22),

The same was true of Noah, Until the ark was completed, the
world was safe from the great Hood. The people seemed to be pros-
pering. Methuselah lived a long life, but after him, the lifespan of
mankind steadily declined. Aaron died at age 123 (Num. 33:39).
Moses died at age 120 (Deut. 31:2). But this longevity was not nor-
mal, even in their day. In a psalm of Moses, he said that “The days
of our years are threescore years and ten; and if by reason of strength
they be fourscore years, yet is their strength labour and sorrow; for it
is soon cut off, and we fly away” (Ps. 90:10). The common curse of
God could be seen even in the blessing of extra years, but long life,
which is a blessing (Ex. 20:12), was being removed by God from
mankind in general.

The Book of Isaiah tells us of a future restoration of long life.
This blessing shall be given to all men, saints and sinners, It is there-
fore a sign of extended common grace. It is a gift to mankind in gen-
eral, Isaiah 65:20 tells us: “There shall be no more thence an infant
of days, nor an old man that hath not filled his days: for the child
shall dic an hundred years old; but the sinner being an hundred
years old shall be accursed.” The gift of long life shall come, though
the common curse of long life shall extend to the sinner, whose long
life is simply extra time for him to fill up his days of iniquity. Never-
theless, the infants will not die, which is a fulfillment of God’s prom-
ise to Israel, namely, the absence of miscarriages (Ex. 23:26). If
there is any passage in Scripture that absolutely refutes the amillen-
nial position, it is this one. This is not a prophecy of the New
Heavens and New Earth in their post-judgment form, but it is a
prophecy of the pre-judgment manifestation of the preliminary
stages of the New Heavens and New Earth—an earnest (down pay-
ment) of our expectations. There are stil] sinners in the world, and
they receive long life. But to them it is an ultimate curse, meaning a
special curse. It is a special curse because this exceptionally long life is
a common blessing—the reduction of the common curse. Again, we
need the concept of common grace to give significance to both
special grace and common curse. Common grace (reduced common
curse) brings special curses to the rebels.

There will be peace on earth extended to men of good will (Luke
2:14), But this means that there will also be peace on earth extended
to evil men. Peace is given to the just as a reward for their covenantal
