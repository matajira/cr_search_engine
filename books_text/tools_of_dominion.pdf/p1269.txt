Kolko, Gabriel, 696n, 788n
Kondratieff, N., 145n-146n
Koo, Anthony, 570

Krentz, Edgar, 1064-65, 1070
Kroner, Richard, 1130
Krupp, 201

Ku Klux Klan, 353-54
Kuhn, Thomas, 9n, 22, 85n
Kulaks, 769

Laban, 137, 246, 254, 256, 262, 650
Labor, 363, 375-67
Lachmann, Ludwig, 130-31
Laissez faire, 334-35
Lake Baikal, 1163
Lake of fire, 145, 166, 170, 171,
204, 535
Lalonde, Peter, 40
Land
airport, 582
care of, 548
conservation, 593
discounts, 552-53, 559
pricing, 508
rest, 823
Land grants, 146-47
Land hunger, 141
Langhein, John, 169n
Language, 67, 85
Larabee, Leonard W., 147n
Lashing, 298-99, 316
Latifundia, 144, 158
Latrine, 665, 864
Lave, Lester, 583n
Law
abstract, 48
administrative, 1060
advice?, 914, 915
arbitrary, 772
authority, 16
autonomy, 9
biblical, 1134
binds and looses, 883
blueprint, 80
boundaries, 72
capitalism &, 770
ceremonial, 59

Index

civil & criminal, 528

civilization &, 90-91

common grace &, 978

conflict &, 764-65

consistent, 709-10

continuity, 9-10

Darwinian, 763-64

deterrence, 527-34

economics &, 1101, 1144-45

efficiency, 1158

effects, 960

enforcement, 529

equality before, 118-19, 329-30,
337, 437

evangelism &, 63, 673-79

ex post facto, 281

external, 978

external obedience, 972-73

fear of, 46-48

final judgment &, 1078

formal, 683

general rules, 396-98

grace &, 15, 975-82

hatred of, 16, 46, 293, 383

heart, 63, 89, 898, 971, 972

ignorance of, 287

impotent?, 84

innocent party, 265

jot and tittle, 47

knowledge of, 961-63

lived, 106

love, 958

majesty of, 429

means of curse, 959

means of grace, 959

mirror, 1

moral, 915

nations, 90

natural, 14n, 18-21, 35

negative, 820

negative injunctions, 713, 836

neutrality, 16

one-many, 99

oral, 33 (see also Oral law)

package deal, 4t1

1261
