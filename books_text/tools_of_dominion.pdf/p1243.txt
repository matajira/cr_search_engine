criticism of, 183-84
embarrassment, 201
mediation, 883
“package deal,” 50
Tom Paine vs., 48

Biblical law (sce entire book)

Bicameralism, 471

Bid, 613, 801, 803, 805

Birth of @ Nation, 353n

Birthright, 846

Blabbermouth, 798

“Black legend” (Spain), 185

Black market, 693

Blacks
Hamites, 230-31
imports, 17th century, 176
political tools, 224
see also abolitionism, slavery

Black Sea, 1161

Blackstone, William, 2-3

Blessing, 147-48, 976, 978 (see
also Curse, Sanctions)

Blindness, 698

Bloch, Marc, 346n

Block, Sidney, 169n

Block, Walter, 10in

Blood, 343, 876f, 877

Blood avenger, 668

Blood money, 903-12

Blueprints, 7, 14, 19, 39, 42, 80

Boaz, 150, 249

Boecker, Hans, 44, 105n, 388-89,
394, 896, 1079

Bohm-Bawerk, Eugen, 723, 180

Bond, 193, 197, 287, 814

Bondservant
commodity, 362
freedom, 448-50
gored, 480-82
mutilation, 439-40
property, 438
rebellion, 440
retaliation, 450-52
see also Slave, Slaves

Bondservice
charity loan, 714-16
defined, 120-23

Index 1235

economic restitution, #6
inescapable, 92
reasons for, 358
resident alien, 131-34
responsibility &, 120
sin &, 124
voluntary, 135-38
see also Slavery
Book of the covenant, 95-96
Book reviews, 23
Boot camp, 378
Boredom, 349
Borrowing, 638-40, 640
Bottom-up, 54-55
Boulding, Kenneth, 135n, 1123n
Boundaries, 72, 467n, 541, 676-77,
801
Bourgeoisie, 900
Bowers, Fredson, 1069
Bowman, Robert, 9-10, 14
Boxing, 344
Bracker, H.-D., 1079
Brand, 835
Branding, 231, 781-82
Brawl, 350, 357
Brazil, 181, 190
Breach of promise, 645n
Breadwinner, 855
Breen, T. H., 180n
Bribery
adultery &, 791
civil government, 800-1
judgment &, 785-810
non-monetary, 852
oppression, 669
reciprocity, 809
retroactive, 792
reverse, 795-96
witchcraft, 791
Brichto, H. C., 522n
Bride (God's people), 210, 227
Bridegroom
bride price, 252
brother, 261
church's authority, 270
covenantal agent, 660-61
covenant lawsuit, 660-63
