Pollution, Ownership, and Responsibility 577

environment also suffer from the pollution. For example, a man
starts an automobile engine. He becomes a polluter of the air (ex-
haust emissions, noise), His car’s contribution to the overall level of
exhaust emissions pollution is infinitesimal— probably unmeasura-
ble from five feet away, unless the car is old and smoking. Yet three
million cars in a valley like California’s Los Angeles and Orange
counties create pollution that is all too measurable. If total air pollu-
tion in a particular region is to be reduced, then all the permanent
polluters in that region—c.g., pcople whose automobiles are licensed
locally, but not visitors from outside the region — must be restrained
by law.

Economically speaking, the emissions-control device on a car is
no different from the exhaust muffler, although the latter is more
readily understood. Both devices raise the price of the car, reduce its
engine’s efficiency, and increase gasoline consumption. Both protect
innocent bystanders: less noise, less bad air. Both protect the owner
of the car: less noisc, less bad air. The protection of the innocent by-
stander is the focus of the law, however. If the owner were the only
person affected, the law would not be legitimate. He should be
allowed to do what he wants with his own cardrums and lungs.

It must be understood that neither of these emissions-control
devices will be paid for entirely by the automobile manufacturers,
for manufacturers are not the only ones involved in the pollution
process. Pollution-control or noise-control devices are, economically
speaking, a kind of sales tax that is paid by consumers, despite the
fact that the “collection” of the sales tax is made by the auto compan-
ies when they sell the cars. Drivers are the local polluters; auto man-
ufacturers are their accomplices. Drivers usually prefer to convert
private costs (lower performance, the cost of the device) into social
costs (noise and air pollution), especially if they believe that other
drivers are allowed to do the same thing. Car buyers are therefore
required by law to pay for the control devices when they purchase
the cars. But in most cases, pollution control devices are not re-
quired for older model cars; the laws only apply to current produc-
tion models and future models.

The automobile companies also losc, as the new car drivers’ “ac-
complices,” for they cannot automatically “pass along” the added
costs of production to buyers. Some buyers may decide to keep driv-
ing older, “hotter” performance cars, especially if new car prices rise.
This raises the question of who pays. If the public insists on buying
