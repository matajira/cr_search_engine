TOOLS OF DOMINION

theft, 1154 lending &, 711-13
toxic waste, 579 restitution, 107
trespassing, 575, 599, 606 sabbatical year, 713
uncertainty of, 551, 559 sheep, 520

voting for, 567-70 sin & 254

water, 550, 574 sloth &, 817

wealth & 583 Soviet Union, 1165-66
zero, 601-4 Powell, Ed, 911-12

Powell, Phillip, 185
Powell, Sumner, 147n
Power
antinomianism vs., 982
decentralized, 53-54, 430
delegated, 389
Jaw &, 972
pain &, 45t

Polygamy, 50, 51, 264, 268n
Polytheism, 107, 355, 881, 884, 890
Pope, 269n
Population, 143-44, 858, 867, 870
Populism, 732
Pornography, 95, 532
Porter, Cole, 314n
Positive confession, 860, 997 Power religion, 19, 60, 155, 881, 883
Positive feedback, 64-67, 189, 868, Pragmatism, 1138

973, 976, 984-85 (see also Growth, _ Prayer, 670

Sanctions) ‘Predestination, 70, 850

Posner, Richard Predictability, 399, 687, 765, 767,
Coase’s influence, 1101 1049
damages, 580 Precision, 1127
interpersonal comparisons, 1097n —_—_-Premillennialism, 55, 690, 967
marriage, 1105 Present-orientation, $30-32, 1185
measuring satisfaction, 1085 Prestige, 1128
negligence, 560n Price
new activities, 56 discounting, 507-8
private property, 576 just, 682, 683
tenant farming, 548n meaning, 1092
time preference, 332 risk &, 501
transaction costs, 1104 slaves, 139, 365-67, 438-39
walue, 1104-5 value &, 907-8, 1086, 10
value of life, 11470 see also Prices

Price competition, 191
Price controls, 665
Price-cutting, 1031n

Possibility, 1092-93
Postmillennialism, 55, 57, 83,
967-68, 978, 980, 981, 982, 996

Prices
Potholes, 492 bidding, 613
Potter, 68 competition, 786
Poverty costs &, 547
causes, 705 information &, 697
cycle of, 822 Israel, 665
definition, 713, 755 pollution &, 568-69, 589
ethics &, 220 Pricing
gleaning, 820 free market, 805-9

legitimate, 722 monopoly, 696-97
