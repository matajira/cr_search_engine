25
FINDERS SHOULD NOT BE KEEPERS

If thou meet thine enemy’s ox or his ass going astray, thou shalt surely
bring it back to him again. Ef thou see the ass of him that hateth thee
lying under his burden, and wouldest forbear to help him, thou shalt
surely help with him (Ex, 23-4-5).

This case law, since it deals with property, is governed by the
theocentric principle of God as the cosmic Owner. He has delegated
temporary ownership of selected portions of His property to individ-
uals and organizations, so that they might work out their salvation
or damnation with fear and trembling (Phil. 2:12). Because God has
delegated responsibility for the care and use of His property to speci-
fic individuals or organizations, who are held responsible for its
management, others are required to honor this distribution of own-
ership and its associated responsibilities.

Exodus 23:4-5 requires the person who finds a stray domesti
cated beast to return it to its owner, an enemy. Why specify an
enemy? Because if a person is obedient to this narrowly defined law,
he will also be obedient to the wider implications of the law. It is not
that one may lawfully ignore a friend’s lost animal, but return an
enemy’s. The Lawgiver assumes that anyone who will do a favor for
an enemy will also do a favor for a friend.

 

There are several beneficial results of such a moral injunction
whenever it is widely obeyed. First, it upholds the sanctity of the
legal rights of property owners. Second, it reasserts man’s legitimate
control over the animal creation. Third, it reduces hostilities between
enemies, Fourth, the passage of time makes it easier to identify
thieves. Fifth, it provides an incentive to develop marks of private
ownership. It must be stressed from the beginning, however, that
this law is not a civil law, for there is no way to develop a system of
compulsory charity or compulsory righteousness through the civil

TIA
