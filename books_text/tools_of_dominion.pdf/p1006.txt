Appendix B
MAIMONIDES’ CODE: IS IT BIBLICAL?

A heathen who bustes himself with the study of the Law deserves
death. He should occupy himself with the (study) of the seven command-
ments anly, So too, a heathen who keeps a day of rest, even if it be ona
weekday, if he has set it apart as his Sabbath, is deserving of death. It is
needless to state that he merits death if he makes a new festival for him-
self. The general principle is: none is permitied to introduce innovations
into religion or devise new commandments. The heathen has the choice
between becoming a true proselyte by accepting all the commandments,
and adhering ta his own religion, neither adding to it nor subtracting
anything from it, If therefore he occupies himself with the study of the
Law, or observes a day of rest, or makes any innovation, he is logged, or
otherwise punished and advised that he is deserving of death, but he is not
put to death.

Moses Maimonides (1180)!

‘Lhe typical non-Jew would imagine that Jews throughout history
would have rejoiced whenever gentiles? read the Old Testament in

1. Moses Maimonides, The Book of Judges, Book 14 of The Code of Maimonides, 14
vols. (New Haven, Connecticut: Yale University Press, 1949), “Laws Concerning
Kings and Wars,” Chapter Ten, Section Nine, p. 237.

2. Ido not capitalize “gentile,” allhough the King James translators did, and il is
still common for writers to do so. I da not view the gentiles as a separate people in
the ethnic or national way that Americans, Mexicans, Chinese, and Jews are, To
capitalize the ward would imply that gentiles are a separate people, meaning a sepa-
rate people as contrasted to Jews, who alone are “not gentiles.” Such ethnic separa-
tion no longer exisis in principle: “What at that time ye were without Christ, being
aliens from the commonweaith of Israel, and strangers from the covenants of prom-
ise, having no hope, and without God in the world: But now in Christ Jesus yc who
sometimes were far off are made nigh by the blood of Christ. For he is our peace,
who hath made both one, and hath broken down the middle wall of partition be-
tween us” (Eph, 2:12-13). Jews equate gentiles with heathen, yet they do not capi-
talize “heathen,” for they correctly understand “hcathenism” as a spiritual condition
rather than an ethnic or national condition. I usc “gentiles” in the sense of “not
Jews,” but not in the sense of a separate cthnic or national group.

998

 
