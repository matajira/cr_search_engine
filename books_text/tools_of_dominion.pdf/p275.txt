Wives and Concubines 267

This is the proper New Testament starting point for any discussion
of the bride price.

One conclusion is inescapable: there are no more concubines in the
New Testament economy, That institution was done away with by Cal-
vary. If concubinage still were lawfully in force, it would point away
from Christ’s definitive overcoming of mankind’s slavery to sin, the
ultimate form of bondage. Permanent servitude, except as a criminal
penalty (restitution), is no longer biblically sanctioned as a valid in-
stitutional arrangement.

The concubine’s second-class legal status always ended with the
consummation of the marriage. It applied only to the betrothal per-
iod. The whole imagery of the marriage supper of the lamb?! points
to the status of the church as a free woman, a full bride in legal pos-
session of a vast dowry, the whole earth.” There are no slave wives
any more; all lawfully married women are regarded by God as hav-
ing entered marriage as free women. They gained their status as free
women by means of Christ’s payment of the bride price at Calvary.
This payment serves as the legal basis of God's adoption of His people
into His eternal family, The covenantal distinctions between the be-
trothed slave wife and the betrothed free wife have disappeared. “For
as many of you as have been baptized into Christ have put on
Christ. There is neither Jew nor Greek, there is neither bond nor
free, there is neither male nor female: for ye are all one in Christ
Jesus” (Gal. 3:27-28). Galatians 4 is the chapter above all others in
the Bible that deals with spiritual adoption—“the adoption of sons”
(v. 5)—and our deliverance out of the family of the bondwoman into
the family of the free woman, the “Jerusalem which is above” which
is free, “the mother of us all” (v, 26). The church rather than the fam-
ily is the agency of covenantal adoption in New Testament times. It
is the agency that publicly represents the new birth.”

23. Gary North, “The Marriage Supper of the Lamb,” Christianity and Civilization,
4 (1985).

24, Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ft. Worth,
Texas: Dominion Press, 1987).

25. {n churches that fully honor this principle, infants are baptized. Parents hand
over the infant to the pastor, who then baptizes it, and hands it back. This is the
public symbol of the inability of parents in their own strength two give eternal life to
their children. The church adopts children publicly, and then hands them back to par-
ents as the designated agents of the church—the covenanted, international, trans-his-
torical institution known as the bride of Christ. This does not guarantee the continu-
ing covenantal faithfulness of the children, but it does honor the legal principle that
without adoption into the family of God, each person stands condemned before Gad.
