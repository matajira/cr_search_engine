116 TOOLS OF DOMINION

Biblically speaking, there can never be a release out of servitude
as such, although there can be a release out of human slavery. Men
serve one of two masters, God or mammon, meaning God or Satan.
The great warfare in history between God and Satan is the war for
the covenantal allegiance of men. Men’s institutions reflect the
nature of the servitude they have chosen: bondservice under God or
slavery to Satan. Permanent slavery of man to man is the system that
covenant-breaking man autonomously (self-law) establishes, thereby
imitating Satan: a system of permanent tyranny. God publicly smashed
this permanent slave system at the exodus. In contrast, hierarchical
temporary bondservice of man to man under biblical law is the system
that God has established for the reformation of covenant-breaking
men, which in turn reflects His permanent covenantal rule. Inden-
tured servitude always points to liberty, meaning covenant-keeping
liberty under God. Covenant-keeping men are institutionally subor-
dinated to God in terms of a law-order that progressively brings
long-term prosperity and liberty (Deut. 28:1-14). Servitude is inesca-
pable, but the forced system of bondage known as indentured servitude
is used by God to bring self-discipline and maturity to His covenant
people. This temporary legal servitude of man to man reflected
God's judicial relationship to His people in the Old Covenant era, in
which He delivered them from Egypt by placing them under His
covenant, but one which was temporary. It would be replaced by a
better covenant, Jeremiah announced:

Behold, the days come, saith the Lorn, that I will make a new covenant
with the house of Israel, and with the house of Judah: Not according to the
covenant that I made with their fathers in the day that I took them by the
hand to bring them out of the land of Egypt; which my covenant they brake,
although I was an husband unto them, saith the Lorn ( Jer. 31:31-32).

The Hebrew phrase that the King James (and most) translators
translated as, “although I was an husband unto them,” is somewhat
obscure. An alternative reading suggested in the New International
Version is: “although I was their master.” Jacob J. Rabinowitz has
translated it: although I had acquired ownership in them, His comments on
this passage are illuminating: “The phrase ‘I took them by the hand’ is
used here in the sense of taking formal possession, such as one would
take when acquiring ownership of a slave, and the phrase ‘although T
had acquired ownership in them’ (da’aiti bam) refers to this formal
act; see also Zech. 14:13. This is to be compared with mancipium—
