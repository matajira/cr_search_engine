The Restoration of Biblical Casuistry 29

The early commentators were sorely tempted to seek a way out
of their common-ground apologetic difficulty by interpreting Paul's
language regarding the annulment of the law’s eternal death sen-
tence against redeemed mankind to mean that the Old Covenant’s
legal order is in no way judicially binding on New Testament society.
They abandoned the concept of God’s historical sanctions as appli-
cable in New Testament history. They lumped together Israel’s civil
case laws with the Old Covenant’s laws of ritual cleanliness, and
then they dismissed both varieties. This tradition lives on in modern
conservative Christian theology.

2. The Case Laws as Antiquarian

Christian Bible commentators pass over these laws on the as-
sumption that they are only of antiquarian interest. Commentators
almost never attempt to explain how these laws might have worked
in ancient Israel. They never discuss how they might be applied in
the New Testament era. Also, the commentators are unfamiliar with
even the rudiments of economic theory, so their comments on the
economic implications of these verses are almost nonexistent. Their
few brief observations are what the reader could readily have figured
out for himself. Another major problem is that far too often, the
commentators compare the biblical text with fragments of the legal
texts of the surrounding Near Eastern cultures. This is not an evil
practice in itself, but it is when they make the unproven assumption
that Israel must have borrowed its legal code from these pagan cul-
tures, They never discuss the possibility that Israel’s law code
preceded these pagan extracts, which once again raises the question
of the need for the reconstruction of biblical and Near Eastern
chronologies.”

3. The Case Laws as Mythical

Liberal humanist Bible scholars are so enamored with biblical
“higher criticism” that they pay little attention to the meaning of the
biblical texts. They prefer instead to spend their lives inventing mul-
tiple authors for each text, re-dating subsections in order to make the
Book of Exodus appear to be a composite document written cen-
turies after the exodus event (which many of them downplay any-

2. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985), Appendix A; “The Reconstruction
of Egypt's Chronology.”
