178 YOOLS OF DOMINION

legalized in Virginia in 1670 under Charles II.1©° In between these
reigns of these two kings, the Puritans ruled England. They did not
attempt to abolish slavery.

Prior to 1660, at least on Barbados, the most profitable West In-
dies island in the mid-seventeenth century, the use of slave labor was
not significantly cheaper than white labor, given the rising prices of
slaves and their greater mortality in their early years of service. But
the proportion of blacks nevertheless increased. Why? Perhaps be-
cause the slave traders of the Calvinist Netherlands continued to sell
slaves to the Caribbean during the years of the English Civil War.1®°
The Dutch also sold slaves on credit, a major incentive.!®? Another
reason was the shift to sugar production; the field workers’ living
conditions deteriorated, and potential English emigrants were not
easily induced to go to Barbados. Blacks had no choice.168 By 1670,
half of the population of the British West Indies was black; this propor-
tion was not reached in the Chesapeake region of Virginia until 1750.!6

Indentured servitude was basic to immigration to British North
America from the Puritan migration of the 1630's until the American
Revolution ended British rule. Something like one-half to two-thirds
of alt white immigrants came to the colonies as indentured servants.+7°
In Virginia, the ratio was closer to three-quarters in the seventeenth
century. !7! Indentured servitude began in Virginia as early as 1620.17?
It ended there seventy years later, replaced by black slavery.!”* But

163. Zbid., p. 389.

166. Hilary McD. Beckles, “The Economic Origins uf Black Slavery in the Bril-
ish West Indies, 1640-1680: A Tentative Analysis of the Barbados Model,” Journal af
Caribbean History, XVI (1982), pp. 36-56, I think this makes more sense than Bean
and Thomas’ argument that wages began rising in England from 1640 to 1645;
hence, it became marginally less expensive to import slaves from Africa rather than
indentured servants from England: op. cit., pp. 393-94. I cannot imagine so rapid a
shift from white indentured servitude to black slavery as a product of merely mar-
ginal price shifts.

167. Ibid., p. 43n.

168, Richard S$. Dunn, Sugar and Slaves: The Rise of the Planter Class in the English
West Indies, 1624-1713 (Chapel Hill: University of North Carolina Press, 1972), pp.
59-72, 110-16, 301-34.

169. Bean and Thomas, “I'he Adoption of Slavery in British America,” of. cit.,
p. 378.

170, Abbot Emerson Smith, Colonists in Bondage, op. cit, p. 336.

171, Craven, White, Red, and Black, p. 5.

172. David W. Galenson, “The Rise and Fall of Indentured Servitude in the
Americas: An Economic Analysis,” Journal of Economic History, XLIV (1984), p. 1.

173. Bean and Thomas, “The Adoption of Slave Labor in British America,” op.
cit., p. 382.
