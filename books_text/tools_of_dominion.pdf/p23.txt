Introduction 15

major stumbling stone for non-theonomic Christian activists. Chris-
tian pietists who self-consciously, religiously, and confidently deny
that Christians should ever gct involved in any form of public con-
frontation with humanism, for any reason, have recognized this
weakness on the part of antinomian Christian activists. They never
tire of telling the activists that they are wasting their time in some
“eschatologically futile reform program.” Such activism is a moral
affront to the pietists, Those of us who have repeatedly marched in
picket lines in front of an abortionist’s office have from time to time
been confronted by some outraged Christian pictist who is clearly far
more incensed by the sight of Christians in a picket line than the
thought of infanticide in the nearby office. “Who do you think you
are?” we are asked. “Why are you out here making a scene when you
could be working in an adoption center or unwed mothers’ home?”
(These same two questions seem equally appropriate for the pietist
critic. Who does he think 4e is, and why isn’t Ae spending his time
working in an adoption center or an unwed mothers’ home?)

Pietists implicitly and occasionally explicitly recognize that the
vast majority of today’s implicitly antinomian Christian activists possess no
biblical blueprint for building a comprehensive alternative to the kingdom of hu-
manism, The pietistic critics of activism also understand that in any
direct confrontation, Christians risk getting the stuffings—or their
tax exemptions~ knocked out of them. They implicitly recognize
that a frontal assault on entrenched humanism is futile and danger-
ous if you have nothing better to offer, since you cannot legitimately
expect to beat something with nothing. They implicitly recognize
that neither modern fundamentalism nor modern antinomian evan-
gelicalism has any such blueprint, and therefore neither movement
has anything better to offer, i.e., nothing biblically sanctioned by
God for use in New Testament times (the so-called Church Age).
Fundamentalism and evangelicalism deny the legitimacy of any
such blueprint, for blueprints inescapably require civil law and civil
sanctions. Fundamentalists have for a century chanted, “We're
under grace, not law!” They have forgotten (or never understood)
that this statement inescapably means: “We're therefore under hu-
manist culture, not Christianity.” When reminded of this, they take
one of three approaches: 1) abandon their fundamentalism in favor
of Christian Reconstructionism, 2) abandon their activism, or 3)
refuse to answer.”

28. Gary North, “The Intellectual Schizophrenia of the New Christian Right,”
Christiunity and Civilization, No. 1 (1982), pp. 1-40.
