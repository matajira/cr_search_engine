Kidnapping 325

of the final and perfect Judge.”

If we interpret the presence of the pleonasm as making the death
penalty mandatory, irrespective of the wishes of the victim, then we
create a problem for the victim. A mandatory death penalty may actually
increase the risk to the victim, once the criminal act has taken place. First, the
victim may have seen the criminal, His positive identification of the
kidnapper and his testimony against him can convict him. Second,
should the criminal begin to suspect that he is about to be caught by
the authorities, he may choose to kill the victim and dispose of the
body. By disposing of the evidence of the crime, the victim loses his
life, while the criminal reduces his risk of being detected. This is a
good reason to suppose that the death penalty for kidnapping is a
maximum allowable penalty, one which a victim can impose but
need not impose on a convicted kidnapper.

What if the kidnapper has stolen more than one adult person?
What if one adult victim asks the court to impose the death penalty,
but the other victim asks for leniency? Or, if the kidnapper has stolen
more than one minor, what if the parent or legal guardian of one
asks for the death penalty, but the parent or legal guardian of the
other recommends leniency? The victim who demands execution is
sovereign. The extension of mercy is not mandatory. The pleonasm
of execution is attached to this law. The presence of the pleonasm in-
dicates that capital punishment is the normal sanction. Anything
less than execution is abnormal: a unique sign of leniency by the vic-
tim. The victim who specifies execution is adhering to God’s written
law. He is upholding the sanctity of the sanction against sacrilege.
His decision is final.

Can the State prosecute if the victim declines? Only if the State is
itself a victim. It seems reasonable to allow the State to recover the
costs of searching for the victim. The kidnapper has stolen from the
State by his criminal act. If the State successfully prosecutes a kid-
napper, judges can impose a double restitution penalty payment for
the costs incurred. But the judges cannot lawfully impose the capital
sanction. They must uphold the principle of victim’s rights.

13. One reason why the torture of a convicted criminal prior to his execution is
immaral is that it symbolically arrogates to the Stale what God reserves exclusively
for Himself: the legal authority to tarture people for eternity. It is not that torture is
inherently wrong; rather, it is a right that God exercises exclusively. By torturing a
person prior to his execution, the State asserts that its punishments arc on a par with
God's, that the State’s penalties are to be feared as much or more than God’s judg-
ment is. Humanist thology lies at the base of such punishments.
