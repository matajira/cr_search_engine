394 TOOLS OF DOMINION

the item stolen, and the penalty is one-fifth of this.*”

The Bible does not teach that a convicted man’s future produc-
tivity should be utterly destroyed by the judges, except in the case of
capital crimes. The dominion covenant imposes a moral obligation
on all men to labor to subdue the earth to the glory of God. A man
whose body has been deliberately mutilated probably will become a
less productive worker. He may find it difficult to earn enough
wealth to repay his debt to the victim. By cutting off the pickpocket’s
hand, the State is saying that there is no effective regeneration in
life, that God cannot restore to wholeness a sinner’s soul and his call-
ing. Because he is a convicted pickpocket, he must be assumed to be
a perpetual thief by nature; therefore, the State must make his
fature labor in his illegal calling less efficient. His hand is not being
cut off because his victim lost a hand; it is being cut off simply as an
assertion of State power, and as a deterrent against crime.

Boecker correctly observes that “The intention of the talion was
not, therefore, to inflict injury ~as it might sound to us today —but to
limit injury.”™ But then he gets everything confused once again. He
says that this law restrained the institution of blood revenge.** He
never bothers to apply this principle of restraint to the modern State.
The Bible teaches that excessive penalties imposed by the State vio-
late a fundamental principle of biblical obedience, both personal and
civil: “Ye shall observe to do therefore as the Lorp your God hath
commanded you: ye shall not turn aside to the right hand or to the
left” (Deut. 5:32). Conclusion: neither is the State to cut off the pick-
pocket’s right hand or his left.”

The Punishment Should Benefit the Victim
Societies that are not governed by biblical law do not place the
proper emphasis on the principle of economic restitution. The con-

37, The King James translation reads: “he shall even restore it in the principal,
and shall add the fifth part more thereto” (6:5), The New English Bible is clearer:
“He shall make full restitution, adding onc fifth to it.” The New American Standard
reads: “{H]e shall make restitution for it in full, and add to it one-fifth more.” The
restitution payment would appear to be the penalty payment equal to the item
stolen.

38. Boecker, Law and the Administration of Justice, p. 174.

39. Ibid., pp. 174-75.

40. The Hammurabi Code specified death for any thief who had taken an oath
that he had not stolen: CH, paragraphs 9-10. There was a 30-fold restitution for
stealing animals belonging to the State: paragraph 8. Ancient Near Eastern Texts,
p. 166.
