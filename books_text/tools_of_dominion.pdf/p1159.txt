The Epistemological Problem of Soctal Cost 151

short, what about the psychic costs to the victim? Coase’s analysis
completely ignores this fundamental issuc.'”

“Coase, Get Your Cattle Off My Land!”

Or what about the farmer who sees the cattleman move in next
door? Or the cattleman who sees the sheepherder move in next door
to him? If the other man’s animals come roaming into his garden or
into his pasture, isn’t the victim entitled to compensation? What if the
“accident” of wandering animals is not an accident, but a regular way
of doing business? Shouldn’t the offender be required to put a fence
around the wandering beasts? Why should the injured party be re-
quired by the court to share the costs of fencing? Are the victim's prop-
erty rights of undisturbed ownership not to receive predictable compensation?
What I am arguing, in short, is that the victimized property owner
has the right to announce: “Coase, get your cattle off my land!”

My land: there is greater value to me in my right to enjoy my land
undisturbed than Coase’s reductionist economic analysis indicates.
To count the market value of the crops that the cattle trampled, and
then to compare that value to society with the meat that someone
will put on his table, is fo reduce the value of a man’s right of undisturbed
ownership to zero, Coase’s concept of social costs ignores one of the
most valuable assets offered to men by a free market social order: the
right of the owner to determine who will and who will not have legal access to
his property, and on what terms. To think that monetary compensation
for damaged goods at a market price is all that matters to an owner is
ridiculous. Rothbard is correct, and I cite his statement again:
“There are many problems with this theory. First, income and
wealth are important to the parties involved, although they might not be
to uninvolved economists. It makes a great deal of difference to both
of them who has to pay whom, Second, this thesis works only if we
deliberately ignore psychological factors. Costs are not only mone-
tary. The farmer might well have an attachment to the orchard far
beyond monetary damage. . . . But then the supposed indifference
totally breaks down,”1%

Even more important, there must also be compensation for the
loss of security that is necessarily involved in every willful violation

197. This is Walter Block’s main criticism: “Coase and Demsetz on Private Prop-
erty Rights,” Journal of Libertarian Studies, 1, No. 2 (1977).

198. Rothbard, “Law, Property Rights, and Air Pollution,” Cato Journal, II (Spring
1982), p. 58,
