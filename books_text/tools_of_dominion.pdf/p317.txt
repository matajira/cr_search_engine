Victim’s Rights vs. the Messianic State 309

Rabbinic Law

Rabbinic law also recognizes the legilimacy of the victim’s option
of reducing or forgiving a criminal, as S. R. Hirsch’s previous com-
ments indicate, but not in capital crimes. While he did not refer to
the pleonasm, Hirsch summarized the principle of Jewish law with
respect to capital crimes. “The whole idea of the right to grant
clemency or mercy was entirely absent in the Jewish Code of Law.
Justice and judgment is [sic] the perogative [sic] of God not Man.
When the very precisely defined Law of God, — giving Man no scope
for his own judgment or arbitrary discretion—ordains death for a
criminal, the carrying out of this sentence is not an act of harshness
ta be commuted for any consideration whatsoever, it is itself the
most considerate atonement, atonement for the community, atone-
ment for the land, atonement for the criminal. . . .”5°

The Christian cannot legitimately speak of atonement through a
criminal’s execution in this post-Calvary era, but he can and should
speak of delivering the criminal directly into God’s court, thereby
placing him under God’s sanctions rather than placing the commu-
nity under God’s sanctions for its unwillingness to obey God's law.
‘The community that allows a criminal convicted of a capital crime to
live is like a community that offers sanctuary to someone who is sup-
posed to be tried in God’s court. The community is required by God
to extradite him. It cannot legitimately offer the evil-doer sanctuary.
The text of Exodus 21:14 is clear: “. . . thou shalt take him from
mine altar, that he may die.” If a criminal is not to be granted sanc-
tuary from a human civil court at the very altar of God, then surely a
human civil court cannot legitimately grant him sanctuary by refus-
ing to extradite him to God’s heavenly court by executing him.

36. Hirsch, Axedus, p. 306: at Exodus 21:14, Hirsch immediately abandons this
rigorous judicial principle in his discussion of kidnapping. ‘I'he Talmud sets up so
many extra stipulations regarding the definition of kidnapping that it is virtually im-
possible to execute a kidnapper under Jewish law. Hirsch says that the kidnapper is
to be executed only “if he has made the man fecl that he is being treated as an object,
a thing” (p. 306). This sounds more like Immanuel! Kant than the God of the Bible.
Jewish lawyer and Talmudic scholar George Horowitz comments on the ‘lalmudic
view of kidnapping: “That the Rabbis considered the death penalty too severe for
this wrong to society and the individual, seems quite plain from the foregoing rules.
But they were bound by the express command of Scripture; hence they devised such
requirements as made conviction virtually impossible, There is no record, more-
over, that a regular court ever convicted 2 person of Manstcaling.” Horowitz, The
Spirit of Javish Law (New York: Central Book Co., [1953] 1963), pp. 197-98.
