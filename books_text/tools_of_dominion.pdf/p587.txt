Pollution, Ownership, and Responsibility 579

sources of pollution that cannot be regulated effectively by law.
There are limits to bureaucratic regulation, in other words. If self-
government fails, then civil government will fail, too,

The world is under a curse. This curse cannot be escaped, only
modified. The land brings forth thorns (Gen. 3:18) —“side effects,” in
other words, meaning unwanted effects. Pollution can be reduced
through self-discipline, better scientific knowledge, market incen-
tives, and the threat of punishment. It cannot be eliminated, how-
ever, because man’s knowledge is limited, and so is his power over
the many known effects of human action. The best that we can hope
to accomplish is to identify major sources of known dangerous pollu-
tion, to study the effects of legislation in reducing the production of
such pollution, and then persuade voters to impose workable sanc-
tions against poliuters. When criminals are convicted for illegally
dumping known toxic wastes into public sewers, and then sold into
lifetime servitude to pay the fines, we will see less toxic waste
dumped into sewers. There will always be some, however.

All government begins with self-government. Self-government
must become more important in regulating pollution, for it is not
possible to identify all polluters, and it is also not possible to elimi-
nate every known form of pollution, When polluters know that they
will suffer economic sanctions and public ostracism when convicted,
they will modify their behavior. They will not modify economically
profitable behavior until the public is willing to impose civil sanc-
tions, however. We can see this in the case of abortion. If physicians
are willing to get rich by aborting babies, we should not be surprised
to find that ordinary businessmen are willing to dump effluents into
rivers, even dangerous effluents. If the voting public and its judges
cannot distinguish between the effects of abortion (legal) and the
effects of the agricultural use of DDT (illegal), then we should not
expect to sce the spread of self-restraint by industrial polluters.
When members of a state-licensed guild are allowed to get rich by
killing babies, few people will take seriously warnings against pollu-
tion. Acid rain is hardly the threat to life and limb that uterine saline
solutions are,

Legitimate State Coercion
In the case of a single violator or a few potential violators, there
are two reasons justifying the coercive intervention of the civil gov-
ernment. First, to use the biblical example of fire, a man who permits
