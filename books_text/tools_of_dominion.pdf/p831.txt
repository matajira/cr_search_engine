Sabbatical Liberty 823

year in which all fields are required to lie fallow in the same year,
and charitable debts are cancelled in that same year (Deut. 15:1-6).®

How can the requirement of the sabbatical year be observed to-
day? Farmers can honor it by refusing to plant their fields one year
in seven, and inviting in gleaners. The land rests, the nematodes
(earthworms) multiply, and the soil is rejuvenated. The land is given
an opportunity to recover from the mistakes the farmer might have
made, including the latest chemical fad. Or farmers may choose io
rest one-seventh of their tillable land each year, The point is, farmers
honor the principle by visibly conforming to the law of the sabbatical
year.

This is not to become a matter of civil law, It is no longer the
State’s responsibility to enforce sabbath requirements. Owners can
do what they please, but God watches closely. Those who own land
that is leased to others can certainly require the lease-holders to
abide by a fallow-rotation system, so that the land’s productivity can
be preserved.

Similarly with the injunction that all zero-interest charitable
debts be cancelled every seventh year. The State is not to enforce
such a requirement. Instead, the State should simply refuse to en-
force any charitable debt contract beyond the seventh year, If credi-
tors can collect what is owed to them by poor debtors without resort-
ing to violence, that is their business, but the coercive authority of
the State will not be used to enforce a contract that clearly violates
the terms of the covenant. The State should no more enforce a mor-
ally mandatory charitable debt obligation beyond the seventh year
than it should enforce any other kind of inherently immoral con-
tract, There are limits to the legitimacy of voluntary contracts.

We should understand that the gleaning requirement from the
beginning applied only to agricultural operations. It was not ex-
tended to the cities in the Old Testament, and it should not be ex-
tended beyond agriculture today. To the extent that the modern
world has become urban, the year of release applies far more to soci-
ety’s debt structure than to its agriculture. Debt slavery is far more
common today than agricultural slavery. Today, it is the farmer who
has sold himself into bondage in his lust for more land and more
comfortable tractors. He has collateralized the present value of his

8. North, Sinai Strategy, pp. 243-48, 253-55,
