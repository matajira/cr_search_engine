culture, 900
economic collapse, 1166
kulaks, 769
liquidations, 202
medicine, 864-65
nuclear war, 34
pollution, 1160-66
poverty, 1165-66
prisons, 407-8
technology, 701

Usury
defined, 686
definition, 721-22, 755
medieval laws, 744-45
Mooney on, 1179-89
Roman church, 719-20
price controls, 509

Utah, 865-67

Utilitarianism, i101, 1122

Utility, 1070

Utopia, 731-36

Utopianism, 113-14

Uzziah, 910

Vahanian, Gabriel, 1090
Valladeres, Armando, 169n
Value
defined, 1104
economic, 558
life's, 1147
price &, 507-8, 1086, 1104
subjective, 433
to whom?, {107
value-free?, 111-12
Van Der Zee, John, 126n
Van Til, Cornelius
Barth on, 108
beads and string, 1125
break in apologetics, 14n
Christian ethics, 1134
common grace, 691n, 968-70
crack of doom, 969, 982
creation, 882
Creator-creature distinction, 882,
883
epistemology, 966, 982-83
ethics, 883

Index 1285

exhaustive knowledge, 1120
father’s lap, 279
flux and law, 1120
God and man, 882
Judaism, 1036
mediation, 883
metaphysics, 1124
natural law &, 21, 102
natural theology, 1134
neutrality, 85
passivity, 38
pietism, 38
point of contact, 1038n
presuppositions, 545
radio analogy, 110
religious consciousness, 1072
science vs. personality, 1129-30
subjective, 1085

‘Vassal, 92

Vassals, 113

Vegetarians, 695

Vengeance
arbitration, 344
biblical law, 386, 389-92
bribery &, 790
covenantal, 391
imperfect, 411
price of, 424
restitution, 420, 790
vigilantes, 352-53

Verbs, 520n

Vermont, 1194-95

Vessels of wrath, 995

Vickers, Douglas, 1130n

Victim
Becker ignores, 1143
circumstances, 705
compensation, 421-26
concern for, 414
covenant lawsuit, 279
estimates by, 430
falsely accused, 286
forgiveness, 295-96
God’s agent, 279
kidnapping, 322-23
law defends, 265
Maimonides on, 1050
