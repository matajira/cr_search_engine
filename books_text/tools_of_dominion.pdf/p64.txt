56 TOOLS OF DOMINION

in an earthly institutional kingdom at all (amillennialism).* Post-
millennialists disagree, for several reasons.

Premillennialism and amillennialism both deny that the preach-
ing of the gospel can ever bring a majority of people to faith in
Christ, thereby bringing in the earthly kingdom of God in history on
a voluntary basis, person by person, culture by culture. Premillen-
nialist author Dave Hunt has gone so far as to argue that such a
person-by-person extension of God’s kingdom is litcrally impossible
for God to achieve. Thus, in order to produce universal peace on
earth, premillennialisis have always maintained, Jesus will have to
impose a top-down bureaucracy when He comes to reign in person.
In opposition to this view, amillennialists deny the premillennial
doctrine that Jesus will ever physically return in history. They insist
{as postmillennialism also insists) that Jesus will physically appear
only at the eud of history at the final judgment. They therefore deny
(in contrast to postmillennialism) the possibility of an earthly mani-
festation of God’s comprehensive kingdom of God in history.

Because of their denial of the widespread acceptance of the gos-
pel at any point in history, premillennialists and amillennialists alike
invariably associate the word “theocracy” with some sort of top-
down, power-imposed, widely resisted rule that is imposed by an
clite. Premillennialists accept this as a valid system of civil rule, but
only if Christ personally and physically runs it from the top of the
bureaucratic pyramid. Amillennialists deny that Christ will ever do
this in history, so they deny bureaucratic theocracy’s legitimacy at
any point in the pre-final judgment future.

63. Oddly enough, Hunt also denies that there can ever be an carthly kingdom,
even in the dispensational millennium. He says in his taped interview: “What hap-
pens at the end of this time, when Satan is loosed? He deceives the nations and like
the sand of the seashore, so many—a multitude. They gather their armies and come
against Christ in Jerusalem, And, of course, that is when they finally have to be
banished from God’s presence forever. I believe it’s the final proof of the incorrigible
nature of the human heart. So, Christ Himself cannot make humanity behave. He
cannot by legislation, or by political or military or coercive means, establish this
kingdom.” did., Tape Two

64. “In fact, dominion —taking dominion and setling up the kingdom for Christ
—is an dmpossibitity, even for Gad. The millennial reign of Christ, far from heing the
kingdom, is actually the final proof of the incorrigible nature of the human heart,
because Christ Himself can’t do whut these people say they are going ta do—New
Agers or Manifested Sons.” (Verbal emphasis in the original interview.) Dominion,
Tape ‘Two.

 
