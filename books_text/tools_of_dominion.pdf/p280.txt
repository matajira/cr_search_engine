272 TOOLS OF DOMINION

fathers (Num. 36:8). With Jesus’ fulfillment of the jubilee law (Luke
4),® and with the destruction of Israel in a.p. 70, these restrictions
on inheritance disappeared. Nevertheless, family responsibilities did
not disappear just because tribal responsibility did. If daughters can
lawfully inherit, then daughters who inherit and their husbands necessarily
become legally responsible far the care of her aged parents. Thus, the hushand
of a daughter who prefers to inherit rather than accept a dowry
should legally agree in advance to become equally liable for the care
of her parents as any of her brothers, Because the West ignores such
responsibilities, it has ignored these sorts of legitimate family legal

 

contracts. As a result, familics have not been careful to take care of
aged parents. This furthered the expansion of the welfare State, for
its proponents have successfully appealed to guilt-ridden voters in
the name of indigent aged parents. The welfare State has steadily
made itself the primary heir.

The dowry is legitimate, though not required, as an alternative
to inheritance. If a father decides to pay for the cducation of his
daughter, he should tell her in advance the terms of the arrange-
ment, If this is not her dowry, but is instead an advance payment of
her lawful inheritance, then he need not seek to collect a bride price
from her future husband, but she and her husband will be expected
to bear their share of the costs of supporting the parents in their old
age. If her education or a very expensive wedding is her dowry, this
constitutes a formal admission on her part and on the part of her
husband of their obligation to repay him in the form of a bride price—
highly unlikely in our day—either before the marriage or in the
years following the marriage.

 

Since the bride price is scldom paid today, daughters and
bridegrooms implicitly do become responsible for the support of her
parents. Such implicit support is no longer regarded as enforceable
by civil law, however. Thus, the State has steadily encroached on the
family as the primary agency for the support of aged parents. ‘axes
have replaced both the bride price and financial support by children.
There has been no escape from these biblical economic and legal re-
sponsibilities; there is only a shift in institutional authority for col-
lecting and distributing the funds.

 

  

30, Gary North, “Ihe Fulfilment of the Jubilee Year,” Biblical Economies Today, VI
{April/May 1983),

31. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Instituie for Christian Economics, 1986), ch. 5.
