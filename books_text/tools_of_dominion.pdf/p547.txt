Proportional Restitution 539

and slavery. He is given responsibility over oxen and sheep, imply-
ing that he is also given responsibility over other men in various cir-
cumstances, To thwart a man in the exercise of his lawful calling is a
crime against dominion man, and is punishable by God.

Proportional restitution is imposed by the civil government as
God's lawful representative on earth. The three economic goals of
proportional restitution are these: 1) restoring full value to the victim;
2) protecting future potential victims by means of the deterrence
effect of the penalty payment (Deut. 13:11): a) animals, especially
those symbolic of man’s helplessness (sheep and oxen), and b) prop-
erty owners; and 3) offsetting the lower economic risks of detection
associated with certain kinds of theft—the slaughter or sale of spe-
cially protected edible animals.

Biblical restitution also has at least three civil goals in addition to
the three economic goals. The first civil goal of restitution is to make
life easier for the law-abiding citizen by fostering external social con-
ditions in which he can live in peace and safety. Peace and safety are
the fully legitimate goals of all biblical justice, which God has prom-
ised to bring to pass in world history through His church during a
future period of earthly millennial peace. The nations will come to
God’s church (“the mountain of the house of the Lor”) in search of
true justice (Mic. 4:1-5).

A second civil goal of biblical restitution is to make possible the
full judicial restoration of the criminal to society after he has paid the
victim what he owes him.* The State is not to concern itself with the
psychological restoration of the criminal, the victim, or society in
general. The State’s jurisdiction is strictly limited to the realm of the
judicial: restitution. The psychological state of the criminal is between
himself and God, as is the psychological state of the victim. Neverthe-
less, as in the case of the salvation of any individual by God's grace,
judicial restoration is the first step toward psychological restoration.

The third civil goal of biblical restitution is not intuitively obvi-
ous, but it may be the most important goal for the modern world, A
system of biblical restitution is required in order to reduce the likeli-

61, The modern U.S, practice of never again allowing convicted felons te vote is
clearly immoral. Under biblical law, a convicted criminal becomes a former convicted
criminal when he has made full restitution to his victims. In this sense, he is “resur-
rected” judicially, After he has paid his debt to his victims, he must be restored to full
political participation. To segregate the former convicted criminal from any area of
civic authority or participation is to deny judicially that full civil restoration is made
possible by means of God’s civil law.

 
