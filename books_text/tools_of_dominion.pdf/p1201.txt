Selling the Birthright: The Ratification of the U.S. Constitution 1193

compilation are the crucial clauses regarding the required confes-
sional oath administered to state officers, reprinted below.)

The 1780 Massachusetts constitution and the 1784 New Hamp-
shire constitution had almost identical passages requiring public
worship. Section I of the Massachusetts document affirms that “All
men are born free and equal, and have natural, essential, and
unalienable rights,” and then lists them: life, liberty, and property
ownership. Section IT says: “It is the right as well as the duty of all
men in society, publicly, and at stated seasons, to worship the
SUPREME BEING, the great Creator and Preserver of the uni-
verse.” This sounds universalistic and even Masonic. But Section IIT
establishes the right of the state to support the building of churches
and the payment of ministers’ salaries. All the denominations were
placed on equal status. Section III ends with these words: “And
every denomination of Christians, demeaning themselves peaceably,
and as good subjects of the commonwealth, shall be equally under
the protection of the law. . . .”* The same religious provisions are
found in Sections I-VI of the New Hampshire constitution, and Sec-
tion VI repeats verbatim the statement from Massachusetts’ consti-
tution: “And every denomination of christians. . . .”!° In short, the
commonwealths were explicitly designated as Christian.

The Virginia constitution of 1776 was less specific. It affirmed
freedom of conscience, and recommended “Christian forbearance,
love, and charity towards each other.”"' Virginia had a state-supported
church. Pennsylvania’s 1776 constitution specified that a man’s civil
rights could not be abridged if he “acknowledges the being of a God.”!?
Delaware in 1776 was more explicit. “That all persons professing the
Christian religion ought forever to enjoy equal rights and privileges
in this state, unless, under color of religion, any man disturb the
peace, the happiness or safety of society.”!3 Maryland’s 1776 constitu-
tion was similar: “. . . all persons, professing the Christian religion,
are equally entitled to protection in their religious liberty. . . .” Fur-
thermore, “the Legislature may, in their discretion, lay a general and
equal tax, for the support of the Christian religion. . . .”!* North

9, Ibid., p. 378.
10, fbid., p. 383.
11, Iid,, p. 312.
12. Ibid., p. 329.
13. Ibid, p. 338.
14, déid., p. 349.
