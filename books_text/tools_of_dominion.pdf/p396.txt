388 TOOLS OF DOMINION

of the “eye for eye” requirement in verse 21. The law reads: “Then
shall ye do unto him [the false witness], as he had thought to have
done unto his brother: so shalt thou put the evil away from among
you” [v, 19].?? Only if the innocent person could prove perjury on the
part of his accuser could he demand that the civil government impose
on the latter the penalty that would have been imposed on him.)

Not every Bible commentator has seen the “eye for eye” sanction
as primitive. Shalom Paul writes: “Rather than being a primitive
residuum, it restricts retaliation to the person of the offender, while
at the same time limiting it to the exact measure of the injury—
thereby according equal justice to all.’** W. F. Albright, the archeol-
ogist who specialized in Hebrew and Palestinian studies, wrote:
“This principle may seem and is often said to be extraordinarily
primitive. But it is actually not in the least primitive. Whereas the
beginnings of lex éalionis are found before Israel, the principle was
now extended by analogy until it dominated all punishment of in-
juries or homicides. In ordinary Ancient Oriental jurisprudence,
men who belonged to the higher social categories or who were
wealthy simply paid fines, otherwise escaping punishment. . . . So
the dex talionis (is)... the principle of equal justice for all!”
Albright understood some of the implications of the passage for the
principle of equal justice for all, meaning equality before the law.
Nevertheless, the myth of “primitive” legislation still clings in
people’s minds.** It seems to some Christians to be a needlessly
bloody law. In a reaction against the rigor of this judicial principle,
liberal scholar Hans Jochen Boecker goes so far as to argue that Old

22. The same rule applied in Hammurabi’s Code: “If a seignior came forward
with false testimony in a case, and has not proved the word which he spoke, if that
case was a case involving life, that seignior shall be put to death. If he came forward
with (false) testimony concerning grain or money, he shall bear the penalty of that
case.” CH, paragraphs 3-4: Ancient Near Eastern Texts, p. 166.

23. A moral judicial system would impose on the accuser or his insurance com-
pany all court costs, plus the costs incurred by the defendant in defending himself.

24. Shalom Paul, Studies in the Book of the Gouenant in the Light of Cuneiform and Bibli-
cal Law (Leiden: E. J, Brill, 1970), p. 40.

25, W.E. Albright, History, Archaeology, and Christian Humanism (New York, 1964),
p. 74; cited in éid., p. 77.

26. Hammurabi’s “code” has similar rules: “If a seignior has destroyed the eye of a
member of the aristocracy, they shall destroy his eye. If he has broken a(nother)
seignior’s bone, they shalf break his bone.” CH, paragraphs 196-97. If an aristocrat
has destroyed the eye of a commoner, however, the dex talionis did not apply: he paid
one mina of silver (CH 198). Ancient Near Eastern Texts, p. 175.
