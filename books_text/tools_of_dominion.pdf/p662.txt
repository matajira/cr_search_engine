654 TOOLS OF DOMINION

the father. The father lost his daughter forever, and the bridegroom
regained his original bride price payment.

What is clear is that in these formal judicial proceedings, the
court was implicitly using 50 shekels as the formal penalty that
would have been implicitly or actually owed to the bridegroom if the
wife had been convicted. Why? Because the payment owed to the
father by the original seducer was 50 shekels, the judicial price of an
adult male slave.

Restitution: Double or Triple?

If the bridegroom lost the case, he was required to pay to the
father-in-law the formal restitution penalty of the 50 shekels he had
sought to collect through divorce by execution, plus another 50
shekels as a penalty. The court recognized the bridegroom as some-
one involved in intent to defraud the girl’s father, whose reputation
{and possibly 50 shekels) was at stake,

Thus, we conclude that the penalty payment from the false accuser was
directly related to the compulsory formal bride price of the seducer. The new
husband had accused his father-in-law of having cheated him out of
the bride price. He never legally owed it, he insisted, yet his father-
in-law had taken it. The court denied his accusation, so he was then
forced to pay 100 shekels to his father-in-law.

The bridegroom had paid a negotiated bride price to the girl’s
father. Her father had transferred all or a part of this to her as her
dowry. She was now formally accused by her husband of being a
whore. If she was convicted, her father would probably have been
forced to pay the bridegroom the formal (50-shekel) bride price; the
bridegroom would also have kept her dowry, as her lawful heir after
her execution. If she was declared innocent, the bridegroom owed
double restitution to the father-in-law: twice the amount of the for-
mal bride price that the father-in-law would have owed to him upon
her conviction. The wife of course kept her dowry.

To repeat: since the court’s decision in this example went against
the bridegroom, he had to pay the father a hundred shekels of silver,
meaning that he returned the maximum bride price of 50 shekels,
plus an additional 50—double restitution. Furthermore, he could
never divorce her in the future (Deut. 22:19), except by public ex-
ecution for a capital crime. This indicates that the maximum formal
bride price was 50 shekeis of silver. It also indicates that any hus-
band bringing such an accusation against his bride believed that he
