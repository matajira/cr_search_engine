The Curse of Zero Growth 867

improvements in public health or medical technology.*? Effective
medicine is widely distributed and widely available. Thus, he con-
cludes, the greatest potential for improving the health of Americans
is a change in their life-style: diet, smoking, drinking, marriage, and
so forth. In short, the fundamental health issues are now ethical.

If he would define ethics as covenantal, and if he would link eth-
ics to such matters as invention, capitalization, and the diffusion of
technology to the masses, I would agree with him. Ethics has effects
far beyond personal life-style. Covenant-keeping and covenant-
breaking affect everything, including personal health.

Stagnation as Judgment

God’s covenants are frequently familistic. So are His blessings:
long life for honoring parents (Ex. 20:12), health for general obedi-
ence (Ex. 23:25), and large families (Ps. 127:5). Long-term stagna-
tion—economically, demographically, intellectually—is a sign of
God’s displeasure. Growth must not be seen as inherently destruc-
tive. More than this: a static culture cannot survive. It has to change in
order to survive. Population growth, like any kind of social growth,
can be either a blessing or a curse (a prelude to disaster), depending
on the character of the people who are experiencing the expansion.

 

43. I disagree with him here. My wife was healed in 1988 of a major viral disease,
spreading with plague-like rapidity in the United States, Epstein-Rarr Virus (also
called Chronic Fatigue Syndrome), by a few days of painless treatment, hooked up
to an electronic “black box.” She had been sutfering from a debilitating weakness for
18 months. The treatment is a repeatable phenomenon. Another friend of mine was
cured of the same disease (and several other major physical defects) by the same
technology after having one week of treatments. My wife met others at the clinic
who were being cured of far worse diseases, including muscular dystrophy (two
months later, the woman was cured, with full use of her formerly paralyzed arm).
‘The machine can be operated effectively and safely by someone with a high school
education and a few months of training.

Short of a medical collapse, however—AIDS-induced, perhaps—the medical
community will resist to the death — yours, if necessary—any such innovation, as it
has resisted others very similar to this one since the 1930’s, Such treatments break
with medical orthodoxy —drugs, surgery, and radiation — and much worse from the
profession’s viewpoint, this technology would not require medical licensure by the
State, the economic basis of the medical profession’s current monopoly. On the
nature of this monopoly, see the classic study by Reuben Kessel, “Price Discrimina-
tion in Medicine,” fournal of Law and Economics, 1 (1958), pp. 1-19. For a scientist-
physician’s cautious appraisal, after a lifetime of pioneering research, of the
astounding effects of electricity on rates of body repair, sec Robert O, Becker, M.D.
and Gary Selden, The Body Electric: Electromagnetism and the Foundation of Life (New
York: Morrow, 1985).

  
