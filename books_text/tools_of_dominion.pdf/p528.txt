520 TOOLS OF DOMINION

ox is a dominion beast, but the steer is only a source of food and
leather. The ox is productive until the day it is killed by man or
beast; the steer is simply fattened for the slaughter.

Sheep are undoubtedly quite different from oxen. They are
stupid animals. Shepherds care for them, sheep dogs monitor their
movements, but wise men do not invest a lot of time and energy in
trying to train them for service. They are not active work animals
like oxen, which pull plows; they are far more passive. A sheep is in
fact the classte passive animal —an animal whose main purpose in life is
to get sheared, They are helpless. For this reason, they are symbolic
in the Bible of the poor.

How do we make sense of the four-fold restitution payment for a
stolen sheep which is subsequently killed or sold by the thief? Why is
this loss (as indicated by the size of the restitution payment) so great
to the owner, compared to the double restitution payment he re-
ceives if the stolen sheep is restored to him by the thief? Economic
analysis of a sheep's output does not throw much light on this prob-
lem, except in a negative sense: there is no strictly economic reason.
A beast of burden such as a donkey has to be trained, and was un-
questionably a valuable asset in the Old Testament economy. So was
a horse. Yet neither slaughtered horses nor slaughtered donkeys are
singled out in the law as entitling their owners to four-fold or five-
fold restitution. What is so special about a sheep? Is its wool produc-
tion that much more valuable than the economic output of a horse or
donkey? Glearly, the answer is in the negative. We are forced to con-
clude that the distinguishing characteristic between a slaughtered
stolen donkey and a slaughtered stolen sheep has nothing to do with
the comparative economic value of each beast’s output. Instead, it
has a great deal to do with the sheep's symbolic subordinate relationship to
the owner.

Of Sheep and Men

In the Bible, animals image man.** Sheep are specifically com-
pared to men throughout the Bible, with God as the Shepherd and

31. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984), pp. 267-69.

32. Animals in men’s image: idid., p. 122. He cites Prov. 6:6; 26:11; 30:15, 19,
24-31; Dan, ; Ex. 13:2, 13. When I use the noun “image” as a verb, I am re-
minded of one cynic’s remark: “There is no noun in the English language that can-
not be verbed.”

 

 
