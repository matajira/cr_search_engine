Servitude, Protection, and Marriage 237

come dominant figures in the inevitably pseudo-farnilies that were
the product of sexual union. He writes: “Under slavery the Negro
family was essentially an amorphous group gathered around the
mother or some female on the plantation, The father was a visitor to
the household without any legal or recognized status in family rela-
tions. He might disappear as the result of the sale of slaves or be-
cause of a whimsical change in his own feelings or affection. Among
certain favored elements on the plantation, house slaves and skilled
artisans, the family might achieve greater stability and the father
and husband might develop a more permanent interest in his family,
Whatever might be the circumstances of the Negro family under the
slave regime, family and sex relations were constantly under the
supervision of the whites. The removal of authority of masters as the
result of the Civil War and Emancipation caused promiscuous sex
relations to become widespread and permitted the constant change
of spouses.”®

After emancipation, he says, white missionaries from the North
labored long and hard to persuade Negro males to formalize their
sexual relationships with women. There was resistance by the now-
emancipated males to establish a new form of “bondage.” This
undercut the development of the Negro family. “A large proportion
of the Negro families among the freedmen continued after Eman-
cipation to be dependent upon the Negro mother as they had been
during slavery.”® He admits that there were strong economic pres-
sures on black males to marry. Freedmen absolutely refused to work
as members of the gangs of laborers that had been basic to the ante-
bellum plantation economy. Instead, they did their best to start
farms as sharecroppers. The man or husband was required to sign a
rent agreement. “The more stable elements among the freedmen
who had been in a position to assimilate the sentiments and ideas of
their former masters soon undertook to buy land. This gave the hus-
band and father an interest in his wife and children that no preach-
ing on the part of white missionaries or Negro preachers could
give.” Leaders of the new post-emancipation black community also
teuded to be church members and church leaders.

     

 

65. B. Franklin Frazier, The Negro Church in America (New York: Schocken, [1964]
1974), pp. 37-38.

66. Tid., p. 39.
Iden
68, Idern

 

   
