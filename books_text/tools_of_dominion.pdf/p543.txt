Proportional Restitution 535

behavior forever. It also complements and reinforces the perfect obe-
dience of covenant-keepers who know perfectly well about the perfect
torment of covenant-breakers, with their perfect bodies that possess
the terrifying ability, like the burning bush that Moses saw, of not
being destroyed by a perfect fire. God’s perfection is manifested in
His perfect wrath.

It is not God's grace that keeps alive covenant-breakers, with
their perfect bodies that are so sensitive to every subtle aspect of their
endless torment; it is instead His uncompromising wrath that keeps
them alive. Covenant promises, conditions, and sanctions are eter-
nally perfect.56 The soul and body of every covenant-breaker are re-
united perfectly at the resurrection, so that each can experience the
eternal torments of covenant judgment as unified and fully human.
There is no dualism of body and soul in the lake of fire.”

55. On this point, I disagree with John Calvin's reference to God's grace in keep-
ing souls alive: “And although the soul, after it has departed from the prison of the
body, remains alive, yet its doing so does not arise from any inherent power of its
own. Were Gad to withdraw his grace, the soul would be nothing morc than a puff
or blast, even as the body is dust; and thus there would doubtless be found in the
whole man nothing but mere vanity” Calvin, Commentary on the Book of Psalms (Grand
Rapids, Michigan: Barker Book House, 1979), Baker's volume VI, p. 138: Ps.
103:15. There is no grace shown by God to the souls of covenant-breakers in hell or
the lake of fire. Grace is shown only to the souls of covenant-keepers. Calvin's loose
language here is misused by Edward William Fudge in his book-long attempt to
deny the biblical doctrine of eternal torment: The Fire That Consumes: A Biblical and
Historical Study of Final Punishment (Houston, Texas: Providential Press, 1982), p. 74.

56. Fudge attempts ta trace Protestantism’s doctrine of the immortality of the soul
to Calvin, and Calvin's doctrine of the immortality of the soul to Plato. This argument
is nonsense, though representative of similar arguments used by heretical theologians
to reject Bible doctrines in the name of rejecting Greek specutation, when in fact they
have adopted some variation of humanist speculation. The Bible’s doctrine of the im-
mortality of the soul and also its doctrine of eternal torment of the wicked are both
grounded in the doctrine of the covenant, It is not surprising that Fudge finds in the
Calvinist tradition the most tenacious die-hard defense of the doctrine of eternal pun-
ishment. Fudge, i#id., pp. 26n, 466. There is a reason for this tenacity. Calvinism,
more than any other Christian tradition, is grounded in the doctrine of the covenant.

57, Fudge and several of the drifting theologians whom he cites continually refer
to the orthodox doctrine of souls in hell as implicitly dualistic. The doctrine of hell is
no more dualistic than the traditional doctrine of heayen. The issue is not heaven or
hell, for both are temporary way stations for souls until God’s final judgment; the
issue is the post-resurrection world, where souls and bodies are reunited. Fudge
fudges this issue, as be does so many others. He covers his flanks with a whole series
of peripheral issues- theological and historical rabbit trails for non-covenant theo-
logians ta pursue until exhaustion. The fundamental issue is the covenant: God’s
eternal dead-end judgment for covenant-breakers. This is the issue Fudge never dis-
cusses in chapter 20, “Focusing on the Issue,” with its subsection, “Traditional
Arguments Summarized,” It is not man who is central to discussions of final judg-
ment, but God and His eternal covenant,
