28 TOOLS OF DOMINION

serve, that the resolving of practical Cases of Conscience, and the reducing
of Theological knowledge into sertous Christian Practice, and promoting
a skilful facility in the faithful exercise of universal obedience and
Holiness of heart and life, is the great work of this Treatise; . . .”!
Unlike Baxter, I had access to my library when I wrote my book; he
did not, having been barred from his pulpit by the State (after the
Restoration of Charles II in 1660), and having to write most of it
from memory, only subsequently checking the original sources.

Ignoring the Case Laws

The major problem I had in writing this book ts that there are
very few books that even explain the case laws, let alone take them
seriously, There are at least three approaches to (or, more accurately,
justifications for the rejection of) the case laws.

I. The Case Laws as Annulled

This is the standard Christian view. It has been the common
viewpoint almost from the beginning of the church. This is why
theonomy appears to be a major break with broad church tradition.
Basically, the position boils down to this: a compromise with late
classical philosophy’s natural law theory began in the carly centuries
of the church, Christian scholars appealed to universal human rea-
son as the source of rational man’s universal knowledge of civil law.
This law was seen as natural, meaning that it is implicitly m the
common possession of all rational men.

There was an early recognition on the part of church scholars
and leaders that an appeal to Old Testament case laws could not be
conformed intellectually to natural law theory. They understood the
obvious question: “If these laws were universally binding on all men,
then why did God have to reveal the specifics of His law to the
Hebrews, and only to them?” This, in fact, is a very good Christian
rhetorical answer to those who declare the universality of natural
law. The answer is simple: there is no such thing as a universal system of ra-
tional natural law which is accessible to fallen human reason. But this an-
swer was too radical to suit scholars and apologists in the early
church, just as it has been too radical for Christians ever since. It in-
volves a sharp break with the doctrine of natural law,

1. Richard Baxter, A Christian Directory: Or, A Summ of Practical Theologte, and Cases
of Conscience (London: Robert White for Nevil Sieamons, [1673] 1678), unnumbered
page, but the second page of Advertisements.
