264 FOOLS OF DOMINION

except insofar as any wife could be divorced. The Bible is silent about
any special divorce proceedings available to him under concubinage.

On the other hand, the concubine could divorce him under cer-
tain specified circumstances. She had the three rights of any wife:
food, clothing, and sexual relations. This meant that she had the
right to be given an opportunity to bear children, The text says, “If
he take him another wife; her food, her raiment, and her duty of
marriage, shall he not diminish. And if he do not these three unto
her, then shall she go out free without money” (vv. 10-11), Why list
food and clothing here? Any bondscrvant had the right to food and
clothing. Masters could legally not starve their servants, nor force
them to go naked. Thus, what the right to food and clothing must
have meant in this case was food and clothing comparable to that re-
ceived by the new wife.

If her husband did not treat the concubine equally, then she
could leave his household free of charge. She could not be legally
compelled to remain in her husband’s household if she could prove to
the authoritics that she was being treated as a second-class wife. In
other words, her legal status as a free woman had been lost when her
father sold her, but once married, she became a wife who could not
be overtly discriminated against. Her second-class legal status disap~
peared upon sexual consummation; only her second-class economic
slalus remained. She could take no cconomic assets out of the mar-
riage, other than her children, but other than this, she possessed
equal status with her husband’s other wives. Of course, she was tied
to him economically to the extent that her lifestyle outside her hus-
band’s care might have looked even worse to her, and she possessed
no dowry. Nevertheless, she retained the formal legal right to leave
his household. Her father kept the original purchase price, and she
went free.

Would she have been able to bring her children with her? It could
be argued that the concubine would ha
behind, for children of a bondservant wife stayed with the master
when the servant left (Ex. 21:4), and the master in this case was her
husband. But this would miss the point. The children did go with the
concubine when her former slave husband redeemed her. The ex-
slave husband’s payment of the redemption price (bride price) to his
former master made her his wife rather than a concubine, for her
children served as her dowry, Hagar took Ishmael when she was
forced out of Abraham’s household (Gen. 21:9-14), She was not

 

had to leave her children

 
