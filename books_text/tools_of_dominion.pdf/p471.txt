The Ransom for a Life 463

purposes of men, except as an example. It has to be cut off in the
midst of time, just as a murderer is to be cut off in the midst of time.

Why Stoning?

J. J. Finkelstein discusses at considerable length the question of
the stoning of the ox. While similar laws regarding the goring ox are
found in many ancient Near Eastern law codes, the Hebrew law is
unique: it specifically requires stoning of the ox that kills any human
being, even a slave. Finkelstein concludes that this requirement tes-
tified to the ox’s crime as being of a different order than the crime of
its negligent owner. It points to /reason, a rebellion against the cosmic
order, a crime comparable to a Hebrew’s enticing of a family mem-
ber to worship foreign gods, which was also to be punished by ston-
ing (Deut. 13:6-11). It is an offense against the whole community,
and the whole community is therefore involved in the execution.
“The real crime of the ox is that by kilting a human being — whether
out of viciousness or by an involuntary motion—it has objectively
committed a de facto insurrection against the hierarchic order estab-
lished by Creation: Man was designated by God ‘to rule over the fish
of the sea, the fowl of the skies, the cattle, the earth, and all creatures
that roam over the earth’ (Gen. 1:26, 28). Simply by its behavior —
and it is vital here to stress that intention is immaterial; the guilt is
objective —the ox has, albeit involuntarily, performed an act whose
effect amounts to ‘treason.’ It has acted against man, its superior in
the hierarchy of Creation, as man acts against God when violating
the Sabbath or when practicing idolatry. It is precisely for this reason
that the flesh of the ox may not be consumed,”!?

Finkelstein traces this biblical law forward into the Middle Ages.
In medieval Europe, trials for animals were actually held by the civil
government. Defense lawyers in secular courts were hired at public
expense to defend accused beasts. Witnesses were called. Guilty ani-
mals were destroyed as a civic act. In some cases, they were publicly
hanged.!! Few people know about this side of European history,
although specialized historians have known all along. Some of the
great minds of Western philosophy, including Aquinas and Leibniz,

10. J. J. Finkelstein, The Ox That Gored (Philadelphia: American Philosophical
Society, 1981), p. 28.

11. A painting of the hanging of a pig in Normandy in 1386 appears on the cover
of the 1987 reprint of E. P. Evans’ 1906 book, The Criminal Prosecution and Capital Pun-
ishment of Animals (London: Faber & Faber). The painting shows the pig dressed in a
jacket.
