328 TOOLS OF DOMINION

Privately, he lent $850,000 to the boy’s father to pay the ransom — at
4 percent, of course. Getty never missed an opportunity for profit.'”
The gamble paid off: the kidnappers released him. '* No other Getty
relatives hecame victims. ?

Equal Penalties or Equal Results?

The Bible does not forbid the victim’s family to pay a ransom,
but the threat of the death penalty makes the risk of conviction so
great that few potential kidnappers would take the risk, except for a
very high return. The average citizen therefore receives additional
but indirect protection because of this biblical law. The penalty to
the convicted kidnapper is so high that the money which the middle-
class victim’s relatives could raise to pay the ransom probably would
not compensate most potential kidnappers for the tremendous risk in-
volved. Presumably, kidnappers will avoid kidnapping poorer people.

In effect, the threat of the death penalty increases the likelthood that mem-
bers of very rich families or senior employees of very rich corporations will be the
primary victims of kidnappers. Also, in cases of politically motivated
kidnappings, the famous or politically powerful could become the
victims. They seem to be discriminated against economically by bib-
lical law: high penalties make it more profitable for kidnappers to
single their families out for attack. On the other hand, these people
possess greater economic resources, making it more likely that they
can more easily afford to protect themselves and their relatives.

From the point of view of economic analysis, the stiff penalty for
kidnapping protects society at large, though not always the actual
victim of the crime, and it protects the average citizen more than it
protects the rich. The law applies to all kidnappers equally; it has
varying effects on different people and groups within the society. Be-

17, Fellow billionaire industrialist Armand Hammer refers to him as “that tight
old weasel.” Armand Hammer (with Neil Lyndon), Hammer (New York; Putnam’s,
1987), p. 386. Hammer did respect him as an entrepreneur, however.

18. The grandson later suffered a stroke as a result of alcohol and drug abuse, and
is paralyzed and blind. Time (March 17, 1986), p. 80.

19. [have instructed my wife never to pay a ransom for me under any conditions
Thave also told her that I will not pay a ransom for her or any of our children, The
goal is to reduce the risk of kidnapping before it takes place, not to increase the like-
lihood of the vietim’s survival, The evil of kidnapping should not be rewarded. It
should be made devastatingly unprofitable. The same should be true for terrorist
kidnappings. The policy of the state of Israel regarding terrorist kidnappings is cor-
rect: a kidnapper-for-victims exchange before any victim is harmed, but no com-
promise thereafter.
