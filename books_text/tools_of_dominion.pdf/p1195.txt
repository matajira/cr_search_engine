Lois of Free Time: The Existentialist Utopia of S.C. Mooney 1187

economic nonsense— incredibly naive nonsense—in his attempted
denial of time-preference in human action; to oppose the Fetter-Mises
view of interest is necessarily to argue nonsense. It is the stark reality
of Mr. Mooney’s nonsense that is so impressive. He makes it clear
that if you refuse to go with Mises on the question of time-preference,
you logically must wind up with Mooney’s view regarding the eco-
nomic irrelevance of the future. If society were to adopt Mr. Mooney’s
view, and then attempt to enforce it by civil law, it would decapi-
talize itself, Rushdoony’s cloquent explanation of capitalization and
his warnings regarding decapitalization should be taken seriously:
we must choose between Christianity and existentialism.

Capitalization is the product of work and thrift, the accumulation of
wealth and the wise use of accumulated wealth, This accumulated wealth is
invested in effect in progress, because it is made available for the develop-
ment of natural resources and the marketing of goods and produce. ‘the
thrift which leads to the savings or accumulation of wealth, to capitalization,
is a product of character. Capitalization is a product in every era of the Pur-
itan disposition, of the willingness to forego present pleasures to accumulate
some wealth for future purposes. Without character, there is no capitaliza-
tion but rather decapitalization, the steady depletion of wealth. As a result,
capitalism is supremely a product of Christianity, and, in particular, of Pur-
itanism, which, more than any other faith, has furthered capitalization.'?

Today, however, the mood of modern Western man can best be de-
scribed as existentialist. It subscribes to a philosophy in which the
“moment” is decisive. It is not future oriented in that it does not plan, save,
and act with the future in mind. The existentialist demands the future
now.'3 Some of the causes which concern student rebels may be valid, but
their existentialist demand that the future arrive today make them incapa-
ble of capitalizing a culture. Existentialism requires that a man act undeter-
mined by standards from the past or plans for the future; the biology of the
moment tnust determine man’s acts.

Very briefly stated, existentialism is basically lower class living con-
verted into a philosophy. It is, moreover, the philosophy which governs
church, state, school, and society today. The “silent majority” has perhaps
never heard of existentialism, but it has been thoroughly bred into it by the
American pragmatic tradition of the “public” or state schools.

Qur basic problem today, all over the Western world, is that Western
civilization no longer has a true upper class at the helm, Future-oriented

12. Chalcedon Report (April 1967).
13. There is no better explanation for why the West has fallen behind Asia in pro-
ductivity.
