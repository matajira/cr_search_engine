722 TOOLS OF DOMINION

‘The question then must be raised: Does this prohibition apply to
every type of loan? The biblical answer is no. The Bible does indeed
prohibit any increase from charitable loans to the impoverished neigh-
bor or brother, if he is willing to live in terms of the biblical civil cove-
nant, and if he is not in poverty because of laziness or rebellion. It is
not the moral obligation of the Christian to subsidize laziness or evil.
The impoverished person must be part of the deserving poor. All four
of these qualifications must be present in order to qualify someone as
a candidate for a morally mandatory, interest-free loan. Deuteron-
omy 23:19-20 does not mention poverty. The other texts do, includ-
ing Ezekiel 18, which warns against a son who “Hath oppressed the
poor and needy, hath spoiled by violence, hath not restored the
pledge, and hath lifted up his eyes to the idols, hath committed
abomination, Hath given forth upon usury, and hath taken increase:
shall he then live? he shall not live: he hath done all these abomina-
tions; he shall surely die; his blood shall be upon him” (Ezek.
18:12-13), The specific texts that detail the limiting conditions should
be used to interpret Deuteronomy 23:19-20.%

The Bible allows other types of interest payments. First, it does
not prohibit interest payments on business loans, as Jesus’ parable of
the talents indicates (Matt. 25:27). Second, the Old Testament spe-
cifically exempted the foreigner from the protection of the prohibi-
tion against interest. It was legal to charge him interest (Deut.
23:20). Thus, any attempt to argue that the Bible always prohibits
interest payments is untenable.

Positive Injunctions

Any attempt to argue that the very nature of interest payments is
illegitimate because they involve demanding “something for
nothing,” and therefore necessarily involve cheating, is inescapably
an attempt to deny the universalism of the ethics of the Bible. The
Bible specifies that certain kinds of positive charity are appropriate for
believers in certain circumstances, but are not required in our deal-
ings with unbelievers in the same circumstances. On the other hand,

24. Those who would place a universal ban on all interest-bearing loans interpret
all Old Testament verses regarding usury in terms of the general, unqualified prohi-
bition of Deuteronomy 23:19-20. They also are forced to deny the plain teaching of
‘Jesus’ parable of the talents in Luke 19:23: “Wherefore then gavest not thou my
money into the bank, that at my coming I might have required mine own with
usury?” See below: “Interest-Seeking Loans,” pp. 744-46.

 
