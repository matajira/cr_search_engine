The Fpistemological Problem of Social Cost 1119

have to possess perfect knowledge — perfect knowledge of the best technol-
ogy, of future actions and reactions by other people, and of future natural
events. But since no one can ever have perfect knowicdge of the future, no
one’s action can be called “efficient.” We live in a world of uncertainty.
Efficiency is therefore a chimera.

Put another way, action is a learning process. As the individual acts ta
achieve his ends, he learns and becomes more proficient about how to pur-
sue them. But in that case, of course, his actions cannot have been efficient
from the start—or even from the end—of his actions, since perfect knowl-
edge is never achieved, and there is always more to learn.

Moreover, the individual's ends are not really given, for there is no rea-
son to assume that they are set in concrete for all time. As the individual
learns more about the world, about nature and about other people, his val-
ues and goals are bound to change. The individual’s ends will change as he
learns from other people; they may also change out of sheer caprice. But if
ends change in the course of an action, the concept of efficiency — which can
only be defined as the best combination of means in pursuit of given ends—
again becomes meaningless.”

‘Two comments are in order. First, we can perceive the whole cor-
pus of economics steadily slipping through our fingers. If the ques-
tion of efficiency is meaningless, what have economists been arguing
about over the last three centuries? An illusion? The answer must be
yes, if we hold to a rigorously subjectivist epistemology. “Not only is
‘efficiency’ a myth, then, but so too is any concept of social or ad-
ditive cost, or even an objectively determinable cost for each individ-
ual. But if cost is individual, ephemeral, and purely subjective, then
it follows that no policy conclusions, including conclusions about
law, can be derived from or even make use of such a concept. There
can be no valid or meaningful cost-benefit analysis of political or
legal decisions or institutions.” Rothbard has shown the intellectual
courage to affirm the validity of the implications that Roy Harrod
used to frighten Lionel Robbins away from his own denial of the
possibility of making interpersonal comparisons of subjective utility.
He denies the possibility of policy-making based on economics.

The Problem of Kxhaustive Knowledge
Second, we discover in Rothbard’s arguments against the concept of
efficiency, an argument based on the impossibility of using a concept

92. Murray N. Rothbard, “Comment: The Myth of Efficiency,” in Mario J. Rizzo
(ed.), Time, Uncertainty, and Disequilibrium, p. 90.
93. ibid., p. 94.
