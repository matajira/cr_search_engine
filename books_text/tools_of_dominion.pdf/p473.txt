The Ransom for a Life 465

Expiation

What none of the scholars discusses is the need for expiation, a
need which is both psychological and covenantal. The animal's
owner and the community at large, through its representatives, must
publicly disassociate themselves from the killer beast. They must
demonstrate publicly that they in no way sanction the beast’s mur-
derous act. There is an Old Testament precedent for the need for this
sort of formal expiation; the requirement in ancient Israel that civic
officials sacrifice a heifer when they could not solve a murder that
had taken place in a nearby field (Deut. 21:1-9). “So shalt thou put
away the guilt of innocent blood from among you, when thou shalt
do that which is right in the sight of the Lorn” (v. 9). In New Testa-
ment times we no longer need to sacrifice animals (Heb. 9, 10), but
the need for formal procedures for the expiation of the crime of man-
killing is still basic. To ignore this need is to unleash the furies of the
human heart.

The medieval world understood this to some degree, however
imperfectly; the modern humanistic West does not understand it at
all, and seeks to deny it by abolishing any trace of such ritual prac-
tices. We cannot make sense of the so-called “primitive folk practices”
of medieval and early modern Western history that dealt with this
fundamental civic and personal need, and so we refuse even to dis-
cuss them in our history books. We execute murderers in private
when we execute them at all. (In the State of Massachusetts in the
early 1970’s, the median jail term served by a murderer was under
two and a half years.)!® Humanist intellectuals in the non-Commu-
nist West seek to persuade the public that society is itself ritually
guilty for maintaining the “barbarous” practice of capital punish-
ment. Meanwhile, in the year of our Lord 1988, in the streets of
southern California, motorists were shooting each other during
traffic jams, and teenage gang members were executing at least one
victim per day.!9 God is not mocked at zero cost to the mockers.

Personal Liability and Self-Discipline

The convicted owner of the habitually goring ox in Exodus 21:28
implicitly misinformed the ox’s victim. He had known that the ox
had been violent in the past, yet he did not take steps to restrain it.

18, James Q. Wilson, Thinking About Crime (New York: Basic Books, 1974), p. 186.
19. ‘An estimated 80,000 gang members were in the county of Los Angeles.
