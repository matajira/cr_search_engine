4 TOOLS OF DOMINION

Producing a true revolution requires the support of many kinds
of printed materials, from pamphlets to thick, technical volumes.
Those in the midst of a revolution seldom have time to think through
every aspect of the changes their slogans and actions are producing,
but the revolution’s leaders need to know that the basic theoretical
work has been done, that workable, principled, and consistent an-
swers to specific historical problems are in reserve, and that after the
dust settles, the heirs of the revolution will be able steadily to
restructure society in ways that are consistent with the ideals of the
revolution. This faith has been misplaced on many past occasions,
the obvious example being Communists’ faith in Marx’s Das Kapital,
which had been inaccurate economics in theory, and which could not
be applied successfully in any Communist nation without destroying
the productivity of that economy. But it was necessary that at least
the first volume of Das Kapital be on the shelves of the revolutionaries
(the three subsequent volumes were not published in the lifetime of
either Marx or Engels). Its very presence gave confidence to those
who were launching the Communist revolution. The book was fat
and unreadable, but that was an advantage; men’s faith in Marx’s
solutions was not shattered by ever having read it.

The wise social strategist writes fat books and thin books and
books in between, not knowing which will work. Augustine and
Aquinas wrote all sorts of books. So did Kant, whose brief Universal
Natural History and Theory of the Heavens first proposed the idea of
galactic evolution. Darwin kept fattening up Origin, and then added
The Descent of Man. Marx wrote the Communist Manifesto, plus endless
journalism pieces, some of which constituted books. He also was in
partnership with Frederick Engels, who was smart enough to extract
and separately publish Socialism: Utopian and Sctentific from the still-
born Herr Eugen Dithring’s Revolution in Science, Lenin wrote materials
of all sizes, decade after decade. I, too, have written my share of thin
and medium-sized books. (Well, mostly the latter.)

Why So Fat?

This book is fat, but it is mot unreadable. It may sit on many
shelves for many years, but those who open it will be able to find spe-
cific answers to real-world economic problems —answers that are
self-consciously structured in terms of the revealed word of God. If
my answers were not detailed, if my logic were not spelled out, and
if my sources were not cited in full, then this book could no more
