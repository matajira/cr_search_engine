808 TOOLS OF DOMINION

efficiency by increasing the cost-per-unit-produced, thereby reduc-
ing output. Reducing output is not the way to national prosperity.

Meanwhile, in the international competition for scarce re-
sources, everyone outside the nation would be operating in terms of
highest money bid wins. If you were a resource owner in another na-
tion, to whom would you sell your assets? To residents of a nation
governed by highest money bid wins or residents of a nation governed
by highest percentage of assets bid wins? Probably you would sell it to
whichever bidder brought in the highest price. So, any nation oper-
ating in terms of “highest percentage of assets presently owned”
would remove itself from the world’s market. Thus cut off, it would
grow steadily poorer. It would be a nation characterized by falling
production and the consumption of present assets. It would be a
capital-consuming society.

Privacy

There is another factor to consider. Every transaction would re-
quire the seller to cxamine the assets of every potential buyer. The
buyer (seller of money) would have to bring with him a government-
authorized statement of exactly what he owned at that moment. It
would be like paying your income tax every time you went to the
market. It would be worse; it would be like going through an audit
by the civil tax collector or ecclesiastical tithe collector every time
you went to the market. No shred of financial privacy would remain
in the society, It would also lead to the creation of counterfeit assct
evaluations, for these would serve as the new currency of the realm.
You can see where the principle of “highest percentage of owned assets
offered in exchange” would lead to: reduced national competitiveness,
reduced savings, falling income, petty tyranny, and massive cheat-
ing. In short, it would lead to bankruptcy and national extinction.

What I have described is a topsy-turvy economic world. It makes
no sense. It sounds like a scene out of Alice in Wonderland. So why
dwell on the obvious? Because not all people acknowledge the obvi-
ous. They seek to operate one sphere of human existence in terms of
financing principles appropriate for another sphere. Today we have
far too many self-professed Christian social theorists who recom-
mend taxing and financing policies that would drastically hamper or
even destroy the free market. It is necessary to demonstrate clearly
that the free market operates under a different set of financing prin-
ciples from those governing a God-ordained monopoly government.
