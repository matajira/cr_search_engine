192 TOOLS OF DOMINION

parliamentary democracy, What should also not surprise us is that
privately owned chattel slaves are today a thing of the past, as are
kings. There are but five kings left in the world today, said deposed
Egyptian “King” Farouk?25: the king of England, and the kings of
spades, hearts, clubs, and diamonds. This would not have been a
believable possibility for most Europeans as recently as the begin-
ning of the twentieth century. By the end of World War I in 1918, this
was an inescapable reality, The kings departed or were dethroned.
For the first time in three millennia, we no longer hear the cries of
God!s people: “We will have a king over us; that we also may be like
all the nations” (I Sam. 8:19b-20a).

God still operates in history, making clear to His people what His
principles are, and enabling them to conform their lives to His word.
It takes time, but eventually we learn. He does not have to spell out
everything in His inspired revealed word in order for us to work out
His principles in our lives and in our societies over time. We have
the Ten Commandments and the case laws; we do not need an edi-
tion of a heavenly version of the U.S. government’s daily mon-
strosity, the Federal Register, with its 150 pages of new bureaucratic
regulations.

The Permanence of Covenants

The economic thesis that underlies my whole discussion of slav-
ery is that the fundamental issue of slavery is theological — the ques-
tion of legitimate vs. illegitimate subordination — but its manifesta-
ion is primarily economic. Man is under God and over nature,
There is no escape from hierarchy, the second point of the covenant.
Individuals are always under the authority of other individuals; au-
tonomy is impossible, The question then arises: How permanent is
the subordination of anyone in any given relationship? The biblical
answer must be: it depends on the covenant involved.

The model of the four covenants has been basic to Protestant so-
cial theory for centuries.? First, the individual covenant: an individ-
ual is always under God metaphysically, for God is the Creator, but
he can switch gods covenantally. Man can rebel against one and

225. His father had been placed on an invented throne by the British afler Britain
replaced the Ottoman Empire in Egypt in World War I. Farouk was deposed in 1952
by an army revolt

226. Richard Baxter used the four covenants as his structuring device for A Chris-
tian Directory (14673).
