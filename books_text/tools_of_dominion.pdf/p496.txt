488 TOOLS OF DOMINION

A responsible adult who comes onto another person’s properly
and falls into a pit has to have a legitimate reason for being there. If
the uncovered pit is located on a path over which a visitor might nor-
mally pass, and the pit is not easily visible, then the owner becomes
legally responsible. The visitor, in this instance, is like a dumb animal:
he is not aware of special prohibitions against walking in the vicinity
of the uncovered pit. But if the visitor has climbed over a fence and is
wandering over the property in the dead of night, where he has no
reason to be, then the owner is innocent. [f the intruder ignores “No
Trespassing” signs, he is also unprotected by the “covered pit” law.
He is not to be treated in a literate culture as if he were a dumb ani-
mal, Albeck comments: “If the dor [pit] (i.e., the hazard) is ade-
quately guarded or left in a place where persons or animals do not
normally pass, such as one’s private property, no negligence or pre-
sumed foreseeability can be ascribed and no liability would arise.”®

The pit-digger is required to reimburse the owner of the dead
beast. The latter can then buy a replacement for the dead animal.
The pit-digger becomes the owner of the dead animal. In Israel, he
could have sold it or eaten it, since it died of a known cause; it did
not die “of itself,” which would have made it forbidden meat for
Israelites (Deut. 14:21). The pit-digger does not suffer a total loss.

In modern times, people build swimming pools on their prop-
erty. These are certainly uncovered most of the swimming season.
They are holes in the ground. Are these the modern equivalent of a
pit? No. A pit is a hole in the ground which is not expected. It is not
readily visible. A swimming pool has a cement deck around it. It
may have a diving board. It is plainly visible in the back yard. It is
anything but inconspicuous. Besides, if an animal falls into it, it will
swim out. If a small child falls into it, liability could be imposed an
the owner only under the “railed roof” statute (Deut. 22:8), not
under the “uncovered pit” statute. The pool is a place of entertain-
ment and recreation, just as flat-roof housetops were in the ancient
world, It is not a pit which men stumble into unexpectedly. The so-
called “attractive nuisance” problem—a dangerous object to which
small children are attracted —falls under the railing statute.

Public “Pits”
There are areas of life that are almost always the responsibility of
the civil government. Highways are one example. If people are to

6. Albeck, “Pit,” Principles of Javish Law, col. 326
