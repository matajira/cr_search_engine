64 ‘TOOLS OF DOMINION

law as presently applicable to society is not widely believed by Chris-
tians. They believe that the cause-and-effect relationship between
obedience to God's law and His positive blessings in history is just
barely true within the socially and culturally narrow confines of the
local church congregation and the Christian family. With respect to
the authority of God’s law in society, fundamentalist Christians deny
it, meo-evangelical scholars deny it, and even traditional Reformed
theologians deny it, and for the same reason; such a view of God's law
makes Christians personally and corporately responsible for obeying
God, for receiving the promised external blessings, and for using this
real-world capital for the fulfillment of God’s dominion covenant! ~
extending His kingdom (civilization) across the face of the earth.

In contrast, Christian Reconstructionists loudly affirm biblical
law as a means of both evangelism and dominion. Indeed, the affirma-
tion of a long-term relationship between covenant-keeping and external blessings
in history, as well as covenant-breaking and external cursings in history, is the
heart and soul of the Christian Reconstructionist position on social theory, its
theological identifying mark.2 This overwhelming confidence in the
long-term historical efficacy of the biblical covenant is the reason
why Christian Reconstructionists self-consciously claim to be the
most consistent of ail covenant theologians in history. It is also why
we are confident that our view of the biblical covenant will eventually
be triumphant in history. After all, God blesses covenant-keeping in
history, and covenant-believing is surely an integral aspect of covenant-
keeping. No doubt our confidence makes us insufferable in other
theological circles, but such is always the effect of faith in God’s cove-
nant. Pharaoh found Moses insufferable, and he banished Moses
from his presence (Ex. 10:28), The Hebrew leaders had earlier tried
to do the same thing (Ex. 5:19-21). Bear in mind that Moses refused
to leave Egypt until he took the people with him. Christian Recon-
structionists have the same attitude.

God’s Sanctions and Positive Feedback in History

God's visible, external covenantal blessings serve as a means of
confirming His people’s confidence in the reliability of His covenant.
Christians are required to affirm the existence of a normative, cove-

1. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute for
Christian Economies, 1987).

2, There are other marks, of course, but this is its unique mark, No other theo-
logical movement proclaims this ethical cause-and-effect relationship in society. In-
deed, all other Christian positions explicitly deny it.
