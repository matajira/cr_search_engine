576 TOOLS OF DOMINION

within a moral framework, his study avoids a sense of unreality,
something that too many other economists have not avoided,

What do we mean by “private property”? Writes legal theorist
and economist Richard Posner: “A property right, in both law and
economics, is a right to exclude everyone else from the use of some
scarce resource.”* Professor Steven Cheung agrees, but adds two
important qualifying aspects of this legal right to exclude: a property
right is the right to exclude others from using an asset, the right to bengft
from an asset’s productivity, and the right to transfer either or both of
these two rights to others.® This is an ideal definition, as he admits.
In practice, exclusivity and transferability are matters of degree.

It should be clear why questions of pollution arise more readily
in cases where private property rights have not been (or cannot be)
established. The great area of pollution is the area of moving fluids,
namely, air and water. Who owns the air? Who owns the oceans?
Who owns the river? Everyone? No one? Economically, it makes lit-
de difference which we conclude, everyone or no one. There is a
tendency for men to waste resources under either assumption, As
Dales says, “There is an old saying that ‘everyone’s property is no
one’s property,’ the inference being that no one looks after it, that
everyone over-uses it, and that the property therefore deteriorates.
History bears out the truth of this saying in many sad ways. Property
that is freely available to all is unowned except in a purcly formal,
constitutional sense, and lack of effective ownership is almost always
the source of much mischief.”*° There is an economic incentive to
convert private costs (smoke, heat, effluents, noise) into social costs —
costs borne by others in society.

Automobile Emissions
The problem is especially acute when there are multipie and
basically unidentifiable polluters. Very often those who pollute the

48. Richard A. Posner, The Economics of Justice (Cambridge, Massachusctts: Har-
vatd University Press, 1983), p. 70.

49. “A good or an asset is defined to be private property if, and only if, three dis-
tinct sets of rights are associated with its ownership. First, the exclusive right to use
(or to decide how to use) the good may be viewed as the right to exclude other indi-
viduals from its use. Second is the exclusive right to receive income generated by the
use of the good. Third, the full right to érans/er, or freely ‘alienate,’ its ownership in-
cludes the right to enler into contracts and to choose their form.” Steven N. S.
Cheung, The Myth of Social Cost, p. 34.

50. Dales, Pollution, pp. 63-64.
