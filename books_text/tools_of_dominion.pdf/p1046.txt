1038 TOOLS OF DOMINION

An Lnvention of Modernism

Arthur A. Cohen, in his provocatively titled book, The Myth of the
Judeo-Christian Tradition, which was published by a respected publish-
ing house that specializes in scholarly Jewish studies, denies that this
tradition ever existed. It is an intellectual fabrication, he argues. He
has identified the origins of this myth: the Enlightenment and, later,
German liberal Protestant scholarship of the late-nineteenth cen-
tury. Protestant “higher critics” of the Old Testament were im-
placably hostile to Old Testament law, so they attempted to disen-
gage the New Testament from the Old. The Jew of the Old Testa-
ment was described as being “in bondage to a hopeless legalism. On
the one hand the genius of the Hebrew Bible is commended; on the
other hand Christianity is set in superior condescension to the tradi-
tions of Judaism which survive, like ruins, the advent of Jesus
Christ, the new architect of mankind. . . . The Judaism which sur-
vives the onslaught of Protestant Higher Criticism is buried under a
mountain of historicist formulations, while a pure, virtuous Kantian
Christianity — freed from Jewish accretion —is defined. Once more,
almost in recapitulation of the Gnostic tendencies of the early
Church (though turned this time to a different task), a ‘Christo-
Jewish’ tradition was defined.”*!

This implicit antinomianism of the higher critics was indeed
quite similar to the anti-Old Testament perspective of the gnostics,
Gnosticism and antinomianism are two sides of the same counterfeit
coin. Denying mankind’s access in history to a permanent higher law
above existing humanist culture, critics of the existing culture face a
grim choice: either their absorption into the prevailing culture or
their removai from influence, i.e., either assimilation or confine-
ment to a cultural ghetto.? The prevailing culture is seen as the
equivalent of ethical quicksand; one should not seek to walk through
it in the pilgrimage of life. But if men dwell in a self-imposed cultural

130, Cohen, Myth of the Judeo-Christian Tradition, pp. xviii, 196-200.

131. [bid., p. 199.

132. This dualism between the individual and socicty is a manifestation of auton
omous man’s philosophical dualism between the onc and the many. If autonomous
man is part of the one (unity), he in principle loses himself, his personality, and his
individuality. But if he maintains his independence (autonomy), he loses any point
of contact with any other individual. 'To use one of Gomelius Van Tif's analogies, he
is like a bead with no hole that seeks attachment to an infinitely long string. Philo-
suphically speaking, without God's higher law and without man as the created image
of God, individuals have no logical point of contact with each other,
