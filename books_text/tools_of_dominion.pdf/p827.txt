Sabbatical Liberty 819

on the part of both lender and borrower. What is the goal of the
lender, passive escapism or active expansion of his capital? To what
purposes will the borrowed money be put, productive or unproduc-
tive? The raw numbers do not tell us these things.

Gleaning and Liberation

“But the seventh year thou shalt let it rest and lie still; that the
poor of thy people may eat: and what they leave the beasts of the
field shall eat.” This verse makes it clear that the poor are allowed to
enter the field and glean whatever grain has come up of its own ac-
cord. The same rule applied to the vineyards. This is an extension of
the rule prohibiting the owner of the land to reap every nook and
cranny of his fields. He had to allow poor people to enter his fields
and glean the corners—the areas more difficult to harvest (Lev. 19:9-
10). The Bible specifically identifies the poor who were to be invited
in: the stranger, the orphan, and the widow (Deut. 24:21), Why is
the landowner told to do this? Predictably, it is because of Israel’s
years in Egypt: “And thou shalt remember that thou wast a bondman
in the land of Egypt: therefore I command thee to do this thing”
(Deut, 24:22),6

Is this a form of government-required public welfare? No. There
are no negative sanctions mentioned, and it is difficult to imagine
how anyone who felt abused could sue for damages. Where there are
no civil sanctions, there can be no crime. None is listed, and it is
difficult to imagine the basis by which appropriate sanctions could be
devised by the civil judges. Lex talionis? Would he be kept from glean-
ing for a year? By double restitution? Double what? How much
could the potential gleaner have gleaned from the field? How many
local potential gleaners could sue? All of them? Does each of them have
a lawful claim against the landowner, no matier how small his fields?

 

1988), p. 98. On the contrary, usury does not always enslave. Becoming a debtor for
productive reasons—to go to college, for example—or to start a business, can be
liberating. It depends on what the borrower intends to do with the money. Like fire,
debt is risky, The older you get, the less you should rely on it. But young men and
citizens in pagan nations that are trying to advance themselves economically can le-
gitimately go into debt for productive purposes. Debt is no more of a curse than per-
sonal apprenticesbip with a master is—a form of personal and professional discipline
that Mc. Mooney would have been wise to consider before he wrote his book.

6. That this law applies to the New Testament Christian gentile stems from the
fact that we are covenantally Israelites through the new birth. “But he is a Jew,
which is one inwardly; and circumcision is that of the heart, in the spirit, and not in
the Iettcr; whose praise is not of men, but of God” (Rom. 2:29).
