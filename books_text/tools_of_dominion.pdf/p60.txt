52 TOOLS OF DOMINION

In this structure of plural governments, the institutional churches
serve as advisors to the other institutions (the Levitical function), but
the churches can only pressure individual leaders through the threat
of excommunication. As a restraining factor on unwarranted church
authority, an excommunication by one local church or denomination
is always subject to review by another, if and when the excommuni-
cated person seeks membership elsewhere. Thus, each of the three
covenantal institutions is to be run under God, as interpreted by its
lawfully elected or ordained leaders, with the advice of the churches,
not their compulsion.

All Christians are in principle theocrats. All Christians say that
God rules the universe. God (éheos} rules (kratos). Theocracy means
simply that God rudes. He rules in every area of life: church, State,
family, business, science, education, etc. There is no zone of neu-
trality. There is no “king’s x” from God. Men are responsible for
everything they think, say, and do. God exercises total jurisdiction.
Jurisdiction means law (juris) and speaking (diction). God speaks His
word. It is a comprehensive word. Anyone who says that God’s law
does not apply to some area of life is thereby saying that God does
not have jurisdiction in that area. “No law—no jurisdiction.”

A Scare Word

The word “theocracy” is a scare word that humanists and fright-
ened Christians use to chase dedicated Christians away from areas
of their God-given responsibility. The critics focus on politics and
civil government as if God’s rule in this area were somehow evil. Be-
cause almost all humanists today believe in salvation through legis-
lation,®° they necessarily believe that politics is the primary means of
social healing.** The Marxists are the most consistent defenders of
human transformation through political action: the religion of revo-
lution. Because Christians are today so used to thinking in these
humanistic terms, they seldom think to themselves: “Wait a minute.
I know that God rules the family, and the government of my family

53. The exceptions to this rule ate classical liberals and free market cconomists
like F. A. Hayek and Milton Friedman, traditional conservatives like Russell Kirk
and William F. Buckley, neo-conservatives like Irving Kristol, and outright anar-
chists like Murray N. Rothbard.

54. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy (Fairfax, Virginia: Thoburn Press, [1974] 1978), chaps. 2-5, 8, 9, 11.

55. Gary North, Marx’ Reliyion of Recolution: Regeneration Through Chaos (rev. ed.;
Tyler, Texas: Institute for Christian Economics, 1989).

  
