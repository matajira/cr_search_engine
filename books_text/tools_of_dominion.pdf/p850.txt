842 TOGLS OF DOMINION

In the New Testament, the mark of the covenant is also by birth,
but only through personal profession of faith (self-acknowledged new
birth) or by parental representation.” In both cases, the person so
marked can lose his citizenship, and in the same way as in the Old
Covenant: by breaking God's laws, by failing to repent and make
restitution, and by failing to attend the required feast of covenant re-
newal: the Lord’s Supper.

The Office of Civil Magistrate

The law of the mandatory feasts did not impose negative civil
sanctions against those who refused to attend the required feasts, but
it did remove a civil privilege: the right to serve as a civil officer.
Every civil government in New Testament times is supposed to
respect the Bible’s definition of what constitutes a true citizen in the
eyes of God: @ person under the covenantal discipline of a Trinitarian church.
A citizen today, as in ancient Israel, is one who cats God’s communion
feast. If he refuses, he thereby removes himself from the jurisdiction
of the church’s court, either through resigning church membership
or through excommunication. He thereby redefines himself as no
longer being a citizen, but rather a stranger in the land, The State
acknowledges his renouncing of his citizenship. This is not a nega-
tive sanction; it is a judicial response to the former citizen’s voluntar-
ily chosen new covenantal status, namely, that of public covenant-
breaker.

Covenant-keepers were the only ones who were entitled to exer-
cise judicial authority in the land of Israel. They could legally serve
as judges or as electors of judges (Deut. 1:13; 16:18). How do we
know this? Because all men were under the protection of biblical
civil law. There was no distinguishing mark based on differing

 

degrees of protection from the civil law; one’s presence in the land
was a sufficient mark entitling one to full legal protection (Ex.
12:49). Thus, etrcumetsion had to be a mark of judicial authority as well as a
mark of judicial subordination. It was a mark of covenantal subordina-
tion under God, and therefore a mark of one’s authority to be eligible
to serve as a judge. This is why Paul could write to the Corinthians:
“Do ye not know that the saints shall judge the world? And if the
world shall be judged by you, are ye unworthy to judge the smallest
matters? Know ye not that we shall judge angels? how much more

22. Ibid., Appendix 9,
