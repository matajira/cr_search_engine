Wives and Concubines 261

law, was making a permanent purchase. If he bought her to give to
his son, then Ae was covenantally becoming her father. He would thereby
take full responsibility as her covenant father for giving her to his own
son, who would guarantee her a lifetime of support. He was in effect
adopting her into his household. It was not a seven-year or less guar-
antee, but rather a lifetime guarantee.!”

Look at Ezekiel 16. At first, Israel is described as a discarded in-
fant, God “passes by” her, picks her up, and raises her until she becomes
an adult (vv. 6-7). This was clearly an act of adoption. Then the same
phrase occurs again, God “passes by” her (vy. 8). This time, however,
God married her. Thus, with respect to God’s salvation of Israel, cov-
enanial adoption took place before covenantal marriage. This is why Exodus
21:8 says, “If she please not her master, who hath betrothed her to
himself, then shall he let her be redeemed.” The master was not
allowed to keep her if he did not marry her and if some relative
would buy her. He was able to buy her only as a bridegroom pur-
chases a wife for himself, or as a father purchases a wife for his son.

The text says, “And if he hath betrothed her unto his son, he shall
deal with her after the manner of daughters” (v. 9). He was required
to treat her as if he were her father, for covenantally speaking, he had
in fact become her father. When Abraham sent his servant to find a wife
for Isaac, he was in effect adopting Rebekah into his household. He
was taking parental responsibility for her. He was promising to watch
over her as conscientiously as her own father or brothers would.

Similarly, when a bridegroom took a wife, he was becoming her
covenantal brother."® This is why Abraham was not lying to
Abimelech when he called Sarah his sister (Gen. 26:7). This is why
the betrothed man in the Song of Solomon exclaimed, “Thou hast
ravished my heart, my sister, my spouse. . . . How fair is thy love,
my sister, my spouse!” (Song 4:9a, 10a). The bridegroom promised
to care for the woman as if he were her brother. Covenantally, she

17. Maimonides viewed her tenure as an espoused bride as ending when she
reached puberty, after age 12. Fathers could not sell daughters, he argued, once they
reached puberty: Acquisition, “Slave Laws,” Chapter Four, Section One, p. 259. She
had to consent to the marriage, Chapter Four, Section Eight, p. 262. If the master
refuses to marry her, either to himself or his son, “she shall go out free for nothing” at
puberty: Section Nine, p. 262. He was silent about the explicit biblical text, “let her
be redeemed” (v. 8). If the master fails to marry her, her father or kinsmuan-redeemer
can redeem her. The text says nothing about going out for free, or her puberty, or
any restriction against the sale of daughters heyond puberty.

18. Sutton, That You May Prosper, pp. 149-51.
