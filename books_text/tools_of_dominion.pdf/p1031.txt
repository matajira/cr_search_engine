Maimonides’ Code: Is It Biblical? 1023

It should not be surprising that there has been a conflict of views
for almost two millennia between Talmudic Jews and Christians.
The two religions are very different, Jesus summarized these ir-
reconcilable differences with His words, “you have heard it said . . .
but I say unto you.”® Paul, a former Pharisee, was even more blunt:

For there are inany unruly and vain talkers aud deceivers, specially they
of the circumcision: Whose mouths must be stopped, who subvert whole
houses, teaching things which they ought not, for filthy lucre’s sake. One of
themselves, even a prophet of their own, said, ‘lhe Cretians are alway liars,

 

evil beasts, slow bellies. This witness is true. Wherefore rebuke them sharply,
that they may be sound in the faith; Nol giving heed to Jewish fables, and
commandments of men, that turn from the truth, Unto the pure all things
are pure: but unto them that are defiled and unbelieving is nothing pure;
but even their mind and conscience is defiled. They profess that they know
God; but in works they deny him, being abominable, and disobedient, and
unto every good work reprobate (‘Titus 1:10-16).

Printing Makes u Difference

When a gentile reads the Talmud or Talmud-related writings, he
necessarily enters into Talmud-forbidden ground. If study by gen-
tiles of the written Torah itself is forbidden by Talmudic law, then
surely the once-secret Jewish oral tradition of the Torah is prohib-
ited. But when the Talmud is made available in vernacular lan-
guages by those who are still believers in its sacred character, as has
been done in this century, the traditional criticisms against gentiles
who read it necessarily fade. Perhaps even more obviously to those
who have struggled through as few as three consecutive pages of the
‘lalmud, by making available a comprehensive index, its defenders
in principle thereby “opened the book.” Its English-language trans-
lators, editors, and publisher have moved the Talmud from the
world of religion exclusively to the world of open scholarship. This
has clearly modified the ancicnt rules.

Of course, this has always been the dilemma of Talmudic Juda-
ism. Maimonides faced it when he wrote A Guide of the Perplexed
(1190). Leo Strauss is correct: the Guide is devoted to “the difficulties
of the Law” or to “the secrets of the law”: “Yet the Law whose secrets

89. [have relied in this section on the summaries and photocopies of 163 passages
in the English-language ‘falwud which was published in Christian News (July 25,
1988 and August 1, 1988), a conservative Lutheran tabloid: P.O. Box 168, New
Haven, Missouri.
