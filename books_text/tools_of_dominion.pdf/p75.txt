What Is Covenant Law? 67

covenant is some kind of New Testament theological “limiting con-
cept,” a kind of theoretical backdrop to history that no longer has any
point of contact with the actual realm of historical cause and effect.
Such a view of God’s covenant I call aniinomian: a denial of the law’s
effects in history. It reflects what I call halfway covenant thinking.

“Pro-nomianism” Defined

What do I mean by the term “antinomian”? To answer this, I
need to offer a description of “pro-nomianism,” meaning a defense of
what God’s law is and what it accomplishes, especially in history. I
begin with a survey of Ray Sutton’s discovery of the five-point bibli-
cal covenant model.1? Sutton argues that the biblical covenant model
has five parts:

Transcendence (sovereignty), yet immanence (presence)
Hierarchy/authority/representation
Ethics/law/dominion

Oath/judgment/sanctions (blessings, cursings)
Succession/continuity/inheritance

While this terminology is slightly different from that which he
adopted in his book, it is an accurate representation. '? This model
has become an integrating framework for the entire Dominion Cove-
nant economic commentary.

I use this model to develop the “pro-nomianism” of Christian
Reconstruction. It is the basis of my definition of anti-nomianism. I
use the biblical covenant model as the source of definition because I
have long maintained that language as well as everything else must
be governed by the Bible. As I wrote in 1973, “Neutrality does not
exist, Everything must be interpreted in terms of what God has re-
vealed. The humanistic goal of neutral language (and therefore neu-
tral law) was overturned at the Tower of Babel. Our definitions must
be in terms of biblical revelation.” 4

As a representative example of the structure of the biblical cove-
nant, I have selected Isaiah 45. From it we can get some sense of

12. Ray R. Sutton, That You May Prosper? Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987).

13. A correspondent to Sutton sent in the new version because it can be used to
create an acronym: THEOS.

14. Gary North, “In Defense of Biblical Bribery,” in R. J. Rushdoony, The insti-
tutes of Biblical Law (Nudey, New Jersey: Craig Press, 1973), p. 843.
