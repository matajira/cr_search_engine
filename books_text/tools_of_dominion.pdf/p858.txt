850 TOOLS OF DOMINTON

of the world, that we should be holy and without blame before him
in love. Having predestinated us unto the adoption of children by
Jesus Christ to himself, according to the good pleasure of his will”
(Eph. 1:3-5). The two most fundamental sanctions in time and eter-
nity—inheritance and disinheritance —are imposed by God in His
office as the Head of the family. This is why it is the head of the earthly
family who is most analogous judicially to God’s role as Judge, not
the civil magistrate or church officer.

Inheritance and Disinheritance

The exodus was based judicially on Israel’s office as God’s son.
God had told Moses: “And thou shalt say unto Pharaoh, ‘hus saith
the Lorn, Israel is my son, even my firstborn: And I say unto thee,
Let my son go, that he may serve me: and if thou refuse to let him
go, behold, I will slay thy son, even thy firstborn” (Ex. 4:22-23). His
ability to deliver His people from bondage in Egypt was the sign of
His office as Father, and the sign of Israel's subordination to Him as
a son. From that point on, the primary question for national Israel
would be: “Am I the son who will inherit?” And the evidence, gener-
ation after generation, pointed to the answer: no. Israel was disin-
herited finally when the true Son, Jesus Christ, came to collect His
inheritance, and the Jews refused to honor His claim:

They answered him, We be Abraham’s seed, and were never in bond-
age to any man: how sayest thou, Ye shall be made frcc? Jesus answered
them, Verily, verily, I say unto you, Whosoever committeth sin is the ser-
vant of sin. And the servant abideth not in the house for ever: but the Son
abideth cver. If the Son therefare shall make you free, ye shall be free in-
decd. know that ye are Abraham’s seed; but ye seek to kill me, because my
word hath no place in you. I speak that which I have seen with my Father:
and ye do that which ye have seen with your father, They answered and
said unto him, Abraham is our father. Jesus saith unto them, If ye were
Abraham’s children, ye would do the works of Abraham. But now ye seek
to kill mec, a man that hath told you the truth, which I have heard of God:
this did not Abraham. Ye do the deeds of your father, Then said they to
him, We be not born of fornication; we have one Father, even’ God. Jesus
said unto them, If God were your Father, ye would love me: for f proceeded
forth and came from God; neither came I of myself, but he sent me. Why
do ye not understand my speech? even because ye cannot hear my word. Ye
are of your father the devil, and the lusts of your father ye will do. He was a
murderer from the beginning, and abode not in the truth, because there is
no truth in him. When he speaketh a lie, he speaketh of his awn: for he is a
liar, and the father of it (John 8:33-44).

 
