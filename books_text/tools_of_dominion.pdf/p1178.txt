1170 TOOLS OF DOMINION

crime: “The Plague of Violent Crime” (Newsweek) and “The Curse of
Violent Crime” (Time). (We might also consider conducting a re-
scarch project on “spying and petty theft in the news magazine pub-
lishing industry.”)

In the United States between the periods 1930-34 and 1975-79,
population grew by 84 percent, 123 million to 226 million. Homicides
went up by almost 600 percent, from 14,618 to 101,044. Homicides
per 100,000 population climbed from 11.9 to 44.7. Interestingly, the
number of civil executions per homicide dropped by over 99 percent,
from one per 18.8 to one per 33,681. The growth in homicides was
relatively low from the 1935-39 era until 1945-49. But the curious fact
is that homicides per 100,000 of population dropped from 1946 until
1962, from 6.9 murders per 100,000 to 4.5. By 1972, it had climbed
to 9.4.16 Homicides went from 44,000 in 1960-64 to 101,000 in the
1975-79 period.” In Los Angeles, the increases were comparable:
population increase was 142 percent, homicides were up 686 per-
cent, and homicides per 100,000 of population tripled.

As evil as the crime of murder is, however, it must be understood
that many of the victims are far from innocent victims. A study of
murder victims in New York City made in i977 found that half of
1,622 victims in 1976 had police records. Thirty-five had been ar-
rested on murder charges themselves. Young men were the most vul-
nerable single group, constituting about a third of the victims.
Youths between the ages of 16 and 20 accounted for over a quarter of
those arrested for murder. Almost half of the victims were black, and
30 percent were hispanic. But 124 of the victims were elderly people
who were probably killed during robberies. #

James Q. Wilson points out that the number of robberies per
100,000 dropped from 1946 until 1959. Then, in 1960, it increased
sharply, remained stable for two years, and then jumped again in
1963, 1964, and 1965. In 1959, the rate had been 51.2 per 100,000; in
1968, it was 131. Auto theft had increased from 1949 until 1963, when
it rose dramatically.° He writes, “It all began in about 1963. That

16. James Q, Wilson, Thinking About Crime (New York: Basic Books, 1975), pp.
5-6.

17. Statistics compiled by the staff of California State Senator H. L. Richardson,
based on the Federal Bureau of investigation’s Uniform Crime Reports and the U. 8.
Department of Justice's Sourcebook of Criminal Justice Statistics, 1979.

18. New York Times (August 28, 1977).

19. Wilson, Thinking Abou! Crime, p. 6.
