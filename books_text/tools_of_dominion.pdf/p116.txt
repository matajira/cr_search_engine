108 TOOLS OF DOMINION

A Lifelong Affliction

This book suffers from a deliberately imposed defect: the foot-
noting of utterly irrelevant and/or utterly erroneous scholarly mate-
rial, [ attended seminary and graduate school, and I picked up some
burdensome habits. One of the great weaknesses of modern Christi-
anity is that prospective ministers are often required to attend semi-
nary, and seminary students are often required by seminary pro-
fessors to spend an inordinate amount of time reading the theological
drivel produced by higher critics. In fact, this general rule governs
seminary curricula: the better the seminary’s academic reputation,
the greater the quantity of assigned drivel. Higher criticism confuses
students in conservative seminaries, forcing them to waste precious
time that could otherwise be used in studying the Bible. It leads stu-
dents into apostasy in liberal seminaries. Professors in conservative
seminaries frequently structure their classes as if an important pas-
toral task is to keep up with the latest theories of liberal theologians,
so they train their students to become familiar with the defunct
theories of long-forgotten German theologians. In the spring
semester of 1964, I put this sign on my door at Westminster Sem-
inary: “Help stamp out dead German theologians: Attend classes
regularly.”

Conservative Bible scholars spend their lives shadow boxing with
liberals, despite the fact that the liberals pay little or no attention to
them, and are barely aware of their existence. (An exception was
Cornelius Van Til’s published criticisms of Karl Barth. “That man
hates me!” Barth was once overheard to say when Van Til’s name
was mentioned. But Barth never responded to Van Til in print, any
more than Ron Sider responded in print to David Chilton.*!
Liberals prefer not to expose their intellectual wounds in public,
especially when these wounds are mortal.) In any case, liberals
revise their theses so often that by the time the conservative has
painstakingly refuted what had been the latest liberal fad, the fad is

30. The problem with the theological seminary is that it is an institution that is
supported by donors primarily because they expect it to train ministers, when it is all
too often a graduate school in theology staffed by men whose real interest is technical
theology, and who have never themselves pastored churches. It is another example
of procedure (formal academic certification) triumphing over substance (producing
pastors).

51. Ron Sider, Rick Christians in an Age of Hunger (2nd ed.; Downers Grove, I-
linois: Inter-Varsity Press, 1984).
