Selling the Birthright: The Ratification of the U.S. Constitution 1201

Framers would have had to abandon their judicial confrontation
against Christianity.

American citizens now take this inherently atheistic national
oath of citizenship. They take it at birth. It is taken implicitly and rep-
resentatively. They are citizens by birth, This concept—citizenship by
physical birth and geography —is absolutely crucial in understand-
ing the transformation of the American covenant. In the Massachu-
setts Bay Colony in the seventeenth century, an adult male became a
citizen by formal church covenant. Without formal church member-
ship, he was merely a town resident, not a citizen. This system
began to break down almost from the beginning; becoming a prop-
erty holder made you eligible to vote in town:elections, though not
always in colony-wide elections. Steadily, the possession of capital
replaced the oath as the basis of political citizenship. Later, the for-
mal development of this principle of civil contract became John
Locke’s intellectual legacy to political thought.”

Nevertheless, there was always the oath taken in a civil court,
God’s name was brought into the proceedings. Locke was aware of
the binding nature of an oath, and its religious foundations. In his
Essay on Toleration (1685), he specifically exempted the atheist from
the civil protection of toleration: “Lastly, those are not all to be toler-
ated who deny the being of God. Promises, covenants, and oaths,
which are the bonds of human society, can have no hold upon an
atheist. The taking away of God, though but even in thought, dis-
solves all; besides also, those that by their atheism undermine and
destroy all religion, can have no pretence of religion whereupon to
challenge the privilege of toleration.”*! The oath to God reminded a
citizen of the Sovereign who would impose sanctions on courtroom
liars, so men were required to swear with one hand on a Bible and
the other one raised toward heaven, Presidents still do this when
they have the Constitutional oath administered to them. This rite is
not required by law. It is an empty formal rite in the eyes of most

40, I do not wish to overemphasize Locke’s impact on American political think-
ing. An American edition of his Treatise on Civil Government did not appear until 1773.
Of far greater influence were the writings of the 1720's by John Trenchard and
Thomas Gordon in Gato’s Letters and The Independent Whig. See Bernard Bailyn, The
Hdeological Origins of the American Revolution (Cambridge, Massachusetts: Harvard
University Press, 1967), pp. 25-36, 43-45, See also Robbins, Eighteenth-Ceniury Com-
monwealthman, op, cit.

41. Locke, Theatise of Civil Government and A Letter Concerning Toleration, edited by
Charles I. Sherman (New York: Appleton-Century Co., 1937), pp. 212-13.
