Maimonides’ Code: Is It Biblical? 1033

rounding gentile Christian culture. It is not surprising that the path
of Greek philosophy into late medieval Judaism, and then into
Christianity, was by way of Islam, especially through Maimonides.
Aristotelian Athens came to Paris through Cairo and Spain.

For centuries, Talmudic Judaism resisted the rational categories
of pagan wisdom, despite The Guide of the Perplexed. But with Rabbi
Samson Raphael Hirsch in the mid-nineteenth century, the episte-
mological barriers began to break down.” This process of cultural
and intellectual assimilation accelerated rapidly in twentieth-century
America, especially after the Second World War. The most presti-
gious American universities opened their doors to all those who
could compete academically, and Jews surely could compete. They
at last gained equal access to the professional schools—law, medicine,
architecture — as well as to the Ph.D-granting graduate schools. The
price they were asked to pay, however, was very high. Too high. The
universities offered a Faustian bargain to Jews (and also to Bible-
believing Christians): “You may go as high as your brains can carry
you, just so long as you leave your religion off campus.” Most aca-
demically oriented Jews could not resist this offer."® Intermarriage
with the gentiles whom they met on campus was also nearly inevita-
ble. Cohen’s remarks are on target: “The Jew, in joining the West, no
longer joined a Christian West, for he did not join a church wedded to
a society. .. . The Jew joined an already de-Christianizing West,
and as part of the bargain he agreed — foolishly —to de-Judaize.”""9
What Nazi Germany’s politics had not achieved in the 1930's, Prussia’s
earlier export of the academic state certification system did achieve:
the suppression of traditional religion through the enthusiastic co-
operation of the suppressed. Secular education is the humanist
world’s hoped-for “final solution” for both orthodox Christianity and
Orthodox Judaism.

In the twentieth century, the tide has rapidly flowed against
Talmudic Judaism; first the Nazis and then secularism uprooted
Orthodox Judaism. Higher criticism of the Bible has produced the

117. I. Grunfeld, “Samson Raphael Hirsch—The Man and His Mission,” in
Judaism Eternal.

118. A very effective presentation of this post-1940 transformation of Judaismn is
found in Chaim Potok’s novel and the movie based on it, The Chosen. In the early
1960's, Potok served as editor of the Jewish Publication Society of America’s transla-
tion of the Hebrew Bible. Potok, “The Bible’s Inspired Art,” New York Times Magazine
(Oct, 3, 1982), p. 63.

19, Cohen, Myth of the Judeo-Christian Tradition, p. 186.
