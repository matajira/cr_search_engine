The Hoax of Higher Criticism 1069

centuries after the fact were pathetic louts who were unable to follow
the logic of any argument, or keep names straight for three con-
secutive pages, or even imitate the style of the previous lout who first
made up some imaginary story and included it in an earlier manu-
script. All of these “discoveries” are reached by means of supposedly
precise literary techniques.

These textual critics regard the Bible as a kind of novel, so they
apply to the study of the Bible techniques that are used in the literary
criticism of fiction, Again, let me cite Wilson’s comments on the ab-
surdity of these techniques when applied to novels, let alone the
Bible. He refers to an edition of Hawthorne’s Marble Faun, edited by
the University of Virginia's specialist in Elizabethan bibliography,
Fredson Bowers, He does not spare Mr. Bowers.

But the fourth volume of the Centenary Edition of the works of Nathaniel
Hawthorne, which contains only The Marble Faun, is the masterpiece of
MLA bad bookmaking. I have weighed it, and it weighs nine pounds. It is
9x 6% inches, and 2% inches thick... . The Marble Faun, since it is mainly
Mr. Bowers’s work, embodies the spirit of Mr. Bowers as no other of these
volumes docs. Of its 610 pages, the 467 of Hawthorne are weighed down by
89 pages of “Textual Introduction” and 143 pages of “Textual Notes.” Therc
are 44 pages of historical introduction preceding the textual introduction.
We are told in these introductions, in accordance with the MLA formula,
that, in the course of writing the book, the author, as novelists often do,
changed the names of certain of the characters; and that many of the de-
scriptions in it—as has been noted, also a common practice—have been
taken from his Italian notebooks. This information is of no interest what-
ever. Nor is it of any interest to be told that Hawthorne’s wife corrected cer-
tain inaccuracies in the Roman descriptions and otherwise made occasional
suggestions, which Hawthorne did not always accept. It has evidently been
trying for Mr. Bowers to find that, in the original manuscript, the author
had been so inconsiderate as usually to make his changes “by wiping out with
a finger while the ink was still wet and writing over the same space.” But the
places where these smudges occur have been carefully noted and listed. (It
seems to me that this whole procedure meets an insurmountable obstacle
when no corrected proofs survive that show the revisions of the author.)2°

Wilson then asks the obvious question: “Now, what conceivable

value have 276 pages of ull this? Surely only that of gratifying the
very small group of monomaniac bibliographers.” He concludes,

20. Wilson, Fruits of the MEA, pp. 18-19.
