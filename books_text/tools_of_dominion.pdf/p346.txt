338 TOOLS OF DOMINION

penalty imposed on both kidnapper and buyer, if coupled with the
moral education of potential buyers of slaves (the final users), re-
duces the size and therefore the efficiency of the slave market.
(Remember Adam Smith’s observation: the division of labor is lim-
ited by the extent of the market.)

Finally, the death penalty overcomes the short-run, present-
oriented time perspective of the potential kidnappers. The magni-
tude of the punishment calls attention to the magnitude of the crime.
A death penalty forces the criminal to contemplate the possible
results of his actions.

As with ail other crimes except murder, the victim has the final
authority to specify the appropriate penalty, up to the biblically spec-
ified limit of the law. Rushdoony does not consider the concept of
victim’s rights in his Znstitutes. He writes that “the death penalty is
mandatory for kidnapping. No discretion is allowed the court. To
rob a man of his freedom requires death.” I would agree with this
statement if it were qualified as follows: “The death penalty is man-
datory for kidnapping. No discretion is allowed the court, once the
victim has specified the death of the kidnapper as his preferred pen-
alty.” To deny the victim the legal right to specify the appropriate
sanction is to deny the concept of victim’s rights.

39, Smith, 7he Wealth of Nations (1776), ch. 3.
40. Rushdoony, Jnstituies, p. 486
