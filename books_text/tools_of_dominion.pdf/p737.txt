The Prohibition Against Usury 729

ing and borrowing, including mortgages; but that would not be a
world of voluntary exchange. It would also be a world of barbarism:
the destruction of all capital by consumption.?*

Inescapable Interest

‘The phenomenon of interest is inescapable in any economy, It is
not something “extracted” from borrowers by lenders. It is inherent
in the very way we-all think about the future, whether as borrowers
or lenders, We are creatures. We are always time-constrained, We live
in the present. Those items which we presently possess are of greater
use to us— and therefore of greater economic value to us— right now
than the prospect of using those same physical items in the future. We
are covenantally responsible now for the use of whatever we presently
own or control. We therefore discount future value as against present
value. It is this present market discount of future value, above all,
which is the reason why there is an interest phenomenon in economics.

Any attempt to legislate away the inescapable effects of the rate
of interest (discount for time-preference) should be seen as a doomed
attempt to escape both time and creaturehood. To put it as bluntly as
possible, anyone who argues that an economy can operate apart
from the effects of the time-preference factor has adopted the economic
equivalent of the perpetual motion machine. Both arguments-— perpetual
motion physics and zero interest economics—rely on men’s obtain-
ing “something for nothing.”

Tn fact, anyone who would recommend civil legislation against
all interest payments is far more dangerous than a person who would
argue for legislation prohibiting all machines except perpetual motion
machines. The second person is instantly recognized as a crackpot
whose proposed legislation would destroy civilization, assuming that
the civil government would seriously attempt to enforce it. The anti-
usurer isn’t as readily recognized as a dangerous crackpot, even
though his recommendation, if seriously enforced by civil law, would
be equally a threat to the survival of civilization. Both forms of legis-
lation, if enforced, would decapitalize society. The crackpot amateur
physicist, however, cannot do what the crackpot amateur economist
can do and has done in the past: present himself as a defender of
“love” in social theory, a protector of society’s “bank-oppressed” little
people, and a person who has found a long-neglected way to climi-

 

36. Ibid., pp. 341-42, 385-86.
