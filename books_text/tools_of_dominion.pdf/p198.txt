190 TOOLS OF DOMINION

Western society at large.2!8 Gad’s means of accelerating this specific histort-
cal change was the advent of industrial capitalism, which opened the labor mar-
kets to price competition and widespread social mobility. In such a world,
slavery appeared as a restraint on trade and production. Slavery be-
came an economic anachronism in the opinion of the majority of
those who believed in the rhetoric of the free market and republican
institutions. Furthermore, capitalism urbanized the nation wherever
it spread, and slavery could not exist in capitalist cities, Slavery re-
quired a degree of social control over the slaves in leisure hours that
urban capitalism simply did not permit. Slaves steadily disappeared
from the South’s cities throughout the nineteenth century.?!9

Diehards against abolitionism held out in the South, but they
could not resist the floodgates of history. The diehards died hard, lit-
erally, by the hundreds of thousands, but they did die, Both military
and moral resistance to abolitionism ended in the United States at
Appomattox in 1865.?2° Two decades later, it ended in Brazil, the last
hold-out. (Three decades after that, slavery reappeared again in the
Soviet Union, but this was a phenomenon of the State, not of private
ownership.)

Does this mean that God works through history, bringing theo-
logical anomalies to light, pressuring His people through historical
forces to rethink their theological presuppositions? Quite clearly, He
does exactly this. There is no better proof of this than the history of
slavery.?”" Lifetime chattel slavery was wrong in principle from the

218. The Quaker founder of Pennsylvania, William Penn, was a slave owner. He
once wrote to his steward regarding the benefits of black slaves over white inden-
tured servants, “It were better if they were blacks, for then a man has them while
they live.” Cited by Scherer, Slavery and the Churches in Farly America, p. 40.

219. In 1820, 37 percent of all town dwellers were blacks. Forty years Jater, the
percentage was down to 17 percent. Urban slaves fell from 22 percent to 10 percent.
Richard C. Wade, Slavery in the Cities: the South, 1820-1860 (New York: Oxford Uni-
versity Press, 1964), pp. 243-52.

220. When the Civil War was over, only Dabney remained as a public defender
of the biblical legitimacy of “the peculiar institution” of chattel slavery. The greatest
of the South’s theologians vainly defended a slave system that Jesus had abolished in
principle the day He publicly announced His ministry by reading Isaiah 61 in the
synagogue, May God spare each of us such an ignominious end to an intellectually
stellar career: one bit of theological leaven corrupted his theological reputation.

221. We could also discuss the invention of the printing press as a crucial techno-
logical and economic factor in the coming of the Reformation. New doctrinal state-
ments came like a flood from the pens of Protestant authors, because the printing
press had created a market for the output of theologians’ pens, as well as a valid eco-
nomic incentive to widespread literacy. At last, the average person could afford a
Bible and some strident theological pamphlets.
