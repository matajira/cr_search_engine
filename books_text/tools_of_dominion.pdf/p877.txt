The Curse of Zero Crowth 869

zation finally capitulated to the inherent pessimism of all cyclical his-
tory, nothing could save it.” Rome fell: to Christianity in the East
(Byzantium), and to the barbarians in the West.

Negative feedback in one’s personal life is not necessarily a sign
of God’s curse. Positive feedback in life is not necessarily a sign of
God’s grace. There are cases where righteous individuals arc judged
(Job, for instance). It all depends on one’s ethical standing before
God. God sometimes “sets up” sinners for destruction —a kind of en-
trapment (the Pharaoh of the exodus, for instance). But generally,
growth is a blessing, and contraction is a curse: “For whosoever
hath, to him shall be given, and he shall have more abundance: but
whosoever hath not, from him shall be taken away even that [which]
he hath” (Matt, 13:12). The general rule is growth for the godly and
contraction for the ungodly. In neither case can people preserve the
status quo.

 

Humanism, Paganism, and the Status Quo

A zeru-growth philosophy is the product of humanism, both sec-
ular and occult. It is a philosophy of the status quo—the preserva-
tion of the society of Satan, as if he had not been dealt a mortal
wound at Calvary, as if he were not on the defensive internationally
against the leaven of Christ’s kingdom (Matt. 13:33). The universe is
cursed; its resources are limited; but this reality is not evidence that
favors a no-growth philosophy. The biblical doctrine of fallen man
does not teach men to believe in a world that is cursed forever. Judg-
ment and final restoration are coming. Time is bounded. Redeemed
mankind must fulfill God’s dominion assignment, in time and on
earth, before Jesus returns in final judgment.

Humanists and satanists wish to deny the sovereignty of God,
and therefore virtually all of them affirm the sovereignty of the en-
tropy process. They wish to escape the eternal judgment of God, so
they affirm an impersonal finality for all biological life. Men have
sometimes turned to a philosophy of historical cycles to help them
avoid the testimony of God concerning linear history. Others have
turned to the entropy process when they have adopted a Western

47. Charles Norris Cochrane, Christianity and Classical Culture: A Study of Thought
and Action from Angustus to Augustine (New York: Oxford University Press, [1944]
1957).

48. Perfect fulfillment is impossible becausc of sin, but it can be approached as an
cthical limit.
