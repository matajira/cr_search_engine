100. TOOLS OF DOMINION

Any principle of biblical interpretation (hcrmencutic) is danger-
ous which argues that unless an Old Testament case law is specifi-
cally repeated in the New Testament, it is automatically annulled in
New Testament times. Anyone who argues this way is going to run
into major problems. For example, bestiality is not specifically men-
tioned in the New Testament. In the Old Testament, it is listed as a
capital crime (Lev. 18:23). How are we to regard bestiality in the
New Testament? As a “victimless crime”? As an example of cruelty
to animals? As creative humanism’s version of animal husbandry?
Or as a capital crime? If the act is still a moral crime in the New Tes-
tament (derived from, say, the law regarding adultery — unless the
interpreter has also abandoned John 8:1-11), is it also a matter for the
civil courts? If it is, is the death penalty still in force? Gan you carry
over the Old ‘Testament’s definition of the act as criminal and yet not
carry over the Old Testament’s penal sanction?.On what hermeneu-
tical basis??”

 

Homosexuality and God's Penal Sanctions

Homosexual behavior was a capital crime in the Old Testament
(Lev. 20:13). Is it still a capital crime today? Virtually all non-theonomic
interpreters draw back from this politically embarrassing (in 1989)
conclusion. The extent to which the Lord will not tolerate it is seen
in the HTLV-III lentivirus. This lentivirus (long-term virus) has
been identified as the source of AIDS, the immunity system-destroying
disease among homosexuals, first designated GRID: Gay Related
Immunodeficiency Disease. The well-organized homosexual com-
munity protested, and the Centers for Disease Control renamed it
AIDS (Acquired Immune Deficiency Syndrome).”® The change in
its name in no way impaired its judgmental effectiveness biologically.

What is happening in our day is that God is now applying His penal
sanctions to male homosexuals, All of them are likely to be dead in the
year 2000, along with perhaps a hundred million of the rest of us,?°

27. Gary North, 75 Bible Questions Your Instructors Pray You Wont Ask (rev. ed;
Tyler, Texas: Institute for Christian Economics, 1988), Question 26.

28. David A. Nobel, Wayne C. Lutton, and Paul Cameron, AIDS: Acquired Im-
mune Deficiency Syndrome: Special Report (Ind ed.; Manitou Springs, Colorado: Sum-
mit Research Institute, 1987), p. 1.

29. One hundred million deaths, worldwide, has become the official estimate by
world health officials, as of early 1987. It is difficult to know whether this figure is
remulely accurate, ar deliberately overestimated (to reduce political pressures to quar-
antine the carriers), or deliberately underestimated (to reduce the threat of panic).
