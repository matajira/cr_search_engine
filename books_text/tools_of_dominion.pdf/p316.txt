308 POOLS OF DOMINION

other function might be. The presence of the pleonasm must indicate
the legitimacy of judicial discretion in cases where the pleonasm is missing. By
requiring judges to impose the maximum penalty in all cases, judi-
cial discretion disappears. The judicial principle of victim’s rights
would therefore disappear.

Second, if we argue that the judges can in all cases legitimately
impose a /esser penalty, then the emphasis aspect of the pleonasm also
disappears judicially. Cases that are governed by the pleonasm
would then become indistinguishable from those that are not. The
pleonasm would lose its force.

My conclusion is this: if the pleonasm of execution is understood
to have any judicial effect in distinguishing capital cases, and if the
principle of victim’s rights is also to be honored in all cases, then the
pleonasm should be interpreted as eliminating judicial discretion in
applying sanctions tn all cases in which prosecution has been lawfully initiated
by the civil government. The judges must not reduce the sanction of ex-
ecution in any case in which 1) the State lawfully initiates the law-
suit, and 2) the sanction is marked by the pleonasm.

Thus, the pleonasm applies ondy to a unique set of capital crimes:
where there is no identifiable human or institutional victim who
could specify a reduced sanction. The victim is God alone. The State
therefore is authorized to initiate the covenant lawsuit. There ts no
earthly victim who has the authority io reduce the sanction. The community
through the civil government is called upon to execute the convicted
criminal. In short, in the so-called “victimless crimes” in which the
pleonasm of execution applies, civil judges have no choice in decid-
ing on the appropriate sanction. The sanction is always execution.
“Dying, he shall die” binds the judges in capital crimes where the State acis as
the covenant lawsuit’s prosecutor without the presence of an intermediary or rep-
resentative human victim.

The pleonasm is not a denial of the principle of victim’s rights be-
cause God, as the primary cosmic victim, has specified the appropri-
ate sanction. This sanction must be imposed by the State in the
absence of any secondary victitn—a victim who is always authorized
to speak in Gud’s name. In the absence of such a representative, the
pleonasm takes effect. The pleonasm must therefore not be under-
stood as a limitation on the judicial principle of victim’s rights. It
limits the discretion of civil judges in those cases where there is no
identifiable earthly victim, but it does not limit the discretion of the
victim, Biblical law allows the victim, as God’s representative, to re-
duce the penalty.
