1248

discontinuity, 332
exile &, 674
extradition, 309, 324-25
goring ox, 419
heresy test, 283
manslaughter, 346
Noahic law, 311
rebellious son, 283-84
rehabilitation, 324
restitution to God, 403-4
sabbath, 913-18
sanction, 311-13
slave-owner, 361
stay of, 300
warning, 324
Executioners, 45
Exile, 674
Existentialism, 1187
Exodus
covenant model, 93
deliverance, 94
hierarchy, 91-92, 92-94
legal, 154
re-creation, 878-79
sabbath, 152
theme of, 111-12, 429
Exotic animals, 502
Expectations, 1186
Experiment, 78
Expiation, 465
Exploitation, 747, 786
Ex post facto, 562
Expulsion, 847
Externalities
automobile, 577
internalization, 563-65
involuntary, 493
Levine & Coase, 1116
“reciprocal harm,” 1105-7
Rothbard on, 1118
social cost, 1102
uncovered pit, 485
Eye for eye, see Lex talionis
Ezra, Ibn, 73n

Facts, 993, 1073, 1074
Fairbairn, Patrick, 14in

TOOLS OF DOMINION

Fair fight, 344
Faith, 816
Fall of man, 880
False oaths, 620, 625
False prophets, 384-85
False report, 827
False witness, 387, 615, 650, 768,
770-72, 785, 790
Family
assault on, 368
black, 238
church &, 271
citizenship &, 831
dominion, 215
economic growth, 872
head of, 301-2, 304
inheritance, 838
Jesus vs., 271
marriage, 644
marriage agents, 649
permanent bond, 135
producer, 855
size, 141
slave, 234-39
slave discipline, 236
slavery, 444
welfare agency, 214
Farmer, 1117-18, 1149-50
Farouk, King, 192
Fascism, 613n
Fat books, 2-6
Father
authority, 643-44
cheating the bridegroom, 651
covenant bond, 646
daughter's physician, 660n
defrauding the bridegroom, 652
liability, 646-47
pimp, 661
positive sanctions, 872
prosecutor, 643
sanctions, 853, 855-56
slave to, 650
Fatherhood
adoption, 246, 266
authority, 269
by purchase, 261-62
