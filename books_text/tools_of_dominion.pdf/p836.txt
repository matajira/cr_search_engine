828 TOOLS OF DOMINION

(Ex. 23:12), They must not mention any other God (Ex. 23:13). Then
they are given the requirement of attending the three annual feasts.

Why bring up the requirements associated with the feasts in a
section of the law that deals with civil judgment and economic op-
pression? Does participation in the feasts have some connection to
the rendering of civil judgment? It does. A circumcised male in Israel who
failed to attend the required sacramental feasts lost his inheritance in the land
and therefore also lost his citizenship. He lost his eligibility to become a
civil magistrate in Israel. This chain of judicial events is not immedi-
ately apparent from the text in Exodus 23, which is why this chapter
is a detailed exposition of implications based on other texts, espe-
cially New Testament texts regarding Israel’s loss of the kingdom
through covenantal rebellion.

An Open Invitation to Israel’s Closed Feasts

There were three required annual feasts in ancient Israel. This
law applied to the circumcised members of the congregation. The
feasts were open to all those in Israel who were circumcised, includ-
ing converts from foreign nations and household slaves. The model
feast was the Passover:

And the Lorn said unto Moses and Aaron, This is the ordinance of the
passover: There shall no stranger eat thereof; But every man’s servant that
is bought for money, when thou hast circumcised him, then shall he eat there-
of. A foreigner and an hired servant shall not eat thereof. In one house shall
it be eaten; thou shalt not carry forth ought of the Hesh abroad out of the
house; ncither shall ye break a bone thereof. All the congregation of Israel
shall keep il, And when a stranger shall sojourn with thee, and will keep the
passover to the Loxu, let all his males be circumcised, and then let him come
near and keep it; and he shall be as one that is born in the land: for no un-
circumcised person shall cat thereof. One law shail be to him that is home-
born, and unto the stranger that sojourneth among you (Ex. 12:43-49).

The Passover was originally a household feast that was actually
celebrated in the home, This is why hired servants were not allowed
to participate. They would have to return to their own households in
order to celebrate the feast. They were hired by money, and there-
fore not under the protection of the hiring family’s covenant. The
covenant was established by physical birth and circumcision, not by
an economic contract. A stranger who was circumcised could parti-
cipate in Passover, but only if all those under his household jurisdic-
