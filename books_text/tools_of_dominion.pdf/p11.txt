Introduction 3

taries on the Laws of England is a four-volume fat book, yet it was read
by just about every lawyer in the British colonies after 1765. The Fed-
eralist is fat. (Of course, it had its greatest initial effect as a series of
newspaper articles, 1787-88, during the debate over the ratification
of the U.S. Constitution, which gives us some comparative indica-
tion of the recent effects of humanist public school programs to
achieve universal literacy in the United States. Try to get the aver-
age American newspaper reader to read, digest, and comment on
The Federalist.)

A decade after Blackstone’s Commentaries, came Adam Smith’s
Wealth of Nations, a fat book. Karl Marx’s Das Kapital is a fat book; if
you include the two posthumous volumes, it is a very fat book. If you
include his posthumous multi-volume Theories of Surplus Value, it is
positively obese. All these fat books have sat on library shelves and
have intimidated people, generation after generation. And a handful
of influential people actually went to the effort to read them, subse-
quently believed them, and then wrote more books in terms of them.

Exceptions to the Rule

There are exceptions to my fat book theory. Machiavelli’s The
Prince is a thin book. So is his Discourses. John Locke's Secand Treatise of
Government is a thin book. Jean Jacques Rousseau’s Social Contract is
thin, So is Edmund Burke’s Reflections on the Revolution in France.

Then there are medium-sized books. The first edition of Charles
Darwin's Origin of Species was a medium-sized book. John Maynard
Keynes’ General Theory of Employment, Interest, and Money is a medium-
sized book. So is F. A. Hayek’s Read to Serfdom. (But when he wrote
it, Hayek’s bookshelf contained Eugen von Béhm-Bawerk’s three-
volume Capital and Interest and Ludwig von Mises’ Theory of Money and
Credit, Socialism, and Nationalékonomie | Human Action], all of which are
fat books.)

Thin and medium-sized books have their rightful place in initiat-
ing social transformations. But to maintain such a transformation,
there had better be some fat back-up books on the shelf. “What
should we do now?” the initially victorious revolutionaries inescap-
ably ask. Fat books provide answers. More than this: if fat books with
believable answers are not already on the shelf, there will not be a successful so-
cial transformation. Men will not draw others into their revolutionary
cause unless the potential recruits become persuaded that the pro-
moters have answers to specific real-world problems — problems that
contemporary society is not dealing with successfully.
