1116 TOOLS OF DOMINION

ics of the solution, there will be serious social consequences. To dis-
cuss the efficiency of any given transaction without also discussing
the equity of it is te begin to deliver the society into the hands of so-
cialist revolutionaries. Or, to put it in language more familiar to
Chicago School economists, penalizing righteousness in the name of eco-
nomic efficiency is not a zera-cost decision,

Micro-Eefficiency and Macro-Revolution

Tt is not possible to discover an economically efficient solution to
just one transaction. We cannot be efficient in just one thing. The
question of efficiency is not simply a microeconomic issue; it is also
macrocconomic. We cannot discover an efficient solution to any eco-
nomic problem that does not in some way affect the whole social or-
der, In short, we cannot do just one thing efficiently. The system of justice
that governs any social order is itself a producer or reducer of both
macro-efficiency and micro-efficiency. Equity cannot be segregated
from efficiency. If our supposedly economically efficient decision at
the micro level calls into question the moral integrity of the prevail-
ing legal system, we have not in fact reached an efficient solution to
our micro problem. This is why it is astonishing to find economist
and Talmudist Aaron Levine siding with Coase: “While the principle
of equity is promoted by the selection of appropriate liability rules,
economic efficiency is realized when the negative externality is elimi-
nated by the éeast-cost method. Hence, should it be less costly to avoid
crop damage by growing smoke-resistant wheat than by installing a
smoke-control device, the former method should be adopted. Whether
the farmer or the factory-owner should bear the additional expense
of eliminating the negative externality is entirely irrelevant as far as
the efficiency question is concerned.”® Charge the farmers for the
cost of the factory’s smoke abatement, and you have violated the
principle of justice that governs Exodus 22:5-6. There will eventually
be negative repercussions, whether economists believe in God or not.

These certified economists are certifiable idiots; they are anar-
chists who are brandishing equations rather than bombs. The reduc-
tionism of economic logic, even without the equations, has become
so great that it has just about eliminated the real-world relevance of
the academic discipline of economics, especially its academic jour-
nals. That which is obvious escapes these people. They speak of a

88. Levine, Free Enterprise and Jewish Law, p. 59
