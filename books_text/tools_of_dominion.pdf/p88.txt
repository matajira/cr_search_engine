380 TOOLS OF DOMINION

The fact that our Scriptures can be used to support or condemn any eco-
nomic philosophy suggests that the Bible is not intended to lay out an eco-
nomic plan which will apply for all times and places. If we are to examine
economic structures in the light of Christian teachings, we will have to do it
in another way,??

Since koinonia includes the participation of everyone involved, there is no
blueprint for what this would look like on a global scale. . . . We are talk-
ing about a process, not final answers.

There is in Scripture no blueprint of the ideal state or the ideal economy.
We cannot turn to chapters of the Bible and find in them a model to copy or
a plan for building the ideal biblical state and national economy.”

“Blueprint” is the code word for biblical law for those who do not
want to abey biblical law. Second, “God’s principles” is the code
phrase for fundamentalists who are nervous about appearing totally
antinomian, but who are equally nervous about breaking openly
with the teachings and language of dispensationalism, i.e., “we're
under grace, not law.” Finally, “God’s moral law” is the code phrase
for the evangelical and Reformed man who does not want to be
branded an antinomian, but who also does not want to be bound by
the case laws of the Old Testament. In all these cases, the speaker re-
jects the idea of the continuing authority of the case laws.

It all boils down to this: Satan’s rhetorical question, “Hath God
said?” (Gen. 3:1). The proper response is, “Yes, God hath said!” He
is the sovereign Creator. He has laid down the law.

4. Oath/ Judgment/Sanctions

There are two kinds of sanctions: blessings and cursings. God
told Adam that in the very day he ate of the tree, he would surely die.
(“Dying, you shall die”: the familiar biblical pleonasm.)?6 This
means a negative sanction in History. Satan told Eve that she would
not surely die. Instead, she would know good and evil, as God does:
a positive sanction. Which would it be? “To die or not to die, that is
the question.”

23. William Diehl, “Ihe Guided-Market System,” in Robert G, Clouse (ed.),
Wealth and Poverty, op. cit., p. 87.

24. Art Gish, “Decentralist Economics,” ihid., p. 154.

25. John Gladwin, “Centralist Economics,” ibid., p. 183.

26. See Chapter 7: “Victim’s Rights.”
