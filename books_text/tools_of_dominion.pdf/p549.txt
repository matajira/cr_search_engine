18
POLLUTION, OWNERSHIP, AND RESPONSIBILITY

Ifa man shall cause a field or vineyard to be eaten, and shall put in
his beast, and shall feed in another man's field; of the best of his own
field, and of the best of his own vineyard, shall he make restitution. If
Jive break oui, and catch in thorns, so that the stacks of corn, or the stand-
ing corn, or the field, be consumed therewith, he that kindled the fire shall
surely make restitution (Ex. 22-5-6).

The theacentric issue raised by this passage rests on the recogni-
tion of each person’s legal obligations as a responsible steward over
private property in a world in which God is the absolute owner of the
world. As part of His providential administration over the world,
God establishes boundaries in life. These boundaries are ultimately
ethical: the boundaries between covenant-keepers and covenant-
breakers. The existence of these ethical boundaries is reflected in
every area of life, Man cannot think or act apart from boundaries of
various kinds. These ethical boundaries are reinforced by legal
boundaries that separate the use of property. Boundaries are there-
fore inescapably tied to the legal issue of personal responsibility be-
fore God and man.

God parcels out property to his subordinates. The very phrase,
parcels out, reflects the noun, a parcel. God places specified units of
land under the administration of specific individuals, families, and
institutions. This division of authority is an aspect of God’s overall
system of the division of labor, Responsibility for the administration
of specific property units can therefore be specified by law. The
allocation of legal responsibility matches the allocation of property.
God holds specific people responsible for their stewardship over spe-
cific pieces of property, This enables owners to evaluate their own
performance as stewards, and it also allows the free market and
God-ordained governmental authorities to evaluate owners’ specific

541

 
