1274

Retreat, 59-60
Retroactive penalty, 558-62
Revenge, 258, 352, 783
Reventlow, Henning G., 184n,
1064n, 1065, 1066-67
Revival, 35, 38-39, 41, 55, 170, 900
Revivalism, 979-81
Revolutidh, 4, 7, 34-36, 983
Rewards, 802, 803, 852
Reymond, Robert, 1039-40
Reynolds, Morgan, 1146
Rheinstein, Max, 99n
Rhode Island, 147, 200n, 233n
Rice, 824n
Rich, 718, 771-72
Rights, 639-40, 674, 775
Rings, 470-71
Risk
“calculated,” 762
caretaker’s, 636
culture &, 590-91
drunk drivers, 491-92
highway, 489-90
inescapable, 563
information, 459
insurable, 557-589
interest rate, 727, 1184
involuntary, 495
kidnapper, 322-23, 325
known, 500
liability, 473-75
ownership &, 459
pit, Chapter 15
pollution, 555
price &, 501
shared, 615-16
shifting of, 749-51, 754-55
slave trade, 335
theft &, 621
time perspective, 331
toxic waste, 572-73
transfer of, 551
undiscovered, 558-59
unequal, 328-29, 337
Risk premium, 727
Rites, 674, 834, 883, 889
Ritual meal, 286

TOOLS OF DOMINION

Rizzo, Mario, 1120-21
Roads, 488-90
Robbins, Lionel, 1096-1100, 1108
Robertson, Pat, 470
Robin Hood, 351
Rockefeller Foundation, 870
Rod, 379
Rodgers, Daniel, 375
Rodkinson, Michael, 1028
Roll, Eric, 735-36
Rolls-Royce, 731
Roman law, 116-17, 1200
Rome
architecture, 900
games, 341-42
Israel &, 251n, 289
jubilee land law, 144
men into gold, 159-60
politics & ritual, 674
present-orientation, 868-69
slavery, 163
Roof, 488
Rope, 472-73, 475
Rostow, W. W., 146n
Rothbard, Murray
capitalization, 508
Coase theorem, i117-18
easement, 552-53
efficiency, 1118
efficiency and ethics, 1123
ends, 1121-22
equilibrium, 1121
externalities, 118
homesteading principle, 552-53
interpersonal comparisons, 1117
Jews and Civilization, 32n
Kondratieff Wave, 146n
learning, 1118, 1121
marriage contracts, 135n
policy, 123
property rights, 1118
psychic costs, 1151
public property, 592-93
self-ownership, 135n
social cost, 1117-18
subjectivism, 1117-23
subpoena, 285n

 
