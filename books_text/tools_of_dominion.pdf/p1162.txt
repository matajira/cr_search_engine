1154 TOOLS OF DOMINION

words, the rights of private ownership —~ the legal right to exclude~
and the sense of outrage at an invasion of one’s property are econom-
ically irrelevant. In a world of perfect competition, amazing things
happen. The economic significance of the theft involved in polluting
a neighbor’s environment is zero.

All we need is to reduce transaction costs. That should not be too
difficult. The polluter can pick up a gun, walk over to his neighbor,
put the gun to his head, and force him to deed over his property.
Presto: the “internalization” of pollution costs! It will not alter eco-
nomic output one little bit, Stigler assures us. This surely is a cost-
effective way to reduce transaction costs. Unless, of course, one’s
neighbor also has a gun. That, of course, is the whole point.

What possible objection can a self-proclaimed ethically neutral
economist offer to this sort of wealth-transfer? This is the question
Leff asks is a perceptive critique of the “economics and law” ap-
proach to social theory:

Let us say T am naturally superior to a rich man in taking things, either by
my own strength or by organizing aggregations of others (call them govern-
ments) to do my will. I am not much of a trader, but I'm onc hell of a grabber.
That's just the way things are. Is there any way to criticize my activities ex-
cept from the standpoint of taste (or some other normative proposition)? It
would be inefficient to allow violent acquisitions? How can one know that?
All of Posner’s arguments about the efficiency-inducing effects of private
property assume only that someone has the right to use and exclude, not that
it be any particular person, If force, organized or not, were admissible as a
method of acquisition there is no reason to assume that eventual equilibrium
would not be reached, albeit in different hands than it presently rests. After
all, as Posner would be the first to tell you, “force” is just an expenditure. If
a man is “willing” to pay that price, and the other party is “unwilling” to pay
the price of successful counterforce, we have an “efficient” solution.

One Nobel Prize-winning economist who does not ignore the trans-
action costs of an economic approach to law that elevates efficiency
over all other considerations is James Buchanan. In a perceptive law
review article, he warned the practitioners of both economics and
law that the great benefit which the free market offers society is not
its efficiency or its maximizing of economic value. What the free

203. In complete agreement is Warren G. Nutter, “The Coase Theorem on Social
Cost: A Footnote,” Journal of Law and Economics, XI (Oct. 1968).

204. Leff, “Economic Analysis of Law: Some Realism About Nominalism,”
Virginia Law Review (1974), op. cit., p. 454.
