Bribery and Judgment 789

great national historical figure with the valid accusation of having
been involved in this kind of scandal. Noonan comments: “Francis
Bacon, Samuel Pepys, Warren Hastings are not merely respectable;
they are heroes— respectively the founders, in the view of their ad-
mirers, of British science, the British navy, and British India. Bacon
was a bribee by the law as actually enforced; Pepys a bribee by his
own measure; Hastings a bribee by the law that was being made.
Apologists by the score have hesitated to give their bribetaking its
proper name. As for bribers, judgment has always been even more
charitable, the underlying assumption being that they are the victims
of extortion, When the persons involved have been preeminently
just, judgment has often been entirely suspended. Who thinks of
Thomas Becket or John Quincy Adams as giving bribes?”!”

The Power of the Bribe

The power of the bribe is very great. This verse tells us that wise
men are blinded, and righteous men become perverse through bribes.
The Bible repeats its warning against bribe-taking judges in Deuter-
onomy 16:19, Isaiah 1:23, Amos 5:12, Psalms 26:10, and I Samuel
12:3. It was this sin that Samuel's two evil sons practiced (I Sam.
8:3), and it led to the people of Israel calling for a king (I Sam. 8:5),
which Samuel warned against (I Sam. 8:11-18). The judges’ sin of
bribery led step by step to the call for a stronger, more centralized
civil government. It was difficult for Samuel to take a stand against
the mauguration of the kingship when the judicial failure of his sons
was the occasion of the people’s demand.

Records from the ancient Near East do not indicate any actual
prosecutions for bribe-taking. There are no Mesopotamian exam-
ples yet translated of any official’s being punished for this crime—
this, out of a total 100,000 cuneiform tablets in museums. !* Nothing
in the records of Egypt indicates that any official was prosecuted for
this crime.‘? Nowhere in the ancient Near East was there a specific
civil law against bribery, with punishment specified. This is even
true of the ancient Israel. “Reliance is not on human enforcement
but on divine assistance.”*° But Noonan understands that this is true

17. Ibid., p. xiii.

18. {bid., p. 14, It should be recognized that only a small proportion of these
tablets has been translated. Translators seldom translate as much as 15 percent of ex-
isting Near Easiern tablets,

19. Ibid, p. 12.

20. Idem,
