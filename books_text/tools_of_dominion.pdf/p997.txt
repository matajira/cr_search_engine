Common Grace, Eschatology, and Biblical Law 989

were not half a dozen outspoken Marxist economists teaching in
American universities (and possibly as few as one, Stanford’s Paul
Baran). Since 1965, however, New Left scholars of a Marxist persua-
sion have become a force to be reckoned with in all the social
sciences, including economics.** The skepticism, pessimism, relativ-
ism, and irrelevance of modern “neutral” education have left facul-
ties without an adequate defense against confident, shrill, vociferous
Marxists, primarily young Marxists, who began to appear on the
campuses after 1964. Epistemological rot has left the establishment
campus liberals with little more than tenure to protect them.®
Since 1965, however, Marxism has made more inroads among
the young intellectuals of the industrialized West than at any time
since the 1930s ~an earlier era of pessimism and skepticism about es-
tablished values and traditions. Marxists are successful among
savages, whether in Alrica or at Harvard —epistemological savages.
Marxism offers an alternative to despair. It has the trappings of op-
timism. It has the trappings of Christianity. It is still a nineteenth-
century system, drawing on the intellectual capital of a more Christian
intellectual universe. These trappings of Christian order are the
source of Marxism’s influence in an increasingly relativistic world.

Satan’s Final Rebellion

In the last days of this final era in human history, the satanists
will still have the trappings of Christian order about them. Satan has
to sit on Goes lap, so to speak, in order to slap His face —or try to.
Satan cannot be consistent to his own philosophy of autonomous
order and still be a threat to God, An autonomous order leads to
chaos and impotence. He knows that there is no neutral ground in
philosophy. He knew Adam and Eve would dic spiritually on the day
that they ate the fruit. He is a good enough theologian to know that
there is one God, and he and his host tremble at the thought (James
2:19). When demonic men take seriously his lies about the nature of
reality, they become impotent, sliding off (or nearly off) God’s lap. It
is when satanists realize that Satan’s official philosophy of chaos and
antinomian lawlessness is a ie that they become dangerous. (Marx-

24, Martin Bronfenbrenner, “Radical Economics in America: A 1970 Survey,”
Journal of Economic Literature, VIUL (Sept. 1970).

23. Gary North, “The Epistemological Crisis of American Universities,” in Gary
North (ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspective
(Vallecito, California: Ross House Books, 1976)

  
