1236 TOOLS OF DOMINION

defrauding father-in-law, 654
false accusation, 649, 650
Jesus, 227, 252
kinsman-redeemer, 227-28
life insurance, 273
representative, 252
subordinate, 256-57
Bridenbaugh, Carl, 471, 580
Bride price
Abraham, 252
Calvary, 212n
compulsory, 225
dowry &, 644n
dowry of virgins, 645
formal, 647-57
Jacob, 212
Jesus paid, 270, 274, 276
maidscrvant, 218, 246
mandatory, 252
negotiable, 647-48
negotiated, 648
one per marriage, 651
optional, 270
Old Testament only, 268
retained, 262
screening, 213
screening device, 258-56
servant's debt, 21-12
symbol, 257
virginity, 651-52
whoredom &, 647
see also Dowry
British Israelites, 733, 912
Broker, 593-94
Brothers, 253-55
Brown, John, 1830
Brown, Michael, 573
Brozen, Yale, 696n
Bruce, Dickson, 347n, 348n, 349,
443-44
Brunner, Emil, 2
Bruno, Giordano, 986
Bucer, Martin, 1043n
Buchanan, James
crime, 335n
economics of crime, 1144

free market's benefit, 1154-55
mathematics, 1129
objective cost, 1121
pollution, 1099
public choice economics, 787n.
uncertainty, 1121
Bugliosi, Vincent, 409n
Bukovsky, Vladimir, 45n, 184n
Bull, 461n, 830, 880
Bundle of rights, 513, 517-19, 614,
639-40, 775, 782, 1109
Bureaucracy
economic growth vs., 873
humanism, 55
law &, 107
millennium?, 55-56, 58
pollution &, 584
Soviet Union, 1163-65
Burr, Aaron, 349
Burton, John, 5671.
Bush, George, 719n
Butterworth, Fox, 592n
Buycr of stolen goods, 512-14, 515
Buyers and sellers, 692-93
Bystanders, 345, 382

Gaillois, Roger, 342-43

Cain, 962-63

Calabresi, Guido, 1100-1, 1112-16

Calculation, 1138, 1140 (see also
Measurement)

Caleb, 150

Calf, 881

California, 526n

Calling, 322, 332

Calvary
bride price, 228, 267
forgiveness, 295, 306
mercy, 302

Calvin College, 53

Calvinism, 37, 953, 954, 980, 985

Calvin, John, 2, 7, 82, 368n, 535n,
953

Campbell, Roger, 527n

Canaan, 857, $59, 897, 961-62

Canaanites, 122, 138, 166, 185, 230

Canada, 732n, 1167
