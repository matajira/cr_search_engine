Criminal Law and Restoration 403

tempts at vengeance.®! It is a way of dealing with guilt. In this sense,
it is a means of restoring wholeness to the criminal, too.

Israel’s history can legitimately be classified in terms of a series of
incidents by which this three-fold relationship — repentance, restitu-
tion, and restoration—was illustrated in a covenantal, communal,
and national way. Israel's deliverance from Babylon is a good exam-
ple of this restorative process. It is also illustrated in the instance of
David's adultery and his murder of Uriah the Hittite. David re-
pented (II Sam. 12:13); the child died (12:18), and so did three of his
adult sons—Amnon, Absalom, and Adonijah—thereby making
four-fold restitution on a “four lives for one” basis.® Four-fold resti-
tution was the required payment for the slaughter of a lamb (Ex.
22:1). Nathan the prophet had used the analogy of the slaughtered
ewe lamb in his confrontation with David (II Sam. 12:4). David rec-
ognized that the culprit was worthy of death (v. 5). David therefore
could not escape making the four-fold restitution payment to God's
sense of justice (adultery and murder are both capital crimes in the
Bible). Subsequently, David and Bathsheba were covenantally re-
stored in their marriage, which God testified to publicly by the birth
of Solomon (12:24), who became the lawful heir of David's throne.

We must understand capital punishment as God's required resti-
tution payment. The death penalty is not a means of revenge alone
or deterrence alone. It was imposed on Adam and his heirs, and also
on the second Adam, Jesus Christ. For any civil crime too great to
be compensated for by a monetary restitution payment to the victim,
God requires the civil magistrate to impose the death penalty, God’s
restitution payment. Homicide, for example, could not be paid for
in Israel by anything less rigorous than life for life (Num. 35:31), a
law which is without parallel in the laws of the ancient Near East.®
It was only later rabbinic Judaism that abandoned the principle that
all murderers are subject to the death penalty, in order to reduce the
penalty for Jews who kill resident aliens or gentiles. Maimonides
was quite open about this: “If an Israelite kills a resident alien, he
does not suffer capital punishment at the hands of the court, because
Scripture says, And if a man come presumptuously upon his neighbor (Exod.

61, Laster, iid., p. 75.

62. Herbert Chanan Brichto, “Kin, Cult, Land and Afterlife—A Biblical Gom-
plex,” Hebrav Union College Annual, XLTV (1973), p. 42.

63. Shalom Paul, Book of the Covenant, p. 61.
