256 TOOLS OF DOMINION

writes that “the Hebrew word for bridegroom means ‘the circumcised,’
the Hebrew word for father-in-law means he who performed the operation
of circumcision, and the Hebrew word for mother-in-law is similar.
This obviously had no reference to the actual physical rite, since
Hebrew males were circumcised on the eighth day. What it meant
was that the father-in-law ensured the fact of spirttual circumcision, as
did the mother-in-law, by making sure of the covenantal status of the
groom. It was their duty to prevent 4 mixed marriage. A man could
marry thcir daughter, and become a bridegroom, only when clearly
a man under God.”’

The bride price was also a sign of the bridegroom’s future-orientation
and self-discipline. Because Jacob came without capital into Laban’s
household, he first had to work for Laban as a servant for seven
years in order to prove his capacity to lead his own household. To
lead covenantally, you must first follow. To rule, you must also have
served. Dominion is by covenant, and covenants are always hierar-
chical.* This hierarchical structure of the biblical covenant is, above
all, the message of the Book of Exodus.® Israel was to be visibly
under God’s administration, not Pharaoh’s.

Finally, the bride price was proof of the bridegroom’s lawful sub-
ordination to his own father, under whom he had probably worked
in an agricultural society, or from whom he had received the bride
price as part of his inheritance.

2. Symbol of Subordination

The bride price was an extension of the bridegroom’s productiv-
ity to the girl’s household. The bride price was therefore symbolic of
the son-in-law’s devotion and subordination to her father, as if he were
4 family member, although this was not an actual contract to become a
son who would inherit. The bride price testified to the covenantal rc-
quirements that sons-in-law owe to fathers-in-law. It testificd that
the bridegroom had previously served someone else (probably his
father) productively, and he had amassed capital equivalent to what
could be accumulated during a period of subordination to the father-

 

7, Rushdoony,,Jnstitates, p. 344,

8. Ray R. Sutton, That You May Prosper: Dominion By Covrané (Tyler, Texas: In-
stitute for Christian Economics, 1987), ch. 2.

9. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute
for Christian Economics, 1987), pp. x-xi.

10. Christ’s faithful service to His Father during Elis earthly ministry was the
basis of His ability lo provide a bride price for the church.
