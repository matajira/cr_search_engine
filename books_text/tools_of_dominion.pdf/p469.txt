The Ransom for a Life 461

against human dominion, killing men or other beasts. Under the symbol-
ism of the Old Covenant, such Satanic beasts represent the Satanic nations
(Lev. 20:22-26), for animals are “images” of men. To eat Satanic animals,
under the Old Covenant, was to “eat” the Satanic lifestyle, to “eat” death
and rebellion,

The ox is a clean animal. The heifer and the pre-pubescent bullock have
sweet tempcraments, and can be sacrificed for human sin, for their gentle,
non-violent dispositions reflect the character of Jesus Christ. When the
bullock enters puberty, however, his temperament changes for the worsc.
He becomes ornery, testy, and sometimes downright vicious. Many a man
has lost his life to a goring bull. The change from bullock to bull can be seen as
analogous to the fall of man, at least potentially. If the ox rises up and gores a
man, he becomes unclean, fallen. . . .

The unnaturalness of an animal's killing a man is only highlighted in the
case of a clean, domesticated beast like the ox. Such an ox, by its actions,
becomes unclean, so that its flesh may not be eaten... .

The fact that the animal is stoned indicates that the purpose of the law is
not simply to rid the earth of a dangerous beast. Stoning in the Bible is the
normal means of capital punishment for men. {ts application to the animal
here shows that animals are to be held accountable to some degree for their
actions, It is also a visual sign of what happens when a clean covenant man
rebels against authority and kills men. Stoning is usually understood to rep-
resent the judgment of God, since the Christ is “the rock” and the “stone”
which threatens to fall upon men and destroy them (Matt. 21:44). In line
with this, the community of believers is often likened to stones, used for
building God's Spiritual Temple, and so forth. In stoning, each member of
the community hurls a rock representing himself and his affirmation of
God’s judgment. The principle of stoning, then, affirms that the judgment
is God’s; the application of stoning affirms the community’s assent and par-
ticipation in that judgment,?

Covenantal Hierarchy and Guilty Animals

“But if the ox were wont to push [gore] with his horn in time past,
and it hath been testified to his owner, and he hath not kept him in,
but that he hath killed a man or a woman; the ox shall be stoned,
and his owner also shall be put to death.” The owner had been warned
that the beast was dangerous. (We shall consider in the next section
what constitutes valid evidence of habitual goring.) He had withheld
this information from the victim. How? By refusing to place ade-

7. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
'Lexas: Institute for Christian Economics, 1984), pp. 122-24.
