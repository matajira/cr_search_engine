Pollution, Ownership, and Responsibility 585

currents, breeze-reducing surrounding mountains) impose their en-
vironmental standards on drivers in wide-open Texas or Wyoming?
Why should they lobby for national auto pollution emission stan-
dards that will necessarily reduce the performance and increase the
purchase price of all cars produced in the United States? They are de-
manding a subsidy: lower costs per unit for required poilution-control
equipment (as a result of higher production of regulated cars), but
increased costs for most other drivers whose communities are not in
need of such devices. In this case, national pollution-control legislation is
4 politically acceptable wealth-redistribution scheme. If people in Southern
California want mandatory pollution-control devices for cars regis-
tered in their region, they can vote accordingly. Indeed, given the
vast number of cars in this region, they must vote for emission stan-
dards if they are to improve the quality of the air they breathe. But
they should not insist on a subsidy from car buyers and operators in
other regions of the country.**

Why should those who worry about pollution be allowed to ex-
tract a subsidy from those who do not worry so much? Those who
hate pollution are allowed to move to a less polluted region of the
country, But they prefer to achieve their goal of living in a cleaner
environment at the expense of local factory workers, whose jobs are
“up in the air.” How many factory workers are enthusiastic and
dedicated supporters of the ecology movement, or were in its early
days in the late 1960’s? Aren’t the movement's white-collar sup-
porters better paid, more highly educated (at taxpayers’ expense),
and more mobile than the blue-collar working people whose jobs are
at stake? The leather goods-selling “street people” with university
degrees in sociology were more likely to be at the forefront of the
ecology movement in 1968 than the average employee with General
Motors. As one book points out, “Preliminary studies indicate in fact
the opposite result from that expected by critics; that is, wealthy peo-
ple tend to be lovers of [ecological] purity while the very poor are
more interested in other problems.”

Some readers may think I am exaggerating. Not so, The Sierra
Club is perhaps the most active lobbying organization for ecology in
the world, along with Friends of the Earth. The group took out an
advertisement in Advertising Age magazine, to attract advertisers for

58, To some extent, this principle is honored. Emissions-control standards have
in the past been far more stringent for California than for other states in the U.S.
59. James C. Hite, a al., Heonomics of Environmental Quality, p. 34.
