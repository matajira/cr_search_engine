The Restoration of Biblical Casuistry 51

pret Leviticus 18:18 in Murray’s fashion, the question arises: Why
specify kings as being prohibited from becoming polygamists if the
same law applied to all men anyway? Possibly to prohibit the system
of political covenanting through marriage (Solomon is a good exam-
ple here), Certainly, there is no equally clear-cut Old Testament
prohibition against polygamy comparable to I Timothy 3:2, which
indicates a tightening of the legal requirements for at least church
officers. The New Testament appears to be more rigorous than the
Old in this instance. Another alteration in marriage law that we find
in the New Testament is the abolition of concubinage that resulted
from Christ’s fulfillment of the terms of the Old Testament’s bride
price system (see Chapter 6). There are no more second-class wives.

Dominion Christianity teaches that there are four covenants
under God, meaning four kinds of vows under God: personal (indi-
vidual), and the three institutional covenants: ecclesiastical, civil,
and familial.°° A! other human institutions (business, educational,
charitable, etc.) are to one degree or other under the jurisdiction of
one or more of these four covenants. No single human covenant is
absolute; therefore, no single human institution is all-powerful.
Thus, Christian liberty is liberty under God and God's law, administered
by plural legal authorities.

Biblical Pluralism

There is no doubt that Christianity teaches pluralism, but a very
special kind of pluralism: plural institutions under God's single com-
prehensive law system. It does nof teach a pluralism of law struc-
tures, or a pluralism of moralities, for this sort of hypothetical legal
pluralism (as distinguished from institutional pluralism) is always
either polytheistic or humanistic.5! Christian people are required to
take dominion over the earth by means of all three God-ordained in-
stitutions, not just the church, or just the State, or just the family.
The kingdom of God includes every human institution, and every aspect of life
for all of life is under God and is governed by His unchanging principles. All of
life is under God and God’s law because God intends to judge all of
life. in terms of His law.5

50. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 4.

51. Gary DeMar, Ruler of the Nations: The Biblical Blueprints for Government (Ft.
Worth, Texas: Dominion Press, 1987), ch. 3.

52. Ibid., ch. 4,
