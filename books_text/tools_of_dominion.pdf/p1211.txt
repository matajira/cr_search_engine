Selling the Birthright: The Ratification of the U.S. Constitution 1203

Scottish Enlightenment rationalism. It would take the rise of Dar-
winism and the victory of the North in the Civil War to make clear
the definitive nature of this definitive step toward Rousseau.

The Framers knew that religious oaths were required for testify-
ing in local and state courts. They knew that religious oaths were
sometimes required for exercising the franchise in state elections.
But they made it clear: Federal jurisdiction is governed by another covenant,
and therefore by another god, It is therefore a rival system of hierarchy. It
is not a complementary system of courts; it is rival system, for an cath
to the God of the Bible is prohibited by law in one of those hierarchies.

Rushdoony’s Rewriting of Constitutional History

It is this covenantal fact which Rushdoony, in his 30-year defense
of the Constitution as an implicitly Christian document, has refused
to face. Indeed, he has created a whole mythology regarding the
oath in order to buttress his case. To an audience of Australian
Christians, who could not be expected to be familiar with the U.S.
Constitution, he said in 1983: “In every country where an oath of
office is required, as is required in the United States by the Constitu-
tion, the oath has reference to swearing to almighty God to abide by
His covenant, invoking the cursings and blessings of God for obedi-
ence and disobedience.” But what does the Constitution actually
say? Exactly the opposite: “no religious Test shall ever be required as a
Qualification to any Office or public Trust under the United States.”
To his own American audiotape audience, he insisted: “The Consti-
tution required an oath of office. To us this doesn’t mean much.
Then it meant that you swore to Almighty God and involved all the
curses and blessings of Deuteronomy 28 and Leviticus 26 for obedi-
ence and disobedience. Nobody knows that anymore.”4* Nobody
knew it then, either; it did not happen. Rushdoony has never offered
so much as a footnote supporting such a claim with respect to the U.S.
Constitution. The story is mythical, What he has done is to pretend

44, | am not arguing that this was a self-conscious step toward Rousseau,
Rousseau’s influence in colonial America was minimal, limited mainly to his educa-
tional theories in Fméle. See Paul M. Spyrlin, Rousseau in America, 1760-1809 (Univer-
sity, Alabama: University of Alabama Press, 1969),

45. Rushdoony, The “Atheism” of the Early Church (Blackheath, New South Wales:
Logos Foundation, 1983), p. 77.

46. Rushdoony, question and answer session at the end of his message on Leviticus
8:1:43 (Jan. 30, 1987).
