Feasts and Citizenship 843

things that pertain to this life? If then ye have judgments of things
pertaining to this life, set them to judge who arc least esteemed in the
church” (I Cor. 6:2-4).

Baptism and Civil Authority

Women were not absolutely required to go to these three feasts,
They also did not normally serve as judges, although Deborah did
(Jud. 4). Women were not to be kept away from these feasts, but
they were not under judicial compulsion to attend. This is why the
New Testament represents a major break with the Old Testament.
Females are baptized in the church; therefore, they are required to take commun-
ton. Females (except infants) are not represented by a man —father
or husband —in either baptism or the required ritual feast.

As was the case in the Old Covenant, they are not allowed to be-
come priests, for they cannot lawfully speak judicially in church.
God presents Himself to humanity as a Husband, and thus He can-
not be lawfully represented in His role as the priestly Lawgiver and
sacrificial lamb by women. Women cannot lawfully declare God’s
law in formal church worship ceremonies (I Cor. 14:34-35). In this
sense, women are analogous to all those attendees at the required
feasts of Israel who were also not authorized to become priests.”?

Covenanted women were and still are eligible to become civil
judges in the holy commonwealth. They did and still can lawfully
represent God in declaring His judgments in civil courts. In ancient
Israel, women did not bear the mark of circumcision, but their fathers,
brothers, husbands, and sons did. Women were circumcised repre-
sentalively. Thus, they had lawful access to the feasts, though not as
actual household priests.2* They could lawfully serve as civil judges,

 

23, Women, male children under age 20 (Ex. 30:14), castrated males (Deut.
23:1), plus: circumcised Moabites, Ammonites, and heirs of bastards to the tenth
generation (Deut. 23:2-3), and circumcised Edomites aud Egyptians to the third
generation (Deut. 23:7-8).

24, It might be argued that a widow with no brothers and no adult son wonld
have been allowed to participate in the required feasts as a recipient of the family’s
burned sacrifice. She was clearly the head of her household, and the priestly office
was a household office. She could take a vow that was binding before God without
having to wait a day for her husband or father to confirm it (Num. 30:9). This
points to her position as a household priest. The response to this argument is that
the importance of God’s masculinity outweighs even the importance of the office as
the head of the household. A Levite could have represented the widow at the actual
ritual sacrifice.
