652 TOOLS OF DOMINION

would have already been paid by the seducer, even though her father
had not consented to their marriage. I am arguing that the bride price
awed to the father by the seducer was 50 shekels, the settlement price of a lifetime
male slave in formal judicial disputes (Lev. 27:3).!! This compulsory
bride price should have been passed on to the daughter as her dowry,
but passed on in a specific formal way, as I discuss below: first to the
bridegroom, then back to her father, and then to the girl. If her
father had not collected the money from the seducer, assuming that
he knew of the seduction, he nevertheless owed a dowry to the
daughter; otherwise, she would become a concubine. Without a
dowry from her father, she was a concubine, yet only her father
could pay for it this time; no subsequent bridegroom could be asked
to pay a second bride price for a non-virgin non-widow.

The bridegroom had been forced to pay a bride price to the girl’s
father. Her father had either kept the 50 shekels that had been paid
to him by the seducer, thereby making his daughter a concubine, or,
if he had not collected the 50 shekels, he then owed the 50-shekel
dowry out of his own assets. Again, the bridegroom had believed he
was marrying a free woman who was bringing a dowry to the mar-
riage, not a concubine. He was not legally required to pay the bride
price because of her status as a non-virgin, so the father must have
been required to pay it. Her father had not paid it. The bridegroom
had been forced to pay. This constituted fraud. Although the actual
fraud involved whatever his negotiated bride price payment had
been, for judicial settlement purposes, the fraud was assumed to be
the maximum required formal bride price, and therefore the re-
quired dowry, of 50 shekels.

How Much Had He Actually Paid?

Tam assuming for the sake of argument that the bridegroom was
in fact the victim of a conspiracy between the girl and her family, or
else at least the victim of the girl, who had kept her status a secret
from her father. After the marriage, the bridegroom then decided to
get rid of the wife on the official grounds that she was not a virgin.
He had not been informed of her status, How could he prove this?
Because he had paid the bride price, which would not have been re-
quired of him in the case of a non-virgin; her seducer should have

11, An exception: the owner of a slave killed by a goring ox always was reimbursed
by payment of 30 shekels of silver (Ex. 21:32).
