Beasts and Citizenship 841

tiles, I magnify mine office: If by any means I may provoke to emulation
them which are my flesh, and might save some of them. For if the casting
away of them be the reconciling of the world, what shall the receiving of
them be, but life from the dead? For if the firstfruit be holy, the lump is aiso
holy; and if the root be holy, so are the branches. And if some of the branches
be broken off, and thou, being a wild olive tree, wert graffed in among
them, and with them partakest of the root and fatncss of the olive tree;
Boast not against the branches. But if thou boast, thou bearest not the ront,
but the root thee. Thou wilt say then, The branches were broken off, that I
might be graffed in. Well; because of unbelief they were broken off, and thou
standest by faith. Be not highminded, but fear: For if God spared not the
natural branches, take heed lest he also spare not thee. Behold therefore the
goodness and severity of God: on them which fell, severity; but toward
thee, goodness, if thou continue in his goodness: otherwise thou also shalt
be cut off, And they also, if they abide not still in unbelief, shall be graffed
in; for God is able to graff them in again. For if thou wert cut out of the
olive tree which is wild by nature, and wert graffed contrary to nature into a
good olive tree: how much more shail these, which be the natural branches,
be graffed into their own olive tree? For I would not, brethren, that ye
should be ignorant of this mystery, lest ye should be wise in your own con-
ceits; that blindness in part is happened to Israel, until the fulness of the
Gentiles be come in. And so all Israel shall be saved: as it is written, There
shall come out of Sion the Deliverer, and shall turn away ungodliness from
Jacob: For this is my covenant unto them, when I shall take away their sins
(Rom, 11:11-27).

Next time, however, they will not have to settle for restoration of
their ownership of tiny Israel. As members of the church, they will
inherit the earth. “His soul shall dwell at ease; and his seed shall in-
herit the earth” (Ps. 25:13).

Citizenship by Birth Within the Covenant

On the eighth day, the Hebrew male child was to be circumcised
(Lev. 12:3). This gave him the mark of citizenship. Birth gave him
access to circumcision, and circumcision gave him citizen’s rights.
He could lose his citizenship by violating the terms of the covenant
in specific ways, most notably by refusing to attend the required
festivals. The feasts were ritual acts of covcnant rencwal,' and these
acts of covenant renewal had definite political consequences.

21, Ray R. Sutton, That You May Prosper: Dominion Hy Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), Appendix 8.
