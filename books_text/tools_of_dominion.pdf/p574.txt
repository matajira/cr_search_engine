566 TOOLS OF DOMINION

price than would have been possible, had the producer not been a
polluter, then he has benefitted at the expense of the residents who
have “absorbed” the pollution. The consumer is a participant in the
pollution process, even if he is unaware of the reason why he has
been offered an opportunity to buy a product at a low price. The
consumer has transformed private costs into social costs, for he in
effect “hires” the polluter as his production agent when he makes the
purchase. He provides the seller with money, which in turn encour-
ages the producer to continue producing the product.

Should the consumer be held legally responsible and economic-
ally responsible? No. The consumer must assume that the producer
is violating no laws in anyone’s community, He cannot investigate
every instance of lower-than-normal prices. He must act in terms of
what is presented before him— product and price—and not become
a full-time, one-man investigative agency. He assumes that the local
civil government in the producer's region is serving as the agent of
any injured local victims of pollution. The State should not attempt
to impose on consumers all the producers’ costs of knowledge in
every economic transaction.

If the civil government in the producer’s community steps in and
requires the producer to install pollution-control equipment, and if
the producer then discovers that he is in a position to pass these costs
along to the buyer, at least temporarily, the buyer may begin to shop
for a cheaper substitute. In this sense, the pollution-controt equip-
ment is essentially a éax. Contrary to popular opinion, taxes cannot be
shifted forward to consumers, at least not without risk, precisely because
consumers may begin shopping around for cheaper, untaxed goods.3!
Now, what if some “foreign” producer—in a foreign nation, or a “for-

31. Murray Rothbard is the one economist who has stressed this point. “It is gen-
erally considered that any tax on production or sales increases the cost of production
and therefore is passed on as an increase in price to the consumer. Prices, however,
are never determined by costs of production, but rather the reverse is true. The
price of a good is determined by its total stock in existence and the demand schedule
for it on the market, But the demand schedule is not affected at all by the tax. The
selling price is set by any firm at the maximum net revenue point, and any higher
price, given the demand schedule, will simply decrease net revenue. A tax, there-
fore, cannot be passed on to the consumer. It is true that a tax can be shifted forward,
in a sense, if the tax causes the supply of the good to decrease, and therefore the
price to rise on the market, This can hardly be called shifting fer se, however, for
shifting implies that the tax is passed on with little or no trouble to the producer.”
Rothbard, Power and Market: Government and the Economy (Menlo Park, California: In-
stitute for Humane Studies, 1970), p. 67.
