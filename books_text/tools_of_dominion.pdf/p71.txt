2
WHAT IS COVENANT LAW?

Behold, I have taught you statutes and judgments, even as the LORD
my God commanded me, that ye should do so in the land whither ye go to
possess it, Keep therefore and do them; for this is pour wisdom and your
understanding in the sight of the nations, which shall hear all these stat-
utes, and say, Surely this great nation is a wise and understanding preo-
ple. For what nation 1s there so great, who hath God so nigh unto them,
as the LORD our God is in all things that we call upon him for? And
what nation is there so great, that hath statutes and judgments so right-
eous as all this law, which I set before you this day? (Deut. 4:5-8).

These verses teach clearly that the law of God is a tool of world-
wide evangelism. The nations of the earth will recognize the justice
that is provided by God’s revealed Jaw, as well as see the external
blessings that inevitably come to any society that covenants itself to
God, and subsequently adheres to the ethical terms of Ged’s cove-
nant. It is crucially important to maintain that these blessings will be
visible (Deut. 28:1-14). The Bible is insistent: there is an inescapable
cause-and-effect relationship between national covenantal faithfulness and na-
tional prosperity. Adherence to biblical law inevitably produces visible
results that are universally regarded as beneficial. Why do covenant-
breakers recognize this? Because all men have the work of God’s law
written on their hearts (Rom, 2:14-15), so they can and do perceive
the blessings of God. This, God promised, would be the visible sign
of Israel’s wisdom, visible to the ends of the earth.

Tt is not remarkable that humanists deny the existence of this
covenantal and historical cause-and-effect relationship, for such a re-
lationship points beyond history to the existence of a sovereign
Creator and Judge who will hold them eternally responsible on judg-
ment day. They hold back the truth in unrighteousness (Rom. 1:18).
What is remarkable, however, is that this view of revealed biblical

63
