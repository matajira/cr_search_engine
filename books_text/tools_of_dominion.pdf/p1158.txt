1150 TOOLS OF DOMINION

the value of total production. “This question can be resolved by consid-
ering what would happen to the value of total production if it were
decided to exempt the railway from liability from fire-damage. . . .”195
Coase argues that it might he better for society in general if the farmer’s
property rights are ignored, leaving him free to pay the railroad
company sufficient money to install the spark-arrester. After all, the
value of the crop may be greater than the cost of the spark-arrester,1%°

What if the farmer had worked for years to build up the soil or
build his family’s dream home? This labor was unquestionably a
manifestation of the dominion covenant. Perhaps he dimly under-
stood that his labor to build the house was in a unique way a moral
act under God, meaning his personal conformity to God’s injunction
to subdue the earth to His glory. His home is not simply a manifesta-
tion of his technical competence as a builder; it may also be a mani-
festation of his self-conscious fulfillment of the dominion covenant.
In other words, this house may be in a very real sense a holy thing—a
thing set apart for God by the very act of constructing it. This is why
people are sometimes “irrationally” committed to a piece of ground.
A spark-emitting train is threatening his home’s existence, meaning
the work of his hands, meaning his dream or vision. Is he entitled to
no compensation? Isn’t the railway always liable for damages? Fur-
thermore, if the court decides that the railway is liable and Coase
denies that the court should automatically decide that it is—is the
man’s shattered dream worth only monetary compensation for the
market value of his crops? Maybe he resents the fact that the railway
is reducing to mere dollars his right to safety from fire, and market-
determined dollars at that? Shouldn’t the engines be fitted with a
spark retardcr, by law? Aficr all, this is not an accidental, occasional
incident; this is a daily threat of fire that is a statistically probable
event because of the technology involved in running the trains. In

195. ibid. p. 33.

196. Clearly, the damage inflicted on the crops planted close to the tracks by nu-
merous farmers could be high, The costs would be high to organize the farmers
together in order to contribute money to finance the installation of the spark ar-
rester, Each farmer would tend to wail for the others to put up the money. Each
would prefer to become a “free rider” in the transaction: paying nothing, but bene-
fitting from the spark arrester. The payment to the railroad firm probably would not
be made apart from intervention by the civil government to compel all farmers who
are benefitted by the spark arrester to pay their proportional share. The civil govern-
ment eventually must decide who pays whom: the railroad firm paying damages to
the farmers, ar the farmers paying “protection money” to the railroad company.
