282 TAOLS OF DOMINION

Capital Punishment: Yesterday and Today

One of the complaints against the continuing legitimacy of bibli-
cal law is that the death penalty is too rigorous to be applied as a
sanction against most of the capital crimes specified by the Old Tes-
tament. Therefore, conclude the Mosaic law’s critics, execution is no
longer a valid civil sanction today, except in the case of murder.’
This line of argumentation leads to the peculiar conclusion that in
the Old Covenant era, covenantally faithful people were expected by
God to be a lot more rigorous about prosecuting criminals, and were
therefore expected to be more willing to see God’s civil sanctions en-
forced. This rigorous “Old Testament attitude” toward criminals is
no longer valid, it is said, because of the coming of the New Cove-
nant. But if Christians are to be less rigorous regarding crime and its
appropriate civil sanctions, then Ged also must have adopted a morc
lenient attitude, which is supposedly reflected in His New Covenant
law. A major problem with this line of reasoning is the fact that
God’s New Covenant standards seem to be more rigorous, e.g., the
prohibition of easy divorce (Matt. 19:7-9).8 With greater maturity
and greater revelation, Christians are supposed to be less lenient
about sin. After all, more is expected from him to whom more has
been given (Luke 12:47-48), The New Testament gives Christians
greater revelation and assigns us far more responsibility than was
the case in the Old Covenant era. Christ’s resurrection is behind us.
The Holy Spirit has come.

Tt could be argued, of course, that because greater mercy has been
shown to us, we should extend greater mercy. With respect to the ju-
dicial principle of victim’s rights, I quite agree. The victim should be
more merciful, so long as his mercy does not subsidize further evil.
He must judge the character of the criminal. But this does not answer
the question of designated capital crimes. Is it the State’s responsibility
to adopt the principle of reduced New Covenant sanctions, despite
the explicit revelation of the Old Covenant case laws? Should the
State adopt a judicial principle different from that which prevailed in
the Old Covenant? I answer no. Furthermore, I also answer that
civil judges in Old Covenant Israel had the God-given authority to
reduce the severity of the specified sanctions under certain circum-
stances. I develop the evidence for this conclusion in this chapter.

7. For example, see John Murray, Principles of Conduct (Grand Rapids, Michigan:
Eerdmans, 1957), p. 118.
8. See below: “Divorce by Covenantal Death,” pp. 289-90.
