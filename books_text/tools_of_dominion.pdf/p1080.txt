1072 TOOLS OF DOMINION

that their debunking operation somehow elevates our view of the
Bible. For example, the internationally respected (unfortunately) Bible
scholar G. Ernest Wright and his co-author argue that in the Bible,
“What is important is what this great Lord has done.”?5 But as soon
as anyone raises the obvious question, “What exactly has God done?”
the authors run for the cover of symbolism and supposed myth, in
order to escape the Bible’s detailed account of what God has done:

This furnishes a clue to our understanding of the prehistoric material
preserved in Genesis 1-11. These traditions go far back into the dim and un-
recoverable history of Israel; they are the popular traditions of a people,
traditions which in part go back to a pre-Ganaanite and North Mesopotamian
background, For this reason there is little question of objective history here.
We are instead faced with the question of why the old traditions were written
down. What was the purpose of the writers who preserved them for us??*

Notice the shift in their argument. They tell us on the one hand
that the Bible is a historical book, unique in the ancient world. The
Bible’s view of God resis squarely on what God has done in history.
But when the key chapters that describe the creation of the universe
and the Fall of man are brought up, as well as the Noachic flood and
the tower of Babel, the authors immediately shift their focus away
from what the Bible says about God; they shift their concern to what
the Hebrews came later to believe about God. Their focus shifts from
God to man. This is the essence of humanism. The fact is, their
focus began with man rather than God—autonomous man.

‘The humanist scholar insists that we cannot deal with God, who
is not an objective fact of history that can be studied. We can only
deal with men’s recorded thoughts about God, which are objective facts of
history that can be studied. Van ‘Vil has summarized this humanistic
impulse: “Men hope to find in a study of the religious consciousness
something that has never been found before. They hope to find out
what religion really is. The claim is made that now for the first time
religion is really being studied from the inside.”?? Man's religious
consciousness becomes determinative in history, not the acts of God.
Wright and Fuller should have titled their book, The Book of the Sur-

25. G. Emest Wright and Reginald H, Fuller, The Book of the Acts of God: Christian
Scholarship Interprets the Bible (Garden City, New York: Doubleday, 1957), p. 36.

26. ibid., p. 24.

27. Cornelius Van Til, Psychology of Religion, vol. IV of In Defense of Biblical Christi-
anity (Phillipsburg, New Jersey: Presbyterian & Reformed, 1971), p. 7.
