70 TOOLS OF DOMINION

consist” (Col. 1:16-17). Nothing lies outside the sovereign providence
of God. There is no area of contingency. There is no area of neutral-
ity. There is no area that is outside the eternal decree of God or the
law of God. This is the biblical doctrine of creation. Humanists hate
it, and so do the vast majority of Christians today.

God as Creator brings all things to pass. When He says, “It shali
come to pass,” it comes to pass. “Declaring the end from the begin-
ning, and from ancient times [the things] that are not [yet] done,
saying, My counsel shal] stand, and I will do all my pleasure” (Isa.
46:10). God does not simply know the future that He predicts; He
causes the future to take place, There is no element of chance any-
where in the universe.

Consider the greatest crime in history: the betrayal and crucifix-
ion of Jesus Christ. The act of betrayal by Judas was predetermined
by God; nevertheless, Judas was still held fully responsible for this
act. “And truly the Son of man goeth, as it was determined: but woe
unto that man by whom he is betrayed!” (Luke 22:22), And what of
those who unlawfully, defiantly condemned Jesus Christ to death?
They were all predestined by God to do it.

‘The kings of the earth stood up, and the rulers were gathered together
against the Lord, and against his Christ. For of a truth against thy holy
child Jesus, whom thou hast anointed, both Herod, and Pontius Pilate,
with the Gentiles, and the people of Israel, were gathered together. For to
do whatsoever thy hand and thy counsel determined before to be done (Acts
4:26-28).

So, the Bible teaches man’s personal responsibility and God’s ab-
solute predestination. If God was willing to predestinate the greatest
crime in history, holding the criminals fully responsible, then surely
He is willing to bring to pass all the other relatively minor crimes in
history, also holding each criminal responsible. God’s law touches
everything, and each man is fully responsible for his thoughts and
actions; he must obey the whole of God’s law.

God did not create the world and then depart, leaving it to run
by itself until the final judgment (Deism’s god), He is present every-
where, but specially present with His people. He delivers them. But
He also gives His law to them. He runs everything, yet men are
made in His image, and they have the ability to understand the ex-
ternal world. They are responsible to God because God is totally
sovereign. He has laid down the law, both moral and physical. His
