The Epistemological Problem of Social Cost 1159

It is a most common experience in law schools to have someone say, of some
action or state of events, “how awful,” with the clear implication that revers-
ing it will de-awfulize the world to the full extent of the initial awfulness.
But the true situation, of course, is that eliminating the “bad” statc of affairs
will not lead to the opposite of that bad state, but to a third state, neither the
bad nor its opposite. ‘That is, before agreeing with any “how awful” critic,
one must always ask him the really nasty question, “compared to what?”
Moreover, it should be, but often is not, apparent to everyone that the proc-
ess of moving the world from one state to another is itself costly. If one were
not doing #at with those resources (money, energy, attention), one could be
doing something else, perhaps righting a few different wrongs, a separate
pile of “how ghastly’s.”2

Coase himself has admitted as much, though he confines this
admission to the narrow confines of the question of transaction costs,
“Since property rights can be changed in such a way as to raise as
well as lower the costs of transactions, how can one say that a move
from regulation to 4 private property rights system, the use of the
market, will necessarily represent an improvement? If the question
is put in such a general form, one cannot say that it will.”21*

Christian economists must therefore enter the debate regarding
costs, whether social or personal. There is no intellectually consist-
ent way that the humanist economist can legitimately keep Christian
economics out of the arena. He has adopted a position of intuitional
and arbitrary ethics in the name of value-free methodology. It is all a
sham. The more loudly the economist insists that ethics should be
left outside the temple of economics, almost as one leaves one’s shoes
outside a Moslem mosque, the more irrelevant his findings and con-
cealed his own systems ethics, It is better to be open about one’s eth-
ics, and the source of one’s ethics. The reduction of self-deception is
clearly a legitimate intellectual end. The problem is, neither the em-
barrassed Christian economist nor the self-deceived humanist econ-
omist is willing to pay the methodological price. But we should have
expected this; it is an ancient problem: “Beware lest any man spoil
you through philosophy and vain deceit, after the tradition of men,
after the rudiments of the world, and not after Christ” (Col. 2:7).

23. Tbid., p. 460.
214. Coase, "The Choice of the Institutional Framework: A Comment,” Journal of
Law and Economics, XVIL (October 1974), p. 493.
