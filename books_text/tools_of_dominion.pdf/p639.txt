Safekeeping, Liability, and Crime Prevention 631

The principle of ownership does not change in the case of the
stolen ox or sheep. If the thief had stolen and killed or sold a sheep or
an ox, and therefore must make a five-fold or four-fold restitution
payment, the safckeeper receives the total restitution payment. He
has become the victim, not the original owner, who has been com-
pensated by the safekeeper; therefore, the safekeeper should receive
the four-fold or five-fold restitution payment.

Conclusion

Accepting the responsibilities associated with safekeeping is a
voluntary act which affirms the existence of covenantal bonds, There
are judicial bonds with the neighbor, with the community, and with
God. By acting as a steward of another man’s property, the safe-
keeper becomes an agent of the neighbor, the community, and God.
He must do his best to keep thicves at bay. He is not responsible for
every possible loss that might befall these entrusted goods, but he is
responsible to see that thieves do not break in and take them, He is
responsible up to the value of the stolen goods, beast for beast, good
for good.

The neighbor who is a thief jeopardizes covenantal social order.
He is to be brought before the judges by the victim. This passage re-
fers exclusively to criminal behavior. This is why double restitution
is imposed in each case. Double restitution is biblical law’s sanction
against criminal intent: an additional restitution payment is imposed
that is equal to the gross return that the thief hoped to gain from the
transgression. If the case comes to trial, the accused must take an
oath before God and the court that he is innocent. The thief takes
great risks in giving a false oath. A false oath involves him in a second
theft: the attempt to avoid paying the victim his lawful restitution. If
he later admits his false oath, he will have to make an additional
payment of 20 percent of the required double restitution to the vie~
tim, plus a trespass offering to the church. If he never admits it, and
his false oath is subsequently proven in court, he will have to make
quadruple restitution to the victim, or six-fold restitution, plus at
least a double trespass offering to the church.

The judicial principle here is that there are progressively greater sanc-
tions as criminal behavior escalates. Confession before each stage of re-
bellion brings with it reduced penalties.
