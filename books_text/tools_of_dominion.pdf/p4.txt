Copyright © 1990
Gary North

Published by
The Institute for Christian Economics,
P.O. Box 8000, Tyler, ‘Vexas 75711

‘Dpesetting by Thoburn Press,
P.O, Box 2459, Reston, Virginia 22090

 

Library of Congress Cataloging-in-Publication Data

North, Gary.
Tools of Dominion : the case laws of Exodus / Gary North.
. om,

Includes bibliographical references.

ISBN 0-930464-10-9 (alk. paper) : $29.95

1, Bible. O.'T. Exodus XXI-XL—Commentaries.
2. Dominion theology. 3. Economics in the Bible. 4. Jewish
law, 5, Economics— Religious aspecis— Christianity. 6. Law
(Theology) 7. Church and state — Biblical teaching.
L, Institute for Christian Economics. II. Title.
BS1245.3.N59 1990
261.8'5 —de20 90-4054

cIrPp

 

 
