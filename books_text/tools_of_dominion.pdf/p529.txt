Proportional Restitution 521

men as helpless dependents. The twenty-third psalm makes use of
the imagery of the shepherd and sheep. David, a shepherd, com-
pares himself to a sheep, for God is described as his shepherd (Ps.
23:1). Christ called Himself the “good shepherd” who gives His life
for His sheep (John 10:11), He said to His disciples on the night of
His capture by the authorities, citing Zechariah 13:7, “Ail ye shall be
offended because of me this night: for it is written, I will smite the
shepherd, and the sheep of the flock shall be scattered abroad”
(Matt. 26:31). He referred to the Jews as “the lost sheep of the house
of Israel” (Matt. 10:6), echoing Jeremiah, “Israel is ascattered sheep”
(Jer. 50:17a) and Ezekiel, “And they were scattered, because there is
no shepherd: and they became meat to all the beasts of the field,
when they were scattered” (Ezk. 34:5). Christ spoke of children as
sheep, and offered the analogy of the man who loses one sheep out of
a hundred. The man searches diligently to locate that one lost sheep
and rejoices if he finds it. “Even so it is not the will of your Father
which is in heaven, that one of these little ones should perish” (Matt.
18:14).

It is thus the helplessness of sheep rather than their value as beasts
of burden or dominion that makes four-fold restitution mandatory.
Shepherds regard sheep as their special responsibility. The position
of sheep is therefore unique. Sheep are representative of the utter helpless-
ness of men. An attack on the sheep under a man’s control strikes at
his position as a covenantally responsible steward. David risked his
life to save a lamb (or perhaps lambs) captured by a bear and a lion,
and he slew them both (I Sam. 17:34-36), taking the lamb, appar-
ently unharmed, out of the mouth of the lion: “I caught him by his
beard” (v, 35). As God had delivered him out of the paw of both lion
and bear, David told Saul, so would He deliver him out of the hand
of Goliath (v. 37). Again, David was comparing himself (and Israel)
with the lamb, and comparing God with the shepherd. Thus, the
recovery of a specific lost or stolen sheep is important to a faithful
shepherd or owner, not just a replacement animal.

Perhaps the best example of sheep as a symbol for defenseless
humans is found in Nathan’s confrontation with King David con-

33. Maimonides ignored all this when he insisted that if a thief “butchers or sclls
on the owner's premises (an aniraal stolen there), he need not pay fourfold or five-
fold, But if he lifts the object up, he is liable for theft even before he removes it from
the owner’s premises. Thus, if one steals a lamb from a fold and it dies on the
owner's premises while he is pulling it away, he is exempt. But if he picks it up, or
takes it off the owner’s premises and it then dies, he is liable.” Maimonides, Torts,
“Laws Concerning Thelt,” Chapter Two, Section Sixteen, p. 67.
