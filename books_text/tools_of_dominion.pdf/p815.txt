Bribery and Judgment 807

bids by potential consumers. To become a consumer, you must first
become a producer, unless you are being supported by your inherit-
ance, or by charity, or by the privately wielded sword (criminals), or
by the civil government's sword (welfare fecipients). The market
steadily pressures participants to become productive because it is
governed by the principle of “highest bid wins”—bids usually reg-
istered by money, but at least in goods or services (barter). The mar-
ket also pressures people to become future-oriented. They have to
earn money through personal productivity in order to make future
purchases.

If the principle of “highest money bid wins” is abandoned, then
the economic system becomes intensely present-oriented. People
would look only to their present assets as the basis of their ability to
buy what they want. They would be able to buy things by becoming
poorer. If they can reduce their net worth sufficiently, and can
squeeze their living standards down to the near-starvation level,
they can buy their one dream item for practically no money, just as
long as the purchase price absorbs a very high percentage of their
assets. They sacrifice nearly everything they own, once, in order to
make that one dream purchase. Attaining their dream impoverishes
them. If this is not a form of implicit idol-worship, what is? The
principle of the “widow’s mite,” which is appropriate for sacrificial
giving, becomes a means of personal and cultural idolatry when it be-
comes sacrificial buying.

Another very efficient and pleasant way to reduce your net worth
is to go deeply into debt for consumer goods and services that de-
preciate faster than the obligation is reduced. This, too, is counter-
productive. It is a decision based on a deeply entrenched present-
orientation.

The Sellers’ Dilemma

We have been speaking of buyers. What about sellers? Consider
the car salesman. He sells cars, but he also buys cars. How would he
be able to order a replacement car for every car sold? Only by offer-
ing the highest percentage of his dealership’s asscts. Small, struggl-
ing, very high-risk dealerships that order a very small number of
cars could get delivery precisely because they have so few cars in in-
ventory —i.e., so little net worth. Obviously, the number of auto-
mobile orders would drop as small, struggling dealers became the
legally competitive bidders. Fewer orders would lower the factories’
