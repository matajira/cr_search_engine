346 TOOLS OF DOMINION

An injured loser who walks again is entitled to full compensation.
But in the case where the loser dies, the judges are required to im-
pose a capital sentence on a surviving fighter. When the loser cannot
“walk abroad,” the victor must not be “quit.” At best, he would have
to pay an enormous fine to the family of the dead man, but even this
would seem to be too lenient, since the only instance of a substitution
of payment for the death sentence involves criminal negligence ~the
failure to contain a dangerous beast which subsequently kills a man —
but not willful violence (Ex. 21:29-30). The autonomous shedding of
man’s blood, even ta “defend one’s good name,” is still murder.
There is the perverse lure of such “conflicts of honor.”

It is clear that if a biblically honorable man refuses to fight be-
cause the civil law supports his position by threatening him with
death should he successfully kill his opponent, he can avoid the fight
in the name of personal self-confidence. He says, in effect, “I know I
can probably kill you; therefore, I choose not to enter this fight be-
cause I will surely be executed after I kill you.” Thus, he can avoid
being regarded as a coward. This breaks the central social hold that
the code duello has always possessed: the honorable man’s fear of
being labeled a coward. But in order to deflect this powerful hold,
the State must be willing to enforce the death penalty on victors.

The Duel in American History

The enforcement of legal prohibitions against private duels is
basic to the culture of the industrial and post-industrial (i.e., service-
oriented) West. The duel is based upon essentially pre-industrial
concepts of personal honor and personal pride. In the years from
the American Revolution until 1800, the duel was a familiar though
illegal activity in both North (outside of Puritan New England)!®
and South. Two important facts are not generally recognized by

15. Mare Bloch writes of late medieval society: “A theory at that time very widely
current represented the human community as being divided into three ‘orders’:
those who prayed, those who fought, and those who worked, It was unanimously
agreed that the second should be placed uruch higher than the third, But the evi-
dence of the epic goes farther still, showing that the soldier had little hesitation in
rating his mission even higher than that of the specialist in prayer. Pride is one of the
essential ingredients of all class-consciousness. ‘That of the ‘nobles’ of the feudal era
was, above all, the pride of the warrior.” Bloch, Feudal Society (University of Chicago
Press, [1961] 1965), pp. 291-92

16. Richard Buel, Jr., Securing the Revolution: Ideology in American Politics, 1789-1815
(Ithaca, New York: Gorell University Press, 1972), pp. 80-81.

  
