90 TOOLS OF DOMINION

This being our position, any attempt to refute the theonomic po-
sition by arguing that the Old Testament case laws were intended by
God to apply only to Old Testament Israel misses a key theological
point: God's revealed law was resurrected to newness of life with Jesus, Old
Testament law, mediated and restored through Jesus Christ and
preached by His church, has in New Testament times become judi-
cially obligatory nationally on a worldwide basis. All nations will be
judged finally in terms of God’s law, as Jesus warned: “And before
him shall be gathered all nations: and he shall separate them one
from another, as a shepherd divideth his sheep from the goats”
(Matt, 25:32), This means that the biblical case laws are now judi-
cially obligatory for the nations, for where there is no binding law,
there can be no valid sanctions.

Biblical Law and Civilization

Though it may seem strange to introduce the problems dealt
with in this chapter with an extended citation from an Orthodox
Jewish scholar, I have decided to do so anyway. Rarely has any com-
mentator better understood the importance of biblical law for the full
flowering of society than I. Grunfeld, the translator of Samson
Raphael Hirsch’s remarkable study, Horeb (1837). Grunfeld wrote
in 1962:

Indeed, the leaders of Christian opinion in Europe, and their Jewish
imitators, conscious or unconscious, have often ‘hit the law of Moses with
their fists’, but it seems that in doing so they have done more harm to Euro-
pean civilization than to the law of Moses.

The separation of law and religion has proved to be one of the greatest
disasters in the history of human civilization. It has done untold harm to
law and religion alike. It has robbed law of its sacred character and thereby
of its strongest moral incentive; it has deprived religion of its legal element
and, with that, of its influence over the greatest social movements of our
time. Law alone can be the regulator of organized human life, The rejec-
tion of law as a religious discipline means, therefore, of necessity, the flight
of religion from the world and its realities, a denial of the value of life anda
state of detachment and capitulation on the part of religion. Hence origi-
nates the deplorably small influence which organized religion has wielded
in the daily affairs of life, especially in its social and economic spheres,
where religious activity should be at least as predominant as in the sphere of
faith and morals, This aloofness of organized religion from the problems
and difficulties of social life has alienated the best and noblest spirits among
the social reformers and has paralysed the influence which organized reli-
