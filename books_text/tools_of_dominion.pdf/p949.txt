Conclusion 941

to every verse in the Old Testament regarding keeping holy the sab-
bath except one, Exodus 35:2: “Six days shall work be done, but on
the seventh day there shall be to you an holy day, a sabbath of rest
to the Lorp: whosoever doeth work therein shall be put to death,”
When it comes to announcing the legitimate imposition of this most
rigorous of Old Testament civil sanctions, capital punishment, the
church flees in holy terror.

A biblically required sanction clearly identifies God’s attitude to-
ward a particular infraction. The severity of the sanction tells us just
how important the infraction is in the overall operation of the king-
dom of God. Without sanctions, there can be no civil law, and with-
out civil law there can be no civilization, meaning no identifiable
kingdom. But there is always some form of civilization. There are no
historical vacuums. Thus, we ought to conclude that God has His
required sanctions, while self-proclaimed autonomous man has his.
God has revealed His required sanctions in His law; man has re-
vealed his required sanctions in his legislation. For as long as there
are infractions of a judicial standard, there will be sanctions. The
question is: Whose? Whose standards and whose sanctions?

The church has not wanted to face the stark contrast between the
two kingdoms. It has wanted to find some rationally acceptable posi-
tion between theocracy and tyranny and also between theocracy and
anarchy. Christian scholars have asserted the existence of neutral,
“natural” laws that can serve as the church’s earthly hope of the ages,
an agreeable middle way that will mitigate the conflict in history be-
tween the Kingdom of God and the kingdom of man. The victor in
such a naive quest will always be the kingdom of man. Theoretical
neutrality means practical autonomy: men do not have to consider what

 

God requires or threatens in history.

God brings His sanctions in history, both positive and negative.
He can do this either through His people, who act representatively
as His agents, or through pagan armies or seemingly impersonal
environmental forces. He can choose war, pestilence, or famine. He
can even choose “all of the above.” But He does bring His sanctions
in history. There is no escape from these historical sanctions, any
more than there is an escape from His eternal sanctions. The former
point to the latter. This is one of the primary functions of historical
sanctions: as a witness to the holiness of Gad.

God’s historical sanctions serve as public evidence of His
theocratic sovereignty over the creation. This is why Christians who
