544 TOOLS OF DOMINION

have so corroded the railway tracks that the trains are not allowed to
go over 25 miles per hour.’ Think of the workers in Cracow’s steel
plant, where in 1980, 80 percent of those leaving the plant received
disability payments, and 7.5 percent died while still employed.§ The
problem is inherent in the State’s ownership of the means of produc-
tion; the means of production necessarily must include the workers.
The State owns their labor. Ultimately, the radical socialist and
Communist States assert actual ownership of the workers, disposing
of them however the bureaucrats see fit. It is “common ownership” —
bureaucratic ownership — which creates most of the economic incen-
tives to pollute and exploit the environment, because leaders within
the civil government’s hierarchy become the unnoticed beneficiaries
of the increased output of lower-cost industrial processes that pro-
duce the pollution. The plant managers meet their State-assigned
output quotas less expensively (for their local plants) by transferring
some of the costs of production to the public: smoke, noise, chemical
wastes, etc. Politically acceptable solutions to widespread pollution
have never been successfully implemented in socialist societies because
it is the private ownership of the means of production that serves as a
key element in any successful program of pollution control.”

At the same time, free market economists have not been able to
produce theoretically acceptable solutions to the problem of pollution
that do not rest heavily on the idea of the necessity of government in-
tervention into market operations. The problem then becomes: How
much intervention is appropriate in any given case? There is no the-
oretically acceptable answer to this problem. In fact, because of the
very nature of modern economic theory, there never will be a theo-
retically acceptable solution that is consistent with contemporary
economics. I have added Appendix D to prove this assertion. It
deals with the crucial, neglected, and somewhat technical problem:
determining social cost. I have added it in order to demonstrate that
conventional humanist economic theory is incapable of dealing with
the problem of pollution — or any problem of applied economics, for
that matter—because there is no self-consistent way for the econo-
mist to go from modern economics’ methodological individualism to
collective decision-making in terms of the presuppositions of modern

5. Lloyd Timberlake, “Poland—the most polluted country in the world?” New
Statesman (22 October 1981), p. 248,

6. [bid., p. 249.

7. See Appendix E: “Pollution in the Soviet Union.”
