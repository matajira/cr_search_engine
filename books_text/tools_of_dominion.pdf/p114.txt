106 TOOLS OF DOMINION

did not contribute to this ‘transcendent’ end was not considered for
inclusion, no matter how fundamental it was to the pursuit of daily
life. As a consequence norms and regulations governing trade, prop-
erty, the crafts, family law, and the like —the institutions that consti-
tute the very fabric of daily life —were of little concern to the biblical
authors and redactors.”

I would add a third aspect of this structure of biblical revelation:
the intention of God to provide a covenant document for all of
human history rather than a temporary law code for professional use
by Hebrew lawyers. The whole of God’s revealed law had to be read
every seventh year to all the residents of Israel (Deut. 31:10-13). Law
had to be understandable by them. God’s law was (and is) to be en-
forced primarily by self-government under God, not by formal agencies
of government, whether civil or ecclesiastical. The law was (and is)
to be dived, not broken into minute technical parts and rolled up in
scrolls on lawyers’ shelves. The inescapable technical and profes-
sional disputes of lawyers were peripheral to the fundamental point:
the restoration of the broken covenant of Adam. God’s revealed law was
given to serve as a guide for the restoration of God’s mandated king-
dom, meaning the earthly, historical manifestation of God’s cosmic
civilization—“in earth, as it is in heaven.” This meant (and still
means) the restoration of God's law-order.*? It was this law-order,
not the technical terms of professional disputation within an elite
guild of lawyers, that was the focus of concern in the Old
Testament’s texts relating to biblical law.

Formal Law and Ethics

Biblical law gives us God’s fixed ethical standards. It also gives us
warning: there will be a final judgment, eternal in its effects, awe-
some in its magnitude, and perfect in its casuistry. All the facts will
be judged by all the law. Yet there is little said about how this final
courtroom drama will be conducted. Any discussion of the technical
details of God’s formal legal procedure is irrelevant. We know only
that there will be at least three witnesses against us, violation by vio-
lation: Father, Son, and Holy Ghost.

46. Ibid., p. 42

47, Rushdoony, {nstitutes, p. 12.

48. Gary North, “Witnesses and Judges,” Biblical Economics Today, VI (Aug./Sept.
1983). Reprinted as Appendix E in North, Dominion Covenant: Genesis (1987).
