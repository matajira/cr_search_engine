788 TOOLS OF DOMINION

and rent-seeking socialists, Communists, fascists, Keyenesian inter-
ventionists, and lifetime bureaucrats whose main goal in life is to
expand their ability to tell other people what to do.!1 Long-term eco-
nomic monopolics, ancient and modern, have almost always been
the creation of civil governments.”

Bribes

Why do judges become allies of economic oppressors, thereby
making possible continuing oppression? This verse tells us why.
Citizens take a portion of their capital and “invest” it. They bribe a
court officer to render unrighteous judgment or to look the other way
and refuse to prosecute unrighteous public behavior. This verse
focuses on direct bribes, but the principle of bribery goes beyond a
direct payoff to a personally corrupt official. Bribes can come in
many forms, including promises for financial or other support dur-
ing the next election.'? Roman Catholic moralist and legal scholar
John Noonan’s massive and brilliant book, Bribes, lists two pages of
bribe prices in history: gold, cash, percentages, etc.!* Even the defi-
nitions of what constitutes a bribe vary widely. Noonan lists four
sources of the possible definitions of bribery, “that of the more ad-
vanced moralists; that of the Jaw as written; that of the Jaw as in any
degree enforced; that of common practice. If one is to say that an act
of bribery has been committed, one should know which standard one
is using.”45 But whatever the definition, in whatever society, bribes
are officially disapproved.'©

This disapproval causes problems for explaining a nation’s his-
tory. We forget or ignore the fact that some of our greatest heroes
have been bribees or bribers. Societies prefer to avoid accusing some

11. This is not to say that big business has not also remained the beneficiary of the
politicians. Big business has itself become the subsidized ally of socialists, Commu-
nists, fascists, and Keynesians. On this point, see Gabriel Kolko, The Triumph of
Conservatism (New York: Free Press of Glencoe, 1963); Carroll Quigley, Tragedy and
Hope: A History of the World in Our Time (New York: Macmillan, 1966), especially
Chapter 17.

12. D. T. Armentano, Antitrust and Monopoly: Anatomy of a Policy Failure (New York:
Wiley, 1982); Walter Adams and Horace M. Gray, Monopoly in America: The Govert-
ment as Promoter (New York: Macmillan, 1955); Mary Bennet Peterson, The Regulated
Consumer (Ottawa, Ilinois: Green Hill, 1971).

13, John T. Noonan, Bribes (New York: Macmillan, 1984), pp. xxi-xii.

14. ‘Tbid., Appendix.

15. Tbid., p. xii.

16, Tbid., p. xx.

 
