Pollution, Ownership, and Responsibility 575

institutionally: non-point sources of water pollution. All our energy and
effort has been lavished on the question of how to reduce point sources,
such as manufacturing plants, municipal sewage treatment centers,
and other “piped” effluents. But what about agriculture? What about
topsoil runoff and livestock urea runoff? In the cities, what about
storm water runoff? Over half of all pollutants coming from non-
point sources were uncontrolled, as of the early 1980's, and over half
of all pollutants entering surface waters come from non-point
sources.* As he says, “In contrast to the limited progress that has
been made in cleaning up point discharges, progress with nonpoint
sources is almost negligible.”*

What Christians must proclaim is that thzs world is God’s, and we
are His stewards. When certain forms of pollution arc beyond our abil-
ity as creatures to deal with effectively, we should abandon the pro-
duction processes that leave the uncontrollable wastes. But this also
means that we have a responsibility to develop economically and institu-
tionally workable allocation systems to dispose of ihe wastes that we can control.
A combination of private ownership, private responsibility, public
sanctions, and the free flow of capital makes possible an efficient
spreading of pollution into those communities that can deal with
them most effectively. There is a division of labor in the world,
There are different environments in different regions of the earth.
We need a cost-effective allocation of pollutants in order to protect the earth’s en-
tire environment. More specifically, we need a program of market in-
centives and State sanctions to distribute pollution in such a manner
that concentrated and dangerous pollutants are rendered harmless,
either by safety packaging or by dilution through geographical dis-
persion, Without the free market, it is unlikely that the earth’s total
pollution will be allocated efficiently. Civil government alone cannot
do it.

Private Property Rights

Dolan links the crime of pollution with the crime of érespassing.*”
Pollution is therefore an invasion of the rights of private ownership.
This explains why it is legitimate to bring in the civil government to
reduce “pollution invasions” in a neighborhood. By placing pollution

45, Ibid., pp. 166, 190.
46. Ibid., p. 190.
47, Dolan, TANSTAAFL, p. 69.
