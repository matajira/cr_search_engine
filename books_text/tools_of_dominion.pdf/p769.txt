Impartial Justice and Legal Predictability 761

involves reasoning by analogy, frequently an intuitive process—a
process beyond the scope of mathematics. '* Hayek writes: “That the
judge can, or ought to, arrive at his decisions exclusively by a proc-
ess of logical inference from explicit premises always has been and
must be a fiction. For in fact the judge never proceeds in this way. As
has been truly said [by Harvard’s Dean Roscoe Pound], ‘the trained
intuition of the judge continuously leads him to right results for
which he is puzzled to give unimpeachable legal reasons.’ ”*? Or as
Nicholas Georgescu-Roegen, perhaps the most brilliant epistemolo-
gist that the economics profession has ever seen, has described the
problem: “And it is because society and its organization are in con-
stant flux that genuine justice cannot mean rigid interpretation of
the words in the written laws.”'*

Human reasoning cannot function without intuition. Reason
can be progressively disciplined by either covenant-keeping intuition or
covenant-breaking intuition, but in either case, reasoning is not a
mechanical-numerical process. “Between the plasticity of the brain
and the mechanistic structure of a computer there is an unbridgeable
gap... .”45 Intuition is the inescapable element of the incalculable
in all human thought and decision-making, Intuition connects the
“steps” in the human reasoning process, a process which in fact can-
not be shown to consist of a series of discrete, identifiable steps. The
process of reasoning is a continuum, and it is applied to change over
time, which is also a continuum. Georgescu-Roegen writes, “The

 

 

tronic computers do not think at all; computer programs are structured numerically
(digitally). As computer programmer A, L, Samuel said so well, computers “are
giant morons, not giant brains,” Samuel, “Artificial Intelligence: A Frontier of Auto-
mation,” Annals of the American Academy of Political and Social Science, COCXL (March
1962), p. 13

12. Higher mathematics, as with all human speculation, also involves the use of
intuition. The popular understanding of mathematics ignores this. Fitting the
aesthetic purity of mathematics to the external world also involves such things as
faith, genius, and insight. It is not predictable, automatic process, and therefore
not “mathernatical.”

13. FLA. Hayek, Law, Legislation and Liberty, vol. I of Rules and Order, 3 vols. (Uni-
versity of Chicago Press, 1973), pp. 16-17. Hayek gocs on to say that “The other
view is a characteristic product of the constructivist [top-down planning] rationalism
which regards ail rules as deliberately made and therefore capable of exhaustive
statement” (p. 117),

14. Nicholas Georgescu-Roegen, The Entropy Law and the Economic Process (Cam-
bridge, Massachusetts: Harvard University Press, [1971] 1981), p. 82.

15. Tbid., p. 90,

16, Ibid., pp. 60-72.

 
