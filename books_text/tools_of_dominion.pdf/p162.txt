154 TOOLS OF DOMINION

They could live either as bondservants to God or slaves to some for-
eign nation; they could never achieve autonomy.

Would they serve the true God or foreign gods? To whom would
they remain in covenantal bondage, God or Satan? If they cove-
nanted with Satan through their worship of foreign deities, they
would then be scattered, seeking rest but not finding it: “And among
these nations shalt thou find no ease, neither shall the sole of thy foot
have rest: but the Lorn shall give thee there a trembling heart, and
failing eyes, and sorrow of mind” (Deut. 28:65). Rest, both physical
and psychological, is the product of spiritual faithfulness. Thus, to
be delivered from institutional slavery is to be released symbolically
from spiritual slavery; it is to receive the grace of God, for freedom
means deliverance into God’s true rest. Freedom is necessarily sabbatical,
But it is also hierarchical.

In Bondage to Whom?

We must also be alert to another aspect of the story of the exodus,
Jordan comments: “. . . the Exodus from Egypt was grounded not
in a whim of God, but on a carefully worked out legal basis, which
cannot be understood apart from the Biblical laws regarding slavery.
Slavery thus forms one perspective from which the whole matter of
salvation may be viewed. As Christ became the Slave (Servant) of
God, so Christians also are slaves of God, delivered from bondage to
sin and death.”®* We therefore need to understand the legal basis as
well as the social and economic implications of the system of servi-
tude outlined in biblical law.

If Tools of Dominion has a fundamental thesis regarding human
servitude, it is this: servitude is an inescapable concept. It is never a ques-
tion of servitude vs. no servitude. It is always a question of servitude to
whom or what, As Jordan remarks, “man is still essentially a creature
who needs an absolute reference point, a supreme master, to whom
he can relate with absolute passivity. Man’s rejection of the Creator
as God does not result in his having no god at all, but in his having
some false god. Man does not obliterate his psychological need for
an absolute, he ‘exchanges’ it for a lie (Rom. 1:23). Thus, man may
be said to have a ‘slave drive’ which ever seeks some god to submit
to.”8? Even more clearly: “Man, being a slave, has a drive to become

86. Jordan, Slavery in Biblical Perspective, unpublished master’s thesis, West-
minster Theological Seminary, Philadelphia (April 1980), p. 5.
87. Ibid., pp. 8-9.
