716 TOOLS OF DOMINION

agrees to a unique set of contractual obligations by entering into
this arrangement.

‘Thus, once the contract was made, either implicitly or explicitly,
the State had a legal definition of poverty. If the borrower was legally
subject to the possibility of being sold into bondservice for defaulting
on the loan, then the lender could not lawfully extract interest from
him. On the other hand, if the borrower was unwilling to place his
own freedom in jeopardy, then he was unwilling to define himself as
a poor man for the sake of the civil law’s definition. Thus, he had to
pay interest on the loan, and Ais obligation to repay the loan extended
beyond the sabbatical year, If he was not under the threat of bondservice,
he was not under the protection of the sabbatical year or the zero-
interest provisions against usury.

Revising Past Mistakes

No one likes to admit publicly that he was wrong in the past, but
honesty requires it. For two decades, I followed R. J. Rushdoony’s
lead on the question of the sabbatical year of debt release. I taught
that no debt should be contracted by the debtor that is longer than
seven years (Rushdoony says six years).*° I adhered to this in my
own finances. It has cost me a great deal of money. I sold a rapidly
appreciating investment property I wanted to keep because my
seven years had run out, and I did not want to pay $45,000 cash to
pay off the loan. I have paid off other real estate investment loans in
the seventh year. I stayed out of other real estate investments I really
should have made. I did my best to honor in practice what I had
taught in theory. God holds us responsible for obeying our own in-
terpretations of His law, even when we have misinterpreted the law.
This is how we learn to obey. This is also how we show Him that we
are serious about being covenantally faithful. But now I realize that I
was wrong in my interpretation. I no longer wish to mislead people.

I was forced to rethink my position by 8. C. Mooney. Mr.
Mooney has written a truly misguided book on usury. He says that
interest on ali loans is immoral and should be illegal in a Christian
society, He also correctly concludes that this law against all forms of
interest would have to apply to all rents, something previous critics
of interest have been unwilling to say in print. Thus, he concludes,

10. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973), p. 510.
