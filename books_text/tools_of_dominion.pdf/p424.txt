416 TOOLS OF DOMINION

begin to take seriously all the specifics of the biblical case laws. This,
above all, they do not want to do. They would much prefer to suffer
injustice, but all in the name of avoiding the supposedly greatest in-
justice of all, Old Testament law.

Were Christians to begin to identify the injustice of this era as the
product of humanism and the self-proclaimed autonomy of man and
his civil law, they would then be confronted with the awesome task of
reclaiming civil law for Jesus Christ. They would have to rethink the
whole of Western civilization. They would have to study the whole
Bible carefully. They would face the exegetical task that so disturbs
James Dobson: “Do you agree that if a man beats his slave to death,
he is to be considered guilty only if the individual dies instantly? If
the slave lives a few days, the owner is considered not guilty (Exodus
21:20-21)[?] Do you believe that we should stone to death rebellious
children (Deuteronomy 21:18-21)? Do you really believe we can draw
subtle meaning about complex issues from Mosaic law, when even
the obvious interpretation makes no sense to us today? We can hardly
sclect what we will and will not apply now. If we accept the verses
you cited, we are obligated to deal with every last jot and tittle.”

My response? It is time for us to start dealing!

Thumb for Thumb, Toe for Toe

We read of Adoni-bezek in the first chapter of Judges. Adoni-
bezek (Lord of Bezek) was a Canaanitic king. The Israelites fought
him and defeated him. “But Adoni-bezek fled; and they pursued
after him, and caught him, and cut off his thumbs and his great toes.
And Adoni-bezek said, Threescore and ten kings, having their
thumbs and their great toes cut off, gathered their meat under my
table: as I have done, so God hath requited me. And they brought
him to Jerusalem, and there he died” (Jud. 1:6-7), This Ganaanitic
king’s confession reveals that he recognized the justice of the punish-
ment imposed on him by his conquerors.* He had cut off the toes and
thumbs of kings; now hc had suffered the same punishment. He had
removed their anatomical “tools of dominion”; now he had his

4, James Dobson, “Dialogue on Abortion,” in Dobson and Gary Bergel, The Deci-
sion of Life (Arcadia, California: Focus on the Family, 1986), p. 14.

5. The Hammurabi Code specifies mutilations on an “eye for eye” basis, para-
graphs 196-201, Ancient Near Eastern Texts Relating to the Old Testament, edited by James
B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton University Press, 1969),
p. 175.
