The Curse of Zero Growth 853

is never to be a final single voice of human authority until Jesus
Christ speaks His words of judgment at the final judgment. The
Bible divides authority in a series of hierarchies that remove final au-
thority from any single individual or group.

It is the dream of the covenant-breaker either to annul this sys-
(em of divided authority, and replace it with a top-down centralized
order (statism), or else annul all hierarchical order and gain auton-
omy for himself (anarchism).

Contrasting Supernatural Spstems of Authority

The visible sign of God's authority is His ability to bring judg-
ments in history: blessing and cursing. He is invisible; His blessings
and cursings are visible. Israel was warned: “And thou shalt become
an astonishment, a proverb, and a byword, among all nations
whither the T.oan shall lead thee” (Deut. 28:37). God can deliver His
people; He can also lead then back into bondage to a foreign nation.

Satan imitates God when he promises his followers blessings and
cursings. But he owns nothing of his own. He is a thief* and a squatter
in history. Neither his threats nor his gifts arc to be taken very seri-
ously. His promises and threats are all deceptions that are designed
to deflect men’s vision of God’s true promises and the true threats.
Jesus warned people to fear God, not Satan: “And fear not them
which kill the body, but are not able to kill the soul: but rather fear
him which is able to destroy both soul and body in hell” (Matt. 10:28).

God owns the world; thus, He has the power to distribute bless-
ings out of his own capital. Satan can offer no blessings that he has
not previously stolen. The mark of Satan’s imitation sovereignty is
his ability to deceive people into believing in something for nothing on
any terms except God’s grace. (And even God’s free gift of grace to
man was paid for by Jesus Christ.) God distributes true gifts; Satan
creates the illusion of distributing rewards, net, but in fact he has to
collect more than he gives. There is waste, confusion, and deception

4, Judas was representative of his covenuntal master, Satan: “Then took Mary a
pound of gintment of spikenard, very costly, and anointed the feet of Jesus, and
wiped his feet with her hair: and the house was filled with the odour of the ointment.
Then saith one of his disciples, Judas Iscariot, Simon's son, which should betray
him, Why was not this ointment sold for three hundred pence, and given to the
pour? This be said, not that he cared for the poor; but because he was a thief, and
had the bag, and bare whai was put therein” (John 12:3-6).

5. Gary North, Lnherit the Earth: Biblical Blueprints for Economics (Ft. Worth, Texas:
Dominion Press, 1987), p. 61.

 
  

 

 
