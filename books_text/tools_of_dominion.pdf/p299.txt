Victim's Rights vs. the Messianic State 291

not include John 7:53-8:11.1° Most modern translations of the Bible
provide a marginal note to this effect. But if this passage is not in the
Bible, then surely the Old Testament’s capital sanction against adul-
tery has not been altered. If John 8 is not in the biblical canon, then
there is no other passage that supports the case for an alteration of
the capital sanction against adulterers except Joseph’s forgiving
Mary, which we will cxamine in detail later.2°

John 8 deals with a woman who was discovered in the very act of
adultery (v. 4). Her accusers (witnesses) brought her before Jesus,
challenging Hitn to render judgment, This was clearly an attempted
trap on their part, for Jesus was neither a civil nor an ecclesiastical
official. The woman's accusers were also judicially corrupt. They
were law-breaking deceivers, for they were being highly selective:
her partner was not brought before Jesus. (Might he have been one
of their ecclesiastical or professional associates?)

Jesus challenged them: “He that is without sin among you, let
him first cast a stone at her” (v. 7b). Then He stooped down and
wrote something in the dirt (v. 8)—the only instance recorded in the
New Testament of His writing anything. (Might He have written the
names of women who were well known — biblically speaking — by the
woman’s accusers?) We do not know what He wrote. We do know
that her accusers immediately decided to Jeave. Discretion was the
better part of valor, in their view. They did not continue to press
charges against her. Thus, without the presence of two witnesses, she could
not be legally convicted of a capital crime, according to Old Covenant law
(Deut. 17:6). The witnesses had to cast the first stones (Deut. 17:7},
but they all had departed. So, Jesus asked her an obviously rhetori-
cal question: “Woman, where are those thine accusers? Hath no man
condemned thee? She said, No man, Lord. And Jesus said unto her,
Neither do I condemn thee: go, and sin no more” (vv. 10b-I1).

Jesus knew she was guilty as initially accused. He told her to go
and sin no more, making clear to her that He knew she was guilty.
But adultery is a civil matter. Without wetnesses, she could not be lawfully
convicted, She acknowledged Him as Lord in her own words; He
warned her not to do this thing again.

19. See Appendix C: “Lhe Hoax of Higher Criticism.”

20. The loss of this supposed defense of a New Testament alteration in the adul-
tery sanction would be a bitter pill to swallow for nco-evangelicals, far too many of
whom are prone to accept the hoax of higher criticism, and virtually ali of whom
spend their intellectual carcers seeking exegetical ways around the Old Testament
case laws and their sanctions.
