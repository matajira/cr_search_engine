1066 TOOLS OF DOMINION

of nature) Christianity could still be defended, albeit as a pedagogical
means to the moral illumination of mankind.”® Once the denial of the
indissoluble unity of the Bible became common, the next step was easy:
the denial of the need for an infallible New Testament in Christianity.

Reventlow has provided evidence of the political aspects of the
war for and against the infallibility of the Bible. He provides over
400 pages of text and 200 pages of endnotes to demonstrate, among
related themes, that “the political thought of the sixteenth, seven-
teenth and eighteenth centuries continually sought its models and
arguments within the Bible, and the approach of each particular
thinker in question provided the real criterion for the analogies
drawn between the reconstructed biblical model and the principles
which were normative for shaping the society of his time.”® The
Deists launched their war on the Old Testament in an attempt to
substitute natural law for biblical law. Anyene who fails to under-
stand the ethical nature of this intellectual conflict does not under-
stand the history of biblical higher criticism. The attack on the Old
Testament was a fundamental aspect of the coming of modern hu-
manist civilization.

Only as a result of the attack by Deists on the authority of Scripture
(preparations for which were made, against thcir own intentions, by Latitu-
dinarians, Locke and Newton), an attack which they made step by step, did
the legacy of antiquity in the form of natural law and Stoic thought, which
since the late Middle Ages had formed the common basis for thought
despite all the changes of theological and philosophical direction, remain
the one undisputed criterion. ‘This produced a basically new stage both in
the history of ideas and in the English constitution. This position already
contains the roots of its own failure, in that the consistent development of
the epistemological principles of Locke and Berkely [sic] by Hume soon
showed that its basic presuppositions were untenable. However, two irre-
versible and definitive developments remained, which had made an appear-
ance with it: the Bible lost its significance for philosophical thought and for
the theoretical foundations of political ideals, and ethical rationalism (with
a new foundation in Kants critique) proved to be one of the forces shaping
the modern period, which only now can really be said to have begun.

Reventlow has pointed out that higher criticism has faded in im-
portance since the end of the Second World War. In the immediate

8. Reventlow, ibid., p. 398.
9. Thid., p. 43.
10. {6id., pp. 413-14.
