Oppression, Omniscience, and Judgment 675

of a particular Hebrew family to adopt an alien son or daughter. God
adopted individuals into His family, just as He had adopted Israel as
a nation. The sign of God's adoption was circumcision, First, a per-
son could gain access to civil and ecclesiastical rites through circum-
cision (Ex. 12:48). Access to membership in the biblical covenant in
Old Testament Israel was not achieved through incorporation into
one of the nation’s founding families, Circumciscd foreigners and
their families were outside the jubilee land redistribution law (Lev.
25), but they were nonetheless full citizens, although they would
have had to live in the cities, where this law did not apply, or else live
as renters or long-term leaseholders in rural areas. Second, resident
aliens who chose not to be circumcised were under the civil law of
Israel (Ex, 12:49), for the God of Israel is a universal God.

It was His assertion of universality that made the claims of the
God of the Hebrews unique in the ancient world. For example, the
theology of ancient Israel taught, in contrast to the theologies of rival
pagan civilizations, that the defeat of His people militarily did not
mean that the gods of Israel's military conquerors had triumphed
over the God of Israel (Isa. 9-11). The Israelites could be scattered
geographically, yet still remain under the terms of God’s covenant
law (Deut. 28:64-68). Why? Because God is a universal God who
judges all men wherever they are in terms of His law or the work of
the law written in their hearts (Rom. 2:14-15). All of the ancient
world was therefore under the ethical requirements of God's revealed
law, The ancients were supposed to conform themselves to the Ten
Commandments and the case laws that applied these commandments
in daily living. Foreign nations were supposed to see the application of
the legal principles outlined in the Ten Commandments in the actual
daily operations of Israelite society, and they were supposed to imitate
Israel. The resident alien was able to acknowledge this fact in a more
visible way than those living outside the land of Israel.

Resident Aliens Deserve Legal Protection

Any attempt on the part of the judges of Israel to place the resi-
dent alien outside the protection of God’s law would have repre-
sented an attempt to pervert God’s universal standards of justice. By
not honoring God’s law in every dispute between a Hebrew and a
resident alien, the judge was in effect announcing: “God’s law is
binding only in terms of circumcision, Those outside this blood cov-
enant are therefore not under the law’s protection. This means that
