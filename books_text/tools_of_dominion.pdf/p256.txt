6
WIVES AND CONCUBINES

And if a man sell his daughter to be a maidservant, she shall not go
out as the menservants do. If she please not her master, who hath betrothed
her to himself, then shall he let her be redeemed: to sell her unto a strange
nation he shall have no power, seeing he hath dealt deceitfully with her.
And if he have betrothed her unto his son, he shall deal with her after the
manner of daughters. If he take him another wife; her food, her raiment,
and her duty of marriage, shall he not diminish. And if he do not these
Uhre unto her, then shall she go out free without money (Ex. 21:7-ID).

The servitude laws that govern female bondservants are tied directly
to the laws governing marriage. The reason is simple, though not in-
herently obvious; a Hebrew woman could not be permanently purchased; she
could only be adopted. She could not go out of her father’s household “as
the menservants do.” The theocentric principle illustrated by this law
is this: adoption by God is the sole basis of man’s deliverance. God adopted
cast-off Israel, who later became His bride (Ezek. 16). This symbol-
ism was not to serve as a license for incest, which was (and still is)
explicitly prohibited by biblical law (Lev. 18:6-7).! This symbolism
was a defense of the biblical office of husband: he adopts a bride.

These laws governed female bondservants, and they also gov-
erned marriage. The marriage of a female bondservant was governed
by laws different from those governing the marriage of a free
woman. Why should this have been the case? How was marriage to
a bondwoman different from marriage to a free woman? Why would
God have established two different forms of marriage? Docs such a
distinction still apply to marriages in New Testament times?

We must begin our analysis with the biblical doctrine of the bride
of God, a theme that appears throughout both Testaments. We must

1. This poses a difficult exegetical problem for those who deny the continuing au-
thority of Old ‘Testament law in the New Testament cra: On what basis can one bib-
ically and authoritatively deny the legality of incest?

248
