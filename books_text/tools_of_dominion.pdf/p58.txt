50 TOOLS OF DOMINION

used to be imposed on kidnappers in the United States, and kidnap-
ping was rare. It is no longer imposed regularly, and kidnapping has
become a blight. Kidnapping by terrorists in Europe is common-
place. Who says that God's law regarding kidnapping is too harsh?
Harsher than kidnapping itself? So it is with ai of God’s civil laws.
They are merciful compared with the effects of unpunished evil. The
modern world is learning just how unmerciful a society can be that is
not governed by biblical law.

“Theocraphobia”: Fear of God’s Rulership

When, in a court of law, the witness puts his hand on the Bible
and swears to tell the truth, the whole truth, and nothing but the
truth, so help him God, he thereby swears on the Word of God— the
whole Word of God, and nothing but the Word of God. The Bible is a
unit. It is a “package deal.” The New Testament did not overturn the
Old Testament; it is a commentary on the Old Testament, It tells us
how to use the Old Testament properly in the period after the death
and resurrection of Israel’s messiah, God’s Son.

Jesus said: “Think not that I am come to destroy the law, or the
prophets: I am come not to destroy, but to fulfil. For verily I say unto
you, Till heaven and earth pass, one jot or one tittle shall in no wise
{way] pass from the law, till all be fulfilled. Whosoever therefore
shall break one of these least commandments, and shall teach men
so, he shall be called the least in the kingdom of heaven: but whoso-
ever shail do and teach them, the same shall be called great in the
kingdom of heaven” (Matt, 5:17-19), Christ took the Old Testament
seriously enough to die for those condemned to the second death
(Rev. 20:14) by its provisions. The Old Testament is not a discarded
first draft of God’s word. It is not “God’s word (emeritus).”

If anything, the New Testament law is more stringent than the
Mosaic law, not less stringent. Paul writes that an elder cannot have
more than one wife (I Tim. 3:2). The king in the Old Testament was
forbidden to have multiple wives (Deut. 17:17). This was not a
general law, unless we interpret the prohibition of Leviticus 18:18 as
applying to all additional wives, and not just to marrying a woman’s
sister, as ethicist John Murray interprets it.* If we attempt to inter-

49, John Murray, Principles of Conduct (Grand Rapids, Michigan: Eerdmans,
1957), Appendix B. Catholic theologian Angelo Tosato agrees with him: “The Law
of Leviticus 18:18; A Reexamination,” Catholic Biblical Quarterly, Vol. 46 (1984), pp.
199-214, They are not followed in this view by most Protestant commentators, nor
by Nachmanides, who said that the verse applies only to a woman’s sister: Rabbi
Moshe hen Nachman {Ramban], Commentary on the Torah: Leviticus (New York:
Shilo, [12672] 1973), p. 255.
