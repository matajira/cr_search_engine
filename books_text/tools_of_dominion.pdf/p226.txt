218 TOOLS OF DOMINION

ciated with marriage would be a spur to the bondservant’s productiv-
ity, marriage was also an incentive to liberty. Thus, contrary to
Maimonides, it is difficult to imagine that the Bible would have
created an economic disincentive for the master to provide his bond-
servant with a wife. He retained a portion of her productivity, and
the productivity of any children born of the union, until the bondser-
vant could afford to redeem her.

The Release Price

There were two ways of reuniting a broken Hebrew servant fam-
ily. First, the servant could voluntarily become a lifetime servant,
The sign of his bondage as an adopted household servant was a pierced
ear (Ex. 21:6), This legal position as an adopted son would have
been in effect until the jubilee year, when he would have returned as
a free man to take possession of his family’s inheritance in the land
(unless he inherited land in his adoptive father’s legacy).'® Second,
he could go out as a free man, returning intermittently for visitation
rights with his wife, until such time as he earned funds to purchase
his wife and children.

Understand, however, that no biblical text explicitly specifies this
right of redemption by the husband if the wife was owned by a
Hebrew master. Nevertheless, such a Jegal right is an inescapable
conclusion of Exodus 21:7-8: “And if a man sell his daughter to be a
maidservant, she shall not go out as the menservants do. If she
please not her master, who hath betrothed her to himself, then shall
he let her be redeemed: to sell her unto a strange nation he shall have
no power, seeing he hath dealt deceitfully with her.” The Hebrew
daughter could be bought and sold as the Hebrew manservant could
be. She could become a maidservant (Deut. 15:12). She could also be
purchased by means of a bride price, that is, to become a wife. Her
father could not legally abolish the God-given judicial, covenantal
office of father; he could only transfer this office to another man who
was promising to become her future husband or her future father-in-

16. Hebrew rabbis agrced that the word “forever” in Exodus 21:6 referred to the
period remaining until the jubilee, said the medieval Jewish commentator, Rabbi
Moshe ben Nachman (Ramban), Commentary on the Torah: Exodus (New York: Shilo,
[12672] 1973), pp. 348-49: Ex. 21:6. We do not know exactly when Nachmanides
wrote this section; he did not complete his commentary on the Pentateuch until his
arrival in Jerusalem in 1267. Charles B. Chavel, RAMBAN: His Life and Teachings
(New York: Philipp Feldheim, 1960), p. 44. He died sometime around 1270,
although the date of his death is not known: iid., p. 66. On “forever,” see also
Maimonides, Acquisition, “Slave Laws,” Chapter Three, Section Seven, p. 255.
