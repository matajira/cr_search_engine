The Restoration of Biblical Casuistry 43

struction represents a radical challenge to modern antinomian
Christianity and modern humanism.

The enemies of God continue to bring up the issue of slavery in
their war against Christianity. They seek to make Christians feel
guilty regarding Christianity’s theological and historical legacy.
Christianity unquestionably condoned and even sanctioned chattel
slavery until the nineteenth century. The enemies of Christianity
then trace this judicial sanctioning of chattel slavery back to the Old
Testament. In this way, they seek to create a sense of guilt and doubt
in their targeted victims. They understand that guilt-ridden people
are not effective opponents of the prevailing messianic social order.
Rushdoony is correct when he says that “The reality of man apart
from Christ is guilt and masochism. And guilt and masochism in-
volve an unbreakable inner slavery which governs the total life of the
non-Christian. The politics of the anti-Christian will thus inescap-
ably be the politics of guilt. In the politics of guilt, man is perpetually
drained in his social energy and cultural activity by his overriding
sense of guilt and his masochistic activity. He will progressively de-
mand of the state a redemptive role. What he cannot do personally,

¢., to save himself, he demands that the state do for him, so that the
state, as man enlarged, becomes the human savior of man.”

That the Christians failed for many centuries to challenge chattel
slavery is a black mark in the history of the church. But to lay the
blame at the doorstep of the Bible is either a mistake or an ideologi-
cal strategy, as I will prove in Tools of Dominion. If this book per-
suades Christians that this doubt-inducing accusation against the
Bible regarding its supposed support of chattel slavery is false, then
it will have achieved a major success.

Pietism vs. God’s Law

What we find in our day is that Christians despise biblical Jaw al-
most as much as secular humanists do. These Christians have begun
to adopt arguments similar to those used by the English Deists. For
example, they attack the very thought of stoning drunken, glut-
tonous sons—not young children, but adult sons who are living at
home with their parents, debauching themselves —as some sort of
“crime against humanity,” when stoning them is specifically a civil
sanction authorized by God (Deut. 21:18). The very idea of execution

37. R. J. Rushdoony, Politics of Guilt and Pity (Fairfax, Virginia: Thoburn Press,
11970] 1978), p. 9.
38, Ed Dobson and Ed Hindson, “Apocalypse Now?”, Poligy Review (Fall 1986), p. 20
