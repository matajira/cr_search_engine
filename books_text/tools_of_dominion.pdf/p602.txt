594 TOOLS OF DOMINION

terest discounts the present market value of the expected future
stream of income (including personal use value) of a capital asset.
The higher the discount, the less the asset is worth in the present. A
future-oriented society has a lower rate of interest than a present-
oriented society. A lower rate of interest therefore allows future gen-
erations to “shout their bids” more effectively to this generation’s
“brokers” or “auctioneers.” It generally takes private, profit-seeking
“auctioneers” to hear those bids clearly and act in terms of them. In
short, the more a soctety conforms itself toa biblical concept of private owner-
ship and a biblical concepi of time, the higher the capitalized value of privately
owned assets, as a result of the greater attention that profit-seeking owners will
pay to the perceived demand of future owners and users.

Summary

The question of resource conservation is intimately tied to the
question of time perspective. When we ask ourselves questions con-
cerning resource conservation, we are asking questions regarding
conservation for future consumption,

The debate over ecology has been dominated by people who be-
lieve (or say they believe) that the civil government has the most re-
sponsible view of the future. They do not raise the obvious question:
What motivates the individuals who control the various agencies of
civil government? What is their motivation regarding pollution and
resource conservation compared to the motivation of private owners?

Free market economists stress the long-range motivations of
those who own property. When a person sells an asset, he is capital-
izing in the present the expected future net productivity of that asset.
The individual who can sell an asset owns it. The government bu-
reaucrat cannot legally sell it and pocket the money, so he does not
own it. Thus, his motivation is to use the asset in such a way that his
income or prestige is increased, He is not paid to represent future
gencrations of users; the private owner is paid to represent those liv-
ing in the future, for an assct’s present price depends heavily on the
expected stream of net income it will generate over time,

What we find is what economics predicts concerning the motiva-
tion of managers under socialist ownership of the means of produc-
tion, Managers in socialist nations tend to pollute the environment,
use State-owned resources, and ignore the complaints of the politi-

82. The other major considerations are selling costs and the prevailing rate of
interest.
