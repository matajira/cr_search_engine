The Prohibition Against Usury 751

form of economic subordination in every debt contract. Debt is still a
threat, even though it can also be very productive, It is like fire: a
useful tool, but a danger if it gets out of control.

The Forfeited Productivity of Inaction

The master in the parable is outraged by the com-burying servant.
The parable is intended to show the subordinate (indebted) position of
all men before God. The servant was cast into outer darkness be-
cause he was an unprofitable servant (v. 30). The parable stands as a
warning to all men because the Bible teaches that all people are un-
profitable servants (Luke 17:10).? This is why we need a profitable
servant as our intermediary before God, our perfect sin-bearer. But
to understand our relationship of indebtedness to God, the parable’s
language must be taken seriously. We cannot make accurate theolog-
ical conclusions about the broader meaning of the parable if the sym-
bolic reference points of the parable are themselves inaccurate.

There is no question that the master not only approves of taking
interest, he sends the servant to the nether regions for not taking it.
This is strong imagery! The interest payment belongs to the master. By
having refused to deposit the master’s money with the money-
jenders, the servant has in effect stolen the master’s rightful increase.
The servant was legally obligated to protect the master’s interests,
and interest on his money was the minimum requirement. He failed.
The master’s judgment of the servant’s past performance had been
accurate; he was entitled to only one talent initially, for he had not
demonstrated competence previously. Had he been given more, he
would have wasted more.

The idea that the interest return was the master’s minimum ex-
pectation leads us to the question of the origin of interest. Why did the
master deserve an interest return? Because he had possession of an
asset that could have been put to productive use, but was not. He
had forfeited an economic return that could have been his. This con-
cept of the forfeited return appeared in medieval economic literature as
the doctrine of fucrum cessans. The owner of money who could have
made a profit by investing it elsewhere, but who loaned the money to
someone, was said by some theologians to be entitled to an interest
payment from the borrower because of the income he had forfeited.
Interest compensated the lender for the opportunity he had missed.

79. Gary North, “Unprofitable Servants,” Biblical Economics Teday (Feb./March
1983).
