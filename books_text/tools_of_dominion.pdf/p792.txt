784 TOOLS OF DOMINION

mean that he need not return a lost animal to his friend? No; the
focus of the law is on the case where the temptation is greater to keep
the animal: the encmy’s beast. The law assumes that if you are re-
quired Lo obey it in the difficult case, you surely are required to obey
it in the easier case.
