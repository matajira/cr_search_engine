Victim's Rights os. the Messiante State 307

The second question is this: If the victim decides not to prose-
cute, can any other court intervene and prosecute in God’s name?
The case of Joseph and Mary indicates that Joseph’s decision would
have been authoritative and final. Her pregnancy would have been
visible Lo all, yet if hc had chosen not to prosecute, she could remain
free of concern about any other court bringing charges against her.
Had she actually been an adulteress, and had her consort been mar-
ried, then the victimized wife could bring charges against them, but
she could gain only a divorce: the court’s declaration of a broken
marriage. She could not require civil penalties against Mary, and
therefore also not against her husband. Joseph, not the victimized
wife, was the primary earthly victim and therefore the one who pos-
sessed the option of freeing his betrothed wife from any civil penalties.

What Does the Pleonasm Emphasize?

I think the pleonasm identifies crimes that are the highest on God's list
of abominations, The normal penalty for these crimes is death; any-
thing less than this which the victim specifies is a manifestation of
great mercy. By upholding the principle of victim's rights, biblical
law also creates incentives for criminals to deal less harshly with vic-
tims during the actual crime. If the victim is not brutalized, he may
decide to show leniency if the criminal is later convicted. This pro-
tects the victim. Biblical law is designed to protect the victim.

Must civil judges impose the maximum penalty allowed by biblical
law when the State is the victim, or when by law the State is God’s
designated agent to protect the community by upholding God's
rights and enforcing His sanctions? Not always. The principle of vic-
tim’s rights governs the imposition of civil sanctions, Judges have the
God-given authority to impose a reduced penalty according to cir-
cumstances. The only exceptions to this rule are those cases in which
the pleonasm occurs; the judges cannot reduce the sanctions in such
cases. This is the meaning of the pleonasm: the elimination of judicial
discretion in imposing sanctions when the State initiates the lawsuit.

Consider two alternative lines of reasoning. First, if we argue
that the judges must impose the maximum penalty in al! cases that
specify the death penalty, irrespective of the presence of the
pleonasm, then the cmphasis aspect of the pleonasm disappears judi-
cially. If all capital crimes require the death penalty, of what purpose
is the pleonasm? This would indicate that the pleonasm has some
function other than judicial emphasis. I cannot imagine what this
