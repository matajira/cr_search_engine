242 TOOLS OF DOMINION

He argued that merely by giving them their freedom, the owner
could never get them to plan ahead. The owner might go back on his
promise. But by selling a slave his freedom, this future-orientation
would affect the slave’s character positively. “Hope would be kept
alive in his bosom; he would have a goal in view, continually urging
him on to faithfulness, fidelity, trust, industry, economy, and every
virtue of good work,”®! He did not honor the seventh-year automatic
release, but he did understand that by allowing a slave to buy his
way to freedom, the very effort would prepare him for independ-
ence. Meanwhile, the efforts of these independence-seeking slaves
made him a rich man.

At his funeral, there were many weeping former slaves, but very
few whites. He had broken covenant with his white contemporaries,
getting rich by adhering to a biblical principle. He had begun to
understand servitude as God had designed it: as a means of impart-
ing self-discipline and eventual independence to the children of for-
eigners from demonic cultures, His peers had regarded slavery as
the ancient Hebrews immediately had come to regard it: as a way to
get rich through other men’s permanent servitude. Judgment fell on
the South, just as it had fallen on Israel. The Assyrians of the North
invaded.®

It must be understood that McDonogh’s system of slave manage-
ment worked well only because of civil laws establishing the perma-
nence of slavery. Only because the normal fate of a slave was to be
born into slavery and die a slave, with his children inheriting his
slave status, could the prospect of self-purchased manumission serve
as a major incentive. Had the slave's release been automatic by law
alter a maximum of seven years of servitude, McDonogh's system
would not have had comparable impact in the lives of his slaves. It
was the ultimate negative earthly sanction of permanent servitude
that enabled McDonogh to employ the positive sanction of liberty
through purchase.

81. Cited by Carl N. Degler, The Other South: Southern Dissenters in the Nineteenth
Century (New York: Harper & Row, 1974), pp. 43-44. Degler relies on twa main
sources: Lane Carter Kendell, “John McDonagh ~Slave-Owner,” Louisiana Historical
Quarterly, XV1 (1932), and William Talbot Childs, Join McDonogh: His Life and Work
(Baltimore, Maryland: Johns Hopkins University Press, 1939).

82. In the Old Testament, Assyria fell to Babylon. The international Babylon of
our day is vastly more merciless than the Babylon of Jeremiah’s day: Robert Con-
quest and Jon Manchip White, What lo Do When the Russians Come: A Survivor's Guide
(New York: Stein and Day, 1984).
