1194 TOOLS OF DOMINION

Carolina simply affirmed liberty of conscience.!® North Carolina’s
Assembly in 1703 passed a rigorous law against blasphemy. Anyone
who had once confessed Christianity could not publicly deny its truth.
Violators could not hold political office. Virginia removed test oaths
alter the Revolutionary War, which had long been James Madison’s
dream.'* So did Pennsylvania.

Confessional Oaths for State Officers

All states except New York had special confessional oaths for
state officials. It was this explicitly Christian character of the state
constitutions that became the target of the handful of anti-
Trinitarian delegates to the Constitutional Convention in
Philadelphia.

To serve in Congress under the Articles, a man had to be ap-
pointed by his state legislature. He could be recalled at any time. He
could serve in only three years out of every six, He was under public
scrutiny continually. And to exercise the authority entrusted to him
by his state legislature, he had to take an oath. These oaths (except
in New York) were both political and religious. The officer of the
state had to swear allegiance to the state constitution and also
allegiance to God. Consider Delaware’s required oath:

Art. 22. Every person who shall be chosen a member of either house, or ap-
pointed to any office or place of trust, before taking his seat, or entering
upon the execution of his office, shall take the following oath, or affirma-
tion, if conscientiously scrupulous of taking an oath, to wit:

“I, A B, will bear true allegiance to the Delaware State, submit to its
constitution and laws, and do no act wittingly whereby the freedom thereof
may be prejudiced.”

‘And also make and subscribe the following declaration, to wit:

“T, A B, do profess faith in God the Father, and in Jesus Christ His only
Son, and in the Holy Ghost, one God, blessed for evermore; and I do
acknowledge the holy scriptures of the Old and New Testament to be given
by divine inspiration.”

And all officers shall also take an oath of office.”

The Cohstitution of Vermont in 1777 was not much different:

15, Ibid., p. 356.

16. Robert A, Rutland, “James Madison's Dream: A Secular Republic,” Free fa-
quiry (Sept. 1983), pp. 8-1.

17. The Founders’ Constitution, edited by Philip B, Kurland and Ralph Lemer, 5
vols. (University of Chicago Press, 1987), V, p. 634.
