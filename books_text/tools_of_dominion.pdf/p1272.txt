1264

servitude, 259n

slaves and Bible, 231

theft, 1050

Torah and gentiles, 998

victims, 1050

wandering beast, 542n

witness and oath, 637n

witnesses, 499

women’s talk, 1021
Maine, Henry, 920
Majority rule, 54-58, 567n
Malabre, Alfred, 222
Malkin, Lawrence, 222n
Managers, 127
Manichaeanism, 61
Manicipium, 1171
Manipulation, 881, 887
Mann, Horace, 406n, 429n
Manna, 816, 824
Manne, Henry, 510n
Manslaughter, 346, 418
Manson, Charles, 409n.
Manumission, 142, 240-42
Marbie Faun, 1069
Marcion, 49
Marcionism, 1068
Mardi Gras, 342
Market failure, 1099
Marriage

adoption, 276

agents, 649

authorization, 269

church’s role, 227, 270

future-orientation, 245

legal bond, 193-94

levirate, 228

market, 1105

slaves, 234-39

succeeds adoption, 261

supper, 267

veto, 259

vows, 245
Marsden, George, 167n
Martyr, Justin, 987
Mars, Karl

exploitation, 786n

TOOLS OF DOMINION

graduated tax, 804
property, 920
revolution & publishing, 3-4
slavery in U.S., 379
workers as commodities (myth),
362-64
Marxism, 191, 988-89
Mary, 294, 306, 317, 660
Maryland, 1193
Masonry, 391-92, 1204-5, 1206
Massachusetts, 443, 446, 465, 1193
Master, 78, 92, 212, 442
Masters, 116, 117
Master's rights, 239-40
Mathematics, 993-94, 1126-28, 1142
Mather, Cotton, 987
Maturity, 986, 9907
Mauritania, 332-33
McDonogh, John, 240-42
McPhee, John, 423
Meaning, 960, 963-64
Measurement
Buchanan on, 1129
costs and benefits, {139
intuitional, f1l1n
metromania, 1128
pleasure-pain, 1146-1148
Rothbard on, 1118
utility, 1107
value theory, 1093-1100
Measures, 684
Mediation, 883
Medicine, 862, 864-65, 867n
Medvedev, Roy, 169n
Meek (before Ged), 996
Meir, Rabbi, 1009, 1014-15
Memorization, 1009, 1028
Memory, 815
Mendelsohn, Isaac, 156, 657n, 6580
Menger, Carl, 1123n
Mercy, 165-66, 169n, 294-95, 317
Meridian, 886
Merit, 953
Mesopotamia, 123, 156, 158, 789
Mesopotamian law, 104-5
Metaphysics, 1128
Methuselah, 975, 982
