302 TOOLS OF DOMINION

lawsuit is first presented by the victimized husband to the suspected
partner, and then (at the discretion of the victimized husband) it is
presented in the appropriate court or courts. The institutional
church has a legitimate role to play if either of the marriage partners
is a member. It pronounces the sentence of covenantal death against
the offending party. Thus, adultery can sometimes affect all three
covenantal institutions. The victim declares that the covenantal
bond of marriage has been broken, and that the adulterers have now
come under God's wrath. If the suspected adulterous male partner is
married, his wife can also file appropriate lawsuits against her hus-
band. Biblical law makes it clear, however, that the husband of the
adulterous wife has primary authority to specify the penalty. It is his cove-
nantal household office as the head of the family that has been
attacked by the adulterers. If he decides on the death penalty for his
wife, as we shall see, the criminal consort cannot escape her fate. As
the officer of his family’s government, the victimized husband
specifies the penalty; the wife of the adulterer cannot stay the hand
of the civil magistrate.

Two questions arise. Can the husband legally grant mercy to the
wife if she is convicted, that is, can he specify a lesser punishment?
Furthermore, if he can, and if he does this, must he show equal mercy
to the convicted man?

 

No Respect for Persons

The example of Jesus on the cross indicates that the victim can
lawfully spare the criminal. He asked His Father to forgive them,
meaning Jews and Romans (Luke 23:34). He spared both of the
“adulterers,” Israel and her consort, Roime. Israel again and again in
Old Testament history committed spiritual adultery with foreign
gods and nations, yet God always spared the nation until a.p. 70.3
The Book of Hosea centers on this theme of the husband’s torgive-
ness of an adulterous wife. Romans f1 indicates that genetic Israel
will someday be re-grafted into the church through mass conversion, 33
so God has still withheld the death penalty from Israel as a cove-
nantal people (though not necessarily as the modern political unit
that we call the state of Israel).

32, David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Pt.
Worth: Dominion Press, 1987).

33. ‘This postmillennial position has been defended by such Calvinist cominenta-
tors on Romans [1 as Charles Hodge, Robert Haldane, and John Murray. The
Larger Catechism of the Westminster Confession of Faith also teaches it: Answer
191
