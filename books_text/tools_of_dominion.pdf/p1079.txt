The Hoax of Higher Criticism 1071

responsible for the presentation of the material available ta us.
E. M. Forster, struck by the cavalier way in which we treat the past,
attributed the attitude to the fact that those who lived then are all
dead and cannot rise up and protest.”?3

He is being much too kind. The scholars’ “demeaning of the in-
telligence of the lawgiver who was responsible for the presentation of
the material available to us” is all too intentional, for that Lawgiver is
God Almighty, who will judge every man on judgment day. Higher
critics are determined to deny that such a cosmic Lawgiver exists,
and they do their best to make His laws seem like an incoherent col-
lection of disjointed and self-contradictory pronouncements, a judicial
jumble compiled by a series of editors who apparently could not keep
clear in their minds anything that was written in the text in front of
them that was farther back or farther forward than three lines.
Somehow, these deceptive ancient masters of language and textual
subtleties could not keep any argument straight, or remember the
plot line of even a one-page story. Their heavy-handed attempts to
revise the ancient texts for their own contemporary purposes were so
badly bungled that they succeeded only in so distorting the text that
no careful reader could possibly believe that God had revealed the
Pentateuch to one man, Moses.

It is not the Pentateuch that is disjointed. It was not the hypo-
thetical “later editors” who could not keep things straight in their
minds. Rather, it is the paid professional army of higher critics. I ap-
preciate C. S. Lewis’ comments, as a master of medieval and early
modern English literature, regarding the ability of textual critics to
understand their texts: “These men ask me to believe they can read
between the lines of old texts; the evidence is their obvious inability
to read (in any sense worth discussing) the lines themselves. They
claim to see fern-seed and can’t see an elephant ten yards away in
broad daylight.”*

Apostate Deceivers

The higher critics present the Bible as a poorly assembled patch-
work of lies and myths, and then they add insult to injury by arguing

23. Calum M. Carmichael, Law and Narrative in the Bible: The Evidence of the
Deuteronomic Laws and the Decatogue (Ithaca, New York: Gornell University Press,
1985), p. 14.

24. C. 8. Lewis, Christian Reflections, edited by Walter Hooper (London: Geotfrey
Bles, 1967), p. 157. The essay is titled, “Modern Thought and Biblical Criticism?
