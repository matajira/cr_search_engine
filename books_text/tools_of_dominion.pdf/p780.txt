772 TOOLS OF DOMINION

the programs. These philosophies universally bear false witness against
the rich in general, charging that the rich have exploited the poor
throughout history. Marxism is only the most successful and most
consistent of these philosophies of organized envy. There are many
others: fabian socialism, national socialism (Nazism), the corporate
State (fascism), social democracy, populism, liberation theology,
Christian socialism, the New World Order, the New International
World Order, New Age communalism, and hundreds of variants.
These philosophies have produced political movements that have
pressured politicians to pass legislation that oppresses the produc-
tive: the present rich (though seldom the “super rich”)#! and the
future rich, meaning all those who would like the opportunity to be-
come rich, i.e., the middle class entrepreneur, the independent busi-
nessman, and the potentially productive but presently poor person,
whose avenues for advancement are cut off.‘

 

Conclusion

Because all men are under God and responsible to God, justice is
to be impartial and predictable. It is not to be arbitrary, for God is
not arbitrary. Law is both constant and theocentric, although appli-
cations of God’s fixed laws can and have changed, as a result of new
historical circumstances. The Bible gives us our standards of appli-
cation, just as it gives us God's law.

Men are to judge in terms of God’s law. This process of rendering
judgment is not mechanical. It is personal and covenantal. It in-
volves the use of intuition, either Bible-based or humanistic. There
is no escape from the “humanness” of human judgment. What is
needed to restrain men from arbitrariness in rendering judgment is a
system of biblical law which restrains the flights of judicial fancy of
intuition-guided judges. But we can never totally eliminate uncer-
tainty from the judicial process. The price of perfectly certain justice
is astronomical; it would destroy justice.

Legal predictability is one of the fundamental historical foundations
for the development of capitalism in the West. The rise of envy-based
political and economic systems is now threatening the productivity

41. Ferdinand Lundberg, The Rich and the Super Rick (Secaucus, New Jersey: Lyle
Stuart, 1968).

42. See, for example, Walter Williams, The State Against Blacks (New York: New
Press, McGraw-Hill, 1982).
