Seduction and Servitude 657

prices in the surrounding cultures, and these are reasonably com-
mensurate with the prices listed in Leviticus 27.!7 The purchase of a
slave gained the buyer the net return from a lifetime of service from
a slave. We are not talking about merely a Hebrew’s seven-year term
of service, for the caravan bought Joseph for resale into permanent
servitude, Thirty shekels of silver must have been a lot of money; 50
shekels was that much more.8

Lifetime Servitude

Would the seducer have come under the provisions of the debt-
release provisions of the sabbatical year (Deut. 15)? Probably not. If
these sabbatical year provisions had applied to this crime, they
would have subsidized seductions in the years immediately preced-
ing a year of release by lowering their financial risk. To avoid this
implicit subsidy of sin, the young man would probably have been re-
garded by the court as the equivalent of a thief who had to make full
restitution to his victims, even if it meant lifetime servitude. He
could not escape the payment of the bride price.

In effect, the young man would have come under his father-in-
law’s jurisdiction for many years. This would have been an appro-
priate form of judgment for his having lured the girl into making a
covenant vow autonomously. They would both be brought under the
jurisdiction of the girl’s father as a punishment, but also as a way to
bring them greater respect for his authority in the future.

All or most of the bride price eventually came to the daughter,
and from her to her children. It was her protection against an incom-

17. Mendelsohn, Slavery In the Ancient Near Fast, pp. 117-18.

18. There is a hidden danger in one account of a purchase in the Old Testament,
David's apparent purchase, for fifty shekels of silver, of the threshing floor that later
became the site of the temple (IT Sam. 24:24). This was a very desirable location on
a mountain top in the midst of the capital city of the nation. How could he have pur-
chased this for the price of a slave? The answer is that he actually paid 600 shekels
of gold (I Chron. 21:25). The fifty shekels probably bought only the oxen used in
the sacrifice.

19. United States Senator Daniel Moynihan (New York) has proposed a sweep-
ing reform of the national welfare system. One of these reforms would make man-
datory that unmarried parents under the age of 18 years old be required to live with
their own parents, or in a foster home or a maternity home, if they receive welfare
payments, The system presently encourages a teenage mother to move away from
her home by paying her more money if she moves out. Suzanne Fields, “Welfare
Reincamnate: Seeking new life for a gasping system,” Washington Times (July 28,
1987), Sect, D, p. 1.
