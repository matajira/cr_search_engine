2
THE AUCTION FOR SUBSTITUTE SANCTIONS

Uf men strive, and hurt a woman with child, so that her fruit depart from
her, and yet no mischief follow: he shail be surely punished, according as
the woman’s husband will lay upon him; and he shall pay as the judges
determine. And if any mischief follow, then thou shalt give life for life,
eye for eye, tooth for tooth, hand for hand, foot for foot, burning for burn-
ing, wound for wound, stripe for stripe (Ex. 21:22-25).

In the previous chapter, I discussed the biblical rationale for the
principle of ex talionis; upholding the principle of victim’s rights
while limiting the State. It provides a means of restoration: to the
victim, to the criminal, and to society, In this chapter, we need to
consider how this principle could be applied in the modern world.

As the reader considers how this system might work in practice,
he must keep in the back of his mind an outline of how the modern
criminal justice system works, for it is surely more criminal than
just. We are not here comparing a pair of theoretical ideals, biblical
vs. humanistic. We are attempting to compare rival systems in ac-
tion, although because there has been such hostility to the lex éalionis
principle, we have not seen the biblical system in operation histori-
cally. This is why this chapter is in part hypothetical—how things
could work—yet also a self-conscious attempt on my part to deal
with the real world.

The reason why we have no real-world examples of how the lex
talionis system has worked is because Christians and Jews have never
seriously attempted to enforce it in civil law, or if they have, they
have left few records of these attempts. They have instead adopted

1. Again, this could be the product either of an historical blackout on the part of
professional historians or the ignorance of the historians regarding biblical law. We
need generations of trained historians who will comb the records of Western legal
history and examine the documents from the point of view of biblical law.

413
