954 TOOLS OF DOMINION

commendeth his love toward us, in that, while we were yet sinners,
Christ died for us” (Rom. 5:8), God selects those on whom He will
have mercy (Rom. 9:18), He has chosen these people to be recipients
of His gift of eternal salvation, and He chose them before the foun-
dation of the world (Eph. 1:4-6).

But there is another kind of grace, and it is misunderstood. Com-
mon grace is equally a gift of God to His creatures, but it is distin-
guished from special grace in a number of crucial ways. A debate
has gone on for close to a century within Calvinistic circles concern-
ing the nature and reality of common grace. I hope that this essay
will contribute some acceptable answers to the people of God,
though I have little hope of convincing those who have been involved
in this debate for 60 years.

Because of the confusion associated with the term “common
grace,” let me offer James Jordan’s description of it. Common grace
is the equivalent of the crumbs that fall from the master’s table that
the dogs eat. This is how the Canaanite woman described her re-
quest of healing by Jesus, and Jesus healed her because of her
understanding and faith (Matt. 15:27-28).?

Background of the Debate

In 1924, the Christian Reformed Church debated the subject,
and the decision of the Synod led to a major and seemingly perma-
nent division within the ranks of the denomination. The debate was
of considerable interest 1o Dutch Calvinists on both sides of the
Atlantic, although traditional American Calvinists were hardly
aware of the issue, and Arminian churches were (and are still) com-
pletely unaware of it. Herman Hoeksema, who was perhaps the
most brilliant systematic theologian in America in this century, left
the Christian Reformed Church to form the Protestant Reformed
Ghurch. He and his followers were convinced that, contrary to the
decision of the GRC, there is no such thing as common grace.

The doctrine of common grace, as formulated in the disputed
“three points” of the Christian Reformed Church in 1924, asserts the
following:

 

2. Dogs in Israel were not highly loved animals, so the analogy with common
grace is biblically legitimate. “And ye shall be holy men unto me: neither shall ye eat
any flesh that is torn of beasts in the field; ye shalll cast it to the dogs” (Ex. 22:31). Tf
we assume that God loves pagans the way that modern people love their dogs, then
the analogy will not fit.
