The Epistemological Problem of Social Cost 1107

very heart of the problem, and he never shows how economists —or
anyone else, for that matter—can, as scientists, make an economic-
ally rational (i.e., neutral) choice in the name of society: crops vs.
meat. Indeed, humanistic economics cannot possibly answer this
question because of the inability of economists, as scientists, to make
interpersonal comparisons of subjective utility.

Subjective Value vs. Social Policy

Coase never comes to grips with this problem. “What answer
should be given is, of course, not clear until we know the value of
what is obtained as well as the value of what is sacrificed to attain
it.”67 Value? As economists, we need to ask ourselves several ques-
tions: Value to whom? Society as a whole? The value to the cattle
owner? The value to the farmer? Also, how can we make such esti-
mates of economic value, since economic value is subjective? Ques-
tions of economic value are the main problems raised by his paper,
yet he cannot answer them by means of the “scientific economics” he
proclaims. No economist can. Economist Peter Lewin has gone to
the heart of the matter when he writes in a withering critique of
Coase that

costs are individual and private and cannot be “social.” The social-cost con-
cept requires the summation of individual costs, which is impossible if costs
are seen in utility terms. The notion of social cost as reflected by market prices
{or even more problematically by hypothetical prices in the absence of a mar-
ket for the item) has validity only in conditions so far removed from reality
as to make its use as a general tool of policy analysis highly suspect. . . .
The foregoing suggests that any perception of efficiency at the social
level is illusory. And the essential thread in ail the objections to the effi-
ciency concept, be it wealth effects, distortions, or technological changes, is
the refusal by economists to make interpersonal comparisons of utility.
Social cost falls to the ground precisely because individual evaluations of
the sacrifice involved in choosing among options cannot be compared.

66. In other words, we cannot make scientific comparisons of the utility gained
by one person vs. the utility thereby forfeited by another man. There is no unit of
“utility measurement” which is common to both men. We cannot as neutral scientists
legitimately say that one man has gained greater utility (a subjective evaluation on
his part) than another man has fost (another subjective evaluation). I discuss this
problem in Dominion Covenant: Genesis, ch. 4,

67. Coase, “Social Cost,” p. 2.

68. Peter Lewin, “Pollution Externalities: Social Gost and Strict Liability,” Cato
Jmurnal, TI (Spring 1982), pp. 220, 222.
