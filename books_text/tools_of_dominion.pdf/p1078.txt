1070 TOOLS OF DOMINION

“The indiscriminate greed for this literary garbage on the part of uni-
versities is a sign of the academic pedantry on which American Lit.
has been stranded.”*!

All of this is both accurate and amusing. But these same techniques
of literary and textual criticism, when applied to biblical texts by mono-
maniacal German pedants and their epigone Anglo-American
imitators, have for over a century undermined people’s faith in the
integrity of the Bible all over the world,”

Criticizing Textual Criticism

The methods used by higher critics are circular: they use their
colleagues’ reconstructed literary texts to reconstruct the biblical
past, and they use their own newly reconstructed biblical past to fur-
ther reconstruct the biblical texts. On and on the academic game
goes, signifying nothing except the futile purposes to which very dull
people’s minds can be put.

These literary techniques are highly complex, yet amazingly
shoddy. The practitioners agree on very little; they reach no testable
conclusions; and their required techniques absorb inordinate quan-
tities of time to master. Liberal Bible scholar Calum Carmichael
puts it mildly when he warns his readers; “Historical and literary
criticism is undeniably useful when working with ancient sources,
but not only has it limitations, it sometimes leads nowhere. One
manifest restriction in its application to most biblical material is that
the historical results hypothesized cannot be corroborated. The spec-
ulative character of most such results is easily overlooked because
the historical method is so deeply entrenched in scholarly ap-
proaches. With a little distance, we can see just how shaky the his-
torical method is. . . . The procedure is a dispiriting one, dull to
read, difficult to follow, and largely illusory given the paucity of the
results and the conjectured historical realities dotted here and there
over a vast span of time. Its most depressing aspect is the no doubt
unintentional demeaning of the intelligence of the lawgiver who was

Q1. Mid, p. 20.

22. Krentz freely admits of literary criticism that “Che four-source theory of Pen-
tateuchal origins and the two-source theory of the Synoptic interrclationships are its
major results, Literary (source) criticism has achieved a more sharply contoured
profile of the various sources and books, and the authors who stand behind them. It
is indispensable for any responsible interpretation of the Bible.” Krentz, Historical-
Critical Method, p. 50.
