Appendix E
POLLUTION IN THE SOVIET UNION

Fyodor Morgun, head of the State Committee for Environmental
Protection, revealed last year [1988] that air pollution in all Soviet in-
dustrial centers now exceeds Soviet safety limits and is more than ten
times the permitted level in 102 Soviel cities. He also revealed (at the 19th
special Communist Party conference last June) that water from the great
rivers of Russia, including the fabled Volga and the Don, is now almost
unusable for drinking or irrigation.

Until the Chernobyl nuclear power plant accident in 1986, the
assumption of many anti-capitalist critics of modern pollution had
been that socialist societies, especially the Soviet Union, had some-
how avoided the social costs of pollution. This belief was always en-
tirely mythical. What is different today is that glasnost has opened up
the outlets for complaints within the U.S.S.R.

For example, Western reporters have now learned of the story of
the Aral Sea. This sea in northwest Uzbekistan is steadily disappear-
ing. At one time, it was the fourth-largest inland body of water on
earth. It has shrunk by 40 percent since 1960, leaving behind 10,000
square miles of salty desert. Soviet developers have siphoned off into
the cotton fields of Uzbekistan and neighboring Turkmenia the waters
of the two rivers that feed the Aral Sea, leaving these rivers little
more than slow-moving sewers. The fish cannery at Muinak which
had been built on the southern shore is now landlocked, 30 miles
from the water. No matter: the sea’s commercial fishing catch has
now fallen to zero because of the high concentration of salt, fer-
tilizers, and pesticides. The Muinak area remains off-limits to for-
eigners, including reporters. Reports the New York Times, “The high
concentration of salt and farm chemicals in the rivers and under-

4. “The Ecology Crisis,’ National Review (April 7, 1989), p. 28.
1160
