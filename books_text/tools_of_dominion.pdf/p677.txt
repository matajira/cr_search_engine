Oppression, Omniscience, and Judgment 669

ye gave me drink: I was a stranger, and yc took me in: Naked, and ye clothed
me: I was sick, and ye visited me: I was in prison, and ye came unto me.
Then shall the righteous answer him, saying, Lord, when saw we thee an
hungred, and fed thee? or thirsty, and gave thee drink? When saw we (hee a
stranger, and took thee in? or naked, and clothed thee? Or when saw we
thee sick, or in prison, and came unto thee? And the King shall answer and
say unto them, Verily I say unto you, Inasmuch as ye have done it unto one
of the least af these my brethren, ye have done it unto me (Matt, 25:34-40).

If one’s protection of the weak testifies to one’s willingness to
honor God, then God in turn will protect those who offer protection.
Men are weak in the sight of God. They need His protection. How
they treat the weak in history will determine how God treats them in
history. “And he said unto them, Take heed what ye hear: with what
measure ye mete, it shall be measured to you: and unto you that
hear shall more be given” (Mark 4:24).

Restraint and Protection

Strangers, widows, and orphans: these three examples, along
with the poor, are seen in the Bible as being especially vulnerable to
oppression.! “Also thou shalt not oppress a stranger: for ye know the
heart of a stranger, seeing ye were strangers in the land of Egypt”
(Ex. 23:9). They deserve protection. If the Hebrews remained faith-
ful to God in this matter of dealing with strangers, God promised,
they would retain their own civil liberties. If the judges of the land
remained so committed to the ethical terms of God’s covenant that
they would restrain the oppression of strangers, widows, and orphans
by fellow Hebrews, then all righteous Hebrews could safely retain
confidence in their judges. On the other hand, if a system of bribes
or special favors corrupted the judges, and they began to show favor
to the interests of Hebrews in their legal disputes with resident
aliens, widows, and orphans, then this would be a preliminary man-
ifestation of looming tyranny, domestic and then foreign. “Your
wives shall be widows, and your children fatherless” (v. 24).

Why does God single out the widow, the orphan, and the resi-
dent alien? They must be representative of a general class of people.
If we search for the distinguishing characteristic of all three— their

1. Charles F. Fensham, “Widow, Orphan and the Poor in Ancient Near Eastern
Legal and Wisdom Literature,’ Journal of Near Eastern Studies, XXXI (1962), pp.
129-39.
