The Prohibition Against Usury 17

no Christian can lawfully collect either interest or rent on his invest-
ment capital. This is economically preposterous, as well as biblically
unwarranted. This was also the official position of the Roman Cath-
olic Church until the sixteenth century, and it collapsed of its own
weight.'' It collapsed because it was not biblical.

Mr. Mooney’s book offered a challenge to me. He observed, cor-
rectly, that I had previously argued that the interest-free loans of the
Bible were (and are) charitable loans, I have always argued that
business loans were (and are) loans of a completely different ethical
and judicial character, and therefore lenders can legitimately ask for
an interest payment. But I had also said that no loan beyond seven
years is valid. He quite properly called me to account, If Rushdoony
and I appeal to Deuteronomy 15 in order to defend the seven-year
(or six-year) maximum on ali loans, yet Deuteronomy 15 is also the
basis of our arguing that morally compulsory charity loans—zero-
interest loans—are unique, then we are mixing our judicial catego-
ries, He asked; “Why do they not hold that only the debts of ‘poor’
brethren are to be cancelled, and [thus] infer from this that it is law-
ful for one to continue to exact the debts of the ‘rich’? The present
writer agrees with their views concerning the remission of debts,
particularly as cited above.”!?

When I read that, I instantly changed my views. In the twin-
kling of an eye, I abandoned my old argument that there must be a
seventh-year debt cancellation by civil law.!? Mooney is correct:
either Christians must accept the fact that there is no biblically valid
judicial distinction between charity loans and profit-seeking loans,
and therefore no biblically legitimate economic distinction, or else
we must interpret Deuteronomy 15 exclusively in terms of charity
loans. Either all loans are to be zero-interest loans, or else charity
loans alone are under the temporal restrictions of the sabbatical
year principle. Thus, from this point on, I will argue, to cite Mr.
Mooney, that “it is lawful for one to continue to exact the debts of
the rich.”

11. For a very clear summary of the transition in the Church away from the
medieval position, see John T, Noonan, “The Amendment of Papal Teaching by
Theologians,” in Charles E. Curran {ed.), Contraception: Authority and Dissent (New
York: Herder & Herder, 1969), pp. 41-75.

12. Mooney, Usury, p. 131.

13, This was not a paradigm shift, but it surely was a sub-paradigm shift, They
can take place very rapidly.
