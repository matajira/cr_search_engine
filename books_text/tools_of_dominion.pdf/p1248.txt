1240

Gommodus, 987
Common grace, Appendix A

bride price, 266

common curse, 959, 965, 976, 977

common ground, 993-94, 990

cross, 963

crumbs, 954, 971

curse &, 975

dispensational view, 691n

earlier grace, 968, 970, 971, 995

eternity &, 169n

final judgment, 978-79, 995

future grace, 995

increasing, 971

Jesus &, 287

Judaism, 1014

Kline on, 894

law &, 978

law's effects, 960

Methuselah, 975

removal of, 975

Rushdoony’s view, 970

sacraments &, 872

scarcity, 963

special grace, 979, 986, 995

special grace &, 981

Van Til’s view, 968-70

see also Special grace
Common ground, 990, 993-94
Common law, 518
Communion, 842
Communion meal, 621
Company town, 581
Competition, 786, 806, 1109
Computers, 854n
Concentration camp, 202, 407
Conception, 838
Concubinage

abolition, 267, 270

annulled, 275

bride price &, 648-49

defined, 246n, 249

escaping poverty, 252

grace, 251
legal status, 263, 264, 265, 278
O.T. only, 267

protection, 266

TOOLS OF DOMINION

Rachel, 262
rights of, 264
slave marriage, 258
Concubines, 51, 652
Confederacy, 371-72
Confession
escape device?, 626
precedes conviction, 326,
515-16
sanctions, 620, 622-29
timely, 627, 631
Confidence, 8-9, 39, 64,
1172-73
Conflict, 550
Congregation, 148, 152, 194,
229
Congressional chaplains, 1206
Connecticut, 980
Conquest, 122, 123, 159
Conquest, Robert, 169n.
Conrad, Alfred, 143n, 334n
Conscience, 690, 694-98, 708
(see also Self-government)
Conscription (see Draft)
Consequences, 331
Conservation, 594
Conservatism, 314
Consiantine, 146, 900
Constantinople, 1047
Constitution
ex post facto, 562
Fifth Amendment, 619n
godless, 1191
humanist, 800, 845, 846
oath, 1191-92
procedural only?, 1211
Consumer
pollution &, 564, 566
representatives of, 555
sovereignty, 614n, 693, 725-26
taxes &, 566
Consumer sovereignty, 725-26, 693
Consummation, 642, 644-45, 647
Contingency, 69
Continuity
aesthetic, 924-26
covenant, 897
