490 TOOLS OF DOMINION

granted a degree of safety by the authorities—not perfect safety,
since automobiles are still permitted in the area, but calculable safety.
They use the streets and sidewalks in terms of this greater degree of
safety. But pedestrians and other (slower) drivers are threatened by
those who refuse to honor the posted speed limit. They have made
decisions in terms of a given environment (“25 m.p.h.”), and a law-
breaker unilaterally alters this environment. He has, in effect, torn
down the protective fencing. He has “uncovered the pit.”

Fines and Restitution

What is the proper remedy? Most communities impose fines for
excessive speeding, with the fines proportional to the violations: a
higher fine for a higher speed. Can a fine be justified biblically? Yes.
The fine is imposed because a specific victim cannot be identified. No one was
injured by the speeding vehicle. Therefore, the civil government col-
lects a restitution payment in the name of all the victims who had their
lives and property threatened by the speeder’s act.

A statistically measurable risk of injury was transferred by the
speeder to those in the area of his speeding vehicle. This is another
case of “externalities”: people are being forced by the speeder to bear
risks involuntarily. The fines should be used to establish @ érust fund
for future victims of “hat and run” auto accidents, where the guilty party
cannot be located and/or convicted. The perpetrator of this “victim-
less crime” becomes a source of restitution payments for the subse-
quent victims of this same criminal act by an unconvicted agent.
Fines are therefore an acknowledgement by the authorities of the limits placed on
their knowledge. If law enforcement authorities were omniscient, all
restitution payments in a biblical society would go from the known
criminal to the known victim.

Fines should be imposed by local authorities for a specific pur-
pose: to make restitution payments to victims who reside in the same
general neighborhood. The civil government acts as a trustee for
future victims in cases where the authorities cannot locate or convict
the violator. Fines are not to be regarded as a normal source of revenue for the
civil government. The civil government must enforce biblical law with-
out prejudice, The bureaucrats’ fond hope of collecting municipal
operating revenues from fines creates prejudice. In a biblical com-
monwealth, taxes are supposed to finance civil government — predict-
able taxes that are collected from every responsible adult in a com-
munity. Citizens must know what law enforcement is really costing
