Wives and Concubines 255

dealt with Jacob and Dinah’s brothers in the matter of his son’s
seduction of Dinah, although the text indicates that Shechem was
also present (Gen. 34:6-11). In general, bargaining being what it is,
the two payments would have been similar in magnitude, except in
the case of a seduction. In this unique case, the bride price was far
more likely to exceed the normal dowry. Because Shechem was a
seducer, he was in no position to bargain: “Ask me never so much
dowry and gift, and I will give according as ye shall say unto me: but
give me the damsel to wife” (Gen. 34:12),

Why couldn't the father have agreed with the bridegroom on
allowing a marriage with neither dowry nor bride price? The girl
would not deplete her brothers’ inheritance by taking a dowry with
her, and the bridegroom would not be required to come up with the
bride price. After all, if the size of the bride price was even close to
the dowry, the marriage could presumably take place without either
of the ritual asset transfers: bridegroom to father, father to daughter.
What would have been wrong with this? There are three reasons:
J) the bride price served as a screening device; 2) it served as a ritual
sign of subordination; and 3) the dowry served as the woman’s pro-
tection against the short-sightedness of her husband and perhaps
also her father and brothers.

1. Screening Device

By the payment of the bride price, the groom was also acknow-
ledging that he was capable of being as good a supporter of the girl as
her father had been. He needed to assure her family of her future
economic protection, thereby releasing her father and brothers from
this legal responsibility. His ability to follow through on this cove-
nantal guarantee was revealed by his ability to pay the bride price.
The bride price was therefore an economic screening device for the
family of the girl. The bridegroom’s ability to pay a bride price was
evidence of his outward faithfulness to the terms of God’s covenant.®
The parents were transferring legal responsibility to a new cove-
nantal head. They were participating in the establishment of a new
family. Thus, the in-laws had to serve as God’s agents. Rushdoony

6. Those who deny that there has ever been any relationship between individual
productivity and personal faithfulness to the external requirements of the covenant
(Deut. 28:1-14) will reject this explanation of the usefulness of the bride price. Those
who think it makes sense as a screening device will be led to conclude that there
must have been a predictable relationship between economic performance and faith-
fulness to the covenants external requirements.
