The Economies of the Tabernacle 901

Thus, rather than expecting huge national cathedrals (a symbol
of nationalism) or international cathedrals (a symbol of ecch
empire), we should expect to see new buildings that coordinate the
activities of various regional Christian groups. They will have to be
functional yet magisterial. Instead of the sports arcnas— modern
man’s urban equivalent of the Roman arenas—we will see artistic,
educational, and meeting centers. They will not be primarily de-
nominational, but oriented toward dominion activities. They will
represent the activism of Christian civilization, not of the church
narrowly defined.

Churches may also build common structures in various regions,
comparable in sacrifice to the Mormon temples we find in many
cities throughout the world. They will reflect the “best” that a de-
nomination’s regional efforts can produce. We will also see national
and international architectural efforts, both secular and ecclesiasti-
cal. There will be regional, national, and international architectural
manifestations of the majesty of God on earth. But there will not be a
single center, as there was in Israel, for God has decentralized sacri-
fice and therefore His kingdom.

Such is my prophecy. Yet the very decentralization of Christian
culture is the would-be prophet’s stumbling-stone. The freedom that
Christianity provides invariably unleashes human creativity that defics
categorization in advance. What is most significant architecturally is
the stylistic freedom that Christian civilization offers within the over-
all constraints of finances and the restored image of God in redeemed
man. What is far less important is the accuracy of the prophecy.

  

stical

Conclusion

Local churches should embody visible elements of personal sacri-
fice, Modern concepts of long-term debt have reduced the psycho-
logical burden of present sacrifice, but long-term uncertainty and
the threat of debt servitude have accompanied the increase in church
indebtedness. The medieval churches sometimes took centuries to
construct, calling forth the sacrifices and talents of many gencra-
tions. Modern congregations build smaller, less beautiful, more effi-
cient structures, borrow heavily from fractional reserve banks to do
30, or sell usurious long-term bonds to church members,”? and then

22. Gary North, “Stewardship, Investment, and Usury: Financing the Kingdom
of God,’ Appendix 3 in R. J. Rushdoony, Jnstitutes of Biblical Law (Nutley, New Jersey:
Craig Press, 1973).
