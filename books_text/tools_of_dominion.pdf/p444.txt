436 TOOLS OF DOMINION

to these difficulties. We have few if any examples of Christian soci-
eties that have attempted to impose the “eye for eye” principle liter-
ally. The basic principle is clear: the punishment should fit the crime. By
allowing the victim to demand restitution in the form pleasing to
him, and by allowing the criminal to counter-offer something more
pleasing to him, the penalty comes close to matching the effects of
the crime, as assessed by the victim.

Each party gets to make one offer. If the victim offers a choice be-
tween penalties, the criminal chooses which one he prefers, or can
offer something completely different. If the victim specifies one and
only one penalty, mutilation, the criminal is entitled to counter-offer.
If the victim specifies only a money payment, but the criminal
prefers mutilation on an “eye for eye” basis, then he has the right to
choose mutilation.

The judges can establish the original restitution payment, whether
physical or economic, but the two affected parties should have the
final determination. This places limits on the State. The economic
assets involved in this auction process are transferred (or retained)
by the person who is more concerned with economic capital than
with physical mutilation. In this way, biblical justice is furthered.

The modern Western world has not imposed deliberate, perma-
nent physical mutilation on violent criminals. These criminals,
when convicted, have been imprisoned. They have been compelled
to pay fines to the State. In very few cases have they been compelled
to make monetary restitution to the victims. The result has been
escalating violence against private citizens, as well as the escalating
power of the State.

Biblical law imposes penalties on violent criminals that tend to
reduce the amount of violent crime. Biblical penalties encourage
criminals to count the cost in advance, In the case of “crimes of pas-
sion,” the convicted passionate criminals would be reminded of the
benefits of self-control. That stump at the end of an arm is a better
reminder than a string tied around a finger. So is the loss of several
years’ worth of savings, or several years as an indentured servant.
What men sow, that shall they also reap (Gal. 6:7-8). A godly society’s
criminal justice system, organized around the /ex éationis principle,
provides criminals with a glimpse of (or preliminary down payment
to) this cosmic principle of justice.
