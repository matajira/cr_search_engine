The Epistemological Problem of Social Cost 1117

world of zera transaction costs and zero rules establishing legal lia-
bility as if it would not be a world of turmoil, unpredictability, and
violence. It is the establishment of liability rules that makes civil or-
der possible. Social order is clearly too important a matter to be left
in the hands of economists, even Chicago School economists.

Rothbard’s Critique: Pure Subjectivism

One economist who has seen at least some of the implications of
Coase’s position is Murray Rothbard. Rothbard very early recog-
nized the reality of Robbins’ complaint against Pigou, namely, that
there can be no scientifically valid interpersonal comparisons of sub-
jective utility.*® He has written a critique of the Coasc theorem
which underscores some of the points I raised in the original draft of
this essay before I discovered Rothbard’s 1982 essay. But he goes to
the full logical conclusion of the subjectivist school, namely, that there
can he no such thing as social cost—not simply that economists cannot
measure it, but that it does not exist as a category of economics. He
discusses the case of the farmer whose orchard is burned by sparks
emitted by a passing train. His analysis focuses on the farmer’s sub-
jective costs that are imposed by the railroad’s aggression. Should
the State solve this dispute by forcing the railroad to pay the farmer
the market value of the lost trees?

There are many problems with this [Coase’s] theory. First, income and
wealth are important #0 the parties involved, although they might not be to un-
involved economists. It makes a great deal of difference to both of them
who has to pay whom. Second, this thesis works only if we deliberately ig-
nore psychological factors. Costs are not only monetary. The farmer might
well have an attachment to the orchard far beyond the monetary damage.

89. Rothbard, “Toward a Reconstruction of Utility and Welfare Economics,” in
Mary Sennholz (ed.), On Freedom and Free Enterprise: Essays in Honor of Laudwig von
Mises (Princeton, New Jerscy: Van Nostrand, 1956). This has been reprinted by
Liberty Press, Indianapolis, Indiana.

90. The Christian economist mast reject this thesis. There are indeed social costs
and social benefits, This is one reason why the Bible can and does specify certain so-
cial policies. They are beneficial for the covenanted community, But Rothbard’s
logic is correct: in terms of the presuppositions of modern, subjectivist cconomics,
there is no way to add up subjective costs ur benefits. In fact, if be were really rigor-
ous, he would admit that this conclusion applies even to the measurement of intra-
personal subjective utilities, since such measurements takes plave over lime, and
therefore we again confront that old nemesis, the index number of satisfaction—an
impossibility, given the premises of subjective utility.
