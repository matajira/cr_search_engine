Churls, 966-67, 979, 995
Circumcision
adoption &, 675
authority, 842
brand, 835
Christians &, 1035
citizenship &, 841-42
father-in-law, 256, 269
feasts of Israel, 828-29
judicial act, 837
rabbinic Judaism, 1022-23, 1035
slave, 173, 182
subordination, 835
subordination symbol, 256, 829
Cities, 568-69
Citizens, 671-72, 675
Citizenship
adoption, 834
basis of, 834
birth, 1201
capital, 1201
church membership, 842
circumcision &, 841-42
classical, 674
colonial, 1192-95
covenant &, 831
Enlightenment, 1191-92
farnily &, 831
feast, 823-31, 834
Fourteenth Amendment, 1200
humanist, 846-47
Kingdom, 229
privilege, 842
residency requirement abolished,
229
rites, 834
Citizen’s arrest, 297, 529
Citizen’s patrols, 353n
City-state, 674
Civil government, see State
Civil magistrate, 810
Givil War, 175, 974, 1200
Clan, 356
Clapp, Rodney, 13n, 48, 66, 121
Class, 765
Class-action suits, 603
Glass conflict, 420-21, 425

Index 1239

Classical civilization, 868-69
Classical education, 1200
Classical world, 157
Clay (potter’s), 68
Cloak, 686, 715, 737-38, 738-39
Coal, 572-73
Coals of fire, 778, 971, 960, 961-62,
957-58, 978, 982
Coase, R. H.
bibliography, 1100
cattle, 1106-7, 1151-52
externalities, 1105
inflicting injury, 1152
injustice’s costs, 1140
Jewish law, 1102
Lewin vs, 1107
Pigou &, 1101-5, 1149
property rights, 1153
protection money, 1150n
real world &, 1110-11
sparks, 1149-50
state nuisances, 592
Typhoid Mary, 1103
value, 1107
Coase theorem
irrelevant, 1103
justice &, 1109
Lione] Robbins vs., 1108
pollution, 1102
Rothbard vs., 1117-18
social costs of, 1155
utopian, 112
Cochrane, Charles N., 869n
Cockroaches, 859n
Code duello, 346, 350, 352, 357
Codrington, Christopher, 231
Coercion, 57, 173, 329, 334
ohen, Arthur, 1038, 1081, 1044
Cohen, Bernard, 573n
Coleman, Jules, it10n
Collateral, 219, 473n, 686, 715,
737-38
Collins, James, 559n
Colson, Charles, 120
Commentaries, 30-33
Committee, 398
Commodities, 362-64
