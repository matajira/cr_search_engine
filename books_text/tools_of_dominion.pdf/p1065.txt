Maimonides’ Code: Is Ht Biblical? 1057

Mastering a Book

There is no doubt in my mind that opening the Talmud does not
really open it. Opening Maimonides’ Code, however, does begin to
get the Talmud’s conclusions into the open, though not its various
modes of reasoning. When jewish scholars co-operated a generation
ago in making available an English-language translation of the Code,
they performed a service analogous to the translating of the Talmud.
But this service, being intellectual in nature, opened the formerly
linguistically locked gates. Inquirers today are free to enter the gateway
and snoop around at their leisure. They may not do justice to every-
thing they find. Or, from a different critic’s perspective, they may do
greater justice than some would prefer. But this is the cost of intellec-
tual progress. Debates arise, and they sometimes continue for cen-
turies without resolution. This is especially true of religious debates.

My part-time odyssey through Jewish literature has led me to
things that I appreciate (e.g., the exegetical insights of U. Cassuto
and S. R. Hirsch) and things that I do not appreciate (e.g., various
teachings regarding Jesus and Christians in general that are found in
the Talmud). The economic teachings of the Pentatcuch are not all
that casy to decipher at first glance. T am sure that Jewish commen-
tators have had the same sorts of problems that | have encountered.
They have come to their share of inaccurate conclusions, Who is to
challenge these conclusions? Jews only? Then are Christians’ conclu-
sions equally immune from challenges by Jews? The answer is clear,
I think, Anyway, it should be. We must all deal with the texts. If
God spoke them, as I believe He did, then we must all seek to under-
stand precisely what He said. Sometimes even higher critics can pin-
point a truth, Surely if they can, then those of us who take the texts
seriously as the word of God can comment on them, as well as on
each other’s comments.

In the Preface to a book on the ethics of Judaism by Unitarian
scholar R. Travers Herford, John J. Tepfer laments: “Over the cen-
turies the many-tomed Talmud, and kindred products of the carly
Rabbinic mind such as the Midrash, have been subjected to keen
scrutiny by numerous learned Christians, mainly, however, with an
eye to their value for Christian faith and dogma. The aims of these
men being largely apologetic, they drew invidious comparisons be-
tween the two faiths, pointing up what they considered to be the ab-
surdities of Rabbinic law and lore, and demonstrating the superior
