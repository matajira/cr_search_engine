360 TOOLS OF DOMINION

foundation of permanent chattel slavery; He did not abolish the
State. The State is a far more important institution historically and
judicially than private chattel slavery ever has been.

Sanctions and Moral Reform

The master is supposed to be an agent of moral reform; his train-
ing, support, and example are supposed to serve as the bondseryant’s
pathway back to self-government and productivity. The master
therefor cs lawful discipline in God’s name, including physi-
cal discipline. He brings covenantal sanctions. Because the servant is
made in God’s image, there are limits placed on the master’s author-
ity. This authority to impose sanctions is not unlimited; it is re-
strained by civil law and, as we shall see, by economic self-interest.

So severe is a Bible-sanctioned beating that a servant may even
die a few days later. This is regarded as a case of accidental death, and
the owner is not to be held responsible. It is acknowledged by God
that servants can be rebellious to the point that they may be severely
beaten. This is the passage that so disturbs Christian family counsellor
James Dobson: “Do you agree that if a man beats his slave to death,
he is to be considered guilty only if the individual dies instantly? If
the slave lives a few days, the owner is considered not guilty (Exodus
21:20-21)[?] Do you believe that we should stone to death rebellious
children (Deuteronomy 21:18-21)? Do you really believe we can draw
subtle meaning about complex issues from Mosaic law, when even
the obvious interpretation makes no sense to us today? We can
hardly select what we will and will not apply now. If we accept the
verses you cited, we are obligated to deal with every last jot and tit-
tle.”? He is correct; we are required to take seriously every last jot
and tittle.

exerci

   

 

affirm the biblical standard, namely, that civil law must not distinguish between the
morally deserving or undeserving nature of income recipients, so long as they did
not use force or fraud in gaining their wealth. The alternative is to conclude that
civil law must assume that cither the successful deserve special treatment at the ex-
pense of the less successful, or vice versa. The law must “take sides.” It must diserim-
inate. This makes the State arbitrary and dangerous.

If the case laws of Exodus du not distinguish between slaves and masters in terms
of their comparative moral stature or their prior outward circumstances, then there
is no way biblically to justify the creation of welfare State wealth-redistribution schemes
based on people’s comparative moral stature or their prier outward circumstances.

2. James Dobsan, “Dialogue on Abortion,” in Dobson and Gary Bergel, The Deci-
sion of Life (Arcadia, California: Focus on the Family, 1986), p. 14.
