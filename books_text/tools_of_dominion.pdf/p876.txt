868 ‘POOLS OF DOMINION

It is ethics, not growth as such, which determines the legitimacy or
illegitimacy of any given social growth process in a particular period
of history.

Greater numbers of people can and often do result in more efficient
ways to fulfill the cultural mandate. ‘The increasing division of labor
permits greater specialization and greater output per unit of resource
input.** Population growth is specifically stated to be a response of
God to covenantal faithfulness, but it is also a tool of dominion.
God’s ethical universe is one of positive feedback: from victory unto vic-
tory. This ethical standard has visible effects in history. Ethical de-
velopment, meaning progressive sanctification (“set-apartness”) in
terms of God’s law, is eventually accompanied by the compound
growth process, i.e., positive feedback, in human affairs.*

Entropy and lis Effects

Negative feedback is a limiting factor in a cursed world. The ani-
mals are not allowed to multiply and overcome the land. They are
restrained by man or by “the forces of nature,” meaning the environ-
ment's built-in limitations on the compound growth process. Negative
feedback is in part the product of God’s curse. There are indeed limits to
growth. Growth is not automatic. Growth is not a zero-price process.
But negative feedback—sometimes characterized as the so-called
‘law of entropy”—is not the characteristic feature of the universe.
The grace of God through faith in Jesus Christ is the characteristic
feature of the universe: redemption, resurrection, and restoration.

Entropy is a fundamental principle of physical science that states
that the movement of molecules tends to become incrcasingly ran-
dom over time. Less and less usable energy is available to perform
work as time goes on. When the idca of entropy —a scientific phe-
nomenon of hypothetically autonomous physical nature — begins to
turn the faith of a particular civilization toward pessimisrn about
mankind’s long-term future, then that civilization has come under
the judgment of God. * It was lack of faith in the future which brought
down the ancient city-states, including Rome, When classical civili-

44. This does not mean that a growing population is always an economic bless-
ing. Again, it is the ethical character of the people, not ratcs of biological reproduc-
tion, which determines the character of the growth process, cither curse or blessing.
45, North, és the World Running Down?, chaps. 7, 8.
46. Sce, for example, the book by Marxist critic and New Age commentator
Jeremy Rifkin, Entropy: A New World View (New York: Bantam, [1980] 1981). For a
detailed refutation, sce my book, Is the World Running Down?.
