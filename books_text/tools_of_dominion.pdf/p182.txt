174 TOOLS OF DOMINION

Baxter recognized this redemption aspect of slavery in 1673, and he
condemned anyone who would sell back a heathen slave to a heathen
trader. The original purchase of slaves is a “heinous sin to buy them,
unless it be in charity to deliver them.” Thus, they may not be
resold; rather, they are to be freed: “He that is bound to help to save
a man, that is faln [fallen] into the hand of thieves by the High-way,
if he should buy that man as a slave of the thieves, may not give him
up to the thieves again.”141 He was quite clear: “Make it your chief
end in buying and using slaves, to win them to Christ, and save their
Souls,”!42

The slave trade had been condemned by a papal bull as early as
1425; Christian slave dealers were to be excommunicated.!*? This
was seldom enforced, and traders ignored it. There is little doubt
that the slave trade was regarded by English-speaking people as a so-
cially very inferior occupation, despite the fact that the rich and up-
wardly mobile used the services of the traders. (There were very few
English slave traders until 1660.)!* There was a kind of residual
awareness of the inherent immorality of the trade in New Testament
times. But this residual awareness was insufficient to bring the judi-
cial condemnation of slavery itself—a classic example of a deep-
seated moral and intellectual schizophrenia that afflicted English
Protestants for centuries. !°

Thomas and Bean argue that the trade as a whole was not uniquely
profitable because of nearly open entry and the impossibility of es-
tablishing property rights to the various markets. As is true in any
profession or industry, individual slave traders reaped huge profits,
but the trade as a whole did not. The slave-owners’ profits from the
slaves led to higher prices for slaves. Profits for the slave traders were
eaten up by expenses in purchasing them and transporting them.
Profits from the slave traders went to the black African “fishers of
men.” Step by step, the slave trade’s profits eroded. Thomas and
Bean conclude with the following remarkable but economically

141, Richard Baxter, 4 Christian Directory (London: Robert White for Nevil Sim-
mons, 1678), Part II, Christian Orconomicks, p. 73, The first edition appeared in 1673.

142. Thid., p. 74.

143, David Brion Davis, The Problem of Slavery in Western Culture (Ithaca, New
York: Cornell University Press, 1966), p. 100.

144, Lester B. Scherer, Slavery and the Churches in Early America, 1619-1819 (Grand
Rapids, Michigan: Eerdmans, 1975), p. 20.

145. This same hostility to slave traders may have been present in other societies;
I do not know one way or the other.
