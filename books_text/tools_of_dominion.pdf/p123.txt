A Biblical Theology of Slavery M5

book of political philosophy in the modern West.12 What Rousseau
was struggling with was an apparent anomaly: the presence of slavery
in a world that proclaims liberty. It was a variation of a theological
dilemma that had plagued the early church fathers from the begin-
ning: the existence of slavery in a world of people born equal in the
eyes of God.*? The early church pointed to sin as the cause of slav-
ery, which it surely is. (This satne explanation was familiar and
widespread in all Protestant circles in seventeenth-century England.)'*
But this only pushes the problem back one step: What is the biblical
meaning of equality? Is all inequality the product of sin? For exam-
ple, is the subordination of the Second Person of the Trinity to the
Father an aspect of sin? Is the subordination of the Holy Spirit to
both Father and Son the product of sin? Obviously not. Then what
is the meaning of equality?

Equality means the equality of being: within the Trinity, and within
all of mankind, The three persons of the Trinity are of the same sub-
stance and majesty, co-eternal. Similarly, all men are made in God’s
image, and therefore are equally responsible before the Creator. In
neither case is equality the equality of function, for there is no equality
of function, Covenantal relationships are always hierarchical, This
leads us inevitably to the two-fold doctrine of the Trinity: the on-
tological Trinity (equality of being) and the economical Trinity (subor-
dination with respect to historical function).!° Both doctrines must
be affirmed in order to preserve Christian orthodoxy. Jesus said that
anyone who has seen Him has seen the Father (John 14:9). “I and
my Father are one” ( John 10:30). Yet He was also a good and faithful
servant of His Father, doing His Father’s business (Luke 2:49), re-
vealing everything that His Father had shown Him (John 6:28;
15:15b). There is equality of being but also functional subordination,
even within the Godhead; how much more among men! ‘There is
always hierarchy,

12, Writes Robert Nisbet: “Plato may be the essential architect of this vision of
community, but no one has equalled Rousseau’s role in making it the single most at-
tractive vision for modern man. Rousseau is the very archetype of the political mod-
em, the cmbodiment of what might be called the modernist revolt in politics.”
Robert Nisbet, The Social Philosophers: Community und Conflict in Western Thought (New
York: Crowell, 1973), p. 145.

13. R. W. Garlyle and A. J. Carlyle, A History of Mediaeval Political Theory in the
West, 6 vols. (London: Blackwood, 1962), L, pp. 119-20.

44. Winthrop D. Jordan, While Over Black: American Altitudes Toward the Negro,
1550-1812 (Chapel Hill: University of North Carolina Press, 1968), pp. 54-55.

15. Cornelius Van Til, Apologetics (Syllabus, Westminster Seminary, 1959), p. 8.
