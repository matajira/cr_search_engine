802 TOOLS OF DOMINION

be governed by the capitalist principle of “highest bid wins.” No man
is supposed to be able to pay a court to gain his preferred decision,
nor should people be able to “shop around” in search of a court more
likely to be favorable to them.

A church government has been granted a unique monopolistic
authority over those who have voluntarily covenanted with it (or
whose parents have). A civil government has been granted a unique
geographical monopoly over those who have covenanted with it (or
whose parents have), The State represents God to those within its
geographical boundaries, and it possesses an authority defined by
constitutional law or custom. Thus, a court is not governed by the
principle of “highest money bid wins.” To imagine that such a princi-
ple governs the courts is to imagine that God honors the same princi-
ple in His rendering of judgment. It would mean that rich people
could buy a decision from God. But they cannot do this, even if they
owned the whole world, “For what shall it profit a man, if he shall
gain the whole world, and lose his own soul?” (Mark 8:36). God
honors Himself alone by honoring His law. All ren are judged by
His law. He does not respect persons, including those who could offer
him a higher bid. The basis of rewards in eternity is righteousness.

Highest Ethical Bid?

Now, it might be argued that the principle of “highest bid wins”
still operates in God’s courtroom of final judgment, in the sense that
righteousness should be the irue “coin of the realm,” and therefore
those who pay the most, ethically speaking, will receive the highest
rewards (I Cor. 3:13-15). But there is a fundamental difference in the
operating principles of the competitive market for goods and the
closed monopoly of God’s final judgment. The free market for goods
operates in terms of objective prices, irrespective of one’s relative ca-
pacity to pay. In contrast, God’s monopoly of final judgment oper-
ates in terms of one’s objective performance relative io one’s gifts. The
story of the widow's two mites informs us of this latter principle.
Those rich people who gave much into the treasury did not give
nearly so much as the poor widow who cast in two small coins, for
this was all she possessed. Jesus said, “For all they did cast in of their
abundance; but she of her want did cast in all that she had, even all

 

Law,” New Individualist Review, | (Winter 1962), pp. 37-40; reprinted in one volume
by Liberty Fund, Indianapolis, Indiana, 1981, pp. 163-66; Rothbard, For « New
Liberty; ‘The Libertarian Manifesto (rev. od.; New York: Collier, 1978), pp. 227-34.
