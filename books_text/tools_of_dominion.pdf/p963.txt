Common Grace, Eschatology, and Biblical Law 955

1. There is a “favorable attitude of God toward mankind in general,
and not alone toward the elect, . . .” Furthermore, there is “also a certain
favor or grace of God which he shows to his creatures in general.”

2, God provides “restraint of sin in the life of the individual and in so-
ciety, .. .”

3. With regard to “the performance of so-called civic righteousness... the
unregenerate, though incapable of any saving good . . . can perform such

civic good.”?

‘These principles can serve as a starting point for a discussion of
common grace. The serious Christian eventually will be faced with
the problem of explaining the good once he faces the biblical doc-
trine of evil. James 1:17 informs us that all good gifts are from God.
The same point is made in Deuteronomy, chapter 8, which is quoted
as the introduction to this essay. It is clear that the unregenerate are
the beneficiaries of God's gifts, None of the participants to the debate
denies the existence of the gifts. What is denied by the Protestant
Reformed critics is that these gifts imply the aver of God as far as the
unregenerate are concerned. They categorically deny the first point
of the original three points.

For the moment, let us refrain from using the word grace. In-
stead, let us limit ourselves to the word gift. The existence of gifts
from God raises a whole series of questions:

Does a gift from God imply His favor?

Does an unregenerate man possess the power to do gaod?

Does the existence of good behavior on the part of the unbeliever deny
the doctrine of total depravity?

Does history reveal a progressive separation between saved and lost?

Would such a separation necessarily lead to the triumph of the unregen-
erate?

Is there a common ground intellectually between Christians and non-
Christians?

Can Christians and non-Christians cooperate successfully in certain
areas?

Do God's gifts increase or decrease over time?

Will the cultural mandate (dominion covenant) of Genesis 1:28 be ful-
filled?

3. Cornelius Van Til, Common Grace (Philadelphia: Presbyterian and Reformed
Publishing Co., 1954), pp. 20-22, This was reprinted in Van Til, Common Grace
and the Gospel (Nutley, New Jersey: Presbyterian & Reformed, 1974), same pagina~
tion.

 
