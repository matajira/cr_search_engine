6 TOOLS OF DOMINION

nation, plus running debates in footnotes, to clarify scholarly points.
Better to write a long book that can be digested in a series of bite-sized
portions than a highly condensed book that takes enormous intellec-
tual energy and vast background knowledge in order to decipher.

I had to make this book long in order to make each section coher-
ent. Writing which is highly condensed is too difficult to read, too
easy to skip over key parts in some argument, and therefore too easy
to misinterpret. On the other hand, long, involved arguments are
difficult to follow and remember. Therefore, I have broken up long
arguments into manageable portions by adopting a liberal use of
subsections and sub-subsections, plus summaries at the end of each
chapter, and in my lengthy chapter on pollution (Chapter 18), at the
end of each major section. I strongly recommend that whenever you
see a bold-faced subhead, you should pay attention to it; the same
goes for the italicized sub-subheads. They are there to help you get
through each argument, as well as for convenient reviewing.

This book is supposed to be consumed in bite-sized portions; I
have therefore done my best to make every mouthful both tasty and
nourishing. To keep readers in their chairs and turning the pages of
this book, I have done my best to put useful information on every
page. There is no fluff in this book. The extended footnotes are also
filled with all sorts of choice tidbits that would otherwise be quite
difficult to locate. I also use footnotes for running debates that do not
belong in the main text. I sometimes settle scores with my critics in
the footnotes. Footnotes can be fun!

Why an Economic Commentary?

I have explained in the Introduction to my economic commen-
tary on Genesis why I began this project in 1973.8 I presented there
my case for the whole idea of a specifically economic commentary.
Basically, my reason is this: the Bible presents mankind with a God-
mandated set of social, economic, educational, political, and legal
principles that God expects His people to use as permanent blue-
prints for the total reconstruction of every society on earth. The Do-
minion Covenant provides a model of what kind of exegetical materials
can and must be produced in every academic field if Christians are
successfully to press the claims of Christ on the world. Since the pub-
lication of the first two commentaries on Exodus, I have also edited

8. Initial presentations of my economic commentary on the Pentateuch appeared
monthly in the Chalcedon Report, from 1973 until 1981.
