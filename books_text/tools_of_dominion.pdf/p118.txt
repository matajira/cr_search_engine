110 TOOLS OF DOMINION

without faith in the God who gave them, rebellious mankind cannot
long sustain the external blessings of God.

The modern world, including the Christian world, does not be-
lieve this. People think that they can have freedom without Christ,
and prosperity without adherence to the external requirements of
biblical law, ‘They really do believe in the autonomy of man. They
really do believe that “my power and the might of my hand hath gotten
me this wealth” (Deut. 8:17). The problem is, God has warned us
that when we say this, judgment is near (Deut. 8:18-20).

We find Christians who argue vehemently that “Christians can
live under any economic or political system!” True, so we reply:
“Even the Old Testament legal system?” And we are told emphatically,
“No! Christians can live under any economic and political system ex-
cept the Old Testament legal system.” Anything is acceptable, there-
fore, except what God requires. So they reply, as Satan replied to
Eve, “Hath God said?” Yes, God hath said!

Cornelius Van Til once wrote that if a covenant-breaking man
could tune in his radio to only one station that did not testify of God
to him, he would listen only to that station. No such station exists,
Van Til says.* The whole creation testifies to the Creator (Rom.
118-23). We can extend this insight to social theory: if antinomian
Christians could live under any system of politics and economics
that did not testify to them of what God really requires, they would
choose to live only under that system. They have said so repeatedly.
But they cannot escape the voice of God. They cannot escape the re-
quirements of Old Testament law. In short, they cannot escape the
Bible. ‘They are inevitably under the covenant’s blessings and cursings.

It is time for Christians to place themselves consistently and
forthrightly under the ethical terms of the covenant, and affirm the
continuing judicial validity for all societies of the case laws. They
can begin with the case laws of Exodus.

53. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987), ch. 6.

54. A variation of this analogy appears in Common Grace and the Gospel (Nutley,
New Jersey: Presbyterian & Reformed, [1954] 1974). pp. 53-54.
