Cornmon Grace, Eschatology, and Biblical Law 965

must operate in a world in which the tares, the unregenerate, are
operating. The servants (angels) instantly recognize the difference,
but they are told not to yank up the tares yet. Such a violent act would
destroy the wheat by plowing up the field. To preserve the growing
wheat, the owner allows the tares to develop. What is preserved is
historical development. Only at the end of the world is a final separation
made. Until then, for the sake of the wheat, the tares are not ripped out.

The rain falls on both the wheat and the tares. The sun shines on
both. The blight hits both, and so do the locusts. Gommon grace and
common curse: the law of God brings both in history. An important
part of historical development is man’s fulfillment of the dominion
covenant. New productive techniques can be implemented through
the common grace of God, once the care of the field is entrusted to
men, The regularilies of nature still play a role, bul increasingly fer-
tilizers, irrigation systems, regular care, scientific management, and
even satellite surveys are part of the life of the field. Men exercise in-
creasing dominion over the world. A question then arises: If the
devil’s followers rule, will they care tenderly for the needs of the godly?
Will they exercise dominion for the benefit of the wheat, so to spcak?
On the other hand, will the tares be cared for by the Christians? If
Christians rule, what happens to the unrighteous?

This is the problem of differentiation in history. Men are not passive.
‘They are commanded to be active, to seek dominion over nature
(Gen. 1:28; 9:1-7), They are to manage the field. As both the good
and the bad work out their God-ordained destinies, what kind of de-
velopment can be expected? Who prospers most, the saved or the
lost? Who becomes dominant?

The final separation comes at the end of time. Until then, the two
groups must share the same world. If wheat and tares imply slow
growth to maturity, then we have to conclude that the radically dis-
continuous event of separation will not mark the time of historical
development. It is an event of the last day: the final judgment. It is a
discontinuous event that is the capstone of historical continuity. The
death, resurrection, and ascension of Christ was the last historically
significant series of events that properly can be said to be radically dis-
continuous (possibly the day of Pentccost could serve as the last earth-
shaking, kingdom-shaking event). The next major eschatological
discontinuity is the day of judgment. So we should expect growth in
our era, the kind of growth indicated by the agricultural parables.*

5. Gary North, Moses and Pharaoh: Dominion Keligion vs. Power Religion (Tyler,
‘Texas: Institute for Christian Economics, 1985), ch. 12: “Continuity and Revolution.”
