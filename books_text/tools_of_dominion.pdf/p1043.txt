Maimonides’ Code. Is It Biblical? 1035

Hermeneutics: An Inescapable Concept

Commenting on anything requires a principle of interpretation.
This is true of Bible commentaries. Principles of interpretation
differ, and sometimes very sharply. This means that rival bermeneu-
tical principles can and do become divisive. That, too, is the price of
open inquiry. It is a price that must be paid on both sides. There is
no way to reconcile these rival principles of biblical interpretation: 1)
Jesus as the sole fulfillment of Old Testament messianic prophecies
vs. Jesus as a false prophet and blasphemer, for which He was law-
fully executed; 2) the New Testament as the sole authoritative com-
mentary on the Old Testament vs. the New Testament as false
prophecy; 3) Christians as the only true covenantal heirs of Abra-
ham vs, Jews as the only true covenantal heirs of Abraham. It is the
ancient debate, recently revived politically in the state of Israel, over
the question, “Who is a Jew?” It is a debate over the truth of Paul’s
assertion: “For we are the circumcision, which worship God in the
spirit, and rejoice in Christ Jesus, and have no confidence in the
flesh” (Phil. 3:3). Only theological liberals on both sides of the
debate can sensibly play down these differences, since liberals do not
accept the truth of either religion’s set of hermeneutical principles.

This essay deals with Orthodox Judaism and its relation with or-
thodox Christianity. Orthodox Christianity is no longer the domi-
nant stream of Christianity in the West, just as Orthodox Judaism is
no longer the dominant stream of Judaism outside of the state of
Israel, and which is in sharp political conflict with secular Judaism
inside that nation. Always in the background of the life of the ortho-
dox Christian and the Orthodox Jew are the liberals “within the
camp.” The Orthodox Christian does not believe that liberal, main-
stream Christianity is really Christianity, just as the Orthodox Jew
does not believe that mainstream Judaism is really Judaism. Van
Til is correct in his assessment of the theological unity of the liberal
Jew and the liberal Christian:

 

123. In Judaism, this question is really, “Who is the rabbi?” The rabbi sanctions
marriages and therefore the legitimacy of the children.

124. There is a problem here for Bible-affirming Christians, They normally do
accept us valid the baptisms of converts out of mainstream churches. They would
nat accept Mormon baptism as valid. So, to some degree, they do accept main-
stream churches as still Christian, For the Orthodox Jew, the determination of who
is a Jew is established by examining the training of the Rabbi who circumcised him
or circumcised her father or husband.

 
