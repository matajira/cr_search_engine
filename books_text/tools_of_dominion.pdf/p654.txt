646 TOOLS OF DOMINION

The Father's Status

Why does the seducer owe moncy to the father, rather than to the
girl? Because the father is legally liable for the girl and for his
‘family’s reputation. But this liability is limited by the extent of his
knowledge. He cannot know everything she does. He always needs
better information. Biblical law creates incentives for the transfer of
appropriate knowledge to those who are God’s legally responsible
representatives.

The daughter’s original consent to the act of seduction does not
itself constitute whoredom. Her failure to tell her father immediately
of the seduction is what constitutes her whoredom, for whoredom (as
distinguished from adultery) is defined biblically as sexual bonding
apart from a marriage vow.’ If she accepts the legitimacy of her sex-
ual union apart from a marriage vow, then she has become a whore.
She had taken the vow implicitly by her consent to the act, but her
unwillingness to tell her father of the act that constituted her vow
thereby establishes her covenantally as a whore.

She remains “in her father’s house” (Num. 30:16), and under his
covenantal jurisdiction, yet she is no longer a virgin. The presence of
this unannounced non-virgin daughter brings disgrace on her house
and on Israel when she is discovered. Because she has willfully
broken her covenantal bond with her father, but has refused to
acknowledge her implicit vow with her seducer, biblical law con-
siders her a whore. The capital penalty can subsequently be imposed
if she marries another man who has been asked to pay a bride price
to her father, if the new husband immediately decides to prosecute
her (Deut. 22:13-19).

If the father had known of her act, yet took no steps to receive
payment from the seducer, he thereby consented to the theology of
sexual bonding without covenantal bonding. He has also become an
implicit idolater. He has no legal excuse. He has identified himself as

7. This indicates that Jesus’ announcement that divorce is legitimate only because
of fornication (parncia) must have been based on a far broader definition of porneia
than mere sexual bonding. The King James translators too narrowly defined the
word as fornication. Under Old ‘Testament law, once a marriage had taken place,
fornication was always defined as adultery, a capital offense. Obviously, divorce
through execution was possible, and Jesus would not have had to raise the issue. He
would have used the term for adultery rather than fornication. Rushdoony, The it-
stitutes of Biblical Law (Nutley, New Jerscy: Craig Press, 1973), pp. 406-14; Greg L.
Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg, New Jersey: Presbyter-
ian & Reformed, 1984), pp. 105-9.
