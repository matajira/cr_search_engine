Servitude, Protection, and Marriage 219

Jaw. This transfer of office was legally possible only because marriage
is judicially a form of adoption 37

We know this must have been the case because of the laws gov-
erning vows. A woman could take a vow, but the male head of
household, father or husband, had to affirm it within 24 hours in or-
der for it to be judicially binding before God (Num. 30:3-14). (This
law appears, appropriately, in the Book of Numbers, the book corre-
sponding to point four of the covenant: oath/sanctions.) Only a
widow could make a judicially binding oath on her own (Num.
30:9). This indicates that a woman, unless a widow, was always
legally under the hierarchical rule of a man. She was under a man’s
judicial authority: the office of household head. This office could not
be transferred except through adoption or temporary maidservice. (A
daughter could be used as collateral for a charity loan. A minor son
could be, too, which is why the widow approached Elisha when the
creditor threatened to make her sons into bondservants [II Ki. 4].
Elisha did not say that the creditor had broken the law. Instead, as
her mediatory kinsman-redeemer [her pastor], he provided a mira-
cle for this widow; oil that could be sold in order to redeem the debt.)

Daube's Hermeneutic: From Law: to Theology

The prominent Old Testament scholar David Daube has gone so
far as to argue that the original right of self-redemption by the
Hebrew bondservant was strictly limited to cases of ownership of
Hebrews by resident aliens. Daube self-consciously prefers to
argue from the legal to the theological, 1? but he then fails to deal with
the actual judicial standards regarding redemption. This is why we
need to argue theologically as well as judicially; otherwisc, we will
yniss important aspects of both the theological and judicial character
of God’s revelation, Daube’s hostility to theology is so great that he
even argues that the priests and prophets who supposedly wrote the
Pentateuch in the eighth century 8.c. (or later) actually invented the
idea of God’s liberating His people from guilt.” Again, he is arguing
from the judicial to the theological: a view based on the prior exclu-
sively judicial concept of God as the liberator from physical bondage

17. See the section, “Marriage and Adoption,” in Chapter 6, pp. 259-62.

18. David Daube, Studies in Biblical Law (Gambridge: At the University Press,
1947), p. 43.

19. Ibid., pp. 1-3, 43.

20. See below, Appendix Ci: “The Hoax of Higher Criticism.”
