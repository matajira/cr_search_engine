A Biblical Theology of Slavery 185

fulfilment of Old Testament prophecy. As [Leslie] Stephen sums up the
pragmatic resolution, Englishmen could still believe everything in the
Bible, “but nothing too vigourousl:
“true enough for practical purposes.
So far as slavery is concerned, the Deists pointed toward the future posi-
tion of [Thomas] Paine and Garrison. Thus God, by definition, was good
and just. Yet the God of the Bible had authorized slavery as a divine pun-
ishment, along with such barbarities as the stoning to death of stubborn
children who refused to obey their parents. It followed that the Bible could
not be God’s word. [Matthew] Tindal specifically compared the Jewish en-
slavement of Canaanites to the Spanish conquest of Mexico. The claim of
divine sanction should not Jessen one’s outrage over either crime.27

     

if the book was not flawless, it was

Ah, yes, those mean old Spaniards. They destroyed the Aztec and
Inca cultures in which human sacrifice and ritual cannibalism were
practiced regularly, where priests on top of pyramids would rip the
hearts out of living sacrificial victims and eat them. In short, the Span-
iards did to the Aztecs and Incas what the Hebrews did to the Can-
aanites, and for similar reasons: i¢ put an end to outrageous cultural sin,

The Spanish used slavery to further empire; the Hebrews were
not to establish an empire based on pagan slave labor, so God did not
allow them to make slaves of the Canaanites. Instead, God told them
to kill or expel from the land evcry Canaanite. In both cases, how-
ever — Spanish slavery and Hebrew slavery —“tender-hearted” (read:
soft-headed) commentators many centuries later are appalled, not at
the ritual murders that preceded the conquests, but at those who put
a stop to ritual murder. For well over three centuries, the Spaniards
have been seen as the cannibals, and the cannibals have been seen as
the victims.?°*

Progress in Redemptive History

Nevertheless, this continuing bias of contemporary historiography
does not relieve Christians of their difficulty: to explain how it was
that Christianity did not pioneer the abolitionist movement, either
in England or the United States, cven though evangelical Christians
later became ardent abolitionists in both nations.?°? Why did slavery

207. Davis, The Problem of Slavery in the Age of Revolution, p. 528.

208, For a corrective to the “Black Legend” story of Spanish atrocities in the New
World, see Prof. Philip Wayne Powell's book, Tie of Hate: Propaganda and Prejudices
Affecting the United States Relations with the Hispanic World (Vallecito, California: Ross
House, 1985). Published originally in 1971 by Basic Books, New York City.

209, Bertram Wyatt-Brown, Lewis Tappan and the Evangelical War Against Slavery
(Cleveland, Ohio: Case Western Reserve University, 1969).
