Proportional Restitution 525

need special protection from thieves. A thief who slaughters an ox or
sheep is subject to more stringent penalties. The higher penalty
tends to restrain him in his blood-letting. This is a more strictly eco-
nomic argument, one based on the economic effects of the law. Sec-
ond, both sheep and oxen are symbolic in the Bible of mankind: oxen for
men of power or office, and sheep for dependent, spiritually helpless
people. Oxen are normally peaceful, dominion beasts that are used
for plowing the fields, never for war. Sheep are passive creatures that
Tequire special care on the part of shepherds. Thus, as archetypes of
man in his relationship to God—creatures in need of care—oxen and
sheep receive special consideration by the law,

Why a five-fold restitution payment for oxen? Why not four-fold?
Probably because oxen are beasts of burden and therefore living
tools of dominion. They are dependent,®’ though not so dependent
as sheep, but they are also symbolic of God’s dominion covenant.
The number five is associated with the covenant in the Bible. Also,
Israel marched in military formations based on the number five.**
The number five is associated with dominion. By killing a stolen ox,
the thief is symbolically sacrificing another person’s economic future
for the sake of his own present enjoyment. This is what Satan at-
tempted to do to Adam, and only the grace of God in Christ pre-
vented Satan’s successful slaughtering of humanity.

This law of restitution singles out oxen and sheep as being spe-
cial creatures. Other passages in the Bible do the same. What the
stringent restitution penalties of Exodus 22:1 point to is a general
principle: how you treat oxen and sheep is indicative of how you treat other
men. The ox is worthy of his hire, how much more a man! The sheep
is helpless, and is deserving of protection; how much more a man! A
society whose legal order protects oxen and sheep from thieves who
would slaughter them is a society whose legal order is likely also to
protect men from oppression, kidnapping, and murder. A biblical
social order offers special protection to oxen, sheep, and men.”

37. [believe that the male ox in this case law is castrated and not a bull. Castra-
tion reduces its threat to men, yet the animal’s strength can still be harnessed for
man’s punposes, It is more dependent on man than a bull would be.

38. James B. Jordan, The Sociology of the Church (Tyler, Texas: Geneva Ministries,
1986), pp. 215-16.

39. David Daube’s comments on the four-fold and five-fold restitution require-
ments acknowledge none of this. Instead, he returns to his favorite theme, like a dog
returns to its vomit: the ‘later addition” thesis. He contrasts the two-fold restitution
requirement with the four-fold and five-fold requirements. The higher penalties are
