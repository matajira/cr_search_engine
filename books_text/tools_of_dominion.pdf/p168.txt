160 TOOLS OF DOMINION

others on a massive scale; as this process continued, the nation laid
the foundations of its own future enslavement. By the second cen-
tury B.c., the slave system in Rome was in place.‘°* From that time
forward, Roman citizens steadily lost their freedoms to the State.
The Roman Empire was based to a great extent on the legacy of
slavery from the Republic. The successful wars abroad became a
snare to Rome. In 262 B.c., Rome captured 25,000 Carthaginians
in the first of a series of mass enslavements during the Punic wars. A
generation earlier, in 296 B.c., as many as 40,000 people were taken
captive by Rome during the third Samnite war.1!° From 200 to 150
B.c., Rome may have taken prisoner as many as 250,000, although
many must have been ransomed back. At least 150,000 people were
enslaved from the 70 towns of Epirus in 167 s.c.!!! What is often ig-
nored is the loss of life that these wars cost Rome.'!? In a sense, the
free peasantry was being replaced by slaves. This process continued
under the Empire, probably through accelerating debt bondage.''>

Finley argues that military conquest led to the creation of large
agricultural estates in Rome,'"* but he denies that military conquest
has been the primary source of slaves historically. “Comparative evi-
dence reveals that a necessary condition for an adequate supply of
slaves is not conquest but the existence, outside the society under
consideration, of a ‘reservoir’ of potential slave labour on which the
society can draw systematically. . . .”145 This is an odd argument,
since the ability to inflict military defeats on a nation’s cnemies is
what creates the so-called reservoir. The victors begin to visualize
the defeated and the easy-to-defeat as potential sources of slaves.
Finley dismisses as “irrelevant” the fact that societies fight against
each other and then sell the captive losers to distant slave societies.
The key, in his view, is the existence of a market-driven slave trade
system. In short, “the demand for slaves precedes the supply.”!1° He
neglects to ask what the economist always asks: Demand af what
price? A strong military reduces slave prices to the rich slave-buying
minority if the costs of financing military conquests are borne by a
broad base of taxpayers.

109, Finley, Ancient Slavery, p. 131.
110. Ibid., p. 83.

111, Westermann, Slave Systems, p, 62.
112, Tbid., p. 61.

113. Finley, Ancient Slavery, pp. 143-44.
114. Ibid,, p. 84.

115. Tbid., p, 85.

116. Tbid., p. 86.
