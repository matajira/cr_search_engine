906 TOOLS OF DOMINION

The sword of the state executes according to the judgments rendered by the
priests. (In the New Covenant age, every believer is a priest, just as
the Old Covenant believers became priests by taking the Nazarite
vow. In our system, the priests render judgment by sitting on a jury,
and then the state executes the judgment.)”5

The point should be clear: the covering or atonement payment of
Exodus 30 has nothing to do with the civil government. It is not a tax
at all. “Thus, the military duty is priestly, and a duty of every
believer-priest. Both Church and state are involved in it, since the
Church must say whether the war is just and holy, and the state must
organize the believer-priests for batile. The mustering of the host for
a census is, then, not a ‘civil’ function as opposed to an ecclesiastical
one, and the atonement money of Exodus 30 is not a poll tax, as
some have alleged.”*

Jordan is being polite (or cautious) by refraining from mention-
ing the target of his exposition, but readers may not fully understand
the nature of the theological problem unless they know the specifics
of the debate. Jordan’s target is R. J. Rushdoony,

Rushdoony’s Theory of the Civil Head Tax

There has been considerable confusion about this in recent years
because of Rushdoony’s insistence that this atonement payment be-
came a civil head tax after the construction of the tabernacle, “It was
used to maintain the civil order after the tabernacle (the throne room
and palace of God’s government) was built.”? He offers no evidence
for this assertion, On the face of it, it seems utterly implausible. How
did such a shift in the locus of taxing sovereignty take place? How
did the State become the recipient of an atonement payment, there-
by converting a ransom paid to God through the priesthood into a
head tax collected by the State? This would implicitly transfer sovereignty
from the church to the State, a procedure totally at odds with everything
else Rushdoony has written about illegitimate State power.

He correctly observes that this payment was an atonement pay-
ment to the tabernacle which was paid by those going into battle,
and he cites other commentators to support his point--a relatively

5. Dbid., pp. 231-32.

6. Ibid., p. 232.

7. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1972), p, 50.
