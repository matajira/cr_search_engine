266 TOOLS OF DOMINION

dren. Fatherhood in all cases was by adoption, not biology. This
legal principle reflects our own covenantal status before God: we are
either disinherited children because of Adam’s sin, or else we are
adopted children in God’s household because of Christ’s death and
resurrection (John 1:12),

Then in what visible way was a former concubine different from
a former wife? Only in terms of her capital. She took no dowry with
her when she left, for she had brought no dowry to the marriage
when she came. A bride price transaction without a dowry for the
daughter in fact was @ servant purchase price. A concubine had no per-
sonally held economic protection. If treated unequally compared to
another wife, she could return to her father’s household, and she
could marry again. She could also remain single and alone, although
that was rare in any agricultural society, except for a few urban oc-
cupations such as tavern-keeping and prostitution, and the court
would probably remove her children from her if she became a prosti-
tute. Nevertheless, an honest, moral woman was legally able to
leave her husband’s house with her children: her new dowry.

She could return to her father’s household without a sense of be-
coming a needless burden, because her father had been paid. He
had kept all of the bride price, which made it more strictly an eco-
nomic transaction. She had borne the risk of winding up with a hus-
band who mistreated her, so her father could have no legitimate
complaints about her returning home.

New Testament Applications

Jesus Christ paid the bride price to God through His death at
Calvary. This is the basis of His marriage to the bride, the church. It is
also the basis of all marriages through God’s common grace.?! Christ
paid the bride price for all of humanity, for each individual, for Old
Covenant Israel, and for New Covenant Israel. It was the highest
price that has ever been paid. Old Covenant Israel looked forward to
this payment, while New Covenant Israel now looks backward.”?

21. Ifwe do not maintain that Christ's payment of the bride price is the foundation
of all marriages through common grace, then we must conclude that there is still a
valid form of concubinage among non-Christians, We would have to argue that only
Christian brides are exempt from the requirements of the bride price/dowry system.

22, Genetic Old Covenant Israelites (the Jews of today), described in Romans il as
the branches that were cut off (v. 17-19), still look forward to this payment, but Gad re-
qtires them to join themselves to the church and begin to look backward. There is only
one bride, the church of Jesus Christ. God is not a polygamist. ‘The old bride, national
Israel, was executed for her whoredoms in a.v. 70. See David Chilton, The Days of Vor-
geance: An Exposition of the Book of Revelation (Ft, Worth, Texas; Dominion Press, 1987).
