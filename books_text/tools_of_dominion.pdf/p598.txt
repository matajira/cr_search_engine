590 TOOLS OF DOMINION

little packet of ideas that followed from that ‘axiom’ soon became the
conventional wisdom: ‘Man,’ not nature, was responsible for the evil
. man’ meant the men who made and used chemicals . . .‘man’
meant industry . . . cancer was fundamcntally a political disease.”
The scientific facts prove otherwise: man’s natural environment is it-
self carcinogenic.”°
What is the biblical perspective? The Bible teaches that man is
cursed, and so is nature. Neither man nor nature is normative, ethi-
cally or biologically. Man sickens and dics because he is under a
curse, but no one environmental source is the primary cause of
man’s condition, To assume that nature is not carcinogenic is an ex-
ercise in fantasy.

Summary

There are certain kinds of damage that can become so wide-
spread that those who produce them endanger too many people. In
the case of some form of pollution that is known to be so damaging
that the producer could not possibly make restitution to those in-
jured, the State possesses the lawful authorily to prohibit or isolate
the activity. The example of fire codes is representative. Similar
codes for polluting processes can and should be worked out by ex-
perts who are hired by the government, with the politicians invoking
the required regulations, The legal justification for outright prohibi-
tion must be the known inability of damage-producers to pay their
victims, should a crisis take place. The more widespread the produc-
tion process is, and the more widespread its spillover effects, the less
likely that any one producer could afford to make restitution. Thus,
the civil government restricts the proccss.

The civil government is the necessary agent for settling disputes
that cannot be worked out voluntarily and peaceably. It is an agent
of last resort, for it uses coercion, a very dangerous monopoly to be
invoked by anyone. The public should be willing to permit people to
settle disputes over pollution on a mutually profit-seeking basis. The
most obvious cxample is to allow people to accept known environ-
mental defects in order to gain discounts on land purchases.

The assessment of risks (costs) and rewards is a cultural phenom-
enon.”? Cultural preferences are expressed locally. They are more

 

75. Ibid., p. 128.

76. Lbid., pp. 125-75.

77. Mary Douglas and Aaron Wildavsky, Risk and Culture: An Essay on the Selection.
of Technical and Environmental Dangers (Berkeley: University of California Press, 1982).
