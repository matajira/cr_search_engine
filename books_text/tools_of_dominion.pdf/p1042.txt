 

1034 TOOLS OF DOMINION

same bitter fruit of skepticism and liberalism in Jewish circles that it
has produced in Christian circles.” There was not only bitter fruit
but also forbidden fruit to be eaten. By the millions, they have
feasted on this forbidden fruit, Solomon Schechter is correct: biblical
higher criticism was in fact the “higher anti-Semitism,” for it obliter-
ated the official foundation of the Jewish experience.'#! But this was
a case of the hermeneutical chickens coming home to roost, for Juda-
ism had long undermined this original foundation through its ever-
evolving traditionalism.

Traditional Judaism’s ethical rules began to change, and there-
fore the whole religion had to change. Reform Judaism launched a
successful intellectual attack on Orthodox Judaism in the early dec-
ades of the nineteenth century, leading to the steady isolation of the
defenders of old Pharisee tradition, and in the twentieth century, sec-
ular Judaism and Conservative Judaism have become the dominant
traditions. Orthodox Judaism today retains very little influence out-
side of the state of Israel. Reform Judaism and conservative Judaism
are overwhelmingly dominant in the West. Secular Jews seem to be
the norm today, as far as gentiles can discern. (The most memorable
description I have ever read regarding the outlook of secular Jews re-
garding Judaism is Lis Harris’ description of her family, “fans whose
home team was the Jews.”)!” Anti-credalism giveth, and anti-
credalism taketh away.

120. The Jewish scholar most responsible for the introduction of higher criticism
into Jewish curricula was the extraordinary linguist, Julian Morgenstern, who also
served as president of Hebrew Union College in Cincinnati, Ohio, after 1921. Born
in 1881, be was still writing scholarly essays in the mid-1960's in the Hebrew Union Col-
loge Annual. (“The Hasidim—Who Were They?” HUCA, XXXVIIL, 1967.) Indicative
of the extent of his life’s work was his four-part study, “The Book of the Covenant.”
Part I appeared in the 1926 issue; Part If appeared in 1930; Part 11] in 1931-32; and
Part IV in 1962. He was elected president of the American Oriental Society in
1928-29 and president of the Soctety of Biblical Literature in 1941. “Morgenstern
assumed a position of pre-eminence as a philosopher and theoretician of Reform
Judaism. . .. Modern developments, he showed convincingly, are only the latest
manifestations of the adjustments that have taken place over and over whenever
Judaism has come into contact with a superior culture.” Morris Lieberman, ‘Julian
Morgenstern —Scholar, Teacher and Leader,” Hebrew Union College Annual, XXXII
(1962), p. 6. Morgenslern was a dedicated humanist and internationalist. Gf, Mor-
genstern, “Nationalism, Universalism, and World Religion,” in Charles Frederick
Walker (ed.), World Fellowship, Addresses and Messages by Leading Statesmen of AUl Faiths,
Races and Countries (New York: Liveright, 1935). This was his address to the second
Parliament of Religions, held in Chicago in 1933.

121. Cited in Cohen, Myth of Judeo-Christian Tradition, p. x

122, Lis Harris, Holy Days: The World of a Hasidic Family (New York: Summit
Books, 1985), p. 17.

 
