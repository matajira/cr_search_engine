The Epistemological Problem of Social Cost 1129

From the very beginning of modern economics in the seven-
teenth century, the use of hypothetically value-free arguments by
economists has been viewed by them as a way to escape questions of
right and wrong, of ethics. William Letwin, historian of this early
period of economic thought, is correct when he says that “there can
be no doubt that economic theory owes its present development to
the fact that some men, in thinking of economic phenomena, force-
fully suspended all judgments of theology, morality, and justice,
were willing to consider the economy as nothing more than an in-
tricate mechanism, refraining for the while from asking whether the
mechanism worked for good or evil... . The economist’s view of
the world, which the public cannot yet comfortably stomach, was in-
troduced by a remarkable four de force, an intellectual revolution
brought off in the seventeenth century.”1?7

The problem with this reliance upon mathematics is that in
removing ethics, it removes responsibility. It removes choice. This
has been the complaint of the Austrian School for many decades.
Buchanan, more an Austrian than a Chicagoan on this point, argues
that the reduction of economics to mathematics is the reduction of
man to an automaton. For the Austrian, cost is subjective. “This
genuine opportunity cost vanishes once a decision is taken. By rela-
tively sharp contrast with this, in the pure science of economic be-
havior choice is itself illusory. In the abstract model, the behavior of
the actor is predictable by an outside observer. This requires that
some criteria be objectively measurable, and this objectivity is sup-
plied when the motivational postulate is plugged into the model.”18
The scientific ideal of prediction runs head-on into the voluntarist’s
case for freedom. As Van Til describes it, this is the Kantian ideal of

 

itself. We may say of Walras what Lagrange ironically said in praise of Newton:
‘Newton was assuredly the man of genius par excellence, but we must agree (hat he
was also the luckiest: one finds only once the system of the world to be established!”
And how lucky he was that ‘in his time the system of the world still remained to be
discovered.’ Substitute ‘system of equilibrium’ for ‘system of the world’ and Walras
for Newton and the equation remains valid.” Samuelson, “Economic ‘I'heory and
Mathematics—An Appraisal,” American Economic Review, X.LM (May 1952), p. 61,
Samuelson’s appraisal concerning the importance of Walras vs. Menger is exactly
the reverse of mine, and so is his appraisal of the comparative advantages of sub-
jective value theory and marginalism vs. the concept of general equilibriam,

127, William Letwin, The Origins of Scientific Heonomics (Garden City, New York:
Doubleday Anchor, [1963] 1965), pp. 158-59.

128, James Buchanan, What Should Economists Do? (Indianapolis, Indiana: Liberty
Press, 1979), p. 46.
