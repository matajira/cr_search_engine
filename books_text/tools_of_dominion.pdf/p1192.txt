1184 TOOLS OF DOMINION

him a house. You are morally obligated to give him the use of the
loan, interest-free, or the use of the house, rent-free. This is the eco-
nomics of love.® It is also a classic crank prescription for creating a
society of homeless people. Sadly, this book is being read by other-
wise intelligent Christians who are not used to following a complex
chain of economic reasoning, so this broken chain of economic error
impresses them.

He wrote the book specifically to refute me, as his footnotes and
text reveal. He has read (but has not understood) my view of time-
preference as the true origin of interest. He recognizes that I am fol-
lowing Béhm-Bawerk and Mises on this point: that there is always a
discount for cash when you purchase an expected stream of future ser-
vices. People discount the present value of expected future goods in
comparison to the same goods in the present. Because of this, no ra-
tional person will pay a thousand ounces of gold, cash, for that hypo-
thetical gold mine.

The “Present” Is Mostly in the Future

Mr. Mooney argues that there are no future goods but only pres-
ent goods. In one sense, he is correct. I would put it this way: “The
present is all that any man can be certain he has, moment by present
moment.” He puts it this way: “Future goods do nol exist. There arc only
present goods in external reality.”? The author believes that he has
somehow refuted the concept of the inescapable discount applied to
future goods. He has not.

Future goods are not real in the present, he says, so therefore
they do not command a cash price. He does not recognize, for one
thing (among many, many others), that this non-existence of future
goods is a very good reason why there is always a risk premium in free
market interest rates: the promised future goods may not actually be
returned to the lender. Instead of acknowledging this obvious fact,
the author concludes: “Since the contemplation of ‘future goods’ is
characterized by idealism, one may not actually compare ‘present
goods’ and ‘future goods’ for purposes of economic calculation. The
preference that is dictated by the discount of the ‘future goods’ can-

6. This is the assertion of Mr, Mooney and his publisher, Mr. Wiley: iid., pp.
ili, 231-34,
7. fbid., p. 207,
