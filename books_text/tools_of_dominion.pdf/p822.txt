814 TOOLS OF DOMINION

and interpretations, then life is anarchy. But on the basis of the logic
of autonomous man, there is no sure reason to believe that there are
such connections. It may be convenient to believe that there are, if
only to make sense of reality, but there is no way to prove that em-
pathy serves as a means of unifying mankind.

But there isa link, the Bible tells us: the image of God in man. Man
is made in God’s image, and he is therefore responsible to God cove-
nantally (Gen. 1:26-27). There are common emotional and ethical
bonds in all men. These bonds can be actively suppressed, in the same
way that the knowledge of God is actively suppressed by sinful men
(Rom. 1:18-22). Nevertheless, these bonds serve as the basis of social
cooperation, which in turn requires people to make ethical judgments.

The Israelites were reminded that they had been strangers in
Egypt. They should therefore not imitate their tyrannical captors by
imposing unrighteous judgments on those who are under their God-
given authority. If they should do so, then God will remove this au-
thority from them and punish them in the same way. To escape
God's temporal covenantal judgments, men must obey God's law.
They must subordinate themselves to this law in order lawfully to execute right-
cous judgment on those beneath them. As they do unto others, so will God
do to them,

Then what about differing tastes? What about using our feelings
as guides for dealing with others? If tastes are ethically random, or
even ethically neutral, how can we rely on introspection as a guide to
external behavior? The biblical response is clear: tastes are neither
random nor ethically neutral. Tastes are inherent in men as God’s
creatures, although this testimony can be suppressed and twisted to
covenant-breaking purposes. Because of sin, tastes must be gov-
erned by the standards of God's law. The Hebrews were supposed to
remind themselves of what it meant to be an oppressed slave in a for-
eign land. They were required to eat bitter herbs each year at pass-
over (Ex. 12:8). Tastes are not random; bitter herbs for one person
will taste bitter for others. The memory of the bitterness of slavery
would be preserved by the bitter taste in people’s mouths each year
at the Passover feast.

The memory of their ancestors’ years in Egypt was important for
the life of the nation. This memory was to stay with them through
the history taught to them as children at the Passover feast (Ex.
12:26-27), in the readings from the Torah, and from their instruction
in the law. Covenant ethics and covenant history could not lawfully be sepe-
