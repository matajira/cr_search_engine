Finders Should Not Be Keepers 781

Thou shalt not see thy brother’s ox or his sheep go astray, and hide thy-
self from them: thou shalt in any case bring them again unto thy brother, And
if thy brother be not nigh unto thee, or if thou know him not, then thou
shalt bring it unto thine own house, and it shall be with thee until thy
brother seek after it, and thou shalt restore it to him again. In like manner
shalt thou do with his ass; and so shalt thou do with his raiment; and with
all lost thing of thy brother’s, which he hath lost, and thou hast found, shalt
thou do likewise: thou mayest not hide thyself (Deut. 22:1-3).

This would not be a suitable excuse three or four times. If a per-
son lives in a society that has developed an information reporting
system, he has a legal requirement to report the whereabouts of lost
articles to the civil authorities if he does not know who the owner is.
Thus, as time passes, the “excuse of the wandering animal” fades.
The owner who discovers his animal in another’s possession has a far
stronger legal case than if this case law were not in God’s law-order.
A lost animal is not supposed to remain indefinitely in another per-
son’s possession, especially after the person who lost it broadcasts its
loss publicly. “Thou shalt bring it unto thine own house, and it shall
be with thee until thy brother seek after it.”

Marks of Ownership and Reduced Search Costs

This case law makes it far more likely that a lost animal will be
immediately returned to the owner. Thus, the law increases the eco-
nomic return from marking property. This is an incentive to pro-
mote the spread of owner’s rights that can be legally protected. A
person’s property is brought under his own administration through a
mark of ownership.

By marking property, the owner reduces future search costs: his
search for the animal and the finder’s search for the owner. It reduces
search costs for a neighbor whose crops have been eaten or ruined by
a wandering beast. He can then gain restitution from the owner (Ex.
22:5). This is an incentive for someone who wants to protect his
property (the beast) from thieves or to protect his neighbor’s prop-
erty (crops) from loss by building a fence or by restraining the ani-
mal in some way.

Branding also reduces search costs for the civil authorities if the
animal should be stolen. By burning an identifying mark into an an-
imal’s flesh, or by attaching a tag to its ear or other flesh, the owner
increases risks to the thief. It also increases risks to those who would
