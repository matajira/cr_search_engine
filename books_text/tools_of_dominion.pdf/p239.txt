Servitude, Protection, and Marriage 231

Jordan has identified the source of the idea of Ham’s curse as black
skin: it first appeared in the Jewish Talmud and the Midrash.#
Moses Maimonides (“Rambam”),** the influential Spanish-born
Jewish Bible expositor and philosopher of twelfth century, who even-
tually wound up in Egypt as the court physician to the Muslim war-
rior Saladin, insisted that slaves should not be taught the Bible.**

The medieval church recognized that Christians were not to be
enslaved by infidels (Jews, Muslims), although Christians could
legally own Christian slaves and non-Christian slaves. The seven-
teenth-century Puritans, as dedicated to Old Testament law as any
Christian group in history, did not believe that the sabbatical year of
release, or any other law of mandatory release, applied to Negro
slavery, whether the slaves were Christians or not.” The price of
slaves was kept high because slave-owners could capitalize the in-
come stream of a lifetime of service, plus the lifetimes of the heirs of
the slaves.

The classic example of “Christian” slavery is probably the case of
the bequest by Christopher Codrington to London’s Socicty for the
Propagation of the Gospel (SPG) in 1710 of a plantation on Barbados
with over 300 slaves. Did the SPG release them? Hardly. In 1732, a
Codrington attorney suggested that the SPG cease branding the
chests of newly purchased slaves with “SOCIETY.” On the subject of
slave marriage, the 5PG was silent. The Society did not even enforce
a sabbath day of rest; the slaves were worked for six days, and allowed
to tend to their own plots and work on Sundays.

Nevertheless, we must recognize that these slaves had been rescued
from the culture of demonism. Those who were converted to Christ are
unquestionably better off today than they would be if they had re-

43. Winthrop D. Jordan, White Over Black: American Attitudes Toward the Negro,
1550-1812 (Chapel Hill: University of North Carolina Press, 1968), p. 18. He cites
the Babylonian Talmud (Soncino Press edition), tractate Sankedrin, vol. TI, p. 745;
Midrash Rabbah (Soncino Press edition), vol. I, p. 293. Reprinted by Bloch Pub.
Go., New York.

44. Rabbi Moshe ben Maimon.

45. Maimonides wrote: “It is forbidden for a man to teach his slave the Scrip-
tures. Ef he does teach him, however, the slave does not become free thereby.”
Maimonides, Acquisition, “Laws Concerning Slaves,” Chapter Eight, Section Bigh-
teen, p. 278.

46. David Brion Davis, Zhe Problem of Slavery in Western Culture (Ithaca, New York:
Cornell University Press, 1966), pp. 98-103.

47. Ibid., pp. 203-7. Cf. Marcus W. Jernegan, “Slavery and Conversion in the
American Colonies,” American Hisiorical Review (April 1916).

48. lbid., pp. 219-20.

  
