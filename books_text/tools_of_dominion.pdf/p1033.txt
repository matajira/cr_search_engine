Maimonides’ Code: {s Ht Biblical? 1025

vulgar must in no way be aware of the contradiction, the author accord-
ingly uses some device to conceal it by all means.”

There may be Orthodox Jews who will criticize me for going to
the Talmud and extracting these embarrassing passages for the pur-
pose of public disclosure and debate. They may say that I am misin-
terpreting these passages because I am not familiar with another oral
teaching tradition that somehow explains away these passages. This

would imply that there is a still more secret tradition, Even if this

 

criticism is correct—that a consistent, universally agreed-upon sec-
ondary secret oral teaching does exist which explains the primary
oral (now translated and printed) once-secret tradition—and even if
this additional secret oral teaching does offer interpretations that
somehow make these passages in the Talmud appear morally accept-
able, all of which I sincerely doubt, Orthodox Jews must then face
the reality of any appeal to yet another oral tradition. A tradition of
secondary oral explanations and glosses on a 1500-year-old written
version (the Talmud) of an authoritative ancient oral tradition is not
going to be regarded by outsiders (or even Orthodox Jewish insiders,
I suspect) as equally authoritative. What is printed eventually be-
comes authoritative, especially in the field of civil and criminal law.
Lawyers and casuists appeal to known written sources. The Talmud
stands as written.

Orthodox Judaism by 1952 had at long last provided the English-
speaking public with an officially sanctioned, expensively published
version of the Talmud: seemingly unexpurgated, fully annotated,
and professionally edited. Until the era of the Industrial Revolution,
the Talmud was regarded by all Jews except a handful of Karaites as
the sacred oral tradition of Judaism. Orthodox Jews should therefore
not object when a gentile reads the Talmud, cites it verbatim, and
criticizes it whenever he can demonstrate that il is obviously at odds
with non-Talmudic morality. What else did they cxpect when they
published it? They should refrain from criticizing gentiles who are
critical of the Talmud’s ethics unless they are prepared to discuss
these issues in public without appealing to the escape hatch of an
even more authoritative secret oral tradition which cannot lawfully
be revealed.

 

92. Guide 10b; p. 18.
