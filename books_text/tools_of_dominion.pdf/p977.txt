Common Grace, Eschatology, and Biblical Law 969

common grace declines. But why? Why should the epistemological
self-awareness described in Isaiah 32 necessarily lead to defeat for
the Christians? By holding to a doctrine of common grace which in-
volves the idea of the common favor of God toward all creatures (ex-
cept Satan, says Van Til), he then argues that this favor is withdrawn,
leaving the unregenerate a free hand to attack God’s elect. If com-
mon grace is linked with God’s favor, and God’s favor steadily
declines, then that other aspect of common grace, namely, God’s
restraint, must also be withdrawn. Furthermore, the third feature of
common grace, civic righteousness, must also disappear. Van Til’s
words are quite powerful:

But when all the reprobate are epistemologically self-conscious, the crack
of doom has come. The fully self-conscious reprobate will do all he can in
every dimension to destroy the people of God. So while we seek with all our
power to hasten the process of differentiation in every dimension we are yel
thankful, on the other hand, for “the day of grace,” the day of undeveloped
differentiation, Such tolerance as we receive on the part of the world is due
to this fact that we live in the earlier, rather than in the later, stage of history.
And such influence on the public situation as we can effect, whether in
socicty or in state, presupposes this undifferentiated stage of development.”

Consider the implications of what Van Til is saying. History is an
earthly threat to Christian man. Why? His amil argument is that com-
mon grace is earlier grace. It declines over time. Why? Because
God's attitude of favor declines over time with respect to the un-
regenerate, With the decline of God’s favor, the other benefits of
common grace are lost. Evil men become more thoroughly evil.

Van Til’s argument is the generally accepted one in Reformed
circles. His is the standard statement of the common grace position.
Yet as the reader should grasp by now, it is deeply flawed. It begins
with false assumptions: 1) that common grace implies common favor;
2) that this common grace-favor is reduced over time; 3) that this
loss of favor necessarily tears down the foundations of civic right-
eousness within the general culture; 4) that the amillennial vision of
the future is accurate, Thus, he concludes that the process of differ-
entiation is leading to the impotence of Christians in every sphere of
life, and that we can be thankful for having lived in the period of
“earlier” grace, meaning greater common grace.

7, Hbid., p. 85.
