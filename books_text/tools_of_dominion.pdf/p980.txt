972 TOOLS OF DOMINION

paradox of Deuteronomy 8: covenantal faithfulness to the law —ex-
ternal blessings by God in response to faithfulness—temptation to
rely on the blessings as if they were the product of man’s hands—
judgment. The blessings can Jead to disaster and impotence. There-
fore, adherence to the terms of biblical law is baste for external success.

Ethics and Dominion

As men become epistemologically self-conscious, they must face
up to reality — God's reality. Ours is a moral universe. It is governed
by a law-order which reflects the very being of God. When men
finally realize who the churls are and who the liberals are, they have
made a significant discovery. They recognize the relationship
between God’s standards and the ethical decisions of men. In short,
they come to grips with the law of God. The /aw is written in the
hearts of Christians. The work of the daw is written in the hearts of all
men. The Christians are therefore increasingly in touch with the
source of earthly power: biblical law. To match the power of the
Christians, the unregenerate must conform their actions externally
to the law of God as preached by Christians, the work of which they
already have in their hearts. The unregenerate are therefore made
far more responsible before God, simply because they have more
knowledge. They desire power. Christians will some day possess cul-
tural power through their adherence to biblical law. Therefore, un-
regenerate men will have to imitate special covenantal faithfulness
by adhering to the demands of God’s external covenants. The un-
regenerate will thereby bring down the final wrath of God upon their
heads, even as they gain external blessings due to their increased
conformity to the external requirements of biblical law, At the end of
time, they revolt.

The unregenerate have two choices: Conform themselves to bib-
licai law, or at least to the work of the law written on their hearts, or,
second, abandon law and thereby abandon power. They can gain
power only on God’s terms: acknowledgement of and conformity to
God’s law. There is no other way. Any turning from the law brings
impotence, fragmentation, and despair. Furthermore, it leaves those
with a commitment to law in the driver's seat. Increasing differentia-
tion over time, therefore, does not lead to the impotence of the
Christians. It leads to their victory culturally. They see the implica-
tions of the law more clearly. So do their enemies, The unrighteous
