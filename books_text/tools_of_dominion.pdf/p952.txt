944 TOOLS OF DOMINION

sanctions must be seen as alternatives to God’s final judgment, not
evidence for it. He must assert this if God’s final sanctions are to be
denied effectively. In order to make such an assumption believable,
the State must be given power to impose sanctions far worse than
those authorized by the Old Testament.

You cannot beat something with nothing. A Christian who is un-
willing to affirm publicly the inescapability of God’s eternal negative
covenant sanctions is also unlikely to insist on the temporal reality of
God’s negative covenant sanctions, for such temporal sanctions are
an carnest— down payment — on His final sanctions. Such sanctions-
denying Christians eventually find themselves under the civil (and
also intellectual) authority of covenant-breakers who also deny the
continuing validity of biblical law, meaning Old Testament sanc-
tions. You can’t beat something with something less. Those who assert their
defiance of covenant law the most insistently are coyenant-breakers
who affirm the autonomy of man, or who at least deny the existence
of the God of the Bible. Thus, in their quest to avoid thinking about
God’s eternal torture chamber beyond the grave, Christians have
willingly submitted in principle to temporal rule by those covenant-
breakers who deny the lake of fire with the greatest self-confidence.

On the other hand, those Christians who in history were most
willing to affirm God’s predestinated, inescapable, eternal sanctions
were also the only ones ready to insist on the covenantal necessity of
legislating the most feared of God’s negative sanctions, public execu-
tion, for every crime identified as a capital crime in the Old Testa-
ment. I am speaking of the Puritans, who did exactly this when they
were given the legal authority in history to do so, in New England:
the Massachusetts Body of Liberties (1641). The Puritans under-
stood that civil liberty begins with the civil government's enforce-
ment of God's required sanctions.

Sanctions and Civilization

Kingdom means civilization. It means either the lawful or
unlawful exercise of authority in history. In short, kingdom means
sanctions. God’s kingdom can operate with minimal sanctions in his-
tory, meaning a minimal State, only because it is authorized by God
and accepted covenantally by people who believe in God’s horrifying
negative sanctions beyond the grave. The widespread belief in hell
and the lake of fire is one of the foundations of Western liberty. It
made less necessary for social order men’s faith in a State that pos-
sesses imitation final sanctions.
