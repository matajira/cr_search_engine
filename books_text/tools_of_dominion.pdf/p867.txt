The Curse of Zero Growth 859

Speaking of the pagan cultures still in the land, God said: “I will not
drive them out from before thee in one year; lest the land become
desolate, and the beast of the field multiply against thee. By little
and little I will drive them out from before thee, until thou be in-
creased, and inherit the land” (Ex. 23:29-30).

This is an extremely important passage. First, it affirms man’s
authority over land and animals. Even the morally perverse
Canaanite tribes possessed God-given authority over the works of
nature. Men, not the beasts, are supposed to subdue the earth, Sec-
ond, this passage warns God’s covenant people against attempting to
achieve instant dominion, They must first build up their numbers,
their skills, and their capital before they can expect to reign over the
creation. Pagans possess skills and capital that are important to the
continuity of human dominion. Pagans can be competent adminis-
trators. Their labor can be used by God and society until an era
comes when God’s people are ready to exercise primary leadership in
terms of God’s law. At that point, ethical rebels will either be
regenerated through God’s grace, or else steadily replaced by the
new rulers of the land. '® Until then, God’s people must be content to
wait patiently, improving their own administrative abilities and in-
creasing their numbers. Dominion is an ethical process, a process of self-
government under God’s law.17

God promised His people a specific reward for covenantal faith-
fulness (23:25): health, including an absence of miscarriages among
both humans and domesticated animals. This conditional promise
would have enabled the Hebrews, had they remained faithful as a
nation, to have achieved cultural dominion more rapidly. Ultimately,
it would have led to the subduing of the whole earth, had the same
rate of population growth which they had experienced in Egypt been
sustained for a few more centuries.

15. The all-too-familiar statement of evolutionists that insects, especially eack-
roaches, are the true inheritors of the carth, the longest-lived of animals, the crea-
tures that endure throughout history, is fully consistent with Darwinian history. It is
also theologically perverse, I call it “cockroach eschatology”: the bugs shall inherit
the earth.

16. See Appendix A: “Common Grace, Eschatology, and Biblical Law.” Sce also
North, Dominion and Common Grace.

17. Ray R. Sutton, That You May Prosper: Dominion’ By Covenant (Tyler, ‘Texas:
Institute for Christian Economics, 1987), ch. 3.
