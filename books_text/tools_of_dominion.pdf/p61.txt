The Restoration of Biblical Casuistry 33

should reflect this fact. God also rules the church, and the govern-
ment of my church is supposed to reflect this fact. I know that God
rules all civil governments, too. So why should it be evil for Chris-
tians to work hard to see to it that the civil government reflects this
fact, just as they do in their families, churches, and businesses?” In
short, why should politics be outside the realm of God-honoring
Christian action?5¢

Humanist critics present Christians with a kind of mental image:
a scarecrow that is locked in the stocks of Puritan New England.
Every tune a Christian walks by this scarecrow, a tape recorded
message blares out: “Beware of theocracy! Beware of theocracy!” If
the critics meant, “Beware of ecclesiocracy,” meaning civil rule by the
institutional church, they would have a valid point, but they mean
something different: “Beware of Christians in every areca of life who
seek to exercise biblical dominion under God by obeying and enfarcing
God's holy law.”

What “Beware of theocracy!” really means is, “Beware of God’s
righteous rule!”

The Dismantling of the Welfare- Warfare State

Those who reject the theocratic ideal are ready to accuse Calvin-
ists of being tyrants. Historian Ronald Wells of Calvin College has
written an attack on Francis Schaeffer, which appears in a collection
of essays that is best described as a neo-evangelical tirade. He points
to the unfooinoted and unmentioncd links between certain aspects of
Schaeffer’s social thought and Christian Reconstruclionism, and
then observes: “This tendency to promote one’s own view by ‘aw’
has always been the dangerous part of Calvinism: one sees Calvin-
isis in power as triumphal and dictatorial. . . . Calvinists in power
have wielded that power oppressively.”5”

I suspect that we Reconstructionists were Mr, Wells’ target, for
we are the only Christians on earth calling for the building of a bibli-
cal theocracy. What I also suspect is that what really disturbs our
neo-evangelical academic critics is that we perceive this theocracy as
a system of decentralized power. We call for a vast purging of present-

56. George Grant, The Changing of the Guard: The Biblical Blueprint for Politics (Fe,
Worth, Texas: Dorninion Press, 1987).

57. Ronald A. Wells, “Schaefer on America,” in Ronald W. Ruegsegger (ed.),
Reflections on Erancis Schasffer (Grand Rapids, Michigan: Zondervan Academie,
1986), p. 237.
