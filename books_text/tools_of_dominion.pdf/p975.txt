Common Grace, Eschatology, and Biblical Law 967

understand knowledge, and the tonguc of the stammerers shall be ready to
speak plainly. The vile person shall be no more called liberal, nor the chur!
said to be bountiful. For the vile person will speak villany, and his heart will
work iniquity, to practise hypacrisy, and to utter error against the Lorn, to
make empty the soul of the hungry, and he wil! cause the drink of the thirsty
to fail. The instruments also of the churl are evil; he deviseth wicked devices
to destroy the poor with lying words, even when the needy speaketh right.

But the liberal deviseth liberal things: and by liberal things shall he stand.

 

To repeat, “The vile person shall be no more called liberal, nor
the churl said to be bountiful” (v. 5). Churls persist in their churlish-
ness; liberal men continue to be gracious. It does not say that all churls
will be converted, but it also does not say that the liberals shall be
destroyed. The two exist together. But the language of promise in-
dicates that Isaiah knew full well that in his day (and in our day),
churls are called liberal and vice versa, Men refuse to apply their
knowledge of God’s standards to the world in which they live. But it
shall not always be thus.

At this point, we face two crucial questions. The answers sepa-
rate many Christian commentators. First, should we expect this
knowledge to come instantaneously? Second, when this prophesied
world of epistemological self-consciousness finally dawns, which
group will be the earthly victors, churls or liberals?

The amillennialist must answer that this paralicl development of
knowledge is gradual. The postmillenialist agrees. The premillennial-
ist must dissent. The premil position is that the day of self-awareness
comes only after the Rapture and the establishment subsequently of the
earthly kingdom, with Christ ruling on earth in person. The amil posi-
tion sees no era of pre-consummation, pre-final judgment rightcous-
ness. Therefore, he must conclude that the growth in self-awareness
does separate the saved from the lost culturally, but since there is no
coming era of godly victory culturally, the amillennialist has to say
that this ethical and epistemological separation leads to the defeat of
Christians on the batilefields of culture. Evil will triumph before the
final judgment, and since this process is continuous, the decline into
darkness must be part of the process of differentiation over time.
This increase in self-knowledge therefore leads to the victory of
Satan’s forces over the church.

The postmillennialist categorically rejects such a view of knowl-
edge. As the ability of Christians to make accurate, God-honoring
judgments in history increases over lime, more authority is transferred
