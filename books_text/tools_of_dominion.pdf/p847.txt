Feasis and Citizenship 839

simply a public acknowledgment by the civil government of the individ-
ual’s change in legal status when he withdrew fromm the congregation
by ceasing to attend the feasts. It was the removal of a covenant
privilege open only to members of the congregation. The State
merely confirmed what the former congregation member had pub-
licly announced: he was no longer a citizen or judge in Israel.

Jesus and the Disinheritance of Israel

The kinsman-redeemer inherited because of the covenantal
death of the covenant-breaker. This was the legal basis for Jesus
Christ, the kinsman-redeemer and also the blood avenger of Israel,
to inherit the kingdom and to pass this inheritance to those under
His covenantal administration. Thus, Jesus prophesied to the Jews
of His day: “Therefore say I unto you, The kingdom of God shail be
taken from you, and given to a nation bringing forth the fruits
thereof” (Matt. 21:43).

Israel had renounced the ethical terms of God's covenant, despite
the fact that all the men bore the mark of covenantal subordination
in their flesh. “Woe unto you, scribes and Pharisees, hypocrites! For
ye pay tithe of mint and anise and cummin, and have omitted the
weightier matters of the law, judgment, mercy, and faith: these
ought ye to have done, and not to leave the other undone” (Matt.
23:23-24), The Jews crucified their kinsman-redeemer, Jesus Christ,
who exercised the office of blood avenger after His resurrection.
Jesus destroyed Jerusalem and the temple in 70 a.p., so that never
again could they honor the feasts. The great tribulation came in 70
a.b.18 The days of vengeance came in 70 4.017

Never again would the temple sacrifices in Jerusalem serve as a
legal covering for the nations. This meant that the Hebrews would
never again serve as judges in God’s Holy Commonwealth. Once
they had lost title to the land, they could be expelled. Once removed
from the land of promise, they no longer lawiully imposed biblical
law’s civil sanctions, either on themselves or on the gentiles.

Talmudic law recognized their new legal status. As I wrote in
Chapter 1, when the Romans captured Jerusalem and burned the
temple in A.p. 70, the ancient official Sanhedrin court came to an
end. The rabbis, under the leadership of Rabbi Johanan ben Zakkai,

16. David Chilton, The Great Tribulation (Ft. Worth, ‘lexas: Dominion Press, 1987),
17. David Chilton, The Days of Vengeance: An Exposition of the Book of Reselation
(Ft. Worth: Dominion Press, 1987).
