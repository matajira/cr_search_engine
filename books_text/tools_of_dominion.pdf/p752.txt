744 TOOLS OF DOMINION

Interest-Sccking Loans

The prohibition against usury only appears in the context of
charitable loans, The Bible does not prohibit loans that draw inter-
est in business dealings, as Jesus’ parable of the talents indicates
(Matt. 25:27), 69

Consider the problem faced by the person who argues, as medieval
theologians argued, that all interest is immoral. What if the banker
comes to the potential depositor and made this offer? “Sir, you have
money that you do not need for immediate consumption. I have sev-
eral prospects for earning money on invested capital. Let us make a
bargain. You loan me the money for a year. I, in turn, will see to it
that your money gets into the hands of low-risk borrowers who have
some excellent business opportunities, if they can only locate some
capital at reasonable rates of interest. I will retain a percentage of the
money they pay me for having located your money. This is my ser-
vice fee. But you will do much better on this loan than you could if
you loaned the money to people you know. I will save you the time,
expense, and trouble of seeking out reputable borrowers. They come
to me. That is my job.

“{ must make this stipulation, however, For the agreed-upon per-
iod of the loan, you won't be able to get your money, The money will
be used by the borrowers in their business operations, After all, we
can’t spend the same money at the same time! So you forfeit the use
of your money for a year; the borrower gets the use of your money
for a year; he pays you for the privilege of using your money, and I
will take a small percentage for my services. Everyone wins, includ-
ing consumers who will benefit from the increased production.”

This sounds good. But the lender wants security. “Mr. Banker, I
will agree to this on the following condition. I want security for my
investment. I will buy an insurance policy from you. If the busincss-
man you loan the moncy to should go bankrupt and be unable to
repay me, then you will pay me the agreed-upon rate of interest any-
way. I have to pay for this protection, of course, but you know so
many businessmen, and can spread the investments of all depositors
over so many different investments, that we all can gain greater se-
curity if you act as an insurance agent for our loans.”

Reasonable? Certainly. It is so reasonable that the medieval pro-
hibition against all interest payments, including business loans, was

69. North, Honest Money, ch. 7.
