478 TOOLS OF DOMINION

sonal liability policies —“ransoms for lives” — but they also require in-
surance companies to sell high-risk drivers this coverage.

If the law did not compel the purchase of auto insurance, or
strongly encourage it by requiring visible evidence of a driver’s abil-
ity to self-insure himself, the insurance firms would be trapped.
They could not easily pass on to low-risk drivers the added costs of
insuring high-risk people. Low-risk drivers are forced by the state to
pay higher premiums for their policies than would have been the
case had the high-risk drivers been refused coverage and thereby
forced off the roads. Without this compulsion, they would not pur-
chase the policies. The companies would then suffer losses because
of the reduced sales. In fact, they do suffer some losses; some buyers
drop coverage and drive illegally. The sellers cannot pass on all the
additional costs to buyers. *

Thus, the concern about criminals’ being able to escape justice
because of private insurance contracts is misplaced. The greater
problem is the civil government’s demand that people who are more
likely to be convicted of criminal negligence be covered by insur-
ance, whether or not they are insurable by private firms on a volun-
tary basis. It is not that the State a//ows insurance companies to pay
“ransoms for the lives” of criminally negligent people; it is rather that
the State compels private firms to sell such coverage to people or firms
that are more likely to be convicted of criminal negligence.

The State as Insurer

The State even enters as the “insurer of last resort” when no pri-
vate firms will insure extremely high-risk people or industries. One
example in the United States—which is common in Western indus-
trial nations, though not in Japan® —is the government-guaranteed
coverage for accidents connected with the generation of electricity
through nuclear power, Power companies are government-licensed

44. Part of these costs are passed on to uninsured or under-insured drivers who
would have liked the coverage but who cannot afford the higher premiums. Also, the
company’s shareholders bear some of these costs. They suffer capital losses because
the companies cannot sell policies to all those who would be willing to buy policies if
the costs were lower. It is erroneous to argue that higher costs can be passed on to
customers indiscriminately or at zero cost to companies. See Murray N, Rothbard,
Power and Market: Government and the Market Economy (Menlo Park, California: Insti-
tute for Humane Studies, 1970), pp. 66-68.

45. H. Peter Metzger, The Atomic Power Establishment (New York: Simon &
Schuster, 1972), p. 218.
