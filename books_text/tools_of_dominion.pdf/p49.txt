The Restoration of Biblical Casuistry 41

many as the ultimate danger to the fragile hold we have on whatever secur-
ity we have achieved in the present. All of this has left some to question the
meaning of their endeavors, while it has left many with a sense of isolation
and loneliness. The irony is that this new sense of insecurity has come at a
lime when the material well-being of those in the advanced industrial na-
tions has reached a height hitherto undreamed of.

This is precisely what the Book of Deuteronomy predicts for a so-
ciety that has covenanted with God, has been blessed with external
wealth, and then has forgotten God in its humanistic confidence
(Deut. 8:17): “. . . the Loro shall give thee there a trembling heart,
and failing of eyes, and sorrow of mind: And thy life shall hang in
doubt before thee; and thou shalt fear day and night, and shalt have
none assurance of thy life” (Deut. 28:65b-66). This sori of widespread
pessimism leads either to cultural collapse or military defeat, or clse to
revival. The first is taking place visibly, the second is a growing possi-
bility,*" and the third, revival, is also becoming more likely. Socielo-
gist Robert Nisbet asks this question: “[W]hat is the future of the
idea of progress? Any logical answer must be that the idea has no
futurc whatever if we assume the indefinite, prolonged continuation
of the kind of culture that has become almost universal in the West in
the late twentieth century. If the roots are dying, as they would ap-
pear to be at the present timc, how can there be shrub and foliage?”
But, he then asks, “is this contemporary Western culture likely to
continue for long? The answer, it seems to me, must be in the nega-
tive—if we take any stock in the lessons of the human past.” He
makes no absolute prophecies—much of his academic career has
been devoted to reminding us that such comprehensive cultural
prophecies are always overturned by the facts of the future — but he
is correct when he says that “never in history have periods of culture
such as our own lasted for very long.” He sees “signs of the beginning
of a religious renewal in Western civilization, notably in America.”

30, Howard J. Vogel, “A Survey and Commentary on the New Literature in Law
and Religion,” Journal of Law and Religion, 1 (1983), p. 151.

31. Arthur Robinson and Gary North, Fighting Chance: Ten Fut to Survival (Ft.
Worth, Texas: American Bureau of Economic Research, 1986)

32. Robert A. Nisbet, History of the Idew of Progress (New York: Basie Books, 1980),
p. 355-56.

33. Nisbet, “The Year 2000 And All That,” Conumeniary (June 1968)

34. Nisbet, History, p. 356.
