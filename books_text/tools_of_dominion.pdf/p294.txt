286 TOOLS OF DOMINION

Ged or Satan? They brought it against God. They served as Satan’s
agents. They implicitly claimed to be the victims of God’s discrimi-
natory restrictions against them, for God had denied them access to
the forbidden fruit, and He had obviously lied to them concerning
His power to enforce His will. They must have regarded His prom-
ised sanctions as a lie. Why else would anyone commit automatic
suicide for a bite of forbidden fruit? They brought their covenant
lawsuit against God in absentia by partaking of the forbidden fruit in
the presence of Satan, thereby indulging in a satanic sacrament, an
unholy communion service. They ate a ritual meal in the presence of
the prince of demons. This is what Paul warns against: eating at the
table of demons (I Cor. 10:21).

From the day that the serpent tempted Adam and Eve by testify-
ing falsely concerning God’s revealed word, there has been a desig-
nated victim of all criminal behavior: God. Satan needed to recruit
human accomplices in his war against God. He needed two wit-
nesses, the required number to prosecute anyone successfully for a
capital crime (Deut. 17:6). But the moment that Adam and Eve
brought their false testimony into God’s court, they became subject
to the penalty for perjury: suffering the same punishment to which
the falsely accused victim was subject (Deut. 19:16-19). If their testi-
mony had been true, then God must have lied about who is truly
sovereign over the universe. He would have given false testimony
against the true god, man. God would have been guilty of calling
man to worship a false god, which is a capital offense (Deut. 13:6-9).
He would also have been guilty of false prophesying, another capital
offense (Deut. 13:1-5), Adam and Eve had sought to indict God for a
capital offense; they were subsequently executed by God. So are all
their heirs who persist in refusing to renounce the judicial accusa-
tions of their parents, who represented them in God’s court.

In His grace, God offered them a judicial covering, a temporary
stay of execution, which was symbolized by the animal skins (Gen.
3:21), This symbolic covering required the slaying of an animal. God
offered them time on earth to repent. He offered them a way to make
restitution to Him: the blood sacrifice of specified animals. He did
this because He looked forward in time to the death of His Son on
the cross, the only possible restitution payment large enough to
cover the sin of Adam and his heirs.

His Son’s representative death is the basis of all of God’s gifts to
mankind in history. Grace is an unearned gift, meaning a gift earned by
