Feasts and Citizenship 831

sojourn with thee, and will keep the passover to the Lorn, let alll his
males be circumcised, and then let him come near and keep it; and
he shall be as one that is born in the land: for no uncircumcised per-
son shall eat thereof” (Ex. 12:48). He had to be marked as a priestly
representative of God within his own home. He had to be a member
of a judicially marked covenantal hierarchy.

Israel as a Sanctuary

This family priestly office, hierarchical in structure, opened the
door to another office, that of ctve! magistrate. To be a citizen in Israel,
a man first had to be under the jurisdiction of a family covenant,*
cither by physical birth into his own family or by adoption (including
a woman’s marriage) into a Hebrew family.* This family-based or-
der of governmental authority and office helps to explain an other-
wise difficult exegetical problem. Immediately following the passage
in Deuteronomy that deals with the feast of the tabernacles we read:
“Judges and officers shalt thou make thee in all thy gates, which the
Lorp thy God giveth thee, throughout thy tribes: and they shall
judge the people with just judgment. Thou shalt not wrest judg-
ment; thou shalt not respect persons, neither take a gift: for a gift
doth blind the eyes of the wise, and pervert the words of the right-
eous. That which is altogether just shalt thou follow, that thou
mayest live, and inherit the land which the Lorp thy God giveth
thee” (Deut. 16:18-20).

Here we find once again that the laws of the festivals are closely assoct-
ated with the laws of civil Justice. The civil judge is warned not to accept
a bribe. He shall not render false or perverted judgments. The con-
text is a court of law. The promise is that those who render righteous
judgments will live and inherit the land. All three are tied together:
required attendance at the festivals, rendering honest civic judg-
ment, and inheriting family-owned land,

4. Just as a church officer must first serve as the bead of his household (I Tim.
3:2, 4).

5. For example, Rahab and Ruth.

6. Adoptions into Hebrew households took place on a widespread basis during
the first century of Israel's stay in Egypt, which is why their population was growing
so rapidly by Moses’ day. See Gary North, Moses and Pharaoh: Dominion Religion vs.
Power Religion (Tyler, Texas: Institute for Christian Economics, 1985), ch. 1: “Popu-
lation Growth: ‘Too! of Dominion.”
