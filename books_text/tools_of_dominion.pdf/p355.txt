The Costs of Private Conflict 347

those who are not specialized historians. First, the South had civil
sanctions against duelling in the late eighteenth century, including
North Carolina’s threat to impose the death penalty without benefit
of clergy on the survivor of a duel in which a man was killed.!? (In
Tennessee, it was discovered that about 90 percent of the duels were
fought by lawyers, so a prospective lawyer was required to swear
that he would never be a participant in a duel in order to gain admis-
sion to the bar.)!® Second, the duel was relatively common in the
North. Thus, in both North and South, civil laws against duelling
were ineffectual prior to 1800. After 1800, social opinion changed in
the North, and duels disappeared. Not so in the South. Social opin-
ion was therefore the dominant force regarding the practice of duell-
ing in both regions, not civil law.

Hamilton and Burr, 1804

In the North prior to 1800, it was considered a loss of honor to
avoid a “legitimate challenge” from a man of upper class standing, a
fact revealed by Alexander Hamilton's unwillingness in 1804 to refuse
the challenge from his old political rival Aaron Burr, for fear of erod-
ing his political influence, Both were legal residents of New York
State. Burr was at the time the Vice President of the U.S. because,
under the old rules of presidential elections, he had tied with Jeffer-
son in the Electoral College vote in 1800, and Hamilton’s influencc in
the House of Representatives, which legally had to settle the tie, had
elected Hamilton's political rival Jefferson, whom Hamilton hated
Jess than he hated Burr.” Burr had actually campaigned for Gover-
nor of New York in 1804, a race which he lost a few months before
the famous duel, in part because of Hamilton’s efforts against him.

Hamilton had repeatedly insulted Burr in private conversation:
“He had insulted Burr's family, impugned his honesty, and accused
him of almost every imaginable crime from taking bribes to coward-
ice in the army.”2! Hamilton had used the word “despicable” in a pri-
vate conversation regarding Burr, and this had become public

17. Dickson 1D. Bruce, Jr., Violence and Culture in the Antebellum South (Austin:
University of Texas Press, 1979), p. 27.

18. Idem.

19. Thid., p. 42.

20. [is opposition to Burr had also cost the latter a seat in the U.S. Senate and
the governorship of New York State: Robert Hendrickson, Hamilton LI (1789-1804)
(New York: Mason/Gharter, 1976), p. 629.

21, Zbid., p. 626.
