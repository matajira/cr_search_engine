A Biblical Theology of Slavery 177

Virginia. About 80,000 Englishmen came to Virginia between 1630
and 1700, half in 1650-75. Only about 56 blacks per year were im-
ported into Virginia, 1660-90.5? Most of the laborers in Virginia
were white until the end of the century,'®

Tt was in the 1660’s that the laws of various Southern colonies be-
came openly racist, notably Virginia’s and Maryland’s.!®! From the
mid-1660’s, there was a declining pool of available white servants in
the Chesapeake region.'®* Black slaves steadily replaced white in-
dentured servants. A judicial transformation accompanied this eco-
nomic shift to black slavery. Historian Kenneth M, Stampp
describes this transformation: “During this decade various statutes
provided that Negroes were to be slaves for life, that the child was to
inherit the condition of the mother, and that Christian baptism did
not change the slave’s status. Even then it took many more years and
many additional statutes to define clearly the nature of slaves as
property, to confer upon the masters the required disciplinary
power, to enact the codes by which the slaves’ movements were sub-
jected to public control, and to give them a peculiar position in
courts of law,”'°3 Thus, what began in the Southern colonies as a
biblically sanctioned system of indentured servitude was judicially
transformed, probably beginning with the restoration in 1660 of the
anti-Puritan Charles II as King of England, into an anti-biblical form
of permanent slavery, one based exclusively on racial lines. This was
the first stage of the American South’s war against the Bible: the in-
troduction of a Bible-prohibited permanent slave system.

A similar transformation had already taken place in the British
West Indies under the reign of the Puritan saints, when sugar replaced
tobacco as the primary export crop around 1640. Chattel slavery had
been legalized in Barbados in 1636 during the reign of Charles I.1°*
Charles was executed by the Puritans in 1649. Chattel slavery was

159. Wesley Frank Craven, White, Red, and Black: The Scoenteenth-Century Virginian
(Charlottesville: University of Virginia Press, 1971), p. 86.

160. Kenneth M. Stampp, Ye Peculiar Institution: Slavery in the Ante-Bellum South
(New York: Vintage, 1956), p. 24.

161. Jordan, White Over Black, pp. 79-82.

162, See Russell R. Menard, “From Servants to Slaves: The Transformation of
the Chesapeake Labor System,” Souther Studies, XVI (1977), pp. 355-90.

163. Stampp, Peculiar Institution, pp, 22-23.

164, Richard N, Bean and Robert P. Thomas, “The Adoption of Slave Labor in
British America,” in Henry A. Gemery and Jan 8. Hogendorn (eds.), The Uncommon
Market: Essays in the Economic History of the Atlantic Slave Trade (New York: Academic
Press, 1979), p. 388,
