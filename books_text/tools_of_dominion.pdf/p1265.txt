Isaac, 252

Ishmael, 264-65

Islam, 32n, 393, 417, 1033, 1202

Israel
adoption, 674-75
adulteress, 250
architecture, 919
army of, 903-4
bastard, 249, 257
bride, 210, 250
bride price, 662
burned, 662
citizenship, 675
concubine, 251
conversion of, 302, 980-81
death of, 664
disinheritance, 851
divorced, 268, 662, 663
dowry, 250, 251
evangelism, 673
execution of, 662
extension of, 673
genetic, 266n
idolatry, 251
inheritance, 850
land, 664-65
national, 266n
ordained judges, 1055
orphan, 249, 257
pilgrims, 896
price controls, 665
prices, 665
primitive?, 387
prisons, 406
re-grafted, 85in
remarriage, 663
salvation of, 1004
sanctuary, 831-35, 892
scattered, 964
sheep, 521
subordinate, 256
warfare state?, 907
whoredom, 661

Israel (state of}, 1001-1003

Jackson, “Stonewall,” 3450

Index 1257

Jacob, 137, 212, 226, 246, 254, 256,
650, 795
Jael, 798, 1021
Jail (see Prison)
Jaki, Stanley, 417
Jamaica, 196-97
Japan, 123n, 358
Jealousy, 425, 612
Jefferson’s Bible, 1199
Jeboiada, 149
Jephthah, 149, 151, 152
Jericho, 165
Jerusalem, 267, 833
Jesus
abolitionist, 188
adoption through, 849-50
adulterous woman &, 289-94
ascension, 1074-75
atonement, 119, 543, 803
bride price, 212n, 256n, 266, 270,
276
Bridegroom, 252, 661-63
bureaucracy, 56
commandments, 677
covenant lawsuit, 289
crucifixion, 70, 963-64
divorce, 646n
family vs., 271
feast parable, 835-36
gift of grace, 286-87
gleaner, 821
head of houschold, 265n
high priest, 876
inheritor, 839
jubilee, 152, 228-30, 336
jubilee &, 232
jubilee year, 122, 125, 127, 144-47
kinsman-redeemer, 227-28, 240,
839
levirate, 228
liberation, 144-45
mercy, 295, 302
merit, 953
O.T, law, 50
overkill, 170
owns world, 534
perfection, 959
