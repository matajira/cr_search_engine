Proportional Restitution 515

How can the defrauded buyer escape these burdens? He can go
to the original owner who has already received full restitution from
the thief (or from the person who has purchased the thief as a slave),
and offer to sell the animal back to him. Once the victimized buyer
identifies himself, the thief now owes restitution to the defrauded
buyer: double restitution, minus the purchase price that the de-
frauded buyer receives from the original owner. The thief has stolen
from the buyer through fraud. As is the case with any other victim of
unconfessed theft, the defrauded buyer is entitled to double restitu-
tion from the thief. Therefore, as soon as the thief gets through pay-
ing his debt to the original owner, he then must pay the victimized
buyer the penalty payment.

If the original owner declines to buy the beast, the buyer becomes
its legal owner. The original owner does not want it back. He has also
been paid: restitution from the thief. But the defrauded buyer remains
a victim. He keeps the beast, but he is also entitled to restitution from
the thief equal to the original purchase price charged by the thief.

If the thief confesses before the trial, he can avoid the risk of the
extra payment to the defrauded buyer. Even if the victim demands
four-fold or five-fold restitution, by paying it, the thief thereby be-
comes the owner of the beast. The criminal’s act of timely confession, plus
his agreement to pray full restitution to the victim, atones judicially for the theft.26

But what about the defrauding of the buyer? I think the con-
fessed thief would owe the buyer a restitution payment of 20 percent
of the purchase price because he had involved the buyer in an illegal
transaction, Having repaid both owner and buyer, he has legiti-
mized the new ownership arrangement. The buyer has gained full
legal title to the animal plus restitution, so he is no longer a defrauded
buyer. He now has no additional complaint against the thief. He
cannot demand any additional restitution payments.

Without confession and restitution, the thief would owe the
buyer at least 100 percent restitution if discovered, which is an im-
portant economic incentive in getting the buyer to identify himself.
Thus, the thief’s silence at the trial regarding the existence of a
defrauded buyer hangs over him continually.””

26. Obviously, I am speaking here only of the earthly court, Atonement means
“covering.”

27. If the victimized buyer waits for several years before identifying the stolen
beast, the court might decide that the stolen beast has aged too much, and that it
constitutes half of the payment owed. Still, the thief would have to make the 100%
penalty payment to him.
